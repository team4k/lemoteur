﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using WebSocketSharp.Server;

namespace WafaServer
{
    public class Server
    {
        private const int MAX_CNX = 100;
        private TcpListener _listener;
        private DateTime _startTime;
        private const string MOTD = "Welcome to Wafa server ! Server uptime : {0} Connection time : {1}";
        private AsyncCallback _clientCnxCallback;
        private AsyncCallback _clientReadCallback;
        private AsyncCallback _clientWriteCallback;

        public const byte USER_JOIN = 1;
        public const byte USER_CLICK = 2;
        public const byte USER_CHAT = 3;
        public const byte USER_LEFT = 4;
        public const byte WORLD_STATE = 5;
        public const byte SERVER_MOTD = 6;
        public const byte SEND_GUID = 7;

        public Dictionary<Guid,ConnectedClient> _connectedClients;

        //for html5 client, we need a server that handle websocket
        private WebSocketServer _wsServer;

        private string _serveraddress;
        private int _serverPort;
        private int _wsport;

        public Server(IPAddress address, int Port, string WebsocketIP, int WebsocketPort)
        {
            _listener = new TcpListener(address, Port);
            _serveraddress = address.ToString();
            _serverPort = Port;
            _wsport = WebsocketPort;

            //websocket server
            _wsServer = new WebSocketServer("ws://"+WebsocketIP+":"+WebsocketPort.ToString());
            _wsServer.AddWebSocketService<WSServer>("/",() => new WSServer(this));

            _clientCnxCallback = this.TcpClientConnection;
            _clientReadCallback = this.TcpClientSendData;
            _clientWriteCallback = this.TcpServerSendData;
            _connectedClients = new Dictionary<Guid, ConnectedClient>(MAX_CNX);

        }

        public void StartServer()
        {
            _startTime = DateTime.Now;
            _listener.Start(100);

            _wsServer.Start();

            Console.Out.WriteLine("Start server on {0} port {1} ws port {2}", _serveraddress,_serverPort,_wsport);
        }

        public void AsyncAcceptClient()
        {
            if (_listener.Pending())
                _listener.BeginAcceptTcpClient(_clientCnxCallback, null);
        }

        public void StopServer()
        {
            Console.Out.WriteLine("Stop server");
            _listener.Stop();
            _wsServer.Stop();
        }

        public void TcpServerSendData(IAsyncResult ar)
        {
            ConnectedClient tmp = (ConnectedClient)ar.AsyncState;

            tmp.Stream.EndWrite(ar);
        }

        public void TcpClientConnection(IAsyncResult ar)
        {
            TcpClient connectedClient = _listener.EndAcceptTcpClient(ar);

            ConnectedClient client = new ConnectedClient(connectedClient.GetStream(), connectedClient);

            byte[] message = this.GetMessage(client, true);

            client.Stream.Write(message, 0, message.Length);

        }

        public byte[] GetMessage(ConnectedClient newClient, bool addCmd)
        {
            Console.Out.WriteLine("Receiving {0} client connection", newClient.type.ToString());

            //get first available position

            /* foreach (KeyValuePair<Tuple<int, int>, bool> pos in _positionList)
             {
                 if (pos.Value == false)
                 {
                     _positionList[pos.Key] = true;
                     newClient.oAvatar.x = pos.Key.Item1;
                     newClient.oAvatar.y = pos.Key.Item2;
                     newClient.oAvatar.clickx = newClient.oAvatar.x;
                     newClient.oAvatar.clicky = newClient.oAvatar.y;
                     break;
                 }
             }*/


            // newClient.oAvatar.id = Guid.NewGuid().ToString();
             newClient.Updated = true;

             //send info back
             MemoryStream p_stream = new MemoryStream();
             //ProtoBuf.Serializer.Serialize<Avatar>(p_stream, newClient.oAvatar);

             byte[] message = null;

             if (addCmd)
             {
                 message = new byte[p_stream.Length + 1];
                 message[0] = SEND_GUID;

                 Array.Copy(p_stream.ToArray(), 0, message, 1, p_stream.Length);
             }
             else
             {
                 message = p_stream.ToArray();
             }

            Guid newClientGuid = Guid.NewGuid();

            lock (_connectedClients)
                _connectedClients.Add(newClientGuid,newClient);

            return message;
        }

        public void TcpClientSendData(IAsyncResult ar)
        {
            ConnectedClient tmp = (ConnectedClient)ar.AsyncState;

            int byteRead = tmp.Stream.EndRead(ar);


            tmp.CurrentMessage.AddRange(tmp.buffer);

            if (tmp.Stream.DataAvailable)//still data to read
                tmp.Stream.BeginRead(tmp.buffer, 0, tmp.buffer.Length, _clientReadCallback, tmp);
            else if (byteRead != 0) //all data have been read
            {
                //get the command, first byte is the command id
                byte action = tmp.CurrentMessage[0];
                byte[] msg = tmp.CurrentMessage.ToArray();


                switch (action)
                {
                    case USER_CLICK:
                        MemoryStream stream = new MemoryStream(msg, 1, msg.Length - 1);
                        stream.Position = 0;

                        tmp.Updated = true;

                        break;
                    case USER_CHAT:
                        int message_length = tmp.CurrentMessage.Count - 1;
                        byte[] message = new byte[message_length + 1];
                        message[0] = USER_CHAT;

                        Array.Copy(tmp.CurrentMessage.ToArray(), 1, message, 1, message_length);
                        this.BroadcastToClient(message);

                        break;
                }
            }
        }

        public void BroadcastToClient(byte[] messageToBroadcast)
        {
            lock (_connectedClients)
            {
                foreach (ConnectedClient client in _connectedClients.Values)
                {
                    if (client.type == ClientType.Native)
                        client.Stream.BeginWrite(messageToBroadcast, 0, messageToBroadcast.Length, _clientWriteCallback, client);
                    else if (client.type == ClientType.Web)
                    {
                        byte[] websocketmessageToBroadcast = new byte[messageToBroadcast.Length - 1];
                        Array.Copy(messageToBroadcast, 1, websocketmessageToBroadcast, 0, websocketmessageToBroadcast.Length);

                        string b64str = Convert.ToBase64String(websocketmessageToBroadcast, Base64FormattingOptions.None);

                        client.WebsocketClient.bSend(messageToBroadcast[0].ToString() + b64str);
                    }
                }
            }

        }

        public void sendSnapshot()
        {
            MemoryStream stream = new MemoryStream();

           /* WorldState wrldState = new WorldState();

            lock (_connectedClients)
            {
                foreach (Client client in _connectedClients)
                {
                    //forge update message
                    // if (client.Updated)
                    // {
                    wrldState.player_array.Add(client.oAvatar);

                    client.Updated = false;
                    // client.oAvatar.oldX = client.oAvatar.x;
                    //client.oAvatar.oldY = client.oAvatar.y;
                    // }
                }
            }

            ProtoBuf.Serializer.Serialize<WorldState>(stream, wrldState);

            byte[] finalMessageBuffer = new byte[stream.Length + 1];

            finalMessageBuffer[0] = WORLD_STATE;

            Array.Copy(stream.ToArray(), 0, finalMessageBuffer, 1, stream.Length);

            this.BroadcastToClient(finalMessageBuffer);*/
        }

        public void tick(float msDiff)
        {
            lock (_connectedClients)
            {
                foreach (ConnectedClient client in _connectedClients.Values)
                {

                    //TODO : do server prediction base on mouse click
                    /* if ((client.oAvatar.keyState & (int)Avatar.KeyState.keyDown) > 0)
                         client.oAvatar.y += msDiff / 5;

                     if ((client.oAvatar.keyState & (int)Avatar.KeyState.keyUp) > 0)
                         client.oAvatar.y -= msDiff / 5;

                     if ((client.oAvatar.keyState & (int)Avatar.KeyState.keyRight) > 0)
                         client.oAvatar.x += msDiff / 5;

                     if ((client.oAvatar.keyState & (int)Avatar.KeyState.keyLeft) > 0)
                         client.oAvatar.x -= msDiff / 5;*/

                }
            }
        }

        public void ProcessConnectedClients()
        {
            lock (_connectedClients)
            {
                List<Guid> disconnectedClients = new List<Guid>(_connectedClients.Count);

                foreach (KeyValuePair<Guid,ConnectedClient> client in _connectedClients)
                {
                    if (client.Value.type == ClientType.Web)
                        continue;

                    if (client.Value.TcpClient.Client.Poll(0, SelectMode.SelectRead))
                    {
                        try
                        {
                            byte[] buff = new byte[1];
                            if (client.Value.TcpClient.Client.Receive(buff, SocketFlags.Peek) == 0)
                            {
                                // Client disconnected
                                disconnectedClients.Add(client.Key);
                                continue;
                            }
                        }
                        catch (SocketException ex)
                        {
                            if (ex.ErrorCode == (int)SocketError.ConnectionReset)
                            {
                                disconnectedClients.Add(client.Key);
                                continue;
                            }
                        }
                    }



                    if (client.Value.Stream.DataAvailable)
                        client.Value.Stream.BeginRead(client.Value.buffer, 0, client.Value.buffer.Length, _clientReadCallback, client);


                }

                // DiscState state = new DiscState();

                foreach (Guid disc in disconnectedClients)
                {
                    Console.Out.WriteLine("Client disconnected");
                    _connectedClients.Remove(disc);

                    //state.disc_guid.Add(disc.oAvatar.id);
                }

                //send update to other client of the disconnection
                /* if (state.disc_guid.Count > 0)
                 {
                     MemoryStream stream = new MemoryStream();

                     ProtoBuf.Serializer.Serialize<DiscState>(stream, state);

                     byte[] Message = new byte[stream.Length + 1];

                     Message[0] = USER_LEFT;

                     Array.Copy(stream.ToArray(), 0, Message, 0, stream.Length);

                     this.BroadcastToClient(Message);
                 }
                 */

                disconnectedClients.Clear();
            }

        }
    }
}
