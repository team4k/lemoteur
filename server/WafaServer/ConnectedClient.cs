﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace WafaServer
{
    public enum ClientType
    {
        Web,
        Native
    }

    public class ConnectedClient
    {

        private ClientType _type;

        public ClientType type
        {
            get
            {
                return _type;
            }
        }

        private bool _Updated = false;
        public bool Updated
        {
            get
            {
                return _Updated;
            }
            set
            {
                _Updated = value;
            }
        }

        public string Command = "join";

        private TcpClient _tcpclient;

        public TcpClient TcpClient
        {
            get
            {
                return _tcpclient;
            }
        }

        private WSServer _websocketclient;

        public WSServer WebsocketClient
        {
            get
            {
                return _websocketclient;
            }
        }

        private NetworkStream _stream;

        public NetworkStream Stream
        {
            get
            {
                return _stream;
            }
        }

        private byte[] _buffer = new byte[256];

        public byte[] buffer
        {
            get
            {
                return _buffer;
            }
        }

        private List<byte> _currentMessage = new List<byte>();

        public List<byte> CurrentMessage
        {
            get
            {
                return _currentMessage;
            }
        }

        private StringBuilder _clientmsg = new StringBuilder();

        public StringBuilder Clientmsg
        {
            get
            {
                return _clientmsg;
            }
        }

        public ConnectedClient(NetworkStream pStream, TcpClient pClient)
        {
            _stream = pStream;
            _tcpclient = pClient;
            _type = ClientType.Native;
        }

        public ConnectedClient(WSServer id)
        {
            _websocketclient = id;
            _type = ClientType.Web;
        }
    }
}
