﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace WafaServer
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] str_adr = Settings.Default.Host.Split(new char[] { '.' });

            byte[] raw_adr = new byte[] { byte.Parse(str_adr[0]), byte.Parse(str_adr[1]), byte.Parse(str_adr[2]), byte.Parse(str_adr[3]) };

            IPAddress serverAdress = new IPAddress(raw_adr);
            Server Wfserver = new Server(serverAdress, Settings.Default.Port, Settings.Default.Host, Settings.Default.WebSocketPort);

            Wfserver.StartServer();

            bool running = true;
            string cmd = string.Empty;

            TimeSpan diff = new TimeSpan();
            DateTime oldStateTime = DateTime.Now;
            DateTime oldTickTime = DateTime.Now;

            while (running)
            {
                Wfserver.AsyncAcceptClient();

                Wfserver.ProcessConnectedClients();

                DateTime newTime = DateTime.Now;
                diff = newTime - oldStateTime;
                //The difference in ms
                int msDiff = diff.Milliseconds;

                if (msDiff == 15) //send snapshot every 15 ms
                {
                    //Renew the time of the last update
                    oldStateTime = newTime;
                    Wfserver.sendSnapshot();
                }

                diff = newTime - oldTickTime;
                msDiff = diff.Milliseconds;

                if (msDiff == 40) // update world every 40 ms
                {
                    oldTickTime = newTime;

                    Wfserver.tick(msDiff);
                }


                if (Console.KeyAvailable)
                {
                    cmd = Console.ReadLine();
                    if (cmd == "stop")
                        running = false;
                }
            }

            Wfserver.StopServer();
        }
    }
}
