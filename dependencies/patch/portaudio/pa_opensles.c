/*
 * $Id$
 * Portable Audio I/O Library OpenSLES implementation
 *
 * Based on the Open Source API proposed by Ross Bencina
 * Copyright (c) 1999-2002 Ross Bencina, Phil Burk
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*
 * The text above constitutes the entire PortAudio license; however, 
 * the PortAudio community also makes the following non-binding requests:
 *
 * Any person wishing to distribute modifications to the Software is
 * requested to send the modifications to the original developer so that
 * they can be incorporated into the canonical version. It is also 
 * requested that these non-binding requests be included along with the 
 * license above.
 */

/** @file
 @ingroup common_src

 @brief Skeleton implementation of support for a host API.

 This file is provided as a starting point for implementing support for
 a new host API. It provides examples of how the common code can be used.

 @note IMPLEMENT ME comments are used to indicate functionality
 which much be customised for each implementation.
*/


#include <string.h> /* strlen() */

#include <pthread.h>
#include "pa_util.h"
#include "pa_allocation.h"
#include "pa_hostapi.h"
#include "pa_stream.h"
#include "pa_cpuload.h"
#include "pa_process.h"
#include <SLES/OpenSLES.h>
#include <SLES/OpenSLES_Android.h>
#include <android/log.h>
#include "pa_debugprint.h"
#include <unistd.h>

#include <sys/resource.h>
#include <sched.h>


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>

#define LOG_TAG "SchedPolicy"


#ifndef SCHED_NORMAL
  #define SCHED_NORMAL 0
#endif

#ifndef SCHED_BATCH
  #define SCHED_BATCH 3
#endif

#define POLICY_DEBUG 0

#define TRUE    1
#define FALSE   0
 
#define LOG PaUtil_DebugPrint
// PA_LOGAPI

/* prototypes for functions declared in this file */

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */

PaError PaOpenSLES_Initialize( PaUtilHostApiRepresentation **hostApi, PaHostApiIndex index );

#ifdef __cplusplus
}
#endif /* __cplusplus */


#define CHECK(condition) do{ if (!(condition)) { CHECK_IMPL(__FILE__, __LINE__, __func__, #condition " Assertion failed\n"); raise(SIGTRAP); } } while(0)
#include <stdarg.h>
#include <string.h>
#include <stdio.h>
#include <signal.h>

#include "pa_opensles.h"

#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_DEBUG, "portaudio-activity", __VA_ARGS__))
#define LOGW(...) ((void)__android_log_print(ANDROID_LOG_DEBUG, "portaudio-activity", __VA_ARGS__))

static void CHECK_IMPL(const char* file, unsigned long line, const char *function, const char *format, ... )
{
	char buffer[512];
	snprintf(buffer, sizeof(buffer), "[%s(%lu) | %s] ", file, line, function);
	size_t len = strlen(buffer);
	va_list args;
    va_start( args, format );
	vsnprintf(&buffer[len], sizeof(buffer)-len, format, args );
    va_end( args );

	PaUtil_DebugPrint(buffer);
}

static void Terminate( struct PaUtilHostApiRepresentation *hostApi );
static PaError IsFormatSupported( struct PaUtilHostApiRepresentation *hostApi,
                                  const PaStreamParameters *inputParameters,
                                  const PaStreamParameters *outputParameters,
                                  double sampleRate );
static PaError OpenStream( struct PaUtilHostApiRepresentation *hostApi,
                           PaStream** s,
                           const PaStreamParameters *inputParameters,
                           const PaStreamParameters *outputParameters,
                           double sampleRate,
                           unsigned long framesPerBuffer,
                           PaStreamFlags streamFlags,
                           PaStreamCallback *streamCallback,
                           void *userData );
static PaError CloseStream( PaStream* stream );
static PaError StartStream( PaStream *stream );
static PaError StopStream( PaStream *stream );
static PaError AbortStream( PaStream *stream );
static PaError IsStreamStopped( PaStream *s );
static PaError IsStreamActive( PaStream *stream );
static PaTime GetStreamTime( PaStream *stream );
static double GetStreamCpuLoad( PaStream* stream );
static PaError ReadStream( PaStream* stream, void *buffer, unsigned long frames );
static PaError WriteStream( PaStream* stream, const void *buffer, unsigned long frames );
static signed long GetStreamReadAvailable( PaStream* stream );
static signed long GetStreamWriteAvailable( PaStream* stream );

/* IMPLEMENT ME: a macro like the following one should be used for reporting
 host errors */
#define PA_SKELETON_SET_LAST_HOST_ERROR( errorCode, errorText ) \
    PaUtil_SetLastHostErrorInfo( paInDevelopment, errorCode, errorText )



#define SLESOBJ    (oslesHostApi->engineObject)
#define SLESENG    (oslesHostApi->engineEngine)

PaError PaOpenSLES_Initialize( PaUtilHostApiRepresentation **hostApi, PaHostApiIndex hostApiIndex )
{
    PA_LOGAPI_ENTER( "PaOpenSLES_Initialize" );
    LOGI("initialize");

    PaError result = paNoError;
    int i, deviceCount;
    PaOpenSLESHostApiRepresentation *oslesHostApi;
    PaDeviceInfo *deviceInfoArray;

    oslesHostApi = (PaOpenSLESHostApiRepresentation*)PaUtil_AllocateMemory( sizeof(PaOpenSLESHostApiRepresentation) );
    if( !oslesHostApi )
    {
        result = paInsufficientMemory;
        goto error;
    }

    oslesHostApi->allocations = PaUtil_CreateAllocationGroup();
    if( !oslesHostApi->allocations )
    {
        result = paInsufficientMemory;
        goto error;
    }
    
    oslesHostApi->engineObject = NULL;
    oslesHostApi->engineEngine = NULL;
    oslesHostApi->outputMixObject = NULL;

    *hostApi = &oslesHostApi->inheritedHostApiRep;
    (*hostApi)->info.structVersion = 1;
    (*hostApi)->info.type = paInDevelopment;            /* IMPLEMENT ME: change to correct type id */
    (*hostApi)->info.name = "OpenSLES";

    (*hostApi)->info.defaultInputDevice = paNoDevice;  /* IMPLEMENT ME */
    (*hostApi)->info.defaultOutputDevice = 0; /* IMPLEMENT ME */

    (*hostApi)->info.deviceCount = 0;  

    /* Init SLES */
    SLresult slresult;
    

    // create engine
    slresult = slCreateEngine(&SLESOBJ, 0, NULL, 0, NULL, NULL);
    if( SL_RESULT_SUCCESS != slresult )
    {
        result = paInvalidHostApi;
        goto error;
    }


    // realize the engine
    slresult = (*SLESOBJ)->Realize(SLESOBJ, SL_BOOLEAN_FALSE);
    if( SL_RESULT_SUCCESS != slresult )
    {
        result = paInvalidHostApi;
        goto error;
    }

    // get the engine interface, which is needed in order to create other objects
    slresult = (*SLESOBJ)->GetInterface(SLESOBJ, SL_IID_ENGINE, &SLESENG);
    if( SL_RESULT_SUCCESS != slresult )
    {
        result = paInvalidHostApi;
        goto error;
    }

    // create output mix

    slresult = (*SLESENG)->CreateOutputMix(SLESENG, &oslesHostApi->outputMixObject, 0, NULL, NULL);
    if( SL_RESULT_SUCCESS != slresult )
    {
        result = paInvalidHostApi;
        goto error;
    }

    // realize the output mix
    result = (*oslesHostApi->outputMixObject)->Realize(oslesHostApi->outputMixObject, SL_BOOLEAN_FALSE);
    if( SL_RESULT_SUCCESS != slresult )
    {
        result = paInvalidHostApi;
        goto error;
    }

    
    // For now hard-code one device
    deviceCount = 1; /* IMPLEMENT ME */
    if( deviceCount > 0 )
    {
        (*hostApi)->deviceInfos = (PaDeviceInfo**)PaUtil_GroupAllocateMemory(
                oslesHostApi->allocations, sizeof(PaDeviceInfo*) * deviceCount );
        if( !(*hostApi)->deviceInfos )
        {
            result = paInsufficientMemory;
            goto error;
        }

        /* allocate all device info structs in a contiguous block */
        deviceInfoArray = (PaDeviceInfo*)PaUtil_GroupAllocateMemory(
                oslesHostApi->allocations, sizeof(PaDeviceInfo) * deviceCount );
        if( !deviceInfoArray )
        {
            result = paInsufficientMemory;
            goto error;
        }

            i=0;
            PaDeviceInfo *deviceInfo = &deviceInfoArray[i];
            deviceInfo->structVersion = 2;
            deviceInfo->hostApi = hostApiIndex;
            deviceInfo->name = 0;  /* IMPLEMENT ME: allocate block and copy name eg: */
            const char *srcName = "AudioPlayer0";
            char *deviceName = (char*)PaUtil_GroupAllocateMemory( oslesHostApi->allocations, strlen(srcName) + 1 );
            if( !deviceName )
            {
                result = paInsufficientMemory;
                goto error;
            }
            strcpy( deviceName, srcName );
            deviceInfo->name = deviceName;

            deviceInfo->maxInputChannels = 0;  /* IMPLEMENT ME */
            deviceInfo->maxOutputChannels = 2;  /* IMPLEMENT ME */
            
            deviceInfo->defaultLowInputLatency = 0.;  /* IMPLEMENT ME */
            deviceInfo->defaultLowOutputLatency = 0.2;  /* IMPLEMENT ME */
            deviceInfo->defaultHighInputLatency = 0.;  /* IMPLEMENT ME */
            deviceInfo->defaultHighOutputLatency = 0.2;  /* IMPLEMENT ME */  

            deviceInfo->defaultSampleRate = SL_SAMPLINGRATE_44_1/1000.0; /* IMPLEMENT ME */
            
            (*hostApi)->deviceInfos[i] = deviceInfo;
            ++(*hostApi)->info.deviceCount;
    }

    (*hostApi)->Terminate = Terminate;
    (*hostApi)->OpenStream = OpenStream;
    (*hostApi)->IsFormatSupported = IsFormatSupported;

    PaUtil_InitializeStreamInterface( &oslesHostApi->callbackStreamInterface, CloseStream, StartStream,
                                      StopStream, AbortStream, IsStreamStopped, IsStreamActive,
                                      GetStreamTime, GetStreamCpuLoad,
                                      PaUtil_DummyRead, PaUtil_DummyWrite,
                                      PaUtil_DummyGetReadAvailable, PaUtil_DummyGetWriteAvailable );

    PaUtil_InitializeStreamInterface( &oslesHostApi->blockingStreamInterface, CloseStream, StartStream,
                                      StopStream, AbortStream, IsStreamStopped, IsStreamActive,
                                      GetStreamTime, PaUtil_DummyGetCpuLoad,
                                      ReadStream, WriteStream, GetStreamReadAvailable, GetStreamWriteAvailable );

LOGI("end initialize");

    return result;

error:
    if( oslesHostApi )
    {
        if( oslesHostApi->allocations )
        {
            PaUtil_FreeAllAllocations( oslesHostApi->allocations );
            PaUtil_DestroyAllocationGroup( oslesHostApi->allocations );
        }
                
        PaUtil_FreeMemory( oslesHostApi );
    }
    return result;
}


static void Terminate( struct PaUtilHostApiRepresentation *hostApi )
{
    PA_LOGAPI_ENTER( "PaOpenSLES_Terminate" );
LOGI("Terminate");
    PaOpenSLESHostApiRepresentation *oslesHostApi = (PaOpenSLESHostApiRepresentation*)hostApi;

    /*
        IMPLEMENT ME:
            - clean up any resources not handled by the allocation group
    */
    // destroy output mix object, and invalidate all associated interfaces
    if(oslesHostApi->outputMixObject)
        (*oslesHostApi->outputMixObject)->Destroy(oslesHostApi->outputMixObject);
    
    // destroy engine object, and invalidate all associated interfaces
    if(oslesHostApi->engineObject)
        (*SLESOBJ)->Destroy(oslesHostApi->engineObject);

    if( oslesHostApi->allocations )
    {
        PaUtil_FreeAllAllocations( oslesHostApi->allocations );
        PaUtil_DestroyAllocationGroup( oslesHostApi->allocations );
    }

    PaUtil_FreeMemory( oslesHostApi );
}


static int BuildSLDataFormat(SLDataFormat_PCM *slFormat, int numChannels, PaSampleFormat sampleFormat, double sampleRate )
{
    memset(slFormat, 0, sizeof(SLDataFormat_PCM));
    slFormat->formatType    = SL_DATAFORMAT_PCM;
    slFormat->samplesPerSec = (SLuint32)sampleRate * 1000;

     LOGI("sample rate  : %f",sampleRate);
     LOGI("sample rate per sec : %d",(int)slFormat->samplesPerSec);
     

    switch(sampleFormat)
    {
    case paInt8:
        slFormat->bitsPerSample = SL_PCMSAMPLEFORMAT_FIXED_8;
        break;
    case paInt16:
        slFormat->bitsPerSample = SL_PCMSAMPLEFORMAT_FIXED_16;
        break;
    case paInt24:
        slFormat->bitsPerSample = SL_PCMSAMPLEFORMAT_FIXED_24;
        break;
    case paInt32:
        slFormat->bitsPerSample = SL_PCMSAMPLEFORMAT_FIXED_32;
        break;
    case paUInt8:
    case paFloat32:
    case paCustomFormat:
    default:
        return FALSE;
    }
    
    slFormat->containerSize = slFormat->bitsPerSample;

    // Channels
    slFormat->numChannels   = (SLuint32)numChannels;
    if(numChannels==1)
        slFormat->channelMask   = SL_SPEAKER_FRONT_CENTER;
    else if(numChannels==2)
        slFormat->channelMask   = SL_SPEAKER_FRONT_LEFT | SL_SPEAKER_FRONT_RIGHT;
    else
        return FALSE; // Unsupported channel configuration

#ifdef PA_LITTLE_ENDIAN
    slFormat->endianness    = SL_BYTEORDER_LITTLEENDIAN;
#else
    slFormat->endianness    = SL_BYTEORDER_BIGENDIAN;
#endif

    return TRUE;
}


// Create or Check AudioPlayer availability for the given stream format
// if playerObject is NULL, just just the availability, otherwise create and
// return the AudioPlayer instance
static int CreateAudioPlayer(SLObjectItf *playerObject, SLEngineItf engine, SLObjectItf mix, int numChannels, PaSampleFormat sampleFormat, double sampleRate )
{
     LOGI("start create audio player");
    // Configure audio source
    SLDataLocator_AndroidSimpleBufferQueue loc_bufq = {SL_DATALOCATOR_ANDROIDSIMPLEBUFFERQUEUE, 4};


    int speakers = SL_SPEAKER_FRONT_LEFT | SL_SPEAKER_FRONT_RIGHT;

    SLDataFormat_PCM format_pcm;

    // Validate and convert format
    if(!BuildSLDataFormat(&format_pcm, numChannels, sampleFormat, sampleRate))
        return FALSE;

     LOGI("data format built");


    SLDataSource audioSrc = {&loc_bufq, &format_pcm};

    // Configure audio sink
    SLDataLocator_OutputMix loc_outmix = {SL_DATALOCATOR_OUTPUTMIX, mix};
    SLDataSink audioSnk = {&loc_outmix, NULL};

    // Create audio player
    const SLInterfaceID ids[2] = {SL_IID_ANDROIDSIMPLEBUFFERQUEUE,SL_IID_PLAYBACKRATE};
    const SLboolean req[2] = {SL_BOOLEAN_TRUE,SL_BOOLEAN_TRUE};

    SLObjectItf bqPlayerObject = NULL;

    int created = FALSE;
    SLresult result = (*engine)->CreateAudioPlayer(engine, &bqPlayerObject, &audioSrc, &audioSnk, 2, ids, req);

     LOGI("audio player created");



    if(SL_RESULT_SUCCESS==result)
    {
        // realize the player.
        result = (*bqPlayerObject)->Realize(bqPlayerObject, SL_BOOLEAN_FALSE);
          LOGI("realize audio player");
        created = SL_RESULT_SUCCESS==result;
    }
    
    // If we don't have a player pointer, the user is just checking requirements
    if( !playerObject )
    {
        // Destroy the instance if available
        if( bqPlayerObject )
            (*bqPlayerObject)->Destroy( bqPlayerObject );
    }
    else // Return the created object to the user
        *playerObject = bqPlayerObject;

 LOGI("end create audio player");

    return created;
}



static PaError IsFormatSupported( struct PaUtilHostApiRepresentation *hostApi,
                                  const PaStreamParameters *inputParameters,
                                  const PaStreamParameters *outputParameters,
                                  double sampleRate )
{
    PaOpenSLESHostApiRepresentation *oslesHostApi = (PaOpenSLESHostApiRepresentation*)hostApi;
    int inputChannelCount, outputChannelCount;
    PaSampleFormat inputSampleFormat, outputSampleFormat;
    


    if( outputParameters )
    {
        outputChannelCount = outputParameters->channelCount;
        outputSampleFormat = outputParameters->sampleFormat;
        
        LOG("IsFormatSupported Output Chan:%d Fmt:%d Rate:%lf", outputChannelCount, outputSampleFormat, sampleRate);

        /* all standard sample formats are supported by the buffer adapter,
            this implementation doesn't support any custom sample formats */
        if( outputSampleFormat & paCustomFormat )
            return paSampleFormatNotSupported;
            
        /* unless alternate device specification is supported, reject the use of
            paUseHostApiSpecificDeviceSpecification */

        if( outputParameters->device == paUseHostApiSpecificDeviceSpecification )
            return paInvalidDevice;

        /* check that output device can support outputChannelCount */
/*        if( outputChannelCount > hostApi->deviceInfos[ outputParameters->device ]->maxOutputChannels )
            return paInvalidChannelCount;*/

        /* validate outputStreamInfo */
        if( outputParameters->hostApiSpecificStreamInfo )
            return paIncompatibleHostApiSpecificStreamInfo; /* this implementation doesn't use custom stream info */

        if(!CreateAudioPlayer(NULL, oslesHostApi->engineEngine, oslesHostApi->outputMixObject, outputChannelCount, outputSampleFormat, sampleRate ))
            return paSampleFormatNotSupported;
    }
    else
    {
        outputChannelCount = 0;
    }

    /* suppress unused variable warnings */
    (void) sampleRate;

    return paFormatIsSupported;
}


// this callback handler is called by opensles every time a buffer finishes enqueued
static void bqPlayerCallback(SLAndroidSimpleBufferQueueItf bq, void *context)
{
     PaOpenSLESStream *stream = (PaOpenSLESStream*)context;
    

     int bytes2 = stream->outBufSamples*sizeof(short);

     SLresult result = (*stream->playerBufferQueue)->Enqueue(stream->playerBufferQueue,&stream->outrb->buffer[stream->outrb->rp], bytes2);

     move_read_pointer(stream->outrb,bytes2);

      if(result != SL_RESULT_SUCCESS)
          LOGI("Error while enqueuing buffer %d",(int)result);
}

//the audio thread loop
static void* AudioThreadLoop( void *userData )
{
    PaOpenSLESStream *stream = (PaOpenSLESStream*)userData;

     volatile int tmpisRunning = 0;

     __sync_val_compare_and_swap(&tmpisRunning,tmpisRunning,stream->isRunning);

     int s = setpriority(PRIO_PROCESS, gettid(), -16);

     if (s != 0) 
         LOGI("set audio thread priority failed!");


     while( tmpisRunning )
     {

          int test_read =   checkspace_circular_buffer(stream->outrb,0); 

          if(test_read >= (stream->framesPerHostCallback*sizeof(short)) * 1.5) //if we are 1.5 time ahead of the read buffer, let the thread sleep and return
          {
               usleep(10000);
			   __sync_val_compare_and_swap(&tmpisRunning,tmpisRunning,stream->isRunning);
               continue;
          }
               
           

         PaStreamCallbackTimeInfo timeInfo = {0,0,0}; /* IMPLEMENT ME */
         int callbackResult;
         unsigned long framesProcessed;
         
         PaUtil_BeginCpuLoadMeasurement( &stream->cpuLoadMeasurer );
         

         PaUtil_BeginBufferProcessing( &stream->bufferProcessor, &timeInfo, 0 /* IMPLEMENT ME: pass underflow/overflow flags when necessary */ );

         PaUtil_SetOutputFrameCount( &stream->bufferProcessor, 0);
         PaUtil_SetInterleavedOutputChannels( &stream->bufferProcessor,
                 0, /* first channel of outputBuffer is channel 0 */
                 stream->outputBuffer,
                 0 ); /* 0 - use outputChannelCount passed to init buffer processor */


         callbackResult = paContinue;
         framesProcessed = PaUtil_EndBufferProcessing( &stream->bufferProcessor, &callbackResult );


          int  bytes = (framesProcessed * stream->bufferProcessor.outputChannelCount) *   sizeof(short);
          
         /*
             If you need to byte swap or shift outputBuffer to convert it to
             host format, do it here.
         */

          


         write_circular_buffer_bytes(stream->outrb, (char *) stream->outputBuffer,bytes);
          //LOGI("write %d bytes in buffer",bytes);

         stream->time += (double) framesProcessed/(stream->samplerate*stream->bufferProcessor.outputChannelCount);

         PaUtil_EndCpuLoadMeasurement( &stream->cpuLoadMeasurer, framesProcessed );


         if( callbackResult == paContinue )
         {
             /* nothing special to do */
         }
         else if( callbackResult == paAbort )
         {
               __sync_val_compare_and_swap(&stream->isRunning,stream->isRunning,0);

             /* once finished, call the finished callback */
             if( stream->streamRepresentation.streamFinishedCallback != 0 )
                 stream->streamRepresentation.streamFinishedCallback( stream->streamRepresentation.userData );
         }
         else
         {
               __sync_val_compare_and_swap(&stream->isRunning,stream->isRunning,0);
             /* User callback has asked us to stop with paComplete or other non-zero value */

             /* IMPLEMENT ME - finish playback once currently queued audio has completed  */

             /* once finished, call the finished callback */
             if( stream->streamRepresentation.streamFinishedCallback != 0 )
                 stream->streamRepresentation.streamFinishedCallback( stream->streamRepresentation.userData );
         }

        __sync_val_compare_and_swap(&tmpisRunning,tmpisRunning,stream->isRunning);

     }

     pthread_exit(0);
}


/* see pa_hostapi.h for a list of validity guarantees made about OpenStream parameters */

static PaError OpenStream( struct PaUtilHostApiRepresentation *hostApi,
                           PaStream** s,
                           const PaStreamParameters *inputParameters,
                           const PaStreamParameters *outputParameters,
                           double sampleRate,
                           unsigned long framesPerBuffer,
                           PaStreamFlags streamFlags,
                           PaStreamCallback *streamCallback,
                           void *userData )
{
    PaError result = paNoError;
    PaOpenSLESHostApiRepresentation *oslesHostApi = (PaOpenSLESHostApiRepresentation*)hostApi;
    PaOpenSLESStream *stream = 0;
    unsigned long framesPerHostBuffer = framesPerBuffer; /* these may not be equivalent for all implementations */
    int inputChannelCount, outputChannelCount;
    PaSampleFormat inputSampleFormat, outputSampleFormat;
    PaSampleFormat hostInputSampleFormat, hostOutputSampleFormat;
    SLObjectItf audioPlayer = 0;

LOGI("Open stream");


    if( outputParameters )
    {
        outputChannelCount = outputParameters->channelCount;
        outputSampleFormat = outputParameters->sampleFormat;
        
        /* unless alternate device specification is supported, reject the use of
            paUseHostApiSpecificDeviceSpecification */

        if( outputParameters->device == paUseHostApiSpecificDeviceSpecification )
            return paInvalidDevice;

        /* check that output device can support inputChannelCount */
        if( outputChannelCount > hostApi->deviceInfos[ outputParameters->device ]->maxOutputChannels )
            return paInvalidChannelCount;

        /* validate outputStreamInfo */
        if( outputParameters->hostApiSpecificStreamInfo )
            return paIncompatibleHostApiSpecificStreamInfo; /* this implementation doesn't use custom stream info */

        /* IMPLEMENT ME - establish which  host formats are available */
//        hostOutputSampleFormat =
//            PaUtil_SelectClosestAvailableFormat( paInt16 /* native formats */, outputSampleFormat ); 
        
        if(!CreateAudioPlayer(&audioPlayer, oslesHostApi->engineEngine, oslesHostApi->outputMixObject, outputChannelCount, outputSampleFormat, sampleRate ))
            return paSampleFormatNotSupported;
        hostOutputSampleFormat = outputSampleFormat;
    }
    else
    {
        outputChannelCount = 0;
        outputSampleFormat = hostOutputSampleFormat = paInt16; /* Surpress 'uninitialized var' warnings. */
    }

    /*
        IMPLEMENT ME:

        ( the following two checks are taken care of by PaUtil_InitializeBufferProcessor() FIXME - checks needed? )

            - check that input device can support inputSampleFormat, or that
                we have the capability to convert from outputSampleFormat to
                a native format

            - check that output device can support outputSampleFormat, or that
                we have the capability to convert from outputSampleFormat to
                a native format

            - if a full duplex stream is requested, check that the combination
                of input and output parameters is supported

            - check that the device supports sampleRate

            - alter sampleRate to a close allowable rate if possible / necessary

            - validate suggestedInputLatency and suggestedOutputLatency parameters,
                use default values where necessary
    */

    /* KK abort if no valid number of channels selected */
    if( inputChannelCount == 0 && outputChannelCount == 0 )
        return paInvalidChannelCount;


    /* validate platform specific flags */
    if( (streamFlags & paPlatformSpecificFlags) != 0 )
        return paInvalidFlag; /* unexpected platform specific flag */


    stream = (PaOpenSLESStream*)PaUtil_AllocateMemory( sizeof(PaOpenSLESStream) );
    if( !stream )
    {
        result = paInsufficientMemory;
        goto error;
    }

    memset(stream, 0, sizeof(PaOpenSLESStream));


    if( streamCallback )
    {
        PaUtil_InitializeStreamRepresentation( &stream->streamRepresentation,
                                               &oslesHostApi->callbackStreamInterface, streamCallback, userData );

      LOGI("bind callback");
    }
    else
    {
        PaUtil_InitializeStreamRepresentation( &stream->streamRepresentation,
                                               &oslesHostApi->blockingStreamInterface, streamCallback, userData );
    }

    PaUtil_InitializeCpuLoadMeasurer( &stream->cpuLoadMeasurer, sampleRate );


    /* we assume a fixed host buffer size in this example, but the buffer processor
        can also support bounded and unknown host buffer sizes by passing 
        paUtilBoundedHostBufferSize or paUtilUnknownHostBufferSize instead of
        paUtilFixedHostBufferSize below. */
        
    result =  PaUtil_InitializeBufferProcessor( &stream->bufferProcessor,
              inputChannelCount, inputSampleFormat, hostInputSampleFormat,
              outputChannelCount, outputSampleFormat, hostOutputSampleFormat,
              sampleRate, streamFlags, framesPerBuffer,
              framesPerHostBuffer, paUtilFixedHostBufferSize,
              streamCallback, userData );
    if( result != paNoError )
        goto error;


    /*
        IMPLEMENT ME: initialise the following fields with estimated or actual
        values.
    */
    stream->streamRepresentation.streamInfo.inputLatency =
            (PaTime)PaUtil_GetBufferProcessorInputLatencyFrames(&stream->bufferProcessor) / sampleRate; /* inputLatency is specified in _seconds_ */
    stream->streamRepresentation.streamInfo.outputLatency =
            (PaTime)PaUtil_GetBufferProcessorOutputLatencyFrames(&stream->bufferProcessor) / sampleRate; /* outputLatency is specified in _seconds_ */
    stream->streamRepresentation.streamInfo.sampleRate = sampleRate;

    /*
        IMPLEMENT ME:
            - additional stream setup + opening
    */

    stream->framesPerHostCallback = framesPerHostBuffer;
    stream->audioPlayer = audioPlayer;

    if(audioPlayer)
    {
         stream->samplerate = sampleRate;
        //buffers allocations
        
          //buffer storing data from user defined callback
          stream->outBufSamples = framesPerBuffer*outputChannelCount;
          stream->outputBuffer = (short *) PaUtil_AllocateMemory((stream->outBufSamples * Pa_GetSampleSize(outputSampleFormat)) * 4); //output of 4k per callback
//stream->outBufSamples *  Pa_GetSampleSize(outputSampleFormat));


          LOGI("output buffer of %d",(stream->outBufSamples * Pa_GetSampleSize(outputSampleFormat)) * 4);

          if(!stream->outputBuffer)
          {
            result = paInsufficientMemory;
            goto error;
          }


          int bytes = stream->framesPerHostCallback*Pa_GetSampleSize(outputSampleFormat);

          stream->emptybuffer = (short *) PaUtil_AllocateMemory(bytes);

          memset(stream->emptybuffer,0,bytes);

          if(!stream->emptybuffer)
          {
            result = paInsufficientMemory;
            goto error;
          }

          //ring buffer for exchange between opensl and audio thread
          stream->outrb = create_circular_buffer((stream->outBufSamples * Pa_GetSampleSize(outputSampleFormat)) * 32); //test with 262k ring buffer

          LOGI("ring buffer of %d",(stream->outBufSamples * Pa_GetSampleSize(outputSampleFormat)) * 32);
//stream->outBufSamples*Pa_GetSampleSize(outputSampleFormat)*16);

          if(!stream->outrb)
          {
               result = paInsufficientMemory;
               goto error;
          }
          
          LOGI("intialize audioplayer interfaces");


        // get the play interface
        result = (*audioPlayer)->GetInterface( audioPlayer, SL_IID_PLAY, &stream->audioPlayerPlay );
        if(SL_RESULT_SUCCESS != result)
            goto error;

        // get the buffer queue interface
        result = (*audioPlayer)->GetInterface( audioPlayer, SL_IID_ANDROIDSIMPLEBUFFERQUEUE, &stream->playerBufferQueue );
        if(SL_RESULT_SUCCESS != result)
            goto error;


          //get the playback rate interface
        result = (*audioPlayer)->GetInterface(audioPlayer,SL_IID_PLAYBACKRATE, &stream->playbackRate);

         if(SL_RESULT_SUCCESS != result)
            goto error;

       SLuint32 capa;
        result = (*stream->playbackRate)->GetRateRange(stream->playbackRate, 0,
                &stream->playbackMinRate, &stream->playbackMaxRate, &stream->playbackRateStepSize, &capa);

         if(SL_RESULT_SUCCESS != result)
            goto error;


          result = (*stream->playbackRate)->SetPropertyConstraints(stream->playbackRate,SL_RATEPROP_NOPITCHCORAUDIO);

          

        // register callback on the buffer queue
        result = (*stream->playerBufferQueue)->RegisterCallback( stream->playerBufferQueue, bqPlayerCallback, stream );
        if(SL_RESULT_SUCCESS != result)
            goto error;
    }


    *s = (PaStream*)stream;

    return result;

error:
    if( audioPlayer )
        (*audioPlayer)->Destroy( audioPlayer );

     if( stream )
     {
          if( stream->outputBuffer )
               free( stream->outputBuffer );

          stream->outputBuffer = NULL;

          if(stream->emptybuffer)
               free(stream->emptybuffer);

          stream->emptybuffer = NULL;


          free_circular_buffer(stream->outrb);

          PaUtil_FreeMemory( stream );
     }

    return result;
}

static void opensl_pause(PaOpenSLESStream *p) {

  if (p->audioPlayerPlay) {
    (*p->audioPlayerPlay)->SetPlayState(p->audioPlayerPlay,
        SL_PLAYSTATE_PAUSED);
  }

  // Note: The current documentation of OpenSL explicitly doesn't rule out that
  // buffer queue callbacks may be invoked after the buffer queue has been
  // stopped.  For the time being, we'll just sleep for a tenth of a second
  // and hope that that's enough to make sure that this stream will truly be
  // paused when we exit this method.
  //
  // TODO: Determine whether this can actually happen and handle it in a way
  // that's provably correct.
  //usleep(100000);
}

static void opensl_stop(PaOpenSLESStream *p) {

  if (p->audioPlayerPlay) {
    (*p->audioPlayerPlay)->SetPlayState(p->audioPlayerPlay,
        SL_PLAYSTATE_STOPPED);
    (*p->playerBufferQueue)->Clear(p->playerBufferQueue);
  }

}



/*
    When CloseStream() is called, the multi-api layer ensures that
    the stream has already been stopped or aborted.
*/
static PaError CloseStream( PaStream* s )
{
    PaError result = paNoError;
    PaOpenSLESStream *stream = (PaOpenSLESStream*)s;


    __sync_val_compare_and_swap(&stream->isRunning,stream->isRunning,0);

     opensl_stop(s);

    /*
        IMPLEMENT ME:
            - additional stream closing + cleanup
    */

    PaUtil_TerminateBufferProcessor( &stream->bufferProcessor );
    PaUtil_TerminateStreamRepresentation( &stream->streamRepresentation );

     if( stream->outputBuffer )
          free( stream->outputBuffer );

     stream->outputBuffer = NULL;

     if(stream->emptybuffer)
               free(stream->emptybuffer);

     stream->emptybuffer = NULL;



     free_circular_buffer(stream->outrb);

    PaUtil_FreeMemory( stream );

LOGI("close stream");

    return result;
}


static PaError StartStream( PaStream *s )
{
    PaError result = paNoError;
    PaOpenSLESStream *stream = (PaOpenSLESStream*)s;



    PaUtil_ResetBufferProcessor( &stream->bufferProcessor );

     //first call to opensl, enqueue empty buffer to kick off buffer sequence

     __sync_val_compare_and_swap(&stream->isRunning,stream->isRunning,1);


     pthread_create(&stream->audio_thread,NULL,AudioThreadLoop,stream);


     int bytes = stream->framesPerHostCallback*sizeof(short);
     
     (*stream->playerBufferQueue)->Enqueue(stream->playerBufferQueue,stream->emptybuffer,bytes);

     if ((*stream->audioPlayerPlay)->SetPlayState(stream->audioPlayerPlay,
           SL_PLAYSTATE_PLAYING) != SL_RESULT_SUCCESS) {
          result = paUnanticipatedHostError;
		goto start_error;
    }



     

     LOGI("start stream");

    return result;

start_error:

	opensl_stop(s);
	return result;


}


static PaError StopStream( PaStream *s )
{

LOGI("start stop stream");
    PaError result = paNoError;
    PaOpenSLESStream *stream = (PaOpenSLESStream*)s;

     __sync_val_compare_and_swap(&stream->isRunning,stream->isRunning,0);
    opensl_pause(s);
   

LOGI("end stop stream");

    return result;
}


static PaError AbortStream( PaStream *s )
{

LOGI("start abort stream");
    PaError result = paNoError;
    PaOpenSLESStream *stream = (PaOpenSLESStream*)s;

     __sync_val_compare_and_swap(&stream->isRunning,stream->isRunning,0);
    opensl_pause(s);
    

LOGI("end abort stream");

    return result;
}


static PaError IsStreamStopped( PaStream *s )
{
    PaOpenSLESStream *stream = (PaOpenSLESStream*)s;

     volatile int tmpIsRunning = 0;

     __sync_val_compare_and_swap(&tmpIsRunning,tmpIsRunning,stream->isRunning);

    return !tmpIsRunning;
}


static PaError IsStreamActive( PaStream *s )
{
    PaOpenSLESStream *stream = (PaOpenSLESStream*)s;

     volatile int tmpIsRunning = 0;
     __sync_val_compare_and_swap(&tmpIsRunning,tmpIsRunning,stream->isRunning);

    return tmpIsRunning;
}


static PaTime GetStreamTime( PaStream *s )
{
    PaOpenSLESStream *stream = (PaOpenSLESStream*)s;

    return stream->time;
}


static double GetStreamCpuLoad( PaStream* s )
{
    PaOpenSLESStream *stream = (PaOpenSLESStream*)s;

    return PaUtil_GetCpuLoad( &stream->cpuLoadMeasurer );
}


/*
    As separate stream interfaces are used for blocking and callback
    streams, the following functions can be guaranteed to only be called
    for blocking streams.
*/

static PaError ReadStream( PaStream* s,
                           void *buffer,
                           unsigned long frames )
{
    PaOpenSLESStream *stream = (PaOpenSLESStream*)s;

    /* suppress unused variable warnings */
    (void) buffer;
    (void) frames;
    (void) stream;
    
    /* IMPLEMENT ME, see portaudio.h for required behavior*/

    return paNoError;
}


static PaError WriteStream( PaStream* s,
                            const void *buffer,
                            unsigned long frames )
{
    PaOpenSLESStream *stream = (PaOpenSLESStream*)s;

    /* suppress unused variable warnings */
    (void) buffer;
    (void) frames;
    (void) stream;
    
    /* IMPLEMENT ME, see portaudio.h for required behavior*/

    return paNoError;
}


static signed long GetStreamReadAvailable( PaStream* s )
{
    PaOpenSLESStream *stream = (PaOpenSLESStream*)s;

    /* suppress unused variable warnings */
    (void) stream;
    
    /* IMPLEMENT ME, see portaudio.h for required behavior*/

    return 0;
}


static signed long GetStreamWriteAvailable( PaStream* s )
{
    PaOpenSLESStream *stream = (PaOpenSLESStream*)s;

    /* suppress unused variable warnings */
    (void) stream;
    
    /* IMPLEMENT ME, see portaudio.h for required behavior*/

    return 0;
}


//circular buffer implementation
 
circular_buffer* create_circular_buffer(int bytes)
{
     circular_buffer *p;

     if ((p = calloc(1, sizeof(circular_buffer))) == NULL) {
          return NULL;
     }

     p->size = bytes;
     p->wp = p->rp = 0;

     if ((p->buffer = calloc(bytes, sizeof(char))) == NULL) {
          free (p);
          return NULL;
     }
     return p;
}
 
int checkspace_circular_buffer(circular_buffer *p, int writeCheck){
    int wp = 0;
     int rp = 0;
     int size = 0;

     __sync_val_compare_and_swap(&wp,wp,p->wp);

     __sync_val_compare_and_swap(&rp,rp,p->rp);

     __sync_val_compare_and_swap(&size,size,p->size);


     if(writeCheck){
          if (wp > rp) 
               return rp - wp + size - 1;
          else if (wp < rp) 
               return rp - wp - 1;
          else 
               return size - 1;
     }
     else {
          if (wp > rp) 
               return wp - rp;
          else if (wp < rp) 
               return wp - rp + size;
          else 
               return 0;
     }
}
 
int read_circular_buffer_bytes(circular_buffer *p, char *out, int bytes){
     int remaining;
     int bytesread;
     int size; 
     int i=0; 
     int rp;

     __sync_val_compare_and_swap(&size,size,p->size);

     __sync_val_compare_and_swap(&rp,rp,p->rp);

     char *buffer = p->buffer; 

     if ((remaining = checkspace_circular_buffer(p, 0)) == 0) {
          return 0;
     }
     bytesread = bytes > remaining ? remaining : bytes;
     for(i=0; i < bytesread; i++){
          out[i] = buffer[rp++];
          if(rp == size) 
               rp = 0;
     }

     __sync_val_compare_and_swap(&p->rp,p->rp,rp);
     return bytesread;
}


int move_read_pointer(circular_buffer* p,int bytes)
{
      int remaining;
     int bytesread;
     int size;
     int i=0;
     int rp = 0;

     __sync_val_compare_and_swap(&size,size,p->size);

     __sync_val_compare_and_swap(&rp,rp,p->rp);

     
     if ((remaining = checkspace_circular_buffer(p, 0)) == 0) {
          return 0;
     }

      bytesread = bytes > remaining ? remaining : bytes;

     rp += bytesread;

     if(rp >= size)
          rp = 0;


     __sync_val_compare_and_swap(&p->rp,p->rp,rp);

     return bytesread;
}


int write_circular_buffer_bytes(circular_buffer *p, const char *in, int bytes){
     int remaining;
     int byteswrite;
     int size;
     int i=0;
     int wp = 0;

     __sync_val_compare_and_swap(&size,size,p->size);

     __sync_val_compare_and_swap(&wp,wp,p->wp);  


     char *buffer = p->buffer;

     if ((remaining = checkspace_circular_buffer(p, 1)) == 0) {
          return 0;
     }
     byteswrite = bytes > remaining ? remaining : bytes;
     for(i=0; i < byteswrite; i++){
          buffer[wp++] = in[i];
          if(wp == size) 
               wp = 0;
     }
     
     __sync_val_compare_and_swap(&p->wp,p->wp,wp);   

     return byteswrite;
}

 
void free_circular_buffer (circular_buffer *p){
     if(p == NULL) 
          return;
     free(p->buffer);
     free(p);
}


static int __sys_supports_schedgroups = -1;

static int add_tid_to_cgroup(int tid, const char *grp_name)
{
    int fd;
    char path[255];
    char text[64];

    sprintf(path, "/dev/cpuctl/%s/tasks", grp_name);

    if ((fd = open(path, O_WRONLY)) < 0) {
        LOGI("add_tid_to_cgroup failed to open '%s' (%s)\n", path,
             strerror(errno));
        return -1;
    }

    sprintf(text, "%d", tid);
    if (write(fd, text, strlen(text)) < 0) {
        close(fd);
	/*
	 * If the thread is in the process of exiting,
	 * don't flag an error
	 */
	if (errno == ESRCH)
		return 0;
        LOGI("add_tid_to_cgroup failed to write '%s' (%s)\n", path,
             strerror(errno));
        return -1;
    }

    close(fd);
    return 0;
}

static inline void initialize()
{
    if (__sys_supports_schedgroups < 0) {
        if (!access("/dev/cpuctl/tasks", F_OK)) {
            __sys_supports_schedgroups = 1;
        } else {
            __sys_supports_schedgroups = 0;
        }
    }
}

/*
 * Try to get the scheduler group.
 *
 * The data from /proc/<pid>/cgroup looks (something) like:
 *  2:cpu:/bg_non_interactive
 *  1:cpuacct:/
 *
 * We return the part after the "/", which will be an empty string for
 * the default cgroup.  If the string is longer than "bufLen", the string
 * will be truncated.
 */
static int getSchedulerGroup(int tid, char* buf, size_t bufLen)
{
#ifdef HAVE_ANDROID_OS
    char pathBuf[32];
    char lineBuf[256];
    FILE *fp;

    snprintf(pathBuf, sizeof(pathBuf), "/proc/%d/cgroup", tid);
    if (!(fp = fopen(pathBuf, "r"))) {
        return -1;
    }

    while(fgets(lineBuf, sizeof(lineBuf) -1, fp)) {
        char *next = lineBuf;
        char *subsys;
        char *grp;
        size_t len;

        /* Junk the first field */
        if (!strsep(&next, ":")) {
            goto out_bad_data;
        }

        if (!(subsys = strsep(&next, ":"))) {
            goto out_bad_data;
        }

        if (strcmp(subsys, "cpu")) {
            /* Not the subsys we're looking for */
            continue;
        }

        if (!(grp = strsep(&next, ":"))) {
            goto out_bad_data;
        }
        grp++; /* Drop the leading '/' */
        len = strlen(grp);
        grp[len-1] = '\0'; /* Drop the trailing '\n' */

        if (bufLen <= len) {
            len = bufLen - 1;
        }
        strncpy(buf, grp, len);
        buf[len] = '\0';
        fclose(fp);
        return 0;
    }

    LOGI("Failed to find cpu subsys");
    fclose(fp);
    return -1;
 out_bad_data:
    LOGI("Bad cgroup data {%s}", lineBuf);
    fclose(fp);
    return -1;
#else
    errno = ENOSYS;
    return -1;
#endif
}

int get_sched_policy(int tid, SchedPolicy *policy)
{
    initialize();

    if (__sys_supports_schedgroups) {
        char grpBuf[32];
        if (getSchedulerGroup(tid, grpBuf, sizeof(grpBuf)) < 0)
            return -1;
        if (grpBuf[0] == '\0') {
            *policy = SP_FOREGROUND;
        } else if (!strcmp(grpBuf, "bg_non_interactive")) {
            *policy = SP_BACKGROUND;
        } 
        else if(!strcmp(grpBuf, "fg_boost")) {
            *policy = SP_AUDIO;
        }
        else {
            errno = ERANGE;
            return -1;
        }
    } else {
        int rc = sched_getscheduler(tid);
        if (rc < 0)
            return -1;
        else if (rc == SCHED_NORMAL)
            *policy = SP_FOREGROUND;
        else if (rc == SCHED_BATCH)
            *policy = SP_BACKGROUND;
        else {
            errno = ERANGE;
            return -1;
        }
    }
    return 0;
}

int set_sched_policy(int tid, SchedPolicy policy)
{
    initialize();

#if POLICY_DEBUG
    char statfile[64];
    char statline[1024];
    char thread_name[255];
    int fd;

    sprintf(statfile, "/proc/%d/stat", tid);
    memset(thread_name, 0, sizeof(thread_name));

    fd = open(statfile, O_RDONLY);
    if (fd >= 0) {
        int rc = read(fd, statline, 1023);
        close(fd);
        statline[rc] = 0;
        char *p = statline;
        char *q;

        for (p = statline; *p != '('; p++);
        p++;
        for (q = p; *q != ')'; q++);

        strncpy(thread_name, p, (q-p));
    }
    if (policy == SP_BACKGROUND) {
        LOGI("vvv tid %d (%s)", tid, thread_name);
    } else if (policy == SP_FOREGROUND) {
        LOGI("^^^ tid %d (%s)", tid, thread_name);
    } else {
        LOGI("??? tid %d (%s)", tid, thread_name);
    }
#endif

    if (__sys_supports_schedgroups) {
        const char *grp = "";

        if (policy == SP_BACKGROUND) {
            grp = "bg_non_interactive";
        }
        else if(policy == SP_AUDIO) {
            grp = "fg_boost";
        }

        if (add_tid_to_cgroup(tid, grp)) {
            if (errno != ESRCH && errno != ENOENT)
                return -errno;
        }
    } else {
        struct sched_param param;

        param.sched_priority = 0;
        sched_setscheduler(tid,
                           (policy == SP_BACKGROUND) ?
                            SCHED_BATCH : SCHED_NORMAL,
                           &param);
    }

    return 0;
}




