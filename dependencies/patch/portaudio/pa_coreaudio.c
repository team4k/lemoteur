/*
 * $Id$
 * Portable Audio I/O Library OpenSLES implementation
 *
 * Based on the Open Source API proposed by Ross Bencina
 * Copyright (c) 1999-2002 Ross Bencina, Phil Burk
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*
 * The text above constitutes the entire PortAudio license; however, 
 * the PortAudio community also makes the following non-binding requests:
 *
 * Any person wishing to distribute modifications to the Software is
 * requested to send the modifications to the original developer so that
 * they can be incorporated into the canonical version. It is also 
 * requested that these non-binding requests be included along with the 
 * license above.
 */

/** @file
 @ingroup common_src

 @brief Skeleton implementation of support for a host API.

 This file is provided as a starting point for implementing support for
 a new host API. It provides examples of how the common code can be used.

 @note IMPLEMENT ME comments are used to indicate functionality
 which much be customised for each implementation.
*/


#include <string.h> /* strlen() */

#include <pthread.h>
#include "pa_util.h"
#include "pa_allocation.h"
#include "pa_hostapi.h"
#include "pa_stream.h"
#include "pa_cpuload.h"
#include "pa_process.h"
#include <AudioToolBox/AudioToolbox.h>
#include "pa_debugprint.h"
#include <unistd.h>



#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
 
#define LOG PaUtil_DebugPrint
// PA_LOGAPI

/* prototypes for functions declared in this file */

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */

PaError PaCoreAudioIOS_Initialize( PaUtilHostApiRepresentation **hostApi, PaHostApiIndex index );

#ifdef __cplusplus
}
#endif /* __cplusplus */


#define CHECK(condition) do{ if (!(condition)) { CHECK_IMPL(__FILE__, __LINE__, __func__, #condition " Assertion failed\n"); raise(SIGTRAP); } } while(0)
#include <stdarg.h>
#include <string.h>
#include <stdio.h>
#include <signal.h>

#include "pa_coreaudio.h"


static void CHECK_IMPL(const char* file, unsigned long line, const char *function, const char *format, ... )
{
	char buffer[512];
	snprintf(buffer, sizeof(buffer), "[%s(%lu) | %s] ", file, line, function);
	size_t len = strlen(buffer);
	va_list args;
    va_start( args, format );
	vsnprintf(&buffer[len], sizeof(buffer)-len, format, args );
    va_end( args );

	PaUtil_DebugPrint(buffer);
}

static void Terminate( struct PaUtilHostApiRepresentation *hostApi );
static PaError IsFormatSupported( struct PaUtilHostApiRepresentation *hostApi,
                                  const PaStreamParameters *inputParameters,
                                  const PaStreamParameters *outputParameters,
                                  double sampleRate );
static PaError OpenStream( struct PaUtilHostApiRepresentation *hostApi,
                           PaStream** s,
                           const PaStreamParameters *inputParameters,
                           const PaStreamParameters *outputParameters,
                           double sampleRate,
                           unsigned long framesPerBuffer,
                           PaStreamFlags streamFlags,
                           PaStreamCallback *streamCallback,
                           void *userData );
static PaError CloseStream( PaStream* stream );
static PaError StartStream( PaStream *stream );
static PaError StopStream( PaStream *stream );
static PaError AbortStream( PaStream *stream );
static PaError IsStreamStopped( PaStream *s );
static PaError IsStreamActive( PaStream *stream );
static PaTime GetStreamTime( PaStream *stream );
static double GetStreamCpuLoad( PaStream* stream );
static PaError ReadStream( PaStream* stream, void *buffer, unsigned long frames );
static PaError WriteStream( PaStream* stream, const void *buffer, unsigned long frames );
static signed long GetStreamReadAvailable( PaStream* stream );
static signed long GetStreamWriteAvailable( PaStream* stream );

/* IMPLEMENT ME: a macro like the following one should be used for reporting
 host errors */
#define PA_SKELETON_SET_LAST_HOST_ERROR( errorCode, errorText ) \
    PaUtil_SetLastHostErrorInfo( paInDevelopment, errorCode, errorText )



PaError PaCoreAudioIOS_Initialize( PaUtilHostApiRepresentation **hostApi, PaHostApiIndex hostApiIndex )
{
    PA_LOGAPI_ENTER( "PaCoreAudioIOS_Initialize" );

    PaError result = paNoError;
    int i, deviceCount;
    PaCoreAudioIOSHostApiRepresentation *coreaudioHostApi;
    PaDeviceInfo *deviceInfoArray;

    coreaudioHostApi = (PaCoreAudioIOSHostApiRepresentation*)PaUtil_AllocateMemory( sizeof(PaCoreAudioIOSHostApiRepresentation) );
    if( !coreaudioHostApi )
    {
        result = paInsufficientMemory;
        goto error;
    }

    coreaudioHostApi->allocations = PaUtil_CreateAllocationGroup();
    if( !coreaudioHostApi->allocations )
    {
        result = paInsufficientMemory;
        goto error;
    }
    

    *hostApi = &coreaudioHostApi->inheritedHostApiRep;
    (*hostApi)->info.structVersion = 1;
    (*hostApi)->info.type = paInDevelopment;            /* IMPLEMENT ME: change to correct type id */
    (*hostApi)->info.name = "CoreAudioIOS";

    (*hostApi)->info.defaultInputDevice = paNoDevice;  /* IMPLEMENT ME */
    (*hostApi)->info.defaultOutputDevice = 0; /* IMPLEMENT ME */

    (*hostApi)->info.deviceCount = 0;  

    
    // For now hard-code one device
    deviceCount = 1; /* IMPLEMENT ME */
    if( deviceCount > 0 )
    {
        (*hostApi)->deviceInfos = (PaDeviceInfo**)PaUtil_GroupAllocateMemory(
                coreaudioHostApi->allocations, sizeof(PaDeviceInfo*) * deviceCount );
        if( !(*hostApi)->deviceInfos )
        {
            result = paInsufficientMemory;
            goto error;
        }

        /* allocate all device info structs in a contiguous block */
        deviceInfoArray = (PaDeviceInfo*)PaUtil_GroupAllocateMemory(
                coreaudioHostApi->allocations, sizeof(PaDeviceInfo) * deviceCount );
        if( !deviceInfoArray )
        {
            result = paInsufficientMemory;
            goto error;
        }

            i=0;
            PaDeviceInfo *deviceInfo = &deviceInfoArray[i];
            deviceInfo->structVersion = 2;
            deviceInfo->hostApi = hostApiIndex;
            deviceInfo->name = 0;  /* IMPLEMENT ME: allocate block and copy name eg: */
            const char *srcName = "AudioPlayer0";
            char *deviceName = (char*)PaUtil_GroupAllocateMemory( coreaudioHostApi->allocations, strlen(srcName) + 1 );
            if( !deviceName )
            {
                result = paInsufficientMemory;
                goto error;
            }
            strcpy( deviceName, srcName );
            deviceInfo->name = deviceName;

            deviceInfo->maxInputChannels = 0;  /* IMPLEMENT ME */
            deviceInfo->maxOutputChannels = 2;  /* IMPLEMENT ME */
            
            deviceInfo->defaultLowInputLatency = 0.;  /* IMPLEMENT ME */
            deviceInfo->defaultLowOutputLatency = 0.2;  /* IMPLEMENT ME */
            deviceInfo->defaultHighInputLatency = 0.;  /* IMPLEMENT ME */
            deviceInfo->defaultHighOutputLatency = 0.2;  /* IMPLEMENT ME */  

            deviceInfo->defaultSampleRate = 44100.0; /* IMPLEMENT ME */
            
            (*hostApi)->deviceInfos[i] = deviceInfo;
            ++(*hostApi)->info.deviceCount;
    }

    (*hostApi)->Terminate = Terminate;
    (*hostApi)->OpenStream = OpenStream;
    (*hostApi)->IsFormatSupported = IsFormatSupported;

    PaUtil_InitializeStreamInterface( &coreaudioHostApi->callbackStreamInterface, CloseStream, StartStream,
                                      StopStream, AbortStream, IsStreamStopped, IsStreamActive,
                                      GetStreamTime, GetStreamCpuLoad,
                                      PaUtil_DummyRead, PaUtil_DummyWrite,
                                      PaUtil_DummyGetReadAvailable, PaUtil_DummyGetWriteAvailable );

    PaUtil_InitializeStreamInterface( &coreaudioHostApi->blockingStreamInterface, CloseStream, StartStream,
                                      StopStream, AbortStream, IsStreamStopped, IsStreamActive,
                                      GetStreamTime, PaUtil_DummyGetCpuLoad,
                                      ReadStream, WriteStream, GetStreamReadAvailable, GetStreamWriteAvailable );


    return result;

error:
    if( coreaudioHostApi )
    {
        if( coreaudioHostApi->allocations )
        {
            PaUtil_FreeAllAllocations( coreaudioHostApi->allocations );
            PaUtil_DestroyAllocationGroup( coreaudioHostApi->allocations );
        }
                
        PaUtil_FreeMemory( coreaudioHostApi );
    }
    return result;
}


static void Terminate( struct PaUtilHostApiRepresentation *hostApi )
{
    PA_LOGAPI_ENTER( "PaCoreAudioIOS_Terminate" );

    PaCoreAudioIOSHostApiRepresentation *coreaudioHostApi = (PaCoreAudioIOSHostApiRepresentation*)hostApi;

    /*
        IMPLEMENT ME:
            - clean up any resources not handled by the allocation group
    */

    if( coreaudioHostApi->allocations )
    {
        PaUtil_FreeAllAllocations( coreaudioHostApi->allocations );
        PaUtil_DestroyAllocationGroup( coreaudioHostApi->allocations );
    }

    PaUtil_FreeMemory( coreaudioHostApi );
}


static int BuildCoreAudioDataFormat(AudioStreamBasicDescription* Format, int numChannels, PaSampleFormat sampleFormat, double sampleRate )
{
    memset(Format, 0, sizeof(AudioStreamBasicDescription));
    Format->mFormatID    =  kAudioFormatLinearPCM;
    Format->mSampleRate = sampleRate;
    
    Format->mFormatFlags = kAudioFormatFlagIsPacked;
    
    Format->mFramesPerPacket = 1;
    Format->mReserved = 0;


    switch(sampleFormat)
    {
    case paInt8:
        Format->mBytesPerPacket = 1 * numChannels;
        Format->mBitsPerChannel = 8;
        Format->mBytesPerFrame = 1 * numChannels;
        Format->mFormatFlags |=  kLinearPCMFormatFlagIsSignedInteger;
        break;
    case paInt16:
        Format->mBytesPerPacket = 2 * numChannels;
        Format->mBitsPerChannel = 16;
        Format->mBytesPerFrame = 2 * numChannels;
        Format->mFormatFlags |=  kLinearPCMFormatFlagIsSignedInteger;
        break;
    case paInt24:
        Format->mBytesPerPacket = 3 * numChannels;
        Format->mBitsPerChannel = 24;
        Format->mBytesPerFrame = 3 * numChannels;
        Format->mFormatFlags |=  kLinearPCMFormatFlagIsSignedInteger;
        break;
    case paInt32:
        Format->mBytesPerPacket = 4 * numChannels;
        Format->mBitsPerChannel = 32;
        Format->mBytesPerFrame = 4 * numChannels;
        Format->mFormatFlags |=  kLinearPCMFormatFlagIsSignedInteger;
        break;
    case paUInt8:
        Format->mBytesPerPacket = 1 * numChannels;
        Format->mBitsPerChannel = 8;
        Format->mBytesPerFrame = 1 * numChannels;
    case paFloat32:
        Format->mBytesPerPacket = 4 * numChannels;
        Format->mBitsPerChannel = 32;
        Format->mBytesPerFrame = 4 * numChannels;
        Format->mFormatFlags |= kLinearPCMFormatFlagIsFloat;
    case paCustomFormat:
    default:
        return FALSE;
    }
    
    // Channels
    Format->mChannelsPerFrame = numChannels;
    
    #ifndef PA_LITTLE_ENDIAN
         Format->mFormatFlags |=  kLinearPCMFormatFlagIsBigEndian;
    #endif

    return TRUE;
}


// this callback handler is called by coreaudio every time a buffer finishes enqueued
static void coreaudioCallback(void* context,AudioQueueRef queue,AudioQueueBufferRef buffer)
{
     PaCoreAudioIOSStream *stream = (PaCoreAudioIOSStream*)context;
    
     int bytes2 = stream->outBufSamples*sizeof(short);
     
     memcpy((short*)buffer->mAudioData,&stream->outrb->buffer[stream->outrb->rp],buffer->mAudioDataByteSize);

     move_read_pointer(stream->outrb,bytes2);
     
     AudioQueueEnqueueBuffer(queue, buffer, 0, NULL);
}

// Create or Check AudioPlayer availability for the given stream format
// if playerObject is NULL, just just the availability, otherwise create and
// return the AudioPlayer instance
static int CreateAudioPlayer(AudioQueueRef* playerObject,PaCoreAudioIOSStream* stream, int numChannels, PaSampleFormat sampleFormat, double sampleRate )
{
    AudioStreamBasicDescription format_pcm;

    // Validate and convert format
    if(!BuildCoreAudioDataFormat(&format_pcm, numChannels, sampleFormat, sampleRate))
        return FALSE;

    OSStatus result = AudioQueueNewOutput(&format_pcm, coreaudioCallback, stream, NULL, NULL, 0, playerObject);
    if(result)
    {
        return 0;
    }


    return 1;
}



static PaError IsFormatSupported( struct PaUtilHostApiRepresentation *hostApi,
                                  const PaStreamParameters *inputParameters,
                                  const PaStreamParameters *outputParameters,
                                  double sampleRate )
{
    PaCoreAudioIOSHostApiRepresentation *coreaudioHostApi = (PaCoreAudioIOSHostApiRepresentation*)hostApi;
    int inputChannelCount, outputChannelCount;
    PaSampleFormat inputSampleFormat, outputSampleFormat;
    


    if( outputParameters )
    {
        outputChannelCount = outputParameters->channelCount;
        outputSampleFormat = outputParameters->sampleFormat;

        /* all standard sample formats are supported by the buffer adapter,
            this implementation doesn't support any custom sample formats */
        if( outputSampleFormat & paCustomFormat )
            return paSampleFormatNotSupported;
            
        /* unless alternate device specification is supported, reject the use of
            paUseHostApiSpecificDeviceSpecification */

        if( outputParameters->device == paUseHostApiSpecificDeviceSpecification )
            return paInvalidDevice;

        /* check that output device can support outputChannelCount */
/*        if( outputChannelCount > hostApi->deviceInfos[ outputParameters->device ]->maxOutputChannels )
            return paInvalidChannelCount;*/

        /* validate outputStreamInfo */
        if( outputParameters->hostApiSpecificStreamInfo )
            return paIncompatibleHostApiSpecificStreamInfo; /* this implementation doesn't use custom stream info */
    }
    else
    {
        outputChannelCount = 0;
    }

    /* suppress unused variable warnings */
    (void) sampleRate;

    return paFormatIsSupported;
}




//the audio thread loop
static void* AudioThreadLoop( void *userData )
{
    PaCoreAudioIOSStream *stream = (PaCoreAudioIOSStream*)userData;

     volatile int tmpisRunning = 0;

     __sync_val_compare_and_swap(&tmpisRunning,tmpisRunning,stream->isRunning);


     while( tmpisRunning )
     {

          int test_read =   checkspace_circular_buffer(stream->outrb,0); 

          if(test_read >= (stream->framesPerHostCallback*sizeof(short)) * 1.5) //if we are 1.5 time ahead of the read buffer, let the thread sleep and return
          {
               usleep(10000);
			   __sync_val_compare_and_swap(&tmpisRunning,tmpisRunning,stream->isRunning);
               continue;
          }
               
           

         PaStreamCallbackTimeInfo timeInfo = {0,0,0}; /* IMPLEMENT ME */
         int callbackResult;
         unsigned long framesProcessed;
         
         PaUtil_BeginCpuLoadMeasurement( &stream->cpuLoadMeasurer );
         

         PaUtil_BeginBufferProcessing( &stream->bufferProcessor, &timeInfo, 0 /* IMPLEMENT ME: pass underflow/overflow flags when necessary */ );

         PaUtil_SetOutputFrameCount( &stream->bufferProcessor, 0);
         PaUtil_SetInterleavedOutputChannels( &stream->bufferProcessor,
                 0, /* first channel of outputBuffer is channel 0 */
                 stream->outputBuffer,
                 0 ); /* 0 - use outputChannelCount passed to init buffer processor */


         callbackResult = paContinue;
         framesProcessed = PaUtil_EndBufferProcessing( &stream->bufferProcessor, &callbackResult );


          int  bytes = (framesProcessed * stream->bufferProcessor.outputChannelCount) *   sizeof(short);
          
         /*
             If you need to byte swap or shift outputBuffer to convert it to
             host format, do it here.
         */

          


         write_circular_buffer_bytes(stream->outrb, (char *) stream->outputBuffer,bytes);
          //LOGI("write %d bytes in buffer",bytes);

         stream->time += (double) framesProcessed/(stream->samplerate*stream->bufferProcessor.outputChannelCount);

         PaUtil_EndCpuLoadMeasurement( &stream->cpuLoadMeasurer, framesProcessed );


         if( callbackResult == paContinue )
         {
             /* nothing special to do */
         }
         else if( callbackResult == paAbort )
         {
               __sync_val_compare_and_swap(&stream->isRunning,stream->isRunning,0);

             /* once finished, call the finished callback */
             if( stream->streamRepresentation.streamFinishedCallback != 0 )
                 stream->streamRepresentation.streamFinishedCallback( stream->streamRepresentation.userData );
         }
         else
         {
               __sync_val_compare_and_swap(&stream->isRunning,stream->isRunning,0);
             /* User callback has asked us to stop with paComplete or other non-zero value */

             /* IMPLEMENT ME - finish playback once currently queued audio has completed  */

             /* once finished, call the finished callback */
             if( stream->streamRepresentation.streamFinishedCallback != 0 )
                 stream->streamRepresentation.streamFinishedCallback( stream->streamRepresentation.userData );
         }

        __sync_val_compare_and_swap(&tmpisRunning,tmpisRunning,stream->isRunning);

     }

     pthread_exit(0);
}


/* see pa_hostapi.h for a list of validity guarantees made about OpenStream parameters */

static PaError OpenStream( struct PaUtilHostApiRepresentation *hostApi,
                           PaStream** s,
                           const PaStreamParameters *inputParameters,
                           const PaStreamParameters *outputParameters,
                           double sampleRate,
                           unsigned long framesPerBuffer,
                           PaStreamFlags streamFlags,
                           PaStreamCallback *streamCallback,
                           void *userData )
{
    PaError result = paNoError;
    PaCoreAudioIOSHostApiRepresentation *coreaudioHostApi = (PaCoreAudioIOSHostApiRepresentation*)hostApi;
    PaCoreAudioIOSStream *stream = 0;
    unsigned long framesPerHostBuffer = framesPerBuffer; /* these may not be equivalent for all implementations */
    int inputChannelCount, outputChannelCount;
    PaSampleFormat inputSampleFormat, outputSampleFormat;
    PaSampleFormat hostInputSampleFormat, hostOutputSampleFormat;
    
    stream = (PaCoreAudioIOSStream*)PaUtil_AllocateMemory( sizeof(PaCoreAudioIOSStream) );
    if( !stream )
    {
        result = paInsufficientMemory;
        goto error;
    }

    memset(stream, 0, sizeof(PaCoreAudioIOSStream));
        
    if( outputParameters )
    {
        outputChannelCount = outputParameters->channelCount;
        outputSampleFormat = outputParameters->sampleFormat;
        
        /* unless alternate device specification is supported, reject the use of
            paUseHostApiSpecificDeviceSpecification */

        if( outputParameters->device == paUseHostApiSpecificDeviceSpecification )
            return paInvalidDevice;

        /* check that output device can support inputChannelCount */
        if( outputChannelCount > hostApi->deviceInfos[ outputParameters->device ]->maxOutputChannels )
            return paInvalidChannelCount;

        /* validate outputStreamInfo */
        if( outputParameters->hostApiSpecificStreamInfo )
            return paIncompatibleHostApiSpecificStreamInfo; /* this implementation doesn't use custom stream info */

        /* IMPLEMENT ME - establish which  host formats are available */
//        hostOutputSampleFormat =
//            PaUtil_SelectClosestAvailableFormat( paInt16 /* native formats */, outputSampleFormat ); 
        
        if(!CreateAudioPlayer(&stream->audioQueue,stream,outputChannelCount, outputSampleFormat, sampleRate ))
            return paSampleFormatNotSupported;
        hostOutputSampleFormat = outputSampleFormat;
    }
    else
    {
        outputChannelCount = 0;
        outputSampleFormat = hostOutputSampleFormat = paInt16; /* Surpress 'uninitialized var' warnings. */
    }

    /*
        IMPLEMENT ME:

        ( the following two checks are taken care of by PaUtil_InitializeBufferProcessor() FIXME - checks needed? )

            - check that input device can support inputSampleFormat, or that
                we have the capability to convert from outputSampleFormat to
                a native format

            - check that output device can support outputSampleFormat, or that
                we have the capability to convert from outputSampleFormat to
                a native format

            - if a full duplex stream is requested, check that the combination
                of input and output parameters is supported

            - check that the device supports sampleRate

            - alter sampleRate to a close allowable rate if possible / necessary

            - validate suggestedInputLatency and suggestedOutputLatency parameters,
                use default values where necessary
    */

    /* KK abort if no valid number of channels selected */
    if( inputChannelCount == 0 && outputChannelCount == 0 )
        return paInvalidChannelCount;


    /* validate platform specific flags */
    if( (streamFlags & paPlatformSpecificFlags) != 0 )
        return paInvalidFlag; /* unexpected platform specific flag */


    if( streamCallback )
    {
        PaUtil_InitializeStreamRepresentation( &stream->streamRepresentation,
                                               &coreaudioHostApi->callbackStreamInterface, streamCallback, userData );

    }
    else
    {
        PaUtil_InitializeStreamRepresentation( &stream->streamRepresentation,
                                               &coreaudioHostApi->blockingStreamInterface, streamCallback, userData );
    }

    PaUtil_InitializeCpuLoadMeasurer( &stream->cpuLoadMeasurer, sampleRate );


    /* we assume a fixed host buffer size in this example, but the buffer processor
        can also support bounded and unknown host buffer sizes by passing 
        paUtilBoundedHostBufferSize or paUtilUnknownHostBufferSize instead of
        paUtilFixedHostBufferSize below. */
        
    result =  PaUtil_InitializeBufferProcessor( &stream->bufferProcessor,
              inputChannelCount, inputSampleFormat, hostInputSampleFormat,
              outputChannelCount, outputSampleFormat, hostOutputSampleFormat,
              sampleRate, streamFlags, framesPerBuffer,
              framesPerHostBuffer, paUtilFixedHostBufferSize,
              streamCallback, userData );
    if( result != paNoError )
        goto error;


    /*
        IMPLEMENT ME: initialise the following fields with estimated or actual
        values.
    */
    stream->streamRepresentation.streamInfo.inputLatency =
            (PaTime)PaUtil_GetBufferProcessorInputLatencyFrames(&stream->bufferProcessor) / sampleRate; /* inputLatency is specified in _seconds_ */
    stream->streamRepresentation.streamInfo.outputLatency =
            (PaTime)PaUtil_GetBufferProcessorOutputLatencyFrames(&stream->bufferProcessor) / sampleRate; /* outputLatency is specified in _seconds_ */
    stream->streamRepresentation.streamInfo.sampleRate = sampleRate;

    /*
        IMPLEMENT ME:
            - additional stream setup + opening
    */

    stream->framesPerHostCallback = framesPerHostBuffer;

    if(stream->audioQueue)
    {
         stream->samplerate = sampleRate;
        //buffers allocations
        
          //buffer storing data from user defined callback
          stream->outBufSamples = framesPerBuffer*outputChannelCount;
          stream->outputBuffer = (short *) PaUtil_AllocateMemory((stream->outBufSamples * Pa_GetSampleSize(outputSampleFormat)) * 4); //output of 4k per callback
//stream->outBufSamples *  Pa_GetSampleSize(outputSampleFormat));


          if(!stream->outputBuffer)
          {
            result = paInsufficientMemory;
            goto error;
          }


          int bytes = stream->framesPerHostCallback*Pa_GetSampleSize(outputSampleFormat);

          stream->emptybuffer = (short *) PaUtil_AllocateMemory(bytes);

          memset(stream->emptybuffer,0,bytes);

          if(!stream->emptybuffer)
          {
            result = paInsufficientMemory;
            goto error;
          }

          //ring buffer for exchange between opensl and audio thread
          stream->outrb = create_circular_buffer((stream->outBufSamples * Pa_GetSampleSize(outputSampleFormat)) * 32); //test with 262k ring buffer


//stream->outBufSamples*Pa_GetSampleSize(outputSampleFormat)*16);

          if(!stream->outrb)
          {
               result = paInsufficientMemory;
               goto error;
          }
        
    }


    *s = (PaStream*)stream;

    return result;

error:

     if( stream )
     {
          if( stream->outputBuffer )
               free( stream->outputBuffer );

          stream->outputBuffer = NULL;

          if(stream->emptybuffer)
               free(stream->emptybuffer);

          stream->emptybuffer = NULL;


          free_circular_buffer(stream->outrb);

          PaUtil_FreeMemory( stream );
     }

    return result;
}

static void coreaudio_pause(PaCoreAudioIOSStream *p) {

  AudioQueueStop(p->audioQueue, true);
}

static void coreaudio_stop(PaCoreAudioIOSStream *p) {

  AudioQueueStop(p->audioQueue, true);

}



/*
    When CloseStream() is called, the multi-api layer ensures that
    the stream has already been stopped or aborted.
*/
static PaError CloseStream( PaStream* s )
{
    PaError result = paNoError;
    PaCoreAudioIOSStream *stream = (PaCoreAudioIOSStream*)s;


    __sync_val_compare_and_swap(&stream->isRunning,stream->isRunning,0);

     coreaudio_stop(s);

    /*
        IMPLEMENT ME:
            - additional stream closing + cleanup
    */

    PaUtil_TerminateBufferProcessor( &stream->bufferProcessor );
    PaUtil_TerminateStreamRepresentation( &stream->streamRepresentation );

     if( stream->outputBuffer )
          free( stream->outputBuffer );

     stream->outputBuffer = NULL;

     if(stream->emptybuffer)
               free(stream->emptybuffer);

     stream->emptybuffer = NULL;



     free_circular_buffer(stream->outrb);

    PaUtil_FreeMemory( stream );


    return result;
}

#define NUM_BUFFERS 2

static PaError StartStream( PaStream *s )
{
    PaError result = paNoError;
    PaCoreAudioIOSStream *stream = (PaCoreAudioIOSStream*)s;



    PaUtil_ResetBufferProcessor( &stream->bufferProcessor );

     //first call to opensl, enqueue empty buffer to kick off buffer sequence

     __sync_val_compare_and_swap(&stream->isRunning,stream->isRunning,1);


     pthread_create(&stream->audio_thread,NULL,AudioThreadLoop,stream);


     int bytes = stream->framesPerHostCallback*sizeof(short);
     
// allocate and prime audio buffers
		for(int i = 0; i < NUM_BUFFERS; ++i)
		{
			AudioQueueBufferRef buffer;
			result = AudioQueueAllocateBuffer(stream->audioQueue, bytes, &buffer);
			if(result)
			{
				//printf("AudioQueueAllocateBuffer failed (%d)\n", result);
				return 0;
			}
			buffer->mAudioDataByteSize = bytes;
			memset(buffer->mAudioData, 0, buffer->mAudioDataByteSize);
			AudioQueueEnqueueBuffer(stream->audioQueue, buffer, 0, NULL);
		}

		// start playback
		result = AudioQueueStart(stream->audioQueue, NULL);
		if(result)
		{
			//printf("AudioQueueStart failed (%d)\n", result);
			return 0;
		}

    return result;

start_error:

	coreaudio_stop(s);
	return result;


}


static PaError StopStream( PaStream *s )
{


    PaError result = paNoError;
    PaCoreAudioIOSStream *stream = (PaCoreAudioIOSStream*)s;

     __sync_val_compare_and_swap(&stream->isRunning,stream->isRunning,0);
    coreaudio_pause(s);
   



    return result;
}


static PaError AbortStream( PaStream *s )
{


    PaError result = paNoError;
    PaCoreAudioIOSStream *stream = (PaCoreAudioIOSStream*)s;

     __sync_val_compare_and_swap(&stream->isRunning,stream->isRunning,0);
    coreaudio_pause(s);
    

    return result;
}


static PaError IsStreamStopped( PaStream *s )
{
    PaCoreAudioIOSStream *stream = (PaCoreAudioIOSStream*)s;

     volatile int tmpIsRunning = 0;

     __sync_val_compare_and_swap(&tmpIsRunning,tmpIsRunning,stream->isRunning);

    return !tmpIsRunning;
}


static PaError IsStreamActive( PaStream *s )
{
    PaCoreAudioIOSStream *stream = (PaCoreAudioIOSStream*)s;

     volatile int tmpIsRunning = 0;
     __sync_val_compare_and_swap(&tmpIsRunning,tmpIsRunning,stream->isRunning);

    return tmpIsRunning;
}


static PaTime GetStreamTime( PaStream *s )
{
    PaCoreAudioIOSStream *stream = (PaCoreAudioIOSStream*)s;

    return stream->time;
}


static double GetStreamCpuLoad( PaStream* s )
{
    PaCoreAudioIOSStream *stream = (PaCoreAudioIOSStream*)s;

    return PaUtil_GetCpuLoad( &stream->cpuLoadMeasurer );
}


/*
    As separate stream interfaces are used for blocking and callback
    streams, the following functions can be guaranteed to only be called
    for blocking streams.
*/

static PaError ReadStream( PaStream* s,
                           void *buffer,
                           unsigned long frames )
{
    PaCoreAudioIOSStream *stream = (PaCoreAudioIOSStream*)s;

    /* suppress unused variable warnings */
    (void) buffer;
    (void) frames;
    (void) stream;
    
    /* IMPLEMENT ME, see portaudio.h for required behavior*/

    return paNoError;
}


static PaError WriteStream( PaStream* s,
                            const void *buffer,
                            unsigned long frames )
{
    PaCoreAudioIOSStream *stream = (PaCoreAudioIOSStream*)s;

    /* suppress unused variable warnings */
    (void) buffer;
    (void) frames;
    (void) stream;
    
    /* IMPLEMENT ME, see portaudio.h for required behavior*/

    return paNoError;
}


static signed long GetStreamReadAvailable( PaStream* s )
{
    PaCoreAudioIOSStream *stream = (PaCoreAudioIOSStream*)s;

    /* suppress unused variable warnings */
    (void) stream;
    
    /* IMPLEMENT ME, see portaudio.h for required behavior*/

    return 0;
}


static signed long GetStreamWriteAvailable( PaStream* s )
{
    PaCoreAudioIOSStream *stream = (PaCoreAudioIOSStream*)s;

    /* suppress unused variable warnings */
    (void) stream;
    
    /* IMPLEMENT ME, see portaudio.h for required behavior*/

    return 0;
}


//circular buffer implementation
 
circular_buffer* create_circular_buffer(int bytes)
{
     circular_buffer *p;

     if ((p = calloc(1, sizeof(circular_buffer))) == NULL) {
          return NULL;
     }

     p->size = bytes;
     p->wp = p->rp = 0;

     if ((p->buffer = calloc(bytes, sizeof(char))) == NULL) {
          free (p);
          return NULL;
     }
     return p;
}
 
int checkspace_circular_buffer(circular_buffer *p, int writeCheck){
    int wp = 0;
     int rp = 0;
     int size = 0;

     __sync_val_compare_and_swap(&wp,wp,p->wp);

     __sync_val_compare_and_swap(&rp,rp,p->rp);

     __sync_val_compare_and_swap(&size,size,p->size);


     if(writeCheck){
          if (wp > rp) 
               return rp - wp + size - 1;
          else if (wp < rp) 
               return rp - wp - 1;
          else 
               return size - 1;
     }
     else {
          if (wp > rp) 
               return wp - rp;
          else if (wp < rp) 
               return wp - rp + size;
          else 
               return 0;
     }
}
 
int read_circular_buffer_bytes(circular_buffer *p, char *out, int bytes){
     int remaining;
     int bytesread;
     int size; 
     int i=0; 
     int rp;

     __sync_val_compare_and_swap(&size,size,p->size);

     __sync_val_compare_and_swap(&rp,rp,p->rp);

     char *buffer = p->buffer; 

     if ((remaining = checkspace_circular_buffer(p, 0)) == 0) {
          return 0;
     }
     bytesread = bytes > remaining ? remaining : bytes;
     for(i=0; i < bytesread; i++){
          out[i] = buffer[rp++];
          if(rp == size) 
               rp = 0;
     }

     __sync_val_compare_and_swap(&p->rp,p->rp,rp);
     return bytesread;
}


int move_read_pointer(circular_buffer* p,int bytes)
{
      int remaining;
     int bytesread;
     int size;
     int i=0;
     int rp = 0;

     __sync_val_compare_and_swap(&size,size,p->size);

     __sync_val_compare_and_swap(&rp,rp,p->rp);

     
     if ((remaining = checkspace_circular_buffer(p, 0)) == 0) {
          return 0;
     }

      bytesread = bytes > remaining ? remaining : bytes;

     rp += bytesread;

     if(rp >= size)
          rp = 0;


     __sync_val_compare_and_swap(&p->rp,p->rp,rp);

     return bytesread;
}


int write_circular_buffer_bytes(circular_buffer *p, const char *in, int bytes){
     int remaining;
     int byteswrite;
     int size;
     int i=0;
     int wp = 0;

     __sync_val_compare_and_swap(&size,size,p->size);

     __sync_val_compare_and_swap(&wp,wp,p->wp);  


     char *buffer = p->buffer;

     if ((remaining = checkspace_circular_buffer(p, 1)) == 0) {
          return 0;
     }
     byteswrite = bytes > remaining ? remaining : bytes;
     for(i=0; i < byteswrite; i++){
          buffer[wp++] = in[i];
          if(wp == size) 
               wp = 0;
     }
     
     __sync_val_compare_and_swap(&p->wp,p->wp,wp);   

     return byteswrite;
}

 
void free_circular_buffer (circular_buffer *p){
     if(p == NULL) 
          return;
     free(p->buffer);
     free(p);
}



