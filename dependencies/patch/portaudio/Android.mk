LOCAL_HOME := $(call my-dir) 
LOCAL_PATH := $(call my-dir)


include $(CLEAR_VARS)
LOCAL_MODULE := portaudio
LOCAL_C_INCLUDES := $(LOCAL_PATH)/../include $(LOCAL_PATH)/../src/common $(LOCAL_PATH)/../src/os/unix
LOCAL_CFLAGS := -std=c99 -DPA_LITTLE_ENDIAN -DPA_USE_OPENSLES
LOCAL_EXPORT_CFLAGS := -DPA_LITTLE_ENDIAN -DPA_USE_OPENSLES
LOCAL_SRC_FILES := ../src/common/pa_converters.c \
../src/common/pa_front.c \
../src/common/pa_cpuload.c \
../src/common/pa_debugprint.c \
../src/common/pa_process.c \
../src/common/pa_trace.c \
../src/common/pa_ringbuffer.c \
../src/common/pa_stream.c \
../src/common/pa_allocation.c \
../src/common/pa_dither.c \
../src/os/unix/pa_unix_hostapis.c \
../src/os/unix/pa_unix_util.c \
../src/hostapi/opensles/pa_opensles.c

include $(BUILD_STATIC_LIBRARY)
