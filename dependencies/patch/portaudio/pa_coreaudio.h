/* PaOpenSLESHostApiRepresentation - host api datastructure specific to this implementation */

typedef struct _circular_buffer {
 char *buffer;
 int wp;
 int rp;
 int size;
} circular_buffer;

circular_buffer* create_circular_buffer(int bytes);
int checkspace_circular_buffer(circular_buffer *p, int writeCheck);
int read_circular_buffer_bytes(circular_buffer *p, char *out, int bytes);
int write_circular_buffer_bytes(circular_buffer *p, const char *in, int bytes);
void free_circular_buffer (circular_buffer *p);
int move_read_pointer(circular_buffer* p,int bytes);

typedef struct
{
    PaUtilHostApiRepresentation inheritedHostApiRep;
    PaUtilStreamInterface callbackStreamInterface;
    PaUtilStreamInterface blockingStreamInterface;

    PaUtilAllocationGroup *allocations;

    /* implementation specific data goes here */
   
}
PaCoreAudioIOSHostApiRepresentation;  /* IMPLEMENT ME: rename this */

/* PaOpenSLESStream - a stream data structure specifically for this implementation */

typedef struct PaCoreAudioIOSStream
{ /* IMPLEMENT ME: rename this */
    PaUtilStreamRepresentation streamRepresentation;
    PaUtilCpuLoadMeasurer cpuLoadMeasurer;
    PaUtilBufferProcessor bufferProcessor;

    /* IMPLEMENT ME:
            - implementation specific data goes here
    */
    unsigned long framesPerHostCallback;
     AudioQueueRef audioQueue;


     volatile int isRunning;

     // buffers
    void* outputBuffer;
    circular_buffer *outrb;
     void* emptybuffer;
   

     // size of buffers
      int outBufSamples;

     //audio thread
     pthread_t audio_thread;

     //time info
     double time;
     int samplerate;
}
PaCoreAudioIOSStream;




