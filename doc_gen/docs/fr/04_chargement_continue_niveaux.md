Le chargement continue sous forme de niveau est géré de la manière suivante :

9 chunk réparti en rectangle : 



<img src="chunks.png"></im>


Le positionnement physique des chunk dans le tableau de chunk est toujours le même, ainsi la position 0 en mémoire correspondera toujours au même chunk.

Par contre, lorsque le joueur arrive au bord d'un chunk, et donc déclenche un nouveau chargement, le positionnement logique des chunk va changer,
on dispose d'un tableau de pointeur nommé "access_chunk" qui permet de connaitre la position logique d'un chunk donnée.

Ainsi, la position en mémoire d'un chunk peut ne pas correspondre à la position visible de ce même chunk.

Concernant la gestion des collisions, seul le tableaux collisions_array  de l'objet level_stream_t et les tableaux collisions_array et tilemap_array du physics_manager_t correspondent à la position visuel des tilesà l'écran. Il sont donc mis à jour à chaque chargement / déchargement de chunk.

A contrario, les tableaux contenants les segments de collisions (collisions_segment par ex) correspondent à la position en mémoire des chunk et non à la position à l'écran de ses même chunk. il faut donc effectuer une conversion de la position logique vers la position physique.


