Pour une tile, on créer au max 4 shape chipmunk (une pour chaque coté)

lors du creusement, on supprime toutes les shape de la tile qui devient vide, et on créer des shapes sur les tiles voisines.

Il ne peut y avoir de tile vide avec une shape

Une tile pleine peut avoir 0, 1, 2 , 3 ou 4 shape en fonction de ses voisins.

les shapes sont stockés comme suit : (tile_index * 4) = bas, (tile_index * 4) +1 = droite, (tile_index * 4) +2 = haut, (tile_index * 4) +3 = gauche

(sens inverse des aiguille d'une montre en partant du bas)
