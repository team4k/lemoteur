Dans le cas d'un système de streaming avec génération procédurale / semi procédurale, l'éditeur de niveau permet de créer 
des fichiers de définition de monde ou une case correspond à un chunk.

Un fichier de monde contient tout d'abord une tilemap ou chaque case dispose d'une valeur INT de 0 à MAX_INT. cette valeur permet d'identifier 
la zone d'une case.

On peut créer une ou plusieurs zone, chaque zone dispose d'un code couleur et d'un ensemble de valeur permettant d'associer une case
de notre monde à une zone en particulier. L'idée étant que chaque zone dispose d'un système de génération de niveau spécifique.

Une zone enfant ne peut avoir un intervale de valeur en dehors de l'interval de la zone parente.
