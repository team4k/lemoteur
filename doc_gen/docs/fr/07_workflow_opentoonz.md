la création d'objet et d'éléments animés dans un jeu utilise le workflow suivant : 

1 - animation initiale sur papier de l'élément à importer (papier animation / pegbar )
2 - scan des différentes étapes de l'animation en Noir et blanc avec GITS (GITS) => les peg sont positionnée tête en bas, la feuille est collée au coin droit
3 - import dans un projet opentoonz de notre animation scanner (OpenToonz)
	-> dans l'onglet browser en haut à droite, aller dans le répertoire scan et glisser / déposer le fichier .tif des scans
	-> si nécessaire, dans l'onglet basics, supprimer les frames inutiles
4 - conversion vectorielle (OpenToonz)
	-> sélectionner les frames à convertir
	-> menu Level > convert to vector avec les paramètres suivant
		threshold = 8
		accuracy = 9
		Despeckling = 5
		Max Thickness = 200
5 - nettoyage / crop via la camera / colorisation des images (OpenToonz)
	dans Xsheet > camera settings on définit le ratio qui servira au moment de l'export
		par exemple:  pixels 512 x 192
	- utiliser l'animate tool pour changer la position de la camera et le scale, cela permet de mettre notre animation au max niveau taille
5 - export au format PNG de la séquence d'animation (OpenToonz)
	dans output settings, ne pas oublier de jouer sur la valeur de shrink pour obtenir la dimension finale de l'export
	puis utiliser la fonction render