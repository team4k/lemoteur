L'éclairage dynamique fonctionne comme suit : 

on créer un lumière de type omni ou directionnelle dans l'éditeur

on active le système d'éclairage dans les paramètes l'éditeur

on s'assure que dans la section paramètres, les shaders de lumières, de rendu final d'éclairage et de masques d'ombres sont correcte.

Shader lumières :
		shader utilisé pour le rendu des volumes de lumières (cone directionnelles et cercle omni)
		
shader masque ombres:
		shader utilisé pour lors de la génération du masque des ombres qui couvre l'ensemble de l'écran, le masque dispose d'emplacement "vide" par dessus lesquels sont affiché les lumières
		
shader rendu final:
		shader utilisé pour afficher la texture ombres + lumières finale à l'écran.
		
		
Chaque gestionnaire de niveau dispose de la liste des segment utilisé pour la génération des ombres. Contrairement au système de collisions où un segment
ne dépasse jamais la taille d'une tile, les segment pour la génération des ombres sont fusionnés dans un segment plus grand si besoin.

Chaque objet lumière est généré à partir des segments d'un niveau. Des "volumes" sont générés pour cette lumière, chaque lumière dispose de ses propres volumes et ainsi en cas de mise à jour des segment, il faut également mettre à jour les volumes des lumières.

Les objet lumières disposent d'une valeur "build_volumes" qui doit être positionné sur wtrue pour générer un volume. Si le gestionnaire d'éclairage s'assure de lancer le code de génération d'une lumière à chaque affichage si cette lumière se trouve dans la zone affiché de l'écran et que build_volumes = wtrue, le programmeur est responsable de l'appel à light_manager_regenlight pour positionné la valeur "build_volumes" à wtrue.

Après regénération des volumes, la valeur build_volumes = wfalse, et les volumes ne seront plus généré sur les frames suivantes, sauf si le programmeur refait un appel à light_manager_regenlight.

Hors mode FFP, les variables suivantes sont envoyé au shader de lumière ou de masque d'ombres : 

render_value = 0.0 ou 1.0 > si 0.0, alors nous sommes en mode masque, si 1.0 en mode lumière

falloff  = {constant, linear,quadratic } > contient les valeurs utilisé pour calculer l'atténuation

light_pos = {x,y,z} > position de notre lumière normalisé par rapport à la résolution de la texture de rendu d'ombres (0.0 / 1.0) Z = 0.075

ambient_alpha = valeur alpha ambiente

resolution = résolution du rendu de l'ombre

ratio = ratio entre la résolution du rendu de l'ombre et la résolution de l'écran

LightInfo = {omni / dir,open_angle,rotation_angle} 1 = si 1.0 alors il s'agit d'une lumière omni si = 2.0 d'une lumière directionnelle, 2 = angle d'ouverture (uniquement pour directionnelle), 3 = angle de rotation (uniquement pour directionnelle)

power_factor = valeur de puissance de la lumière

