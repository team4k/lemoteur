Pour créer des animations avec le moteur, utiliser graphics gale

Voici la marche à suivre pour la conception d'animation : 

Créer les animation via l'outil graphics gale ou exporter des fichiers PNG depuis un autre logiciel et importer ses fichiers dans graphics gales

ajouter les fichier .gal dans le répertoire animated_assets de notre répertoire game_template > assets

Le fichier pack_config.txt dans ce répertoire permet de regrouper des fichier .gal dans un seul fichier de sortie png comme suit : 

garcon1=garcon1_droite.gal,garcon1_haut.gal,garcon1_bas.gal,garcon1_fixe_droite.gal,garcon1_fixe_haut.gal,garcon1_fixe_bas.gal,garcon1_fixe_objet.gal

ici, les fichier .gal donneront en sortie un seul fichier garcon1.png

un fichier .gal, correspond à une animation. Il est possible d'ajouter un offset pour recentrer une animation comme suit : 

ghost=ghost_breath.gal,ghost_hard_punch.gal(x=0:y=2),ghost_walk.gal,ghost_jump.gal,ghost_stand_up.gal(x=-18:y=-10),ghost_projection.gal,ghost_garde.gal,ghost_degat.gal,ghost_fall2.gal,ghost_die.gal,ghost_telekinesie.gal,ghost_low_punch.gal(x=4:y=4),ghost_chop.gal,ghost_proj_kinesie.gal

ici, l'animation stand_up sera recentrer avec la valeur -18 en x et la valeur -10 en y.

Les données d'animations sont présentes dans le fichier .awf en sortie

Pour générer un  fichier .png + un fichier .awf, on lance export_assets.bat dans le répertoire game_template. Ce fichier .bat fait appel à l'utilitaire AnimationConverter.exe fournit qui transforme nos fichier .gal en fichier .png + .awf utilisable par le moteur.

Le fichier anim_config.txt dans le répertoire game_template > assets > animated_assets sert à définir des "point d'accroches" pour le moteur, notamment utilisable dans un jeu de baston pour définir ou se trouve les poing / pied d'un personnage.

exemple d'une ligne de config : 

kim=[poing1=0x0000FF,poing2=0xFF0000,pied1=0x00FF00,pied2=0xFFFF00]

La valeur hexadecimal après l'identifiant du point d'accroche correspond à la couleur du pixel définissant la position du point d'accroche.


Pour définir un point d'accroche dans graphic gal, il faut : 

Créer un nouveau calque nommé contact sur chaque frame de notre animation.

Sur ce calque, ajouter un pixel avec la bonne couleur pour chaque point d'accroche.


Nom d'une animation : 

Pour nommer une animation dans notre moteur, donner un nom à la première frame de notre animation.

par défaut, les animation bouclent sur elle même lorsqu'elle sont activées dans le moteur, pour créer une animation qui reste sur la dernière frame une fois jouée, ajouter au nom
de l'animation le paramètre (noloop)





