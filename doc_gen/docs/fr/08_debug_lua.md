WafaEngine embarque tout le nécessaire (luasocket) pour permettre le debug de script lua directement depuis l'IDE ZeroBrane, voici la procédure
à suivre pour debugger un projet : 

prérequis : 

	- ZeroBrane studio en version 1.0 au minimum.
	
1- Lancer ZeroBrane
2- ouvrir le fichier lua à debugger
3- choisir le répertoire du projet, dans le cas où on débugge un script lancé dans l'éditeur de niveau, le répertoire sera le même que celui du fichier de script du niveau.

4- ajouter mobdebug.lua dans le répertoire projet, le fichier se trouve dans le répertoire lualibs/mobdebug du répertoire racine de zerobrane

5- lancer le serveur de debug de zerobrane (project > start debugger server) dans la fenêtre d'output var apparaitre la ligne suivante : 
Debugger server started at <nom machine>:8172. 

c'est à ce serveur de debug de mobdebug va se connecter

5- ajouter la ligne suivante au début du script à débugger 

	require('mobdebug').start('<nom machine>')
	
	remplacer <nom machine> par la valeur qui s'était affichée dans la fenêtre d'output

6- lancer le jeu ou passer en mode jeu dans l'éditeur de niveau, cela va lancer le debugger, normalement le jeu / éditeur va se mettre en pause et une flêche verte va apparaitre sur la première ligne de votre fichier, appuyer simplement sur F5 pour terminer le chargement initiale (sauf si nécessité de debugger le chargement du script). Vous pouvez ensuite ajouter des points d'arrêt à l'intérieur de fonction, au prochain appel, le jeu va se mettre en pause et s'arrêter à l'endroit choisi.

Attention : ne pas utilser "start debugging", c'est bien le jeu / l'éditeur qui lancer la commande initiale de debug on non zerobrane, on ne fait que se connecter au serveur de debug de zerobrane.