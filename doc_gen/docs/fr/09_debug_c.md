aide de appverifier : 

D:\>appverif -?

Application Verifier 3.3.0047
Copyright (c) Microsoft Corporation. All rights reserved.

Application Verifier Command Line Usage:

    -enable TEST ... -for TARGET ... [-with [TEST.]PROPERTY=VALUE ...]
    -disable TEST ... -for TARGET ...
    -query TEST ... -for TARGET ...
    -configure STOP ... -for TARGET ... -with PROPERTY=VALUE...
    -verify TARGET [-faults [PROBABILITY [TIMEOUT [DLL ...]]]]
    -export log -for TARGET -with To=XML_FILE [Symbols=SYMBOL_PATH] [StampFrom=LOG_STAMP] [StampTo=LOG_STAMP] [Log=RELAT
IVE_TO_LAST_INDEX]
    -delete [logs|settings] -for TARGET ...
    -stamp log -for TARGET -with Stamp=LOG_STAMP [Log=RELATIVE_TO_LAST_INDEX]
    -logtoxml LOGFILE XMLFILE
    -installprovider PROVIDERBINARY

Available Tests:

    Heaps
    COM
    RPC
    Handles
    Locks
    Memory
    TLS
    Exceptions
    DirtyStacks
    LowRes
    DangerousAPIs
    TimeRollOver
    Threadpool
    Hangs
    HighVersionLie
    FilePaths
    KernelModeDriverInstall
    InteractiveServices
    Security
    Encryption
    LuaPriv
    PrintAPI
    PrintDriver
    Service

(For descriptions of tests, run appverif.exe in GUI mode.)

Examples:
    appverif -enable handles locks -for foo.exe bar.exe
        (turn on handles locks for foo.exe & bar.exe)
    appverif -enable heaps handles -for foo.exe -with heaps.full=false
        (turn on handles and normal pageheap for foo.exe)
    appverif -enable heaps -for foo.exe -with full=true dlls=mydll.dll
        (turn on full pageheap for the module of mydll.dll in the foo.exe
    appverif -enable * -for foo.exe
        (turn on all tests for foo.exe)
    appverif -disable * -for foo.exe bar.exe
        (turn off all tests for foo.exe & bar.exe)
    appverif -disable * -for *
        (wipe out all the settings in the system)
    appverif -export log -for foo.exe -with to=c:\sample.xml
        (export the most recently log associated with foo.exe to c:\sample.xml)
    appverif /verify notepad.exe /faults 5 1000 kernel32.dll advapi32.dll
        (enable fault injection for notepad.exe. Faults should happen with
         probability 5%, only 1000 msecs after process got launched and only
         for operations initiated from kernel32.dll and advapi32.dll)
		 
		 
usage de app verifier avec par exemple l'éditeur

appverif -enable Heaps -for WafaEditorV2.exe