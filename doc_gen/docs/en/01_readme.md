# Disclaimer #

This engine is not ready for public consumption, it's only made public because of the Ludum Dare compo rules.

# Description #

Wasted fantasy is a 2D  game engine written in C best used for small and medium sized games. It compile under MSVC 2010 or better, GCC and Emscripten (clang). Platform supported are Windows (XP / Vista / 7 / 8) , MacOSX from snow leopard, Linux (Tested under Ubuntu linux, Linux Mint and Arch Linux), Android (from jellybean versions / 4.1 / API 16) and HTML5 / Webgl (Firefox and Chrome on all supported platforms)

# Example code #

Example demo can be found in client/c/src

# How to build #
First, you need the following installed:

- .NET framework 4.0 or better on Windows (Already installed on Windows 7 and better), mono v4 or better on other platforms
- Cmake v3.1 or better installed
- working c99 compiler
- MinGW installed under windows if you want to compile for Android and HTML5 platforms
- on macosx through homebrew:  autogen, autoconf, automake,libtool, pkg-config , uuid-dev
- on linux: sudo apt-get install autogen autoconf automake libtool pkg-config build-essential libx11-dev libxmu-dev libxi-dev libgl1-mesa-dev libxrandr-dev libxinerama-dev libxcursor-dev libglu1-mesa-dev libasound2-dev uuid-dev

Then, just run WafaBuilder.exe at the root of the repository, check the toolchains you want to use (only the detected toolchain can be used, if a toolchain is greyed out, check that it's properly installed), check the 'build dependencies' options then click build. The process should take several minutes the first time as it need to download and install dependencies. When the process is finished, you will find a dev_pack.zip file at the root of the repository, it contains everything you need to develop games with the engine.

To Build the game packager, you need nuget : "sudo apt-get install nuget",  then use "nuget restore GamePackager.sln" in game packager folder. 

finally, use xbuild GamePackager.sln, don't worry about info.plist errors on linux
