<p class="lead">
	<strong>Daux.io</strong> is a documentation generator that uses a simple folder structure and Markdown files to create custom documentation on the fly. It helps you create great looking documentation in a developer friendly way.
</p>

---

### Features

---


</div>
<div class="col-third">

#### For Marketing

* 100% Mobile Responsive
* 4 Built-In Themes or roll your own
* Functional, Flat Design Style
* Optional code float layout
* Shareable/Linkable SEO Friendly URLs
* Supports Google Analytics and Piwik Analytics

</div>
</div>

---

### Installation and usage

If you have __PHP__ and Composer installed

```bash
composer global require daux/daux.io

# Next to your `docs` folder, run
daux generate
```

Or if you wish to use __Docker__

```bash
# Next to your `docs` folder, run
docker run --rm -it -w /build -v "$PWD":/build daux/daux.io daux generate
```

---

<!-- Google Code -->
<script type="text/javascript">
var google_conversion_id = 983836026;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
</script>

<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/983836026/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
