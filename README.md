# Disclaimer #

This engine is not for public use, it's only made public because of the Ludum Dare compo rules.

# Description #

Lemoteur is a 2D  game engine written in C with a lua API best used for small and medium sized games. It compile under MSVC 2010 or better, GCC and Emscripten (clang). Platform supported are Windows (7 / 8 / 10) , MacOSX from snow leopard, Linux (Tested under Ubuntu linux, Linux Mint and Arch Linux), Android (from jellybean versions / 4.1 / API 16) and HTML5 / Webgl (Firefox, Chrome, Edge on all supported platforms)

# Example code #

Example demo can be found in client/c/src

# How to build #
First, you need the following installed:

- .NET framework 4.0 or better on Windows (Already installed on Windows 7 and better), mono v4 or better on other platforms (on windows, msbuild must be found from command line, if not put the path to .net framework in PATH)
- nuget https://docs.microsoft.com/fr-fr/nuget/install-nuget-client-tools
- Cmake v3.15 or better installed
- working c99 compiler / c++ compiler on Windows
- MinGW installed under windows if you want to compile for Android and HTML5 platforms
- on MacOSX through homebrew:  autogen, autoconf, automake,libtool, pkg-config , uuid-dev, nuget. If you are on MacOS sierra or later, you need to install the osx 10.9 SDK. See  MacOS additional step.
- on linux: sudo apt-get install autogen autoconf automake libtool pkg-config build-essential libx11-dev libxmu-dev libxi-dev libgl1-mesa-dev libxrandr-dev libxinerama-dev libxcursor-dev libglu1-mesa-dev libasound2-dev uuid-dev nuget

Then, just run WafaBuilder.exe at the root of the repository, or use the following command on MacOSX / linux : 

Mono32 WafaBuilder.exe

check the toolchains you want to use (only the detected toolchain can be used, if a toolchain is greyed out, check that it's properly installed), check the 'build dependencies' options then click build. The process should take several minutes the first time as it need to download and install dependencies. When the process is finished, you will find a dev_pack.zip file at the root of the repository, it contains everything you need to develop games with the engine.

To build the game packager, use "nuget restore GamePackager.sln" in game packager folder beforehand 

finally, use the command 
Msbuild GamePackager.sln /p:Configuration=Release /p:Platform=x64

don't worry about info.plist errors on linux

#  MacOS additional step #

Depending on your version of macos, it's possible that you don't have the OSX SDK 10.9  installed (needed by portaudio), for that you need to use xcodelegacy : 

https://github.com/devernay/xcodelegacy

follow installation instruction (make sure to have a apple developper account in order to download older xcode version), then use the following commands : 

./XcodeLegacy.sh -osx109 buildpackages

./XcodeLegacy.sh -osx109 install

#  Emscripten additional step #

You will need to edit the cmake toolchain provided with emscripten first, see WafaBuilder/emscripten_cmake_patch.txt for the list of variables to edit.

# Updating protobuf interface #

To update protobuf interface, you need protogen and dotnetcore
https://dotnet.microsoft.com/download/dotnet-core/3.1

dotnet tool install --global protobuf-net.Protogen --version 3.0.0
