#!/bin/bash

reckpunk_outdir=/home/venice/Desktop/Reckpunk/
reckpunk_builddir=/home/venice/Projects/reckpunk/
wafa_builddir=/home/venice/Projects/wastedfantasy/
package_dir=/home/venice/Desktop
#usr_bin_dir=/usr/lib/x86_64-linux-gnu/
usr_bin_dir=/home/venice/Projects/steam-runtime-sdk_2013-09-05/runtime/amd64/usr/lib/x89_64-linux-gnu/
usr_bin_local_dir=/home/venice/Projects/steam-runtime-sdk_2013-09-05/runtime/amd64/usr/local/lib/
steam_dir=/home/venice/Projects/steam_sdk/sdk/

echo "copie exécutable"

rm "${reckpunk_outdir}reckpunk"
cp "${reckpunk_builddir}bin/Releasex64/reckpunk" ${reckpunk_outdir}

echo "copie game DLL"

rm "${reckpunk_outdir}ReckGame.so"
cp "${reckpunk_builddir}bin/Releasex64/ReckGame.so" ${reckpunk_outdir}

echo "copie moteur"

rm "${reckpunk_outdir}libwafaengine.so"
cp "${wafa_builddir}dev_pack/libs-linux/libWafaEngine.so" ${reckpunk_outdir}

echo "copie dépendances moteur"

rm "${reckpunk_outdir}libasound.so.2"
rm "${reckpunk_outdir}libFLAC.so.8"
rm "${reckpunk_outdir}libGLEW.so.1.8"
rm "${reckpunk_outdir}libglfw.so.3"
rm "${reckpunk_outdir}libogg.so.0"
rm "${reckpunk_outdir}libportaudio.so.2"
rm "${reckpunk_outdir}libsndfile.so.1"
rm "${reckpunk_outdir}libvorbis.so.0"
rm "${reckpunk_outdir}libvorbisenc.so.2"
rm "${reckpunk_outdir}libXxf86vm.so.1"
rm "${reckpunk_outdir}libXi.so.6.1.0"
rm "${reckpunk_outdir}libXcursor.so.1.0.2"
rm "${reckpunk_outdir}libXinerama.so.1.0.0"
rm "${reckpunk_outdir}libXrandr.so.2.2.0"
rm "${reckpunk_outdir}libXrender.so.1.3.0"


#cp "${usr_bin_dir}libasound.so.2" ${reckpunk_outdir}
#cp /usr/local/lib/libFLAC.so.8 ${reckpunk_outdir}
cp "${wafa_builddir}dev_pack/libs-linux/libGLEW.so" ${reckpunk_outdir}
cp "${wafa_builddir}dev_pack/libs-linux/libglfw.so" ${reckpunk_outdir}
cp "${wafa_builddir}dev_pack/libs-linux/libportaudio.so" ${reckpunk_outdir}
cp "${wafa_builddir}dev_pack/libs-linux/libsndfile.so" ${reckpunk_outdir}
cp "${wafa_builddir}dev_pack/libs-linux/libsndfile.so" ${reckpunk_outdir}
cp "${steam_dir}redistributable_bin/linux64/libsteam_api.so" ${reckpunk_outdir}

#cp "${usr_bin_dir}libXxf86vm.so.1" ${reckpunk_outdir}
#cp "${usr_bin_dir}libXi.so.6.1.0" ${reckpunk_outdir}
#cp "${usr_bin_dir}libXcursor.so.1.0.2" ${reckpunk_outdir}
#cp "${usr_bin_dir}libXinerama.so.1.0.0" ${reckpunk_outdir}
#cp "${usr_bin_dir}libXrandr.so.2.2.0" ${reckpunk_outdir}
#cp "${usr_bin_dir}libXrandr.so.2.2.0" ${reckpunk_outdir}
#cp "${usr_bin_dir}libXrender.so.1.3.0" ${reckpunk_outdir}

echo "packaging final"


cd $package_dir

#tar -czvf "Reckpunk_"$1"_x64.tar.gz" Reckpunk

echo "terminé !"

exit 0
