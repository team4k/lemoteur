#!/bin/bash

reckpunk_outdir=/home/venice/Desktop/Reckpunk_demo/
reckpunk_builddir=/home/venice/Projects/reckpunk/
wafa_builddir=/home/venice/Projects/wastedfantasy/
package_dir=/home/venice/Desktop
usr_bin_dir=/usr/lib/x86_64-linux-gnu/

echo "copie exécutable"

rm "${reckpunk_outdir}reckpunk"
cp "${reckpunk_builddir}bin/Releasex64/reckpunk" ${reckpunk_outdir}

echo "copie game DLL"

rm "${reckpunk_outdir}ReckGame.so"
cp "${reckpunk_builddir}bin/Releasex64/ReckGame.so" ${reckpunk_outdir}

echo "copie moteur"

rm "${reckpunk_outdir}libwafaengine.so"
cp "${wafa_builddir}engine/c/dist/Releasex64/GNU-Linux-x86/libwafaengine.so" ${reckpunk_outdir}

echo "copie dépendances moteur"

rm "${reckpunk_outdir}libasound.so.2"
rm "${reckpunk_outdir}libFLAC.so.8"
rm "${reckpunk_outdir}libGLEW.so.1.8"
rm "${reckpunk_outdir}libglfw.so.3"
rm "${reckpunk_outdir}libogg.so.0"
rm "${reckpunk_outdir}libportaudio.so.2"
rm "${reckpunk_outdir}libsndfile.so.1"
rm "${reckpunk_outdir}libvorbis.so.0"
rm "${reckpunk_outdir}libvorbisenc.so.2"
rm "${reckpunk_outdir}libXxf86vm.so.1"


cp "${usr_bin_dir}libasound.so.2" ${reckpunk_outdir}
cp /usr/local/lib/libFLAC.so.8 ${reckpunk_outdir}
cp "${usr_bin_dir}libGLEW.so.1.8" ${reckpunk_outdir}
cp /usr/local/lib/libglfw.so.3 ${reckpunk_outdir}
cp /usr/local/lib/libogg.so.0 ${reckpunk_outdir}
cp /usr/local/lib/libportaudio.so.2 ${reckpunk_outdir}
cp /usr/local/lib/libsndfile.so.1 ${reckpunk_outdir}
cp /usr/local/lib/libvorbis.so.0 ${reckpunk_outdir}
cp /usr/local/lib/libvorbisenc.so.2 ${reckpunk_outdir}
cp "${usr_bin_dir}libXxf86vm.so.1" ${reckpunk_outdir}


echo "packaging final"


cd $package_dir

tar -czvf "Reckpunk_"$1"_x64.tar.gz" Reckpunk_demo

echo "terminé !"

exit 0
