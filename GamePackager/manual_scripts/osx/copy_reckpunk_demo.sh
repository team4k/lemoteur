#!/bin/bash

echo "copie exécutable"

cp /Users/svp/Projects/recklessoctober/Reckpunkdarwin/build/Release/Reckpunkdarwin /Users/svp/Desktop/Reckpunk/Reckpunk_demo/Contents/MacOS/Reckpunk_demo

echo "copie game DLL"

cp /Users/svp/Projects/recklessoctober/ReckGame/Reckgamedarwin/build/Release/libReckgamedarwin.dylib /usr/local/lib
cp /Users/svp/Projects/recklessoctober/ReckGame/Reckgamedarwin/build/Release/libReckgamedarwin.dylib /Users/svp/Desktop/Reckpunk/Reckpunk_demo/Contents/MacOS

echo "copie moteur"

cp /Users/svp/Projects/wastedfantasy/engine/c/WastedEnginedarwin/build/Release/libWastedEnginedarwin.dylib /usr/local/lib

echo "packaging final"

cd /Users/svp/Desktop/Reckpunk

dylibbundler -od -b -x ./Reckpunk_demo/Contents/MacOS/Reckpunk_demo -d ./Reckpunk_demo/Contents/libs

dylibbundler -x ./Reckpunk_demo/Contents/MacOS/libReckgamedarwin.dylib

cp -r ./Reckpunk_demo ../Reckpunk_demo.app

echo "terminé !"

exit 0
