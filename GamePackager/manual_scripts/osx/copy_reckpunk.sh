#!/bin/bash

echo "copie exécutable"

cp /Users/svp/Projects/recklessoctober/Reckpunkdarwin/build/Release/Reckpunkdarwin /Users/svp/Desktop/Reckpunk/Reckpunk/Contents/MacOS/Reckpunk

echo "copie game DLL"

cp /Users/svp/Projects/recklessoctober/ReckGame/Reckgamedarwin/build/Release/libReckgamedarwin.dylib /usr/local/lib
cp /Users/svp/Projects/recklessoctober/ReckGame/Reckgamedarwin/build/Release/libReckgamedarwin.dylib /Users/svp/Desktop/Reckpunk/Reckpunk/Contents/MacOS

echo "copie moteur"

cp /Users/svp/Projects/wastedfantasy/engine/c/WastedEnginedarwin/build/Release/libWastedEnginedarwin.dylib /usr/local/lib

echo "packaging final"

cd /Users/svp/Desktop/Reckpunk

dylibbundler -od -b -x ./Reckpunk/Contents/MacOS/Reckpunk -d ./Reckpunk/Contents/libs

dylibbundler -x ./Reckpunk/Contents/MacOS/libReckgamedarwin.dylib

echo "set permissions"

cp -r ./Reckpunk ../Reckpunk.app


chmod 777 /Users/svp/Desktop/Reckpunk.app
chmod 777 /Users/svp/Desktop/Reckpunk.app/Contents/MacOS
chmod 777 /Users/svp/Desktop/Reckpunk.app/Contents/MacOS/config.lua
chmod 777 /Users/svp/Desktop/Reckpunk.app/Contents/MacOS/save.db

chmod +x /Users/svp/Desktop/Reckpunk.app/Contents/MacOS/launch.sh
chmod +x /Users/svp/Desktop/Reckpunk.app/Contents/MacOS/Reckpunk



echo "terminé !"

exit 0
