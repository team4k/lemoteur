﻿using System;
using Eto;
using Eto.Forms;
using GamePackager;

namespace GamePackager.Desktop
{
    public class Program
    {
        [STAThread]
        public static void Main(string[] args)
        {
            new Application(Platform.Detect).Run(new MainForm()); 
        }
    }
}
