﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using SpriteSheetLibrary;

namespace GameExporter
{
    public class EmscriptenExporter : IPlatformExporter
    {
        public ExportResult Export(string rootFolder, string gamename, string outputPath, ExportSettings settings, ILogInterface logObject, Delegate applicationEventDelegate)
        {
            ExportResult result = new ExportResult();

            //recherche fichier cmake
            if(!File.Exists(rootFolder + @"\game_template\CMakeLists.txt"))
            {
                result.Message = "Aucune fichier CMakeLists.txt dans le répertoire game_template ! abandon...";
                return result;
            }


            //recherche fichier de config
            if (!File.Exists(string.Format(@"{0}\game_template\assets\config_pc.lua", rootFolder)))
            {
                result.Message =  "Aucune fichier config_pc.lua dans le répertoire d'assets ! abandon";
                return result;
            }

            //recherche emcc
            StringBuilder resultLocate = new StringBuilder();

            ProcessStartInfo locateinfo = new ProcessStartInfo(settings.shell, settings.shell_op1 + "where emcc" + settings.shell_op2);
            locateinfo.UseShellExecute = false;
            locateinfo.RedirectStandardOutput = true;
            locateinfo.CreateNoWindow = true;
            locateinfo.RedirectStandardError = true;
            Process locate_proc = Process.Start(locateinfo);
            locate_proc.OutputDataReceived += (sender, e) => resultLocate.Append(e.Data + Environment.NewLine);
            locate_proc.BeginOutputReadLine();
            locate_proc.WaitForExit();

            string root_emscripten = string.Empty;

            if (!string.IsNullOrEmpty(resultLocate.ToString().Trim()))
            {
                string fstring = resultLocate.ToString();
                string path = fstring.Substring(0, fstring.IndexOf(Environment.NewLine));
                path = path.Substring(0, path.LastIndexOf(Path.DirectorySeparatorChar));
                root_emscripten = path;
            }


            //-2- compilation emcc

            //affichage fenêtre export
            logObject.SetTitle("Export Emscripten en cours...");

            logObject.ShowForm();

            OperatingSystem os = Environment.OSVersion;

            PlatformID pid = os.Platform;

            string toolchain = string.Format("-DCMAKE_TOOLCHAIN_FILE={0}/cmake/Modules/Platform/Emscripten.cmake", root_emscripten);

            string makefile_format = "-G \"Unix Makefiles\"";
            string make = "make";

            if (pid == PlatformID.Win32NT)
            {
                makefile_format = "-G \"MinGW Makefiles\"";
                make = "mingw32-make";
            }
                


            logObject.AppendLog("Compilation du fichier projet...");
            

            ProcessStartInfo GenerationInfo = new ProcessStartInfo(settings.shell,settings.shell_op1 +  string.Format("cmake {0} {1}", toolchain, makefile_format) + settings.shell_op2);
            GenerationInfo.UseShellExecute = false;
            GenerationInfo.WorkingDirectory = rootFolder + @"\game_template\";
            GenerationInfo.RedirectStandardOutput = true;
            GenerationInfo.CreateNoWindow = true;
            GenerationInfo.RedirectStandardError = true;

            ProcessStartInfo CompilationInfo = new ProcessStartInfo(settings.shell, settings.shell_op1 + make + settings.shell_op2);
            CompilationInfo.UseShellExecute = false;
            CompilationInfo.WorkingDirectory = rootFolder + @"\game_template\";
            CompilationInfo.RedirectStandardOutput = true;
            CompilationInfo.CreateNoWindow = true;
            CompilationInfo.RedirectStandardError = true;

            if (!settings.doSkipBuild)
            {
                //génération 
                Process GenerationProc = Process.Start(GenerationInfo);
                GenerationProc.OutputDataReceived += (osender, oe) => logObject.AppendLog(oe.Data, threaded: true);
                GenerationProc.ErrorDataReceived += (osender, oe) => logObject.AppendLog(oe.Data, threaded: true);
                GenerationProc.BeginOutputReadLine();
                GenerationProc.BeginErrorReadLine();

                while (!GenerationProc.HasExited)
                {
                    if (applicationEventDelegate != null)
                        applicationEventDelegate.DynamicInvoke();
                }
                   

                //compilation
                Process CompilationProc = Process.Start(CompilationInfo);
                CompilationProc.OutputDataReceived += (osender, oe) => logObject.AppendLog(oe.Data, threaded: true);
                CompilationProc.ErrorDataReceived += (osender, oe) => logObject.AppendLog(oe.Data, threaded: true);
                CompilationProc.BeginOutputReadLine();
                CompilationProc.BeginErrorReadLine();

                while (!CompilationProc.HasExited)
                {
                    if (applicationEventDelegate != null)
                        applicationEventDelegate.DynamicInvoke();
                }
            }
            else
            {
                logObject.AppendLog("Etape de compilation ignorée...");
            }


            // DirectoryInfo rootInfo = new DirectoryInfo(rootFolder);

            //            string export_file_name = rootInfo.Name;

            string cmakelistcontent = File.ReadAllText(rootFolder + @"\game_template\CMakeLists.txt");

            //récupération nom de l'executable dans le cmakefile, toujours déclaré en dernier après le game dll
            const string PROJECT_STR = "project(";
            int indexproj = cmakelistcontent.LastIndexOf(PROJECT_STR);

            if(indexproj == -1)
            {
                result.Message = "Impossible de trouver le nom du projet CMake ! abandon";
                return result;
            }

            int endindex = cmakelistcontent.IndexOf(')', indexproj);

            string export_file_name = cmakelistcontent.Substring(indexproj + PROJECT_STR.Length, endindex - (indexproj + PROJECT_STR.Length));


             string temp_directory = string.Format(@"{0}\game_template\temp_emscripten", rootFolder);

            if (Directory.Exists(temp_directory))
                Directory.Delete(temp_directory, true);

            Directory.CreateDirectory(temp_directory);
            Directory.CreateDirectory(temp_directory + @"\assets");
            Directory.CreateDirectory(temp_directory + @"\scripts");


            //-3- packaging fichier assets + config

            try
            {
                logObject.AppendLog("Copie fichier sauvegarde...");
                File.Copy(string.Format(@"{0}\game_template\assets\save.db", rootFolder), string.Format(@"{0}\game_template\temp_emscripten\save.db", rootFolder), true);
            }
            catch (Exception ex)
            {
                logObject.AppendLog("Erreur lors de la copie du fichier de sauvegarde " + ex.ToString(), LogType.ERROR);
            }

            try
            {
                logObject.AppendLog("Copie fichier locale...");
                File.Copy(string.Format(@"{0}\game_template\assets\locale.db", rootFolder), string.Format(@"{0}\game_template\temp_emscripten\locale.db", rootFolder), true);
            }
            catch (Exception ex)
            {
                logObject.AppendLog("Erreur lors de la copie de locale " + ex.ToString(), LogType.ERROR);
            }



            logObject.AppendLog("Exports assets et fichiers scripts...");

            string exec = string.Format(@"{0}\animation_converter\AnimationConverter.exe {0}\game_template\assets\animated_assets {0}\game_template\assets {0}\game_template\temp_emscripten\assets {0}\game_template\temp_emscripten\scripts {0}\game_template\assets\exclude_pc.txt", rootFolder);

            ProcessStartInfo exportAssetsInfo = new ProcessStartInfo(settings.shell, settings.shell_op1 + exec + settings.shell_op2);
            exportAssetsInfo.UseShellExecute = false;
            exportAssetsInfo.RedirectStandardOutput = true;
            exportAssetsInfo.CreateNoWindow = true;
            exportAssetsInfo.RedirectStandardError = true;
            exportAssetsInfo.RedirectStandardInput = true;

            Process exportAssetsProc = Process.Start(exportAssetsInfo);
            exportAssetsProc.OutputDataReceived += (osender, oe) => logObject.AppendLog(oe.Data, threaded: true);
            exportAssetsProc.ErrorDataReceived += (osender, oe) => logObject.AppendLog(oe.Data, threaded: true);
            exportAssetsProc.BeginOutputReadLine();
            exportAssetsProc.BeginErrorReadLine();
            exportAssetsProc.WaitForExit();

            while (!exportAssetsProc.HasExited)
            {
                if (applicationEventDelegate != null)
                    applicationEventDelegate.DynamicInvoke();
            }

            //msdf - atlas - gen.exe - font..\game_template\assets\visitor1.ttf - charset charset.txt - size 48 - potr - format png - type msdf - imageout..\game_template\bin\assets\visitor1.png - json..\game_template\bin\assets\visitor1.json
            logObject.AppendLog("Exports polices de caractères...");

            IEnumerable<string> listfont = Directory.EnumerateFiles(rootFolder + @"\game_template\assets\", "*.ttf");

            foreach(string font in listfont)
            {
                FileInfo inf = new FileInfo(font);

                string filename = inf.Name.Substring(0, inf.Name.LastIndexOf('.'));

                string execfont = string.Format(@"{0}\tools\msdf-atlas-gen.exe -font {0}\game_template\assets\{1}.ttf -charset {0}\tools\charset.txt -size 48 -potr -format png -type msdf -imageout {0}\game_template\temp_emscripten\assets\{1}.png -json {0}\game_template\temp_emscripten\assets\{1}.json", rootFolder, filename);

                logObject.AppendLog(execfont);

                ProcessStartInfo exportFontInfo = new ProcessStartInfo(settings.shell, settings.shell_op1 + execfont + settings.shell_op2);
                exportFontInfo.UseShellExecute = false;
                exportFontInfo.RedirectStandardOutput = true;
                exportFontInfo.CreateNoWindow = true;
                exportFontInfo.RedirectStandardError = true;
                exportFontInfo.RedirectStandardInput = true;

                Process exportFontProc = Process.Start(exportFontInfo);
                exportFontProc.OutputDataReceived += (osender, oe) => logObject.AppendLog(oe.Data, threaded: true);
                exportFontProc.ErrorDataReceived += (osender, oe) => logObject.AppendLog(oe.Data, threaded: true);
                exportFontProc.BeginOutputReadLine();
                exportFontProc.BeginErrorReadLine();
                exportFontProc.WaitForExit();

                while (!exportFontProc.HasExited)
                {
                    if (applicationEventDelegate != null)
                        applicationEventDelegate.DynamicInvoke();
                }
            }
           

            int packer_result = 0;

            //-4.1- packing texture si nécessaire
            if (settings.packTextures)
            {
                logObject.AppendLog("Packing texture...");

                string[] exclude = File.ReadAllLines(string.Format(@"{0}\game_template\assets\exclude_packer.txt", rootFolder));

                List<string> list_exclude = new List<string>(exclude);

                packer_result = SpriteSheetPack.Pack(string.Format(@"{0}\game_template\temp_emscripten\assets\", rootFolder), true, (settings.packTextureHeight == settings.packTextureWidth), (int)settings.packTextureWidth, (int)settings.packTextureHeight, 2, string.Format(@"{0}\game_template\temp_emscripten\assets\{1}", rootFolder, "packtexture.png"), string.Format(@"{0}\game_template\temp_emscripten\assets\{1}", rootFolder, "packtexture.pack"), true, list_exclude);

                if (packer_result != 0)
                {
                    logObject.AppendLog("Erreur lors du packing de texture ", LogType.ERROR);
                }
            }

            try
            {
                logObject.AppendLog("Modification fichier config...");

                var text = File.ReadAllText(string.Format(@"{0}\game_template\assets\config_pc.lua", rootFolder));

                int has_asset = text.IndexOf("asset_path");

                if (has_asset != -1)
                {
                    int start_asset = text.IndexOf('"', has_asset);
                    int end_asset = text.IndexOf('"', start_asset + 1) + 1;

                    text = text.Remove(start_asset, end_asset - start_asset);

                    text = text.Insert(start_asset, "\"assets\"");
                }

                int has_scripts = text.IndexOf("script_path");

                if (has_scripts != -1)
                {
                    int start_script = text.IndexOf('"', has_scripts);
                    int end_script = text.IndexOf('"', start_script + 1) + 1;

                    text = text.Remove(start_script, end_script - start_script);

                    text = text.Insert(start_script, "\"scripts\"");
                }

                if (settings.packTextures && packer_result == 0)
                {
                    int first_pos = text.IndexOf('{') + 1;

                    text = text.Insert(first_pos, "\npacker = \"packtexture.pack\",");
                }

                File.WriteAllText(string.Format(@"{0}\game_template\temp_emscripten\config.lua", rootFolder), text);
            }
            catch (Exception ex)
            {
                logObject.AppendLog("Erreur lors de la modification du fichier de config " + ex.ToString(), LogType.ERROR);
            }



            //-3.1- appel packager emscripten
            //recherche chemin emcc
            logObject.AppendLog("Création fichier data...");
            ProcessStartInfo findemccinfo = new ProcessStartInfo(settings.shell, settings.shell_op1 +  "where emcc"+ settings.shell_op2); //which sous linux
            findemccinfo.UseShellExecute = false;
            findemccinfo.RedirectStandardOutput = true;
            findemccinfo.CreateNoWindow = true;
            findemccinfo.RedirectStandardError = true;

            string emcc_lines = string.Empty;

            Process findemccproc = Process.Start(findemccinfo);
            findemccproc.OutputDataReceived += (osender, oe) =>
            {
                if (string.IsNullOrEmpty(emcc_lines))
                    emcc_lines = oe.Data;
            };

            findemccproc.BeginOutputReadLine();
            findemccproc.WaitForExit();

            //récup chemin
            string emscriptenpath = emcc_lines.Substring(0, emcc_lines.LastIndexOf("emcc"));

            //packing emscripten
            ProcessStartInfo packemscripteninfo = new ProcessStartInfo(settings.shell, settings.shell_op1 + string.Format(@"python {0}\tools\file_packager.py {1}_assets.data --preload assets scripts save.db locale.db config.lua > {1}_assets.js", emscriptenpath, export_file_name) + settings.shell_op2);
            packemscripteninfo.WorkingDirectory = rootFolder + @"\game_template\temp_emscripten";
            packemscripteninfo.UseShellExecute = false;
            packemscripteninfo.RedirectStandardOutput = true;
            packemscripteninfo.CreateNoWindow = true;
            packemscripteninfo.RedirectStandardError = true;

            Process packemscriptenproc = Process.Start(packemscripteninfo);
            packemscriptenproc.OutputDataReceived += (osender, oe) => logObject.AppendLog(oe.Data, threaded: true);
            packemscriptenproc.BeginOutputReadLine();

            while (!packemscriptenproc.HasExited)
            {
                if (applicationEventDelegate != null)
                    applicationEventDelegate.DynamicInvoke();
            }

            //-4- calcul taille js et data générées + modification fichier template HTML 
            logObject.AppendLog("Calcul taille fichiers générés...");
            long game_size, data_size, game_gzip = 0, data_gzip = 0;

            FileInfo sizeinf = new FileInfo(rootFolder + @"\game_template\" + export_file_name + ".js");

            game_size = sizeinf.Length;

            sizeinf = new FileInfo(rootFolder + @"\game_template\temp_emscripten\" + export_file_name + "_assets.data");
            data_size = sizeinf.Length;

            ProcessStartInfo gzinfo = new ProcessStartInfo(settings.shell, settings.shell_op1 + string.Format(@"gzip -c {0}\game_template\{1}.js | wc -c ", rootFolder, export_file_name) + settings.shell_op2);
            gzinfo.UseShellExecute = false;
            gzinfo.RedirectStandardInput = true;
            gzinfo.RedirectStandardOutput = true;
            gzinfo.RedirectStandardError = true;
            gzinfo.CreateNoWindow = true;

            Process gzproc = Process.Start(gzinfo);

            StreamReader myOutput = gzproc.StandardOutput;

            string value = myOutput.ReadToEnd();

            if(!string.IsNullOrEmpty(value))
                game_gzip = Convert.ToInt64(value);

            gzproc.Close();


            gzinfo = new ProcessStartInfo(settings.shell, settings.shell_op1 + string.Format(@"gzip -c {0}\game_template\temp_emscripten\{1}_assets.data | wc -c", rootFolder, export_file_name) + settings.shell_op2);
            gzinfo.UseShellExecute = false;
            gzinfo.RedirectStandardOutput = true;
            gzinfo.RedirectStandardError = true;
            gzinfo.CreateNoWindow = true;

            gzproc = Process.Start(gzinfo);
            myOutput = gzproc.StandardOutput;

            string value2 = myOutput.ReadToEnd();

            if (!string.IsNullOrEmpty(value2))
                data_gzip = Convert.ToInt64(value2);

            gzproc.Close();


            logObject.AppendLog("Création HTML depuis template...");

            StringBuilder html_template_content = new StringBuilder(EmscriptenResources.emscripten_template);

            html_template_content.Replace("Template_game", gamename);
            html_template_content.Replace("_HERE_GAME_SIZE_", game_size.ToString());
            html_template_content.Replace("_HERE_DATA_SIZE_", data_size.ToString());
            html_template_content.Replace("_HERE_GAME_GZSIZE_", game_gzip.ToString());
            html_template_content.Replace("_HERE_DATA_GZSIZE_", data_gzip.ToString());
            html_template_content.Replace("_HERE_GAMEJS_NAME_", "\"" + export_file_name + ".js\"");
            html_template_content.Replace("_HERE_DATA_NAME_", "\"" + export_file_name + "_assets.data\"");
            html_template_content.Replace("_HERE_ASSETS_LOADER_", "\"" + export_file_name + "_assets.js\"");



            //-5- copie vers répertoire de destination
            try
            {
                logObject.AppendLog("Copie des fichiers dans le répertoire de destination...");
                File.Copy(rootFolder + @"\libs-emscripten\astar_worker.js", outputPath + @"\astar_worker.js", true);

                File.Copy(rootFolder + @"\libs-emscripten\astar_worker.wasm", outputPath + @"\astar_worker.wasm", true);
                File.Copy(rootFolder + @"\game_template\temp_emscripten\" + export_file_name + "_assets.data", outputPath + @"\" + export_file_name + "_assets.data", true);
                File.Copy(rootFolder + @"\game_template\temp_emscripten\" + export_file_name + "_assets.js", outputPath + @"\" + export_file_name + "_assets.js", true);

                File.Copy(rootFolder + @"\game_template\" + export_file_name + ".js", outputPath + @"\" + export_file_name + ".js", true);
                File.Copy(rootFolder + @"\game_template\" + export_file_name + ".wasm", outputPath + @"\" + export_file_name + ".wasm", true);

                if (File.Exists(rootFolder + @"\game_template\" + export_file_name + ".wasm"))
                    File.Copy(rootFolder + @"\game_template\" + export_file_name + ".wasm", outputPath + @"\" + export_file_name + ".wasm", true);

                File.WriteAllText(outputPath + @"\" + export_file_name + ".html", html_template_content.ToString());

                if (Directory.Exists(outputPath + @"\style"))
                    Directory.Delete(outputPath + @"\style", true);

                Directory.CreateDirectory(outputPath + @"\style");

                if (Directory.Exists(outputPath + @"\scripts"))
                    Directory.Delete(outputPath + @"\scripts", true);

                Directory.CreateDirectory(outputPath + @"\scripts");


                File.WriteAllText(outputPath + @"\style\main.css", EmscriptenResources.main);
                File.WriteAllText(outputPath + @"\scripts\jquery-2.1.4.min.js", EmscriptenResources.jquery_2_1_4_min);
                File.WriteAllText(outputPath + @"\scripts\jquery.browser.min.js", EmscriptenResources.jquery_browser_min);
                File.WriteAllText(outputPath + @"\scripts\game_loader.js", EmscriptenResources.game_loader);
            }
            catch (Exception ex)
            {
                logObject.AppendLog("Erreur lors de la copie des fichiers dans le répertoire de destination " + ex.ToString());
            }

            //suppression répertoire temporaires
            Directory.Delete(temp_directory, true);

            logObject.AppendLog("Export Emscripten terminé !");
            logObject.SetTitle("Export Emscripten terminé !");

            result.Result = Result.Success;
            return result;
        }
    }
}
