﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Diagnostics;
using SpriteSheetLibrary;

namespace GameExporter
{
    public class AndroidExporter : IPlatformExporter
    {
        public ExportResult Export(string rootFolder, string gamename, string outputPath, ExportSettings settings, ILogInterface logObject, Delegate applicationEventDelegate)
        {
            ExportResult result = new ExportResult();

            if (!File.Exists(string.Format(@"{0}\game_template\assets\config_android.lua", rootFolder)))
            {
                result.Message = "Aucune fichier config_android.lua dans le répertoire d'assets ! abandon";
                return result;
            }


            logObject.SetTitle("Export Android en cours...");

            //création des répertoire nécessaire à l'export android si ils n'existent pas
            if (!Directory.Exists(string.Format(@"{0}\game_template\template_android\Assets", rootFolder)))
                Directory.CreateDirectory(string.Format(@"{0}\game_template\template_android\Assets", rootFolder));

            if (!Directory.Exists(string.Format(@"{0}\game_template\template_android\bin", rootFolder)))
                Directory.CreateDirectory(string.Format(@"{0}\game_template\template_android\bin", rootFolder));

            //suppression du contenu du répertoire assets
            foreach (string deletefile in Directory.GetFiles(string.Format(@"{0}\game_template\template_android\Assets", rootFolder)))
                File.Delete(deletefile);

            //-1-export assets,save.db et locale.db
            logObject.AppendLog("Exports assets et fichiers scripts..");
            ProcessStartInfo exportAssetsInfo = new ProcessStartInfo("cmd.exe", string.Format(@"/c {0}\animation_converter\AnimationConverter.exe {0}\game_template\assets\animated_assets {0}\game_template\assets {0}\game_template\template_android\Assets {0}\game_template\template_android\Assets {0}\game_template\assets\exclude_android.txt", rootFolder));
            exportAssetsInfo.UseShellExecute = false;
            exportAssetsInfo.RedirectStandardOutput = true;

            Process exportAssetsProc = Process.Start(exportAssetsInfo);
            exportAssetsProc.OutputDataReceived += (osender, oe) => logObject.AppendLog(oe.Data, threaded: true);
            exportAssetsProc.BeginOutputReadLine();

            while (!exportAssetsProc.HasExited)
                applicationEventDelegate.DynamicInvoke();

            try
            {
                logObject.AppendLog("Copie fichier sauvegarde...");
                File.Copy(string.Format(@"{0}\game_template\assets\save.db", rootFolder), string.Format(@"{0}\game_template\template_android\Assets\save.db", rootFolder), true);
            }
            catch (Exception ex)
            {
                logObject.AppendLog("Erreur lors de la copie du fichier de sauvegarde " + ex.ToString(), LogType.ERROR);
            }



            try
            {
                logObject.AppendLog("Copie fichier locale...");
                File.Copy(string.Format(@"{0}\game_template\assets\locale.db", rootFolder), string.Format(@"{0}\game_template\template_android\Assets\locale.db", rootFolder), true);
            }
            catch (Exception ex)
            {
                logObject.AppendLog("Erreur lors de la copie de locale " + ex.ToString(), LogType.ERROR);
            }

            int packer_result = 0;

            //-4.1- packing texture si nécessaire
            if (settings.packTextures)
            {
                logObject.AppendLog("Packing texture...");

                string[] exclude = File.ReadAllLines(string.Format(@"{0}\game_template\assets\exclude_packer.txt", rootFolder));

                List<string> list_exclude = new List<string>(exclude);

                packer_result = SpriteSheetPack.Pack(string.Format(@"{0}\game_template\template_android\Assets\", rootFolder), true, (settings.packTextureHeight == settings.packTextureWidth), (int)settings.packTextureWidth, (int)settings.packTextureHeight, 2, string.Format(@"{0}\game_template\template_android\Assets\{1}", rootFolder, "packtexture.png"), string.Format(@"{0}\game_template\template_android\Assets\{1}", rootFolder, "packtexture.pack"), true, list_exclude);

                if (packer_result != 0)
                {
                    logObject.AppendLog("Erreur lors du packing de texture ", LogType.ERROR);
                }
            }


            try
            {
                logObject.AppendLog("Modification fichier config...");

                var text = File.ReadAllText(string.Format(@"{0}\game_template\assets\config_android.lua", rootFolder));

                if (settings.packTextures && packer_result == 0)
                {
                    int first_pos = text.IndexOf('{') + 1;

                    text = text.Insert(first_pos, "\npacker = \"packtexture.pack\",");
                }

                File.WriteAllText(string.Format(@"{0}\game_template\template_android\Assets\config.lua", rootFolder), text);
            }
            catch (Exception ex)
            {
                logObject.AppendLog("Erreur lors de la modification du fichier de config " + ex.ToString(), LogType.ERROR);
            }


            //-2- appel ant pour création apk

            string build_params = "debug";

            if (settings.exportCleanBuild)
                build_params = "clean " + build_params;

            ProcessStartInfo antbuildInfo = new ProcessStartInfo("cmd.exe", string.Format(@"/c ""{0}\bin\ant"" -f {1}\game_template\template_android {2}", settings.antFolder, rootFolder, build_params));
            antbuildInfo.EnvironmentVariables["JAVA_HOME"] = settings.jdkFolder;
            antbuildInfo.UseShellExecute = false;
            antbuildInfo.RedirectStandardOutput = true;
            antbuildInfo.RedirectStandardError = true;

            Process antbuildProc = Process.Start(antbuildInfo);
            antbuildProc.OutputDataReceived += (osender, oe) => logObject.AppendLog(oe.Data, threaded: true);
            antbuildProc.ErrorDataReceived += (osender2, oe2) => logObject.AppendLog(oe2.Data, threaded: true);
            antbuildProc.BeginOutputReadLine();
            antbuildProc.BeginErrorReadLine();

            while (!antbuildProc.HasExited)
                applicationEventDelegate.DynamicInvoke();

            //-3- copie de l'apk dans le répertoire cible
            string[] apk_list = Directory.GetFiles(string.Format(@"{0}\game_template\template_android\bin", rootFolder), "*.apk");
            string generated_apk = string.Empty;

            foreach (string apk_file in apk_list)
            {
                FileInfo finfo = new FileInfo(apk_file);

                if (!finfo.Name.Contains("-unaligned"))
                {
                    if (string.IsNullOrEmpty(generated_apk))
                        generated_apk = apk_file;
                    else if (!finfo.Name.Contains("-debug"))
                        generated_apk = apk_file;
                }
            }

            try
            {
                logObject.AppendLog("Copie de l'apk dans le répertoire cible");
                File.Copy(generated_apk, outputPath, true);
            }
            catch (Exception ex)
            {
                logObject.AppendLog("Erreur lors de la copie de l'apk dans le répertoire cible " + ex.ToString(), LogType.ERROR);
            }

            result.Result = Result.Success;
            logObject.SetTitle("Export Android terminé !");

            return result;
        }
    }
}
