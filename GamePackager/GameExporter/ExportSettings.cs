﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameExporter
{
    public class ExportSettings
    {
        //texture packing settings
        public bool packTextures { get; set; }
        public int packTextureWidth { get; set; }
        public int packTextureHeight { get; set; }

        //clean build directory before export ?
        public bool exportCleanBuild { get; set; }

        //jdk folder (android export)
        public string jdkFolder { get; set; }
        //ant folder (android export)
        public string antFolder { get; set; }

        //skip compilation when exporting ?
        public bool doSkipBuild { get; set; }

        public string shell { get; set; }
        public string shell_op1 { get; set; }
        public string shell_op2 { get; set; }
    }
}
