﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using SpriteSheetLibrary;
using System.Linq;
using System.Text;

namespace GameExporter
{
    public class WindowsExporter : IPlatformExporter
    {
        public ExportResult Export(string rootFolder, string gamename, string outputPath,ExportSettings settings,ILogInterface logObject,Delegate applicationEventDelegate)
        {
            ExportResult result = new ExportResult();
            //recherche fichier sln
            string[] solutionFiles = Directory.GetFiles(rootFolder + @"\game_template\", "*.sln");

            if (solutionFiles.Length == 0)
            {
                result.Message = "Aucun fichier .sln dans le répertoire game_template ! abandon...";
                return result;
            }

            //recherche fichier de config
            if (!File.Exists(string.Format(@"{0}\game_template\assets\config_pc.lua", rootFolder)))
            {
                result.Message = "Aucune fichier config_pc.lua dans le répertoire d'assets ! abandon";
                return result;
            }

            if (solutionFiles.Length > 1)
                logObject.AppendLog("Le répertoire game_template contient plus qu'un fichier .sln, le premier fichier " + solutionFiles[0] + " sera utilisé", LogType.WARNING);



            //-2- compilation game template (DLL plus client)

            logObject.AppendLog("Compilation du fichier projet...");
            ProcessStartInfo CompilationInfo = new ProcessStartInfo("cmd.exe", string.Format(@"/c MSBuild {0} /p:Configuration=Release /p:Platform=Win32", solutionFiles[0]));
            CompilationInfo.UseShellExecute = false;
            CompilationInfo.RedirectStandardOutput = true;

            Process CompilationProc = Process.Start(CompilationInfo);
            CompilationProc.OutputDataReceived += (osender, oe) => logObject.AppendLog(oe.Data, threaded: true);
            CompilationProc.BeginOutputReadLine();

            while (!CompilationProc.HasExited)
                applicationEventDelegate.DynamicInvoke();


            string temp_directory = string.Format(@"{0}\game_template\temp_windows", rootFolder);

            if (Directory.Exists(temp_directory))
                Directory.Delete(temp_directory, true);

            Directory.CreateDirectory(temp_directory);
            Directory.CreateDirectory(temp_directory + @"\assets");
            Directory.CreateDirectory(temp_directory + @"\scripts");

            //-3- export assets, save.db et locale.db
            logObject.AppendLog("Exports assets et fichiers scripts...");
            ProcessStartInfo exportAssetsInfo = new ProcessStartInfo("cmd.exe", string.Format(@"/c {0}\animation_converter\AnimationConverter.exe {0}\game_template\assets\animated_assets {0}\game_template\assets {0}\game_template\temp_windows\assets {0}\game_template\temp_windows\scripts {0}\game_template\assets\exclude_pc.txt", rootFolder));
            exportAssetsInfo.UseShellExecute = false;
            exportAssetsInfo.RedirectStandardOutput = true;

            Process exportAssetsProc = Process.Start(exportAssetsInfo);
            exportAssetsProc.OutputDataReceived += (osender, oe) => logObject.AppendLog(oe.Data, threaded: true);
            exportAssetsProc.BeginOutputReadLine();

            while (!exportAssetsProc.HasExited)
                applicationEventDelegate.DynamicInvoke();



            try
            {
                logObject.AppendLog("Copie fichier sauvegarde...");
                File.Copy(string.Format(@"{0}\game_template\assets\save.db", rootFolder), string.Format(@"{0}\game_template\bin\Release\save.db", rootFolder), true);
            }
            catch (Exception ex)
            {
                logObject.AppendLog("Erreur lors de la copie du fichier de sauvegarde " + ex.ToString(), LogType.ERROR);
            }

            try
            {
                logObject.AppendLog("Copie fichier locale...");
                File.Copy(string.Format(@"{0}\game_template\assets\locale.db", rootFolder), string.Format(@"{0}\game_template\bin\Release\locale.db", rootFolder), true);
            }
            catch (Exception ex)
            {
                logObject.AppendLog("Erreur lors de la copie de locale " + ex.ToString(), LogType.ERROR);
            }

            //-4- copie fichiers dans répertoire temporaire
            string tempDirectory = string.Format(@"{0}\game_template\bin\Releasetemp\", rootFolder);

            try
            {
                logObject.AppendLog("Création répertoire temporaire...");
                Directory.CreateDirectory(tempDirectory);

                logObject.AppendLog("Copie fichiers...");

                foreach (string file in Directory.GetFiles(string.Format(@"{0}\game_template\bin\Release", rootFolder)))
                {
                    FileInfo inf = new FileInfo(file);

                    if (inf.Extension != ".log" && inf.Extension != ".pdb" && inf.Extension != ".exp" && inf.Extension != ".lib")
                        File.Copy(file, tempDirectory + inf.Name, true);
                }

                logObject.AppendLog("Copie librairie standard...");

                File.Copy(string.Format(@"{0}\libs-msvc\msvcp100.dll", rootFolder), tempDirectory + "msvcp100.dll", true);
                File.Copy(string.Format(@"{0}\libs-msvc\msvcr100.dll", rootFolder), tempDirectory + "msvcr100.dll", true);

            }
            catch (Exception ex)
            {
                logObject.AppendLog("Erreur lors de la création du répertoire temporaire " + ex.ToString(), LogType.ERROR);
            }

            int packer_result = 0;

            //-4.1- packing texture si nécessaire
            if (settings.packTextures)
            {
                logObject.AppendLog("Packing texture...");

                string[] exclude = File.ReadAllLines(string.Format(@"{0}\game_template\assets\exclude_packer.txt", rootFolder));

                List<string> list_exclude = new List<string>(exclude);


                packer_result = SpriteSheetPack.Pack(string.Format(@"{0}\game_template\temp_windows\assets\", rootFolder), true, (settings.packTextureWidth == settings.packTextureHeight), (int)settings.packTextureWidth, (int)settings.packTextureHeight, 2, string.Format(@"{0}\game_template\temp_windows\assets\{1}", rootFolder, "packtexture.png"), string.Format(@"{0}\game_template\temp_windows\assets\{1}", rootFolder, "packtexture.pack"), true, list_exclude);

                if (packer_result != 0)
                {
                    logObject.AppendLog("Erreur lors du packing de texture ", LogType.ERROR);
                }
            }

            //-5- zip assets
            List<string> assets_folder = new List<string>()
            {
                string.Format(@"{0}\game_template\temp_windows\assets", rootFolder),
                string.Format(@"{0}\game_template\temp_windows\scripts", rootFolder)
            };

            try
            {
                logObject.AppendLog("Compression assets...");
                ZipUtils.CreateArchive(string.Format(@"{0}\game_template\bin\Releasetemp\data.zip", rootFolder), assets_folder, new List<string>());

            }
            catch (Exception ex)
            {
                logObject.AppendLog("Erreur lors de la compression des assets " + ex.ToString(), LogType.ERROR);
            }

            try
            {
                logObject.AppendLog("Modification fichier config...");
                //-6- modif chemin vers assets dans fichier de config
                var text = File.ReadAllText(string.Format(@"{0}\game_template\assets\config_pc.lua", rootFolder));

                int has_asset = text.IndexOf("asset_path");

                if (has_asset != -1)
                {
                    int start_asset = text.IndexOf('"', has_asset);
                    int end_asset = text.IndexOf('"', start_asset + 1) + 1;

                    text = text.Remove(start_asset, end_asset - start_asset);

                    text = text.Insert(start_asset, "\"data.zip\"");
                }

                int has_scripts = text.IndexOf("script_path");

                if (has_scripts != -1)
                {
                    int start_script = text.IndexOf('"', has_scripts);
                    int end_script = text.IndexOf('"', start_script + 1) + 1;

                    text = text.Remove(start_script, end_script - start_script);

                    text = text.Insert(start_script, "\"data.zip\"");
                }

                if (settings.packTextures && packer_result == 0)
                {
                    int first_pos = text.IndexOf('{') + 1;

                    text = text.Insert(first_pos, "\npacker = \"packtexture.pack\",");
                }

                File.WriteAllText(string.Format(@"{0}\game_template\bin\Releasetemp\config.lua", rootFolder), text);
            }
            catch (Exception ex)
            {
                logObject.AppendLog("Erreur lors de la modification du fichier de config " + ex.ToString(), LogType.ERROR);
            }

            //-7- zip dossier jeu dans destination finale 

            List<string> _gamefolder = new List<string>()
            {
              tempDirectory
            };


            List<string> _replace_name = new List<string>()
            {
              gamename
            };

            try
            {
                logObject.AppendLog("Compression dossier jeu...");
                ZipUtils.CreateArchive(outputPath, _gamefolder, _replace_name);

            }
            catch (Exception ex)
            {
                logObject.AppendLog("Erreur lors de la compression du dossier jeu" + ex.ToString(), LogType.ERROR);
            }

            //-8- suppression dossier temporaire
            try
            {
                logObject.AppendLog("Suppression répertoire temporaire...");
                Directory.Delete(tempDirectory, true);
                Directory.Delete(temp_directory, true);

            }
            catch (Exception ex)
            {
                logObject.AppendLog("Erreur lors de la suppression du répertoire temporaire" + ex.ToString(), LogType.ERROR);
            }

            result.Result = Result.Success;

            logObject.AppendLog("Export terminé !");
            logObject.SetTitle("Export Windows terminé !");

            return result;
        }
    }
}
