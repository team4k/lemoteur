﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using SpriteSheetLibrary;

namespace GameExporter
{
    public class LinuxExporter : IPlatformExporter
    {
        public ExportResult Export(string rootFolder, string gamename, string outputPath, ExportSettings settings, ILogInterface logObject, Delegate applicationEventDelegate)
        {
            ExportResult result = new ExportResult();

            //recherche fichier cmake
            if(!File.Exists(rootFolder + @"\game_template\CMakeLists.txt"))
            {
                result.Message = "Aucune fichier CMakeLists.txt dans le répertoire game_template ! abandon...";
                return result;
            }


            //recherche fichier de config
            if (!File.Exists(string.Format(@"{0}\game_template\assets\config_pc.lua", rootFolder)))
            {
                result.Message =  "Aucune fichier config_pc.lua dans le répertoire d'assets ! abandon";
                return result;
            }


            //-2- compilation gcc

            //affichage fenêtre export
            logObject.SetTitle("Export Linux en cours...");

            logObject.ShowForm();

            OperatingSystem os = Environment.OSVersion;

            PlatformID pid = os.Platform;

            string makefile_format = "-G \"Unix Makefiles\"";
            string make = "make";
                


            logObject.AppendLog("Compilation du fichier projet...");
            

            ProcessStartInfo GenerationInfo = new ProcessStartInfo(settings.shell,settings.shell_op1 +  string.Format("cmake {0}", makefile_format) + settings.shell_op2);
            GenerationInfo.UseShellExecute = false;
            GenerationInfo.WorkingDirectory = rootFolder + @"\game_template\";
            GenerationInfo.RedirectStandardOutput = true;
            GenerationInfo.CreateNoWindow = true;
            GenerationInfo.RedirectStandardError = true;

            ProcessStartInfo CompilationInfo = new ProcessStartInfo(settings.shell, settings.shell_op1 + make + settings.shell_op2);
            CompilationInfo.UseShellExecute = false;
            CompilationInfo.WorkingDirectory = rootFolder + @"\game_template\";
            CompilationInfo.RedirectStandardOutput = true;
            CompilationInfo.CreateNoWindow = true;
            CompilationInfo.RedirectStandardError = true;

            if (!settings.doSkipBuild)
            {
                //génération 
                Process GenerationProc = Process.Start(GenerationInfo);
                GenerationProc.OutputDataReceived += (osender, oe) => logObject.AppendLog(oe.Data, threaded: true);
                GenerationProc.ErrorDataReceived += (osender, oe) => logObject.AppendLog(oe.Data, threaded: true);
                GenerationProc.BeginOutputReadLine();
                GenerationProc.BeginErrorReadLine();

                while (!GenerationProc.HasExited)
                {
                    if (applicationEventDelegate != null)
                        applicationEventDelegate.DynamicInvoke();
                }
                   

                //compilation
                Process CompilationProc = Process.Start(CompilationInfo);
                CompilationProc.OutputDataReceived += (osender, oe) => logObject.AppendLog(oe.Data, threaded: true);
                CompilationProc.ErrorDataReceived += (osender, oe) => logObject.AppendLog(oe.Data, threaded: true);
                CompilationProc.BeginOutputReadLine();
                CompilationProc.BeginErrorReadLine();

                while (!CompilationProc.HasExited)
                {
                    if (applicationEventDelegate != null)
                        applicationEventDelegate.DynamicInvoke();
                }
            }
            else
            {
                logObject.AppendLog("Etape de compilation ignorée...");
            }


            DirectoryInfo rootInfo = new DirectoryInfo(rootFolder);

            string export_file_name = rootInfo.Name;


            string temp_directory = string.Format(@"{0}\game_template\temp_linux", rootFolder);

            if (Directory.Exists(temp_directory))
                Directory.Delete(temp_directory, true);

            Directory.CreateDirectory(temp_directory);
            Directory.CreateDirectory(temp_directory + @"\assets");
            Directory.CreateDirectory(temp_directory + @"\scripts");


            //-3- packaging fichier assets + config

            try
            {
                logObject.AppendLog("Copie fichier sauvegarde...");
                File.Copy(string.Format(@"{0}\game_template\assets\save.db", rootFolder), string.Format(@"{0}\game_template\temp_linux\save.db", rootFolder), true);
            }
            catch (Exception ex)
            {
                logObject.AppendLog("Erreur lors de la copie du fichier de sauvegarde " + ex.ToString(), LogType.ERROR);
            }

            try
            {
                logObject.AppendLog("Copie fichier locale...");
                File.Copy(string.Format(@"{0}\game_template\assets\locale.db", rootFolder), string.Format(@"{0}\game_template\temp_linux\locale.db", rootFolder), true);
            }
            catch (Exception ex)
            {
                logObject.AppendLog("Erreur lors de la copie de locale " + ex.ToString(), LogType.ERROR);
            }



            logObject.AppendLog("Exports assets et fichiers scripts...");

            ProcessStartInfo exportAssetsInfo = new ProcessStartInfo(settings.shell, settings.shell_op1 + string.Format(@"{0}\animation_converter\AnimationConverter.exe {0}\game_template\assets\animated_assets {0}\game_template\assets {0}\game_template\temp_linux\assets {0}\game_template\temp_linux\scripts {0}\game_template\assets\exclude_pc.txt", rootFolder) + settings.shell_op2);
            exportAssetsInfo.UseShellExecute = false;
            exportAssetsInfo.RedirectStandardOutput = true;
            exportAssetsInfo.CreateNoWindow = true;
            exportAssetsInfo.RedirectStandardError = true;

            Process exportAssetsProc = Process.Start(exportAssetsInfo);
            exportAssetsProc.OutputDataReceived += (osender, oe) => logObject.AppendLog(oe.Data, threaded: true);
            exportAssetsProc.BeginOutputReadLine();

            while (!exportAssetsProc.HasExited)
            {
                if (applicationEventDelegate != null)
                    applicationEventDelegate.DynamicInvoke();
            }



            int packer_result = 0;

            //-4.1- packing texture si nécessaire
            if (settings.packTextures)
            {
                logObject.AppendLog("Packing texture...");

                string[] exclude = File.ReadAllLines(string.Format(@"{0}\game_template\assets\exclude_packer.txt", rootFolder));

                List<string> list_exclude = new List<string>(exclude);

                packer_result = SpriteSheetPack.Pack(string.Format(@"{0}\game_template\temp_linux\assets\", rootFolder), true, (settings.packTextureHeight == settings.packTextureWidth), (int)settings.packTextureWidth, (int)settings.packTextureHeight, 2, string.Format(@"{0}\game_template\temp_linux\assets\{1}", rootFolder, "packtexture.png"), string.Format(@"{0}\game_template\temp_linux\assets\{1}", rootFolder, "packtexture.pack"), true, list_exclude);

                if (packer_result != 0)
                {
                    logObject.AppendLog("Erreur lors du packing de texture ", LogType.ERROR);
                }
            }

            //-5- zip assets
            List<string> assets_folder = new List<string>()
            {
                string.Format(@"{0}\game_template\temp_linux\assets", rootFolder),
                string.Format(@"{0}\game_template\temp_linux\scripts", rootFolder)
            };

            try
            {
                logObject.AppendLog("Compression assets...");
                ZipUtils.CreateArchive(string.Format(@"{0}\game_template\bin\Releasetemp\data.zip", rootFolder), assets_folder, new List<string>());

            }
            catch (Exception ex)
            {
                logObject.AppendLog("Erreur lors de la compression des assets " + ex.ToString(), LogType.ERROR);
            }



            try
            {
                logObject.AppendLog("Modification fichier config...");

                var text = File.ReadAllText(string.Format(@"{0}\game_template\assets\config_pc.lua", rootFolder));

                int has_asset = text.IndexOf("asset_path");

                if (has_asset != -1)
                {
                    int start_asset = text.IndexOf('"', has_asset);
                    int end_asset = text.IndexOf('"', start_asset + 1) + 1;

                    text = text.Remove(start_asset, end_asset - start_asset);

                    text = text.Insert(start_asset, "\"assets\"");
                }

                int has_scripts = text.IndexOf("script_path");

                if (has_scripts != -1)
                {
                    int start_script = text.IndexOf('"', has_scripts);
                    int end_script = text.IndexOf('"', start_script + 1) + 1;

                    text = text.Remove(start_script, end_script - start_script);

                    text = text.Insert(start_script, "\"scripts\"");
                }

                if (settings.packTextures && packer_result == 0)
                {
                    int first_pos = text.IndexOf('{') + 1;

                    text = text.Insert(first_pos, "\npacker = \"packtexture.pack\",");
                }

                File.WriteAllText(string.Format(@"{0}\game_template\temp_linux\config.lua", rootFolder), text);
            }
            catch (Exception ex)
            {
                logObject.AppendLog("Erreur lors de la modification du fichier de config " + ex.ToString(), LogType.ERROR);
            }
        
            //copie librairies de dépendances



            //-7- tar gz dossier jeu dans destination finale 



            try
            {
                logObject.AppendLog("Compression dossier jeu...");
                 ProcessStartInfo packinfo = new ProcessStartInfo(settings.shell, settings.shell_op1 + string.Format(@"tar -czvf {0}.tar.gz .",gamename) + settings.shell_op2);
                packinfo.WorkingDirectory = rootFolder + @"\game_template\temp_linux";
                packinfo.UseShellExecute = false;
                packinfo.RedirectStandardOutput = true;
                packinfo.CreateNoWindow = true;
                packinfo.RedirectStandardError = true;

                Process packinfoproc = Process.Start(packinfo);
                packinfoproc.OutputDataReceived += (osender, oe) => logObject.AppendLog(oe.Data, threaded: true);
                packinfoproc.BeginOutputReadLine();

                while (!packinfoproc.HasExited)
                {
                    if (applicationEventDelegate != null)
                        applicationEventDelegate.DynamicInvoke();
                }

            }
            catch (Exception ex)
            {
                logObject.AppendLog("Erreur lors de la compression du dossier jeu" + ex.ToString(), LogType.ERROR);
            }
           

            //suppression répertoire temporaires
            Directory.Delete(temp_directory, true);

            logObject.AppendLog("Export Linux terminé !");
            logObject.SetTitle("Export Linux terminé !");

            result.Result = Result.Success;
            return result;
        }
    }
}
