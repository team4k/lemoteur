﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameExporter
{
    public enum Result
    {
        Error,
        Success
    }

    public class ExportResult
    {
        public string Message { get; set; }

        public Result Result { get; set; }

        public ExportResult()
        {
            this.Result = Result.Error;
        }

    }
}
