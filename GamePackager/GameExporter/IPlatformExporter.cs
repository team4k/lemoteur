﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameExporter
{
    public interface IPlatformExporter
    {

        ExportResult Export(string rootFolder, string gamename, string outputPath, ExportSettings settings, ILogInterface logObject, Delegate applicationEventDelegate);

    }
}
