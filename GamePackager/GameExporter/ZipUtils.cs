﻿//adapted from https://github.com/icsharpcode/SharpZipLib/wiki/Zip-Samples

using System;
using System.IO;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;
using System.Collections.Generic;

namespace GameExporter
{
    public class ZipUtils
    {
        // Compresses the files in the nominated folder, and creates a zip file on disk named as outPathname.
        //
        public static void CreateArchive(string outPathname, List<string> folderNames,List<string> replacefoldername)
        {
            IEntryFactory factory = new ZipEntryFactory();
            

            FileStream fsOut = File.Create(outPathname);
            ZipOutputStream zipStream = new ZipOutputStream(fsOut);

            zipStream.SetLevel(3); //0-9, 9 being the highest level of compression
            //zipStream.Password = password;  // optional. Null is the same as not setting. Required if using AES.

            // This setting will strip the leading part of the folder path in the entries, to
            // make the entries relative to the starting folder.
            // To include the full path for each entry up to the drive root, assign folderOffset = 0.

            for (int ifold = 0;ifold <folderNames.Count;ifold++)
            {
                string folder = folderNames[ifold];
                string rep_name_folder = "";

                if(ifold < replacefoldername.Count)
                    rep_name_folder = replacefoldername[ifold];

                ZipUtils.CompressFolder(folder, zipStream, factory, replace_name_folder: rep_name_folder);
            }

            zipStream.IsStreamOwner = true; // Makes the Close also Close the underlying stream
            zipStream.Close();
        }


        // Recurses down the folder structure
        private static void CompressFolder(string path, ZipOutputStream zipStream,IEntryFactory factory,string parent_folder = "",string replace_name_folder = "")
        {
            string[] files = Directory.GetFiles(path);
            DirectoryInfo dirinfo = new DirectoryInfo(path);
            string folder_name = string.Format("{0}{1}",parent_folder ,(string.IsNullOrEmpty(replace_name_folder)) ? dirinfo.Name : replace_name_folder);
            zipStream.PutNextEntry(factory.MakeDirectoryEntry(folder_name));
            folder_name += "/";
            zipStream.CloseEntry();
      
            foreach (string filename in files)
            {

                FileInfo fi = new FileInfo(filename);

                string entryName = folder_name + fi.Name;
                entryName = ZipEntry.CleanName(entryName); // Removes drive from name and fixes slash direction
                ZipEntry newEntry = factory.MakeFileEntry(entryName);
                newEntry.DateTime = fi.LastWriteTime; // Note the zip format stores 2 second granularity

                // Specifying the AESKeySize triggers AES encryption. Allowable values are 0 (off), 128 or 256.
                // A password on the ZipOutputStream is required if using AES.
                //   newEntry.AESKeySize = 256;

                // To permit the zip to be unpacked by built-in extractor in WinXP and Server2003, WinZip 8, Java, and other older code,
                // you need to do one of the following: Specify UseZip64.Off, or set the Size.
                // If the file may be bigger than 4GB, or you do not need WinXP built-in compatibility, you do not need either,
                // but the zip will be in Zip64 format which not all utilities can understand.
                //   zipStream.UseZip64 = UseZip64.Off;
                newEntry.Size = fi.Length;

                zipStream.PutNextEntry(newEntry);

                // Zip the file in buffered chunks
                // the "using" will close the stream even if an exception occurs
                byte[] buffer = new byte[4096];
                using (FileStream streamReader = File.OpenRead(filename))
                {
                    StreamUtils.Copy(streamReader, zipStream, buffer);
                }
                zipStream.CloseEntry();
            }

            string[] folders = Directory.GetDirectories(path);
            foreach (string folder in folders)
            {
                CompressFolder(folder, zipStream, factory, folder_name);
            }
        }
    }
}


