﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameExporter
{
    public enum LogType
    {
        INFO,
        WARNING,
        ERROR
    }

    public interface ILogInterface
    {
        void ShowForm();
        void AppendLog(string message,LogType type = LogType.INFO,bool threaded = false);
        void SetTitle(string Title);
    }
}
