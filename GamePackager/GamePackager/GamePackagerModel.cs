﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Eto.Forms;

namespace GamePackager
{
    public class GamePackagerModel : INotifyPropertyChanged
    {
        private string _projectPath;

        public string ProjectPath
        {
            get
            {
                return _projectPath;
            }
            set
            {
                _projectPath = value;
                OnPropertyChanged();
            }
        }

        private string _outputPath;

        public string OutputPath
        {
            get
            {
                return _outputPath;
            }

            set
            {
                _outputPath = value;
                OnPropertyChanged();
            }
        }

        public GamePackagerModel()
        {

        }

        void OnPropertyChanged([CallerMemberName] string memberName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(memberName));
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
