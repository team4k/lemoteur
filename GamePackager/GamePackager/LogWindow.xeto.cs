﻿using System;
using System.Collections.Generic;
using Eto.Forms;
using Eto.Drawing;
using Eto.Serialization.Xaml;
using GameExporter;
using System.Threading;

namespace GamePackager
{
    public class LogWindow : Form,ILogInterface
    {
        private LogModel _logModel = new LogModel();

        public LogWindow()
        {
            XamlReader.Load(this);
            DataContext = _logModel;
        }

        public void AppendLog(string message, LogType type = LogType.INFO, bool threaded = false)
        {

            if (threaded)
            {
                Application.Instance.AsyncInvoke(new Action(() =>
                {
                    _logModel.Content += message + Environment.NewLine;
                    _logModel.Position = _logModel.Content.Length - 1;
                }));
            }
            else
            {
                Application.Instance.Invoke(() => {
                    _logModel.Content += message + Environment.NewLine;
                    _logModel.Position = _logModel.Content.Length - 1;
                });
            }
        }

        public void ShowForm()
        {
            Application.Instance.Invoke(() => this.Show());
        }

        public void SetTitle(string Title)
        {
            Application.Instance.Invoke(() => this.Title = Title);
        }
    }
}
