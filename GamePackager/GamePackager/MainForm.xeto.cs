﻿using System;
using System.Collections.Generic;
using Eto.Forms;
using Eto.Drawing;
using Eto.Serialization.Xaml;
using GameExporter;
using System.Threading.Tasks;

namespace GamePackager
{
    public class MainForm : Form
    {

        private GamePackagerModel _projectModel = new GamePackagerModel();

        delegate void del();


        public MainForm()
        {
            XamlReader.Load(this);
            DataContext = _projectModel;
        }

        protected void HandleShowPathBrowser(object sender,EventArgs e)
        {
            SelectFolderDialog selectPath = new SelectFolderDialog();
            
            if(selectPath.ShowDialog(this) == DialogResult.Ok)
            {
                _projectModel.ProjectPath = selectPath.Directory;
            }
        }

        protected void HandleShowPathBrowser2(object sender, EventArgs e)
        {
            SelectFolderDialog selectPath = new SelectFolderDialog();

            if (selectPath.ShowDialog(this) == DialogResult.Ok)
            {
                _projectModel.OutputPath = selectPath.Directory;
            }
        }

        protected void HandleExport(object sender,EventArgs e)
        {
            IPlatformExporter exporter = new EmscriptenExporter();

            ExportSettings setting = new ExportSettings();
            setting.shell = "cmd.exe";
            setting.shell_op1 = "/c ";
            setting.doSkipBuild = false;
            setting.packTextures = true;
            setting.packTextureWidth = 2048;
            setting.packTextureHeight = 2048;

            LogWindow logger = new LogWindow();
            logger.Show();

           Task.Run(() =>
            {
                try
                {
                    ExportResult result = exporter.Export(_projectModel.ProjectPath, "test", _projectModel.OutputPath, setting, logger, null);


                    if (result.Result != Result.Success)
                    {
                        logger.AppendLog(result.Message);
                    }
                }
                catch(Exception ex)
                {
                    logger.AppendLog(ex.ToString());
                }
                

            });

        }

        protected void HandleClickMe(object sender, EventArgs e)
        {
            MessageBox.Show("I was clicked!");
        }

        protected void HandleQuit(object sender, EventArgs e)
        {
            Application.Instance.Quit();
        }
    }
}
