﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WafaBuilder
{
    public class DependBuildOptions
    {
        public bool DeleteExistingOutput { get; set; }
        private Dictionary<string, bool> _enginedependencies;

        public DependBuildOptions()
        {
            this.DeleteExistingOutput = true;
            _enginedependencies = new Dictionary<string, bool>();
        }

        public void AddDependency(string name)
        {
            this._enginedependencies.Add(name, true);
        }

        public void RemoveDependency(string name)
        {
            this._enginedependencies.Remove(name);
        }

        public void SetDependencyBuildState(string name, bool dobuild)
        {
            this._enginedependencies[name] = dobuild;
        }

        public bool CanBuildDependency(string name)
        {
            return this._enginedependencies[name];
        }

        public Dictionary<string, bool> GetDependencyDict()
        {
            return this._enginedependencies;
        }


    }
}
