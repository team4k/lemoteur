﻿namespace WafaBuilder
{
    partial class DependenciesOptions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpDepend = new System.Windows.Forms.GroupBox();
            this.panelDepend = new System.Windows.Forms.Panel();
            this.chkDeleteOutput = new System.Windows.Forms.CheckBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.chkCheckall = new System.Windows.Forms.CheckBox();
            this.grpDepend.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpDepend
            // 
            this.grpDepend.Controls.Add(this.panelDepend);
            this.grpDepend.Location = new System.Drawing.Point(18, 47);
            this.grpDepend.Margin = new System.Windows.Forms.Padding(4);
            this.grpDepend.Name = "grpDepend";
            this.grpDepend.Padding = new System.Windows.Forms.Padding(4);
            this.grpDepend.Size = new System.Drawing.Size(463, 360);
            this.grpDepend.TabIndex = 0;
            this.grpDepend.TabStop = false;
            this.grpDepend.Text = "Dependencies";
            // 
            // panelDepend
            // 
            this.panelDepend.AutoScroll = true;
            this.panelDepend.Location = new System.Drawing.Point(9, 26);
            this.panelDepend.Margin = new System.Windows.Forms.Padding(4);
            this.panelDepend.Name = "panelDepend";
            this.panelDepend.Size = new System.Drawing.Size(454, 319);
            this.panelDepend.TabIndex = 0;
            // 
            // chkDeleteOutput
            // 
            this.chkDeleteOutput.AutoSize = true;
            this.chkDeleteOutput.Location = new System.Drawing.Point(18, 415);
            this.chkDeleteOutput.Margin = new System.Windows.Forms.Padding(4);
            this.chkDeleteOutput.Name = "chkDeleteOutput";
            this.chkDeleteOutput.Size = new System.Drawing.Size(195, 21);
            this.chkDeleteOutput.TabIndex = 1;
            this.chkDeleteOutput.Text = "Delete existing output files";
            this.chkDeleteOutput.UseVisualStyleBackColor = true;
            this.chkDeleteOutput.CheckedChanged += new System.EventHandler(this.chkDeleteOutput_CheckedChanged);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(170, 521);
            this.btnOk.Margin = new System.Windows.Forms.Padding(4);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(112, 31);
            this.btnOk.TabIndex = 2;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // chkCheckall
            // 
            this.chkCheckall.AutoSize = true;
            this.chkCheckall.Checked = true;
            this.chkCheckall.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkCheckall.Location = new System.Drawing.Point(27, 16);
            this.chkCheckall.Margin = new System.Windows.Forms.Padding(4);
            this.chkCheckall.Name = "chkCheckall";
            this.chkCheckall.Size = new System.Drawing.Size(18, 17);
            this.chkCheckall.TabIndex = 3;
            this.chkCheckall.UseVisualStyleBackColor = true;
            this.chkCheckall.CheckedChanged += new System.EventHandler(this.chkCheckall_CheckedChanged);
            // 
            // DependenciesOptions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(499, 580);
            this.Controls.Add(this.chkCheckall);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.chkDeleteOutput);
            this.Controls.Add(this.grpDepend);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "DependenciesOptions";
            this.Text = "DependenciesOptions";
            this.grpDepend.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grpDepend;
        private System.Windows.Forms.CheckBox chkDeleteOutput;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Panel panelDepend;
        private System.Windows.Forms.CheckBox chkCheckall;
    }
}