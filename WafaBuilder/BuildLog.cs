﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WafaBuilder
{
    public partial class BuildLog : Form
    {
        private Font defaultFont = new Font("Courier New", 10);
        public static readonly Font TitleFont = new Font("Courier New", 16);

        public BuildLog()
        {
            InitializeComponent();
        }

        public void log(string logmessage,bool threaded = false,Color? color = null,Font font = null)
        {
            if (color == null)
                color = Color.Black;

            if (font == null)
                font = defaultFont;

            if (threaded)
            {
                this.BeginInvoke(new MethodInvoker(() =>
                {
                    this.AppendTextWithColor(logmessage + "\n", color.Value,font);
                }));
            }
            else
            {
                this.AppendTextWithColor(logmessage + "\n", color.Value,font);
            }
            
        }

        private void AppendTextWithColor(string text, Color color,Font font)
        {
            rtbLog.SelectionFont = font;
            rtbLog.SelectionStart = rtbLog.TextLength;
            rtbLog.SelectionLength = 0;

            rtbLog.SelectionColor = color;
            rtbLog.AppendText(text);
            rtbLog.SelectionColor = rtbLog.ForeColor;
            rtbLog.SelectionFont = defaultFont;
        }

        public void logfixed(string message)
        {
            int firstchar = rtbLog.GetFirstCharIndexOfCurrentLine();
            int len = rtbLog.TextLength;
            rtbLog.Select(firstchar, len - firstchar);
            rtbLog.SelectedText = message;
            rtbLog.DeselectAll();
            //rtbLog.AppendText(message);
        }
    }
}
