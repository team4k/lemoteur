using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Microsoft.Win32;
using System.Diagnostics;
using System.Windows.Forms;
using System.Drawing;

namespace WafaBuilder
{

    public enum BuildSystem
    {
        msbuild,
        make,
        emmake,
        ndk_build,
        xcodebuild,
        xcodebuildios
    }

    public class CmbItem
    {
        public string value1 {get;set;}
        public int value2 { get; set; }
    }

    public class CmbItemString
    {
        public string value1 { get; set; }
        public string value2 { get; set; }

        public CmbItemString(string pvalue1, string pvalue2)
        {
            value1 = pvalue1;
            value2 = pvalue2;
        }
    }

    public class Toolchain : ICloneable
    {
        private Dictionary<int,int> vsyear = new Dictionary<int,int>()
        {
            {10,2010},
            {11,2012},
            {12,2013},
            {14,2015},
            {15, 2017},
            {16, 2019}
        };

        public static readonly Dictionary<int, string> vsplat = new Dictionary<int, string>()
        {
            {10,"v100"},
            {11,"v110"},
            {12,"v120"},
            {14,"v140"},
            {15, "v150"},
            {16, "v160"}
        };

        public string Name { get; set; }
        public List<CheckToolchain> cmdChecks { get; set; }
        public List<PostBuildStep> cmdPostbuilds { get; set; }
        public bool includeDebugBuild { get; set; }
        public BuildSystem buildsystem;
        private string platformstring;
        private string cmakeplatformstring;
        private string dynlibext;
        private string staticlibext;
        public bool DebugBuild { get; set; }
        public bool LocateToolChain { get; set; }
        public string ChainRoot { get; set; }
        public bool CustomBuildShell { get; set; }
        public bool HasEditor { get; set; }
        private string _Platform;

        public bool useX64 { get; set; }

        public Toolchain(string pName, CheckToolchain[] pCheck, bool pIncludeDebug,BuildSystem pbuild,string pdynlibext,string pstaticlibext,bool locatetoolchain = false,bool custombuildshell=false,bool has_editor = false,PostBuildStep[] pPostBuild = null)
        {
            cmdChecks = new List<CheckToolchain>(pCheck);
            Name = pName;
            includeDebugBuild = pIncludeDebug;
            buildsystem = pbuild;
            dynlibext = pdynlibext;
            staticlibext = pstaticlibext;
            this.DebugBuild = false;
            this.LocateToolChain = locatetoolchain;
            this.CustomBuildShell = custombuildshell;
            this.HasEditor = has_editor;
            this.useX64 = false;
            cmdPostbuilds = new List<PostBuildStep>();

            if (pPostBuild != null)
                cmdPostbuilds.AddRange(pPostBuild);

        }

        public void SetPlatform(string pplatformnumber)
        {
            this._Platform = pplatformnumber;

            switch(this.buildsystem)
            {

                case BuildSystem.msbuild:
                    {

                        int platformnumber = Convert.ToInt32(pplatformnumber);

                        this.platformstring = string.Format("/p:PlatformToolset=v{0}0 /p:VisualStudioVersion={0}.0", platformnumber);

                        string arch = "";

                        if(vsyear[platformnumber] >= 2019)
                        {
                            arch = "-A \"Win32\"";
                        }

                        this.cmakeplatformstring = string.Format("-G \"Visual Studio {0} {1}\" {2}", platformnumber, vsyear[platformnumber],arch);

                    }
                    break;

                case BuildSystem.emmake:
                {
                    OperatingSystem os = Environment.OSVersion;

                    PlatformID pid = os.Platform;

                    this.cmakeplatformstring = string.Format("-DCMAKE_TOOLCHAIN_FILE={0}/cmake/Modules/Platform/Emscripten.cmake", this.ChainRoot);

                    if (pid == PlatformID.Win32NT)
                        this.cmakeplatformstring += " -G \"MinGW Makefiles\"";
                    else
                        this.cmakeplatformstring += " -G \"Unix Makefiles\"";
                }


                break;

                case BuildSystem.ndk_build:
                {

                    OperatingSystem os = Environment.OSVersion;

                    PlatformID pid = os.Platform;

                    DirectoryInfo dirinf = new DirectoryInfo("android-cmake");

                    this.cmakeplatformstring = string.Format("-DCMAKE_TOOLCHAIN_FILE={0}/android.toolchain.cmake -DANDROID_NDK={1} -DCMAKE_BUILD_TYPE={2} -DANDROID_ABI=armeabi-v7a -DANDROID_NATIVE_API_LEVEL=android-9 -DANDROID_TOOLCHAIN_NAME=arm-linux-androideabi-4.6", dirinf.FullName, this.ChainRoot, (this.DebugBuild) ? "Debug" : "Release");

                    if (pid == PlatformID.Win32NT)
                    {
                        this.cmakeplatformstring += " -G \"MinGW Makefiles\"";
                        this.cmakeplatformstring += string.Format(@" -DCMAKE_MAKE_PROGRAM={0}\prebuilt\windows-x86_64\bin\make.exe", this.ChainRoot);
                    }
                    else
                        this.cmakeplatformstring += " -G \"Unix Makefiles\"";
                }


                break;

                case BuildSystem.xcodebuildios:
                {
                    DirectoryInfo dirinf = new DirectoryInfo("ios-cmake");

                    this.cmakeplatformstring = string.Format("-DCMAKE_TOOLCHAIN_FILE={0}/IOS.cmake -DIOS_PLATFORM={1}", dirinf.FullName, pplatformnumber);

                }
                break;

            }
        }

        public string GetLibName(bool islib,string libname,bool withprefix= false)
        {
            string prefix = "";

            if(this.buildsystem != BuildSystem.msbuild)
                prefix = "lib";

            return ((withprefix)? prefix : "") +  libname + "." + ((islib) ? staticlibext : dynlibext);
        }

        public string Getoutdir()
        {
            string suffix = string.Empty;

            if (this.DebugBuild)
                suffix += "-debug";

            switch (this.buildsystem)
            {
                case BuildSystem.msbuild:
                        return "msvc"+suffix;
                case BuildSystem.emmake:
                    return "emscripten"+suffix;
                case BuildSystem.make:
                    return "linux"+suffix;
                case BuildSystem.ndk_build:
                    return "android"+suffix;
                case BuildSystem.xcodebuild:
                    return "macosx"+suffix;
                case BuildSystem.xcodebuildios:
                    return "ios" + suffix;
            }

            return string.Empty;
        }


        public string GetCmakestring()
        {
            return this.cmakeplatformstring;
        }

        public string GetCustomBuildShell()
        {
            switch (this.buildsystem)
            {
                case BuildSystem.emmake:
                    OperatingSystem os = Environment.OSVersion;

                    PlatformID pid = os.Platform;

                    if (pid == PlatformID.Win32NT)
                    {
                        return "mingw32-make";
                    }
                    break;
            }

            return string.Empty;
        }

        public Dictionary<string, string> GetConfigArray(string conf)
        {
            Dictionary<string, string> confArray = new Dictionary<string, string>();

            string[] config_ele_arr = conf.Split(new char[] { ';' });

            foreach (string conf_ele in config_ele_arr)
            {
                string[] ele_brk = conf_ele.Split(new char[] { '=' });

                confArray.Add(ele_brk[0], ele_brk[1]);
            }

            return confArray;

        }

        public static void BuildCsharpProj(string projpath,BuildLog logform)
        {
			//on windows use msbuild, on other platform use xbuild
            //build the generated project file
            string builder = "msbuild";

            string shell = "cmd.exe";
            string shell_op = @"/c ";
            string shell_op2 = "";

             OperatingSystem os = Environment.OSVersion;

            PlatformID pid = os.Platform;

            if (pid == PlatformID.MacOSX || pid == PlatformID.Unix)
            {
                builder = "xbuild";
                shell = "/bin/bash";
                shell_op = "-c '";
                shell_op2 = "'";
            }

            //keep only the directory
            string projdir = Path.GetDirectoryName(projpath);

            //run nuget restore to download dependencies
            ProcessStartInfo nuget_info = new ProcessStartInfo(shell, shell_op+ "nuget restore "+ projdir + "  "+ shell_op2);
            nuget_info.UseShellExecute = false;
            nuget_info.RedirectStandardOutput = true;
            nuget_info.WorkingDirectory = Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf(Path.DirectorySeparatorChar));
            nuget_info.CreateNoWindow = true;
            nuget_info.RedirectStandardError = true;
            Process nuget_buildproj = Process.Start(nuget_info);
            nuget_buildproj.OutputDataReceived += (snd, earg) => logform.log(earg.Data, true);
            nuget_buildproj.ErrorDataReceived += (snd2, earg2) => logform.log(earg2.Data, true, Color.Red);
            nuget_buildproj.BeginOutputReadLine();
            nuget_buildproj.BeginErrorReadLine();
			
			while (!nuget_buildproj.HasExited)
                Application.DoEvents();
			
			
            


            ProcessStartInfo csharpbuild_info = new ProcessStartInfo(shell, shell_op+ builder + " "+projpath+" /t:Rebuild /p:Configuration=Release "  + shell_op2);
            csharpbuild_info.UseShellExecute = false;
            csharpbuild_info.RedirectStandardOutput = true;
            csharpbuild_info.WorkingDirectory = Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf(Path.DirectorySeparatorChar));
            csharpbuild_info.CreateNoWindow = true;
            csharpbuild_info.RedirectStandardError = true;
            Process csharp_buildproj = Process.Start(csharpbuild_info);
            csharp_buildproj.OutputDataReceived += (snd, earg) => logform.log(earg.Data, true);
            csharp_buildproj.ErrorDataReceived += (snd2, earg2) => logform.log(earg2.Data, true, Color.Red);
            csharp_buildproj.BeginOutputReadLine();
            csharp_buildproj.BeginErrorReadLine();

            while (!csharp_buildproj.HasExited)
                Application.DoEvents();
        }

        public static void FixConfigure(string filepath)
        {
          string config_data = File.ReadAllText(filepath);
          //add back arobase
          int indx = 0;

          while(indx != -1)
          {
              indx = config_data.IndexOf("#undef ",indx);
    
            if(indx != -1)
            {
               int linepos = config_data.IndexOf(System.Environment.NewLine,indx);
               string var_name = " @"+config_data.Substring(indx+7,linepos - (indx+7))+"@";
       
               config_data = config_data.Insert(linepos,var_name);
               indx = linepos + var_name.Length;
    
            }
          }
  
          File.WriteAllText(filepath,config_data);

        }       
 
        public void BuildProj(string buildoptions, string projpath, string buildpath, string selected_generator,BuildLog logform,string outtype="dynlib",string options = "")
        {
            string shell = "cmd.exe";
            string shell_op = @"/c ";
            string shell_op2 = "";

            OperatingSystem os = Environment.OSVersion;

            PlatformID pid = os.Platform;

            if (pid == PlatformID.MacOSX || pid == PlatformID.Unix)
            {
                shell = "/bin/bash";
                shell_op = "-c '";
                shell_op2 = "'";
            }


            if ((this.buildsystem == BuildSystem.make || this.buildsystem == BuildSystem.xcodebuild)  && selected_generator == "make" && (pid == PlatformID.MacOSX || pid == PlatformID.Unix))
            {
                string configure_options = "";
                
                if(!string.IsNullOrEmpty(options))
                    configure_options = " " + options;

                //call configure before make
                ProcessStartInfo configureinfo = new ProcessStartInfo(shell, shell_op + "sh configure" + configure_options +  shell_op2);
                configureinfo.UseShellExecute = false;
                configureinfo.RedirectStandardOutput = true;
                configureinfo.WorkingDirectory = buildpath;
                configureinfo.CreateNoWindow = true;
                configureinfo.RedirectStandardError = true;
                Process configure_proc = Process.Start(configureinfo);
                configure_proc.OutputDataReceived += (snd, earg) => logform.log(earg.Data, true);
                configure_proc.ErrorDataReceived += (snd2, earg2) => logform.log(earg2.Data, true, Color.Red);
                configure_proc.BeginOutputReadLine();
                configure_proc.BeginErrorReadLine();

                while (!configure_proc.HasExited)
                    Application.DoEvents();


            }

            ProcessStartInfo startinfo = new ProcessStartInfo(shell, shell_op + this.GenerateBuildCmd(buildoptions, projpath, buildpath, selected_generator,outtype) + shell_op2);
            startinfo.UseShellExecute = false;
            startinfo.RedirectStandardOutput = true;
            startinfo.WorkingDirectory = Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf(Path.DirectorySeparatorChar));
            startinfo.CreateNoWindow = true;
            startinfo.RedirectStandardError = true;
            Process build_proc = Process.Start(startinfo);
            build_proc.OutputDataReceived += (snd, earg) => logform.log(earg.Data, true);
            build_proc.ErrorDataReceived += (snd2, earg2) => logform.log(earg2.Data, true,Color.Red);
            build_proc.BeginOutputReadLine();
            build_proc.BeginErrorReadLine();

            while (!build_proc.HasExited)
                Application.DoEvents();
        }

        public void ExecScript(string script,string buildpath,BuildLog logform,string params_exec="")
        {
            string shell = "cmd.exe";
            string shell_op = @"/c ";
            string shell_op2 = "";
            string script_ex = "";


            OperatingSystem os = Environment.OSVersion;

            PlatformID pid = os.Platform;

            if (pid == PlatformID.MacOSX || pid == PlatformID.Unix)
            {
                shell = "/bin/bash";
                shell_op = "-c '";
                shell_op2 = "'";
                script_ex = "sh";
            }

            ProcessStartInfo configureinfo = new ProcessStartInfo(shell, shell_op + script_ex +" "+ script + " "+params_exec+ shell_op2);
            configureinfo.UseShellExecute = false;
            configureinfo.RedirectStandardOutput = true;
            configureinfo.WorkingDirectory = buildpath;
            configureinfo.CreateNoWindow = true;
            configureinfo.RedirectStandardError = true;
            Process configure_proc = Process.Start(configureinfo);
            configure_proc.OutputDataReceived += (snd, earg) => logform.log(earg.Data, true);
            configure_proc.ErrorDataReceived += (snd2, earg2) => logform.log(earg2.Data, true, Color.Red);
            configure_proc.BeginOutputReadLine();
            configure_proc.BeginErrorReadLine();

            while (!configure_proc.HasExited)
                Application.DoEvents();
        }


        public string GenerateBuildCmd(string buildoptions, string projpath, string buildpath, string selectect_generator = "", string outtype = "dynlib")
        {
            string buildcmd = string.Empty;

            switch (this.buildsystem)
            {
                case BuildSystem.msbuild:

                    if (!projpath.EndsWith(".sln") && !projpath.EndsWith(".vcxproj"))
                    {
                        //find the first sln in the dependency path
                        string[] files = Directory.GetFiles(buildpath);

                        foreach(string file in files)
                        {
                            if (file.EndsWith(".sln"))
                            {
                                projpath = file;
                                break;
                            }
                        }


                    }

                    buildcmd = "msbuild " + projpath + " " + this.platformstring;

                    if (!string.IsNullOrEmpty(buildoptions))
                    {
                        Dictionary<string, string> confarray = this.GetConfigArray(buildoptions);

                        if (confarray.ContainsKey("target"))
                            buildcmd += " /t:" + confarray["target"];

                        if (confarray.ContainsKey("config"))
                            buildcmd += " /p:Configuration=" + confarray["config"];
                        else
                            buildcmd += (this.DebugBuild) ? " /p:Configuration=Debug" : " /p:Configuration=Release";

                        if (confarray.ContainsKey("platform"))
                            buildcmd += " /p:Platform=" + confarray["platform"];
                    }


                    break;

                case BuildSystem.ndk_build:
                    if (selectect_generator == "ndkbuild")
                        buildcmd = "ndk-build -C " + projpath;
                    else
                    {
                        OperatingSystem os = Environment.OSVersion;

                        PlatformID pid = os.Platform;

                        if (pid == PlatformID.Win32NT)
                        {
                            buildcmd = "cmake --build " + projpath;
                        }
                        else
                        {
                            buildcmd = "make -j8 -C " + projpath;
                        }

                        if (!string.IsNullOrEmpty(buildoptions))
                        {
                            Dictionary<string, string> confarray = this.GetConfigArray(buildoptions);

                            if (confarray.ContainsKey("target"))
                                buildcmd += ((pid == PlatformID.Win32NT) ?" --target " : " ") + confarray["target"];

                        }
                    }

                 break;
                case BuildSystem.emmake:
                 {
                     OperatingSystem os = Environment.OSVersion;

                     PlatformID pid = os.Platform;

                     if (pid == PlatformID.Win32NT)
                     {
                         buildcmd = "-C " + projpath;
                     }
                     else
                     {
                         buildcmd = "make -C " + projpath;
                     }

                     if (!string.IsNullOrEmpty(buildoptions))
                     {
                         Dictionary<string, string> confarray = this.GetConfigArray(buildoptions);

                         if (confarray.ContainsKey("target"))
                             buildcmd += " " + confarray["target"];

                     }
                 }

                 break;
                case BuildSystem.make:

                    buildcmd = "make -C " + projpath;

                    //if (this.useX64 && outtype == "staticlib")
                      //  buildcmd += " CFLAGS='-g -O2 -fPIC'";


                    if (!string.IsNullOrEmpty(buildoptions))
                    {
                        Dictionary<string, string> confarray = this.GetConfigArray(buildoptions);

                        if (confarray.ContainsKey("target"))
                            buildcmd += " " + confarray["target"];

                    }

                    break;
                case BuildSystem.xcodebuild:
                    buildcmd = "make -C " + projpath;

                    if (!string.IsNullOrEmpty(buildoptions))
                    {
                        Dictionary<string, string> confarray = this.GetConfigArray(buildoptions);

                        if (confarray.ContainsKey("target"))
                            buildcmd += " " + confarray["target"];

                    }
                    break;
                
                case BuildSystem.xcodebuildios:
                    buildcmd = "make -C " + projpath;
                    
                    switch(this._Platform)
                    {
                        case "OS":
                            buildcmd += " IOS=1";
                        break;
                    }

                    if (!string.IsNullOrEmpty(buildoptions))
                    {
                        Dictionary<string, string> confarray = this.GetConfigArray(buildoptions);

                        if (confarray.ContainsKey("target"))
                            buildcmd += " " + confarray["target"];

                    }
                    break;
            }

            return buildcmd;

        }

        public static List<CmbItem> GetMsbuildPlatforms()
        {
            RegistryKey key = Registry.CurrentUser.OpenSubKey("Software\\Microsoft\\VisualStudio");

            if (key == null)
                return new List<CmbItem>();

            string[] subkeynames = key.GetSubKeyNames();

            List<CmbItem> msbuildplat = new List<CmbItem>();

            foreach (string subkey in subkeynames)
            {
                if (subkey.EndsWith("_Config"))
                {
                    int conf_pos = subkey.IndexOf("_Config");
                    string vsver = subkey.Substring(0, conf_pos);

                    string vsver2 = vsver.Substring(0, vsver.IndexOf('.'));


                    int iver = Convert.ToInt32(vsver2);

                    CmbItem newitem = new CmbItem();
                    newitem.value1 = vsplat[iver];
                    newitem.value2 = iver;

                    msbuildplat.Add(newitem);
                }
            }


            return msbuildplat;
        }


        public object Clone()
        {
            Toolchain cloneobj = (Toolchain)this.MemberwiseClone();
            cloneobj.cmdChecks = new List<CheckToolchain>(cloneobj.cmdChecks);

            return cloneobj;
        }
    }

    public class CheckToolchain
    {
        public string cmdCheck;
        public bool msreg;

        public PlatformID? restrict_os = null;

        public CheckToolchain(string cmd, bool pmsreg = false,PlatformID? prestrict_os = null)
        {
            cmdCheck = cmd;
            msreg = pmsreg;
            restrict_os = prestrict_os;
        }
    }

    public class PostBuildStep
    {
        private string _baseDirectory;

        public string baseDirectory
        {
            get
            {
                return _baseDirectory;
            }
        }

        private string _cmdPostBuild;
        public string cmdPostBuild
        {
            get
            {
                return _cmdPostBuild;
            }
        }

        private string _args;

        public string args
        {
            get
            {
                return _args;
            }
        }

        public PostBuildStep(string pcmdPostBuild,string baseDirectory,string pargs)
        {
            _cmdPostBuild = pcmdPostBuild;
            _baseDirectory = baseDirectory;
            _args = pargs;
        }

        public string SeekFile(string dir)
        {
            foreach (string file in Directory.GetFiles(dir))
            {
                string filename = Path.GetFileNameWithoutExtension(file);

                if (filename == _cmdPostBuild)
                    return dir + Path.PathSeparator + _cmdPostBuild + " " + args;
            }

            foreach (string subdir in Directory.GetDirectories(dir))
                return SeekFile(subdir);

            return string.Empty;
        }

        public string getCmd(string ChainRootDir,string generatedFile)
        {
            string baseDir = string.Format("{0}{1}{2}", ChainRootDir,Path.DirectorySeparatorChar,this.baseDirectory);

            string result = SeekFile(baseDir);

            if(!string.IsNullOrEmpty(result))
                return result + " " + generatedFile;

            return string.Empty;
        }
    }
}
