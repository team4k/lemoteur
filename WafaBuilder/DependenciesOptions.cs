﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WafaBuilder
{
    public partial class DependenciesOptions : Form
    {
        private DependBuildOptions _ref_options;

        public DependenciesOptions(DependBuildOptions options)
        {
            InitializeComponent();
            _ref_options = options;

            chkDeleteOutput.Checked = options.DeleteExistingOutput;

            CheckBox tmp_check = null;

            foreach (KeyValuePair<string, bool> depend_info in options.GetDependencyDict())
            {
                tmp_check = this.CreateDependCheck(depend_info, tmp_check);
            }

        }

        private CheckBox CreateDependCheck(KeyValuePair<string,bool> depend_info,CheckBox LastCheck)
        {
            CheckBox toolcheck = new CheckBox();

            if (LastCheck != null)
                toolcheck.Location = new Point(LastCheck.Location.X, LastCheck.Location.Y + 32);
            else
                toolcheck.Location = new Point(16, 16);

            toolcheck.Text = depend_info.Key;
            toolcheck.Size = new Size(200, 32);
            toolcheck.Checked = depend_info.Value;
            toolcheck.Tag = depend_info.Key;
            toolcheck.CheckedChanged += new EventHandler(toolcheck_CheckedChanged);
            panelDepend.Controls.Add(toolcheck);

            return toolcheck;
        }

        void toolcheck_CheckedChanged(object sender, EventArgs e)
        {
            string name = ((CheckBox)sender).Tag as string;

            _ref_options.SetDependencyBuildState(name, ((CheckBox)sender).Checked);

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void chkDeleteOutput_CheckedChanged(object sender, EventArgs e)
        {
            _ref_options.DeleteExistingOutput = chkDeleteOutput.Checked;
        }

        private void chkCheckall_CheckedChanged(object sender, EventArgs e)
        {
            foreach (Control cont in panelDepend.Controls)
            {
                if (cont is CheckBox)
                {
                    ((CheckBox)cont).Checked = chkCheckall.Checked;
                }
            }
        }


    }
}
