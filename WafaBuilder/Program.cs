﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace WafaBuilder
{
    static class Program
    {

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            
            if (Environment.OSVersion.Platform == PlatformID.Win32NT &&  Environment.OSVersion.Version.Major >= 6) {
               DynamicDllFunctionInvoke("user32.dll", "SetProcessDPIAware");
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new BuilderForm());
        }

        //from https://www.codeproject.com/Articles/9214/Dynamic-Invoke-from-Unmanaged-DLL
        public static object DynamicDllFunctionInvoke(string DllPath, string EntryPoint)
        {
            // Version string definition
            //Define return type of your dll function.
            Type returnType = typeof(bool);
            //out or in parameters of your function.   
            Type[] parameterTypes = new Type[0];
            object[] parameterValues = new object[0];
            string entryPoint = EntryPoint;

            // Create a dynamic assembly and a dynamic module
            AssemblyName asmName = new AssemblyName();
            asmName.Name = "tempDll";
            AssemblyBuilder dynamicAsm =
              AppDomain.CurrentDomain.DefineDynamicAssembly(asmName,
              AssemblyBuilderAccess.Run);
            ModuleBuilder dynamicMod =
              dynamicAsm.DefineDynamicModule("tempModule");

            // Dynamically construct a global PInvoke signature 
            // using the input information
            MethodBuilder dynamicMethod = dynamicMod.DefinePInvokeMethod(
              entryPoint, DllPath, MethodAttributes.Static | MethodAttributes.Public
              | MethodAttributes.PinvokeImpl, CallingConventions.Standard,
              returnType, parameterTypes, CallingConvention.Cdecl,
              CharSet.Ansi);

            // This global method is now complete
            dynamicMod.CreateGlobalFunctions();

            // Get a MethodInfo for the PInvoke method
            MethodInfo mi = dynamicMod.GetMethod(EntryPoint);
            // Invoke the static method and return whatever it returns
            object retval = mi.Invoke(null, parameterValues);
            // Filled verstr paramter.
            //MessageBox.Show(System.Text.ASCIIEncoding.ASCII.GetString(verstr));
            return retval;
        }
    }
}
