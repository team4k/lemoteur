﻿namespace WafaBuilder
{
    partial class BuilderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BuilderForm));
            this.btnBuild = new System.Windows.Forms.Button();
            this.grpToolchain = new System.Windows.Forms.GroupBox();
            this.chkBuilddepend = new System.Windows.Forms.CheckBox();
            this.chkDevPack = new System.Windows.Forms.CheckBox();
            this.btnDependencies = new System.Windows.Forms.Button();
            this.chkDependOnly = new System.Windows.Forms.CheckBox();
            this.chkKeepBuild = new System.Windows.Forms.CheckBox();
            this.chkCurlSupport = new System.Windows.Forms.CheckBox();
            this.fbCopyto = new System.Windows.Forms.FolderBrowserDialog();
            this.txtFolderCopyTo = new System.Windows.Forms.TextBox();
            this.lblcopyto = new System.Windows.Forms.Label();
            this.btnCopyto = new System.Windows.Forms.Button();
            this.chkcopyonly = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // btnBuild
            // 
            resources.ApplyResources(this.btnBuild, "btnBuild");
            this.btnBuild.Name = "btnBuild";
            this.btnBuild.UseVisualStyleBackColor = true;
            this.btnBuild.Click += new System.EventHandler(this.btnBuild_Click);
            // 
            // grpToolchain
            // 
            resources.ApplyResources(this.grpToolchain, "grpToolchain");
            this.grpToolchain.Name = "grpToolchain";
            this.grpToolchain.TabStop = false;
            // 
            // chkBuilddepend
            // 
            resources.ApplyResources(this.chkBuilddepend, "chkBuilddepend");
            this.chkBuilddepend.Name = "chkBuilddepend";
            this.chkBuilddepend.UseVisualStyleBackColor = true;
            this.chkBuilddepend.CheckedChanged += new System.EventHandler(this.chkBuilddepend_CheckedChanged);
            // 
            // chkDevPack
            // 
            resources.ApplyResources(this.chkDevPack, "chkDevPack");
            this.chkDevPack.Name = "chkDevPack";
            this.chkDevPack.UseVisualStyleBackColor = true;
            // 
            // btnDependencies
            // 
            resources.ApplyResources(this.btnDependencies, "btnDependencies");
            this.btnDependencies.Name = "btnDependencies";
            this.btnDependencies.UseVisualStyleBackColor = true;
            this.btnDependencies.Click += new System.EventHandler(this.btnDependencies_Click);
            // 
            // chkDependOnly
            // 
            resources.ApplyResources(this.chkDependOnly, "chkDependOnly");
            this.chkDependOnly.Name = "chkDependOnly";
            this.chkDependOnly.UseVisualStyleBackColor = true;
            // 
            // chkKeepBuild
            // 
            resources.ApplyResources(this.chkKeepBuild, "chkKeepBuild");
            this.chkKeepBuild.Name = "chkKeepBuild";
            this.chkKeepBuild.UseVisualStyleBackColor = true;
            // 
            // chkCurlSupport
            // 
            resources.ApplyResources(this.chkCurlSupport, "chkCurlSupport");
            this.chkCurlSupport.Name = "chkCurlSupport";
            this.chkCurlSupport.UseVisualStyleBackColor = true;
            // 
            // fbCopyto
            // 
            this.fbCopyto.RootFolder = System.Environment.SpecialFolder.MyComputer;
            this.fbCopyto.ShowNewFolderButton = false;
            // 
            // txtFolderCopyTo
            // 
            resources.ApplyResources(this.txtFolderCopyTo, "txtFolderCopyTo");
            this.txtFolderCopyTo.Name = "txtFolderCopyTo";
            this.txtFolderCopyTo.ReadOnly = true;
            // 
            // lblcopyto
            // 
            resources.ApplyResources(this.lblcopyto, "lblcopyto");
            this.lblcopyto.Name = "lblcopyto";
            // 
            // btnCopyto
            // 
            resources.ApplyResources(this.btnCopyto, "btnCopyto");
            this.btnCopyto.Name = "btnCopyto";
            this.btnCopyto.UseVisualStyleBackColor = true;
            this.btnCopyto.Click += new System.EventHandler(this.btnCopyto_Click);
            // 
            // chkcopyonly
            // 
            resources.ApplyResources(this.chkcopyonly, "chkcopyonly");
            this.chkcopyonly.Name = "chkcopyonly";
            this.chkcopyonly.UseVisualStyleBackColor = true;
            // 
            // BuilderForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.chkcopyonly);
            this.Controls.Add(this.btnCopyto);
            this.Controls.Add(this.lblcopyto);
            this.Controls.Add(this.txtFolderCopyTo);
            this.Controls.Add(this.chkCurlSupport);
            this.Controls.Add(this.chkKeepBuild);
            this.Controls.Add(this.chkDependOnly);
            this.Controls.Add(this.btnDependencies);
            this.Controls.Add(this.chkDevPack);
            this.Controls.Add(this.chkBuilddepend);
            this.Controls.Add(this.grpToolchain);
            this.Controls.Add(this.btnBuild);
            this.MaximizeBox = false;
            this.Name = "BuilderForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnBuild;
        private System.Windows.Forms.GroupBox grpToolchain;
        private System.Windows.Forms.CheckBox chkBuilddepend;
        private System.Windows.Forms.CheckBox chkDevPack;
        private System.Windows.Forms.Button btnDependencies;
        private System.Windows.Forms.CheckBox chkDependOnly;
        private System.Windows.Forms.CheckBox chkKeepBuild;
        private System.Windows.Forms.CheckBox chkCurlSupport;
        private System.Windows.Forms.FolderBrowserDialog fbCopyto;
        private System.Windows.Forms.TextBox txtFolderCopyTo;
        private System.Windows.Forms.Label lblcopyto;
        private System.Windows.Forms.Button btnCopyto;
        private System.Windows.Forms.CheckBox chkcopyonly;
    }
}

