using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Xml;
using System.Net;
using System.Configuration;
using System.Collections.Specialized;
using System.Threading;

using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.GZip;
using ICSharpCode.SharpZipLib.Tar;
using ICSharpCode.SharpZipLib.Zip;
using DiffMatchPatch;

using Microsoft.Win32;
using WafaBuilder.Properties;
using System.Threading.Tasks;

namespace WafaBuilder
{
    public partial class BuilderForm : Form
    {

        //32 ou 64 bit ? uname -m = i386 / i686 / x86_64

        private List<Toolchain> _toolchains = new List<Toolchain>
        {
            new Toolchain("windows",new CheckToolchain[]{ new CheckToolchain("msbuild /version",false,PlatformID.Win32NT)},true,BuildSystem.msbuild,"dll","lib",has_editor: true),
            new Toolchain("android",new CheckToolchain[]{new CheckToolchain("ndk-build -v"),new CheckToolchain("adb version"),new CheckToolchain("mingw32-make -v",false,PlatformID.Win32NT)},false,BuildSystem.ndk_build,"so","a",true,true,false,new PostBuildStep[] {new PostBuildStep("arm-linux-androideabi-strip","prebuilt","--strip-unneeded")}),
            new Toolchain("emscripten",new CheckToolchain[]{ new CheckToolchain("emcc --version")},false,BuildSystem.emmake,"so","bc",true,true),
            new Toolchain("linux",new CheckToolchain[]{ new CheckToolchain("gcc --version",false, PlatformID.Unix),new CheckToolchain("uname -a", false, PlatformID.Unix) },true,BuildSystem.make,"so","a"),
            new Toolchain("macosx",new CheckToolchain[]{new CheckToolchain("xcodebuild -version",false, PlatformID.MacOSX) },false,BuildSystem.xcodebuild,"dylib","a"),
            new Toolchain("ios",new CheckToolchain[]{new CheckToolchain("xcodebuild -version",false, PlatformID.MacOSX) },false,BuildSystem.xcodebuildios,"dylib","a")
        };
       
        private DependBuildOptions depend_options = new DependBuildOptions();



        private CheckBox CreatetoolchainCheck(Toolchain tchain,CheckBox LastCheck,bool enabled)
        {
            CheckBox toolcheck = new CheckBox();

            
            if (LastCheck != null)
                toolcheck.Location = new Point(LastCheck.Location.X, LastCheck.Location.Y + 32);
            else
                toolcheck.Location = new Point(16, 16);

            toolcheck.Text = Resources.ResourceManager.GetString(tchain.Name);
            toolcheck.Size = new Size(200, 32);
            toolcheck.Enabled = enabled;
            toolcheck.Tag = tchain;


            grpToolchain.Controls.Add(toolcheck);

            if (tchain.buildsystem == BuildSystem.msbuild && enabled)
            {
                ComboBox vsplat = new ComboBox();
                vsplat.ValueMember = "value2";
                vsplat.DisplayMember = "value1";
                vsplat.Items.AddRange(Toolchain.GetMsbuildPlatforms().ToArray());
                vsplat.SelectedIndexChanged += new EventHandler(vsplat_SelectedIndexChanged);
               // vsplat.Size = new Size(200, 32);
                vsplat.Location = new Point(toolcheck.Location.X + 200, toolcheck.Location.Y);
                vsplat.DropDownStyle = ComboBoxStyle.DropDownList;
                vsplat.Tag = tchain;
                vsplat.Enabled = enabled;
                grpToolchain.Controls.Add(vsplat);

                if(vsplat.Items.Count > 0)
                {
                    vsplat.SelectedIndex = 0;
                }
                else
                {
                    vsplat.Visible = false;
                    //add textbox to manually set a plateform
                    TextBox vsplattext = new TextBox();
                    vsplattext.Location = new Point(toolcheck.Location.X + 200, toolcheck.Location.Y);
                    vsplattext.Tag = tchain;
                    vsplattext.TextChanged += Vsplattext_TextChanged;
                    vsplattext.Enabled = enabled;
                    grpToolchain.Controls.Add(vsplattext);
                }
                
            }

            if (tchain.buildsystem == BuildSystem.xcodebuildios && enabled)
            {
                ComboBox iosoption = new ComboBox();
                iosoption.ValueMember = "value2";
                iosoption.DisplayMember = "value1";

                List<CmbItemString> item_list = new List<CmbItemString>();

                item_list.Add(new CmbItemString("Platform simulator", "SIMULATOR32"));
                item_list.Add(new CmbItemString("Platform simulator 64", "SIMULATOR64"));
                item_list.Add(new CmbItemString("Device", "OS"));


                iosoption.Items.AddRange(item_list.ToArray());

                iosoption.SelectedIndexChanged += new EventHandler(iosoption_SelectedIndexChanged);

                iosoption.Location = new Point(toolcheck.Location.X + 200, toolcheck.Location.Y);
                iosoption.DropDownStyle = ComboBoxStyle.DropDownList;
                iosoption.Tag = tchain;
                iosoption.Enabled = enabled;
                grpToolchain.Controls.Add(iosoption);
                iosoption.SelectedIndex = 0;
            }


            if (tchain.includeDebugBuild)
            {
                Point lastpoint = toolcheck.Location;
                toolcheck = new CheckBox();
                toolcheck.Location = new Point(lastpoint.X, lastpoint.Y + 32);
                toolcheck.Text = Resources.ResourceManager.GetString(tchain.Name) + " Debug";
                toolcheck.Size = new Size(200, 32);
                toolcheck.Enabled = enabled;
                grpToolchain.Controls.Add(toolcheck);
                Toolchain debugToolchain = (Toolchain)tchain.Clone();
                debugToolchain.DebugBuild = true;
                toolcheck.Tag = debugToolchain;

                if (tchain.buildsystem == BuildSystem.msbuild && enabled)
                {
                    ComboBox vsplat = new ComboBox();
                    vsplat.ValueMember = "value2";
                    vsplat.DisplayMember = "value1";
                    vsplat.Items.AddRange(Toolchain.GetMsbuildPlatforms().ToArray());
                    vsplat.SelectedIndexChanged += new EventHandler(vsplat_SelectedIndexChanged);
                    // vsplat.Size = new Size(200, 32);
                    vsplat.Location = new Point(toolcheck.Location.X + 200, toolcheck.Location.Y);
                    vsplat.DropDownStyle = ComboBoxStyle.DropDownList;
                    vsplat.Tag = debugToolchain;
                    vsplat.Enabled = enabled;
                    grpToolchain.Controls.Add(vsplat);

                    if (vsplat.Items.Count > 0)
                    {
                        vsplat.SelectedIndex = 0;
                    }
                    else
                    {
                        vsplat.Visible = false;
                        //add textbox to manually set a plateform
                        TextBox vsplattext = new TextBox();
                        vsplattext.Location = new Point(toolcheck.Location.X + 200, toolcheck.Location.Y);
                        vsplattext.Tag = debugToolchain;
                        vsplattext.TextChanged += Vsplattext_TextChanged;
                        vsplattext.Enabled = enabled;
                        grpToolchain.Controls.Add(vsplattext);
                    }
                }
            }


            
            return toolcheck;
        }

        private void Vsplattext_TextChanged(object sender, EventArgs e)
        {
            Toolchain chain = ((TextBox)sender).Tag as Toolchain;

            string platcandidate = ((TextBox)sender).Text;

            if (Toolchain.vsplat.ContainsValue(platcandidate))
            {
                int plat = 0;
                foreach(KeyValuePair<int,string> platval in Toolchain.vsplat)
                {
                    if(platval.Value == platcandidate)
                    {
                        plat = platval.Key;
                        break;
                    }
                }

                chain.SetPlatform(plat.ToString());
            }
            
        }

        void iosoption_SelectedIndexChanged(object sender, EventArgs e)
        {
            Toolchain chain = ((ComboBox)sender).Tag as Toolchain;

            CmbItemString selecteditem = ((ComboBox)sender).SelectedItem as CmbItemString;

            chain.SetPlatform(selecteditem.value2);
        }

        void vsplat_SelectedIndexChanged(object sender, EventArgs e)
        {
            Toolchain chain = ((ComboBox)sender).Tag as Toolchain ;

            CmbItem selecteditem = ((ComboBox)sender).SelectedItem as CmbItem;

            chain.SetPlatform(selecteditem.value2.ToString());
        }


        public BuilderForm()
        {
            InitializeComponent();

            CheckBox refCheck = null;

            string shell = "cmd.exe";
            string shell_op = @"/c ";
            string shell_op2 = "";

           // int p = (int)Environment.OSVersion.Platform;

          //  if ((p == 4) || (p == 128))
              //  shell = "sh";

            OperatingSystem os = Environment.OSVersion;

            PlatformID pid = os.Platform;

            if (pid == PlatformID.MacOSX || pid == PlatformID.Unix)
            {
                shell = "/bin/bash";
                shell_op = "-c '";
                shell_op2 = "'";
            }

            //TODO : for windows, check the list of visual studio platform installed
            //also check msbuild version and issue warning if the msbuild version in path doesn't work with the selected platform tools


            foreach (Toolchain chain in _toolchains)
            {
                bool enabled = false;
                StringBuilder resultCheck = new StringBuilder();

                Console.WriteLine("Check for toolchain " + chain.Name+" ...");

                foreach (CheckToolchain check in chain.cmdChecks)
                {
                    if (check.msreg)
                    {
                        resultCheck.Clear();

                         RegistryKey key = Registry.CurrentUser.OpenSubKey(check.cmdCheck);

                         if (key != null)
                         {
                             Object inst = key.GetValue("Installed");

                             if (inst != null)
                             {
                                 int result = Convert.ToInt32(inst);

                                 if (result == 1)
                                     resultCheck.Append("checkok");
                                 else
                                     break;
                             }
                             else
                                 break;
                         }
                         else
                             break;
                    }
                    else
                    {

                        resultCheck.Clear();

                        if (check.restrict_os == null || check.restrict_os == os.Platform)
                        {

                            StringBuilder errorcheck = new StringBuilder();
                            ProcessStartInfo startinfo = new ProcessStartInfo(shell, shell_op + check.cmdCheck + shell_op2);
                            startinfo.UseShellExecute = false;
                            startinfo.RedirectStandardOutput = true;
                            startinfo.CreateNoWindow = true;
                            startinfo.RedirectStandardError = true;
                            Process build_proc = Process.Start(startinfo);
                            build_proc.OutputDataReceived += (sender, e) => resultCheck.Append(e.Data);
                            build_proc.ErrorDataReceived += (sender, e) => errorcheck.Append(e.Data);
                            build_proc.BeginOutputReadLine();
                            build_proc.BeginErrorReadLine();
                            build_proc.WaitForExit();

                            if (string.IsNullOrEmpty(resultCheck.ToString().Trim()))
                                break;
                        }
                    }
                }

                enabled = !string.IsNullOrEmpty(resultCheck.ToString().Trim());

                if (enabled && chain.LocateToolChain)
                {
                    string locatecommand = "where";

                    if(pid == PlatformID.Unix) {
                        locatecommand = "which";
                    }

                    StringBuilder resultLocate = new StringBuilder(); 

                    ProcessStartInfo locateinfo = new ProcessStartInfo(shell, shell_op + locatecommand+" "+chain.cmdChecks[0].cmdCheck + shell_op2);
                    locateinfo.UseShellExecute = false;
                    locateinfo.RedirectStandardOutput = true;
                    locateinfo.CreateNoWindow = true;
                    locateinfo.RedirectStandardError = true;
                    Process locate_proc = Process.Start(locateinfo);
                    locate_proc.OutputDataReceived += (sender, e) => resultLocate.Append(e.Data+Environment.NewLine);
                    locate_proc.BeginOutputReadLine();
                    locate_proc.WaitForExit();

                    if(!string.IsNullOrEmpty(resultLocate.ToString().Trim()))
                    {
                        string fstring = resultLocate.ToString();
                        string path = fstring.Substring(0, fstring.IndexOf(Environment.NewLine));
                        path = path.Substring(0, path.LastIndexOf(Path.DirectorySeparatorChar));
                        chain.ChainRoot = path;
                    }
                }

                //check for x64 architecture on unix platform
                if (enabled && pid == PlatformID.Unix)
                {
                    StringBuilder resultArch = new StringBuilder();

                    ProcessStartInfo archinfo = new ProcessStartInfo(shell, shell_op + "uname -m"+ shell_op2);
                    archinfo.UseShellExecute = false;
                    archinfo.RedirectStandardOutput = true;
                    archinfo.CreateNoWindow = true;
                    archinfo.RedirectStandardError = true;
                    Process arch_proc = Process.Start(archinfo);
                    arch_proc.OutputDataReceived += (sender, e) => resultArch.Append(e.Data + Environment.NewLine);
                    arch_proc.BeginOutputReadLine();
                    arch_proc.WaitForExit();

                    string archString = resultArch.ToString().Trim();

                    if (!string.IsNullOrEmpty(archString))
                    {
                        //use x64 if the arch match, otherwise use x32 by default
                        if (archString == "x86_64")
                        {
                            Console.WriteLine("Using x64 for toolchain " + chain.Name + " ...");
                            chain.useX64 = true;
                        }
                    }
                }


                refCheck = CreatetoolchainCheck(chain,refCheck,enabled);

                if (chain.buildsystem != BuildSystem.msbuild)
                    chain.SetPlatform("0");

            }

            if(!string.IsNullOrEmpty(Settings.Default.lastcopytofolder))
            {
                txtFolderCopyTo.Text = Settings.Default.lastcopytofolder;
                fbCopyto.SelectedPath = Settings.Default.lastcopytofolder;
            }

            Console.WriteLine("Looking for dependency file ...");
            //check if we have a dependency config document
            string dependpath = Path.Combine("dependencies", "depend.xml");
            if (!(Directory.Exists("dependencies") && File.Exists(dependpath)))
            {
                chkBuilddepend.Enabled = false;
                Console.WriteLine("No dependency file!");
            }
            else
            {
                Console.WriteLine("Dependency file loaded!");

                //gather the list of dependencies to build

                XmlDocument dependconf = new XmlDocument();
                dependconf.Load(dependpath);

                XmlNodeList dependnodes = dependconf.SelectNodes("/dependencies/depend");

                foreach (XmlNode node in dependnodes)
                {
                    string dependname = string.Empty;

                    if (node.Attributes["name"] != null)
                        dependname = node.Attributes["name"].Value;

                    depend_options.AddDependency(dependname);
                }

            }
        }

        /// <summary>
        /// check if a node element should be used with this toolchain
        /// </summary>
        /// <param name="cnode">xmlnode element</param>
        /// <param name="chain">the current toolchain</param>
        /// <returns>true if the element can be used, false if this node is not used for this toolchain</returns>
        private static bool CheckNodeRestriction(XmlNode cnode,Toolchain chain)
        {
            if(cnode.Attributes["unless"] != null)
            {

                string[] unlesslist = cnode.Attributes["unless"].Value.Split(new char[] { ' ' });

                return !unlesslist.Any(p => p == chain.Name);

            }

            if (cnode.Attributes["restrict"] != null)
            {
                string[] restrictlist = cnode.Attributes["restrict"].Value.Split(new char[] { ' ' });

                return restrictlist.Any(p => p == chain.Name);
            }

            return true;
        }

        public static bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }

        private static bool LinkIsReachable(string url, BuildLog logform)
        {
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                // Skip validation of SSL/TLS certificate
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                //Creating the HttpWebRequest
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;

                //Setting the Request method HEAD, you can also use GET too.
                request.Method = "HEAD";
                //Getting the Web Response.
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                //Returns TRUE if the Status code == 200
                response.Close();
                return (response.StatusCode == HttpStatusCode.OK);
            }
            catch(WebException ex)
            {
                logform.log(ex.Message);
                //Any exception will returns false.
                return false;
            }   
        }

        private static bool SafeFileCopy(string sourceName, string destName, bool overwrite, BuildLog logform)
        {
            if (!File.Exists(sourceName))
            {
                logform.log(sourceName+ " doesn't exist!", color: Color.Red);
                return false;
            }

            if (!overwrite && File.Exists(destName))
            {
                logform.log("Can't copy " + sourceName + " to " + destName + ", the destination file already exists, use overwrite", color: Color.Red);
                return false;
            }

            FileInfo sf_info = new FileInfo(sourceName);

            //while (IsFileLocked(sf_info))
                //continue;

            File.Copy(sourceName, destName, overwrite);
            logform.log("Copied " + sourceName);

            return true;
        }

        private static bool SafeDirDelete(string dirname, bool recursive,BuildLog logform)
        {
            if (!Directory.Exists(dirname))
            {
                logform.log(dirname + " doesn't exist!", color: Color.Red);
                return false;
            }

            try
            {
                Directory.Delete(dirname, recursive);
            }
            catch (Exception ex)
            {
                logform.log("Error while deleting " + dirname + " ex : " + ex.ToString(), color: Color.Red);
                return false;
            }

            return true;
        }

        private static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs,BuildLog logform = null,List<string> excludefiles = null)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            DirectoryInfo[] dirs = dir.GetDirectories();
            // If the destination directory doesn't exist, create it.
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                if(excludefiles != null && excludefiles.Contains(file.Name))
                {
                    continue;
                }

                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, true);

                if (logform != null)
                    logform.log("Copied file " + file.FullName, true);

            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }

        private void CopyDevpackToGameRoot(string gamedirroot,List<string> chainsoutputs,BuildLog logform)
        {
            //copy / replace files

            List<string> dirtocopy = new List<string>(new string[] { "include","testor", "tools","doc_wafa","depends_inc","animation_converter","LMS"});

            foreach(string chainout in chainsoutputs)
            {
                dirtocopy.Add("libs-"+chainout);

                if(chainout == "msvc" || chainout == "msvc-debug")
                {
                    dirtocopy.Add("include-msvc");
                }

            }


            foreach (string srcdir in dirtocopy)
            {
                string srcpath = Path.Combine("dev_pack", srcdir);
                string destpath = Path.Combine(gamedirroot, srcdir);
                DirectoryCopy(srcpath, destpath, true);
                logform.log("Copied directory " + srcpath+ " to "+ destpath, true);
            }

            //copy /replace editor (keep config files)

            string srceditorpath = Path.Combine("dev_pack", "wafaeditor_v2");
            string desteditorpath = Path.Combine(gamedirroot, "wafaeditor_v2");

            List<string> excludefiles = new List<string>(new string[] { "config.lua","locale.db","save.db" });

            DirectoryCopy(srceditorpath, desteditorpath, false,null,excludefiles);

            logform.log("Copied directory " + srceditorpath + " to " + desteditorpath, true);
        }

        BuildLog logform = null;
        private void btnBuild_Click(object sender, EventArgs e)
        {
            string shell = "cmd.exe";
            string shell_op = @"/c ";
            string shell_op2 = "";

            // int p = (int)Environment.OSVersion.Platform;

            //  if ((p == 4) || (p == 128))
            //  shell = "sh";

            OperatingSystem os = Environment.OSVersion;

            PlatformID pid = os.Platform;

            if (pid == PlatformID.MacOSX || pid == PlatformID.Unix)
            {
                shell = "/bin/bash";
                shell_op = "-c '";
                shell_op2 = "'";
            }

            List<CheckBox> selboxes = new List<CheckBox>(grpToolchain.Controls.Count);
            Dictionary<string, dependinfo> depinfo = new Dictionary<string, dependinfo>();

            foreach(Control control in grpToolchain.Controls)
            {
                if(control is CheckBox)
                {
                   if(((CheckBox)control).Checked)
                       selboxes.Add((CheckBox)control);
                }
            }

            logform = new BuildLog();

            logform.Show();

            diff_match_patch diff_obj = new diff_match_patch();

            WebClient dependclient = new WebClient();
            dependclient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(dependclient_DownloadProgressChanged);
            dependclient.DownloadFileCompleted += new AsyncCompletedEventHandler(dependclient_DownloadFileCompleted);

            string outdir = Path.Combine("dependencies", "output");
            string outdirinclude = Path.Combine(outdir, "include");

            if (chkBuilddepend.Checked)
            {
                if (!Directory.Exists(outdir))
                    Directory.CreateDirectory(outdir);
                
                if(!Directory.Exists(outdirinclude))
                    Directory.CreateDirectory(outdirinclude);
            }

            bool hasbuild_editor = false;

            //for each selected toolchain

            List<string> chaintocopy = new List<string>();

            foreach (CheckBox sel in selboxes)
            {
                Toolchain chain = sel.Tag as Toolchain;

                chaintocopy.Add(chain.Getoutdir());

                //only copy devpack, no build, break out of build loop
                if (chkcopyonly.Checked)
                {
                    break;
                }


                logform.log("Build for " + chain.Name + " platform...", color: Color.Blue, font: BuildLog.TitleFont);

                string outdirlib = Path.Combine("dependencies", "output", "libs-" + chain.Getoutdir());

                //build dependencies
                if (chkBuilddepend.Checked)
                {
                    //create outdir for current toolchain
                    if (Directory.Exists(outdirlib) && depend_options.DeleteExistingOutput)
                        Directory.Delete(outdirlib, true);

                    Directory.CreateDirectory(outdirlib);

                    //-1- read dependencies xml document
                    XmlDocument dependconf = new XmlDocument();
                    string dependpath = Path.Combine("dependencies", "depend.xml");
                    dependconf.Load(dependpath);

                    XmlNodeList dependnodes = dependconf.SelectNodes("/dependencies/depend");

                    foreach (XmlNode node in dependnodes)
                    {
                        string dependname = string.Empty;

                        if (node.Attributes["name"] != null)
                            dependname = node.Attributes["name"].Value;

                        //do not build this dependency if it's not used by the current toolchain
                        if (!CheckNodeRestriction(node, chain))
                            continue;

                        //do not build this dependency if we unselected it in dependencies options
                        if (!depend_options.CanBuildDependency(dependname))
                            continue;

                        dependinfo inf = null;

                        if (depinfo.ContainsKey(dependname))
                            inf = depinfo[dependname];
                        else
                        {
                            inf = new dependinfo();
                            depinfo.Add(dependname, inf);
                        }


                        logform.log("Start building dependency " + dependname, color: Color.Blue, font: BuildLog.TitleFont);

                        //create dependency directory if not existing
                        string dirpath = Path.Combine("dependencies", dependname);

                        if (!Directory.Exists(dirpath))
                            Directory.CreateDirectory(dirpath);


                        XmlNode linknode = node.SelectSingleNode("link");

                        string linkaddr = linknode.InnerText;
                        string filedest = linkaddr.Substring(linkaddr.LastIndexOf("/") + 1);
                        string pathdest = Path.Combine("dependencies", dependname, filedest);


                        if (File.Exists(pathdest))
                            inf.sourcedownloaded = true;

                        //fetch dependency source
                        if (!inf.sourcedownloaded)
                        {
                            if(!LinkIsReachable(linkaddr,logform))
                            {
                                logform.log("Source code at addresse "+linkaddr+" is unreachable, check your internet connection and that the link is valid / the resource is available, skipping this dependency...");
                                continue;
                            }

                            logform.log("Download source code...");


                            if (!File.Exists(pathdest))
                            {
                                try
                                {

                                    dependclient.DownloadFileAsync(new Uri(linkaddr), pathdest);


                                    while (dependclient.IsBusy)
                                        Application.DoEvents();


                                }
                                catch (WebException ex)
                                {
                                    logform.log("Error while downloading " + dependname + " source code! " + ex.ToString(), color: Color.Red);
                                }
                            }

                        }

                        //clear existing data if any
                        foreach (string old_dir in Directory.GetDirectories(dirpath))
                            Directory.Delete(old_dir, true);

                        foreach (string old_file in Directory.GetFiles(dirpath))
                            if (old_file != pathdest)
                                File.Delete(old_file);

                        //extract downloaded file
                        if (filedest.EndsWith(".zip"))
                        {
                            logform.log("Extracting zip file...");

                            FastZip fastzip = new FastZip();
                            fastzip.ExtractZip(pathdest, dirpath, null);
                        }
                        else if (filedest.EndsWith(".tar.gz") || filedest.EndsWith(".tgz"))
                        {
                            logform.log("Extracting tgz file");
                            Stream inStream = File.OpenRead(pathdest);
                            Stream gzipstream = new GZipInputStream(inStream);

                            TarArchive tarArchive = TarArchive.CreateInputTarArchive(gzipstream);
                            tarArchive.ExtractContents(dirpath);
                            tarArchive.Close();
                            inStream.Close();
                            gzipstream.Close();
                        }

                        //get the first directory in dependency dir

                        string[] dir_list = Directory.GetDirectories(dirpath);

                        string build_dir = dir_list[0];

                        //fix access right (only for unix)
                        if (pid == PlatformID.MacOSX || pid == PlatformID.Unix)
                        {
                            ProcessStartInfo chmodinfo = new ProcessStartInfo(shell, shell_op + "chmod -R 777 " + build_dir + shell_op2);
                            chmodinfo.UseShellExecute = false;
                            chmodinfo.RedirectStandardOutput = true;
                            chmodinfo.WorkingDirectory = Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf(Path.DirectorySeparatorChar)); ;
                            chmodinfo.CreateNoWindow = true;
                            chmodinfo.RedirectStandardError = true;
                            Process chmod_proc = Process.Start(chmodinfo);
                            chmod_proc.OutputDataReceived += (snd, earg) => logform.log(earg.Data, true);
                            chmod_proc.ErrorDataReceived += (snd2, earg2) => logform.log(earg2.Data, true, Color.Red);
                            chmod_proc.BeginOutputReadLine();
                            chmod_proc.BeginErrorReadLine();

                            while (!chmod_proc.HasExited)
                                Application.DoEvents();
                        }



                        //apply patch (if any) to downloaded source code
                        XmlNodeList patchnodes = node.SelectNodes("patchs/patch");

                        //patch list
                        foreach (XmlNode patchnode in patchnodes)
                        {
                            //get path attribute (if any)
                            string patch_path = string.Empty;

                            if (patchnode.Attributes["path"] != null)
                                patch_path = patchnode.Attributes["path"].Value;


                            if (!CheckNodeRestriction(patchnode, chain))
                                continue;

                            string patch_file = Path.Combine(new string[] { "dependencies", "patch", dependname, patchnode.InnerText });
                            List<Patch> patch_list = diff_obj.patch_fromText(File.ReadAllText(patch_file));

                            //apply patch
                            //get the original source file
                            string fixed_path = patch_path.Replace('/', Path.DirectorySeparatorChar);
                            string orig_file = Path.Combine(new string[] { build_dir, fixed_path });


                            string orig_content = File.ReadAllText(orig_file);

                            //all patches files are supposed to be in unix end of line format, since the source code is also supposed to be in this format, in some rare case, the source code could be in Windows format (like with libpng), in this case we translate the file before patching it 
                            if (orig_content.IndexOf("\r\n") != -1)
                            {
                                orig_content = orig_content.Replace("\r\n", "\n");
                            }

                            object[] patch_result = diff_obj.patch_apply(patch_list, orig_content);

                            //check if the patch worked
                            bool[] result_patch = patch_result[1] as bool[];

                            for (int patch_res = 0; patch_res < result_patch.Length; patch_res++)
                            {
                                if (!result_patch[patch_res])
                                {
                                    logform.log("Failed to apply patch to " + orig_file, false, Color.Red);
                                }
                                else
                                {
                                    logform.log("Applied patch to " + orig_file);
                                }
                            }

                            string patched_content = patch_result[0] as string;

                            File.WriteAllText(orig_file, patched_content);
                        }

                        //new dir list
                        XmlNodeList newdirnodes = node.SelectNodes("patchs/newdir");

                        foreach (XmlNode newdirnode in newdirnodes)
                        {
                            //get path attribute (if any)
                            string patch_path = string.Empty;

                            if (newdirnode.Attributes["path"] != null)
                                patch_path = newdirnode.Attributes["path"].Value;


                            if (!CheckNodeRestriction(newdirnode, chain))
                                continue;


                            string new_dir_path = Path.Combine(build_dir, patch_path, newdirnode.InnerText);

                            if (!Directory.Exists(new_dir_path))
                            {
                                logform.log("Create new directory " + new_dir_path);
                                Directory.CreateDirectory(new_dir_path);
                            }
                        }


                        //new file list
                        XmlNodeList newfilenodes = node.SelectNodes("patchs/newfile");

                        foreach (XmlNode newnode in newfilenodes)
                        {
                            //get path attribute (if any)
                            string patch_path = string.Empty;

                            if (newnode.Attributes["path"] != null)
                                patch_path = newnode.Attributes["path"].Value;

                            if (!CheckNodeRestriction(newnode, chain))
                                continue;

                            string fixed_path = patch_path.Replace('/', Path.DirectorySeparatorChar);
                            string file_path = Path.Combine(new string[] { "dependencies", "patch", dependname, newnode.InnerXml });
                            string new_file_path = Path.Combine(new string[] { build_dir, fixed_path, newnode.InnerXml });

                            logform.log("Create new file " + new_file_path);

                            File.Copy(file_path, new_file_path, true);

                        }

                        //rename file list
                        XmlNodeList renamefilenodes = node.SelectNodes("patchs/renamefile");

                        foreach (XmlNode rnnode in renamefilenodes)
                        {
                            string pathrn = string.Empty;

                            if (rnnode.Attributes["path"] != null)
                                pathrn = rnnode.Attributes["path"].Value;


                            if (!CheckNodeRestriction(rnnode, chain))
                                continue;


                            string fixed_path = pathrn.Replace('/', Path.DirectorySeparatorChar);
                            // string file_path = Path.Combine(new string[] { "dependencies", "patch", dependname, rnnode.InnerXml });
                            string old_file_path = Path.Combine(new string[] { build_dir, fixed_path });

                            FileInfo old = new FileInfo(old_file_path);

                            if (File.Exists(old.DirectoryName + Path.DirectorySeparatorChar + rnnode.InnerText))
                                File.Delete(old.DirectoryName + Path.DirectorySeparatorChar + rnnode.InnerText);

                            File.Move(old_file_path, old.DirectoryName + Path.DirectorySeparatorChar + rnnode.InnerText);
                        }

                        //get prebuild action
                        XmlNodeList prebuild_list = node.SelectNodes("prebuild");

                        foreach (XmlNode prebuild in prebuild_list)
                        {
                            if (!CheckNodeRestriction(prebuild, chain))
                                continue;

                            string action = prebuild.Attributes["action"].Value;
                            string sourcedir = prebuild.Attributes["sourcedir"].Value;

                            string params_exec = string.Empty;

                            if (prebuild.Attributes["params"] != null)
                                params_exec = prebuild.Attributes["params"].Value;

							if (prebuild.Attributes["paramsdebug"] != null && chain.DebugBuild)
                                params_exec = prebuild.Attributes["paramsdebug"].Value;


                            // DirectoryInfo dirinfo = new DirectoryInfo(sourcedir);
                            if (Directory.Exists(sourcedir) || File.Exists(sourcedir))
                            {
                                FileAttributes attr = File.GetAttributes(sourcedir);

                                string destDirName = prebuild.InnerText;

                                if (action == "copy")
                                {
                                    if ((attr & FileAttributes.Directory) > 0)
                                        DirectoryCopy(sourcedir, destDirName, true);
                                    else
                                        SafeFileCopy(sourcedir, destDirName, true, logform);
                                }
                                else if (action == "exec")
                                {
                                    chain.ExecScript(destDirName, sourcedir, logform, params_exec);
                                }
                                else if (action == "fixconfigure")
                                {
                                    Toolchain.FixConfigure(sourcedir);
                                }
                            }
                        }



                        //generate project file if needed and compile code using selected toolchain
                        XmlNodeList generatornodes = node.SelectNodes("generator");
                        string selectedgenerator = string.Empty;
                        string projpath = string.Empty;
                        string options = string.Empty;
                        string buildoptions = string.Empty;
                        string buildpath = string.Empty;
                        string genpath = string.Empty;

                        XmlNode selectednode = null;

                        if (generatornodes.Count == 1)
                        {
                            selectednode = generatornodes[0];
                           
                        }
                        else
                        {
                            foreach (XmlNode gencandidate in generatornodes)
                            {
                                if (!CheckNodeRestriction(gencandidate, chain))
                                    continue;

                                selectednode = gencandidate;
                              
                            }
                        }


                        if(selectednode != null)
                        {
                            selectedgenerator = selectednode.InnerText;

                            if (selectednode.Attributes["path"] != null)
                                projpath = selectednode.Attributes["path"].Value;

                            if (selectednode.Attributes["options"] != null)
                                options = selectednode.Attributes["options"].Value;

                            if (selectednode.Attributes["buildoptions"] != null)
                                buildoptions = selectednode.Attributes["buildoptions"].Value;

                            if (selectednode.Attributes["buildpath"] != null)
                                buildpath = selectednode.Attributes["buildpath"].Value;

                            if (selectednode.Attributes["genpath"] != null)
                                genpath = selectednode.Attributes["genpath"].Value;

                            if (chain.DebugBuild && selectednode.Attributes["optionsdebug"] != null)
                                options = generatornodes[0].Attributes["optionsdebug"].Value;

                            if (chain.DebugBuild && selectednode.Attributes["buildoptionsdebug"] != null)
                                buildoptions = selectednode.Attributes["buildoptionsdebug"].Value;
                        }
                      

                        //get output type
                        XmlNodeList outnodelist = node.SelectNodes("outtype");
                        XmlNode outnode = null;

                        foreach (XmlNode outnodecandidate in outnodelist)
                        {
                            if (!CheckNodeRestriction(outnodecandidate, chain))
                                continue;

                            outnode = outnodecandidate;
                        }

                        if(outnode == null)
                        {
                            logform.log("Missing outtype for selected platforme, please check dependencies...");
                            continue;
                        }


                        string outtype = outnode.InnerText;

                        string outpath = string.Empty;

                        if (outnode.Attributes["path"] != null)
                            outpath = outnode.Attributes["path"].Value;
                        else
                        {
                            logform.log("Missing path attribute in outtype, skipping dependency...");
                            continue;
                        }

                        string depinclude = outnode.Attributes["include"].Value;

                        string opt_include_dest = string.Empty;

                        if (outnode.Attributes["includedestdir"] != null)
                            opt_include_dest = outnode.Attributes["includedestdir"].Value;

                        string outbin = string.Empty;

                        if (outnode.Attributes["pathbin"] != null)
                            outbin = outnode.Attributes["pathbin"].Value;

                        if (chain.DebugBuild && outnode.Attributes["pathdebug"] != null)
                            outpath = outnode.Attributes["pathdebug"].Value;

                        if (chain.DebugBuild && outnode.Attributes["pathbindebug"] != null)
                            outbin = outnode.Attributes["pathbindebug"].Value;


                        string c_shell = string.Empty;

                        if (chain.CustomBuildShell && selectedgenerator == "cmake")
                            c_shell = chain.GetCustomBuildShell();

                        string build_shell = (c_shell != string.Empty) ? c_shell : shell;
                        string build_shell_op = (c_shell != string.Empty) ? string.Empty : shell_op;
                        string build_shell_op2 = (c_shell != string.Empty) ? string.Empty : shell_op2;

                        if (selectedgenerator != string.Empty && outtype != "builtlib")//nothing to generate if the lib is already built
                        {

                            string FullProjPath = Path.Combine(build_dir, projpath.Replace('/', Path.DirectorySeparatorChar));
                            string FullBuildPath = Path.Combine(build_dir, buildpath.Replace('/', Path.DirectorySeparatorChar));
                            string FullGenPath = Path.Combine(build_dir, genpath.Replace('/', Path.DirectorySeparatorChar));

                            logform.log("Start building " + dependname + " using " + selectedgenerator);

                            if (selectedgenerator == "cmake")
                            {
                                string cmake_options = string.Empty;

                                if (options != string.Empty)
                                {
                                    string[] opt_array = options.Split(new char[] { ';' });

                                    foreach (string opt_single in opt_array)
                                        cmake_options += "-D " + opt_single + " ";
                                }

                                string sh = shell_op + "cmake " + chain.GetCmakestring() + " " + cmake_options + " -B" + FullBuildPath + " -H" + FullGenPath + shell_op2;
                                ProcessStartInfo startinfo = new ProcessStartInfo(shell, sh);
                                startinfo.UseShellExecute = false;
                                startinfo.RedirectStandardOutput = true;
                                startinfo.CreateNoWindow = true;
                                startinfo.WorkingDirectory = Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf(Path.DirectorySeparatorChar));
                                startinfo.RedirectStandardError = true;
                                Process build_proc = Process.Start(startinfo);
                                build_proc.OutputDataReceived += (snd, earg) => logform.log(earg.Data, true);
                                build_proc.ErrorDataReceived += (snd2, earg2) => logform.log(earg2.Data, true, Color.Red);
                                build_proc.BeginOutputReadLine();
                                build_proc.BeginErrorReadLine();

                                while (!build_proc.HasExited)
                                    Application.DoEvents();

                                string cmake_build_options = "";

                                if (!string.IsNullOrEmpty(buildoptions))
                                {
                                    Dictionary<string, string> confarray = chain.GetConfigArray(buildoptions);

                                    if (confarray.ContainsKey("target"))
                                        cmake_build_options += " --target " + confarray["target"];


                                    if (confarray.ContainsKey("config"))
                                        cmake_build_options += " --config " + confarray["config"];
                                }


                                // ProcessStartInfo startinfo2 = new ProcessStartInfo(build_shell, build_shell_op + chain.GenerateBuildCmd(buildoptions, FullProjPath, build_dir, selectedgenerator, outtype) + build_shell_op2);
                                string sh2 = shell_op + "cmake --build " + FullBuildPath + cmake_build_options + shell_op2;

                                ProcessStartInfo startinfo2 = new ProcessStartInfo(shell, sh2);
                                startinfo2.UseShellExecute = false;
                                startinfo2.RedirectStandardOutput = true;
                                startinfo2.WorkingDirectory = Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf(Path.DirectorySeparatorChar));
                                startinfo2.CreateNoWindow = true;
                                startinfo2.RedirectStandardError = true;
                                Process build_proc2 = Process.Start(startinfo2);
                                build_proc2.OutputDataReceived += (snd, earg) => logform.log(earg.Data, true);
                                build_proc2.ErrorDataReceived += (snd2, earg2) => logform.log(earg2.Data, true, Color.Red);
                                build_proc2.BeginOutputReadLine();
                                build_proc2.BeginErrorReadLine();

                                while (!build_proc2.HasExited)
                                    Application.DoEvents();
                            }
                            else
                            {

                                chain.BuildProj(buildoptions, FullProjPath, build_dir, selectedgenerator, logform, outtype, options);
                            }


                            logform.log(dependname + " built");
                        }


                        logform.log("Copying " + dependname + " headers and libs");

                        //copy include and built library to output directory
                        string rpath = chain.GetLibName(true, outpath);

                        string converted_path = Path.Combine(build_dir, rpath.Replace('/', Path.DirectorySeparatorChar));

                        int lsep = rpath.LastIndexOf('/') + 1;
                        string outputlibrarybindir = Path.Combine(outdirlib, rpath.Substring(lsep, rpath.Length - lsep));

                        string outputlibraryincludedir = Path.Combine(outdirinclude, dependname, opt_include_dest.Replace('/', Path.DirectorySeparatorChar));

                        if (!Directory.Exists(outputlibraryincludedir))
                            Directory.CreateDirectory(outputlibraryincludedir);

                        //msbuild always generate at least a .lib, emmake generate static lib (.a file) only and make will generate either a .a or a .so / .dylib
                        if ((chain.buildsystem == BuildSystem.msbuild || chain.buildsystem == BuildSystem.emmake) || outbin == string.Empty)
                        {
                            try
                            {
                                File.Copy(converted_path, outputlibrarybindir, true);
                                logform.log("Copied " + converted_path);
                            }
                            catch (Exception ex)
                            {
                                logform.log("Error copying " + converted_path + " " + ex.ToString(), color: Color.Red);
                            }
                        }

                        if (outbin != string.Empty)
                        {
                            string bpath = chain.GetLibName(false, outbin);
                            string converted_bin_path = Path.Combine(build_dir, bpath.Replace('/', Path.DirectorySeparatorChar));

                            int lsep2 = bpath.LastIndexOf('/') + 1;
                            string outputbindir = Path.Combine(outdirlib, bpath.Substring(lsep2, bpath.Length - lsep2));

                            try
                            {
                                File.Copy(converted_bin_path, outputbindir, true);
                                logform.log("Copied " + converted_bin_path);
                            }
                            catch (Exception ex)
                            {
                                logform.log("Error copying " + converted_bin_path + " " + ex.ToString(), color: Color.Red);
                            }
                        }

                        string[] list_header = depinclude.Split(new char[] { ';' });

                        foreach (string dep_single_include in list_header)
                        {

                            string converted_include_path = Path.Combine(build_dir, dep_single_include.Replace('/', Path.DirectorySeparatorChar));

                            if (!converted_include_path.EndsWith(".h"))
                            {
                                try
                                {
                                    DirectoryCopy(converted_include_path, outputlibraryincludedir, true);
                                    logform.log("Copied " + converted_include_path);
                                }
                                catch (Exception ex)
                                {
                                    logform.log("Error copying " + converted_include_path + " " + ex.ToString(), color: Color.Red);
                                }
                            }
                            else
                            {
                                if (!dep_single_include.EndsWith("*.h"))
                                {
                                    int iname = converted_include_path.LastIndexOf(Path.DirectorySeparatorChar) + 1;

                                    try
                                    {
                                        File.Copy(converted_include_path, Path.Combine(outputlibraryincludedir, converted_include_path.Substring(iname, converted_include_path.Length - iname)), true);
                                        logform.log("Copied " + converted_include_path);
                                    }
                                    catch (Exception ex)
                                    {
                                        logform.log("Error copying " + converted_include_path + " " + ex.ToString(), color: Color.Red);
                                    }
                                }
                                else
                                {
                                    string searchincpath = converted_include_path.Substring(0, converted_include_path.LastIndexOf('*'));

                                    foreach (string file in Directory.GetFiles(searchincpath))
                                    {
                                        FileInfo includeinfo = new FileInfo(file);

                                        if (includeinfo.Extension == ".h")
                                        {
                                            try
                                            {
                                                File.Copy(includeinfo.FullName, Path.Combine(outputlibraryincludedir, includeinfo.Name), true);
                                                logform.log("Copied " + includeinfo.FullName);
                                            }
                                            catch (Exception ex)
                                            {
                                                logform.log("Error copying " + includeinfo.FullName + " " + ex.ToString(), color: Color.Red);
                                            }
                                        }
                                    }
                                }
                            }
                        }


                    }


                    logform.log("Finished building all dependencies", color: Color.Blue, font: BuildLog.TitleFont);

                }

                if (!chkDependOnly.Checked)
                {


                    //build engine
                    logform.log("Start building engine ", color: Color.Blue, font: BuildLog.TitleFont);

                    //remove the autobuild directory if any
                    string path_autobuild = Path.Combine("engine", "c", "autobuild");

                    if (Directory.Exists(path_autobuild) && !chkKeepBuild.Checked)
                        Directory.Delete(path_autobuild, true);

                    //generate a projet file using cmake
                    string extra_option = string.Empty;

                    if (chain.DebugBuild)
                        extra_option += " -D USE_DEBUG_DEPEND=ON ";

                    if (chkCurlSupport.Checked)
                        extra_option += " -D WITH_CURL=ON ";


                    if (!chkKeepBuild.Checked)
                    {
                        string sh_engine = shell_op + "cmake " + chain.GetCmakestring() + extra_option + " -Bengine/c/autobuild" + " -Hengine/c" + shell_op2;
                        ProcessStartInfo engine_startinfo = new ProcessStartInfo(shell, sh_engine);
                        engine_startinfo.UseShellExecute = false;
                        engine_startinfo.RedirectStandardOutput = true;
                        engine_startinfo.CreateNoWindow = true;
                        engine_startinfo.WorkingDirectory = Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf(Path.DirectorySeparatorChar));
                        engine_startinfo.RedirectStandardError = true;
                        Process build_proc_engine = Process.Start(engine_startinfo);
                        build_proc_engine.OutputDataReceived += (snd, earg) => logform.log(earg.Data, true);
                        build_proc_engine.ErrorDataReceived += (snd2, earg2) => logform.log(earg2.Data, true, Color.Red);
                        build_proc_engine.BeginOutputReadLine();
                        build_proc_engine.BeginErrorReadLine();

                        while (!build_proc_engine.HasExited)
                            Application.DoEvents();
                    }

                    string c_shell_engine = string.Empty;

                    if (chain.CustomBuildShell)
                        c_shell_engine = chain.GetCustomBuildShell();

                    string build_shell_engine = (c_shell_engine != string.Empty) ? c_shell_engine : shell;
                    string build_shell_op_engine = (c_shell_engine != string.Empty) ? string.Empty : shell_op;
                    string build_shell_op2_engine = (c_shell_engine != string.Empty) ? string.Empty : shell_op2;

                    //build the generated project file
                    ProcessStartInfo startinfo2_engine = new ProcessStartInfo(build_shell_engine, build_shell_op_engine + chain.GenerateBuildCmd("test=test", path_autobuild, path_autobuild) + build_shell_op2_engine);
                    startinfo2_engine.UseShellExecute = false;
                    startinfo2_engine.RedirectStandardOutput = true;
                    startinfo2_engine.WorkingDirectory = Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf(Path.DirectorySeparatorChar));
                    startinfo2_engine.CreateNoWindow = true;
                    startinfo2_engine.RedirectStandardError = true;
                    Process build_proc2_engine = Process.Start(startinfo2_engine);
                    build_proc2_engine.OutputDataReceived += (snd, earg) => logform.log(earg.Data, true);
                    build_proc2_engine.ErrorDataReceived += (snd2, earg2) => logform.log(earg2.Data, true, Color.Red);
                    build_proc2_engine.BeginOutputReadLine();
                    build_proc2_engine.BeginErrorReadLine();

                    while (!build_proc2_engine.HasExited)
                        Application.DoEvents();

                    logform.log("Engine built", color: Color.Blue, font: BuildLog.TitleFont);

                    //copy generated engine 

                    logform.log("Copy engine files...");

                    string dev_pack_libs = Path.Combine("dev_pack", "libs-" + chain.Getoutdir());
                    string enginepath = string.Empty;

                    if (chain.buildsystem == BuildSystem.msbuild)
                    {
                        if (chain.DebugBuild)
                            enginepath = Path.Combine(path_autobuild, "Debug");
                        else
                            enginepath = Path.Combine(path_autobuild, "Release");
                    }
                    else
                    {
                        enginepath = path_autobuild;
                    }

                    //clear library directory content for current toolchain
                    if (Directory.Exists(dev_pack_libs))
                    {
                        foreach (string file in Directory.GetFiles(dev_pack_libs))
                        {
                            File.Delete(file);
                        }
                    }
                    else
                    {
                        //create a new directory for libraries
                        Directory.CreateDirectory(dev_pack_libs);
                    }


                    if (chain.buildsystem != BuildSystem.emmake)
                    {
                        //copy dynlib
                        SafeFileCopy(Path.Combine(enginepath, chain.GetLibName(false, "WafaEngine", true)), Path.Combine(dev_pack_libs, chain.GetLibName(false, "WafaEngine", true)), true, logform);
                    }

                    //no static lib outside of emmake and msbuild
                    if (chain.buildsystem == BuildSystem.emmake || chain.buildsystem == BuildSystem.msbuild)
                    {
                        //copy staticlib
                        SafeFileCopy(Path.Combine(enginepath, chain.GetLibName(true, "WafaEngine", true)), Path.Combine(dev_pack_libs, chain.GetLibName(true, "WafaEngine", true)), true, logform);

                        if (chain.buildsystem == BuildSystem.emmake)
                        {
                            //copy astar worker
                            SafeFileCopy(Path.Combine(enginepath, "astar_worker.wasm"), Path.Combine(dev_pack_libs, "astar_worker.wasm"), true, logform);
                            SafeFileCopy(Path.Combine(enginepath, "astar_worker.js"), Path.Combine(dev_pack_libs, "astar_worker.js"), true, logform);
                        }

                    }

                    //execute post build operation on the generated file
                    foreach (PostBuildStep pbuild in chain.cmdPostbuilds)
                    {
                        string cmd = pbuild.getCmd(chain.ChainRoot,Path.Combine(dev_pack_libs,chain.GetLibName(false,"WafaEngine",true)));

                        if (string.IsNullOrEmpty(cmd))
                            continue;

                        ProcessStartInfo postbuild_info = new ProcessStartInfo(build_shell_engine, build_shell_op_engine + cmd  + build_shell_op2_engine);
                        postbuild_info.UseShellExecute = false;
                        postbuild_info.RedirectStandardOutput = true;
                        postbuild_info.WorkingDirectory = Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf(Path.DirectorySeparatorChar));
                        postbuild_info.CreateNoWindow = true;
                        postbuild_info.RedirectStandardError = true;
                        Process postbuild_process = Process.Start(postbuild_info);
                        postbuild_process.OutputDataReceived += (snd, earg) => logform.log(earg.Data, true);
                        postbuild_process.ErrorDataReceived += (snd2, earg2) => logform.log(earg2.Data, true, Color.Red);
                        postbuild_process.BeginOutputReadLine();
                        postbuild_process.BeginErrorReadLine();

                        while (!postbuild_process.HasExited)
                            Application.DoEvents();
                    }


                    string dev_pack_headers = Path.Combine("dev_pack", "include");
                    //remove existing headers
                    SafeDirDelete(dev_pack_headers, true, logform);

                    //copy engine headers
                    string engine_headers = Path.Combine("engine", "c", "include");

                    DirectoryCopy(engine_headers, dev_pack_headers, true);

                    if (chain.buildsystem == BuildSystem.msbuild)
                    {
                        dev_pack_headers = Path.Combine("dev_pack", "include-msvc");
                        SafeDirDelete(dev_pack_headers, true, logform);

                        engine_headers = Path.Combine("engine", "c", "include-msvc");
                        DirectoryCopy(engine_headers, dev_pack_headers, true);

                        SafeFileCopy(Path.Combine(enginepath, "WafaEngine.pdb"), Path.Combine(dev_pack_libs, "WafaEngine.pdb"), true, logform);
                    }

                    logform.log("Copy dependencies files to dev pack");
                    //copy generated dependencies to dev pack directory
                    foreach (string files_depend in Directory.GetFiles(outdirlib))
                    {
                        FileInfo inf_dep = new FileInfo(files_depend);

                        SafeFileCopy(files_depend, Path.Combine(dev_pack_libs, inf_dep.Name), true, logform);
                    }



                    //we need to build the editor code only once
                    if (!hasbuild_editor && !chain.DebugBuild && chain.HasEditor)
                    {
                        logform.log("Start building editor ", color: Color.Blue, font: BuildLog.TitleFont);



                        //build editor
                        string editor_path = Path.Combine("editor", "WafaEditorV2", "WafaEditorV2.csproj");

                        Toolchain.BuildCsharpProj(editor_path, logform);

                        logform.log("Editor built");

                        //build animation converter
                        string converter_path = Path.Combine("editor", "AnimationConverter", "AnimationConverter.sln");

                        Toolchain.BuildCsharpProj(converter_path, logform);

                        logform.log("Animation converter built");

                        //build native animation
                        string native_anim_path = Path.Combine("editor", "NativeAnimation", "NativeAnimation.vcxproj");

                        chain.BuildProj("config=Release",native_anim_path, Path.Combine("editor", "NativeAnimation"), "",logform);

                        logform.log("NativeAnimation built");

                        //build LMS
                        string lms_path = Path.Combine("editor", "LMSA", "LMS.csproj");

                        Toolchain.BuildCsharpProj(lms_path, logform);

                        logform.log("LMS built");

                        //build testor
                        string testor_path = Path.Combine("editor", "WafaTestor", "WafaTestor.csproj");

                        Toolchain.BuildCsharpProj(testor_path, logform);

                        logform.log("Testor built");

                        logform.log("Copy editor files");

                        //copy editor
                        string relpath = Path.Combine(new string[] { "editor", "WafaEditorV2", "bin", "Release" });
                        string destpath = Path.Combine(new string[] { "dev_pack", "wafaeditor_v2" });

                        string resxpath = Path.Combine(new string[] { "editor", "WafaEditorV2", "Resources" });

                        //create editor output folder if needed
                        if(!Directory.Exists(destpath))
                        {
                            Directory.CreateDirectory(destpath);
                        }


                        SafeFileCopy(Path.Combine(relpath, "WafaEditorV2.exe"), Path.Combine(destpath, "WafaEditorV2.exe"), true, logform);
                        SafeFileCopy(Path.Combine(resxpath, "config.lua"), Path.Combine(destpath, "config.lua"), true, logform);
                        SafeFileCopy(Path.Combine(outdirlib, chain.GetLibName(false, "galefile")), Path.Combine(destpath, chain.GetLibName(false, "galefile")), true, logform);
                        SafeFileCopy(Path.Combine(outdirlib, chain.GetLibName(false, "glew32")), Path.Combine(destpath, chain.GetLibName(false, "glew32")), true, logform);
                        SafeFileCopy(Path.Combine(outdirlib, chain.GetLibName(false, "glfw3")), Path.Combine(destpath, chain.GetLibName(false, "glfw3")), true, logform);
                        SafeFileCopy(Path.Combine(outdirlib, chain.GetLibName(false, "libsndfile")), Path.Combine(destpath, chain.GetLibName(false, "libsndfile")), true, logform);
						SafeFileCopy(Path.Combine(outdirlib, chain.GetLibName(false, "chipmunk")), Path.Combine(destpath, chain.GetLibName(false, "chipmunk")), true, logform);
                        SafeFileCopy(Path.Combine(outdirlib, chain.GetLibName(false, "sqlite3")), Path.Combine(destpath, chain.GetLibName(false, "sqlite3")), true, logform);
						SafeFileCopy(Path.Combine(resxpath, "locale.db"), Path.Combine(destpath, "locale.db"), true, logform);
                        SafeFileCopy(Path.Combine(resxpath, "save.db"), Path.Combine(destpath, "save.db"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "protobuf-net.dll"), Path.Combine(destpath, "protobuf-net.dll"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "protobuf-net.Core.dll"), Path.Combine(destpath, "protobuf-net.Core.dll"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "System.Buffers.dll"), Path.Combine(destpath, "System.Buffers.dll"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "System.Collections.Immutable.dll"), Path.Combine(destpath, "System.Collections.Immutable.dll"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "System.Memory.dll"), Path.Combine(destpath, "System.Memory.dll"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "System.Numerics.Vectors.dll"), Path.Combine(destpath, "System.Numerics.Vectors.dll"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "System.Runtime.CompilerServices.Unsafe.dll"), Path.Combine(destpath, "System.Runtime.CompilerServices.Unsafe.dll"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "System.ServiceModel.Primitives.dll"), Path.Combine(destpath, "System.ServiceModel.Primitives.dll"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "System.Data.SQLite.dll"), Path.Combine(destpath, "System.Data.SQLite.dll"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "WafaEditorV2.exe.config"), Path.Combine(destpath, "WafaEditorV2.exe.config"), true, logform);
                        SafeFileCopy(Path.Combine(enginepath, chain.GetLibName(false, "WafaEngine")), Path.Combine(destpath, chain.GetLibName(false, "WafaEngine")), true, logform);
                        SafeFileCopy(Path.Combine(enginepath, "WafaEngine.map"), Path.Combine(destpath, "WafaEngine.map"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "ICSharpCode.SharpZipLib.dll"), Path.Combine(destpath, "ICSharpCode.SharpZipLib.dll"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "SpriteSheetLibrary.dll"), Path.Combine(destpath, "SpriteSheetLibrary.dll"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "AssetsImport.dll"), Path.Combine(destpath, "AssetsImport.dll"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "Magick.NET.Core.dll"), Path.Combine(destpath, "Magick.NET.Core.dll"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "Magick.NET-Q8-AnyCPU.dll"), Path.Combine(destpath, "Magick.NET-Q8-AnyCPU.dll"), true, logform);

                        //copy animation converter
                        relpath = Path.Combine(new string[] { "editor", "AnimationConverter", "bin", "Release" });
                        destpath = Path.Combine(new string[] { "dev_pack", "animation_converter" });

                        if(!Directory.Exists(destpath))
                        {
                            Directory.CreateDirectory(destpath);
                        }

                        SafeFileCopy(Path.Combine(relpath, "AnimationConverter.exe"), Path.Combine(destpath, "AnimationConverter.exe"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "AnimationConverter.exe.config"), Path.Combine(destpath, "AnimationConverter.exe.config"), true, logform);
                        SafeFileCopy(Path.Combine(outdirlib, chain.GetLibName(false, "galefile")), Path.Combine(destpath, chain.GetLibName(false, "galefile")), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "protobuf-net.dll"), Path.Combine(destpath, "protobuf-net.dll"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "protobuf-net.Core.dll"), Path.Combine(destpath, "protobuf-net.Core.dll"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "System.Buffers.dll"), Path.Combine(destpath, "System.Buffers.dll"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "System.Collections.Immutable.dll"), Path.Combine(destpath, "System.Collections.Immutable.dll"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "System.Memory.dll"), Path.Combine(destpath, "System.Memory.dll"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "System.Numerics.Vectors.dll"), Path.Combine(destpath, "System.Numerics.Vectors.dll"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "System.Runtime.CompilerServices.Unsafe.dll"), Path.Combine(destpath, "System.Runtime.CompilerServices.Unsafe.dll"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "System.ServiceModel.Primitives.dll"), Path.Combine(destpath, "System.ServiceModel.Primitives.dll"), true, logform);
                        
                        
                        //copy testor
                        relpath = Path.Combine(new string[] { "editor", "WafaTestor", "bin", "Release" });
                        destpath = Path.Combine(new string[] { "dev_pack", "testor" });
                        resxpath = Path.Combine(new string[] { "editor", "WafaTestor", "Resources" });

                        if (!Directory.Exists(destpath))
                        {
                            Directory.CreateDirectory(destpath);
                        }

                        SafeFileCopy(Path.Combine(relpath, "WafaTestor.exe"), Path.Combine(destpath, "WafaTestor.exe"), true, logform);
                        SafeFileCopy(Path.Combine(outdirlib, chain.GetLibName(false, "galefile")), Path.Combine(destpath, chain.GetLibName(false, "galefile")), true, logform);
                        SafeFileCopy(Path.Combine(outdirlib, chain.GetLibName(false, "glew32")), Path.Combine(destpath, chain.GetLibName(false, "glew32")), true, logform);
                        SafeFileCopy(Path.Combine(outdirlib, chain.GetLibName(false, "glfw3")), Path.Combine(destpath, chain.GetLibName(false, "glfw3")), true, logform);
                        SafeFileCopy(Path.Combine(outdirlib, chain.GetLibName(false, "libsndfile")), Path.Combine(destpath, chain.GetLibName(false, "libsndfile")), true, logform);
                        SafeFileCopy(Path.Combine(outdirlib, chain.GetLibName(false, "chipmunk")), Path.Combine(destpath, chain.GetLibName(false, "chipmunk")), true, logform);
                        SafeFileCopy(Path.Combine(outdirlib, chain.GetLibName(false, "sqlite3")), Path.Combine(destpath, chain.GetLibName(false, "sqlite3")), true, logform);
                        SafeFileCopy(Path.Combine(resxpath, "locale.db"), Path.Combine(destpath, "locale.db"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "protobuf-net.dll"), Path.Combine(destpath, "protobuf-net.dll"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "protobuf-net.Core.dll"), Path.Combine(destpath, "protobuf-net.Core.dll"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "System.Buffers.dll"), Path.Combine(destpath, "System.Buffers.dll"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "System.Collections.Immutable.dll"), Path.Combine(destpath, "System.Collections.Immutable.dll"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "System.Memory.dll"), Path.Combine(destpath, "System.Memory.dll"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "System.Numerics.Vectors.dll"), Path.Combine(destpath, "System.Numerics.Vectors.dll"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "System.Runtime.CompilerServices.Unsafe.dll"), Path.Combine(destpath, "System.Runtime.CompilerServices.Unsafe.dll"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "System.ServiceModel.Primitives.dll"), Path.Combine(destpath, "System.ServiceModel.Primitives.dll"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "MoonSharp.Interpreter.dll"), Path.Combine(destpath, "MoonSharp.Interpreter.dll"), true, logform);
                        SafeFileCopy(Path.Combine(enginepath, chain.GetLibName(false, "WafaEngine")), Path.Combine(destpath, chain.GetLibName(false, "WafaEngine")), true, logform);
                        SafeFileCopy(Path.Combine(enginepath, "WafaEngine.map"), Path.Combine(destpath, "WafaEngine.map"), true, logform);

                        //copy LMS
                        relpath = Path.Combine(new string[] { "editor", "LMSA", "bin", "Release" });
                        destpath = Path.Combine(new string[] { "dev_pack", "LMS" });

                        if (!Directory.Exists(destpath))
                        {
                            Directory.CreateDirectory(destpath);
                        }

                        SafeFileCopy(Path.Combine(relpath, "LMS.exe"), Path.Combine(destpath, "LMS.exe"), true, logform);

                        SafeFileCopy(Path.Combine(outdirlib, chain.GetLibName(false, "glew32")), Path.Combine(destpath, chain.GetLibName(false, "glew32")), true, logform);
                        SafeFileCopy(Path.Combine(outdirlib, chain.GetLibName(false, "glfw3")), Path.Combine(destpath, chain.GetLibName(false, "glfw3")), true, logform);
                        SafeFileCopy(Path.Combine(outdirlib, chain.GetLibName(false, "libsndfile")), Path.Combine(destpath, chain.GetLibName(false, "libsndfile")), true, logform);
                        SafeFileCopy(Path.Combine(outdirlib, chain.GetLibName(false, "chipmunk")), Path.Combine(destpath, chain.GetLibName(false, "chipmunk")), true, logform);
                        SafeFileCopy(Path.Combine(outdirlib, chain.GetLibName(false, "sqlite3")), Path.Combine(destpath, chain.GetLibName(false, "sqlite3")), true, logform);

                        SafeFileCopy(Path.Combine(relpath, "AssetsImport.dll"), Path.Combine(destpath, "AssetsImport.dll"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "Magick.NET.Core.dll"), Path.Combine(destpath, "Magick.NET.Core.dll"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "Magick.NET-Q8-AnyCPU.dll"), Path.Combine(destpath, "Magick.NET-Q8-AnyCPU.dll"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "protobuf-net.dll"), Path.Combine(destpath, "protobuf-net.dll"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "protobuf-net.Core.dll"), Path.Combine(destpath, "protobuf-net.Core.dll"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "System.Buffers.dll"), Path.Combine(destpath, "System.Buffers.dll"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "System.Collections.Immutable.dll"), Path.Combine(destpath, "System.Collections.Immutable.dll"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "System.Memory.dll"), Path.Combine(destpath, "System.Memory.dll"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "System.Numerics.Vectors.dll"), Path.Combine(destpath, "System.Numerics.Vectors.dll"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "System.Runtime.CompilerServices.Unsafe.dll"), Path.Combine(destpath, "System.Runtime.CompilerServices.Unsafe.dll"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "System.Runtime.CompilerServices.Unsafe.dll"), Path.Combine(destpath, "System.Runtime.CompilerServices.Unsafe.dll"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "NativeAnimation.dll"), Path.Combine(destpath, "NativeAnimation.dll"), true, logform);
                        SafeFileCopy(Path.Combine(enginepath, chain.GetLibName(false, "WafaEngine")), Path.Combine(destpath, chain.GetLibName(false, "WafaEngine")), true, logform);
                        SafeFileCopy(Path.Combine(enginepath, "WafaEngine.map"), Path.Combine(destpath, "WafaEngine.map"), true, logform);
                        SafeFileCopy(Path.Combine(relpath, "BatchConvertLms.py"), Path.Combine(destpath, "BatchConvertLms.py"), true, logform);

                        hasbuild_editor = true;

                    }


                }
				}


                if (!chkDependOnly.Checked && !chkcopyonly.Checked)
				{
                    logform.log("Update dev pack", color: Color.Blue, font: BuildLog.TitleFont);
                    //finally update template code and copy dependencies includes
                    IAsyncResult include_copy_result = this.BeginInvoke(new MethodInvoker(() =>
                    {
                        DirectoryCopy(outdirinclude, Path.Combine("dev_pack", "depends_inc"), true);
                    }));

                    while (!include_copy_result.IsCompleted)
                        Application.DoEvents();

                    string template_path = Path.Combine(new string[] { "client", "c", "WafaDummyGame" });
                    string template_dev_pack = Path.Combine(new string[] { "dev_pack", "game_template", "dummygame" });

                    SafeFileCopy(Path.Combine(template_path, "Gamemain.c"), Path.Combine(template_dev_pack, "Gamemain.c"), true, logform);
                    SafeFileCopy(Path.Combine(template_path, "Gamemain.h"), Path.Combine(template_dev_pack, "Gamemain.h"), true, logform);
                    SafeFileCopy(Path.Combine(template_path, "game_physics.c"), Path.Combine(template_dev_pack, "game_physics.c"), true, logform);
                    SafeFileCopy(Path.Combine(template_path, "game_physics.h"), Path.Combine(template_dev_pack, "game_physics.h"), true, logform);
                    SafeFileCopy(Path.Combine(template_path, "game_export.h"), Path.Combine(template_dev_pack, "game_export.h"), true, logform);
                    SafeFileCopy(Path.Combine(template_path, "game_struct.h"), Path.Combine(template_dev_pack, "game_struct.h"), true, logform);
                    SafeFileCopy(Path.Combine(template_path, "script_funcs.c"), Path.Combine(template_dev_pack, "script_funcs.c"), true, logform);
                    SafeFileCopy(Path.Combine(template_path, "script_funcs.h"), Path.Combine(template_dev_pack, "script_funcs.h"), true, logform);
                    SafeFileCopy(Path.Combine(template_path, "common_include_game.h"), Path.Combine(template_dev_pack, "common_include_game.h"), true, logform);



                    //check if dev pack template build correctly
                    if (chkDevPack.Checked)
                    {
                        logform.log("Check template", color: Color.Blue, font: BuildLog.TitleFont);

                        //totally non cross platform code for now, replace with cmake generation of template and building, execute it for each selected toolchain

                        ProcessStartInfo test_template = new ProcessStartInfo("cmd.exe", "/c MSBuild dev_pack/game_template/game_template.sln /p:Configuration=Release /p:Platform=Win32 /p:bin\tmp");
                        test_template.UseShellExecute = false;
                        test_template.RedirectStandardOutput = true;
                        //startinfo2_engine.WorkingDirectory = Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf(Path.DirectorySeparatorChar));
                        test_template.CreateNoWindow = true;
                        test_template.RedirectStandardError = true;
                        Process proc_test_template = Process.Start(test_template);
                        proc_test_template.OutputDataReceived += (snd, earg) => logform.log(earg.Data, true);
                        proc_test_template.ErrorDataReceived += (snd2, earg2) => logform.log(earg2.Data, true, Color.Red);
                        proc_test_template.BeginOutputReadLine();
                        proc_test_template.BeginErrorReadLine();

                        while (!proc_test_template.HasExited)
                            Application.DoEvents();

                        //delete temporary files
                        SafeDirDelete(Path.Combine(new string[] { "dev_pack", "game_template", "bin", "tmp" }), true, logform);
                        SafeDirDelete(Path.Combine(new string[] { "dev_pack", "game_template", "Emscripten" }), true, logform);
                        SafeDirDelete(Path.Combine(new string[] { "dev_pack", "game_template", "dummygame", "bin" }), true, logform);
                        SafeDirDelete(Path.Combine(new string[] { "dev_pack", "game_template", "dummygame", "Emscripten" }), true, logform);
                        SafeDirDelete(Path.Combine(new string[] { "dev_pack", "game_template", "dummygame", "Release" }), true, logform);
                        SafeDirDelete(Path.Combine(new string[] { "dev_pack", "game_template", "Release" }), true, logform);

                    }

                    logform.log("Copy documentation", color: Color.Blue, font: BuildLog.TitleFont);
                    //copy documentation
                    string doc_dest = Path.Combine("dev_pack", "doc_wafa");

                    if (!Directory.Exists(doc_dest))
                        Directory.CreateDirectory(doc_dest);

                    SafeFileCopy("api_lua.txt", Path.Combine(doc_dest, "api_lua.txt"), true, logform);
                    SafeFileCopy("baselib.lua", Path.Combine(doc_dest, "baselib.lua"), true, logform);
                    SafeFileCopy("config_doc.txt", Path.Combine(doc_dest, "config_doc.txt"), true, logform);
                    SafeFileCopy("doc.txt", Path.Combine(doc_dest, "doc.txt"), true, logform);
                    SafeFileCopy("lua_entitycreation_doc.txt", Path.Combine(doc_dest, "lua_entitycreation_doc.txt"), true, logform);
                    SafeFileCopy("shader_macro.txt", Path.Combine(doc_dest, "shader_macro.txt"), true, logform);
                    SafeFileCopy("lights_values_doc.txt", Path.Combine(doc_dest, "lights_values_doc.txt"), true, logform);



                    logform.log("Zip dev pack", color: Color.Blue, font: BuildLog.TitleFont);
                    //zipping dev pack
                    string zip_file = "dev_pack.zip";

                    if (File.Exists(zip_file))
                        File.Delete(zip_file);

                    List<string> pack_folder = new List<string>()
                    {
                      "dev_pack"
                    };

                    Task ziptask = Task.Run(() => { ZipUtils.CreateArchive(zip_file, pack_folder, new List<string>()); });

                    while (ziptask.Status == TaskStatus.Running)
                    {
                        Application.DoEvents();
                    }

            }

            if (!string.IsNullOrEmpty(txtFolderCopyTo.Text))
            {
                if (!Directory.Exists(txtFolderCopyTo.Text))
                {
                    logform.log("can't copy to game root folder, folder doesn't exist !");
                }
                else
                {
                    this.CopyDevpackToGameRoot(txtFolderCopyTo.Text, chaintocopy,logform);
                    logform.log("dev pack copied to game root");
                }
            }
            else
            {
                if(chkcopyonly.Checked)
                {
                    logform.log("can't copy to game root folder, folder path is empty !");
                }
            }


            logform.log("Finished!", color: Color.Blue, font: BuildLog.TitleFont);
        }

        void dependclient_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if(logform != null)
                logform.log("Finished downloading source code");
        }

        void dependclient_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            if (logform != null)
                logform.logfixed(e.BytesReceived+" Bytes downloaded...");

        }

        private void btnDependencies_Click(object sender, EventArgs e)
        {
            DependenciesOptions options_form = new DependenciesOptions(depend_options);

            options_form.ShowDialog();
        }

        private void chkBuilddepend_CheckedChanged(object sender, EventArgs e)
        {
            btnDependencies.Enabled = chkBuilddepend.Enabled;
        }

        private void btnCopyto_Click(object sender, EventArgs e)
        {
            DialogResult copytofolder = fbCopyto.ShowDialog();

            if(copytofolder == DialogResult.OK)
            {
                txtFolderCopyTo.Text = fbCopyto.SelectedPath;
                Settings.Default.lastcopytofolder = txtFolderCopyTo.Text;
                Settings.Default.Save();
            }
        }
    }
}
