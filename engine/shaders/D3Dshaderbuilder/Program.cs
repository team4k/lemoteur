﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PBInterface;
using ProtoBuf;
using System.IO;
using System.Diagnostics;

namespace D3Dshaderbuilder
{
    class Program
    {
        static void Main(string[] args)
        {
            #if DEBUG
                args = new string[1];
                args[0] = "vertex_shader.hlsl";
            #endif
            //parse the hlsl file in input and output a shaddef file

            if (args.Length == 0)
            {
                Console.WriteLine("Please specify a .hlsl file to parse");
                return;
            }

            string outdir = string.Empty;

            if (args.Length == 2)
            {
                outdir = args[1];
            }

            string file_path = args[0];

            D3Dshaderconstantbuffer constantbuffer = null;

            Console.Out.WriteLine("Start parsing shader {0}...", file_path);


            string content = File.ReadAllText(file_path);

            int cbufferpos = content.IndexOf("cbuffer");

            if (cbufferpos != -1)
            {
                Console.Out.WriteLine("Parsing constant buffer...");

                constantbuffer = new D3Dshaderconstantbuffer();

                int start =  content.IndexOf(' ', cbufferpos);
                int end = content.IndexOf('{',start);
                constantbuffer.name = content.Substring(start, end - start).Trim();

                string param_string = content.Substring(end + 1, content.IndexOf('}', end) - (end + 1));

                param_string = param_string.Trim();
                string[] param_array = param_string.Split(new char[]{';'});

                foreach (string param in param_array)
                {
                    if (string.IsNullOrEmpty(param))
                        continue;

                    D3Dshaderparameter oparam = new D3Dshaderparameter();
                    oparam.name = param.Substring(param.IndexOf(' ')).Trim();

                    string type_param = param.Substring(0,param.IndexOf(' ')).Trim().ToLower();

                    switch (type_param)
                    {
                        case "float4x4":
                            oparam.type_param = (int)D3Dshadertype.MATRIX4X4;
                            break;
                        case "float4":
                            oparam.type_param = (int)D3Dshadertype.FLOAT4;
                            break;
                        case "float3":
                            oparam.type_param = (int)D3Dshadertype.FLOAT3;
                            break;
                        case "float2":
                            oparam.type_param = (int)D3Dshadertype.FLOAT2;
                            break;
                        case "float":
                            oparam.type_param = (int)D3Dshadertype.FLOAT;
                            break;

                    }

                    constantbuffer.param_list.Add(oparam);
                }
            }

            int layoutpos = content.IndexOf("main(");
            D3Dshaderlayout shaderlayout = null;

            if (layoutpos != -1 && constantbuffer != null)
            {
                Console.Out.WriteLine("Parsing shader layout...");

                shaderlayout = new D3Dshaderlayout();


                int start_param = content.IndexOf('(', layoutpos);

                string param_string = content.Substring(start_param + 1, content.IndexOf(')', start_param) - (start_param + 1));

                param_string = param_string.Trim();
                string[] param_array = param_string.Split(new char[] { ',' });

                foreach (string param in param_array)
                {
                    if (string.IsNullOrEmpty(param))
                        continue;

                    D3Dshaderparameter oparam = new D3Dshaderparameter();
                    int start = param.IndexOf(' ');
                    int end = param.IndexOf(':');
                    oparam.name = param.Substring(start, end - start).Trim();

                    string type_param = param.Substring(0, param.IndexOf(' ')).Trim().ToLower();

                    switch (type_param)
                    {
                        case "float4x4":
                            oparam.type_param = (int)D3Dshadertype.MATRIX4X4;
                            break;
                        case "float4":
                            oparam.type_param = (int)D3Dshadertype.FLOAT4;
                            break;
                        case "float3":
                            oparam.type_param = (int)D3Dshadertype.FLOAT3;
                            break;
                        case "float2":
                            oparam.type_param = (int)D3Dshadertype.FLOAT2;
                            break;
                        case "float":
                            oparam.type_param = (int)D3Dshadertype.FLOAT;
                            break;

                    }

                    string type_semantic = param.Substring(end+1).Trim().ToUpper();

                    switch (type_semantic)
                    {
                        case "POSITION":
                            oparam.semantic = (int)D3Dshadersemantic.POSITION;
                            break;
                        case "TEXCOORD":
                            oparam.semantic = (int)D3Dshadersemantic.TEXCOORD;
                            break;
                        case "NORMAL":
                            oparam.semantic = (int)D3Dshadersemantic.NORMAL;
                            break;
                        case "COLOR":
                            oparam.semantic = (int)D3Dshadersemantic.COLOR;
                            break;
                    }

                    shaderlayout.param_list.Add(oparam);
                }
            }

            //build shader
             FileInfo finfo = new FileInfo(file_path);

             Console.Out.WriteLine("Compiling shader...");

             string shad_name = finfo.Name.Substring(0, finfo.Name.LastIndexOf('.'));

             string compilation_format = "ps_4_0_level_9_3";

             if (constantbuffer != null)
                 compilation_format = "vs_4_0_level_9_3";

             ProcessStartInfo startinfo = new ProcessStartInfo("cmd.exe", string.Format(@"/c fxc.exe /E main /Fo {3}{0}.shad /T {1} /nologo {2}", shad_name, compilation_format, file_path, outdir));
            startinfo.UseShellExecute = false;
            startinfo.RedirectStandardOutput = true;
            Process build_proc = Process.Start(startinfo);
            build_proc.OutputDataReceived += (sender, e) => Console.WriteLine(e.Data);
            build_proc.BeginOutputReadLine();
            build_proc.WaitForExit();

            if (constantbuffer != null && shaderlayout != null)
            {
                D3Dshaderinfo shader_info = new D3Dshaderinfo();
                shader_info.shader_name = shad_name;
                shader_info.constant_def = constantbuffer;
                shader_info.layout_def = shaderlayout;


                using (FileStream shad_stream = File.Create(outdir +  shad_name + ".shaddef"))
                    ProtoBuf.Serializer.Serialize<D3Dshaderinfo>(shad_stream, shader_info);
            }

             #if DEBUG
                Console.In.ReadLine();
             #endif

        }
    }
}
