cbuffer ModelViewProjectionConstantBuffer
{
	float4x4 WVP;
	float4 inColor;
}

struct VertexShaderOutput
{
	float4 pos : SV_POSITION;
	float4 color : COLOR;
};

VertexShaderOutput main(float2 inPos : POSITION)
{
	VertexShaderOutput output;
	output.pos = mul(float4(inPos,0.0,1.0),WVP);
	output.color = inColor;
    return output;
}

