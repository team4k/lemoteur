static const float3 light_direction = float3(0.0, -0.816497, -0.408248);
static const float4 light_diffuse = float4(1.0, 1.0, 1.0, 0.0);
static const float4 light_ambient = float4(0.2, 0.2, 0.2, 1.0);

struct PixelShaderInput
{
	float4 pos : SV_POSITION;
	float4 color : COLOR;
	float4 frag_normals : NORMAL;
};


float4 main(PixelShaderInput input) : SV_TARGET
{
     float3 mv_light_direction = (float4(light_direction, 0.0)).xyz;
     float3 normal = normalize(input.frag_normals);
     float3 reflection = reflect(mv_light_direction, normal);

    float4 frag_diffuse = input.color;
    float4 diffuse_factor = max(-dot(normal, mv_light_direction), 0.0) * light_diffuse;
    float4 ambient_diffuse_factor= diffuse_factor + light_ambient;
    
    return ambient_diffuse_factor * frag_diffuse;
}
