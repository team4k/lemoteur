cbuffer ModelViewProjectionConstantBuffer
{
	float4x4 WVP;
    float4x4 W;
    float4x4 V;
    float4x4 P;
	float4 inColor;
}

struct VertexShaderOutput
{
	float4 pos : SV_POSITION;
	float4 color : COLOR;
	float4 frag_normals : NORMAL;
};

VertexShaderOutput main(float3 inPos : POSITION,float3 inNormals : NORMAL)
{
	VertexShaderOutput output;
	output.pos = mul(float4(inPos,1.0),WVP);
	output.color = inColor;
	output.frag_normals = mul(float4(inNormals,1.0),W * V);
    return output;
}
