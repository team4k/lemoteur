cbuffer ModelViewProjectionConstantBuffer
{
	float4x4 WVP;
	float4 inColor;
}

struct VertexShaderOutput
{
	float4 pos : SV_POSITION;
	float4 color : COLOR;
	float2 texcoord :TEXCOORD;
};

VertexShaderOutput main(float2 inPos : POSITION,float2 inTex : TEXCOORD)
{
	VertexShaderOutput output;
	output.pos = mul(float4(inPos,0.0,1.0),WVP);
	output.color = inColor;
	output.texcoord = inTex;
    return output;
}
