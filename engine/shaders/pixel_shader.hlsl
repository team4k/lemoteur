Texture2D sprite_texture;

SamplerState textureSampler;

struct PixelShaderInput
{
	float4 pos : SV_POSITION;
	float4 color : COLOR;
	float2 texcoord : TEXCOORD;
};



float4 main(PixelShaderInput input) : SV_TARGET
{
	return sprite_texture.Sample(textureSampler,input.texcoord) * input.color;
}
