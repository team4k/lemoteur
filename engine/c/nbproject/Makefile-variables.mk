#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debugx32 configuration
CND_PLATFORM_Debugx32=GNU-Linux-x86
CND_ARTIFACT_DIR_Debugx32=dist/Debugx32/GNU-Linux-x86
CND_ARTIFACT_NAME_Debugx32=libwafaengine.so
CND_ARTIFACT_PATH_Debugx32=dist/Debugx32/GNU-Linux-x86/libwafaengine.so
CND_PACKAGE_DIR_Debugx32=dist/Debugx32/GNU-Linux-x86/package
CND_PACKAGE_NAME_Debugx32=libc.so.tar
CND_PACKAGE_PATH_Debugx32=dist/Debugx32/GNU-Linux-x86/package/libc.so.tar
# Debugx64 configuration
CND_PLATFORM_Debugx64=GNU-Linux-x86
CND_ARTIFACT_DIR_Debugx64=dist/Debugx64/GNU-Linux-x86
CND_ARTIFACT_NAME_Debugx64=libwafaengine.so
CND_ARTIFACT_PATH_Debugx64=dist/Debugx64/GNU-Linux-x86/libwafaengine.so
CND_PACKAGE_DIR_Debugx64=dist/Debugx64/GNU-Linux-x86/package
CND_PACKAGE_NAME_Debugx64=libc.so.tar
CND_PACKAGE_PATH_Debugx64=dist/Debugx64/GNU-Linux-x86/package/libc.so.tar
# Releasex32 configuration
CND_PLATFORM_Releasex32=GNU-Linux-x86
CND_ARTIFACT_DIR_Releasex32=dist/Releasex32/GNU-Linux-x86
CND_ARTIFACT_NAME_Releasex32=libwafaengine.so
CND_ARTIFACT_PATH_Releasex32=dist/Releasex32/GNU-Linux-x86/libwafaengine.so
CND_PACKAGE_DIR_Releasex32=dist/Releasex32/GNU-Linux-x86/package
CND_PACKAGE_NAME_Releasex32=libc.so.tar
CND_PACKAGE_PATH_Releasex32=dist/Releasex32/GNU-Linux-x86/package/libc.so.tar
# Releasex64 configuration
CND_PLATFORM_Releasex64=GNU-Linux-x86
CND_ARTIFACT_DIR_Releasex64=dist/Releasex64/GNU-Linux-x86
CND_ARTIFACT_NAME_Releasex64=libwafaengine.so
CND_ARTIFACT_PATH_Releasex64=dist/Releasex64/GNU-Linux-x86/libwafaengine.so
CND_PACKAGE_DIR_Releasex64=dist/Releasex64/GNU-Linux-x86/package
CND_PACKAGE_NAME_Releasex64=libc.so.tar
CND_PACKAGE_PATH_Releasex64=dist/Releasex64/GNU-Linux-x86/package/libc.so.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
