#if defined(_WIN32)
	#include <windows.h>
	
	#if defined(WINAPI_FAMILY) && (WINAPI_FAMILY == WINAPI_FAMILY_PHONE_APP)
		#include <thread>
	#endif

#elif defined(EMSCRIPTEN)
	#include <emscripten.h>
#else
	#include <pthread.h>
#endif

#include <inttypes.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>
#include <math.h>
#include <Engine.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Threading/thread_func.h>

#include <Debug/Logprint.h>


#include <Utils/hashmap.h>
#include <Pathfinding/AStar.h>
#include <Pathfinding/lombric.h>
#include <Pathfinding/pathfind_worker.h>



#ifdef EMSCRIPTEN
static void callback_worker(char *data, int size, void *arg) 
{
	pathfind_worker_t* pathwork_obj = (pathfind_worker_t*)arg;

	//extract data 
	//format request id(char 128) solution size (int) solution (solutionsize * sizeof(int))
	char* request_id = &data[0];

	//get request object

	pathfind_result_t* result =  (pathfind_result_t*)hashmap_getvalue(&pathwork_obj->request_array,request_id);

	if(result->path_computed)
		return;

	int32_t sol_size = 0;

	memcpy(&sol_size,&data[128],sizeof(int32_t));

	int32_t flags_result = 0;

	memcpy(&flags_result, &data[128 + sizeof(int32_t)], sizeof(int32_t));

	if(sol_size > 0)
	{
		result->path_result = (int32_t*)wf_malloc(sol_size * sizeof(int32_t));
		memcpy(result->path_result,&data[128 + (sizeof(int32_t) * 2)],sol_size * sizeof(int32_t));
		result->path_result_size = sol_size;
	}

	result->flagsresult = flags_result;
	result->path_computed = wtrue;
}
#endif

#if defined(_WIN32)
DWORD WINAPI pathfind_dowork(void* data) {
#else
void* pathfind_dowork(void* data) {
#endif
	pathfind_worker_t* pathwork_obj = (pathfind_worker_t*)data;

	thread_cond_init(&pathwork_obj->thread_cond);

	#ifndef EMSCRIPTEN
	while(1) {
	#endif
		uint32_t req_index = 0;
		uint32_t req_count = 0;

		volatile uint32_t t_value = 0;

		atomic_set_value(&pathwork_obj->thread_state,WORKER_UNSAFE);

		atomic_set_value(&pathwork_obj->state,PATHFIND_PENDING);

		wbool sleepthread = wtrue;

		for(pathfind_result_t* result = (pathfind_result_t*)hashmap_iterate(&pathwork_obj->request_array,&req_index,&req_count);result != NULL;result = (pathfind_result_t*)hashmap_iterate(&pathwork_obj->request_array,&req_index,&req_count))
		{
			atomic_set_value(&pathwork_obj->thread_state,WORKER_SAFE);

			#ifdef EMSCRIPTEN
			if(!result->result_send)
			{
				//update grid data
				memcpy(&pathwork_obj->worker_data[sizeof(int32_t) * 2], pathwork_obj->grid, (pathwork_obj->grid_width * pathwork_obj->grid_height) * sizeof(int32_t));

				//set cell_start / end value
				memcpy(&pathwork_obj->worker_data[pathwork_obj->grid_size],&result->cell_start,sizeof(int32_t));
				memcpy(&pathwork_obj->worker_data[pathwork_obj->grid_size + sizeof(int32_t)],&result->cell_goal,sizeof(int32_t));
				memcpy(&pathwork_obj->worker_data[pathwork_obj->grid_size + (sizeof(int32_t) * 2)],&result->result_id[0],128);
				memcpy(&pathwork_obj->worker_data[pathwork_obj->grid_size + (sizeof(int32_t) * 2) + 128], &result->flags, sizeof(int32_t));

				if (pathwork_obj->algo == astar_jps) 
				{
					emscripten_call_worker(pathwork_obj->thread, "worker_astar", pathwork_obj->worker_data, pathwork_obj->worker_data_size, callback_worker, pathwork_obj);
				}
				else if (pathwork_obj->algo == lombric)
				{
					emscripten_call_worker(pathwork_obj->thread, "worker_lombric", pathwork_obj->worker_data, pathwork_obj->worker_data_size, callback_worker, pathwork_obj);
				}

				result->result_send = wtrue;
			}
			#else
			if(!result->path_computed)
			{
				pathfind_compute_t pathfunc = NULL;
				int primary_value = NO_COLLISION;
				int secondary_value = COLLISION;

				if (result->algo == astar_jps)
				{
					pathfunc = astar_compute;
					
				}
				else if (result->algo == lombric)
				{
					pathfunc = lombric_compute;

					if ((result->flags & LOMBRIC_MOVETHROUGHROCK) > 0) {
						secondary_value = (1 << COLLISION) | (1 << COLLISION_EDITABLE);
					}

					if ((result->flags & LOMBRIC_NOSAFEZONE) == 0) {
						primary_value = COLLISION_EDITABLE_2;
					}
				}

				volatile uint32_t s_value = 0;

				atomic_set_value(&s_value, pathwork_obj->outside_signal);

				if (s_value == PATHFIND_RERUN)
				{
					atomic_set_value(&pathwork_obj->outside_signal, NO_SIGNAL);
					s_value = NO_SIGNAL;
				}

				if (pathfunc != NULL) {
					pathfind_response_t response = pathfunc(pathwork_obj->grid, &result->path_result_size, pathwork_obj->grid_width, pathwork_obj->grid_height, result->cell_start, result->cell_goal, primary_value, secondary_value, result->flags);
					result->path_result = response.pathresult;
					result->flagsresult = response.flagsresult;
				}


				atomic_set_value(&s_value, pathwork_obj->outside_signal);

				while (s_value == PATHFIND_GRIDCHANGE)
				{
					atomic_set_value(&s_value, pathwork_obj->outside_signal);
				}

				//run again the pathfind algo, since the grid changed
				if (s_value == PATHFIND_RERUN)
				{
					result->path_result_size = 0;

					if (result->path_result)
					{
						wf_free(result->path_result);
						result->path_result = NULL;
					}

					atomic_set_value(&pathwork_obj->outside_signal,NO_SIGNAL);
					sleepthread = wfalse;
				}
				else
				{
					result->path_computed = wtrue;
				}
			}
			#endif

			atomic_set_value(&t_value,pathwork_obj->thread_state);

			while(t_value == WORKER_ADDINGELEMENTS)
			{
				atomic_set_value(&t_value,pathwork_obj->thread_state);
			}
		
			if(t_value == WORKER_ELEMENTS_ADDED)
			{
				req_index = req_count = 0;
			}

			atomic_set_value(&pathwork_obj->thread_state,WORKER_UNSAFE);
		}

		#ifdef EMSCRIPTEN
			if(emscripten_get_worker_queue_size(pathwork_obj->thread) == 0)
				atomic_set_value(&pathwork_obj->state,PATHFIND_PROCESSED);
		#else
			atomic_set_value(&pathwork_obj->state,PATHFIND_PROCESSED);
		#endif

		atomic_set_value(&pathwork_obj->thread_state,WORKER_SAFE);

		if (sleepthread)
		{
			thread_cond_wait(&pathwork_obj->thread_cond);//make the thread sleep and wait for the next element to add
		}
		

#ifndef EMSCRIPTEN
	}
#endif


	return NULL;
}


static void pathfind_createthread(pathfind_worker_t* const pathwork_obj)
{
	#if defined(_WIN32)
		#if defined(WINAPI_FAMILY) && (WINAPI_FAMILY == WINAPI_FAMILY_PHONE_APP)
			if (pathwork_obj->thread)
				delete pathwork_obj->thread;
		
			pathwork_obj->thread = new std::thread(pathfind_dowork, pathwork_obj);

		#else
		if(pathwork_obj->thread)
			CloseHandle(pathwork_obj->thread);

		pathwork_obj->thread = CreateThread(NULL, 0, pathfind_dowork, pathwork_obj, 0, NULL);

		#endif	

		if(!pathwork_obj->thread)
		{
			logprint("Can't create pathfind thread!");
		}		
	#elif defined(EMSCRIPTEN)
		if(pathwork_obj->thread)
			emscripten_destroy_worker(pathwork_obj->thread);

		pathwork_obj->thread = emscripten_create_worker("astar_worker.js");

	#else
		if(pathwork_obj->thread)
			wf_free(pathwork_obj->thread);

		pathwork_obj->thread = (pthread_t*)wf_malloc(sizeof(pthread_t));
		memset(pathwork_obj->thread,0,sizeof(pthread_t));

		if(pthread_create(pathwork_obj->thread,NULL,pathfind_dowork,pathwork_obj) != 0)
		{
			logprint("Can't create pathfind thread!");
		}
	#endif

	atomic_set_value(&pathwork_obj->state,PATHFIND_PENDING);
}


void pathfind_init(pathfind_worker_t* const pathwork_obj,int32_t request_max,const int32_t* grid, int32_t grid_width, int32_t grid_height)
{

	hashmap_initalloc(&pathwork_obj->request_array,request_max,sizeof(pathfind_result_t),DEFAULT_POOL_REQUEST);
	pathwork_obj->thread = 0;
	pathwork_obj->initialized = wtrue;

	#ifdef EMSCRIPTEN
		//initialise the worker data (the data size is fixed)
		//format : grid width ,grid height, grid data, then cellstart , cell_goal, request id
		pathwork_obj->grid_size = ((grid_width * grid_height) * sizeof(int32_t)) + (sizeof(int32_t) * 2);
		pathwork_obj->worker_data_size = (pathwork_obj->grid_size + (sizeof(int32_t) * 2) + 128 + sizeof(int32_t));
		pathwork_obj->worker_data = (char*)wf_malloc(pathwork_obj->worker_data_size);
		memset(pathwork_obj->worker_data,0,pathwork_obj->worker_data_size);

		//copy grid data
		memcpy(pathwork_obj->worker_data,&grid_width,sizeof(int32_t));
		memcpy(&pathwork_obj->worker_data[sizeof(int32_t)],&grid_height,sizeof(int32_t));
		memcpy(&pathwork_obj->worker_data[sizeof(int32_t) * 2],grid,(grid_width * grid_height) * sizeof(int32_t));
		memset(pathwork_obj->custom_worker,0,256);

	#endif

	atomic_set_value(&pathwork_obj->state,PATHFIND_NOTSTARTED);
	atomic_set_value(&pathwork_obj->thread_state,WORKER_SAFE);
	atomic_set_value(&pathwork_obj->outside_signal, NO_SIGNAL);

	pathwork_obj->grid = grid;
	pathwork_obj->grid_width = grid_width;
	pathwork_obj->grid_height = grid_height;
	pathwork_obj->algo = astar_jps;
}

void pathfind_setalgo(pathfind_worker_t* const pathwork_obj, pathfind_algo algo)
{
	pathwork_obj->algo = algo;
}

void pathfind_startgridchange(pathfind_worker_t* const pathwork_obj)
{
	atomic_set_value(&pathwork_obj->outside_signal, PATHFIND_GRIDCHANGE);
}

void pathfind_endgridchange(pathfind_worker_t* const pathwork_obj)
{
	atomic_set_value(&pathwork_obj->outside_signal, PATHFIND_RERUN);
}

void pathfind_updategrid(pathfind_worker_t* const pathwork_obj,const int32_t* grid,int32_t grid_width, int32_t grid_height)
{
	if(!pathwork_obj->initialized)
	{
		logprint("can't update the grid of a non initialized pathfind_worker!");
		return;
	}

	#ifdef EMSCRIPTEN

		//initialise the worker data (the data size is fixed)
		//format : grid width ,grid height, grid data, then cellstart , cell_goal, request id
		pathwork_obj->grid_size = ((grid_width * grid_height) * sizeof(int32_t)) + (sizeof(int32_t) * 2);
		pathwork_obj->worker_data_size = (pathwork_obj->grid_size + (sizeof(int32_t) * 2) + 128 + sizeof(int32_t));
		pathwork_obj->worker_data = (char*)wf_realloc(pathwork_obj->worker_data,pathwork_obj->worker_data_size);
		memset(pathwork_obj->worker_data,0,pathwork_obj->worker_data_size);

		//copy grid data
		memcpy(pathwork_obj->worker_data,&grid_width,sizeof(int32_t));
		memcpy(&pathwork_obj->worker_data[sizeof(int32_t)],&grid_height,sizeof(int32_t));
		memcpy(&pathwork_obj->worker_data[sizeof(int32_t) * 2],grid,(grid_width * grid_height) * sizeof(int32_t));
	#endif

	pathwork_obj->grid = grid;
	pathwork_obj->grid_width = grid_width;
	pathwork_obj->grid_height = grid_height;

}

#ifdef EMSCRIPTEN
void pathfind_simupdate(pathfind_worker_t* const pathwork_obj)
{
	if(pathwork_obj->state == PATHFIND_PENDING)
		pathfind_dowork(pathwork_obj);
}
#endif


pathfind_result_t* pathfind_dorequest(pathfind_worker_t* const pathwork_obj, const char* request_id, int32_t cell_start, int32_t cell_goal, wbool recycle_request, int32_t flags)
{
	return pathfind_dorequest_withalgo(pathwork_obj,request_id,cell_start, cell_goal, recycle_request, flags, pathwork_obj->algo);
}

pathfind_result_t* pathfind_dorequest_withalgo(pathfind_worker_t* const pathwork_obj,const char* request_id, int32_t cell_start, int32_t cell_goal,wbool recycle_request, int32_t flags,pathfind_algo algo)
{
	volatile uint32_t s_value = 0;
	volatile uint32_t t_value = 0;

	atomic_set_value(&s_value,pathwork_obj->state);
	atomic_set_value(&t_value,pathwork_obj->thread_state);

	while(t_value == WORKER_UNSAFE)
		atomic_set_value(&t_value,pathwork_obj->thread_state);

	atomic_set_value(&pathwork_obj->thread_state,WORKER_ADDINGELEMENTS);

	pathfind_result_t* result =  NULL;
	
	if(!recycle_request)
		result = (pathfind_result_t*)hashmap_push(&pathwork_obj->request_array,request_id);
	else
	{
		result = (pathfind_result_t*)hashmap_getvalue(&pathwork_obj->request_array, request_id);

		//if the request we need to recycle is not completed yet, return
		if (!result->path_computed)
		{
			atomic_set_value(&pathwork_obj->thread_state, WORKER_SAFE);
			return result;
		}

		result->recycled = wfalse;

	}

	result->flags = flags;
	result->cell_start = cell_start;
	result->cell_goal = cell_goal;
	result->path_computed = wfalse;
	result->result_send = wfalse;

	result->algo = algo;

	result->path_result_size = 0;

	if(result->path_result)
	{
		wf_free(result->path_result);
		result->path_result = NULL;
	}


	w_strcpy(result->result_id,128,request_id);

	atomic_set_value(&pathwork_obj->thread_state, WORKER_ELEMENTS_ADDED);

	
	if(s_value == PATHFIND_NOTSTARTED)
		pathfind_createthread(pathwork_obj);
	else if(s_value == PATHFIND_PROCESSED)
	{
		#ifdef EMSCRIPTEN
			atomic_set_value(&pathwork_obj->state, PATHFIND_PENDING);
		#else
			thread_cond_wakeup(&pathwork_obj->thread_cond);
		#endif
	}

	return result;
}

void pathfind_freerequest(pathfind_worker_t* const pathwork_obj,const char* request_id)
{
	volatile uint32_t t_value = 0;
	atomic_set_value(&t_value,pathwork_obj->thread_state);

	volatile uint32_t s_value = 0;
	atomic_set_value(&s_value,pathwork_obj->state);

	while(t_value == WORKER_UNSAFE)
		atomic_set_value(&t_value,pathwork_obj->thread_state);
	     
	atomic_set_value(&pathwork_obj->thread_state,WORKER_ADDINGELEMENTS);

	pathfind_result_t* result = (pathfind_result_t*)hashmap_getvalue(&pathwork_obj->request_array,request_id);

	if(result->path_computed && result->path_result)
		wf_free(result->path_result);


	hashmap_remove(&pathwork_obj->request_array,request_id);

	atomic_set_value(&pathwork_obj->thread_state, WORKER_ELEMENTS_ADDED);

	if(s_value == PATHFIND_NOTSTARTED || s_value == PATHFIND_PROCESSED)
		atomic_set_value(&pathwork_obj->thread_state,WORKER_SAFE);

}

void pathfind_freeallrequest(pathfind_worker_t* const pathwork_obj)
{
	volatile uint32_t t_value = 0;
	atomic_set_value(&t_value,pathwork_obj->thread_state);

	volatile uint32_t s_value = 0;
	atomic_set_value(&s_value,pathwork_obj->state);

	while(t_value == WORKER_UNSAFE)
		atomic_set_value(&t_value,pathwork_obj->thread_state);
	     
	atomic_set_value(&pathwork_obj->thread_state,WORKER_ADDINGELEMENTS);

	uint32_t req_index = 0;
	uint32_t req_count = 0;


	for(pathfind_result_t* result = (pathfind_result_t*)hashmap_iterate(&pathwork_obj->request_array,&req_index,&req_count);result != NULL;result = (pathfind_result_t*)hashmap_iterate(&pathwork_obj->request_array,&req_index,&req_count))
		{
			if(result->path_computed && result->path_result)
				wf_free(result->path_result);
		}

	hashmap_recycle(&pathwork_obj->request_array);

	atomic_set_value(&pathwork_obj->thread_state, WORKER_ELEMENTS_ADDED);

	if(s_value == PATHFIND_NOTSTARTED || s_value == PATHFIND_PROCESSED)
		atomic_set_value(&pathwork_obj->thread_state,WORKER_SAFE);
}

void pathfind_free(pathfind_worker_t* pathwork_obj)
{
	hashmap_free(&pathwork_obj->request_array);

	#if defined(_WIN32)
		#if defined(WINAPI_FAMILY) && (WINAPI_FAMILY == WINAPI_FAMILY_PHONE_APP)
			if (pathwork_obj->thread)
				delete pathwork_obj->thread;
		#else
			if(pathwork_obj->thread)
				CloseHandle(pathwork_obj->thread);
		#endif

	#elif defined(EMSCRIPTEN)
		if(pathwork_obj->thread)
			emscripten_destroy_worker(pathwork_obj->thread);

		wf_free(pathwork_obj->worker_data);

	#else
		if(pathwork_obj->thread)
			wf_free(pathwork_obj->thread);
	#endif

	thread_cond_free(&pathwork_obj->thread_cond);

	pathwork_obj->thread = 0;
	pathwork_obj->initialized = wfalse;

}
