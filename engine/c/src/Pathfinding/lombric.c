#include <inttypes.h>
#include <time.h>
#include <stdio.h>

#if defined(_WIN32)
#include <Windows.h>
#elif defined(EMSCRIPTEN)
#include <emscripten.h>
#endif


#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <Engine.h>
#include <Base/types.h>
#include <Base/functions.h>
#include "Pathfinding/lombric.h"


#if defined EMSCRIPTEN && defined WITH_WORKER
void worker_lombric(char* data, int size)
{
	//extract data
	int32_t grid_width = 0;
	int32_t grid_height = 0;
	int32_t start = 0;
	int32_t end = 0;
	int32_t flags = 0;

	memcpy(&grid_width, &data[0], sizeof(int32_t));
	memcpy(&grid_height, &data[sizeof(int32_t)], sizeof(int32_t));

	int32_t* grid = (int32_t*)&data[sizeof(int32_t) * 2];

	memcpy(&start, &data[(sizeof(int32_t) * 2) + ((grid_width * grid_height) * sizeof(int32_t))], sizeof(int32_t));
	memcpy(&end, &data[(sizeof(int32_t) * 3) + ((grid_width * grid_height) * sizeof(int32_t))], sizeof(int32_t));

	char* request_id = &data[(sizeof(int32_t) * 4) + ((grid_width * grid_height) * sizeof(int32_t))];

	memcpy(&flags, &data[(sizeof(int32_t) * 4) + ((grid_width * grid_height) * sizeof(int32_t)) + 128], sizeof(int32_t));

	int secondary_value = COLLISION;

	if ((flags & LOMBRIC_MOVETHROUGHROCK) > 0) {
		secondary_value = (1 << COLLISION) | (1 << COLLISION_EDITABLE);
	}

	int primary_value = NO_COLLISION;

	if ((flags & LOMBRIC_NOSAFEZONE) == 0) {
		primary_value = COLLISION_EDITABLE_2;
	}

	int32_t path_length = 0;
	pathfind_response_t resp = lombric_compute(grid, &path_length, grid_width, grid_height, start, end, primary_value, secondary_value, flags);
	int32_t* path_result = resp.pathresult;

	size_t size_result = 128 + (sizeof(int32_t) * 2) + (path_length * sizeof(int32_t));
	char* result_data = (char*)wf_malloc(size_result);
	memset(result_data, 0, size_result);

	memcpy(&result_data[0], request_id, 128);
	memcpy(&result_data[128], &path_length, sizeof(int32_t));
	memcpy(&result_data[128 + sizeof(int32_t)], &resp.flagsresult, sizeof(int32_t));

	if (path_length > 0)
		memcpy(&result_data[128 + (sizeof(int32_t) * 2)], path_result, (path_length * sizeof(int32_t)));

	emscripten_worker_respond(result_data, size_result);
}
#endif


typedef struct pathfind_data {
	const int32_t* orig_grid;
	int32_t* grid;
	coord_lomb_t bounds;
	int32_t start;
	coord_lomb_t start_coord;
	int32_t goal;
	coord_lomb_t goal_coord;
	int32_t enterable_value;
	int32_t diggable_value;
	size_t maxpathsize;
	wbool can_stick_ceil;
	int32_t extra_dig_val;
	int32_t safe_zone_val;
	wbool prefer_move_up;
} pathfind_data_t;

typedef struct vector {
	coord_lomb_t dir;
	coord_lomb_t stick;
} vector_move_t;



static int32_t sign(int32_t val)
{
	return (val > 0) - (val < 0);
}

static int32_t peek_last_element(int32_t* path, int32_t* pathlen)
{
	if (pathlen == 0) {
		return -1;
	}

	return path[(*pathlen) - 1];
}

static wbool push_element(const pathfind_data_t* const data, int32_t* path, int32_t* pathlen, int32_t node)
{
	if ((size_t)(*pathlen) >= data->maxpathsize || (size_t)node >= data->maxpathsize || node < 0) {
		return wfalse;
	}

	path[(*pathlen)++] = node;

	//maj valeur collision dans la grille
	data->grid[node] = data->enterable_value;

	return wtrue;
}

static wbool pop_element(const pathfind_data_t* const data, int32_t* path, int32_t* pathlen)
{
	if ((*pathlen) <= 0) {
		return wfalse;
	}

	(*pathlen)--;

	//maj valeur collision dans la grille
	data->grid[path[(*pathlen)]] = data->orig_grid[path[(*pathlen)]];

	path[(*pathlen)] = 0;
	return wtrue;
}


static coord_lomb_t getCoord(coord_lomb_t bounds, int32_t pos)
{
	coord_lomb_t rv = { pos % bounds.x,  pos / bounds.x };
	return rv;
}

static coord_lomb_t coord(int x, int y)
{
	coord_lomb_t ncoord;
	ncoord.x = x;
	ncoord.y = y;
	return ncoord;
}

static vector_move_t vectormove(coord_lomb_t dir, coord_lomb_t stick)
{
	vector_move_t vector;
	vector.dir = dir;
	vector.stick = stick;
	return vector;
}

static int getcandidatefromdir(const pathfind_data_t* const data,coord_lomb_t dir, int32_t rootnode)
{
	return rootnode + (dir.x * 1) + (dir.y * data->bounds.x);
}

//une valeur de type rocher n'est creusable que de bas en haut
static W_INLINE wbool is_diggable(const int32_t grid_value, const int32_t diggable_value, const int32_t rock_value, const coord_lomb_t dig_dir)
{
	return (wbool)((rock_value != -1 && rock_value == grid_value && dig_dir.y == 1 && dig_dir.x == 0) || (grid_value == diggable_value));
}

static W_INLINE wbool is_enterable(const int32_t grid_value, const int32_t enterable_value, const int32_t safezone_value)
{
	return (wbool)(grid_value == enterable_value || (safezone_value != -1 && grid_value == safezone_value));
}

static W_INLINE wbool is_climbable(const int32_t grid_value, const int32_t enterable_value, const int32_t safezone_value)
{
	return (wbool)(grid_value != enterable_value && (safezone_value == -1 || grid_value != safezone_value));
}


static wbool on_ground(const pathfind_data_t* const data, int32_t node)
{
	if (node < data->bounds.x) {
		return wtrue;
	}

	return (wbool)(!is_enterable(data->grid[node - data->bounds.x], data->enterable_value,data->safe_zone_val));
}

static int len(coord_lomb_t coord)
{
	return coord.x * coord.x + coord.y * coord.y;
}




static int getnodeindirection(const pathfind_data_t* const data, int32_t rootnode, vector_move_t* move_dir,wbool forcechangedir)
{
	coord_lomb_t dir = move_dir->dir;

	int32_t candidate = getcandidatefromdir(data, dir, rootnode);
	coord_lomb_t candcoord = getCoord(data->bounds, candidate);

	coord_lomb_t divedir = move_dir->stick;

	coord_lomb_t diffcandidate = { abs(data->goal_coord.x - candcoord.x), abs(data->goal_coord.y - candcoord.y) };

	int32_t candidatedive = getcandidatefromdir(data, divedir, rootnode);
	coord_lomb_t divecoord = getCoord(data->bounds, candidatedive);
	coord_lomb_t diffdive = { abs(data->goal_coord.x - divecoord.x), abs(data->goal_coord.y - divecoord.y) };
	wbool stickceil = (wbool)(move_dir->stick.y == 1);

	//force le déplacement dans la direction demandée, vérifie juste que la case est vide, ou que l'on peut creuser
	if (forcechangedir) {
		if (is_diggable(data->grid[candidate], data->diggable_value, data->extra_dig_val, dir) || is_enterable(data->grid[candidate], data->enterable_value, data->safe_zone_val)) {
			return candidate;
		}
	}

	//vérif si notre case est libre
	if (!is_enterable(data->grid[candidate], data->enterable_value,data->safe_zone_val)) {
		
		coord_lomb_t nodigdir = dir;

		nodigdir.y = abs(nodigdir.x);
		nodigdir.x = -move_dir->stick.x;

		//cas particulier descente
		if (nodigdir.x == 0 && nodigdir.y == 0) {
			nodigdir.y = -1;
		}

		int candidatenodig = getcandidatefromdir(data, nodigdir, rootnode);
		coord_lomb_t nodigcoord = getCoord(data->bounds, candidatenodig);

		wbool nodigok = (wbool)((is_enterable(data->grid[candidatenodig], data->enterable_value, data->safe_zone_val) || is_diggable(data->grid[candidatenodig], data->diggable_value, data->extra_dig_val, nodigdir)));

		//on doit obligatoirement creuser si un bloc est devant nous et que l'on est accroché sur un des mur au cas où il est impossible de se déplacer au plafond
		if (!data->can_stick_ceil && move_dir->stick.x != 0 && !on_ground(data, candidatenodig)) {
			nodigok = wfalse;
		}

		//vérif si creuser nous rapproche du but
		if (is_diggable(data->grid[candidate],data->diggable_value, data->extra_dig_val, dir) && (on_ground(data,candidate) || move_dir->stick.y != -1)) {
			coord_lomb_t diffnodig = { abs(data->goal_coord.x - nodigcoord.x), abs(data->goal_coord.y - nodigcoord.y) };
			
			//comparaison entre creuser et escalader
			//si on bouge depuis le plafond, le déplacement dans le sens de la marche est toujours préféré au déplacement en bas
			if (len(diffnodig) < len(diffcandidate) && nodigok && (data->can_stick_ceil || dir.y != 1)) {
				candidate = candidatenodig;
				move_dir->stick = dir;

				if (stickceil) {
					move_dir->stick.x = -move_dir->stick.x;
				}
				
				move_dir->dir = nodigdir;
			}
			else if (candcoord.y > data->goal_coord.y && move_dir->dir.y > 0) {

				if (is_diggable(data->grid[candidatedive],data->diggable_value, data->extra_dig_val, divedir) && len(diffdive) < len(diffcandidate)) {
					candidate = candidatedive;
					move_dir->dir = move_dir->stick;
					move_dir->stick.x = 0;
					move_dir->stick.y = -1;
				}

			}
		}
		else {
			if(nodigok) {
				candidate = candidatenodig;
				move_dir->stick = dir;

				if (stickceil) {
					move_dir->stick.x = -move_dir->stick.x;
				}

				move_dir->dir = nodigdir;
			}
			else {
				//en principe ne peut pas arriver, mais si le cas se présente, retour arrière
				candidate = rootnode;
				move_dir->dir.x = -move_dir->dir.x;
				move_dir->dir.y = -move_dir->dir.y;
			}
			
		}

	}
	else {
		//vérif si on doit plonger
		if (candcoord.y > data->goal_coord.y && move_dir->dir.y > 0) {
			
			if (is_diggable(data->grid[candidatedive],data->diggable_value, data->extra_dig_val, divedir) && len(diffdive) < len(diffcandidate)) {
				candidate = candidatedive;
				move_dir->dir = move_dir->stick;
				move_dir->stick.x = 0;
				move_dir->stick.y = -1;
			}

		}

	}

	return candidate;
}

static wbool apply_corner_turn(const pathfind_data_t* const data, int32_t node, vector_move_t* move_dir)
{
	//vérif si stick à toujours une valeur adjacente
	if (move_dir->stick.y != -1) {
		int32_t sidenode = getcandidatefromdir(data, move_dir->stick, node);

		if (is_enterable(data->grid[sidenode],data->enterable_value,data->safe_zone_val)) {
			coord_lomb_t oldidr = move_dir->dir;
			move_dir->dir = move_dir->stick;

			move_dir->stick = coord(-oldidr.x, -oldidr.y);
			
			return wtrue;
		}
	}

	return wfalse;
}

//changement de direction du pathfind pour se rapporcher le mieux possible du but, si retourne true, on impose à la prochaine itération de pathfind de suivre obligatoirement le chemin
static wbool apply_follow_goal(const pathfind_data_t* const data, vector_move_t* move_dir, int32_t node,int32_t* path,int32_t* pathlen,wbool onstartrow,wbool forcechangedir)
{
	//en mode atteinte rapide du but, on cherche à aller dans la direction du but si les conditions le permettent
	coord_lomb_t coord = getCoord(data->bounds, node);
	int32_t xdiff = sign(data->goal_coord.x - coord.x);


	//modif direction en fonction de la position du but si on est au même niveau ou au dessus et que l'on se déplace horizontalement et que l'on ne se trouve par sur la ligne de départ
	if (move_dir->dir.x != 0 && xdiff != move_dir->dir.x && coord.y >= data->goal_coord.y && !onstartrow) {
		int32_t last_node = peek_last_element(path, pathlen);

		wbool gobackontrack = wfalse;

		if (last_node != -1 && !forcechangedir) {
			coord_lomb_t last_coord = getCoord(data->bounds, last_node);

			gobackontrack = (wbool)(last_coord.y == coord.y && ((last_coord.x < coord.x && xdiff < 0) || (last_coord.x > coord.x && xdiff > 0)));
		}

		if (!gobackontrack) {
			move_dir->dir.x = xdiff;
		}
		else if(coord.y != data->goal_coord.y) {
			int32_t downnode = node - data->bounds.x;

			coord_lomb_t gravitydir;
			gravitydir.x = 0;
			gravitydir.y = -1;

			if (is_enterable(data->grid[downnode],data->enterable_value,data->safe_zone_val) || is_diggable(data->grid[downnode], data->diggable_value, data->extra_dig_val, gravitydir)) {
				move_dir->dir.y = -1;
				move_dir->dir.x = 0;
			}
			
		}
		
	}
	else if (move_dir->dir.x == 0 && move_dir->dir.y == -1 && coord.y == data->goal_coord.y) {
		move_dir->dir.x = xdiff;
		move_dir->dir.y = 0;
	}

	//dans le cas où on est en dessous du but, et que l'on est sur une map majoritairement vide, on préfèrera monter que creuser
	if (data->prefer_move_up && data->goal_coord.y > coord.y)
	{
		//on doit avoir un mur pour pouvoir monter
		int32_t candidateindir = getcandidatefromdir(data, move_dir->dir, node);

		if (candidateindir >=0 && candidateindir < data->maxpathsize && is_climbable(data->grid[candidateindir], data->enterable_value ,data->safe_zone_val) && move_dir->dir.x != 0)
		{
			move_dir->stick.x = move_dir->dir.x;
			move_dir->stick.y = 0;

			move_dir->dir.x = 0;
			move_dir->dir.y = 1;

			return wtrue;
		}
	}

	//cas particulier accrochage au plafond impossible, si on est au dessus du but de 5 ligne et que l'on monte, retour mouvement à l'horizontal
	if (!data->can_stick_ceil && !onstartrow && coord.y >= data->goal_coord.y + 5 && move_dir->dir.y == 1 && move_dir->stick.x != 0) {
		move_dir->dir.x = move_dir->stick.x;
		move_dir->dir.y = 0;
		move_dir->stick.x = 0;
		move_dir->stick.y = -1;
		return wtrue;
	}

	return wfalse;
}

static int apply_gravity(const pathfind_data_t* const data, int32_t node, vector_move_t* move_dir, int32_t* path, int32_t* path_len)
{
	coord_lomb_t coordnode = getCoord(data->bounds, node);

	if (coordnode.x == data->goal_coord.x && coordnode.y > data->goal_coord.y) {

		push_element(data, path, path_len, node);
		int goalnode = node - data->bounds.x;
		coord_lomb_t gravitydir;
		gravitydir.x = 0;
		gravitydir.y = -1;

		while (goalnode != data->goal && (is_enterable(data->grid[goalnode], data->enterable_value, data->safe_zone_val) || is_diggable(data->grid[goalnode],data->diggable_value,data->extra_dig_val, gravitydir))) {
			push_element(data, path, path_len, goalnode);

			goalnode -= data->bounds.x;

			if (goalnode < data->bounds.x) {
				break;
			}
		}

		if (is_enterable(data->grid[goalnode], data->enterable_value, data->safe_zone_val) || is_diggable(data->grid[goalnode], data->diggable_value, data->extra_dig_val, gravitydir)) {
			return goalnode;
		}
		else {
			//we can't move down (no enterable value and no diggable value) move on the side
			return getcandidatefromdir(data, move_dir->dir, (goalnode + data->bounds.x));
		}

	}
	else if (move_dir->stick.y == -1 && !on_ground(data, node) && node != data->goal) {//gravité jusqu'a ce qu'on trouve un sol

		
		push_element(data, path, path_len, node);

		int32_t next_tile = getcandidatefromdir(data, move_dir->dir, node);
		
		//gestion dash, il y a un bloc adjacent
		if (on_ground(data, next_tile)) {
			move_dir->stick.x = 0;
			move_dir->stick.y = -1;
			return next_tile;
		}
	
		//un bloc est de l'autre coté, nous permettant de nous accrocher
		/*if (data->grid[next_tile] != data->enterable_value) {
			int sidenode = getcandidatefromdir(data, move_dir->stick, node);
			coord_lomb_t oldidr = move_dir->dir;
			move_dir->dir = move_dir->stick;
			move_dir->stick = coord(-oldidr.x,-oldidr.y);
			return sidenode;
		}*/
		

		int32_t gnode = node - data->bounds.x;

		while (is_enterable(data->grid[gnode], data->enterable_value, data->safe_zone_val)) {

			push_element(data, path, path_len, gnode);

			gnode -= data->bounds.x;

			if (gnode < data->bounds.x) {
				break;
			}
		}

		pop_element(data, path, path_len);

		return gnode + data->bounds.x;
	}

	return node;
}


//déplacement coté droit et coté gauche simultané, le premier chemin qui atteint le but est retourné
//en mode secondaire, nous pouvons avoir deux valeurs dans diggable_value, stocké non pas sous forme de drapeau binaire classique, mais sous
//forme l'index binaire dans la valeur int correspondant à la valeur des collisions à vérifier
//ex : 1 et 7 (COLLISION  et COLLISION_EDITABLE) = 130, c'est à dire l'index 1 et l'index 7 avec valeur = 1, nos valeur ne sont donc pas 2 et 128 comme avec un drapeau binaire classique mais 1 et 7
pathfind_response_t lombric_compute(const int32_t* grid,
	int32_t* solLength,
	int32_t boundX,
	int32_t boundY,
	int32_t start,
	int32_t end,
	int32_t enterable_value,
	int32_t diggable_value,
	int32_t flags
	)
{
	
	pathfind_response_t resp = { 0 };
	int32_t* winner = NULL;

	int32_t side1len = 0;
	int32_t side2len = 0;

	int32_t ground_value = -1;
	int32_t rock_value = -1;

	if ((wbool)((flags & LOMBRIC_MOVETHROUGHROCK) > 0)) {

		//décomposition de la valeur de dig en deux valeur, la première pour la terre et la seconde pour le rocher
		//10 = valeur de collision maximum
		for (int32_t idigval = 1; idigval < 10; idigval++) {
			if (TAKE_N_BITS_FROM(diggable_value, idigval, 1) == 1) {
				if (ground_value == -1) {
					ground_value = idigval;
				}
				else if (rock_value == -1) {
					rock_value = idigval;
				}
			}
		}
	}
	else {
		ground_value = diggable_value;
	}

	int32_t empty_value = -1;
	int32_t safezone_value = -1;

	if ((wbool)((flags & LOMBRIC_NOSAFEZONE) == 0)) {
		
		if (enterable_value != 0) {
			empty_value = 0;
			safezone_value = enterable_value;
		}

	}
	else {
		empty_value = enterable_value;
	}



	pathfind_data_t data;
	data.maxpathsize = (boundX*boundY);
	data.bounds = coord(boundX,boundY);
	data.orig_grid = grid;
	data.grid = NULL;
	data.enterable_value = empty_value;
	data.safe_zone_val = safezone_value;
	data.diggable_value = ground_value;
	data.start = start;
	data.start_coord = getCoord(data.bounds, start);
	data.goal = end;
	data.goal_coord = getCoord(data.bounds, end);
	data.can_stick_ceil = (wbool)((flags & LOMBRIC_NO_STICK_CEIL) == 0);
	data.extra_dig_val = rock_value;
	data.prefer_move_up = wfalse;
		//(wbool)(empty_stat > full_stat);
	

	int32_t* side1 = (int32_t*)wf_malloc(data.maxpathsize * sizeof(int32_t));
	int32_t* side2 = (int32_t*)wf_malloc(data.maxpathsize * sizeof(int32_t));
	

	int32_t* grid1 = (int32_t*)wf_malloc(data.maxpathsize * sizeof(int32_t));
	//copie de la grille car nous avons besoin de modifier les collisions pour prendre en compte les destructions de murs induite par le passage de notre algo
	w_memcpy(grid1, data.maxpathsize * sizeof(int32_t),grid, data.maxpathsize * sizeof(int32_t));

	int32_t* grid2 = (int32_t*)wf_malloc(data.maxpathsize * sizeof(int32_t));
	w_memcpy(grid2, data.maxpathsize * sizeof(int32_t), grid, data.maxpathsize * sizeof(int32_t));

	wbool goalreached = wfalse;

	int32_t side1node = start;
	int32_t side2node = start;

	vector_move_t side1move = { {1,0},{0,-1} };
	vector_move_t side2move = { { -1,0},{ 0,-1 } };

	data.grid = grid1;
	int32_t iterationlimit = boundX;
		
	int32_t citeration = 0;

	//application de la gravité dès le départ
	if (!on_ground(&data, start) && abs(start - end) > 1) {
		int newnode = apply_gravity(&data, start, &side1move, side1, &side1len);
		if (newnode == data.goal) {
			goalreached = wtrue;
			winner = side1;
			(*solLength) = side1len;
		}
		else {
			side1node = newnode;

			//départ du deuxième coté au même niveau que le premier coté
			apply_gravity(&data, start, &side2move, side2, &side2len);
			side2node = newnode;
		}
	}
	

	push_element(&data, side1, &side1len, side1node);
	push_element(&data, side2, &side2len, side2node);

	//les deux drapeaux sont incompatibles, on ne choisi que le drapeaux de gauche dans ce cas
	if ((flags & LOMBRIC_ONLY_LEFT) > 0 && (flags & LOMBRIC_ONLY_RIGHT) > 0) {
		printf("LOMBRIC_ONLY_LEFT and LOMBRIC_ONLY_RIGHT flags can't be put together, removing LOMBRIC_ONLY_RIGHT...");
		flags &= ~LOMBRIC_ONLY_RIGHT;
	}


	wbool doside1 = (wbool)((flags & LOMBRIC_ONLY_LEFT) == 0); //recherche à droite si le drapeau LOMBRIC_ONLY_LEFT n'est pas défini
	wbool doside2 = (wbool)((flags & LOMBRIC_ONLY_RIGHT) == 0);//recherche à gauche si le drapeau LOMBRIC_ONLY_RIGHT n'est pas défini

	wbool side1onstartrow = (wbool)(data.start_coord.y == data.goal_coord.y);
	wbool side2onstartrow = (wbool)(data.start_coord.y == data.goal_coord.y);
	wbool forcechangedirside1 = wfalse;
	wbool forcechangedirside2 = wfalse;

	wbool follow_goal = wtrue;

	char searchmode = 1; //1 = follow goal, no move up,2 = follow goal, move up, 3 = no follow goal, no move up, 4 = no follow goal, move up

	while (!goalreached)
	{
		//start node is same than end node, no pathfinding needed
		if (start == end)
		{
			break;
		}

		//première itération, recherche rapide, chemin pas plus long que la longueur de la grille
		//seconde itération, recherche complète, chemin pas plus long que (longueur * largeur)
		if (citeration >= iterationlimit)
		{
			searchmode++;

			if (searchmode <= 4)
			{
				citeration = 0;
				side1len = side2len = 1;
				side1node = side2node = start;
				side1move = vectormove(coord(1, 0), coord(0, -1));
				side2move = vectormove(coord(-1, 0), coord(0, -1));

				//réinitialisation des grilles
				w_memcpy(grid1, data.maxpathsize * sizeof(int32_t), grid, data.maxpathsize * sizeof(int32_t));
				w_memcpy(grid2, data.maxpathsize * sizeof(int32_t), grid, data.maxpathsize * sizeof(int32_t));

			}
			
			if (searchmode == 2)
			{
				follow_goal = wtrue;
				data.prefer_move_up = wtrue;
				iterationlimit = boundX * 3;
			}
			else if (searchmode == 3) //la recherche rapide ne donne aucun résultat, recherche complète sur la grille
			{
				follow_goal = wfalse;
				data.prefer_move_up = wfalse;
				iterationlimit = (boundX * boundY);
			}
			else if (searchmode == 4)
			{
				follow_goal = wfalse;
				data.prefer_move_up = wtrue;
				iterationlimit = (boundX * boundY);
			}
			else {
				break;
			}

			/*if (follow_goal) {
				
				follow_goal = wfalse;
				iterationlimit = (boundX * boundY);
				citeration = 0;
				side1len = side2len = 1;
				side1node = side2node = start;
				side1move = vectormove(coord( 1,0),coord( 0,-1 ) );
				side2move = vectormove(coord( -1,0),coord( 0,-1 ) );

				//on ne privilège plus la montée en mode complet
				
			}
			else {
				break;
			}*/
		}

		wbool side1reachgoal = wfalse;
		wbool side2reachgoal = wfalse;

		if (doside1)
		{
			data.grid = grid1;

			//application gravité sur la node de départ

			int32_t newnode = getnodeindirection(&data, side1node, &side1move, forcechangedirside1);

			newnode = apply_gravity(&data, newnode, &side1move, side1, &side1len);

			if (side1onstartrow && getCoord(data.bounds,newnode).y != data.start_coord.y) {
				side1onstartrow = wfalse;
			}


			if (!apply_corner_turn(&data, newnode, &side1move) && follow_goal) {
				forcechangedirside1 = apply_follow_goal(&data, &side1move, newnode,side1,&side1len, side1onstartrow, forcechangedirside1);
			}

			wbool resultside1 = wfalse;

			if (newnode != end) {
				resultside1 = push_element(&data, side1, &side1len, newnode);
				side1node = newnode;
			}
			else {
				side1reachgoal = wtrue;
			}

			if (!resultside1 && !side1reachgoal)
			{
				doside1 = wfalse;
			}
		}

		if (doside2)
		{
			data.grid = grid2;
			int32_t newnode = getnodeindirection(&data, side2node, &side2move, forcechangedirside2);

			newnode = apply_gravity(&data, newnode, &side2move, side2, &side2len);

			if (side2onstartrow && getCoord(data.bounds, newnode).y != data.start_coord.y) {
				side2onstartrow = wfalse;
			}

			if (!apply_corner_turn(&data, newnode, &side2move) && follow_goal) {
				forcechangedirside2 = apply_follow_goal(&data, &side2move, newnode,side2,&side2len, side2onstartrow, forcechangedirside2);
			}

			wbool resultside2 = wfalse;

			if (newnode != end) {
				resultside2 = push_element(&data, side2, &side2len, newnode);
				side2node = newnode;
			}
			else {
				side2reachgoal = wtrue;
			}

			if (!resultside2 && !side2reachgoal)
			{
				doside2 = wfalse;
			}
		}
		

		if (side1reachgoal && !side2reachgoal) {
			winner = side1;
			(*solLength) = side1len;
			goalreached = wtrue;
			resp.flagsresult = LOMBRIC_ONLY_RIGHT;
		}
		else if (side2reachgoal && !side1reachgoal) {
			winner = side2;
			(*solLength) = side2len;
			goalreached = wtrue;
			resp.flagsresult = LOMBRIC_ONLY_LEFT;
		}
		else if (side1reachgoal && side2reachgoal) {
			winner = (side1len < side2len) ? side1 : side2;
			(*solLength) = (side1len < side2len) ? side1len : side2len;
			resp.flagsresult = (side1len < side2len) ? LOMBRIC_ONLY_RIGHT : LOMBRIC_ONLY_LEFT;
			goalreached = wtrue;
		}


		citeration++;
	}

	if (winner != NULL) {
		push_element(&data, winner, solLength, end);
	}

	if (winner != side1)
	{
		wf_free(side1);
	}

	if (winner != side2)
	{
		wf_free(side2);
	}

	wf_free(grid1);
	wf_free(grid2);

	resp.pathresult = winner;

	return resp;

}