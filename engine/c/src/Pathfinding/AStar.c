#include <inttypes.h>
#include <time.h>

#if defined(_WIN32)
    #include <Windows.h>
#elif defined(EMSCRIPTEN)
	#include <emscripten.h>
#endif


#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <Engine.h>
#include <Base/types.h>
#include <Base/functions.h>
#include "Pathfinding/AStar.h"
#include "Pathfinding/IndexPriorityQueue.h"

#define ASTAR_TWO_ENTERABLE 1


// Distance metrics, you might want to change these to match your game mechanics

// Chebyshev distance metric for distance estimation by default
static double estimateDistance (coord_t start, coord_t end)
{
	return fmax ((float)abs (start.x - end.x), (float)abs (start.y - end.y));
}

// Since we only work on uniform-cost maps, this function only needs
// to see the coordinates, not the map itself.
// Euclidean geometry by default.
// Note that since we jump over points, we actually have to compute 
// the entire distance - despite the uniform cost we can't just collapse
// all costs to 1
static double preciseDistance (coord_t start, coord_t end)
{
	if (start.x - end.x != 0 && start.y - end.y != 0)
		return sqrt (pow ((double)(start.x - end.x), 2) + 
			     pow ((double)(start.y - end.y), 2)) ;
	else
		return abs (start.x - end.x) + abs (start.y - end.y);
}

// Below this point, not a lot that there should be much need to change!

typedef int32_t node;

typedef struct astar {
	const int32_t* grid;
	coord_t bounds;
	node start;
	node goal;
	queue *open;
	char *closed;
	double *gScores;
	node *cameFrom;
	int32_t*solutionLength;
	int32_t enterable_value;
	int32_t secondary_value;
	wbool secondary_mode;
} astar_t;

// The order of directions is: 
// N, NE, E, SE, S, SW, W, NW 
typedef unsigned char direction;
#define NO_DIRECTION 8
typedef unsigned char directionset;

// return and remove a direction from the set
// returns NO_DIRECTION if the set was empty
static direction nextDirectionInSet (directionset *dirs)
{
	for (int32_t i = 0; i < 8; i++) {
		char bit = 1 << i;
		if (*dirs & bit) {
			*dirs ^= bit;
			return i;
		}
	}
	return NO_DIRECTION;
}

static directionset addDirectionToSet (directionset dirs, direction dir)
{
	return dirs | 1 << dir;
}

/* Coordinates are represented either as pairs of an x-coordinate and
   y-coordinate, or map indexes, as appropriate. getIndex and getCoord
   convert between the representations. */
static int32_t getIndex (coord_t bounds, coord_t c)
{
	return c.x + c.y * bounds.x;
}

int32_t astar_getIndexByWidth (int32_t width, int32_t x, int32_t y)
{
	return x + y * width;
}

static coord_t getCoord (coord_t bounds, int32_t c)
{
	coord_t rv = { c % bounds.x, c / bounds.x };
	return rv;
}

void astar_getCoordByWidth (int32_t width, int32_t node, int32_t *x, int32_t *y)
{
	*x = node % width;
	*y = node / width;
}


// is this coordinate contained within the map bounds?
static int32_t contained (coord_t bounds, coord_t c)
{
	return c.x >= 0 && c.y >= 0 && c.x < bounds.x && c.y < bounds.y;
}

static int32_t directionIsDiagonal (direction dir)
{
	return (dir % 2) != 0;
}




// the coordinate one tile in the given direction
static coord_t adjustInDirection (coord_t c, int32_t dir)
{
	// we want to implement "rotation" - that is, for instance, we can
	// subtract 2 from the direction "north" and get "east"
	// C's modulo operator doesn't quite behave the right way to do this,
	// but for our purposes this kluge should be good enough
	coord_t ret = { -1, -1};

	switch ((dir + 65536) % 8) {
	case 0: 
		ret.x = c.x;
		ret.y = c.y + 1;
		return ret;
	case 1: 
		ret.x = c.x + 1;
		ret.y = c.y + 1;
		return ret;
	case 2: 
		ret.x = c.x + 1;
		ret.y = c.y;
		return ret;
	case 3: 
		ret.x = c.x + 1;
		ret.y = c.y - 1;
		return ret;
	case 4: 
		ret.x = c.x;
		ret.y = c.y - 1;
		return ret;
	case 5: 
		ret.x = c.x - 1;
		ret.y = c.y - 1;
		return ret;
	case 6: 
		ret.x = c.x -1;
		ret.y = c.y;
		return ret;
	case 7:
		ret.x = c.x -1;
		ret.y = c.y + 1;
		return ret;
	}
	return ret;
}

// is this coordinate within the map bounds, and also walkable?
static int32_t isEnterable (astar_t *astar, coord_t coord)
{
	node c_node = getIndex (astar->bounds, coord);

	int32_t  contain = contained (astar->bounds, coord);

	int32_t result = contain && (astar->grid[c_node] == astar->enterable_value);

	if(astar->secondary_mode && contain && !result)
		result =  (astar->grid[c_node] == astar->secondary_value);

	return result;
}

static int32_t checkNeighbourDiagonal(astar_t *astar, coord_t coord,int32_t dir)
{
	//check that neighbour tiles are enterable too
	if(directionIsDiagonal(dir))
	{

		coord_t nei_1 = adjustInDirection(coord,((dir + 5) % 8));

		coord_t nei_2 = adjustInDirection(coord,((dir + 3) % 8));

		return isEnterable(astar,nei_1) || isEnterable(astar,nei_2);

   }
   else
   {
		return 1;
   }
}

// logical implication operator
static int32_t implies (int32_t a, int32_t b)
{
	return a ? b : 1; 
}

/* Harabor's explanation of exactly how to determine when a cell has forced
   neighbours is a bit unclear IMO, but this is the best explanation I could
   figure out. I won't go through everything in the paper, just the extra
   insights above what I thought was immediately understandable that it took
   to actually implement this function.

   First, to introduce the problem, we're looking at the immedate neighbours
   of a cell on the grid, considering what tile we arrived from.

   ...  This is the basic situation we're looking at. Supposing the top left
   -X.  period is cell (0,0), we're coming in to cell (1, 1) from (0, 1).
   ...  

   ...  The other basic case, the diagonal case. All other cases are obviously
   .X.  derivable from these two cases by symmetry.
   /..

   The question is: Given that some tiles might have walls, *how many tiles
   are there that we can reach better by going through the center tile than
   any other way?* (for the horizontal case it's ok to only be able to reach
   them as well some other as through the center tile too)

   In case there are no obstructions, the answers are simple: In the horizontal
   or vertical case, the cell directly ahead; in the diagonal case, the three
   cells ahead.

   The paper is pretty abstract about what happens when there *are* 
   obstructions, but fortunately the abstraction seems to collapse into some
   fairly simple practical cases:

   123  Position 4 is a natural neighbour (according to the paper's terminology)
   -X4  so we don't need to count it. Positions 1, 2, 5 and 6 are accessible
   567  without going through the center tile. This leaves positions 3 and 7
   to be looked at.

   Considering position 3 (everything here also follows for 7 by symmetry):
   If 3 is obstructed, then it doesn't matter what's in position in 2.
   If 3 is free and 2 is obstructed, 3 is a forced neighbour.
   If 3 is free and 2 is free, 3 is pruned (not a forced neighbour)

   i.e. logically, 
   3 is not a forced neighbour iff (3 is obstructed) implies (2 is obstructed).

   Similar reasoning applies for the diagonal case, except with bigger angles.
   
 */
/*
static int32_t hasForcedNeighbours (astar_t *astar, coord_t coord, int32_t dir)
{
#define ENTERABLE(n) isEnterable (astar, \
	                          adjustInDirection (coord, dir + (n)))
	if (directionIsDiagonal (dir))
		return !implies (ENTERABLE (-2), ENTERABLE (-3)) ||
		       !implies (ENTERABLE (2), ENTERABLE (3));
	else 
		return !implies (ENTERABLE (-1), ENTERABLE (-2)) ||
		       !implies (ENTERABLE (1), ENTERABLE (2));
#undef ENTERABLE
}
*/
static directionset forcedNeighbours (astar_t *astar, 
				      coord_t coord, 
				      direction dir)
{
	if (dir == NO_DIRECTION)
		return 0;

	directionset dirs = 0;
#define ENTERABLE(n) isEnterable (astar, \
				  adjustInDirection (coord, (dir + (n)) % 8))
	if (directionIsDiagonal (dir)) {
		if (!implies (ENTERABLE (6), ENTERABLE (5))) //check if we can go to W relative to our actual direction, we need the W tile to be unobstructed and the SW tile to be obstructed
			dirs = addDirectionToSet (dirs, (dir + 6) % 8);
		if (!implies (ENTERABLE (2), ENTERABLE (3))) //check if we can go to E relative to our actual direction, we need the E tile to be unobstructed and the SE tile to be obstructed
			dirs = addDirectionToSet (dirs, (dir + 2) % 8);
	}
	else {
		if (!implies (ENTERABLE (7), ENTERABLE (6))) //check if we can go to NW relative to our actual direction, we need the NW tile to be unobstructed and the W tile to be obstructed
			dirs = addDirectionToSet (dirs, (dir + 7) % 8);
		if (!implies (ENTERABLE (1), ENTERABLE (2))) //check if we can go to NE relative to our actual direction, we need the NE tile to be unobstructed and the E tile to be obstructed
			dirs = addDirectionToSet (dirs, (dir + 1) % 8);
	}	
#undef ENTERABLE	
	return dirs;
}

static directionset naturalNeighbours (direction dir)
{
	if (dir == NO_DIRECTION)
		return 255;

	directionset dirs = 0;
	dirs = addDirectionToSet (dirs, dir);
	if (directionIsDiagonal (dir)) {
		dirs = addDirectionToSet (dirs, (dir + 1) % 8);
		dirs = addDirectionToSet (dirs, (dir + 7) % 8);
	}
	return dirs;
}

static void addToOpenSet (astar_t *astar,
			  int32_t node, 
			  int32_t nodeFrom)
{
	coord_t nodeCoord = getCoord (astar->bounds, node);
	coord_t nodeFromCoord = getCoord (astar->bounds, nodeFrom);

	if (!exists (astar->open, node)) {
		astar->cameFrom[node] = nodeFrom;
		astar->gScores[node] = astar->gScores[nodeFrom] + 
			preciseDistance (nodeFromCoord, nodeCoord);
		insert (astar->open, node, astar->gScores[node] + 
			estimateDistance (nodeCoord, 
					  getCoord (astar->bounds, astar->goal)));
	}
	else if (astar->gScores[node] > 
		 astar->gScores[nodeFrom] + 
		 preciseDistance (nodeFromCoord, nodeCoord)) {
		astar->cameFrom[node] = nodeFrom;
		int32_t oldGScore = (int32_t)astar->gScores[node];
		astar->gScores[node] = astar->gScores[nodeFrom] + 
			preciseDistance (nodeFromCoord, nodeCoord);
		double newPri = priorityOf (astar->open, node)
			- oldGScore
			+ astar->gScores[node];
		changePriority (astar->open, node, newPri);
	}	
}


// directly translated from "algorithm 2" in the paper
static int32_t jump (astar_t *astar, direction dir, int32_t start)
{
	coord_t coord = adjustInDirection (getCoord (astar->bounds, start), dir);
	int32_t node = getIndex (astar->bounds, coord);
	if (!isEnterable (astar, coord) || !checkNeighbourDiagonal(astar,coord,dir))
		return -1;

	if (node == astar->goal || 
	    forcedNeighbours (astar, coord, dir)) {
		return node;
	}

	if (directionIsDiagonal (dir)) {
		int32_t next = jump (astar, (dir + 7) % 8, node);
		if (next >= 0)
			return node;
		next = jump (astar, (dir + 1) % 8, node);
		if (next >= 0)
			return node;
	}
	return jump (astar, dir, node);
}

// path interpolation between jump points in here
static int32_t nextNodeInSolution (astar_t *astar,
			       int32_t *target,
			       int32_t node)
{
	coord_t c = getCoord (astar->bounds, node);
	coord_t cTarget = getCoord (astar->bounds, *target);

	if (c.x < cTarget.x) 
		c.x++;
	else if (c.x > cTarget.x)
		c.x--;

	if (c.y < cTarget.y) 
		c.y++;
	else if (c.y > cTarget.y)
		c.y--;

	node = getIndex (astar->bounds, c);

	if (node == *target)
		*target = astar->cameFrom[*target];

	return node;
}

// a bit more complex than the usual A* solution-recording method,
// due to the need to interpolate path chunks
static int32_t *recordSolution (astar_t *astar)
{
	int32_t rvLen = 1;
	*astar->solutionLength = 0;
	int32_t target = astar->goal;
	int32_t *rv = (int32_t*)malloc (rvLen * sizeof (int32_t));
	int32_t i = astar->goal;

	for (;;) {
		i = nextNodeInSolution (astar, &target, i);
		rv[*astar->solutionLength] = i;
		(*astar->solutionLength)++;
		if (*astar->solutionLength >= rvLen) {
			rvLen *= 2;
			rv = (int32_t*)realloc (rv, rvLen * sizeof (int32_t));
			if (!rv)
				return NULL;
		}
		if (i == astar->start)
			break;
	}

	//(*astar->solutionLength)--; // don't include the starting tile
	return rv;
}


static direction directionOfMove (coord_t to, coord_t from)
{
	if (from.x == to.x) {
		if (from.y == to.y)
			return -1;
		else if (from.y < to.y)
			return 0;
		else // from.y > to.y
			return 4;
	}
	else if (from.x < to.x) {
		if (from.y == to.y)
			return 2;
		else if (from.y < to.y)
			return 1;
		else // from.y > to.y
			return 3;
	}
	else { // from.x > to.x
		if (from.y == to.y)
			return 6;
		else if (from.y < to.y)
			return 7;
		else // from.y > to.y
			return 5;
	}

}

static direction directionWeCameFrom (astar_t *astar, int32_t node, int32_t nodeFrom)
{
	if (nodeFrom == -1)
		return NO_DIRECTION;

	return directionOfMove (getCoord (astar->bounds, node), 
				getCoord (astar->bounds, nodeFrom));
}

static int32_t init_astar_object (astar_t* astar, const int32_t* grid, int32_t *solLength, int32_t boundX, int32_t boundY, int32_t start, int32_t end,int32_t enterable_value,int32_t secondary_value,wbool secondary_mode)
{
	*solLength = -1;
	coord_t bounds = {boundX, boundY};

	int32_t size = bounds.x * bounds.y;

	if (start >= size || start < 0 || end >= size || end < 0)
		return 0;

	coord_t startCoord = getCoord (bounds, start);
	coord_t endCoord = getCoord (bounds, end);

	if (!contained (bounds, startCoord) || !contained (bounds, endCoord))
		return 0;

	astar->solutionLength = solLength;
	astar->bounds = bounds;
	astar->start = start;
	astar->goal = end;
	astar->grid = grid;
	astar->enterable_value = enterable_value;
	astar->secondary_value = secondary_value;
	astar->secondary_mode = secondary_mode;

	astar->open = createQueue();
	if (!astar->open)
		return 0;

	astar->closed = (char*)malloc (size);
	if (!astar->closed) {
		freeQueue (astar->open);
		return 0;
	}

	astar->gScores = (double*)malloc (size * sizeof (double));
	if (!astar->gScores) {
		freeQueue (astar->open);
		wf_free(astar->closed);
		return 0;
	}

	astar->cameFrom = (int*)malloc (size * sizeof (int));
	if (!astar->cameFrom) {
		freeQueue (astar->open);
		wf_free(astar->closed);
		wf_free(astar->gScores);
		return 0;
	}

	memset (astar->closed, 0, size);

	astar->gScores[start] = 0;
	astar->cameFrom[start] = -1;

	insert (astar->open, astar->start, estimateDistance (startCoord, endCoord));

	return 1;
}

#if defined EMSCRIPTEN && defined WITH_WORKER
	void worker_astar(char* data,int size)
	{
		//extract data
		int32_t grid_width = 0;
		int32_t grid_height = 0;
		int32_t start = 0;
		int32_t end = 0;
		int32_t flags = 0;

		memcpy(&grid_width,&data[0],sizeof(int32_t));
		memcpy(&grid_height,&data[sizeof(int32_t)],sizeof(int32_t));
			
		int32_t* grid = (int32_t*)&data[sizeof(int32_t) * 2];

		memcpy(&start,&data[(sizeof(int32_t) * 2) + ((grid_width * grid_height) * sizeof(int32_t))],sizeof(int32_t));
		memcpy(&end,&data[(sizeof(int32_t) * 3) + ((grid_width * grid_height) * sizeof(int32_t))],sizeof(int32_t));

		char* request_id = &data[(sizeof(int32_t) * 4) + ((grid_width * grid_height) * sizeof(int32_t))];

		memcpy(&flags, &data[(sizeof(int32_t) * 4) + ((grid_width * grid_height) * sizeof(int32_t)) + 128], sizeof(int32_t));

		wbool secondary_mode = (wbool)((flags & ASTAR_TWO_ENTERABLE) > 0);

		int32_t path_length = 0;
		pathfind_response_t resp = astar_compute(grid, &path_length, grid_width, grid_height, start, end, NO_COLLISION, COLLISION, secondary_mode);
		int32_t* path_result = resp.pathresult;

		size_t size_result = 128 + (sizeof(int32_t) * 2) + (path_length * sizeof(int32_t));
		char* result_data = (char*)wf_malloc(size_result);
		memset(result_data,0,size_result);

		memcpy(&result_data[0],request_id,128);
		memcpy(&result_data[128],&path_length,sizeof(int32_t));
		memcpy(&result_data[128 + sizeof(int32_t)], &resp.flagsresult, sizeof(int32_t));

		if(path_length > 0)
			memcpy(&result_data[128 + (sizeof(int32_t) * 2)],path_result,(path_length * sizeof(int32_t)));

		emscripten_worker_respond(result_data,size_result);
	}
#endif


pathfind_response_t astar_compute (const int32_t* grid,
		    int32_t *solLength, 
		    int32_t boundX, 
		    int32_t boundY, 
		    int32_t start, 
		    int32_t end,
			int32_t enterable_value,
			int32_t secondary_value,
			int32_t flags)
{
	astar_t astar;

	wbool secondary_mode = (wbool)((flags & ASTAR_TWO_ENTERABLE) > 0);
	pathfind_response_t resp = { 0 };

	if (!init_astar_object (&astar, grid, solLength, boundX, boundY, start, end,enterable_value,secondary_value,secondary_mode))
		return resp;

	coord_t bounds = {boundX, boundY};
	coord_t endCoord = getCoord (bounds, end);

	while (astar.open->size) {
		int32_t node = findMin (astar.open)->value; 
		coord_t nodeCoord = getCoord (bounds, node);
		if (nodeCoord.x == endCoord.x && nodeCoord.y == endCoord.y) {
			freeQueue (astar.open);
			wf_free(astar.closed);
			wf_free(astar.gScores);

			int32_t *rv = recordSolution (&astar);

			wf_free(astar.cameFrom);
			resp.pathresult = rv;
			return resp;
		}

		deleteMin (astar.open);
		astar.closed[node] = 1;

		direction from = directionWeCameFrom (&astar, 
						      node,
						      astar.cameFrom[node]);

		directionset dirs = 
			forcedNeighbours (&astar, nodeCoord, from) 
		      | naturalNeighbours (from);

		for (int32_t dir = nextDirectionInSet (&dirs); dir != NO_DIRECTION; dir = nextDirectionInSet (&dirs))
		{
			int32_t newNode = jump (&astar, dir, node);
			coord_t newCoord = getCoord (bounds, newNode);

			// this'll also bail out if jump() returned -1
			if (!contained (bounds, newCoord))
				continue;

			if (astar.closed[newNode])
				continue;
			
			addToOpenSet (&astar, newNode, node);

		}
	}
	freeQueue (astar.open);
	wf_free(astar.closed);
	wf_free(astar.gScores);
	wf_free(astar.cameFrom);

	return resp;
}



pathfind_response_t astar_unopt_compute (const int32_t* grid,
		    int32_t *solLength, 
		    int32_t boundX, 
		    int32_t boundY, 
		    int32_t start, 
		    int32_t end,
			int32_t enterable_value,
			int32_t secondary_value,
			int32_t flags)
{
	astar_t astar;

	wbool secondary_mode = (wbool)((flags & ASTAR_TWO_ENTERABLE) > 0);

	pathfind_response_t resp = { 0 };

	if (!init_astar_object (&astar, grid, solLength, boundX, boundY, start, end,enterable_value,secondary_value,secondary_mode))
		return resp;

	coord_t bounds = {boundX, boundY};
	coord_t endCoord = getCoord (bounds, end);

	while (astar.open->size) {
		int32_t node = findMin (astar.open)->value; 
		coord_t nodeCoord = getCoord (bounds, node);
		if (nodeCoord.x == endCoord.x && nodeCoord.y == endCoord.y) {
			freeQueue (astar.open);
			wf_free(astar.closed);
			wf_free(astar.gScores);

			int32_t *rv = recordSolution (&astar);

			wf_free(astar.cameFrom);
			resp.pathresult = rv;
			return resp;
		}

		deleteMin (astar.open);
		astar.closed[node] = 1;

		//only use 4 dir
		for (int32_t dir = 0; dir < 8; dir+=2)
		{
			coord_t newCoord = adjustInDirection (nodeCoord, dir);
			int32_t newNode = getIndex (bounds, newCoord);

			if (!contained (bounds, newCoord) || (grid[newNode] != astar.enterable_value && (astar.secondary_mode && grid[newNode] != astar.secondary_value)) || !checkNeighbourDiagonal(&astar,newCoord,dir))
				continue;

			if (astar.closed[newNode])
				continue;
			
			addToOpenSet (&astar, newNode, node);

		}
	}
	freeQueue (astar.open);
	wf_free(astar.closed);
	wf_free(astar.gScores);
	wf_free(astar.cameFrom);
	return resp;
}

