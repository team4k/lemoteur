#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <math.h>

#include <Engine.h>

#include <Render.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>
#include <Base/geom.h>

#include <Render/render_manager.h>

#include <Debug/geomdebug.h>

ColorA_t debug_color = {0.0,1.0,0.0,0.5};

void Debug_Drawedge(const edge_t* edge,ColorA_t color,render_manager_t* const render_mngr)
{
	  float vertices[] = { 
		  edge->start.x,
		  edge->start.y,

		  edge->end.x,
		  edge->end.y
	};

	render_push_matrix(render_mngr);

	render_set_color(render_mngr,color);

	render_start_draw_array(render_mngr,2,2,vertices,NULL);

	render_draw_line(render_mngr,0,2);

	render_end_draw_array(render_mngr,wfalse);

	render_pop_matrix(render_mngr);

	color.r = color.b = color.g = color.a = 1.0f;

	render_set_color(render_mngr,color);
}

void Debug_Drawpath(const Vector_t* const vector_array,size_t array_size,render_manager_t* const render_mngr,ColorA_t color)
{
	float* path_vertices = (float*)wf_calloc(array_size * 2,sizeof(float));

	for(uint32_t ivec = 0;ivec < array_size;ivec++)
	{
		path_vertices[(ivec*2)] = (float)vector_array[ivec].x;
		path_vertices[(ivec*2)+1] = (float)vector_array[ivec].y;
	}


	render_push_matrix(render_mngr);

	render_set_color(render_mngr,color);

	render_start_draw_array(render_mngr,2,array_size,path_vertices,NULL);

	render_draw_line(render_mngr,0,array_size);

	render_end_draw_array(render_mngr,wfalse);

	render_pop_matrix(render_mngr);

	color.r = color.b = color.g = color.a = 1.0f;

	render_set_color(render_mngr,color);

	wf_free(path_vertices);
}

void Debug_Drawshadowvolume(const shadow_volume_t* shadow_volume,ColorA_t color,render_manager_t* const render_mngr)
{
	float vertices[] = { 
		shadow_volume->point_a.x,
		shadow_volume->point_a.y,

		shadow_volume->point_b.x,
		shadow_volume->point_b.y,

		shadow_volume->point_c.x,
		shadow_volume->point_c.y,

		shadow_volume->point_c.x,
		shadow_volume->point_c.y,

		shadow_volume->point_d.x,
		shadow_volume->point_d.y,

		shadow_volume->point_a.x,
		shadow_volume->point_a.y
	};

	render_push_matrix(render_mngr);

	render_set_color(render_mngr,color);

	render_start_draw_array(render_mngr,2,6,vertices,NULL);

	render_draw_array(render_mngr,0,6);
	render_end_draw_array(render_mngr,wfalse);

	render_pop_matrix(render_mngr);

	color.r = color.b = color.g = color.a = 1.0f;

	render_set_color(render_mngr,color);
}

void vector_tostring(const Vector_t* vector,char* string,size_t size_string)
{
	w_sprintf(string, size_string,"x : %f y : %f ",vector->x,vector->y);
}


void rect_tostring(const Rect_t* rect,char* string,size_t size_string)
{
	w_sprintf(string,size_string,"width : %d height : %d ",rect->width,rect->height);
}
