#if defined(RENDERER_OPENGL) || defined(RENDERER_OPENGL_ES)

/* Copyright (c) 2007 Scott Lembcke
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <limits.h>
#include <string.h>
#include <inttypes.h>
#define __USE_MISC 1
#define _USE_MATH_DEFINES 1
#include <math.h>


#include <Render.h>



#include <Engine.h>
#include <Base/types.h>
#include <Base/functions.h>
#include <Base/geom.h>
#include <Base/Color.h>
#include <Render/render_manager.h>

#include <chipmunk/chipmunk_private.h>
#include <Physics/physics_utils.h>
#include <Physics/convert.h>
#include "Debug/ChipmunkDebugDraw.h"



/*
	IMPORTANT - READ ME!
	
	This file sets up a simple interface that the individual demos can use to get
	a Chipmunk space running and draw what's in it. In order to keep the Chipmunk
	examples clean and simple, they contain no graphics code. All drawing is done
	by accessing the Chipmunk structures at a very low level. It is NOT
	recommended to write a game or application this way as it does not scale
	beyond simple shape drawing and is very dependent on implementation details
	about Chipmunk which may change with little to no warning.
*/

const Color LINE_COLOR = {200.0f/255.0f, 210.0f/255.0f, 230.0f/255.0f, 1.0f};
const Color CONSTRAINT_COLOR = {0.0f, 0.75f, 0.0f, 1.0f};
const float SHAPE_ALPHA = 0.5f;

typedef struct { float x, y; } float2;
typedef struct { uint8_t r, g, b, a; } RGBA8;
typedef struct { float2 pos; float2 uv; float r; RGBA8 fill, outline; } Vertex;
typedef uint16_t Index;

static RGBA8 cp_to_rgba(cpSpaceDebugColor c) {
	RGBA8 r = { (uint8_t)(0xFF * c.r), (uint8_t)(0xFF * c.g), (uint8_t)(0xFF * c.b), (uint8_t)(0xFF * c.a) };
	return r;
}

static cpTransform ChipmunkDebugDrawVPMatrix;
static float ChipmunkDebugDrawPointLineScale = 1.0f;

static cpSpaceDebugColor Colors[] = {
	{ 0xb5 / 255.0f, 0x89 / 255.0f, 0x00 / 255.0f, SHAPE_ALPHA },
	{ 0xcb / 255.0f, 0x4b / 255.0f, 0x16 / 255.0f, SHAPE_ALPHA },
	{ 0xdc / 255.0f, 0x32 / 255.0f, 0x2f / 255.0f, SHAPE_ALPHA },
	{ 0xd3 / 255.0f, 0x36 / 255.0f, 0x82 / 255.0f, SHAPE_ALPHA },
	{ 0x6c / 255.0f, 0x71 / 255.0f, 0xc4 / 255.0f, SHAPE_ALPHA },
	{ 0x26 / 255.0f, 0x8b / 255.0f, 0xd2 / 255.0f, SHAPE_ALPHA },
	{ 0x2a / 255.0f, 0xa1 / 255.0f, 0x98 / 255.0f, SHAPE_ALPHA },
	{ 0x85 / 255.0f, 0x99 / 255.0f, 0x00 / 255.0f, SHAPE_ALPHA },
};


cpSpaceDebugColor ColorForShape(cpShape *shape, cpDataPointer data)
{
	if (cpShapeGetSensor(shape)) {
		return LAColor(1.0f, 0.1f);
	}
	else {
		cpBody *body = cpShapeGetBody(shape);

		if (cpBodyIsSleeping(body)) {
			return RGBAColor(0x58 / 255.0f, 0x6e / 255.0f, 0x75 / 255.0f, SHAPE_ALPHA);
		}
		else if (body->sleeping.idleTime > shape->space->sleepTimeThreshold) {
			return RGBAColor(0x93 / 255.0f, 0xa1 / 255.0f, 0xa1 / 255.0f, SHAPE_ALPHA);
		}
		else {
			uint32_t val = (uint32_t)shape->hashid;

			// scramble the bits up using Robert Jenkins' 32 bit integer hash function
			val = (val + 0x7ed55d16) + (val << 12);
			val = (val ^ 0xc761c23c) ^ (val >> 19);
			val = (val + 0x165667b1) + (val << 5);
			val = (val + 0xd3a2646c) ^ (val << 9);
			val = (val + 0xfd7046c5) + (val << 3);
			val = (val ^ 0xb55a4f09) ^ (val >> 16);
			return Colors[val & 0x7];
		}
	}
}

// Meh, just max out 16 bit index size.
#define VERTEX_MAX (64*1024)
#define INDEX_MAX (4*VERTEX_MAX)

static size_t VertexCount, IndexCount;

static VBO_ID VertexBuffer, IndexBuffer;

static Vertex Vertexes[VERTEX_MAX];
static Index Indexes[INDEX_MAX];

static SHADERPROGRAM_ID chipmunk_prog = NULL;


void ChipmunkDebugDrawInit(render_manager_t* const render_mngr)
{
	VertexBuffer = render_gen_buffer(render_mngr, Vertexes, VERTEX_MAX * sizeof(Vertex), wtrue, wtrue);
	IndexBuffer = render_gen_bufferindices(render_mngr, Indexes, INDEX_MAX * sizeof(Index), wtrue, wtrue);

	chipmunk_prog = render_get_program(render_mngr, "prog_debug_chipmunk");

}

static Vertex *push_vertexes(size_t vcount, const Index *index_src, size_t icount) {
	cpAssertHard(VertexCount + vcount <= VERTEX_MAX && IndexCount + icount <= INDEX_MAX, "Geometry buffer full.");

	Vertex *vertex_dst = Vertexes + VertexCount;
	size_t base = VertexCount;
	VertexCount += vcount;

	Index *index_dst = Indexes + IndexCount;
	for (size_t i = 0; i < icount; i++) index_dst[i] = index_src[i] + (Index)base;
	IndexCount += icount;

	return vertex_dst;
}

void ChipmunkDebugDrawDot(cpFloat size, cpVect pos, cpSpaceDebugColor fillColor)
{
	float r = (float)(size*0.5f*ChipmunkDebugDrawPointLineScale);
	RGBA8 fill = cp_to_rgba(fillColor);

	Index arr[6] = { 0, 1, 2, 0, 2, 3 };

	Vertex *vertexes = push_vertexes(4, arr , 6);

	//TODO remove MSVC specific code after moving to newer compiler version
	#ifdef _MSC_VER
		vertexes[0] = { { (float)pos.x,(float)pos.y },{ -1, -1 },r, fill,fill };
		vertexes[1] = { { (float)pos.x, (float)pos.y },{ -1,  1 }, r, fill, fill };
		vertexes[2] = { { (float)pos.x, (float)pos.y },{ 1,  1 }, r, fill, fill };
		vertexes[3] = { { (float)pos.x, (float)pos.y },{ 1, -1 }, r, fill, fill };
	#else
		vertexes[0] = (Vertex) { .pos = { (float)pos.x,(float)pos.y }, .uv = { -1, -1 }, .r = r, .fill = fill, .outline = fill };
		vertexes[1] = (Vertex) { .pos = { (float)pos.x, (float)pos.y }, .uv = { -1,  1 }, .r = r, .fill = fill, .outline = fill };
		vertexes[2] = (Vertex) { .pos = { (float)pos.x, (float)pos.y }, .uv = { 1,  1 }, .r = r, .fill = fill, .outline = fill };
		vertexes[3] = (Vertex) { .pos = { (float)pos.x, (float)pos.y }, .uv = { 1, -1 }, .r = r, .fill = fill, .outline = fill };
	#endif
}

void ChipmunkDebugDrawCircle(cpVect pos, cpFloat angle, cpFloat radius, cpSpaceDebugColor outlineColor, cpSpaceDebugColor fillColor)
{
	float r = (float)radius + ChipmunkDebugDrawPointLineScale;
	RGBA8 fill = cp_to_rgba(fillColor), outline = cp_to_rgba(outlineColor);

	Index arr[6] = { 0, 1, 2, 0, 2, 3 };

	Vertex *vertexes = push_vertexes(4, arr, 6);

	#ifdef _MSC_VER
		vertexes[0] = { {(float)pos.x, (float)pos.y}, { -1, -1 }, r, fill, outline };
		vertexes[1] = { {(float)pos.x, (float)pos.y}, { -1,  1 }, r, fill, outline };
		vertexes[2] = { {(float)pos.x, (float)pos.y}, { 1,  1 }, r, fill, outline };
		vertexes[3] = { {(float)pos.x, (float)pos.y}, { 1, -1 }, r, fill, outline };
	#else
		vertexes[0] = (Vertex){ .pos= { (float)pos.x, (float)pos.y },.uv={ -1, -1 }, .r = r, .fill = fill, .outline = outline };
		vertexes[1] = (Vertex){ .pos = { (float)pos.x, (float)pos.y },.uv = { -1,  1 },.r = r,.fill = fill,.outline = outline };
		vertexes[2] = (Vertex){ .pos = { (float)pos.x, (float)pos.y },.uv = { 1,  1 },.r = r,.fill = fill,.outline = outline };
		vertexes[3] = (Vertex){ .pos = { (float)pos.x, (float)pos.y },.uv = { 1, -1 },.r = r,.fill = fill,.outline = outline };
	#endif

	ChipmunkDebugDrawSegment(pos, cpvadd(pos, cpvmult(cpvforangle(angle), 0.75f*radius)), outlineColor);
}

void ChipmunkDebugDrawSegment(cpVect a, cpVect b, cpSpaceDebugColor color)
{
	ChipmunkDebugDrawFatSegment(a, b, 0.0f, color, color);
}

void ChipmunkDebugDrawFatSegment(cpVect a, cpVect b, cpFloat radius, cpSpaceDebugColor outlineColor, cpSpaceDebugColor fillColor)
{
	static const Index indexes[] = { 0, 1, 2, 1, 2, 3, 2, 3, 4, 3, 4, 5, 4, 5, 6, 5, 6, 7 };
	Vertex *vertexes = push_vertexes(8, indexes, 18);

	cpVect t = cpvnormalize(cpvsub(b, a));

	float r = (float)radius + ChipmunkDebugDrawPointLineScale;
	RGBA8 fill = cp_to_rgba(fillColor), outline = cp_to_rgba(outlineColor);

	#ifdef _MSC_VER
		vertexes[0] = { {(float)a.x, (float)a.y}, { (float)(-t.x + t.y), (float)(-t.x - t.y) }, r, fill, outline };
		vertexes[1] = { {(float)a.x, (float)a.y}, { (float)(-t.x - t.y), (float)(+t.x - t.y) }, r, fill, outline };
		vertexes[2] = { {(float)a.x, (float)a.y}, { (float)(-0.0 + t.y), (float)(-t.x + 0.0) }, r, fill, outline };
		vertexes[3] = { {(float)a.x, (float)a.y}, { (float)(-0.0 - t.y), (float)(+t.x + 0.0) }, r, fill, outline };
		vertexes[4] = { {(float)b.x, (float)b.y}, { (float)(+0.0 + t.y), (float)(-t.x - 0.0) }, r, fill, outline };
		vertexes[5] = { {(float)b.x, (float)b.y}, { (float)(+0.0 - t.y), (float)(+t.x - 0.0) }, r, fill, outline };
		vertexes[6] = { {(float)b.x, (float)b.y}, { (float)(+t.x + t.y), (float)(-t.x + t.y) }, r, fill, outline };
		vertexes[7] = { {(float)b.x, (float)b.y}, { (float)(+t.x - t.y), (float)(+t.x + t.y) }, r, fill, outline };
	#else
		vertexes[0] = (Vertex){ .pos={ (float)a.x, (float)a.y },.uv={ (float)(-t.x + t.y), (float)(-t.x - t.y) }, .r=r, .fill=fill, .outline=outline };
		vertexes[1] = (Vertex){ .pos = { (float)a.x, (float)a.y }, .uv = { (float)(-t.x - t.y), (float)(+t.x - t.y) }, .r = r, .fill = fill, .outline = outline };
		vertexes[2] = (Vertex){ .pos = { (float)a.x, (float)a.y }, .uv = { (float)(-0.0 + t.y), (float)(-t.x + 0.0) }, .r = r, .fill = fill, .outline = outline };
		vertexes[3] = (Vertex){ .pos = { (float)a.x, (float)a.y }, .uv = { (float)(-0.0 - t.y), (float)(+t.x + 0.0) }, .r = r, .fill = fill, .outline = outline };
		vertexes[4] = (Vertex){ .pos = { (float)b.x, (float)b.y }, .uv = { (float)(+0.0 + t.y), (float)(-t.x - 0.0) }, .r = r, .fill = fill, .outline = outline };
		vertexes[5] = (Vertex){ .pos = { (float)b.x, (float)b.y }, .uv = { (float)(+0.0 - t.y), (float)(+t.x - 0.0) }, .r = r, .fill = fill, .outline = outline };
		vertexes[6] = (Vertex){ .pos = { (float)b.x, (float)b.y }, .uv = { (float)(+t.x + t.y), (float)(-t.x + t.y) }, .r = r, .fill = fill, .outline = outline };
		vertexes[7] = (Vertex){ .pos = { (float)b.x, (float)b.y }, .uv = { (float)(+t.x - t.y), (float)(+t.x + t.y) }, .r = r, .fill = fill, .outline = outline };
	#endif
}

#define MAX_POLY_VERTEXES 64
// Fill needs (count - 2) triangles.
// Outline needs 4*count triangles.
#define MAX_POLY_INDEXES (3*(5*MAX_POLY_VERTEXES - 2))

void ChipmunkDebugDrawPolygon(int count, const cpVect *verts, cpFloat radius, cpSpaceDebugColor outlineColor, cpSpaceDebugColor fillColor)
{
	RGBA8 fill = cp_to_rgba(fillColor), outline = cp_to_rgba(outlineColor);

	Index indexes[MAX_POLY_INDEXES];

	// Polygon fill triangles.
	for (int i = 0; i < count - 2; i++) {
		indexes[3 * i + 0] = 0;
		indexes[3 * i + 1] = 4 * (i + 1);
		indexes[3 * i + 2] = 4 * (i + 2);
	}

	// Polygon outline triangles.
	Index *cursor = indexes + 3 * (count - 2);
	for (int i0 = 0; i0 < count; i0++) {
		int i1 = (i0 + 1) % count;
		cursor[12 * i0 + 0] = 4 * i0 + 0;
		cursor[12 * i0 + 1] = 4 * i0 + 1;
		cursor[12 * i0 + 2] = 4 * i0 + 2;
		cursor[12 * i0 + 3] = 4 * i0 + 0;
		cursor[12 * i0 + 4] = 4 * i0 + 2;
		cursor[12 * i0 + 5] = 4 * i0 + 3;
		cursor[12 * i0 + 6] = 4 * i0 + 0;
		cursor[12 * i0 + 7] = 4 * i0 + 3;
		cursor[12 * i0 + 8] = 4 * i1 + 0;
		cursor[12 * i0 + 9] = 4 * i0 + 3;
		cursor[12 * i0 + 10] = 4 * i1 + 0;
		cursor[12 * i0 + 11] = 4 * i1 + 1;
	}

	float inset = (float)-cpfmax(0, 2 * ChipmunkDebugDrawPointLineScale - radius);
	float outset = (float)radius + ChipmunkDebugDrawPointLineScale;
	float r = outset - inset;

	Vertex *vertexes = push_vertexes(4 * count, indexes, 3 * (5 * count - 2));

	for (int i = 0; i<count; i++) {
		cpVect v0 = verts[i];
		cpVect v_prev = verts[(i + (count - 1)) % count];
		cpVect v_next = verts[(i + (count + 1)) % count];

		cpVect n1 = cpvnormalize(cpvrperp(cpvsub(v0, v_prev)));
		cpVect n2 = cpvnormalize(cpvrperp(cpvsub(v_next, v0)));
		cpVect of = cpvmult(cpvadd(n1, n2), 1.0 / (cpvdot(n1, n2) + 1.0f));
		cpVect v = cpvadd(v0, cpvmult(of, inset));

		#ifdef _MSC_VER
			vertexes[4 * i + 0] = { {(float)v.x, (float)v.y}, { 0.0f, 0.0f }, 0.0f, fill, outline };
			vertexes[4 * i + 1] = { {(float)v.x, (float)v.y}, { (float)n1.x, (float)n1.y }, r, fill, outline };
			vertexes[4 * i + 2] = { {(float)v.x, (float)v.y}, { (float)of.x, (float)of.y }, r, fill, outline };
			vertexes[4 * i + 3] = { {(float)v.x, (float)v.y}, { (float)n2.x, (float)n2.y }, r, fill, outline };
		#else
			vertexes[4 * i + 0] = (Vertex) { .pos={ (float)v.x, (float)v.y },.uv={ 0.0f, 0.0f }, .r=0.0f, .fill=fill, .outline=outline };
			vertexes[4 * i + 1] = (Vertex) { .pos = { (float)v.x, (float)v.y },.uv = { (float)n1.x, (float)n1.y },.r = r,.fill = fill,.outline = outline };
			vertexes[4 * i + 2] = (Vertex) { .pos = { (float)v.x, (float)v.y },.uv = { (float)of.x, (float)of.y },.r = r,.fill = fill,.outline = outline };
			vertexes[4 * i + 3] = (Vertex) { .pos = { (float)v.x, (float)v.y },.uv = { (float)n2.x, (float)n2.y },.r = r,.fill = fill,.outline = outline };
		#endif
	}
}

void ChipmunkDebugDrawBB(cpBB bb, cpSpaceDebugColor color)
{
	cpVect verts[] = {
		cpv(bb.r, bb.b),
		cpv(bb.r, bb.t),
		cpv(bb.l, bb.t),
		cpv(bb.l, bb.b),
	};
	ChipmunkDebugDrawPolygon(4, verts, 0.0f, color, LAColor(0, 0));
}

void ChipmunkDebugSetDrawMatrix(cpTransform drawmatrix)
{
	ChipmunkDebugDrawVPMatrix = drawmatrix;
}

void ChipmunkDebugSetPointScale(float pointscale)
{
	ChipmunkDebugDrawPointLineScale = pointscale;
}

void ChipmunkDebugDrawFlushRenderer(render_manager_t* const render_mngr)
{
	cpTransform t = ChipmunkDebugDrawVPMatrix;
	float uniforms[16] = { (float)t.a , (float)t.b , 0.0f, 0.0f, (float)t.c , (float)t.d , 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, (float)t.tx, (float)t.ty, 0.0f, 1.0f };

	render_set_backfacecull(render_mngr, wfalse);

	render_update_buffer(render_mngr, VertexBuffer, Vertexes, 0, VertexCount * sizeof(Vertex));
	render_update_indices_buffer(render_mngr, IndexBuffer,Indexes,0, IndexCount * sizeof(Index));

	render_use_program(render_mngr, chipmunk_prog);

	render_bind_buffer(render_mngr, VertexBuffer);

	render_setuniform_matrix4(render_mngr, "U_vp_matrix", uniforms);
	render_setshader_attribute_float(render_mngr, chipmunk_prog, "IN_pos", 2, sizeof(Vertex), (void*)offsetof(Vertex, pos));
	render_setshader_attribute_float(render_mngr, chipmunk_prog, "IN_uv", 2, sizeof(Vertex), (void*)offsetof(Vertex, uv));
	render_setshader_attribute_float(render_mngr, chipmunk_prog, "IN_radius", 1, sizeof(Vertex), (void*)offsetof(Vertex, r));
	render_setshader_attribute_ubyte(render_mngr, chipmunk_prog, "IN_fill", 4, sizeof(Vertex), (void*)offsetof(Vertex, fill),wtrue);
	render_setshader_attribute_ubyte(render_mngr, chipmunk_prog, "IN_outline", 4, sizeof(Vertex), (void*)offsetof(Vertex, outline),wtrue);


	render_draw_indices(render_mngr, IndexBuffer, IndexCount);
	render_unbind_buffer(render_mngr, VertexBuffer);

	render_set_backfacecull(render_mngr, wtrue);

}

void ChipmunkDebugDrawClearRenderer(void)
{
	VertexCount = IndexCount = 0;
}


static size_t PushedVertexCount, PushedIndexCount;

void ChipmunkDebugDrawPushRenderer(void)
{
	PushedVertexCount = VertexCount;
	PushedIndexCount = IndexCount;
}

void ChipmunkDebugDrawPopRenderer(void)
{
	VertexCount = PushedVertexCount;
	IndexCount = PushedIndexCount;
}

static void
DrawCircle(cpVect p, cpFloat a, cpFloat r, cpSpaceDebugColor outline, cpSpaceDebugColor fill, cpDataPointer data)
{
	ChipmunkDebugDrawCircle(p, a, r, outline, fill);
}

static void
DrawSegment(cpVect a, cpVect b, cpSpaceDebugColor color, cpDataPointer data)
{
	ChipmunkDebugDrawSegment(a, b, color);
}

static void
DrawFatSegment(cpVect a, cpVect b, cpFloat r, cpSpaceDebugColor outline, cpSpaceDebugColor fill, cpDataPointer data)
{
	ChipmunkDebugDrawFatSegment(a, b, r, outline, fill);
}

static void
DrawPolygon(int count, const cpVect *verts, cpFloat r, cpSpaceDebugColor outline, cpSpaceDebugColor fill, cpDataPointer data)
{
	ChipmunkDebugDrawPolygon(count, verts, r, outline, fill);
}

static void
DrawDot(cpFloat size, cpVect pos, cpSpaceDebugColor color, cpDataPointer data)
{
	ChipmunkDebugDrawDot(size, pos, color);
}

cpSpaceDebugDrawOptions ChipmunkDebugDrawGetDefaultOptions()
{
	 cpSpaceDebugDrawOptions drawOptions = {
		DrawCircle,
		DrawSegment,
		DrawFatSegment,
		DrawPolygon,
		DrawDot,

		(cpSpaceDebugDrawFlags)(CP_SPACE_DEBUG_DRAW_SHAPES | CP_SPACE_DEBUG_DRAW_CONSTRAINTS | CP_SPACE_DEBUG_DRAW_COLLISION_POINTS),

		{ 0xEE / 255.0f, 0xE8 / 255.0f, 0xD5 / 255.0f, 1.0f }, // Outline color
		ColorForShape,
		{ 0.0f, 0.75f, 0.0f, 1.0f },
		{ 1.0f, 0.0f, 0.0f, 1.0f },
		NULL
	};

	 return drawOptions;
}

void ChipmunkDebugDrawImpl(render_manager_t* const render_mngr, cpSpace* const space)
{
	ChipmunkDebugDrawImplWithOptions(render_mngr, space, ChipmunkDebugDrawGetDefaultOptions());
}


void ChipmunkDebugDrawImplWithOptions(render_manager_t* const render_mngr, cpSpace* const space, cpSpaceDebugDrawOptions drawOptions)
{
	cpFloat heightscale = render_mngr->screen_info.height * render_mngr->zoom_factor;
	cpVect view_translate = { -render_mngr->screen_info.position.x, (heightscale - render_mngr->screen_info.position.y) };
	cpFloat view_scale = 1.0f / render_mngr->zoom_factor;

	drawOptions.data = render_mngr;

	cpVect screen_size = { (cpFloat)render_mngr->screen_info.width, (cpFloat)render_mngr->screen_info.height };
	cpTransform view_matrix = cpTransformMult(cpTransformScale(view_scale, view_scale), cpTransformTranslate(view_translate));

	cpTransform projection_matrix = cpTransformOrtho(cpBBNew(0.0f, 0.0f, screen_size.x, screen_size.y));

	ChipmunkDebugSetPointScale(1.0f / (float)view_scale);
	ChipmunkDebugSetDrawMatrix(cpTransformMult(projection_matrix, view_matrix));

	ChipmunkDebugDrawPushRenderer();

	cpSpaceDebugDraw(space, &drawOptions);

	ChipmunkDebugDrawFlushRenderer(render_mngr);

	ChipmunkDebugDrawPopRenderer();

	//render_setup_config(render_mngr);
}

#endif
