#ifdef ALLOC_TRACER

#include <stdio.h>
#include <inttypes.h>
#include <errno.h> 
#include <stdarg.h>
#include <string.h> 
#include <malloc.h>
#include <Engine.h>


#ifdef ANDROID_BUILD
	#include <android/log.h>
#endif

#include <Debug/Logprint.h>

#ifdef MEM_TRACE
	#include "MemTracer/MemTracer.h"
static const int kNumLoaders = 9;
#endif


#ifdef MEM_TRACE
void init_mem_trace()
{

	logprint("MemTracer: waiting for connection...\n");
	MemTracer::FunctionHooks hooks;
	const unsigned short port = 1000;
	if (!MemTracer::Init(hooks, port, kNumLoaders + 1, MemTracer::BlockingMode::BLOCKING))
	{
		logprint("Failed to initialize MemTracer\n");
	}

	logprint("MemTracer: Connected, memory overhead ~= %d kb\n", MemTracer::GetMemoryOverhead() >> 10);

}

void snapshot_mem_trace(const char* snap_name)
{
	MemTracer::AddSnapshot(snap_name);
}

void frameend_mem_trace()
{
	MemTracer::FrameEnd();
}

void shutdown_mem_trace()
{
	MemTracer::Shutdown();
}

#endif

static void test_heap()
{
	void* test = malloc(7345);
	free(test);
}

void* malloc_tracer(float line,const char* filename,size_t bytes)
{
	void* ptr = malloc(bytes);

	test_heap();

	#ifdef MEM_TRACE
		MemTracer::OnAlloc(ptr, bytes, "SYSM");
	#else
		logprint("Adress %p => Allocate %d bytes File : %s Line : %d ",ptr,bytes,filename,(int)line);
	#endif

	return ptr;
}

void free_tracer(float line,const char* filename,void* ptr)
{
	
	#ifdef MEM_TRACE
		if (ptr)
			MemTracer::OnFree(ptr);
	#else
		logprint("Adress %p => Free File : %s Line : %d ",ptr,filename,(int)line);
	#endif

	free(ptr);

	test_heap();
}

void* realloc_tracer(float line,const char* filename,size_t bytes,void* ptr)
{
	ptr = realloc(ptr,bytes);

	test_heap();
	
	#ifdef MEM_TRACE
		MemTracer::OnAlloc(ptr, bytes, "SYSM");
	#else
		logprint("Adress %p => Reallocate %d bytes File : %s Line : %d ",ptr,bytes,filename,(int)line);
	#endif

	

	return ptr;
}

void* calloc_tracer(float line,const char* filename,size_t count,size_t size)
{
	void* ptr = calloc(count,size);

	test_heap();
	
	#ifdef MEM_TRACE
		MemTracer::OnAlloc(ptr, size * count, "SYSM");
	#else
		logprint("Adress %p => Callocate %d bytes File : %s Line : %d ",size * count,filename,(int)line);
	#endif

	return ptr;
}

#endif