#include <stdio.h>
#include <inttypes.h>
#include <errno.h> 
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <Engine.h>


#ifdef ANDROID_BUILD
	#include <android/log.h>
#endif

#include <Debug/Logprint.h>


static char file_log[260];

void logprint_setdir(const char* dir)
{
	if (dir[strlen(dir) - 1] != PATH_SEP)
		w_sprintf(file_log, 260, "%s%cwafa.log", dir, PATH_SEP);
	else
		w_sprintf(file_log, 260, "%swafa.log", dir);
}


int8_t logprint_c(float line, const char* filename,const char* function,const char* message,...) //ret : 1 = ok, 0 = can't write to log
{
    int8_t retval = 1;
	va_list argptr = { 0 };
	time_t rawtime = { 0 };
	struct tm timeinfo = { 0 };
	struct tm* ptr = &timeinfo;
	char buffer[80] = { 0 };

	 time (&rawtime);
	 w_localtime(ptr,&rawtime);

	strftime (buffer,80,"[%Y-%m-%d %Hh:%Mm:%Ss]",&timeinfo);
	
    va_start(argptr, message);

#ifdef ANDROID_BUILD
	__android_log_vprint(ANDROID_LOG_DEBUG, "WAFA",message, argptr);
#else

	//no file logging with emscripten
	#ifndef EMSCRIPTEN
		FILE* fhandle = NULL;

		w_fopen(fhandle,file_log,"a+");

		if(fhandle != NULL)
		{
			fpos_t fpos;

			fgetpos(fhandle,&fpos);

			//check log file size
			fseek(fhandle, 0, SEEK_END);
			size_t log_size = ftell(fhandle);
			fsetpos(fhandle,&fpos);

			if(log_size >= MAX_LOG_SIZE)
			{
				rewind(fhandle);
				//make an archive of the log file
				unsigned char* archive_buffer = (unsigned char*) wf_malloc(log_size * sizeof(unsigned char));
				fread(archive_buffer,1,log_size,fhandle);
				fclose(fhandle);

				char date[80] = { 0 };

				strftime(date,80,".%Y%m%d.%Hh.%Mm.%Ss",&timeinfo);

				char archive_file[260];

				w_sprintf(archive_file,260,"%s%s",file_log,date);

				FILE* arch_handle = NULL;
				
				w_fopen(arch_handle,archive_file,"w");

				if(arch_handle != NULL)
				{
					fwrite(archive_buffer,sizeof(unsigned char),log_size,arch_handle);
					fclose(arch_handle);
					//reopen log file with different mode
					w_fopen(fhandle,file_log,"w");
				}
				else
				{
					 w_fopen(fhandle,file_log,"a");//archiving failed, reopen log in append mode
					 char errorbuff[256] = { 0 };
					 w_strerror(errorbuff, 256, errno);
					 printf("file archiving failed %s", errorbuff);
				}

				wf_free(archive_buffer);	
			}
		}
	#endif


	#ifndef ALLOC_TRACER
		//#if defined(DEBUG)
			fprintf(stdout,"[%s]",function);
			vfprintf(stdout, message, argptr);//print to stdout first
                        va_end(argptr);
                        va_start(argptr, message);
		//#endif
	#endif

	//no file logging with emscripten
	#ifndef EMSCRIPTEN
    if(fhandle != 0)
    {
		fprintf(fhandle,"%s : File : %s Line : %d  Function : %s \n",buffer,filename,(int)line,function);
        vfprintf(fhandle,message,argptr);//print to our log file
		fprintf(fhandle,"\n");
        fclose(fhandle);
    }
    else
    {
		char bufferr[1024] = { 0 };
		w_strerror(bufferr, 1024, errno);
        printf("file logging failed %s",bufferr);
        retval = 0;
    }
	#endif

#endif

    va_end(argptr);

	//feed new line
	printf("\n");

    return retval;
}
