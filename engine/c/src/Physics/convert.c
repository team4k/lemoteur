#include <inttypes.h>

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <stdarg.h>

#include <Render.h>

#include <Engine.h>

#include <chipmunk/chipmunk.h>

#include <Physics/convert.h>


void convert_cpvect_to_float(cpVect* src,float** dest,size_t array_size)
{
	for(size_t isize = 0; isize < array_size ;isize++)
	{
		dest[isize][0] = (float)src[isize].x;
		dest[isize][1] = (float)src[isize].y;
	}
}

void convert_cpfloat_to_float(cpFloat* src, float* dest, size_t array_size)
{
	for (size_t isize = 0; isize < array_size; isize++)
	{
		dest[isize] = (float)src[isize];
	}
}

