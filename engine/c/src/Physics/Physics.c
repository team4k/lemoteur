#include <inttypes.h>

#include <Render.h>

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <stdarg.h>

#include <Engine.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>
#include <Base/geom.h>

#include <Render/render_manager.h>

#include <Debug/Logprint.h>

#include <chipmunk/chipmunk.h>

#include <Utils/Hash.h>

#include <Debug/geomdebug.h>


#include <Physics/Physics.h>

void physics_init(physics_manager_t* const  manager, wbool use_spatial_hash)
{
	physics_init2(manager, use_spatial_hash, wfalse);
}


void physics_init2(physics_manager_t* const  manager,wbool use_spatial_hash,wbool pool_alloc)
{
	//create the physics world
	manager->world =  cpSpaceNew();
	manager->unload_inprogress = 0;
    cpSpaceSetIterations(manager->world,10);
    cpSpaceSetUserData(manager->world,manager);
	manager->pool_alloc = pool_alloc;

	if(use_spatial_hash)
		cpSpaceUseSpatialHash(manager->world,32.0f,500);
	
	manager->is_slave = wfalse;
	manager->defaultwallmask = (LAYER_PLAYER | LAYER_ENTITY | LAYER_FORCE);
}

/*
	This function allocated the maximum number of elements required by our physics manager given the size of the tilemap,
	it can only be use with pool_alloc = wtrue, keep in mind that using this mode will use way more memory than
	normal mode but will avoid constant allocation / reallocation and is best suited for level streaming
*/
void physics_alloc(physics_manager_t* const  manager, const int32_t col_count, const int32_t row_count,const wbool has_destructible_data)
{
	if (!manager->pool_alloc)
	{
		logprint("You must use physics_init2 with pool_alloc = wtrue to use preallocation function, aborting...");
		return;
	}

	if (manager->is_slave)
	{
		logprint("Can't use this function with a slave manager!");
		return;
	}

	manager->row_count = row_count;
	manager->col_count = col_count;

	uint32_t tilemap_size = (col_count * row_count);


	//allocate size of the tilemap * 4 sides * size of cpshape pointer
	manager->static_shapes = (cpShape**)wf_calloc((tilemap_size * 4) ,sizeof(cpShape*));
	manager->callback_refs = (callback_ref_t*)wf_calloc((tilemap_size * 4) ,sizeof(callback_ref_t));

	//temporary segments info used to build collisions
	manager->collisions_segment = (segment_t**)wf_calloc((tilemap_size * 4) ,sizeof(segment_t*));

	manager->allocated_segment = (segment_t*)wf_calloc((tilemap_size * 4), sizeof(segment_t));

	manager->allocated_shape = (cpSegmentShape**)wf_calloc((tilemap_size * 4), sizeof(cpSegmentShape*));

	for (uint32_t ialloc = 0; ialloc < (tilemap_size * 4); ialloc++)
	{
		manager->allocated_shape[ialloc] = cpSegmentShapeAlloc();
	}

	//create a copy of the collisions array, using it to track change in the collisions ingame
	manager->collisions_array = (int32_t*)wf_calloc(tilemap_size , sizeof(int32_t));

	manager->tilemap_array = NULL;

	if (has_destructible_data)
	{
		//create a copy of the destructible tilemap array, using it to reset any change ingame
		manager->tilemap_array = (int32_t*)wf_calloc(tilemap_size, sizeof(int32_t));
	}
}


void create_slave_manager(physics_manager_t* const slave_manager,physics_manager_t* const master_manager)
{
	slave_manager->world = master_manager->world;
	slave_manager->is_slave = wtrue;
}

//default function to check which tile collision type are neighbour dependent (only have a border shape if no adjacent neighbour filled tile exist)
wbool default_check_col(int32_t collision_value)
{
	if (collision_value == COLLISION || collision_value == COLLISION_KILL)
		return wtrue;

	return wfalse;
}

//default function to check which tile collision type have all it border shape no matter neighbour value
static wbool default_check_editable(int32_t collision_value)
{
	if (collision_value == COLLISION_DESTRUCTIBLE || collision_value == COLLISION_EDITABLE || collision_value == COLLISION_EDITABLE_2 || collision_value == COLLISION_EDITABLE_3)
		return wtrue;

	return wfalse;
}



void physics_create_collision_shape(physics_manager_t* const  manager,int32_t tile_col,int32_t tile_row,int32_t tile_size,int32_t shape_index,int32_t index,cpCollisionType col_type,const int32_t col_count,const int32_t row_count,const int32_t tile_index,const int32_t* const collisions_array,segment_t** segment,WorldRect_t dim,int16_t ichunk,check_col_t check_col_func,check_col_t check_col_func_editable,int32_t num_chunk_col)
{
	//! it pool_alloc = wtrue, no dynamic allocation is made here, all allocation are supposed to be made before hand

		int32_t YLimit = row_count - 1;
		int32_t XLimit = col_count - 1;

		check_col_t check_col = &default_check_col;
		check_col_t check_editable_col = &default_check_editable;

		if (check_col_func != NULL)
		{
			check_col = check_col_func;
		}

		if (check_col_func_editable != NULL)
		{
			check_editable_col = check_col_func_editable;
		}

		int32_t rowdiff = 0;
		int32_t coldiff = 0;

		if (dim.loadeddimension.position.x != vectorzero.x || dim.loadeddimension.position.y != vectorzero.y)
		{
			rowdiff = dim.loadeddimension.position.y / tile_size;
			coldiff = dim.loadeddimension.position.x / tile_size;
		}


		int32_t worldindex = index;

		if (dim.loadeddimension.width != 0 && dim.worldwidth != 0)
		{
			int32_t localrow = (int32_t)wfloor(index / (dim.loadeddimension.width / tile_size));
			int32_t localcol = (int32_t)index % (dim.loadeddimension.width / tile_size);
			worldindex = (localrow + rowdiff) * (dim.worldwidth / tile_size) + (localcol + coldiff);
		}
		
		

		//if col_type is slope, create only one shape
		if(col_type == COLLISION_SLOPE || col_type ==  COLLISION_SLOPE_INVERSE)
		{
			cpVect slopeA = cpvzero,slopeB = cpvzero;

			if(col_type == COLLISION_SLOPE)
			{
				slopeA = cpv((tile_col * tile_size) + tile_size, tile_row * tile_size);
				slopeA.x += dim.loadeddimension.position.x;
				slopeA.y += dim.loadeddimension.position.y;

				slopeB = cpv(slopeA.x - tile_size,slopeA.y + tile_size);
			}
			else if(col_type == COLLISION_SLOPE_INVERSE)
			{
				slopeA = cpv(tile_col * tile_size, tile_row * tile_size);
				slopeA.x += dim.loadeddimension.position.x;
				slopeA.y += dim.loadeddimension.position.y;

				slopeB = cpv(slopeA.x + tile_size,slopeA.y + tile_size);
			}

			manager->callback_refs[shape_index].shape_index = index * 4;
			//world = unique across the entire game ! it translate to collision_index + (index offset of bottom_left chunk)
			manager->callback_refs[shape_index].world_index = worldindex;
			manager->callback_refs[shape_index].tilemap_index = tile_index;
			manager->callback_refs[shape_index].tile_type = PTILE_SLOPE;
			manager->callback_refs[shape_index].ichunk = ichunk;
			manager->callback_refs[shape_index].base_typ_col = col_type;

			if (!manager->pool_alloc)
				segment[shape_index] = (segment_t*)wf_malloc(sizeof(segment_t));
			else
				segment[shape_index] = &manager->allocated_segment[shape_index];

			segment[shape_index]->start_segment = slopeA;
			segment[shape_index]->end_segment = slopeB;
			segment[shape_index]->data = &manager->callback_refs[shape_index];
			segment[shape_index]->neighbour1 = slopeA;
			segment[shape_index]->neighbour2 = slopeB;
			segment[shape_index++]->typ_col = col_type;
		}
		else if(check_col(col_type))
		{
			//check for neighbour, add shape only if tile empty or tile type is different
			wbool has_lower_neighbour = wfalse;
			wbool has_upper_neighbour = wfalse;
			wbool has_left_neighbour = wfalse;
			wbool has_right_neighbour = wfalse;

			//check upper neighbour
			if(tile_row < YLimit && check_col(collisions_array[((tile_row + 1) * col_count) + tile_col]))
			{
				has_upper_neighbour = wtrue;
			}


			//check right neighbour
			if(tile_col < XLimit && check_col(collisions_array[(tile_row * col_count) + (tile_col + 1)]))
			{
				has_right_neighbour = wtrue;
			}

			//check lower neighbour
			if(tile_row > 0 && check_col(collisions_array[((tile_row - 1) * col_count) + tile_col]))
			{
				has_lower_neighbour = wtrue;
			}


			//check left neighbour
			if(tile_col > 0 && check_col(collisions_array[(tile_row * col_count) + (tile_col - 1)]))
			{
				has_left_neighbour = wtrue;
			}



			cpVect bottomsideA = cpv(tile_col * tile_size, tile_row * tile_size);
			bottomsideA.x += dim.loadeddimension.position.x;
			bottomsideA.y += dim.loadeddimension.position.y;

			cpVect bottomsideB = cpv(bottomsideA.x + tile_size,bottomsideA.y);

			

			cpVect rightsideA = cpv(bottomsideB.x, bottomsideB.y);
			cpVect rightsideB = cpv(rightsideA.x ,rightsideA.y + tile_size);

			cpVect topsideA = cpv(bottomsideA.x,rightsideB.y);
			cpVect topsideB = cpv(rightsideB.x, rightsideB.y);

			cpVect leftsideA = cpv(bottomsideA.x, bottomsideA.y);
			cpVect leftsideB = cpv(topsideA.x,topsideB.y);


			manager->callback_refs[shape_index].shape_index = index * 4;
			manager->callback_refs[shape_index].world_index = worldindex;
			manager->callback_refs[shape_index].tilemap_index = tile_index;
			manager->callback_refs[shape_index].ichunk = ichunk;
			manager->callback_refs[shape_index].base_typ_col = col_type;

			if(!has_lower_neighbour)
			{
				manager->callback_refs[shape_index].tile_type = PTILE_BOTTOM;

				if (!manager->pool_alloc)
					segment[shape_index] = (segment_t*)wf_malloc(sizeof(segment_t));
				else
					segment[shape_index] = &manager->allocated_segment[shape_index];

				segment[shape_index]->start_segment = bottomsideA;
				segment[shape_index]->end_segment = bottomsideB;
				segment[shape_index]->data = &manager->callback_refs[shape_index];
				segment[shape_index]->typ_col = col_type;

				if(has_left_neighbour)
					segment[shape_index]->neighbour1 = cpv(bottomsideA.x - tile_size,bottomsideA.y);
				else
					segment[shape_index]->neighbour1 = bottomsideA;

				if(has_right_neighbour)
					segment[shape_index]->neighbour2 = cpv(bottomsideB.x + tile_size,bottomsideB.y);
				else
					segment[shape_index]->neighbour2 = bottomsideB;


			}

			shape_index++;

			manager->callback_refs[shape_index].shape_index = index * 4;
			manager->callback_refs[shape_index].world_index = worldindex;
			manager->callback_refs[shape_index].tilemap_index = tile_index;
			manager->callback_refs[shape_index].ichunk = ichunk;
			manager->callback_refs[shape_index].base_typ_col = col_type;

			if(!has_right_neighbour)
			{
				manager->callback_refs[shape_index].tile_type = PTILE_RIGHT;

				if (!manager->pool_alloc)
					segment[shape_index] = (segment_t*)wf_malloc(sizeof(segment_t));
				else
					segment[shape_index] = &manager->allocated_segment[shape_index];

				segment[shape_index]->start_segment = rightsideA;
				segment[shape_index]->end_segment = rightsideB;
				segment[shape_index]->data = &manager->callback_refs[shape_index];
				segment[shape_index]->typ_col = col_type;


				if(has_lower_neighbour)
					segment[shape_index]->neighbour1 = cpv(rightsideA.x,rightsideA.y - tile_size);
				else
					segment[shape_index]->neighbour1 = rightsideA;

				if(has_upper_neighbour)
					segment[shape_index]->neighbour2 = cpv(rightsideB.x,rightsideB.y + tile_size);
				else
					segment[shape_index]->neighbour2 = rightsideB;
			}

			shape_index++;

			manager->callback_refs[shape_index].shape_index = index * 4;
			manager->callback_refs[shape_index].world_index = worldindex;
			manager->callback_refs[shape_index].tilemap_index = tile_index;
			manager->callback_refs[shape_index].ichunk = ichunk;
			manager->callback_refs[shape_index].base_typ_col = col_type;

			if(!has_upper_neighbour)
			{
				manager->callback_refs[shape_index].tile_type = PTILE_TOP;

				if (!manager->pool_alloc)
					segment[shape_index] = (segment_t*)wf_malloc(sizeof(segment_t));
				else
					segment[shape_index] = &manager->allocated_segment[shape_index];

				segment[shape_index]->start_segment = topsideA;
				segment[shape_index]->end_segment = topsideB;
				segment[shape_index]->data = &manager->callback_refs[shape_index];
				segment[shape_index]->typ_col = col_type;


				if(has_left_neighbour)
					segment[shape_index]->neighbour1 = cpv(topsideA.x - tile_size,topsideA.y);
				else
					segment[shape_index]->neighbour1 = topsideA;

				if(has_right_neighbour)
					segment[shape_index]->neighbour2 = cpv(topsideB.x + tile_size,topsideB.y);
				else
					segment[shape_index]->neighbour2 = topsideB;
			}

			shape_index++;

			manager->callback_refs[shape_index].shape_index = index * 4;
			manager->callback_refs[shape_index].world_index = worldindex;
			manager->callback_refs[shape_index].tilemap_index = tile_index;
			manager->callback_refs[shape_index].ichunk = ichunk;
			manager->callback_refs[shape_index].base_typ_col = col_type;

			if(!has_left_neighbour)
			{
				manager->callback_refs[shape_index].tile_type = PTILE_LEFT;

				if (!manager->pool_alloc)
					segment[shape_index] = (segment_t*)wf_malloc(sizeof(segment_t));
				else
					segment[shape_index] = &manager->allocated_segment[shape_index];

				segment[shape_index]->start_segment = leftsideA;
				segment[shape_index]->end_segment = leftsideB;
				segment[shape_index]->data = &manager->callback_refs[shape_index];
				segment[shape_index]->typ_col = col_type;


				if(has_lower_neighbour)
					segment[shape_index]->neighbour1 = cpv(leftsideA.x,leftsideA.y - tile_size);
				else
					segment[shape_index]->neighbour1 = leftsideA;

				if(has_upper_neighbour)
					segment[shape_index]->neighbour2 = cpv(leftsideB.x,leftsideB.y + tile_size);
				else
					segment[shape_index]->neighbour2 = leftsideB;
			}

			shape_index++;


		}	
		else if(check_editable_col(col_type))
		{
			
			cpVect bottomsideA = cpv(tile_col * tile_size, tile_row * tile_size);
			bottomsideA.x += dim.loadeddimension.position.x;
			bottomsideA.y += dim.loadeddimension.position.y;
			cpVect bottomsideB = cpv(bottomsideA.x + tile_size,bottomsideA.y);

			

			manager->callback_refs[shape_index].shape_index = index * 4;
			manager->callback_refs[shape_index].world_index = worldindex;
			manager->callback_refs[shape_index].tilemap_index = tile_index;
			manager->callback_refs[shape_index].tile_type = PTILE_BOTTOM;
			manager->callback_refs[shape_index].ichunk = ichunk;
			manager->callback_refs[shape_index].base_typ_col = col_type;

			if (!manager->pool_alloc)
				segment[shape_index] = (segment_t*)wf_malloc(sizeof(segment_t));
			else
				segment[shape_index] = &manager->allocated_segment[shape_index];

			segment[shape_index]->start_segment = bottomsideA;
			segment[shape_index]->end_segment = bottomsideB;
			segment[shape_index]->data = &manager->callback_refs[shape_index];
			segment[shape_index]->neighbour1 = bottomsideA;
			segment[shape_index]->neighbour2 = bottomsideB;
			segment[shape_index++]->typ_col = col_type;


			cpVect rightsideA = cpv(bottomsideB.x, bottomsideB.y);
			cpVect rightsideB = cpv(rightsideA.x ,rightsideA.y + tile_size);

			manager->callback_refs[shape_index].shape_index = index * 4;
			manager->callback_refs[shape_index].world_index = worldindex;
			manager->callback_refs[shape_index].tilemap_index = tile_index;
			manager->callback_refs[shape_index].tile_type = PTILE_RIGHT;
			manager->callback_refs[shape_index].ichunk = ichunk;
			manager->callback_refs[shape_index].base_typ_col = col_type;

			if (!manager->pool_alloc)
				segment[shape_index] = (segment_t*)wf_malloc(sizeof(segment_t));
			else
				segment[shape_index] = &manager->allocated_segment[shape_index];

			segment[shape_index]->start_segment = rightsideA;
			segment[shape_index]->end_segment = rightsideB;
			segment[shape_index]->neighbour1 = rightsideA;
			segment[shape_index]->neighbour2 = rightsideB;
			segment[shape_index]->data = &manager->callback_refs[shape_index];
			segment[shape_index++]->typ_col = col_type;

			cpVect topsideA = cpv(rightsideB.x, rightsideB.y);
			cpVect topsideB = cpv(topsideA.x - tile_size,topsideA.y);

			manager->callback_refs[shape_index].shape_index = index * 4;
			manager->callback_refs[shape_index].world_index = worldindex;
			manager->callback_refs[shape_index].tilemap_index = tile_index;
			manager->callback_refs[shape_index].tile_type = PTILE_TOP;
			manager->callback_refs[shape_index].ichunk = ichunk;
			manager->callback_refs[shape_index].base_typ_col = col_type;

			if (!manager->pool_alloc)
				segment[shape_index] = (segment_t*)wf_malloc(sizeof(segment_t));
			else
				segment[shape_index] = &manager->allocated_segment[shape_index];

			segment[shape_index]->start_segment = topsideA;
			segment[shape_index]->end_segment = topsideB;
			segment[shape_index]->neighbour1 = topsideA;
			segment[shape_index]->neighbour2 = topsideB;
			segment[shape_index]->data = &manager->callback_refs[shape_index];
			segment[shape_index++]->typ_col = col_type;


			cpVect leftsideA = cpv(topsideB.x, topsideB.y);
			cpVect leftsideB = cpv(leftsideA.x,leftsideA.y - tile_size);

			manager->callback_refs[shape_index].shape_index = index*4;
			manager->callback_refs[shape_index].world_index = worldindex;
			manager->callback_refs[shape_index].tilemap_index = tile_index;
			manager->callback_refs[shape_index].tile_type = PTILE_LEFT;
			manager->callback_refs[shape_index].ichunk = ichunk;
			manager->callback_refs[shape_index].base_typ_col = col_type;

			if (!manager->pool_alloc)
				segment[shape_index] = (segment_t*)wf_malloc(sizeof(segment_t));
			else
				segment[shape_index] = &manager->allocated_segment[shape_index];

			segment[shape_index]->start_segment = leftsideA;
			segment[shape_index]->end_segment = leftsideB;
			segment[shape_index]->neighbour1 = leftsideA;
			segment[shape_index]->neighbour2 = leftsideB;
			segment[shape_index]->data = &manager->callback_refs[shape_index];
			segment[shape_index++]->typ_col = col_type;

		}
}


void physics_set_shape_layer(physics_manager_t* const  manager,int32_t shape_index,int32_t layer_id,int32_t mask)
{
	cpShapeFilter filter = cpShapeFilterNew(CP_NO_GROUP, layer_id, mask);

    cpShapeSetFilter(manager->static_shapes[shape_index++],filter);
    cpShapeSetFilter(manager->static_shapes[shape_index++],filter);
    cpShapeSetFilter(manager->static_shapes[shape_index++],filter);
    cpShapeSetFilter(manager->static_shapes[shape_index++],filter);
}

void physics_setelasticity(physics_manager_t* const manager,cpFloat elasticity)
{
	uint32_t tilemap_size = (manager->col_count * manager->row_count);
	uint32_t shape_index = 0;
	uint32_t index = 0;

	while(index<tilemap_size)
	{
		if(manager->collisions_array[index] == 0)
		{
			shape_index += 4;
			index++;
			continue;
		}

		for(int side = 0; side< 4;side++)
		{
			if(manager->static_shapes[shape_index] != NULL)
                cpShapeSetElasticity(manager->static_shapes[shape_index],elasticity);

			shape_index++;
		}

		index++;
	}
}

void physics_setfriction(physics_manager_t* const manager,cpFloat friction)
{
	uint32_t tilemap_size = (manager->col_count * manager->row_count);
	uint32_t shape_index = 0;
	uint32_t index = 0;

	while(index<tilemap_size)
	{
		if(manager->collisions_array[index] == 0)
		{
			shape_index += 4;
			index++;
			continue;
		}

		for(int side = 0; side< 4;side++)
		{
			if(manager->static_shapes[shape_index] != NULL)
                cpShapeSetFriction(manager->static_shapes[shape_index],friction);

			shape_index++;
		}

		index++;
	}
}

void physics_free_tilemap_collisions(physics_manager_t* const manager,wbool removeonly)
{
	uint32_t tilemap_size = (manager->col_count * manager->row_count);
	uint32_t shape_index = 0;
	uint32_t index = 0;

	while(index<tilemap_size)
	{
		for(int side = 0; side< 4;side++)
		{
			if(manager->static_shapes[shape_index] != NULL)
			{
				cpSpaceRemoveShape(manager->world,manager->static_shapes[shape_index]);

				if (!manager->pool_alloc && !removeonly)
					cpShapeFree(manager->static_shapes[shape_index]);

				manager->static_shapes[shape_index] = NULL;
			}

			shape_index++;
		}

		index++;
	}

	wf_free(manager->static_shapes);
	wf_free(manager->callback_refs);
	wf_free(manager->collisions_array);

	manager->static_shapes = NULL;
	manager->callback_refs = NULL;
	manager->collisions_array = NULL;
	manager->col_count = manager->row_count = 0;

	if(manager->tilemap_array)
	{
		wf_free(manager->tilemap_array);
		manager->tilemap_array = NULL;
	}

	if (manager->pool_alloc && !removeonly)
	{
		wf_free(manager->allocated_segment);
		wf_free(manager->collisions_segment);

		for (uint32_t ialloc = 0; ialloc < (tilemap_size * 4); ialloc++)
		{
			cpShapeFree((cpShape*)manager->allocated_shape[ialloc]);
		}

		wf_free(manager->allocated_shape);
	}
}



void update_physics(physics_manager_t* const manager,float timestep)
{
	if(!manager->is_slave)
		cpSpaceStep(manager->world,timestep);
}

void physics_free(physics_manager_t* const manager)
{

	if(!manager->is_slave)
	{
		cpSpaceFree(manager->world);
		manager->world = NULL;
	}

}
