#include <inttypes.h>

#include <Render.h>

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <stdarg.h>

#include <Engine.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>
#include <Base/geom.h>

#include <Render/render_manager.h>

#include <Debug/Logprint.h>

#include <chipmunk/chipmunk.h>

#include <Utils/Hash.h>
#include <Utils/hashmap.h>

#include <Debug/geomdebug.h>

#include <Graphics/Light.h>
#include <Graphics/LightManager.h>

#include <GameObjects/Shaders.pb-c.h>
#include <Resx/shader_loader.h>
#include <GameObjects/Characters.pb-c.h>
#include <Graphics/TextureLoader.h>
#include <Graphics/Animation.h>
#include <Graphics/Sprite.h>

#include <Graphics/TextureRegion.h>
#include <Graphics/SpriteBatch.h>
#include <Graphics/FontLoader.h>

#include "GameObjects/Level.pb-c.h"


#ifndef EMSCRIPTEN
#include <portaudio.h>
#endif

#include <Sound/Sound.h>

#include <Resx/Resources.h>


#include "GameObjects/Level.pb-c.h"
#include <Collisions/Collisions.h>
#include <Graphics/TilemapLayer.h>
#include <GameObjects/Tilemap.h>


#include <Physics/Physics.h>
#include <Physics/physics_utils.h>

#include <Physics/physics_tilemap_utils.h>

void rebuild_tilemap_fromcache(physics_manager_t* const  manager, Tilemap_t* const ctmap, render_manager_t* const render_mngr, check_col_t check_col_func)
{
	if (!ctmap->layer_destructible)
	{
		logprint("No destructible layer!");
		return;
	}

	int i = 0;

	int16_t tile_row = 0;
	int16_t tile_col = 0;
	int32_t shape_index = 0;

	check_col_t check_col = &default_check_col;

	if (check_col_func != NULL)
	{
		check_col = check_col_func;
	}

	for (i = 0; i < (ctmap->col_count * ctmap->row_count); i++)
	{
		if (ctmap->layer_destructible->tileArray[i] != manager->tilemap_array[i])//check if we need changes
		{
			TilemapLayer_editTile(ctmap->layer_destructible, manager->tilemap_array[i], i, render_mngr, wtrue);//rebuild texture coords
		}

		//recreate missing collisions
		tile_row = (i / ctmap->col_count);//ligne Y
		tile_col = i - (ctmap->col_count * tile_row);//ligne X

		shape_index = (i * 4);

		if (!check_col_func(ctmap->collisions_array[i]) && ctmap->collisions_array[i] != NO_COLLISION && manager->collisions_array[i] != ctmap->collisions_array[i])
		{

			physics_set_shape_layer(manager, shape_index, LAYER_STATIC, manager->defaultwallmask);

			manager->collisions_array[i] = ctmap->collisions_array[i];
		}
	}
}


void build_tilemap_collisions(physics_manager_t* const  manager, int32_t* const destructible_data, const int32_t* const collisions_array, const int32_t col_count, const int32_t row_count, const int32_t tile_size, WorldRect_t dim,uint16_t num_chunk_col,uint16_t num_chunk_row, check_col_t check_col_func, check_col_t check_col_func_editable)
{

	int32_t tile_row = 0;
	int32_t tile_col = 0;
	int32_t shape_index = 0;

	manager->row_count = row_count;
	manager->col_count = col_count;

	uint32_t tilemap_size = (col_count * row_count);

	segment_t** temp_segment = NULL;

	if (!manager->pool_alloc)
	{
		//allocate size of the tilemap * 4 sides * size of cpshape pointer
		manager->static_shapes = (cpShape**)wf_calloc((tilemap_size * 4), sizeof(cpShape*));
		manager->callback_refs = (callback_ref_t*)wf_calloc((tilemap_size * 4), sizeof(callback_ref_t));

		//temporary segments info used to build collisions
		temp_segment = (segment_t**)wf_calloc((tilemap_size * 4), sizeof(segment_t*));

		//create a copy of the collisions array, using it to track change in the collisions ingame
		manager->collisions_array = (int32_t*)wf_calloc(tilemap_size, sizeof(int32_t));


		w_memcpy(manager->collisions_array, tilemap_size * sizeof(int32_t), collisions_array, tilemap_size * sizeof(int32_t));

		manager->tilemap_array = NULL;

		if (destructible_data != NULL)
		{
			//create a copy of the destructible tilemap array, using it to reset any change ingame
			manager->tilemap_array = (int32_t*)wf_calloc(tilemap_size, sizeof(int32_t));

			w_memcpy(manager->tilemap_array, tilemap_size * sizeof(int32_t), destructible_data, tilemap_size * sizeof(int32_t));
		}
	}
	else
	{
		temp_segment = manager->collisions_segment;
		w_memcpy(manager->collisions_array, tilemap_size * sizeof(int32_t), collisions_array, tilemap_size * sizeof(int32_t));

		if (destructible_data != NULL)
			w_memcpy(manager->tilemap_array, tilemap_size * sizeof(int32_t), destructible_data, tilemap_size * sizeof(int32_t));
	}


	uint32_t col_cut = col_count / num_chunk_col;

	uint32_t row_cut = row_count / num_chunk_row;


	// Create static collisions for the wall of our map
	for (uint32_t index = 0; index<tilemap_size; index++)
	{

		if (collisions_array[index] == 0)
		{
			temp_segment[shape_index++] = NULL;
			temp_segment[shape_index++] = NULL;
			temp_segment[shape_index++] = NULL;
			temp_segment[shape_index++] = NULL;
			continue;
		}

		tile_row = (index / col_count);//ligne Y
		tile_col = index - (col_count * tile_row);//ligne X

		cpCollisionType col_type = collisions_array[index];

		int32_t chunk_col = (int32_t)floorf((float)tile_col / col_cut);

		int32_t chunk_row = (int32_t)floorf((float)tile_row / row_cut);

		int32_t chunk_pos = (chunk_row * num_chunk_col) + chunk_col;

		int32_t local_index = convert_worldpos_to_tilepos(index, num_chunk_col, col_cut, row_cut);


		physics_create_collision_shape(manager, tile_col, tile_row, tile_size, shape_index, index, col_type, col_count, row_count, local_index, manager->collisions_array, temp_segment, dim, chunk_pos,check_col_func,check_col_func_editable,0);

		shape_index += 4;

	}

	cpShape* shape = NULL;
    cpBody* background = cpSpaceGetStaticBody(manager->world);

	//create collisions from segment informations
	for (uint32_t i = 0; i < (tilemap_size * 4); i++)
	{
		if (temp_segment[i] != NULL)
		{
			cpShape* shape_new = NULL;

			if (!manager->pool_alloc)
			{
				shape_new = cpSegmentShapeNew(background, temp_segment[i]->start_segment, temp_segment[i]->end_segment, 0.0f);
			}
			else
			{
				shape_new = (cpShape*)cpSegmentShapeInit(manager->allocated_shape[i], background, temp_segment[i]->start_segment, temp_segment[i]->end_segment, 0.0f);
			}
			

			shape = cpSpaceAddShape(manager->world, shape_new);
            cpShapeSetElasticity(shape,1.0f);
            cpShapeSetFriction(shape,1.0f);

			cpShapeFilter filter = cpShapeFilterNew(CP_NO_GROUP, LAYER_STATIC, manager->defaultwallmask);

            cpShapeSetFilter(shape,filter);

			cpShapeSetCollisionType(shape, temp_segment[i]->typ_col);
            cpShapeSetUserData(shape,temp_segment[i]->data);

			cpSegmentShapeSetNeighbors(shape, temp_segment[i]->neighbour1, temp_segment[i]->neighbour2);

			manager->static_shapes[i] = shape;

			if (!manager->pool_alloc)
				wf_free(temp_segment[i]);
		}


	}

	if (!manager->pool_alloc)
		wf_free(temp_segment);

	cpSpaceReindexStatic(manager->world);

}


static void update_single_tile(physics_manager_t* const  manager,const int32_t* const collisions_array,const int32_t index, int32_t shape_index,int32_t world_tile_row,int32_t world_tile_col,const int32_t tile_size, WorldRect_t dim, cpBody* background, uint16_t num_chunk_col, uint16_t num_chunk_row,uint16_t chunk_mem,check_col_t check_col_func,check_col_t check_col_func_editable)
{	
	//remove shape in range first
	for (int32_t update_index = shape_index; update_index < shape_index + 4; update_index++)
	{
		manager->collisions_segment[update_index] = NULL;

		if (manager->static_shapes[update_index] != NULL)
		{
			cpSpaceRemoveShape(manager->world, manager->static_shapes[update_index]);
			manager->static_shapes[update_index] = NULL;
		}
	}

	uint32_t col_cut = manager->col_count / num_chunk_col;

	uint32_t row_cut = manager->row_count / num_chunk_row;


	int32_t local_index = convert_worldpos_to_tilepos(index, num_chunk_col, col_cut, row_cut);



	cpCollisionType col_type = collisions_array[index];
	manager->collisions_array[index] = collisions_array[index];


	physics_create_collision_shape(manager, world_tile_col, world_tile_row, tile_size, shape_index, index, col_type, manager->col_count, manager->row_count, local_index, collisions_array, manager->collisions_segment, dim, chunk_mem, check_col_func, check_col_func_editable, num_chunk_col);

	for (int32_t update_index = shape_index; update_index < shape_index + 4; update_index++)
	{
		if (manager->collisions_segment[update_index] != NULL && manager->static_shapes[update_index] == NULL)
		{
			cpShape* shape_new = (cpShape*)cpSegmentShapeInit((cpSegmentShape*)manager->allocated_shape[update_index], background, manager->collisions_segment[update_index]->start_segment, manager->collisions_segment[update_index]->end_segment, 0.0f);

			cpShape* shape = cpSpaceAddShape(manager->world, shape_new);

            cpShapeSetElasticity(shape,1.0f);
            cpShapeSetFriction(shape,1.0f);

			cpShapeFilter filter = cpShapeFilterNew(CP_NO_GROUP, LAYER_STATIC, manager->defaultwallmask);

            cpShapeSetFilter(shape,filter);
			cpShapeSetCollisionType(shape, manager->collisions_segment[update_index]->typ_col);
            cpShapeSetUserData(shape, manager->collisions_segment[update_index]->data);

			cpSegmentShapeSetNeighbors(shape, manager->collisions_segment[update_index]->neighbour1, manager->collisions_segment[update_index]->neighbour2);

			manager->static_shapes[update_index] = shape;
		}
		 
		/*if (manager->collisions_segment[update_index] == NULL && manager->static_shapes[update_index] != NULL)
		{
			cpSpaceRemoveShape(manager->world, manager->static_shapes[update_index]);
			manager->static_shapes[update_index] = NULL;
		}*/
	}
}

//update a specific range of the collisions array, 
//destructible data is LOCAL, it only contains the updated data of the range
//collisions_array is GLOBAL, it contains the full collisions data, even outside of the range, since we need neighbouring data to generate collisions
//index_start, the first tile to update
//col_count the number of columns to update 
//row_count the number of row to update
//tile_size size in pixel of a tile
//index_jump the increment of the index per row 
void update_tilemap_ranged(physics_manager_t* const  manager, int32_t* const destructible_data, const int32_t* const collisions_array, WorldRect_t dim, int32_t index_start, int32_t col_count, int32_t row_count, const int32_t tile_size, const int32_t index_jump,int32_t shape_index,int32_t left_neigh_index,int32_t right_neigh_index,int32_t top_neigh_index,int32_t bottom_neigh_index,uint16_t num_chunk_col,uint16_t num_chunk_row,uint16_t chunk_mem,check_col_t check_col_func,check_col_t check_col_func_editable)
{
	if (!manager->pool_alloc)
	{
		logprint("update_tilemap_ranged is only usable when pool_alloc = wtrue, use  physics_free_tilemap_collisions then build_tilemap_collisions instead");
		return;
	}


	int32_t update_count = (col_count * row_count);

	int32_t updated = 0;

	int32_t index = index_start;
	//int32_t shape_index = index_start * 4;
	int32_t world_tile_row = 0;
	int32_t world_tile_col = 0;

	int32_t icol = 1;
	int32_t irow = 1;

	int32_t row_cpy = 0;
	int32_t index_cpy = index_start;
	int32_t src_cpy = 0;

    cpBody* background = cpSpaceGetStaticBody(manager->world);

	//num row / col on a single chunk
	uint32_t base_col = manager->col_count / num_chunk_col;

	uint32_t base_row = manager->row_count / num_chunk_row;


	//update per TILE
	while (updated < update_count)
	{


		world_tile_row = (index / manager->col_count);//ligne Y
		world_tile_col = index - (manager->col_count * world_tile_row);//ligne X

		update_single_tile(manager, collisions_array, index, shape_index, world_tile_row, world_tile_col, tile_size, dim, background, num_chunk_col, num_chunk_row, chunk_mem, check_col_func,check_col_func_editable);

		//update neighbouring tiles in different chunks

		//left side
		if (icol == 1 && world_tile_col > 0 && left_neigh_index != -1)
		{
			int32_t new_index = index - 1;
			int32_t new_tile_row = (new_index / manager->col_count);//ligne Y
			int32_t new_tile_col = new_index - (manager->col_count * new_tile_row);//ligne X

			int32_t shape_offset_index = ((irow - 1) * manager->col_count);

			int32_t new_shape_index = (left_neigh_index + shape_offset_index + (col_count - 1)) * 4;

			uint16_t chunk_pos = get_chunk_pos_from_tilepos(new_index, num_chunk_col, base_col, base_row);

			update_single_tile(manager, collisions_array, new_index, new_shape_index, new_tile_row, new_tile_col, tile_size, dim, background, num_chunk_col,num_chunk_row, chunk_pos, check_col_func, check_col_func_editable);
		}
		
		//bottom side
		if (irow == 1 && world_tile_row > 0 && bottom_neigh_index != -1)
		{
			int32_t new_index = ((index - index_jump) - col_count);

			int32_t new_tile_row = (new_index / manager->col_count);//ligne Y
			int32_t new_tile_col = new_index - (manager->col_count * new_tile_row);//ligne X

			int32_t shape_offset_index = (manager->col_count * (row_count -1)) + (icol - 1);

			int32_t new_shape_index = (bottom_neigh_index + shape_offset_index) * 4;

			uint16_t chunk_pos = get_chunk_pos_from_tilepos(new_index, num_chunk_col, base_col, base_row);

			update_single_tile(manager, collisions_array, new_index, new_shape_index, new_tile_row, new_tile_col, tile_size, dim, background, num_chunk_col, num_chunk_row, chunk_pos, check_col_func, check_col_func_editable);
		}

		//right side
		if (icol >= col_count && world_tile_col < manager->col_count - 1 && right_neigh_index != -1)
		{
			int32_t new_index = index + 1;
			int32_t new_tile_row = (new_index / manager->col_count);//ligne Y
			int32_t new_tile_col = new_index - (manager->col_count * new_tile_row);//ligne X

			int32_t shape_offset_index = ((irow - 1) * manager->col_count);

			int32_t new_shape_index = (right_neigh_index + shape_offset_index) * 4;

			uint16_t chunk_pos = get_chunk_pos_from_tilepos(new_index, num_chunk_col, base_col, base_row);

			update_single_tile(manager, collisions_array, new_index, new_shape_index, new_tile_row, new_tile_col, tile_size, dim, background, num_chunk_col, num_chunk_row, chunk_pos, check_col_func, check_col_func_editable);
		}

		//top side
		if (irow == row_count && world_tile_row < manager->row_count - 1 && top_neigh_index != -1)
		{
			int32_t new_index = (index + index_jump) + col_count;

			int32_t new_tile_row = (new_index / manager->col_count);//ligne Y
			int32_t new_tile_col = new_index - (manager->col_count * new_tile_row);//ligne X

			int32_t shape_offset_index = (icol-1);

			int32_t new_shape_index = (top_neigh_index + shape_offset_index) * 4;

			uint16_t chunk_pos = get_chunk_pos_from_tilepos(new_index, num_chunk_col, base_col, base_row);

			update_single_tile(manager, collisions_array, new_index, new_shape_index, new_tile_row, new_tile_col, tile_size, dim, background, num_chunk_col, num_chunk_row, chunk_pos, check_col_func, check_col_func_editable);
		}

		


		if (icol < col_count)
		{
			index++;
			shape_index += 4;
			icol++;
		}
		else
		{
			index += (index_jump + 1);
			shape_index += (index_jump * 4) + 4;
			icol = 1;
			irow++;
		}

		updated++;
	}




	//cpSpaceReindexStatic(manager->world);

	//update destructible data
	if (destructible_data != NULL)
	{
		while (row_cpy++ < row_count)
		{
			//w_memcpy(&manager->collisions_array[index_cpy], col_count * sizeof(int32_t), &collisions_array[src_cpy], col_count * sizeof(int32_t));


			w_memcpy(&manager->tilemap_array[index_cpy], col_count * sizeof(int32_t), &destructible_data[src_cpy], col_count * sizeof(int32_t));

			index_cpy += (col_count + index_jump);
			src_cpy += col_count;
		}
	}
	

}
