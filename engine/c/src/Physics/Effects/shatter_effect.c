#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <stdlib.h>
#include <inttypes.h>
#include <math.h> 
#include <time.h>

#if defined(_WIN32)
    #include <Windows.h>
#endif


#if defined (__GNUC__)
#include <alloca.h>
#endif

#include <Engine.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>
#include <Base/geom.h>

#include <Utils/Hash.h>

#include <Debug/Logprint.h>

#include <Render.h>

#include <Render/render_manager.h>

#include <png.h>
#include <Graphics/TextureLoader.h>

#include <Graphics/compound_sprite.h>

#include <chipmunk/chipmunk.h>
#include <Physics/convert.h>
#include <Scripting/ScriptPointers.h>


#include <Physics/Effects/shatter_effect.h>


void shatter_effect_init(shatter_effect_t* const oshatter,const int32_t effect_width,const int32_t effect_height,texture_info* const base_texture,const int32_t tile_size,render_manager_t* const render_mngr,int collision_type)
{
	compound_sprite_init(&oshatter->shattered_sprite,effect_width,effect_height,base_texture,tile_size,render_mngr);
	oshatter->cleanup_delay = CLEANUP_DELAY;
	oshatter->status = 0;
	oshatter->shatter_collision_type = collision_type;
	oshatter->sprite_onlymode = wfalse;
	oshatter->script_id = script_registerpointer((intptr_t)oshatter, SHATTER_EFFECT);
} 

void shatter_effect_draw(shatter_effect_t* const oshatter,render_manager_t* const render_mngr)
{
	if(oshatter->status)
		compound_sprite_draw(&oshatter->shattered_sprite,render_mngr);
}


static inline cpVect HashVect(uint32_t x, uint32_t y, uint32_t seed)
{
//	cpFloat border = 0.21f;
	cpFloat border = 0.05f;
	uint32_t h = (x*1640531513 ^ y*2654435789) + seed;
	
	return cpv(
		cpflerp(border, 1.0f - border, (cpFloat)(      h & 0xFFFF)/(cpFloat)0xFFFF),
		cpflerp(border, 1.0f - border, (cpFloat)((h>>16) & 0xFFFF)/(cpFloat)0xFFFF)
	);
}

static cpVect shatter_effect_WorleyPoint(int i, int j, WorleyContex *context)
{
	cpFloat size = context->cellSize;
	int width = context->width;
	int height = context->height;
	cpBB bb = context->bb;
	
	cpVect fv = HashVect(i, j, context->seed);
	
	return cpv(
		cpflerp(bb.l, bb.r, 0.5f) + size*(i + fv.x -  width*0.5f),
		cpflerp(bb.b, bb.t, 0.5f) + size*(j + fv.y - height*0.5f)
	);
}

static int shatter_effect_ClipCell(cpShape *shape, cpVect center, int i, int j, WorleyContex* context, cpVect *verts, cpVect *clipped, int count)
{
	cpVect other = shatter_effect_WorleyPoint(i, j, context);

    if(cpShapePointQuery(shape, other, NULL) > 0.0f){
		memcpy(clipped, verts, count*sizeof(cpVect));
		return count;
    }
	
	cpVect n = cpvsub(other, center);
	cpFloat dist = cpvdot(n, cpvlerp(center, other, 0.5f));
	
	int clipped_count = 0;
	for(int jv=0, iv=count-1; jv<count; iv=jv, jv++){
		cpVect a = verts[iv];
		cpFloat a_dist = cpvdot(a, n) - dist;
		
		if(a_dist <= 0.0){
			clipped[clipped_count] = a;
			clipped_count++;
		}
		
		cpVect b = verts[jv];
		cpFloat b_dist = cpvdot(b, n) - dist;
		
		if(a_dist*b_dist < 0.0f){
			cpFloat t = cpfabs(a_dist)/(cpfabs(a_dist) + cpfabs(b_dist));
			
			clipped[clipped_count] = cpvlerp(a, b, t);
			clipped_count++;
		}
	}
	
	return clipped_count;
}


static void shatter_effect_UpdateSpritePos(cpBody *body, cpVect gravity, cpFloat damping, cpFloat dt)
{
    int16_t id = ((csprite_pointer_t*)cpBodyGetUserData(body))->shape_id;

    compound_sprite_t* sprite = (compound_sprite_t*)((csprite_pointer_t*)cpBodyGetUserData(body))->sprite;

    cpVect pos = cpBodyGetPosition(body);

    Vector_t position = {(float)pos.x,(float)pos.y};
    cpFloat rotation = cpBodyGetAngle(body);

	compound_sprite_updateshape(sprite,position,(float)rotation,id);

    if(((csprite_pointer_t*)cpBodyGetUserData(body))->gravity_inverted)
	{
		gravity.x = -gravity.x;
		gravity.y = -gravity.y;
	}

	cpBodyUpdateVelocity(body,gravity,damping,dt);
}

void shatter_effect_ShatterCell(cpSpace *space, cpShape *shape, cpVect cell, int cell_i, int cell_j, WorleyContex* context,float rotation,wbool sprite_onlymode,wbool gravity_inverted)
{
	cpBody *body = cpShapeGetBody(shape);

	cpVect *ping = (cpVect *)alloca(SHE_MAX_VERTEXES_PER_VORONOI * sizeof(cpVect));
	cpVect *pong = (cpVect *)alloca(SHE_MAX_VERTEXES_PER_VORONOI * sizeof(cpVect));

	int count = cpPolyShapeGetCount(shape);
	count = (count > SHE_MAX_VERTEXES_PER_VORONOI ? SHE_MAX_VERTEXES_PER_VORONOI : count);

	for (int i = 0; i<count; i++) {
		ping[i] = cpBodyLocalToWorld(body, cpPolyShapeGetVert(shape, i));
	}

	for (int i = 0; i<context->width; i++) {
		for (int j = 0; j<context->height; j++) {
			if (
				!(i == cell_i && j == cell_j) &&
				cpShapePointQuery(shape, cell, NULL) < 0.0f
				) {
				count = shatter_effect_ClipCell(shape, cell, i, j, context, ping, pong, count);
				memcpy(ping, pong, count * sizeof(cpVect));
			}
		}
	}

	cpVect centroid = cpCentroidForPoly(count, ping);

	if (rotation != 0)
	{
		cpVect bpoint = cpBodyGetPosition(body);

		float radius = (float)cpvdist(bpoint, centroid);

		centroid.x = radius * cosf(rotation) + bpoint.x;
		centroid.y = radius * sinf(rotation) + bpoint.y;
	}

	cpFloat mass = cpAreaForPoly(count, ping, 0.0f)*SHE_DENSITY;
	cpFloat moment = cpMomentForPoly(mass, count, ping, cpvneg(centroid), 0.0f);

	cpBody* shpbody = cpShapeGetBody(shape);
	shatter_effect_t* sfx = (shatter_effect_t*)cpBodyGetUserData(shpbody);

	sfx->frag_list[sfx->frag_count] = cpSpaceAddBody(space, cpBodyNew(mass, moment));
	cpBodySetPosition(sfx->frag_list[sfx->frag_count], centroid);
	cpBodySetVelocity(sfx->frag_list[sfx->frag_count], cpBodyGetVelocityAtWorldPoint(body, centroid));
	cpBodySetAngularVelocity(sfx->frag_list[sfx->frag_count], cpBodyGetAngularVelocity(body));
	cpBodySetAngle(sfx->frag_list[sfx->frag_count], deg2rad(rotation));

	cpTransform transform = cpTransformTranslate(cpvneg(centroid));

	sfx->shape_frag_list[sfx->frag_count] = cpSpaceAddShape(space, cpPolyShapeNew(sfx->frag_list[sfx->frag_count], count, ping, transform, 0.0f));

	cpShapeSetCollisionType(sfx->shape_frag_list[sfx->frag_count], sfx->shatter_collision_type);

	// Copy whatever properties you have set on the original shape that are important
	cpShapeSetFriction(sfx->shape_frag_list[sfx->frag_count], cpShapeGetFriction(shape));

	float** float_verts = (float**)alloca(sizeof(float*) * count);

	for (int i = 0; i <count; i++)
		float_verts[i] = (float*)alloca(sizeof(float) * 2);

	//chipmunk give us the vertices in CW order, we render in CCW order, invert vertices order
	int invert = count - 1;
	for (int i = 0; i<count; i++) {
		ping[i] = cpPolyShapeGetVert(sfx->shape_frag_list[sfx->frag_count], invert--);
	}
	

	convert_cpvect_to_float(ping, float_verts, count);

	Vector_t position = { (float)centroid.x,(float)centroid.y };

	//Vector_t position = vectorzero;

	csprite_pointer_t* pnt = compound_sprite_addshape(&sfx->shattered_sprite, float_verts, count, position, rotation, gravity_inverted);
	cpBodySetUserData(sfx->frag_list[sfx->frag_count], pnt);

	if (!sprite_onlymode)
		cpBodySetVelocityUpdateFunc(sfx->frag_list[sfx->frag_count], shatter_effect_UpdateSpritePos);

	sfx->frag_count++;

}


static void shatter_effect_ShatterShape(cpSpace *space, cpShape *shape, cpFloat cellSize, cpVect focus,render_manager_t* const render_mngr,float rotation,wbool mirror_x,wbool mirror_y,wbool sprite_onlymode,wbool invert_gravity)
{
	cpSpaceRemoveShape(space, shape);
    cpBody* bod =  cpShapeGetBody(shape);
    shatter_effect_t* sfx = (shatter_effect_t*)cpBodyGetUserData(bod);
     cpVect bodpos = cpBodyGetPosition(bod);
    cpSpaceRemoveBody(space,bod);


	compound_sprite_clearshape(&sfx->shattered_sprite);
	
	cpBB bb = cpShapeGetBB(shape);
	int width = (int)((bb.r - bb.l)/cellSize + 1);
	int height = (int)((bb.t - bb.b)/cellSize + 1);
	WorleyContex context = {(uint32_t)rand(), cellSize, width, height, bb, focus};
	
	for(int i=0; i<context.width; i++){
		for(int j=0; j<context.height; j++){
			cpVect cell = shatter_effect_WorleyPoint(i, j, &context);
            if(cpShapePointQuery(shape, cell, NULL) < 0.0f){
				//shatter cell only if we arent' over max number of shape
				if(sfx->frag_count < COMPOUND_MAX_SHAPE)
					shatter_effect_ShatterCell(space, shape, cell, i, j, &context,rotation,sprite_onlymode,invert_gravity);
				else
					break;
            }
		}
	}



    compound_sprite_compute_textcoord(&sfx->shattered_sprite,(float)bodpos.x,(float)bodpos.y,sfx->shattered_sprite.tile_index,wtrue,mirror_x,mirror_y,render_mngr);
	
    cpBodyFree(bod);
	cpShapeFree(shape);
}

void shatter_effect_updatesize(shatter_effect_t* const oshatter,const int32_t sprite_width,const int32_t sprite_height,const int32_t tile_size)
{
	compound_sprite_update_size(&oshatter->shattered_sprite,sprite_width,sprite_height,tile_size);
}

void shatter_effect_updatetexture(shatter_effect_t* const oshatter,texture_info* const new_texture)
{
	compound_sprite_updatetexture(&oshatter->shattered_sprite,new_texture);
}

void shatter_effect_create(shatter_effect_t* const oshatter,const int32_t tile_index,cpVect* base_vertices,int16_t size,cpSpace* const world,float mass,cpVect pos,cpVect impactPoint,float cellSize,cpVect shatter_velocity,cpFloat rotation,wbool mirror_x,wbool mirror_y,render_manager_t* const render_mngr,wbool sprite_onlymode,wbool invert_gravity)
{
	if(oshatter->status)
	{
		logprint("Effect already active!");
		return;
	}

	if(cellSize <= 5.0f)
	{
		logprint("Can't shatter a sprite with a cell size less than or egal to 5!");
		return;
	}


	float** float_verts = (float**)alloca(sizeof(float*) * size);

	for(int i = 0; i <size; i++)
	{
		float_verts[i] = (float*)alloca(sizeof(float) * 2);
	}

	convert_cpvect_to_float(base_vertices,float_verts,size);


	cpFloat moment = cpMomentForBox(mass, oshatter->shattered_sprite.width, oshatter->shattered_sprite.height);

	cpBody* base_body = cpSpaceAddBody(world, cpBodyNew(mass, moment));
    cpBodySetPosition(base_body ,pos);
	cpBodySetUserData(base_body,oshatter);
    cpBodySetVelocity(base_body,shatter_velocity);


    cpShape* base_shape = cpSpaceAddShape(world, cpBoxShapeNew(base_body,  oshatter->shattered_sprite.width, oshatter->shattered_sprite.height,0.0f));
	cpShapeSetFriction(base_shape, 0.6f);
	cpShapeSetCollisionType(base_shape,oshatter->shatter_collision_type);

	oshatter->shattered_sprite.tile_index = tile_index;


	oshatter->sprite_onlymode = sprite_onlymode;

	shatter_effect_ShatterShape(world, base_shape, cellSize, impactPoint,render_mngr,(float)rad2deg((wfloat)rotation),mirror_x,mirror_y, sprite_onlymode,invert_gravity);

	//apply an impulse to all the body
	//for(int32_t i = 0; i < oshatter->frag_count;i++)
		//cpBodyApplyImpulse(oshatter->frag_list[i],cpvmult(shatter_direction,IMPULSE_POWER),cpvzero);

	oshatter->status = 1;

}

void shatter_effect_update(shatter_effect_t* const oshatter,float delta,cpSpace* const world)
{
	if(!oshatter->status)
		return;

	if(!oshatter->sprite_onlymode)
	{
		if(oshatter->shattered_sprite.alpha_value > 0.0f)
		{
			oshatter->cleanup_delay -= delta;

			if(oshatter->cleanup_delay <= 0)
			{
				oshatter->shattered_sprite.alpha_value -= ALPHA_STEP;
				oshatter->cleanup_delay = CLEANUP_DELAY;
			}
		}
		else
		{
			shatter_effect_reinit(oshatter,world);
		}
	}

}

void shatter_effect_reinit(shatter_effect_t* const oshatter,cpSpace* const world)
{
	//destroy all bodies and shape, cleanup sprite, reset effect
	compound_sprite_clearshape(&oshatter->shattered_sprite);

	if (oshatter->script_id != 0)
	{
		script_unregisterpointer(oshatter->script_id);
		oshatter->script_id = 0;
	}


	for(int32_t i = 0; i < oshatter->frag_count;i++)
	{
		cpSpaceRemoveBody(world,oshatter->frag_list[i]);
		cpSpaceRemoveShape(world,oshatter->shape_frag_list[i]);

		cpBodyFree(oshatter->frag_list[i]);
		cpShapeFree(oshatter->shape_frag_list[i]);

		oshatter->frag_list[i] = NULL;
		oshatter->shape_frag_list[i] = NULL;
	}

	oshatter->frag_count = 0;

	oshatter->shattered_sprite.alpha_value = 1.0f;
	oshatter->status = 0;
	oshatter->cleanup_delay = CLEANUP_DELAY;
}
