#include <inttypes.h>

#include <Render.h>

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <stdarg.h>

#include <Engine.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>
#include <Base/geom.h>

#include <Debug/Logprint.h>

#include <chipmunk/chipmunk.h>


#include <Physics/physics_utils.h>


Vector_t convert_cpvec_tovec(cpVect vec)
{
	return Vec((wfloat)vec.x, (wfloat)vec.y);
}

Vector_t convert_cpfloatpair_tovec(cpFloat x, cpFloat y)
{
	return Vec((wfloat)x, (wfloat)y);
}

static wbool default_check_col(int32_t collision_value)
{
	if (collision_value == COLLISION)
		return wtrue;

	return wfalse;
}

edge_container_t* alloc_edges_list(const int32_t row_count, const int32_t col_count, edge_container_t* dst_edge_array, int32_t* const edge_array_size, wbool regen, edge_t**  preallocated_edge_arrayp)
{
	if (dst_edge_array != NULL)
	{
		if (!regen)
			logprint("Warning: edge list already exist, old list will be erased");

		wf_free(dst_edge_array);
		dst_edge_array = NULL;

	}

	(*edge_array_size) = row_count * col_count * 4;


	edge_container_t* segment = (edge_container_t*)wf_calloc((*edge_array_size), sizeof(edge_container_t));

	if ((*preallocated_edge_arrayp) != NULL)
		wf_free((*preallocated_edge_arrayp));

	(*preallocated_edge_arrayp) = (edge_t*)wf_calloc((*edge_array_size), sizeof(edge_t));

	dst_edge_array = segment;

	return dst_edge_array;
}

void gen_edges_list(const int32_t row_count,const int32_t col_count,const int32_t tile_size,const int32_t* const src_collisions_array,edge_container_t* const segment,edge_t**  preallocated_edge_arrayp, check_col_t check_col_func, Vector_t edge_offset)
{
	if (src_collisions_array == NULL)
	{
		logprint("Can't generate edge list with no collisions!");
		return;
	}

	int32_t i_col = 0;
	int32_t shape_index = 0;

	check_col_t check_col = &default_check_col;

	if (check_col_func != NULL)
	{
		check_col = check_col_func;
	}

	int32_t YLimit = row_count - 1;
	int32_t XLimit = col_count - 1;


	edge_t* preallocated_edge_array = (*preallocated_edge_arrayp);


	for (int32_t tile_row = 0; tile_row < row_count; tile_row++)
	{
		for (int32_t tile_col = 0; tile_col < col_count; tile_col++)
		{
			int32_t col_type = src_collisions_array[i_col];

			//if col_type is slope, create only one shape
			if (col_type == COLLISION_SLOPE || col_type == COLLISION_SLOPE_INVERSE)
			{
				Vector_t slopeA = vectorzero, slopeB = vectorzero;

				if (col_type == COLLISION_SLOPE)
				{
					slopeA = Vec((wfloat)(tile_col * tile_size) + tile_size, (wfloat)tile_row * tile_size);
					slopeB = Vec(slopeA.x - tile_size, slopeA.y + tile_size);
				}
				else if (col_type == COLLISION_SLOPE_INVERSE)
				{
					slopeA = Vec((wfloat)tile_col * tile_size, (wfloat)tile_row * tile_size);
					slopeB = Vec(slopeA.x + tile_size, slopeA.y + tile_size);
				}


				edge_t* edge_ref = &preallocated_edge_array[shape_index];

				edge_ref->start = slopeA;
				edge_ref->end = slopeB;
				edge_ref->type = e_slope;

				segment[shape_index].p_edge = edge_ref;
				segment[shape_index].is_src = wtrue;
				segment[shape_index].row_num = tile_row;
				segment[shape_index].col_num = tile_col;

				shape_index += 4;
			}
			else if (check_col(col_type))
			{
				//check for neighbour, add shape only if tile empty of tile type is different, expand shape otherwise
				wbool create_lower = wtrue;
				wbool create_upper = wtrue;
				wbool create_left = wtrue;
				wbool create_right = wtrue;

				//corner order, 1 = top left, 2 = top right, 3 = bottom right, 4 = bottom left
				wbool has_corner_1 = wfalse;
				wbool has_corner_2 = wfalse;
				wbool has_corner_3 = wfalse;
				wbool has_corner_4 = wfalse;

				int32_t bottom_shape_index = shape_index;
				int32_t right_shape_index = shape_index + 1;
				int32_t top_shape_index = shape_index + 2;
				int32_t left_shape_index = shape_index + 3;

				//check upper neighbour
				if (tile_row < YLimit && check_col(src_collisions_array[((tile_row + 1) * col_count) + tile_col]))
				{
					create_upper = wfalse;
				}

				//check top left corner
				has_corner_1 = (wbool)(tile_row < YLimit && tile_col > 0 && check_col(src_collisions_array[((tile_row + 1) * col_count) + (tile_col - 1)]));

				//check right neighbour
				if (tile_col < XLimit && check_col(src_collisions_array[(tile_row * col_count) + (tile_col + 1)]))
				{
					create_right = wfalse;
				}

				//check top right corner
				has_corner_2 = (wbool)(tile_row < YLimit && tile_col < XLimit &&  check_col(src_collisions_array[((tile_row + 1) * col_count) + (tile_col + 1)]));

				//check lower neighbour
				if (tile_row > 0 && check_col(src_collisions_array[((tile_row - 1) * col_count) + tile_col]))
				{
					create_lower = wfalse;
				}

				//check bottom right corner
				has_corner_3 = (wbool)(tile_row > 0 && tile_col < XLimit &&  check_col(src_collisions_array[((tile_row - 1) * col_count) + (tile_col + 1)]));

				//check left neighbour
				if (tile_col > 0 && check_col(src_collisions_array[(tile_row * col_count) + (tile_col - 1)]))
				{
					create_left = wfalse;
				}

				//check bottom left corner
				has_corner_4 = (wbool)(tile_row > 0 && tile_col > 0 && check_col(src_collisions_array[((tile_row - 1) * col_count) + (tile_col - 1)]));



				Vector_t bottomsideA = Vec(((wfloat)tile_col * tile_size) + edge_offset.x, ((wfloat)tile_row * tile_size) + edge_offset.y);
				Vector_t bottomsideB = Vec(bottomsideA.x + tile_size, bottomsideA.y);

				Vector_t rightsideA = Vec(bottomsideB.x, bottomsideB.y);
				Vector_t rightsideB = Vec(rightsideA.x, rightsideA.y + tile_size);

				Vector_t topsideA = Vec(bottomsideA.x, rightsideB.y);
				Vector_t topsideB = Vec(rightsideB.x, rightsideB.y);

				Vector_t leftsideA = Vec(bottomsideA.x, bottomsideA.y);
				Vector_t leftsideB = Vec(topsideA.x, topsideB.y);


				if (create_lower)
				{
					if (create_left || has_corner_4)
					{
						edge_t* edge_ref = &preallocated_edge_array[bottom_shape_index];

						edge_ref->start = bottomsideA;
						edge_ref->end = bottomsideB;
						edge_ref->type = e_bottom;
						segment[bottom_shape_index].p_edge = edge_ref;
						segment[bottom_shape_index].is_src = wtrue;
					}
					else
					{
						int32_t prev_indx = (bottom_shape_index - 4);

						segment[prev_indx].p_edge->end = bottomsideB;
						segment[bottom_shape_index].p_edge = segment[prev_indx].p_edge;
						segment[bottom_shape_index].is_src = wfalse;
					}
				}

				segment[shape_index].row_num = tile_row;
				segment[shape_index].col_num = tile_col;

				shape_index++;

				if (create_right)
				{
					if (create_lower || has_corner_3)
					{
						edge_t* edge_ref = &preallocated_edge_array[right_shape_index];

						edge_ref->start = rightsideA;
						edge_ref->end = rightsideB;
						edge_ref->type = e_right;
						segment[right_shape_index].p_edge = edge_ref;
						segment[right_shape_index].is_src = wtrue;
					}
					else
					{
						int32_t prev_indx = (right_shape_index - (col_count * 4));

						segment[prev_indx].p_edge->end = rightsideB;
						segment[right_shape_index].p_edge = segment[prev_indx].p_edge;
						segment[right_shape_index].is_src = wfalse;

					}
				}


				segment[shape_index].row_num = tile_row;
				segment[shape_index].col_num = tile_col;
				shape_index++;

				if (create_upper)
				{
					if (create_left || has_corner_1)//we are at the beginning of a bloc
					{
						edge_t* edge_ref = &preallocated_edge_array[top_shape_index];

						edge_ref->start = topsideA;
						edge_ref->end = topsideB;
						edge_ref->type = e_top;
						segment[top_shape_index].p_edge = edge_ref;
						segment[top_shape_index].is_src = wtrue;
					}
					else //we have a neighbour on our left, expand it upper shape rather than creating a new one
					{
						int32_t prev_indx = (top_shape_index - 4);

						segment[prev_indx].p_edge->end = topsideB;
						segment[top_shape_index].p_edge = segment[prev_indx].p_edge;
						segment[top_shape_index].is_src = wfalse;
					}
				}

				segment[shape_index].row_num = tile_row;
				segment[shape_index].col_num = tile_col;
				shape_index++;

				if (create_left)
				{
					if (create_lower || has_corner_4)
					{
						edge_t* edge_ref = &preallocated_edge_array[left_shape_index];

						edge_ref->start = leftsideA;
						edge_ref->end = leftsideB;
						edge_ref->type = e_left;
						segment[left_shape_index].p_edge = edge_ref;
						segment[left_shape_index].is_src = wtrue;
					}
					else
					{
						int32_t prev_indx = (left_shape_index - (col_count * 4));

						segment[prev_indx].p_edge->end = leftsideB;
						segment[left_shape_index].p_edge = segment[prev_indx].p_edge;
						segment[left_shape_index].is_src = wfalse;
					}
				}

				segment[shape_index].row_num = tile_row;
				segment[shape_index].col_num = tile_col;
				shape_index++;


			}
			else
			{
				shape_index += 4;
			}

			i_col++;
		}
	}
}

//chaque case du tableau contient soit l'adresse du segment de collision (m�me si le segment est pr�sent sur plusieurs case) soit null (pas de collision)
//ce qui permet de r�cup�rer les bon segment directement via la position de la tile
//update neighbour edge given a tilepos
void update_edges(const int32_t col_count,const int32_t row_count,const int32_t tile_size,edge_container_t* const edge_array,const int32_t edge_array_size,const int32_t* const collisions_array,int32_t tilePos, wbool add_tile, edge_t*  preallocated_edge_array, Vector_t edge_offset)
{
	if (edge_array == NULL)
	{
		logprint("No edge array generated! please call Tilemap_gen_edge_list before updating edge");
		return;
	}

	if (add_tile && collisions_array[tilePos] != NO_COLLISION)
		return;

	if (!add_tile && collisions_array[tilePos] == NO_COLLISION)
		return;

	//-1- find the matching existing edges for all four segment
	int32_t tRow = (int32_t)floorf((float)(tilePos / col_count));//ligne Y
	int32_t tCol = tilePos - (col_count * tRow);//ligne X

	int32_t x_tile = (tCol * tile_size) + (int32_t)edge_offset.x;
	int32_t y_tile = (tRow * tile_size) + (int32_t)edge_offset.y;

	edge_t tile_edges[] =
	{
		{ { (wfloat)x_tile,(wfloat)y_tile },{ (wfloat)x_tile + tile_size,(wfloat)y_tile },e_bottom },
		{ { (wfloat)x_tile + tile_size,(wfloat)y_tile },{ (wfloat)x_tile + tile_size,(wfloat)y_tile + tile_size },e_right },
		{ { (wfloat)x_tile,(wfloat)y_tile + tile_size },{ (wfloat)x_tile + tile_size,(wfloat)y_tile + tile_size },e_top },
		{ { (wfloat)x_tile,(wfloat)y_tile },{ (wfloat)x_tile,(wfloat)y_tile + tile_size },e_left }
	};

	//int32_t inverse_value[] = {2,,

	//edge case handling, when moving on one row (from one column to another) check that we stay on the same row all the time to avoid
	//"wrap around" effects (unless when we use edge pointer as a condition)

	//-2- change edges according to the position of the matching edge
	for (int32_t iec = 0; iec < 4; iec++)
	{

		edge_t* match = NULL;
		int32_t match_index = (tilePos * 4) + iec;

		if (!add_tile)
			match = edge_array[match_index].p_edge;
		else
		{
			switch (tile_edges[iec].type)
			{
			case e_bottom:
				if (tRow > 0)
				{
					match = edge_array[((tilePos - col_count) * 4) + 2].p_edge;
					match_index = ((tilePos - col_count) * 4) + 2;
				}
				break;
			case e_top:
				if (tRow < row_count - 1)
				{
					match = edge_array[((tilePos + col_count) * 4)].p_edge;
					match_index = ((tilePos + col_count) * 4);
				}
				break;
			case e_left:
				if (tCol > 0)
				{
					match = edge_array[((tilePos - 1) * 4) + 1].p_edge;
					match_index = ((tilePos - 1) * 4) + 1;
				}
				break;
			case e_right:
				if (tCol < col_count - 1)
				{
					match = edge_array[((tilePos + 1) * 4) + 3].p_edge;
					match_index = ((tilePos + 1) * 4) + 3;
				}
				break;
			case e_slope:
				break;
			}
		}



		if (match != NULL)
		{

			if (match && (match->type == e_bottom || match->type == e_top))
			{
				//the segment equal our tile
				if ((match->end.x - match->start.x) == tile_size)
				{

				}
				else
				{
					//start of the segment
					if (match->start.x == tile_edges[iec].start.x)
					{
						match->start.x += tile_size;

						//change the src value for the next tile
						int32_t next_tile = match_index + 4;

						if (next_tile < edge_array_size && edge_array[next_tile].p_edge == match)
							edge_array[next_tile].is_src = wtrue;

					}
					else if (match->end.x == tile_edges[iec].end.x)//end of the segment
					{
						match->end.x -= tile_size;
					}
					else //the tile is in the middle of the segment
					{
						wfloat old_end = match->end.x;
						match->end.x = tile_edges[iec].start.x;

						//affect the new edge to cells to our right
						int32_t reaffect_indx = match_index + 4;

						edge_t* new_edge = &preallocated_edge_array[reaffect_indx];
						

						new_edge->start.x = tile_edges[iec].end.x;
						new_edge->start.y = match->start.y;
						new_edge->end.x = old_end;
						new_edge->end.y = match->end.y;
						new_edge->type = match->type;


						wbool first_cell = wtrue;

						while (reaffect_indx < edge_array_size && edge_array[reaffect_indx].p_edge == match)
						{
							edge_array[reaffect_indx].p_edge = new_edge;
							edge_array[reaffect_indx].is_src = first_cell;

							if (first_cell)
								first_cell = wfalse;

							reaffect_indx += 4;
						}

					}
				}
			}
			else if (match && (match->type == e_left || match->type == e_right))
			{
				//the segment equal our tile
				if ((match->end.y - match->start.y) == tile_size)
				{

				}
				else
				{
					//start of the segment
					if (match->start.y == tile_edges[iec].start.y)
					{
						match->start.y += tile_size;

						//change the src value for the next tile
						int32_t next_tile = match_index + (col_count * 4);

						if (next_tile < edge_array_size && edge_array[next_tile].p_edge == match)
							edge_array[next_tile].is_src = wtrue;
					}
					else if (match->end.y == tile_edges[iec].end.y)//end of the segment
						match->end.y -= tile_size;
					else //the tile is in the middle of the segment
					{
						wfloat old_end = match->end.y;
						match->end.y = tile_edges[iec].start.y;

						//affect the new edge to cells at the top
						int32_t reaffect_indx = match_index + (col_count * 4);

						edge_t* new_edge = &preallocated_edge_array[reaffect_indx];


						new_edge->start.y = tile_edges[iec].end.y;
						new_edge->start.x = match->start.x;
						new_edge->end.y = old_end;
						new_edge->end.x = match->end.x;
						new_edge->type = match->type;



						wbool first_cell = wtrue;

						while (reaffect_indx < edge_array_size && edge_array[reaffect_indx].p_edge == match)
						{
							edge_array[reaffect_indx].p_edge = new_edge;
							edge_array[reaffect_indx].is_src = first_cell;

							if (first_cell)
								first_cell = wfalse;

							reaffect_indx += (col_count * 4);
						}

					}
				}
			}
		}
		else //no edges exist, we must modify adjacent edges (if any)
		{

			edge_t* adjacent_1 = NULL;
			edge_t* adjacent_2 = NULL;
			int32_t index_add = 0;

			edge_type adj = tile_edges[iec].type;
			int32_t affect_pos = (tilePos * 4) + iec;

			int32_t adj_pos1 = (tilePos * 4) + iec;
			int32_t adj_pos2 = (tilePos * 4) + iec;

			if (!add_tile)
			{

				wbool seek_adj1 = wtrue;
				wbool seek_adj2 = wtrue;

				switch (tile_edges[iec].type)
				{
				case e_bottom:
					adj_pos1 = (((tilePos - 1) - col_count) * 4) + 2;
					adj_pos2 = (((tilePos + 1) - col_count) * 4) + 2;

					affect_pos = ((tilePos - col_count) * 4) + 2;
					adj = e_top;

					index_add = 4;
					seek_adj1 = (wbool)(tRow > 0 && tCol > 0);
					seek_adj2 = (wbool)(tRow > 0 && tCol < col_count - 1);

					break;
				case e_top:

					adj_pos1 = (((tilePos - 1) + col_count) * 4);
					adj_pos2 = (((tilePos + 1) + col_count) * 4);

					adj = e_bottom;
					affect_pos = ((tilePos + col_count) * 4);

					index_add = 4;
					seek_adj1 = (wbool)(tRow < row_count - 1 && tCol > 0);
					seek_adj2 = (wbool)(tRow < row_count - 1 && tCol < col_count - 1);
					break;
				case e_left:

					adj_pos1 = (((tilePos - 1) - col_count) * 4) + 1;
					adj_pos2 = (((tilePos - 1) + col_count) * 4) + 1;

					adj = e_right;
					affect_pos = ((tilePos - 1) * 4) + 1;

					index_add = col_count * 4;
					seek_adj1 = (wbool)(tRow > 0 && tCol > 0);
					seek_adj2 = (wbool)(tRow < row_count - 1 && tCol > 0);
					break;
				case e_right:
					adj_pos1 = (((tilePos + 1) - col_count) * 4) + 3;
					adj_pos2 = (((tilePos + 1) + col_count) * 4) + 3;
					index_add = col_count * 4;

					adj = e_left;
					affect_pos = ((tilePos + 1) * 4) + 3;

					seek_adj1 = (wbool)(tRow > 0 && tCol < col_count - 1);
					seek_adj2 = (wbool)(tRow < row_count - 1 && tCol < col_count - 1);
					break;
				case e_slope:
					break;
				}


				if (seek_adj1 && adj_pos1 >= 0 && adj_pos1 < edge_array_size)
					adjacent_1 = edge_array[adj_pos1].p_edge;

				if (seek_adj2 && adj_pos2 >= 0 && adj_pos2 < edge_array_size)
					adjacent_2 = edge_array[adj_pos2].p_edge;

			}
			else
			{
				wbool seek_adj1 = wtrue;
				wbool seek_adj2 = wtrue;

				switch (tile_edges[iec].type)
				{
				case e_bottom:
				case e_top:

					adj_pos1 -= 4;
					adj_pos2 += 4;

					index_add = 4;
					seek_adj1 = (wbool)(tCol > 0);
					seek_adj2 = (wbool)(tCol < col_count - 1);
					break;
				case e_left:
				case e_right:
					adj_pos1 -= (col_count * 4);
					adj_pos2 += (col_count * 4);
					index_add = col_count * 4;


					seek_adj1 = (wbool)(tRow > 0);
					seek_adj2 = (wbool)(tRow < row_count - 1);
					break;
				case e_slope:
					break;
				}


				if (seek_adj1 && adj_pos1 >= 0 && adj_pos1 < edge_array_size)
					adjacent_1 = edge_array[adj_pos1].p_edge;

				if (seek_adj2 && adj_pos2 >= 0 && adj_pos2 < edge_array_size)
					adjacent_2 = edge_array[adj_pos2].p_edge;
			}

			if (adjacent_1 != NULL)
			{
				wfloat end_value = (tile_edges[iec].type == e_top || tile_edges[iec].type == e_bottom) ? tile_edges[iec].end.x : tile_edges[iec].end.y;

				if (adjacent_2 != NULL)
				{
					end_value = (tile_edges[iec].type == e_top || tile_edges[iec].type == e_bottom) ? (float)adjacent_2->end.x : (float)adjacent_2->end.y;
					//reaffect following cells

					edge_array[adj_pos2 - index_add].p_edge = adjacent_1;

					int32_t reaffect_indx = adj_pos2;

					while (reaffect_indx < edge_array_size && edge_array[reaffect_indx].p_edge == adjacent_2)
					{
						edge_array[reaffect_indx].p_edge = adjacent_1;
						edge_array[reaffect_indx].is_src = wfalse;
						reaffect_indx += index_add;
					}

				}

				if ((tile_edges[iec].type == e_top || tile_edges[iec].type == e_bottom))
					adjacent_1->end.x = end_value;
				else
					adjacent_1->end.y = end_value;

				//affect adjacent 1 to the next tile
				edge_array[adj_pos1 + index_add].p_edge = adjacent_1;

			}
			else if (adjacent_2 != NULL)
			{
				wfloat start_value = (tile_edges[iec].type == e_top || tile_edges[iec].type == e_bottom) ? tile_edges[iec].start.x : tile_edges[iec].start.y;

				if ((tile_edges[iec].type == e_top || tile_edges[iec].type == e_bottom))
					adjacent_2->start.x = start_value;
				else
					adjacent_2->start.y = start_value;

				//affect adjacent 2 to the prev tile
				edge_array[adj_pos2 - index_add].p_edge = adjacent_2;
				edge_array[adj_pos2 - index_add].is_src = wtrue;
				edge_array[adj_pos2].is_src = wfalse;
			}
			else //no adjacent, we must create the edge
			{
				if (affect_pos > 0 && affect_pos < edge_array_size)
				{

					edge_t* new_edge = &preallocated_edge_array[affect_pos];

					new_edge->start.x = tile_edges[iec].start.x;
					new_edge->start.y = tile_edges[iec].start.y;
					new_edge->end.x = tile_edges[iec].end.x;
					new_edge->end.y = tile_edges[iec].end.y;
					new_edge->type = adj;
					edge_array[affect_pos].is_src = wtrue;
					edge_array[affect_pos].p_edge = new_edge;
				}

			}
		}

		if (match != NULL)
			edge_array[match_index].p_edge = NULL;
	}
}

int32_t convert_worldpos_to_tilepos(uint32_t worldPos,uint32_t num_chunk_col,uint32_t base_col_count,uint32_t base_row_count)
{

	//get the chunk from the tile pos

	int32_t row = (int32_t)floorf((float)(worldPos / (base_col_count * num_chunk_col)));
	int32_t col = worldPos - ((base_col_count * num_chunk_col) * row);

	int32_t chunk_col = (int32_t)floorf((float)col / base_col_count);

	int32_t chunk_row = (int32_t)floorf((float)row / base_row_count);

	int32_t row_in_chunk = row - (chunk_row *   base_row_count);

	int32_t col_in_chunk = col - (chunk_col * base_col_count);

	return (row_in_chunk * base_col_count) + col_in_chunk;
}

/*
ichunk = the logical position of the chunk (as it appear on the screen) and NOT it position in memory
*/
int32_t convert_tilepos_to_worldpos(uint32_t tilepos, uint16_t ichunk, uint32_t num_chunk_col,uint32_t base_col_count,uint32_t base_row_count)
{
	//get the worldpos from a tile position in a chunk
	uint32_t chunk_row = (uint32_t)floorf((float)(ichunk / num_chunk_col));

	uint32_t chunk_col = ichunk - (chunk_row * num_chunk_col);

	uint32_t row_in_chunk = (int32_t)floorf((float)(tilepos / base_col_count));
	int32_t col_in_chunk = tilepos - (base_col_count * row_in_chunk);


	row_in_chunk += chunk_row * base_row_count;

	col_in_chunk += chunk_col * base_col_count;

	return (row_in_chunk * (base_col_count * num_chunk_col)) + col_in_chunk;
}

uint16_t get_chunk_pos_from_tilepos(uint32_t worldtilepos, uint32_t num_chunk_col, uint32_t base_col_count, uint32_t base_row_count)
{
	int32_t row = (int32_t)floorf((float)(worldtilepos / (base_col_count * num_chunk_col)));
	int32_t col = worldtilepos - ((base_col_count * num_chunk_col) * row);

	int32_t chunk_col = (int32_t)floorf((float)col / base_col_count);

	int32_t chunk_row = (int32_t)floorf((float)row / base_row_count);

	uint16_t chunk_pos = (uint16_t)(chunk_row * num_chunk_col) + chunk_col;

	return chunk_pos;
}

