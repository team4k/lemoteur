#ifdef USE_SDL_RENDER

#include <Render.h>

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

#include <Engine.h>
#include <Base/types.h>
#include <Base/functions.h>
#include <Base/geom.h>
#include <Base/Color.h>

#include <Utils/Hash.h>

#include <Render/render_gl_math.h>
#include <Render/render_gl_matrix.h>
#include <Render/render_gl_viewprojection.h>


#include <Debug/Logprint.h>

#include <Render/render_manager.h>

int render_init_window_render(render_manager_t* const rendermngr, wbool editor_mode, wbool fullscreen, const char* window_title, void* app_instance, void* app_handle, int16_t refreshrate)
{
	/* First, initialize SDL's video subsystem. */
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		/* Failed, exit. */
		logprint("SDL RENDER: Video initialization failed: %s",SDL_GetError());
		return 0;
	}

	rendermngr->video_resolutions = NULL;
	rendermngr->countresolutions = 0;

	//try to create an opengl context
	rendermngr->major_opengl_version = 3;
	rendermngr->minor_opengl_version = 0;

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	if (!fullscreen) {
		rendermngr->oglWindow = SDL_CreateWindow(window_title, 0, 0, rendermngr->screen_info.width, rendermngr->screen_info.height, SDL_WINDOW_OPENGL);
	}
	else {
		SDL_DisplayMode current = { 0 };
		int should_be_zero = SDL_GetCurrentDisplayMode(0, &current);

		if (should_be_zero != 0) {
			// In case of error...
			logprint("Could not get display mode for video display #0: %s", SDL_GetError());
			return 0;
		}

		rendermngr->fullscreen_width = rendermngr->screen_info.width;
		rendermngr->fullscreen_height = rendermngr->screen_info.height;

		current.w = rendermngr->screen_info.width;
		current.h = rendermngr->screen_info.height;
		current.refresh_rate = refreshrate;

		rendermngr->fullscreen_offsetx = 0;
		rendermngr->fullscreen_offsety = 0;
		rendermngr->oglWindow = SDL_CreateWindow(window_title, 0, 0, rendermngr->fullscreen_width, rendermngr->fullscreen_height, SDL_WINDOW_OPENGL);
		SDL_SetWindowFullscreen(rendermngr->oglWindow, SDL_WINDOW_FULLSCREEN);
		SDL_SetWindowDisplayMode(rendermngr->oglWindow, &current);
	}
	
	
	if (!rendermngr->oglWindow)
	{
		logprint("SDL RENDER: error while trying to open a SDL windows!, No OpenGL %d.%d support error : %s", rendermngr->major_opengl_version, rendermngr->minor_opengl_version, SDL_GetError());
		exit(EXIT_FAILURE);
		return 0;
	}

	rendermngr->oglcontext = SDL_GL_CreateContext(rendermngr->oglWindow);

	if (!rendermngr->oglcontext)
	{
		logprint("SDL RENDER: error while creating opengl context %s", SDL_GetError());
		exit(EXIT_FAILURE);
		return 0;
	}

	//SDL_GL_MakeCurrent(SDL_Window * window,SDL_GLContext context);


	logprint("SDL RENDER: created OpenGL %d.%d context", rendermngr->major_opengl_version, rendermngr->minor_opengl_version);


	return 1;
}

void render_swap_buffers(render_manager_t* const rendermngr)
{
	SDL_GL_SwapWindow(rendermngr->oglWindow);
}


void render_set_swap_interval(render_manager_t* const rendermngr)
{
	SDL_GL_SetSwapInterval(0);
}

wbool render_recreate_window(render_manager_t* const rendermngr, wbool fullscreen, void* app_handle, int16_t refreshrate)
{
	wbool setup_opengl = wfalse;

	rendermngr->fullscreen = fullscreen;

	SDL_DestroyWindow(rendermngr->oglWindow);

	//recreate opengl context
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, rendermngr->major_opengl_version);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, rendermngr->minor_opengl_version);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	rendermngr->oglWindow = SDL_CreateWindow(rendermngr->window_title, 0, 0, rendermngr->screen_info.width, rendermngr->screen_info.height, SDL_WINDOW_OPENGL);

	if (!fullscreen) {
		rendermngr->oglWindow = SDL_CreateWindow(rendermngr->window_title, 0, 0, rendermngr->screen_info.width, rendermngr->screen_info.height, SDL_WINDOW_OPENGL);
	}
	else {
		SDL_DisplayMode current = { 0 };

		int should_be_zero = SDL_GetCurrentDisplayMode(0, &current);

		if (should_be_zero != 0) {
			// In case of error...
			logprint("Could not get display mode for video display #0: %s", SDL_GetError());
			return 0;
		}

		rendermngr->fullscreen_width = rendermngr->screen_info.width;
		rendermngr->fullscreen_height = rendermngr->screen_info.height;

		current.w = rendermngr->screen_info.width;
		current.h = rendermngr->screen_info.height;
		current.refresh_rate = refreshrate;

		rendermngr->fullscreen_offsetx = 0;
		rendermngr->fullscreen_offsety = 0;
		rendermngr->oglWindow = SDL_CreateWindow(rendermngr->window_title, 0, 0, rendermngr->fullscreen_width, rendermngr->fullscreen_height, SDL_WINDOW_OPENGL);
		SDL_SetWindowFullscreen(rendermngr->oglWindow, SDL_WINDOW_FULLSCREEN);
		SDL_SetWindowDisplayMode(rendermngr->oglWindow,&current);
	}


	rendermngr->oglcontext = SDL_GL_CreateContext(rendermngr->oglWindow);

	glewInit();
	setup_opengl = wtrue;
	render_freevideoresolutions(rendermngr);

	return setup_opengl;
}

void render_destroy_window(render_manager_t* const rendermngr)
{
	//Close window and terminate
	SDL_GL_DeleteContext(rendermngr->oglcontext);
	SDL_DestroyWindow(rendermngr->oglWindow);
	SDL_Quit();
}

double render_gettime()
{
	return SDL_GetTicks();
}

int render_getrefreshrate(render_manager_t* const rendermngr)
{
	SDL_DisplayMode current;

	int displayindex = SDL_GetWindowDisplayIndex(rendermngr->oglWindow);

	int should_be_zero = SDL_GetCurrentDisplayMode(displayindex, &current);

	if (should_be_zero != 0) {
		logprint("SDL : Could not get display mode for video display #%d: %s", displayindex, SDL_GetError());
		return -1;
	}

	return current.refresh_rate;
}

const videomode_t** const render_getvideoresolutions(render_manager_t* const rendermngr, int* outcountresolutions)
{
	if (rendermngr->video_resolutions != NULL) {
		(*outcountresolutions) = rendermngr->countresolutions;
		return (const videomode_t** const)rendermngr->video_resolutions;
	}

	int displayindex = SDL_GetWindowDisplayIndex(rendermngr->oglWindow);

	int countmodes = SDL_GetNumDisplayModes(displayindex);

	videomode_t** resolutions = (videomode_t**)wf_calloc(countmodes, sizeof(videomode_t*));

	int cwidth = 0;
	int cheight = 0;
	int refreshrate = 0;

	(*outcountresolutions) = 0;

	for (int imode = 0; imode < countmodes; imode++) {

		SDL_DisplayMode current;

		int should_be_zero = SDL_GetDisplayMode(displayindex, imode, &current);

		if (should_be_zero != 0) {
			logprint("SDL : Could not get display mode for video display #%d: %s", displayindex, SDL_GetError());
			return NULL;
		}



		if (cwidth == current.w && cheight == current.h && refreshrate == current.refresh_rate)
			continue;

		videomode_t* iresol = (videomode_t*)wf_calloc(1, sizeof(videomode_t));
		iresol->width = current.w;
		iresol->height = current.h;
		iresol->refreshrate = current.refresh_rate;

		float valar = (float)iresol->height / (float)iresol->width;

		if (valar == 0.75f) {
			iresol->aspectratio = AR_4_3;
		}
		else if (valar == 0.625f) {
			iresol->aspectratio = AR_16_10;
		}
		else if (valar == 0.5625f) {
			iresol->aspectratio = AR_16_9;
		}
		else if (valar == 0.421875f) {
			iresol->aspectratio = AR_21_9;
		}
		else {
			iresol->aspectratio = AR_UNKOWN;
		}
		resolutions[(*outcountresolutions)] = iresol;
		(*outcountresolutions)++;
		cwidth = current.w;
		cheight = current.h;
		refreshrate = current.refresh_rate;

	}

	rendermngr->video_resolutions = resolutions;
	rendermngr->countresolutions = (*outcountresolutions);

	return (const videomode_t** const)resolutions;
}


#endif
