#include <inttypes.h>

#include <Render.h>

#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <Engine.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>
#include <Base/geom.h>

#include <Utils/Hash.h>
#include <Utils/hashmap.h>

#include <Render/render_manager.h>

#include <Graphics/TextureLoader.h>
#include <GameObjects/Shaders.pb-c.h>
#include <Resx/shader_loader.h>
#include <GameObjects/Characters.pb-c.h>
#include <Graphics/Animation.h>
#include <Graphics/Sprite.h>
#include <Graphics/Light.h>
#include <Graphics/LightManager.h>

#include "GameObjects/Level.pb-c.h"

#include <Debug/Logprint.h>

#include <Graphics/TextureRegion.h>
#include <Graphics/SpriteBatch.h>
#include <Graphics/FontLoader.h>

#include <Sound/Sound.h>

#include <Resx/Resources.h>

#include <Render/render_shader_utils.h>

static float yoyo_val = 0.0f;
static float last_yoyo_value = 0.0f;

void bind_shaders_params(light_manager_t* light_mngr,render_manager_t* const render_mngr, ShadersParameters** const params,size_t num_params,resources_manager_t* const resx_mngr,const Vector_t* const obj_pos,const Vector_t* const mirror_val,float rotation_value,Vector_t offset_coords,light_t** light_arr,int32_t num_a_light)
{

	light_t* nearest_light = NULL;
	
	if(light_mngr != NULL && obj_pos != NULL)
		nearest_light = light_manager_getnearestlight(light_mngr,(*obj_pos),render_mngr);

	//bind custom parameters if needed
	for(uint32_t i_param = 0; i_param < num_params;i_param++)
	{
		switch(params[i_param]->param_type)
		{
			case TYPE_PARAM_SHADER__uniform_float:
				if(params[i_param]->uniform_float->num_component == 1)
					render_setuniform_float(render_mngr,params[i_param]->param_name,params[i_param]->uniform_float->v1);
				else if(params[i_param]->uniform_float->num_component == 2)
				{
					float values[] = {params[i_param]->uniform_float->v1,params[i_param]->uniform_float->v2};
					render_setuniform_float2(render_mngr,params[i_param]->param_name,values);
				}
				else if(params[i_param]->uniform_float->num_component == 3)
				{
					float values[] = {params[i_param]->uniform_float->v1,params[i_param]->uniform_float->v2,params[i_param]->uniform_float->v3};
					render_setuniform_float3(render_mngr,params[i_param]->param_name,values);
				}
				else if(params[i_param]->uniform_float->num_component == 4)
				{
					float values[] = {params[i_param]->uniform_float->v1,params[i_param]->uniform_float->v2,params[i_param]->uniform_float->v3,params[i_param]->uniform_float->v4};

					render_setuniform_float4(render_mngr,params[i_param]->param_name,values);
				}

			break;

			case TYPE_PARAM_SHADER__uniform_int:

				render_setuniform_int(render_mngr, params[i_param]->param_name,params[i_param]->uniform_int->v1);

				break;
			case TYPE_PARAM_SHADER__light:
				
				if(light_mngr != NULL)
				{

					light_t* olight = light_manager_getlight(light_mngr,params[i_param]->uniform_light->light_id);

					if(olight != NULL)
					{
						float values[] = {(float)olight->position.x,(float)olight->position.y,0.075f};

						if(params[i_param]->uniform_light->normalized)
						{
							values[0] = (values[0] + (float)render_mngr->scroll_vector.x) / render_mngr->screen_info.width;
							values[1] = (values[1] + (float)render_mngr->scroll_vector.y) / render_mngr->screen_info.height;
						}

						render_setuniform_float3(render_mngr,params[i_param]->param_name,values);
					}
				}


				break;

			case TYPE_PARAM_SHADER__texture:
				{
					texture_info* selected_texture = gettexture_resx(resx_mngr,params[i_param]->uniform_texture->texture_id);

					render_bindtexture_tosampler(render_mngr,selected_texture->texture_pointer,params[i_param]->param_name,params[i_param]->uniform_texture->bind_slot,wfalse);

					if(params[i_param]->uniform_texture->has_set_offset && params[i_param]->uniform_texture->set_offset)
					{
						//texture offset
						Vector_t offset = vectorzero;

						if(selected_texture->virtual_texture)
						{
							offset = Vec_minus(selected_texture->region->position,offset_coords);
							offset.x /= gettexture_resx(resx_mngr,selected_texture->parent_name)->width;
							offset.y /= gettexture_resx(resx_mngr,selected_texture->parent_name)->height;
						}

						float offset_f[] = {offset.x,offset.y};

						char offset_name[256];

						w_sprintf(offset_name,256,"%s_offset",params[i_param]->param_name);
					
						render_setuniform_float2(render_mngr,offset_name,offset_f);
					}

					if(params[i_param]->uniform_texture->has_set_size && params[i_param]->uniform_texture->set_size)
					{
						//texture size
						Vector_t size = Vec(selected_texture->width,selected_texture->height);

						float size_f[] = {size.x,size.y};

						char size_name[256];

						w_sprintf(size_name,256,"%s_size",params[i_param]->param_name);
					
						render_setuniform_float2(render_mngr,size_name,size_f);
					}
				}
				break;
			case TYPE_PARAM_SHADER__constant:

				if(strcmp(params[i_param]->uniform_const->const_name,"resolution") == 0)
				{
					float resolution[] = {(float)render_mngr->screen_info.width,(float)render_mngr->screen_info.height};

					render_setuniform_float2(render_mngr,params[i_param]->param_name,resolution);
				}
				else if(strcmp(params[i_param]->uniform_const->const_name,"time") == 0)
				{
					float value_sec = render_gettime2(wfalse);
					render_setuniform_float(render_mngr,params[i_param]->param_name,value_sec);
				}
				else if(strcmp(params[i_param]->uniform_const->const_name,"time_msec") == 0)
				{
					float value_msec = render_gettime2(wtrue);
					render_setuniform_float(render_mngr,params[i_param]->param_name,value_msec);
				}
				else if(strcmp(params[i_param]->uniform_const->const_name,"yoyo_time") == 0)
				{
					float value_sec = render_gettime2(wfalse);
					float c_value = floorf(value_sec);
					
					float fract =  value_sec - c_value;

					if(last_yoyo_value < c_value)
						yoyo_val = (yoyo_val == 0.0f)? 1.0f : 0.0f; 

					if(yoyo_val > 0.0f)
						fract = yoyo_val - fract;

					//logprint("fract val %5.2f",fract);

					render_setuniform_float(render_mngr,params[i_param]->param_name,fract);

					last_yoyo_value = c_value;
				}
				else if(strcmp(params[i_param]->uniform_const->const_name,"light_0_pos") == 0 && light_mngr != NULL && obj_pos != NULL)
				{
					if(nearest_light != NULL)
					{
						float light_pos[] = {(float)(nearest_light->position.x + render_mngr->scroll_vector.x) / render_mngr->screen_info.width,(float)(nearest_light->position.y + render_mngr->scroll_vector.y) / render_mngr->screen_info.height,0.075f};

						render_setuniform_float3(render_mngr,params[i_param]->param_name,light_pos);
					}
				}
				else if(strcmp(params[i_param]->uniform_const->const_name,"light_0_color") == 0 && light_mngr != NULL && obj_pos != NULL)
				{
					if(nearest_light != NULL)
					{
						float light_color[] = {(float)nearest_light->color.r,(float)nearest_light->color.g,(float)nearest_light->color.b,(float)nearest_light->color.a};
						render_setuniform_float4(render_mngr,params[i_param]->param_name,light_color);
					}
				}
				else if(strcmp(params[i_param]->uniform_const->const_name,"light_0_radius") == 0 && light_mngr != NULL && obj_pos != NULL)
				{
					if(nearest_light != NULL)
					{
						render_setuniform_float(render_mngr,params[i_param]->param_name,nearest_light->radius);
					}
				}
				else if(strcmp(params[i_param]->uniform_const->const_name,"light_0_falloff") == 0 && light_mngr != NULL && obj_pos != NULL)
				{
					if(nearest_light != NULL)
					{
						float falloff[] = {nearest_light->constant,nearest_light->linear,nearest_light->quadratic};

						render_setuniform_float3(render_mngr,params[i_param]->param_name,falloff);
					}
				}
				else if(strcmp(params[i_param]->uniform_const->const_name,"light_0_info") == 0 && light_mngr != NULL && obj_pos != NULL)
				{
					if(nearest_light != NULL)
					{
						float info[] ={(nearest_light->l_type == light_omni)?1.0f:2.0f,nearest_light->open_angle,nearest_light->rotation_angle};

						render_setuniform_float3(render_mngr,params[i_param]->param_name,info);
					}
				}
				else if(strcmp(params[i_param]->uniform_const->const_name,"inrange_lights") == 0 && light_mngr != NULL && light_arr != NULL)
				{
					
					float light_pos[MAX_LIGHT_PER_ELEMENT * 3] = {0.0f};
					float light_color[MAX_LIGHT_PER_ELEMENT * 4] = {0.0f};
					float light_falloff[MAX_LIGHT_PER_ELEMENT * 3] = {0.0f};
					float light_info[MAX_LIGHT_PER_ELEMENT * 3] = {0.0f};

					for(int ilight = 0;ilight < num_a_light;ilight++)
					{
						light_t* olight = light_arr[ilight];

						light_pos[(ilight * 3)] = (float)(olight->position.x + render_mngr->scroll_vector.x) / render_mngr->screen_info.width;
						light_pos[(ilight * 3) +1] = (float)(olight->position.y + render_mngr->scroll_vector.y) / render_mngr->screen_info.height;
						light_pos[(ilight * 3) +2] = 0.075f;

						light_color[(ilight * 4)] = (float)olight->color.r;
						light_color[(ilight * 4)+1] = (float)olight->color.g;
						light_color[(ilight * 4)+2] = (float)olight->color.b;
						light_color[(ilight * 4)+3] = (float)olight->color.a;

						light_falloff[(ilight * 3)] = olight->constant;
						light_falloff[(ilight * 3)+1] = olight->linear;
						light_falloff[(ilight * 3)+2] = olight->quadratic;

						light_info[(ilight * 3)] = (olight->l_type == light_omni)?1.0f:2.0f;
						light_info[(ilight * 3)+1] = olight->open_angle;
						light_info[(ilight * 3)+2] = olight->rotation_angle;
					}


					render_setuniform_arrayfloat3(render_mngr,"LightPos",light_pos,num_a_light);
					render_setuniform_arrayfloat4(render_mngr,"LightColor",light_color,num_a_light);
					render_setuniform_arrayfloat3(render_mngr,"Falloff",light_falloff,num_a_light);
					render_setuniform_arrayfloat3(render_mngr,"LightInfo",light_info,num_a_light);
					render_setuniform_int(render_mngr,"NumLights",num_a_light);


				}
				else if(strcmp(params[i_param]->uniform_const->const_name,"ambient_color") == 0 && light_mngr != NULL)
				{
					float ambient[] = {(float)light_mngr->ambient_color.r,(float)light_mngr->ambient_color.g,(float)light_mngr->ambient_color.b,(float)light_mngr->ambient_color.a};
					render_setuniform_float4(render_mngr,params[i_param]->param_name,ambient);
				}
				else if(strcmp(params[i_param]->uniform_const->const_name,"mirror_val") == 0)
				{
					float mirror[] = {(float)mirror_val->x,(float)mirror_val->y};
					render_setuniform_float2(render_mngr,params[i_param]->param_name,mirror);
				}
				else if(strcmp(params[i_param]->uniform_const->const_name,"rotation_val") == 0)
				{
					render_setuniform_float(render_mngr,params[i_param]->param_name,rotation_value);
				}
				else if(strcmp(params[i_param]->uniform_const->const_name,"shadow_mask") == 0 && light_mngr != NULL)
				{
					int32_t i_slot = 1;

					if(params[i_param]->uniform_const->extra_param != NULL && strcmp(params[i_param]->uniform_const->extra_param,"") != 0) 
					{
						char* endptr;
						i_slot = (int32_t)strtoimax(params[i_param]->uniform_const->extra_param,&endptr,10);
					}

					render_bindtexture_tosampler(render_mngr,light_mngr->shadow_texture,"shadow_mask",i_slot,wfalse);
					float size[] = {(float)light_mngr->width_texture,(float)light_mngr->height_texture};
					render_setuniform_float2(render_mngr,"shadow_mask_size",size);
				}

				break;
			case TYPE_PARAM_SHADER__attribute_array:
				
				if(params[i_param]->attribute_array->n_array_content == 0)
					break;

				VBO_ID buffer_id;

				//generate a buffer if no buffer exists
				if(!params[i_param]->attribute_array->has_array_ref)
				{
					buffer_id = render_gen_buffer(render_mngr,params[i_param]->attribute_array->array_content,params[i_param]->attribute_array->n_array_content * sizeof(float),wfalse,wfalse);

					params[i_param]->attribute_array->array_ref.data = (unsigned char*)wf_calloc(1,sizeof(VBO_ID));
					params[i_param]->attribute_array->array_ref.len = sizeof(VBO_ID);

					memcpy(params[i_param]->attribute_array->array_ref.data,&buffer_id,sizeof(VBO_ID));
					params[i_param]->attribute_array->has_array_ref = (protobuf_c_boolean)1;
				}

				memcpy(&buffer_id,params[i_param]->attribute_array->array_ref.data,sizeof(VBO_ID));

				if(params[i_param]->attribute_array->update_buffer)
				{
					render_discard_updatebuffer(render_mngr,buffer_id,params[i_param]->attribute_array->array_content,params[i_param]->attribute_array->n_array_content * sizeof(float),wfalse,wfalse);
					params[i_param]->attribute_array->update_buffer = (protobuf_c_boolean)0;
				}

				render_bindattribute_floatVBO(render_mngr,params[i_param]->param_name,buffer_id,1);

				break;
			case TYPE_PARAM_SHADER__uniform_array:
				if (params[i_param]->attribute_array == NULL || params[i_param]->attribute_array->n_array_content == 0)
					break;

				//bind to a uniform parameter our array
				render_setuniform_arrayfloat(render_mngr, params[i_param]->param_name, params[i_param]->attribute_array->array_content, params[i_param]->attribute_array->n_array_content);

				break;
		}
	}
}

void unbind_shaders_params(render_manager_t* const render_mngr, ShadersParameters** const params,size_t num_params,resources_manager_t* const resx_mngr,light_manager_t* light_mngr)
{
	//unbind params
	for(uint32_t i_param = 0; i_param < num_params;i_param++)
	{
		switch(params[i_param]->param_type)
		{
			case TYPE_PARAM_SHADER__constant:
				if(strcmp(params[i_param]->uniform_const->const_name,"shadow_mask") == 0 && light_mngr != NULL)
				{
					int32_t i_slot = 1;

					if(params[i_param]->uniform_const->extra_param != NULL && strcmp(params[i_param]->uniform_const->extra_param,"") != 0) 
					{
						char* endptr;
						i_slot = (int32_t)strtoimax(params[i_param]->uniform_const->extra_param,&endptr,10);
					}

					render_unbindtexture_tosampler(render_mngr,light_mngr->shadow_texture,"shadow_mask",i_slot,wfalse);
				}
			break;

			case TYPE_PARAM_SHADER__texture:
				render_unbindtexture_tosampler(render_mngr,gettexture_resx(resx_mngr,params[i_param]->uniform_texture->texture_id)->texture_pointer,params[i_param]->param_name,params[i_param]->uniform_texture->bind_slot,wfalse);

				break;
		}
	}
}