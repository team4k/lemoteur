#include <Render.h>

#if defined(RENDERER_OPENGL) || defined(RENDERER_OPENGL_ES)

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

#include <Engine.h>
#include <Base/types.h>
#include <Base/functions.h>
#include <Base/geom.h>
#include <Base/Color.h>

#include <Render/render_gl_math.h>

GLfloat glMaxf(const GLfloat value0, const GLfloat value1)
{
    return value0 > value1 ? value0 : value1;
}

GLfloat glMinf(const GLfloat value0, const GLfloat value1)
{
    return value0 < value1 ? value0 : value1;
}

GLfloat glRadToDegf(const GLfloat radians)
{
    return radians * 360.0f / (2.0f *WM_PI);
}

GLfloat glDegToRadf(const GLfloat degrees)
{
    return degrees * 2.0f *WM_PI / 360.0f;
}

GLfloat glMixf(const GLfloat value0, const GLfloat value1, const GLfloat t)
{
	return value0 * (1.0f - t) + value1 * t;
}

GLfloat glClampf(const GLfloat value, const GLfloat min, const GLfloat max)
{
	if (value < min)
	{
		return min;
	}
	if (value > max)
	{
		return max;
	}
	return value;
}

GLfloat glLengthf(const GLfloat x, const GLfloat y, const GLfloat z)
{
	return sqrtf(x*x + y*y + z*z);
}

 void glVector3Normalizef(GLfloat vector[3])
 {
	 GLfloat mag = sqrtf(vector[0] * vector[0] + vector[1] * vector[1] + vector[2] * vector[2]);

	vector[0] /= mag;
	vector[1] /= mag;
	vector[2] /= mag;
 }

#endif