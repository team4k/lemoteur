﻿#ifdef ANDROID_BUILD

#include <Render.h>

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

#include <Engine.h>
#include <Base/types.h>
#include <Base/functions.h>
#include <Base/geom.h>
#include <Base/Color.h>

#include <Utils/Hash.h>

#include <Render/render_gl_math.h>
#include <Render/render_gl_matrix.h>
#include <Render/render_gl_viewprojection.h>


#include <Debug/Logprint.h>

#include <jni.h>
#include <android/native_window.h> // requires ndk r5 or newer
#include <android/native_window_jni.h> // requires ndk r5 or newer
#include <EGL/egl.h>


#include <Render/render_manager.h>

int render_init_window_render(render_manager_t* const rendermngr, wbool editor_mode, wbool fullscreen, const char* window_title, void* app_instance, void* app_handle)
{
	EGLint lFormat, lNumConfigs, lErrorResult;
	EGLConfig lConfig;

	// Defines display requirements. 16bits mode here.
	const EGLint lAttributes[] = {
		EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
		EGL_RENDERABLE_TYPE, EGL_OPENGL_ES_BIT,
		EGL_BLUE_SIZE, 8, EGL_GREEN_SIZE, 8, EGL_RED_SIZE, 8,
		EGL_ALPHA_SIZE,8,
		EGL_NONE
	};
	// Retrieves a display connection and initializes it.
	logprint("Connecting to the display.");
	rendermngr->oglDisplay = eglGetDisplay(EGL_DEFAULT_DISPLAY);

	if (rendermngr->oglDisplay == EGL_NO_DISPLAY)
	{
		logprint("Error retrieving egl display!");
		return 0;
	}

	if (!eglInitialize(rendermngr->oglDisplay, NULL, NULL))
	{
		logprint("Error initializing egl display!");
		return 0;
	}
	// Selects the first OpenGL configuration found.
	logprint("Selecting a display config.");
	if (!eglChooseConfig(rendermngr->oglDisplay, lAttributes, &lConfig, 1,
		&lNumConfigs) || (lNumConfigs <= 0))
	{
		logprint("Unable to select a display config!");
		return 0;
	}


	EGLint buffer_value;
	eglGetConfigAttrib(rendermngr->oglDisplay, lConfig, EGL_BUFFER_SIZE, &buffer_value);

	EGLint config_value;
	eglGetConfigAttrib(rendermngr->oglDisplay, lConfig, EGL_CONFIG_ID, &config_value);

	EGLint native_value;
	eglGetConfigAttrib(rendermngr->oglDisplay, lConfig, EGL_NATIVE_RENDERABLE, &native_value);


	EGLint red_value;
	eglGetConfigAttrib(rendermngr->oglDisplay, lConfig, EGL_RED_SIZE, &red_value);

	EGLint green_value;
	eglGetConfigAttrib(rendermngr->oglDisplay, lConfig, EGL_GREEN_SIZE, &green_value);

	EGLint blue_value;
	eglGetConfigAttrib(rendermngr->oglDisplay, lConfig, EGL_BLUE_SIZE, &blue_value);

	EGLint alpha_value;
	eglGetConfigAttrib(rendermngr->oglDisplay, lConfig, EGL_ALPHA_SIZE, &alpha_value);

	//print selected egl config attributes
	logprint("egl config id %d", config_value);
	logprint("egl config buffer size %d", buffer_value);
	logprint("egl config native renderable %d", native_value);

	logprint("egl config red size %d", red_value);
	logprint("egl config green size %d", green_value);
	logprint("egl config blue size %d", blue_value);
	logprint("egl config alpĥa size %d", alpha_value);

	//create a opengl es 2.0 context by default
	EGLint attrib_list[] = { EGL_CONTEXT_CLIENT_VERSION, 2, EGL_NONE };

	if (rendermngr->fixed_func_pipeline)//fallback to gl es context v1 in case of FFP
		attrib_list[1] = 1;

	rendermngr->oglContext = eglCreateContext(rendermngr->oglDisplay, lConfig, EGL_NO_CONTEXT, attrib_list);

	// Reconfigures the Android window with the EGL format.
	logprint("Configuring window format.");
	if (!eglGetConfigAttrib(rendermngr->oglDisplay, lConfig,
		EGL_NATIVE_VISUAL_ID, &lFormat))
	{
		logprint("Unable to configure the android window!");
	}

	logprint("Set buffer geometry");
	ANativeWindow_setBuffersGeometry((ANativeWindow*)app_handle, 0, 0, lFormat);

	// Creates the display surface.
	logprint("Initializing the display.");

	rendermngr->oglSurface = eglCreateWindowSurface(rendermngr->oglDisplay, lConfig, (ANativeWindow*)app_handle, NULL);


	if (eglMakeCurrent(rendermngr->oglDisplay, rendermngr->oglSurface, rendermngr->oglSurface, rendermngr->oglContext) == EGL_FALSE)
	{
		logprint("Error while calling eglMakeCurrent!");
		return 0;
	}

	EGLint tmp_screen_w = 0;
	EGLint tmp_screen_h = 0;

	eglQuerySurface(rendermngr->oglDisplay, rendermngr->oglSurface, EGL_WIDTH, (EGLint*)&tmp_screen_w);
	eglQuerySurface(rendermngr->oglDisplay, rendermngr->oglSurface, EGL_HEIGHT, (EGLint*)&tmp_screen_h);
	rendermngr->screen_info.width = tmp_screen_w;
	rendermngr->screen_info.height = tmp_screen_h;

	logprint("Surface size %d %d", rendermngr->screen_info.width, rendermngr->screen_info.height);
}


void render_swap_buffers(render_manager_t* const rendermngr)
{
	if (!eglSwapBuffers(rendermngr->oglDisplay, rendermngr->oglSurface))
	{
		int error = eglGetError();

		if (error == EGL_CONTEXT_LOST)
			logprint("EGL context lost");

		if (error == EGL_BAD_NATIVE_WINDOW)
			logprint("EGL bad native window");

		logprint("eglSwapBuffers error %d", error);
	}
}

void render_set_swap_interval(render_manager_t* const rendermngr)
{
	eglSwapInterval(rendermngr->oglDisplay, 0);
}


wbool render_recreate_window(render_manager_t* const rendermngr, wbool fullscreen, void* app_handle)
{
	//create context and surface
	EGLint lFormat, lNumConfigs, lErrorResult;
	EGLConfig lConfig;

	// Defines display requirements. 32bits mode here.
	const EGLint lAttributes[] = {
		EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
		EGL_RENDERABLE_TYPE, EGL_OPENGL_ES_BIT,
		EGL_BLUE_SIZE, 8, EGL_GREEN_SIZE, 8, EGL_RED_SIZE, 8,
		EGL_ALPHA_SIZE,8,
		EGL_NONE
	};
	// Retrieves a display connection and initializes it.
	logprint("Connecting to the display.");
	rendermngr->oglDisplay = eglGetDisplay(EGL_DEFAULT_DISPLAY);

	if (rendermngr->oglDisplay == EGL_NO_DISPLAY)
	{
		logprint("Error retrieving egl display!");
		return;
	}

	if (!eglInitialize(rendermngr->oglDisplay, NULL, NULL))
	{
		logprint("Error initializing egl display!");
		return;
	}
	// Selects the first OpenGL configuration found.
	logprint("Selecting a display config.");
	if (!eglChooseConfig(rendermngr->oglDisplay, lAttributes, &lConfig, 1,
		&lNumConfigs) || (lNumConfigs <= 0))
	{
		logprint("Unable to select a display config!");
		return;
	}

	//create a opengl es 2.0 context by default
	EGLint attrib_list[] = { EGL_CONTEXT_CLIENT_VERSION, 2, EGL_NONE };

	if (rendermngr->fixed_func_pipeline)//fallback to gl es context v1 in case of FFP
		attrib_list[1] = 1;


	rendermngr->oglContext = eglCreateContext(rendermngr->oglDisplay, lConfig, EGL_NO_CONTEXT, attrib_list);

	// Reconfigures the Android window with the EGL format.
	logprint("Configuring window format.");
	if (!eglGetConfigAttrib(rendermngr->oglDisplay, lConfig,
		EGL_NATIVE_VISUAL_ID, &lFormat))
	{
		logprint("Unable to configure the android window!");
	}

	logprint("Set buffer geometry");
	ANativeWindow_setBuffersGeometry((ANativeWindow*)app_handle, 0, 0, lFormat);

	// Creates the display surface.
	logprint("Initializing the display.");


	rendermngr->oglSurface = eglCreateWindowSurface(rendermngr->oglDisplay, lConfig, (ANativeWindow*)app_handle, NULL);



	if (eglMakeCurrent(rendermngr->oglDisplay, rendermngr->oglSurface, rendermngr->oglSurface, rendermngr->oglContext) == EGL_FALSE)
	{
		logprint("Error while calling eglMakeCurrent!");
		return;
	}

	EGLint tmp_screen_w = 0;
	EGLint tmp_screen_h = 0;

	eglQuerySurface(rendermngr->oglDisplay, rendermngr->oglSurface, EGL_WIDTH, (EGLint*)&tmp_screen_w);
	eglQuerySurface(rendermngr->oglDisplay, rendermngr->oglSurface, EGL_HEIGHT, (EGLint*)&tmp_screen_h);
	rendermngr->screen_info.width = tmp_screen_w;
	rendermngr->screen_info.height = tmp_screen_h;

	return wtrue;

}

void render_destroy_window(render_manager_t* const rendermngr)
{
	eglDestroySurface(rendermngr->oglDisplay, rendermngr->oglSurface);
	eglTerminate(rendermngr->oglDisplay);
}

double render_gettime()
{
	return clock() * 1000 / CLOCKS_PER_SEC;
}

int render_getrefreshrate(render_manager_t* const rendermngr)
{
	logprint("render_getrefreshrate not implemented in android, always return 60");
	//TODO : implement get refresh rate
	//https://github.com/brackeen/glfm/blob/master/src/glfm_platform_android.c
	return 60;
}




#endif