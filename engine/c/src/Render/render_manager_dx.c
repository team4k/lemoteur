#include <Render.h>

#ifdef RENDERER_DX

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

#include <Engine.h>
#include <Base/types.h>
#include <Base/functions.h>
#include <Base/geom.h>
#include <Base/Color.h>

#include <GameObjects/Shaders.pb-c.h>

#include <Utils/Hash.h>
#include <Utils/hashmap.h>

#include <Debug/logprint.h>


#include <Render/render_manager.h>

static const char* semantics[] =
{
	"POSITION",
	"TEXCOORD",
	"NORMAL",
	"COLOR"
};


//from http://www.catalinzima.com/2012/12/handling-orientation-in-a-windows-phone-8-game/
static void _render_compute_orientation(render_manager_t* const rendermngr,orientation_mode orientation_value)
{
	// Store the current device orientation
	rendermngr->screen_orientation = orientation_value;

	// Generate the matrix transformations for rendering to the new orientation.
	switch (rendermngr->screen_orientation)
	{
		case R_PORTRAIT:
		// Portrait is default for WP8, so no changes need to be made here
		// Just use Identity Matrices
		rendermngr->Orientation2D = XMMatrixIdentity();
		rendermngr->Orientation2D = XMMatrixIdentity();
		rendermngr->screen_info.width = rendermngr->orig_screen_width;
		rendermngr->screen_info.height = rendermngr->orig_screen_height;
		break;
	case R_LANDSCAPE:
		//2D: 90-degree Rotation + translation of origin
		rendermngr->Orientation2D = XMMatrixMultiply(
			XMMatrixRotationZ(XM_PIDIV2),
			XMMatrixTranslation(rendermngr->orig_screen_width, 0, 0));
		//3D: 90-degree Z-rotation
		rendermngr->Orientation3D = XMMATRIX(
			0.0f, -1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 1.0f);
		rendermngr->screen_info.width = rendermngr->orig_screen_height;
		rendermngr->screen_info.height = rendermngr->orig_screen_width;
		break;
	case R_PORTRAIT_FLIPPED:
		//This is not supported on the phone, but we leave the math here, just in case...
		//2D: 180-degree Rotation + translation of origin
		rendermngr->Orientation2D = XMMatrixMultiply(
			XMMatrixRotationZ(XM_PI),
			XMMatrixTranslation(rendermngr->orig_screen_width, rendermngr->orig_screen_height, 0));
		//3D: // 180-degree Z-rotation
		rendermngr->Orientation3D = XMMATRIX(
			-1.0f, 0.0f, 0.0f, 0.0f,
			0.0f, -1.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 1.0f
			);
		rendermngr->screen_info.width = rendermngr->orig_screen_width;
		rendermngr->screen_info.height = rendermngr->orig_screen_height;
		break;
	case R_LANDSCAPE_FLIPPED:
		//2D: 270-degree Rotation + translation of origin
		rendermngr->Orientation2D = XMMatrixMultiply(
			XMMatrixRotationZ(3 * XM_PIDIV2),
			XMMatrixTranslation(0, rendermngr->orig_screen_width, 0));
		//3D: 270-degree Z-rotation
		rendermngr->Orientation3D = XMMATRIX(
			0.0f, 1.0f, 0.0f, 0.0f,
			-1.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 1.0f);
		rendermngr->screen_info.width = rendermngr->orig_screen_height;
		rendermngr->screen_info.height = rendermngr->orig_screen_width;
		break;
	}
}

static Vector_t _render_transform_position_torientation(render_manager_t* const render_mngr, Vector_t pos)
{
	Vector_t return_val = { 0 };

	switch (render_mngr->screen_orientation)
	{
	case R_PORTRAIT:
		return_val = pos;
		break;
	case R_LANDSCAPE:
		return_val = Vec(pos.y, render_mngr->orig_screen_width - pos.x);
		break;
	case R_PORTRAIT_FLIPPED:
		return_val = Vec(render_mngr->orig_screen_width - pos.x, render_mngr->orig_screen_height - pos.y);
		break;
	case R_LANDSCAPE_FLIPPED:
		return_val = Vec(render_mngr->orig_screen_height -  pos.y,pos.x);
		break;
	}

	return return_val;
}


D3D_FEATURE_LEVEL featureLevels[] = 
{
	D3D_FEATURE_LEVEL_9_3
};


static void _update_uniform(render_manager_t* const rendermngr,void* data,const char* uniform_name)
{
	if(rendermngr->current_program->vertex_shader->const_buf_def)
	{
		hashmap_t* map = (hashmap_t*)rendermngr->current_program->vertex_shader->const_buf_def->buffer_element_def;

		element_def_t* uni_def = (element_def_t*)hashmap_getvalue(map,uniform_name);

		if(uni_def != NULL)
		{
			memcpy(&rendermngr->current_program->vertex_shader->const_buf_def->buffer_content[uni_def->offset_element],data,uni_def->size_element);

			rendermngr->d3d11DevCon->UpdateSubresource(rendermngr->current_program->vertex_shader->constant_buffer,0,NULL,rendermngr->current_program->vertex_shader->const_buf_def->buffer_content,0,0);
		}
		else
		{
			logprint("Can't find uniform named %s!",uniform_name);
		}
	}
}


int render_init(render_manager_t* const rendermngr,int16_t width, int16_t height,const char* app_path,const char* window_title,void* app_instance,void* app_handle,wbool editor_mode,wbool fullscreen,wbool force_ffp,texture_filter texture_filtering)
{
	if(force_ffp)
	{
		logprint("Can't force FFP on D3D renderer! please use OpenGL renderer instead, aborting...");
		return 0;
	}

	rendermngr->orig_screen_width = width;
	rendermngr->orig_screen_height = height;

	 rendermngr->hashtable_shader_programs = (hashtable_t*)wf_malloc(sizeof(hashtable_t));

	 hashtable_t* tmp = (hashtable_t*)rendermngr->hashtable_shader_programs;

	 tmp->keys = (char**) wf_malloc(MAX_SHADER_PROGRAMS * sizeof (char*));

	//initialise shaders programs keys array
	for(int32_t sh_indx = 0; sh_indx < MAX_SHADER_PROGRAMS;sh_indx++)
		 tmp->keys[sh_indx] = 0;

	tmp->max_elem = MAX_SHADER_PROGRAMS;
	tmp->num_elem = 0;


	#if !defined(WINAPI_FAMILY) || (WINAPI_FAMILY != WINAPI_FAMILY_PHONE_APP)
		HINSTANCE hInstance = (HINSTANCE)app_instance;
		//HWND hwnd = (HWND)app_handle;
		int option = 1;

		const wchar_t CLASS_NAME[]  = L"Sample Window Class";
  
	   rendermngr->wc.cbSize = sizeof(WNDCLASSEX);
	   rendermngr->wc.style = CS_HREDRAW | CS_VREDRAW;
	   rendermngr->wc.cbClsExtra = NULL;
	   rendermngr->wc.cbWndExtra = NULL;
	   rendermngr->wc.hInstance = hInstance;
	   rendermngr->wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	   rendermngr->wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	   rendermngr->wc.hbrBackground = (HBRUSH)(COLOR_WINDOW + 2);
	   rendermngr->wc.lpszMenuName = NULL;
	   rendermngr->wc.lpszClassName = CLASS_NAME;
	   rendermngr->wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	   if (!RegisterClassEx(&rendermngr->wc))
	   {
		   logprint("Error registering class");
		   return 0;
	   }

	   HWND hwnd = CreateWindowEx(NULL,CLASS_NAME,L"Test DX11",WS_OVERLAPPEDWINDOW,CW_USEDEFAULT, CW_USEDEFAULT, width, height,NULL,NULL,hInstance,NULL);

	   if (!hwnd)
		{
			DWORD error_code = GetLastError();
			logprint("Error creating window %d",error_code);
		   return 0;
		}

	   ShowWindow(hwnd,1);
	   UpdateWindow(hwnd);

	#endif

	rendermngr->screen_info.position.x = 0.0f;
	rendermngr->screen_info.position.y = 0.0f;
	rendermngr->screen_info.width = width;
	rendermngr->screen_info.height = height;
	rendermngr->zoom_factor = 1.0f;
	rendermngr->near_value = 0.0f;
	rendermngr->far_value = 1.0f;
	rendermngr->current_program = NULL;

	 HRESULT hr;
   
	
	//Create our SwapChain

	#if defined(WINAPI_FAMILY) && (WINAPI_FAMILY == WINAPI_FAMILY_PHONE_APP)
	DXGI_SWAP_CHAIN_DESC1 swapChainDesc = { 0 };
	swapChainDesc.Width = width;
	swapChainDesc.Height = height;
	swapChainDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	swapChainDesc.Stereo = false;
	swapChainDesc.SampleDesc.Count = 1;
	swapChainDesc.SampleDesc.Quality = 0;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.BufferCount = 2;
	swapChainDesc.Scaling = DXGI_SCALING_STRETCH;
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL;
	swapChainDesc.Flags = 0;


	//Windows phone 8.1 swapChainDesc.Flags = DXGI_SWAP_CHAIN_FLAG_FRAME_LATENCY_WAITABLE_OBJECT; // Enable GetFrameLatencyWaitableObject()

	UINT flags = D3D11_CREATE_DEVICE_BGRA_SUPPORT;

	#if _DEBUG
		flags |= D3D11_CREATE_DEVICE_DEBUG;
	#endif

	hr = D3D11CreateDevice(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, flags, featureLevels, ARRAYSIZE(featureLevels), D3D11_SDK_VERSION, &rendermngr->d3d11Device, NULL, &rendermngr->d3d11DevCon);

	if (FAILED(hr))
	{
		logprint("D3D : Failed creating device!");
		return -1;
	}

	ID3D11Device1* device111 = NULL;
	ID3D11DeviceContext1* devicecon111 = NULL;

	rendermngr->d3d11Device->QueryInterface(__uuidof(ID3D11Device1), (void**)&device111);
	rendermngr->d3d11DevCon->QueryInterface(__uuidof(ID3D11DeviceContext1), (void**)&devicecon111);

	IDXGIDevice1* dxgidevice = NULL;

	hr = device111->QueryInterface(__uuidof(IDXGIDevice1), (void**)&dxgidevice);

	if (FAILED(hr))
	{
		logprint("D3D : Failed Querying IDXGIDevice!");
		return -1;
	}

	// Ensure that DXGI does not queue more than one frame at a time. This both reduces 
	// latency and ensures that the application will only render after each VSync, minimizing 
	// power consumption.
	dxgidevice->SetMaximumFrameLatency(1);

	IDXGIAdapter* dxgiAdapter = NULL;

	hr = dxgidevice->GetAdapter(&dxgiAdapter);

	if (FAILED(hr))
	{
		logprint("D3D : Failed obtaining IDXGIAdapter!");
		return -1;
	}

	hr = dxgiAdapter->EnumOutputs(0, &rendermngr->dxgiOutput);

	if (FAILED(hr))
	{
		logprint("D3D : Failed obtaining IDXGIOutput!");
		return -1;
	}

	IDXGIFactory2* dxgifactory  = NULL;

	hr = dxgiAdapter->GetParent(__uuidof(IDXGIFactory2), (void**)&dxgifactory);

	if (FAILED(hr))
	{
		logprint("D3D : Failed obtaining IDXGIFactory");
		return -1;
	}

	IUnknown* window = (IUnknown*)app_handle;

	hr = dxgifactory->CreateSwapChainForComposition((IUnknown*)device111, &swapChainDesc, NULL, &rendermngr->SwapChain);
	//hr = dxgifactory->CreateSwapChainForCoreWindow((IUnknown*)device111, window, &swapChainDesc, NULL, &rendermngr->SwapChain);
	if (FAILED(hr))
	{
		logprint("D3D : Failed Creating SwapChain for CoreWindow");
		return -1;
	}

	device111->Release();
	devicecon111->Release();
	dxgidevice->Release();
	dxgiAdapter->Release();
	dxgifactory->Release();

	#else
	//Describe our Buffer
	 DXGI_MODE_DESC bufferDesc;

	 ZeroMemory(&bufferDesc, sizeof(DXGI_MODE_DESC));

	 bufferDesc.Width = width;
	 bufferDesc.Height = height;
	 bufferDesc.RefreshRate.Numerator = 60;
	 bufferDesc.RefreshRate.Denominator = 1;
	 bufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	 bufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	 bufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

	//Describe our SwapChain
	DXGI_SWAP_CHAIN_DESC swapChainDesc;

	ZeroMemory(&swapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));

	swapChainDesc.BufferDesc = bufferDesc;
	swapChainDesc.SampleDesc.Count = 1;
	swapChainDesc.SampleDesc.Quality = 0;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.BufferCount = 1;
	swapChainDesc.OutputWindow = hwnd;
	swapChainDesc.Windowed = TRUE;
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;

	hr = D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, D3D11_CREATE_DEVICE_DEBUG  , featureLevels, ARRAYSIZE(featureLevels),
		D3D11_SDK_VERSION, &swapChainDesc, &rendermngr->SwapChain, &rendermngr->d3d11Device, NULL, &rendermngr->d3d11DevCon);

	if (FAILED(hr))
	{
		logprint("D3D : Failed Creating device and swap chain! error: %d",hr);
		return -1;
	}

	//hr = D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, 0  , featureLevels, ARRAYSIZE(featureLevels),
	//	D3D11_SDK_VERSION, &swapChainDesc, &rendermngr->SwapChain, &rendermngr->d3d11Device, NULL, &rendermngr->d3d11DevCon);
	#endif

	//Create our BackBuffer
	ID3D11Texture2D* BackBuffer;
	hr = rendermngr->SwapChain->GetBuffer( 0, __uuidof( ID3D11Texture2D ), (void**)&BackBuffer );

	if (FAILED(hr))
	{
		logprint("D3D : Failed Creating backbuffer! error: %d",hr);
		return -1;
	}

	//Create our Render Target
	hr = rendermngr->d3d11Device->CreateRenderTargetView( BackBuffer, NULL, &rendermngr->renderTargetView );
	BackBuffer->Release();

	if (FAILED(hr))
	{
		logprint("D3D : Failed Creating RenderTargetView! error: %d",hr);
		return -1;
	}

	//Set our Render Target
	rendermngr->d3d11DevCon->OMSetRenderTargets( 1, &rendermngr->renderTargetView, NULL );

	rendermngr->currentTargetView = rendermngr->renderTargetView;

	
	//create the viewport
	
	D3D11_VIEWPORT vp;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	vp.Width = width;
	vp.Height = height;
	vp.MinDepth = rendermngr->near_value;
	vp.MaxDepth = rendermngr->far_value;


	rendermngr->d3d11DevCon->RSSetViewports(1,&vp);

	

	//initialize matrix

	rendermngr->view = XMMatrixLookAtLH( XMVectorSet( 0.0f, 0.0f, -0.5f, 0.0f ),XMVectorSet( 0.0f, 0.0f, 0.0f, 0.0f ),XMVectorSet( 0.0f, 1.0f, 0.0f, 0.0f ));

	rendermngr->projection = XMMatrixOrthographicOffCenterLH(-rendermngr->scroll_vector.x * rendermngr->zoom_factor, (-rendermngr->scroll_vector.x + (float)rendermngr->screen_info.width ) * rendermngr->zoom_factor,-rendermngr->scroll_vector.y * rendermngr->zoom_factor,(-rendermngr->scroll_vector.y + (float)rendermngr->screen_info.height) * rendermngr->zoom_factor, -1.0, 1.0);
	rendermngr->world = XMMatrixIdentity();


	rendermngr->tmpvertexBuffer = NULL;

	//set rasterizer options
	// Setup the raster description which will determine how and what polygons will be drawn.
	D3D11_RASTERIZER_DESC rasterDesc;
	rasterDesc.AntialiasedLineEnable = false;
	rasterDesc.CullMode = D3D11_CULL_BACK;
	rasterDesc.DepthBias = 0;
	rasterDesc.DepthBiasClamp = 0.0f;
	rasterDesc.DepthClipEnable = true;
	rasterDesc.FillMode = D3D11_FILL_SOLID;
	rasterDesc.FrontCounterClockwise = true;
	rasterDesc.MultisampleEnable = false;
	rasterDesc.ScissorEnable = false;
	rasterDesc.SlopeScaledDepthBias = 0.0f;

	// Create the rasterizer state from the description we just filled out.
	hr = rendermngr->d3d11Device->CreateRasterizerState(&rasterDesc, &rendermngr->m_rasterState);


	// Now set the rasterizer state.
	rendermngr->d3d11DevCon->RSSetState(rendermngr->m_rasterState);


	//set sampler state
	D3D11_SAMPLER_DESC sampDesc;
	ZeroMemory( &sampDesc, sizeof(sampDesc) );
	sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
    sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
    sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
    sampDesc.MinLOD = 0;
    sampDesc.MaxLOD = D3D11_FLOAT32_MAX;


	rendermngr->d3d11Device->CreateSamplerState(&sampDesc,&rendermngr->texSamplerState);

	rendermngr->d3d11DevCon->PSSetSamplers(0,1,&rendermngr->texSamplerState);


	render_blend_alpha(rendermngr);

	//_render_compute_orientation(rendermngr, R_LANDSCAPE);


	return 0;
}

void render_recreatescreen(render_manager_t* const rendermngr,wbool fullscreen,void* app_handle)
{
	logprint("Not implemented");
}

void render_resizescreen(render_manager_t* const rendermngr,int16_t new_width,int16_t new_height)
{
	logprint("Not implemented");
}

void render_destroyscreen(render_manager_t* const rendermngr)
{
	logprint("Not implemented");
}


void render_draw_compound_textured_array(render_manager_t* const render_mngr,TEXTURE_ID texture,float* vertices,float* texture_coord,wbool repeat_texture,ColorA_t color,int16_t* sprite_sizes,int32_t num_sprite,int32_t size_vertice)
{
	int32_t draw_size = 0;
	int32_t ivertices = 0;

	//compute matrix
	render_mngr->world = render_mngr->scale_matrix * render_mngr->rotation_matrix * render_mngr->position_matrix;
	XMMATRIX wvp =  XMMatrixTranspose(render_mngr->world * render_mngr->view  * render_mngr->projection);
	_update_uniform(render_mngr,&wvp,"WVP");


	float col[] = {color.r,color.g,color.b,color.a};

	render_setuniform_float4(render_mngr,"inColor",col);

	render_bind_texture(render_mngr,texture,wfalse,wfalse);


	for(int32_t isprite = 0; isprite < num_sprite; isprite++)
	{
		if(sprite_sizes[isprite] == 0)
			break;

		draw_size += sprite_sizes[isprite];
	}

	D3D11_BUFFER_DESC bd;
	bd.Usage = D3D11_USAGE_IMMUTABLE;
	bd.ByteWidth = sizeof( float ) * (draw_size * size_vertice);
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;

	// Fill in the subresource data.
	D3D11_SUBRESOURCE_DATA InitData;
	InitData.pSysMem = vertices;
	InitData.SysMemPitch = 0;
	InitData.SysMemSlicePitch = 0;

	render_mngr->d3d11Device->CreateBuffer(&bd,&InitData,&render_mngr->tmpvertexBuffer);

	D3D11_SUBRESOURCE_DATA TexCoordData;
	TexCoordData.pSysMem = texture_coord;
	TexCoordData.SysMemPitch = 0;
	TexCoordData.SysMemSlicePitch = 0;

	render_mngr->d3d11Device->CreateBuffer(&bd,&TexCoordData,&render_mngr->tmptexCoordBuffer);

	UINT stride[] = {sizeof( float ) * size_vertice,sizeof( float ) * size_vertice};
	UINT offset[] = {0,0};

	ID3D11Buffer* buffer_array[] = {render_mngr->tmpvertexBuffer,render_mngr->tmptexCoordBuffer};

	render_mngr->d3d11DevCon->IASetVertexBuffers(0,2,buffer_array,stride,offset);


	render_mngr->d3d11DevCon->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	render_mngr->d3d11DevCon->Draw(draw_size,0);

	render_mngr->tmpvertexBuffer->Release();
	render_mngr->tmpvertexBuffer = NULL;

	render_mngr->tmptexCoordBuffer->Release();
	render_mngr->tmptexCoordBuffer = NULL;
}

void render_getworld_coord(render_manager_t* const rendermngr,int winx,int winy,float* xcoord,float* ycoord,float* zcoord)
{
	XMFLOAT3 tmp_vector = XMFLOAT3((float)winx,(float)winy,0.0f);
	XMVECTOR outvector = XMVector3Unproject(XMLoadFloat3(&tmp_vector),0,0,rendermngr->screen_info.width,rendermngr->screen_info.height,0.0,1.0,rendermngr->projection,rendermngr->view,rendermngr->world);
	
	#if defined(WINAPI_FAMILY) && (WINAPI_FAMILY == WINAPI_FAMILY_PHONE_APP)
		(*xcoord) = outvector.n128_f32[0];
		(*ycoord) = outvector.n128_f32[1];
		(*zcoord) = outvector.n128_f32[2];
	#else
		(*xcoord) = outvector.m128_f32[0];
		(*ycoord) = outvector.m128_f32[1];
		(*zcoord) = outvector.m128_f32[2];
	#endif
}

void render_setzoomproj(render_manager_t* const render_mngr,float zoom)
{
	float left = (int)(-render_mngr->scroll_vector.x * zoom);
	float right = (int)(left + (render_mngr->screen_info.width * zoom));

	float bottom = (int)(-render_mngr->scroll_vector.y * zoom);
	float top = (int)(bottom + (render_mngr->screen_info.height * zoom));

	render_mngr->projection = XMMatrixOrthographicOffCenterLH(left,right,bottom,top, -1.0, 1.0);

	//render_mngr->projection = XMMatrixMultiply(render_mngr->projection, render_mngr->Orientation3D);

	XMMATRIX WVP = XMMatrixTranspose(render_mngr->world * render_mngr->view  * render_mngr->projection);

	_update_uniform(render_mngr,&WVP,"WVP");
}


void render_update_screen(render_manager_t* const render_mngr)
{
	float left = (int)(-render_mngr->scroll_vector.x * render_mngr->zoom_factor);
	float right = (int)(left + (render_mngr->screen_info.width * render_mngr->zoom_factor));

	float bottom = (int)(-render_mngr->scroll_vector.y * render_mngr->zoom_factor);
	float top = (int)(bottom + (render_mngr->screen_info.height * render_mngr->zoom_factor));

	render_mngr->projection = XMMatrixOrthographicOffCenterLH(left,right,bottom,top, -1.0, 1.0);

	//render_mngr->projection = XMMatrixMultiply(render_mngr->projection, render_mngr->Orientation3D);

	render_mngr->world  = XMMatrixIdentity();

	//update screen position
	render_mngr->screen_info.position.x = (int)(-render_mngr->scroll_vector.x * render_mngr->zoom_factor);
	render_mngr->screen_info.position.y = (int)((-render_mngr->scroll_vector.y + render_mngr->screen_info.height) * render_mngr->zoom_factor);

	XMMATRIX WVP = XMMatrixTranspose(render_mngr->world * render_mngr->view  * render_mngr->projection);

	render_mngr->d3d11DevCon->OMSetRenderTargets(1,&render_mngr->renderTargetView,NULL);

	_update_uniform(render_mngr,&WVP,"WVP");
}

void render_blend_alpha(render_manager_t* const render_mngr)
{
	render_setblend(render_mngr,blend_add,blend_alpha);
}

void render_setblend(render_manager_t* const render_mngr,blend_eq eq,blend_op op)
{
	if(render_mngr->Transparency != NULL)
		render_mngr->Transparency->Release();

	D3D11_BLEND_DESC blendDesc;
	ZeroMemory( &blendDesc, sizeof(blendDesc) );

	D3D11_RENDER_TARGET_BLEND_DESC rtbd;
	ZeroMemory( &rtbd, sizeof(rtbd) );

	rtbd.BlendEnable			 = true;
	rtbd.SrcBlend				 = D3D11_BLEND_SRC_ALPHA;
	rtbd.DestBlend				 = D3D11_BLEND_INV_SRC_ALPHA;
	rtbd.BlendOp				 = D3D11_BLEND_OP_ADD;
	rtbd.SrcBlendAlpha			 = D3D11_BLEND_ZERO;
	rtbd.DestBlendAlpha			 = D3D11_BLEND_ZERO;
	rtbd.BlendOpAlpha			 = D3D11_BLEND_OP_ADD;
	rtbd.RenderTargetWriteMask	 = D3D11_COLOR_WRITE_ENABLE_ALL;

	if(eq != render_mngr->c_blend_eq)
	{
		switch(eq)
		{
			case blend_add:
				rtbd.BlendOp = D3D11_BLEND_OP_ADD;
				rtbd.BlendOpAlpha = D3D11_BLEND_OP_ADD;
				break;
			case blend_max:
				rtbd.BlendOp = D3D11_BLEND_OP_MAX;
				rtbd.BlendOpAlpha = D3D11_BLEND_OP_MAX;
				break;
			case blend_min:
				rtbd.BlendOp = D3D11_BLEND_OP_MIN;
				rtbd.BlendOpAlpha = D3D11_BLEND_OP_MIN;
				break;
		}

		render_mngr->c_blend_eq = eq;
	}

	if(op != render_mngr->c_blend_op)
	{
		switch(op)
		{
			case blend_replace:
				rtbd.SrcBlend = D3D11_BLEND_ONE;
				rtbd.DestBlend = D3D11_BLEND_ZERO;
			break;
			case blend_alpha:
				rtbd.SrcBlend = D3D11_BLEND_SRC_ALPHA;
				rtbd.DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
			break;
			case blend_additive:
				rtbd.SrcBlend = D3D11_BLEND_SRC_ALPHA;
				rtbd.DestBlend = D3D11_BLEND_ONE;
				break;
		}

		render_mngr->c_blend_op = op;
	}

	render_mngr->enabled_blend = wtrue;

	blendDesc.AlphaToCoverageEnable = false;
	blendDesc.RenderTarget[0] = rtbd;
	
	render_mngr->d3d11Device->CreateBlendState(&blendDesc, &render_mngr->Transparency);
	render_mngr->d3d11DevCon->OMSetBlendState(render_mngr->Transparency, NULL, 0xffffffff);
}

void render_clear_screen(render_manager_t* const render_mngr)
{
	const float bgColor[] = { 0.0f, 0.0f, 0.0f, 1.0f };
	render_mngr->d3d11DevCon->ClearRenderTargetView(render_mngr->currentTargetView,bgColor);
}

void render_clear_screenwithcolor(render_manager_t* const rendermngr,ColorA_t back_color)
{
	const float bgColor[] = { back_color.r, back_color.g, back_color.b, back_color.a };
	rendermngr->d3d11DevCon->ClearRenderTargetView(rendermngr->currentTargetView,bgColor);
}


//no stack management in our case, we just reset world matrix when calling push matrix
void render_push_matrix(render_manager_t* const render_mngr)
{
	//reset world matrix
	render_mngr->scale_matrix = XMMatrixIdentity();
	render_mngr->rotation_matrix = XMMatrixIdentity();
	render_mngr->position_matrix = XMMatrixIdentity();

	render_mngr->world = XMMatrixIdentity();
	XMMATRIX wvp = XMMatrixTranspose(render_mngr->world * render_mngr->view  * render_mngr->projection);
	_update_uniform(render_mngr, &wvp, "WVP");
}

void render_pop_matrix(render_manager_t* const render_mngr)
{
	
}


void render_draw_textured_array(render_manager_t* const render_mngr,TEXTURE_ID texture,float* vertices,float* texture_coord,wbool repeat_texture,float rotation,int32_t num_vertices,int32_t size_vertice,float x,float y,ColorA_t* const color,wbool is_3d_texture)
{
	//bind texture
	
	render_bind_texture(render_mngr,texture,repeat_texture,is_3d_texture);

	if(rotation != 0.0f)
		render_mngr->world *=  XMMatrixRotationZ(rotation);

	if(color != NULL)
	{
		float col[] = {color->r,color->g,color->b,color->a};

		render_setuniform_float4(render_mngr,"inColor",col);
	}

	render_mngr->world *= XMMatrixTranslation(x,y,0);
	XMMATRIX wvp = XMMatrixTranspose(render_mngr->world * render_mngr->view  * render_mngr->projection);
	_update_uniform(render_mngr,&wvp,"WVP");

	D3D11_BUFFER_DESC bd;
	bd.Usage = D3D11_USAGE_IMMUTABLE;
	bd.ByteWidth = sizeof( float ) * (num_vertices * size_vertice);
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;

	// Fill in the subresource data.
	D3D11_SUBRESOURCE_DATA InitData;
	InitData.pSysMem = vertices;
	InitData.SysMemPitch = 0;
	InitData.SysMemSlicePitch = 0;

	render_mngr->d3d11Device->CreateBuffer(&bd,&InitData,&render_mngr->tmpvertexBuffer);

	D3D11_SUBRESOURCE_DATA TexCoordData;
	TexCoordData.pSysMem = texture_coord;
	TexCoordData.SysMemPitch = 0;
	TexCoordData.SysMemSlicePitch = 0;

	render_mngr->d3d11Device->CreateBuffer(&bd,&TexCoordData,&render_mngr->tmptexCoordBuffer);


	UINT stride[] = {sizeof( float ) * size_vertice,sizeof( float ) * size_vertice};
	UINT offset[] = {0,0};

	ID3D11Buffer* buffer_array[] = {render_mngr->tmpvertexBuffer,render_mngr->tmptexCoordBuffer};

	render_mngr->d3d11DevCon->IASetVertexBuffers(0,2,buffer_array,stride,offset);


	render_mngr->d3d11DevCon->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	render_mngr->d3d11DevCon->Draw(num_vertices,0);

	render_mngr->tmpvertexBuffer->Release();
	render_mngr->tmpvertexBuffer = NULL;

	render_mngr->tmptexCoordBuffer->Release();
	render_mngr->tmptexCoordBuffer = NULL;

	//reset world matrix
	render_mngr->world = XMMatrixIdentity();
	wvp =  XMMatrixTranspose(render_mngr->world * render_mngr->view  * render_mngr->projection);
	_update_uniform(render_mngr,&wvp,"WVP");
}

void render_start_draw_buffer(render_manager_t* const render_mngr,TEXTURE_ID texture,VBO_ID vertices_buffer,VBO_ID texcoord_buffer,VBO_ID color_buffer,int32_t size_vertice)
{
	if(texture)
	{
		//bind texture
		render_bind_texture(render_mngr,texture,wfalse,wfalse);
	}

	UINT stride[] = {sizeof( float ) * size_vertice,sizeof( float ) * size_vertice};
	UINT offset[] = {0,0};

	ID3D11Buffer* buffer_array[] = {vertices_buffer,texcoord_buffer};

	render_mngr->d3d11DevCon->IASetVertexBuffers(0,2,buffer_array,stride,offset);
	render_mngr->d3d11DevCon->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	//compute matrix
	render_mngr->world = render_mngr->scale_matrix * render_mngr->rotation_matrix * render_mngr->position_matrix;
	XMMATRIX wvp =  XMMatrixTranspose(render_mngr->world * render_mngr->view  * render_mngr->projection);
	_update_uniform(render_mngr,&wvp,"WVP");
}

void render_end_draw_buffer(render_manager_t* const render_mngr,wbool has_textcoord,wbool has_colors)
{

}

void render_draw_buffer(render_manager_t* const render_mngr,int32_t start_index,int32_t length)
{
	render_mngr->d3d11DevCon->Draw(length,start_index);
}

void render_end_draw_buffer()
{

}

void render_bind_buffer(render_manager_t* const rendermngr,VBO_ID buffer)
{

}

void render_step_update_buffer(render_manager_t* const rendermngr,float* buffer_data,size_t offset,size_t size,VBO_ID buffer)
{
	D3D11_BOX destRegion;
	destRegion.left = offset;
	destRegion.right = destRegion.left + size;
	destRegion.top = 0;
	destRegion.bottom = 1;
	destRegion.front = 0;
	destRegion.back = 1;

	rendermngr->d3d11DevCon->UpdateSubresource(buffer,0,&destRegion,buffer_data,size,sizeof(float));
}

void render_unbind_buffer(render_manager_t* const rendermngr,VBO_ID buffer)
{

}

void render_draw_3Dbufferindices(render_manager_t* const render_mngr,VBO_ID vertices,VBO_ID indices,VBO_ID normals,size_t num_indices,render_mode mesh_mode,wbool has_normals)
{
	int32_t draw_size = 0;
	int32_t ivertices = 0;
	int32_t offset_draw = 0;
	int32_t offset_vertex_draw = 0;

	UINT stride[] = {sizeof( float ) * 3,sizeof( float ) * 3};
	UINT offset[] = {0,0};

	if(has_normals)
	{
		ID3D11Buffer* buffer_array[] = {vertices,normals};

		render_mngr->d3d11DevCon->IASetVertexBuffers(0,2,buffer_array,stride,offset);
	}
	else
	{
		ID3D11Buffer* buffer_array[] = {vertices};

		render_mngr->d3d11DevCon->IASetVertexBuffers(0,1,buffer_array,stride,offset);
	}

	if(mesh_mode == R_FILL)
		render_mngr->d3d11DevCon->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	else if(mesh_mode == R_WIREFRAME)
		render_mngr->d3d11DevCon->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);

	render_mngr->d3d11DevCon->IASetIndexBuffer(indices, DXGI_FORMAT_R16_UINT,0);

	render_mngr->d3d11DevCon->DrawIndexed(num_indices,0,0);
}


void render_draw_compound_textured_buffer(render_manager_t* const render_mngr,VBO_ID vertices_id,VBO_ID text_coord,VBO_ID indices,Vector_t* positions,float* rotations,int32_t num_sprite,int16_t* sprite_sizes,int16_t* sprite_indices_size)
{
	int32_t draw_size = 0;
	int32_t ivertices = 0;
	int32_t offset_draw = 0;
	int32_t offset_vertex_draw = 0;

	UINT stride[] = {sizeof( float ) * 2,sizeof( float ) * 2};
	UINT offset[] = {0,0};

	ID3D11Buffer* buffer_array[] = {vertices_id,text_coord};

	render_mngr->d3d11DevCon->IASetVertexBuffers(0,2,buffer_array,stride,offset);
	render_mngr->d3d11DevCon->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	render_mngr->d3d11DevCon->IASetIndexBuffer(indices, DXGI_FORMAT_R16_UINT,0);

	for(int32_t isprite = 0; isprite < num_sprite; isprite++)
	{
		if(sprite_sizes[isprite] == 0)
			break;

		draw_size += sprite_indices_size[isprite];
		
	}

	render_mngr->d3d11DevCon->DrawIndexed(draw_size, 0, 0);
}


void render_set_position(render_manager_t* const render_mngr,Vector_t position)
{
	position.x = (int)position.x;
	position.y = (int)position.y;
	render_mngr->position_matrix = XMMatrixTranslation((float)position.x,(float)position.y,0);
}

void render_set_position3(render_manager_t* const render_mngr,Vector3_t position)
{
	render_mngr->position_matrix = XMMatrixTranslation( (float)position.x,(float)position.y,(float)position.z);
}



void render_set_rotation(render_manager_t* const render_mngr,float angle)
{
	render_mngr->rotation_matrix =  XMMatrixRotationZ((float)deg2rad(angle));
}

void render_set_scale(render_manager_t* const render_mngr,float scaleX,float scaleY,float scaleZ)
{
	render_mngr->scale_matrix = XMMatrixScaling(scaleX,scaleY,scaleZ);
}

void render_set_rotation2(render_manager_t* const render_mngr,float angle_x,float angle_y,float angle_z)
{
	render_mngr->scale_matrix =  XMMatrixRotationRollPitchYaw(angle_x,angle_y,angle_z);
}

void render_mult_matrix(render_manager_t* const render_mngr,const float* matrix)
{
	logprint("not implemented");
	/*render_mngr->world *=  XMMATRIX(matrix);
	XMMATRIX wvp =  XMMatrixTranspose(render_mngr->world * render_mngr->view  * render_mngr->projection);
	_update_uniform(render_mngr,&wvp,"WVP");*/
}


void render_start_draw_array(render_manager_t* const render_mngr, int32_t size_vertice,uint32_t numVertices,float* vertices_array,float* tex_coord_array)
{
		//compute matrix
		render_mngr->world = render_mngr->scale_matrix * render_mngr->rotation_matrix * render_mngr->position_matrix;
		XMMATRIX wvp =  XMMatrixTranspose(render_mngr->world * render_mngr->view  * render_mngr->projection);
		_update_uniform(render_mngr,&wvp,"WVP");

		D3D11_BUFFER_DESC bd;
		bd.Usage = D3D11_USAGE_IMMUTABLE;
		bd.ByteWidth = sizeof( float ) * (numVertices * size_vertice);
		bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bd.CPUAccessFlags = 0;
		bd.MiscFlags = 0;

		// Fill in the subresource data.
		D3D11_SUBRESOURCE_DATA InitData;
		InitData.pSysMem = vertices_array;
		InitData.SysMemPitch = 0;
		InitData.SysMemSlicePitch = 0;

		render_mngr->d3d11Device->CreateBuffer(&bd,&InitData,&render_mngr->tmpvertexBuffer);


		if(tex_coord_array != NULL)
		{
			D3D11_SUBRESOURCE_DATA TexCoordData;
			TexCoordData.pSysMem = tex_coord_array;
			TexCoordData.SysMemPitch = 0;
			TexCoordData.SysMemSlicePitch = 0;

			UINT stride[] = {sizeof( float ) * size_vertice,sizeof( float ) * size_vertice};
			UINT  offset[] = {0,0};

			render_mngr->d3d11Device->CreateBuffer(&bd,&TexCoordData,&render_mngr->tmptexCoordBuffer);

			ID3D11Buffer* buffer_array[] = {render_mngr->tmpvertexBuffer,render_mngr->tmptexCoordBuffer};

			render_mngr->d3d11DevCon->IASetVertexBuffers(0,2,buffer_array,stride,offset);
		}
		else
		{
			UINT stride[] = {sizeof( float ) * size_vertice};
			UINT  offset[] = {0};
			render_mngr->d3d11DevCon->IASetVertexBuffers(0,1,&render_mngr->tmpvertexBuffer,stride,offset);
		}	

		render_mngr->d3d11DevCon->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}

void render_draw_array(render_manager_t* const render_mngr,int32_t offset, int32_t numVertices)
{
	render_mngr->d3d11DevCon->Draw(numVertices,offset);
}

void render_draw_line(render_manager_t* const render_mngr,int32_t offset, int32_t numVertices)
{
	render_mngr->d3d11DevCon->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP);
	render_mngr->d3d11DevCon->Draw(numVertices,offset);
}

void render_draw_lineloop(render_manager_t* const render_mngr,int32_t offset, int32_t numVertices)
{
	render_mngr->d3d11DevCon->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);
	render_mngr->d3d11DevCon->Draw(numVertices,offset);
}
void render_draw_fan(render_manager_t* const render_mngr,int32_t offset, int32_t numVertices)
{
	render_mngr->d3d11DevCon->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
	render_mngr->d3d11DevCon->Draw(numVertices,offset);
}

void render_draw_points(render_manager_t* const render_mngr,int32_t offset, int32_t numVertices,float pointsize)
{
	render_mngr->d3d11DevCon->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
	render_mngr->d3d11DevCon->Draw(numVertices,offset);
}


void render_end_draw_array(render_manager_t* const render_mngr,wbool has_textcoord)
{
	if(render_mngr->tmpvertexBuffer)
	{
		render_mngr->tmpvertexBuffer->Release();
		render_mngr->tmpvertexBuffer = NULL;
	}

	if(has_textcoord && render_mngr->tmptexCoordBuffer)
	{
		render_mngr->tmptexCoordBuffer->Release();
		render_mngr->tmptexCoordBuffer = NULL;
	}
}


void render_swap_buffers(render_manager_t* const rendermngr)
{
	#if defined(WINAPI_FAMILY) && (WINAPI_FAMILY == WINAPI_FAMILY_PHONE_APP)
		rendermngr->SwapChain->Present(1,0);
	#else
		rendermngr->SwapChain->Present(0,0);
	#endif
	
}

void render_use_program(render_manager_t* const rendermngr,SHADERPROGRAM_ID program)
{
	rendermngr->d3d11DevCon->VSSetShader(program->vertex_shader->vertex_shader,0,0);
	rendermngr->d3d11DevCon->PSSetShader(program->pixel_shader->pixel_shader,0,0);

	if(program->vertex_shader->constant_buffer != NULL)
		rendermngr->d3d11DevCon->VSSetConstantBuffers(0,1,&program->vertex_shader->constant_buffer);

	if(program->vertex_shader->input_layout != NULL)
		rendermngr->d3d11DevCon->IASetInputLayout(program->vertex_shader->input_layout);


	rendermngr->current_program = program;
}

void render_use_subprogram(render_manager_t* const rendermngr,SHADERPROGRAM_ID prog_o,int32_t i_sub)
{
	logprint("Not implemented");
}

int32_t render_get_numsubprogram(render_manager_t* const rendermngr,SHADERPROGRAM_ID prog_o)
{
	return 0;
}


void render_delete_shader(render_manager_t* const rendermngr,SHADER_ID shader)
{
	if(shader->is_fragment_shader && shader->pixel_shader)
	{
		shader->pixel_shader->Release();
	}
	else if(!shader->is_fragment_shader && shader->vertex_shader)
	{
		shader->vertex_shader->Release();
	}

	if(shader->input_layout != NULL)
		shader->input_layout->Release();

	if(shader->constant_buffer != NULL)
	{
		shader->constant_buffer->Release();

		wf_free(shader->const_buf_def->buffer_content);
		hashmap_free((hashmap_t*)shader->const_buf_def->buffer_element_def);
		wf_free(shader->const_buf_def->buffer_element_def);
		wf_free(shader->const_buf_def);
	}



	wf_free(shader);
}

void render_setshader_attribute(render_manager_t* const rendermngr,SHADERPROGRAM_ID prog_id,const char* name,int32_t size,int32_t stride,void* offset)
{
	logprint("Not implemented");
}


void render_setuniform_int(render_manager_t* const rendermngr,const char* uniform_name,int value)
{
	logprint("Not implemented");
}


void render_setuniform_float(render_manager_t* const rendermngr,const char* uniform_name,float value)
{
	_update_uniform(rendermngr,&value,uniform_name);

}
void render_setuniform_float2(render_manager_t* const rendermngr,const char* uniform_name,float vector[2])
{
	XMFLOAT2 vec;
	vec.x = vector[0];
	vec.y = vector[1];

	_update_uniform(rendermngr,&vec,uniform_name);

}
void render_setuniform_float3(render_manager_t* const rendermngr,const char* uniform_name,float vector[3])
{
	XMFLOAT3 vec;
	vec.x = vector[0];
	vec.y = vector[1];
	vec.z = vector[2];

	_update_uniform(rendermngr,&vec,uniform_name);
}
void render_setuniform_float4(render_manager_t* const rendermngr,const char* uniform_name,float vector[4])
{
	XMFLOAT4 vec;
	vec.x = vector[0];
	vec.y = vector[1];
	vec.z = vector[2];
	vec.w = vector[3];

	_update_uniform(rendermngr,&vec,uniform_name);

}

void render_setuniform_arrayfloat3(render_manager_t* const rendermngr,const char* uniform_name,float* data,int32_t count)
{
	logprint("Not implemented");
}

void render_setuniform_arrayfloat4(render_manager_t* const rendermngr,const char* uniform_name,float* data,int32_t count)
{
	logprint("Not implemented");
}

void render_bindattribute_floatVBO(render_manager_t* const render_mngr,const char* attribute_name,VBO_ID vbo,size_t size_element)
{
	logprint("Not implemented");
}

VAO_ID render_gen_vao(render_manager_t* const rendermngr)
{
	logprint("Not implemented");
	return NULL;
}

void render_bind_vao(render_manager_t* const rendermngr,VAO_ID vao)
{
	logprint("Not implemented");
}

void render_unbind_vao(render_manager_t* const rendermngr)
{
	logprint("Not implemented");
}

FBO_ID render_alloc_fbo(render_manager_t* const rendermngr)
{
	FBO_ID fbo = (FBO_ID)wf_malloc(sizeof(D3DFbo));
	return fbo;
}

RBO_ID render_alloc_rbo(render_manager_t* const rendermngr)
{
	logprint("Not implemented");
	return NULL;
}

TEXTURE_ID render_alloc_texture(render_manager_t* const rendermngr)
{
	TEXTURE_ID texture = (TEXTURE_ID)wf_malloc(sizeof(D3DTexture));
	return texture;
}

void render_create_fbo(render_manager_t* const rendermngr,FBO_ID fbo_id,TEXTURE_ID fbo_texture,int16_t fbo_width,int16_t fbo_height)
{
	D3D11_TEXTURE2D_DESC textureDesc;
	ZeroMemory(&textureDesc, sizeof(textureDesc));
    
    // Setup the texture description.
    // We will have our map be a square
    // We will need to have this texture bound as a render target AND a shader resource
    textureDesc.Width = fbo_width;
    textureDesc.Height = fbo_height;
    textureDesc.MipLevels = 1;
    textureDesc.ArraySize = 1;
    textureDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
    textureDesc.SampleDesc.Count = 1;
    textureDesc.Usage = D3D11_USAGE_DEFAULT;
    textureDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
    textureDesc.CPUAccessFlags = 0;
    textureDesc.MiscFlags = 0;
    
    // Create the texture
	rendermngr->d3d11Device->CreateTexture2D(&textureDesc,NULL,&fbo_texture->texture);

	// Setup the description of the render target view.
	D3D11_RENDER_TARGET_VIEW_DESC renderTargetViewDesc;
    renderTargetViewDesc.Format = textureDesc.Format;
    renderTargetViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
    renderTargetViewDesc.Texture2D.MipSlice = 0;
    
    // Create the render target view.
	rendermngr->d3d11Device->CreateRenderTargetView(fbo_texture->texture, &renderTargetViewDesc, &fbo_id->m_renderTargetView);

	D3D11_SHADER_RESOURCE_VIEW_DESC shaderResourceViewDesc;
	shaderResourceViewDesc.Format = textureDesc.Format;
    shaderResourceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
    shaderResourceViewDesc.Texture2D.MostDetailedMip = 0;
    shaderResourceViewDesc.Texture2D.MipLevels = 1;
    
    // Create the shader resource view.
	rendermngr->d3d11Device->CreateShaderResourceView(fbo_texture->texture, &shaderResourceViewDesc, &fbo_texture->texturerv);

	fbo_id->m_renderTexture = fbo_texture;
}
void render_bind_framebuffer(render_manager_t* const rendermngr,FBO_ID framebuffer,TEXTURE_ID texture,int16_t fbo_width,int16_t fbo_height)
{
	D3D11_VIEWPORT vp;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	vp.Width = fbo_width;
	vp.Height = fbo_height;
	vp.MinDepth = rendermngr->near_value;
	vp.MaxDepth = rendermngr->far_value;

	rendermngr->d3d11DevCon->RSSetViewports(1,&vp);

	//initialize matrix
	rendermngr->projection = XMMatrixOrthographicOffCenterLH(0.0f, (float)fbo_width,0.0f,fbo_height, rendermngr->near_value, rendermngr->far_value);

	rendermngr->d3d11DevCon->OMSetRenderTargets(1,&framebuffer->m_renderTargetView,NULL);
	rendermngr->currentTargetView = framebuffer->m_renderTargetView;
}

void render_unbind_framebuffer(render_manager_t* const rendermngr,FBO_ID framebuffer)
{
	rendermngr->currentTargetView = rendermngr->renderTargetView;
	render_update_screen(rendermngr);

	D3D11_VIEWPORT vp;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	vp.Width = (float)rendermngr->screen_info.width;
	vp.Height = (float)rendermngr->screen_info.height;
	vp.MinDepth = rendermngr->near_value;
	vp.MaxDepth = rendermngr->far_value;

	rendermngr->d3d11DevCon->RSSetViewports(1,&vp);
}
void render_delete_fbo(render_manager_t* const rendermngr,FBO_ID framebuffer)
{
	framebuffer->m_renderTargetView->Release();
	wf_free(framebuffer);
}
void render_delete_rbo(render_manager_t* const rendermngr,RBO_ID renderbuffer)
{
	logprint("Not implemented");
}

void render_bindtexture_tosampler(render_manager_t* const rendermngr,TEXTURE_ID texture,const char* sampler_id,int32_t texture_unit_id,wbool is_3d)
{
	logprint("Not implemented");
}

void render_unbindtexture_tosampler(render_manager_t* const rendermngr,TEXTURE_ID texture,const char* sampler_id,int32_t texture_unit_id,wbool is_3d)
{
	logprint("Not implemented");
}

TEXTURE_ID render_gen_texture(render_manager_t* const rendermngr,uint32_t width,uint32_t height,unsigned char* buffer,unsigned long buffer_size,wbool luminance)
{
	TEXTURE_ID texture = (TEXTURE_ID)wf_malloc(sizeof(TEXTURE_ID));

	HRESULT hr;

	D3D11_TEXTURE2D_DESC tex_desc;


	tex_desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;

	tex_desc.Width = width;
	tex_desc.Height = height;
	tex_desc.ArraySize = 1;
	tex_desc.MipLevels = 1;
	tex_desc.Usage = D3D11_USAGE_DEFAULT;
	tex_desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	tex_desc.CPUAccessFlags = 0;
	tex_desc.MiscFlags = 0;
	tex_desc.SampleDesc.Count = 1;
	tex_desc.SampleDesc.Quality = 0;

	//fill in texture data
	D3D11_SUBRESOURCE_DATA texture_data;
	texture_data.pSysMem = buffer;

	texture_data.SysMemPitch = width * 4;

	texture_data.SysMemSlicePitch = buffer_size;

	hr = rendermngr->d3d11Device->CreateTexture2D(&tex_desc,&texture_data,&texture->texture);

	if(hr != S_OK)
	{
		logprint("Error while loading texture, %d",hr);
		return NULL;
	}

	//create the associated shader resource view
	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
	srvDesc.Format = tex_desc.Format;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	srvDesc.Texture2D.MipLevels = tex_desc.MipLevels;
	srvDesc.Texture2D.MostDetailedMip = 0;

	hr = rendermngr->d3d11Device->CreateShaderResourceView(texture->texture,&srvDesc,&texture->texturerv);

	if(hr != S_OK)
	{
		logprint("Error while loading texture, %d",hr);
		return NULL;
	}


	return texture;
}

TEXTURE_ID render_gen_texture3D(render_manager_t* const rendermngr,uint32_t width,uint32_t height,uint32_t depth,unsigned char* buffer,unsigned long buffer_size)
{
	logprint("Not implemented");
	return NULL;
}

void render_delete_texture(render_manager_t* const rendermngr,TEXTURE_ID texture,wbool is_3d)
{
	if(rendermngr->current_bind_texture == texture)
	{
		rendermngr->current_bind_texture = NULL;
	}

	texture->texture->Release();
	texture->texturerv->Release();
	wf_free(texture);
}

void render_bind_texture(render_manager_t* const rendermngr,TEXTURE_ID texture,wbool repeat_texture,wbool is_3d)
{
	if(rendermngr->current_bind_texture == NULL || rendermngr->current_bind_texture != texture)
	{
		rendermngr->d3d11DevCon->PSSetShaderResources(0,1,&texture->texturerv);
		rendermngr->current_bind_texture = texture;
	}
}

void render_unbind_texture(render_manager_t* const rendermngr,wbool is_3d)
{
	logprint("Not implemented");
}

void render_set_color(render_manager_t* const render_mngr,ColorA_t color)
{
	float col[] = {color.r,color.g,color.b,color.a};

	render_setuniform_float4(render_mngr,"inColor",col);

}

void render_reset_color(render_manager_t* const render_mngr)
{
	float col[] = {1.0f,1.0f,1.0f,1.0f};

	render_setuniform_float4(render_mngr,"inColor",col);
}

VBO_ID render_gen_buffer(render_manager_t* const rendermngr,float* buffer_data,size_t buffer_size,wbool dynamic,wbool stream)
{
	// Fill in a buffer description.
	D3D11_BUFFER_DESC bufferDesc;

	if (dynamic || stream)
	{
		bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
		bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	}
	else
	{
		bufferDesc.Usage = D3D11_USAGE_DEFAULT;
		bufferDesc.CPUAccessFlags = 0;
	}
	
	bufferDesc.ByteWidth        = buffer_size;
	bufferDesc.BindFlags        = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.MiscFlags        = 0;


	// Fill in the subresource data.
	D3D11_SUBRESOURCE_DATA InitData;
	InitData.pSysMem = buffer_data;
	InitData.SysMemPitch = 0;
	InitData.SysMemSlicePitch = 0;

	VBO_ID ret_val = NULL;

	HRESULT hr = rendermngr->d3d11Device->CreateBuffer( &bufferDesc, &InitData, &ret_val );

	return ret_val;
}

VBO_ID render_gen_bufferindices(render_manager_t* const rendermngr,const uint16_t* const indices,size_t buffer_size,wbool dynamic,wbool stream)
{
	// Fill in a buffer description.
	D3D11_BUFFER_DESC bufferDesc;

	if (dynamic || stream)
	{
		bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
		bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	}
	else
	{
		bufferDesc.Usage = D3D11_USAGE_DEFAULT;
		bufferDesc.CPUAccessFlags = 0;
	}

	bufferDesc.ByteWidth        = buffer_size;
	bufferDesc.BindFlags        = D3D11_BIND_INDEX_BUFFER;
	bufferDesc.MiscFlags        = 0;


	// Fill in the subresource data.
	D3D11_SUBRESOURCE_DATA InitData;
	InitData.pSysMem = indices;
	InitData.SysMemPitch = 0;
	InitData.SysMemSlicePitch = 0;

	VBO_ID ret_val = (VBO_ID)wf_calloc(1, sizeof(VBO_ID));


	HRESULT hr = rendermngr->d3d11Device->CreateBuffer( &bufferDesc, &InitData, &ret_val );

	return ret_val;
}

void render_update_buffer(render_manager_t* const rendermngr,VBO_ID buffer,float* buffer_data,size_t offset,size_t size)
{

	D3D11_BOX destRegion;
	destRegion.left = offset;
	destRegion.right = destRegion.left + size;
	destRegion.top = 0;
	destRegion.bottom = 1;
	destRegion.front = 0;
	destRegion.back = 1;

	rendermngr->d3d11DevCon->UpdateSubresource(buffer,0,&destRegion,buffer_data,size,sizeof(float));
}

void render_discard_updatebuffer(render_manager_t* const rendermngr,VBO_ID buffer,float* buffer_data,size_t buffer_size,wbool dynamic,wbool stream)
{
	if (dynamic || stream)
	{
		D3D11_MAPPED_SUBRESOURCE mappedResource;
		ZeroMemory(&mappedResource, sizeof(D3D11_MAPPED_SUBRESOURCE));

		//	Disable GPU access to the vertex buffer data.
		rendermngr->d3d11DevCon->Map(buffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
		//	Update the vertex buffer here.
		memcpy_s(mappedResource.pData, buffer_size, buffer_data, buffer_size);
		//	Reenable GPU access to the vertex buffer data.
		rendermngr->d3d11DevCon->Unmap(buffer, 0);
	}
	else
	{
		D3D11_BOX destRegion;
		destRegion.left = 0;
		destRegion.right = destRegion.left + buffer_size;
		destRegion.top = 0;
		destRegion.bottom = 1;
		destRegion.front = 0;
		destRegion.back = 1;

		rendermngr->d3d11DevCon->UpdateSubresource(buffer, 0, &destRegion, buffer_data, buffer_size, sizeof(float));
	}
}

void render_update_indices_buffer(render_manager_t* const rendermngr,VBO_ID buffer,int16_t* buffer_data,size_t offset,size_t size)
{

	D3D11_BOX destRegion;
	destRegion.left = offset;
	destRegion.right = destRegion.left + size;
	destRegion.top = 0;
	destRegion.bottom = 1;
	destRegion.front = 0;
	destRegion.back = 1;

	rendermngr->d3d11DevCon->UpdateSubresource(buffer,0,&destRegion,buffer_data,size,sizeof(int16_t));
}

void render_discard_update_indicesbuffer(render_manager_t* const rendermngr,VBO_ID buffer,uint16_t* buffer_data,size_t buffer_size,wbool dynamic,wbool stream)
{
	if (dynamic || stream)
	{
		D3D11_MAPPED_SUBRESOURCE mappedResource;
		ZeroMemory(&mappedResource, sizeof(D3D11_MAPPED_SUBRESOURCE));

		//	Disable GPU access to the vertex buffer data.
		rendermngr->d3d11DevCon->Map(buffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
		//	Update the vertex buffer here.
		memcpy_s(mappedResource.pData, buffer_size, buffer_data, buffer_size);
		//	Reenable GPU access to the vertex buffer data.
		rendermngr->d3d11DevCon->Unmap(buffer, 0);
	}
	else
	{
		D3D11_BOX destRegion;
		destRegion.left = 0;
		destRegion.right = destRegion.left + buffer_size;
		destRegion.top = 0;
		destRegion.bottom = 1;
		destRegion.front = 0;
		destRegion.back = 1;

		rendermngr->d3d11DevCon->UpdateSubresource(buffer, 0, &destRegion, buffer_data, buffer_size, sizeof(int16_t));
	}
	
}



void render_delete_buffer(VBO_ID buffer)
{
	buffer->Release();
}

SHADERPROGRAM_ID render_get_program(render_manager_t* const rendermngr,const char* program_id)
{

	if(!hashtable_haskey((hashtable_t*)rendermngr->hashtable_shader_programs,program_id))
	{
		logprint("shader program %s doesn't exist!",program_id);
		return 0;
	}

	int pos = hashtable_index((hashtable_t*)rendermngr->hashtable_shader_programs,program_id);

	return rendermngr->shader_programs[pos];
}

wbool render_delete_program(render_manager_t* const rendermngr,const char* program_id)
{
	if(!hashtable_haskey((hashtable_t*)rendermngr->hashtable_shader_programs,program_id))
	{
		logprint("shader program %s doesn't exist!",program_id);
		return wfalse;
	}

	int pos = hashtable_index((hashtable_t*)rendermngr->hashtable_shader_programs,program_id);

	wf_free(rendermngr->shader_programs[pos]);

	char* key_del = ((hashtable_t*)rendermngr->hashtable_shader_programs)->keys[pos];

	//remove from hashtable
	hashtable_removekey((hashtable_t*)rendermngr->hashtable_shader_programs,program_id);

	wf_free(key_del);

	rendermngr->shader_programs[pos] = NULL;

	return wtrue;

}

const char* render_get_error(render_manager_t* const render_mngr)
{
	logprint("Not Implemented");
	return NULL;
}

SHADERPROGRAM_ID render_get_emptyprogvalue()
{
	return NULL;
}

VBO_ID render_get_emptybuffervalue()
{
	return NULL;
}

SHADER_ID render_load_shader(render_manager_t* const rendermngr,const char* shader_name,char* shader_buffer,size_t shader_data_size,wbool is_fragment_shader,void* shader_info)
{
	//generate the shaders for rendering

	SHADER_ID o_shader = (D3DShader*)wf_calloc(1,sizeof(D3DShader));

	HRESULT comp_result;

	if(!is_fragment_shader)
		comp_result= rendermngr->d3d11Device->CreateVertexShader(shader_buffer,shader_data_size,NULL,&o_shader->vertex_shader);
	else
		comp_result= rendermngr->d3d11Device->CreatePixelShader(shader_buffer,shader_data_size,NULL,&o_shader->pixel_shader);

	o_shader->is_fragment_shader = is_fragment_shader;

	if(shader_info != NULL)
	{
		D3Dshaderinfo* inf = (D3Dshaderinfo*)shader_info;


		if(inf->constant_def != NULL)
		{

			o_shader->const_buf_def = (constant_buffer_t*)wf_calloc(1,sizeof(constant_buffer_t));

			o_shader->const_buf_def->buffer_element_def = wf_calloc(1,sizeof(hashmap_t));
		

			hashmap_t* c_hash = (hashmap_t*)o_shader->const_buf_def->buffer_element_def;

			hashmap_initalloc(c_hash,inf->constant_def->n_param_list,sizeof(element_def_t),inf->constant_def->n_param_list);

			size_t c_offset = 0;

			for(uint32_t i_param = 0;i_param < inf->constant_def->n_param_list;i_param++)
			{
				element_def_t* new_ele = (element_def_t*)hashmap_push(c_hash,inf->constant_def->param_list[i_param]->name);

				switch(inf->constant_def->param_list[i_param]->type_param)
				{
					case D3_DSHADERTYPE__MATRIX4X4:
						new_ele->size_element = sizeof(XMMATRIX);
					break;

					case D3_DSHADERTYPE__FLOAT4:
						new_ele->size_element = sizeof(XMFLOAT4);
					break;

					case D3_DSHADERTYPE__FLOAT3:
						new_ele->size_element = sizeof(XMFLOAT3);
					break;

					case D3_DSHADERTYPE__FLOAT2:
						new_ele->size_element = sizeof(XMFLOAT2);
					break;

					case D3_DSHADERTYPE__FLOAT:
						new_ele->size_element = sizeof(float);
					break;
				}

				new_ele->offset_element = c_offset;

				c_offset += new_ele->size_element;
			}

			o_shader->const_buf_def->buffer_content = (char*)wf_calloc(c_offset,sizeof(char));

			D3D11_BUFFER_DESC cbbd;	
			ZeroMemory(&cbbd, sizeof(D3D11_BUFFER_DESC));

			cbbd.Usage = D3D11_USAGE_DEFAULT;
			cbbd.ByteWidth = c_offset;
			cbbd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
			cbbd.CPUAccessFlags = 0;
			cbbd.MiscFlags = 0;

			//check constant buffer size
			if ((cbbd.ByteWidth % 16) != 0)
			{
				logprint("Constant buffer size is not a multiple of 16, please add padding values");
			}
			else
			{
				HRESULT buffer_result;
				buffer_result = rendermngr->d3d11Device->CreateBuffer(&cbbd, NULL, &o_shader->constant_buffer);

				if (FAILED(buffer_result))
				{
					logprint("Error while creating constant buffer, please run in debugger with D3D debug driver activated and check detailed error in debugger output");
				}

				rendermngr->d3d11DevCon->UpdateSubresource(o_shader->constant_buffer, 0, NULL, &o_shader->const_buf_def->buffer_content, 0, 0);
			}
			
		}

		if(inf->layout_def != NULL)
		{
			D3D11_INPUT_ELEMENT_DESC* layout = (D3D11_INPUT_ELEMENT_DESC*)wf_calloc(inf->layout_def->n_param_list,sizeof(D3D11_INPUT_ELEMENT_DESC));

			for(uint32_t i_lay = 0; i_lay < inf->layout_def->n_param_list;i_lay++)
			{
				int sem = inf->layout_def->param_list[i_lay]->semantic;

				if(sem > 0)
					sem -= 1;

				layout[i_lay].SemanticName = semantics[sem];

				layout[i_lay].SemanticIndex = 0;

				switch(inf->layout_def->param_list[i_lay]->type_param)
				{
					case D3_DSHADERTYPE__FLOAT4:
						layout[i_lay].Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
					break;

					case D3_DSHADERTYPE__FLOAT3:
						layout[i_lay].Format = DXGI_FORMAT_R32G32B32_FLOAT;
					break;

					case D3_DSHADERTYPE__FLOAT2:
						layout[i_lay].Format = DXGI_FORMAT_R32G32_FLOAT;
					break;

					case D3_DSHADERTYPE__FLOAT:
						layout[i_lay].Format = DXGI_FORMAT_R32_FLOAT;
					break;
				}

				layout[i_lay].InputSlot = i_lay;
				layout[i_lay].AlignedByteOffset = 0;
				layout[i_lay].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
				layout[i_lay].InstanceDataStepRate = 0;
			}

			HRESULT input_result = rendermngr->d3d11Device->CreateInputLayout(layout,inf->layout_def->n_param_list,shader_buffer,shader_data_size,&o_shader->input_layout);

			if (FAILED(input_result))
			{
				logprint("Error while creating input layout, please run in debugger with D3D debug driver activated and check detailed error in debugger output");
			}

			wf_free(layout);
		}
	}


	if(comp_result != S_OK)
	{
		logprint("Error while loading shader %s",shader_name);
		return NULL;
	}

	return o_shader;
}

void render_reload_shader(render_manager_t* const rendermngr,const char* shader_name,SHADER_ID shader_id,char* shader_buffer)
{
	if(shader_id->is_fragment_shader)
		shader_id->pixel_shader->Release();
	else
		shader_id->vertex_shader->Release();

	size_t size = strlen(shader_buffer) + 1;

	HRESULT comp_result;

	if(!shader_id->is_fragment_shader)
		comp_result= rendermngr->d3d11Device->CreateVertexShader(shader_buffer,size,NULL,&shader_id->vertex_shader);
	else
		comp_result= rendermngr->d3d11Device->CreatePixelShader(shader_buffer,size,NULL,&shader_id->pixel_shader);

	if(comp_result != S_OK)
	{
		logprint("Error while reloading shader %s ",shader_name);
		return;
	}
	else
	{
		logprint("Shader %s reloaded",shader_name);
	}
}

SHADERPROGRAM_ID render_create_program(render_manager_t* const rendermngr,const char* program_id,SHADER_ID vertex_shader,SHADER_ID fragment_shader)
{

	if(hashtable_haskey((hashtable_t*)rendermngr->hashtable_shader_programs,program_id))
	{
		logprint("shader program %s already exist!",program_id);
		return 0;
	}

	SHADERPROGRAM_ID shaderProgram = (SHADERPROGRAM_ID)wf_calloc(1,sizeof(D3DProgram));

	shaderProgram->vertex_shader = vertex_shader;
	shaderProgram->pixel_shader = fragment_shader;

	char* prog_id = (char*)wf_malloc(strlen(program_id)+1);
	w_strcpy(prog_id,strlen(program_id)+1,program_id);

	//save program in hashtable
	int pos = hashtable_pushkey((hashtable_t*)rendermngr->hashtable_shader_programs,prog_id);

	rendermngr->shader_programs[pos] = shaderProgram;

	return shaderProgram;
}

void render_relink_program(render_manager_t* const rendermngr,const char* program_id,SHADERPROGRAM_ID program)
{
	logprint("Not implemented");
}

wbool render_has_program(render_manager_t* const rendermngr,const char* program_id)
{

	return (wbool)hashtable_haskey((hashtable_t*)rendermngr->hashtable_shader_programs,program_id);
}



double render_gettime()
{
	return clock() * 1000 / CLOCKS_PER_SEC;
}

float render_gettime2(wbool msec)
{
	if(msec)
		return (float)clock() * 1000;
	else
		return (float)render_gettime();
}

void render_stop(render_manager_t* const rendermngr)
{
	//freeing shader programs hashtable
	hashtable_t* tmp = (hashtable_t*)rendermngr->hashtable_shader_programs;

	uint32_t ish = 0;
	uint32_t count_sh = 0;

	while(count_sh < tmp->num_elem)
	{
		if(tmp->keys[ish])
		{
			wf_free(tmp->keys[ish]);
			count_sh++;
		}

		ish++;
	}
	
	wf_free(tmp->keys);
	tmp->keys = NULL;
	tmp->num_elem = 0;
	tmp->max_elem = 0;

	wf_free((hashtable_t*)rendermngr->hashtable_shader_programs);

	rendermngr->hashtable_shader_programs = NULL;

	if(rendermngr->tmpvertexBuffer)
	{
		rendermngr->tmpvertexBuffer->Release();
		rendermngr->tmpvertexBuffer = NULL;
	}

	if (rendermngr->tmptexCoordBuffer)
	{
		rendermngr->tmptexCoordBuffer->Release();
		rendermngr->tmpvertexBuffer = NULL;
	}

	rendermngr->d3d11DevCon->OMSetRenderTargets(0, NULL, NULL);
	rendermngr->d3d11DevCon->Flush();

	rendermngr->Transparency->Release();

	rendermngr->renderTargetView->Release();
	rendermngr->SwapChain->Release();
	rendermngr->d3d11Device->Release();
	rendermngr->d3d11DevCon->Release();
	rendermngr->dxgiOutput->Release();
	rendermngr->m_rasterState->Release();
	rendermngr->texSamplerState->Release();
	rendermngr->currentTargetView->Release();

	rendermngr->Transparency = NULL;
	rendermngr->renderTargetView = NULL;
	rendermngr->SwapChain = NULL;
	rendermngr->d3d11Device = NULL;
	rendermngr->d3d11DevCon = NULL;
	rendermngr->dxgiOutput = NULL;
	rendermngr->m_rasterState = NULL;
	rendermngr->texSamplerState = NULL;
	rendermngr->currentTargetView = NULL;

	rendermngr->current_program = NULL;
}

#endif

