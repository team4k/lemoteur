#include <Render.h>

#if defined(RENDERER_OPENGL) || defined(RENDERER_OPENGL_ES)

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

#include <Engine.h>
#include <Base/types.h>
#include <Base/functions.h>
#include <Base/geom.h>
#include <Base/Color.h>

#include <Render/render_gl_viewprojection.h>
#include <Render/render_gl_matrix.h>

wbool  w_glOrthof(GLfloat result[16], const GLfloat left, const GLfloat right, const GLfloat bottom, const GLfloat top, const GLfloat nearVal, const GLfloat farVal)
{
	if ((right - left) == 0.0f || (top - bottom) == 0.0f || (farVal - nearVal) == 0.0f)
	{
		return wfalse;
	}

    result[0] = 2.0f / (right - left);
    result[1] = 0.0f;
    result[2] = 0.0f;
    result[3] = 0.0f;

    result[4] = 0.0f;
    result[5] = 2.0f / (top - bottom);
    result[6] = 0.0f;
    result[7] = 0.0f;

    result[8] = 0.0f;
    result[9] = 0.0f;
    result[10] = -2.0f / (farVal - nearVal);
    result[11] = 0.0f;

    result[12] = -(right + left) / (right - left);
    result[13] = -(top + bottom) / (top - bottom);
    result[14] =  -(farVal + nearVal) / (farVal - nearVal);
    result[15] = 1.0f;

    return wtrue;
}

void w_glPerspective(GLfloat result[16],GLfloat fovy,GLfloat aspect,GLfloat zNear,GLfloat zFar)
{
	// Generates a perspective viewing matrix
	GLfloat cotan = 1.0f / tanf(fovy/2.0f);

	result[0] = cotan/aspect;
	result[1] = 0.0f;
	result[2] = 0.0f;
	result[3] = 0.0f;

	result[4] = 0.0f;
	result[5] = cotan;
	result[6] = 0.0f;
	result[7] = 0.0f;

	result[8] = 0.0f;
	result[9] = 0.0f;
	result[10] = (zFar+zNear) / (zNear-zFar);
	result[11] = -1.0f;

	result[12] = 0.0f;
	result[13] = 0.0f;
	result[14] = 2.0f * zFar * zNear / (zNear-zFar);
	result[15] = 0.0f;
}

// Generates lookat viewing matrix
void w_glLookat(GLfloat result[16],GLfloat eyePosition[3],GLfloat FocusPosition[3],GLfloat upDirection[3])
{
	GLfloat n[3];

	//add to our eye vector our negated focus vector
	n[0] = FocusPosition[0] - eyePosition[0] ;
	n[1] = FocusPosition[1] - eyePosition[1];
	n[2] = FocusPosition[2] - eyePosition[2];

	//normalize
	GLfloat nmag = sqrtf(n[0] * n[0] + n[1] * n[1] + n[2] * n[2]);

	n[0] /= nmag;
	n[1] /= nmag;
	n[2] /= nmag;

	GLfloat u[3];

	//cross product between upDirection and n vector
	u[0] = upDirection[1] * n[2] - upDirection[2] * n[1];
	u[1] = upDirection[2] * n[0] - upDirection[0] * n[2];
	u[2] = upDirection[0] * n[1] - upDirection[1] * n[0];

	//normalize
	GLfloat umag = sqrtf(u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);

	u[0] /= umag;
	u[1] /= umag;
	u[2] /= umag;

	//cross product between n and u
	GLfloat v[3];

	v[0] = n[1] * u[2] - n[2] * u[1];
	v[1] = n[2] * u[0] - n[0] * u[2];
	v[2] = n[0] * u[1] - n[1] * u[0];



	result[0] = u[0];
	result[1] = v[0];
	result[2] = n[0];
	result[3] = 0.0f;
	result[4] = u[1];
	result[5] = v[1];
	result[6] = n[1];
	result[7] = 0.0f;
	result[8] = u[2];
	result[9] = v[2];
	result[10] = n[2];
	result[11] = 0.0f;

	result[12] = ((u[0] * -1.0f) * eyePosition[0]) + ((u[1] * -1.0f) * eyePosition[1]) + ((u[2] * -1.0f) * eyePosition[2]) ; //dot product between negated vector and eye position
	result[13] = ((v[0] * -1.0f) * eyePosition[0]) + ((v[1] * -1.0f) * eyePosition[1]) + ((v[2] * -1.0f) * eyePosition[2]) ; //dot product between negated vector and eye position
	result[14] = ((n[0] * -1.0f) * eyePosition[0]) + ((n[1] * -1.0f) * eyePosition[1]) + ((n[2] * -1.0f) * eyePosition[2]) ; //dot product between negated vector and eye position
	result[15] = 1.0f;
}

#endif