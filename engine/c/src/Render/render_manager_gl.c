﻿#include <Render.h>

#if defined(RENDERER_OPENGL) || defined(RENDERER_OPENGL_ES)

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

#include <Engine.h>
#include <Base/types.h>
#include <Base/functions.h>
#include <Base/geom.h>
#include <Base/Color.h>

#include <Utils/Hash.h>

#include <Render/render_gl_math.h>
#include <Render/render_gl_matrix.h>
#include <Render/render_gl_viewprojection.h>


#include <Debug/Logprint.h>

#ifdef ANDROID_BUILD
     #include <jni.h>
	#include <android/native_window.h> // requires ndk r5 or newer
     #include <android/native_window_jni.h> // requires ndk r5 or newer
     #include <EGL/egl.h>
#endif


#include <Render/render_manager.h>

static void _set_attrib_shader(render_manager_t* const render_mngr,const char* attrib_name,GLint* attrib_cache)
{
	if((*attrib_cache) == -1)
	{
		(*attrib_cache) = glGetAttribLocation(render_mngr->current_program->active_prog_id,attrib_name);
		glEnableVertexAttribArray((*attrib_cache));
	}
}

static void _disable_attrib_shader(render_manager_t*const render_mngr,GLint* attrib_cache)
{
	if((*attrib_cache) != -1)
	{
		glDisableVertexAttribArray((*attrib_cache));
		(*attrib_cache) = -1;
	}
}

static void _set_uniform_shader(render_manager_t* const render_mngr,const char* uni_name,GLint* uni_cache)
{
	if (render_mngr->current_program == NULL) {
		return;
	}

	if((*uni_cache) == -1)
		(*uni_cache) = glGetUniformLocation(render_mngr->current_program->active_prog_id,uni_name);
}

static void _set_texture(render_manager_t* const render_mngr,TEXTURE_ID texture,wbool repeat_texture,wbool is_3d)
{
	if(render_mngr->current_bind_texture == NULL || render_mngr->current_bind_texture != texture)
	{
		glActiveTexture(GL_TEXTURE0);

		if(is_3d)
			glBindTexture(GL_TEXTURE_3D,0);
		else
			glBindTexture(GL_TEXTURE_2D,0);

		if(texture != NULL)
		{
			if(is_3d)
				glBindTexture(GL_TEXTURE_3D,*texture);
			else
				glBindTexture(GL_TEXTURE_2D,*texture);
		}
			
		render_mngr->current_bind_texture = texture;
	}

	if(repeat_texture && texture != NULL)
	{
		if(is_3d)
		{
			glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_REPEAT);
		}
		else
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		}
	}
}

static void _render_update_matrixes(render_manager_t* const render_mngr,int mat_flag)
{
	glMatrix4x4Multiplyf(render_mngr->WVP,render_mngr->ProjMatrix,render_mngr->ViewMatrix);
	glMatrix4x4Multiplyf(render_mngr->WVP,render_mngr->WVP,render_mngr->WorldMatrix);
	
	_set_uniform_shader(render_mngr,"WVP",&render_mngr->uniTrans);

	glUniformMatrix4fv(render_mngr->uniTrans,1,GL_FALSE,render_mngr->WVP);

	if((mat_flag & SET_WORLD_MAT) > 0)
	{
		
		_set_uniform_shader(render_mngr,"W",&render_mngr->uniWorld);
		glUniformMatrix4fv(render_mngr->uniWorld,1,GL_FALSE,render_mngr->WorldMatrix);
	}

	if((mat_flag & SET_VIEW_MAT) > 0)
	{
		_set_uniform_shader(render_mngr,"V",&render_mngr->uniView);
		glUniformMatrix4fv(render_mngr->uniView,1,GL_FALSE,render_mngr->ViewMatrix);
	}

	if((mat_flag & SET_PROJ_MAT) > 0)
	{
		_set_uniform_shader(render_mngr,"P",&render_mngr->uniProj);
		glUniformMatrix4fv(render_mngr->uniProj,1,GL_FALSE,render_mngr->ProjMatrix);
	}
}

int render_init(render_manager_t* const rendermngr,int16_t width, int16_t height,const char* app_path,const char* window_title,void* app_instance,void* app_handle,wbool editor_mode,wbool fullscreen,texture_filter texture_filtering,int16_t refreshrate)
{
	 rendermngr->screen_info.width = width;
	 rendermngr->screen_info.height = height;
	 rendermngr->fullscreen = fullscreen;
	 rendermngr->refreshrate = refreshrate;
	 rendermngr->window_title = window_title;
	 rendermngr->current_bind_texture = NULL;
	 rendermngr->fullscreen_width = width;
	 rendermngr->fullscreen_height = height;
	 rendermngr->fullscreen_offsetx = 0;
	 rendermngr->fullscreen_offsety = 0;
	 rendermngr->zoom_factor = 1.0f;
	 rendermngr->near_value = -1.0f;
	 rendermngr->far_value = 1.0f;
	 rendermngr->current_program = NULL;
	 rendermngr->oglWindow = NULL;

	 rendermngr->texture_filtering = texture_filtering;

	 rendermngr->last_size_texcoord_vbo = 0;
	 rendermngr->last_size_vert_vbo = 0;
	 rendermngr->frame_number = 0;

	 rendermngr->inColor_attrib = rendermngr->inPos_attrib = rendermngr->inTex_attrib = rendermngr->uniColor = rendermngr->uniTrans = rendermngr->inPos3_attrib = rendermngr->inNormal_attrib = rendermngr->uniWorld = rendermngr->uniView = rendermngr->uniProj = -1;

	rendermngr->major_opengl_version = 3;
	rendermngr->minor_opengl_version = 2;

	 rendermngr->hashtable_shader_programs = (hashtable_t*)wf_malloc(sizeof(hashtable_t));

	 hashtable_t* tmp = (hashtable_t*)rendermngr->hashtable_shader_programs;

	 tmp->keys = (char**) wf_malloc(MAX_SHADER_PROGRAMS * sizeof (char*));

	//initialise shaders programs keys array
	for(int32_t sh_indx = 0; sh_indx < MAX_SHADER_PROGRAMS;sh_indx++)
		 tmp->keys[sh_indx] = 0;

	tmp->max_elem = MAX_SHADER_PROGRAMS;
	tmp->num_elem = 0;
	 

 	render_init_window_render(rendermngr, editor_mode, fullscreen, window_title, app_instance, app_handle, refreshrate);
	

	#ifndef ANDROID_BUILD
		//init glew
		logprint("GL RENDER: Init Glew...");

		//needed by virtualbox
		glewExperimental = GL_TRUE;
		GLenum err = glewInit();

		if(err != GLEW_OK)
		{
			logprint("GL RENDER: Error while initializing Glew %s", glewGetErrorString(err));
			return 0;
		}
		else
		{
			logprint("GL RENDER: Glew version %s initialized",glewGetString(GLEW_VERSION));
		}
	
	#endif

    //generation vao
    rendermngr->global_vao = render_gen_vao(rendermngr);
    render_bind_vao(rendermngr,rendermngr->global_vao);
	
	logprint("GL RENDER: Setup Opengl View...");

 
	render_setup_config(rendermngr);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);

    //clear screen
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	if(!editor_mode)
		render_swap_buffers(rendermngr);

    //setup opengl view
	int left = (int)(-rendermngr->scroll_vector.x * rendermngr->zoom_factor);
	int right = (int)((left + rendermngr->screen_info.width) * rendermngr->zoom_factor);

	int bottom = (int)(-rendermngr->scroll_vector.y * rendermngr->zoom_factor);
	int top = (int)((bottom + rendermngr->screen_info.height)  * rendermngr->zoom_factor);

	GLfloat eyePos[3] = { 0.0f, 0.0f, -0.5f };
	GLfloat focusPos[3] = { 0.0f, 0.0f, 0.0f};
	GLfloat upDir[3] = { 0.0f, 1.0f, 0.0f} ;

	w_glLookat(rendermngr->ViewMatrix,eyePos,focusPos,upDir);

	w_glOrthof(rendermngr->ProjMatrix,(GLfloat)left, (GLfloat)right, (GLfloat)bottom, (GLfloat)top, rendermngr->near_value, rendermngr->far_value);

	if(fullscreen)
		glViewport(0,0,rendermngr->fullscreen_width,rendermngr->fullscreen_height);
	else
		glViewport(0,0,rendermngr->screen_info.width,rendermngr->screen_info.height);


	rendermngr->screen_info.position.x = -rendermngr->scroll_vector.x * rendermngr->zoom_factor;
	rendermngr->screen_info.position.y = (-rendermngr->scroll_vector.y + rendermngr->screen_info.height) * rendermngr->zoom_factor;

	render_set_swap_interval(rendermngr);

	rendermngr->current_program = 0;
	
    return 1;
}

void render_set_backfacecull(render_manager_t* const rendermngr,wbool cull)
{
	if (cull)
	{
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
	}
	else {
		glDisable(GL_CULL_FACE);
	}
}

void render_setup_config(render_manager_t* const rendermngr)
{
	//setup opengl
	glEnable(GL_BLEND);

	glEnable(GL_CULL_FACE);
	//glDisable(GL_CULL_FACE);
	glFrontFace(GL_CCW);
	glCullFace(GL_BACK);
	

	//glEnable(GL_DEPTH_TEST);
	
	/*glEnable(GL_BLEND);

	glDisable(GL_DEPTH_TEST);
	glDisable(GL_STENCIL_TEST);
	glCullFace(GL_BACK);*/

	//rendering quality hint
	if (rendermngr->texture_filtering == filter_linear)
	{
		glHint(GL_GENERATE_MIPMAP_HINT, GL_NICEST);
	}

	render_blend_alpha(rendermngr,wtrue);
}

void render_setup_debugconfig(render_manager_t* const rendermngr)
{
	//glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

//	glEnable(GL_DEPTH_TEST);
	//glDepthFunc(GL_ALWAYS);
	//glDepthMask(GL_FALSE);
	//glDisable(GL_STENCIL_TEST);
	//glStencilFunc(GL_ALWAYS, 0, 0);
	//glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
	//glStencilMask(0);
	//glDisable(GL_BLEND);
	//glBlendFuncSeparate(GL_LINES, GL_NONE, GL_LINES, GL_NONE);
	//glBlendEquationSeparate(GL_FUNC_ADD, GL_FUNC_ADD);
	//glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
	//glBlendColor(0.0f, 0.0f, 0.0f, 0.0f);
	//glPolygonOffset(0.0f, 0.0f);
	//glDisable(GL_POLYGON_OFFSET_FILL);
	
	//glFrontFace(GL_CW);
	//glCullFace(GL_BACK);
	//glEnable(GL_SCISSOR_TEST);
	//glDisable(GL_SAMPLE_ALPHA_TO_COVERAGE);
	//glEnable(GL_DITHER);
	//glEnable(GL_MULTISAMPLE);
	//glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);
}

void render_destroyscreen(render_manager_t* const rendermngr)
{
     #ifdef ANDROID_BUILD
          //destroy previous context and surface
     if (rendermngr->oglDisplay != EGL_NO_DISPLAY) 
     {
             eglMakeCurrent(rendermngr->oglDisplay, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
             if (rendermngr->oglContext != EGL_NO_CONTEXT) {
                 eglDestroyContext(rendermngr->oglDisplay, rendermngr->oglContext);
             }
             if (rendermngr->oglSurface != EGL_NO_SURFACE) {
                 eglDestroySurface(rendermngr->oglDisplay, rendermngr->oglSurface);
             }
             eglTerminate(rendermngr->oglDisplay);
       }
          rendermngr->oglDisplay = EGL_NO_DISPLAY;
          rendermngr->oglContext = EGL_NO_CONTEXT;
          rendermngr->oglSurface = EGL_NO_SURFACE;
     #endif
}

void render_resizescreen(render_manager_t* const rendermngr,int16_t new_width,int16_t new_height)
{
	rendermngr->screen_info.width = new_width;
	rendermngr->screen_info.height = new_height;

	int left = (int)(-rendermngr->scroll_vector.x * rendermngr->zoom_factor);
	int right = (int)((left + rendermngr->screen_info.width) * rendermngr->zoom_factor);

	int bottom = (int)(-rendermngr->scroll_vector.y * rendermngr->zoom_factor);
	int top = (int)((bottom + rendermngr->screen_info.height)  * rendermngr->zoom_factor);

	//setup opengl view
	GLfloat eyePos[3] = { 0.0f, 0.0f, -0.5f };
	GLfloat focusPos[3] = { 0.0f, 0.0f, 0.0f};
	GLfloat upDir[3] = { 0.0f, 1.0f, 0.0f} ;

	w_glLookat(rendermngr->ViewMatrix,eyePos,focusPos,upDir);

	w_glOrthof(rendermngr->ProjMatrix,(GLfloat)left, (GLfloat)right, (GLfloat)bottom, (GLfloat)top, rendermngr->near_value, rendermngr->far_value);

	rendermngr->screen_info.position.x = -rendermngr->scroll_vector.x * rendermngr->zoom_factor;
	rendermngr->screen_info.position.y = (-rendermngr->scroll_vector.y + rendermngr->screen_info.height) * rendermngr->zoom_factor;

	if (rendermngr->fullscreen) {
		glViewport(0, 0, rendermngr->fullscreen_width, rendermngr->fullscreen_height);
	}
	else {
		glViewport(0, 0, rendermngr->screen_info.width, rendermngr->screen_info.height);
	}
	
}

void render_recreatescreen(render_manager_t* const rendermngr,wbool fullscreen,void* app_handle,int16_t new_width,int16_t new_height,int16_t refreshrate)
{
	wbool setup_opengl = wfalse;

     //reset all state flags
     rendermngr->enabled_vertex_array = wfalse;
     rendermngr->enabled_texcoord_array = wfalse;
     rendermngr->enabled_blend = wfalse;
	 rendermngr->current_color.r = rendermngr->current_color.g = rendermngr->current_color.b = rendermngr->current_color.a = 0.0f;

	_disable_attrib_shader(rendermngr,&rendermngr->inColor_attrib);
	_disable_attrib_shader(rendermngr,&rendermngr->inPos_attrib);
	_disable_attrib_shader(rendermngr,&rendermngr->inTex_attrib);
	_disable_attrib_shader(rendermngr,&rendermngr->inPos3_attrib);
	_disable_attrib_shader(rendermngr,&rendermngr->inNormal_attrib);

	rendermngr->screen_info.width = new_width;
	rendermngr->screen_info.height = new_height;

	rendermngr->inColor_attrib = rendermngr->inPos_attrib = rendermngr->inTex_attrib = rendermngr->uniColor = rendermngr->uniTrans = rendermngr->inPos3_attrib = rendermngr->inNormal_attrib = rendermngr->uniWorld = rendermngr->uniView = rendermngr->uniProj = -1;
	
	rendermngr->current_program = 0;
	glUseProgram(0);


	if(rendermngr->last_size_texcoord_vbo != 0)
		render_delete_buffer(rendermngr->tmp_texcoord_vbo);

	if(rendermngr->last_size_vert_vbo != 0)
		render_delete_buffer(rendermngr->tmp_vertices_vbo);

	rendermngr->last_size_texcoord_vbo = 0;
	rendermngr->last_size_vert_vbo = 0;

	render_unbind_vao(rendermngr);
	render_delete_vao(rendermngr, rendermngr->global_vao);


	setup_opengl = render_recreate_window(rendermngr, fullscreen, app_handle,refreshrate);

     if(setup_opengl)
     {
		 rendermngr->global_vao = render_gen_vao(rendermngr);
		 render_bind_vao(rendermngr, rendermngr->global_vao);

          //setup opengl

		 render_setup_config(rendermngr);


		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

		glClearDepth(1.0f);
		glClearStencil(1);


		//clear screen
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

          
		render_swap_buffers(rendermngr);
          

     #ifndef ANDROID_BUILD
		if(fullscreen)
			glViewport(0,0,rendermngr->fullscreen_width,rendermngr->fullscreen_height);
		else
			glViewport(0,0,rendermngr->screen_info.width,rendermngr->screen_info.height);
     #endif

		
		int left = (int)(-rendermngr->scroll_vector.x * rendermngr->zoom_factor);
		int right = (int)((left + rendermngr->screen_info.width) * rendermngr->zoom_factor);

		int bottom = (int)(-rendermngr->scroll_vector.y * rendermngr->zoom_factor);
		int top = (int)((bottom + rendermngr->screen_info.height)  * rendermngr->zoom_factor);

		//setup opengl view
		GLfloat eyePos[3] = { 0.0f, 0.0f, -0.5f };
		GLfloat focusPos[3] = { 0.0f, 0.0f, 0.0f};
		GLfloat upDir[3] = { 0.0f, 1.0f, 0.0f} ;

		w_glLookat(rendermngr->ViewMatrix,eyePos,focusPos,upDir);

		w_glOrthof(rendermngr->ProjMatrix,(GLfloat)left, (GLfloat)right, (GLfloat)bottom, (GLfloat)top, rendermngr->near_value, rendermngr->far_value);

		render_set_swap_interval(rendermngr);

		rendermngr->screen_info.position.x = -rendermngr->scroll_vector.x * rendermngr->zoom_factor;
		rendermngr->screen_info.position.y = (-rendermngr->scroll_vector.y + rendermngr->screen_info.height) * rendermngr->zoom_factor;
     }
}


static SHADER_ID _render_compile_shader(render_manager_t* const rendermngr,const char* shader_name,char* shader_buffer,size_t shader_data_size,wbool is_fragment_shader,SHADER_ID shader_src)
{

	//set glsl shader version 
	char* version_pos = strstr(shader_buffer,"__VERSION______");

	if(version_pos != NULL)//replace __VERSION___ macro with current acceptable shader version
	{
		char version_str[64];

		#ifdef RENDERER_OPENGL_ES
			w_sprintf(version_str, 64, "#version 300 es");
		#else
			w_sprintf(version_str, 64, "#version 150   ");
		#endif

		memcpy(version_pos,version_str,strlen(version_str));
	}

	//dynamic shader creation
	size_t num_shader_sources = 1;
	char** shaders_source = (char**)wf_calloc(num_shader_sources,sizeof(char*));
	
	shaders_source[0] = shader_buffer;

	//set the maximum number of light
	char* max_light_pos = strstr(shader_buffer,"__MAX_LIGHT__");
	char* updated_buffer = NULL;
	size_t updated_buffer_size = 0;

	if(max_light_pos != NULL)
	{
		int32_t size_move =  (intptr_t)(shader_buffer+shader_data_size) - (intptr_t)(max_light_pos+strlen("__MAX_LIGHT__"));

		updated_buffer_size = shader_data_size * 10; //oversize our buffer to generate shaders
		//create a new buffer for our macro
		updated_buffer = (char*)wf_malloc(updated_buffer_size);
		memset(updated_buffer,0,updated_buffer_size);

		w_strcpy(updated_buffer,shader_data_size,shader_buffer);

		max_light_pos = strstr(updated_buffer,"__MAX_LIGHT__");
		memset(max_light_pos,32,strlen("__MAX_LIGHT__"));

		char light_str[64];
		w_sprintf(light_str,64,"#define MAX_LIGHT %d",MAX_LIGHT_PER_ELEMENT);

		memmove(max_light_pos + strlen(light_str),max_light_pos+strlen("__MAX_LIGHT__"),size_move);

		memcpy(max_light_pos,light_str,strlen(light_str));

		shaders_source[0] = updated_buffer;
	}
	else
	{
		updated_buffer = shader_buffer;
		updated_buffer_size = shader_data_size;
	}

	//generate a shader per light
	char* num_light_pos = strstr(updated_buffer,"__NUM_LIGHT__");

	char* f_light_pos = strstr(updated_buffer,"__FOREACH_NUM_LIGHT__") + strlen("__FOREACH_NUM_LIGHT__");
	char* endf_light_pos = strstr(updated_buffer,"__ENDFOREACH_NUM_LIGHT__");

	if(num_light_pos != NULL || (f_light_pos && endf_light_pos))
	{
		num_shader_sources = MAX_LIGHT_PER_ELEMENT+1;

		shaders_source = (char**)wf_realloc(shaders_source,sizeof(char*) * num_shader_sources);

		char* foreach_duplicate = NULL;

		//check if we have a foreach based on num lights
		
		size_t foreach_duplicate_size = 0;

		int32_t size_formove = (intptr_t)(updated_buffer+strlen(updated_buffer)) - (intptr_t)(endf_light_pos+strlen("__ENDFOREACH_NUM_LIGHT__"));

		if(f_light_pos != NULL && endf_light_pos != NULL)
		{
			foreach_duplicate_size = ((intptr_t)endf_light_pos - (intptr_t)f_light_pos);
			foreach_duplicate = (char*)wf_malloc(foreach_duplicate_size+1);
			memset(foreach_duplicate,0,foreach_duplicate_size+1);
			memcpy(foreach_duplicate,f_light_pos,foreach_duplicate_size);
		}

		int32_t size_move = 0;
		
		if(num_light_pos != NULL)
			size_move = (intptr_t)(updated_buffer+strlen(updated_buffer)) - (intptr_t)(num_light_pos+strlen("__NUM_LIGHT__"));

		for(int32_t ilight = 0;ilight < MAX_LIGHT_PER_ELEMENT+1;ilight++)
		{
			//copy intial buffer
			shaders_source[ilight] = (char*)wf_malloc(updated_buffer_size);
			memcpy(shaders_source[ilight],updated_buffer,updated_buffer_size);

			if(num_light_pos != NULL)
			{
				//replace values
				num_light_pos = strstr(shaders_source[ilight],"__NUM_LIGHT__");
				memset(num_light_pos,32,strlen("__NUM_LIGHT__"));
				char light_str[64];
				w_sprintf(light_str,64,"#define NUM_LIGHT %d",ilight);

				//push content
				memmove(num_light_pos+strlen(light_str),num_light_pos+strlen("__NUM_LIGHT__"),size_move);

				memcpy(num_light_pos,light_str,strlen(light_str));
			}


			char* foreach_light_pos = strstr(shaders_source[ilight],"__FOREACH_NUM_LIGHT__");
			char* endforeach_light_pos = strstr(shaders_source[ilight],"__ENDFOREACH_NUM_LIGHT__");
			
			if(foreach_light_pos != NULL && endforeach_light_pos != NULL)
			{
				memset(foreach_light_pos,32,strlen("__FOREACH_NUM_LIGHT__"));
				memset(endforeach_light_pos,32,strlen("__ENDFOREACH_NUM_LIGHT__"));
				
				
				if(ilight > 1)
				{
					char* destination = foreach_light_pos + (foreach_duplicate_size * (ilight));
					//push content
					if(destination > endforeach_light_pos + strlen("__ENDFOREACH_NUM_LIGHT__"))
						memmove(destination,endforeach_light_pos + strlen("__ENDFOREACH_NUM_LIGHT__"),size_formove);

					//duplicate ilight time the content in foreach
					for(int32_t i_for = 0;i_for < ilight;i_for++)
					{
						memcpy(foreach_light_pos,foreach_duplicate,foreach_duplicate_size);

						//replace __I_LIGHT__ if it exist by a valid value
						char* i_light_pos = strstr(shaders_source[ilight],"__I_LIGHT__");

						while(i_light_pos != NULL)
						{
							memset(i_light_pos,32,strlen("__I_LIGHT__"));
							char istr[11];
							w_sprintf(istr,11,"%d",i_for);
							memcpy(i_light_pos,istr,strlen(istr));
							i_light_pos = strstr(shaders_source[ilight],"__I_LIGHT__");
						}
						
						foreach_light_pos += foreach_duplicate_size;
					}
				}
				else
				{
					if(ilight == 0)
					{
						//remove content between foreach tags
						memset(foreach_light_pos+strlen("__FOREACH_NUM_LIGHT__"),32,foreach_duplicate_size);
					}
					else
					{
						//replace __I_LIGHT__ if it exist by a valid value
						char* i_light_pos = strstr(shaders_source[ilight],"__I_LIGHT__");

						while(i_light_pos != NULL)
						{
							memset(i_light_pos,32,strlen("__I_LIGHT__"));
							char istr[11];
							w_sprintf(istr,11,"%d",0);
							memcpy(i_light_pos,istr,strlen(istr));
							i_light_pos = strstr(shaders_source[ilight],"__I_LIGHT__");
						}
					}

				}
			}

		}

		if(foreach_duplicate != NULL)
			wf_free(foreach_duplicate);
	}

	GLShader* shader = NULL;
	wbool create_sub = wfalse;

	if(shader_src == NULL)
	{
		shader = (GLShader*)wf_calloc(1,sizeof(GLShader));
		shader->num_sub_shader = num_shader_sources-1;
		shader->is_fragment_shader = (char)is_fragment_shader;

		if(shader->num_sub_shader > 0)
			shader->sub_shader_ids = (GLuint*)wf_calloc(shader->num_sub_shader,sizeof(GLuint));
	}
	else
	{
		shader = shader_src;

		if(shader->num_sub_shader > 0 && (num_shader_sources-1) == 0)
		{
			//free old shader
			for(uint32_t i = 0;i <shader->num_sub_shader;i++)
			{
				glDeleteShader(shader->sub_shader_ids[i]);
			}

			wf_free(shader->sub_shader_ids);

			shader->sub_shader_ids = NULL;
			shader->num_sub_shader = 0;

		}
		else if(shader->num_sub_shader == 0 && (num_shader_sources-1) > 0)
		{
			create_sub = wtrue;
			shader->num_sub_shader = num_shader_sources-1;

			if(shader->num_sub_shader > 0)
				shader->sub_shader_ids = (GLuint*)wf_calloc(shader->num_sub_shader,sizeof(GLuint));
		}
	}



	wbool has_error = wfalse;
	
	for(uint16_t i_shadersource = 0;i_shadersource < num_shader_sources;i_shadersource++)
	{
		const char* buffer_const = (const char*)shaders_source[i_shadersource];

		GLuint shader_id = 0;
		
		if((shader_src == NULL) || (i_shadersource > 0 && create_sub))
			shader_id =  glCreateShader((is_fragment_shader) ? GL_FRAGMENT_SHADER : GL_VERTEX_SHADER);
		else if(i_shadersource == 0)
			shader_id = shader_src->shader_id;
		else
			shader_id = shader_src->sub_shader_ids[i_shadersource-1];


		glShaderSource(shader_id, 1, &buffer_const, NULL);

		glCompileShader(shader_id);

		GLint status;
		glGetShaderiv(shader_id,GL_COMPILE_STATUS,&status);

		if(status != GL_TRUE)
		{
			char log[512];

			glGetShaderInfoLog(shader_id,512,NULL,log);

			logprint("Error while compiling shader %s : log : %s",shader_name,log);
			has_error = wtrue;
		}

		if(i_shadersource == 0)
			shader->shader_id = shader_id;
		else
		{
			shader->sub_shader_ids[i_shadersource-1] = shader_id;
		}

		if(shaders_source[i_shadersource] != shader_buffer)
			wf_free(shaders_source[i_shadersource]);
	}

	wf_free(shaders_source);


	if(has_error)
	{
		return NULL;
	}


	return shader;
}


SHADER_ID render_load_shader(render_manager_t* const rendermngr,const char* shader_name,char* shader_buffer,size_t shader_data_size,wbool is_fragment_shader,void* shader_info)
{
	return _render_compile_shader(rendermngr,shader_name,shader_buffer,shader_data_size,is_fragment_shader,NULL);
}

void render_reload_shader(render_manager_t* const rendermngr,const char* shader_name,SHADER_ID shader_id,char* shader_buffer)
{

	SHADER_ID result_shader = _render_compile_shader(rendermngr,shader_name,shader_buffer,strlen(shader_buffer)+1,(wbool)shader_id->is_fragment_shader,shader_id);


	if(result_shader == NULL)
		return;

	//relink any program that this shader is attached to
	hashtable_t* tmp = (hashtable_t*)rendermngr->hashtable_shader_programs;

	uint32_t ish = 0;
	uint32_t count_sh = 0;

	while(count_sh < tmp->num_elem)
	{
		if(tmp->keys[ish])
		{
			GLuint shaders[2];
			GLsizei count;

			glGetAttachedShaders(rendermngr->shader_programs[ish]->prog_id,2,&count,shaders);

			for(int ishad = 0; ishad < count;ishad++)
			{
				if(shaders[ishad] == shader_id->shader_id)
				{
					render_relink_program(rendermngr,tmp->keys[ish],rendermngr->shader_programs[ish]);//relink program associated with our shader
				}
			}

			count_sh++;
		}

		ish++;
	}

}

static SHADERPROGRAM_ID _render_link_program(SHADERPROGRAM_ID const src_prog,const char* program_id,SHADER_ID vertex_shader,SHADER_ID fragment_shader)
{
	SHADERPROGRAM_ID c_prog = NULL;

	if(src_prog != NULL)
		c_prog = src_prog;
	else
	{
		c_prog = (GLProgram*)wf_calloc(1,sizeof(GLProgram));
		c_prog->vertex_shader = vertex_shader;
		c_prog->pixel_shader = fragment_shader;
	}

	int32_t tot_program = (vertex_shader->num_sub_shader + 1) * (fragment_shader->num_sub_shader + 1);

	int32_t diff_prog = 0;
	int32_t i_diff = 0;

	if(tot_program > 1)
	{
		if(src_prog == NULL)
		{
			c_prog->num_sub_program = tot_program - 1;
			c_prog->sub_prog_ids = (GLuint*)wf_calloc(c_prog->num_sub_program,sizeof(GLuint));
		}
		else
		{
			diff_prog = (tot_program - 1) - c_prog->num_sub_program;

			i_diff = c_prog->num_sub_program;

			//delete old programs
			while(diff_prog < 0)
			{
				glDeleteProgram(c_prog->sub_prog_ids[c_prog->num_sub_program + diff_prog]);

				diff_prog++;
			}

			c_prog->num_sub_program = tot_program - 1;

			c_prog->sub_prog_ids = (GLuint*)wf_realloc(c_prog->sub_prog_ids,sizeof(GLuint) * c_prog->num_sub_program);
		}
	}

	wbool error = wfalse;

	uint32_t isubvert = 0;
	uint32_t isubfrag = 0;

	for(int32_t iter = 0;iter < tot_program;iter++)
	{
		GLuint prog_id;
		wbool created_prog = wfalse;
		
		if((src_prog == NULL) || iter >= i_diff)
		{
			prog_id = glCreateProgram();
			created_prog = wtrue;
		}
		else if(iter == 0)
			prog_id = c_prog->prog_id;
		else
			prog_id = c_prog->sub_prog_ids[iter-1];

		GLuint v_shader_id = 0;
		GLuint f_shader_id = 0;

		if(iter == 0)
		{
			v_shader_id =  vertex_shader->shader_id;
			f_shader_id = fragment_shader->shader_id;
		}
		else
		{
			if(vertex_shader->num_sub_shader > 0 && isubvert < vertex_shader->num_sub_shader)
				v_shader_id = vertex_shader->sub_shader_ids[isubvert++];
			else
				v_shader_id =  vertex_shader->shader_id;


			if(fragment_shader->num_sub_shader == 0)
				f_shader_id =  fragment_shader->shader_id;
			else if(isubvert >= vertex_shader->num_sub_shader)
			{
				f_shader_id = fragment_shader->sub_shader_ids[isubfrag++];
			}

			if(isubvert >= vertex_shader->num_sub_shader)
				isubvert = 0;

		}

		if(created_prog)
		{
			glAttachShader(prog_id, v_shader_id);
			glAttachShader(prog_id, f_shader_id);
		}

		glLinkProgram(prog_id);

		GLint status;
		glGetProgramiv(prog_id,GL_LINK_STATUS,&status);

		if(status != GL_TRUE)
		{
			char log[512];
			glGetProgramInfoLog(prog_id,512,NULL,log);

			logprint("Error while linking program %s : log : %s",program_id,log);
			error = wtrue;
		}

		if(iter == 0)
			c_prog->prog_id = prog_id;
		else
			c_prog->sub_prog_ids[iter - 1] = prog_id;
	}

	if(error)
	{
		if(src_prog == NULL)
		{
			wf_free(c_prog);

			if(c_prog->num_sub_program > 0)
				wf_free(c_prog->sub_prog_ids);
		}

		return NULL;
	}
		

	return c_prog;
}

SHADERPROGRAM_ID render_create_program(render_manager_t* const rendermngr,const char* program_id,SHADER_ID vertex_shader,SHADER_ID fragment_shader)
{
	if(hashtable_haskey((hashtable_t*)rendermngr->hashtable_shader_programs,program_id))
	{
		logprint("shader program %s already exist!",program_id);
		return NULL;
	}

	SHADERPROGRAM_ID shaderProgram = _render_link_program(NULL,program_id,vertex_shader,fragment_shader);

	if(shaderProgram != NULL)
	{
		size_t prog_len = strlen(program_id)+1;
		char* prog_id = (char*)wf_malloc(prog_len);
		w_strcpy(prog_id,prog_len,program_id);

		//save program in hashtable
		int pos = hashtable_pushkey((hashtable_t*)rendermngr->hashtable_shader_programs,prog_id);

		rendermngr->shader_programs[pos] = shaderProgram;

		return shaderProgram;
	}

	return NULL;
}

void render_relink_program(render_manager_t* const rendermngr,const char* program_id,SHADERPROGRAM_ID program)
{
	if(rendermngr->current_program == program)
	{
		_disable_attrib_shader(rendermngr,&rendermngr->inColor_attrib);
		_disable_attrib_shader(rendermngr,&rendermngr->inPos_attrib);
		_disable_attrib_shader(rendermngr,&rendermngr->inTex_attrib);
		rendermngr->uniColor = rendermngr->uniTrans = rendermngr->uniProj = rendermngr->uniView = rendermngr->uniWorld = -1;

		rendermngr->current_program = render_get_emptyprogvalue();
	}

	if(_render_link_program(program,program_id,program->vertex_shader,program->pixel_shader) != NULL)
		logprint("Program %s relinked",program_id);

}

void render_use_program(render_manager_t* const rendermngr,SHADERPROGRAM_ID program)
{

	if(rendermngr->current_program != program || rendermngr->current_program->active_prog_id != program->prog_id)
	{
		_disable_attrib_shader(rendermngr,&rendermngr->inColor_attrib);
		_disable_attrib_shader(rendermngr,&rendermngr->inPos_attrib);
		_disable_attrib_shader(rendermngr,&rendermngr->inTex_attrib);
		_disable_attrib_shader(rendermngr,&rendermngr->inPos3_attrib);
		_disable_attrib_shader(rendermngr,&rendermngr->inNormal_attrib);
		rendermngr->uniColor = rendermngr->uniTrans = rendermngr->uniProj = rendermngr->uniView = rendermngr->uniWorld = -1;

		rendermngr->current_program = program;
		glUseProgram(program->prog_id);
		program->active_prog_id = program->prog_id;
		render_reset_color(rendermngr);
		GLint pass_loc = glGetUniformLocation(program->prog_id, "pass");
		program->has_pass = (char)(pass_loc != -1);

	}
}

void render_setuniform_int(render_manager_t* const rendermngr,const char* uniform_name,int value)
{
	GLint uni_loc =  glGetUniformLocation(rendermngr->current_program->active_prog_id,uniform_name);
	glUniform1i(uni_loc,value);
}

wbool render_has_passparam(render_manager_t* const rendermngr)
{
	if (rendermngr->current_program != NULL)
	{
		return (wbool)rendermngr->current_program->has_pass;
	}

	return wfalse;
}

void render_setuniform_float(render_manager_t* const rendermngr,const char* uniform_name,float value)
{
	GLint uni_loc =  glGetUniformLocation(rendermngr->current_program->active_prog_id,uniform_name);
	glUniform1f(uni_loc,value);
}

void render_setuniform_float2(render_manager_t* const rendermngr,const char* uniform_name,float vector[2])
{
	GLint uni_loc =  glGetUniformLocation(rendermngr->current_program->active_prog_id,uniform_name);
	glUniform2f(uni_loc,vector[0],vector[1]);
}

void render_setuniform_float3(render_manager_t* const rendermngr,const char* uniform_name,float vector[3])
{
	GLint uni_loc =  glGetUniformLocation(rendermngr->current_program->active_prog_id,uniform_name);
	glUniform3f(uni_loc,vector[0],vector[1],vector[2]);
}

void render_setuniform_float4(render_manager_t* const rendermngr,const char* uniform_name,float vector[4])
{
	GLint uni_loc =  glGetUniformLocation(rendermngr->current_program->active_prog_id,uniform_name);
	glUniform4f(uni_loc,vector[0],vector[1],vector[2],vector[3]);
}

void render_setuniform_arrayfloat(render_manager_t* const rendermngr, const char* uniform_name, float* data, int32_t count)
{
	GLint uni_loc = glGetUniformLocation(rendermngr->current_program->active_prog_id, uniform_name);
	glUniform1fv(uni_loc, count, data);
}

void render_setuniform_arrayfloat3(render_manager_t* const rendermngr,const char* uniform_name,float* data,int32_t count)
{
	GLint uni_loc =  glGetUniformLocation(rendermngr->current_program->active_prog_id,uniform_name);
	glUniform3fv(uni_loc,count,data);
}

void render_setuniform_arrayfloat4(render_manager_t* const rendermngr,const char* uniform_name,float* data,int32_t count)
{
	GLint uni_loc =  glGetUniformLocation(rendermngr->current_program->active_prog_id,uniform_name);
	glUniform4fv(uni_loc,count,data);
}

void render_setuniform_matrix4(render_manager_t* const rendermngr, const char* uniform_name,float matrix[16])
{
	GLint uni_loc = glGetUniformLocation(rendermngr->current_program->active_prog_id, uniform_name);
	glUniformMatrix4fv(uni_loc, 1, GL_FALSE, matrix);
}


void render_bindattribute_floatVBO(render_manager_t* const render_mngr,const char* attribute_name,VBO_ID vbo,size_t size_element)
{
	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	GLint attrib_pos = glGetAttribLocation(render_mngr->current_program->active_prog_id,attribute_name);
	glEnableVertexAttribArray(attrib_pos);

	glVertexAttribPointer(attrib_pos, size_element, GL_FLOAT, GL_FALSE, 0, 0);
}

void render_bindtexture_tosampler(render_manager_t* const rendermngr,TEXTURE_ID texture,const char* sampler_id,int32_t texture_unit_id,wbool is_3d)
{
	GLint samplerLoc = glGetUniformLocation(rendermngr->current_program->active_prog_id,sampler_id);

	glUniform1i(samplerLoc,texture_unit_id);

	glActiveTexture(GL_TEXTURE0 + texture_unit_id);

	if(is_3d)
		glBindTexture(GL_TEXTURE_3D,*texture);
	else
		glBindTexture(GL_TEXTURE_2D,*texture);
}

void render_unbindtexture_tosampler(render_manager_t* const rendermngr,TEXTURE_ID texture,const char* sampler_id,int32_t texture_unit_id,wbool is_3d)
{
	glActiveTexture(GL_TEXTURE0 + texture_unit_id);

	if(is_3d)
		glBindTexture(GL_TEXTURE_3D,0);
	else
		glBindTexture(GL_TEXTURE_2D,0);
}

int32_t render_get_numsubprogram(render_manager_t* const rendermngr,SHADERPROGRAM_ID prog_o)
{
	return prog_o->num_sub_program;
}

void render_use_subprogram(render_manager_t* const rendermngr,SHADERPROGRAM_ID prog_o,int32_t i_sub)
{

	if(	rendermngr->current_program != prog_o || prog_o->active_prog_id != prog_o->sub_prog_ids[i_sub])
	{
		_disable_attrib_shader(rendermngr,&rendermngr->inColor_attrib);
		_disable_attrib_shader(rendermngr,&rendermngr->inPos_attrib);
		_disable_attrib_shader(rendermngr,&rendermngr->inTex_attrib);
		_disable_attrib_shader(rendermngr,&rendermngr->inPos3_attrib);
		_disable_attrib_shader(rendermngr,&rendermngr->inNormal_attrib);
		rendermngr->uniColor = rendermngr->uniTrans = -1;
		rendermngr->current_program = prog_o;
		GLint pass_loc = glGetUniformLocation(prog_o->prog_id, "pass");
		prog_o->has_pass = (char)(pass_loc != -1);
		glUseProgram(prog_o->sub_prog_ids[i_sub]);
		prog_o->active_prog_id = prog_o->sub_prog_ids[i_sub];
		render_reset_color(rendermngr);
	}
}

SHADERPROGRAM_ID render_get_program(render_manager_t* const rendermngr,const char* program_id)
{

	if(!hashtable_haskey((hashtable_t*)rendermngr->hashtable_shader_programs,program_id))
	{
		logprint("shader program %s doesn't exist!",program_id);
		return 0;
	}

	int pos = hashtable_index((hashtable_t*)rendermngr->hashtable_shader_programs,program_id);

	return rendermngr->shader_programs[pos];
}

SHADERPROGRAM_ID render_get_emptyprogvalue()
{
	return NULL;
}

VBO_ID render_get_emptybuffervalue()
{
	return UINT_MAX;
}

wbool render_has_program(render_manager_t* const rendermngr,const char* program_id)
{
	return (wbool)hashtable_haskey((hashtable_t*)rendermngr->hashtable_shader_programs,program_id);
}

wbool render_delete_program(render_manager_t* const rendermngr,const char* program_id)
{

	if(!hashtable_haskey((hashtable_t*)rendermngr->hashtable_shader_programs,program_id))
	{
		logprint("shader program %s doesn't exist!",program_id);
		return wfalse;
	}

	int pos = hashtable_index((hashtable_t*)rendermngr->hashtable_shader_programs,program_id);


	glDeleteProgram(rendermngr->shader_programs[pos]->prog_id);

	for(uint32_t isub = 0;isub < rendermngr->shader_programs[pos]->num_sub_program;isub++)
	{
		glDeleteProgram(rendermngr->shader_programs[pos]->sub_prog_ids[isub]);
	}

	if(rendermngr->shader_programs[pos]->num_sub_program > 0)
		wf_free(rendermngr->shader_programs[pos]->sub_prog_ids);

	wf_free(rendermngr->shader_programs[pos]);


	char* key_del = ((hashtable_t*)rendermngr->hashtable_shader_programs)->keys[pos];

	//remove from hashtable
	hashtable_removekey((hashtable_t*)rendermngr->hashtable_shader_programs,program_id);

	wf_free(key_del);

	rendermngr->shader_programs[pos] = 0;

	return wtrue;
}

void render_clear_screen(render_manager_t* const rendermngr)
{
	glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );
	glClear( GL_COLOR_BUFFER_BIT);
}

void render_clear_screenwithcolor(render_manager_t* const rendermngr,ColorA_t back_color)
{
	glClearColor( back_color.r,back_color.g,back_color.b,back_color.a);
    glClear( GL_COLOR_BUFFER_BIT);
}

void render_setzoomproj(render_manager_t* const render_mngr,float zoom)
{
	GLfloat left = truncf(-render_mngr->scroll_vector.x * zoom);
	GLfloat right = truncf(left + (render_mngr->screen_info.width * zoom)) ;

	GLfloat bottom = truncf(-render_mngr->scroll_vector.y * zoom);
	GLfloat top = truncf(bottom + (render_mngr->screen_info.height * zoom));

	w_glOrthof(render_mngr->ProjMatrix,left, right, bottom, top, render_mngr->near_value, render_mngr->far_value);
}


void render_update_screen(render_manager_t* const render_mngr)
{
	//scrolling code

	GLfloat left = truncf(-render_mngr->scroll_vector.x * render_mngr->zoom_factor);
	GLfloat right = truncf(left + (render_mngr->screen_info.width * render_mngr->zoom_factor)) ;

	GLfloat bottom = truncf(-render_mngr->scroll_vector.y * render_mngr->zoom_factor);
	GLfloat top = truncf(bottom + (render_mngr->screen_info.height * render_mngr->zoom_factor));

	w_glOrthof(render_mngr->ProjMatrix,left, right, bottom, top, render_mngr->near_value, render_mngr->far_value);

	//update screen position
	render_mngr->screen_info.position.x = (wfloat)(-render_mngr->scroll_vector.x * render_mngr->zoom_factor);
	render_mngr->screen_info.position.y = (wfloat)((-render_mngr->scroll_vector.y * render_mngr->zoom_factor) + (render_mngr->screen_info.height * render_mngr->zoom_factor)) ;

	render_reset_color(render_mngr);
}

void render_blend_alpha(render_manager_t* const render_mngr,wbool force)
{
	if(!render_mngr->enabled_blend || force)
	{
		glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
		glBlendEquation(GL_FUNC_ADD);
		render_mngr->enabled_blend = wtrue;
		render_mngr->c_blend_eq = blend_add;
		render_mngr->c_blend_op = blend_alpha;
	}
}

void render_setblend(render_manager_t* const render_mngr,blend_eq eq,blend_op op)
{
	if(eq != render_mngr->c_blend_eq)
	{
		switch(eq)
		{
			case blend_add:
				glBlendEquation(GL_FUNC_ADD);
				break;
			#ifndef RENDERER_OPENGL_ES
				case blend_max:
					glBlendEquation(GL_MAX);
					break;
				case blend_min:
					glBlendEquation(GL_MIN);
					break;
			#else
				case blend_max:
				case blend_min:
					logprint(" blend_max / blend_min are not supported under android!");
				break;
			#endif
		}

		render_mngr->c_blend_eq = eq;
	}

	if(op != render_mngr->c_blend_op)
	{
		switch(op)
		{
			case blend_replace:
				glBlendFunc(GL_ONE,GL_ZERO);
			break;
			case blend_alpha:
				glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
			break;
			case blend_additive:
				glBlendFunc(GL_SRC_ALPHA,GL_ONE);
				break;
		}

		render_mngr->c_blend_op = op;
	}

	render_mngr->enabled_blend = wtrue;
}


void render_push_matrix(render_manager_t* const render_mngr)
{
	//reset world matrix
	glMatrix4x4Identityf(render_mngr->WorldMatrix);

	glMatrix4x4Multiplyf(render_mngr->WVP,render_mngr->ProjMatrix,render_mngr->ViewMatrix);
	glMatrix4x4Multiplyf(render_mngr->WVP,render_mngr->WVP,render_mngr->WorldMatrix);

	if(render_mngr->current_program != 0)
	{
		_set_uniform_shader(render_mngr,"WVP",&render_mngr->uniTrans);
		glUniformMatrix4fv(render_mngr->uniTrans,1,GL_FALSE,render_mngr->WVP);
	}
}

void render_pop_matrix(render_manager_t* const render_mngr)
{

}


void render_draw_compound_textured_array(render_manager_t* const render_mngr,TEXTURE_ID texture,float* vertices,float* texture_coord,wbool repeat_texture,ColorA_t color,int16_t* sprite_sizes,int32_t num_sprite,int32_t size_vertice)
{
	GLsizei draw_size = 0;
	GLint offset = 0;

	render_set_color(render_mngr,color);

	_set_texture(render_mngr,texture,repeat_texture,wfalse);

	_set_attrib_shader(render_mngr,"inPos",&render_mngr->inPos_attrib);

	glVertexAttribPointer(render_mngr->inPos_attrib, 2, GL_FLOAT, GL_FALSE, 0, vertices);

	_set_attrib_shader(render_mngr,"inTex",&render_mngr->inTex_attrib);

	glVertexAttribPointer(render_mngr->inTex_attrib ,2,GL_FLOAT,GL_FALSE,0,texture_coord);

	for(int32_t isprite = 0; isprite < num_sprite; isprite++)
	{
		if(sprite_sizes[isprite] == 0)
			break;

		draw_size = sprite_sizes[isprite];

		glDrawArrays(GL_TRIANGLE_FAN, offset, draw_size);

		offset += draw_size;
	}

	render_pop_matrix(render_mngr);
}


void render_draw_compound_textured_buffer(render_manager_t* const render_mngr,VBO_ID vertices_id,VBO_ID text_coord,VBO_ID indices,Vector_t* positions,float* rotations,int32_t num_sprite,int16_t* sprite_sizes,int16_t* sprite_indices_size)
{

	GLsizei draw_size = 0;

	glBindBuffer(GL_ARRAY_BUFFER,vertices_id);

	_set_attrib_shader(render_mngr,"inPos",&render_mngr->inPos_attrib);

	glVertexAttribPointer(render_mngr->inPos_attrib, 2, GL_FLOAT, GL_FALSE, 0, 0);
	

	glBindBuffer(GL_ARRAY_BUFFER,text_coord);

	_set_attrib_shader(render_mngr,"inTex",&render_mngr->inTex_attrib);

	glVertexAttribPointer(render_mngr->inTex_attrib,2,GL_FLOAT,GL_FALSE,0,0);
	
	_set_uniform_shader(render_mngr,"WVP",&render_mngr->uniTrans);

	glUniformMatrix4fv(render_mngr->uniTrans ,1,GL_FALSE,render_mngr->WVP);

	for(int32_t isprite = 0; isprite < num_sprite; isprite++)
	{
		if(sprite_sizes[isprite] == 0)
			break;

		 draw_size += sprite_indices_size[isprite];
	}

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,indices);
	
	glDrawElements(GL_TRIANGLES,draw_size,GL_UNSIGNED_SHORT,0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void render_draw_bufferindices(render_manager_t* const render_mngr, VBO_ID vertices, VBO_ID indices, size_t num_indices)
{
	glBindBuffer(GL_ARRAY_BUFFER, vertices);

	_set_attrib_shader(render_mngr, "inPos", &render_mngr->inPos_attrib);

	glVertexAttribPointer(render_mngr->inPos_attrib, 2, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indices);

	glDrawElements(GL_TRIANGLES, num_indices, GL_UNSIGNED_SHORT, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void render_draw_3Dbufferindices(render_manager_t* const render_mngr,VBO_ID vertices,VBO_ID indices,VBO_ID normals,size_t num_indices,render_mode mesh_mode,wbool has_normals)
{
	glBindBuffer(GL_ARRAY_BUFFER,vertices);

	_set_attrib_shader(render_mngr,"inPos3",&render_mngr->inPos3_attrib);

	glVertexAttribPointer(render_mngr->inPos3_attrib, 3, GL_FLOAT, GL_FALSE, 0, 0);

	if(has_normals)
	{
		glBindBuffer(GL_ARRAY_BUFFER,normals);
		_set_attrib_shader(render_mngr,"inNormals",&render_mngr->inNormal_attrib);
		glVertexAttribPointer(render_mngr->inNormal_attrib,3,GL_FLOAT,GL_FALSE,0,0);
	}


	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,indices);

	if(mesh_mode == R_FILL)
		glDrawElements(GL_TRIANGLES,num_indices,GL_UNSIGNED_SHORT,0);
	else if(mesh_mode == R_WIREFRAME)
		glDrawElements(GL_LINE_STRIP,num_indices,GL_UNSIGNED_SHORT,0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);

	glBindBuffer(GL_ARRAY_BUFFER,0);

}


void render_draw_textured_array(render_manager_t* const render_mngr,TEXTURE_ID texture,float* vertices,float* texture_coord,wbool repeat_texture,float rotation,int32_t num_vertices,int32_t size_vertice,float x,float y,ColorA_t* const color,wbool is_3d_texture)
{
	glMatrix4x4Identityf(render_mngr->WorldMatrix);
	glMatrix4x4Translatef(render_mngr->WorldMatrix,x,y,0.0f);

	if(rotation != 0.0f)
		glMatrix4x4Rotatef(render_mngr->WorldMatrix,rotation,0.0f,0.0f,1.0f);

	glMatrix4x4Multiplyf(render_mngr->WVP,render_mngr->ProjMatrix,render_mngr->ViewMatrix);
	glMatrix4x4Multiplyf(render_mngr->WVP,render_mngr->WVP,render_mngr->WorldMatrix);

	_set_uniform_shader(render_mngr,"WVP",&render_mngr->uniTrans);

	glUniformMatrix4fv(render_mngr->uniTrans,1,GL_FALSE,render_mngr->WVP);


	if(color != NULL)
	{
		render_set_color(render_mngr,(*color));
	}

	_set_texture(render_mngr,texture,repeat_texture,is_3d_texture);

	_set_attrib_shader(render_mngr,"inPos",&render_mngr->inPos_attrib);

	glVertexAttribPointer(render_mngr->inPos_attrib, size_vertice, GL_FLOAT, GL_FALSE, 0, vertices);

	
	_set_attrib_shader(render_mngr,"inTex",&render_mngr->inTex_attrib);

	glVertexAttribPointer(render_mngr->inTex_attrib,2,GL_FLOAT,GL_FALSE,0,texture_coord);


    glDrawArrays(GL_TRIANGLES, 0, num_vertices);

	if(color != NULL)
	{
		render_reset_color(render_mngr);
	}
    
}

void render_draw_indices(render_manager_t* const render_mngr, VBO_ID indices, size_t num_indices)
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indices);

	glDrawElements(GL_TRIANGLES, num_indices, GL_UNSIGNED_SHORT, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}



VBO_ID render_gen_buffer(render_manager_t* const rendermngr,void* buffer_data,size_t buffer_size,wbool dynamic,wbool stream)
{
	VBO_ID buffer = 0;
	glGenBuffers(1, &buffer);
	glBindBuffer(GL_ARRAY_BUFFER, buffer);

	if(dynamic && stream)
		glBufferData(GL_ARRAY_BUFFER, buffer_size, buffer_data, GL_STREAM_DRAW);
     else if(dynamic && !stream)
          glBufferData(GL_ARRAY_BUFFER, buffer_size, buffer_data, GL_DYNAMIC_DRAW);
	else
		glBufferData(GL_ARRAY_BUFFER,buffer_size,buffer_data,GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	return buffer;
}


void render_update_buffer(render_manager_t* const rendermngr,VBO_ID buffer,void* buffer_data,size_t offset,size_t size)
{
	glBindBuffer(GL_ARRAY_BUFFER, buffer);
	glBufferSubData(GL_ARRAY_BUFFER,offset, size, buffer_data);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

VBO_ID render_gen_bufferindices(render_manager_t* const rendermngr,const uint16_t* const indices,size_t buffer_size,wbool dynamic,wbool stream)
{
	 GLenum usage = GL_STATIC_DRAW;

     if(dynamic && stream)
          usage = GL_STREAM_DRAW;
     else if(dynamic && !stream)
          usage = GL_DYNAMIC_DRAW;

	VBO_ID buffer = 0;
	glGenBuffers(1, &buffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, buffer_size, indices, usage);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	return buffer;
}

void render_bind_buffer(render_manager_t* const rendermngr,VBO_ID buffer)
{
	glBindBuffer(GL_ARRAY_BUFFER, buffer);
}

void render_step_update_buffer(render_manager_t* const rendermngr,float* buffer_data,size_t offset,size_t size,VBO_ID buffer)
{
	glBufferSubData(GL_ARRAY_BUFFER,offset, size, buffer_data);
}

void render_unbind_buffer(render_manager_t* const rendermngr,VBO_ID buffer)
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void render_update_indices_buffer(render_manager_t* const rendermngr,VBO_ID buffer,void* buffer_data,size_t offset,size_t size)
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer);
	glBufferSubData(GL_ELEMENT_ARRAY_BUFFER,offset, size, buffer_data);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void render_discard_update_indicesbuffer(render_manager_t* const rendermngr,VBO_ID buffer,uint16_t* buffer_data,size_t buffer_size,wbool dynamic,wbool stream)
{
      GLenum usage = GL_STATIC_DRAW;

     if(dynamic && stream)
          usage = GL_STREAM_DRAW;
     else if(dynamic && !stream)
          usage = GL_DYNAMIC_DRAW;

     glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,buffer_size, NULL, usage);
     glBufferData(GL_ELEMENT_ARRAY_BUFFER,buffer_size,buffer_data,usage);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}


void render_discard_updatebuffer(render_manager_t* const rendermngr,VBO_ID buffer,float* buffer_data,size_t buffer_size,wbool dynamic,wbool stream)
{
     GLenum usage = GL_STATIC_DRAW;

     if(dynamic && stream)
          usage = GL_STREAM_DRAW;
     else if(dynamic && !stream)
          usage = GL_DYNAMIC_DRAW;

     glBindBuffer(GL_ARRAY_BUFFER, buffer);
     glBufferData(GL_ARRAY_BUFFER,buffer_size,NULL,usage);
     glBufferData(GL_ARRAY_BUFFER,buffer_size,buffer_data,usage);
     glBindBuffer(GL_ARRAY_BUFFER,0);
}


FENCEID render_addfencesync(render_manager_t* const rendermngr)
{
	return glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
}

void render_removefencesync(FENCEID syncid)
{
	glDeleteSync(syncid);
}

wbool render_fencesyncisready(render_manager_t* const rendermngr, FENCEID syncid)
{
	GLenum result = glClientWaitSync(syncid, 0, 0);

	if (result == GL_TIMEOUT_EXPIRED || result == GL_WAIT_FAILED) {
		return wfalse;
	}

	return wtrue;
}

void render_unsync_updatebuffer(render_manager_t* const rendermngr, VBO_ID buffer, float* buffer_data, size_t buffer_size)
{
	glBindBuffer(GL_ARRAY_BUFFER, buffer);

	//see : https://github.com/emscripten-core/emscripten/issues/6692
	//the error message being : glMapBufferRange is only supported when access is MAP_WRITE|INVALIDATE_BUFFER
	#ifdef EMSCRIPTEN
		float* data = (float*)glMapBufferRange(GL_ARRAY_BUFFER, 0, buffer_size, GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
	#else
		float* data = (float*)glMapBufferRange(GL_ARRAY_BUFFER, 0, buffer_size, GL_MAP_WRITE_BIT | GL_MAP_UNSYNCHRONIZED_BIT);
	#endif

	for (size_t ibuf = 0; ibuf < buffer_size / sizeof(float); ibuf++) {
		data[ibuf] = buffer_data[ibuf];
	}

	glUnmapBuffer(GL_ARRAY_BUFFER);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void render_resize_updatebuffer(render_manager_t* const rendermngr, VBO_ID buffer, float* buffer_data, size_t buffer_size,wbool dynamic,wbool stream)
{
	GLenum usage = GL_STATIC_DRAW;

	if (dynamic && stream)
		usage = GL_STREAM_DRAW;
	else if (dynamic && !stream)
		usage = GL_DYNAMIC_DRAW;

	glBindBuffer(GL_ARRAY_BUFFER, buffer);
	glBufferData(GL_ARRAY_BUFFER, buffer_size, buffer_data, usage);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void render_start_draw_buffer(render_manager_t* const render_mngr,TEXTURE_ID texture,VBO_ID vertices_buffer,VBO_ID texcoord_buffer, VBO_ID color_buffer,int32_t size_vertice)
{
	_set_texture(render_mngr,texture,wfalse,wfalse);
	
	glBindBuffer(GL_ARRAY_BUFFER, vertices_buffer);

	_set_attrib_shader(render_mngr,"inPos",&render_mngr->inPos_attrib);

	glVertexAttribPointer(render_mngr->inPos_attrib, size_vertice, GL_FLOAT, GL_FALSE, 0, 0);
	


	if(texcoord_buffer != 0)
	{
		glBindBuffer(GL_ARRAY_BUFFER,texcoord_buffer);

		_set_attrib_shader(render_mngr,"inTex",&render_mngr->inTex_attrib);
		glVertexAttribPointer(render_mngr->inTex_attrib,2,GL_FLOAT,GL_FALSE,0,0);
	}
	
	if(color_buffer != 0)
	{
		glBindBuffer(GL_ARRAY_BUFFER,color_buffer);

		_set_attrib_shader(render_mngr,"inColors",&render_mngr->inColor_attrib);
		glVertexAttribPointer(render_mngr->inColor_attrib,4,GL_FLOAT,GL_TRUE,0,0);
	}

}

void render_draw_buffer(render_manager_t* const render_mngr,int32_t start_index,int32_t length)
{
	glDrawArrays(GL_TRIANGLES, start_index, length);
}

void render_end_draw_buffer(render_manager_t* const render_mngr,wbool has_textcoord,wbool has_colors)
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}


void render_delete_buffer(VBO_ID buffer)
{
	glDeleteBuffers(1,&buffer);
}



void render_set_position(render_manager_t* const render_mngr,Vector_t position)
{
	position.x =  truncf(position.x);
	position.y = truncf(position.y);

	glMatrix4x4Identityf(render_mngr->WorldMatrix);
	glMatrix4x4Translatef(render_mngr->WorldMatrix,position.x,position.y,0.0f);

	_render_update_matrixes(render_mngr,render_mngr->mat_flag);
}

void render_set_position3(render_manager_t* const render_mngr,Vector3_t position)
{
	glMatrix4x4Identityf(render_mngr->WorldMatrix);
	glMatrix4x4Translatef(render_mngr->WorldMatrix,position.x,position.y,position.z);

	_render_update_matrixes(render_mngr,render_mngr->mat_flag);
}

void render_set_rotation(render_manager_t* const render_mngr,float angle)
{
	glMatrix4x4Rotatef(render_mngr->WorldMatrix,angle,0.0f,0.0f,1.0f);

	_render_update_matrixes(render_mngr,render_mngr->mat_flag);
}

void render_set_rotation2(render_manager_t* const render_mngr,float angle_x,float angle_y,float angle_z)
{
	if(angle_x != 0.0f)
			glMatrix4x4Rotatef(render_mngr->WorldMatrix,angle_x,1.0f,0.0f,0.0f);

	if(angle_y != 0.0f)
		glMatrix4x4Rotatef(render_mngr->WorldMatrix,angle_y,0.0f,1.0f,0.0f);
		
	if(angle_z != 0.0f)
		glMatrix4x4Rotatef(render_mngr->WorldMatrix,angle_z,0.0f,0.0f,1.0f);

	_render_update_matrixes(render_mngr,render_mngr->mat_flag);
}

void render_set_scale(render_manager_t* const render_mngr,float scaleX,float scaleY,float scaleZ)
{
	glMatrix4x4Scalef(render_mngr->WorldMatrix,scaleX,scaleY,scaleZ);
	
	_render_update_matrixes(render_mngr,render_mngr->mat_flag);
}


void render_start_draw_array(render_manager_t* const render_mngr,int32_t size_vertice,uint32_t numVertices,float* vertices_array,float* tex_coord_array)
{
	if(!tex_coord_array)
	{
		render_unbind_texture(render_mngr,wfalse);
	}

	_set_uniform_shader(render_mngr,"WVP",&render_mngr->uniTrans);

	glUniformMatrix4fv(render_mngr->uniTrans,1,GL_FALSE,render_mngr->WVP);

	_set_attrib_shader(render_mngr,"inPos",&render_mngr->inPos_attrib);

	//#if defined(RENDERER_OPENGL_ES) && !defined(ANDROID_BUILD)
		if(render_mngr->last_size_vert_vbo == 0)
			render_mngr->tmp_vertices_vbo = render_gen_buffer(render_mngr,vertices_array,size_vertice * numVertices * sizeof(float),wfalse,wfalse);
		else if(render_mngr->last_size_vert_vbo < numVertices)
			render_discard_updatebuffer(render_mngr,render_mngr->tmp_vertices_vbo,vertices_array,size_vertice * numVertices * sizeof(float),wfalse,wfalse);
		else
			render_update_buffer(render_mngr,render_mngr->tmp_vertices_vbo,vertices_array,0,size_vertice * numVertices * sizeof(float));

		render_mngr->last_size_vert_vbo = numVertices;

		render_bind_buffer(render_mngr,render_mngr->tmp_vertices_vbo);

		glVertexAttribPointer(render_mngr->inPos_attrib, 2, GL_FLOAT, GL_FALSE, 0, 0);
	//#else
	//	glVertexAttribPointer(render_mngr->inPos_attrib, 2, GL_FLOAT, GL_FALSE, 0, vertices_array);
	//#endif

    
    if(tex_coord_array)
    {
		_set_attrib_shader(render_mngr,"inTex",&render_mngr->inTex_attrib);

		//#if defined(RENDERER_OPENGL_ES) && !defined(ANDROID_BUILD)
			if(render_mngr->last_size_texcoord_vbo == 0)
				render_mngr->tmp_texcoord_vbo = render_gen_buffer(render_mngr,tex_coord_array,size_vertice * numVertices * sizeof(float),wfalse,wfalse);
			else if(render_mngr->last_size_texcoord_vbo < numVertices)
				render_discard_updatebuffer(render_mngr,render_mngr->tmp_texcoord_vbo,tex_coord_array,size_vertice * numVertices * sizeof(float),wfalse,wfalse);
			else
				render_update_buffer(render_mngr,render_mngr->tmp_texcoord_vbo,tex_coord_array,0,size_vertice * numVertices * sizeof(float));

			render_mngr->last_size_texcoord_vbo = numVertices;

			render_bind_buffer(render_mngr,render_mngr->tmp_texcoord_vbo);

			glVertexAttribPointer(render_mngr->inTex_attrib, 2, GL_FLOAT, GL_FALSE, 0, 0);
	//	#else
	//		glVertexAttribPointer(render_mngr->inTex_attrib,2,GL_FLOAT,GL_FALSE,0,tex_coord_array);
	//	#endif

    }
        
}

void render_draw_array(render_manager_t* const render_mngr,int32_t offset, int32_t numVertices)
{
	glDrawArrays(GL_TRIANGLES,offset,numVertices);
}

void render_draw_line(render_manager_t* const render_mngr,int32_t offset, int32_t numVertices)
{
	glDrawArrays(GL_LINES,offset,numVertices);
}

void render_draw_fan(render_manager_t* const render_mngr,int32_t offset, int32_t numVertices)
{
	glDrawArrays(GL_TRIANGLE_FAN,offset,numVertices);
}

void render_draw_lineloop(render_manager_t* const render_mngr,int32_t offset, int32_t numVertices)
{
	glDrawArrays(GL_LINE_LOOP,offset,numVertices);
}

void render_draw_points(render_manager_t* const render_mngr,int32_t offset, int32_t numVertices,float pointsize)
{
     #ifndef RENDERER_OPENGL_ES
	     glPointSize(pointsize);
     #endif
	glDrawArrays(GL_POINTS,offset,numVertices);
}


void render_mult_matrix(render_manager_t* const render_mngr,const float* matrix)
{
	glMatrix4x4Identityf(render_mngr->WorldMatrix);
	glMatrix4x4Multiplyf(render_mngr->WorldMatrix,render_mngr->WorldMatrix,matrix);

	_render_update_matrixes(render_mngr,render_mngr->mat_flag);
}

void render_end_draw_array(render_manager_t* const render_mngr,wbool has_textcoord)
{

	if(has_textcoord)
	{
		#if defined(RENDERER_OPENGL_ES) && !defined(ANDROID_BUILD)
		render_unbind_buffer(render_mngr,0);
		#endif

		//_disable_attrib_shader(render_mngr,&render_mngr->inTex_attrib);
		//GLint tex_attrib = glGetAttribLocation(render_mngr->current_program,"inTex");
		//glDisableVertexAttribArray(tex_attrib); // Clear Texture Coords State
	} 

	//_disable_attrib_shader(render_mngr,&render_mngr->inPos_attrib);

   // GLint pos_attrib = glGetAttribLocation(render_mngr->current_program,"inPos");
	//glDisableVertexAttribArray(pos_attrib);
}

TEXTURE_ID render_gen_texture(render_manager_t* const rendermngr, uint32_t width, uint32_t height, unsigned char* buffer, unsigned long buffer_size, wbool luminance)
{
	return render_gen_texture_withfilter(rendermngr, width, height, buffer, buffer_size,luminance, rendermngr->texture_filtering);
}


TEXTURE_ID render_gen_texture_withfilter(render_manager_t* const rendermngr,uint32_t width,uint32_t height,unsigned char* buffer,unsigned long buffer_size,wbool luminance,texture_filter filter)
{
	TEXTURE_ID texture = (TEXTURE_ID)wf_malloc(sizeof(GLuint));

	glGenTextures(1, texture);                  // Create The Texture

    //Typical Texture Generation Using Data From The Bitmap
    glBindTexture(GL_TEXTURE_2D,*texture);

	if(filter == filter_pixelart)
	{
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );  // Set Minification Filter
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );  // Set Magnification Filter
	}
	else if (filter == filter_linear)
	{
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);  // Set Minification Filter
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );  // Set Magnification Filter
	}


    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE);

	#ifdef RENDERER_OPENGL_ES
		glTexImage2D(GL_TEXTURE_2D, 0,GL_RGBA, width,height, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
	#else
		glTexImage2D(GL_TEXTURE_2D, 0,GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
	#endif 

	if (filter == filter_linear)
	{
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	
	glBindTexture(GL_TEXTURE_2D, 0);

	rendermngr->current_bind_texture = NULL;

	return texture;
}

TEXTURE_ID render_gen_texture3D(render_manager_t* const rendermngr,uint32_t width,uint32_t height,uint32_t depth,unsigned char* buffer,unsigned long buffer_size)
{
    #ifndef RENDERER_OPENGL_ES
	TEXTURE_ID texture = (TEXTURE_ID)wf_malloc(sizeof(GLuint));

	glGenTextures(1, texture);                  // Create The Texture

    //Typical Texture Generation Using Data From The Bitmap
    glBindTexture(GL_TEXTURE_3D,*texture);

	glTexParameteri( GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );  // Set Minification Filter
	glTexParameteri( GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );  // Set Magnification Filter


    glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_R,GL_CLAMP_TO_EDGE);

	glTexImage3D(GL_TEXTURE_3D, 0,GL_RGBA8, width, height,depth, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);

	glBindTexture(GL_TEXTURE_3D, 0);

	rendermngr->current_bind_texture = NULL;

	return texture;
    #else
        logprint("No texture 3D support under GLES!");
        return NULL;
    #endif
}

void render_delete_texture(render_manager_t* const rendermngr,TEXTURE_ID texture,wbool is_3d)
{
	if(rendermngr->current_bind_texture == texture)
	{
		rendermngr->current_bind_texture = NULL;

		if(is_3d)
			glBindTexture(GL_TEXTURE_3D,0);
		else
			glBindTexture(GL_TEXTURE_2D,0);
	}

	glDeleteTextures(1,texture);
	wf_free(texture);
}

void render_delete_shader(render_manager_t* const rendermngr,SHADER_ID shader)
{
	glDeleteShader(shader->shader_id);

	if(shader->num_sub_shader > 0 && shader->sub_shader_ids)
	{
		for(uint32_t is = 0;is < shader->num_sub_shader;is++)
			glDeleteShader(shader->sub_shader_ids[is]);

		wf_free(shader->sub_shader_ids);
	}

	wf_free(shader);
}

void render_setshader_attribute_float(render_manager_t* const rendermngr,SHADERPROGRAM_ID prog_id,const char* name,int32_t size,int32_t stride,void* offset)
{
	GLint index = glGetAttribLocation(rendermngr->current_program->active_prog_id,name);
	glEnableVertexAttribArray(index);

	glVertexAttribPointer(index,size,GL_FLOAT,GL_FALSE,stride,offset);
}

void render_setshader_attribute_uint(render_manager_t* const rendermngr, SHADERPROGRAM_ID prog_id, const char* name, int32_t size, int32_t stride, void* offset)
{
	GLint index = glGetAttribLocation(rendermngr->current_program->active_prog_id, name);
	glEnableVertexAttribArray(index);

	glVertexAttribPointer(index, size, GL_UNSIGNED_INT, GL_FALSE, stride, offset);
}

void render_setshader_attribute_ubyte(render_manager_t* const rendermngr, SHADERPROGRAM_ID prog_id, const char* name, int32_t size, int32_t stride, void* offset,wbool normalize)
{
	GLint index = glGetAttribLocation(rendermngr->current_program->active_prog_id, name);
	glEnableVertexAttribArray(index);

	glVertexAttribPointer(index, size, GL_UNSIGNED_BYTE, (normalize) ? GL_TRUE : GL_FALSE, stride, offset);
}



VAO_ID render_gen_vao(render_manager_t* const rendermngr)
{
     #ifndef ANDROID_BUILD
	     VAO_ID vao = 0;
	     #if __APPLE__
		     glGenVertexArraysAPPLE(1, &vao);
	     #else
		     glGenVertexArrays(1, &vao);
	     #endif
	return vao;
     #else
     return 0;
     #endif
}

void render_bind_vao(render_manager_t* const rendermngr,VAO_ID vao)
{
     #ifndef ANDROID_BUILD

	     #if __APPLE__
		     glBindVertexArrayAPPLE(vao);
	     #else
		     glBindVertexArray(vao);
	     #endif
     #endif
}

void render_unbind_vao(render_manager_t* const rendermngr)
{
     #ifndef ANDROID_BUILD
	     #if __APPLE__
		     glBindVertexArrayAPPLE(0);
	     #else
		     glBindVertexArray(0);
	     #endif
     #endif
}

void render_delete_vao(render_manager_t* const rendermngr,VAO_ID vao)
{
     #ifndef ANDROID_BUILD

	     #if __APPLE__
		     glDeleteVertexArraysAPPLE(1,&vao);
	     #else
		     glDeleteVertexArrays(1,&vao);
	     #endif
     #endif
    
}

FBO_ID render_alloc_fbo(render_manager_t* const rendermngr)
{
	FBO_ID fbo = (FBO_ID)wf_malloc(sizeof(GLuint));
	return fbo;
}

RBO_ID render_alloc_rbo(render_manager_t* const rendermngr)
{
	RBO_ID rbo = (RBO_ID)wf_malloc(sizeof(GLuint));
	return rbo;
}

TEXTURE_ID render_alloc_texture(render_manager_t* const rendermngr)
{
	TEXTURE_ID texture = (TEXTURE_ID)wf_malloc(sizeof(GLuint));
	return texture;
}

void render_create_fbo(render_manager_t* const rendermngr,FBO_ID fbo_id,TEXTURE_ID fbo_texture,int16_t fbo_width,int16_t fbo_height)
{
	//gen fbo
	glGenFramebuffers(1,fbo_id);
	glBindFramebuffer(GL_FRAMEBUFFER,(*fbo_id));

	//gen texture
	glGenTextures(1,fbo_texture);
	glBindTexture(GL_TEXTURE_2D,(*fbo_texture));

	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,fbo_width,fbo_height,0,GL_RGBA,GL_UNSIGNED_BYTE,0);

	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);

	#ifdef RENDERER_OPENGL_ES
		 glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, (*fbo_texture), 0);
	#else
		glFramebufferTexture(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,(*fbo_texture),0);

		GLenum drawbuffers[1] = {GL_COLOR_ATTACHMENT0};

		glDrawBuffers(1,drawbuffers);
	#endif
	
	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		logprint("Error while creating frame buffer");

	glBindFramebuffer(GL_FRAMEBUFFER,0);
	glBindTexture(GL_TEXTURE_2D,0);
}

void render_bind_framebuffer(render_manager_t* const rendermngr,FBO_ID framebuffer,TEXTURE_ID texture,int16_t fbo_width,int16_t fbo_height)
{
	glBindFramebuffer(GL_FRAMEBUFFER,(*framebuffer));

	w_glOrthof(rendermngr->ProjMatrix,(GLfloat)0.0f, (GLfloat)fbo_width, (GLfloat)0.0f, (GLfloat)fbo_height, rendermngr->near_value, rendermngr->far_value);
	glViewport(0,0,fbo_width,fbo_height);
}

void render_unbind_framebuffer(render_manager_t* const rendermngr,FBO_ID framebuffer)
{
	glBindFramebuffer(GL_FRAMEBUFFER,0);

	render_update_screen(rendermngr);

	if(rendermngr->fullscreen)
		glViewport(0,0,rendermngr->fullscreen_width,rendermngr->fullscreen_height);
	else
		glViewport(0,0,rendermngr->screen_info.width,rendermngr->screen_info.height);
}

void render_delete_fbo(render_manager_t* const rendermngr,FBO_ID framebuffer)
{
	glDeleteFramebuffers(1,framebuffer);
	wf_free(framebuffer);
}

void render_delete_rbo(render_manager_t* const rendermngr,RBO_ID renderbuffer)
{
	glDeleteRenderbuffers(1,renderbuffer);
	wf_free(renderbuffer);
}



void render_bind_texture(render_manager_t* const rendermngr,TEXTURE_ID texture,wbool repeat_texture,wbool is_3d)
{
	_set_texture(rendermngr,texture,repeat_texture,is_3d);
}

void render_unbind_texture(render_manager_t* const rendermngr,wbool is_3d)
{
	if(is_3d)
		glBindTexture(GL_TEXTURE_3D,0);
	else
		glBindTexture(GL_TEXTURE_2D,0);
	rendermngr->current_bind_texture = NULL;
}

void render_set_color(render_manager_t* const render_mngr,ColorA_t color)
{
	if(render_mngr->current_color.r != color.r || render_mngr->current_color.g != color.g || render_mngr->current_color.b != color.b || render_mngr->current_color.a != color.a)
	{
		_set_uniform_shader(render_mngr,"inColor",&render_mngr->uniColor);
		glUniform4f(render_mngr->uniColor,color.r, color.g, color.b, color.a ); // Set Color+Alpha

		render_mngr->current_color.r = color.r;
		render_mngr->current_color.g = color.g;
		render_mngr->current_color.b = color.b;
		render_mngr->current_color.a = color.a;
	}

	
}

void render_reset_color(render_manager_t* const render_mngr)
{
	render_mngr->current_color.r = render_mngr->current_color.g = render_mngr->current_color.b = render_mngr->current_color.a = 0.0f;
	ColorA_t color = {1.0f,1.0f,1.0f,1.0f};
	render_set_color(render_mngr,color);
}

const char* render_get_error(render_manager_t* const render_mngr)
{
	GLenum error_value =  glGetError();

	switch(error_value)
	{
		case GL_NO_ERROR:
			return "GL_NO_ERROR";
		case GL_INVALID_ENUM:
			return "GL_INVALID_ENUM";
		case GL_INVALID_VALUE:
			return "GL_INVALID_VALUE";
		case GL_INVALID_OPERATION:
			return "GL_INVALID_OPERATION";
		case GL_INVALID_FRAMEBUFFER_OPERATION:
			return "GL_INVALID_FRAMEBUFFER_OPERATION";
		case GL_OUT_OF_MEMORY:
			return "GL_OUT_OF_MEMORY";
		case GL_STACK_UNDERFLOW:
			return "GL_STACK_UNDERFLOW";
		case GL_STACK_OVERFLOW:
			return "GL_STACK_OVERFLOW";
	}

	return "GL unknow error";
}

void render_stop(render_manager_t* const rendermngr)
{
	//freeing shader programs hashtable
	hashtable_t* tmp = (hashtable_t*)rendermngr->hashtable_shader_programs;

	uint32_t ish = 0;
	uint32_t count_sh = 0;

	while(count_sh < tmp->num_elem)
	{
		if(tmp->keys[ish])
		{
			wf_free(tmp->keys[ish]);
			count_sh++;
		}

		ish++;
	}
	
	wf_free(tmp->keys);
	tmp->keys = NULL;
	tmp->num_elem = 0;
	tmp->max_elem = 0;

	wf_free((hashtable_t*)rendermngr->hashtable_shader_programs);

	rendermngr->hashtable_shader_programs = NULL;

	render_delete_buffer(rendermngr->tmp_texcoord_vbo);

	render_delete_buffer(rendermngr->tmp_vertices_vbo);

    render_unbind_vao(rendermngr);
    render_delete_vao(rendermngr,rendermngr->global_vao);

	render_destroy_window(rendermngr);

	render_freevideoresolutions(rendermngr);
}

float render_gettime2(wbool msec)
{
	if(msec)
		return (float)(clock() * 1000 / CLOCKS_PER_SEC);
	else
	{
		float value_msec = (float)(clock() * 1000 / CLOCKS_PER_SEC);
		return value_msec / 1000.0f;
	}
}

void render_freevideoresolutions(render_manager_t* const rendermngr)
{
	if (rendermngr->video_resolutions != NULL) {

		for (int ires = 0; ires < rendermngr->countresolutions; ires++) {
			wf_free(rendermngr->video_resolutions[ires]);
		}

		wf_free(rendermngr->video_resolutions);
		rendermngr->video_resolutions = NULL;
		rendermngr->countresolutions = 0;
	}

}

#endif

