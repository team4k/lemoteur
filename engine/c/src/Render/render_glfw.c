#ifdef USE_GLFW_RENDER

#include <Render.h>

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

#include <Engine.h>
#include <Base/types.h>
#include <Base/functions.h>
#include <Base/geom.h>
#include <Base/Color.h>

#include <Utils/Hash.h>

#include <Render/render_gl_math.h>
#include <Render/render_gl_matrix.h>
#include <Render/render_gl_viewprojection.h>


#include <Debug/Logprint.h>

#include <Render/render_manager.h>

int render_init_window_render(render_manager_t* const rendermngr, wbool editor_mode, wbool fullscreen, const char* window_title, void* app_instance, void* app_handle,int16_t refreshrate)
{
	if (!glfwInit())
	{
		logprint("GL RENDER: error while glfw init !");
		return 0;
	}

	rendermngr->video_resolutions = NULL;
	rendermngr->countresolutions = 0;

	//try to create an opengl context
	#ifdef RENDERER_OPENGL_ES
		glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_ES_API);
	#endif

		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, rendermngr->major_opengl_version);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, rendermngr->minor_opengl_version);

		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	#ifdef _DEBUG
		glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
	#endif

	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	if (!fullscreen || editor_mode)//no fullscreen in editor mode
		#if defined(_WIN32)
				rendermngr->oglWindow = glfwCreateWindow(rendermngr->screen_info.width, rendermngr->screen_info.height, window_title, NULL, NULL, app_handle);
		#else
				rendermngr->oglWindow = glfwCreateWindow(rendermngr->screen_info.width, rendermngr->screen_info.height, window_title, NULL, NULL);
		#endif
	else
	{
		rendermngr->fullscreen_width = rendermngr->screen_info.width;
		rendermngr->fullscreen_height = rendermngr->screen_info.height;

		#if defined(_WIN32)
				rendermngr->oglWindow = glfwCreateWindow(rendermngr->screen_info.width, rendermngr->screen_info.height, window_title, glfwGetPrimaryMonitor(), NULL, NULL);
		#else
				rendermngr->oglWindow = glfwCreateWindow(cmode->width, cmode->height, window_title, glfwGetPrimaryMonitor(), NULL);
		#endif

		glfwSetWindowMonitor(rendermngr->oglWindow, glfwGetPrimaryMonitor(), 0, 0, rendermngr->screen_info.width, rendermngr->screen_info.height, refreshrate);
	}

	if (!rendermngr->oglWindow)
	{
		logprint("GL RENDER: error while trying to open a GLFW windows!, No OpenGL %d.%d support", rendermngr->major_opengl_version, rendermngr->minor_opengl_version);
		glfwTerminate();
		exit(EXIT_FAILURE);
	}


	glfwMakeContextCurrent(rendermngr->oglWindow);

	return 1;
}

void render_swap_buffers(render_manager_t* const rendermngr)
{
	glfwSwapBuffers(rendermngr->oglWindow);
}


void render_set_swap_interval(render_manager_t* const rendermngr)
{
	glfwSwapInterval(0); //0 = no vsync, 1 = vsync
}

wbool render_recreate_window(render_manager_t* const rendermngr,wbool fullscreen, void* app_handle,int16_t refreshrate)
{
	wbool setup_opengl = wfalse;
	rendermngr->fullscreen = fullscreen;
	rendermngr->refreshrate = refreshrate;

	glfwDestroyWindow(rendermngr->oglWindow);

	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);


	//recreate opengl context
	#ifdef RENDERER_OPENGL_ES
			glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_ES_API);
	#endif

			glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, rendermngr->major_opengl_version);
			glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, rendermngr->minor_opengl_version);

			glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
			glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

			if (!fullscreen)
			{
	#if defined(_WIN32)
				rendermngr->oglWindow = glfwCreateWindow(rendermngr->screen_info.width, rendermngr->screen_info.height, rendermngr->window_title, NULL, NULL, NULL);
	#else
				rendermngr->oglWindow = glfwCreateWindow(rendermngr->screen_info.width, rendermngr->screen_info.height, rendermngr->window_title, NULL, NULL);
	#endif
			}
			else
			{
					
				/*const GLFWvidmode* cmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
				rendermngr->fullscreen_width = cmode->width;
				rendermngr->fullscreen_height = cmode->height;

				rendermngr->fullscreen_offsetx = (int32_t)((cmode->width - rendermngr->screen_info.width) * 0.5f);
				rendermngr->fullscreen_offsety = (int32_t)((cmode->height - rendermngr->screen_info.height) * 0.5f);*/

				rendermngr->fullscreen_width = rendermngr->screen_info.width;
				rendermngr->fullscreen_height = rendermngr->screen_info.height;

	#if defined(_WIN32)
				rendermngr->oglWindow = glfwCreateWindow(rendermngr->screen_info.width, rendermngr->screen_info.height, rendermngr->window_title, glfwGetPrimaryMonitor(), NULL, NULL);
	#else
				rendermngr->oglWindow = glfwCreateWindow(cmode->width, cmode->height, rendermngr->window_title, glfwGetPrimaryMonitor(), NULL);
	#endif

				glfwSetWindowMonitor(rendermngr->oglWindow, glfwGetPrimaryMonitor(), 0, 0, rendermngr->screen_info.width, rendermngr->screen_info.height, refreshrate);

			}


			glfwMakeContextCurrent(rendermngr->oglWindow);

			glewInit();
			setup_opengl = wtrue;
			render_freevideoresolutions(rendermngr);

	return setup_opengl;
}

void render_destroy_window(render_manager_t* const rendermngr)
{
	//Close window and terminate
	glfwDestroyWindow(rendermngr->oglWindow);
	glfwTerminate();
}

double render_gettime()
{
	return glfwGetTime() * 1000;
}

int render_getrefreshrate(render_manager_t* const rendermngr)
{
	GLFWmonitor* monitor = glfwGetPrimaryMonitor();

	const GLFWvidmode* mode = glfwGetVideoMode(monitor);

	return mode->refreshRate;
}

//TODO : return a list of resolution with the corresponding aspect ratio, rather than an already build string (aspectratio * height = width)
const videomode_t** const render_getvideoresolutions(render_manager_t* const rendermngr,int* outcountresolutions)
{
	if (rendermngr->video_resolutions != NULL) {
		(*outcountresolutions) = rendermngr->countresolutions;
		return (const videomode_t** const)rendermngr->video_resolutions;
	}


	GLFWmonitor* monitor = glfwGetPrimaryMonitor();

	int countmodes = 0;

	const GLFWvidmode* modes =  glfwGetVideoModes(monitor, &countmodes);

	videomode_t** resolutions = (videomode_t**)wf_calloc(countmodes, sizeof(videomode_t*));

	int cwidth = 0;
	int cheight = 0;
	int refreshrate = 0;

	(*outcountresolutions) = 0;

	for (int imode = 0; imode < countmodes; imode++) {

		if (cwidth == modes[imode].width && cheight == modes[imode].height && refreshrate == modes[imode].refreshRate)
			continue;

		videomode_t* iresol = (videomode_t*)wf_calloc(1, sizeof(videomode_t));
		iresol->width = modes[imode].width;
		iresol->height = modes[imode].height;
		iresol->refreshrate = modes[imode].refreshRate;

		float valar = (float)iresol->height / (float)iresol->width;

		if (valar == 0.75f) {
			iresol->aspectratio = AR_4_3;
		}
		else if (valar == 0.625f) {
			iresol->aspectratio = AR_16_10;
		}
		else if (valar == 0.5625f) {
			iresol->aspectratio = AR_16_9;
		}
		else if (valar == 0.421875f) {
			iresol->aspectratio = AR_21_9;
		}
		else {
			iresol->aspectratio = AR_UNKOWN;
		}


		resolutions[(*outcountresolutions)] = iresol;
		(*outcountresolutions)++;
		cwidth = modes[imode].width;
		cheight = modes[imode].height;
		refreshrate = modes[imode].refreshRate;
	}

	rendermngr->video_resolutions = resolutions;
	rendermngr->countresolutions = (*outcountresolutions);

	return (const videomode_t** const)rendermngr->video_resolutions;
}


#endif
