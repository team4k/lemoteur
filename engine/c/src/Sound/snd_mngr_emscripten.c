﻿#ifdef EMSCRIPTEN

#include <inttypes.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_audio.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <Engine.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/geom.h>

#include <limits.h>

#include <Debug/Logprint.h>

#include <Sound/Sound.h>
#include <Utils/Hash.h>
#include <Sound/snd_mngr.h>
#include <emscripten.h>

static void sdl_audio_callback(void* userData,Uint8* _stream,int _length)
{

	//check that our sound buffer have enough space
	if((_length / sizeof(float)) > MAX_BUFFER_SIZE)
	{
		logprint("framecount too big for buffer size! buffer size=%i framecount=%i",MAX_BUFFER_SIZE,_length);
		return;
	}

	snd_mngr_t*  snd_mngr = (snd_mngr_t*)userData;

	uint32_t add_counter = snd_mngr->add_counter;

	//we check our add array to see if we have any sound to add
	for(uint32_t a = 0; a < add_counter;a++)
	{
		int pos = hashtable_insertkey(&snd_mngr->snd_hash,snd_mngr->add_array[a]);

		snd_mngr->snd_array[pos].sample = snd_mngr->snd_add_array[a].sample;
		snd_mngr->snd_array[pos].loop =  snd_mngr->snd_add_array[a].loop;
		snd_mngr->snd_array[pos].position =  snd_mngr->snd_add_array[a].position;
		snd_mngr->snd_array[pos].ended =  snd_mngr->snd_add_array[a].ended;
		snd_mngr->snd_array[pos].force_ended = snd_mngr->snd_add_array[a].force_ended;
		snd_mngr->snd_array[pos].remove_on_ended = snd_mngr->snd_add_array[a].remove_on_ended;
		snd_mngr->snd_array[pos].sound_type = snd_mngr->snd_add_array[a].sound_type;
		snd_mngr->snd_array[pos].game_position = snd_mngr->snd_add_array[a].game_position;
		snd_mngr->snd_array[pos].positionning = snd_mngr->snd_add_array[a].positionning;
		snd_mngr->snd_array[pos].falloffdistance = snd_mngr->snd_add_array[a].falloffdistance;
		w_strcpy(snd_mngr->snd_array[pos].map_id,128,snd_mngr->snd_add_array[a].map_id);

		hashtable_pushkey(&snd_mngr->snd_hash,snd_mngr->add_array[a]);
		snd_mngr->add_array[a] = NULL;
	}

	snd_mngr->add_counter = 0;


  float *cursor; /* current pointer into the output  */
  float *out = (float*)_stream;
  sound_size_t thisSize = (sound_size_t)((_length / sizeof(float)) / snd_mngr->num_channels);
  sound_size_t thisRead = 0;
  int16_t snd_count = 0;
  int16_t snd_index = 0;
 // wbool no_more_sample = wtrue;

  float sample_buffer[MAX_BUFFER_SIZE] = {0};//buffer used to retrieve data from sound file
  memset(&snd_mngr->snd_buffer[0],0,sizeof(snd_mngr->snd_buffer));//reset our mix buffer

   cursor = out; /* set the output cursor to the beginning */
   

  //aquiring sound values and mixing them into an intermediate buffer
	while(snd_count <  snd_mngr->snd_hash.num_elem)
	{
		if(snd_mngr->snd_hash.keys[snd_index])
		{

			snd_count++;

			sound_t* snd_tmp = &snd_mngr->snd_array[snd_index];

			uint32_t can_read_map_id = 0;

			atomic_set_value(&can_read_map_id,snd_mngr->mapid_can_read);
			//check if the sound is on the current map
			if(can_read_map_id && strcmp(snd_tmp->map_id,"") != 0 && strcmp(snd_mngr->current_map_id,"") != 0 && strcmp(snd_tmp->map_id,snd_mngr->current_map_id) != 0)
			{
				snd_index++;
				continue;
			}

			uint32_t ended = snd_tmp->ended;
			uint32_t remove_on_ended = snd_tmp->remove_on_ended;
			uint32_t force_ended = snd_tmp->force_ended;
			uint32_t rewind = snd_tmp->rewind;

			float left_channel_tweak = 1.0f;
			float right_channel_tweak = 1.0f;

			
			if(rewind == 1)
			{
				snd_tmp->position = 0;
				snd_tmp->rewind = 0;

				ended = 0;
				force_ended = 0;

				snd_tmp->ended = ended;
				snd_tmp->force_ended = force_ended;
			}

				
			if((ended || force_ended) && remove_on_ended)
			{
				 snd_mngr->rm_array[snd_mngr->rm_counter] = snd_mngr->snd_hash.keys[snd_index];
				 snd_mngr->rm_counter++;
			}

			if((ended && !snd_tmp->loop) || force_ended)
			{
				snd_index++;
				continue;
			}

			//position the sound sample
			if(snd_tmp->positionning)
			{
				Vector_t direction_vec = Vec_minus(snd_tmp->game_position,snd_mngr->center_point);
				wfloat distance = Vec_normalise(&direction_vec);

				float volume_from_falloff = (1.0-(distance / snd_tmp->falloffdistance));

				if(volume_from_falloff < 0.0f)
					volume_from_falloff = 0.0f;

				left_channel_tweak = volume_from_falloff;
				right_channel_tweak = volume_from_falloff;

				Vector_t direction_vec2 = Vec_minus(snd_tmp->game_position,snd_mngr->center_point);
				direction_vec2.y = 0;
				wfloat distance2 = Vec_normalise(&direction_vec2);

				float pan_value = (float)((snd_mngr->center_point.x - distance2) / snd_mngr->center_point.x);

				if(pan_value < 0.0f)
					pan_value = 0.0f;


				if(direction_vec.x > 0)
					left_channel_tweak *= pan_value;
				else if(direction_vec.x < 0)
					right_channel_tweak *= pan_value;
				else
				{
					left_channel_tweak *= pan_value;
					right_channel_tweak *= pan_value;
				}
			
			}


			//no_more_sample = wfalse;

			//read sound value from file
			while (thisSize > 0)
			{
 
				// seek to our current file position 
				sound_seek(snd_tmp->sample, snd_tmp->position);
                      

				// are we going to read past the end of the file?
				if (thisSize > sound_getframes_left(snd_tmp->sample,snd_tmp->position))
				{
				  //if we are,  first read to the end of the file then read from the beginning
				  thisRead = sound_getframes_left(snd_tmp->sample,snd_tmp->position);


				  if(snd_tmp->loop)
				  {
					  snd_tmp->position = sound_getlooppoint(snd_tmp->sample); //loop to the beginning of the file (or the loop point) to properly fill the buffer

					   //first read to end of file
					  sound_size_t prev_read = sound_read_float(snd_tmp->sample,&sample_buffer[0], thisRead);
					  thisRead = thisSize - thisRead;
					  //then read from the beginning (if needed)
					  if(thisRead > 0)
					  {
						  //seek from beginnning
						   sound_seek(snd_tmp->sample, snd_tmp->position);
						   //fill remaining bytes
						   //sf_readf_short(snd_tmp->sample->sndfile,&sample_buffer[(prev_read * snd_mngr->num_channels)], thisRead);
						   sound_read_float(snd_tmp->sample,&sample_buffer[(prev_read * snd_mngr->num_channels)], thisRead);
						   snd_tmp->position += thisRead;
					  }

					  //put the final read value back
					  thisRead = thisSize;
				  }
				  else
				  {
					  snd_tmp->ended = 1;
					
					  ended = 1;
					 // sf_readf_short(snd_tmp->sample->sndfile,&sample_buffer[0], thisRead);
					   sound_read_float(snd_tmp->sample,&sample_buffer[0], thisRead);
					   
					 snd_tmp->position += thisRead;
					 // break;//we reached the end of the file, and we don't loop this sample
				  }

				 
				}
				else
				{
				  //otherwise, we'll just fill up the rest of the output buffer 
				  thisRead = thisSize;
				  // and increment the file position
				  snd_tmp->position += thisRead;

				  //sf_readf_int take the number of frame in input , but returns  number of frames * number of channels items
				  //sf_readf_short(snd_tmp->sample->sndfile,&sample_buffer[0], thisRead);
				  sound_read_float(snd_tmp->sample,&sample_buffer[0], thisRead);
				}

				

				
				//mix with existing values
				double res_mix = 0;
				float res_result = 0;

				//even mixi value = left channel
				//odd mixi value = right channel
				//channel order: left / right / left / right / etc.

				for(int mixi = 0; mixi < (thisRead * snd_mngr->num_channels); mixi++)
				{
					float volume_value = 1.0f;

					if(snd_tmp->sound_type == MUSIC_TYPE)
						volume_value = snd_mngr->music_volume;
					else if(snd_tmp->sound_type == SFX_TYPE)
						volume_value = snd_mngr->sfx_volume;
					else
						volume_value = snd_mngr->music_volume;

					if(snd_tmp->positionning)
					{
						float tweak_value = 1.0f;

						if((mixi & 1) == 0)
							tweak_value = left_channel_tweak;
						else
							tweak_value = right_channel_tweak;

						sample_buffer[mixi] = (float)(sample_buffer[mixi] *  tweak_value);
					}


					//INT_MAX
					res_mix = (double)(snd_mngr->snd_buffer[mixi] + (sample_buffer[mixi] * volume_value));

					if(res_mix > INT_MAX)
						res_result = (float)INT_MAX;
					else if(res_mix < -INT_MAX)
						res_result = (float)-INT_MAX;
					else
						res_result = (float)res_mix;


					snd_mngr->snd_buffer[mixi] = res_result;
				}

				
				//decrement the number of samples left to process
				thisSize -= thisRead;

				if(ended)
					break;
			}

			//rewind cursor
			thisSize = (sound_size_t)((_length / sizeof(float)) / snd_mngr->num_channels);

		}

		snd_index++;
	}
	

	//copy content of our mixed buffer into the ouput
	for(int tot = 0; tot < (thisSize * snd_mngr->num_channels);tot++)
		*cursor++ = snd_mngr->snd_buffer[tot];


	for(int r = 0; r < snd_mngr->rm_counter;r++)
	{
		int pos = hashtable_index(&snd_mngr->snd_hash,snd_mngr->rm_array[r]);
		hashtable_removekey(&snd_mngr->snd_hash,snd_mngr->rm_array[r]);
		wf_free(snd_mngr->rm_array[r]);
		snd_mngr->rm_array[r] = NULL;
		memset(&snd_mngr->snd_array[pos],0,sizeof(sound_t));
	}

	snd_mngr->rm_counter = 0;
}


void snd_mngr_init(snd_mngr_t* snd_mngr,int audio_buffersize,float suggestedLatency)
{
	//init SDL

   snd_mngr->snd_hash.keys = (char**) wf_malloc(MAX_SOUND_SAMPLE * sizeof (char*));

	//initialise sound keys array
	for(int32_t ent_indx = 0; ent_indx < MAX_SOUND_SAMPLE;ent_indx++)
		 snd_mngr->snd_hash.keys[ent_indx] = 0;

	snd_mngr->snd_hash.max_elem = MAX_SOUND_SAMPLE;
	snd_mngr->snd_hash.num_elem = 0;
	snd_mngr->rm_counter = 0;

	snd_mngr->add_counter = 0;

   snd_mngr->audio_buffersize = audio_buffersize;
   snd_mngr->stream_opened = wfalse;
   snd_mngr->music_volume = snd_mngr->sfx_volume = 1.0f;
   snd_mngr->game_space_info.position.x = 0;
   snd_mngr->game_space_info.position.y = 0;
   snd_mngr->game_space_info.width = 100;
   snd_mngr->game_space_info.height = 100;
   snd_mngr->center_point.x = 50;
   snd_mngr->center_point.y = 50;
   snd_mngr->mapid_can_read = 1;

   memset(snd_mngr->current_map_id,0,128);


   if(!snd_mngr->stream_opened)
	{

		logprint("[AUDIO] Using audio driver %s",SDL_GetCurrentAudioDriver());

		//get native webaudio frequency
		 EM_ASM({
				if(typeof(SDL2) === 'undefined')
				SDL2 = {};
				if(typeof(SDL2.audio) === 'undefined')
				SDL2.audio = {};
				if (!SDL2.audioContext) {
				if (typeof(AudioContext) !== 'undefined') {
				SDL2.audioContext = new AudioContext();
				} else if (typeof(webkitAudioContext) !== 'undefined') {
				SDL2.audioContext = new webkitAudioContext();
				} else {
				throw 'Web Audio API is not available!';
				}
				}
				});


		int sampleRate = EM_ASM_INT_V({
				return SDL2.audioContext['sampleRate'];
				});


		SDL_AudioSpec desiredSpec;


		if(sampleRate % AUDIO_SAMPLE_RATE == 0)
		{
			desiredSpec.freq = AUDIO_SAMPLE_RATE;
			 snd_mngr->fallback_to_44100 = wfalse;
		}
		else if(sampleRate % 44100 == 0)
		{
			desiredSpec.freq = 44100;
			snd_mngr->fallback_to_44100 = wtrue;
			logprint("[AUDIO] fallback to 44100 freq");
		}
		else
		{
			 logprint("[AUDIO] No support for device with frequency of %d, aborting...",sampleRate);
			 return;
		}

	   desiredSpec.format = AUDIO_F32LSB;
	   desiredSpec.channels = AUDIO_CHANNELS_NUM;
	   desiredSpec.samples = snd_mngr->audio_buffersize / AUDIO_CHANNELS_NUM; // This is samples per channel.
	   desiredSpec.callback = sdl_audio_callback;
	   desiredSpec.userdata = snd_mngr;

	   SDL_AudioSpec obtainedSpec;
	   SDL_AudioDeviceID dev = SDL_OpenAudioDevice(NULL, 0, &desiredSpec, &obtainedSpec, 0);//don't allow change, force conversion if the native freq is not the exact requested freq

	   if(dev == 0)
	   {
		   logprint("[AUDIO] Couldn't open audio device: %s", SDL_GetError());
		   return;
	   }

	    snd_mngr->audio_device = dev;

		logprint("[AUDIO] SDL Audio: Start Stream");

		snd_mngr->num_channels = obtainedSpec.channels;
		snd_mngr->stream_opened = wtrue;
       
		SDL_PauseAudioDevice(snd_mngr->audio_device, 0);
	}



}

void snd_mngr_rewind_sound(snd_mngr_t* snd_mngr,const char* sample_key)
{
	//SDL_LockAudioDevice(snd_mngr->audio_device);
		if(hashtable_haskey(&snd_mngr->snd_hash,sample_key))
		{
			int32_t indx = hashtable_index(&snd_mngr->snd_hash,sample_key);
			snd_mngr->snd_array[indx].rewind = 1;
		}
	//SDL_UnlockAudioDevice(snd_mngr->audio_device);
}

void snd_mngr_setmapid(snd_mngr_t* const snd_mngr,const char* new_mapid)
{
	atomic_set_value(&snd_mngr->mapid_can_read,0);
	w_strcpy(snd_mngr->current_map_id,128,new_mapid);
	atomic_set_value(&snd_mngr->mapid_can_read,1);
}

void snd_mngr_update_position(snd_mngr_t* const snd_mngr,const char* sound_key,Vector_t new_pos)
{
	//SDL_LockAudioDevice(snd_mngr->audio_device);

	if(hashtable_haskey(&snd_mngr->snd_hash,sound_key))
	{
		int32_t indx = hashtable_index(&snd_mngr->snd_hash,sound_key);

		snd_mngr->snd_array[indx].game_position.x = new_pos.x;
		snd_mngr->snd_array[indx].game_position.y = new_pos.y;
	}

	//SDL_UnlockAudioDevice(snd_mngr->audio_device);
}

void snd_mngr_update_falloff(snd_mngr_t* const snd_mngr,const char* sound_key,float newfalloff)
{
	if(hashtable_haskey(&snd_mngr->snd_hash,sound_key))
	{
		int32_t indx = hashtable_index(&snd_mngr->snd_hash,sound_key);

		snd_mngr->snd_array[indx].falloffdistance = newfalloff;
	}
}

void snd_mngr_update_gamespace(snd_mngr_t* const snd_mngr,const Rect_t new_gamespace,const Vector_t center_point)
{
	//SDL_LockAudioDevice(snd_mngr->audio_device);
	snd_mngr->game_space_info.position.x = new_gamespace.position.x;
	snd_mngr->game_space_info.position.y = new_gamespace.position.y;
	snd_mngr->game_space_info.width = new_gamespace.width;
	snd_mngr->game_space_info.height = new_gamespace.height;

	snd_mngr->center_point.x = center_point.x;
	snd_mngr->center_point.y = center_point.y;

	//SDL_UnlockAudioDevice(snd_mngr->audio_device);
}


wbool snd_mngr_has_sound(snd_mngr_t* const snd_mngr,const char* sample_key)
{
	SDL_LockAudioDevice(snd_mngr->audio_device);

	if(hashtable_haskey(&snd_mngr->snd_hash,sample_key))
	{
		SDL_UnlockAudioDevice(snd_mngr->audio_device);
		return wtrue;
	}

	uint32_t add_counter = snd_mngr->add_counter;

	//we check our add array to see if we have any sound to add
	for(uint32_t a = 0; a < add_counter;a++)
	{
		if(strcmp(snd_mngr->add_array[a],sample_key) == 0)
		{
			SDL_UnlockAudioDevice(snd_mngr->audio_device);
			return wtrue;
		}
	}

	SDL_UnlockAudioDevice(snd_mngr->audio_device);

	return wfalse;
}

void snd_mngr_setvolume(snd_mngr_t* const snd_mngr,int16_t music_volume,int16_t sfx_volume)
{
	//SDL_LockAudioDevice(snd_mngr->audio_device);
	if(music_volume != -1)
	{
		if(music_volume > 10)
			snd_mngr->music_volume = 1.0f;
		else if(music_volume < 0)
			snd_mngr->music_volume = 0.0f;
		else
			snd_mngr->music_volume = (float)music_volume / 10;
	}

	if(sfx_volume != -1)
	{

		if(sfx_volume > 10)
			snd_mngr->sfx_volume = 1.0f;
		else if(sfx_volume < 0)
			snd_mngr->sfx_volume = 0.0f;
		else
			snd_mngr->sfx_volume = (float)sfx_volume / 10;
	}

	//SDL_UnlockAudioDevice(snd_mngr->audio_device);
}


void snd_mngr_playsound(snd_mngr_t* const snd_mngr,sound_sample_t* const snd_to_play,wbool loop,const char* sample_key,wbool remove_on_ended,wbool createonly,int16_t sound_type,Vector_t game_position,wbool positionning,float falloffdistance,const char* map_id)
{
	if(snd_mngr->stream_opened)
		SDL_LockAudioDevice(snd_mngr->audio_device);

	if(snd_mngr->snd_hash.num_elem + snd_mngr->add_counter  >= MAX_SOUND_SAMPLE)
	{
		logprint("can't play another sound sample, max number of %i reached!",MAX_SOUND_SAMPLE);

		if(snd_mngr->stream_opened)
			SDL_UnlockAudioDevice(snd_mngr->audio_device);

		return;
	}

	if(hashtable_haskey(&snd_mngr->snd_hash,sample_key))
	{
		logprint("A sound sample with the key %s already exist!",sample_key);

		if(snd_mngr->stream_opened)
			SDL_UnlockAudioDevice(snd_mngr->audio_device);

		return;
	}

	char* sample_id = (char*)wf_malloc((strlen(sample_key)+1) * sizeof(char));
	strcpy(sample_id,sample_key);


	uint32_t add_counter = snd_mngr->add_counter;

	//create a sample object
	snd_mngr->add_array[add_counter] = sample_id;
	snd_mngr->snd_add_array[add_counter].sample = snd_to_play;
	snd_mngr->snd_add_array[add_counter].position = 0;
	snd_mngr->snd_add_array[add_counter].ended = createonly;
	snd_mngr->snd_add_array[add_counter].force_ended = createonly;
	snd_mngr->snd_add_array[add_counter].loop = loop;
	snd_mngr->snd_add_array[add_counter].remove_on_ended = remove_on_ended;
	snd_mngr->snd_add_array[add_counter].sound_type = sound_type;
	snd_mngr->snd_add_array[add_counter].game_position = game_position;
	snd_mngr->snd_add_array[add_counter].positionning = positionning;
	snd_mngr->snd_add_array[add_counter].falloffdistance = falloffdistance;

	if(map_id != NULL)
		w_strcpy(snd_mngr->snd_add_array[add_counter].map_id,128,map_id);
	else
		memset(snd_mngr->snd_add_array[add_counter].map_id,0,128);

	snd_mngr->add_counter++;

	if(snd_mngr->stream_opened)
		SDL_UnlockAudioDevice(snd_mngr->audio_device);
}

void snd_mngr_stopsound(snd_mngr_t* const snd_mngr,const char* sample_key)
{
	SDL_LockAudioDevice(snd_mngr->audio_device);

	if(!hashtable_haskey(&snd_mngr->snd_hash,sample_key))
	{
		uint32_t add_counter = snd_mngr->add_counter;

		wbool has_sound = wfalse;
		//we check our add array to see if we have any sound to add
		for(uint32_t a = 0; a < add_counter;a++)
		{
			if(strcmp(snd_mngr->add_array[a],sample_key) == 0)
			{
				snd_mngr->snd_add_array[a].force_ended = 1;
				has_sound = wtrue;
				break;
			}
		}

		if(!has_sound)
			logprint("No sound with the key %s",sample_key);

		SDL_UnlockAudioDevice(snd_mngr->audio_device);
		return;
	}

	int snd_indx = hashtable_index(&snd_mngr->snd_hash,sample_key);

	sound_t* snd_tmp = &snd_mngr->snd_array[snd_indx];
	snd_tmp->force_ended = 1;
	snd_tmp->rewind = 0;

	SDL_UnlockAudioDevice(snd_mngr->audio_device);
}

void snd_mngr_removesound(snd_mngr_t* const snd_mngr,const char* sample_key)
{
	SDL_LockAudioDevice(snd_mngr->audio_device);
	if(!hashtable_haskey(&snd_mngr->snd_hash,sample_key))
	{
		uint32_t add_counter = snd_mngr->add_counter;

		wbool has_sound = wfalse;
		//we check our add array to see if we have any sound to add
		for(uint32_t a = 0; a < add_counter;a++)
		{
			if(strcmp(snd_mngr->add_array[a],sample_key) == 0)
			{
				snd_mngr->snd_add_array[a].remove_on_ended = 1;
				snd_mngr->snd_add_array[a].force_ended = 1;
				has_sound = wtrue;
				break;
			}
		}

		if(!has_sound)
			logprint("No sound with the key %s",sample_key);

		SDL_UnlockAudioDevice(snd_mngr->audio_device);
		return;
	}

	int snd_indx = hashtable_index(&snd_mngr->snd_hash,sample_key);

	sound_t* snd_tmp = &snd_mngr->snd_array[snd_indx];

	snd_tmp->remove_on_ended = 1;
	snd_tmp->force_ended = 1;
	snd_tmp->rewind = 0;

	SDL_UnlockAudioDevice(snd_mngr->audio_device);
}

wbool snd_mngr_isplaying(snd_mngr_t* const snd_mngr,const char* sample_key)
{
	SDL_LockAudioDevice(snd_mngr->audio_device);

	if(!hashtable_haskey(&snd_mngr->snd_hash,sample_key))
	{
		SDL_UnlockAudioDevice(snd_mngr->audio_device);
		return wfalse;
	}

	int snd_indx = hashtable_index(&snd_mngr->snd_hash,sample_key);

	sound_t* snd_tmp = &snd_mngr->snd_array[snd_indx];

	uint32_t _ended = snd_tmp->ended;
	uint32_t _force_ended = snd_tmp->force_ended;

	SDL_UnlockAudioDevice(snd_mngr->audio_device);
	return (wbool)!(_ended || _force_ended);
}

void snd_mngr_stopallsounds(snd_mngr_t* const snd_mngr,wbool only_with_positioning)
{
	SDL_LockAudioDevice(snd_mngr->audio_device);
	int snd_indx = 0;
	int snd_count = 0;

	while(snd_count <  snd_mngr->snd_hash.num_elem)
	{
		if(snd_mngr->snd_hash.keys[snd_indx])
		{
			sound_t* snd_tmp = &snd_mngr->snd_array[snd_indx];

			if((snd_tmp->positionning == wtrue && only_with_positioning == wtrue) || (only_with_positioning == wfalse))
				snd_tmp->force_ended = 1;

			snd_count++;
		}

		snd_indx++;
	}
	SDL_UnlockAudioDevice(snd_mngr->audio_device);
}

void snd_mngr_start(snd_mngr_t* snd_mngr)
{
	logprint("No way to start the sound manager manually under emscripten, use snd_mngr_free() and snd_mngr_init() to stop / start the stream");
}

void snd_mngr_stop(snd_mngr_t* snd_mngr)
{
	if(snd_mngr->stream_opened)
	{
		SDL_CloseAudioDevice(snd_mngr->audio_device);
		snd_mngr->stream_opened = wfalse;

		return;
	}

}


void snd_mngr_free(snd_mngr_t* snd_mngr)
{
	if(snd_mngr->stream_opened)
	{
		SDL_CloseAudioDevice(snd_mngr->audio_device);
		snd_mngr->stream_opened = wfalse;
	}

	 //cleanup all variables
	 wf_free(snd_mngr->snd_hash.keys);
	 snd_mngr->snd_hash.keys = NULL;
	 snd_mngr->snd_hash.num_elem = 0;
	 snd_mngr->snd_hash.max_elem = 0;
}

#endif

