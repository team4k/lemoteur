#include <inttypes.h>
#include <time.h>

#if defined(_WIN32)
    #include <Windows.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <Engine.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/geom.h>

#include <limits.h>

#include <Debug/Logprint.h>

//use sndfile in most use case, only ios use a specific code path
#if !defined(NO_SNDFILE)
	#include <sndfile.h>
#elif defined (IOS_BUILD)
	#include <vorbis/vorbisfile.h>
#endif

#include <Sound/Sound.h>

#if !defined(NO_SNDFILE)
struct _internal_sound_sample
{
	SNDFILE* sndfile;
	SF_INFO sndinfo;
    void* data_buffer;
	sound_size_t loop_point;
};
#elif defined(IOS_BUILD)


struct _internal_sound_sample
{
	sound_size_t loop_point;
	wbool is_ogg;
	
	//wav file variables
	ExtAudioFileRef eaf;
	AudioStreamBasicDescription format;
	SInt64 wav_framescount;

	//ogg file variables
	OggVorbis_File mVorbisFile;
	vorbis_info* ovInfo;


};
#endif

typedef struct
{
  unsigned char* start_ptr;
  unsigned char* ptr;
  size_t size;
} memory_buffer_t;

#if !defined(NO_SNDFILE)
//virtual IO for libsndfile
sf_count_t membuffer_getlen(void* userdata)
{
     memory_buffer_t* tmp_buff = (memory_buffer_t*)userdata;
	
     return tmp_buff->size;
}

sf_count_t membuffer_read(void* ptr, sf_count_t count,void* userdata)
{
     memory_buffer_t* tmp_buff = (memory_buffer_t*)userdata;
     
     sf_count_t pos = (sf_count_t)(tmp_buff->ptr - tmp_buff->start_ptr);

     if(pos + count >= tmp_buff->size)
          count = tmp_buff->size - pos;

     memcpy(ptr,tmp_buff->ptr,(size_t)count);
     
     tmp_buff->ptr += count;

     return count;
}

sf_count_t membuffer_seek(sf_count_t offset,int whence, void* userdata)
{
     memory_buffer_t* tmp_buff = (memory_buffer_t*)userdata;
     
     sf_count_t pos = 0;

     switch(whence)
     {
          case SEEK_SET:
               pos = offset;
          break;

          case SEEK_CUR:
               pos = (sf_count_t)(tmp_buff->ptr - tmp_buff->start_ptr) + offset;
          break;

          case SEEK_END:
               pos = tmp_buff->size - offset;
          break;

          default:
               pos = 0;
          break;
     }    


     if(pos >= tmp_buff->size)
          pos = tmp_buff->size - 1;
     else if(pos < 0)
          pos = 0;

     tmp_buff->ptr = tmp_buff->start_ptr + pos;

     return pos; 
}

sf_count_t membuffer_tell(void* userdata)
{
     memory_buffer_t* tmp_buff = (memory_buffer_t*)userdata;

     return (sf_count_t)(tmp_buff->ptr - tmp_buff->start_ptr);
}

sf_count_t membuffer_write(const void* ptr, sf_count_t count, void* user_data)
{
     logprint("write to buffer is not implemented for this libsndfile virtual IO");
     return 0;
}

static SF_VIRTUAL_IO sf_vio;
static wbool vio_init = wfalse;

#endif


sound_sample_t* alloc_sound_struct()
{
	return (sound_sample_t*)wf_malloc(sizeof(sound_sample_t));
}

void sound_setlooppoint(sound_sample_t* const ptr,sound_size_t point)
{
	ptr->loop_point = point;
}

void sound_seek(sound_sample_t* const ptr,sound_size_t position)
{
	#if !defined(NO_SNDFILE)
		sf_seek(ptr->sndfile, position, SEEK_SET);
	#elif defined(IOS_BUILD)
		if(ptr->is_ogg)	
			ov_pcm_seek(&ptr->mVorbisFile, position);
		else
			ExtAudioFileSeek(ptr->eaf,position);
	#endif
}

sound_size_t sound_getframes_left(sound_sample_t* const ptr,sound_size_t position)
{
	#if !defined(NO_SNDFILE)
		return ptr->sndinfo.frames - position;
	#elif defined(IOS_BUILD)
		if(ptr->is_ogg)
			return ptr->mVorbisFile.end - position;
		else
			return ptr->wav_framescount - position;
	#endif
}

sound_size_t sound_getlooppoint(sound_sample_t* const ptr)
{
	return ptr->loop_point;
}

sound_size_t sound_read_float(sound_sample_t* const ptr,float* buffer,sound_size_t read_len)
{
    return sf_readf_float(ptr->sndfile,buffer,read_len);
}

sound_size_t sound_read_short(sound_sample_t* const ptr,int16_t* buffer,sound_size_t read_len)
{
	#if !defined(NO_SNDFILE)
		return sf_readf_short(ptr->sndfile,buffer, read_len);
	#elif defined(IOS_BUILD)
		if(ptr->is_ogg)
		{
			UInt32	currentSection = 0;
			UInt32		framesRemaining		= read_len;
		    UInt32		totalFramesRead		= 0;
			int16_t** temp_buffer = NULL;
			sound_size_t decoded = 0;

			while(framesRemaining > 0) 
			{
				// Decode a chunk of samples from the file

				int frame_to_read = min(2048,framesRemaining);

				long framesRead = ov_read(&ptr->mVorbisFile, &temp_buffer, frame_to_read * sizeof(int16_t) * ptr->format.mChannelsPerFrame,0,2,1,&currentSection);
			
				if(framesRead < 0) {
					logprint("Error decoding ogg vorbis!");
					return 0;
				}
		
				// 0 frames indicates EOS
				if(framesRead == 0)
					break;
		
				// Copy the frames from the decoding buffer to the output buffer
				for(UInt32 channel = 0; channel < ptr->format.mChannelsPerFrame; ++channel) {
					// Skip over any frames already decoded
					memcpy(buffer + totalFramesRead, temp_buffer[channel], (size_t)framesRead * sizeof(int16_t));
					//decoded += (size_t)framesRead * sizeof(int16_t);
				}
		
				totalFramesRead += (UInt32)framesRead;
				framesRemaining -= (UInt32)framesRead;
			}

			return totalFramesRead * ptr->format.mChannelsPerFrame;

		}
		else
		{
			ExtAudioFileRead(ptr->eaf,&read_len,buffer);
			return read_len;
		}
	#endif
}



void sound_close(sound_sample_t* const ptr)
{
	#if !defined(NO_SNDFILE)
		sf_close(ptr->sndfile);
	#elif defined(IOS_BUILD)

	if(ptr->is_ogg)
	{
		if(ov_clear(&ptr->mVorbisFile) != 0)
			logprint("Error while closing ogg file!");
	}
	else
	{
		ExtAudioFileDispose(ptr->eaf);
	}

	#endif
}

void free_sound_struct(sound_sample_t* ptr)
{
	if(ptr->data_buffer != NULL)
    {
        wf_free(((memory_buffer_t*)ptr->data_buffer)->start_ptr);//freeing sound file content
        wf_free((memory_buffer_t*)ptr->data_buffer);//freeing memory buffer struct
    }     

	wf_free(ptr);
}

wbool sound_initfrombuffer(sound_sample_t* const ptr,unsigned char* buffer,size_t buffer_size)
{
	#if !defined(NO_SNDFILE)
		if(!vio_init)
		{
			//to access sound file from memory buffer, initialize a virtual IO interface for libsndfile
			sf_vio.get_filelen = membuffer_getlen;
			sf_vio.read = membuffer_read;
			sf_vio.seek = membuffer_seek;
			sf_vio.tell = membuffer_tell;
			sf_vio.write = membuffer_write;
			vio_init = wtrue;
		}

		memory_buffer_t* virtual_file = (memory_buffer_t*)wf_malloc(sizeof(memory_buffer_t));
		virtual_file->start_ptr = buffer;
		virtual_file->ptr = buffer;
		virtual_file->size = buffer_size;

		memset (&ptr->sndinfo, 0, sizeof (SF_INFO)) ;

		ptr->sndfile = sf_open_virtual(&sf_vio,SFM_READ,&ptr->sndinfo,virtual_file);
		ptr->data_buffer = virtual_file;

		if(!ptr->sndfile)
		{
			logprint("Error opening sndfile! %s",sf_strerror(NULL));
			return wfalse;
		}

		return wtrue;

	#else
		logprint("not implemented!");
		return wfalse;
	#endif
}

wbool sound_initfromfile(sound_sample_t* const ptr,const char* path)
{
	#if !defined(NO_SNDFILE)
		memset (&ptr->sndinfo, 0, sizeof (SF_INFO)) ;

		ptr->sndfile = sf_open(path,SFM_READ,&ptr->sndinfo);
		ptr->data_buffer = NULL;

		if(!ptr->sndfile)
		{
			logprint("Error opening sndfile! %s",sf_strerror(NULL));
			return wfalse;
		}

		return wtrue;

	#elif defined(IOS_BUILD)

		//handle wav files
		if(str_end_with(".wav",path))
		{
			CFURLRef url = path;
		
			OSStatus err = ExtAudioFileOpenURL((CFURLRef)url, &ptr->eaf);

			if(noErr != err)
			{
				logprint("Error while opening audio file %s",err);
				return wfalse;
			}

			/*ptr->format.mSampleRate = 44100;
			ptr->format.mFormatID = kAudioFormatLinearPCM;
			ptr->format.mFormatFlags = kAudioFormatFormatFlagIsPacked;
			ptr->format.mBitsPerChannel = 16;
			ptr->format.mChannelsPerFrame = 2;
			ptr->format.mBytesPerFrame = ptr->format.mChannelsPerFrame * 2;
			ptr->format.mFramesPerPacket = 1;
			ptr->format.mBytesPerPacket = ptr->format.mFramesPerPacket * ptr->format.mBytesPerFrame;*/

			ExtAudioFileSetProperty(ptr->eaf, kExtAudioFileProperty_ClientDataFormat, sizeof(ptr->format), &ptr->format);

			ExtAudioFileSetProperty(ptr->eaf, kExtAudioFileProperty_FileLengthFrames, sizeof(ptr->wav_framescount), &ptr->wav_framescount);

			

			ptr->is_ogg = wfalse;

			return wtrue;

			/* Read the file contents using ExtAudioFileRead */
		}
		else if(str_end_with(".ogg",path))//handle ogg files
		{
			ptr->is_ogg = wtrue;
			ptr->ovInfo = NULL;

			/*ov_callbacks callbacks = {
				.read_func = read_func_callback,
				.seek_func = seek_func_callback,
				.tell_func = tell_func_callback,
				.close_func = NULL
			};

			if(ov_test_callbacks(&ptr, &ptr->mVorbisFile, NULL, 0, callbacks) != 0) 
			{

				logprint("The audio file %s is not a valida OggVorbis file",path);
				return wfalse;
			}*/
	
			if(ov_test_open(&ptr->mVorbisFile) != 0) 
			{
				logprint("Failed testing ogg audio file opening %s",path);

				if(ov_clear(&ptr->mVorbisFile) !=0)
					logprint("Failed clearing ogg audio file %s",path);
		
				return wfalse;
			}
	
			ptr->ovInfo = ov_info(&ptr->mVorbisFile, -1);

			if(ptr->ovInfo == NULL) 
			{
				logprint("Failed retrieving ogg audio file info! %s",path);

				if(ov_clear(&ptr->mVorbisFile) != 0)
					logprint("Failed clearing ogg audio file %s",path);
		
				return wfalse;
			}
	
			// Canonical Core Audio format
			/*ptr->format.mFormatID			= kAudioFormatLinearPCM;
			ptr->format.mFormatFlags		= kAudioFormatFlagsNativeFloatPacked | kAudioFormatFlagIsNonInterleaved;
	
			ptr->format.mBitsPerChannel		= 8 * sizeof(float);
			ptr->format.mSampleRate			= ovInfo->rate;
			ptr->format.mChannelsPerFrame	= (UInt32)ovInfo->channels;
	
			ptr->format.mBytesPerPacket		= (mFormat.mBitsPerChannel / 8);
			ptr->format.mFramesPerPacket	= 1;
			ptr->format.mBytesPerFrame		= mFormat.mBytesPerPacket * mFormat.mFramesPerPacket;
	
			ptr->format.mReserved			= 0;*/
	
			// Set up the source format
			ptr->format.mFormatID				= 'VORB';
	
			ptr->format.mSampleRate			= ovInfo->rate;
			ptr->format.mChannelsPerFrame		= (UInt32)ovInfo->channels;
	
			/*switch(ovInfo->channels) {
					// Default channel layouts from Vorbis I specification section 4.3.9
					// http://www.xiph.org/vorbis/doc/Vorbis_I_spec.html#x1-800004.3.9

				case 1:		mChannelLayout = ChannelLayout::ChannelLayoutWithTag(kAudioChannelLayoutTag_Mono);			break;
				case 2:		mChannelLayout = ChannelLayout::ChannelLayoutWithTag(kAudioChannelLayoutTag_Stereo);		break;
				case 3:		mChannelLayout = ChannelLayout::ChannelLayoutWithTag(kAudioChannelLayoutTag_AC3_3_0);		break;
				case 4:		mChannelLayout = ChannelLayout::ChannelLayoutWithTag(kAudioChannelLayoutTag_Quadraphonic);	break;
				case 5:		mChannelLayout = ChannelLayout::ChannelLayoutWithTag(kAudioChannelLayoutTag_MPEG_5_0_C);	break;
				case 6:		mChannelLayout = ChannelLayout::ChannelLayoutWithTag(kAudioChannelLayoutTag_MPEG_5_1_C);	break;

				case 7:
					mChannelLayout = ChannelLayout::ChannelLayoutWithChannelLabels({
						kAudioChannelLabel_Left, kAudioChannelLabel_Center, kAudioChannelLabel_Right,
						kAudioChannelLabel_LeftSurround, kAudioChannelLabel_RightSurround, kAudioChannelLabel_CenterSurround,
						kAudioChannelLabel_LFEScreen});
					break;

				case 8:
					mChannelLayout = ChannelLayout::ChannelLayoutWithChannelLabels({
						kAudioChannelLabel_Left, kAudioChannelLabel_Center, kAudioChannelLabel_Right,
						kAudioChannelLabel_LeftSurround, kAudioChannelLabel_RightSurround, kAudioChannelLabel_RearSurroundLeft, kAudioChannelLabel_RearSurroundRight,
						kAudioChannelLabel_LFEScreen});
					break;
			}*/

			return wtrue;
		}
		else
		{
			logprint("Audio file type not handled! %s",path);
			return wfalse;
		}

	#endif

}

