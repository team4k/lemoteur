#ifdef _MSC_VER
extern "C" {
#endif
 
#include "lauxlib.h"
#include "lualib.h"
#include "lua.h"
    
#ifdef _MSC_VER    
}
#endif

#include <stdio.h>
#include <inttypes.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include <Base/types.h>

#include <Render.h>

#include <Engine.h>

#include <Base/functions.h>
#include <Base/geom.h>
#include <Base/Color.h>

#include <Debug/Logprint.h>

#include <Utils/Hash.h>

#include <Render/render_manager.h>
#include <Scripting/ScriptEngine.h>

#include <GameObjects/Level.pb-c.h>
#include <GameObjects/Characters.pb-c.h>
#include <GameObjects/Shaders.pb-c.h>

#include <Sound/Sound.h>

#include <Graphics/TextureRegion.h>
#include <Graphics/TextureLoader.h>
#include <Graphics/FontLoader.h>
#include <Resx/shader_loader.h>

#include <Resx/Resources.h>

#include <Config/config.h>

void config_init(config_t* const config,lua_State* ref_state)
{
	config->lua_state = ref_state;
}

wbool config_readconf(config_t* const config,void* resx_mngr,const char* config_path)
{

	//load config file

	if(script_loadfile(config->lua_state,config_path) == -1)
	{
		script_execstring(config->lua_state,"config={}",NULL,0);//create an empty config array
		logprint("[config] Can't find config file!");
		return wfalse;
	}

	return wtrue;
}

void config_reload(config_t* const config,const char* config_path)
{
	script_execstring(config->lua_state,"config=nil",NULL,0);//create an empty config array

	 config_readconf(config,NULL,config_path);
}

void config_saveconf(config_t* const config,const char* config_path)
{
	FILE* conf_file = NULL;
	
	w_fopen(conf_file,config_path, "w");

	if(!conf_file)
	{
		logprint("[config] can't open/create config file!");
		return;
	}


	lua_getglobal(config->lua_state,"config");


	lua_pushnil(config->lua_state);

	const char* init_conf = "config = {\n";

	fwrite(init_conf,sizeof(char),strlen(init_conf),conf_file);

	while(lua_next(config->lua_state,-2))
	{
		char conf_line[256];

		const char* key = lua_tostring(config->lua_state,-2);

		if(lua_isnumber(config->lua_state,-1))
		{
			float value = (float)lua_tonumber(config->lua_state,-1);
			w_sprintf(conf_line,256,"%s=%g",key,value);
			
			for(uint32_t ci = 0; ci < strlen(conf_line);ci++)
			{
				if(conf_line[ci] == ',')
					conf_line[ci] = '.';
			}
		}
		else if(lua_isstring(config->lua_state,-1))
		{
			const char* val = lua_tostring(config->lua_state,-1);
			char val_san[256] = {0};
			int32_t vis = 0;

			for(uint32_t vi = 0;vi< strlen(val);vi++)
			{
				val_san[vis++] = val[vi];

				if(val[vi] == '\\')
					val_san[vis++] = '\\';
			}

			val_san[vis] = '\0';


			w_sprintf(conf_line,256,"%s=\"%s\"",key,val_san);
		}
		else if(lua_isboolean(config->lua_state,-1))
		{
			int bres = lua_toboolean(config->lua_state,-1);
			w_sprintf(conf_line,256,"%s=%s",key,(bres)?"true":"false");
		}

		fwrite(conf_line,sizeof(char),strlen(conf_line),conf_file);

		lua_pop(config->lua_state,1);


		const char* endline = ",\n";
		fwrite(endline,sizeof(char),strlen(endline),conf_file);
	}

	const char* endfile = "}";
	fwrite(endfile,sizeof(char),strlen(endfile),conf_file);

	fclose(conf_file);

	lua_settop(config->lua_state,0); //clear lua stack


	//notify configuration changed
	if(config->config_saved != NULL)
		config->config_saved();
}

wbool config_getint(config_t* const config,const char* key,int32_t* ret)
{
	lua_getglobal(config->lua_state,"config");

	lua_pushstring(config->lua_state,key);
	lua_gettable(config->lua_state,-2);


	if(lua_isnil(config->lua_state,-1))
	{
		logprint("[config] No key %s in config!",key);
		lua_settop(config->lua_state,0);
		return wfalse;
	}

	if(!lua_isnumber(config->lua_state,-1))
	{
		logprint("[config] key %s is not a number!",key);
		lua_settop(config->lua_state,0);
		return wfalse;
	}

	(*ret) = (int32_t)lua_tointeger(config->lua_state,-1);

	lua_settop(config->lua_state,0);

	return wtrue;
}

wbool config_getshort(config_t* const config,const char* key,int16_t* ret)
{
	lua_getglobal(config->lua_state,"config");

	lua_pushstring(config->lua_state,key);
	lua_gettable(config->lua_state,-2);


	if(lua_isnil(config->lua_state,-1))
	{
		logprint("[config] No key %s in config!",key);
		lua_settop(config->lua_state,0);
		return wfalse;
	}

	if(!lua_isnumber(config->lua_state,-1))
	{
		logprint("[config] key %s is not a number!",key);
		lua_settop(config->lua_state,0);
		return wfalse;
	}

	(*ret) = (int16_t)lua_tointeger(config->lua_state,-1);

	lua_settop(config->lua_state,0);

	return wtrue;
}

wbool config_setint(config_t* const config,const char* key,int32_t in)
{
	lua_getglobal(config->lua_state,"config");

	lua_pushnumber(config->lua_state,in);

	lua_setfield(config->lua_state,-2,key);

	lua_settop(config->lua_state,0);

	return wtrue;
}

wbool config_setshort(config_t* const config,const char* key,int16_t in)
{
	lua_getglobal(config->lua_state,"config");

	lua_pushnumber(config->lua_state,in);

	lua_setfield(config->lua_state,-2,key);

	lua_settop(config->lua_state,0);

	return wtrue;
}

wbool config_getfloat(config_t* const config,const char* key,float* ret)
{
	lua_getglobal(config->lua_state,"config");

	lua_pushstring(config->lua_state,key);
	lua_gettable(config->lua_state,-2);

	if(lua_isnil(config->lua_state,-1))
	{
		logprint("[config] No key %s in config!",key);
		lua_settop(config->lua_state,0);
		return wfalse;
	}

	if(!lua_isnumber(config->lua_state,-1))
	{
		logprint("[config] key %s is not a number!",key);
		lua_settop(config->lua_state,0);
		return wfalse;
	}

	(*ret) = (float)lua_tonumber(config->lua_state,-1);

	lua_settop(config->lua_state,0);

	return wtrue;
}

wbool config_setfloat(config_t* const config,const char* key,float in)
{
	lua_getglobal(config->lua_state,"config");

	lua_pushnumber(config->lua_state,in);

	lua_setfield(config->lua_state,-2,key);

	lua_settop(config->lua_state,0);

	return wtrue;
}

wbool config_getstring(config_t* const config,const char* key,char* return_string,size_t size_string)
{
	lua_getglobal(config->lua_state,"config");

	lua_pushstring(config->lua_state,key);
	lua_gettable(config->lua_state,-2);

	if(lua_isnil(config->lua_state,-1))
	{
		logprint("[config] No key %s in config!",key);
		lua_settop(config->lua_state,0);
		return wfalse;
	}

	if(!lua_isstring(config->lua_state,-1))
	{
		logprint("[config] key %s is not a string!",key);
		lua_settop(config->lua_state,0);
		return wfalse;
	}

	const char* ret = lua_tostring(config->lua_state,-1);
	w_strcpy(return_string,size_string,ret);

	lua_settop(config->lua_state,0);

	return wtrue;
}

wbool config_setstring(config_t* const config,const char* key, const char* in_str)
{
	lua_getglobal(config->lua_state,"config");

	lua_pushstring(config->lua_state,in_str);

	lua_setfield(config->lua_state,-2,key);

	lua_settop(config->lua_state,0);

	return wtrue;
}

wbool config_getbool(config_t* const config,const char* key,wbool* return_bool)
{
	lua_getglobal(config->lua_state,"config");

	lua_pushstring(config->lua_state,key);
	lua_gettable(config->lua_state,-2);

	if(lua_isnil(config->lua_state,-1))
	{
		logprint("[config] No key %s in config!",key);
		lua_settop(config->lua_state,0);
		return wfalse;
	}

	if(!lua_isboolean(config->lua_state,-1))
	{
		logprint("[config] key %s is not a boolean!",key);
		lua_settop(config->lua_state,0);
		return wfalse;
	}

	(*return_bool) =  (wbool)lua_toboolean(config->lua_state,-1);

	lua_settop(config->lua_state,0);

	return wtrue;
}

wbool config_setbool(config_t* const config, const char* key, wbool in_bool)
{
	lua_getglobal(config->lua_state,"config");

	lua_pushboolean(config->lua_state,(int32_t)in_bool);

	lua_setfield(config->lua_state,-2,key);

	lua_settop(config->lua_state,0);

	return wtrue;
}


