#ifdef _MSC_VER
extern "C" {
#endif
 
#include "lauxlib.h"
#include "lualib.h"
#include "lua.h"
    
#ifdef _MSC_VER    
}
#endif

#include <stdio.h>
#include <inttypes.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include <Base/types.h>

#include <Render.h>

#include <Engine.h>

#include <Base/functions.h>
#include <Base/geom.h>
#include <Base/Color.h>

#include <Debug/Logprint.h>

#include <Utils/Hash.h>

#include <Render/render_manager.h>
#include <Scripting/ScriptEngine.h>

#include <GameObjects/Level.pb-c.h>
#include <GameObjects/Characters.pb-c.h>
#include <GameObjects/Shaders.pb-c.h>

#include <Sound/Sound.h>

#include <Graphics/TextureRegion.h>
#include <Graphics/TextureLoader.h>
#include <Graphics/FontLoader.h>
#include <Resx/shader_loader.h>

#include <Resx/Resources.h>

#include <Config/shader_config.h>

static wbool _shader_config_internal(render_manager_t* const render_mngr,lua_State* const ref_state,resources_manager_t* const resx_mngr,wbool unload)
{
	//remove existing config package
	script_execstring(ref_state,"if package.loaded['shader_config'] ~= nil then  package.loaded['shader_config'] = nil end",NULL,0);

	const char* result = script_execstring(ref_state,"require('shader_config')",NULL,0);

	if(result != NULL)
	{
		logprint("No shader config available");
		return wfalse;
	}

	//read config content and create shaders from here
	lua_getglobal(ref_state,"shader_config");

	lua_Integer prog_count =  luaL_len(ref_state,-1);

	for(int32_t iprog = 1; iprog <= prog_count;iprog++)
	{
		lua_rawgeti(ref_state,-1,iprog);

		if(lua_istable(ref_state,1))
		{
			lua_pushstring(ref_state,"prog");
			lua_gettable(ref_state,-2);

			const char* prog_name = lua_tostring(ref_state,-1);

			lua_pop(ref_state,1);

			lua_pushstring(ref_state,"vertex_shader");
			lua_gettable(ref_state,-2);

			const char* vs_file = lua_tostring(ref_state,-1);

			lua_pop(ref_state,1);

			lua_pushstring(ref_state,"pixel_shader");
			lua_gettable(ref_state,-2);

			const char* ps_file = lua_tostring(ref_state,-1);

			lua_pop(ref_state,2);

			if(!render_has_program(render_mngr,prog_name) && !unload)
			{
				shader_info* vshader_data = getshader_resx(resx_mngr,vs_file,wfalse);
				shader_info* pshader_data = getshader_resx(resx_mngr,ps_file,wtrue);

				if(vshader_data != NULL && pshader_data != NULL)
				{
					if(render_create_program(render_mngr,prog_name,vshader_data->shader_pointer,pshader_data->shader_pointer) != NULL)
						logprint("shader program %s created!",prog_name);
				}
			}
			else if(unload)
			{
				render_delete_program(render_mngr,prog_name);
			}
		}
	}

	lua_settop(ref_state,0);

	return wtrue;
}

wbool shader_config_load(render_manager_t* const render_mngr,lua_State* const ref_state,resources_manager_t* const resx_mngr)
{
	return _shader_config_internal(render_mngr,ref_state,resx_mngr,wfalse);
}

wbool shader_config_unload(render_manager_t* const render_mngr,lua_State* const ref_state,resources_manager_t* const resx_mngr)
{
	return _shader_config_internal(render_mngr,ref_state,resx_mngr,wtrue);
}




