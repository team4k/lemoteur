#include <stdio.h>
#include <wchar.h>
#include <string.h>
#include <inttypes.h>
#include <math.h>

#include <sys/types.h>

#include <Engine.h>
#include <Base/types.h>

#include <Debug/Logprint.h>

#include <time.h>

#include <wchar.h>


#ifdef __APPLE__
	#include <mach-o/dyld.h>
	#include <sys/param.h>
	#include <stdlib.h>
#endif

#if defined(_WIN32)
    #include <Windows.h>
#else
    #include <unistd.h>
    #include <arpa/inet.h>
	#include <pthread.h>
#endif


#include <Base/functions.h>
#include <Threading/thread_func.h>


void thread_cond_init(cond_wait_t* const thread_cond)
{
	#if defined(_WIN32)
		InitializeConditionVariable(&thread_cond->cr_cond);

		#if defined(WINAPI_FAMILY) && (WINAPI_FAMILY == WINAPI_FAMILY_PHONE_APP)
			InitializeCriticalSectionEx(&thread_cond->cr_sec,0,0);
		#else
			InitializeCriticalSection(&thread_cond->cr_sec);
		#endif

		thread_cond->pred = 0;
	#elif defined(EMSCRIPTEN)

	#else
		pthread_cond_init(&thread_cond->wait_cond,NULL);
		pthread_mutex_init(&thread_cond->wait_mutex,NULL);
	#endif
}

void thread_cond_wait(cond_wait_t* const thread_cond)
{
	volatile uint32_t pred = 0;

	#if defined(_WIN32)

	EnterCriticalSection(&thread_cond->cr_sec);

	while(pred == 0)
	{
		SleepConditionVariableCS(&thread_cond->cr_cond,&thread_cond->cr_sec,INFINITE);
		atomic_set_value(&pred,thread_cond->pred);
	}

	LeaveCriticalSection(&thread_cond->cr_sec);

	#elif defined(EMSCRIPTEN)

	#else

		pthread_mutex_lock(&thread_cond->wait_mutex);

		while(pred == 0)
		{
			pthread_cond_wait(&thread_cond->wait_cond, &thread_cond->wait_mutex);
			atomic_set_value(&pred,thread_cond->pred);
		}

		pthread_mutex_unlock(&thread_cond->wait_mutex);
	#endif

	atomic_set_value(&thread_cond->pred,0);
}

void thread_cond_wakeup(cond_wait_t* const thread_cond)
{
	#if defined(_WIN32)
		atomic_set_value(&thread_cond->pred,1);
		WakeConditionVariable(&thread_cond->cr_cond);
	#elif defined(EMSCRIPTEN)

	#else
		pthread_mutex_lock(&thread_cond->wait_mutex);
		atomic_set_value(&thread_cond->pred,1);
		pthread_cond_signal(&thread_cond->wait_cond);
		pthread_mutex_unlock(&thread_cond->wait_mutex);
	#endif
}

void thread_cond_free(cond_wait_t* const thread_cond)
{
	thread_cond->pred = 0;

	#if defined(_WIN32)
		DeleteCriticalSection(&thread_cond->cr_sec);
	#elif defined(EMSCRIPTEN)

	#else
		pthread_mutex_destroy(&thread_cond->wait_mutex);
	#endif
}