#include <stdio.h>
#include <wchar.h>
#include <string.h>
#include <inttypes.h>
#include <math.h>

#include <sys/types.h>

#include <Engine.h>
#include <Base/types.h>

#include <Debug/Logprint.h>

#include <time.h>

#include <wchar.h>


#ifdef __APPLE__
	#include <mach-o/dyld.h>
	#include <sys/param.h>
	#include <stdlib.h>
#endif

#if defined(_WIN32)
    #include <Windows.h>
#else
    #include <unistd.h>
	#include <pthread.h>
#endif

#if defined(EMSCRIPTEN)
	#include <emscripten.h>
#endif


#include <Base/functions.h>

#include <Threading/thread_func.h>
#include <Threading/threads.h>

#ifdef EMSCRIPTEN
static void callback_worker(char *data, int size, void *arg)
{
}
#endif


#if defined(_WIN32)
DWORD WINAPI thread_dowork(void* data) {
#else
void* thread_dowork(void* data) {
#endif
        thread_t* thread_obj = (thread_t*)data;

        thread_cond_init(&thread_obj->thread_cond);
        
        atomic_set_value(&thread_obj->thread_state,THREAD_WORKING);

        #ifndef EMSCRIPTEN
        while(1) {
        #endif

            #ifdef EMSCRIPTEN
            emscripten_call_worker(thread_obj->thread,thread_obj->func_name,thread_obj->worker_data,thread_obj->worker_data_size, callback_worker,thread_obj);
            #else
            thread_obj->func(thread_obj->func_param);
            #endif

            atomic_set_value(&thread_obj->thread_state,THREAD_WORKING);

            #ifdef EMSCRIPTEN
                if(emscripten_get_worker_queue_size(thread_obj->thread) == 0)
                        atomic_set_value(&thread_obj->thread_state,THREAD_WAITING);
            #else
                atomic_set_value(&thread_obj->thread_state,THREAD_WAITING);
            #endif
            
            thread_cond_wait(&thread_obj->thread_cond);//make the thread sleep and wait for the call

#ifndef EMSCRIPTEN
        }
#endif


        return NULL;
}


void thread_create(thread_t* const thread_obj,thread_func_t func,void* func_param,const char* worker_name,const char* func_name)
{
    thread_obj->func = func;
    thread_obj->func_param = func_param;

  #if defined(_WIN32)
            #if defined(WINAPI_FAMILY) && (WINAPI_FAMILY == WINAPI_FAMILY_PHONE_APP)
                    if (thread_obj->thread)
                            delete thread_obj->thread;

                    thread_obj->thread = new std::thread(thread_dowork, thread_obj);

            #else
            if(thread_obj->thread)
                    CloseHandle(thread_obj->thread);

            thread_obj->thread = CreateThread(NULL, 0, thread_dowork, thread_obj, 0, NULL);

            #endif

            if(!thread_obj->thread)
            {
                    logprint("Can't create thread!");
            }
    #elif defined(EMSCRIPTEN)
            if(thread_obj->thread)
                    emscripten_destroy_worker(thread_obj->thread);

            thread_obj->thread = emscripten_create_worker(worker_name);
            w_strcpy(thread_obj->func_name,128, func_name);

    #else
            if(thread_obj->thread)
                    wf_free(thread_obj->thread);

            thread_obj->thread = (pthread_t*)wf_malloc(sizeof(pthread_t));
            memset(thread_obj->thread,0,sizeof(pthread_t));

            if(pthread_create(thread_obj->thread,NULL,thread_dowork,thread_obj) != 0)
            {
                    logprint("Can't create thread!");
            }
    #endif
    
    atomic_set_value(&thread_obj->thread_state,THREAD_NOTSTARTED);
    thread_obj->initialized = wtrue;
}

void thread_run(thread_t* const thread_obj)
{
    volatile uint32_t t_value = 0;

    atomic_set_value(&t_value,thread_obj->thread_state);

    if(t_value == THREAD_WAITING)
    {
        #ifdef EMSCRIPTEN
            atomic_set_value(&thread_obj->thread_state, THREAD_WORKING);
        #else
            thread_cond_wakeup(&thread_obj->thread_cond);
        #endif
    }

}


#ifdef EMSCRIPTEN
void thread_simupdate(thread_t* const thread_obj)
{
    if(thread_obj->thread_state == THREAD_WORKING) {
		thread_dowork(thread_obj);
    }
}
#endif


void thread_free(thread_t* thread_obj)
{
  #if defined(_WIN32)
            #if defined(WINAPI_FAMILY) && (WINAPI_FAMILY == WINAPI_FAMILY_PHONE_APP)
                    if (thread_obj->thread)
                            delete thread_obj->thread;
            #else
                    if(thread_obj->thread)
                            CloseHandle(thread_obj->thread);
            #endif

    #elif defined(EMSCRIPTEN)
            if(thread_obj->thread)
                    emscripten_destroy_worker(thread_obj->thread);

            wf_free(thread_obj->worker_data);

    #else
            if(thread_obj->thread)
                    wf_free(thread_obj->thread);
    #endif

    thread_cond_free(&thread_obj->thread_cond);

    thread_obj->thread = 0;
    thread_obj->initialized = wfalse;
}
