#include <math.h>
#include <inttypes.h>
#include <string.h>
#include <wchar.h>
#include <time.h>

#if defined(_WIN32)
    #include <Windows.h>
#endif

#include <Engine.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>
#include <Base/geom.h>

#include <Base/geom3D.h>

void Vec3_add(Vector3_t* src,const Vector3_t add)
{
	src->x += add.x;
	src->y += add.y;
	src->z += add.z;
}

void Vec3_sub(Vector3_t* src, const Vector3_t sub)
{
	src->x -= sub.x;
	src->y -= sub.y;
	src->z -= sub.z;
}

Vector3_t Vec3_mulvec_f(const Vector3_t vec1,const wfloat fmul)
{
	Vector3_t ret = {vec1.x * fmul, vec1.y * fmul,vec1.z * fmul};
	return ret;
}

Vector3_t Vec3_mulvec(const Vector3_t vec1,const Vector3_t vec2)
{
	Vector3_t ret = {vec1.x * vec2.x, vec1.y * vec2.y,vec1.z * vec2.z};
	return ret;
}

void Vec3_mulsrc_f(Vector3_t* src,const wfloat mul)
{
	src->x *= mul;
	src->y *= mul;
	src->z *= mul;
}

void Vec3_mulsrc(Vector3_t* src,const Vector3_t mul)
{
	src->x *= mul.x;
	src->y *= mul.y;
	src->z *= mul.z;
}

Vector3_t Vec3_div_f(const Vector3_t vec1,const wfloat div)
{
	Vector3_t ret = {vec1.x / div, vec1.y / div,vec1.z / div};
	return ret;
}

Vector3_t Vec3_div(const Vector3_t vec1,const Vector3_t vec2)
{
	Vector3_t ret = {vec1.x / vec2.x, vec1.y / vec2.y,vec1.z / vec2.z};
	return ret;
}

void Vec3_divsrc_f(Vector3_t* src,const wfloat div)
{
	src->x /= div;
	src->y /= div;
	src->z /= div;
}

void Vec3_divsrc(Vector3_t* src,const Vector3_t div)
{
	src->x /= div.x;
	src->y /= div.y;
	src->z /= div.z;
}


char Vec3_isequal(const Vector3_t vec1,const Vector3_t vec2)
{
	if(vec1.x == vec2.x && vec1.y == vec2.y && vec1.z == vec2.z)
		return 1;
	
	return 0;
}

wbool Vec3_isfuzzyequal(const Vector3_t* const vec1,const Vector3_t* const vec2,wfloat tolerance)
{
	if(fuzzyEquals(vec1->x,vec2->x,tolerance) && fuzzyEquals(vec1->y,vec2->y,tolerance) && fuzzyEquals(vec1->z,vec2->z,tolerance))
		return wtrue;

	return wfalse;
}

Vector3_t Vec3_plus(const Vector3_t vec1,const Vector3_t vec2)
{
	Vector3_t ret = {vec1.x + vec2.x, vec1.y + vec2.y,vec1.z + vec2.z};
	return ret;
}
Vector3_t Vec3_minus(const Vector3_t vec1,const Vector3_t vec2)
{
	Vector3_t ret = {vec1.x - vec2.x, vec1.y - vec2.y,vec1.z - vec2.z};
	return ret;
}
wfloat Vec3_length(Vector3_t* vec)
{
	return wsqrt(vec->x * vec->x + vec->y * vec->y + vec->z * vec->z);
}
wfloat Vec3_lengthsquared(const Vector3_t vec)
{
	return (vec.x * vec.x + vec.y * vec.y + vec.z * vec.z);
}
wfloat Vec3_normalise(Vector3_t* vec)
{
	wfloat len = Vec3_length(vec);
	
	wfloat fInvLen = 1.0f / len;
	
	Vec3_mulsrc_f(vec,fInvLen);
	
	return len;
	
}

wfloat Vec3_dot(Vector3_t vec1, Vector3_t vec2)
{
	return (vec1.x * vec2.x + vec1.y * vec2.y + vec1.z * vec2.z);
}

Vector3_t Vec3_cross(Vector3_t vec1,Vector3_t vec2)
{
	Vector3_t cross = {(vec1.y * vec2.z - vec1.z * vec2.y),(vec1.z * vec2.x - vec1.x * vec2.z),(vec1.x * vec2.y - vec1.y * vec2.x) };

	return cross;
}

wfloat Vec3_dist(const Vector3_t vec1,const Vector3_t vec2)
{
	Vector3_t dir = Vec3_minus(vec2,vec1);

	return Vec3_normalise(&dir);
}

Vector3_t Vec3(wfloat x,wfloat y,wfloat z)
{
	Vector3_t tmp = {x,y,z};
	return tmp;
}