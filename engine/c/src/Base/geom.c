#include <math.h>
#include <inttypes.h>
#include <string.h>
#include <wchar.h>
#include <time.h>

#if defined(_WIN32)
    #include <Windows.h>
#endif

#include <Engine.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>
#include <Base/geom.h>

void Vec_add(Vector_t* src,const Vector_t add)
{
	src->x += add.x;
	src->y += add.y;
}

void Vec_sub(Vector_t* src, const Vector_t sub)
{
	src->x -= sub.x;
	src->y -= sub.y;
}

Vector_t Vec_mulvec_f(const Vector_t vec1,const wfloat fmul)
{
	Vector_t ret = {vec1.x * fmul, vec1.y * fmul};
	return ret;
}

Vector_t Vec_mulvec(const Vector_t vec1,const Vector_t vec2)
{
	Vector_t ret = {vec1.x * vec2.x, vec1.y * vec2.y};
	return ret;
}

void Vec_mulsrc_f(Vector_t* src,const wfloat mul)
{
	src->x *= mul;
	src->y *= mul;
}

void Vec_mulsrc(Vector_t* src,const Vector_t mul)
{
	src->x *= mul.x;
	src->y *= mul.y;
}

Vector_t Vec_div_f(const Vector_t vec1,const wfloat div)
{
	Vector_t ret = {vec1.x / div, vec1.y / div};
	return ret;
}

Vector_t Vec_div(const Vector_t vec1,const Vector_t vec2)
{
	Vector_t ret = {vec1.x / vec2.x, vec1.y / vec2.y};
	return ret;
}

void Vec_divsrc_f(Vector_t* src,const wfloat div)
{
	src->x /= div;
	src->y /= div;
}

void Vec_divsrc(Vector_t* src,const Vector_t div)
{
	src->x /= div.x;
	src->y /= div.y;
}


char Vec_isequal(const Vector_t vec1,const Vector_t vec2)
{
	if(vec1.x == vec2.x && vec1.y == vec2.y)
		return 1;
	
	return 0;
}

wbool Vec_isfuzzyequal(const Vector_t* const vec1,const Vector_t* const vec2,wfloat tolerance)
{
	if(fuzzyEquals(vec1->x,vec2->x,tolerance) && fuzzyEquals(vec1->y,vec2->y,tolerance))
		return wtrue;

	return wfalse;
}

Vector_t Vec_plus(const Vector_t vec1,const Vector_t vec2)
{
	Vector_t ret = {vec1.x + vec2.x, vec1.y + vec2.y};
	return ret;
}
Vector_t Vec_minus(const Vector_t vec1,const Vector_t vec2)
{
	Vector_t ret = {vec1.x - vec2.x, vec1.y - vec2.y};
	return ret;
}
wfloat Vec_length(Vector_t* vec)
{
	return wsqrt(vec->x * vec->x + vec->y * vec->y);
}
wfloat Vec_lengthsquared(const Vector_t vec)
{
	return (vec.x * vec.x + vec.y * vec.y);
}
wfloat Vec_normalise(Vector_t* vec)
{
	wfloat len = Vec_length(vec);
	
	wfloat fInvLen = 1.0f / len;
	
	Vec_mulsrc_f(vec,fInvLen);
	
	return len;
	
}

wfloat Vec_dot(Vector_t vec1, Vector_t vec2)
{
	return (vec1.x * vec2.x + vec1.y * vec2.y);
}

Vector_t Vec_rotate(const Vector_t vec1,const wfloat degAngle)
{
	Vector_t ret = {vec1.x,vec1.y};

	wfloat theta = deg2rad(degAngle);

	float vcos = cosf((float)theta);
	float vsin = sinf((float)theta);

	ret.x = vec1.x * vcos - vec1.y * vsin;
	ret.y = vec1.x * vsin + vec1.y * vcos;

	return ret;
}

void Vec_ApplyAngle(Vector_t* src,const wfloat degAngle)
{
	wfloat tmpx = 0.0;

	wfloat theta = deg2rad(degAngle);

	float vcos = cosf((float)theta);
	float vsin = sinf((float)theta);

	tmpx = src->x * vcos - src->y * vsin;
	src->y = src->x * vsin + src->y * vcos;
	src->x = tmpx;
}

wfloat Vec_dist(const Vector_t vec1,const Vector_t vec2)
{
	Vector_t dir = Vec_minus(vec2,vec1);

	return Vec_normalise(&dir);
}

Vector_t Vec(wfloat x,wfloat y)
{
	Vector_t tmp = {x,y};
	return tmp;
}

//project point at the distance project_distance from src
Vector_t Vec_project(const Vector_t* const point,const Vector_t* const src,const wfloat project_distance)
{
	Vector_t src_to_point = {point->x,point->y};
	Vec_sub(&src_to_point,(*src));
	wfloat ex_dist = Vec_normalise(&src_to_point);
	Vec_mulsrc_f(&src_to_point,project_distance - ex_dist);
	Vector_t projected_point = {point->x,point->y};
	Vec_add(&projected_point,src_to_point);

	return projected_point;
}

wbool edge_intersect_circle(const edge_t* const edge,const Vector_t circle_center,const wfloat circle_radius)
{
	wfloat e1x = edge->end.x - edge->start.x;
	wfloat e1y = edge->end.y - edge->start.y;

	wfloat c1x = circle_center.x - edge->start.x;
	wfloat c1y = circle_center.y - edge->start.y;

	wfloat k = c1x * e1x + c1y * e1y;

	if(k > 0)
	{
		wfloat len = e1x * e1x + e1y * e1y;

		if(k < len)
		{
			wfloat c1sqr = c1x*c1x + c1y * c1y - (circle_radius * circle_radius);

			if(c1sqr * len <= k * k)
				return wtrue;

		}
	}

	return wfalse;
}

wbool circle_intersect_triangle(const Circle_t circle,const triangle_t triangle)
{
	//-1- check if triangle vertex are in circle
	wfloat c1x = circle.position.x - triangle.point_a.x;
	wfloat c1y = circle.position.y - triangle.point_a.y;

	wfloat radius_sqr =  circle.radius  * circle.radius;
	wfloat c1sqr = (c1x * c1x + c1y * c1y) - radius_sqr;

	if(c1sqr <= 0)
		return wtrue;

	wfloat c2x = circle.position.x - triangle.point_b.x;
	wfloat c2y = circle.position.y - triangle.point_b.y;

	wfloat c2sqr = (c2x * c2x + c2y * c2y) - radius_sqr;

	if(c2sqr <= 0)
		return wtrue;

	wfloat c3x = circle.position.x - triangle.point_c.x;
	wfloat c3y = circle.position.y - triangle.point_c.y;

	wfloat c3sqr = (c3x * c3x + c3y * c3y) - radius_sqr;

	if(c3sqr <= 0)
		return wtrue;

	//-2- check if the circle center is within the triangle
	if(Vec_in_triangle(&circle.position,&triangle))
		return wtrue;

	//-3- check if the circle intersects edge

	wfloat e1x = triangle.point_b.x - triangle.point_a.x;
	wfloat e1y = triangle.point_b.y - triangle.point_a.y;

	wfloat e2x = triangle.point_c.x - triangle.point_b.x;
	wfloat e2y = triangle.point_c.y - triangle.point_b.y;

	wfloat e3x = triangle.point_a.x - triangle.point_c.x;
	wfloat e3y = triangle.point_a.y - triangle.point_c.y;

	wfloat k = c1x * e1x + c1y * e1y;

	if(k > 0)
	{
		wfloat len = e1x * e1x + e1y * e1y;

		if(k < len)
		{
			if(c1sqr * len <= k*k)
				return wtrue;
		}
	}

	k = c2x * e2x + c2y * e2y;

	if(k > 0)
	{
		wfloat len = e2x *e2x + e2y * e2y;

		if(k < len)
		{
			if(c2sqr * len <= k*k)
				return wtrue;
		}
	}

	k = c3x * e3x + c3y * e3y;

	if(k > 0)
	{
		wfloat len = e3x * e3x + e3y * e3y;

		if(k < len)
		{
			if(c3sqr * len <= k*k)
				return wtrue;
		}
	}

	return wfalse;
}

wbool circle_intersect_rect(const Rect_t rec,const Circle_t  circ)
{
	triangle_t trig_1 = {{rec.position.x,rec.position.y},{rec.position.x + rec.width,rec.position.y},{rec.position.x,rec.position.y - rec.height}};
	triangle_t trig_2 = {{rec.position.x + rec.width,rec.position.y},{rec.position.x + rec.width,rec.position.y - rec.height},{rec.position.x,rec.position.y - rec.height}};

	if(circle_intersect_triangle(circ,trig_1))
		return wtrue;

	if(circle_intersect_triangle(circ,trig_2))
		return wtrue;

	return wfalse;
}

wbool circle_intersect_circle(const Circle_t circle1,const Circle_t circle2)
{
	Vector_t diff = Vec_minus(circle1.position,circle2.position);
	wfloat square_diff = Vec_lengthsquared(diff);

	if(square_diff <= (circle1.radius + circle2.radius) * (circle1.radius + circle2.radius))
		return wtrue;

	return wfalse;
}

wbool Vec_in_triangle(const Vector_t* const vec,const triangle_t* const triangle)
{
	wbool b1,b2,b3;

	b1 = (wbool)(sign(vec,&triangle->point_a,&triangle->point_b) < 0.0f);
	b2 = (wbool)(sign(vec,&triangle->point_b,&triangle->point_c) < 0.0f);
	b3 = (wbool)(sign(vec,&triangle->point_c,&triangle->point_a) < 0.0f);

	return (wbool)((b1 == b2) && (b2 == b3));
}

wbool triangle_intersect_triangle(const triangle_t* const triangle1,const triangle_t* const triangle2)
{
	if(Vec_in_triangle(&triangle1->point_a,triangle2))
		return wtrue;
	
	if(Vec_in_triangle(&triangle1->point_b,triangle2))
		return wtrue;

	if(Vec_in_triangle(&triangle1->point_c,triangle2))
		return wtrue;

	if(Vec_in_triangle(&triangle2->point_a,triangle1))
		return wtrue;
	
	if(Vec_in_triangle(&triangle2->point_b,triangle1))
		return wtrue;

	if(Vec_in_triangle(&triangle2->point_c,triangle1))
		return wtrue;

	return wfalse; 
}

/*
wbool edge_intersect_shadowvolume(const edge_t* const edge,const shadow_volume_t* const volume)
{
	//check for 2 triangles
	triangle_t triangle1 = {volume->point_a,volume->point_b,volume->point_c};
	triangle_t triangle2 = {volume->point_a,volume->point_c,volume->point_d};

	if(Vec_in_triangle(&edge->start,&triangle1))
		return wtrue;

	if(Vec_in_triangle(&edge->end,&triangle1))
		return wtrue;

	if(Vec_in_triangle(&edge->start,&triangle2))
		return wtrue;

	if(Vec_in_triangle(&edge->end,&triangle2))
		return wtrue;

	return wfalse;
}*/

