
//utf8 <-> wchar_t function from
/*
 * Copyright (c) 2007 Alexey Vatchenko <av@bsdua.org>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifdef __GNUC__
	#if __STDC_VERSION__ >= 199901L
		#define _XOPEN_SOURCE 600
	#else
		#define _XOPEN_SOURCE 500
	#endif /* __STDC_VERSION__ */
#endif

#include <stdio.h>
#include <wchar.h>
#include <string.h>
#include <inttypes.h>
#include <math.h>
#include <stdlib.h>

#include <sys/types.h>

#include <Engine.h>
#include <Base/types.h>

#include <time.h>

#include <wchar.h>


#ifdef __APPLE__
	#include <mach-o/dyld.h>
	#include <sys/param.h>
	#include <stdlib.h>
	#include <mach/mach_time.h>
	#define _NANO (+1.0E-09)
	#define _GIGA UINT64_C(1000000000)
	static double _timebase = 0.0;
	static uint64_t _timestart = 0;
#endif

#if defined(_WIN32)
    #include <Windows.h>
	#include <shlobj.h>
	#include <strsafe.h>
	#include <io.h>
#else
    #include <unistd.h>
    #include <arpa/inet.h>
    #include <uuid/uuid.h>
	#include <sys/stat.h>
	#include <sys/types.h>
#endif

#ifdef EMSCRIPTEN
	#include <emscripten.h>
#endif

#include <Base/functions.h>

void get_app_path(char* app_path)
{
    #if defined(_WIN32)
		#if (defined(WINAPI_FAMILY) && (WINAPI_FAMILY != WINAPI_FAMILY_PHONE_APP)) || !defined(WINAPI_FAMILY)
			wchar_t* last_char;
			wchar_t tmp_path[WAFA_MAX_PATH];
			wchar_t tmp_app_path[WAFA_MAX_PATH];
			const wchar_t *ptr[1];
			mbstate_t mbs;

			ptr[0] = tmp_app_path;

        
			GetModuleFileName(NULL, tmp_path, WAFA_MAX_PATH);
			last_char = wcsrchr(tmp_path,L'\\');
        
			int32_t len = ((((int32_t)last_char) - (int32_t)(tmp_path)) / sizeof(wchar_t)) + 1;

			w_wcsncpy(tmp_app_path,tmp_path,len);
		

			tmp_app_path[len] = '\0';
			
			mbrlen (NULL,0,&mbs);
			size_t retval = 0;
			w_wcsrtombs(&retval,app_path,len+1,ptr,len+1,&mbs);
		#endif
	#elif defined(__APPLE__)
		char tmp_path[WAFA_MAX_PATH];
		char tmp_path2[WAFA_MAX_PATH];
	
		uint32_t buff_size = sizeof(tmp_path);
	
		_NSGetExecutablePath(tmp_path,&buff_size);
	
		realpath(tmp_path,&tmp_path2[0]);
	
		char* last_char = strrchr(tmp_path2,'/');
		int final_len = (((intptr_t)last_char) - ((intptr_t)tmp_path2) + 1);
		memmove(app_path,tmp_path2,final_len * sizeof(char));
		app_path[final_len] = '\0';
	
		
	
    #else
       char * last_char;
       char tmp_path[WAFA_MAX_PATH];
       memset(tmp_path,0,WAFA_MAX_PATH);
       readlink("/proc/self/exe", tmp_path, WAFA_MAX_PATH);
       //memmove(app_path,tmp_path,len);
       last_char = strrchr(tmp_path,'/');
       int final_len = (((intptr_t)last_char) - ((intptr_t)tmp_path) + 1);
       memmove(app_path,tmp_path,final_len * sizeof(char));
       app_path[final_len] = '\0';
    #endif
}

void get_user_home_dir(wchar_t* userprofilepath)
{
	#if defined(_WIN32)
		PWSTR userFolderPath = NULL;
		HRESULT result = SHGetKnownFolderPath(FOLDERID_RoamingAppData, 0, NULL, &userFolderPath);

		if (result == S_OK)
		{
			w_wcscpy(userprofilepath, WAFA_MAX_PATH, userFolderPath);
		}

		CoTaskMemFree(userFolderPath);

	#elif defined(__APPLE__)
		char path[1024];
		CFStringRef name = CSCopyUserName(true);
		CFStringRef full = CFStringCreateWithFormat(NULL, NULL, CFSTR("/Users/%@"), name);
		CFStringGetCString(full, path, sizeof(path), kCFStringEncodingUTF8);
		w_mbstowcs(WAFA_MAX_PATH, userprofilepath, WAFA_MAX_PATH, path, strlen(path));
	#else

		struct passwd pwd;
		struct passwd *result;
		char *buf;
		size_t bufsize;
		int s;
		bufsize = sysconf(_SC_GETPW_R_SIZE_MAX);
		if (bufsize == -1)
			bufsize = 0x4000; // = all zeroes with the 14th bit set (1 << 14)
		buf = malloc(bufsize);
		if (buf == NULL) {
			perror("malloc");
		}
		s = getpwuid_r(getuid(), &pwd, buf, bufsize, &result);
		if (result == NULL) {
			if (s == 0)
				printf("Not found\n");
			else {
				errno = s;
				perror("getpwnam_r");
			}
		}
		char *homedir = result.pw_dir;

		w_mbstowcs(WAFA_MAX_PATH, userprofilepath, WAFA_MAX_PATH, homedir, strlen(homedir));
		free(buf);
	#endif
}

wbool directory_exist(const wchar_t* directory)
{
	#if defined(_WIN32)
		HANDLE hFind = INVALID_HANDLE_VALUE;
		wchar_t szDir[WAFA_MAX_PATH];
		WIN32_FIND_DATA ffd;

		StringCchCopy(szDir, WAFA_MAX_PATH, directory);
		StringCchCat(szDir, WAFA_MAX_PATH, L"\\*");

		hFind = FindFirstFileEx(szDir, FindExInfoStandard, &ffd, FindExSearchNameMatch, NULL, 0);



		if (hFind == INVALID_HANDLE_VALUE) {
			return wfalse;
		}
		return wtrue;
	#else
		DIR* resx_dir = 0;
		char directorypath[1024] = { 0 };
		wchar_to_utf8(directory, wcslen(directory), directorypath, 1024);

		if (!(resx_dir = opendir(directorypath))) {
			return wfalse;
		}

		return wtrue;
	#endif

}

wbool create_directory(const wchar_t* directory)
{
	#if defined(_WIN32)
		int ret = CreateDirectory(directory, NULL);

		if (ret == 0)
			return wfalse;

		return wtrue;
	#else
		mode_t perm = 0660; //RW for user and group	
		char directorypath[1024] = { 0 };
		wchar_to_utf8(directory, wcslen(directory), directorypath, 1024);

		int res = mkdir(directorypath, perm);

		if (res == 0)
			return wtrue;

		return wfalse;
	#endif
}

wbool copy_file(const char* src, const char* dst,wbool overwrite)
{
	#if defined(_WIN32)
		wchar_t tmpsrc[WAFA_MAX_PATH] = { 0 };
		wchar_t tmpdst[WAFA_MAX_PATH] = { 0 };
		MultiByteToWideChar(CP_THREAD_ACP, 0, src, 260, tmpsrc, 260);
		MultiByteToWideChar(CP_THREAD_ACP, 0, dst, 260, tmpdst, 260);
		return (wbool)CopyFile(tmpsrc, tmpdst, (overwrite) ? 0:1);
	#else
		int fd_to, fd_from;
		char buf[4096];
		ssize_t nread;
		int saved_errno;

		fd_from = open(src, O_RDONLY);
		if (fd_from < 0)
			return -1;

		int flags = O_WRONLY | O_EXCL;

		if (overwrite) {
			flags = O_WRONLY | O_CREAT | O_EXCL;
		}

		fd_to = open(dst, flags, 0660);

		if (fd_to < 0)
			goto out_error;

		while (nread = read(fd_from, buf, sizeof buf), nread > 0)
		{
			char *out_ptr = buf;
			ssize_t nwritten;

			do {
				nwritten = write(fd_to, out_ptr, nread);

				if (nwritten >= 0)
				{
					nread -= nwritten;
					out_ptr += nwritten;
				}
				else if (errno != EINTR)
				{
					goto out_error;
				}
			} while (nread > 0);
		}

		if (nread == 0)
		{
			if (close(fd_to) < 0)
			{
				fd_to = -1;
				goto out_error;
			}
			close(fd_from);

			/* Success! */
			return 0;
		}

	out_error:
		saved_errno = errno;

		close(fd_from);
		if (fd_to >= 0)
			close(fd_to);

		errno = saved_errno;
		return -1;
	#endif
}

void get_file_name(const char* file_path,char* file_name,wbool with_ext) {
	const char* slash;
	int32_t len = 0;

        slash = strrchr(file_path,'\\');//since the editor is on windows, we could have a path with anti slash
        
        if(!slash)
            slash = strrchr(file_path,'/');
        
	if(with_ext)
		len = ((intptr_t)strrchr(file_path,'\0')) - ((intptr_t)slash);
	else
		len =  ((intptr_t)strrchr(file_path, '.')) - ((intptr_t)slash) - 1;

	memmove(file_name,slash + 1, len * sizeof(char));

	if(!with_ext)
		file_name[len] = '\0';
}

const char* get_file_name2(const char* file_name) {
	const char* slash;

        slash = strrchr(file_name,'\\');//since the editor is on windows, we could have a path with anti slash
        
        if(!slash)
            slash = strrchr(file_name,'/');

	return slash + 1;
}

char* string_alloc(const char* src)
{
	size_t str_size = strlen(src)+1;
	char* dest = (char*)wf_malloc(str_size);
	w_strcpy(dest,str_size,src);

	return dest;
}


void combine_rel_path(char* base_path,const char* base_rel_path,size_t size_base_path)
{
	uint32_t ipath = 0;

	char rel_path[260];
    char cp_base_path[260];
    w_strcpy(cp_base_path,260,base_path);

	w_strcpy(rel_path,260,base_rel_path);

	while(ipath < strlen(rel_path) && rel_path[ipath] != '\0')
	{
		if(ipath < strlen(rel_path) && rel_path[ipath] == '.' && rel_path[ipath + 1] == '.')
		{
			//remove last directory of app path
			char* sep_pos = strrchr(cp_base_path,PATH_SEP);
			if(sep_pos != NULL)
			{
				*sep_pos = '\0';

				ipath += 3;
			}
			else
			{
				break;
			}
		}
		else
		{
			break;
		}
	}

	w_sprintf(base_path, size_base_path,"%s%c%s",cp_base_path,PATH_SEP,&rel_path[ipath]);
}


void get_file_without_ext(char* file_name) {
	int32_t len = 0;
	len =  ((intptr_t)strrchr(file_name, '.')) - ((intptr_t)file_name);
	file_name[len] = '\0';
}

void str_double_slash(const char* src,char* dest,const char* slash)
{
	uint32_t src_i = 0;
	uint32_t dest_i = 0;

    for(src_i = 0; src_i < strlen(src); src_i++)
	{
		if(src[src_i] == slash[0])
			((char*)dest)[dest_i++] = ((char*)src)[src_i];

        ((char*)dest)[dest_i++] = ((char*)src)[src_i];
	}

	((char*)dest)[dest_i] = '\0';

}

wbool str_has_slash(const char* file_path)
{
	const char* slash = strrchr(file_path, '\\');//since the editor is on windows, we could have a path with anti slash

	if (!slash)
		slash = strrchr(file_path, '/');

	return (wbool)(slash != NULL);
}

wbool str_starts_with(const char *pre, const char *str)
{
    size_t lenpre = strlen(pre),
           lenstr = strlen(str);

	wbool retval = wfalse;

	if(lenstr >= lenpre)
	{
		if(strncmp(pre, str, lenpre) == 0)
			retval = wtrue;
	}
		

    return retval;
}

wbool str_end_with(const char* pre, const char* str)
{
	size_t lenpre = strlen(pre),
		lenstr = strlen(str);

	wbool retval = wfalse;

	if (lenstr >= lenpre)
	{
		size_t str_start = lenstr - lenpre;
		const char* start_ptr = str + str_start;

		if (strncmp(pre,start_ptr, lenpre) == 0)
			retval = wtrue;
	}

	return retval;
}



void atomic_set_value(volatile uint32_t* target,uint32_t value)
{
	#if defined (_WIN32)
		InterlockedExchange(target,value);
	#elif EMSCRIPTEN
		(*target) = value;
	#else
        __sync_val_compare_and_swap(target,(*target),value);
	#endif
}

void atomic_increment(volatile uint32_t* target)
{
	#if defined (_WIN32)
		InterlockedIncrement(target);
	#elif EMSCRIPTEN
		(*target)++;
	#else
		__sync_fetch_and_add(target,1);
	#endif
}

#if defined(_WIN32)	

void usleep(double waitTime)
{
	DWORD w_waitTime = (DWORD)waitTime;

	LARGE_INTEGER perfCnt, start, now, ElapsedMicroseconds;
 
	QueryPerformanceFrequency(&perfCnt);
	QueryPerformanceCounter(&start);
 
	do {
		QueryPerformanceCounter((LARGE_INTEGER*) &now);
		ElapsedMicroseconds.QuadPart = now.QuadPart - start.QuadPart;
		ElapsedMicroseconds.QuadPart *= 1000000;
		ElapsedMicroseconds.QuadPart /= perfCnt.QuadPart;
	} while (ElapsedMicroseconds.QuadPart < w_waitTime);

}

#endif


TIME_PROF time_diff(TIME_PROF* start, TIME_PROF* end)
{
	TIME_PROF temp;
	
	#if defined(_WIN32)
		temp.QuadPart = (end->QuadPart - start->QuadPart);
	#else
		if ((end->tv_nsec-start->tv_nsec)<0) {
			temp.tv_sec = end->tv_sec-start->tv_sec-1;
			temp.tv_nsec = 1000000000+end->tv_nsec-start->tv_nsec;
		} else {
			temp.tv_sec = end->tv_sec-start->tv_sec;
			temp.tv_nsec = end->tv_nsec-start->tv_nsec;
		}
	#endif
	
	return temp;
} 

void time_get_char_diff(char* content,TIME_PROF* start,TIME_PROF* end,size_t content_size)
{
	TIME_PROF tmp = time_diff(start,end);
	long nsec = time_get_nsec(&tmp);

	w_sprintf(content, content_size,"%5.3f ms",(float)nsec / 1000000);
}

void time_get_time(TIME_PROF* prof_value)
{
	#if defined(_WIN32)
		QueryPerformanceCounter(prof_value);
	#elif defined(EMSCRIPTEN) 
		clock_gettime(CLOCK_MONOTONIC, prof_value);
	#elif defined(__APPLE__)
	
	//TODO : make it thread safe
	if(!_timestart) {
		mach_timebase_info_data_t tb = {0};
		mach_timebase_info(&tb);
		_timebase = tb.numer;
		_timebase /= tb.denom;
		_timestart = mach_absolute_time();
	}
	
	double diff = (mach_absolute_time() - _timestart) * _timebase;
	prof_value->tv_sec = diff * _NANO;
	prof_value->tv_nsec = diff - (prof_value->tv_sec * _GIGA);
	
	#else
		clock_gettime(CLOCK_PROCESS_CPUTIME_ID, prof_value);
	#endif
}

long time_get_nsec(TIME_PROF* prof_value)
{	
	#if defined(_WIN32)
		LARGE_INTEGER perfCnt;
		QueryPerformanceFrequency(&perfCnt);
		return (long) ((prof_value->QuadPart * 1000000000L) / perfCnt.QuadPart);
	#else
		return (long)(prof_value->tv_nsec + (prof_value->tv_sec * 1000000000L));
	#endif
}


#if defined(_WIN32)	

void nano_sleep(long waitTime,wbool focus)
{
	DWORD w_waitTime = (DWORD)waitTime;

	//logprint("wait for %f time",waitTime);

	if(focus)
	{
		LARGE_INTEGER perfCnt, start, now, ElapsedNanoseconds;
 
		QueryPerformanceFrequency(&perfCnt);
		QueryPerformanceCounter(&start);
 
		do {
			QueryPerformanceCounter((LARGE_INTEGER*) &now);
			ElapsedNanoseconds.QuadPart = (now.QuadPart - start.QuadPart) * 1000000000L;
			ElapsedNanoseconds.QuadPart /= perfCnt.QuadPart;
		} while (ElapsedNanoseconds.QuadPart < w_waitTime);

	}
	else
	{
		#if (defined(WINAPI_FAMILY) && (WINAPI_FAMILY != WINAPI_FAMILY_PHONE_APP)) || !defined(WINAPI_FAMILY)
			Sleep(15);
		#endif
	}
}

#else
void nano_sleep(long waitTime,wbool focus)
{
	struct timespec elapsed_time;
	struct timespec now;
	struct timespec start;
	
	if(waitTime > 500000L)//sleep for 500 usec
	{
		waitTime -=  500000L;
		usleep(500);
	}
	
	time_get_time(&start);

	do
	{
		time_get_time(&now);
		elapsed_time = time_diff(&start,&now);
	} while(elapsed_time.tv_nsec < waitTime);
}
#endif



#ifndef _WIN32

static int __wchar_forbitten(wchar_t sym);
static int __utf8_forbitten(unsigned char octet);

static int
__wchar_forbitten(wchar_t sym)
{

	/* Surrogate pairs */
	if (sym >= 0xd800 && sym <= 0xdfff)
		return (-1);

	return (0);
}

static int
__utf8_forbitten(unsigned char octet)
{

	switch (octet) {
	case 0xc0:
	case 0xc1:
	case 0xf5:
	case 0xff:
		return (-1);
	}

	return (0);
}

/*
 * DESCRIPTION
 *	This function translates UTF-8 string into UCS-4 string (all symbols
 *	will be in local machine byte order).
 *
 *	It takes the following arguments:
 *	in	- input UTF-8 string. It can be null-terminated.
 *	insize	- size of input string in bytes.
 *	out	- result buffer for UCS-4 string. If out is NULL,
 *		function returns size of result buffer.
 *	outsize - size of out buffer in wide characters.
 *
 * RETURN VALUES
 *	The function returns size of result buffer (in wide characters).
 *	Zero is returned in case of error.
 *
 * CAVEATS
 *	1. If UTF-8 string contains zero symbols, they will be translated
 *	   as regular symbols.
 *	2. If UTF8_IGNORE_ERROR or UTF8_SKIP_BOM flag is set, sizes may vary
 *	   when `out' is NULL and not NULL. It's because of special UTF-8
 *	   sequences which may result in forbitten (by RFC3629) UNICODE
 *	   characters.  So, the caller must check return value every time and
 *	   not prepare buffer in advance (\0 terminate) but after calling this
 *	   function.
 */
 
size_t mb_to_wchar(const unsigned char *in, size_t insize, wchar_t *out, size_t outsize,
    int flags)
{
	unsigned char *p, *lim;
	wchar_t *wlim, high;
	size_t n, total, i, n_bits;

	if (in == NULL || insize == 0 || (outsize == 0 && out != NULL))
		return (0);

	total = 0;
	p = (unsigned char *)in;
	lim = p + insize;
	wlim = out + outsize;

	for (; p < lim; p += n) {
		if (__utf8_forbitten(*p) != 0 &&
		    (flags & UTF8_IGNORE_ERROR) == 0)
			return (0);

		/*
		 * Get number of bytes for one wide character.
		 */
		n = 1;	/* default: 1 byte. Used when skipping bytes. */
		if ((*p & 0x80) == 0)
			high = (wchar_t)*p;
		else if ((*p & 0xe0) == _SEQ2) {
			n = 2;
			high = (wchar_t)(*p & 0x1f);
		} else if ((*p & 0xf0) == _SEQ3) {
			n = 3;
			high = (wchar_t)(*p & 0x0f);
		} else if ((*p & 0xf8) == _SEQ4) {
			n = 4;
			high = (wchar_t)(*p & 0x07);
		} else if ((*p & 0xfc) == _SEQ5) {
			n = 5;
			high = (wchar_t)(*p & 0x03);
		} else if ((*p & 0xfe) == _SEQ6) {
			n = 6;
			high = (wchar_t)(*p & 0x01);
		} else {
			if ((flags & UTF8_IGNORE_ERROR) == 0)
				return (0);
			continue;
		}

		/* does the sequence header tell us truth about length? */
		if (lim - p <= n - 1) {
			if ((flags & UTF8_IGNORE_ERROR) == 0)
				return (0);
			n = 1;
			continue;	/* skip */
		}

		/*
		 * Validate sequence.
		 * All symbols must have higher bits set to 10xxxxxx
		 */
		if (n > 1) {
			for (i = 1; i < n; i++) {
				if ((p[i] & 0xc0) != _NXT)
					break;
			}
			if (i != n) {
				if ((flags & UTF8_IGNORE_ERROR) == 0)
					return (0);
				n = 1;
				continue;	/* skip */
			}
		}

		total++;

		if (out == NULL)
			continue;

		if (out >= wlim)
			return (0);		/* no space left */

		*out = 0;
		n_bits = 0;
		for (i = 1; i < n; i++) {
			*out |= (wchar_t)(p[n - i] & 0x3f) << n_bits;
			n_bits += 6;		/* 6 low bits in every byte */
		}
		*out |= high << n_bits;

		if (__wchar_forbitten(*out) != 0) {
			if ((flags & UTF8_IGNORE_ERROR) == 0)
				return (0);	/* forbitten character */
			else {
				total--;
				out--;
			}
		} else if (*out == _BOM && (flags & UTF8_SKIP_BOM) != 0) {
			total--;
			out--;
		}

		out++;
	}

	return (total);
}

/*
 * DESCRIPTION
 *	This function translates UCS-4 symbols (given in local machine
 *	byte order) into UTF-8 string.
 *
 *	It takes the following arguments:
 *	in	- input unicode string. It can be null-terminated.
 *	insize	- size of input string in wide characters.
 *	out	- result buffer for utf8 string. If out is NULL,
 *		function returns size of result buffer.
 *	outsize - size of result buffer.
 *
 * RETURN VALUES
 *	The function returns size of result buffer (in bytes). Zero is returned
 *	in case of error.
 *
 * CAVEATS
 *	If UCS-4 string contains zero symbols, they will be translated
 *	as regular symbols.
 */
size_t wchar_to_mb(const wchar_t *in, size_t insize, char *out, size_t outsize,int flags)
{
	wchar_t *w, *wlim, ch;
	u_char *p, *lim, *oc;
	size_t total, n;

	if (in == NULL || insize == 0 || (outsize == 0 && out != NULL))
		return (0);

	w = (wchar_t *)in;
	wlim = w + insize;
	p = (u_char *)out;
	lim = p + outsize;
	total = 0;
	for (; w < wlim; w++) {
		if (__wchar_forbitten(*w) != 0) {
			if ((flags & UTF8_IGNORE_ERROR) == 0)
				return (0);
			else
				continue;
		}

		if (*w == _BOM && (flags & UTF8_SKIP_BOM) != 0)
			continue;

		if (*w < 0) {
			if ((flags & UTF8_IGNORE_ERROR) == 0)
				return (0);
			continue;
		} else if (*w <= 0x0000007f)
			n = 1;
		else if (*w <= 0x000007ff)
			n = 2;
		else if (*w <= 0x0000ffff)
			n = 3;
		else if (*w <= 0x001fffff)
			n = 4;
		else if (*w <= 0x03ffffff)
			n = 5;
		else /* if (*w <= 0x7fffffff) */
			n = 6;

		total += n;

		if (out == NULL)
			continue;

		if (lim - p <= n - 1)
			return (0);		/* no space left */

		/* make it work under different endians */
		ch = htonl(*w);
		oc = (u_char *)&ch;
		switch (n) {
		case 1:
			*p = oc[3];
			break;

		case 2:
			p[1] = _NXT | (oc[3] & 0x3f);
			p[0] = _SEQ2 | (oc[3] >> 6) | ((oc[2] & 0x07) << 2);
			break;

		case 3:
			p[2] = _NXT | (oc[3] & 0x3f);
			p[1] = _NXT | (oc[3] >> 6) | ((oc[2] & 0x0f) << 2);
			p[0] = _SEQ3 | ((oc[2] & 0xf0) >> 4);
			break;

		case 4:
			p[3] = _NXT | (oc[3] & 0x3f);
			p[2] = _NXT | (oc[3] >> 6) | ((oc[2] & 0x0f) << 2);
			p[1] = _NXT | ((oc[2] & 0xf0) >> 4) |
			    ((oc[1] & 0x03) << 4);
			p[0] = _SEQ4 | ((oc[1] & 0x1f) >> 2);
			break;

		case 5:
			p[4] = _NXT | (oc[3] & 0x3f);
			p[3] = _NXT | (oc[3] >> 6) | ((oc[2] & 0x0f) << 2);
			p[2] = _NXT | ((oc[2] & 0xf0) >> 4) |
			    ((oc[1] & 0x03) << 4);
			p[1] = _NXT | (oc[1] >> 2);
			p[0] = _SEQ5 | (oc[0] & 0x03);
			break;

		case 6:
			p[5] = _NXT | (oc[3] & 0x3f);
			p[4] = _NXT | (oc[3] >> 6) | ((oc[2] & 0x0f) << 2);
			p[3] = _NXT | (oc[2] >> 4) | ((oc[1] & 0x03) << 4);
			p[2] = _NXT | (oc[1] >> 2);
			p[1] = _NXT | (oc[0] & 0x3f);
			p[0] = _SEQ6 | ((oc[0] & 0x40) >> 6);
			break;
		}

		/*
		 * NOTE: do not check here for forbitten UTF-8 characters.
		 * They cannot appear here because we do proper convertion.
		 */

		p += n;
	}

	return (total);
}

#else

size_t mb_to_wchar(const unsigned char *in, size_t insize, wchar_t *out, size_t outsize, int flags)
{
	return MultiByteToWideChar(CP_THREAD_ACP, 0, (const char*)in, insize, out, outsize);
}

size_t wchar_to_mb(const wchar_t *in, size_t insize, char *out, size_t outsize, int flags)
{
	return WideCharToMultiByte(CP_THREAD_ACP, 0, in, insize, out, outsize, "?", 0);
}

#endif

#if ANDROID_BUILD
size_t a_wcslen(const wchar_t *s)
{
        const wchar_t *p;


        p = s;
        while (*p)
                p++;


        return p - s;
}

void a_mbstowcs(wchar_t* dest,const char* src,size_t count)
{
	
		for(int i =0; i < count;i++)
		{
			*dest = (wchar_t)(*src);
			dest++;
			src++;
		}
}

int a_wcscmp(const wchar_t* str1,const wchar_t* str2)
{
	size_t sz = a_wcslen(str1);
	size_t sz2 = a_wcslen(str2);

	if(sz != sz2)
		return 1;

	for(int i =0; i < sz;i++)
	{
		if(*str1 != *str2)
			return 1;

		str1++;
		str2++;
	}

	return 0;
}

wchar_t* a_wcscpy(wchar_t* dest,size_t copy_size,const wchar_t* src)
{
	size_t sz = a_wcslen(src);

	if (sz > copy_size) {
		sz = copy_size;
	}

	for(int i =0; i < sz;i++)
	{
		*dest = (*src);
		dest++;
		src++;
	}

	*dest = L'\0';
}

#endif

void gen_guid(char* out_guid)
{
    #if defined(_WIN32)
   
        GUID guid;
        CoCreateGuid(&guid);
		RPC_CSTR guid_str;

		if (UuidToStringA(&guid, &guid_str) == RPC_S_OK)
		{
			w_strcpy(out_guid, 64, (const char*)guid_str);
			RpcStringFreeA(&guid_str);
		}
		

    #else
        uuid_t out;
        uuid_generate(&out[0]);
        uuid_unparse(out, &out_guid[0]);
    #endif
}
