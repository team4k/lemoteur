#include <stdlib.h>
#include <inttypes.h>
#include <math.h>
#include <string.h>

#include <Engine.h>

#include <Render.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>
#include <Base/geom.h>

#include <Debug/Logprint.h>

#include <Utils/Hash.h>

#include <Render/render_manager.h>

#include <Graphics/TextureLoader.h>

#include <GameObjects/Characters.pb-c.h>
#include <Graphics/Animation.h>
#include <Graphics/Sprite.h>


#include <Graphics/TextureRegion.h>
#include <Graphics/SpriteBatch.h>



void SpriteBatch_Init(SpriteBatch_t* const spritebatch,int32_t maxSprites,render_manager_t* const render_mngr)
{
	
	int32_t vert_size = (maxSprites * VERTICES_PER_SPRITE * VERTEX_SIZE) * sizeof(float);

	spritebatch->vertexBuffer = (float*)wf_malloc(vert_size);

	memset(spritebatch->vertexBuffer,0,vert_size);

	spritebatch->textcoordBuffer = (float*)wf_malloc(vert_size);

	memset(spritebatch->textcoordBuffer,0,vert_size);

	int32_t color_size = (maxSprites * VERTICES_PER_SPRITE * COLOR_SIZE) * sizeof(float);

	spritebatch->colorBuffer = (float*)wf_malloc(color_size);

	memset(spritebatch->colorBuffer, 0, color_size);

	spritebatch->pendingupdateflags = 0;

	//initial GPU buffers allocation
	for (uint8_t ibuf = 0; ibuf < 3; ibuf++) {
		spritebatch->vertices[ibuf] = render_gen_buffer(render_mngr, spritebatch->vertexBuffer, (maxSprites * VERTICES_PER_SPRITE * VERTEX_SIZE) * sizeof(float), wtrue, wtrue);
		spritebatch->textcoord[ibuf] = render_gen_buffer(render_mngr, spritebatch->textcoordBuffer, (maxSprites * VERTICES_PER_SPRITE * VERTEX_SIZE) * sizeof(float), wtrue, wtrue);
		spritebatch->colors[ibuf] = render_gen_buffer(render_mngr, spritebatch->colorBuffer, (maxSprites * VERTICES_PER_SPRITE * COLOR_SIZE) * sizeof(float), wtrue, wtrue);
		spritebatch->bufferupdated[ibuf] = wtrue;
		spritebatch->buffersync[ibuf] = NULL;
	}

	spritebatch->dirty_sprite = (wbool*)wf_malloc(maxSprites * sizeof(wbool));
	memset(spritebatch->dirty_sprite,0,maxSprites * sizeof(wbool));
	
	spritebatch->maxSprites = maxSprites;// Save Maximum Sprites
	spritebatch->numSprites = 0;// Clear Sprite Counter
	spritebatch->position.x = spritebatch->position.y = 0.0f;
	spritebatch->rotation = 0.0f;
	spritebatch->scale_x = 1.0f;
	spritebatch->scale_y = 1.0f;
}

void SpriteBatch_Free(SpriteBatch_t * spritebatch)
{
	wf_free(spritebatch->vertexBuffer);
	wf_free(spritebatch->textcoordBuffer);
	wf_free(spritebatch->dirty_sprite);
	wf_free(spritebatch->colorBuffer);

	spritebatch->vertexBuffer = NULL;
	spritebatch->textcoordBuffer = NULL;
	spritebatch->colorBuffer = NULL;
	spritebatch->dirty_sprite = NULL;

	SpriteBatch_g_clean(spritebatch);
}

void SpriteBatch_recycle(SpriteBatch_t * spritebatch, render_manager_t* const render_mngr)
{
	SpriteBatch_g_clean(spritebatch);

	int32_t vert_size = (spritebatch->maxSprites * VERTICES_PER_SPRITE * VERTEX_SIZE) * sizeof(float);
	int32_t color_size = (spritebatch->maxSprites * VERTICES_PER_SPRITE * COLOR_SIZE) * sizeof(float);

	memset(spritebatch->vertexBuffer, 0, vert_size);
	memset(spritebatch->textcoordBuffer, 0, vert_size);
	memset(spritebatch->dirty_sprite, 0, spritebatch->maxSprites * sizeof(wbool));
	memset(spritebatch->colorBuffer, 0, color_size);

	spritebatch->numSprites = 0;// Clear Sprite Counter
	SpriteBatch_g_create(spritebatch, render_mngr);
}

void SpriteBatch_g_clean(SpriteBatch_t* const spritebatch)
{
	for (uint8_t ibuf = 0; ibuf < 3; ibuf++) {
		render_delete_buffer(spritebatch->vertices[ibuf]);
		render_delete_buffer(spritebatch->textcoord[ibuf]);
		render_delete_buffer(spritebatch->colors[ibuf]);

		if (spritebatch->buffersync[ibuf] != NULL) {
			render_removefencesync(spritebatch->buffersync[ibuf]);
			spritebatch->buffersync[ibuf] = NULL;
		}
			
		spritebatch->bufferupdated[ibuf] = wtrue;
	}
}

void SpriteBatch_g_create(SpriteBatch_t* const spritebatch,render_manager_t* const render_mngr)
{
	/*spritebatch->bufferswitch = wfalse;
	spritebatch->waitforsync = wfalse;*/
	spritebatch->pendingupdateflags = 0;

	//initial GPU buffers allocation
	for (uint8_t ibuf = 0; ibuf < 3; ibuf++) {
		spritebatch->vertices[ibuf] = render_gen_buffer(render_mngr, spritebatch->vertexBuffer, (spritebatch->maxSprites * VERTICES_PER_SPRITE * VERTEX_SIZE) * sizeof(float), wtrue, wtrue);
		spritebatch->textcoord[ibuf] = render_gen_buffer(render_mngr, spritebatch->textcoordBuffer, (spritebatch->maxSprites * VERTICES_PER_SPRITE * VERTEX_SIZE) * sizeof(float), wtrue, wtrue);
		spritebatch->colors[ibuf] = render_gen_buffer(render_mngr, spritebatch->colorBuffer, (spritebatch->maxSprites * VERTICES_PER_SPRITE * COLOR_SIZE) * sizeof(float), wtrue, wtrue);
		spritebatch->bufferupdated[ibuf] = wtrue;
		spritebatch->buffersync[ibuf] = NULL;
	}

}

static void _SpriteBatch_updatebuffer(SpriteBatch_t * spritebatch, render_manager_t* const render_mngr, int16_t update_flag, const int buffer_number)
{

	int32_t indx_sprite = 0;

	while (indx_sprite < spritebatch->numSprites)
		spritebatch->dirty_sprite[indx_sprite++] = wfalse;

	int32_t numupdate = spritebatch->numSprites;

	if ((update_flag & BUFFER_UPDATE_FULL) > 0) {
		numupdate = spritebatch->maxSprites;
	}

	if ((update_flag & BUFFER_UPDATE_VERTICES) > 0)
	{
		render_unsync_updatebuffer(render_mngr, spritebatch->vertices[buffer_number], spritebatch->vertexBuffer, (numupdate * VERTICES_PER_SPRITE * VERTEX_SIZE) * sizeof(float));
	}

	if ((update_flag & BUFFER_UPDATE_TEXCOORDS) > 0)
	{
		render_unsync_updatebuffer(render_mngr, spritebatch->textcoord[buffer_number], spritebatch->textcoordBuffer, (numupdate * VERTICES_PER_SPRITE * VERTEX_SIZE) * sizeof(float));
	}

	if ((update_flag & BUFFER_UPDATE_COLORS) > 0)
	{
		render_unsync_updatebuffer(render_mngr, spritebatch->colors[buffer_number], spritebatch->colorBuffer, (numupdate * VERTICES_PER_SPRITE * COLOR_SIZE) * sizeof(float));
	}

	spritebatch->bufferupdated[buffer_number] = wtrue;

}


void SpriteBatch_draw(SpriteBatch_t * spritebatch,render_manager_t* const render_mngr,TEXTURE_ID texture_id)
{
	if (spritebatch->numSprites > 0)
	{
		render_set_position(render_mngr, spritebatch->position);
		render_set_scale(render_mngr, spritebatch->scale_x, spritebatch->scale_y, 1.0f);
		render_set_rotation(render_mngr, spritebatch->rotation);

		const int buffer_number = render_mngr->frame_number % 3;

		if (!spritebatch->bufferupdated[buffer_number]) {

			if (spritebatch->buffersync[buffer_number] != NULL) {
				if (!render_fencesyncisready(render_mngr, spritebatch->buffersync[buffer_number])) {
					logprint("Error with buffer transfers, synchronisation occurred...");
					return;
				}

				render_removefencesync(spritebatch->buffersync[buffer_number]);
				spritebatch->buffersync[buffer_number] = NULL;
			}


			_SpriteBatch_updatebuffer(spritebatch, render_mngr, spritebatch->pendingupdateflags, buffer_number);
		}
		else if (spritebatch->buffersync[buffer_number] != NULL) {
			render_removefencesync(spritebatch->buffersync[buffer_number]);
			spritebatch->buffersync[buffer_number] = NULL;
		}
		

		if (texture_id != NULL)
			render_start_draw_buffer(render_mngr, texture_id, spritebatch->vertices[buffer_number], spritebatch->textcoord[buffer_number], spritebatch->colors[buffer_number], VERTEX_SIZE);
		else
			render_start_draw_buffer(render_mngr, NULL, spritebatch->vertices[buffer_number], 0, spritebatch->colors[buffer_number], VERTEX_SIZE);

		render_draw_buffer(render_mngr, 0, (spritebatch->numSprites * VERTICES_PER_SPRITE));

		render_end_draw_buffer(render_mngr, wtrue, wtrue);

		spritebatch->buffersync[buffer_number] = render_addfencesync(render_mngr);
	}
}

void SpriteBatch_scheduleupdatebuffer(SpriteBatch_t * spritebatch, int16_t update_flag)
{
	for (uint8_t ibuf = 0; ibuf < 3; ibuf++) {
		spritebatch->bufferupdated[ibuf] = wfalse;
		spritebatch->pendingupdateflags = update_flag;
	}
}

void SpriteBatch_cleanupsprite(SpriteBatch_t * const spritebatch, int32_t sprite)
{
	int32_t buffer_index = sprite * VERTICES_PER_SPRITE * VERTEX_SIZE;
	int32_t textcoord_index = sprite * VERTICES_PER_SPRITE * VERTEX_SIZE;
	int32_t color_index = sprite * VERTICES_PER_SPRITE * COLOR_SIZE;

	for (uint8_t ivert = 0; ivert < VERTICES_PER_SPRITE; ivert++)
	{
		spritebatch->vertexBuffer[buffer_index++] = WFLOAT0;               // clean X for Vertex ivert
		spritebatch->vertexBuffer[buffer_index++] = WFLOAT0;               // clean Y for Vertex ivert
		spritebatch->textcoordBuffer[textcoord_index++] = WFLOAT0;        // clean U for Vertex ivert
		spritebatch->textcoordBuffer[textcoord_index++] = WFLOAT0;        // clean V for Vertex ivert

		spritebatch->colorBuffer[color_index++] = WFLOAT0; //clean R for vertex ivert
		spritebatch->colorBuffer[color_index++] = WFLOAT0;//clean G for vertex ivert
		spritebatch->colorBuffer[color_index++] = WFLOAT0;//clean B for vertex ivert
		spritebatch->colorBuffer[color_index++] = WFLOAT0;//clean A for vertex ivert
	}
}

void SpriteBatch_cleanupbuffer(SpriteBatch_t * const spritebatch, int32_t startsprite)
{
	for (int32_t isprite = startsprite; isprite < spritebatch->maxSprites; isprite++) {
		SpriteBatch_cleanupsprite(spritebatch, isprite);
	}

}


void SpriteBatch_updateSprite(SpriteBatch_t * spritebatch,int32_t isprite,wfloat x, wfloat y, wfloat width, wfloat height,const TextureRegion_t* region, const ColorA_t color,render_manager_t* const render_mngr)
{

	int32_t buffer_index = isprite * VERTICES_PER_SPRITE * VERTEX_SIZE;
	int32_t textcoord_index = isprite * VERTICES_PER_SPRITE * VERTEX_SIZE;
	int32_t color_index = isprite * VERTICES_PER_SPRITE * COLOR_SIZE;

	wfloat x1 = x;
	wfloat y1 = y - (height);
	wfloat x2 = x + (width);
	wfloat y2 = y;

	for (uint8_t ivert = 0; ivert < VERTICES_PER_SPRITE; ivert++) {
		spritebatch->colorBuffer[color_index++] = color.r; //add R for vertex ivert
		spritebatch->colorBuffer[color_index++] = color.g;//add G for vertex ivert
		spritebatch->colorBuffer[color_index++] = color.b;//add B for vertex ivert
		spritebatch->colorBuffer[color_index++] = color.a;//add A for vertex ivert
	}	

    spritebatch->vertexBuffer[buffer_index++] = x1;               // Add X for Vertex 0
    spritebatch->vertexBuffer[buffer_index++] = y1;               // Add Y for Vertex 0
	spritebatch->textcoordBuffer[textcoord_index++] = region->u1;        // Add U for Vertex 0
    spritebatch->textcoordBuffer[textcoord_index++] = region->v2;        // Add V for Vertex 0

    spritebatch->vertexBuffer[buffer_index++] = x2;               // Add X for Vertex 1
    spritebatch->vertexBuffer[buffer_index++] = y1;               // Add Y for Vertex 1
    spritebatch->textcoordBuffer[textcoord_index++] = region->u2;        // Add U for Vertex 1
    spritebatch->textcoordBuffer[textcoord_index++] = region->v2;        // Add V for Vertex 1

    spritebatch->vertexBuffer[buffer_index++] = x2;               // Add X for Vertex 2
    spritebatch->vertexBuffer[buffer_index++] = y2;               // Add Y for Vertex 2
    spritebatch->textcoordBuffer[textcoord_index++] = region->u2;        // Add U for Vertex 2
    spritebatch->textcoordBuffer[textcoord_index++] = region->v1;        // Add V for Vertex 2


	spritebatch->vertexBuffer[buffer_index++] = x1;               // Add X for Vertex 3
    spritebatch->vertexBuffer[buffer_index++] = y1;               // Add Y for Vertex 3
	spritebatch->textcoordBuffer[textcoord_index++] = region->u1;        // Add U for Vertex 3
    spritebatch->textcoordBuffer[textcoord_index++] = region->v2;        // Add V for Vertex 3

	spritebatch->vertexBuffer[buffer_index++] = x2;               // Add X for Vertex 4
    spritebatch->vertexBuffer[buffer_index++] = y2;               // Add Y for Vertex 4
    spritebatch->textcoordBuffer[textcoord_index++] = region->u2;        // Add U for Vertex 4
    spritebatch->textcoordBuffer[textcoord_index++] = region->v1;        // Add V for Vertex 4

    spritebatch->vertexBuffer[buffer_index++] = x1;               // Add X for Vertex 5
    spritebatch->vertexBuffer[buffer_index++] = y2;               // Add Y for Vertex 5
    spritebatch->textcoordBuffer[textcoord_index++] = region->u1;        // Add U for Vertex 5
    spritebatch->textcoordBuffer[textcoord_index++] = region->v1;        // Add V for Vertex 5

	spritebatch->dirty_sprite[isprite] = wtrue;
}

void SpriteBatch_updateSprite2(SpriteBatch_t * const spritebatch,int32_t isprite,wfloat x, wfloat y, sprite_t* const sprite,render_manager_t* const render_mngr)
{
	int32_t buffer_index = isprite * VERTICES_PER_SPRITE * VERTEX_SIZE;
	int32_t textcoord_index = isprite * VERTICES_PER_SPRITE * VERTEX_SIZE;
	int32_t color_index = isprite * VERTICES_PER_SPRITE * COLOR_SIZE;

	int32_t sp_v_indx = 0;
	int32_t sp_t_indx = 0;


	int32_t num_sprite = (sprite->repeat_x + 1) * (sprite->repeat_y + 1);

	for(int32_t i = 0; i < ((sprite->repeat_x + 1) * (sprite->repeat_y + 1) * 6);i++)
	{
		spritebatch->vertexBuffer[buffer_index++] = x + sprite->vertices[sp_v_indx++];               // Add X for Vertex 0
		spritebatch->vertexBuffer[buffer_index++] = y + sprite->vertices[sp_v_indx++];               // Add Y for Vertex 0
		spritebatch->textcoordBuffer[textcoord_index++] = sprite->tex_coords[sp_t_indx++];        // Add U for Vertex 0
		spritebatch->textcoordBuffer[textcoord_index++] = sprite->tex_coords[sp_t_indx++];        // Add V for Vertex 0

		spritebatch->colorBuffer[color_index++] = sprite->color.r; //add R for vertex ivert
		spritebatch->colorBuffer[color_index++] = sprite->color.g;//add G for vertex ivert
		spritebatch->colorBuffer[color_index++] = sprite->color.b;//add B for vertex ivert
		spritebatch->colorBuffer[color_index++] = sprite->color.a;//add A for vertex ivert
	}

	for(int32_t i = 0;i < num_sprite;i++)
		spritebatch->dirty_sprite[isprite + i] = wtrue;

}

int32_t SpriteBatch_AddSprite(SpriteBatch_t * spritebatch,wfloat x, wfloat y, wfloat width, wfloat height,const TextureRegion_t* region, const ColorA_t color, render_manager_t* const render_mngr)
{
	int32_t nsprite = spritebatch->numSprites;

	int32_t buffer_index = spritebatch->numSprites * VERTICES_PER_SPRITE * VERTEX_SIZE;
	int32_t textcoord_index = spritebatch->numSprites * VERTICES_PER_SPRITE * VERTEX_SIZE;
	int32_t color_index = spritebatch->numSprites * VERTICES_PER_SPRITE * COLOR_SIZE;

	if( spritebatch->numSprites >= spritebatch->maxSprites)
	{
		logprint("Max number of sprite reached!");
		return -1;
	}

	float x1 = x;
	float y1 = y - (height);
	float x2 = x + (width);
	float y2 = y;

	for (uint8_t ivert = 0; ivert < VERTICES_PER_SPRITE; ivert++) {
		spritebatch->colorBuffer[color_index++] = color.r; //add R for vertex ivert
		spritebatch->colorBuffer[color_index++] = color.g;//add G for vertex ivert
		spritebatch->colorBuffer[color_index++] = color.b;//add B for vertex ivert
		spritebatch->colorBuffer[color_index++] = color.a;//add A for vertex ivert
	}

    spritebatch->vertexBuffer[buffer_index++] = x1;               // Add X for Vertex 0
    spritebatch->vertexBuffer[buffer_index++] = y1;               // Add Y for Vertex 0
	spritebatch->textcoordBuffer[textcoord_index++] = region->u1;        // Add U for Vertex 0
    spritebatch->textcoordBuffer[textcoord_index++] = region->v2;        // Add V for Vertex 0

    spritebatch->vertexBuffer[buffer_index++] = x2;               // Add X for Vertex 1
    spritebatch->vertexBuffer[buffer_index++] = y1;               // Add Y for Vertex 1
    spritebatch->textcoordBuffer[textcoord_index++] = region->u2;        // Add U for Vertex 1
    spritebatch->textcoordBuffer[textcoord_index++] = region->v2;        // Add V for Vertex 1

    spritebatch->vertexBuffer[buffer_index++] = x2;               // Add X for Vertex 2
    spritebatch->vertexBuffer[buffer_index++] = y2;               // Add Y for Vertex 2
    spritebatch->textcoordBuffer[textcoord_index++] = region->u2;        // Add U for Vertex 2
    spritebatch->textcoordBuffer[textcoord_index++] = region->v1;        // Add V for Vertex 2


	spritebatch->vertexBuffer[buffer_index++] = x1;               // Add X for Vertex 3
    spritebatch->vertexBuffer[buffer_index++] = y1;               // Add Y for Vertex 3
	spritebatch->textcoordBuffer[textcoord_index++] = region->u1;        // Add U for Vertex 3
    spritebatch->textcoordBuffer[textcoord_index++] = region->v2;        // Add V for Vertex 3

	spritebatch->vertexBuffer[buffer_index++] = x2;               // Add X for Vertex 4
    spritebatch->vertexBuffer[buffer_index++] = y2;               // Add Y for Vertex 4
    spritebatch->textcoordBuffer[textcoord_index++] = region->u2;        // Add U for Vertex 4
    spritebatch->textcoordBuffer[textcoord_index++] = region->v1;        // Add V for Vertex 4

    spritebatch->vertexBuffer[buffer_index++] = x1;               // Add X for Vertex 5
    spritebatch->vertexBuffer[buffer_index++] = y2;               // Add Y for Vertex 5
    spritebatch->textcoordBuffer[textcoord_index++] = region->u1;        // Add U for Vertex 5
    spritebatch->textcoordBuffer[textcoord_index++] = region->v1;        // Add V for Vertex 5

	spritebatch->dirty_sprite[spritebatch->numSprites] = wtrue;

    spritebatch->numSprites++;                                   // Increment Sprite Count
	return nsprite;
}

int32_t SpriteBatch_AddSprite2(SpriteBatch_t * spritebatch,wfloat x,wfloat y,sprite_t* const sprite,render_manager_t* const render_mngr)
{
	int32_t nsprite = spritebatch->numSprites;
	int32_t buffer_index = spritebatch->numSprites * VERTICES_PER_SPRITE * VERTEX_SIZE;
	int32_t textcoord_index = spritebatch->numSprites * VERTICES_PER_SPRITE * VERTEX_SIZE;
	int32_t color_index = spritebatch->numSprites * VERTICES_PER_SPRITE * COLOR_SIZE;

	if( spritebatch->numSprites >= spritebatch->maxSprites)
	{
		logprint("Max number of sprite reached!");
		return -1;
	}

	int32_t sp_v_indx = 0;
	int32_t sp_t_indx = 0;


	int32_t num_sprite = (sprite->repeat_x + 1) * (sprite->repeat_y + 1);

	for(int32_t i = 0; i < ((sprite->repeat_x + 1) * (sprite->repeat_y + 1) * 6);i++)
	{
		spritebatch->vertexBuffer[buffer_index++] = x + sprite->vertices[sp_v_indx++];               // Add X for Vertex 0
		spritebatch->vertexBuffer[buffer_index++] = y + sprite->vertices[sp_v_indx++];               // Add Y for Vertex 0
		spritebatch->textcoordBuffer[textcoord_index++] = sprite->tex_coords[sp_t_indx++];        // Add U for Vertex 0
		spritebatch->textcoordBuffer[textcoord_index++] = sprite->tex_coords[sp_t_indx++];        // Add V for Vertex 0

		spritebatch->colorBuffer[color_index++] = sprite->color.r; //add R for vertex ivert
		spritebatch->colorBuffer[color_index++] = sprite->color.g;//add G for vertex ivert
		spritebatch->colorBuffer[color_index++] = sprite->color.b;//add B for vertex ivert
		spritebatch->colorBuffer[color_index++] = sprite->color.a;//add A for vertex ivert
	}

	for(int32_t i = 0;i < num_sprite;i++)
	{
		spritebatch->dirty_sprite[spritebatch->numSprites] = wtrue;

		spritebatch->numSprites++;                                   // Increment Sprite Count
	}

	return nsprite;
}

int32_t SpriteBatch_AddSprite3(SpriteBatch_t* const spritebatch,Vector_t* const a,Vector_t* const b,Vector_t* const c,Vector_t* const d, const ColorA_t color)
{
	int32_t nsprite = spritebatch->numSprites;
	int32_t buffer_index = spritebatch->numSprites * VERTICES_PER_SPRITE * VERTEX_SIZE;
	int32_t textcoord_index = spritebatch->numSprites * VERTICES_PER_SPRITE * VERTEX_SIZE;
	int32_t color_index = spritebatch->numSprites * VERTICES_PER_SPRITE * COLOR_SIZE;

	if( spritebatch->numSprites >= spritebatch->maxSprites)
	{
		logprint("Max number of sprite reached!");
		return -1;
	}

	for (uint8_t ivert = 0; ivert < VERTICES_PER_SPRITE; ivert++) {
		spritebatch->colorBuffer[color_index++] = color.r; //add R for vertex ivert
		spritebatch->colorBuffer[color_index++] = color.g;//add G for vertex ivert
		spritebatch->colorBuffer[color_index++] = color.b;//add B for vertex ivert
		spritebatch->colorBuffer[color_index++] = color.a;//add A for vertex ivert
	}

    spritebatch->vertexBuffer[buffer_index++] = a->x;               // Add X for Vertex 0
	spritebatch->vertexBuffer[buffer_index++] = a->y;               // Add Y for Vertex 0
	spritebatch->textcoordBuffer[textcoord_index++] = 0.0f;        // Add U for Vertex 0
    spritebatch->textcoordBuffer[textcoord_index++] = 0.0f;        // Add V for Vertex 0

	spritebatch->vertexBuffer[buffer_index++] = b->x;               // Add X for Vertex 1
	spritebatch->vertexBuffer[buffer_index++] = b->y;               // Add Y for Vertex 1
    spritebatch->textcoordBuffer[textcoord_index++] = 0.0f;        // Add U for Vertex 1
    spritebatch->textcoordBuffer[textcoord_index++] = 0.0f;        // Add V for Vertex 1

    spritebatch->vertexBuffer[buffer_index++] = c->x;               // Add X for Vertex 2
	spritebatch->vertexBuffer[buffer_index++] = c->y;               // Add Y for Vertex 2
    spritebatch->textcoordBuffer[textcoord_index++] = 0.0f;        // Add U for Vertex 2
    spritebatch->textcoordBuffer[textcoord_index++] = 0.0f;        // Add V for Vertex 2


	spritebatch->vertexBuffer[buffer_index++] = c->x;               // Add X for Vertex 3
	spritebatch->vertexBuffer[buffer_index++] = c->y;               // Add Y for Vertex 3
	spritebatch->textcoordBuffer[textcoord_index++] = 0.0f;        // Add U for Vertex 3
    spritebatch->textcoordBuffer[textcoord_index++] = 0.0f;        // Add V for Vertex 3

	spritebatch->vertexBuffer[buffer_index++] = d->x;               // Add X for Vertex 4
	spritebatch->vertexBuffer[buffer_index++] = d->y;               // Add Y for Vertex 4
    spritebatch->textcoordBuffer[textcoord_index++] = 0.0f;        // Add U for Vertex 4
    spritebatch->textcoordBuffer[textcoord_index++] = 0.0f;        // Add V for Vertex 4

	spritebatch->vertexBuffer[buffer_index++] = a->x;               // Add X for Vertex 5
	spritebatch->vertexBuffer[buffer_index++] = a->y;               // Add Y for Vertex 5
    spritebatch->textcoordBuffer[textcoord_index++] = 0.0f;        // Add U for Vertex 5
    spritebatch->textcoordBuffer[textcoord_index++] = 0.0f;        // Add V for Vertex 5

	spritebatch->dirty_sprite[spritebatch->numSprites] = wtrue;

    spritebatch->numSprites++;                                   // Increment Sprite Count
	return nsprite;

}

void SpriteBatch_RemoveSprite2(SpriteBatch_t * spritebatch, sprite_t* const sprite,int32_t index, render_manager_t* const render_mngr)
{
	int32_t buffer_index = index * VERTICES_PER_SPRITE * VERTEX_SIZE;
	int32_t textcoord_index = index * VERTICES_PER_SPRITE * VERTEX_SIZE;
	int32_t color_index = spritebatch->numSprites * VERTICES_PER_SPRITE * COLOR_SIZE;

	if (spritebatch->numSprites <= 0)
	{
		logprint("Can't remove sprite, num sprite is 0 !");
		return;
	}


	int32_t num_sprite = (sprite->repeat_x + 1) * (sprite->repeat_y + 1);

	int32_t endverts = ((sprite->repeat_x + 1) * (sprite->repeat_y + 1) * 6);
	int32_t endtex = endverts;
	int32_t endcolor = endverts;

	int32_t maxbuffer = spritebatch->numSprites  * VERTICES_PER_SPRITE;


	for (int32_t i = (index * VERTICES_PER_SPRITE); i < (maxbuffer - (VERTICES_PER_SPRITE * num_sprite)); i++)
	{
		spritebatch->vertexBuffer[buffer_index] = spritebatch->vertexBuffer[buffer_index + endverts];
		buffer_index++;
		spritebatch->vertexBuffer[buffer_index] = spritebatch->vertexBuffer[buffer_index + endverts];
		buffer_index++;

		spritebatch->textcoordBuffer[textcoord_index] = spritebatch->textcoordBuffer[textcoord_index + endtex];
		textcoord_index++;
		spritebatch->textcoordBuffer[textcoord_index] = spritebatch->textcoordBuffer[textcoord_index + endtex];
		textcoord_index++;

		spritebatch->colorBuffer[color_index] = spritebatch->colorBuffer[color_index + endcolor];
		color_index++;
		spritebatch->colorBuffer[color_index] = spritebatch->colorBuffer[color_index + endcolor];
		color_index++;
		spritebatch->colorBuffer[color_index] = spritebatch->colorBuffer[color_index + endcolor];
		color_index++;
		spritebatch->colorBuffer[color_index] = spritebatch->colorBuffer[color_index + endcolor];
		color_index++;
	}
	

	spritebatch->numSprites-= num_sprite;

	for (int32_t i = index; i < spritebatch->numSprites; i++)
	{
		spritebatch->dirty_sprite[i] = wtrue;
	}

}
