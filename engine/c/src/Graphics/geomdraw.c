#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include <math.h>

#include <Engine.h>

#include <Render.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>
#include <Base/geom.h>

#include <Render/render_manager.h>

#include <Graphics/geomdraw.h>

void drawrect(const Rect_t* rect,ColorA_t color,render_manager_t* const render_mngr)
{
	//CCW
    float vertices[] = { 
			0,
			0,

			(float)rect->width,
			-((float)rect->height),

			(float)rect->width,
			0,

			0,
			0,

			0,
			-((float)rect->height),

			(float)rect->width,
			-((float)rect->height)
	};

	render_push_matrix(render_mngr);

	render_set_position(render_mngr,rect->position);

	render_set_color(render_mngr,color);

	render_start_draw_array(render_mngr,2,6,vertices,NULL);

	render_draw_array(render_mngr,0,6);
	render_end_draw_array(render_mngr,wfalse);

	render_pop_matrix(render_mngr);

	render_reset_color(render_mngr);
}

void drawcircle(const Circle_t* circle,ColorA_t color,render_manager_t* const render_mngr)
{
	float vertices[144];

	int32_t ivertices = 0;

	for(int32_t angle = 0; angle < 360;angle += 15)
	{
		//position of vertices
		vertices[ivertices] = 0.0f;
		vertices[ivertices+1] = 0.0f;

		wfloat radval = deg2rad((float)angle);
		wfloat radval2 = deg2rad((float)(angle + 15));

		vertices[ivertices+2] = circle->radius * cosf((float)radval);
		vertices[ivertices+3] = circle->radius * sinf((float)radval);

		vertices[ivertices+4] = circle->radius * cosf((float)radval2);
		vertices[ivertices+5] = circle->radius * sinf((float)radval2);

		ivertices += 6;
	}


	render_push_matrix(render_mngr);

	render_set_position(render_mngr,circle->position);

	render_set_color(render_mngr,color);

	render_start_draw_array(render_mngr,2,72,vertices,NULL);

	render_draw_array(render_mngr,0,72);
	render_end_draw_array(render_mngr,wfalse);

	render_pop_matrix(render_mngr);

	render_reset_color(render_mngr);
}
