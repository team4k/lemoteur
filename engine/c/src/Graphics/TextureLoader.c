#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <math.h>

#include <Render.h>

#include <Engine.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/geom.h>
#include <Base/Color.h>

#include "Debug/Logprint.h"

#include <Render/render_manager.h>

#include <Graphics/TextureLoader.h>

wbool load_texture(texture_info* texture, unsigned char* png_buffer, size_t buffer_size, const char* filePath, render_manager_t* const render_mngr, wbool backgrounload)
{
	return load_texture_withfilter(texture, png_buffer, buffer_size, filePath, render_mngr, render_mngr->texture_filtering, backgrounload);
}

wbool load_texture_deferred(texture_info* texture, render_manager_t* const render_mngr)
{
	if (texture->deferredload)
	{
		texture->texture_pointer = render_gen_texture_withfilter(render_mngr, texture->tmpimage->width, texture->tmpimage->height, texture->tmpbuffer, PNG_IMAGE_SIZE(*texture->tmpimage), wfalse, texture->filter);
		logprint("Load deferred png texture %s %d X %d \n", texture->texture_name,texture->tmpimage->width, texture->tmpimage->height);
		wf_free(texture->tmpbuffer);
		wf_free(texture->tmpimage);
		texture->tmpbuffer = NULL;
		texture->tmpimage = NULL;
		texture->deferredload = wfalse;
		return wtrue;
	}
	
	return wfalse;
}

wbool load_texture_withfilter(texture_info* texture, unsigned char* png_buffer,size_t buffer_size,const char* filePath,render_manager_t* const render_mngr,texture_filter filter,wbool backgrounload)
{ 
      png_imagep image = (png_imagep)wf_malloc(sizeof(png_image));

      /* Only the image structure version number needs to be set. */
      memset(image, 0, sizeof(png_image));
      image->version = PNG_IMAGE_VERSION;
	  texture->tmpbuffer = NULL;
	  texture->tmpimage = NULL;
	  texture->filter = filter;

	  texture->deferredload = backgrounload;

      if (png_image_begin_read_from_memory(image,png_buffer,buffer_size))
      {
         png_bytep buffer;

         /* Change this to try different formats!  If you set a colormap format
          * then you must also supply a colormap below.
          */
		  //variables to pass to get info
		/*int bit_depth, color_type;
		png_uint_32 twidth, theight;

		// get info about png
		png_get_IHDR(png_ptr, info_ptr, &twidth, &theight, &bit_depth, &color_type,
			NULL, NULL, NULL);*/

         image->format = PNG_FORMAT_RGBA;

         buffer = (png_bytep)wf_malloc(PNG_IMAGE_SIZE(*image));

         if (buffer != NULL)
         {
            if (png_image_finish_read(image, NULL/*background*/, buffer,
               0/*row_stride*/, NULL/*colormap for PNG_FORMAT_FLAG_COLORMAP */))
            {
                texture->width = image->width;
                texture->height = image->height;

                //generate Texture from png file data in buffer
				if (!backgrounload) {
					texture->texture_pointer = render_gen_texture_withfilter(render_mngr, image->width, image->height, buffer, PNG_IMAGE_SIZE(*image), wfalse, filter);
					logprint("Loaded png texture %s %d X %d \n", filePath, image->width, image->height);
					wf_free(buffer);
					buffer = NULL;
					wf_free(image);
					image = NULL;
					
				}
				else {
					texture->texture_pointer = NULL;
					texture->tmpbuffer = buffer;
					texture->tmpimage = image;
					logprint("Deferred png texture %s %d X %d \n", filePath, image->width, image->height);
				}

               

                return wtrue;
            }
            else
            {
                logprint("Error loading png file %s : %s\n",filePath,image->message);

               /* This is the only place where a 'free' is required; libpng does
                * the cleanup on error and success, but in this case we couldn't
                * complete the read because of running out of memory.
                */
               png_image_free(image);
            }
         }

         else
         {
               logprint("Error loading png file, out of memory %s : %lu bytes\n",filePath, (unsigned long)PNG_IMAGE_SIZE(*image));
         }
      }
      else
      {
           logprint("Error loading png file, can't read file %s : %s \n",filePath, image->message);
      }

      return wfalse;
}


wbool load_raw_texture(texture_info* texture,unsigned char* textureData,unsigned long texturedata_size,render_manager_t* const render_mngr)
{

	texture->texture_pointer = render_gen_texture(render_mngr,texture->width,texture->height,textureData,texturedata_size,wfalse);

	logprint("Loaded raw texture %d X %d \n",texture->width, texture->height,wfalse);

	return wtrue;
}
