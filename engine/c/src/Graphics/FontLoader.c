#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <math.h>



#include <Render.h>

#include <Engine.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>
#include <Base/geom.h>

#include <Render/render_manager.h>

#include <png.h>

#include "Debug/Logprint.h"

#include <Font/Fontglyph.h>

#include <Graphics/TextureRegion.h>
#include <Graphics/FontLoader.h>

#include <Utils/jsmn.h>

#define JSONTMPSZ 512

static int jsoneq(const char *json, jsmntok_t *tok, const char *s) {
	if (tok->type == JSMN_STRING && (int)strlen(s) == tok->end - tok->start &&
		strncmp(json + tok->start, s, tok->end - tok->start) == 0) {
		return 0;
	}
	return -1;
}

static void jsoncpy(const char* json, jsmntok_t* tok, char* buffer, size_t buffer_size)
{
	size_t size = tok->end - tok->start;

	if (size > buffer_size) {
		size = buffer_size;
	}

	memset(buffer, 0, buffer_size);
	w_memcpy(buffer, size, json + tok->start, size);
}

static int32_t jsongetint(const char* json, jsmntok_t* tok, char* buffer, size_t buffer_size)
{
	jsoncpy(json, tok, buffer, buffer_size);
	return atoi(buffer);
}

static float jsongetfloat(const char* json, jsmntok_t* tok, char* buffer, size_t buffer_size)
{
	jsoncpy(json, tok, buffer, buffer_size);
	return (float)atof(buffer);
}

static const char* json_prettyerror(int err)
{
	switch (err)
	{
		case  JSMN_ERROR_NOMEM:
			return "JSON buffer too small";
		case JSMN_ERROR_INVAL:
			return "Invalid value in JSON";
		case JSMN_ERROR_PART:
			return "Not a full JSON packet";
	}

	return "Unknown error";
}

wbool load_font(font_info* font, const char* font_name, unsigned char* font_data, long lSize, render_manager_t* const render_mngr)
{
	return load_font2(font, font_name, font_data, lSize, render_mngr, 0, 0);
}


wbool load_font2(font_info* font, const char* font_name, unsigned char* font_data, long lSize, render_manager_t* const render_mngr, unsigned long char_start, unsigned long char_end)
{

	unsigned long char_count = 0;

	if ((char_start == 0 || char_end == 0) || (char_end < char_start) || char_start < 32)
	{
		char_start = CHAR_START;
		char_end = CHAR_END;
		char_count = CHAR_CNT;
	}
	else
	{
		char_count = ((char_end - char_start) + 1);
	}

	font->glyphs = (Fontglyph_t*)wf_malloc(char_count * sizeof(Fontglyph_t));// Create the Array of font glyphs
	font->char_start = char_start;
	font->char_end = char_end;
	font->char_cnt = char_count;
	w_strcpy(font->font_name, font_name);




	//load font data in json format
	jsmn_parser p = { 0 };
	jsmntok_t t[4096]; /* We expect no more than 4096 tokens */

	jsmn_init(&p);

	const char* json_string = (const char*)font_data;

	int root = jsmn_parse(&p, json_string, lSize, t,
		sizeof(t) / sizeof(t[0]));

	if (root < 1 || t[0].type != JSMN_OBJECT) {
		logprint("Error while loading font data in JSON format ,%s", json_prettyerror(root));
		return wfalse;
	}

	char json_tmp[JSONTMPSZ] = { 0 };

	for (int32_t i = 1; i < root; i++) {
		if (jsoneq(json_string, &t[i], "atlas") == 0) {

			//get atlas information
			if (t[i + 1].type == JSMN_OBJECT) {

				int32_t size = t[i + 1].size;
				i += 2;

				for (int32_t j = 0; j < size; j++) {

					jsmntok_t *g = &t[i];

					if (jsoneq(json_string, g, "width") == 0) {
						jsmntok_t* value = &t[i +1];
						font->width = jsongetint(json_string, value, json_tmp, JSONTMPSZ);
					}
					else if (jsoneq(json_string, g, "height") == 0) {
						jsmntok_t* value = &t[i + 1];
						font->height = jsongetint(json_string, value, json_tmp, JSONTMPSZ);
					}
					else if (jsoneq(json_string, g, "distanceRange") == 0) {
						jsmntok_t* value = &t[i + 1];
						font->distanceRange = jsongetint(json_string, value, json_tmp, JSONTMPSZ);
					}

					i += 2;
				}

				i--;

			}
		}
		else if (jsoneq(json_string, &t[i], "metrics") == 0) {
			//get metrics information
			if (t[i + 1].type == JSMN_OBJECT) {

				int32_t size = t[i + 1].size;
				i += 2;

				for (int32_t j = 0; j < size; j++) {
					jsmntok_t *g = &t[i];
					if (jsoneq(json_string, g, "ascender") == 0) {
						jsmntok_t* value = &t[i + 1];
						font->fontAscent = jsongetfloat(json_string, value, json_tmp, JSONTMPSZ);
					}
					else if (jsoneq(json_string, g, "descender") == 0) {
						jsmntok_t* value = &t[i + 1];
						font->fontDescent = jsongetfloat(json_string, value, json_tmp, JSONTMPSZ);
					}
					else if (jsoneq(json_string, g, "emSize") == 0) {
						jsmntok_t* value = &t[i + 1];
						font->emSize = jsongetint(json_string, value, json_tmp, JSONTMPSZ);
					}
					else if (jsoneq(json_string, g, "lineHeight") == 0) {
						jsmntok_t* value = &t[i + 1];
						font->lineHeight = jsongetfloat(json_string, value, json_tmp, JSONTMPSZ);
					}

					i += 2;
				}
				i--;
			}
		}
		else if (jsoneq(json_string, &t[i], "glyphs") == 0) {
			if (t[i + 1].type == JSMN_ARRAY) {

				int32_t size = t[i + 1].size;
				i += 2;

				for (int32_t j = 0; j < size; j++) {
					jsmntok_t *g = &t[i];

					if (g->type == JSMN_OBJECT) {
						unsigned long char_code = 32;
						float advance = 0.0f;
						float pleft, pbottom, pright, ptop = 0.0f;
						float aleft, abottom, aright, atop = 0.0f;
						wbool hasplane = wfalse;
						wbool hasatlas = wfalse;

						i++;

						for (int32_t k = 0; k < g->size; k++) {
							jsmntok_t *gg = &t[i];
							if (jsoneq(json_string, gg, "unicode") == 0) {
								jsmntok_t* value = &t[i + 1];
								char_code = jsongetint(json_string, value, json_tmp, JSONTMPSZ);
							}
							else if (jsoneq(json_string, gg, "advance") == 0) {
								jsmntok_t* value = &t[i + 1];
								advance = jsongetfloat(json_string, value, json_tmp, JSONTMPSZ);
							}
							else if (jsoneq(json_string, gg, "planeBounds") == 0) {
								jsmntok_t* ggo = &t[i+1];
								hasplane = wtrue;
								if (ggo->type == JSMN_OBJECT) {
									i += 2;
									for (int32_t l = 0; l < ggo->size; l++) {
										jsmntok_t *ggg = &t[i];

										if (jsoneq(json_string, ggg, "left") == 0) {
											jsmntok_t* value = &t[i + 1];
											pleft = jsongetfloat(json_string, value, json_tmp, JSONTMPSZ);
										}
										else if (jsoneq(json_string, ggg, "bottom") == 0) {
											jsmntok_t* value = &t[i + 1];
											pbottom = jsongetfloat(json_string, value, json_tmp, JSONTMPSZ);
										}
										else if (jsoneq(json_string, ggg, "right") == 0) {
											jsmntok_t* value = &t[i + 1];
											pright = jsongetfloat(json_string, value, json_tmp, JSONTMPSZ);
										}
										else if (jsoneq(json_string, ggg, "top") == 0) {
											jsmntok_t* value = &t[i + 1];
											ptop = jsongetfloat(json_string, value, json_tmp, JSONTMPSZ);
										}

										i += 2;
									}
									i -= 2;
								}
							}
							else if (jsoneq(json_string, gg, "atlasBounds") == 0) {
								jsmntok_t* ggo = &t[i+1];
								hasatlas = wtrue;
								if (ggo->type == JSMN_OBJECT) {
									i += 2;
									for (int32_t l = 0; l < ggo->size; l++) {
										jsmntok_t *ggg = &t[i];

										if (jsoneq(json_string, ggg, "left") == 0) {
											jsmntok_t* value = &t[i + 1];
											aleft = jsongetfloat(json_string, value, json_tmp, JSONTMPSZ);
										}
										else if (jsoneq(json_string, ggg, "bottom") == 0) {
											jsmntok_t* value = &t[i + 1];
											abottom = jsongetfloat(json_string, value, json_tmp, JSONTMPSZ);
										}
										else if (jsoneq(json_string, ggg, "right") == 0) {
											jsmntok_t* value = &t[i + 1];
											aright = jsongetfloat(json_string, value, json_tmp, JSONTMPSZ);
										}
										else if (jsoneq(json_string, ggg, "top") == 0) {
											jsmntok_t* value = &t[i + 1];
											atop = jsongetfloat(json_string, value, json_tmp, JSONTMPSZ);
										}
										i += 2;
									}
									i -= 2;
								}
							}

							i += 2;
						}


						int32_t glyphpos = (int32_t)(((int32_t)char_code) - char_start);  // Calculate Character Index (Offset by First Char in Font)

						if (glyphpos > (int32_t)char_end) {
							logprint("Error while loading font data in JSON format, the charcode %d is out of bounds", glyphpos);
							return wfalse;
						}

						Fontglyph_t* cglyph = &font->glyphs[glyphpos];

						if (hasplane && hasatlas) {
							float u1 = aleft / font->width;
							float u2 = aright / font->width;
							float v1 = (font->height - atop) / font->height;//msdfgen provide inverted atlas coord, since opengl texture are flipped in memory, but our renderer already take care of that, flip it back
							float v2 = (font->height - abottom) / font->height; //invert v1 / v2 otherwise our font is upside down


							Fontglyph_Init(cglyph, u1, v1, u2, v2, char_code, aleft, abottom, aright, atop, pleft, pbottom, pright, ptop, advance);
						}
						else {
							Fontglyph_Init_empty(cglyph, char_code, advance);
						}
					}
				}
			}
		}
	}


	return wtrue;

}

void free_font(render_manager_t* const render_mngr ,font_info* font)
{
	wf_free(font->glyphs);
	font->glyphs = NULL;
}




