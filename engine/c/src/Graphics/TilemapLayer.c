#include <stdlib.h>

#include <Render.h>

#include <math.h>
#include <inttypes.h>
#include <string.h>

#include <Base/types.h>

#include <Engine.h>
#include <Base/functions.h>
#include <Base/geom.h>
#include <Base/Color.h>
#include <GameObjects/Level.pb-c.h>
#include <Render/render_manager.h>
#include <Graphics/TextureLoader.h>
#include <Graphics/TilemapLayer.h>
#include "Debug/Logprint.h"

void TilemapLayer_Init(TilemapLayer_t* tmap, int32_t rowCount, int32_t colCount, int32_t tileWidth, int32_t tileHeight, texture_info* const texturePointer, Layer* layer_data, render_manager_t* const rendermngr, tlayer_randomizer_t randomizer)
{
	TilemapLayer_Init_withback(tmap, rowCount, colCount, tileWidth, tileHeight, texturePointer, layer_data, rendermngr, randomizer, wfalse);
}


void TilemapLayer_Init_withback(TilemapLayer_t* tmap,int32_t rowCount,int32_t colCount,int32_t tileWidth, int32_t tileHeight,texture_info* const texturePointer,Layer* layer_data,render_manager_t* const rendermngr, tlayer_randomizer_t randomizer,wbool backgroundload)
{
    size_t tileArraySize = rowCount * colCount;
    size_t verticesArraySize = tileArraySize * 12;
    size_t textureArraySize = tileArraySize * 12;
	
	tmap->rowCount = (uint16_t)rowCount;
	tmap->colCount = (uint16_t)colCount;
	tmap->tileWidth = tileWidth;
	tmap->tileHeight = tileHeight;
	tmap->textPointer = texturePointer;
	tmap->animated = wfalse;
	tmap->frames = NULL;
	tmap->current_frame = 0;


	if(layer_data->no_scroll_x)
		tmap->parallax_x = 0.0f;
	else
	{
		if(layer_data->has_parallax_x)
			tmap->parallax_x = layer_data->parallax_x;
		else
			tmap->parallax_x = 1.0f;
	}

	if(layer_data->no_scroll_y)
		tmap->parallax_y = 0.0f;	
	else
	{
		if(layer_data->has_parallax_y)
			tmap->parallax_y = layer_data->parallax_y;
		else
			tmap->parallax_y = 1.0f;
	}

	//default to white color
	tmap->vertices_color.r = tmap->vertices_color.g = tmap->vertices_color.b = 1.0f;
	tmap->vertices_color.a = layer_data->opacity;

	tmap->tileArray = (int32_t*)wf_malloc(rowCount * colCount * sizeof(int32_t));

	wbool random = wfalse;
	if (layer_data->has_random_tiles && layer_data->random_tiles) {
		if (randomizer == NULL) {
			logprint("Layer %s content set as random, but no random function provided, please set level_stream or level_object random function", layer_data->layer_id);
		}
		else {
			random = wtrue;
		}
	}

	if (!random) {
		w_memcpy(tmap->tileArray, rowCount * colCount * sizeof(int32_t), layer_data->tile_array, rowCount * colCount * sizeof(int32_t));
	}
	else {
		randomizer(tmap->tileArray, tmap->colCount, tmap->rowCount,tmap->tileWidth,texturePointer->width, texturePointer->height);
	}
	

	tmap->tileArraySize = tileArraySize;
	tmap->textureArraySize = textureArraySize;
	tmap->verticeArraySize = verticesArraySize;
	tmap->visible = wtrue;
	tmap->draw_order = layer_data->order;
	

	
	TilemapLayer_genTileMap(tmap,tmap->tileArray,tileArraySize,wfalse);

	if (backgroundload)
	{
		tmap->VBOvertices = tmap->VBOtexcoord = render_get_emptybuffervalue();
	}
	else {
		TilemapLayer_frontload(tmap, rendermngr);
	}


	if(layer_data->shader_program != NULL && strcmp(layer_data->shader_program,"") != 0)
	{
		tmap->program_id = render_get_program(rendermngr,layer_data->shader_program);

		w_strcpy(tmap->prog_name, 128, layer_data->shader_program);

		if(layer_data->parameters != NULL && layer_data->n_parameters > 0)
		{
			//copy protobuf parameters
			tmap->num_parameters = layer_data->n_parameters;
			tmap->shader_parameters = (ShadersParameters**)wf_calloc(tmap->num_parameters,sizeof(ShadersParameters*));

			unsigned char* param_buffer = NULL;

			for(uint32_t i_param = 0; i_param < tmap->num_parameters;i_param++)
			{
				size_t size_param = shaders_parameters__get_packed_size(layer_data->parameters[i_param]);

				param_buffer = (unsigned char*)wf_realloc(param_buffer,size_param);
				memset(param_buffer,0,size_param);
				shaders_parameters__pack(layer_data->parameters[i_param],param_buffer);

				tmap->shader_parameters[i_param] = shaders_parameters__unpack(NULL,size_param,param_buffer);
			}

			if(param_buffer != NULL)
				wf_free(param_buffer);
			
		}
		else
		{
			tmap->shader_parameters = NULL;
			tmap->num_parameters = 0;
		}
	}
	else
	{
		tmap->program_id = render_get_emptyprogvalue();
		tmap->shader_parameters = NULL;
		tmap->num_parameters = 0;
		memset(tmap->prog_name, 0, sizeof(tmap->prog_name));
	}

}

void TilemapLayer_frontload(TilemapLayer_t* const tmap, render_manager_t* const rendermngr)
{
	tmap->VBOvertices = render_gen_buffer(rendermngr, tmap->vertices, tmap->verticeArraySize * sizeof(float), wfalse, wfalse);

	tmap->VBOtexcoord = render_gen_buffer(rendermngr, tmap->textureCoord, tmap->textureArraySize * sizeof(float), (wbool)tmap->animated, (wbool)tmap->animated);

	load_texture_deferred(tmap->textPointer, rendermngr);
}


void TilemapLayer_set_shader_program(TilemapLayer_t* const tmap,Layer* layer_data,render_manager_t* const rendermngr)
{
	if(layer_data->shader_program != NULL && strcmp(layer_data->shader_program,"") != 0)
	{
		tmap->program_id = render_get_program(rendermngr,layer_data->shader_program);
	}
	else
	{
		tmap->program_id = render_get_emptyprogvalue();
	}
}

//copy new param to tilemap param array
void TilemapLayer_addshaderparam(TilemapLayer_t* const tmap,const ShadersParameters* const new_param)
{
	if(tmap->shader_parameters == NULL)
		tmap->shader_parameters = (ShadersParameters**)wf_calloc(1,sizeof(ShadersParameters*));
	else
		tmap->shader_parameters = (ShadersParameters**)wf_realloc(tmap->shader_parameters,(tmap->num_parameters + 1) * sizeof(ShadersParameters*));

	unsigned char* param_buffer = NULL;
	size_t size_param = shaders_parameters__get_packed_size(new_param);

	param_buffer = (unsigned char*)wf_malloc(size_param);
	memset(param_buffer,0,size_param);
	shaders_parameters__pack(new_param,param_buffer);

	tmap->shader_parameters[tmap->num_parameters++] =  shaders_parameters__unpack(NULL,size_param,param_buffer);

	wf_free(param_buffer);
}

void TilemapLayer_setcolor(TilemapLayer_t* const tmap, ColorA_t new_color)
{
	tmap->vertices_color = new_color;
}

void TilemapLayer_fullupdateattributearray_shader(TilemapLayer_t* const tmap,const char* attribute_name,const float* const array_update)
{

	if(tmap->shader_parameters != NULL)
	{
		for(uint32_t i_param = 0; i_param < tmap->num_parameters;i_param++)
		{
			if(tmap->shader_parameters[i_param]->param_type == TYPE_PARAM_SHADER__attribute_array && strcmp(tmap->shader_parameters[i_param]->param_name,attribute_name) == 0)
			{
				for(uint32_t indx = 0; indx < tmap->shader_parameters[i_param]->attribute_array->n_array_content;indx++)
					tmap->shader_parameters[i_param]->attribute_array->array_content[indx] = array_update[indx];

				tmap->shader_parameters[i_param]->attribute_array->update_buffer = (protobuf_c_boolean)1;
				break;

			}
		}
	}

}

void TilemapLayer_updateattributearray_shader(TilemapLayer_t* const tmap,const char* attribute_name,float value_update,int32_t value_index)
{
	if(tmap->shader_parameters != NULL)
	{
		for(uint32_t i_param = 0; i_param < tmap->num_parameters;i_param++)
		{
			if(tmap->shader_parameters[i_param]->param_type == TYPE_PARAM_SHADER__attribute_array && strcmp(tmap->shader_parameters[i_param]->param_name,attribute_name) == 0)
			{
				tmap->shader_parameters[i_param]->attribute_array->array_content[(value_index * 6)] = tmap->shader_parameters[i_param]->attribute_array->array_content[(value_index * 6) + 1] = tmap->shader_parameters[i_param]->attribute_array->array_content[(value_index * 6) + 2] = tmap->shader_parameters[i_param]->attribute_array->array_content[(value_index * 6) + 3] = tmap->shader_parameters[i_param]->attribute_array->array_content[(value_index * 6) + 4] = tmap->shader_parameters[i_param]->attribute_array->array_content[(value_index * 6) + 5] = value_update;

				tmap->shader_parameters[i_param]->attribute_array->update_buffer = (protobuf_c_boolean)1;
				break;
			}
		}
	}
}

uint32_t TilemapLayer_getshaderparamindex(TilemapLayer_t* const tmap, const char* attribute_name)
{
	if (tmap->shader_parameters != NULL)
	{
		for (uint32_t i_param = 0; i_param < tmap->num_parameters; i_param++)
		{
			if (strcmp(tmap->shader_parameters[i_param]->param_name, attribute_name) == 0)
			{
				return i_param;
			}
		}
	}

	return -1;
}

void TilemapLayer_bitaddtoattributearray_shader(TilemapLayer_t* const tmap, uint32_t i_param, int32_t value_add, int32_t value_index)
{
	float origval = tmap->shader_parameters[i_param]->attribute_array->array_content[(value_index * 6)];

	int32_t newval = (int32_t)origval | value_add;

	tmap->shader_parameters[i_param]->attribute_array->array_content[(value_index * 6)] = tmap->shader_parameters[i_param]->attribute_array->array_content[(value_index * 6) + 1] = tmap->shader_parameters[i_param]->attribute_array->array_content[(value_index * 6) + 2] = tmap->shader_parameters[i_param]->attribute_array->array_content[(value_index * 6) + 3] = tmap->shader_parameters[i_param]->attribute_array->array_content[(value_index * 6) + 4] = tmap->shader_parameters[i_param]->attribute_array->array_content[(value_index * 6) + 5] = (float)newval;
}

void TilemapLayer_bitremovefromattributearray_shader(TilemapLayer_t* const tmap, uint32_t i_param, int32_t value_remove, int32_t value_index)
{
	float origval = tmap->shader_parameters[i_param]->attribute_array->array_content[(value_index * 6)];
	int32_t newval = (int32_t)origval & ~value_remove;

	tmap->shader_parameters[i_param]->attribute_array->array_content[(value_index * 6)] = tmap->shader_parameters[i_param]->attribute_array->array_content[(value_index * 6) + 1] = tmap->shader_parameters[i_param]->attribute_array->array_content[(value_index * 6) + 2] = tmap->shader_parameters[i_param]->attribute_array->array_content[(value_index * 6) + 3] = tmap->shader_parameters[i_param]->attribute_array->array_content[(value_index * 6) + 4] = tmap->shader_parameters[i_param]->attribute_array->array_content[(value_index * 6) + 5] = (float)newval;
}

float TilemapLayer_getshaderattributarrayvalue(TilemapLayer_t* const tmap, uint32_t i_param, int32_t value_index)
{
	return tmap->shader_parameters[i_param]->attribute_array->array_content[(value_index * 6)];
}

void TilemapLayer_updateparambuffer(TilemapLayer_t* const tmap, uint32_t i_param)
{
	tmap->shader_parameters[i_param]->attribute_array->update_buffer = (protobuf_c_boolean)1;
}

void TilemapLayer_setanimated(TilemapLayer_t* const tmap,TilemapFrame** array_of_frames,size_t num_frames)
{
	//deleted existing data if needed
	TilemapLayer_clearanimated(tmap);

	if(tmap->frames == NULL) //check that the frames have been properly cleaned
	{
		tmap->animated = wtrue;

		tmap->frames = (tilemap_frame_t**)wf_malloc(sizeof(tilemap_frame_t*) * num_frames);
		tmap->num_frames = num_frames;

		//keep track of current tile values
		tmap->tileFrameArray = (int32_t*)wf_malloc(tmap->tileArraySize * sizeof(int32_t));
		memcpy(tmap->tileFrameArray,tmap->tileArray,tmap->tileArraySize * sizeof(int32_t));


		for(uint32_t indx = 0; indx < num_frames;indx++)
		{
			tmap->frames[indx] = (tilemap_frame_t*)wf_malloc(sizeof(tilemap_frame_t));
			tmap->frames[indx]->time_step = array_of_frames[indx]->time_step;
			tmap->frames[indx]->num_update = array_of_frames[indx]->n_tile_update;
			tmap->frames[indx]->tile_update = (tile_info_t**)wf_malloc(sizeof(tile_info_t*) * array_of_frames[indx]->n_tile_update);

			for(uint32_t upd_indx = 0; upd_indx < array_of_frames[indx]->n_tile_update;upd_indx++)
			{
				tmap->frames[indx]->tile_update[upd_indx] = (tile_info_t*)wf_malloc(sizeof(tile_info_t));
				tmap->frames[indx]->tile_update[upd_indx]->id_tile = array_of_frames[indx]->tile_update[upd_indx]->id_tile;
				tmap->frames[indx]->tile_update[upd_indx]->index = array_of_frames[indx]->tile_update[upd_indx]->index;
				tmap->frames[indx]->tile_update[upd_indx]->draw = wfalse;
			}
		}
	}

}

void TilemapLayer_clearanimated(TilemapLayer_t* const tmap)
{
	if(tmap->frames != NULL)
	{
		for(uint32_t indx = 0; indx < tmap->num_frames;indx++)
		{
			for(uint32_t upd_indx = 0; upd_indx < tmap->frames[indx]->num_update;upd_indx++)
			{
				wf_free(tmap->frames[indx]->tile_update[upd_indx]);
			}

			wf_free(tmap->frames[indx]->tile_update);

			wf_free(tmap->frames[indx]);
		}

		wf_free(tmap->frames);

		wf_free(tmap->tileFrameArray);
		
		tmap->tileFrameArray = NULL;
		tmap->frames = NULL;
		tmap->num_frames = 0;
		tmap->animated = wfalse;
	}
}


//generate tilemap texture coordinates and vertices positions
void TilemapLayer_genTileMap(TilemapLayer_t* tmap,const int32_t* tileArray, const size_t tileArraySize,wbool regen)
{	
	size_t idTile = 0;
	int32_t idtx = 0;
	int32_t idvrt = 0;
	int32_t tileRow = 0;
	int32_t tileCol = 0;

	//coord unit are NOT used in case of virtual texture (from a packed texture)
	tmap->coordUnitX = (1.0f / (tmap->textPointer->width / tmap->tileWidth));
	tmap->coordUnitY = (1.0f / (tmap->textPointer->height / tmap->tileHeight));


	if(tmap->textPointer->virtual_texture)
		tmap->txtColCount = tmap->textPointer->region->width / tmap->tileWidth;
	else
		tmap->txtColCount = tmap->textPointer->width / tmap->tileWidth;


	if (tmap->txtColCount == 0)
	{
		tmap->txtColCount = 1;
	}


	//generate texture coord and vertices from our tilearray
	if(!regen)
	{
		tmap->textureCoord = (float*)wf_malloc(sizeof(float) * tileArraySize * 12);
		tmap->vertices =(float*)wf_malloc(sizeof(float) * tileArraySize * 12);
	}
	else
	{
		tmap->textureCoord = (float*)wf_realloc(tmap->textureCoord,sizeof(float) * tileArraySize * 12);
		tmap->vertices =(float*)wf_realloc(tmap->vertices,sizeof(float) * tileArraySize * 12);
	}

	
	
	for(idTile = 0, idtx = 0, idvrt = 0 ; idTile < tileArraySize;idTile++, idtx += 12, idvrt += 12)
	{
		int32_t txtRow = (int32_t)floorf((float)(tileArray[idTile] / tmap->txtColCount));//ligne Y
		int32_t txtCol = tileArray[idTile] - (tmap->txtColCount * txtRow);//ligne X

		//precompute texture coordinates
		float x1 = (tmap->coordUnitX * txtCol);
		float y2 = (tmap->coordUnitY * txtRow);

		float y1 = (tmap->coordUnitY + y2);
		float x2 = (tmap->coordUnitX + x1);
	

		if(tmap->textPointer->virtual_texture)
		{
			wfloat basepointx = tmap->textPointer->region->position.x;
			wfloat basepointy = tmap->textPointer->region->position.y;
			float one_TILE_SIZE = (float)tmap->tileWidth;
			float one_tile_height = (float)tmap->tileHeight;

			x1 = (basepointx  + (one_TILE_SIZE * txtCol)) / tmap->textPointer->width;
			y2 = (basepointy + (one_tile_height * txtRow)) / tmap->textPointer->height;

			x2 = ((basepointx  + (one_TILE_SIZE * txtCol)) + one_TILE_SIZE) / tmap->textPointer->width;
			y1 = ((basepointy + (one_tile_height * txtRow)) + one_tile_height) / tmap->textPointer->height;
		}
		
		//compute texture coords
        tmap->textureCoord[idtx]   = x1;
		tmap->textureCoord[idtx+1] = y1;
		
		tmap->textureCoord[idtx+2] = x2;
		tmap->textureCoord[idtx+3] = y1;
		
		tmap->textureCoord[idtx+4] = x2;
		tmap->textureCoord[idtx+5] = y2;
		
		tmap->textureCoord[idtx+6] = x2;
		tmap->textureCoord[idtx+7] = y2;
		
		tmap->textureCoord[idtx+8] = x1;
		tmap->textureCoord[idtx+9] = y2;
		
		tmap->textureCoord[idtx+10] = x1;
		tmap->textureCoord[idtx+11] = y1;
		
		tileRow = (int32_t)floorf((float)(idTile / tmap->colCount));//ligne Y
		tileCol = idTile - (tmap->colCount * tileRow);//ligne X
		
		//compute vertices positions
		
		tmap->vertices[idvrt] = (float)(tileCol * tmap->tileWidth);
		tmap->vertices[idvrt+1] = (float)(tileRow * tmap->tileHeight);
		
		tmap->vertices[idvrt+2] = (float)tmap->tileWidth + (tileCol * tmap->tileWidth);
		tmap->vertices[idvrt+3] = (float)(tileRow * tmap->tileHeight);
		
		tmap->vertices[idvrt+4] = (float)tmap->tileWidth + (tileCol * tmap->tileWidth);
		tmap->vertices[idvrt+5] = (float)tmap->tileHeight + (tileRow * tmap->tileHeight);
		
		tmap->vertices[idvrt+6] = (float)tmap->tileWidth + (tileCol * tmap->tileWidth);
		tmap->vertices[idvrt+7] = (float)tmap->tileHeight + (tileRow * tmap->tileHeight);
		
		tmap->vertices[idvrt+8] = (float)(tileCol * tmap->tileWidth);
		tmap->vertices[idvrt+9] = (float)tmap->tileHeight + (tileRow * tmap->tileHeight);
		
		tmap->vertices[idvrt+10] =  (float)(tileCol * tmap->tileWidth);
		tmap->vertices[idvrt+11] =  (float)(tileRow * tmap->tileHeight);
		
	}

}

void TilemapLayer_update_tilesize(TilemapLayer_t* tmap,const int32_t tile_size,render_manager_t* const rendermngr)
{
	tmap->tileWidth = tmap->tileHeight = tile_size;

	//generate new vertices and texture coordinates to match our new tile size
	TilemapLayer_genTileMap(tmap,tmap->tileArray,tmap->tileArraySize,wtrue);

	//update hardware buffers
	render_update_buffer(rendermngr,tmap->VBOvertices,tmap->vertices,0,tmap->verticeArraySize * sizeof(float));
	render_update_buffer(rendermngr,tmap->VBOtexcoord,tmap->textureCoord,0,tmap->textureArraySize * sizeof(float));
}


//add/remove columns to the top of the map row_direction = 0, add/remove columns to the bottom of the map  row_direction = 1 /  add / remove columns to the right of the map col_direction = 0,  add / remove columns to the left of the map col_direction = 1
void TilemapLayer_resize(TilemapLayer_t* tmap,const int32_t rowCount,const int32_t colCount,const int16_t row_direction,const int16_t col_direction,render_manager_t* const rendermngr)
{
    size_t tileArraySize = rowCount * colCount;
    size_t verticesArraySize = tileArraySize * 12;
    size_t textureArraySize = tileArraySize * 12;


	wbool skip_row = wfalse;
	wbool skip_col = wfalse;

	//
	int32_t diff_row = abs(rowCount - tmap->rowCount);
	int32_t diff_col = abs(colCount - tmap->colCount);

	int32_t remove_col = 0;

	if(rowCount < tmap->rowCount)
		 tmap->rowCount = rowCount;

	
	if(colCount < tmap->colCount)
	{
		tmap->colCount = colCount;
		remove_col = diff_col;
	}

	//resize tile array
	if(diff_col > 0 || diff_row > 0)
	{
		int32_t* new_tile_array = (int32_t*)wf_malloc((rowCount * colCount) * sizeof(int32_t));

		uint32_t old_id = 0;

		for(int ri1 = 0; ri1 < rowCount;ri1++)
		{
			skip_row = (wbool)(row_direction == 1 && ri1 < diff_row);

			
			if(col_direction == 1)
				old_id += remove_col;

			for(int ri = 0; ri < colCount;ri++)
			{
				if(col_direction == 1 && remove_col == 0)
					skip_col = (wbool)(ri < diff_col);
				else
					skip_col = (wbool)(ri >= tmap->colCount);

				if(!skip_col && !skip_row)
				{
					if(old_id < tmap->tileArraySize)
						new_tile_array[ri + (ri1 * colCount)] = tmap->tileArray[old_id++];
					else
						new_tile_array[ri + (ri1 * colCount)] = 0;
				}
				else
				{
					new_tile_array[ri + (ri1 * colCount)] = 0;
				}
			}

			if(col_direction == 0)
				old_id += remove_col;
		}

		wf_free(tmap->tileArray);
		tmap->tileArray= new_tile_array;
	}
	else
	{
		tmap->tileArray = (int32_t*)wf_realloc(tmap->tileArray,(rowCount * colCount) * sizeof(int32_t));
	}


	

	if(tmap->animated)
	{
		tmap->tileFrameArray = (int32_t*)wf_realloc(tmap->tileFrameArray,tileArraySize * sizeof(int32_t));
		memcpy(tmap->tileFrameArray,tmap->tileArray,tileArraySize * sizeof(int32_t));

		//change animation values
		for(uint32_t iframe = 0; iframe < tmap->num_frames;iframe++)
		{
			for(uint32_t inupd = 0; inupd < tmap->frames[iframe]->num_update;inupd++)
			{
				int32_t aniRow = (int32_t)floorf((float)(tmap->frames[iframe]->tile_update[inupd]->index / tmap->colCount));//ligne Y

				if(remove_col == 0)
				{
					if(col_direction == 0)
						tmap->frames[iframe]->tile_update[inupd]->index += (diff_col * aniRow);
					else if(col_direction == 1)
						tmap->frames[iframe]->tile_update[inupd]->index += (diff_col * (1+aniRow));

					if(row_direction == 1)
						tmap->frames[iframe]->tile_update[inupd]->index += (diff_row * colCount);
				}
				else
				{
					if(col_direction == 0)
						tmap->frames[iframe]->tile_update[inupd]->index -= (diff_col * aniRow);
					else if(col_direction == 1)
						tmap->frames[iframe]->tile_update[inupd]->index -= (diff_col * (1+aniRow));

					if(row_direction == 1)
						tmap->frames[iframe]->tile_update[inupd]->index -= (diff_row * colCount);
				}
			}
		}

	}

	tmap->rowCount = rowCount;
	tmap->colCount = colCount;
	tmap->tileArraySize = tileArraySize;
	tmap->textureArraySize = textureArraySize;
	tmap->verticeArraySize = verticesArraySize;

	
	
	TilemapLayer_genTileMap(tmap,tmap->tileArray,tileArraySize,wtrue);
	
	render_delete_buffer(tmap->VBOvertices);
	render_delete_buffer(tmap->VBOtexcoord);

	tmap->VBOvertices = render_gen_buffer(rendermngr,tmap->vertices,verticesArraySize * sizeof(float),wfalse,wfalse);
	tmap->VBOtexcoord = render_gen_buffer(rendermngr,tmap->textureCoord,textureArraySize * sizeof(float),(wbool)tmap->animated,(wbool)tmap->animated);
}

void TilemapLayer_regenTexCoord(TilemapLayer_t* tmap,render_manager_t* const rendermngr)
{
	size_t idTile = 0;
	int32_t idtx = 0;

	for(idTile = 0, idtx = 0 ; idTile < tmap->tileArraySize;idTile++, idtx += 12)
	{
		int32_t txtRow = (int32_t)floorf((float)(tmap->tileArray[idTile] / tmap->txtColCount));//ligne Y
		int32_t txtCol = tmap->tileArray[idTile] - (tmap->txtColCount * txtRow);//ligne X
		
		//precompute texture coordinates
		float x1 = (tmap->coordUnitX * txtCol);
		float y2 = (tmap->coordUnitY * txtRow);

		float y1 = (tmap->coordUnitY + y2);
		float x2 = (tmap->coordUnitX + x1);
	

		if(tmap->textPointer->virtual_texture)
		{
			wfloat basepointx = tmap->textPointer->region->position.x;
			wfloat basepointy = tmap->textPointer->region->position.y;
			float one_TILE_SIZE = (float)tmap->tileWidth;
			float one_tile_height = (float)tmap->tileHeight;

			x1 = (basepointx  + (one_TILE_SIZE * txtCol)) / tmap->textPointer->width;
			y2 = (basepointy + (one_tile_height * txtRow)) / tmap->textPointer->height;

			x2 = ((basepointx  + (one_TILE_SIZE * txtCol)) + one_TILE_SIZE) / tmap->textPointer->width;
			y1 = ((basepointy + (one_tile_height * txtRow)) + one_tile_height) / tmap->textPointer->height;
		}
		
		//compute texture coords
        tmap->textureCoord[idtx]   = x1;
		tmap->textureCoord[idtx+1] = y1;
		
		tmap->textureCoord[idtx+2] = x2;
		tmap->textureCoord[idtx+3] = y1;
		
		tmap->textureCoord[idtx+4] = x2;
		tmap->textureCoord[idtx+5] = y2;
		
		tmap->textureCoord[idtx+6] = x2;
		tmap->textureCoord[idtx+7] = y2;
		
		tmap->textureCoord[idtx+8] = x1;
		tmap->textureCoord[idtx+9] = y2;
		
		tmap->textureCoord[idtx+10] = x1;
		tmap->textureCoord[idtx+11] = y1;
	}

	render_update_buffer(rendermngr,tmap->VBOtexcoord,tmap->textureCoord,0,tmap->textureArraySize * sizeof(float));

}



//edit a single tile in the tile map, only the texture coord are updated
void TilemapLayer_editTile(TilemapLayer_t* tmap,int32_t tileId,int32_t tilePos,render_manager_t* const rendermngr,wbool edit_gpubuffer)
{
	int32_t txtRow = (int32_t)floorf((float)(tileId / tmap->txtColCount));//ligne Y
	int32_t txtCol = tileId - (tmap->txtColCount * txtRow);//ligne X
	int32_t idtx = tilePos * 12;

	tmap->tileArray[tilePos] = tileId;

	if(tmap->animated)
		tmap->tileFrameArray[tilePos] = tileId;
		
	TilemapLayer_update_texcoord(tmap,idtx,txtCol,txtRow,rendermngr,edit_gpubuffer);
}

void TilemapLayer_update_texcoord(TilemapLayer_t* const tmap,int32_t idtx,int32_t txtCol,int32_t txtRow,render_manager_t* const rendermngr,wbool update_gpu_buffer)
{
		//precompute texture coordinates
		float x1 = (tmap->coordUnitX * txtCol);
		float y2 = (tmap->coordUnitY * txtRow);

		float y1 = (tmap->coordUnitY + y2);
		float x2 = (tmap->coordUnitX + x1);
	

		if(tmap->textPointer->virtual_texture)
		{
			wfloat basepointx = tmap->textPointer->region->position.x;
			wfloat basepointy = tmap->textPointer->region->position.y;
			float one_TILE_SIZE = (float)tmap->tileWidth;
			float one_tile_height = (float)tmap->tileHeight;

			x1 = (basepointx  + (one_TILE_SIZE * txtCol)) / tmap->textPointer->width;
			y2 = (basepointy + (one_tile_height * txtRow)) / tmap->textPointer->height;

			x2 = ((basepointx  + (one_TILE_SIZE * txtCol)) + one_TILE_SIZE) / tmap->textPointer->width;
			y1 = ((basepointy + (one_tile_height * txtRow)) + one_tile_height) / tmap->textPointer->height;
		}
		
		//compute texture coords
        tmap->textureCoord[idtx]   = x1;
		tmap->textureCoord[idtx+1] = y1;
		
		tmap->textureCoord[idtx+2] = x2;
		tmap->textureCoord[idtx+3] = y1;
		
		tmap->textureCoord[idtx+4] = x2;
		tmap->textureCoord[idtx+5] = y2;
		
		tmap->textureCoord[idtx+6] = x2;
		tmap->textureCoord[idtx+7] = y2;
		
		tmap->textureCoord[idtx+8] = x1;
		tmap->textureCoord[idtx+9] = y2;
		
		tmap->textureCoord[idtx+10] = x1;
		tmap->textureCoord[idtx+11] = y1;

	
	if(update_gpu_buffer)
		render_update_buffer(rendermngr,tmap->VBOtexcoord, &tmap->textureCoord[idtx],idtx * sizeof(float),12 * sizeof(float));
}

void TilemapLayer_synctexcoord(TilemapLayer_t* const tmap,render_manager_t* const rendermngr)
{
	//update textcoord only on minimum distance view size

	#ifdef ANDROID_BUILD
		render_discard_updatebuffer(rendermngr,tmap->VBOtexcoord,&tmap->textureCoord[0],tmap->textureArraySize * sizeof(float),wtrue,wtrue);
	#else
		int16_t scr_row_count =  (int16_t)ceilf(((float)rendermngr->screen_info.height / (float)tmap->tileHeight) * rendermngr->zoom_factor) + TILEUPDATE_EXTRA_TILE;
	
		int16_t scr_col_count = (int16_t)ceilf(((float)rendermngr->screen_info.width / (float)tmap->tileWidth) * rendermngr->zoom_factor) + TILEUPDATE_EXTRA_TILE;

		Vector_t cpos = rendermngr->scroll_vector;

		cpos.x = (cpos.x * tmap->parallax_x) - cpos.x;
		cpos.y =  (cpos.y * tmap->parallax_y) - cpos.y;


		int32_t tile_col = (int32_t)((rendermngr->screen_info.position.x - cpos.x)  / tmap->tileWidth) - TILEUPDATE_EXTRA_TILE_HALF;
		int32_t tile_row = (int32_t)((rendermngr->screen_info.position.y - cpos.y)  / tmap->tileHeight) - (scr_row_count - TILEUPDATE_EXTRA_TILE_HALF);

		if(tile_col < 0)
			tile_col = 0;
		else if(tile_col >= tmap->colCount)
			tile_col = tmap->colCount - 1;

		if(tile_row < 0)
			tile_row = 0;
		else if(tile_row >= tmap->rowCount)
			tile_row = tmap->rowCount -1;

	
		size_t startIndex = 0;
		size_t num_update = scr_col_count * 12;
	 
		if((tile_row + scr_row_count) > tmap->rowCount)
			scr_row_count = tmap->rowCount - tile_row;

		render_bind_buffer(rendermngr,tmap->VBOtexcoord);

		for(int32_t i_row = tile_row;i_row < tile_row + scr_row_count;i_row++)
		{
			startIndex = ((i_row * tmap->colCount) + tile_col) * 12;

			if((startIndex + num_update) >=  tmap->textureArraySize)
				num_update = tmap->textureArraySize - startIndex;

			render_step_update_buffer(rendermngr,&tmap->textureCoord[startIndex],startIndex * sizeof(float),num_update * sizeof(float),tmap->VBOtexcoord);
		}

		render_unbind_buffer(rendermngr,tmap->VBOtexcoord);
	#endif
    
}

void TilemapLayer_edit_tile_animated(TilemapLayer_t* const tmap,const int32_t tileId,const int32_t tilePos,const int32_t current_frame,render_manager_t* const rendermngr)
{
	//if animated, update animation value
	if(tmap->animated)
	{
		int32_t txtRow = (int32_t)floorf((float)(tileId / tmap->txtColCount));//ligne Y
		int32_t txtCol = tileId - (tmap->txtColCount * txtRow);//ligne X
		int32_t idtx = tilePos * 12;

		tmap->tileFrameArray[tilePos] = tileId;

		wbool exist = wfalse;
		wbool same_as_previous = wfalse;
		uint32_t i = 0;

		//search for a existing update
		for(i = 0; i < tmap->frames[current_frame]->num_update;i++)
		{
			if(tmap->frames[current_frame]->tile_update[i]->index == tilePos)
			{
				tmap->frames[current_frame]->tile_update[i]->id_tile = tileId;
				exist = wtrue;
				break;
			}
		}


		//search for similar update before the tile
		int32_t frame_search = current_frame -1;

		if(frame_search >= 0)
		{
			for(i = 0;i < tmap->frames[frame_search]->num_update;i++)
			{
				if(tmap->frames[frame_search]->tile_update[i]->index == tilePos && tmap->frames[frame_search]->tile_update[i]->id_tile == tileId)
				{
					same_as_previous = wtrue;
					break;
				}
			}
		}
		else if(current_frame == 0)//we edit the first frame after the original tile value, check against tile value
		{
			if(tmap->tileArray[tilePos] == tileId)
			{
				same_as_previous = wtrue;
			}
		}


		if(!exist && !same_as_previous)//add animation info for a tile
		{
			if(tmap->frames[current_frame]->num_update == 0)
				tmap->frames[current_frame]->tile_update = (tile_info_t**)wf_malloc(sizeof(tile_info_t*) * (tmap->frames[current_frame]->num_update + 1));
			else
				tmap->frames[current_frame]->tile_update = (tile_info_t**)wf_realloc(tmap->frames[current_frame]->tile_update,sizeof(tile_info_t*) * (tmap->frames[current_frame]->num_update + 1));

			tmap->frames[current_frame]->tile_update[tmap->frames[current_frame]->num_update] = (tile_info_t*)wf_malloc(sizeof(tile_info_t));

			tmap->frames[current_frame]->tile_update[tmap->frames[current_frame]->num_update]->id_tile = tileId;
			tmap->frames[current_frame]->tile_update[tmap->frames[current_frame]->num_update]->index = tilePos;
			tmap->frames[current_frame]->tile_update[tmap->frames[current_frame]->num_update]->draw = wfalse;

			tmap->frames[current_frame]->num_update++;
		}
		else if(tmap->frames[current_frame]->num_update > 0 && exist && same_as_previous)//if tile exist and is the same value than the previous tile value, remove the update
		{
			tmap->frames[current_frame]->num_update--;
			tile_info_t** new_ptr = (tile_info_t**)wf_malloc(sizeof(tile_info_t*) * (tmap->frames[current_frame]->num_update));

			int32_t old_indx = 0;

			for(uint32_t upd_indx = 0; upd_indx < tmap->frames[current_frame]->num_update;upd_indx++)
			{
				new_ptr[upd_indx] = (tile_info_t*)wf_malloc(sizeof(tile_info_t));

				if(upd_indx == i)
				{
					wf_free(tmap->frames[current_frame]->tile_update[old_indx]);
					old_indx++;
				}

				new_ptr[upd_indx]->id_tile = tmap->frames[current_frame]->tile_update[old_indx]->id_tile;
				new_ptr[upd_indx]->index = tmap->frames[current_frame]->tile_update[old_indx]->index;
				new_ptr[upd_indx]->draw = wfalse;
					
				wf_free(tmap->frames[current_frame]->tile_update[old_indx]);

				old_indx++;
			}

			wf_free(tmap->frames[current_frame]->tile_update);
			tmap->frames[current_frame]->tile_update = new_ptr;
		}

		TilemapLayer_update_texcoord(tmap,idtx,txtCol,txtRow,rendermngr,wtrue);
	}
}

void TilemapLayer_remove_tile_animation(TilemapLayer_t* const tmap,const int32_t tile_pos,const int32_t current_frame,render_manager_t* const rendermngr)
{
	if(tmap->animated)
	{
		wbool exist = wfalse;
		uint32_t i = 0;
		//search for a existing update
		for(i = 0; i < tmap->frames[current_frame]->num_update;i++)
		{
			if(tmap->frames[current_frame]->tile_update[i]->index == tile_pos)
			{
				exist = wtrue;
				break;
			}
		}

		if(!exist)
			return;

		tmap->frames[current_frame]->num_update--;
		tile_info_t** new_ptr = (tile_info_t**)wf_malloc(sizeof(tile_info_t*) * (tmap->frames[current_frame]->num_update));

		int32_t old_indx = 0;
		uint32_t upd_indx = 0;

		for(upd_indx = 0; upd_indx < tmap->frames[current_frame]->num_update;upd_indx++)
		{
			new_ptr[upd_indx] = (tile_info_t*)wf_malloc(sizeof(tile_info_t));
			wbool gfr = (wbool)(upd_indx == i);

			if(gfr)
			{
				wf_free(tmap->frames[current_frame]->tile_update[old_indx]);
				old_indx++;
			}

			new_ptr[upd_indx]->id_tile = tmap->frames[current_frame]->tile_update[old_indx]->id_tile;
			new_ptr[upd_indx]->index = tmap->frames[current_frame]->tile_update[old_indx]->index;
			new_ptr[upd_indx]->draw = wfalse;
					
			wf_free(tmap->frames[current_frame]->tile_update[old_indx]);

			old_indx++;
	   }

		if(i == upd_indx)
		{
			wf_free(tmap->frames[current_frame]->tile_update[i]);
		}

		wf_free(tmap->frames[current_frame]->tile_update);
		tmap->frames[current_frame]->tile_update = new_ptr;

		int32_t tileId = tmap->tileArray[tile_pos];

		int32_t txtRow = (int32_t)floorf((float)(tileId / tmap->txtColCount));//ligne Y
		int32_t txtCol = tileId - (tmap->txtColCount * txtRow);//ligne X
		int32_t idtx = tile_pos * 12;

		TilemapLayer_update_texcoord(tmap,idtx,txtCol,txtRow,rendermngr,wtrue);
	}
}

void TilemapLayer_setDestructible(TilemapLayer_t* const tmap,const wbool destructible, const int32_t tile_destruct)
{
	tmap->destructible = destructible;

	if(tmap->destructible)
		tmap->destruct_tile_id = tile_destruct;
	else
		tmap->destruct_tile_id = 0;
}

//update texture coords unit and number of columns in our texture (txtColCount)
void TilemapLayer_TextureChanged(TilemapLayer_t* tmap,render_manager_t* const render_mngr)
{
	if(tmap->textPointer->virtual_texture)
		tmap->txtColCount = tmap->textPointer->region->width / tmap->tileWidth;
	else
		tmap->txtColCount = tmap->textPointer->width / tmap->tileWidth;

	tmap->coordUnitX = (1.0f / (tmap->textPointer->width / tmap->tileWidth));
	tmap->coordUnitY = (1.0f / (tmap->textPointer->height / tmap->tileHeight));

	TilemapLayer_regenTexCoord(tmap,render_mngr);
}


void TilemapLayer_Draw(const TilemapLayer_t* tmap,render_manager_t* const render_mngr,Vector_t offset)
{
	int32_t startIndex = 0;
	int32_t i = 0;

	render_set_color(render_mngr,tmap->vertices_color);

	Vector_t cpos = render_mngr->scroll_vector;

	int32_t scroll_x = (int32_t)(((cpos.x * tmap->parallax_x) - cpos.x) + offset.x);
	int32_t scroll_y = (int32_t)(((cpos.y * tmap->parallax_y) - cpos.y) + offset.y);

	cpos.x = (wfloat)scroll_x;
	cpos.y = (wfloat)scroll_y;

	render_set_position(render_mngr, cpos);
	

	render_start_draw_buffer(render_mngr,tmap->textPointer->texture_pointer,tmap->VBOvertices,tmap->VBOtexcoord,0,2);

	//get number of row for the screen

	int16_t scr_row_count =  (int16_t)ceilf(((float)render_mngr->screen_info.height / (float)tmap->tileHeight) * render_mngr->zoom_factor) + TILEDRAW_EXTRA_TILE;

	int16_t scr_col_count = (int16_t)ceilf(((float)render_mngr->screen_info.width / (float)tmap->tileWidth) * render_mngr->zoom_factor) + TILEDRAW_EXTRA_TILE;



	int32_t tile_col = (int32_t)((render_mngr->screen_info.position.x - cpos.x)  / tmap->tileWidth);
	int32_t tile_row = (int32_t)((render_mngr->screen_info.position.y - cpos.y)  / tmap->tileHeight);

	if(tile_col < 0)
		tile_col = 0;
	else if(tile_col >= tmap->colCount)
		tile_col = tmap->colCount - 1;

	if(tile_row < 0)
		tile_row = 0;
	else if(tile_row >= tmap->rowCount)
		tile_row = tmap->rowCount -1;


	startIndex = ((tile_row * tmap->colCount) + tile_col) * 6;

	int32_t max_index =  (tmap->rowCount * tmap->colCount) * 6;

	if(tmap->colCount < scr_col_count)
		scr_col_count = tmap->colCount;

	if(tmap->rowCount < scr_row_count)
		scr_row_count = tmap->rowCount;
	
	
	//our layer triangle array start with the row at the bottom and end with the row at the top, invert draw order

	
	for(i = tile_row; i >= (tile_row - scr_row_count) && i > -1 ;i--)
	{
		int32_t count = scr_col_count * 6;

		if(startIndex + count > max_index)
			count = max_index - startIndex;

		render_draw_buffer(render_mngr,startIndex,count);

		startIndex -= (tmap->colCount * 6);
	}


	render_end_draw_buffer(render_mngr,wtrue,wfalse);


	render_reset_color(render_mngr);

}

void TilemapLayer_clearframe(TilemapLayer_t* const tmap,render_manager_t* const render_mngr)
{
	//reset tiles updates from existing tilemap definition
	for(uint32_t tile_pos = 0; tile_pos < tmap->tileArraySize;tile_pos++)
	{
		if(tmap->tileFrameArray[tile_pos] != tmap->tileArray[tile_pos])
		{
			int32_t tileId = tmap->tileArray[tile_pos];

			int32_t txtRow = (int32_t)floorf((float)(tileId / tmap->txtColCount));//ligne Y
			int32_t txtCol = tileId - (tmap->txtColCount * txtRow);//ligne X
			int32_t idtx = tile_pos * 12;
			tmap->tileFrameArray[tile_pos] =  tmap->tileArray[tile_pos];

			TilemapLayer_update_texcoord(tmap,idtx,txtCol,txtRow,render_mngr,wfalse);
		}
	}

	TilemapLayer_synctexcoord(tmap,render_mngr);

	tmap->current_step_time = 0;
}

void TilemapLayer_gotoframe(TilemapLayer_t* const tmap,uint32_t frame,render_manager_t* const render_mngr)
{
	if(tmap->num_frames == 0 || frame >= tmap->num_frames)
		return;

	tmap->current_frame = frame;
	tmap->current_step_time = 0;

	//update tilemap
	for(uint32_t indx_upd = 0; indx_upd < tmap->frames[tmap->current_frame]->num_update;indx_upd++)
	{
		int32_t tilePos = tmap->frames[tmap->current_frame]->tile_update[indx_upd]->index;
		int32_t tileId = tmap->frames[tmap->current_frame]->tile_update[indx_upd]->id_tile;

		int32_t txtRow = (int32_t)floorf((float)(tileId / tmap->txtColCount));//ligne Y
		int32_t txtCol = tileId - (tmap->txtColCount * txtRow);//ligne X
		int32_t idtx = tilePos * 12;

		tmap->tileFrameArray[tilePos] = tileId;

		TilemapLayer_update_texcoord(tmap,idtx,txtCol,txtRow,render_mngr,wfalse);
	}

	TilemapLayer_synctexcoord(tmap,render_mngr);
}

void TilemapLayer_Update(TilemapLayer_t* const tmap,const float deltaTime,render_manager_t* const render_mngr)
{
	if(tmap->num_frames == 0)
		return;

	float goaltimestep = (tmap->current_frame == 0) ? tmap->frames[tmap->current_frame]->time_step : tmap->frames[tmap->current_frame-1]->time_step;

	if(tmap->current_step_time == 0.0f)
    {
		wbool resync_textcoord = wfalse;

		if( tmap->current_frame != 0)
		{

			//update tilemap
			for(uint32_t indx_upd = 0; indx_upd < tmap->frames[tmap->current_frame-1]->num_update;indx_upd++)
			{
				int32_t tilePos = tmap->frames[tmap->current_frame-1]->tile_update[indx_upd]->index;
				int32_t tileId = tmap->frames[tmap->current_frame-1]->tile_update[indx_upd]->id_tile;

				if(tmap->tileFrameArray[tilePos] == tileId)//don't update if the current tile value is the same as the update
					continue;

				int32_t txtRow = (int32_t)floorf((float)(tileId / tmap->txtColCount));//ligne Y
				int32_t txtCol = tileId - (tmap->txtColCount * txtRow);//ligne X
				int32_t idtx = tilePos * 12;

				tmap->tileFrameArray[tilePos] = tileId;
				TilemapLayer_update_texcoord(tmap,idtx,txtCol,txtRow,render_mngr,wfalse);
				resync_textcoord = wtrue;
			}

			
		}
		else
		{
			//reset tiles updates from existing tilemap definition
			for(uint32_t tile_pos = 0; tile_pos < tmap->tileArraySize;tile_pos++)
			{
				if(tmap->tileFrameArray[tile_pos] != tmap->tileArray[tile_pos])
				{
					int32_t tileId = tmap->tileArray[tile_pos];

					int32_t txtRow = (int32_t)floorf((float)(tileId / tmap->txtColCount));//ligne Y
					int32_t txtCol = tileId - (tmap->txtColCount * txtRow);//ligne X
					int32_t idtx = tile_pos * 12;
					tmap->tileFrameArray[tile_pos] =  tmap->tileArray[tile_pos];

					TilemapLayer_update_texcoord(tmap,idtx,txtCol,txtRow,render_mngr,wfalse);
					resync_textcoord = wtrue;
				}
			}

		}


		if(resync_textcoord)
			TilemapLayer_synctexcoord(tmap,render_mngr);


    }
	else if(tmap->current_step_time >= goaltimestep)
	{
		if(++tmap->current_frame > tmap->num_frames)
		{
			tmap->current_frame = 0;
		}

		tmap->current_step_time = 0.0f;
		return;
	}

	tmap->current_step_time += deltaTime;
	
}

//cleanup function
void TilemapLayer_Free(TilemapLayer_t* tmap)
{
	//free frames data
	if(tmap->animated)
		TilemapLayer_clearanimated(tmap);

	render_delete_buffer(tmap->VBOvertices);
	render_delete_buffer(tmap->VBOtexcoord);

	wf_free(tmap->textureCoord);
	tmap->textureCoord = NULL;
	wf_free(tmap->vertices);
	tmap->vertices = NULL;
	wf_free(tmap->tileArray);
	tmap->tileArray = NULL;

	if(tmap->shader_parameters != NULL)
	{
		for(uint32_t i_param = 0; i_param < tmap->num_parameters;i_param++)
		{
			if(tmap->shader_parameters[i_param]->param_type == TYPE_PARAM_SHADER__attribute_array)
			{
				if(tmap->shader_parameters[i_param]->attribute_array->has_array_ref)
				{
					VBO_ID vbo;

					memcpy(&vbo,tmap->shader_parameters[i_param]->attribute_array->array_ref.data,sizeof(VBO_ID));

					render_delete_buffer(vbo);
				}
			}

			shaders_parameters__free_unpacked(tmap->shader_parameters[i_param],NULL);
		}

		wf_free(tmap->shader_parameters);
	}

	tmap->shader_parameters = NULL;
	tmap->program_id = render_get_emptyprogvalue();
	tmap->num_parameters = 0;
}
