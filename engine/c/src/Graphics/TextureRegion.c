#include <inttypes.h>
#include <stdio.h>
#include <Engine.h>
#include <Graphics/TextureRegion.h>



void TextureRegion_Init(TextureRegion_t* texRegion,float texWidth,float textHeight, float x, float y, float width, float height)
{
    texRegion->u1 = x / texWidth;
	texRegion->v1 = (y / textHeight) + 0.01f;
	texRegion->u2 = texRegion->u1 + (width / texWidth);
	texRegion->v2 = texRegion->v1 + (height / textHeight);
}

void TextureRegion_Init2(TextureRegion_t* texRegion, float u1, float v1, float u2, float v2)
{
	texRegion->u1 = u1;
	texRegion->v1 = v1;
	texRegion->u2 = u2;
	texRegion->v2 = v2;
}