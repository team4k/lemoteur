#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>

#include <inttypes.h>

#if defined(_WIN32)
    #include <Windows.h>
#endif

#include <Engine.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/geom.h>
#include <Base/geom3D.h>
#include <Base/Color.h>



#include <Debug/Logprint.h>

#include <Render.h>
#include <Render/render_manager.h>

#include <Scripting/ScriptPointers.h>

#include <Graphics/Mesh.h>

typedef void(*mesh_generate_t)(Vector3_t** outverts,uint16_t** outindexes, size_t* out_num_vertices, size_t* out_num_indices);


static void generate_cylinder(Vector3_t** outverts, uint16_t** outindexes, size_t* out_num_vertices, size_t* out_num_indices)
{
	// Cylinder with y axis up
	float cylinder_height = 1.0f,
		cylinder_radius = 0.5f;

	uint16_t nr_of_points_cylinder = 360;


	(*outverts) = (Vector3_t*)wf_calloc((nr_of_points_cylinder * 2)+1, sizeof(Vector3_t));

	uint16_t i = 1;

	(*outverts)[0] = Vec3(0.0f, 1.0f, 0.0f);

	while (i < (nr_of_points_cylinder * 2) + 1)
	{
		float u = (float)i / (float)nr_of_points_cylinder;

		(*outverts)[i] = Vec3(cylinder_radius*cos(2 * WM_PI *u), 0, cylinder_radius*sin(2 * WM_PI*u));
		(*outverts)[i+1] = Vec3(cylinder_radius*cos(2 * WM_PI *u), cylinder_height, cylinder_radius*sin(2 * WM_PI*u));
		i += 2;
	}

	//g�n�ration index
	(*outindexes) = (uint16_t*)wf_calloc(nr_of_points_cylinder * 9, sizeof(uint16_t));

	uint16_t ivertices = 1;
	uint16_t iindexes = 0;

	while (iindexes < (nr_of_points_cylinder * 9))
	{
		if (ivertices + 2 < (nr_of_points_cylinder * 2) + 1)
		{
			(*outindexes)[iindexes] = ivertices;
			(*outindexes)[iindexes + 1] = ivertices + 1;
			(*outindexes)[iindexes + 2] = ivertices + 3;

			(*outindexes)[iindexes + 3] = ivertices;
			(*outindexes)[iindexes + 4] = ivertices + 3;
			(*outindexes)[iindexes + 5] = ivertices + 2;

			//g�n�ration du haut du cylindre
			(*outindexes)[iindexes + 6] = 0;
			(*outindexes)[iindexes + 7] = ivertices + 3; 
			(*outindexes)[iindexes + 8] = ivertices + 1;
		}
		else {
			//on referme le cylindre
			(*outindexes)[iindexes] = ivertices;
			(*outindexes)[iindexes + 1] = ivertices + 1;
			(*outindexes)[iindexes + 2] = 2;

			(*outindexes)[iindexes + 3] = ivertices;
			(*outindexes)[iindexes + 4] = 2;
			(*outindexes)[iindexes + 5] = 1;

			//g�n�ration du haut du cylindre
			(*outindexes)[iindexes + 6] = 0;
			(*outindexes)[iindexes + 7] = 2;
			(*outindexes)[iindexes + 8] = ivertices + 1;
		}


		iindexes += 9;

		ivertices += 2;
	}

	(*out_num_vertices) = nr_of_points_cylinder * 2;
	(*out_num_indices) = nr_of_points_cylinder * 9;
}

typedef struct
{
	const Vector3_t* verts;
	const uint16_t* indexes;
	size_t num_vertices;
	size_t num_indices;
	mesh_generate_t gen_func;

} _template_def;


static const Vector3_t tetrahedron_verts[] =
{
	 {0.0f, 1.0f, 0.0f}, 
	 {0.943f, -0.333f, 0.0f},
	 {-0.471f, -0.333f, -0.8165f},
	 {-0.471f, -0.333f, 0.8165f}, 
	 
};


//CCW
static const uint16_t tetrahedron_indexes[] =
{
	//front triangle
	0,3,1, 
	//right side triangle
	0,1,2,
	//left side triangle
	0,2,3,
	//bottom triangle
	3,2,1
};

static const Vector3_t cube_verts[] = {
	{-0.5f,-0.5f,-0.5f},
	{0.5f,-0.5f,-0.5f},

	{-0.5f,-0.5f,0.5f},
	{0.5f,-0.5f,0.5f},

	{-0.5f,0.5f,-0.5f},
	{0.5f,0.5f,-0.5f},

	{-0.5f,0.5f,0.5f},
	{0.5f,0.5f,0.5f},
};

static const uint16_t cube_indexes[] = 
{
	//bottom rectangle
	0,3,2,0,1,3,

	//front rectangle
	2,7,6,2,3,7,

	//top rectangle
	4,6,7,4,7,5,

	//right rectangle
	7,3,1,7,1,5,

	//back rectangle
	0,5,1,0,4,5,

	//left rectangle
	0,2,6,0,6,4
};


static const _template_def template_array[]=
{
	{NULL,NULL,0,0,NULL},//MT_NONE
	{tetrahedron_verts,tetrahedron_indexes,4,12,NULL},
	{cube_verts,cube_indexes,8,36,NULL},
	{NULL,NULL,0,0,generate_cylinder},
};


static void mesh_genvertices(Mesh_t* const mesh_obj,const Vector3_t* const vertices,const uint16_t* const indices,size_t num_vertices,size_t num_indices,wbool generate_normals,render_manager_t* const render_mngr)
{
	if(!generate_normals)
	{
		if(mesh_obj->vertices_data == NULL)
			mesh_obj->vertices_data = (float*)wf_calloc(num_vertices * 3,sizeof(float));


		uint32_t orig_vert = 0;

		for(uint32_t i_vert = 0;i_vert < num_vertices * 3;i_vert+=3)
		{
			mesh_obj->vertices_data[i_vert] = vertices[orig_vert].x * mesh_obj->width;
			mesh_obj->vertices_data[i_vert+1] = vertices[orig_vert].y * mesh_obj->height;
			mesh_obj->vertices_data[i_vert+2] = vertices[orig_vert].z * mesh_obj->depth;

			orig_vert++;
		}

		if(mesh_obj->indices_data == NULL)
			mesh_obj->indices_data = (uint16_t*)wf_calloc(num_indices,sizeof(uint16_t));

		for(uint16_t i_indices = 0;i_indices < num_indices;i_indices++)
			mesh_obj->indices_data[i_indices] = indices[i_indices];
	}
	else
	{
		//calculate the normals, in our case we just use the surface normal for each vertices since the defined mesh are quite simple, need to use smooth normal in case of more complex mesh
		if(mesh_obj->vertices_data == NULL)
			mesh_obj->vertices_data = (float*)wf_calloc(num_indices * 3,sizeof(float));

		num_vertices = num_indices;

		uint32_t i_vert = 0;


		for(uint16_t i_indices = 0;i_indices < num_indices;i_indices++)
		{
			mesh_obj->vertices_data[i_vert] = vertices[indices[i_indices]].x * mesh_obj->width;
			mesh_obj->vertices_data[i_vert+1] = vertices[indices[i_indices]].y * mesh_obj->height;
			mesh_obj->vertices_data[i_vert+2] = vertices[indices[i_indices]].z * mesh_obj->depth;
			i_vert +=3;
		}


		if(mesh_obj->indices_data == NULL)
			mesh_obj->indices_data = (uint16_t*)wf_calloc(num_indices,sizeof(uint16_t));

		for(uint16_t i_indices = 0;i_indices < num_indices;i_indices++)
			 mesh_obj->indices_data[i_indices] = i_indices;
	

		if(mesh_obj->normal_data == NULL)
			mesh_obj->normal_data = (float*)wf_calloc(num_indices * 3,sizeof(float));

		int32_t i_normal = 0;

		for(uint16_t i_face = 0;i_face < num_indices / 3;i_face++)
		{
			//get the three vector for the face
			Vector3_t p1 = vertices[indices[(i_face * 3)]];
			Vector3_t p2 = vertices[indices[(i_face * 3) +1]];
			Vector3_t p3 = vertices[indices[(i_face * 3) +2]];

			Vector3_t a = Vec3_minus(p2,p1);
			Vector3_t b = Vec3_minus(p3,p1);

			Vector3_t normal = Vec3_cross(b,a);

			Vec3_normalise(&normal);

			for(int16_t i_v = 0;i_v < 3;i_v++)
			{
				mesh_obj->normal_data[i_normal] = normal.x;
				mesh_obj->normal_data[i_normal+1] = normal.y;
				mesh_obj->normal_data[i_normal+2] = normal.z;
				i_normal += 3;
			}
		}


		mesh_obj->normal_id = render_gen_buffer(render_mngr,mesh_obj->normal_data,num_indices * 3 * sizeof(float),wfalse,wfalse);
	}



	//generate vbo
	mesh_obj->vertices_id = render_gen_buffer(render_mngr,mesh_obj->vertices_data,num_vertices * 3 * sizeof(float),wfalse,wfalse);
	mesh_obj->indices_id = render_gen_bufferindices(render_mngr,mesh_obj->indices_data,num_indices * sizeof(uint16_t),wfalse,wfalse);
	mesh_obj->num_indices = num_indices;

}



void mesh_init(Mesh_t* const mesh_obj,const int32_t width,const int32_t height,const int32_t depth,mesh_template_type template_type,render_manager_t* const render_mngr)
{
	mesh_obj->width = width;
	mesh_obj->height = height;
	mesh_obj->depth = depth;
	mesh_obj->m_template = template_type;
	mesh_obj->vertices_data = NULL;
	mesh_obj->indices_data = NULL;
	mesh_obj->normal_data = NULL;
	mesh_obj->position = vector3zero;
	mesh_obj->rotation_x = mesh_obj->rotation_y = mesh_obj->rotation_z = 0.0f;
	mesh_obj->mesh_mode = R_FILL;
	mesh_obj->color = white_color;
	mesh_obj->mat_flag = 0;
	mesh_obj->script_id = 0;

	mesh_obj->scale_x = 1.0f;
	mesh_obj->scale_y = 1.0f;
	mesh_obj->scale_z = 1.0f;

	if(template_type != MT_NONE)
	{
		//gen vertices from existing template
		//soit on utilise des vertices et index d�j� existant, soit on utilise une fonction de g�n�ration
		if (template_array[(int)template_type].gen_func == NULL)
		{
			mesh_genvertices(mesh_obj, template_array[(int)template_type].verts, template_array[(int)template_type].indexes, template_array[(int)template_type].num_vertices, template_array[(int)template_type].num_indices, wtrue, render_mngr);
		}
		else
		{
			Vector3_t** outverts = (Vector3_t**)wf_calloc(1, sizeof(Vector3_t*));
			uint16_t** outindexes = (uint16_t**)wf_calloc(1, sizeof(uint16_t*));
			size_t out_num_vertices = 0;
			size_t out_num_indices = 0;

			template_array[(int)template_type].gen_func(outverts, outindexes, &out_num_vertices, &out_num_indices);

			mesh_genvertices(mesh_obj, outverts[0], outindexes[0], out_num_vertices, out_num_indices, wtrue, render_mngr);

			wf_free(outverts[0]);
			wf_free(outindexes[0]);
			wf_free(outverts);
			wf_free(outindexes);
		}
		
	}


}

void mesh_setposition(Mesh_t* const mesh_obj,Vector3_t position)
{
	mesh_obj->position = position;
}

void mesh_setrotation(Mesh_t* const mesh_obj,float rot_x,float rot_y,float rot_z)
{
	mesh_obj->rotation_x = rot_x;
	mesh_obj->rotation_y = rot_y;
	mesh_obj->rotation_z = rot_z;
}

void mesh_setscale(Mesh_t* const mesh_obj, float scale_x, float scale_y, float scale_z)
{
	mesh_obj->scale_x = scale_x;
	mesh_obj->scale_y = scale_y;
	mesh_obj->scale_z = scale_z;
}

void mesh_setrendermode(Mesh_t* const mesh_obj,render_mode mesh_mode)
{
	mesh_obj->mesh_mode = mesh_mode;
}

void mesh_setcolor(Mesh_t* const mesh_obj,ColorA_t color)
{
	mesh_obj->color = color;
}

void mesh_flicker(Mesh_t* const mesh,float flicker_time)
{
	mesh->flicker_time = flicker_time;
	mesh->flicker_step_time = 0;
}

void mesh_draw(Mesh_t* const mesh_obj,render_manager_t* const render_mngr)
{
	if(mesh_obj->skip_draw)
		return;

	int old_mat_flag = render_mngr->mat_flag;

	if(mesh_obj->mat_flag != 0)
		render_mngr->mat_flag = mesh_obj->mat_flag;

	render_set_color(render_mngr,mesh_obj->color);

	//render_push_matrix(render_mngr);

	render_set_position3(render_mngr,mesh_obj->position);

	render_set_scale(render_mngr, mesh_obj->scale_x, mesh_obj->scale_y, mesh_obj->scale_z);

	render_set_rotation2(render_mngr,mesh_obj->rotation_x,mesh_obj->rotation_y,mesh_obj->rotation_z);

	render_draw_3Dbufferindices(render_mngr,mesh_obj->vertices_id,mesh_obj->indices_id,mesh_obj->normal_id,mesh_obj->num_indices,mesh_obj->mesh_mode,(wbool)(mesh_obj->normal_data != NULL));

	//render_pop_matrix(render_mngr);

	render_reset_color(render_mngr);

	if(mesh_obj->mat_flag != 0)
		render_mngr->mat_flag = old_mat_flag;
}

void mesh_update(Mesh_t* const mesh_obj,float elapsed)
{
	//flickering
	if(mesh_obj->flicker_time > 0)
	{
		mesh_obj->flicker_time -= elapsed;
		mesh_obj->flicker_step_time -= elapsed;

		if(mesh_obj->flicker_step_time <= 0)
		{
			mesh_obj->skip_draw = (mesh_obj->skip_draw == wtrue) ? wfalse : wtrue;
			mesh_obj->flicker_step_time = (mesh_obj->skip_draw)? M_FLICKER_HIDE_STEP : M_FLICKER_DRAW_STEP;
		}

	}
	else
	{
		mesh_obj->skip_draw = wfalse;

		//fade in / out
		/*if(mesh_obj->dofade)
		{
			mesh_obj->fade_amount += (mesh_obj->fade_amout_step * elapsed);

			if(mesh_obj->fade_amount <= 0.0f || mesh_obj->fade_amount >= 1.0f)
				mesh_obj->dofade = wfalse;

			if(mesh_obj->fade_amount < 0.0f)
				mesh_obj->fade_amount = 0.0f;

			if(mesh_obj->fade_amount > 1.0f)
				mesh_obj->fade_amount = 1.0f;
		}*/
	}
}


void mesh_g_create(Mesh_t* const mesh_obj,render_manager_t* const render_mngr)
{
	//TODO : regenerate VBO
}

void mesh_g_clean(Mesh_t* const mesh_obj)
{
	//TODO : clean VBO
}

void mesh_free(Mesh_t* mesh_obj)
{
	if (mesh_obj->script_id != 0)
	{
		script_unregisterpointer(mesh_obj->script_id);
		mesh_obj->script_id = 0;
	}

	if(mesh_obj->vertices_data != NULL)
		wf_free(mesh_obj->vertices_data);

	if(mesh_obj->indices_data != NULL)
		wf_free(mesh_obj->indices_data);

	mesh_obj->vertices_data = NULL;
	mesh_obj->indices_data = NULL;
}