#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <stdlib.h>
#include <inttypes.h>
#include <math.h>
#include <time.h>

#if defined(_WIN32)
    #include <Windows.h>
#endif


#if !defined (__APPLE__)
#include <malloc.h>
#endif

#include <Engine.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/geom.h>
#include <Base/Color.h>
#include <Utils/Hash.h>

#include <Debug/Logprint.h>

#include <Render.h>
#include <Render/render_manager.h>

#include <Graphics/TextureLoader.h>

#include <GameObjects/Characters.pb-c.h>
#include <Graphics/Animation.h>


#include <Graphics/Sprite.h>

#include <Scripting/ScriptPointers.h>


void sprite_genvertices_vbo(sprite_t* const sprite,render_manager_t* const render_manager)
{
	int32_t num_repeat = (sprite->repeat_x + 1) * (sprite->repeat_y + 1);

	size_t buffer_size = (12 * num_repeat) * sizeof(float);

	if(sprite->vertices == NULL)
		sprite->vertices = (float*)wf_malloc(buffer_size);
	else if(sprite->vertices_buffer_size != buffer_size)
		sprite->vertices = (float*)wf_realloc(sprite->vertices,buffer_size);

	sprite->vertices_buffer_size = buffer_size;

	float* vertices = sprite->vertices;

	int32_t ivertices = 0;


	float width_chunk = (float)(sprite->width / (sprite->repeat_x + 1));
	float height_chunk = (float)(sprite->height / (sprite->repeat_y + 1));

	float halfWidth = (sprite->width) *0.5f;
	float halfHeight = (sprite->height) * 0.5f;

	float halfw = -halfWidth;
	float halfh = -halfHeight;


	for(int32_t ry = 1; ry <= sprite->repeat_y + 1 ;ry++)
	{
		for(int32_t rx = 1; rx <= sprite->repeat_x + 1;rx++)
		{
			vertices[ivertices++] = halfw;
			vertices[ivertices++] = halfh;

			vertices[ivertices++] = halfw + width_chunk;
			vertices[ivertices++] = halfh;

			vertices[ivertices++] = halfw + width_chunk;
			vertices[ivertices++] = halfh + height_chunk;

			vertices[ivertices++] = halfw + width_chunk;
			vertices[ivertices++] = halfh + height_chunk;

			vertices[ivertices++] = halfw;
			vertices[ivertices++] = halfh + height_chunk;

			vertices[ivertices++] = halfw;
			vertices[ivertices++] = halfh;

			halfw += width_chunk;
		}

		halfw = -halfWidth;
		halfh += height_chunk;
	}

	if(!sprite->in_group)
	{
		if(!sprite->has_vert_vbo)
		{
			sprite->vertices_vbo = render_gen_buffer(render_manager,vertices,buffer_size,wfalse,wfalse);//create vbo and upload data
			sprite->has_vert_vbo = wtrue;
		}
		else
			render_discard_updatebuffer(render_manager,sprite->vertices_vbo,vertices,buffer_size,wfalse,wfalse);
	}

	sprite->vertices_needupdate = wfalse;
	sprite->num_vertices = (6 * num_repeat);
}

void sprite_setvertices(sprite_t* const sprite,Vector_t* verts,size_t vert_count,render_manager_t* const render_manager)
{
	int32_t ch_vert_count = (vert_count * 2) - (vert_count % 2);

	size_t buffer_size = (ch_vert_count * 2) * sizeof(float); 

	if(sprite->vertices == NULL)
		sprite->vertices = (float*)wf_malloc(buffer_size);
	else if(sprite->vertices_buffer_size != buffer_size)
		sprite->vertices = (float*)wf_realloc(sprite->vertices,buffer_size);

	if(sprite->tex_coords == NULL)
		sprite->tex_coords = (float*)wf_malloc(buffer_size);
	else if(sprite->tex_coords_buffer_size != buffer_size)
		sprite->tex_coords = (float*)wf_realloc(sprite->tex_coords,buffer_size);

	sprite->tex_coords_buffer_size = buffer_size;

	sprite->vertices_buffer_size = buffer_size;

	float* vertices = sprite->vertices;
	float* texcoords = sprite->tex_coords;

	uint32_t i_ele = 0;
	uint32_t i_tex = 0;

	for(uint32_t ivert = 1;ivert< vert_count - 1;ivert++)
	{
		vertices[i_ele++] = (float)verts[0].x;
		vertices[i_ele++] = (float)verts[0].y;

		vertices[i_ele++] = (float)verts[ivert].x;
		vertices[i_ele++] = (float)verts[ivert].y;

		vertices[i_ele++] = (float)verts[ivert+1].x;
		vertices[i_ele++] = (float)verts[ivert+1].y;

		texcoords[i_tex] = texcoords[i_tex+1] = texcoords[i_tex+2] = texcoords[i_tex+3] = texcoords[i_tex+4] = texcoords[i_tex+5] = 0.0f;

		i_tex += 6;
	}

	sprite->num_vertices = ch_vert_count;

	if(!sprite->in_group)
	{
		if(!sprite->has_vert_vbo)
		{
			sprite->vertices_vbo = render_gen_buffer(render_manager,vertices,buffer_size,wfalse,wfalse);//create vbo and upload data
			sprite->has_vert_vbo = wtrue;
		}
		else
			render_discard_updatebuffer(render_manager,sprite->vertices_vbo,vertices,buffer_size,wfalse,wfalse);

		if(!sprite->has_tex_vbo)
		{
			sprite->tex_coords_vbo = render_gen_buffer(render_manager,texcoords,buffer_size,wfalse,wfalse);
			sprite->has_tex_vbo = wtrue;
		}
		else
		{
			render_discard_updatebuffer(render_manager,sprite->tex_coords_vbo,texcoords,buffer_size,wfalse,wfalse);
		}
	}

	sprite->tex_coord_needupdate = wfalse;

	sprite->vertices_needupdate = wfalse;

}

void sprite_gentexturesvbo(sprite_t* const sprite,render_manager_t* const render_manager)
{

	int32_t num_repeat = (sprite->repeat_x + 1) * (sprite->repeat_y + 1);

	size_t buffer_size = (12 * num_repeat) * sizeof(float);

	if(sprite->tex_coords == NULL)
		sprite->tex_coords = (float*)wf_malloc(buffer_size);
	else if(sprite->tex_coords_buffer_size != buffer_size)
		sprite->tex_coords = (float*)wf_realloc(sprite->tex_coords,buffer_size);

	sprite->tex_coords_buffer_size = buffer_size;

	float* tmpTexCoord = sprite->tex_coords;


	float offset_coord_x = 0.0f;
	float offset_coord_y = 0.0f;

	float step_coord_x = sprite->coordUnitX;
	float step_coord_y = sprite->coordUnitY;

	int32_t itxtcoord = 0;

	int32_t prev_frame = -1;
	float x1 = 0,y1 = 0,x2 = 0,y2 = 0;
	wfloat xstride = WFLOAT0;
	wfloat ystride = WFLOAT0;

	if (sprite->mirror_texture_x && !sprite->repeat_texture && sprite->repeat_x > 0)
	{
		xstride = step_coord_x * num_repeat;
	}

	if (sprite->mirror_texture_y && !sprite->repeat_texture && sprite->repeat_y > 0)
	{
		ystride = step_coord_y * num_repeat;
	}

	for(int32_t r = 0; r < num_repeat;r++)
	{

		if(prev_frame != sprite->frame[r]) //recompute tex coords if needed
		{
			if(sprite->animated && sprite->current_anim != NULL)
			{
				int32_t width = sprite->width;

				if(sprite->current_anim->anim_def->has_width)
					width = sprite->current_anim->anim_def->width;

				int32_t height = sprite->height;

				if(sprite->current_anim->anim_def->has_height)
					height = sprite->current_anim->anim_def->height;


				if(sprite->texture_pointer->virtual_texture)
				{
					offset_coord_x = (float)(sprite->current_anim->anim_def->offsetx + (sprite->current_anim->frame_counter * width))  / sprite->texture_pointer->width;
					offset_coord_y = (float)sprite->current_anim->anim_def->offsety / sprite->texture_pointer->height;
				}
				else
				{
					offset_coord_x = (float)(sprite->current_anim->anim_def->offsetx + (sprite->current_anim->frame_counter * width))  / sprite->texture_width;
					offset_coord_y = (float)sprite->current_anim->anim_def->offsety / sprite->texture_height;
				}
			}
			else
			{
				int32_t txtRow = (int32_t)floorf((float)(sprite->frame[r] / sprite->txtColCount));//ligne Y
				int32_t txtCol = sprite->frame[r] - (sprite->txtColCount * txtRow);//ligne X

				offset_coord_x = txtCol * sprite->coordUnitX;
				offset_coord_y = txtRow * sprite->coordUnitY;

			}

			//precompute texture coordinates


			if(sprite->texture_pointer->virtual_texture)
			{
				float basepointx = (float)sprite->texture_pointer->region->position.x;
				float basepointy = (float)sprite->texture_pointer->region->position.y;

				x1 = (basepointx  / sprite->texture_pointer->width) + offset_coord_x;
				y2 = (basepointy / sprite->texture_pointer->height) + offset_coord_y;

				x2 = step_coord_x + x1;
				y1 = step_coord_y + y2;
			}
			else
			{
				x1 = offset_coord_x;
				y2 = offset_coord_y;

				y1 = (step_coord_y + y2);
				x2 = (step_coord_x + x1);
			}

			prev_frame = sprite->frame[r];
		}

		if (!sprite->repeat_texture && r > 0)
		{
			if(!sprite->mirror_texture_x)
				xstride += step_coord_x;
			else {
				xstride -= step_coord_x;
			}

			if (sprite->repeat_y > 0 && (r % (sprite->repeat_y + 1)) == 0)
			{
				if (!sprite->mirror_texture_y) {
					ystride += step_coord_y;
				}
				else
				{
					ystride -= step_coord_y;
				}


				if (sprite->mirror_texture_x && !sprite->repeat_texture)
				{
					xstride = step_coord_x * num_repeat;
				}
				else {
					xstride = 0;
				}
				
				
			}
			
		}


		if(!sprite->mirror_texture_x && !sprite->mirror_texture_y)
		{
			tmpTexCoord[itxtcoord++] = x1 + xstride;
			tmpTexCoord[itxtcoord++] = y1 + ystride;
	
			tmpTexCoord[itxtcoord++] = x2 + xstride;
			tmpTexCoord[itxtcoord++] = y1 + ystride;
	
			tmpTexCoord[itxtcoord++] = x2 + xstride;
			tmpTexCoord[itxtcoord++] = y2 + ystride;
	
			tmpTexCoord[itxtcoord++] = x2 + xstride;
			tmpTexCoord[itxtcoord++] = y2 + ystride;
	
			tmpTexCoord[itxtcoord++] = x1 + xstride;
			tmpTexCoord[itxtcoord++] = y2 + ystride;
	
			tmpTexCoord[itxtcoord++] = x1 + xstride;
			tmpTexCoord[itxtcoord++] = y1 + ystride;
		}
		else if(sprite->mirror_texture_x)
		{
			tmpTexCoord[itxtcoord++] = x2 + xstride;
			tmpTexCoord[itxtcoord++] = y1 + ystride;
	
			tmpTexCoord[itxtcoord++] = x1 + xstride;
			tmpTexCoord[itxtcoord++] = y1 + ystride;
	
			tmpTexCoord[itxtcoord++] = x1 + xstride;
			tmpTexCoord[itxtcoord++] = y2 + ystride;
	
			tmpTexCoord[itxtcoord++] = x1 + xstride;
			tmpTexCoord[itxtcoord++] = y2 + ystride;
	
			tmpTexCoord[itxtcoord++] = x2 + xstride;
			tmpTexCoord[itxtcoord++] = y2 + ystride;
	
			tmpTexCoord[itxtcoord++] = x2 + xstride;
			tmpTexCoord[itxtcoord++] = y1 + ystride;
		}
		else if(sprite->mirror_texture_y)
		{
			tmpTexCoord[itxtcoord++] = x1 + xstride;
			tmpTexCoord[itxtcoord++] = y2 + ystride;
	
			tmpTexCoord[itxtcoord++] = x2 + xstride;
			tmpTexCoord[itxtcoord++] = y2 + ystride;
	
			tmpTexCoord[itxtcoord++] = x2 + xstride;
			tmpTexCoord[itxtcoord++] = y1 + ystride;
	
			tmpTexCoord[itxtcoord++] = x2 + xstride;
			tmpTexCoord[itxtcoord++] = y1 + ystride;
	
			tmpTexCoord[itxtcoord++] = x1 + xstride;
			tmpTexCoord[itxtcoord++] = y1 + ystride;
	
			tmpTexCoord[itxtcoord++] = x1 + xstride;
			tmpTexCoord[itxtcoord++] = y2 + ystride;
		}
	}
  
	if(!sprite->in_group)
	{
		if(!sprite->has_tex_vbo)
		{
			sprite->tex_coords_vbo = render_gen_buffer(render_manager,tmpTexCoord,buffer_size,wfalse,wfalse);
			sprite->has_tex_vbo = wtrue;
		}
		else
		{
			render_discard_updatebuffer(render_manager,sprite->tex_coords_vbo,tmpTexCoord,buffer_size,wfalse,wfalse);
		}
	}

	sprite->tex_coord_needupdate = wfalse;
}


static void new_frame_callback(void* data)
{
	((sprite_t*)data)->tex_coord_needupdate = wtrue;
}



void sprite_init(sprite_t* const sprite,const int32_t sprite_width,const int32_t sprite_height, const int32_t texture_width, const int32_t texture_height,texture_info* const texture_pointer,const AnimationList* const animation_def,int32_t repeat_x,int32_t repeat_y,render_manager_t* const render_manager,wbool in_group,int32_t start_frame,float scale_x,float scale_y,wbool repeat_texture)
{
    sprite->width = sprite_width;
    sprite->height = sprite_height;
	sprite->repeat_x = repeat_x;
	sprite->repeat_y = repeat_y;
	sprite->repeat_texture = repeat_texture;
	sprite->tex_coords_vbo = render_get_emptybuffervalue();
	sprite->vertices_vbo = render_get_emptybuffervalue();
	sprite->tex_coord_needupdate = wfalse;
	sprite->vertices_needupdate = wfalse;
	sprite->tex_coords = NULL;
	sprite->vertices = NULL;
	sprite->in_group = in_group;
	sprite->scale_x = scale_x;
	sprite->scale_y = scale_y;
	sprite->num_vertices = 6;
	sprite->mat_flag = 0;
	sprite->script_id = 0;
	sprite->max_anim_name_size = 0;

	sprite->storedoffset = vectorzero;

	sprite->has_tex_vbo = sprite->has_vert_vbo = wfalse;

	if(sprite->repeat_x < 0)
		sprite->repeat_x = 0;

	if(sprite->repeat_y < 0)
		sprite->repeat_y = 0;

	if(!texture_pointer->virtual_texture)
	{
		sprite->texture_width = texture_width;
		sprite->texture_height = texture_height;
		sprite->coordUnitX = (1.0f / ((float)sprite->texture_width / (float)(sprite->width / (sprite->repeat_x + 1) )));
		sprite->coordUnitY = (1.0f / ((float)sprite->texture_height / (float)(sprite->height / (sprite->repeat_y + 1))));
	}
	else
	{
		sprite->texture_width = texture_pointer->region->width;
		sprite->texture_height = texture_pointer->region->height;
		sprite->coordUnitX = (1.0f / ((float)texture_pointer->width / (float)(sprite->width / (sprite->repeat_x + 1) )));
		sprite->coordUnitY = (1.0f / ((float)texture_pointer->height / (float)(sprite->height / (sprite->repeat_y + 1))));
	}

    sprite->texture_pointer = texture_pointer;
    sprite->animated = (animation_def) ? wtrue : wfalse;

    sprite->txtColCount = max(sprite->texture_width / (sprite_width / (sprite->repeat_x + 1)),1);

	sprite->_rotation = 0.0f;
	sprite->fade_amount = 1.0f;
	sprite->current_anim = NULL;


	int32_t num_frame = (sprite->repeat_x + 1) * (sprite->repeat_y + 1);

	sprite->frame = (int32_t*)wf_malloc(sizeof(int32_t) * num_frame);

	for(int32_t i_frame = 0; i_frame < num_frame;i_frame++)
		sprite->frame[i_frame] = start_frame;


	sprite->dofade = wfalse;
	sprite->mirror_texture_x = wfalse;
	sprite->mirror_texture_y = wfalse;
	

	sprite->color.r = sprite->color.g = sprite->color.b = sprite->color.a = 1.0f;

	


    if( sprite->animated )
	{
		sprite->anim_hash.keys = (char**) wf_malloc(animation_def->n_anim_array * sizeof (char*));
		sprite->anim_array = (animation_t*)wf_malloc(animation_def->n_anim_array * sizeof(animation_t));

		 //initialise keys array
		for (uint32_t a_indx = 0; a_indx < animation_def->n_anim_array; a_indx++)
			sprite->anim_hash.keys[a_indx] = 0;

		sprite->anim_hash.max_elem = animation_def->n_anim_array;
		sprite->anim_hash.num_elem = 0;

		//initialise arrays
		for (uint32_t anim_indx = 0; anim_indx < animation_def->n_anim_array; anim_indx++)
		{
			int index = hashtable_pushkey(&sprite->anim_hash,animation_def->anim_array[anim_indx]->name);
			animation_init(&sprite->anim_array[index],animation_def->anim_array[anim_indx],&new_frame_callback,sprite);

			if (strlen(animation_def->anim_array[anim_indx]->name) > sprite->max_anim_name_size) {
				sprite->max_anim_name_size = strlen(animation_def->anim_array[anim_indx]->name);
			}
		}

		sprite->anim_hash.max_elem = animation_def->n_anim_array;
		sprite->anim_hash.num_elem = animation_def->n_anim_array;
	}

	sprite_genvertices_vbo(sprite,render_manager);
	sprite_gentexturesvbo(sprite,render_manager);
}

void sprite_updateanimation(sprite_t* const sprite,const AnimationList* const animation_def)
{
	if( sprite->animated )
	{
		char canim[256] = {0};

		if(sprite->current_anim != NULL)
		{
			w_strcpy(canim,256,sprite->current_anim->anim_def->name);
			sprite->current_anim = NULL;
		}

		wf_free(sprite->anim_array);
		wf_free(sprite->anim_hash.keys);

		sprite->anim_hash.keys = (char**) wf_malloc(animation_def->n_anim_array * sizeof (char*));
		sprite->anim_array = (animation_t*)wf_malloc(animation_def->n_anim_array * sizeof(animation_t));

		 //initialise keys array
		for (uint32_t a_indx = 0; a_indx < animation_def->n_anim_array; a_indx++)
			sprite->anim_hash.keys[a_indx] = 0;

		sprite->anim_hash.max_elem = animation_def->n_anim_array;
		sprite->anim_hash.num_elem = 0;

		//initialise arrays
		for (uint32_t anim_indx = 0; anim_indx < animation_def->n_anim_array; anim_indx++)
		{
			int index = hashtable_pushkey(&sprite->anim_hash,animation_def->anim_array[anim_indx]->name);
			animation_init(&sprite->anim_array[index],animation_def->anim_array[anim_indx],&new_frame_callback,sprite);
		}

		sprite->anim_hash.max_elem = animation_def->n_anim_array;
		sprite->anim_hash.num_elem = animation_def->n_anim_array;

		if(strcmp(canim,"") != 0)
			sprite_playanimation(sprite,canim);

	}
}

void sprite_settexture(sprite_t* const sprite,texture_info* const texture_pointer)
{
	
	if(!texture_pointer->virtual_texture)
	{
		sprite->texture_width = texture_pointer->width;
		sprite->texture_height = texture_pointer->height;
		sprite->coordUnitX = (1.0f / ((float)sprite->texture_width / (float)(sprite->width / (sprite->repeat_x + 1) )));
		sprite->coordUnitY = (1.0f / ((float)sprite->texture_height / (float)(sprite->height / (sprite->repeat_y + 1))));
	}
	else
	{
		sprite->texture_width = texture_pointer->region->width;
		sprite->texture_height = texture_pointer->region->height;
		sprite->coordUnitX = (1.0f / ((float)texture_pointer->width / (float)(sprite->width / (sprite->repeat_x + 1) )));
		sprite->coordUnitY = (1.0f / ((float)texture_pointer->height / (float)(sprite->height / (sprite->repeat_y + 1))));
	}

    sprite->texture_pointer = texture_pointer;

	sprite->txtColCount = max(sprite->texture_width / (sprite->width  / (sprite->repeat_x + 1)),1);

	sprite->tex_coord_needupdate = wtrue;
}

void sprite_setcolor(sprite_t* const sprite,ColorA_t new_color)
{
	sprite->color = new_color;
	sprite->fade_amount = new_color.a;
}

float sprite_getrotation(sprite_t* const sprite)
{
	return sprite->_rotation;
}

void sprite_setrotation(sprite_t* const sprite,float rotation)
{
	sprite->_rotation = rotation;
}

void sprite_setsize(sprite_t* const sprite,int32_t width,int32_t height)
{
	sprite->width = width;
	sprite->height = height;
	sprite->vertices_needupdate = wtrue;
}

void sprite_setstoredoffset(sprite_t* const sprite, Vector_t offset) {
	sprite->storedoffset = offset;
}

Vector_t sprite_getstoredoffset(sprite_t* const sprite) {
	return sprite->storedoffset;
}

void sprite_playanimation(sprite_t* const sprite, const char* anim_name)
{
	sprite_playanimation2(sprite, anim_name, wtrue);
}

void sprite_playanimation2(sprite_t* const sprite,const char* anim_name,wbool reset_flicker)
{
	sprite_playanimation3(sprite, anim_name, reset_flicker,wfalse);
}

void sprite_playanimation3(sprite_t* const sprite, const char* anim_name, wbool reset_flicker, wbool reverse)
{
	if (sprite->anim_hash.num_elem == 0)
		return;

	if (sprite->current_anim != NULL && strcmp(sprite->current_anim->anim_def->name, anim_name) == 0 && !sprite->current_anim->ended)
		return;

	if (reset_flicker) {
		sprite->flicker_time = 0.0f;
		sprite->flicker_step_time = 0;
		sprite->skip_draw = wfalse;
	}

	//get the animation key
	int anim_indx = hashtable_index(&sprite->anim_hash, anim_name);

	if (anim_indx != -1)
	{
		sprite->current_anim = &sprite->anim_array[anim_indx];

		if (!reverse) {
			sprite->current_anim->current_frame = sprite->current_anim->anim_def->frames[0];
			sprite->current_anim->frame_counter = 0;
			sprite->current_anim->reverse = wfalse;
		}
		else {
			sprite->current_anim->current_frame = sprite->current_anim->anim_def->frames[sprite->current_anim->anim_def->n_frames - 1];
			sprite->current_anim->frame_counter = sprite->current_anim->anim_def->n_frames-1;
			sprite->current_anim->reverse = wtrue;
		}
		

		sprite->current_anim->current_step_time = 0.0f;
		sprite->current_anim->ended = wfalse;
		sprite->tex_coord_needupdate = wtrue;

		if (sprite->width != sprite->current_anim->anim_def->width || sprite->height != sprite->current_anim->anim_def->height)
		{
			if (sprite->current_anim->anim_def->has_width)
				sprite->width = sprite->current_anim->anim_def->width * (sprite->repeat_x + 1);

			if (sprite->current_anim->anim_def->has_height)
				sprite->height = sprite->current_anim->anim_def->height * (sprite->repeat_y + 1);

			sprite->vertices_needupdate = wtrue;
		}

	}
}

void sprite_stopanimation(sprite_t* const sprite)
{
	if(sprite->current_anim == NULL)
		return;

	animation_reset(sprite->current_anim);
	animation_set_pause(sprite->current_anim,wtrue);
}

void sprite_draw(sprite_t* const sprite, float x, float y, render_manager_t* const render_mngr, void(*ondraw_func)(render_manager_t* const render_manager))
{
	sprite_draw_withzindex(sprite, x, y, 0, render_mngr, ondraw_func);
}

void sprite_draw_withzindex(sprite_t* const sprite, float x,float y,int z_index,render_manager_t* const render_mngr,void  (*ondraw_func)(render_manager_t* const render_manager))
{
	if(sprite->skip_draw)
		return;

	 if(sprite->tex_coord_needupdate)
		sprite_gentexturesvbo(sprite,render_mngr);

	 if(sprite->vertices_needupdate)
		 sprite_genvertices_vbo(sprite,render_mngr);

	 ColorA_t col_fade = sprite->color;
	 col_fade.a = sprite->fade_amount;

	 Vector3_t pos = {x,y,(float)z_index};

	 //apply packing offset, in case our animations are packed together
	 if(sprite->animated && sprite->current_anim != NULL && (sprite->current_anim->anim_def->sprite_offsetx != 0 || sprite->current_anim->anim_def->sprite_offsety != 0))
	 {
		 pos.x += sprite->current_anim->anim_def->sprite_offsetx;
		 pos.y += sprite->current_anim->anim_def->sprite_offsety;
	 }

	 
	 render_set_color(render_mngr,col_fade);

	 int old_mat_flag = render_mngr->mat_flag;

	 if(sprite->mat_flag != 0)
		 render_mngr->mat_flag = sprite->mat_flag;

	 //apply stored offset, in case of multi sprite, we might need to offset sprite positions
	 if (!Vec_isequal(sprite->storedoffset, vectorzero)) {
		 pos.x += sprite->storedoffset.x;
		 pos.y += sprite->storedoffset.y;
	 }

	 render_set_position3(render_mngr, pos);
	 render_set_scale(render_mngr,sprite->scale_x,sprite->scale_y,1.0f);
	 render_set_rotation(render_mngr,sprite->_rotation);

	 render_start_draw_buffer(render_mngr,sprite->texture_pointer->texture_pointer,sprite->vertices_vbo,sprite->tex_coords_vbo,0,2);

	 if(ondraw_func != NULL)
		 ondraw_func(render_mngr);

	 render_draw_buffer(render_mngr,0,sprite->num_vertices);
	 render_end_draw_buffer(render_mngr,wtrue,wfalse);

	  if(sprite->mat_flag != 0)
		 render_mngr->mat_flag = old_mat_flag;
}



void sprite_setframe(sprite_t* const sprite,int frame,int position)
{
	if(sprite->animated)
	{
		if (sprite->current_anim != NULL)
		{
			sprite->current_anim->frame_counter = frame;
		}
	}

	int32_t num_repeat = (sprite->repeat_x + 1) * (sprite->repeat_y + 1);

	if(frame >= 0)
	{
		if(position == -1)
		{
			for(int32_t i_frame = 0; i_frame < num_repeat;i_frame++)
				sprite->frame[i_frame] = frame;

			sprite->tex_coord_needupdate = wtrue;
		}
		else
		{

			if(position <  num_repeat )
			{
				sprite->frame[position] = frame;
				sprite->tex_coord_needupdate = wtrue;
			}
			else
			{
				logprint("The sprite position is greater than the number of sprite part!");
			}
		}
	}
	else
		logprint("The frame number must be positive or equal to zero");
}

void sprite_setrepeat(sprite_t* const sprite,int32_t repeatx,int32_t repeaty)
{
	int32_t old_repeat = (sprite->repeat_x + 1) * (sprite->repeat_y + 1);

	int32_t new_repeat = (repeatx + 1) * (repeaty + 1);

	if(old_repeat != new_repeat)
	{
		sprite->frame = (int32_t*)wf_realloc(sprite->frame,new_repeat * sizeof(int32_t));
		sprite->vertices_needupdate = wtrue;
	}

	sprite->repeat_x = repeatx;
	sprite->repeat_y = repeaty; 
}


void sprite_flicker(sprite_t* const sprite,float flicker_time)
{
	sprite->flicker_time = flicker_time;
	sprite->flicker_step_time = 0;
}

void sprite_fade(sprite_t* const sprite,float fade_time,int fade_direction)//fade_direction : 1 = fadein (from invisible to visible, -1 = fadeout (from visible to invisible)
{
	sprite->fade_amout_step = fade_direction * (1.0f / fade_time);

	if(fade_direction > 0)
		sprite->fade_amount = 0.0f;
	else
		sprite->fade_amount = 1.0f;

	sprite->dofade = wtrue;
}

void sprite_update(sprite_t* const sprite, const float elapsed)
{
	if(sprite->animated && sprite->current_anim)
		animation_update(sprite->current_anim,elapsed);


	//flickering
	if(sprite->flicker_time > 0)
	{
		sprite->flicker_time -= elapsed;
		sprite->flicker_step_time -= elapsed;

		if(sprite->flicker_step_time <= 0)
		{
			sprite->skip_draw = (sprite->skip_draw == wtrue) ? wfalse : wtrue;
			sprite->flicker_step_time = (sprite->skip_draw)? FLICKER_HIDE_STEP :FLICKER_DRAW_STEP;
		}

	}
	else
	{
		sprite->skip_draw = wfalse;

		//fade in / out
		if(sprite->dofade)
		{
			sprite->fade_amount += (sprite->fade_amout_step * elapsed);

			if(sprite->fade_amount <= 0.0f || sprite->fade_amount >= 1.0f)
				sprite->dofade = wfalse;

			if(sprite->fade_amount < 0.0f)
				sprite->fade_amount = 0.0f;

			if(sprite->fade_amount > 1.0f)
				sprite->fade_amount = 1.0f;
		}
	}

}

void sprite_g_clean(sprite_t* const sprite)
{
	if(!sprite->in_group)
	{
		render_delete_buffer(sprite->vertices_vbo);
		render_delete_buffer(sprite->tex_coords_vbo);
		sprite->has_tex_vbo = sprite->has_vert_vbo = wfalse;
	}
}

void sprite_g_create(sprite_t* const sprite,render_manager_t* const render_mngr)
{
	if(!sprite->in_group)
	{
		sprite_genvertices_vbo(sprite,render_mngr);
		sprite_gentexturesvbo(sprite,render_mngr);
	}
}

void sprite_free(sprite_t* const sprite)
{
	if (sprite->script_id != 0) {
		script_unregisterpointer(sprite->script_id);
		sprite->script_id = 0;
	}

	if(sprite->animated)
	{
		wf_free(sprite->anim_hash.keys);
		sprite->anim_hash.keys = NULL;
		wf_free(sprite->anim_array);
		sprite->anim_array = NULL;
		sprite->current_anim = NULL;
	}

	if(sprite->vertices)
	{
		wf_free(sprite->vertices);
		sprite->vertices = NULL;
	}

	if(sprite->tex_coords)
	{
		wf_free(sprite->tex_coords);
		sprite->tex_coords = NULL;
	}

	sprite_g_clean(sprite);
}
