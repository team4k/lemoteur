#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <stdlib.h>
#include <inttypes.h>
#include <math.h>
#include <time.h>

#if defined(_WIN32)
    #include <Windows.h>
#else
    #include <unistd.h>
#endif

#include <Engine.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/geom.h>

#include <Debug/Logprint.h>

#include <GameObjects/Characters.pb-c.h>
#include <Graphics/Animation.h>

void animation_init(animation_t* const p_anim,AnimationDef* const anim_def,new_frame_callback_t new_frame_callback,void* new_frame_data)
{
	p_anim->anim_def = anim_def;
	p_anim->current_frame = p_anim->anim_def->frames[0];
    p_anim->frame_counter = 0;
    p_anim->current_step_time = 0.0f;
    p_anim->ended = wfalse;
	p_anim->new_frame = new_frame_callback;
	p_anim->new_frame_data = new_frame_data;
	p_anim->reverse = wfalse;
}

wbool animation_hascontactpoints(animation_t* const p_anim)
{
	return (wbool)(p_anim->anim_def->n_attachpoint_id > 0);
}

int32_t animation_getcontactpointindx(animation_t* const p_anim,const char* contact_pointname)
{
	for(uint32_t icont = 0; icont < p_anim->anim_def->n_attachpoint_id;icont++)
	{
		if(strcmp(p_anim->anim_def->attachpoint_id[icont],contact_pointname) == 0)
			return icont;
	}

	return -1;
}

void animation_setnewframecallback(animation_t* const p_anim,new_frame_callback_t new_frame_callback,void* new_frame_data)
{
	p_anim->new_frame = new_frame_callback;
	p_anim->new_frame_data = new_frame_data;
}

wbool animation_iscontactactive_atframe(animation_t* const p_anim,int32_t contact_indx)
{
	return (wbool)(!p_anim->anim_def->attachpoint_values[p_anim->frame_counter]->attachpointpos[contact_indx]->isnull);
}

Vector_t animation_getcontactpos_atframe(animation_t* const p_anim,int32_t contact_indx)
{
	Vector_t vec = {0.0f,0.0f};
	Avector* tmp = p_anim->anim_def->attachpoint_values[p_anim->frame_counter]->attachpointpos[contact_indx];

	if(!tmp->isnull)
	{
		vec.x = (float)tmp->x;
		vec.y = (float)tmp->y;
	}

	return vec;
}

void animation_update(animation_t* const p_anim,const float elapsed)
{
    if(p_anim->ended)
        return;

    p_anim->current_step_time += elapsed;

	if(p_anim->current_step_time >= p_anim->anim_def->timesteps[p_anim->frame_counter])
    {
		if (!p_anim->reverse) {
			if (++p_anim->frame_counter >= p_anim->anim_def->n_frames)
			{
				if (p_anim->anim_def->loop)
					p_anim->frame_counter = 0;
				else
				{
					p_anim->ended = wtrue;
					p_anim->frame_counter = p_anim->anim_def->n_frames - 1;
				}
			}
		}
		else {
			if (--p_anim->frame_counter <= 0)
			{
				if (p_anim->anim_def->loop)
					p_anim->frame_counter = p_anim->anim_def->n_frames-1;
				else
				{
					p_anim->ended = wtrue;
				}
			}
		}
		

		if(!p_anim->ended)
		{
			p_anim->current_step_time = 0.0f;
			p_anim->current_frame = p_anim->anim_def->frames[p_anim->frame_counter];

			if(p_anim->new_frame)
				p_anim->new_frame(p_anim->new_frame_data);
		}
		else
		{
			p_anim->current_step_time = 0.0f;
			p_anim->current_frame = p_anim->anim_def->frames[p_anim->anim_def->n_frames - 1];
		}
    }
    
}

void animation_reset(animation_t* const p_anim)
{
    p_anim->frame_counter = 0;
    p_anim->current_step_time = 0.0f;
    p_anim->current_frame = p_anim->anim_def->frames[p_anim->frame_counter];
    p_anim->ended = wfalse;
}

void animation_set_pause(animation_t* const p_anim,const wbool pause)
{
    p_anim->ended = pause;
}


