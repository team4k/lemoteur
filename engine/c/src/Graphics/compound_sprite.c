#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <stdlib.h>
#include <inttypes.h>
#include <math.h> 
#include <time.h>

#if defined(_WIN32)
    #include <Windows.h>
#endif


#include <Engine.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>
#include <Base/geom.h>
#include <Debug/Logprint.h>
#include <Utils/Hash.h>

#include <Render.h>
#include <Render/render_manager.h>

#include <png.h>
#include <Graphics/TextureLoader.h>


#include <Graphics/compound_sprite.h>
#include <Scripting/ScriptPointers.h>


void compound_sprite_init(compound_sprite_t* const sprite,const int32_t sprite_width,const int32_t sprite_height,texture_info* const texture_inf,const int32_t tile_size,render_manager_t* const render_mngr)
{
	sprite->width = sprite_width;
	sprite->height = sprite_height;
	sprite->tile_size = tile_size;
	sprite->z_index = 1;
	sprite->script_id = 0;
	sprite->has_hwd_buffer_indices = sprite->has_hwd_buffer_texcoords = sprite->has_hwd_buffer_vertices = wfalse;

	if(texture_inf != NULL)
	{
		sprite->texture_width = texture_inf->width;
		sprite->texture_height = texture_inf->height;
		sprite->texture_pointer = texture_inf;
		sprite->txtColCount = texture_inf->width / sprite->tile_size;

		if(sprite->tile_size != -1)//we have a tileset as the base texture, compute texure coordinates from the size of a tile
		{
			sprite->coordUnitX = (1.0f / ((float)sprite->texture_width / (float)sprite->tile_size));
			sprite->coordUnitY = (1.0f / ((float)sprite->texture_height / (float)sprite->tile_size));
		}
		else //we apply the whole texture on our sprite
		{
			sprite->coordUnitX = (1.0f / ((float)sprite->texture_width / (float)sprite->width));
			sprite->coordUnitY = (1.0f / ((float)sprite->texture_height / (float)sprite->height));
		}
	}


	sprite->shape_size[0] = 0;
	sprite->shape_indices_size[0] = 0;
	sprite->tile_index = 0;
	sprite->shape_indx = 0;
	sprite->indices_indx = 0;
	sprite->shape_minx =  -(sprite_width * 0.5f);
	sprite->shape_miny = -(sprite_height * 0.5f);
	sprite->alpha_value = 1.0f;


     sprite->buffer_needupdate = wfalse;

	//create an hardware buffer for our shatter effects

	sprite->hardware_buffer_vertices = render_gen_buffer(render_mngr,sprite->buffer_vertices,(COMPOUND_MAX_SHAPE * COMPOUND_MAX_VERTICES) * sizeof(float),wtrue,wtrue);
	sprite->hardware_buffer_texcoords = render_gen_buffer(render_mngr,sprite->draw_texcoord,(COMPOUND_MAX_SHAPE * COMPOUND_MAX_VERTICES) * sizeof(float),wtrue,wtrue);
	sprite->hardware_buffer_indices = render_gen_bufferindices(render_mngr,sprite->draw_indices,(COMPOUND_MAX_SHAPE * COMPOUND_MAX_INDICES) * sizeof(uint16_t),wtrue,wtrue);
	sprite->has_hwd_buffer_indices = sprite->has_hwd_buffer_texcoords = sprite->has_hwd_buffer_vertices = wtrue;

}

void compound_sprite_update_size(compound_sprite_t* const sprite,const int32_t sprite_width,const int32_t sprite_height,const int32_t tilesize)
{
	sprite->width = sprite_width;
	sprite->height = sprite_height;
	sprite->shape_minx =  -(sprite_width * 0.5f);
	sprite->shape_miny = -(sprite_height * 0.5f);
	sprite->tile_size = tilesize;
}

void compound_sprite_updatetexture(compound_sprite_t* const sprite,texture_info* const new_tex)
{
	sprite->texture_width = new_tex->width;
	sprite->texture_height = new_tex->height;
	sprite->texture_pointer = new_tex;
	sprite->txtColCount = new_tex->width / sprite->tile_size;

	if(sprite->tile_size != -1)//we have a tileset as the base texture, compute texure coordinates from the size of a tile
	{
		sprite->coordUnitX = (1.0f / ((float)sprite->texture_width / (float)sprite->tile_size));
		sprite->coordUnitY = (1.0f / ((float)sprite->texture_height / (float)sprite->tile_size));
	}
	else //we apply the whole texture on our sprite
	{
		sprite->coordUnitX = (1.0f / ((float)sprite->texture_width / (float)sprite->width));
		sprite->coordUnitY = (1.0f / ((float)sprite->texture_height / (float)sprite->height));
	}
}

//vertices = array of array of float (size = 2), size = number of vertices
csprite_pointer_t* compound_sprite_addshape(compound_sprite_t* const sprite,float** vertices,const int16_t size,Vector_t position,float rotation,wbool invert_gravity)
{
	for(int32_t isprite = 0; isprite < COMPOUND_MAX_SHAPE; isprite++)
	{
		if(sprite->shape_size[isprite] == 0) //find the first empty shape cell
		{
			sprite->shape_size[isprite] = size;
			sprite->shape_position[isprite].x = position.x;
			sprite->shape_position[isprite].y = position.y;

			sprite->shape_rotation[isprite] = rotation;

			sprite->shape_pointer[isprite].shape_indx = sprite->shape_indx;
			sprite->shape_pointer[isprite].shape_id = isprite;


			for(int16_t isize = 0; isize < size;isize++) //copy the vertices from the array in parameter
			{
				sprite->draw_vertices[sprite->shape_indx] = vertices[isize][0];
				sprite->draw_vertices[sprite->shape_indx+1] = vertices[isize][1];

                sprite->buffer_vertices[sprite->shape_indx] = vertices[isize][0] + position.x;
				sprite->buffer_vertices[sprite->shape_indx+1] = vertices[isize][1] + position.y;
				sprite->shape_indx += 2;
			}

			uint16_t indices_num = 0;
			uint16_t initial_index = sprite->indices_val;
			uint16_t iindices = sprite->indices_val;


			int16_t num_tri = size - 2;

			for(int16_t itri = 0;itri < num_tri;itri++)
			{
				sprite->draw_indices[sprite->indices_indx++] = iindices + 2;
				sprite->draw_indices[sprite->indices_indx++] = iindices + 1;
				sprite->draw_indices[sprite->indices_indx++] = initial_index;
				
				iindices +=1;

				indices_num += 3;
			}


			sprite->indices_val = iindices + 2;

			sprite->shape_indices_size[isprite] = indices_num;


			sprite->shape_size[isprite + 1] = 0;
			sprite->shape_indices_size[isprite + 1] = 0;

			sprite->shape_pointer[isprite].sprite = sprite;
			sprite->shape_pointer[isprite].gravity_inverted = invert_gravity;

			return &sprite->shape_pointer[isprite];
		}
	}

	return NULL;
}

void compound_sprite_compute_textcoord(compound_sprite_t* const sprite,float basex,float basey,int32_t tile_index,wbool gen_hardware_buffer,wbool mirror_x,wbool mirror_y,render_manager_t* const render_mngr)
{

	int32_t txtRow = 0;

	if(sprite->txtColCount != 0)
		txtRow = (int32_t)floorf((float)(tile_index / sprite->txtColCount));//ligne Y

	int32_t txtCol = tile_index - (sprite->txtColCount * txtRow);//ligne X
	sprite->tile_index = tile_index;
	float stretch_factor_x = 1.0f;
	float stretch_factor_y = 1.0f;

	if(sprite->tile_size != -1)
	{
		stretch_factor_x = (float)(sprite->width / sprite->tile_size);
		stretch_factor_y = (float)(sprite->height / sprite->tile_size);
	}

	int32_t isprite = 0;
	int32_t size_count = 0;

	float current_coordunitX = sprite->coordUnitX * txtCol;
	float current_coordunitY = sprite->coordUnitY * txtRow;

	float max_coordunitX = ((current_coordunitX + sprite->coordUnitX) * stretch_factor_x);
	float max_coordunitY = ((current_coordunitY + sprite->coordUnitY) * stretch_factor_y);


	if(!mirror_x)
		basex -= sprite->width * 0.5f;
	else
		basex += sprite->width * 0.5f;

	if(!mirror_y)
		basey += sprite->height * 0.5f;
	else
		basey -= sprite->height * 0.5f;

	for(int32_t ivert = 0; ivert < sprite->shape_indx; ivert+=2)
	{
		float x = sprite->shape_position[isprite].x;
		float y = sprite->shape_position[isprite].y;
	
		float posX_in_sprite = 0;
		float posY_in_sprite = 0;
		
		if(!mirror_x)
			posX_in_sprite = ((sprite->draw_vertices[ivert] + x) - basex);
		else
			posX_in_sprite = (basex - (sprite->draw_vertices[ivert] + x));

		if(!mirror_y)
			posY_in_sprite =  (basey - (sprite->draw_vertices[ivert+1] + y));
		else
			posY_in_sprite =  ((sprite->draw_vertices[ivert+1] + y) - basey);

		sprite->draw_texcoord[ivert] =  ( current_coordunitX +(((posX_in_sprite / sprite->width) * (sprite->coordUnitX * stretch_factor_x) ) ));
		
		sprite->draw_texcoord[ivert+1] = ( current_coordunitY + ( ( posY_in_sprite / sprite->height)  * (sprite->coordUnitY * stretch_factor_y) ) ) ;//invert y component since opengl load texture in an inverted way

		if(sprite->draw_texcoord[ivert] < 0)
			sprite->draw_texcoord[ivert] = 0;

		if(sprite->draw_texcoord[ivert+1] < 0)
			sprite->draw_texcoord[ivert+1] = 0;

		if(sprite->draw_texcoord[ivert] > max_coordunitX)
		{
			sprite->draw_texcoord[ivert] = max_coordunitX;
		}

		if(sprite->draw_texcoord[ivert+1] > max_coordunitY)
		{
			sprite->draw_texcoord[ivert+1] = max_coordunitY;
		}

		size_count++;

		if(size_count >= sprite->shape_size[isprite])
		{
			isprite++;
			size_count = 0;
		}

         /* sprite->buffer_vertices[ivert] =	posX_in_sprite + x;
          sprite->buffer_vertices[ivert+1] = posY_in_sprite + y;*/
	}

	if(gen_hardware_buffer)
	{
		//upload to the graphic card our vertex and textcoord data
		render_discard_updatebuffer(render_mngr,sprite->hardware_buffer_vertices,sprite->buffer_vertices,sprite->shape_indx * sizeof(float),wtrue,wtrue);//vertices
		render_discard_updatebuffer(render_mngr,sprite->hardware_buffer_texcoords,sprite->draw_texcoord,sprite->shape_indx * sizeof(float),wtrue,wtrue);//textcoord
		render_discard_update_indicesbuffer(render_mngr,sprite->hardware_buffer_indices,sprite->draw_indices,sprite->indices_indx * sizeof(uint16_t),wtrue,wtrue);//indices
	}
}

void compound_sprite_updateshape(compound_sprite_t* const sprite,Vector_t position,float rotation, int16_t isprite)
{
	sprite->shape_position[isprite] = position;
	sprite->shape_rotation[isprite] = rad2deg(rotation);
   
     //update shape vertices and buffer

	int32_t val_i = sprite->shape_pointer[isprite].shape_indx;

     for(int32_t ivert = 0; ivert < sprite->shape_size[isprite]; ivert++)
	{
		 //rotate
		 int nx =  (int)(sprite->draw_vertices[val_i] * cosf(rotation) -  sprite->draw_vertices[val_i+1] * sinf(rotation));
		 int ny = (int)(sprite->draw_vertices[val_i] * sinf(rotation) +  sprite->draw_vertices[val_i+1] * cosf(rotation));

          sprite->buffer_vertices[val_i] =  nx + position.x;
          sprite->buffer_vertices[val_i+1] =  ny + position.y;

		  val_i += 2;
     }
     
      sprite->buffer_needupdate = wtrue;

     //sprite->buffer_updateoffset = (isprite * 2);
     //sprite->buffer_updatesize = sprite->shape_size[isprite] * sizeof(float);

    // render_update_buffer(render_mngr,sprite->hardware_buffer_vertices,sprite->draw_vertices,(isprite * 2),sprite->shape_size[isprite] * sizeof(float));//vertices

}

void compound_sprite_clearshape(compound_sprite_t* const sprite)
{
	sprite->shape_size[0] = 0;
	sprite->shape_indices_size[0] = 0;
	sprite->shape_indx = 0;
	sprite->indices_indx = 0;
	sprite->indices_val = 0;

}

void compound_sprite_draw(compound_sprite_t* const sprite,render_manager_t* const render_mngr)
{

     if(sprite->buffer_needupdate)
     {
          render_discard_updatebuffer(render_mngr,sprite->hardware_buffer_vertices,sprite->buffer_vertices, sprite->shape_indx * sizeof(float),wtrue,wtrue);//vertices
         // sprite->buffer_updateoffset = 0;
          //sprite->buffer_updatesize = 0;
          sprite->buffer_needupdate = wfalse;
     }

	render_push_matrix(render_mngr);

	ColorA_t color = {1.0f,1.0f,1.0f,sprite->alpha_value};

	render_set_color(render_mngr,color);


	render_bind_texture(render_mngr,sprite->texture_pointer->texture_pointer,wtrue,wfalse);


	render_draw_compound_textured_buffer(render_mngr,sprite->hardware_buffer_vertices,sprite->hardware_buffer_texcoords,sprite->hardware_buffer_indices,&sprite->shape_position[0],&sprite->shape_rotation[0],COMPOUND_MAX_SHAPE,&sprite->shape_size[0],&sprite->shape_indices_size[0]);



	//render_unbind_texture(render_mngr);
	color.a = 1.0f;

	render_pop_matrix(render_mngr);

	render_set_color(render_mngr,color);
	
}

void compound_sprite_create_VBO(compound_sprite_t* const sprite,render_manager_t* const render_mngr)
{
	sprite->hardware_buffer_vertices = render_gen_buffer(render_mngr,sprite->buffer_vertices,(COMPOUND_MAX_SHAPE * COMPOUND_MAX_VERTICES) * sizeof(float),wtrue,wtrue);
	sprite->hardware_buffer_texcoords = render_gen_buffer(render_mngr,sprite->draw_texcoord,(COMPOUND_MAX_SHAPE * COMPOUND_MAX_VERTICES) * sizeof(float),wtrue,wtrue);
	sprite->hardware_buffer_indices = render_gen_bufferindices(render_mngr,sprite->draw_indices,(COMPOUND_MAX_SHAPE * COMPOUND_MAX_INDICES) * sizeof(int16_t),wtrue,wtrue);
	sprite->has_hwd_buffer_indices = sprite->has_hwd_buffer_texcoords = sprite->has_hwd_buffer_vertices = wtrue;
}

void compound_sprite_free(compound_sprite_t* sprite)
{
	if (sprite->script_id != 0)
	{
		script_unregisterpointer(sprite->script_id);
		sprite->script_id = 0;
	}
	
	if(sprite->has_hwd_buffer_vertices)
		render_delete_buffer(sprite->hardware_buffer_vertices);

	if(sprite->has_hwd_buffer_texcoords)
		render_delete_buffer(sprite->hardware_buffer_texcoords);

	if(sprite->has_hwd_buffer_indices)
		render_delete_buffer(sprite->hardware_buffer_indices);

	sprite->has_hwd_buffer_indices = sprite->has_hwd_buffer_texcoords = sprite->has_hwd_buffer_vertices = wfalse;
}
