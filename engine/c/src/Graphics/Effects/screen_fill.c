#include <inttypes.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

#include <Render.h>

#include <Engine.h>
#include <Base/types.h>
#include <Base/functions.h>
#include <Base/geom.h>
#include <Base/Color.h>
#include <Utils/Hash.h>

#include <Debug/Logprint.h>


#include <Render/render_manager.h>
#include <Graphics/TextureLoader.h>

#include <GameObjects/Characters.pb-c.h>
#include <Graphics/Animation.h>

#include <Graphics/Effects/screen_fill.h>


void screen_fill_init(screen_fill_t* const screen_obj,float move_per_tick,float move_timeout,Vector_t direction_fill,texture_info* const fill_texture,int16_t piece_width,int16_t piece_height,render_manager_t* const render_mngr,const AnimationList* const animation_def,const char* fill_anim,const char* edge_anim)
{
	screen_obj->move_per_tick = move_per_tick;
	screen_obj->has_sibling = wfalse;
	screen_obj->move_timeout = move_timeout;
	screen_obj->direction_fill = direction_fill;
	screen_obj->fill_texture = fill_texture;
	screen_obj->piece_width = piece_width;
	screen_obj->piece_height = piece_height;
	screen_obj->active = wfalse;
	screen_obj->z_index = MAX_ZINDEX - 1;//highest z-index by default
	screen_obj->edge_position = EDGE_ON_BOTTOM;

	screen_obj->anim_hash.keys = (char**) wf_malloc(animation_def->n_anim_array * sizeof (char*));
	screen_obj->anim_array = (animation_t*)wf_malloc(animation_def->n_anim_array * sizeof(animation_t));

	 //initialise keys array
    for (uint32_t a_indx = 0; a_indx < animation_def->n_anim_array; a_indx++)
        screen_obj->anim_hash.keys[a_indx] = 0;

	screen_obj->anim_hash.max_elem = animation_def->n_anim_array;
	screen_obj->anim_hash.num_elem = 0;
	//initialise arrays
	for (uint32_t b_indx = 0; b_indx < animation_def->n_anim_array; b_indx++)
	{
		int index = hashtable_pushkey(&screen_obj->anim_hash,animation_def->anim_array[b_indx]->name);

		animation_init(&screen_obj->anim_array[index],animation_def->anim_array[b_indx],NULL,NULL);
	}
	

	int32_t anim_indx = -1;

	//get the animation key
	if (fill_anim != NULL) {

		anim_indx = hashtable_index(&screen_obj->anim_hash, fill_anim);

		if (anim_indx != -1)
		{
			screen_obj->fill_anim = &screen_obj->anim_array[anim_indx];
			screen_obj->fill_anim->current_frame = screen_obj->fill_anim->anim_def->frames[0];
			screen_obj->fill_anim->frame_counter = 0;
			screen_obj->fill_anim->current_step_time = 0.0f;
			screen_obj->fill_anim->ended = wfalse;
		}
		else {
			logprint("Can't find filler animation named %s !", fill_anim);
		}

	}
	else {
		//first anim by default
		screen_obj->fill_anim = &screen_obj->anim_array[0];
		screen_obj->fill_anim->current_frame = screen_obj->fill_anim->anim_def->frames[0];
		screen_obj->fill_anim->frame_counter = 0;
		screen_obj->fill_anim->current_step_time = 0.0f;
		screen_obj->fill_anim->ended = wfalse;
	}
	
	if (edge_anim != NULL) {
		anim_indx = hashtable_index(&screen_obj->anim_hash, edge_anim);

		if (anim_indx != -1)
		{
			screen_obj->edge_anim = &screen_obj->anim_array[anim_indx];
			screen_obj->edge_anim->current_frame = screen_obj->edge_anim->anim_def->frames[0];
			screen_obj->edge_anim->frame_counter = 0;
			screen_obj->edge_anim->current_step_time = 0.0f;
			screen_obj->edge_anim->ended = wfalse;
		}
		else {
			logprint("Can't find filler edge animation named %s !", edge_anim);
		}
	}
	else {
		//first anim by default
		screen_obj->edge_anim = &screen_obj->anim_array[0];
		screen_obj->edge_anim->current_frame = screen_obj->edge_anim->anim_def->frames[0];
		screen_obj->edge_anim->frame_counter = 0;
		screen_obj->edge_anim->current_step_time = 0.0f;
		screen_obj->edge_anim->ended = wfalse;
	}


	screen_obj->coordUnitX = (1.0f / (fill_texture->width / screen_obj->piece_width));
	screen_obj->coordUnitY = (1.0f / (fill_texture->height / screen_obj->piece_height));
	screen_obj->tile_per_row = fill_texture->width / screen_obj->piece_width;  


	//create a buffer that match the screen size

	screen_obj->piece_num_width = (int32_t)ceilf((float)render_mngr->screen_info.width /  (float)screen_obj->piece_width);
	screen_obj->piece_num_height = (int32_t)ceilf((float)render_mngr->screen_info.height / (float)screen_obj->piece_height);

	screen_obj->texture_coords = (float*)wf_malloc(sizeof(float) * (screen_obj->piece_num_width * screen_obj->piece_num_height)  * 12);
	screen_obj->vertices =(float*)wf_malloc(sizeof(float) * (screen_obj->piece_num_width * screen_obj->piece_num_height) * 12);

	int32_t idtx = 0;
	int32_t txtCol = 0;
	int32_t txtRow = 0;

	screen_obj->current_fill_frame =  screen_obj->fill_anim->current_frame;
	screen_obj->current_edge_frame = screen_obj->edge_anim->current_frame;

	for(int32_t i = 0; i < screen_obj->piece_num_height;i++)//for each row
	{

		if( (screen_obj->edge_position == EDGE_ON_BOTTOM &&  i == 0) || (screen_obj->edge_position == EDGE_ON_TOP && i == (screen_obj->piece_num_height - 1)) )
		{
			txtRow = (int32_t)floorf((float)(screen_obj->edge_anim->current_frame / screen_obj->tile_per_row));
			txtCol = screen_obj->edge_anim->current_frame - (screen_obj->tile_per_row * txtRow);
			
		}
		else
		{
			txtRow = (int32_t)floorf((float)(screen_obj->fill_anim->current_frame / screen_obj->tile_per_row));
			txtCol = screen_obj->fill_anim->current_frame - (screen_obj->tile_per_row * txtRow);
		}

		for(int32_t j = 0; j < screen_obj->piece_num_width;j++, idtx += 12) //for each column
		{
			//vertices
			screen_obj->vertices[idtx] = (float)(j * screen_obj->piece_width);
			screen_obj->vertices[idtx+1] = (float)(i * screen_obj->piece_height);
		
			screen_obj->vertices[idtx+2] = (float)screen_obj->piece_width + (j * screen_obj->piece_width);
			screen_obj->vertices[idtx+3] = (float)(i * screen_obj->piece_height);
		
			screen_obj->vertices[idtx+4] = (float)screen_obj->piece_width + (j * screen_obj->piece_width);
			screen_obj->vertices[idtx+5] = (float)screen_obj->piece_height + (i * screen_obj->piece_height);
		
			screen_obj->vertices[idtx+6] = (float)screen_obj->piece_width + (j * screen_obj->piece_width);
			screen_obj->vertices[idtx+7] = (float)screen_obj->piece_height + (i * screen_obj->piece_height);
		
			screen_obj->vertices[idtx+8] = (float)(j * screen_obj->piece_width);
			screen_obj->vertices[idtx+9] = (float) screen_obj->piece_height + (i *  screen_obj->piece_height);
		
			screen_obj->vertices[idtx+10] =  (float)(j * screen_obj->piece_width);
			screen_obj->vertices[idtx+11] =  (float)(i *  screen_obj->piece_height);

			//precompute texture coordinates
			float x1 = (screen_obj->coordUnitX * txtCol);
			float y2 = (screen_obj->coordUnitY* txtRow);

			float y1 = (screen_obj->coordUnitY + y2);
			float x2 = (screen_obj->coordUnitX + x1);
	

			if(fill_texture->virtual_texture)
			{
				wfloat basepointx = fill_texture->region->position.x;
				wfloat basepointy = fill_texture->region->position.y;
				float one_TILE_SIZE = (float)screen_obj->piece_width;
				float one_tile_height = (float)screen_obj->piece_height;

				x1 = (basepointx  + (one_TILE_SIZE * txtCol)) / fill_texture->width;
				y2 = (basepointy + (one_tile_height * txtRow)) / fill_texture->height;

				x2 = ((basepointx  + (one_TILE_SIZE * txtCol)) + one_TILE_SIZE) / fill_texture->width;
				y1 = ((basepointy + (one_tile_height * txtRow)) + one_tile_height) / fill_texture->height;
			}



			//texture coords
			screen_obj->texture_coords[idtx]   = x1;
			screen_obj->texture_coords[idtx+1] = y1;
		
			screen_obj->texture_coords[idtx+2] = x2;
			screen_obj->texture_coords[idtx+3] = y1;
		
			screen_obj->texture_coords[idtx+4] = x2;
			screen_obj->texture_coords[idtx+5] = y2;
		
			screen_obj->texture_coords[idtx+6] = x2;
			screen_obj->texture_coords[idtx+7] = y2;
		
			screen_obj->texture_coords[idtx+8] = x1;
			screen_obj->texture_coords[idtx+9] = y2;
		
			screen_obj->texture_coords[idtx+10] = x1;
			screen_obj->texture_coords[idtx+11] = y1;

		}
	}

	screen_obj->VBOvertices = render_gen_buffer(render_mngr,screen_obj->vertices,((screen_obj->piece_num_width * screen_obj->piece_num_height)  * 12) * sizeof(float),wfalse,wfalse);

	screen_obj->VBOtexcoord = render_gen_buffer(render_mngr,screen_obj->texture_coords,((screen_obj->piece_num_width * screen_obj->piece_num_height)  * 12) * sizeof(float),wtrue,wtrue);
}

void screen_fill_setanimations(screen_fill_t* const screen_obj,render_manager_t* const render_mngr,const char* fill_anim,const char* edge_anim,int32_t edge_pos)
{
	//get the animation key
	int32_t anim_indx = hashtable_index(&screen_obj->anim_hash,fill_anim);

	if(anim_indx != -1)
	{
		screen_obj->fill_anim = &screen_obj->anim_array[anim_indx];
		screen_obj->fill_anim->current_frame = screen_obj->fill_anim->anim_def->frames[0];
		screen_obj->fill_anim->frame_counter = 0;
		screen_obj->fill_anim->current_step_time = 0.0f;
		screen_obj->fill_anim->ended = wfalse;
	}

	anim_indx = hashtable_index(&screen_obj->anim_hash,edge_anim);

	if(anim_indx != -1)
	{
		screen_obj->edge_anim = &screen_obj->anim_array[anim_indx];
		screen_obj->edge_anim->current_frame = screen_obj->edge_anim->anim_def->frames[0];
		screen_obj->edge_anim->frame_counter = 0;
		screen_obj->edge_anim->current_step_time = 0.0f;
		screen_obj->edge_anim->ended = wfalse;
	}

	screen_obj->current_fill_frame =  screen_obj->fill_anim->current_frame;
	screen_obj->current_edge_frame = screen_obj->edge_anim->current_frame;
	screen_obj->edge_position = edge_pos;

	screen_fill_updatetxtcoord(screen_obj,render_mngr);
}

void screen_fill_setgraphics(screen_fill_t* const screen_obj,render_manager_t* const render_mngr,texture_info* const new_texture,const AnimationList* const new_animations,const char* fill_anim,const char* edge_anim)
{
	//clear old data first
	wf_free(screen_obj->anim_hash.keys);
	screen_obj->anim_hash.keys = NULL;
	wf_free(screen_obj->anim_array);
	screen_obj->anim_array = NULL;

	screen_obj->anim_hash.keys = (char**) wf_malloc(new_animations->n_anim_array * sizeof (char*));
	screen_obj->anim_array = (animation_t*)wf_malloc(new_animations->n_anim_array * sizeof(animation_t));

	 //initialise keys array
    for (uint32_t a_indx = 0; a_indx < new_animations->n_anim_array; a_indx++)
        screen_obj->anim_hash.keys[a_indx] = 0;

	screen_obj->anim_hash.max_elem = new_animations->n_anim_array;
	screen_obj->anim_hash.num_elem = 0;
	//initialise arrays
	for (uint32_t b_indx = 0; b_indx < new_animations->n_anim_array; b_indx++)
	{
		int index = hashtable_pushkey(&screen_obj->anim_hash,new_animations->anim_array[b_indx]->name);

		animation_init(&screen_obj->anim_array[index],new_animations->anim_array[b_indx],NULL,NULL);
	}
	

	//get the animation key
	int32_t anim_indx = hashtable_index(&screen_obj->anim_hash,fill_anim);

	if(anim_indx != -1)
	{
		screen_obj->fill_anim = &screen_obj->anim_array[anim_indx];
		screen_obj->fill_anim->current_frame = screen_obj->fill_anim->anim_def->frames[0];
		screen_obj->fill_anim->frame_counter = 0;
		screen_obj->fill_anim->current_step_time = 0.0f;
		screen_obj->fill_anim->ended = wfalse;
	}

	anim_indx = hashtable_index(&screen_obj->anim_hash,edge_anim);

	if(anim_indx != -1)
	{
		screen_obj->edge_anim = &screen_obj->anim_array[anim_indx];
		screen_obj->edge_anim->current_frame = screen_obj->edge_anim->anim_def->frames[0];
		screen_obj->edge_anim->frame_counter = 0;
		screen_obj->edge_anim->current_step_time = 0.0f;
		screen_obj->edge_anim->ended = wfalse;
	}

	screen_obj->current_fill_frame =  screen_obj->fill_anim->current_frame;
	screen_obj->current_edge_frame = screen_obj->edge_anim->current_frame;

	screen_fill_updatetxtcoord(screen_obj,render_mngr);

}

void screen_fill_updatetxtcoord(screen_fill_t* const screen_obj,render_manager_t* const render_mngr)
{
	int32_t txtCol = 0;
	int32_t txtRow = 0;
	int32_t idtx = 0;

	for(int32_t i = 0; i < screen_obj->piece_num_height;i++)//for each row
	{

		if( (screen_obj->edge_position == EDGE_ON_BOTTOM &&  i == 0) || (screen_obj->edge_position == EDGE_ON_TOP && i == (screen_obj->piece_num_height - 1)) )
		{
			txtRow = (int32_t)floorf((float)(screen_obj->edge_anim->current_frame / screen_obj->tile_per_row));
			txtCol = screen_obj->edge_anim->current_frame - (screen_obj->tile_per_row * txtRow);
		}
		else
		{
			txtRow = (int32_t)floorf((float)(screen_obj->fill_anim->current_frame / screen_obj->tile_per_row));
			txtCol = screen_obj->fill_anim->current_frame - (screen_obj->tile_per_row * txtRow);
		}

		//precompute texture coordinates
		float x1 = (screen_obj->coordUnitX * txtCol);
		float y2 = (screen_obj->coordUnitY* txtRow);

		float y1 = (screen_obj->coordUnitY + y2);
		float x2 = (screen_obj->coordUnitX + x1);
	

		if(screen_obj->fill_texture->virtual_texture)
		{
			wfloat basepointx = screen_obj->fill_texture->region->position.x;
			wfloat basepointy = screen_obj->fill_texture->region->position.y;
			float one_TILE_SIZE = (float)screen_obj->piece_width;
			float one_tile_height = (float)screen_obj->piece_height;

			x1 = (basepointx  + (one_TILE_SIZE * txtCol)) / screen_obj->fill_texture->width;
			y2 = (basepointy + (one_tile_height * txtRow)) / screen_obj->fill_texture->height;

			x2 = ((basepointx  + (one_TILE_SIZE * txtCol)) + one_TILE_SIZE) / screen_obj->fill_texture->width;
			y1 = ((basepointy + (one_tile_height * txtRow)) + one_tile_height) / screen_obj->fill_texture->height;
		}

		for(int32_t j = 0; j < screen_obj->piece_num_width;j++,idtx+=12) //for each column
		{
			//texture coords
			screen_obj->texture_coords[idtx]   = x1;
			screen_obj->texture_coords[idtx+1] = y1;
		
			screen_obj->texture_coords[idtx+2] = x2;
			screen_obj->texture_coords[idtx+3] = y1;
		
			screen_obj->texture_coords[idtx+4] = x2;
			screen_obj->texture_coords[idtx+5] = y2;
		
			screen_obj->texture_coords[idtx+6] = x2;
			screen_obj->texture_coords[idtx+7] = y2;
		
			screen_obj->texture_coords[idtx+8] = x1;
			screen_obj->texture_coords[idtx+9] = y2;
		
			screen_obj->texture_coords[idtx+10] = x1;
			screen_obj->texture_coords[idtx+11] = y1;

		}
	}

	render_discard_updatebuffer(render_mngr,screen_obj->VBOtexcoord, &screen_obj->texture_coords[0],((screen_obj->piece_num_width * screen_obj->piece_num_height)  * 12) * sizeof(float),wtrue,wtrue);
}

void screen_fill_draw(screen_fill_t* const screen_obj,render_manager_t* const render_mngr)
{
	Vector_t cpos = screen_obj->base_position;

	render_push_matrix(render_mngr);

	render_reset_color(render_mngr);

	cpos.x = -render_mngr->scroll_vector.x;

	render_set_position(render_mngr, cpos);

	wfloat diff = cpos.y - render_mngr->screen_info.position.y;

	render_start_draw_buffer(render_mngr,screen_obj->fill_texture->texture_pointer,screen_obj->VBOvertices,screen_obj->VBOtexcoord,0,2);

	int32_t start_index = 0;
	int32_t length = (screen_obj->piece_num_width * screen_obj->piece_num_height) * 6;

	if(diff > 0)
	{
		int32_t row_to_hide = (int32_t)floorf(diff / (float)screen_obj->piece_height);
		start_index = (row_to_hide * screen_obj->piece_num_width) * 6;
		length -= start_index;
	}

	render_draw_buffer(render_mngr,start_index,length);

	render_end_draw_buffer(render_mngr,wtrue,wfalse);

	render_pop_matrix(render_mngr);

}

void screen_fill_update(screen_fill_t* const screen_obj,render_manager_t* const render_mngr,const float elapsed)
{
	wbool update_text_coord = wfalse;

	if(screen_obj->fill_anim)
	{
		animation_update(screen_obj->fill_anim,elapsed);

		//update fill frame coord
		if(screen_obj->current_fill_frame !=  screen_obj->fill_anim->current_frame)
		{
			update_text_coord = wtrue;
			screen_obj->current_fill_frame =  screen_obj->fill_anim->current_frame;
		}
	}

	if(screen_obj->edge_anim)
	{
		animation_update(screen_obj->edge_anim,elapsed);

		//update edge frame coord
		if(screen_obj->current_edge_frame != screen_obj->edge_anim->current_frame)
		{
			update_text_coord = wtrue;
			screen_obj->current_edge_frame = screen_obj->edge_anim->current_frame;
		}
	}

	if(update_text_coord)
	{
		screen_fill_updatetxtcoord(screen_obj,render_mngr);
	}

	
}

void screen_fill_g_clean(screen_fill_t* const screen_obj)
{
	render_delete_buffer(screen_obj->VBOvertices);
	render_delete_buffer(screen_obj->VBOtexcoord);
}

void screen_fill_g_create(screen_fill_t* const screen_obj,render_manager_t* const render_mngr)
{
	screen_obj->VBOvertices = render_gen_buffer(render_mngr,screen_obj->vertices,((screen_obj->piece_num_width * screen_obj->piece_num_height)  * 12) * sizeof(float),wfalse,wfalse);
	screen_obj->VBOtexcoord = render_gen_buffer(render_mngr,screen_obj->texture_coords,((screen_obj->piece_num_width * screen_obj->piece_num_height)  * 12) * sizeof(float),wtrue,wtrue);
}


void screen_fill_free(screen_fill_t* const screen_obj)
{
	render_delete_buffer(screen_obj->VBOvertices);
	render_delete_buffer(screen_obj->VBOtexcoord);

	wf_free(screen_obj->anim_hash.keys);
	screen_obj->anim_hash.keys = NULL;
	wf_free(screen_obj->anim_array);
	screen_obj->anim_array = NULL;
	wf_free(screen_obj->vertices);
	wf_free(screen_obj->texture_coords);
}

