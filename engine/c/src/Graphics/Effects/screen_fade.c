#include <inttypes.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

#include <Render.h>

#include <Engine.h>
#include <Base/types.h>
#include <Base/functions.h>
#include <Base/geom.h>
#include <Base/Color.h>

#include <Render/render_manager.h>

#include <Graphics/Effects/screen_fade.h>

void screen_fade_init(screen_fade_t* const scr_fade)
{
	scr_fade->active = wfalse;
	scr_fade->current_alpha = 0.0f;
	scr_fade->fade_amout_step = 0;
	scr_fade->target_alpha = scr_fade->current_alpha;
	scr_fade->fade_amout_step = 0;
	scr_fade->fading = wfalse;
	Color_t base_color = {0.0f,0.0f,0.0f};
	scr_fade->fade_color = base_color;
}

void screen_fade_draw(screen_fade_t* const scr_fade,render_manager_t* const render_mngr)
{
	ColorA_t c_color = {scr_fade->fade_color.r,scr_fade->fade_color.g,scr_fade->fade_color.b,scr_fade->current_alpha};

	render_set_color(render_mngr,c_color);

	int16_t width_screen = (int16_t)(render_mngr->screen_info.width * render_mngr->zoom_factor);

	int16_t height_screen = (int16_t)(render_mngr->screen_info.height * render_mngr->zoom_factor);

	float vertices[] = {
		render_mngr->screen_info.position.x,render_mngr->screen_info.position.y - height_screen,
		render_mngr->screen_info.position.x + width_screen,render_mngr->screen_info.position.y - height_screen,
		render_mngr->screen_info.position.x + width_screen,render_mngr->screen_info.position.y,

		render_mngr->screen_info.position.x + width_screen,render_mngr->screen_info.position.y,
		render_mngr->screen_info.position.x,render_mngr->screen_info.position.y,
		render_mngr->screen_info.position.x,render_mngr->screen_info.position.y - height_screen
	};

	render_push_matrix(render_mngr);

	render_start_draw_array(render_mngr,2,6,vertices,NULL);

	render_set_color(render_mngr,c_color);

	render_draw_array(render_mngr,0,6);
	render_end_draw_array(render_mngr,wfalse);

	render_pop_matrix(render_mngr);
}

void screen_fade_setcolor(screen_fade_t* const scr_fade,Color_t new_color)
{
	scr_fade->fade_color = new_color;
}

void screen_fade_dofade(screen_fade_t* const scr_fade,float duration,int direction,float target_alpha)
{
	scr_fade->fade_amout_step = direction * (1.0f / duration);
	scr_fade->fade_direction = direction;

	scr_fade->active = wtrue;
	scr_fade->target_alpha = target_alpha;
	scr_fade->fading = wtrue;
}

void screen_fade_resetfade(screen_fade_t* const scr_fade,wbool fadein)
{
	if(fadein)
	{
		scr_fade->current_alpha = 1.0f;
		scr_fade->active = wtrue;
	}
	else
	{
		scr_fade->current_alpha = 0.0f;
	}

	scr_fade->target_alpha = scr_fade->current_alpha;
}

void screen_fade_update(screen_fade_t* const scr_fade,const float elapsed)
{
	if(!scr_fade->fading)
		return;

	scr_fade->current_alpha += (scr_fade->fade_amout_step * elapsed);

	if((scr_fade->current_alpha <= scr_fade->target_alpha && scr_fade->fade_direction < 0)  || (scr_fade->current_alpha >= scr_fade->target_alpha && scr_fade->fade_direction > 0) )
	{
		scr_fade->current_alpha = scr_fade->target_alpha;
		scr_fade->fading = wfalse;
	}

	if(scr_fade->current_alpha <= 0.0f)
	{
		scr_fade->current_alpha = 0.0f;
		scr_fade->active = wfalse;
	}

	if(scr_fade->current_alpha > 1.0f)
		scr_fade->current_alpha = 1.0f;

}