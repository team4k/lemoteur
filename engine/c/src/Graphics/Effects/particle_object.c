#include <inttypes.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

#include <Render.h>

#include <Engine.h>
#include <Base/types.h>
#include <Base/functions.h>
#include <Base/geom.h>
#include <Base/Color.h>

#include <Debug/Logprint.h>

#include <Render/render_manager.h>
#include <Graphics/TextureLoader.h>

#include <GameObjects/Level.pb-c.h>

#include <Graphics/Effects/particle_object.h>


void particle_object_init(particle_object_t* const obj,render_manager_t* const render_manager,const char* prog_id,texture_info* texture,particle_mov_callback_t move_callback,particle_created_callback_t created_callback,particle_destroyed_callback_t destroyed_callback)
{
	obj->prog_id = render_get_program(render_manager,prog_id);
	obj->texture = texture;
	obj->vertices_vbo = 0;
	obj->particle_mov_callback = move_callback;
	obj->particle_created_callback = created_callback;
	obj->particle_destroyed_callback = destroyed_callback;
	obj->ref_physics = NULL;
	obj->ref_script = NULL;
	obj->ref_entity = NULL;
	memset(obj->ref_module,0,128);

	//init pool size
	if(obj->emitter_array == NULL)
	{
		obj->emitter_array = (emitter_t*)wf_calloc(PARTICLE_EMITTER_POOLSIZE,sizeof(emitter_t));
		obj->pool_size = PARTICLE_EMITTER_POOLSIZE;
		obj->num_emitter = 0;
	}
}

static void _set_vertice(int16_t width,int16_t height,float* const vertices_array,const Vector_t* const position,uint32_t index)
{
	
	wfloat halfw = -(width * WFLOAT05);
	wfloat halfh = -(height * WFLOAT05);


	vertices_array[index++] = (position->x +  halfw);
	vertices_array[index++] = (position->y + (halfh + height));

	vertices_array[index++] = (position->x + halfw);
	vertices_array[index++] = (position->y + halfh);

	vertices_array[index++] = (position->x +  (halfw + width));
	vertices_array[index++] = (position->y +  halfh);

	vertices_array[index++] = (position->x +  halfw);
	vertices_array[index++] = (position->y + (halfh + height));

	vertices_array[index++] = (position->x +  (halfw + width));
	vertices_array[index++] = (position->y +  halfh);

	vertices_array[index++] = (position->x + (halfw + width));
	vertices_array[index++] = (position->y + (halfh + height));
}

void particle_object_addemitter(particle_object_t* const obj,ParticleEmitter* emitter_data,render_manager_t* const render_manager,const Vector_t center_position)
{
	if(obj->num_emitter == obj->pool_size)
		obj->emitter_array = (emitter_t*)wf_realloc(obj->emitter_array,(++obj->pool_size) * sizeof(emitter_t));

	emitter_t* emit_ptr = &obj->emitter_array[obj->num_emitter++];

	emit_ptr->color.r = (float)(emitter_data->color->r / 255.0f);
	emit_ptr->color.g = (float)(emitter_data->color->g / 255.0f);
	emit_ptr->color.b = (float)(emitter_data->color->b / 255.0f);
	emit_ptr->color.a = (float)(emitter_data->color->a / 255.0f);

	emit_ptr->particles = (particle_t*)wf_calloc(emitter_data->max_particle,sizeof(particle_t));

	emit_ptr->direction.x = (wfloat)emitter_data->direction->x;
	emit_ptr->direction.y = (wfloat)emitter_data->direction->y;
	emit_ptr->position.x = center_position.x + emitter_data->offset->x;
	emit_ptr->position.y = center_position.y + emitter_data->offset->y;

	emit_ptr->height_particle = emitter_data->height_particle;
	emit_ptr->width_particle = emitter_data->width_particle;
	emit_ptr->life_time = emitter_data->life_time;
	emit_ptr->max_particle = emitter_data->max_particle;
	emit_ptr->total_particle = emitter_data->total_particle;
	emit_ptr->rate = emitter_data->rate;

	emit_ptr->rate_prop = 1000.0f / emit_ptr->rate;

	emit_ptr->num_alive_particles = 0;
	emit_ptr->speed = emitter_data->speed;
	emit_ptr->rate_track = 0;
	emit_ptr->total_particle = 0;
	emit_ptr->total_particle_track = 0;

	if(emitter_data->has_frame)
		emit_ptr->texture_frame = emitter_data->frame;
	else
		emit_ptr->texture_frame = 0;

	//emitter_gen_vertices(emit_ptr);
}

void particle_object_genvbo(particle_object_t* const obj,render_manager_t* const render_manager)
{
	if(obj->num_emitter == 0)
	{
		logprint("No emitter for this particle object! use add emitter first");
		return;
	}

	size_t num_part = 0;

	for(int32_t iem = 0;iem < obj->num_emitter;iem++)
		num_part += obj->emitter_array[iem].max_particle;

	obj->num_vertices = num_part * 12;
	obj->all_vertices = (float*)wf_calloc(obj->num_vertices,sizeof(float));

	obj->vertices_vbo = render_gen_buffer(render_manager,obj->all_vertices,obj->num_vertices * sizeof(float),wtrue,wtrue);
}


void particle_object_draw(particle_object_t* const obj,render_manager_t* const render_manager,Vector_t offset)
{
	uint32_t vertices_indx = 0;
	uint32_t num_particles_to_draw = 0;

	for(int32_t iemit = 0; iemit < obj->num_emitter;iemit++)
	{
		uint32_t count_particules = 0;

		for(int32_t i_part = 0;i_part < obj->emitter_array[iemit].max_particle;i_part++)
		{
			if(count_particules == obj->emitter_array[iemit].num_alive_particles)
				break;

			if(obj->emitter_array[iemit].particles[i_part].alive)
			{
				_set_vertice(obj->emitter_array[iemit].particles[i_part].width,obj->emitter_array[iemit].particles[i_part].height,obj->all_vertices,&obj->emitter_array[iemit].particles[i_part].position,vertices_indx);

				num_particles_to_draw++;
				vertices_indx += 12;
				count_particules++;
			}
		}
	}


	if(num_particles_to_draw == 0)
		return;

	render_discard_updatebuffer(render_manager,obj->vertices_vbo,obj->all_vertices,obj->num_vertices * sizeof(float),wtrue,wtrue);

	render_use_program(render_manager,obj->prog_id);

	TEXTURE_ID texture = NULL;

	if(obj->texture)
		texture = obj->texture->texture_pointer;

	render_push_matrix(render_manager);

	render_set_position(render_manager,offset);

	render_set_color(render_manager,red_color);

	render_start_draw_buffer(render_manager,texture,obj->vertices_vbo,0,0,2);
	render_draw_buffer(render_manager,0,num_particles_to_draw * 6);
	render_end_draw_buffer(render_manager,wfalse,wfalse);

	render_pop_matrix(render_manager);

	render_reset_color(render_manager);

}

void particle_object_update(particle_object_t* const obj, float delta_time)
{
	for(int32_t iemit = 0; iemit < obj->num_emitter;iemit++)
	{
		emitter_t* ptr_emit = &obj->emitter_array[iemit];

		//emit particle
		if(ptr_emit->total_particle == 0 || ptr_emit->total_particle > ptr_emit->total_particle_track)//emit particle only if we are under the total or if there is not limit
		{
			if(ptr_emit->rate_track >= ptr_emit->rate_prop)
			{
				int32_t ipart = 0;
				//emit particle
				if(ptr_emit->num_alive_particles < ptr_emit->max_particle)//emit particle if we didn't reached the max number of particle on screen
				{

					while(ptr_emit->particles[ipart].alive == wtrue && ipart < ptr_emit->max_particle) //find first unused particle
							ipart++;

					if(ipart < ptr_emit->max_particle)
					{
						ptr_emit->particles[ipart].position = ptr_emit->position;
						ptr_emit->particles[ipart].particule_movement = PART_MOV_LINEAR;
						ptr_emit->particles[ipart].direction = ptr_emit->direction;
						ptr_emit->particles[ipart].speed = ptr_emit->speed;
						ptr_emit->particles[ipart].alive = wtrue;
						ptr_emit->particles[ipart].width = ptr_emit->width_particle;
						ptr_emit->particles[ipart].height = ptr_emit->height_particle;
						ptr_emit->particles[ipart].life_time =  ptr_emit->life_time;
						ptr_emit->num_alive_particles++;
						ptr_emit->total_particle_track++;

						if(obj->particle_created_callback != NULL)
							obj->particle_created_callback(obj,&ptr_emit->particles[ipart]);
					}
					else
					{
						logprint("Can't emit particle!, max number of particle per emitter reach, please review your emitter settings");
					}
				}

				ptr_emit->rate_track = 0.0f;
			}
			else
			{
				ptr_emit->rate_track += delta_time;
			}
		}

		//update particle positions
		

		int32_t delta_num_particle = 0;
		int32_t delta_array_element =0;
		int32_t count_particles = 0;

		for(int32_t i_particle = 0; i_particle< ptr_emit->max_particle;i_particle++)
		{
			if(count_particles == ptr_emit->num_alive_particles)
				break;

			if(ptr_emit->particles[i_particle].alive)
			{
				//update life time
				ptr_emit->particles[i_particle].life_time -= delta_time;

				if(ptr_emit->particles[i_particle].life_time <= 0.0f) //particle life ended, deactivate it
				{
					ptr_emit->particles[i_particle].alive = wfalse;
					delta_num_particle++;
					delta_array_element++;

					if(obj->particle_destroyed_callback != NULL)
						obj->particle_destroyed_callback(obj,&ptr_emit->particles[i_particle]);
					
				}
				else //particle is alive, move it
				{
					
					if(ptr_emit->particles[i_particle].particule_movement == PART_MOV_LINEAR) //default linear movement
					{
						//compute our movement vector
						float mov = ptr_emit->particles[i_particle].speed * (delta_time * 0.001f);
						Vector_t movevec = Vec_mulvec_f(ptr_emit->particles[i_particle].direction,mov);
						Vec_add(&ptr_emit->particles[i_particle].position,movevec);
					}
					else if(ptr_emit->particles[i_particle].particule_movement == PART_MOV_SCRIPT)//use a callback function to let a script deciding of the particle movement
					{
						if(obj->particle_mov_callback != NULL)
							obj->particle_mov_callback(obj,&ptr_emit->particles[i_particle]);
					}


					if(delta_array_element != 0)
					{
						//copy the alive particle in the slot of the dead particle
						memcpy(&ptr_emit->particles[i_particle-delta_array_element],&ptr_emit->particles[i_particle],sizeof(particle_t));
						//set to zero the content of the copied alive particle
						memset(&ptr_emit->particles[i_particle],0,sizeof(particle_t));
						delta_array_element--;
					}

				}

				count_particles++;
			}
		}

		ptr_emit->num_alive_particles -= delta_num_particle;

	}
}

void particle_object_recycle(particle_object_t* const obj)
{
	if(obj->emitter_array != NULL)
	{
		for(int32_t iem = 0; iem < obj->num_emitter;iem++)
			wf_free(obj->emitter_array[iem].particles);

		memset(obj->emitter_array,0,sizeof(emitter_t) * obj->pool_size);
		obj->num_emitter = 0;
	}
}

void particle_object_g_create(particle_object_t* const obj,render_manager_t* const render_manager)
{
	particle_object_genvbo(obj,render_manager);
}

void particle_object_g_free(particle_object_t* const obj)
{
	render_delete_buffer(obj->vertices_vbo);
}

void particle_object_free(particle_object_t* const obj)
{
	if(obj->emitter_array != NULL)
	{
		for(int32_t iem = 0; iem < obj->num_emitter;iem++)
			wf_free(obj->emitter_array[iem].particles);

		wf_free(obj->emitter_array);
		wf_free(obj->all_vertices);
		obj->emitter_array = NULL;
		obj->num_emitter = 0;
		obj->pool_size = 0;

		particle_object_g_free(obj);
	}
}