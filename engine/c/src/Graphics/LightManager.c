#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <stdlib.h>
#include <inttypes.h>
#include <math.h>
#include <time.h>

#if defined(_WIN32)
    #include <Windows.h>
#endif

#include <Engine.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/geom.h>
#include <Base/Color.h>

#include <Utils/hashmap.h>

#include <Debug/Logprint.h>

#include <Render.h>
#include <Render/render_manager.h>

#include <Graphics/Light.h>

#include <Graphics/LightManager.h>

//default function to check which tile collision type are not blocking light
static wbool default_check_flood(const int32_t grid_value)
{
	if (grid_value == NO_COLLISION)
		return wtrue;

	return wfalse;
}

void light_manager_init(light_manager_t* const light_manager, ColorA_t ambient_color, render_manager_t* const render_mngr, SHADERPROGRAM_ID prog_mask, SHADERPROGRAM_ID prog_light, SHADERPROGRAM_ID prog_render,
	light_model light_gen_type, int32_t width_texture, int32_t height_texture, uint32_t col_count, uint32_t row_count)
{
	light_manager->ambient_color = ambient_color;
	light_manager->prog_light = prog_light;
	light_manager->prog_render = prog_render;
	light_manager->prog_mask = prog_mask;
	light_manager->light_gen_type = light_gen_type;
	light_manager->check_flood = &default_check_flood;

	light_manager->lightfloodfillarray = (uint32_t*)wf_calloc(col_count * row_count, sizeof(uint32_t));
	light_manager->lightfloodfillarray_size = sizeof(uint32_t) * (col_count * row_count);

	hashmap_initalloc(&light_manager->light_hash,POOL_LIGHT_NUM,sizeof(light_t),POOL_LIGHT_NUM);

	//create FBO
	light_manager->fbo = render_alloc_fbo(render_mngr);
	light_manager->shadow_texture = render_alloc_texture(render_mngr);
	light_manager->width_texture = width_texture;
	light_manager->height_texture = height_texture;

	render_create_fbo(render_mngr,light_manager->fbo,light_manager->shadow_texture,width_texture,height_texture);

	float tmp_vertices[] =  {
		0.0f,
		0.0f,

		(float)render_mngr->screen_info.width,
		0.0f,

		(float)render_mngr->screen_info.width,
		(float)render_mngr->screen_info.height,

		(float)render_mngr->screen_info.width,
		(float)render_mngr->screen_info.height,

		0.0f,
		(float)render_mngr->screen_info.height,

		0.0f,
		0.0f
	};

	memcpy(&light_manager->texture_render_vertices[0],&tmp_vertices[0],sizeof(float) * 12);

	 float tmp_texcoords [] = {
		0.0f,
		0.0f,

		1.0f,
		0.0f,

		1.0f,
		1.0f,

		1.0f,
		1.0f,

		0.0f,
		1.0f,

		0.0f,
		0.0f
	};

	memcpy(&light_manager->texture_render_texcoords[0],&tmp_texcoords[0],sizeof(float) * 12);


	//create our shadow texture render buffer
	light_manager->r_vert_buff = render_gen_buffer(render_mngr,&light_manager->texture_render_vertices[0],sizeof(float) * 12,wfalse,wfalse);
	light_manager->r_tex_buff = render_gen_buffer(render_mngr,&light_manager->texture_render_texcoords[0],sizeof(float) * 12,wfalse,wfalse);


}

void light_manager_setshaderprograms(light_manager_t* const light_manager, SHADERPROGRAM_ID prog_mask, SHADERPROGRAM_ID prog_light, SHADERPROGRAM_ID prog_render)
{
	light_manager->prog_light = prog_light;
	light_manager->prog_render = prog_render;
	light_manager->prog_mask = prog_mask;
}

void light_manager_resizerender(light_manager_t* const light_manager,render_manager_t* const render_mngr)
{
	float zoom_value = fmaxf(render_mngr->zoom_factor,1.0f);

	float tmp_vertices[] =  {
		0.0f,
		0.0f,

		(float)render_mngr->screen_info.width * zoom_value,
		0.0f,

		(float)render_mngr->screen_info.width * zoom_value,
		(float)render_mngr->screen_info.height * zoom_value,

		(float)render_mngr->screen_info.width * zoom_value,
		(float)render_mngr->screen_info.height * zoom_value,

		0.0f,
		(float)render_mngr->screen_info.height * zoom_value,

		0.0f,
		0.0f
	};

	memcpy(&light_manager->texture_render_vertices[0],&tmp_vertices[0],sizeof(float) * 12);
	render_discard_updatebuffer(render_mngr,light_manager->r_vert_buff,&light_manager->texture_render_vertices[0],sizeof(float) * 12,wfalse,wfalse);
}

light_t* light_manager_addlight(light_manager_t* const light_manager,Vector_t position,ColorA_t color,float radius,const char* light_id,const char* parent_id,light_type l_type)
{
	light_t* o_light = (light_t*)hashmap_push(&light_manager->light_hash,light_id);

	light_init(o_light,position,radius,color,light_manager->light_gen_type,light_id,parent_id,l_type);

	return o_light;
}

light_t* light_manager_getlight(light_manager_t* const light_manager,const char* light_id)
{
	return (light_t*)hashmap_getvalue(&light_manager->light_hash,light_id);
}

light_t* light_manager_getnearestlight(light_manager_t* const light_manager,Vector_t position,render_manager_t* const render_mngr)
{
	uint32_t light_index = 0;
	uint32_t light_count = 0;

	wfloat nearest_light_distance = -1.0;
	light_t* nearest_light = NULL;

	Rect_t render_rect = {(uint32_t)(render_mngr->screen_info.width * render_mngr->zoom_factor),(uint32_t)(render_mngr->screen_info.height * render_mngr->zoom_factor), render_mngr->screen_info.position};

	for(light_t* olight = (light_t*)hashmap_iterate(&light_manager->light_hash,&light_index,&light_count);olight != NULL;olight =  (light_t*)hashmap_iterate(&light_manager->light_hash,&light_index,&light_count))
	{
		Circle_t light_circ = {olight->radius,olight->position};

		if(!circle_intersect_rect(render_rect,light_circ))//don't use light outside of viewport
			continue;

		wfloat current_dist_squared = Vec_lengthsquared(Vec_minus(olight->position,position));
		
		if(nearest_light_distance == -1.0 || current_dist_squared < nearest_light_distance)
		{
			nearest_light_distance = current_dist_squared;
			nearest_light = olight;
		}
	}

	return nearest_light;
}

int32_t light_manager_getinrangelight(light_manager_t* const light_manager,const Rect_t* const render_box,Vector_t position,render_manager_t* const render_mngr,light_t* light_array[MAX_LIGHT_PER_ELEMENT])
{
	uint32_t light_index = 0;
	uint32_t light_count = 0;

	int32_t light_get = 0;

	//Rect_t render_rect = {(int16_t)(render_mngr->screen_info.width * render_mngr->zoom_factor),(int16_t)(render_mngr->screen_info.height * render_mngr->zoom_factor), render_mngr->screen_info.position};

	for(light_t* olight = (light_t*)hashmap_iterate(&light_manager->light_hash,&light_index,&light_count);olight != NULL;olight =  (light_t*)hashmap_iterate(&light_manager->light_hash,&light_index,&light_count))
	{
		if(light_get >= MAX_LIGHT_PER_ELEMENT)
			return light_get;

		Circle_t light_circ = {olight->radius * 2,olight->position};

		if(!circle_intersect_rect((*render_box),light_circ))//don't use light which don't intersect with render box
			continue;

		light_array[light_get] = olight;

		light_get++;
	}

	return light_get;
}

void light_manager_removelight(light_manager_t* const light_manager,const char* light_id)
{
	light_t* rm_light = (light_t*)hashmap_getvalue(&light_manager->light_hash,light_id);

	if(rm_light != NULL)
	{
		light_free(rm_light);
		hashmap_remove(&light_manager->light_hash,light_id);
	}
}

void light_manager_regenall(light_manager_t* const light_manager)
{
	uint32_t light_index = 0;
	uint32_t light_count = 0;

	for(light_t* olight = (light_t*)hashmap_iterate(&light_manager->light_hash,&light_index,&light_count);olight != NULL;olight =  (light_t*)hashmap_iterate(&light_manager->light_hash,&light_index,&light_count))
	{
		olight->build_volumes = wtrue;
	}
}

void light_manager_regenlight(light_manager_t* const light_manager,const Circle_t circ_regen)
{
	uint32_t light_index = 0;
	uint32_t light_count = 0;

	for(light_t* olight = (light_t*)hashmap_iterate(&light_manager->light_hash,&light_index,&light_count);olight != NULL;olight =  (light_t*)hashmap_iterate(&light_manager->light_hash,&light_index,&light_count))
	{
		Circle_t light_circ = {olight->radius,olight->position};

		if(circle_intersect_circle(light_circ,circ_regen))
			olight->build_volumes = wtrue;
	}
}

static wbool canflood(uint32_t* const floodfillarray, size_t floodfillarraycount, uint32_t col, uint32_t row, const uint32_t col_count, const uint32_t row_count, check_flood_t floodcheck,uint32_t* return_index)
{
	if (row >= row_count || col >= col_count) {
		return wfalse;
	}

	uint32_t index = (row * col_count) + col;

	if (index < 0 || (size_t)index >= floodfillarraycount) {
		return wfalse;
	}

	if (floodcheck(floodfillarray[index])) {//== NO_COLLISION
		(*return_index) = index;
		return wtrue;
	}

	return wfalse;
}

static void _floodfill(uint32_t* const floodfillarray, size_t floodfillarraycount, uint32_t col, uint32_t row, const uint32_t col_count, const uint32_t row_count, check_flood_t floodcheck)
{
	uint32_t index = 0;

	wbool bflood = canflood(floodfillarray, floodfillarraycount, col, row, col_count, row_count, floodcheck,&index);

	if (bflood) {
		floodfillarray[index] = FLOODLIGHT;

		_floodfill(floodfillarray, floodfillarraycount, col + 1, row, col_count, row_count, floodcheck);
		_floodfill(floodfillarray, floodfillarraycount, col - 1, row, col_count, row_count, floodcheck);
		_floodfill(floodfillarray, floodfillarraycount, col, row + 1, col_count, row_count, floodcheck);
		_floodfill(floodfillarray, floodfillarraycount, col, row - 1, col_count, row_count, floodcheck);
	}
}

void light_manager_setfloodcheckfunc(light_manager_t* const light_manager, check_flood_t floodcheck)
{
	light_manager->check_flood = floodcheck;
}

void light_manager_updatefloodfillarray(light_manager_t* const light_manager, size_t collisions_array_size)
{
	if (collisions_array_size == light_manager->lightfloodfillarray_size)
		return;

	light_manager->lightfloodfillarray = (uint32_t*)wf_realloc(light_manager->lightfloodfillarray, collisions_array_size);
	light_manager->lightfloodfillarray_size = collisions_array_size;
	memset(light_manager->lightfloodfillarray, 0, light_manager->lightfloodfillarray_size);

}

void light_manager_floodlight(light_manager_t* const light_manager, level_base_t* const level_container, const int32_t* const collisions_array, size_t collisions_array_size, const uint32_t tile_size, const uint32_t col_count, const uint32_t row_count,const uint32_t start_tile,const char* map_id)
{
	//copy collisions array in our floodfillarray
	if (collisions_array_size != light_manager->lightfloodfillarray_size) {
		logprint("collisions array don't have the same size of floodfillarray, you have to call light_manager_updatefloodfillarray()");
		return;
	}

	w_memcpy(light_manager->lightfloodfillarray, light_manager->lightfloodfillarray_size, collisions_array, collisions_array_size);
	uint32_t start_row = (uint32_t)floorf((float)(start_tile / col_count));
	uint32_t start_col = start_tile - (col_count * start_row);

	//flood light from the start tile
	_floodfill(light_manager->lightfloodfillarray, light_manager->lightfloodfillarray_size / sizeof(uint32_t), start_col, start_row, col_count, row_count, light_manager->check_flood);

	//iterate through light and enable only the one in a flooded tile
	//Rect_t render_rect = { (int16_t)(render_mngr->screen_info.width * render_mngr->zoom_factor),(int16_t)(render_mngr->screen_info.height * render_mngr->zoom_factor), render_mngr->screen_info.position };

	uint32_t light_index = 0;
	uint32_t light_count = 0;

	//set which light are visible
	for (light_t* olight = (light_t*)hashmap_iterate(&light_manager->light_hash, &light_index, &light_count); olight != NULL; olight = (light_t*)hashmap_iterate(&light_manager->light_hash, &light_index, &light_count))
	{
		if (map_id != NULL && strcmp(olight->parent_id, map_id) != 0)
			continue;

		//get the current light tile
		uint32_t lighttile = lvlbase_gettilefromvector(lvlbase_getcurrentlevelobj(level_container), level_container->current_level_type, olight->position);

		//check for out of bound
		if (lighttile >= light_manager->lightfloodfillarray_size / sizeof(uint32_t)) {
			continue;
		}

		//visible if in floodlight
		olight->visiblebyplayer = (wbool)(light_manager->lightfloodfillarray[lighttile] == FLOODLIGHT);
	}


}

void light_manager_rendermask(light_manager_t* const light_manager,edge_container_t* const edge_array,int32_t edge_count,render_manager_t* const render_mngr,wbool draw_direct,const char* parent_id,int32_t tile_size,int32_t col_count,int32_t row_count, Vector_t offset)
{

	render_use_program(render_mngr,light_manager->prog_mask);


	if(!draw_direct)
	{
		render_bind_framebuffer(render_mngr,light_manager->fbo,light_manager->shadow_texture,light_manager->width_texture,light_manager->height_texture);
		render_clear_screenwithcolor(render_mngr,light_manager->ambient_color);
	}

	render_setblend(render_mngr,blend_add,blend_replace);

	int32_t shadow_volumes_count = 0;

	uint32_t light_index = 0;
	uint32_t light_count = 0;

	float tex_to_screen_ratio_x = light_manager->width_texture / (float)render_mngr->screen_info.width;
	float tex_to_screen_ratio_y = light_manager->height_texture / (float)render_mngr->screen_info.height;


	//Vector_t off_pos = {render_mngr->screen_info.position.x  , render_mngr->screen_info.position.y - (render_mngr->screen_info.height * render_mngr->zoom_factor)};
	Vector_t off_pos =  {-(render_mngr->scroll_vector.x * render_mngr->zoom_factor), -(render_mngr->scroll_vector.y * render_mngr->zoom_factor)};
	 
	Vector_t ratio = Vec(tex_to_screen_ratio_x,tex_to_screen_ratio_y);

	if(render_mngr->zoom_factor > 1.0f)
	{
		Vec_divsrc_f(&ratio,render_mngr->zoom_factor);
	}

	if(draw_direct)
		ratio = Vec(1.0f,1.0f);


	Rect_t render_rect = {(uint32_t)(render_mngr->screen_info.width * render_mngr->zoom_factor),(uint32_t)(render_mngr->screen_info.height * render_mngr->zoom_factor), render_mngr->screen_info.position};

	//create the alpha mask
	for(light_t* olight = (light_t*)hashmap_iterate(&light_manager->light_hash,&light_index,&light_count);olight != NULL;olight =  (light_t*)hashmap_iterate(&light_manager->light_hash,&light_index,&light_count))
	{
		if((parent_id != NULL && strcmp(olight->parent_id,parent_id) != 0) || !olight->visiblebyplayer)
			continue;


		Circle_t light_circ = {olight->radius,olight->position};

		if(!circle_intersect_rect(render_rect,light_circ))//don't draw light outside of viewport
			continue;

		light_genlight_volumes(olight, render_mngr,edge_array,edge_count,tile_size,col_count,row_count,ratio, offset);

		//draw light object
		light_draw(olight,render_mngr,wtrue,off_pos,ratio,light_manager->ambient_color.a,light_manager->width_texture,light_manager->height_texture);

	}

	

	if(!draw_direct)
		render_unbind_framebuffer(render_mngr,light_manager->fbo);

	render_setblend(render_mngr,blend_add,blend_alpha);

}


void light_manager_draw(light_manager_t* const light_manager,render_manager_t* const render_mngr,wbool draw_direct,const char* parent_id,int32_t tile_size,int32_t col_count,int32_t row_count)
{
	render_use_program(render_mngr,light_manager->prog_light);

	int32_t shadow_volumes_count = 0;

	if(!draw_direct)
		render_bind_framebuffer(render_mngr,light_manager->fbo,light_manager->shadow_texture,light_manager->width_texture,light_manager->height_texture);

	uint32_t light_index = 0;
	uint32_t light_count = 0;

	float tex_to_screen_ratio_x = light_manager->width_texture / (float)render_mngr->screen_info.width;
	float tex_to_screen_ratio_y = light_manager->height_texture / (float)render_mngr->screen_info.height;


	//Vector_t off_pos = {render_mngr->screen_info.position.x  , render_mngr->screen_info.position.y - (render_mngr->screen_info.height * render_mngr->zoom_factor)};
	Vector_t off_pos =  {-(render_mngr->scroll_vector.x * render_mngr->zoom_factor), -(render_mngr->scroll_vector.y * render_mngr->zoom_factor)};
	 
	Vector_t ratio = Vec(tex_to_screen_ratio_x,tex_to_screen_ratio_y);

	if(render_mngr->zoom_factor > 1.0f)
	{
		Vec_divsrc_f(&ratio,render_mngr->zoom_factor);
	}

	if(draw_direct)
		ratio = Vec(1.0f,1.0f);


	Rect_t render_rect = {(uint32_t)(render_mngr->screen_info.width * render_mngr->zoom_factor),(uint32_t)(render_mngr->screen_info.height * render_mngr->zoom_factor), render_mngr->screen_info.position};


	//blend lights together

	render_setblend(render_mngr,blend_add,blend_additive);
	
	//draw a second time with blend mode on
	for(light_t* olight = (light_t*)hashmap_iterate(&light_manager->light_hash,&light_index,&light_count);olight != NULL;olight =  (light_t*)hashmap_iterate(&light_manager->light_hash,&light_index,&light_count))
	{
		if((parent_id != NULL && strcmp(olight->parent_id,parent_id) != 0) || !olight->visiblebyplayer)
			continue;

		Circle_t light_circ = {olight->radius,olight->position};

		if(!circle_intersect_rect(render_rect,light_circ))//don't draw light outside of viewport
			continue;

		light_draw(olight,render_mngr,wfalse,off_pos,ratio,light_manager->ambient_color.a,light_manager->width_texture,light_manager->height_texture);
	}

	render_setblend(render_mngr,blend_add,blend_alpha);


	if(!draw_direct)
	{
		render_unbind_framebuffer(render_mngr,light_manager->fbo);

		render_use_program(render_mngr,light_manager->prog_render);

		render_push_matrix(render_mngr);

		//Vector_t tex_pos = {(int32_t)render_mngr->screen_info.position.x, (int32_t)render_mngr->screen_info.position.y - (render_mngr->screen_info.height * render_mngr->zoom_factor)};
		Vector_t tex_pos = {-(render_mngr->scroll_vector.x * render_mngr->zoom_factor), -(render_mngr->scroll_vector.y * render_mngr->zoom_factor)};


		render_set_position(render_mngr,tex_pos);

		render_start_draw_buffer(render_mngr,light_manager->shadow_texture,light_manager->r_vert_buff,light_manager->r_tex_buff,0,2);

		render_draw_array(render_mngr,0,6);
		render_end_draw_buffer(render_mngr,wtrue,wfalse);

		render_pop_matrix(render_mngr);
	}

}

void light_manager_recycle(light_manager_t* const light_manager)
{
	hashmap_recycle(&light_manager->light_hash);
}

void light_manager_g_create(light_manager_t* const light_manager, render_manager_t* const render_mngr)
{
	light_manager->fbo = render_alloc_fbo(render_mngr);
	light_manager->shadow_texture = render_alloc_texture(render_mngr);

	render_create_fbo(render_mngr, light_manager->fbo, light_manager->shadow_texture, light_manager->width_texture,light_manager->height_texture);

	//recreate vertices in case screen size changed
	float tmp_vertices[] = {
		0.0f,
		0.0f,

		(float)render_mngr->screen_info.width,
		0.0f,

		(float)render_mngr->screen_info.width,
		(float)render_mngr->screen_info.height,

		(float)render_mngr->screen_info.width,
		(float)render_mngr->screen_info.height,

		0.0f,
		(float)render_mngr->screen_info.height,

		0.0f,
		0.0f
	};

	memcpy(&light_manager->texture_render_vertices[0], &tmp_vertices[0], sizeof(float) * 12);

	//create our shadow texture render buffer
	light_manager->r_vert_buff = render_gen_buffer(render_mngr, &light_manager->texture_render_vertices[0], sizeof(float) * 12, wfalse, wfalse);
	light_manager->r_tex_buff = render_gen_buffer(render_mngr, &light_manager->texture_render_texcoords[0], sizeof(float) * 12, wfalse, wfalse);

	uint32_t light_index = 0;
	uint32_t light_count = 0;

	for (light_t* olight = (light_t*)hashmap_iterate(&light_manager->light_hash, &light_index, &light_count); olight != NULL; olight = (light_t*)hashmap_iterate(&light_manager->light_hash, &light_index, &light_count))
	{
		light_g_create(olight);
	}
}


void light_manager_g_clean(light_manager_t* const light_manager,render_manager_t* const render_mngr)
{
	uint32_t light_index = 0;
	uint32_t light_count = 0;

	for (light_t* olight = (light_t*)hashmap_iterate(&light_manager->light_hash, &light_index, &light_count); olight != NULL; olight = (light_t*)hashmap_iterate(&light_manager->light_hash, &light_index, &light_count))
	{
		light_g_clean(olight);
	}

	if (light_manager->fbo != NULL)
	{
		render_delete_fbo(render_mngr, light_manager->fbo);
		light_manager->fbo = NULL;
	}

	if (light_manager->shadow_texture != NULL)
	{
		render_delete_texture(render_mngr, light_manager->shadow_texture, wfalse);
		light_manager->shadow_texture = NULL;
	}

	render_delete_buffer(light_manager->r_vert_buff);

	render_delete_buffer(light_manager->r_tex_buff);
}

void light_manager_free(light_manager_t* const light_manager,render_manager_t* const render_mngr)
{
	uint32_t light_index = 0;
	uint32_t light_count = 0;

	for (light_t* olight = (light_t*)hashmap_iterate(&light_manager->light_hash, &light_index, &light_count); olight != NULL; olight = (light_t*)hashmap_iterate(&light_manager->light_hash, &light_index, &light_count))
	{
		light_free(olight);
	}

	hashmap_free(&light_manager->light_hash);

	//delete fbo and texture
	
	if(light_manager->fbo != NULL)
	{
		render_delete_fbo(render_mngr,light_manager->fbo);
		light_manager->fbo = NULL;
	}

	if(light_manager->shadow_texture != NULL)
	{
		render_delete_texture(render_mngr,light_manager->shadow_texture,wfalse);
		light_manager->shadow_texture = NULL;
	}

	render_delete_buffer(light_manager->r_vert_buff);

	render_delete_buffer(light_manager->r_tex_buff);

	wf_free(light_manager->lightfloodfillarray);
	light_manager->lightfloodfillarray = NULL;
}