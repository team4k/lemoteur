#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <stdlib.h>
#include <inttypes.h>
#include <math.h>
#include <time.h>

#if defined(_WIN32)
    #include <Windows.h>
#endif

#include <Engine.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/geom.h>
#include <Base/Color.h>

#include <Utils/Hash.h>

#include <Debug/Logprint.h>

#include <Render.h>
#include <Render/render_manager.h>

#include <Graphics/geomdraw.h>
#include <Graphics/Light.h>



void light_init(light_t* const light,Vector_t position,float radius,ColorA_t color,light_model gen_type,const char* light_id,const char* parent_id,light_type l_type)
{
	light->position = position;
	light->radius = radius;
	light->color = color;
	light->build_volumes = wtrue;
	light->power_factor = 1.0f;
	light->visiblebyplayer = wtrue;

	w_strcpy(light->light_id,128,light_id);
	
	w_strcpy(light->parent_id,64,parent_id);

	light->num_shadow_volumes = 0;
	light->max_shadow_volumes = POOL_SHADOWS_VOLUMES;
	light->enabled = wtrue;
	light->gen_type = gen_type;
	light->l_type = l_type;
	//shadow volume pools
	light->shadows_volumes = (shadow_volume_t*)wf_malloc(POOL_SHADOWS_VOLUMES * sizeof(shadow_volume_t));

	light->num_light_volumes = 0;
	light->max_light_volumes = POOL_LIGHT_VOLUMES;

	light->light_volumes = (triangle_t*)wf_calloc(POOL_LIGHT_VOLUMES,sizeof(triangle_t));

	//default directional value
	light->rotation_angle = deg2rad(15.0f);
	light->open_angle = deg2rad(30.0f);
	
	//attenuation values
	light->constant = 1.0f;
	light->linear = 1.0f;
	light->quadratic = 0.0f;

	light->vertexBuffer = NULL;
	light->baseratio = vectorzero;

	for (uint8_t ibuf = 0; ibuf < 3; ibuf++) {
		light->bufferupdated[ibuf] = wtrue;
		light->buffersync[ibuf] = NULL;
		light->vertices[ibuf] = render_get_emptybuffervalue();
		light->num_volumes_in_buffer[ibuf] = 0;
	}
}

static Vector_t Intersect(Vector_t p1,Vector_t p2,Vector_t q1,Vector_t q2)
{
	Vector_t i = vectorzero;

	if((p2.x - p1.x) == 0 && (q2.x - q1.x) == 0)
		i.x = 0, i.y = 0;
	else if((p2.x - p1.x) == 0)
	{
		i.x = p1.x;

		wfloat c = (q2.y - q1.y) / (q2.x - q1.x);
		wfloat d = q1.y - q1.x * c;

		i.y = c * i.x + d;
	}
	else if((q2.x - q1.x) == 0)
	{
		i.x = q1.x;

		wfloat a = (p2.y - p1.y) / (p2.x - p1.x);
		wfloat b = p1.y - p1.x * a;

		i.y = a * i.x + b;
	}
	else
	{
		wfloat a = (p2.y - p1.y) / (p2.x - p1.x);
		wfloat b = p1.y - p1.x * a;

		wfloat c = (q2.y - q1.y) / (q2.x - q1.x);
		wfloat d = q1.y - q1.x * c;

		i.x = (d-b)/(a-c);
		i.y = a * i.x + b;
	}

	//logprint("intersect val x = %5.2f y = %5.2f",i.x,i.y);

	return i;
}

static Vector_t Collision(Vector_t p1, Vector_t p2, Vector_t q1, Vector_t q2)
{
	Vector_t i;
	i = Intersect(p1, p2, q1, q2);

	if(((i.x >= p1.x - 0.1 && i.x <= p2.x + 0.1)
		|| (i.x >= p2.x - 0.1 && i.x <= p1.x + 0.1))
	&& ((i.x >= q1.x - 0.1 && i.x <= q2.x + 0.1)
		|| (i.x >= q2.x - 0.1 && i.x <= q1.x + 0.1))
	&& ((i.y >= p1.y - 0.1 && i.y <= p2.y + 0.1)
		|| (i.y >= p2.y - 0.1 && i.y <= p1.y + 0.1))
	&& ((i.y >= q1.y - 0.1 && i.y <= q2.y + 0.1)
		|| (i.y >= q2.y - 0.1 && i.y <= q1.y + 0.1)))
		return i;
	else
		return Vec(0.0f,0.0f);
}


static void light_add_volume(light_t* const light,Vector_t pt1,Vector_t pt2, int32_t min_val,edge_container_t* const edge_array,int32_t edge_array_count,int32_t light_index,int32_t max_index,Vector_t grid_dir,int32_t row_count,int32_t col_count,int32_t ignore_counter,int32_t max_ignore,int32_t ignore_step,int32_t current_row,int32_t current_col)
{
	int32_t iedge = min_val - 1;

	//walk the edge array in a circular way, we take the light position as the starting point, then rotate around it related tile to get the edges sorted by distance
	while(++iedge < (max_index * 4))
	{		
		if((iedge % 4) == 0 && iedge != min_val)
		{
			if(ignore_counter == 0)
			{
				if(grid_dir.y != 0.0f)
				{
					grid_dir.x = grid_dir.y * -1;
					grid_dir.y = 0.0f;
				
				}
				else if(grid_dir.x != 0.0f)
				{
					grid_dir.y = grid_dir.x;
					grid_dir.x = 0.0f;
				}

				ignore_counter = max_ignore;
				ignore_step++;
			}
			else
			{
				ignore_counter--;
			}


			if(ignore_step == 2)
			{
				max_ignore++;

				ignore_counter = max_ignore;
				ignore_step = 0;
			}

			current_row += (int32_t)grid_dir.y;
			current_col += (int32_t)grid_dir.x;

			iedge = ((current_row * col_count) + current_col) * 4;

		}

		if(current_row < 0 || current_row >= row_count || current_col < 0 || current_col >= col_count)
			continue;


		//the edge shouldn't be null
		if(edge_array[iedge].p_edge == NULL)
			continue;

		//the edge should be a source edge
		//if(edge_array[iedge].is_src == wfalse)
			//continue;

		edge_t* edge_src = edge_array[iedge].p_edge;

		if(light->gen_type == light_jrpg)
		{
			//check if the edge y is higher than the light position
			if(edge_src->type == e_bottom)
				continue;

			/*if(edge_src->type == e_right && ((edge_src->start.x < light->position.x && edge_src->start.y > light->position.y) || (edge_src->end.y < light->position.y && edge_src->start.x > light->position.x) || (edge_src->start.x > light->position.x && edge_src->start.y < light->position.y ))  )
				continue;

			if(edge_src->type == e_left && ((edge_src->start.x > light->position.x && edge_src->start.y > light->position.y) || (edge_src->end.y < light->position.y && edge_src->start.x < light->position.x) || (edge_src->start.x < light->position.x && edge_src->start.y < light->position.y) ) )
				continue;*/
		}
		else
		{
			if((edge_src->type == e_bottom && light->position.y > edge_src->start.y) || (edge_src->type == e_top && light->position.y < edge_src->start.y) || (edge_src->type == e_left && light->position.x > edge_src->start.x) || (edge_src->type == e_right && light->position.x < edge_src->start.x) )
				continue;
		}

		Vector_t l1 = Vec(edge_src->start.x-light->position.x,edge_src->start.y - light->position.y);//vector from light position to the start of the edge
		Vector_t l2 = Vec(edge_src->end.x - light->position.x,edge_src->end.y - light->position.y);//vector from light position to the end of the edge

		if(l1.x * l1.x + l1.y * l1.y < light->radius * light->radius) //is the first vector length smaller than the light distance ?
		{
			Vector_t i = Intersect(pt1,pt2,Vec(0.0f,0.0f),l1);

			if((pt1.x > i.x && pt2.x < i.x) || (pt1.x < i.x && pt2.x > i.x))
					if((pt1.y > i.y && pt2.y < i.y) || (pt1.y < i.y && pt2.y > i.y))
						if((l1.y > 0 && i.y > 0) || (l1.y < 0 && i.y < 0))
						if((l1.x > 0 && i.x > 0) || (l1.x < 0 && i.x < 0))
						{
							light_add_volume(light,i,pt2,iedge,edge_array,edge_array_count,light_index,max_index,grid_dir,row_count,col_count,ignore_counter,max_ignore,ignore_step,current_row,current_col);
							 pt2 = i;
						}
		}

		if(l2.x * l2.x + l2.y * l2.y < light->radius * light->radius) //is the second vector length smaller than the light distance
		{
			Vector_t i = Intersect(pt1,pt2,Vec(0,0),l2);

			if((pt1.x > i.x && pt2.x < i.x) || (pt1.x < i.x && pt2.x > i.x))
			if((pt1.y > i.y && pt2.y < i.y) || (pt1.y < i.y && pt2.y > i.y))
				if((l2.y > 0 && i.y > 0) || (l2.y < 0 && i.y < 0))
				if((l2.x > 0 && i.x > 0) || (l2.x < 0 && i.x < 0))
				{
					light_add_volume(light,pt1,i,iedge,edge_array,edge_array_count,light_index,max_index,grid_dir,row_count,col_count,ignore_counter,max_ignore,ignore_step,current_row,current_col);
					pt1 = i;
				}
		}

		Vector_t m = Collision(l1, l2, Vec(0.0f,0.0f), pt1);
		Vector_t n = Collision(l1, l2, Vec(0.0f,0.0f), pt2);
		Vector_t o = Collision(l1, l2, pt1, pt2);

		if((m.x != 0 || m.y != 0) && (n.x != 0 || n.y != 0))
			pt1 = m, pt2 = n;
		else
		{
			if((m.x != 0 || m.y != 0) && (o.x != 0 || o.y != 0))
			{
				light_add_volume(light,m,o,iedge,edge_array,edge_array_count,light_index,max_index,grid_dir,row_count,col_count,ignore_counter,max_ignore,ignore_step,current_row,current_col);
				pt1 = o;
			}

			if((n.x != 0 || n.y != 0) && (o.x != 0 || o.y != 0))
			{
				light_add_volume(light,o,n,iedge,edge_array,edge_array_count,light_index,max_index,grid_dir,row_count,col_count,ignore_counter,max_ignore,ignore_step,current_row,current_col);
				pt2 = o;
			}
		}

	}


	if(light->num_light_volumes == light->max_light_volumes)
	{
		light->max_light_volumes += POOL_LIGHT_VOLUMES;

		light->light_volumes = (triangle_t*)wf_realloc(light->light_volumes,light->max_light_volumes * sizeof(triangle_t));
	}

	triangle_t* m_shape = &light->light_volumes[light->num_light_volumes++];
				
	m_shape->point_a = Vec(0.0f,0.0f);
	m_shape->point_b = pt1;
	m_shape->point_c = pt2;
}

static void _light_update_hardware_buffer(light_t* const light, render_manager_t* const render_mngr,const int buffer_number)
{


	//in case we have more light volumes than the default allocated value, resize the buffer
	if ((size_t)light->num_light_volumes > light->num_volumes_in_buffer[buffer_number]) {
		render_resize_updatebuffer(render_mngr, light->vertices[buffer_number], light->vertexBuffer, (light->num_light_volumes * 3 * 2) * sizeof(float), wtrue, wtrue);
		light->num_volumes_in_buffer[buffer_number] = light->num_light_volumes;
	}
	else {
		//update with no resize of our buffer
		render_unsync_updatebuffer(render_mngr, light->vertices[buffer_number], light->vertexBuffer, (light->num_light_volumes * 3 * 2) * sizeof(float));
	}

	light->bufferupdated[buffer_number] = wtrue;

	
}

void light_genlight_volumes(light_t* const light, render_manager_t* const render_mngr, edge_container_t* const edge_array,int32_t edge_array_count,int32_t tile_size,int32_t col_count,int32_t row_count, Vector_t ratio,Vector_t offset)
{
	//rebuild volumes is aspect ratio changed
	if (!Vec_isequal(light->baseratio, ratio)) {
		light->build_volumes = wtrue;
	}

	if(!light->build_volumes)
		return;

	//first, remove all existing volumes
	light->num_light_volumes = 0;

	//select the max tilemap index based on light position

	//int32_t light_tile_pos = 

	int32_t light_row =  (int32_t)floorf((light->position.y - offset.y) / (float)tile_size);
	int32_t light_col = (int32_t)floorf((light->position.x - offset.x) / (float)tile_size);

	light_row--;

	if(light_row < 0)
		light_row = 0;
	else if(light_row >= row_count)
		light_row = row_count - 1;

	if(light_col < 0)
		light_col = 0;
	else if(light_col >= col_count)
		light_col = light_col -1;

	int32_t tile_dist = (int32_t)ceilf((light->radius * 2.0f) / (float)tile_size);

	int32_t max_row = light_row + tile_dist;
	int32_t max_col = (light_col + tile_dist + 1);

	int32_t max_index = (max_row * col_count) + max_col;
	int32_t light_index = (light_row * col_count) + light_col;


	if(max_index >= edge_array_count)
		max_index = edge_array_count - 1;

	int32_t num_tri = 16;

	Vector_t grid_dir = Vec(0,-1);

	if(light->l_type == light_omni)
	{
		float buf = (WM_PI * 2)/(float)num_tri;

		for(int i = 0 ;i < num_tri;i++)
		{
			light_add_volume(light,Vec(light->radius * cosf((float)i * buf),light->radius *sinf((float)i * buf)),Vec(light->radius * cosf((float)(i+1) * buf),light->radius * sinf((float)(i+1) * buf)),(light_index * 4),edge_array,edge_array_count,light_index,max_index,grid_dir,row_count,col_count,0,0,0,light_row,light_col);
		}
	}
	else if(light->l_type == light_directional)
	{
		float buf = light->open_angle / (float)num_tri;

		float angle_diff = (light->open_angle * 0.5f);

		float start_angle = light->rotation_angle;

		if(angle_diff > start_angle)
		{
			angle_diff -= start_angle;

			start_angle = (WM_PI * 2) - angle_diff;
		}
		else
		{
			start_angle -= angle_diff;
		}

		for(int i = 0; i < num_tri;i++)
		{
			float step_angle = (i * buf);
			float step_angle_2 = ((float)(i + 1) * buf);

			light_add_volume(light,Vec(light->radius * cosf(start_angle + step_angle),light->radius * sinf(start_angle + step_angle)),Vec(light->radius * cosf(start_angle + step_angle_2),light->radius * sinf(start_angle + step_angle_2)),(light_index*4),edge_array,edge_array_count,light_index,max_index,grid_dir,row_count,col_count,0,0,0,light_row,light_col);
		}
	}
	else
	{
		logprint("Unrecognized light type %d",(int)light->l_type);
	}

	if (light->vertexBuffer != NULL)
	{
		wf_free(light->vertexBuffer);	
	}

	light->vertexBuffer = (float*)wf_calloc(2 * 3 * light->num_light_volumes, sizeof(float));

	int32_t ivert = 0;

	for (int32_t ilight = 0; ilight < light->num_light_volumes; ilight++)
	{
		light->vertexBuffer[ivert++] = (light->light_volumes[ilight].point_a.x * ratio.x);
		light->vertexBuffer[ivert++] = (light->light_volumes[ilight].point_a.y * ratio.y);

		light->vertexBuffer[ivert++] = (light->light_volumes[ilight].point_b.x * ratio.x);
		light->vertexBuffer[ivert++] = (light->light_volumes[ilight].point_b.y * ratio.y);

		light->vertexBuffer[ivert++] = (light->light_volumes[ilight].point_c.x * ratio.x);
		light->vertexBuffer[ivert++] = (light->light_volumes[ilight].point_c.y * ratio.y);
	}

	//initial GPU buffers allocation
	for (uint8_t ibuf = 0; ibuf < 3; ibuf++) {
		if (light->num_volumes_in_buffer[ibuf] == 0)
		{
			light->num_volumes_in_buffer[ibuf] = light->num_light_volumes;
		}

		if (light->vertices[ibuf] == render_get_emptybuffervalue())
			light->vertices[ibuf] = render_gen_buffer(render_mngr, light->vertexBuffer, (light->num_volumes_in_buffer[ibuf] * 3 * 2) * sizeof(float), wtrue, wtrue);

		light->bufferupdated[ibuf] = wfalse;
	}

	light->baseratio = ratio;
	light->build_volumes = wfalse;

}

void light_draw(light_t* const light,render_manager_t* const render_mngr,wbool mask_mode,Vector_t offset,Vector_t ratio,float ambient_alpha,int32_t width_render,int32_t height_render)
{
	if(!light->enabled)
		return;
	 

	render_push_matrix(render_mngr);


	Vector_t l_pos = Vec((light->position.x - offset.x) * ratio.x,(light->position.y - offset.y) * ratio.y);
	
	render_set_position(render_mngr,l_pos);

	if(mask_mode)
	{
		ColorA_t col = {0.0f,0.0f,0.0f,0.0f};
		render_set_color(render_mngr,col);
		render_setuniform_float(render_mngr,"render_value",0.0);
	}
	else
	{
		render_set_color(render_mngr,light->color);
		render_setuniform_float(render_mngr,"render_value",1.0);
	}


	float falloff[] = {light->constant,light->linear,light->quadratic};

	render_setuniform_float3(render_mngr,"falloff",falloff);
	
	float values[] = {l_pos.x / (float)width_render,l_pos.y / (float)height_render,0.075f};

	render_setuniform_float3(render_mngr,"lightpos",values);

	render_setuniform_float(render_mngr,"ambient_alpha",ambient_alpha);

	float resolution[] = {(float)width_render,(float)height_render};

	render_setuniform_float2(render_mngr,"resolution",resolution);

	float ratio_f[] = {(float)ratio.x,(float)ratio.y};
	render_setuniform_float2(render_mngr,"ratio",ratio_f);

	float light_info[] = {(light->l_type == light_omni)?1.0f:2.0f,light->open_angle,light->rotation_angle};
	render_setuniform_float3(render_mngr,"LightInfo",light_info);

	render_setuniform_float(render_mngr,"power_factor",light->power_factor);

	//render_start_draw_array(render_mngr,2,(3 * light->num_light_volumes), light->vertexBuffer,NULL);

	//render_draw_array(render_mngr,0,(3 * light->num_light_volumes));

	//render_end_draw_array(render_mngr,wfalse);

	const int buffer_number = render_mngr->frame_number % 3;

	if (!light->bufferupdated[buffer_number]) {

		if (light->buffersync[buffer_number] != NULL) {
			if (!render_fencesyncisready(render_mngr, light->buffersync[buffer_number])) {
				logprint("Error with buffer transfers, synchronisation occurred...");
				return;
			}

			render_removefencesync(light->buffersync[buffer_number]);
			light->buffersync[buffer_number] = NULL;

		}

		_light_update_hardware_buffer(light, render_mngr, buffer_number);
	}
	else if (light->buffersync[buffer_number] != NULL) {
		render_removefencesync(light->buffersync[buffer_number]);
		light->buffersync[buffer_number] = NULL;
	}

	render_start_draw_buffer(render_mngr, 0, light->vertices[buffer_number], 0, 0, 2);

	render_draw_buffer(render_mngr, 0, (light->num_light_volumes * 3));

	render_end_draw_buffer(render_mngr, wfalse, wfalse);

	light->buffersync[buffer_number] = render_addfencesync(render_mngr);

	#if defined(_DEBUG)
		render_set_color(render_mngr,red_color);

		render_start_draw_array(render_mngr,2,(3 * light->num_light_volumes), light->vertexBuffer,NULL);
		render_draw_lineloop(render_mngr,0,(3 * light->num_light_volumes));
		render_end_draw_array(render_mngr,wfalse);
	#endif

	render_pop_matrix(render_mngr);

	render_reset_color(render_mngr);

}

void light_setposition(light_t* const light,Vector_t new_pos)
{
	light->position = new_pos;
	light->build_volumes = wtrue;
}

void light_setrotationangle(light_t* const light,float deg_angle)
{
	light->rotation_angle = deg2rad(deg_angle);
	light->build_volumes = wtrue;
}

void light_setopenangle(light_t* const light,float deg_angle)
{
	light->open_angle = deg2rad(deg_angle);
	light->build_volumes = wtrue;
}

void light_setattenuation(light_t* const light,float constant,float linear,float quadratic)
{
	light->constant = constant;
	light->linear = linear;
	light->quadratic = quadratic;
}

void light_g_create(light_t* const light)
{
	light->build_volumes = wtrue;

}

void light_g_clean(light_t* const light)
{
	for (uint8_t ibuf = 0; ibuf < 3; ibuf++) {
		if (light->vertices[ibuf] != render_get_emptybuffervalue())
			render_delete_buffer(light->vertices[ibuf]);

		light->vertices[ibuf] = render_get_emptybuffervalue();

		if(light->buffersync[ibuf] != NULL)
			render_removefencesync(light->buffersync[ibuf]);

		light->buffersync[ibuf] = NULL;

		light->num_volumes_in_buffer[ibuf] = 0;
		light->bufferupdated[ibuf] = wtrue;
	}
}

void light_free(light_t* const light)
{
	if(light->shadows_volumes != NULL)
	{
		light->num_shadow_volumes = 0;
		light->max_shadow_volumes = POOL_SHADOWS_VOLUMES;

		wf_free(light->shadows_volumes);
		light->shadows_volumes = NULL;
	}

	light->build_volumes = wtrue;

	if(light->vertexBuffer != NULL)
		wf_free(light->vertexBuffer);

	light->vertexBuffer = NULL;

	light_g_clean(light);
}