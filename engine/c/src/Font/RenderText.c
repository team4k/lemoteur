// This is a OpenGL ES 1.0 dynamic font rendering system. It loads actual font
// files, generates a font map (texture) from them, and allows rendering of
// text strings.
//
// NOTE: the rendering portions of this class uses a sprite batcher in order
// provide decent speed rendering. Also, rendering assumes a BOTTOM-LEFT
// origin, and the (x,y) positions are relative to that, as well as the
// bottom-left of the string to render.
//dlan : ported from java to c

#include <stdlib.h>
#include <inttypes.h>
#include <math.h>
#include <string.h>
#include <wchar.h>

#include <Engine.h>

#include <Render.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/geom.h>
#include <Base/Color.h>

#include <Render/render_manager.h>

#include <Utils/Hash.h>

#include <Graphics/TextureLoader.h>

#include <GameObjects/Characters.pb-c.h>
#include <Graphics/Animation.h>
#include <Graphics/Sprite.h>

#include <Graphics/TextureRegion.h>
#include <Graphics/SpriteBatch.h>


#include <Graphics/FontLoader.h>

#include <Font/RenderText.h>

#include <Debug/Logprint.h>

#include <Utils/random.h>

int Render_Text_Init(Render_Text_t* const text,int32_t batch_size,render_manager_t* const render_mngr)
{
    SpriteBatch_Init(&text->batch,batch_size,render_mngr);// Create Sprite Batch (with Defined Size)
	text->message = NULL;
	text->fixed_size = wfalse;
	return 1;
}

void Render_Text_setfixedsize(Render_Text_t* const text,wbool fix_size)
{
	text->fixed_size = fix_size;

	if(!fix_size)
		text->batch.scale_x = text->batch.scale_y = 1.0f;
}

void Render_Text_setscale(Render_Text_t* text,float scale_x,float scale_y)
{
	if(text->fixed_size)
	{
		logprint("Can't set scale for a text with a fixed size, set fixed size to false first");
		return;
	}

	text->batch.scale_x = scale_x;
	text->batch.scale_y = scale_y;
}

void Render_Text_Setinfo(Render_Text_t* text,const font_info* info)
{
	text->info = info;
}

void Render_Text_Free(Render_Text_t* text)
{
	SpriteBatch_Free(&text->batch);

	if(text->message != NULL)
	{
		wf_free(text->message);
		text->message = NULL;
	}
}


//--Begin/End Text Drawing--//
   // D: call these methods before/after (respectively all draw() calls using a text instance
   //    NOTE: color is set on a per-batch basis, and fonts should be 8-bit alpha only!!!
   // A: red, green, blue - RGB values for font (default = 1.0)
   //    alpha - optional alpha value for font (default = 1.0)
void Render_Text_begin(Render_Text_t* text,float red,float green, float blue, float alpha,render_manager_t* const render_mngr)
{
	ColorA_t color = {red,green,blue,alpha};
	render_set_color(render_mngr,color);
}

void Render_Text_end(Render_Text_t* text,render_manager_t* const render_mngr)
{
	if(text->fixed_size)
		text->batch.scale_x = text->batch.scale_y = render_mngr->zoom_factor;

	SpriteBatch_draw(&text->batch,render_mngr,text->info->texture->texture_pointer);

	render_reset_color(render_mngr);
}

void Render_Text_Draw(Render_Text_t* const text, const wchar_t* message, float x_orig, float y_orig, uint8_t fontsize, render_manager_t* const render_mngr)
{
	Render_Text_Draw2(text, message, x_orig, y_orig, fontsize, render_mngr, 0, 0,NULL,NULL);
}

void Render_Text_Draw_withcolornsize(Render_Text_t* const text, const wchar_t* message, float x_orig, float y_orig, uint8_t fontsize, render_manager_t* const render_mngr,const ColorA_t* colors, const float* char_factor)
{
	Render_Text_Draw2(text, message, x_orig, y_orig, fontsize, render_mngr, 0, 0,colors,char_factor);
}

void Render_Text_Draw2(Render_Text_t* const text,const wchar_t* message,float x_orig, float y_orig,uint8_t fontsize,render_manager_t* const render_mngr, uint32_t hidechance, int32_t charreplacer, const ColorA_t* colors, const float* char_factor)
{
	text->batch.position.x = x_orig;
	text->batch.position.y = y_orig;       

	if(text->message == NULL || a_wcscmp(text->message , message) != 0)
	{

		size_t len = a_wcslen(message); // Get String Length                   
		int i = 0;
		int char_index = 0;
		float start_x = 0;
		float x = 0;
		float y = 0;

		wbool new_sprite = wfalse;

		int32_t sprite_cnt = 0;

		float cursorheight = (text->info->lineHeight * (fontsize / text->info->emSize) + (text->info->distanceRange >> 1));
		float cursorwidth = text->info->fontAscent * (fontsize / text->info->emSize) + (text->info->distanceRange >> 1);

		const wchar_t* orig_ptr = message;


		//cleanup remaining buffer data
		//SpriteBatch_cleanupbuffer(&text->batch, 0);

		for ( i = 0; i < len; i++ )  
		{              // FOR Each Character in String
			wchar_t cChar = *message;

			if(text->message != NULL && !new_sprite)
			{
				if (text->message[i] == L'\0')
				{
					new_sprite = wtrue;
					text->batch.numSprites = sprite_cnt;
				}
			}

			if((int32_t)cChar == 10)//handle new line
			{
				y -= cursorheight;
				x = start_x;
				message++;
			}
			else
			{
				char_index = (int32_t)(((int32_t)cChar) - text->info->char_start);  // Calculate Character Index (Offset by First Char in Font)
				message++;

				//hidechance
				if (hidechance != 0) {

					uint32_t dice = wf_randrange(1, hidechance);

					if (dice == 1) {
						char_index = charreplacer;
					}
				}

				if ( char_index < 0 || char_index >= (int32_t)text->info->char_cnt )                // IF Character Not In Font
				{
					logprint("Error using char %c not in font, number : %d \n",cChar,char_index);
						continue;
				}

				if (char_factor != NULL)
				{
					cursorheight = (text->info->lineHeight * ((fontsize * char_factor[i]) / text->info->emSize) + (text->info->distanceRange >> 1));
					cursorwidth = text->info->fontAscent * ((fontsize * char_factor[i]) / text->info->emSize) + (text->info->distanceRange >> 1);
				}
				
				float advance = 0.0f;
				float charx,chary,charwidth,charheight = 0.0f;

				Fontglyph_t* glyph = &text->info->glyphs[char_index];

				TextureRegion_t region;
					
				advance = glyph->advance;

				if (!glyph->emptychar) {
					charx = (glyph->planeleft * cursorwidth);
					chary = (glyph->planetop * cursorheight);
					charwidth = (glyph->planeright * cursorheight) - charx;
					charheight = (glyph->planetop * cursorheight) - (glyph->planebottom * cursorheight);


					TextureRegion_Init2(&region, glyph->u1, glyph->v1, glyph->u2, glyph->v2);

					ColorA_t charcolor = render_mngr->current_color;

					if (colors != NULL) {
						charcolor = colors[i];
					}

					if (text->message == NULL || new_sprite)
						SpriteBatch_AddSprite(&text->batch, x + charx, y + chary, charwidth, charheight, &region, charcolor, render_mngr); // add the character sprite
					else
						SpriteBatch_updateSprite(&text->batch, sprite_cnt, x + charx, y + chary, charwidth, charheight, &region, charcolor, render_mngr); // update the character sprite

					sprite_cnt++;
				}
				else {
					charx = 0;

					if (char_factor == NULL)
					{
						charwidth = (advance * (fontsize / text->info->emSize) + (text->info->distanceRange >> 1));
					}
					else {
						charwidth = (advance * ((fontsize * char_factor[i]) / text->info->emSize) + (text->info->distanceRange >> 1));
					}
				}

				x += charx + charwidth;
			}
		}


		size_t copy_size = (a_wcslen(orig_ptr) + 1);

		if(text->message == NULL)
		{
			text->message = (wchar_t*)wf_malloc(copy_size *  sizeof(wchar_t));
			//update whole buffer to remove any existing text data
			SpriteBatch_scheduleupdatebuffer(&text->batch, BUFFER_UPDATE_VERTICES | BUFFER_UPDATE_TEXCOORDS | BUFFER_UPDATE_COLORS);
		}
		else
		{

			text->message = (wchar_t*)wf_realloc(text->message,copy_size *  sizeof(wchar_t));

			text->batch.numSprites = sprite_cnt;          

			//update whole buffer to remove any existing text data
			SpriteBatch_scheduleupdatebuffer(&text->batch, BUFFER_UPDATE_VERTICES | BUFFER_UPDATE_TEXCOORDS | BUFFER_UPDATE_COLORS);
		}

		a_wcscpy(text->message,copy_size,orig_ptr);

	}
}


void Render_Text_Draw_centered(Render_Text_t* text,const wchar_t* message,render_manager_t* const render_mngr,render_text_positions position,SHADERPROGRAM_ID text_prog,ColorA_t text_color,Vector_t offset,uint8_t fontsize)
{
	render_use_program(render_mngr,text_prog);
	render_push_matrix(render_mngr);

	float msg_len = Render_Text_strlen(text,message);

	int scroll_x = (int)((-render_mngr->scroll_vector.x) * render_mngr->zoom_factor);
	int scroll_y = (int)((-render_mngr->scroll_vector.y) * render_mngr->zoom_factor);

	float text_x = (float)scroll_x;
	float text_y = (float)scroll_y;

	float char_height =  Render_Text_charheight(text);

	float real_screen_width =  render_mngr->screen_info.width * render_mngr->zoom_factor;
	float real_screen_height = render_mngr->screen_info.height * render_mngr->zoom_factor;

	switch(position)
	{
		case TP_TOPLEFT:
			text_y += real_screen_height;
		break;

		case TP_TOPCENTER:
			text_x += (real_screen_width * 0.5f) - (msg_len * 0.5f);
			text_y += real_screen_height;
		break;

		case TP_TOPRIGHT:
			text_x += (real_screen_width) - (msg_len);
			text_y += real_screen_height;
		break;

		case TP_LEFTCENTER:
			text_y += (real_screen_height * 0.5f) + (char_height * 0.5f);
		break;

		case TP_CENTER:
			text_x += (real_screen_width * 0.5f) - (msg_len * 0.5f);
			text_y += (real_screen_height * 0.5f) + (char_height * 0.5f);
		break;

		case TP_RIGHTCENTER:
			text_x += (real_screen_width) - (msg_len);
			text_y += (real_screen_height * 0.5f) + (char_height * 0.5f);
		break;

		case TP_BOTTOMLEFT:
			text_y += char_height;
		break;

		case TP_BOTTOMCENTER:
			text_x += (real_screen_width * 0.5f) - (msg_len * 0.5f);
			text_y += char_height;
		break;

		case TP_BOTTOMRIGHT:
			text_x += (real_screen_width) - (msg_len);
			text_y += char_height;
		break;
	}

	text_x += (float)offset.x;
	text_y += (float)offset.y;

	Render_Text_begin(text,text_color.r,text_color.g,text_color.b,text_color.a,render_mngr);
	Render_Text_Draw(text,message,truncf(text_x),truncf(text_y),fontsize,render_mngr);
	Render_Text_end(text,render_mngr);
	render_pop_matrix(render_mngr);
}


float Render_Text_strlen(Render_Text_t* text,const wchar_t* message)
{
	  float len = 0.0f;                               // Working Length
	  int strLen = a_wcslen(message);                 // Get String Length (Characters)
	  int i = 0;
	  int char_index = 0;
	  float maxlen = 0.0f;
	  
	  for ( i = 0; i < strLen; i++ )   // For Each Character in String (Except Last
	  {          
		 wchar_t cChar = *message++;

		 if((int32_t)cChar == 10)//handle new line
		 {
			 if(len > maxlen)
				 maxlen = len;

			 len = 0.0f;
			 continue;
		 }

		 float width = text->info->glyphs[char_index].atlasright - text->info->glyphs[char_index].atlasleft;

		 char_index = (int32_t)cChar - text->info->char_start;  // Calculate Character Index (Offset by First Char in Font)
		 len += ( width );           // Add Scaled Character Width to Total Length
	  }
	  
	  if(len > maxlen)
			maxlen = len;

	  maxlen += ( strLen > 1 ? ( ( strLen - 1 ) )  : 0 );  // Add Space Length
	  return maxlen * text->batch.scale_x;  
}




float Render_Text_charheight(Render_Text_t* text)
{
	return ( 10 * text->batch.scale_y);                 // Return Scaled Character Height
}

float Render_Text_charwidth(Render_Text_t* text)
{
	return (10 * text->batch.scale_x);
}



void Render_Text_drawtexture(Render_Text_t* text,float x,float y,render_manager_t* const render_mngr)
{
	//SpriteBatch_begin(&text->batch, text->info->texture_pointer,render_mngr);
    //SpriteBatch_drawSprite(&text->batch, x, y,  (float)text->info->textureSize, (float)text->info->textureSize,&text->info->textureRgn,render_mngr);
}




