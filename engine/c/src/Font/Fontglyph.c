#include <inttypes.h>
#include <Base/types.h>
#include <Font/Fontglyph.h>

void Fontglyph_Init(Fontglyph_t* const fontglyph, const float u1, const float v1, const float u2, const float v2, const unsigned long char_code, 
	const float atlasleft, const float atlasbottom, const float atlasright, const float atlastop, 
	const float planeleft, const float planebottom, const float planeright, const float planetop,
	const float advance)
{
	fontglyph->u1 = u1;
	fontglyph->v1 = v1;
	fontglyph->u2 = u2;
	fontglyph->v2 = v2;
	fontglyph->char_code = char_code;
	
	fontglyph->atlasleft = atlasleft;
	fontglyph->atlasbottom = atlasbottom;
	fontglyph->atlasright = atlasright;
	fontglyph->atlastop = atlastop;

	fontglyph->planeleft = planeleft;
	fontglyph->planebottom = planebottom;
	fontglyph->planeright = planeright;
	fontglyph->planetop = planetop;

	fontglyph->advance = advance;
	fontglyph->emptychar = wfalse;
}

void Fontglyph_Init_empty(Fontglyph_t* const fontglyph, const unsigned long char_code,const float advance)
{
	fontglyph->u1 = 0.0f;
	fontglyph->v1 = 0.0f;
	fontglyph->u2 = 0.0f;
	fontglyph->v2 = 0.0f;
	fontglyph->char_code = char_code;

	fontglyph->atlasleft = 0.0f;
	fontglyph->atlasbottom = 0.0f;
	fontglyph->atlasright = 0.0f;
	fontglyph->atlastop = 0.0f;

	fontglyph->planeleft = 0.0f;
	fontglyph->planebottom = 0.0f;
	fontglyph->planeright = 0.0f;
	fontglyph->planetop = 0.0f;

	fontglyph->advance = advance;
	fontglyph->emptychar = wtrue;
}

