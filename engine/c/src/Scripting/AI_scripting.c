//TODO: lua_is* function are misleading and don't check variable type but if a variable can be converted to a type, replace
//all lua_is* function with lua_type(L,stack_pos) == <LUA_TYPE> to check the type

#ifdef _MSC_VER
    extern "C" {
#endif
        
#include "lauxlib.h"
#include "lualib.h"
#include "lua.h"
        
#ifdef _MSC_VER
 }
#endif

#include <Base/functions.h>
#include <GameObjects/level_base.h>

#include <Pathfinding/pathfind_worker.h>
#include <Scripting/ScriptLoader.h>
#include <Scripting/ScriptUtils.h>


static int lua_pathfind_setalgo(lua_State* L)
{
	if (!lua_isinteger(L, 1)) {
		luaL_error(L, "please set a valid pathfinding algo (0 = astar_jps, 1 = lombric)");
		return 1;
	}

	lua_Integer algonum = lua_tointeger(L, 1);
	pathfind_algo algo = (pathfind_algo)algonum;

	pathfind_worker_t* worker = (pathfind_worker_t*)lua_touserdata(L, -1);
	pathfind_setalgo(worker, algo);

	return 1;
}


static int lua_pathfind_search(lua_State* L)
{
	const char* request_id = lua_tostring(L,1);
	lua_Integer start_tile = lua_tointeger(L,2);
	lua_Integer goal_tile = lua_tointeger(L,3);
	lua_Integer flags = 0;
	lua_Integer algo = 0;
	wbool hasalgo = wfalse;

	if (lua_isinteger(L, 4)) {
		flags = lua_tointeger(L, 4);
	}

	if (lua_isinteger(L, 5)) {
		algo = lua_tointeger(L, 5);
		hasalgo = wtrue;
	}

	lua_getglobal(L,"mngr");

	lua_pushstring(L,"pathfind_worker");
	lua_gettable(L,-2);

	pathfind_worker_t* worker  = (pathfind_worker_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	int recycle = hashmap_haskey(&worker->request_array,request_id);

	if (!hasalgo)
	{
		pathfind_dorequest(worker, request_id, (int32_t)start_tile, (int32_t)goal_tile, (wbool)recycle, (int32_t)flags);
	}
	else {
		pathfind_dorequest_withalgo(worker, request_id, (int32_t)start_tile, (int32_t)goal_tile, (wbool)recycle, (int32_t)flags,(pathfind_algo)algo);
	}
	

	return 1;
}

static int lua_pathfind_exist(lua_State* L)
{
	const char* request_id = lua_tostring(L, 1);

	lua_getglobal(L, "mngr");

	lua_pushstring(L, "pathfind_worker");
	lua_gettable(L, -2);

	pathfind_worker_t* worker = (pathfind_worker_t*)lua_touserdata(L, -1);

	lua_pop(L, 1);

	lua_pushboolean(L, hashmap_haskey(&worker->request_array, request_id));

	return 1;
}

static int lua_pathfindusable(lua_State* L)
{
	const char* request_id = lua_tostring(L, 1);

	lua_getglobal(L, "mngr");

	lua_pushstring(L, "pathfind_worker");
	lua_gettable(L, -2);

	pathfind_worker_t* worker = (pathfind_worker_t*)lua_touserdata(L, -1);

	lua_pop(L, 1);

	pathfind_result_t* result = (pathfind_result_t*)hashmap_getvalue(&worker->request_array, request_id);

	lua_pushboolean(L, !result->recycled);

	return 1;
}

static int lua_pathfind_hasfound(lua_State* L)
{
	const char* request_id = lua_tostring(L,1);

	lua_getglobal(L,"mngr");

	lua_pushstring(L,"pathfind_worker");
	lua_gettable(L,-2);

	pathfind_worker_t* worker  = (pathfind_worker_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	if(hashmap_haskey(&worker->request_array,request_id))
	{
		pathfind_result_t* result = (pathfind_result_t*)hashmap_getvalue(&worker->request_array,request_id);
		lua_pushboolean(L,(int)result->path_computed);
	}
	else
	{
		luaL_error(L,"no pathfind request with the id %s exist! use pathfind_search first",request_id);
		lua_pushboolean(L,0);
	}

	return 1;
}

static int lua_pathfind_getresult(lua_State* L)
{
	const char* request_id = lua_tostring(L,1);
	wbool recycle = (wbool)lua_toboolean(L, 2);

	LEVEL_TYPE type = L_STATIC;

	void* lvlbase = lua_lvlbase_getmanager(L, 2, &type);

	WorldRect_t dim = lvlbase_getdimension(lvlbase, type);

	int32_t tile_size = lvlbase_gettilesize(lvlbase, type);

	lua_getglobal(L,"mngr");

	lua_pushstring(L,"pathfind_worker");
	lua_gettable(L,-2);

	pathfind_worker_t* worker  = (pathfind_worker_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	int32_t rowdiff = 0;
	int32_t coldiff = 0;

	if (dim.loadeddimension.position.x != vectorzero.x || dim.loadeddimension.position.y != vectorzero.y)
	{
		rowdiff = dim.loadeddimension.position.y / tile_size;
		coldiff = dim.loadeddimension.position.x / tile_size;
	}


	if(hashmap_haskey(&worker->request_array,request_id))
	{
		pathfind_result_t* result = (pathfind_result_t*)hashmap_getvalue(&worker->request_array,request_id);

		if (result->recycled)
		{
			luaL_error(L, "pathfind request %s is recycled ! can't access path result", request_id);
			lua_pushnil(L);
			return 1;
		}
		
		if(result->path_computed && result->path_result != NULL)
		{
			lua_newtable(L);

			lua_newtable(L);

			for (int32_t i = 0; i < result->path_result_size; i++)
			{
				//lua indexing start at one
				lua_pushnumber(L, i + 1);
				int32_t localrow = (int32_t)wfloor(result->path_result[i] / (dim.loadeddimension.width / tile_size));
				int32_t localcol = (int32_t)result->path_result[i] % (dim.loadeddimension.width / tile_size);
				int32_t worldindex = (localrow + rowdiff) * (dim.worldwidth / tile_size) + (localcol + coldiff);
				lua_pushnumber(L, worldindex);
				lua_settable(L, -3);
			}

			lua_setfield(L, -2, "path");
			lua_pushinteger(L, result->flagsresult);
			lua_setfield(L, -2, "flagsresult");
		}
		else
		{
			lua_pushnil(L);
		}

		//keep request in memory
		if (recycle)
		{
			result->recycled = wtrue;
		}
		
	}
	else
	{
		luaL_error(L,"no pathfind request with the id %s exist! use pathfind_search first",request_id);
		lua_pushnil(L);
	}

	return 1;
}

static int lua_pathfind_freerequest(lua_State* L)
{
	const char* request_id = lua_tostring(L,1);

	lua_getglobal(L,"mngr");

	lua_pushstring(L,"pathfind_worker");
	lua_gettable(L,-2);

	pathfind_worker_t* worker  = (pathfind_worker_t*)lua_touserdata(L,-1);

	lua_pop(L,1);


	if(hashmap_haskey(&worker->request_array,request_id))
		pathfind_freerequest(worker,request_id);
	else
		luaL_error(L,"no pathfind request with the id %s, did you try to freed it twice ?",request_id);

	return 1;
}

void register_ai_functions(lua_State *L)
{
	//pathfinding functions
	lua_pushcfunction(L, lua_pathfind_setalgo);
	lua_setglobal(L, "pathfind_setalgo");

	lua_pushcfunction(L,lua_pathfind_search);
	lua_setglobal(L,"pathfind_search");

	lua_pushcfunction(L,lua_pathfind_hasfound);
	lua_setglobal(L,"pathfind_hasfound");

	lua_pushcfunction(L, lua_pathfind_exist);
	lua_setglobal(L, "pathfind_exist");

	lua_pushcfunction(L, lua_pathfindusable);
	lua_setglobal(L, "pathfind_usable");

	lua_pushcfunction(L,lua_pathfind_getresult);
	lua_setglobal(L,"pathfind_getresult");

	lua_pushcfunction(L,lua_pathfind_freerequest);
	lua_setglobal(L,"pathfind_freerequest");

}
