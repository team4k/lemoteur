#ifdef _MSC_VER
extern "C" {
#endif
#include <lua.h>

#ifdef _MSC_VER
}
#endif

#if defined(EMSCRIPTEN)
	#include <emscripten.h>
#endif


#include <Render.h>
#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <time.h>
#include <string.h>
#include <limits.h>
#include <math.h>
#include <sqlite3.h>

#include <Engine.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>
#include <Base/geom.h>

#include <Render/render_manager.h>

#include <Graphics/TextureLoader.h>
#include <Resx/shader_loader.h>

#include <Utils/Hash.h>
#include <Utils/hashmap.h>

#include <Threading/thread_func.h>
#include <Pathfinding/pathfind_worker.h>

#include <GameObjects/Characters.pb-c.h>
#include <Graphics/TextureRegion.h>

#include "GameObjects/Level.pb-c.h"

#ifndef EMSCRIPTEN
	#include <portaudio.h>
#endif

#include <Sound/Sound.h>

#include <Graphics/FontLoader.h>

#include <Resx/Resources.h>
#include <Resx/localization.h>
#include <GameMngr/save_mngr.h>

#include "Debug/Logprint.h"

#include <Sound/snd_mngr.h>

#include <chipmunk/chipmunk.h>
#include <Physics/Physics.h>
#include <Physics/physics_utils.h>

#include <Config/config.h>
#include <Config/shader_config.h>
#include <GameMngr/input_mngr.h>
#include <game_functions.h>
#include <GameMngr/module_mngr.h>

#include <Scripting/ScriptLoader.h>

static int lua_input_getkeyname(lua_State* L)
{
	int32_t key_val = (int32_t)lua_tointeger(L, 1);

	input_mngr_t* inputmngr;

	lua_getglobal(L, "mngr");

	lua_pushstring(L, "input_mngr");
	lua_gettable(L, -2);

	inputmngr = (input_mngr_t*)lua_touserdata(L, -1);

	lua_pop(L, 1);

	const char* keyname = input_mngr_getkeyname(inputmngr, key_val);

	if (keyname == NULL) {
		lua_pushnil(L);
	}
	else {
		lua_pushstring(L, keyname);
	}

	return 1;
}

static int lua_input_getbuttonname(lua_State* L)
{
	int16_t button_val = (int16_t)lua_tointeger(L, 1);
	gamepad_scheme scheme = (gamepad_scheme)lua_tointeger(L, 2);

	input_mngr_t* inputmngr;

	lua_getglobal(L, "mngr");

	lua_pushstring(L, "input_mngr");
	lua_gettable(L, -2);

	inputmngr = (input_mngr_t*)lua_touserdata(L, -1);

	lua_pop(L, 1);

	const char* buttonname = input_mngr_getbuttonname(inputmngr, button_val, scheme);

	if (buttonname == NULL) {
		lua_pushnil(L);
	}
	else {
		lua_pushstring(L, buttonname);
	}


	return 1;
}

static int lua_input_getaxisname(lua_State* L)
{
	int16_t axis_val = (int16_t)lua_tointeger(L, 1);
	gamepad_scheme scheme = (gamepad_scheme)lua_tointeger(L, 2);

	input_mngr_t* inputmngr;

	lua_getglobal(L, "mngr");

	lua_pushstring(L, "input_mngr");
	lua_gettable(L, -2);

	inputmngr = (input_mngr_t*)lua_touserdata(L, -1);

	lua_pop(L, 1);

	const char* axisname = input_mngr_getaxisname(inputmngr, axis_val, scheme);

	if (axisname == NULL) {
		lua_pushnil(L);
	}
	else {
		lua_pushstring(L, axisname);
	}

	return 1;
}

void register_input_functions(lua_State *L)
{
	//input functions
	lua_pushcfunction(L, lua_input_getkeyname);
	lua_setglobal(L, "input_getkeyname");

	lua_pushcfunction(L, lua_input_getbuttonname);
	lua_setglobal(L, "input_getbuttonname");

	lua_pushcfunction(L, lua_input_getaxisname);
	lua_setglobal(L, "input_getaxisname");
}