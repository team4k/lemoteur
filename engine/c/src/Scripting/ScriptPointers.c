#include <stdarg.h>
#include <string.h>
#include <inttypes.h>
#include <Base/types.h>
#include <Engine.h>
#include <Debug/Logprint.h>

#include <Scripting/ScriptPointers.h>

typedef struct
{
	intptr_t pointer;
	script_type type;
} script_pointer;

static script_pointer script_allowed_pointers[MAX_ALLOWED_SCRIPT_POINTERS] = { 0 };
static uint32_t low_id_script_pointer = 1;

uint32_t script_registerpointer(intptr_t pointervalue, script_type type)
{
	for (uint32_t ipointer = low_id_script_pointer; ipointer < MAX_ALLOWED_SCRIPT_POINTERS; ipointer++) {
		if (script_allowed_pointers[ipointer].pointer == 0) {
			script_allowed_pointers[ipointer].pointer = pointervalue;
			script_allowed_pointers[ipointer].type = type;

			low_id_script_pointer = ipointer + 1;

			return ipointer;
		}
	}

	logprint("max number %d of allowed pointer in script reached !!!", MAX_ALLOWED_SCRIPT_POINTERS);

	return 0;
}

void script_unregisterpointer(uint32_t pointerid)
{
	if (pointerid >= MAX_ALLOWED_SCRIPT_POINTERS || pointerid == 0) {
		logprint("invalid pointer id, can't be zero or superior or equal to the max number of allowed pointer !");
		return;
	}

	if (script_allowed_pointers[pointerid].pointer == 0) {
		logprint("invalid pointer id, no pointer exist for this id !");
		return;
	}


	script_allowed_pointers[pointerid].pointer = 0;
	script_allowed_pointers[pointerid].type = UNDEFINED;

	if (pointerid < low_id_script_pointer)
		low_id_script_pointer = pointerid;
}

intptr_t script_getpointerfromid(uint32_t pointerid, script_type type)
{
	if (pointerid >= MAX_ALLOWED_SCRIPT_POINTERS || pointerid == 0) {
		logprint("invalid pointer id, can't be zero or superior or equal to the max number of allowed pointer !");
		return 0;
	}

	if (script_allowed_pointers[pointerid].pointer == 0) {
		logprint("invalid pointer id, no pointer exist for this id !");
		return 0;
	}

	if (script_allowed_pointers[pointerid].type != type) {
		logprint("invalid pointer id, the supplied pointer type is not valid / does not match !");
		return 0;
	}


	return script_allowed_pointers[pointerid].pointer;
}