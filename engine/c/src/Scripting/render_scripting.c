//TODO: lua_is* function are misleading and don't check variable type but if a variable can be converted to a type, replace
//all lua_is* function with lua_type(L,stack_pos) == <LUA_TYPE> to check the type


#ifdef _MSC_VER
    extern "C" {
#endif
        
#include "lauxlib.h"
#include "lualib.h"
#include "lua.h"
        
#ifdef _MSC_VER
 }
#endif

#if defined(_WIN32)
    #include <windows.h>

	#if defined(WINAPI_FAMILY) && (WINAPI_FAMILY == WINAPI_FAMILY_PHONE_APP)
		#include <thread>
	#endif
#else
	#ifndef __USE_BSD
        	#define __USE_BSD
	#endif
        #include <unistd.h>
        #include <dlfcn.h>
		#include <wchar.h>
		#include <pthread.h>
#endif

#include <stdarg.h>
#include <string.h>
#include <inttypes.h>
#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include <math.h>
#include <sqlite3.h>

#include <Engine.h>

#include <Render.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>
#include <Base/geom.h>

#include <Render/render_manager.h>

#include <GameObjects/Shaders.pb-c.h>
#include <Graphics/TextureLoader.h>
#include <Resx/shader_loader.h>

#include <Utils/Hash.h>
#include <Utils/hashmap.h>

#include <GameObjects/Characters.pb-c.h>
#include <Graphics/Animation.h>
#include <Graphics/Sprite.h>
#include <Graphics/Mesh.h>

#include <Graphics/Light.h>
#include <Graphics/LightManager.h>

#include <Graphics/TextureRegion.h>
#include <Graphics/SpriteBatch.h>
#include <Debug/Logprint.h>


#include <Debug/geomdebug.h>

#include <GameObjects/Level.pb-c.h>
#include <Collisions/Collisions.h>

#include <chipmunk/chipmunk_private.h>

#include <Physics/Physics.h>

#include <Resx/localization.h>


#include <Graphics/FontLoader.h>



#include <GameObjects/entity.h>
#include <GameObjects/Trigger.h>
#include <Font/RenderText.h>
#include <GameObjects/text_entity.h>


#ifdef EMSCRIPTEN
	#include <SDL2/SDL_audio.h>
	#include <emscripten.h>
#else
	#include <portaudio.h>
#endif

#include <Threading/thread_func.h>
#include <Pathfinding/pathfind_worker.h>

#include <Sound/Sound.h>
#include <Sound/snd_mngr.h>

#include <Resx/Resources.h>

#include <Graphics/TilemapLayer.h>
#include <GameObjects/Tilemap.h>

#include <GameObjects/entity_group.h>
#include <Graphics/Effects/particle_object.h>
#include <GameObjects/particles_entity.h>

#include <Config/config.h>
#include <GameMngr/input_mngr.h>
#include <game_functions.h>
#include <GameMngr/module_mngr.h>
#include <GameObjects/map.h>
#include <GameObjects/Collider.h>
#include <GameObjects/waypoint.h>
#include <GameObjects/Levelobject.h>
#include <GameObjects/level_base.h>


#include <Graphics/compound_sprite.h>


#include <Scripting/ScriptUtils.h>
#include <Scripting/ScriptLuaUtils.h>
#include <Scripting/ScriptLoader.h>
#include <Scripting/ScriptPointers.h>


static int camera_focuson(lua_State* L)
{
	const char* position_focus = lua_tostring(L,1);

	lua_getglobal(L,"mngr");

	level_base_t* lvl_mngr;

	lua_pushstring(L,"level_mngr");
	lua_gettable(L,-2);

	lvl_mngr = (level_base_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	lua_getglobal(L,"mngr");

	render_manager_t* render_mngr;

	lua_pushstring(L,"render_mngr");
	lua_gettable(L,-2);

	render_mngr = (render_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	if(strcmp(position_focus,"centermap") == 0) //center view in the middle of tilemap
	{
		if (lvl_mngr->current_level_type == L_STREAM) {
			luaL_error(L, "camera_focuson with centermap can't be used on a streaming context !");
			return 1;
		}

		Levelobject_t* lvl_obj = (Levelobject_t*)lvl_mngr->static_level_obj;

		if (lvl_obj->current_map != NULL)
		{
			int32_t width = lvl_obj->current_map->tilemap.col_count * lvl_obj->current_map->tilemap.tile_size;
			int32_t height = lvl_obj->current_map->tilemap.row_count * lvl_obj->current_map->tilemap.tile_size;

			render_mngr->scroll_vector.x = (render_mngr->screen_info.width / 2) - ((width * 0.5f) / render_mngr->zoom_factor);
			render_mngr->scroll_vector.y = (render_mngr->screen_info.height / 2) - ((height * 0.5f) / render_mngr->zoom_factor);
		}
	}

	return 1;
}

static int camera_focusonentity(lua_State* L)
{
	const char* entity_focus = lua_tostring(L,1);

	entity_t* entity_fo = lua_lvlbase_get_entity(L, entity_focus, 2);

	if(entity_fo)
	{
		lua_getglobal(L,"mngr");

		render_manager_t* render_mngr;

		lua_pushstring(L,"render_mngr");
		lua_gettable(L,-2);

		render_mngr = (render_manager_t*)lua_touserdata(L,-1);

		lua_pop(L,1);

		cpVect entity_pos = entity_getposition(entity_fo);


		render_mngr->scroll_vector.x = (render_mngr->screen_info.width / 2) -  ((float)entity_pos.x / render_mngr->zoom_factor);
		render_mngr->scroll_vector.y = (render_mngr->screen_info.height / 2) -  ((float)entity_pos.y / render_mngr->zoom_factor);
	}

	return 1;
}

static int camera_focusonposition(lua_State* L)
{
	if(!lua_istable(L,1))
	{
		luaL_error(L,"The first parameter of camera_focusonposition is not a vector!"); 
		return 1;
	}

	Vector_t pos_vector;

	pos_vector.x = getluatable_value(L,"x",1);
	pos_vector.y = getluatable_value(L,"y",1);


	lua_getglobal(L,"mngr");

	render_manager_t* render_mngr;

	lua_pushstring(L,"render_mngr");
	lua_gettable(L,-2);

	render_mngr = (render_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	render_mngr->scroll_vector.x = (render_mngr->screen_info.width / 2) -  ((float)pos_vector.x / render_mngr->zoom_factor);
	render_mngr->scroll_vector.y = (render_mngr->screen_info.height / 2) -  ((float)pos_vector.y / render_mngr->zoom_factor);

	return 1;
}

static int camera_getscroll(lua_State* L)
{
	lua_getglobal(L,"mngr");

	render_manager_t* render_mngr;

	lua_pushstring(L,"render_mngr");
	lua_gettable(L,-2);

	render_mngr = (render_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);


	lua_newtable(L);
	lua_pushnumber(L, render_mngr->scroll_vector.x);
	lua_setfield(L,-2,"x");
	lua_pushnumber(L,render_mngr->scroll_vector.y);
	lua_setfield(L,-2,"y");

	lua_getglobal(L,"Vector");//we use an already defined metatable in lua, it's stored in a global in this case and not in the registry
	lua_setmetatable(L,-2);

	return 1;
}

static int camera_setscroll(lua_State* L)
{
	Vector_t scroll_vector;

	scroll_vector.x = getluatable_value(L,"x",1);
	scroll_vector.y = getluatable_value(L,"y",1);

	lua_getglobal(L,"mngr");
	render_manager_t* render_mngr;

	lua_pushstring(L,"render_mngr");
	lua_gettable(L,-2);

	render_mngr = (render_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	render_mngr->scroll_vector = scroll_vector;

	return 1;
}

static int camera_getviewsize(lua_State* L)
{
	lua_getglobal(L,"mngr");

	render_manager_t* render_mngr;

	lua_pushstring(L,"render_mngr");
	lua_gettable(L,-2);

	render_mngr = (render_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);


	lua_newtable(L);
	lua_pushnumber(L, render_mngr->screen_info.width);
	lua_setfield(L,-2,"width");
	lua_pushnumber(L,render_mngr->screen_info.height);
	lua_setfield(L,-2,"height");


	return 1;
}

static int camera_getzoom(lua_State* L)
{
	lua_getglobal(L,"mngr");

	render_manager_t* render_mngr;

	lua_pushstring(L,"render_mngr");
	lua_gettable(L,-2);

	render_mngr = (render_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	lua_pushnumber(L, render_mngr->zoom_factor);

	return 1;
}

static int camera_setzoom(lua_State* L)
{
	float new_zoom = (float)lua_tonumber(L,1);

	lua_getglobal(L,"mngr");

	render_manager_t* render_mngr;

	lua_pushstring(L,"render_mngr");
	lua_gettable(L,-2);

	render_mngr = (render_manager_t*)lua_touserdata(L,-1);

	lua_pop(L, 1);

	lua_pushstring(L, "light_mngr");
	lua_gettable(L, -2);

	light_manager_t* light_mngr = (light_manager_t*)lua_touserdata(L, -1);

	lua_pop(L,1);

	render_mngr->zoom_factor = new_zoom;

	render_update_screen(render_mngr);
	light_manager_resizerender(light_mngr, render_mngr);

	return 1;
}

static int lua_render_getvideoresolutions(lua_State* L)
{
	LUA_INTEGER aspect_ratio = lua_tointeger(L, 1);
	LUA_INTEGER refreshrate = lua_tointeger(L, 2);

	lua_getglobal(L, "mngr");
	render_manager_t* render_mngr;

	lua_pushstring(L, "render_mngr");
	lua_gettable(L, -2);

	render_mngr = (render_manager_t*)lua_touserdata(L, -1);

	lua_pop(L, 1);

	aspectratio ratio = (aspectratio)aspect_ratio;
	int numresolutions = 0;


	const videomode_t** const resolutions = render_getvideoresolutions(render_mngr, &numresolutions);

	//return an array of video resolutions

	if (resolutions != NULL) {

		lua_newtable(L);

		LUA_NUMBER iarray = 1;

		for (int32_t i = 0; i < numresolutions; i++)
		{
			if(resolutions[i]->aspectratio != ratio || resolutions[i]->refreshrate != refreshrate)
				continue;

			//lua indexing start at one
			char resolution[256];
			w_sprintf(resolution, 256, "%dx%d", resolutions[i]->width, resolutions[i]->height);
			lua_pushnumber(L, iarray++);
			lua_pushstring(L, resolution);
			lua_settable(L, -3);
		}
	}
	else {
		lua_pushnil(L);
	}

	return 1;
}

static int lua_render_getaspectratios(lua_State* L)
{
	lua_getglobal(L, "mngr");
	render_manager_t* render_mngr;

	lua_pushstring(L, "render_mngr");
	lua_gettable(L, -2);

	render_mngr = (render_manager_t*)lua_touserdata(L, -1);

	lua_pop(L, 1);

	int numresolutions = 0;

	const videomode_t** const resolutions = render_getvideoresolutions(render_mngr, &numresolutions);
	aspectratio** ratios = (aspectratio**)alloca(numresolutions * sizeof(aspectratio*));
	int numaspectratio = 0;

	//return an array of aspect ratios

	if (resolutions != NULL) {

		lua_newtable(L);

		LUA_NUMBER iarray = 1;

		for (int32_t i = 0; i < numresolutions; i++)
		{
			wbool hasra = wfalse;
			for (int ira = 0; ira < numaspectratio; ira++) {
				if ((*ratios[ira]) == resolutions[i]->aspectratio) {
					hasra = wtrue;
					break;
				}
			}

			if (hasra) {
				continue;
			}

			//lua indexing start at one
			lua_pushnumber(L, iarray++);
			lua_pushinteger(L, (int)resolutions[i]->aspectratio);
			lua_settable(L, -3);
			aspectratio* ra = (aspectratio*)alloca(sizeof(aspectratio));
			(*ra) = resolutions[i]->aspectratio;
			ratios[numaspectratio] = ra;
			numaspectratio++;
		}
	}
	else {
		lua_pushnil(L);
	}

	return 1;
}

static int lua_render_getfps(lua_State* L)
{
	LUA_INTEGER res_x = lua_tointeger(L, 1);
	LUA_INTEGER res_y = lua_tointeger(L, 2);

	lua_getglobal(L, "mngr");
	render_manager_t* render_mngr;

	lua_pushstring(L, "render_mngr");
	lua_gettable(L, -2);

	render_mngr = (render_manager_t*)lua_touserdata(L, -1);

	lua_pop(L, 1);

	int numresolutions = 0;

	const videomode_t** const resolutions = render_getvideoresolutions(render_mngr, &numresolutions);

	//return an array of refresh rates for the resolution

	if (resolutions != NULL) {

		lua_newtable(L);

		LUA_NUMBER iarray = 1;

		for (int32_t i = 0; i < numresolutions; i++)
		{
			if (!(resolutions[i]->width == res_x && resolutions[i]->height == res_y)) {
				continue;
			}

			//lua indexing start at one
			lua_pushnumber(L, iarray++);
			lua_pushinteger(L, resolutions[i]->refreshrate);
			lua_settable(L, -3);
		}
	}
	else {
		lua_pushnil(L);
	}

	return 1;
}


static int lua_sprite_getsize(lua_State* L)
{
	uint32_t ptr_id = (uint32_t)lua_tointeger(L, 1);

	intptr_t ptr = script_getpointerfromid(ptr_id,SPRITE);

	sprite_t* sprite_obj = (sprite_t*)(ptr);

	if(sprite_obj)
	{
		lua_newtable(L);
		lua_pushnumber(L, sprite_obj->width);
		lua_setfield(L,-2,"width");
		lua_pushnumber(L, sprite_obj->height);
		lua_setfield(L,-2,"height");
	}

	return 1;
}


static int lua_sprite_getrepeat(lua_State* L)
{
	uint32_t ptr_id = (uint32_t)lua_tointeger(L, 1);

	intptr_t ptr = script_getpointerfromid(ptr_id,SPRITE);

	sprite_t* sprite_obj = (sprite_t*)(ptr);

	if(sprite_obj)
	{
		lua_newtable(L);
		lua_pushnumber(L, sprite_obj->repeat_x);
		lua_setfield(L,-2,"repeatx");
		lua_pushnumber(L, sprite_obj->repeat_y);
		lua_setfield(L,-2,"repeaty");
	}

	return 1;
}


static int lua_sprite_setsize(lua_State* L)
{
	uint32_t ptr_id = (uint32_t)lua_tointeger(L, 1);
	int32_t width = (int32_t)lua_tointeger(L,2);
	int32_t height = (int32_t)lua_tointeger(L,3);

	intptr_t ptr = script_getpointerfromid(ptr_id,SPRITE);

	sprite_t* sprite_obj = (sprite_t*)(ptr);

	if(sprite_obj)
	{
		sprite_setsize(sprite_obj,width,height);
	}

	return 1;
}

static int lua_sprite_setrepeat(lua_State* L)
{
	uint32_t ptr_id = (uint32_t)lua_tointeger(L, 1);
	int32_t repeatx = (int32_t)lua_tointeger(L,2);
	int32_t repeaty = (int32_t)lua_tointeger(L,3);

	intptr_t ptr = script_getpointerfromid(ptr_id,SPRITE);

	sprite_t* sprite_obj = (sprite_t*)(ptr);

	if(sprite_obj)
	{
		sprite_setrepeat(sprite_obj,repeatx,repeaty);
	}

	return 1;
}

static int lua_sprite_setscale(lua_State* L)
{
	uint32_t ptr_id = (uint32_t)lua_tointeger(L, 1);
	float scale_x = (float)lua_tonumber(L,2);
	float scale_y = (float)lua_tonumber(L,3);

	intptr_t ptr = script_getpointerfromid(ptr_id,SPRITE);

	sprite_t* sprite_obj = (sprite_t*)(ptr);

	if(sprite_obj)
	{
		sprite_obj->scale_x = scale_x;
		sprite_obj->scale_y = scale_y;
	}


	return 1;
}

static int lua_sprite_setrotation(lua_State* L)
{
	uint32_t ptr_id = (uint32_t)lua_tointeger(L, 1);
	float rotation = (float)lua_tonumber(L,2);

	intptr_t ptr = script_getpointerfromid(ptr_id,SPRITE);

	sprite_t* sprite_obj = (sprite_t*)(ptr);

	if(sprite_obj)
	{
		sprite_setrotation(sprite_obj,rotation);
	}

	return 1;
}

static int lua_sprite_getrotation(lua_State* L)
{
	uint32_t ptr_id = (uint32_t)lua_tointeger(L, 1);

	intptr_t ptr = script_getpointerfromid(ptr_id,SPRITE);

	sprite_t* sprite_obj = (sprite_t*)(ptr);

	if(sprite_obj)
	{
		lua_pushnumber(L,sprite_getrotation(sprite_obj));
	}

	return 1;
}

static int lua_sprite_setcolor(lua_State* L)
{
	uint32_t ptr_id = (uint32_t)lua_tointeger(L, 1);
	float r = (float)lua_tonumber(L,2);
	float g = (float)lua_tonumber(L,3);
	float b = (float)lua_tonumber(L,4);
	float a = (float)lua_tonumber(L,5);

	ColorA_t new_color;

	new_color.r = r;
	new_color.g = g;
	new_color.b = b;
	new_color.a = a;

	intptr_t ptr = script_getpointerfromid(ptr_id,SPRITE);

	if(!ptr)
	{
		luaL_error(L,"invalid sprite pointer!");
		return 1;
	}

	sprite_t* sprite_obj = (sprite_t*)(ptr);

	if(sprite_obj)
		sprite_setcolor(sprite_obj,new_color);

	return 1;
}

static int sprite_checkanimended(lua_State* L)
{
	uint32_t ptr_id = (uint32_t)lua_tointeger(L, 1);

	intptr_t ptr = script_getpointerfromid(ptr_id,SPRITE);

	sprite_t* sprite_obj = (sprite_t*)(ptr);

	if(sprite_obj && sprite_obj->animated && sprite_obj->current_anim != NULL)
		lua_pushboolean(L,(int)sprite_obj->current_anim->ended);
		

	return 1;
}

static int lua_sprite_setmirror(lua_State* L)
{
	uint32_t ptr_id = (uint32_t)lua_tointeger(L, 1);
	wbool mirror = (wbool)lua_toboolean(L,2);
	wbool y_axis = (wbool)lua_toboolean(L,3);

	intptr_t ptr = script_getpointerfromid(ptr_id,SPRITE);

	sprite_t* sprite_obj = (sprite_t*)(ptr);

	if(sprite_obj)
	{
		sprite_obj->mirror_texture_x = (!y_axis) ? mirror : wfalse;
		sprite_obj->mirror_texture_y = (y_axis) ? mirror : wfalse;
		sprite_obj->tex_coord_needupdate = wtrue;
	}

	return 1;
}

static int lua_sprite_setstoredoffset(lua_State* L)
{
	uint32_t ptr_id = (uint32_t)lua_tointeger(L, 1);
	
	Vector_t offset = vectorzero;

	if (lua_istable(L, 2))
	{
		offset.x = getluatable_value(L, "x", 2);
		offset.y = getluatable_value(L, "y", 2);
	}

	intptr_t ptr = script_getpointerfromid(ptr_id,SPRITE);

	sprite_t* sprite_obj = (sprite_t*)(ptr);

	if(sprite_obj)
	{
		sprite_setstoredoffset(sprite_obj, offset);
	}

	return 1;

}

static int lua_sprite_getstoredoffset(lua_State* L) 
{
	uint32_t ptr_id = (uint32_t)lua_tointeger(L, 1);

	intptr_t ptr = script_getpointerfromid(ptr_id,SPRITE);

	sprite_t* sprite_obj = (sprite_t*)(ptr);

	if (sprite_obj)
	{
		lua_newtable(L);
		lua_pushnumber(L, sprite_obj->storedoffset.x);
		lua_setfield(L, -2, "x");
		lua_pushnumber(L, sprite_obj->storedoffset.y);
		lua_setfield(L, -2, "y");

		lua_getglobal(L, "Vector");//we use an already defined metatable in lua, it's stored in a global in this case and not in the registry
		lua_setmetatable(L, -2);
	}
	else
	{
		lua_pushnil(L);
	}

	return 1;
}

static int lua_sprite_flicker(lua_State* L)
{
	uint32_t ptr_id = (uint32_t)lua_tointeger(L, 1);
	float flicker_time = (float)lua_tonumber(L,2);

	intptr_t ptr = script_getpointerfromid(ptr_id,SPRITE);

	sprite_t* sprite_obj = (sprite_t*)(ptr);

	if(sprite_obj)
		sprite_flicker(sprite_obj,flicker_time);

	return 1;
}

static int lua_sprite_getflicker(lua_State* L)
{
	uint32_t ptr_id = (uint32_t)lua_tointeger(L, 1);

	intptr_t ptr = script_getpointerfromid(ptr_id,SPRITE);

	sprite_t* sprite_obj = (sprite_t*)(ptr);

	if(sprite_obj)
          lua_pushnumber(L,sprite_obj->flicker_time);
     else
          lua_pushnumber(L,0.0f);

     return 1;
}

static int lua_sprite_getcurrentanimation(lua_State* L)
{
	uint32_t ptr_id = (uint32_t)lua_tointeger(L, 1);

	intptr_t ptr = script_getpointerfromid(ptr_id,SPRITE);

	sprite_t* sprite_obj = (sprite_t*)(ptr);

	if(sprite_obj && sprite_obj->animated && sprite_obj->current_anim)
	{
		lua_pushstring(L,sprite_obj->current_anim->anim_def->name);
	}
	else
	{
		lua_pushnil(L);
	}

	return 1;
}

static int lua_sprite_stopanimation(lua_State* L)
{
	uint32_t ptr_id = (uint32_t)lua_tointeger(L, 1);

	intptr_t ptr = script_getpointerfromid(ptr_id, SPRITE);

	sprite_t* sprite_obj = (sprite_t*)(ptr);

	if(sprite_obj && sprite_obj->animated && sprite_obj->current_anim)
	{
		sprite_stopanimation(sprite_obj);
	}

	return 1;
}

static int sprite_getcontactindx(lua_State* L)
{
	uint32_t ptr_id = (uint32_t)lua_tointeger(L, 1);
	const char* contact_str = lua_tostring(L,2);


	intptr_t ptr = script_getpointerfromid(ptr_id, SPRITE);

	sprite_t* sprite_obj = (sprite_t*)(ptr);

	if(sprite_obj && sprite_obj->animated && sprite_obj->current_anim)
	{
		int32_t indx = animation_getcontactpointindx(sprite_obj->current_anim,contact_str);

		if(indx == -1)
			luaL_error(L,"a contact point named %s doesn't exist!",contact_str);

		lua_pushinteger(L,indx);
	}
	else
	{
		lua_pushinteger(L,-1);
	}

	return 1;
}

static int sprite_getcontactoffset(lua_State* L)
{
	uint32_t ptr_id = (uint32_t)lua_tointeger(L, 1);
	lua_Integer contact_index = lua_tointeger(L, 2);


	intptr_t ptr = script_getpointerfromid(ptr_id, SPRITE);

	sprite_t* sprite_obj = (sprite_t*)(ptr);

	if (sprite_obj && sprite_obj->animated && sprite_obj->current_anim)
	{
		Vector_t contactpos = animation_getcontactpos_atframe(sprite_obj->current_anim, (int32_t)contact_index);

		lua_newtable(L);
		lua_pushnumber(L, contactpos.x);
		lua_setfield(L, -2, "x");
		lua_pushnumber(L, contactpos.y);
		lua_setfield(L, -2, "y");

		lua_getglobal(L, "Vector");//we use an already defined metatable in lua, it's stored in a global in this case and not in the registry
		lua_setmetatable(L, -2);
	}
	else
	{
		lua_pushnil(L);
	}

	return 1;
}

static int sprite_getcontactlist(lua_State* L)
{
	uint32_t ptr_id = (uint32_t)lua_tointeger(L, 1);

	intptr_t ptr = script_getpointerfromid(ptr_id, SPRITE);

	sprite_t* sprite_obj = (sprite_t*)(ptr);

	if(sprite_obj && sprite_obj->animated && sprite_obj->current_anim)
	{
		lua_newtable(L);

		for(uint32_t icont = 0; icont < sprite_obj->current_anim->anim_def->n_attachpoint_id;icont++)
		{
			lua_pushnumber(L,icont+1);
			lua_pushstring(L,sprite_obj->current_anim->anim_def->attachpoint_id[icont]);
			lua_settable(L,-3);
		}
	}

	return 1;
}



static int sprite_iscontactactive(lua_State* L)
{
	uint32_t ptr_id = (uint32_t)lua_tointeger(L, 1);
	int32_t contact_indx = (int32_t)lua_tointeger(L,2);

	intptr_t ptr = script_getpointerfromid(ptr_id, SPRITE);

	sprite_t* sprite_obj = (sprite_t*)(ptr);

	if(sprite_obj && sprite_obj->animated && sprite_obj->current_anim && contact_indx != -1)
	{
		lua_pushboolean(L,animation_iscontactactive_atframe(sprite_obj->current_anim,contact_indx));
	}
	else
	{
		lua_pushboolean(L,0);
	}

	return 1;
}

//mesh scripting
static int lua_mesh_setrotation(lua_State* L)
{
	uint32_t ptr_id = (uint32_t)lua_tointeger(L, 1);
	float rotationX = (float)lua_tonumber(L,2);
	float rotationY = (float)lua_tonumber(L,3);
	float rotationZ = (float)lua_tonumber(L,4);

	intptr_t ptr = script_getpointerfromid(ptr_id, MESH);

	Mesh_t* mesh_obj = (Mesh_t*)(ptr);

	if(mesh_obj)
	{
		mesh_setrotation(mesh_obj,rotationX,rotationY,rotationZ);
	}

	return 1;
}

static int lua_mesh_getrotation(lua_State* L)
{
	uint32_t ptr_id = (uint32_t)lua_tointeger(L, 1);

	intptr_t ptr = script_getpointerfromid(ptr_id, MESH);

	Mesh_t* mesh_obj = (Mesh_t*)(ptr); 

	if(mesh_obj)
	{
		lua_newtable(L);
		lua_pushnumber(L, mesh_obj->rotation_x);
		lua_setfield(L,-2,"rotationx");
		lua_pushnumber(L, mesh_obj->rotation_y);
		lua_setfield(L,-2,"rotationy");
		lua_pushnumber(L, mesh_obj->rotation_z);
		lua_setfield(L,-2,"rotationz");
	}
	else
	{
		lua_pushnil(L);
	}

	return 1;
}

static int lua_mesh_setscale(lua_State* L)
{
	uint32_t ptr_id = (uint32_t)lua_tointeger(L, 1);
	float scaleX = (float)lua_tonumber(L, 2);
	float scaleY = (float)lua_tonumber(L, 3);
	float scaleZ = (float)lua_tonumber(L, 4);

	intptr_t ptr = script_getpointerfromid(ptr_id, MESH);

	Mesh_t* mesh_obj = (Mesh_t*)(ptr);

	if (mesh_obj)
	{
		mesh_setscale(mesh_obj, scaleX, scaleY, scaleZ);
	}

	return 1;
}

static int lua_mesh_getscale(lua_State* L)
{
	uint32_t ptr_id = (uint32_t)lua_tointeger(L, 1);

	intptr_t ptr = script_getpointerfromid(ptr_id, MESH);

	Mesh_t* mesh_obj = (Mesh_t*)(ptr);

	if (mesh_obj)
	{
		lua_newtable(L);
		lua_pushnumber(L, mesh_obj->scale_x);
		lua_setfield(L, -2, "scalex");
		lua_pushnumber(L, mesh_obj->scale_y);
		lua_setfield(L, -2, "scaley");
		lua_pushnumber(L, mesh_obj->scale_z);
		lua_setfield(L, -2, "scalez");
	}
	else
	{
		lua_pushnil(L);
	}

	return 1;
}




static int lua_mesh_setcolor(lua_State* L)
{
	uint32_t ptr_id = (uint32_t)lua_tointeger(L, 1);

	ColorA_t color = white_color;

	if(lua_type(L,2) == LUA_TTABLE)
	{
		color.r = getluatable_value(L,"r",2);
		color.g = getluatable_value(L,"g",2);
		color.b = getluatable_value(L,"b",2);
		color.a = getluatable_value(L,"a",2);
	}

	intptr_t ptr = script_getpointerfromid(ptr_id, MESH);

	Mesh_t* mesh_obj = (Mesh_t*)(ptr);

	if(mesh_obj)
	{
		mesh_setcolor(mesh_obj,color);
	}

	return 1;
}

static int lua_mesh_flicker(lua_State* L)
{
	uint32_t ptr_id = (uint32_t)lua_tointeger(L, 1);
	float flicker_time = (float)lua_tonumber(L,2);

	intptr_t ptr = script_getpointerfromid(ptr_id, MESH);

	Mesh_t* mesh_obj = (Mesh_t*)(ptr);

	if(mesh_obj)
		mesh_flicker(mesh_obj,flicker_time);

	return 1;
}

static int lua_mesh_getflicker(lua_State* L)
{
	uint32_t ptr_id = (uint32_t)lua_tointeger(L, 1);

	intptr_t ptr = script_getpointerfromid(ptr_id, MESH);

	Mesh_t* mesh_obj = (Mesh_t*)(ptr);

	if(mesh_obj)
          lua_pushnumber(L,mesh_obj->flicker_time);
     else
          lua_pushnumber(L,0.0f);

     return 1;
}

static int lua_mesh_setz(lua_State* L)
{
	uint32_t ptr_id = (uint32_t)lua_tointeger(L, 1);
	 float z_value = (float)lua_tonumber(L,2);

	 intptr_t ptr = script_getpointerfromid(ptr_id, MESH);

	Mesh_t* mesh_obj = (Mesh_t*)(ptr);

	if(mesh_obj)
		mesh_obj->position.z = z_value;

     return 1;
}


//Compound sprite

static int compoundsprite_setzindex(lua_State* L)
{
	uint32_t ptr_id = (uint32_t)lua_tointeger(L, 1);
	int32_t z_index = (int32_t)lua_tointeger(L,2);

	intptr_t ptr = script_getpointerfromid(ptr_id, COMPOUND_SPRITE);

	compound_sprite_t* sprite_obj = (compound_sprite_t*)(ptr);

	if(sprite_obj)
		sprite_obj->z_index = z_index;

	return 1;
}

static int compoundsprite_getposition(lua_State* L)
{
	uint32_t ptr_id = (uint32_t)lua_tointeger(L, 1);
	int16_t shape_index = (int16_t)lua_tointeger(L,2);

	intptr_t ptr = script_getpointerfromid(ptr_id, COMPOUND_SPRITE);

	compound_sprite_t* sprite_obj = (compound_sprite_t*)(ptr);

	if(sprite_obj)
	{
		 lua_newtable(L);
		 lua_pushnumber(L,sprite_obj->shape_position[shape_index].x);
		 lua_setfield(L,-2,"x");
		 lua_pushnumber(L,sprite_obj->shape_position[shape_index].y);
		 lua_setfield(L,-2,"y");

		 //luaL_getmetatable(L,"Vector");
		 lua_getglobal(L,"Vector");//we use an already defined metatable in lua, it's stored in a global in this case and not in the registry
		 lua_setmetatable(L,-2);
	}


	return 1;
}

static int compoundsprite_setposition(lua_State* L)
{
	uint32_t ptr_id = (uint32_t)lua_tointeger(L, 1);

	Vector_t position = vectorzero;

	if(lua_istable(L,2))
	{
		position.x =  getluatable_value(L,"x",2);
		position.y = getluatable_value(L,"y",2);
	}

	int16_t shape_index = (int16_t)lua_tointeger(L,3);

	intptr_t ptr = script_getpointerfromid(ptr_id, COMPOUND_SPRITE);

	compound_sprite_t* sprite_obj = (compound_sprite_t*)(ptr);

	if(sprite_obj)
		compound_sprite_updateshape(sprite_obj,position,0.0f,shape_index);

	return 1;
}

static int compoundsprite_getnumshape(lua_State* L)
{
	uint32_t ptr_id = (uint32_t)lua_tointeger(L, 1);
	intptr_t ptr = script_getpointerfromid(ptr_id, COMPOUND_SPRITE);

	compound_sprite_t* sprite_obj = (compound_sprite_t*)(ptr);

	if(sprite_obj)
	{
		int32_t isprite = 0;
		//count shape
		for(isprite = 0; isprite < COMPOUND_MAX_SHAPE; isprite++)
		{
			if(sprite_obj->shape_size[isprite] == 0) //find the first empty shape cell
				break;
		}

          #ifdef USE_LUA_JIT
               lua_pushnumber(L,isprite);
          #else
			lua_pushinteger(L,isprite);
          #endif
	}
	else
	{
           #ifdef USE_LUA_JIT
               lua_pushnumber(L,0);
          #else
		   lua_pushinteger(L,0);
          #endif
	}

		
	return 1;
}

void register_render_functions(lua_State *L)
{
	//camera functions
	lua_pushcfunction(L,camera_focuson);
	lua_setglobal(L,"camera_focuson");

	lua_pushcfunction(L,camera_focusonentity);
	lua_setglobal(L,"camera_focusonentity");

	lua_pushcfunction(L,camera_focusonposition);
	lua_setglobal(L,"camera_focusonposition");

	lua_pushcfunction(L,camera_getscroll);
	lua_setglobal(L,"camera_getscroll");

	lua_pushcfunction(L,camera_setscroll);
	lua_setglobal(L,"camera_setscroll");

	lua_pushcfunction(L,camera_getviewsize);
	lua_setglobal(L,"camera_getviewsize");

	lua_pushcfunction(L,camera_getzoom);
	lua_setglobal(L,"camera_getzoom");

	lua_pushcfunction(L,camera_setzoom);
	lua_setglobal(L,"camera_setzoom");

	//render info functions
	lua_pushcfunction(L, lua_render_getvideoresolutions);
	lua_setglobal(L, "render_getvideoresolutions");

	lua_pushcfunction(L, lua_render_getaspectratios);
	lua_setglobal(L, "render_getaspectratios");

	lua_pushcfunction(L, lua_render_getfps);
	lua_setglobal(L, "render_getfps");
	
	//sprite functions
	lua_pushcfunction(L,lua_sprite_setcolor);
	lua_setglobal(L,"sprite_setcolor");

	lua_pushcfunction(L,lua_sprite_setmirror);
	lua_setglobal(L,"sprite_setmirror");

	lua_pushcfunction(L,sprite_checkanimended);
	lua_setglobal(L,"sprite_checkanimended");

	lua_pushcfunction(L,lua_sprite_flicker);
	lua_setglobal(L,"sprite_flicker");

     lua_pushcfunction(L,lua_sprite_getflicker);
	lua_setglobal(L,"sprite_getflicker");

	lua_pushcfunction(L,lua_sprite_getcurrentanimation);
	lua_setglobal(L,"sprite_getcurrentanimation");

	lua_pushcfunction(L,lua_sprite_stopanimation);
	lua_setglobal(L,"sprite_stopanimation");

	lua_pushcfunction(L,sprite_getcontactindx);
	lua_setglobal(L,"sprite_getcontactindx");

	lua_pushcfunction(L, sprite_getcontactoffset);
	lua_setglobal(L, "sprite_getcontactoffset");

	lua_pushcfunction(L,sprite_getcontactlist);
	lua_setglobal(L,"sprite_getcontactlist");
	
	lua_pushcfunction(L,sprite_iscontactactive);
	lua_setglobal(L,"sprite_iscontactactive");

	lua_pushcfunction(L,lua_sprite_getsize);
	lua_setglobal(L,"sprite_getsize");

	lua_pushcfunction(L,lua_sprite_getrepeat);
	lua_setglobal(L,"sprite_getrepeat");

	lua_pushcfunction(L,lua_sprite_setsize);
	lua_setglobal(L,"sprite_setsize");

	lua_pushcfunction(L, lua_sprite_setstoredoffset);
	lua_setglobal(L, "sprite_setstoredoffset");
	
	lua_pushcfunction(L, lua_sprite_getstoredoffset);
	lua_setglobal(L, "sprite_getstoredoffset");

	lua_pushcfunction(L,lua_sprite_setrepeat);
	lua_setglobal(L,"sprite_setrepeat");

	lua_pushcfunction(L,lua_sprite_setscale);
	lua_setglobal(L,"sprite_setscale");

	lua_pushcfunction(L,lua_sprite_setrotation);
	lua_setglobal(L,"sprite_setrotation");

	lua_pushcfunction(L,lua_sprite_getrotation);
	lua_setglobal(L,"sprite_getrotation");

	//mesh functions
	lua_pushcfunction(L,lua_mesh_setrotation);
	lua_setglobal(L,"mesh_setrotation");
	
	lua_pushcfunction(L,lua_mesh_getrotation);
	lua_setglobal(L,"mesh_getrotation");

	lua_pushcfunction(L, lua_mesh_setscale);
	lua_setglobal(L, "mesh_setscale");

	lua_pushcfunction(L, lua_mesh_getscale);
	lua_setglobal(L, "mesh_getscale");

	lua_pushcfunction(L,lua_mesh_setcolor);
	lua_setglobal(L,"mesh_setcolor");

	lua_pushcfunction(L,lua_mesh_flicker);
	lua_setglobal(L,"mesh_flicker");

	lua_pushcfunction(L,lua_mesh_getflicker);
	lua_setglobal(L,"mesh_getflicker");

	lua_pushcfunction(L,lua_mesh_setz);
	lua_setglobal(L,"mesh_setz");

	//compound sprite functions
	lua_pushcfunction(L,compoundsprite_setzindex);
	lua_setglobal(L,"compoundsprite_setzindex");

	lua_pushcfunction(L,compoundsprite_getposition);
	lua_setglobal(L,"compoundsprite_getposition");

	lua_pushcfunction(L,compoundsprite_setposition);
	lua_setglobal(L,"compoundsprite_setposition");

	lua_pushcfunction(L,compoundsprite_getnumshape);
	lua_setglobal(L,"compoundsprite_getnumshape");
}
