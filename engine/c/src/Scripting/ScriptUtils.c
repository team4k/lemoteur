//TODO: lua_is* function are misleading and don't check variable type but if a variable can be converted to a type, replace
//all lua_is* function with lua_type(L,stack_pos) == <LUA_TYPE> to check the type


#ifdef _MSC_VER
    extern "C" {
#endif
        
#include "lauxlib.h"
#include "lualib.h"
#include "lua.h"
        
#ifdef _MSC_VER
 }
#endif

#if defined(_WIN32)
    #include <windows.h>
#else
	#ifndef __USE_BSD
        	#define __USE_BSD
	#endif
        #include <unistd.h>
        #include <dlfcn.h>
		#include <wchar.h>
		#include <pthread.h>
#endif

#include <stdarg.h>
#include <string.h>
#include <inttypes.h>
#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include <math.h>
#include <sqlite3.h>

#include <Engine.h>

#include <Render.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>
#include <Base/geom.h>

#include <Render/render_manager.h>

#include <GameObjects/Shaders.pb-c.h>
#include <Graphics/TextureLoader.h>
#include <Resx/shader_loader.h>

#include <Utils/Hash.h>
#include <Utils/hashmap.h>

#include <GameObjects/Characters.pb-c.h>
#include <Graphics/Animation.h>
#include <Graphics/Sprite.h>
#include <Graphics/Mesh.h>

#include <Graphics/Light.h>
#include <Graphics/LightManager.h>

#include <Graphics/TextureRegion.h>
#include <Graphics/SpriteBatch.h>
#include <Debug/Logprint.h>

#include <GameObjects/Level.pb-c.h>
#include <Collisions/Collisions.h>

#include <chipmunk/chipmunk_private.h>

#include <Physics/Physics.h>

#include <Resx/localization.h>


#include <Graphics/FontLoader.h>


#include <GameObjects/entity.h>
#include <GameObjects/Trigger.h>
#include <Font/RenderText.h>
#include <GameObjects/text_entity.h>

#ifdef EMSCRIPTEN
	#include <SDL2/SDL_audio.h>
	#include <emscripten.h>
#else
	#include <portaudio.h>
#endif


#include <Sound/Sound.h>

#include <Resx/Resources.h>

#include <Graphics/TilemapLayer.h>
#include <GameObjects/Tilemap.h>

#include <GameObjects/entity_group.h>
#include <Graphics/Effects/particle_object.h>
#include <GameObjects/particles_entity.h>

#include <Config/config.h>
#include <GameMngr/input_mngr.h>
#include <game_functions.h>
#include <GameMngr/module_mngr.h>
#include <GameObjects/map.h>
#include <GameObjects/Collider.h>
#include <GameObjects/waypoint.h>
#include <GameObjects/Levelobject.h>

#include <GameObjects/level_base.h>



#include <Scripting/ScriptUtils.h>
#include <Scripting/ScriptLuaUtils.h>
#include <Scripting/ScriptPointers.h>

level_base_t* lua_getlvlbase(lua_State* L)
{
	lua_getglobal(L, "mngr");

	lua_pushstring(L, "level_mngr");
	lua_gettable(L, -2);

	level_base_t* lvlcontainer = NULL;

	if (lua_isuserdata(L, -1))
	{
		 lvlcontainer = (level_base_t*)lua_touserdata(L, -1);
	}

	lua_pop(L, 1);

	return lvlcontainer;
}


void* lua_lvlbase_getmanager(lua_State* L, int stack_pos, LEVEL_TYPE* type)
{
	void* lvl_obj = NULL;
	(*type) = L_STATIC;

	lua_getglobal(L, "mngr");

	lua_pushstring(L, "level_mngr");
	lua_gettable(L, -2);

	level_base_t* lvlcontainer = NULL;

	if (lua_isuserdata(L, -1))
	{
		lvlcontainer = (level_base_t*)lua_touserdata(L, -1);
		lvl_obj = lvlbase_getcurrentlevelobj(lvlcontainer);
		(*type) = lvlcontainer->current_level_type;
	}

	lua_pop(L, 1);

	//check if we have a pointer id in the stack position provided
	//in most case, it's a static level type, check against the existing stream type as we can only have one
	if (lua_isinteger(L, stack_pos))
	{
		uint32_t ptr = (uint32_t)lua_tointeger(L, stack_pos);
		intptr_t number = script_getpointerfromid(ptr, LEVELOBJECT);

		(*type) = L_STATIC;

		lvl_obj = (void*)number;

		if (lvlcontainer != NULL && lvlcontainer->stream_level_obj != NULL && lvl_obj == lvlcontainer->stream_level_obj) {
			(*type) = L_STREAM;
		}
	}

	return lvl_obj;
}

LEVEL_TYPE lua_lvlbase_gettype(lua_State* L)
{

	lua_getglobal(L, "mngr");

	lua_pushstring(L, "level_mngr");
	lua_gettable(L, -2);

	LEVEL_TYPE type = L_STATIC;

	if (lua_isuserdata(L, -1))
	{
		level_base_t* lvlcontainer = (level_base_t*)lua_touserdata(L, -1);
		type = lvlcontainer->current_level_type;
	}

	lua_pop(L, 1);
	return type;
}



Tilemap_t* lua_lvlbase_get_tilemap(lua_State* L, int stack_pos)
{
	void* lvl_obj = NULL;
	LEVEL_TYPE mtype = L_STATIC;

	lvl_obj = lua_lvlbase_getmanager(L, stack_pos, &mtype);

	return (Tilemap_t*)lvlbase_getcurrenttilemap(lvl_obj,mtype);
}

WorldRect_t lua_lvlbase_getdimension(lua_State* L, int stack_pos)
{

	void* lvl_obj = NULL;
	LEVEL_TYPE mtype = L_STATIC;

	lvl_obj = lua_lvlbase_getmanager(L, stack_pos, &mtype);

	return lvlbase_getdimension(lvl_obj, mtype);
}

int32_t lua_lvlbase_gettilesize(lua_State* L, int stack_pos)
{
	void* lvl_obj = NULL;
	LEVEL_TYPE mtype = L_STATIC;

	lvl_obj = lua_lvlbase_getmanager(L, stack_pos, &mtype);

	 return lvlbase_gettilesize(lvl_obj, mtype);
}

waypoint_t* lua_lvlbase_getwaypoint(lua_State* L, const char* waypoint_id, int stack_pos)
{
	if (!waypoint_id)
	{
		luaL_error(L, "[lua_lvlbase_getwaypoint] waypoint_id is null !");
		return NULL;
	}

	LEVEL_TYPE mtype = L_STATIC;
	void* lvl_obj = lua_lvlbase_getmanager(L, stack_pos, &mtype);
	

	if (lvlbase_haswaypoint(lvl_obj, mtype, waypoint_id))
	{
		return (waypoint_t*)lvlbase_getwaypoint(lvl_obj, mtype, waypoint_id);
	}
	else
	{
		luaL_error(L, "The waypoint '%s' doesn't exist ", waypoint_id);
		return NULL;
	}
}

collider_t* lua_lvlbase_getcollider(lua_State* L, const char* collider_id, int stack_pos)
{
	if (!collider_id)
	{
		luaL_error(L, "[lua_lvlbase_getcollider] collider_id is null !");
		return NULL;
	}


	LEVEL_TYPE mtype = L_STATIC;
	void* lvl_obj = lua_lvlbase_getmanager(L, stack_pos, &mtype);


	if (lvlbase_hascollider(lvl_obj, mtype, collider_id))
	{
		return (collider_t*)lvlbase_getcollider(lvl_obj, mtype, collider_id);
	}
	else
	{
		luaL_error(L, "The collider '%s' doesn't exist ", collider_id);
		return NULL;
	}
}


text_entity_t* lua_lvlbase_get_text(lua_State* L,const char* entity_id,int stack_pos)
{
	if(!entity_id)
	{
		luaL_error(L,"[lua_lvlbase_get_text] entity_id is null !");
		return NULL;
	}


	LEVEL_TYPE mtype = L_STATIC;
	void* lvl_obj = lua_lvlbase_getmanager(L, stack_pos, &mtype);


	if (lvlbase_hastext(lvl_obj, mtype, entity_id))
	{
		return (text_entity_t*)lvlbase_gettext(lvl_obj, mtype, entity_id);
	}
	else
	{
		luaL_error(L, "The text '%s' doesn't exist ", entity_id);
		return NULL;
	}

}

wbool lua_lvlbase_has_entity(lua_State* L, const char* entity_id, int stack_pos)
{
	if (!entity_id)
	{
		luaL_error(L, "[lua_lvlbase_get_entity] entity_id is null !");
		return wfalse;
	}

	LEVEL_TYPE mtype = L_STATIC;
	void* lvl_obj = lua_lvlbase_getmanager(L, stack_pos, &mtype);

	return lvlbase_hasentity(lvl_obj, mtype, entity_id);
}

int lua_lvlbase_get_entitypos(lua_State* L, const char* entity_id, int stack_pos)
{
	if (!entity_id)
	{
		luaL_error(L, "[lua_lvlbase_get_entitypos] entity_id is null !");
		return -1;
	}

	LEVEL_TYPE mtype = L_STATIC;
	void* lvl_obj = lua_lvlbase_getmanager(L, stack_pos, &mtype);


	if (lvlbase_hasentity(lvl_obj, mtype, entity_id))
	{
		return lvlbase_getentitypos(lvl_obj, mtype, entity_id);
	}
	else
	{
		luaL_error(L, "The entity '%s' doesn't exist !", entity_id);
		return -1;
	}

}

entity_t* lua_lvlbase_get_entity(lua_State* L,const char* entity_id,int stack_pos)
{
	if(!entity_id)
	{
		luaL_error(L,"[lua_lvlbase_get_entity] entity_id is null !");
		return NULL;
	}

	LEVEL_TYPE mtype = L_STATIC;
	void* lvl_obj = lua_lvlbase_getmanager(L, stack_pos, &mtype);


	if (lvlbase_hasentity(lvl_obj, mtype, entity_id))
	{
		return (entity_t*)lvlbase_getentity(lvl_obj, mtype, entity_id);
	}
	else
	{
		luaL_error(L, "The entity '%s' doesn't exist !", entity_id);
		return NULL;
	}

}

trigger_t* lua_lvlbase_get_trigger(lua_State* L,const char* trigger_id,int stack_pos)
{
	if(!trigger_id)
	{
		luaL_error(L,"[lua_lvlbase_get_trigger] trigger_id is null !");
		return NULL;
	}

	LEVEL_TYPE mtype = L_STATIC;
	void* lvl_obj = lua_lvlbase_getmanager(L, stack_pos, &mtype);

	if (lvlbase_hastrigger(lvl_obj, mtype, trigger_id))
	{
		return (trigger_t*)lvlbase_gettrigger(lvl_obj, mtype, trigger_id);
	}
	else
	{
		luaL_error(L, "The trigger '%s' doesn't exist !", trigger_id);
		return NULL;
	}

}

wbool luatable_hasvalue(lua_State* L,const char* key,int stack_pos)
{
	wbool result = wfalse;
	lua_pushstring(L,key);
	lua_gettable(L, stack_pos);

	result = (wbool)(!lua_isnil(L,-1));

	lua_pop(L,1);

	return result;
}

float getluatable_value(lua_State* L,const char* key,int stack_pos)
{
	float result;
	lua_pushstring(L,key);
	lua_gettable(L, stack_pos);

	if(!lua_isnumber(L,-1))
		luaL_error(L,"can't found / invalid number component for parameter %s!",key);

	result = (float)lua_tonumber(L,-1);
	lua_pop(L,1);

	return result;
}

const char* getluatable_stringvalue(lua_State* L,const char* key,int stack_pos)
{
	const char* result;
	lua_pushstring(L,key);
	lua_gettable(L, stack_pos);

	if(!lua_isstring(L,-1))
		luaL_error(L,"can't found / invalid string component for parameter %s!",key);

	result = (const char*)lua_tostring(L,-1);
	lua_pop(L,1);

	return result;
}

int32_t getluatableint_value(lua_State* L,const char* key,int stack_pos)
{
	int32_t result;
	lua_pushstring(L,key);
	lua_gettable(L, stack_pos);

	if(!lua_isinteger(L,-1))
		luaL_error(L,"can't found / invalid integer component for parameter %s!",key);

	result = (int32_t)lua_tointeger(L,-1);
	lua_pop(L,1);

	return result;
}

wbool getluatablebool_value(lua_State* L,const char* key,int stack_pos)
{
	wbool result;
	lua_pushstring(L,key);
	lua_gettable(L, stack_pos);

	if(!lua_isboolean(L,-1))
		luaL_error(L,"can't found / invalid bool component for parameter %s!",key);

	result = (wbool)lua_toboolean(L,-1);
	lua_pop(L,1);


	return result;
}

size_t getluatable_nestedtable(lua_State* L,const char* key,int stack_pos)
{
	lua_pushstring(L,key);
	lua_gettable(L,stack_pos);

	if(!lua_istable(L,-1))
		luaL_error(L,"can't found / invalid table component for parameter %s",key);

	return (size_t)luaL_len(L,-1);
}

void getlua_rawtable(lua_State* L,int stack_pos,int raw_pos)
{
	lua_rawgeti(L,stack_pos,raw_pos);

	if(!lua_istable(L,-1))
		luaL_error(L,"can't found / invalid table component for parameter at pos %d",raw_pos);
}

void printlua_stack(lua_State* L)
{
	int i = 0;

	int top = lua_gettop(L);

	for (i = 1; i <= top; i++) {
		int t = lua_type(L, i);

		switch (t)
		{
			case LUA_TSTRING: {
				logprint("'%s'", lua_tostring(L, i));
				break;
			}
			case LUA_TBOOLEAN: {
				logprint(lua_toboolean(L, i) ? "true" : "false");
				break;
			}
			case LUA_TNUMBER: {
				logprint("%g", lua_tonumber(L, i));
				break;
			}
			default: {
				logprint("%s", lua_typename(L, t));
				break;
			}
				
		}

		logprint("  ");

	}
}


void no_gravity(cpBody *body, cpVect gravity, cpFloat damping, cpFloat dt)
{
	gravity = cpvzero;
	cpBodyUpdateVelocity(body, gravity,damping,dt);
}

void _protobuf_create_entity(lua_State* L,Entity* const entity_data)
{
	entity__init(entity_data);

	const char* entity_id =  getluatable_stringvalue(L,"entity_id",1);
	size_t size_id = (strlen(entity_id) + 1);
	entity_data->entity_id = (char*)wf_malloc(size_id);
	w_strcpy(entity_data->entity_id,size_id,entity_id);

	const char* script_module =  getluatable_stringvalue(L,"script_module",1);
	size_t module_size = strlen(script_module)+1;
	entity_data->script_module = (char*)wf_malloc(module_size);
	w_strcpy(entity_data->script_module,module_size,script_module);

	const char* script_ctor =  getluatable_stringvalue(L,"script_ctor",1);
	size_t ctor_size = strlen(script_ctor) + 1;
	entity_data->script_ctor = (char*)wf_malloc(ctor_size);
	w_strcpy(entity_data->script_ctor,ctor_size,script_ctor);

	const char* start_animation = getluatable_stringvalue(L,"start_animation",1);
	size_t animation_size = strlen(start_animation) + 1;
	entity_data->start_animation = (char*)wf_malloc(animation_size);
	w_strcpy(entity_data->start_animation,animation_size,start_animation);

	const char* tileset = getluatable_stringvalue(L,"tileset",1);
	size_t tileset_size = strlen(tileset)+1;
	entity_data->tileset = (char*)wf_malloc(tileset_size);
	w_strcpy(entity_data->tileset,tileset_size,tileset);

	const char* animation_file = getluatable_stringvalue(L,"animation_file",1);
	size_t anim_size = strlen(animation_file) + 1;
	entity_data->animation_file = (char*)wf_malloc(anim_size);
	w_strcpy(entity_data->animation_file,anim_size,animation_file);

	entity_data->position = (Wvector*)wf_malloc(sizeof(Wvector));
	wvector__init(entity_data->position);

	entity_data->position->x = getluatableint_value(L,"pos_x",1);
	entity_data->position->y = getluatableint_value(L,"pos_y",1);
	
	
	entity_data->width = getluatableint_value(L,"width",1);
	entity_data->height = getluatableint_value(L,"height",1);
	entity_data->shape_type = getluatableint_value(L,"shape_type",1);
	entity_data->type = getluatableint_value(L,"_type",1);
	entity_data->has_controller = (protobuf_c_boolean)getluatablebool_value(L,"has_controller",1);
	entity_data->no_collisions = (protobuf_c_boolean)getluatablebool_value(L,"no_collisions",1);
	entity_data->has_callback = (protobuf_c_boolean)getluatablebool_value(L,"has_callback",1);
	entity_data->z_order = getluatableint_value(L,"z_order",1);
	entity_data->custom_collisions = (protobuf_c_boolean)getluatablebool_value(L,"custom_collisions",1);
	entity_data->offsetx = getluatableint_value(L,"offsetx",1);
	entity_data->offsety = getluatableint_value(L,"offsety",1);
	entity_data->col_height = getluatableint_value(L,"col_height",1);
	entity_data->col_width = getluatableint_value(L,"col_width",1);
	entity_data->repeat_x = getluatableint_value(L,"repeat_x",1);
	entity_data->repeat_y = getluatableint_value(L,"repeat_y",1);
	entity_data->start_frame = getluatableint_value(L,"start_frame",1);
	entity_data->parallax_x = getluatable_value(L,"parallax_x",1);
	entity_data->parallax_y = getluatable_value(L,"parallax_y",1);
	entity_data->col_radius = getluatable_value(L,"col_radius",1);

	if (luatable_hasvalue(L, "mesh_obj", 1)) {
		entity_data->has_has_mesh = (protobuf_c_boolean)1;
		entity_data->has_mesh = (protobuf_c_boolean)1;
		entity_data->mesh_obj = (mesh_template_type)getluatableint_value(L, "mesh_obj", 1);
	}

	if (luatable_hasvalue(L, "mat_flag", 1)) {
		entity_data->has_mat_flag = (protobuf_c_boolean)1;
		entity_data->mat_flag = getluatableint_value(L, "mat_flag", 1);
	}

	if (luatable_hasvalue(L, "script_init_only", 1)) {
		entity_data->has_script_init_only = (protobuf_c_boolean)1;
		entity_data->script_init_only = (protobuf_c_boolean)getluatablebool_value(L, "script_init_only", 1);
	}

	
	if(luatable_hasvalue(L,"shader_program",1))
	{
		const char* shader_program = getluatable_stringvalue(L,"shader_program",1);
		size_t shader_prog_size = strlen(shader_program) + 1;
		entity_data->shader_program = (char*)wf_malloc(shader_prog_size);
		w_strcpy(entity_data->shader_program,shader_prog_size,shader_program);
	}

	if(luatable_hasvalue(L,"scale_x",1))
	{
		entity_data->scale_x = getluatable_value(L,"scale_x",1);
		entity_data->has_scale_x = (protobuf_c_boolean)wtrue;
	}

	if(luatable_hasvalue(L,"scale_y",1))
	{
		entity_data->scale_y = getluatable_value(L,"scale_y",1);
		entity_data->has_scale_y = (protobuf_c_boolean)wtrue;
	}

	if(luatable_hasvalue(L,"shader_params",1))
	{
		entity_data->n_parameters = getluatable_nestedtable(L,"shader_params",1);

		entity_data->parameters = (ShadersParameters**)wf_calloc(entity_data->n_parameters,sizeof(ShadersParameters*));

		for(uint32_t i_param = 0;i_param < entity_data->n_parameters ;i_param++)
		{
			getlua_rawtable(L,-1,i_param+1);

			ShadersParameters* cparam =  (ShadersParameters*)wf_malloc(sizeof(ShadersParameters));

			shaders_parameters__init(cparam);

			const char* param_name = getluatable_stringvalue(L,"param_name",-2);
			size_t size_param = strlen(param_name)+1;
			cparam->param_name = (char*)wf_malloc(size_param);

			w_strcpy(cparam->param_name,size_param,param_name);

			cparam->param_type = getluatableint_value(L,"param_type",-2);

			switch(cparam->param_type)
			{
				case TYPE_PARAM_SHADER__uniform_float:
					{
						cparam->uniform_float = (ShaderFloat*)wf_malloc(sizeof(ShaderFloat));
						shader_float__init(cparam->uniform_float);

						cparam->uniform_float->v1 = getluatable_value(L,"v1",-2);
						cparam->uniform_float->num_component = (int32_t)getluatable_value(L,"num_component",-2);

						if(cparam->uniform_float->num_component > 1)
						{
							cparam->uniform_float->v2 =  getluatable_value(L,"v2",-2);
							cparam->uniform_float->has_v2 = 1;
						}

						if(cparam->uniform_float->num_component > 2)
						{
							cparam->uniform_float->v3 =  getluatable_value(L,"v3",-2);
							cparam->uniform_float->has_v3 = 1;
						}

						if(cparam->uniform_float->num_component > 3)
						{
							cparam->uniform_float->v4 =  getluatable_value(L,"v4",-2);
							cparam->uniform_float->has_v4 = 1;
						}
					
					}
				break;

				case TYPE_PARAM_SHADER__texture:
					{
						cparam->uniform_texture = (ShaderTexture*)wf_malloc(sizeof(ShaderTexture));
						shader_texture__init(cparam->uniform_texture);

						const char* texture_id = getluatable_stringvalue(L,"texture_id",-2);
						size_t size_tex_id = strlen(texture_id)+1;
						cparam->uniform_texture->texture_id = (char*)wf_malloc(size_tex_id);

						w_strcpy(cparam->uniform_texture->texture_id,size_tex_id,texture_id);

						cparam->uniform_texture->bind_slot = getluatableint_value(L,"bind_slot",-2);

						if(luatable_hasvalue(L,"set_offset",-2))
						{
							cparam->uniform_texture->has_set_offset = 1;
							cparam->uniform_texture->set_offset = (int)getluatablebool_value(L,"set_offset",-2);
						}

						if(luatable_hasvalue(L,"set_size",-2))
						{
							cparam->uniform_texture->has_set_size = 1;
							cparam->uniform_texture->set_size = (int)getluatablebool_value(L,"set_size",-2);
						}	
					}
					break;

				case TYPE_PARAM_SHADER__light:
					{
						cparam->uniform_light = (ShaderLight*)wf_malloc(sizeof(ShaderLight));
						shader_light__init(cparam->uniform_light);

						const char* light_id = getluatable_stringvalue(L,"light_id",-2);
						size_t light_size = strlen(light_id)+1;
						cparam->uniform_light->light_id = (char*)wf_malloc(light_size);

						w_strcpy(cparam->uniform_light->light_id,light_size,light_id);

						cparam->uniform_light->normalized = (protobuf_c_boolean) getluatablebool_value(L,"normalized",-2);
					}

				break;

				case TYPE_PARAM_SHADER__constant:
					{
						cparam->uniform_const = (ShaderConst*)wf_malloc(sizeof(ShaderConst));

						shader_const__init(cparam->uniform_const);

						const char* const_name = getluatable_stringvalue(L,"const_name",-2);
						size_t const_size = strlen(const_name)+1;
						cparam->uniform_const->const_name = (char*)wf_malloc(const_size);

						w_strcpy(cparam->uniform_const->const_name,const_size,const_name);

						if(luatable_hasvalue(L,"extra_param",-2))
						{
							cparam->uniform_const->extra_param = string_alloc(getluatable_stringvalue(L,"extra_param",-2));
						}
					}

					break;

			}
			
			lua_pop(L,1);//pop shaders param

			entity_data->parameters[i_param] = cparam;

		}

		lua_pop(L,1);//pop lua shaders params table

	}
}
