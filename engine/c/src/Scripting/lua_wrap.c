#ifdef _MSC_VER
       extern "C" {
#endif
	#include "lauxlib.h"
#include "lualib.h"
	#include <lua.h>

#ifdef _MSC_VER
}
#endif

#include <Engine.h>

#include <Scripting/lua_wrap.h>

const char* w_lua_tostring(lua_State *L, int idx)
{
	return lua_tostring(L,idx);
}

const char* w_luaL_checkstring(lua_State* L,int numArg)
{
	return luaL_checkstring(L,numArg);
}

const char* w_lua_pushstring(lua_State* L,const char* s)
{
     #ifdef USE_LUA_JIT
          lua_pushstring(L,s);
          return "lua_jit";
     #else
     	return lua_pushstring(L,s);
     #endif
}

int w_luaL_loadbuffer(lua_State *L, const char *buff, size_t sz, const char *name)
{
	return luaL_loadbuffer(L,buff,sz,name);
}

int w_luaL_ref(lua_State *L, int t)
{
	return luaL_ref(L, t);
}

int  w_lua_pcall(lua_State *L, int nargs, int nresults, int errfunc)
{
	return lua_pcall(L, nargs, nresults, errfunc);
}

int w_lua_rawgeti(lua_State *L, int idx, lua_Integer n)
{
	return lua_rawgeti(L, idx, n);
}


int w_luaL_error(lua_State *L, const char *fmt, ...)
{
	va_list argptr;
	va_start(argptr, fmt);
	int ret = luaL_error(L,fmt,argptr);
	va_end(argptr);
	return ret;
}

void  w_lua_newtable(lua_State *L)
{
	lua_newtable(L);
}

void  w_lua_gettable (lua_State *L, int idx)
{
	lua_gettable(L,idx);
}

int  w_lua_isnumber (lua_State *L, int idx)
{
	return lua_isnumber(L,idx);
}

int   w_lua_istable (lua_State *L, int idx)
{
	return lua_istable(L,idx);
}

int w_lua_type(lua_State* L,int idx)
{
	return lua_type(L,idx);
}

void w_lua_pushboolean(lua_State *L, int b)
{
	lua_pushboolean(L,b);
}

void w_lua_pushcfunction(lua_State *L,lua_CFunction fn)
{
	lua_pushcfunction(L,fn);
}

void w_lua_pushnil(lua_State* L)
{
	lua_pushnil(L);
}

void w_lua_pushnumber(lua_State* L,lua_Number number)
{
	lua_pushnumber(L,number);
}

void w_lua_pushinteger(lua_State* L, lua_Integer integer)
{
	lua_pushinteger(L, integer);
}

void w_lua_getglobal(lua_State* L,const char* var)
{
	lua_getglobal(L,var);
}

void w_lua_setfield(lua_State* L,int idx,const char* k)
{
	lua_setfield(L,idx,k);
}

void w_lua_setglobal(lua_State* L,const char* var)
{
	lua_setglobal(L,var);
}

void w_lua_setmetatable(lua_State* L,int idx)
{
	lua_setmetatable(L,idx);
}

void w_lua_pop(lua_State* L,int idx)
{
	lua_pop(L,idx);
}

int w_lua_toboolean(lua_State* L,int idx)
{
	return lua_toboolean(L,idx);
}

lua_Number w_lua_tonumber(lua_State* L,int idx)
{
	return lua_tonumber(L,idx);
}

lua_Integer w_lua_tointeger(lua_State* L,int idx)
{
	return lua_tointeger(L,idx);
}


void* w_lua_touserdata(lua_State* L,int idx)
{
	return lua_touserdata(L,idx);
}

void w_lua_settop(lua_State* L,int idx)
{
	lua_settop(L,idx);
}

int w_lua_gettop(lua_State* L)
{
	return lua_gettop(L);
}

void w_luaL_traceback(lua_State *L, lua_State *L1,const char *msg, int level)
{
	luaL_traceback(L, L1, msg, level);
}

void w_luaL_unref(lua_State *L, int t, int ref)
{
	luaL_unref(L, t, ref);
}

lua_Integer w_lua_len(lua_State*L,int idx)
{
	return luaL_len(L, idx);
}
