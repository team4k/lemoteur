//TODO: lua_is* function are misleading and don't check variable type but if a variable can be converted to a type, replace
//all lua_is* function with lua_type(L,stack_pos) == <LUA_TYPE> to check the type


#ifdef _MSC_VER
    extern "C" {
#endif
        
#include "lauxlib.h"
#include "lualib.h"
#include "lua.h"
        
#ifdef _MSC_VER
 }
#endif

#if defined(_WIN32)
    #include <windows.h>
#else
	#ifndef __USE_BSD
        	#define __USE_BSD
	#endif
        #include <unistd.h>
        #include <dlfcn.h>
		#include <wchar.h>
		#include <pthread.h>
#endif

#if defined(EMSCRIPTEN)
	#include <emscripten.h>
#endif

#include <stdarg.h>
#include <string.h>
#include <inttypes.h>
#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#define __USE_MISC 1
#define _USE_MATH_DEFINES 1
#include <math.h>
#include <sqlite3.h>

#include <Engine.h>

#include <Render.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>
#include <Base/geom.h>

#include <Render/render_manager.h>

#include <GameObjects/Shaders.pb-c.h>
#include <Graphics/TextureLoader.h>
#include <Resx/shader_loader.h>

#include <Utils/Hash.h>
#include <Utils/hashmap.h>
#include <Utils/tempallocator.h>
#include <Utils/circular_array.h>

#include <GameObjects/Characters.pb-c.h>
#include <Graphics/Animation.h>
#include <Graphics/Sprite.h>
#include <Graphics/Mesh.h>

#include <Graphics/Light.h>
#include <Graphics/LightManager.h>

#include <Graphics/TextureRegion.h>
#include <Graphics/SpriteBatch.h>
#include <Debug/Logprint.h>


#include <Debug/geomdebug.h>

#include <GameObjects/Level.pb-c.h>
#include <Collisions/Collisions.h>

#include <chipmunk/chipmunk_private.h>
#include <chipmunk/chipmunk_unsafe.h>

#include <Physics/Physics.h>

#include <Resx/localization.h>


#include <Graphics/FontLoader.h>



#include <GameObjects/entity.h>
#include <GameObjects/Trigger.h>
#include <Font/RenderText.h>
#include <GameObjects/text_entity.h>

#include <Sound/Sound.h>

#include <Resx/Resources.h>

#include <Graphics/TilemapLayer.h>
#include <GameObjects/Tilemap.h>

#include <GameObjects/entity_group.h>
#include <Graphics/Effects/particle_object.h>
#include <GameObjects/particles_entity.h>

#include <Threading/thread_func.h>
#include <Threading/threads.h>

#include <Config/config.h>
#include <GameMngr/input_mngr.h>
#include <game_functions.h>
#include <GameMngr/module_mngr.h>
#include <GameObjects/Collider.h>
#include <GameObjects/waypoint.h>
#include <GameObjects/map.h>
#include <GameObjects/Levelobject.h>
#include <GameObjects/level_base.h>
#include <GameObjects/level_stream.h>

#include <Scripting/ScriptUtils.h>
#include <Scripting/ScriptLuaUtils.h>
#include <Scripting/ScriptLoader.h>
#include <Scripting/ScriptPointers.h>

static int entity_addentity(lua_State* L)
{
	Entity* entity_data = (Entity*)wf_calloc(1,sizeof(Entity));

	_protobuf_create_entity(L,entity_data);

	lua_getglobal(L,"mngr");

	lua_pop(L,1);

	lua_getglobal(L,"mngr");

	resources_manager_t* resx_mngr;

	lua_pushstring(L,"resx_mngr");
	lua_gettable(L,-2);

	resx_mngr = (resources_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	LEVEL_TYPE type = lua_lvlbase_gettype(L);

	lua_getglobal(L,"mngr");

	physics_manager_t* phy_mngr;

	lua_pushstring(L, (type == L_STATIC) ? "static_physics_mngr" : "stream_physics_mngr");
	lua_gettable(L,-2);

	phy_mngr = (physics_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	lua_getglobal(L,"mngr");

	render_manager_t* render_mngr;

	lua_pushstring(L,"render_mngr");
	lua_gettable(L,-2);

	render_mngr = (render_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	const char* map_id = lua_tostring(L,2);


	LEVEL_TYPE ctype = L_STATIC;

	void* lvl_mngr = lua_lvlbase_getmanager(L, 3, &ctype);

	wbool dynamic = wtrue;

	if (lua_isboolean(L, 4))
	{
		dynamic = (wbool)lua_toboolean(L, 4);
	}

	entity_t* outentity = NULL;

	if(ctype == L_STATIC)
	{
		//create a rogue entity id pointer, since the pb object pointer will be freed later on
		size_t entity_id_size = strlen(entity_data->entity_id) + 1;
		char* entity_id = (char*)wf_malloc(entity_id_size);
		w_strcpy(entity_id, entity_id_size, entity_data->entity_id);
		outentity = Levelobject_addentity2((Levelobject_t*)lvl_mngr, resx_mngr, phy_mngr, entity_id, entity_data, map_id, render_mngr, vectorzero, wfalse, dynamic);
	}
	else
	{
		outentity = level_stream_add_entity((level_stream_t*)lvl_mngr, NULL, resx_mngr, phy_mngr, entity_data->entity_id, entity_data, render_mngr, dynamic);
	}
		
	//run script only ctor, regular case is handled by the developer
	if (entity_data->has_script_init_only && outentity != NULL) {
		luaL_dostring(L, outentity->script_ctor);
	}
	


	entity__free_unpacked(entity_data,NULL);


	//wf_free(entity_data);

	return 1;
}

static int lua_entity_addcontactbody(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);
	const char* contact_name = lua_tostring(L,2);
	int32_t width_contact = (int32_t)lua_tointeger(L,3);
	int32_t height_contact = (int32_t)lua_tointeger(L,4);
	cpCollisionType col_type = (cpCollisionType)lua_tointeger(L,5);
	cpBitmask col_layer = LAYER_INACTIVE;
	cpBitmask mask = 0;

	if (lua_isinteger(L, 6)) {
		col_layer = (cpBitmask)lua_tointeger(L, 6);
	}

	if (lua_isinteger(L, 7)) {
		mask = (cpBitmask)lua_tointeger(L, 7);
	}

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,8);

	LEVEL_TYPE type = lua_lvlbase_gettype(L);

	lua_getglobal(L, "mngr");

	physics_manager_t* phy_mngr;

	lua_pushstring(L, (type == L_STATIC) ? "static_physics_mngr" : "stream_physics_mngr");
	lua_gettable(L,-2);

	phy_mngr = (physics_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	if(ent_obj)
		entity_addcontactbody(ent_obj,width_contact,height_contact,contact_name,phy_mngr,col_type,col_layer,mask);

	return 1;
}

static int lua_entity_removeallcontactbodies(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,2);

	LEVEL_TYPE type = lua_lvlbase_gettype(L);

	lua_getglobal(L, "mngr");

	physics_manager_t* phy_mngr;

	lua_pushstring(L, (type == L_STATIC) ? "static_physics_mngr" : "stream_physics_mngr");
	lua_gettable(L,-2);

	phy_mngr = (physics_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	if(ent_obj)
		entity_removeallcontactbodies(ent_obj,phy_mngr);

	return 1;
}


static int entity_getcontactpos(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);
	int32_t contact_indx = (int32_t)lua_tointeger(L,2);

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,3);

	sprite_t* first_sprite = entity_getsprite(ent_obj, 0);

	if(ent_obj && first_sprite->animated && first_sprite->current_anim && contact_indx != -1)
	{
		Vector_t pos = animation_getcontactpos_atframe(first_sprite->current_anim,contact_indx);

		pos.x *= first_sprite->scale_x;
		pos.y *= first_sprite->scale_y;

		//rotate if needed
		if(sprite_getrotation(first_sprite) != 0.0f)
		{
			Vector_t pos_center = {(first_sprite->texture_width * first_sprite->scale_x) * 0.5f,(first_sprite->texture_height * first_sprite->scale_y) * 0.5f};

			float rad_angle = atan2f(pos.y - pos_center.y,pos.x -pos_center.x);

			float radius = Vec_dist(pos_center,pos);

			float rad = deg2rad(sprite_getrotation(first_sprite));

			float final_rot = rad + rad_angle;

			if(final_rot >= M_PI * 2)
				final_rot = ((float)M_PI * 2.0f) - final_rot;
			else if(final_rot <= 0)
				final_rot = final_rot + ((float)M_PI * 2.0f);

			pos.x = radius * cosf(final_rot) + pos_center.x;
			pos.y = radius * sinf(final_rot) + pos_center.y;
		}


		//get the coordinates in world space
		if(first_sprite->mirror_texture_x)
			pos.x = (first_sprite->width * first_sprite->scale_x) - pos.x;

		if(first_sprite->mirror_texture_y)
			pos.y = (first_sprite->height * first_sprite->scale_y) - pos.y;

		cpVect ent_pos = entity_getposition(ent_obj);

		ent_pos.x +=  ent_obj->offsetx + first_sprite->current_anim->anim_def->sprite_offsetx;
		ent_pos.y +=  ent_obj->offsety + first_sprite->current_anim->anim_def->sprite_offsety;

		cpVect fpos = cpv((ent_pos.x + pos.x) - ((first_sprite->width * first_sprite->scale_x) * 0.5f), (ent_pos.y + pos.y) - ((first_sprite->height * first_sprite->scale_y) * 0.5f));

		 lua_newtable(L);
		 lua_pushnumber(L,fpos.x);
		 lua_setfield(L,-2,"x");
		 lua_pushnumber(L,fpos.y);
		 lua_setfield(L,-2,"y");

		 //luaL_getmetatable(L,"Vector");
		 lua_getglobal(L,"Vector");//we use an already defined metatable in lua, it's stored in a global in this case and not in the registry
		 lua_setmetatable(L,-2);
	}

	return 1;
}

static int entity_getzindex(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,2);

	if(ent_obj)
	{
		lua_pushnumber(L,ent_obj->z_order);
	}

	return 1;
}

static int entity_getmapid(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,2);

	if(ent_obj)
	{
		lua_pushstring(L,ent_obj->ref_map_id);
	}

	return 1;
}

static int lua_entity_setshaderparam_floatvalue(lua_State* L)
{
	int32_t stackpos = 0;
	wbool haspointerinstack = wfalse;
	
	if (lua_isinteger(L, 1)) {
		stackpos = 1;
		haspointerinstack = wtrue;
	}
	else if (lua_isnil(L, 1)) {
		stackpos = 1;
	}

	const char* entity_key = lua_tostring(L, stackpos+1);
	const char* shader_param = lua_tostring(L, stackpos+2);

	float shader_values[4] = {0};


	for(int32_t i = 0; i < 4;i++)
	{
		if (lua_isnumber(L, i + (3 + stackpos))) {
			shader_values[i] = (float)lua_tonumber(L, i + (3+ stackpos));
		}
		else {
			break;
		}
	}

	if (!haspointerinstack) {
		stackpos = lua_gettop(L) + 1;
	}

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key, stackpos);

	if(ent_obj)
		entity_setshaderparam_floatvalue(ent_obj,shader_param,shader_values);

	return 1;
}

static int lua_entity_setshaderparam_intvalue(lua_State* L)
{
	const char* entity_key = lua_tostring(L, 1);
	const char* shader_param = lua_tostring(L, 2);

	int shader_value = (int)lua_tointeger(L, 3);

	entity_t* ent_obj = lua_lvlbase_get_entity(L, entity_key, 4);

	if (ent_obj)
		entity_setshaderparam_intvalue(ent_obj, shader_param, shader_value);

	return 1;
}

static int lua_entity_setshaderparam_uniformarrayfloat(lua_State* L)
{
	int32_t stackpos = 0;
	wbool haspointerinstack = wfalse;

	if (lua_isinteger(L, 1)) {
		stackpos = 1;
		haspointerinstack = wtrue;
	}
	else if (lua_isnil(L, 1)) {
		stackpos = 1;
	}

	const char* entity_key = lua_tostring(L, stackpos + 1);
	const char* shader_param = lua_tostring(L, stackpos + 2);

	luaL_checktype(L, stackpos + 3, LUA_TTABLE);

	size_t arraylen = (size_t)luaL_len(L, stackpos + 3);

	if (arraylen > 256) {
		luaL_error(L, "entity_setshaderparam_arrayfloat Cant' have an array of more than 256 elements ");
		return 1;
	}

	//stack allocation to avoid multiple malloc in case we need regular updates of the uniform parameter
	float* shader_array = (float*)_alloca(arraylen * sizeof(float));
	
	for (uint16_t i = 1; i <= arraylen; i++)
	{
		lua_rawgeti(L, 3 + stackpos, i);

		shader_array[i-1] = (float)lua_tonumber(L, -1);

		lua_pop(L, 1);//pop float param
	}

	if (!haspointerinstack) {
		stackpos = lua_gettop(L) + 1;
	}

	entity_t* ent_obj = lua_lvlbase_get_entity(L, entity_key, stackpos);

	if (ent_obj)
		entity_setshaderparam_uniformarrayfloat(ent_obj, shader_param, shader_array,arraylen);

	return 1;
}


static int entity_removeentity(lua_State* L)
{
	const char* entity_id = lua_tostring(L,1);

	LEVEL_TYPE type = lua_lvlbase_gettype(L);

	lua_getglobal(L, "mngr");

	physics_manager_t* phy_mngr;

	lua_pushstring(L, (type == L_STATIC) ? "static_physics_mngr" : "stream_physics_mngr");
	lua_gettable(L,-2);

	phy_mngr = (physics_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	void* menu = lua_lvlbase_getmanager(L, 2, &type);

	if (type == L_STREAM) {
		luaL_error(L, "not implemented");
		return 1;
	}

	Levelobject_t* ent_mngr = (Levelobject_t*)menu;
		
	char* ent_ptr = NULL;

	if (hashtable_haskey(&ent_mngr->entities_hash, entity_id))
	{
		int av_indx = hashtable_index(&ent_mngr->entities_hash, entity_id);

		for (uint8_t ispr = 0; ispr < ent_mngr->entities[av_indx].sprite_count; ispr++) {
			sprite_t* spr = entity_getsprite(&ent_mngr->entities[av_indx], ispr);

			if (spr->script_id != 0) {
				script_unregisterpointer(spr->script_id);
				spr->script_id = 0;
			}
		}

		if (ent_mngr->entities[av_indx].mesh_obj != NULL) {
			if (ent_mngr->entities[av_indx].mesh_obj->script_id != 0) {
				script_unregisterpointer(ent_mngr->entities[av_indx].mesh_obj->script_id);
				ent_mngr->entities[av_indx].mesh_obj->script_id = 0;
			}
		}
	}

	ent_ptr = Levelobject_removeentity(ent_mngr,phy_mngr,entity_id,GT_ENTITY);

	wf_free(ent_ptr);

	return 1;
}

static int entity_setspritefreeze(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);
	wbool freezestate = (wbool)lua_toboolean(L,2);

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,3);

	if(ent_obj)
	{
		if(freezestate)
			entity_freezesprite(ent_obj);
		else
			entity_unfreezesprite(ent_obj);
	}

	return 1;
}

static int entity_setspritefreezepos(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);
	cpVect position = cpv(getluatable_value(L,"x",2),getluatable_value(L,"y",2));

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,3);

	if(ent_obj)
		ent_obj->freeze_pos = position;

	return 1;
}

static int entity_getspritefreezepos(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,2);


	if(ent_obj)
	{
		 lua_newtable(L);
		 lua_pushnumber(L,ent_obj->freeze_pos.x);
		 lua_setfield(L,-2,"x");
		 lua_pushnumber(L,ent_obj->freeze_pos.y);
		 lua_setfield(L,-2,"y");

		 lua_getglobal(L,"Vector");//we use an already defined metatable in lua, it's stored in a global in this case and not in the registry
		 lua_setmetatable(L,-2);
	}


	return 1;
}

static int entity_getspriteoffset(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,2);


	if(ent_obj)
	{
		 lua_newtable(L);
		 lua_pushnumber(L,ent_obj->offsetx);
		 lua_setfield(L,-2,"x");
		 lua_pushnumber(L,ent_obj->offsety);
		 lua_setfield(L,-2,"y");

		 lua_getglobal(L,"Vector");//we use an already defined metatable in lua, it's stored in a global in this case and not in the registry
		 lua_setmetatable(L,-2);
	}


	return 1;
}

static int entity_setspriteoffset(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);
	cpVect position = cpv(getluatable_value(L,"x",2),getluatable_value(L,"y",2));

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,3);


	if(ent_obj)
	{
		ent_obj->offsetx = (int32_t)position.x;
		ent_obj->offsety = (int32_t)position.y;
	}

	return 1;
}

static int entity_getsingleshapeoffset(lua_State* L)
{
	const char* entity_key = lua_tostring(L, 1);
	int shape_indx = (int)lua_tointeger(L, 2);

	entity_t* ent_obj = lua_lvlbase_get_entity(L, entity_key, 3);

	if (ent_obj && ent_obj->shape)
	{

		if (ent_obj->shape_num <= shape_indx)
			luaL_error(L, "invalid %d shape index for entity %s!", shape_indx, entity_key);
		else {

			cpBB box = cpShapeGetBB(ent_obj->shape[shape_indx]);

			cpVect boxcenter = cpBBCenter(box);
			cpVect origin = cpBodyGetPosition(cpShapeGetBody(ent_obj->shape[shape_indx]));

			cpVect currentoffset = cpvsub(boxcenter,origin);

			lua_newtable(L);
			lua_pushnumber(L, currentoffset.x);
			lua_setfield(L, -2, "x");
			lua_pushnumber(L, currentoffset.y);
			lua_setfield(L, -2, "y");

			lua_getglobal(L, "Vector");//we use an already defined metatable in lua, it's stored in a global in this case and not in the registry
			lua_setmetatable(L, -2);

		}
	}

	return 1;
}

static int entity_setsingleshapeoffset(lua_State* L)
{
	const char* entity_key = lua_tostring(L, 1);
	int shape_indx = (int)lua_tointeger(L, 2);
	cpVect position = cpv(getluatable_value(L, "x", 3), getluatable_value(L, "y", 3));

	entity_t* ent_obj = lua_lvlbase_get_entity(L, entity_key, 4);

	if (ent_obj && ent_obj->shape)
	{

		if (ent_obj->shape_num <= shape_indx)
			luaL_error(L, "invalid %d shape index for entity %s!", shape_indx, entity_key);
		else {
			//translate back from body position
			cpVect origin = cpBodyGetPosition(cpShapeGetBody(ent_obj->shape[shape_indx]));

			cpTransform trans = cpTransformTranslate(cpvadd(origin,position));

			cpBB box = cpShapeGetBB(ent_obj->shape[shape_indx]);

			cpVect boxcenter = cpBBCenter(box);

			cpVect verts[] = {
				cpvsub(cpvsub(cpv(box.l, box.b),boxcenter),origin),
				cpvsub(cpvsub(cpv(box.l, box.t),boxcenter),origin),
				cpvsub(cpvsub(cpv(box.r, box.t),boxcenter),origin),
				cpvsub(cpvsub(cpv(box.r, box.b),boxcenter),origin),
			};

			cpPolyShapeSetVerts(ent_obj->shape[shape_indx], 4, verts, trans);
		}
	}

	return 1;
}


static int entity_playanim(lua_State* L) 
{
 	const char* entity_key = lua_tostring(L,1);
	const char* animation = lua_tostring(L,2);

	uint8_t sprite_part = 0;
	wbool reset_flicker = wtrue;
	wbool reverse = wfalse;

	int stackpos = 3;

	if (lua_isinteger(L, 3)) {
		sprite_part = (uint8_t)lua_tointeger(L, 3);
		stackpos++;
	}

	if (lua_isboolean(L, 4)) {
		reset_flicker = (wbool)lua_toboolean(L, 4);
		stackpos++;
	}

	if (lua_isboolean(L, 5)) {
		reverse = (wbool)lua_toboolean(L, 5);
		stackpos++;
	}


	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key, stackpos);


	if(ent_obj)
		entity_playanimation3(ent_obj,animation, sprite_part,reset_flicker,reverse);


	return 1;

}


static int entity_setcollisionlayer(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);
	int layer_value = (int)lua_tointeger(L,2);
	wbool update_layer_ori = (wbool)lua_toboolean(L,3);

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,4);

	if(ent_obj && ent_obj->shape)
	{
        for(int32_t ishape = 0; ishape < ent_obj->shape_num;ishape++) {
			cpShapeFilter filter = cpShapeFilterNew(CP_NO_GROUP, layer_value, ent_obj->ori_mask);
			cpShapeSetFilter(ent_obj->shape[ishape], filter);
        }


		if(update_layer_ori)
			ent_obj->ori_layer = layer_value;
	}

	return 1;
}


static int entity_setsingleshapelayer(lua_State* L)
{
	const char* entity_key = lua_tostring(L, 1);
	int layer_value = (int)lua_tointeger(L, 2);
	int shape_indx = (int)lua_tointeger(L, 3);

	entity_t* ent_obj = lua_lvlbase_get_entity(L, entity_key, 4);

	if (ent_obj && ent_obj->shape)
	{

		if (ent_obj->shape_num <= shape_indx)
			luaL_error(L, "invalid %d shape index for entity %s!", shape_indx, entity_key);
		else {
			cpShapeFilter filter = cpShapeFilterNew(CP_NO_GROUP, layer_value, ent_obj->ori_mask);
			cpShapeSetFilter(ent_obj->shape[shape_indx], filter);
		}
	}

	return 1;
}



static int entity_setsingleshapelayerandmask(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);
	int layer_value = (int)lua_tointeger(L,2);
	int layer_mask = (int)lua_tointeger(L, 3);
	int shape_indx = (int)lua_tointeger(L,4);

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,5);

	if(ent_obj && ent_obj->shape)
	{

		if(ent_obj->shape_num <= shape_indx)
			luaL_error(L,"invalid %d shape index for entity %s!",shape_indx,entity_key);
        else {
			cpShapeFilter filter = cpShapeFilterNew(CP_NO_GROUP, layer_value, layer_mask);
			cpShapeSetFilter(ent_obj->shape[shape_indx], filter);
        }
	}

	return 1;
}

static int entity_setcollisionmask(lua_State* L)
{
	const char* entity_key = lua_tostring(L, 1);
	int mask_value = (int)lua_tointeger(L, 2);
	wbool update_mask_ori = (wbool)lua_toboolean(L, 3);

	entity_t* ent_obj = lua_lvlbase_get_entity(L, entity_key, 4);

	if (ent_obj && ent_obj->shape)
	{
		for (int32_t ishape = 0; ishape < ent_obj->shape_num; ishape++) {
			cpShapeFilter filter = cpShapeFilterNew(CP_NO_GROUP, ent_obj->ori_layer, mask_value);
			cpShapeSetFilter(ent_obj->shape[ishape], filter);
		}


		if (update_mask_ori)
			ent_obj->ori_mask = mask_value;
	}

	return 1;
}

static int entity_setsingleshapemask(lua_State* L)
{
	const char* entity_key = lua_tostring(L, 1);
	int mask_value = (int)lua_tointeger(L, 2);
	int shape_indx = (int)lua_tointeger(L, 3);

	entity_t* ent_obj = lua_lvlbase_get_entity(L, entity_key, 4);

	if (ent_obj && ent_obj->shape)
	{

		if (ent_obj->shape_num <= shape_indx)
			luaL_error(L, "invalid %d shape index for entity %s!", shape_indx, entity_key);
		else {
			cpShapeFilter filter = cpShapeFilterNew(CP_NO_GROUP, ent_obj->ori_layer, mask_value);
			cpShapeSetFilter(ent_obj->shape[shape_indx], filter);
		}
	}

	return 1;
}



static int entity_setzorder(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);
	int32_t new_zorder = (int32_t)lua_tointeger(L,2);

	LEVEL_TYPE type = L_STATIC;

	void* lvlbase = lua_lvlbase_getmanager(L, 3, &type);

	lvlbase_updatezorder(lvlbase,type,entity_key,new_zorder,GT_ENTITY);

	return 1;

}

static int entity_setcollisiontype(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);
	int col_value = (int)lua_tointeger(L,2);

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,3);


	if(ent_obj && ent_obj->shape)
	{
		for(int32_t ishape = 0; ishape < ent_obj->shape_num;ishape++)
			cpShapeSetCollisionType(ent_obj->shape[ishape],col_value);
	}

	return 1;
}

static int entity_setsingleshapecollisiontype(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);
	int col_value = (int)lua_tointeger(L,2);
	int shape_indx = (int)lua_tointeger(L,3);

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,4);

	if(ent_obj && ent_obj->shape)
	{
		if(ent_obj->shape_num <= shape_indx)
			luaL_error(L,"invalid %d shape index for entity %s!",shape_indx,entity_key);
		else
			cpShapeSetCollisionType(ent_obj->shape[shape_indx],col_value);
	}

	return 1;
}

static int entity_getcollisiontype(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);
	int shape_indx = (int)lua_tointeger(L,2);

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,3);

	if(ent_obj && ent_obj->shape)
	{
		if(ent_obj->shape_num <= shape_indx)
			luaL_error(L,"invalid %d shape index for entity %s!",shape_indx,entity_key);

        lua_pushnumber(L,ent_obj->shape[shape_indx]->filter.categories);
	}
	else
		lua_pushnumber(L,0);

	return 1;
}

static int entity_getsingleshapebox(lua_State* L)
{
	const char* entity_key = lua_tostring(L, 1);
	int shape_indx = (int)lua_tointeger(L, 2);

	entity_t* ent_obj = lua_lvlbase_get_entity(L, entity_key, 4);

	if (ent_obj && ent_obj->shape)
	{
		if (ent_obj->shape_num <= shape_indx)
			luaL_error(L, "invalid %d shape index for entity %s!", shape_indx, entity_key);
		else {
			cpBB bb = cpShapeGetBB(ent_obj->shape[shape_indx]);

			lua_newtable(L);
			lua_pushnumber(L, bb.r - bb.l);
			lua_setfield(L, -2, "width");
			lua_pushnumber(L, bb.t - bb.b);
			lua_setfield(L, -2, "height");

		}
			
	}

	return 1;
}



static int entity_getpos(lua_State* L)
{
    const char* entity_key = lua_tostring(L,1);

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,2);

	 if(ent_obj)
	 {
		 cpVect pos = entity_getposition(ent_obj);

		 lua_newtable(L);
		 lua_pushnumber(L,pos.x);
		 lua_setfield(L,-2,"x");
		 lua_pushnumber(L,pos.y);
		 lua_setfield(L,-2,"y");

		 lua_getglobal(L,"Vector");//we use an already defined metatable in lua, it's stored in a global in this case and not in the registry
		 lua_setmetatable(L,-2);
	 }
	
	return 1;
}

static int entity_getcontrolpos(lua_State* L)
{
    const char* entity_key = lua_tostring(L,1);

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,2);


	 if(ent_obj && ent_obj->controlBody)
	 {
         cpVect pos = cpBodyGetPosition(ent_obj->controlBody);

		 lua_newtable(L);
		 lua_pushnumber(L,pos.x);
		 lua_setfield(L,-2,"x");
		 lua_pushnumber(L,pos.y);
		 lua_setfield(L,-2,"y");

		 lua_getglobal(L,"Vector");//we use an already defined metatable in lua, it's stored in a global in this case and not in the registry
		 lua_setmetatable(L,-2);
	 }
	
	return 1;
}

static int entity_setcontrolpos(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);
	

	if(!lua_istable(L,2))
	{
		luaL_error(L,"The second parameter of entity_setcontrolpos is not a table!"); 
		return 1;
	}

	cpVect position = cpv(getluatable_value(L,"x",2),getluatable_value(L,"y",2));

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,3);

	if(ent_obj && ent_obj->controlBody)
	 {
         cpBodySetPosition(ent_obj->controlBody,position);
	 }
	
	return 1;
}

static int entity_setpos(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);
	

	if(!lua_istable(L,2))
	{
		luaL_error(L,"The second parameter of entity_setpos is not a table!"); 
		return 1;
	}

	cpVect position = cpv(getluatable_value(L,"x",2),getluatable_value(L,"y",2));

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,3);

	if(ent_obj)
		entity_setposition(ent_obj,(float)position.x,(float)position.y);
	
	return 1;
}


static int entity_setelast(lua_State* L)
{
    const char* entity_key = lua_tostring(L,1);
	float elast = (float)lua_tonumber(L,2);

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,3);

	if(ent_obj && ent_obj->shape)
	{
		for(int32_t ishape = 0; ishape < ent_obj->shape_num;ishape++)
			cpShapeSetElasticity(ent_obj->shape[ishape],elast);
	}

	   return 1;
}

static int entity_setfriction(lua_State* L)
{
	 const char* entity_key = lua_tostring(L,1);
	float friction = (float)lua_tonumber(L,2);

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,3);

	 if(ent_obj && ent_obj->shape)
	 {
		for(int32_t ishape = 0; ishape < ent_obj->shape_num;ishape++)
			cpShapeSetFriction(ent_obj->shape[ishape],friction);
	 }

	   return 1;
}

static int entity_setmass(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);
	float mass = 0;

	

	if(lua_isnumber(L,2))
		mass = (float)lua_tonumber(L,2);

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,3);


	 if(ent_obj && ent_obj->body)
	 {
		 cpBodySetMass(ent_obj->body,mass);

		 cpFloat momentofInertia = 0;

		 float width = 0.0f;
		 float height = 0.0f;

		 if(ent_obj->sprite)
		 {
			 sprite_t* fsprite = entity_getsprite(ent_obj, 0);

			 width = (float)fsprite->width;
			 height = (float)fsprite->height;
		 }
		 else
		 {
			 width = (float)ent_obj->mesh_obj->width;
			 height = (float)ent_obj->mesh_obj->height;
		 }


		 if(ent_obj->shape_type == TYPE_PHYSICS__CIRCLE)
		 {
			 momentofInertia = cpMomentForCircle(mass,0,fmax(width,height),cpvzero);
		 }
		 else if(ent_obj->shape_type == TYPE_PHYSICS__BOX)
		 {
			 momentofInertia = cpMomentForBox(mass,width,height);
		 }
		 else if(ent_obj->shape_type == TYPE_PHYSICS__U_SHAPE)
		 {
			 momentofInertia = cpMomentForBox(mass,width,height);
		 }
		 else if(ent_obj->shape_type == TYPE_PHYSICS__MULTI_SHAPES)
		 {
			 momentofInertia = cpMomentForBox(mass,width,height);
		 }

		
		 cpBodySetMoment(ent_obj->body,momentofInertia);
	 }

	   return 1;
}


static int entity_setmoment(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);
	float moment = 0;

	

	if(lua_isnumber(L,2))
		moment = (float)lua_tonumber(L,2);
	else if(lua_isstring(L,2))
	{
		if(strcmp(lua_tostring(L,2),"INF") == 0)
			moment = INFINITY;
	}

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,3);

	 if(ent_obj && ent_obj->body)
	 {
		 cpBodySetMoment(ent_obj->body,moment);
	 }

	   return 1;
}

static int entity_setmaxvel(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);
	float maxvel = 0;

	if(lua_isnumber(L,2))
		maxvel = (float)lua_tonumber(L,2);
	else if(lua_isstring(L,2))
	{
		if(strcmp(lua_tostring(L,2),"INF") == 0)
			maxvel = INFINITY;
	}


	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,3);

	 if(ent_obj && ent_obj->body)
	 {
         //TODO : cpBodySetVelLimit has been removed. To emulate it, we need to implement a custom velocity
         //function https://chipmunk-physics.net/forum/viewtopic.php?f=1&t=6962
        // cpBodySetVelLimit(ent_obj->body,maxvel);

         //if(ent_obj->controlBody)
            // cpBodySetVelLimit(ent_obj->controlBody,maxvel);
	 }

	   return 1;
}

static int entity_setmaxangvel(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);
	float maxvelang = (float)lua_tonumber(L,2);

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,3);

    //see max vel
     //if(ent_obj && ent_obj->body)
        // cpBodySetAngVelLimit(ent_obj->body,maxvelang);

	   return 1;
}

static int entity_applyimpulse(lua_State* L)
{
    const char* entity_key = lua_tostring(L,1);

	if(!lua_istable(L,2))
	{
		luaL_error(L,"The second parameter of entity_applyimpulse is not a table!"); 
		return 1;
	}

	cpVect impulse = cpv(getluatable_value(L,"x",2),getluatable_value(L,"y",2));

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,3);

	 if(ent_obj && ent_obj->body)
         cpBodyApplyImpulseAtLocalPoint(ent_obj->body,impulse,cpvzero);

	   return 1;
}


static int entity_setvel(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);

	if(!lua_istable(L,2))
	{
		luaL_error(L,"The second parameter of entity_setvel is not a table!"); 
		return 1;
	}

	cpVect velocity = cpv(getluatable_value(L,"x",2),getluatable_value(L,"y",2));

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,3);

	 if(ent_obj && ent_obj->body)
	 {
        cpBodySetVelocity(ent_obj->body,velocity);
	 }

	
	return 1;
}

static int entity_setcontrolvel(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);

	if(!lua_istable(L,2))
	{
		luaL_error(L,"The second parameter of entity_setvel is not a table!"); 
		return 1;
	}

	cpVect velocity = cpv(getluatable_value(L,"x",2),getluatable_value(L,"y",2));

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,3);

	if(ent_obj && ent_obj->body && ent_obj->controlBody)
	 {
        cpBodySetVelocity(ent_obj->controlBody,velocity);
	 }

	
	return 1;
}
static int entity_getvel(lua_State* L)
{
    const char* entity_key = NULL;

    entity_key = lua_tostring(L,1);

	 entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,2);

	 if(ent_obj && ent_obj->body)
	 {
		 lua_newtable(L);
		 lua_pushnumber(L, ent_obj->body->v.x);
		 lua_setfield(L,-2,"x");
		 lua_pushnumber(L,ent_obj->body->v.y);
		 lua_setfield(L,-2,"y");

		 lua_getglobal(L,"Vector");//we use an already defined metatable in lua, it's stored in a global in this case and not in the registry
		 lua_setmetatable(L,-2);
	 }
	
	return 1;
}

static int entity_getcontrolvel(lua_State* L)
{
    const char* entity_key = NULL;

    entity_key = lua_tostring(L,1);

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,2);

	if(ent_obj && ent_obj->controlBody)
	{
		lua_newtable(L);
		lua_pushnumber(L, ent_obj->controlBody->v.x);
		lua_setfield(L,-2,"x");
		lua_pushnumber(L,ent_obj->controlBody->v.y);
		lua_setfield(L,-2,"y");

		lua_getglobal(L,"Vector");//we use an already defined metatable in lua, it's stored in a global in this case and not in the registry
		lua_setmetatable(L,-2);
	}
	
	return 1;
}

static int entity_getbox(lua_State* L)
{
	const char* entity_key = NULL;

    entity_key = lua_tostring(L,1);

    entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,2);


	if(ent_obj)
	{
		Rect_t* rnd_box = entity_getrenderbox(ent_obj);

		lua_newtable(L);
		lua_pushnumber(L, rnd_box->width);
		lua_setfield(L,-2,"width");
		lua_pushnumber(L, rnd_box->height);
		lua_setfield(L,-2,"height");
	}
	
	return 1;
}

static int entity_getaabb(lua_State* L)
{
	const char* entity_key = NULL;

	entity_key = lua_tostring(L, 1);

	entity_t* ent_obj = lua_lvlbase_get_entity(L, entity_key, 2);


	if (ent_obj)
	{
		Rect_t* rnd_box = entity_getrenderbox(ent_obj);

		lua_newtable(L);
		lua_pushnumber(L, rnd_box->position.x);
		lua_setfield(L, -2, "left");
		lua_pushnumber(L, rnd_box->position.x + rnd_box->width);
		lua_setfield(L, -2, "right");
		lua_pushnumber(L, rnd_box->position.y);
		lua_setfield(L, -2, "top");
		lua_pushnumber(L, rnd_box->position.y - rnd_box->height);
		lua_setfield(L, -2, "bottom");
	}

	return 1;
}

static int entity_noresetflag(lua_State* L)
{
	const char* entity_key = NULL;

    entity_key = lua_tostring(L,1);

	wbool entity_reset_flag = (wbool)lua_toboolean(L,2);

    entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,3);



	if(ent_obj)
	{
		ent_obj->no_reset = entity_reset_flag;
	}

	return 1;
}


static int entity_exist(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);

	wbool result = lua_lvlbase_has_entity(L, entity_key, 2);

	lua_pushboolean(L,(int)result);
	return 1;
}


static int entity_setvisible(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);
	wbool visible = (wbool)lua_toboolean(L,2);

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,3);

	if(ent_obj)
		ent_obj->is_visible = visible;

	return 1;
}

static int lua_entity_checkrender(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,2);

	lua_getglobal(L,"mngr");

	render_manager_t* render_mngr;

	lua_pushstring(L,"render_mngr");
	lua_gettable(L,-2);

	render_mngr = (render_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	if(ent_obj)
		lua_pushboolean(L,(int)entity_checkrender(ent_obj,render_mngr->screen_info));
	else
		lua_pushboolean(L,0);

	return 1;
}

static int entity_movmaxbias(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);
	float maxbias = 0.0f;

	if(lua_isnumber(L,2))
		maxbias = (float)lua_tonumber(L,2);
	else if(lua_isstring(L,2))
	{
		if(strcmp(lua_tostring(L,2),"INF") == 0)
			maxbias = INFINITY;
	}

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,3);

	if(ent_obj)
	{
		if(ent_obj->move_constraint)
		{
			cpConstraintSetMaxBias(ent_obj->move_constraint, maxbias); //joint correction
		}
			
	}

	return 1;
}

static int entity_movmaxforce(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);
	float maxforce = 0;


	if(lua_isnumber(L,2))
		maxforce = (float)lua_tonumber(L,2);
	else if(lua_isstring(L,2))
	{
		const char* str = lua_tostring(L,2);

		if(strcmp(str,"INF") == 0)
			maxforce = INFINITY;
	}

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,3);

	if(ent_obj)
	{
		if(ent_obj->move_constraint)
			cpConstraintSetMaxForce(ent_obj->move_constraint, maxforce); //joint correction
	}

	return 1;
}

static int entity_movmaxerror(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);

	float maxerror = (float)lua_tonumber(L,2);

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,3);

	if(ent_obj && ent_obj->move_constraint)
	{
		cpConstraintSetErrorBias(ent_obj->move_constraint,maxerror);
	}

	return 1;
}

static int entity_linkerrorrateleft(lua_State* L)
{
	const char* entity_id = lua_tostring(L,1);
	cpFloat errorbias = (wfloat)lua_tonumber(L,2);

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_id,3);


	if(ent_obj != NULL && ent_obj->link_constraint != NULL)
	{
		cpConstraintSetErrorBias(ent_obj->link_constraint,errorbias);
	}

	return 1;
}

static int entity_linkmaxbias(lua_State* L)
{
	const char* entity_id = lua_tostring(L,1);
	float maxbias = 0.0f;

	if(lua_isnumber(L,2))
		maxbias = (float)lua_tonumber(L,2);
	else if(lua_isstring(L,2))
	{
		const char* str = lua_tostring(L,2);

		if(strcmp(str,"INF") == 0)
			maxbias = INFINITY;
	}

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_id,3);


	if(ent_obj != NULL && ent_obj->link_constraint != NULL)
	{
		cpConstraintSetMaxBias(ent_obj->link_constraint,maxbias);
	}

	return 1;
}

static int entity_linkmaxforce(lua_State* L)
{
	const char* entity_id = lua_tostring(L,1);
	float maxforce = 0.0f;

	if(lua_isnumber(L,2))
		maxforce = (float)lua_tonumber(L,2);
	else if(lua_isstring(L,2))
	{
		const char* str = lua_tostring(L,2);

		if(strcmp(str,"INF") == 0)
			maxforce = INFINITY;
	}

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_id,3);


	if(ent_obj != NULL && ent_obj->link_constraint != NULL)
	{
		cpConstraintSetMaxForce(ent_obj->link_constraint,maxforce);
	}

	return 1;
}



static int entity_registercallback(lua_State* L)
{
    const char* entity_key = NULL;
	const char* script_func = NULL;
	const char* func_key = NULL;

    entity_key = lua_tostring(L,1);
	script_func = lua_tostring(L,2);
	func_key = lua_tostring(L,3);

	uint32_t menu_id = 0;
	entity_t* ent_obj  = NULL;

	if(lua_isinteger(L,4))
	{
		menu_id = (uint32_t)lua_tointeger(L,4);
	}

	ent_obj = lua_lvlbase_get_entity(L, entity_key, 4);
		

	 if(ent_obj)
	 {
		 if(ent_obj->callback_hash == NULL)
		{
			luaL_error(L,"The entity '%s' can't have any callback functions! set the callback chekbox to true",entity_key); 
			return 1;
		}

		lua_getglobal(L,"mngr");

		void  (*callback_func)(const char* id, const char* func,uint32_t parent,const char* params,void* return_value,size_t return_size);

		lua_pushstring(L,"callback_entity");
		lua_gettable(L,-2);

		callback_func = (void (*)(const char* id,const char* func, uint32_t parent,const char* params,void* return_value,size_t return_size))lua_touserdata(L,-1);

		lua_pop(L,1);

		entity_addcallback(ent_obj,func_key,script_func,callback_func, menu_id);
	 }

	 return 1;
}

static int lua_entity_hascallback(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,2);

	if(ent_obj)
	{
		lua_pushboolean(L,(ent_obj->callback_hash != NULL));
	}
	else
	{
		lua_pushboolean(L,0);
	}

	return 1;
}

static int lua_entity_clearcallback(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,2);

	if(ent_obj)
		entity_clearcallback(ent_obj);

	return 1;
}



static int entity_enableconstraint(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,2);


	LEVEL_TYPE type = lua_lvlbase_gettype(L);

	lua_getglobal(L, "mngr");

	physics_manager_t* phy_mngr;

	lua_pushstring(L, (type == L_STATIC) ? "static_physics_mngr" : "stream_physics_mngr");
	lua_gettable(L,-2);

	phy_mngr = (physics_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);


	if(ent_obj)
	{
		entity_set_constraint(ent_obj,phy_mngr,wtrue);
	}

	return 1;
}

static int entity_disableconstraint(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,2);

     
          
	LEVEL_TYPE type = lua_lvlbase_gettype(L);

	lua_getglobal(L, "mngr");

	physics_manager_t* phy_mngr;

	lua_pushstring(L, (type == L_STATIC) ? "static_physics_mngr" : "stream_physics_mngr");
	lua_gettable(L,-2);

	phy_mngr = (physics_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	if(ent_obj)
	{
		entity_set_constraint(ent_obj,phy_mngr,wfalse);
	}

	return 1;
}

static int lua_entity_setanchorpoint(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);

	if(!lua_istable(L,2))
	{
		luaL_error(L,"The second parameter of entity_setanchorpoint is not a table!"); 
		return 1;
	}

	cpVect anchor = cpv(getluatable_value(L,"x",2),getluatable_value(L,"y",2));

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,3);

     
          

	 if(ent_obj)
		 entity_setanchorpoint(ent_obj,(float)anchor.x,(float)anchor.y);

	return 1;
}

static int lua_entity_movetopos(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);

	if(!lua_istable(L,2))
	{
		luaL_error(L,"The second parameter of entity_setanchorpoint is not a table!"); 
		return 1;
	}

	cpVect pos = cpv(getluatable_value(L,"x",2),getluatable_value(L,"y",2));

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,3);

     
          

	 if(ent_obj)
		 entity_movetopos(ent_obj,(float)pos.x,(float)pos.y);

	return 1;
}

static int entity_getlinkname(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);
	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,2);

     
          

	if(ent_obj)
	{
		if(ent_obj->link_constraint_activated)
			lua_pushstring(L,ent_obj->linked_to_id);
		else
			lua_pushnil(L);
	}

	return 1;
}

static int lua_entity_linkto(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);
	const char* entity_link = lua_tostring(L,2);
	float min_distance = (float)lua_tonumber(L,3);
	float max_distance = (float)lua_tonumber(L,4);

	Vector_t vec_anchr1 = Vec(getluatable_value(L,"x",5),getluatable_value(L,"y",5));
	Vector_t vec_anchr2 = Vec(getluatable_value(L,"x",6),getluatable_value(L,"y",6));


	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,7);

    
          


	entity_t* ent_link_obj = lua_lvlbase_get_entity(L,entity_link,7);

	LEVEL_TYPE type = lua_lvlbase_gettype(L);

	lua_getglobal(L, "mngr");

	physics_manager_t* phy_mngr;

	lua_pushstring(L, (type == L_STATIC) ? "static_physics_mngr" : "stream_physics_mngr");
	lua_gettable(L,-2);

	phy_mngr = (physics_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	if(ent_obj && ent_link_obj)
	{
		entity_linkto(ent_obj,ent_link_obj,phy_mngr,wtrue,min_distance,max_distance,vec_anchr1,vec_anchr2);
	}
	else
	{
		if(!ent_obj)
			logprint("[entity_linkto] entity_id is not valid!");

		if(!ent_link_obj)
			logprint("[entity_linkto] entity_tolinkto is not valid!");


	}

	return 1;
}

static int lua_entity_pintoworld(lua_State* L)
{
	const char* entity_key = lua_tostring(L, 1);
	
	cpFloat maxforce = lua_tonumber(L, 2);
	cpVect vec_anchr1 = cpv(getluatable_value(L, "x", 3), getluatable_value(L, "y", 3));
	cpVect vec_anchr2 = cpv(getluatable_value(L, "x", 4), getluatable_value(L, "y", 4));


	entity_t* ent_obj = lua_lvlbase_get_entity(L, entity_key, 5);

	LEVEL_TYPE type = lua_lvlbase_gettype(L);

	lua_getglobal(L, "mngr");

	physics_manager_t* phy_mngr;

	lua_pushstring(L, (type == L_STATIC) ? "static_physics_mngr" : "stream_physics_mngr");
	lua_gettable(L, -2);

	phy_mngr = (physics_manager_t*)lua_touserdata(L, -1);

	lua_pop(L, 1);

	if (ent_obj)
	{
		entity_pintoworld(ent_obj, phy_mngr, vec_anchr1, vec_anchr2, wtrue);
	}
	else
	{
		logprint("[entity_pintoworld] entity_id is not valid!");
	}

	return 1;
}

static int lua_entity_removepin(lua_State* L)
{
	const char* entity_key = lua_tostring(L, 1);

	entity_t* ent_obj = lua_lvlbase_get_entity(L, entity_key, 2);

	LEVEL_TYPE type = lua_lvlbase_gettype(L);

	lua_getglobal(L, "mngr");

	physics_manager_t* phy_mngr;

	lua_pushstring(L, (type == L_STATIC) ? "static_physics_mngr" : "stream_physics_mngr");
	lua_gettable(L, -2);

	phy_mngr = (physics_manager_t*)lua_touserdata(L, -1);

	lua_pop(L, 1);

	if (ent_obj)
	{
		entity_removepin(ent_obj, phy_mngr);
	}
	else
	{
		logprint("[entity_removepin] entity_id is not valid!");
	}

	return 1;

}

static int entity_unlink(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,2);


	LEVEL_TYPE type = lua_lvlbase_gettype(L);

	lua_getglobal(L, "mngr");

	physics_manager_t* phy_mngr;

	lua_pushstring(L, (type == L_STATIC) ? "static_physics_mngr" : "stream_physics_mngr");
	lua_gettable(L,-2);

	phy_mngr = (physics_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	if(ent_obj)
	{
		if(ent_obj->link_constraint_activated)
			entity_linkto(ent_obj,NULL,phy_mngr,wfalse,-1,-1,vectorzero,vectorzero);
	}

	return 1;
}

static int entity_relink(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,2);

	LEVEL_TYPE type = lua_lvlbase_gettype(L);

	lua_getglobal(L, "mngr");

	physics_manager_t* phy_mngr;

	lua_pushstring(L, (type == L_STATIC) ? "static_physics_mngr" : "stream_physics_mngr");
	lua_gettable(L,-2);

	phy_mngr = (physics_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	if(ent_obj)
	{
		if(!ent_obj->link_constraint_activated && ent_obj->link_constraint)
			entity_linkto(ent_obj,NULL,phy_mngr,wtrue,-1,-1,vectorzero,vectorzero);
	}

	return 1;
}


static int entity_setframe(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);
	int32_t frame = (int32_t)lua_tointeger(L,2);
	int32_t position = (int32_t)lua_tointeger(L,3);

	entity_t* entity_obj = lua_lvlbase_get_entity(L,entity_key,4);

	
		

	if (entity_obj && entity_obj->sprite) {
		sprite_t* fsprite = entity_getsprite(entity_obj, 0);

		sprite_setframe(fsprite, frame, position);
	}
		

	return 1;
}

static int entity_fade(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);
	float entity_fade_time = (float)lua_tonumber(L,2);
	int fade_direction = (int)lua_tointeger(L,3);

	entity_t* entity_obj = lua_lvlbase_get_entity(L,entity_key,4);

    
		

	if (entity_obj && entity_obj->sprite) {
		sprite_t* fsprite = entity_getsprite(entity_obj, 0);
		sprite_fade(fsprite, entity_fade_time, fade_direction);
	}
		

	return 1;
}

static int entity_getfade(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);

	entity_t* entity_obj = lua_lvlbase_get_entity(L,entity_key,2);

    
		

	if(entity_obj && entity_obj->sprite)
	{
		sprite_t* fsprite = entity_getsprite(entity_obj, 0);
		lua_pushboolean(L, fsprite->dofade);
	}
	else
	{
		lua_pushnil(L);
	}

	return 1;
}

static int entity_endfade(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);

	entity_t* entity_obj = lua_lvlbase_get_entity(L,entity_key,2);

    
		

	if (entity_obj && entity_obj->sprite)
	{
		sprite_t* fsprite = entity_getsprite(entity_obj, 0);
		fsprite->dofade = wfalse;
	}
		

	return 1;
}

static int lua_entity_startrotation(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);
	float rot_radius = (float)lua_tonumber(L,2);
	int rot_direction = (int)lua_tointeger(L,3);
	float start_rotation = (float)lua_tonumber(L,4);
	float speed = (float)lua_tonumber(L,5);

	if(!lua_istable(L,6))
	{
		luaL_error(L,"The sixth parameter of lua_entity_startrotation is not a Vector!"); 
		return 1;
	}

	cpVect base_point = cpv(getluatable_value(L,"x",6),getluatable_value(L,"y",6));

	entity_t* entity_obj = lua_lvlbase_get_entity(L,entity_key,7);

    
		

	if(entity_obj)
		entity_startrotation(entity_obj,base_point,rot_radius,rot_direction,start_rotation,speed);

	return 1;
}

static int lua_entity_endrotation(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);
	entity_t* entity_obj = lua_lvlbase_get_entity(L,entity_key,2);

    
		

	if(entity_obj)
		entity_endrotation(entity_obj);

	return 1;
}

static int lua_entity_getrotation(lua_State* L)
{
	const char* entity_key = lua_tostring(L, 1);

	entity_t* entity_obj = lua_lvlbase_get_entity(L, entity_key, 2);

	if (entity_obj)
	{
		float rot = entity_getrotation(entity_obj);
		lua_pushnumber(L, rot);
	}
	else 
	{
		lua_pushnil(L);
	}

	return 1;
}

static int lua_entity_setrotation(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);
	float rotation = (float)lua_tonumber(L,2);

	entity_t* entity_obj = lua_lvlbase_get_entity(L,entity_key,3);

	if(entity_obj)
		entity_setrotation(entity_obj,rotation);


	return 1;
}

static int lua_entity_setspriterotationlink(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);
	wbool set_link = (wbool)lua_toboolean(L,2);

	entity_t* entity_obj = lua_lvlbase_get_entity(L,entity_key,3);

    
		


	if(entity_obj)
		entity_obj->set_sprite_rotation = set_link;

	return 1;
}

static int lua_entity_settexture(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);
	const char* entity_texture = lua_tostring(L,2);

	entity_t* entity_obj = lua_lvlbase_get_entity(L,entity_key,3);

    
		

	resources_manager_t* resx_mngr;

	lua_pushstring(L,"resx_mngr");
	lua_gettable(L,-2);

	resx_mngr = (resources_manager_t*)lua_touserdata(L,-1);


	if(entity_obj)
	{
		texture_info* new_texture = gettexture_resx(resx_mngr,entity_texture);

		if(new_texture == NULL)
		{
			luaL_error(L,"[entity_settexture] Can't find texture %s in the resources list!",entity_texture);
		}
		else
		{
			entity_settexture(entity_obj,new_texture);
		}
	}

	return 1;
}

static int entity_getspritepointer(lua_State* L)
{
	const char* entity_id = lua_tostring(L,1);

	uint8_t sprite_part = 0;

	int stackpos = 2;

	if (lua_isinteger(L, 2)) {
		sprite_part = (uint8_t)lua_tointeger(L, 2);
		stackpos++;
	}

	entity_t* entity_obj = lua_lvlbase_get_entity(L,entity_id, stackpos);

	if(entity_obj)
	{
		sprite_t* esprite = entity_getsprite(entity_obj, sprite_part);

		if(esprite)
		{
			//char sprite_ptr[128];
			if (esprite->script_id == 0) {
				esprite->script_id = script_registerpointer((intptr_t)esprite,SPRITE);
			}

			lua_pushinteger(L, esprite->script_id);
		}
		else
		{
			luaL_error(L,"[entity_getspritepointer] Can't get a sprite pointer on an entity without a sprite! %s",entity_id);
			lua_pushnil(L);
		}
	}
	else
	{

		lua_pushnil(L);
	}

	return 1;
}

static int lua_entity_getnumsprite(lua_State* L)
{
	const char* entity_id = lua_tostring(L, 1);

	entity_t* entity_obj = lua_lvlbase_get_entity(L, entity_id, 2);

	if (entity_obj)
	{
		uint8_t numsprite = entity_getnumsprite(entity_obj);
		lua_pushinteger(L, numsprite);
		
	}
	else
	{
		lua_pushinteger(L, 0);
	}

	return 1;
}

static int entity_getmeshpointer(lua_State* L)
{
	const char* entity_id = lua_tostring(L,1);

	entity_t* entity_obj = lua_lvlbase_get_entity(L,entity_id,2);

	
		

	if(entity_obj)
	{
		if(entity_obj->mesh_obj)
		{
			if (entity_obj->mesh_obj->script_id == 0) {
				entity_obj->mesh_obj->script_id = script_registerpointer((intptr_t)entity_obj->mesh_obj,MESH);
			}

			lua_pushinteger(L, entity_obj->mesh_obj->script_id);
		}
		else
		{
			luaL_error(L,"[entity_getmeshpointer] Can't get a mesh pointer on an entity without a mesh! %s",entity_id);
			lua_pushnil(L);
		}
	}
	else
	{

		lua_pushnil(L);
	}

	return 1;
}

static int lua_entity_resetrotation(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);
	entity_t* entity_obj = lua_lvlbase_get_entity(L,entity_key,2);

    
		

	if(entity_obj)
		entity_resetrotation(entity_obj);


	return 1;
}


static int entity_resetentity(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);

	entity_t* ent = lua_lvlbase_get_entity(L,entity_key,2);

	LEVEL_TYPE type = lua_lvlbase_gettype(L);

	lua_getglobal(L, "mngr");

	physics_manager_t* phy_mngr;

	lua_pushstring(L, (type == L_STATIC) ? "static_physics_mngr" : "stream_physics_mngr");
	lua_gettable(L,-2);

	phy_mngr =  (physics_manager_t*)lua_touserdata(L,-1) ;

	lua_pop(L,1);


	if(ent)
	{
		if(ent->static_ent)
		{
			entity_setposition(ent,(float)ent->start_position.x,(float)ent->start_position.y);
		}
		else
		{
            cpBodySetPosition(ent->body,ent->start_position);
			cpBodySetAngle(ent->body,0);
            cpBodySetAngularVelocity(ent->body,0);
            cpBodySetVelocity(ent->body,cpvzero);

            for(int32_t ishape = 0; ishape < ent->shape_num;ishape++) {
                ent->shape[ishape]->filter.categories = ent->shape[ishape]->filter.mask = ent->ori_layer;
            }

			if (ent->set_sprite_rotation) {
				sprite_t* fsprite = entity_getsprite(ent, 0);
				sprite_setrotation(fsprite, 0.0f);
			}
				

			ent->is_visible = wtrue;

			entity_setposition(ent,(float)ent->start_position.x,(float)ent->start_position.y);

			if(ent->controlBody)
			{
                cpBodySetVelocity(ent->controlBody,cpvzero);
                cpBodySetPosition(ent->controlBody,ent->start_position);

				entity_set_constraint(ent,phy_mngr,wtrue);
			}
		}
	}

	return 1;
}

static int entity_setgravityenable(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);

	wbool enable = (wbool)lua_toboolean(L,2);

	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_key,3);	

	if(ent_obj && ent_obj->body)
	{
		if (!enable)
			cpBodySetVelocityUpdateFunc(ent_obj->body, no_gravity);
		else
			cpBodySetVelocityUpdateFunc(ent_obj->body, cpBodyUpdateVelocity);
	}

	return 1;
}

static int lua_entity_lockwhenanim(lua_State* L)
{
	const char* entity_key = lua_tostring(L, 1);
	const char* anim_key = lua_tostring(L, 2);
	uint8_t sprite_part = (uint8_t)lua_tointeger(L, 3);

	entity_t* ent_obj = lua_lvlbase_get_entity(L, entity_key, 4);

	if(ent_obj)
		entity_lockwhenanim(ent_obj, anim_key, sprite_part);

	return 1;
}

static int lua_entity_resetanimlock(lua_State* L)
{
	const char* entity_key = lua_tostring(L, 1);

	entity_t* ent_obj = lua_lvlbase_get_entity(L, entity_key, 2);

	if (ent_obj)
		entity_resetanimlock(ent_obj);

	return 1;
}

static int lua_entity_setextraparams(lua_State* L)
{
	const char* entity_key = lua_tostring(L, 1);
	uint32_t  extrap1 = (uint32_t)lua_tointeger(L, 2);
	uint32_t  extrap2 = (uint32_t)lua_tointeger(L, 3);

	entity_t* ent_obj = lua_lvlbase_get_entity(L, entity_key, 4);

	if (ent_obj) {
		entity_setextraparams(ent_obj, extrap1, extrap2);
	}


	return 1;
}

static int lua_entity_setextraptr(lua_State* L)
{
	const char* entity_key = lua_tostring(L, 1);

	uint32_t extra_ptr = lua_tointeger(L, 2);

	intptr_t extra_ptr_t = script_getpointerfromid(extra_ptr, CUSTOM_1);

	entity_t* ent_obj = lua_lvlbase_get_entity(L, entity_key, 3);

	if (ent_obj) {
		entity_setextraptr(ent_obj, extra_ptr_t);
	}

	return 1;
}

static int lua_entity_clearextraptr(lua_State* L)
{
	const char* entity_key = lua_tostring(L, 1);

	entity_t* ent_obj = lua_lvlbase_get_entity(L, entity_key, 2);
	entity_setextraptr(ent_obj, 0);

	return 1;
}

static int trigger_getpos(lua_State* L)
{
	const char* trigger_key = lua_tostring(L,1);

	trigger_t* trig_obj = lua_lvlbase_get_trigger(L,trigger_key,2);

	if(trig_obj)
	{
		lua_newtable(L);
		lua_pushnumber(L, trig_obj->box.l);
		lua_setfield(L,-2,"x");
		lua_pushnumber(L,trig_obj->box.t);
		lua_setfield(L,-2,"y");

		lua_getglobal(L,"Vector");//we use an already defined metatable in lua, it's stored in a global in this case and not in the registry
		lua_setmetatable(L,-2);
	}

	return 1;
}

static int trigger_setpos(lua_State* L)
{
	const char* trigger_key = lua_tostring(L,1);
	float x = (float)lua_tonumber(L,2);
	float y = (float)lua_tonumber(L,3);

	trigger_t* trig_obj = lua_lvlbase_get_trigger(L,trigger_key,4);

	if(trig_obj)
	{
		trig_obj->box.l = x;
		trig_obj->box.r = x + trig_obj->width;
		trig_obj->box.t = y;
		trig_obj->box.b = y - trig_obj->height;

		trig_obj->render_box.position.x = (wfloat)trig_obj->box.l;
		trig_obj->render_box.position.y = (wfloat)trig_obj->box.t;
	}

	return 1;
}

static int trigger_resetpos(lua_State* L)
{
	const char* trigger_key = lua_tostring(L, 1);

	trigger_t* trig_obj = lua_lvlbase_get_trigger(L, trigger_key, 4);

	if (trig_obj)
	{
		trig_obj->box.l = trig_obj->origpos.x;
		trig_obj->box.r = trig_obj->origpos.x + trig_obj->width;
		trig_obj->box.t = trig_obj->origpos.y;
		trig_obj->box.b = trig_obj->origpos.y - trig_obj->height;

		trig_obj->render_box.position.x = (wfloat)trig_obj->box.l;
		trig_obj->render_box.position.y = (wfloat)trig_obj->box.t;
	}

	return 1;
}

static int trigger_getactivator(lua_State* L)
{
	const char* trigger_key = lua_tostring(L,1);

	trigger_t* trig_obj = lua_lvlbase_get_trigger(L,trigger_key,2);

	if(trig_obj)
	{
		if(strcmp(trig_obj->activator,"") == 0)
			lua_pushnil(L);
		else
			lua_pushstring(L,trig_obj->activator);
	}

	return 1;
}

static int trigger_set_status(lua_State* L)
{
	const char* trigger_key = lua_tostring(L,1);
	wbool trigger_activated = (wbool)lua_toboolean(L,2);
	wbool trigger_repeat = (wbool)lua_toboolean(L,3);

	trigger_t* trig_obj = lua_lvlbase_get_trigger(L,trigger_key,4);

	if(trig_obj)
	{
		trig_obj->activated = trigger_activated;
		trig_obj->repeat = trigger_repeat;
	}

	return 1;
}

static int trigger_intersectentity(lua_State* L)
{
	const char* trigger_key = lua_tostring(L, 1);
	const char* entity_key = lua_tostring(L, 2);

	trigger_t* trig_obj = lua_lvlbase_get_trigger(L, trigger_key, 3);
	entity_t* ent_obj = lua_lvlbase_get_entity(L, entity_key, 3);

	if (trig_obj && ent_obj)
	{
		wbool intersect = trigger_checkbb(trig_obj, ent_obj->shape[0]->bb);
		lua_pushboolean(L, (lua_Integer)intersect);
	}

	return 1;
}

static int tilemap_setframe(lua_State* L)
{
	const char* layer_id = lua_tostring(L,1);
	int32_t frame = (int32_t)lua_tointeger(L,2);

	Tilemap_t* tmap = lua_lvlbase_get_tilemap(L,3);

	int layer_indx = hashtable_index(&tmap->layer_hash,layer_id);

	lua_getglobal(L,"mngr");

	render_manager_t* render_mngr;

	lua_pushstring(L,"render_mngr");
	lua_gettable(L,-2);

	render_mngr = (render_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	TilemapLayer_clearframe(&tmap->layer_array[layer_indx],render_mngr);

	TilemapLayer_gotoframe(&tmap->layer_array[layer_indx],frame,render_mngr);

	return 1;
}

static int tilemap_setcolor(lua_State* L)
{
	const char* layer_id = lua_tostring(L,1);
	float r = (float)lua_tonumber(L,2);
	float g = (float)lua_tonumber(L,3);
	float b = (float)lua_tonumber(L,4);
	float a = (float)lua_tonumber(L,5);


	ColorA_t new_color;
	new_color.r = r;
	new_color.g = g;
	new_color.b = b;
	new_color.a = a;

	Tilemap_t* tmap = lua_lvlbase_get_tilemap(L, 6);

	int layer_indx = hashtable_index(&tmap->layer_hash,layer_id);

	TilemapLayer_setcolor(&tmap->layer_array[layer_indx],new_color);

	return 1;
}

static int tilemap_setlayervisibility(lua_State* L)
{
	const char* layer_id = lua_tostring(L,1);
	wbool visible = (wbool)lua_toboolean(L,2);

	Tilemap_t* tmap = lua_lvlbase_get_tilemap(L, 3);

	int layer_indx = hashtable_index(&tmap->layer_hash,layer_id);

	tmap->layer_array[layer_indx].visible = visible;

	return 1;

}

static int tilemap_settile(lua_State* L)
{
	const char* layer_id = lua_tostring(L,1);
	int32_t index = (int32_t)lua_tointeger(L,2);
	int32_t new_tileset_index = (int32_t)lua_tointeger(L,3);

	Tilemap_t* tmap = lua_lvlbase_get_tilemap(L, 4);

	lua_getglobal(L,"mngr");

	render_manager_t* render_mngr;

	lua_pushstring(L,"render_mngr");
	lua_gettable(L,-2);

	render_mngr = (render_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);


	Tilemap_editTile(tmap,new_tileset_index,index,layer_id,render_mngr,wtrue);

	return 1;
}

static int tilemap_gettile(lua_State* L)
{
	const char* layer_id = lua_tostring(L,1);
	int32_t index = (int32_t)lua_tointeger(L,2);

	Tilemap_t* tmap = lua_lvlbase_get_tilemap(L, 3);

	lua_getglobal(L,"mngr");

	render_manager_t* render_mngr;

	lua_pushstring(L,"render_mngr");
	lua_gettable(L,-2);

	render_mngr = (render_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	if(hashtable_haskey(&tmap->layer_hash,layer_id))
	{
		int layer_index = hashtable_index(&tmap->layer_hash,layer_id);

		lua_pushinteger(L,tmap->layer_array[layer_index].tileArray[index]);
	}
	else
	{
		luaL_error(L,"[tilemap_gettile] tilemap layer named %s doesn't exist!",layer_id);
		lua_pushinteger(L,0);
	}

	return 1;
}


static int text_registercallback(lua_State* L)
{
    const char* text_key = NULL;
	const char* script_func = NULL;
	const char* func_key = NULL;

    text_key = lua_tostring(L,1);
	script_func = lua_tostring(L,2);
	func_key = lua_tostring(L,3);

	uint32_t menu_ptr_id = 0;
	text_entity_t* text_obj = NULL;


	int type = lua_type(L, 4);

	int res1 = lua_isinteger(L, 4);
	int res2 = lua_isnumber(L, 4);

	if(lua_isinteger(L,4))
	{
		menu_ptr_id = (uint32_t)lua_tointeger(L,4);
	}
	
	text_obj = lua_lvlbase_get_text(L, text_key, 4);


	 if(text_obj)
	 {
		if(text_obj->callback_hash == NULL)
		{
			luaL_error(L,"The text '%s' can't have any callback functions! set the callback chekbox to true",text_key); 
			return 1;
		}

		lua_getglobal(L,"mngr");

		void  (*callback_func)(const char* id,const char* func,uint32_t parent,const char* params);

		lua_pushstring(L,"callback_texte");
		lua_gettable(L,-2);

		callback_func = (void (*)(const char* id,const char* func, uint32_t parent,const char* params))lua_touserdata(L,-1);

		lua_pop(L,1);

		text_entity_addcallback(text_obj,func_key,script_func,callback_func, menu_ptr_id);
	 }

	 return 1;
}


static int lua_text_addtext(lua_State* L)
{
	TextObject text_data;
	Wvector text_pos;

	text_object__init(&text_data);

	const char* text_id = getluatable_stringvalue(L,"text_id",1);
	text_data.text_id = (char*)wf_malloc((strlen(text_id)+1) * sizeof(char));
	w_strcpy(text_data.text_id,(strlen(text_id)+1) ,text_id);

	const char* script_module = getluatable_stringvalue(L,"script_module",1);
	char tmp_script[256];
	text_data.script_module = &tmp_script[0];
	w_strcpy(text_data.script_module,256,script_module);

	const char* script_ctor = getluatable_stringvalue(L,"script_ctor",1);
	char tmp_ctor[256];
	text_data.script_ctor = &tmp_ctor[0];
	w_strcpy(text_data.script_ctor,256,script_ctor);

	text_data.position = &text_pos;
	wvector__init(text_data.position);

	text_pos.x = getluatableint_value(L,"pos_x",1);
	text_pos.y = getluatableint_value(L,"pos_y",1);

	text_data.width = getluatableint_value(L,"width",1);
	text_data.height = getluatableint_value(L,"height",1);
	text_data.font_size = getluatableint_value(L,"font_size",1);
	text_data.has_anti_aliasing = (protobuf_c_boolean)wtrue;
	text_data.anti_aliasing = getluatablebool_value(L,"antialias",1);

	const char* font_name = getluatable_stringvalue(L,"font_name",1);
	char tmp_font_name[256];
	text_data.font_name = &tmp_font_name[0];
	w_strcpy(text_data.font_name,256,font_name);
	text_data.font_align = getluatableint_value(L,"font_align",1);

	const char* text_content = getluatable_stringvalue(L,"text_content",1);
	char text_cnt_tmp[512];
	text_data.text_content = &text_cnt_tmp[0];
	w_strcpy(text_cnt_tmp,512,text_content);

	Wcolor text_color;
	text_data.text_color = &text_color;
	wcolor__init(text_data.text_color);
	text_color.r = getluatableint_value(L,"text_color_r",1);
	text_color.g = getluatableint_value(L,"text_color_g",1);
	text_color.b = getluatableint_value(L,"text_color_b",1);
	text_color.a = getluatableint_value(L,"text_color_a",1);

	text_data.has_callback = getluatablebool_value(L,"has_callback",1);
	text_data.z_order = getluatableint_value(L,"z_order",1);
	text_data.parallax_x = getluatable_value(L,"parallax_x",1);
	text_data.parallax_y = getluatable_value(L,"parallax_y",1);
	text_data.no_culling = getluatablebool_value(L,"no_culling",1);
	text_data.no_translation = getluatablebool_value(L,"no_translation",1);


	lua_getglobal(L,"mngr");

	resources_manager_t* resx_mngr;

	lua_pushstring(L,"resx_mngr");
	lua_gettable(L,-2);

	resx_mngr = (resources_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	LEVEL_TYPE type = lua_lvlbase_gettype(L);

	lua_getglobal(L, "mngr");

	physics_manager_t* phy_mngr;

	lua_pushstring(L, (type == L_STATIC) ? "static_physics_mngr" : "stream_physics_mngr");
	lua_gettable(L,-2);

	phy_mngr = (physics_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	lua_getglobal(L,"mngr");

	render_manager_t* render_mngr;

	lua_pushstring(L,"render_mngr");
	lua_gettable(L,-2);

	render_mngr = (render_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	lua_getglobal(L,"mngr");

	localization_t* local_obj;

	lua_pushstring(L,"localization");
	lua_gettable(L,-2);

	local_obj = (localization_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	const char* map_id = lua_tostring(L,2);

	void* ent_mngr = lua_lvlbase_getmanager(L, 3, &type);

	if (type == L_STATIC) {
		levelobject_addtext((Levelobject_t*)ent_mngr, local_obj, text_data.text_id, &text_data, map_id, resx_mngr, render_mngr);
	}
	else {
		luaL_error(L, "Can't add dynamic text on a streamed level !");
	}
		

	return 1;
}

static int lua_text_removetext(lua_State* L)
{
	const char* text_id = lua_tostring(L,1);
	LEVEL_TYPE type = lua_lvlbase_gettype(L);

	lua_getglobal(L, "mngr");

	physics_manager_t* phy_mngr;

	lua_pushstring(L, (type == L_STATIC) ? "static_physics_mngr" : "stream_physics_mngr");
	lua_gettable(L,-2);

	phy_mngr = (physics_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	void* menu = lua_lvlbase_getmanager(L, 2, &type);

	char* ent_ptr = NULL;
		
	if (type == L_STREAM) {
		luaL_error(L, "not implemented");
		return 1;
	}

	Levelobject_t* ent_mngr = (Levelobject_t*)menu;

	ent_ptr = Levelobject_removeentity(ent_mngr,phy_mngr,text_id,GT_TEXT);

	wf_free(ent_ptr);

	return 1;
}

static int lua_text_fixsize(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);
	wbool fix_size = (wbool)lua_toboolean(L,2);

	text_entity_t* ent_obj = lua_lvlbase_get_text(L,entity_key,3);


	if(ent_obj)
	{
		Render_Text_setfixedsize(&ent_obj->font_object,fix_size);
	}

	return 1;
}

static int lua_text_hascallback(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);

	text_entity_t* ent_obj = lua_lvlbase_get_text(L,entity_key,2);

	if(ent_obj)
	{
		lua_pushboolean(L,(ent_obj->callback_hash != NULL));
	}
	else
	{
		lua_pushboolean(L,0);
	}

	return 1;
}

static int text_setzindex(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);
	int32_t zorder = 0;
		
	if(lua_type(L,2) == LUA_TSTRING)
	{
		const char* val = lua_tostring(L,2);
		
		if(strcmp(val,"MAX") == 0)
			zorder = MAX_ZINDEX -1;
	}
	else
	{
		zorder = (int32_t)lua_tointeger(L,2);
	}
		

	LEVEL_TYPE type = L_STATIC;

	void* menu = lua_lvlbase_getmanager(L, 3, &type);

	char* ent_ptr = NULL;

	if (type == L_STREAM) {
		luaL_error(L, "not implemented");
		return 1;
	}

	lvlbase_updatezorder(menu, type, entity_key, zorder, GT_TEXT);

	return 1;
}

static int lua_text_clearcallback(lua_State* L)
{
	const char* entity_key = lua_tostring(L,1);

	text_entity_t* ent_obj = lua_lvlbase_get_text(L,entity_key,2);

	if(ent_obj)
		text_entity_clearcallback(ent_obj);

	return 1;
}

static int text_message(lua_State* L)
{
    const char* text_key = NULL;
	const char* text_content = NULL;

    text_key = lua_tostring(L,1);
	text_content = lua_tostring(L,2);
	wbool updateoriginalcontent = wfalse;
	int stackpos = 3;

	if (lua_isboolean(L, 3)) {
		updateoriginalcontent = (wbool)lua_toboolean(L, 3);
		stackpos = 4;
	}

	text_entity_t* text_obj = lua_lvlbase_get_text(L,text_key, stackpos);

	if(text_obj)
	{
		WorldRect_t full_dimension = lua_lvlbase_getdimension(L, stackpos);

		Rect_t map_dimension = full_dimension.loadeddimension;

		LEVEL_TYPE mtype = L_STATIC;
		void* lvl =  lua_lvlbase_getmanager(L, stackpos, &mtype);

		//in case of static lvl, get the dimension of the text_entity map, and not of the current active map
		if (mtype == L_STATIC) {
			Levelobject_t* olvl = (Levelobject_t*)lvl;

			if (hashtable_haskey(&olvl->map_hash, text_obj->ref_map_id)) {
				map_dimension = olvl->maps[hashtable_index(&olvl->map_hash, text_obj->ref_map_id)].map_dimension;
			}
			else {
				luaL_error(L, "The text object with id %s don't have a valid map reference, alignement could be off",text_key);
			}
		}

		
		//ref_map_id

		lua_getglobal(L,"mngr");

		lua_pushstring(L,"localization");
		lua_gettable(L,-2);

		localization_t* local_obj = (localization_t*)lua_touserdata(L,-1);

		lua_pop(L,1);

		text_entity_updatecontent(text_obj,local_obj,text_content,map_dimension, updateoriginalcontent);
	}

	 return 1;
}

static int text_setposition(lua_State* L)
{
	const char* text_key = lua_tostring(L,1);

	if(!lua_istable(L,2))
	{
		luaL_error(L,"The second parameter of text_setposition is not a table!"); 
		return 1;
	}

	cpVect position = cpv(getluatable_value(L,"x",2),getluatable_value(L,"y",2));

	text_entity_t* text_obj = lua_lvlbase_get_text(L,text_key,3);


	if(text_obj)
	{
		text_entity_setposition(text_obj,(float)position.x,(float)position.y);
	}

	 return 1;
}

static int text_getposition(lua_State* L)
{
	const char* text_key = lua_tostring(L,1);

	text_entity_t* text_obj = lua_lvlbase_get_text(L,text_key,2);

	if(text_obj)
	{
		Vector_t pos = text_entity_getposition(text_obj);

		lua_newtable(L);
		lua_pushnumber(L, pos.x);
		lua_setfield(L,-2,"x");
		lua_pushnumber(L,pos.y);
		lua_setfield(L,-2,"y");

		lua_getglobal(L,"Vector");//we use an already defined metatable in lua, it's stored in a global in this case and not in the registry
		lua_setmetatable(L,-2);
	}

	 return 1;
}

static int text_getsize(lua_State* L)
{
	const char* text_key = lua_tostring(L,1);

	text_entity_t* text_obj = lua_lvlbase_get_text(L,text_key,2);

	if(text_obj)
	{
		lua_newtable(L);
		lua_pushnumber(L, text_obj->bbox.width);
		lua_setfield(L,-2,"width");
		lua_pushnumber(L,text_obj->bbox.height);
		lua_setfield(L,-2,"height");
	}

	 return 1;
}

static int text_getstring(lua_State* L)
{
	const char* text_key = lua_tostring(L, 1);

	text_entity_t* text_obj = lua_lvlbase_get_text(L, text_key, 2);

	if (text_obj != NULL)
	{
		lua_pushstring(L, text_obj->original_content);
	}

	return 1;

}

static int text_getstringlen(lua_State* L)
{
	const char* text_key = lua_tostring(L, 1);

	text_entity_t* text_obj = lua_lvlbase_get_text(L,text_key,2);
	if(text_obj != NULL)
	{
		size_t len = a_wcslen(text_obj->text_content);

		lua_pushinteger(L,len);
	}

	return 1;
}

static int text_appendmessage(lua_State* L)
{
	const char* text_key = NULL;
	const char* text_content = NULL;

	text_key = lua_tostring(L,1);
	text_content = lua_tostring(L,2);

	text_entity_t* text_obj = lua_lvlbase_get_text(L,text_key,3);

	WorldRect_t full_dimension = lua_lvlbase_getdimension(L, 3);
	Rect_t map_dimension = full_dimension.loadeddimension;

	if(text_obj)
	{
		lua_getglobal(L,"mngr");

		lua_pushstring(L,"localization");
		lua_gettable(L,-2);

		localization_t* local_obj = (localization_t*)lua_touserdata(L,-1);

		lua_pop(L,1);

		text_entity_appendcontent(text_obj,local_obj,text_content,map_dimension);
	}

	return 1;
}

static int text_append_raw(lua_State* L)
{
	const char* text_key = NULL;
	const char* text_content = NULL;

	text_key = lua_tostring(L,1);
	text_content = lua_tostring(L,2);

	WorldRect_t full_dimension = lua_lvlbase_getdimension(L, 3);
	Rect_t map_dimension = full_dimension.loadeddimension;
	text_entity_t* text_obj = lua_lvlbase_get_text(L,text_key,3);

	if(text_obj)
	{
		text_entity_append_raw(text_obj,text_content,map_dimension);
	}

	return 1;
}


static int text_show(lua_State* L)
{
    const char* text_key = lua_tostring(L,1);

	text_entity_t* text_obj = lua_lvlbase_get_text(L,text_key,2);


	 if(text_obj)
	 {
		 text_obj->is_visible = wtrue;
	 }

	 return 1;
}

static int text_hide(lua_State* L)
{
    const char* text_key = lua_tostring(L,1);

	text_entity_t* text_obj = lua_lvlbase_get_text(L,text_key,2);

	 if(text_obj)
	 {
		 text_obj->is_visible = wfalse;
	 }

	 return 1;
}

static int text_changecolor(lua_State* L)
{
	const char* text_key = lua_tostring(L,1);
	float r = (float)lua_tonumber(L,2);
	float g = (float)lua_tonumber(L,3);
	float b = (float)lua_tonumber(L,4);
	float a = (float)lua_tonumber(L,5);

	text_entity_t* text_obj = lua_lvlbase_get_text(L,text_key,6);

	 if(text_obj)
	 {
		 text_obj->color.r = r;
		 text_obj->color.g = g;
		 text_obj->color.b = b;
		 text_obj->color.a = a;
	 }

	 return 1;
}

static int text_resetcolor(lua_State* L)
{
	const char* text_key = lua_tostring(L,1);

	text_entity_t* text_obj = lua_lvlbase_get_text(L,text_key,2);

	 if(text_obj)
	 {
		 text_obj->color.r = text_obj->original_color.r;
		 text_obj->color.g = text_obj->original_color.g;
		 text_obj->color.b = text_obj->original_color.b;
		 text_obj->color.a = text_obj->original_color.a;
	 }

	 return 1;
}

static int text_resetcontent(lua_State* L)
{
	const char* text_key = NULL;

	text_key = lua_tostring(L,1);

	text_entity_t* text_obj = lua_lvlbase_get_text(L,text_key,2);

	WorldRect_t full_dimension = lua_lvlbase_getdimension(L, 2);
	Rect_t map_dimension = full_dimension.loadeddimension;
	if(text_obj)
	{
		lua_getglobal(L,"mngr");

		lua_pushstring(L,"localization");
		lua_gettable(L,-2);

		localization_t* local_obj = (localization_t*)lua_touserdata(L,-1);

		lua_pop(L,1);

		text_entity_resetcontent(text_obj,local_obj,map_dimension);
	}

	return 1;
}

static int lua_text_setrandomhide(lua_State* L)
{
	const char* text_key = NULL;

	text_key = lua_tostring(L, 1);
	uint32_t hidechance = (uint32_t)lua_tointeger(L, 2);
	int charreplacer = (int)lua_tointeger(L, 3);

	text_entity_t* text_obj = lua_lvlbase_get_text(L, text_key, 4);

	if (text_obj)
	{
		text_entity_setrandomhide(text_obj, hidechance, charreplacer);
	}

	return 1;

	
}

static int lua_waypoint_getpos(lua_State* L)
{
	const char* waypoint_id = lua_tostring(L,1);

	waypoint_t* waypoint = lua_lvlbase_getwaypoint(L, waypoint_id, 2);

	if (waypoint)
	{
		Vector_t pos = waypoint_getpos(waypoint);

		lua_newtable(L);
		lua_pushnumber(L, pos.x);
		lua_setfield(L, -2, "x");
		lua_pushnumber(L, pos.y);
		lua_setfield(L, -2, "y");

		lua_getglobal(L, "Vector");//we use an already defined metatable in lua, it's stored in a global in this case and not in the registry
		lua_setmetatable(L, -2);
	}

	return 1;

}

static int lua_collider_setparameters(lua_State* L)
{
	const char* collider_id = lua_tostring(L, 1);
	wfloat elasticity = (wfloat)lua_tonumber(L, 2);
	wfloat friction = (wfloat)lua_tonumber(L, 3);

	collider_t* collider = lua_lvlbase_getcollider(L, collider_id, 4);

	if (collider)
	{
		collider_setparameters(collider, elasticity, friction);
	}

	return 1;
}

static int lua_collider_getcenteronground(lua_State* L)
{
	const char* collider_id = lua_tostring(L, 1);

	collider_t* collider = lua_lvlbase_getcollider(L, collider_id, 4);

	if (collider)
	{
		Rect_t box = collider_getbox(collider);

		Vector_t centeronground = Vec(box.position.x + (box.width * WFLOAT05), box.position.y - box.height);

		lua_newtable(L);
		lua_pushnumber(L, centeronground.x);
		lua_setfield(L, -2, "x");
		lua_pushnumber(L, centeronground.y);
		lua_setfield(L, -2, "y");

		lua_getglobal(L, "Vector");//we use an already defined metatable in lua, it's stored in a global in this case and not in the registry
		lua_setmetatable(L, -2);
	}

	return 1;

}



void register_entities_functions(lua_State *L)
{
	
	//entity functions
	lua_pushcfunction(L,entity_addentity);
	lua_setglobal(L,"entity_addentity");

	lua_pushcfunction(L,entity_removeentity);
	lua_setglobal(L,"entity_removeentity");

	lua_pushcfunction(L,entity_playanim);
	lua_setglobal(L,"entity_playanim");

	lua_pushcfunction(L,entity_setcollisionlayer);
	lua_setglobal(L,"entity_setcollisionlayer");

	lua_pushcfunction(L,entity_setsingleshapelayerandmask);
	lua_setglobal(L,"entity_setsingleshapelayerandmask");

	lua_pushcfunction(L, entity_setsingleshapelayer);
	lua_setglobal(L, "entity_setsingleshapelayer");

	lua_pushcfunction(L, entity_setcollisionmask);
	lua_setglobal(L, "entity_setcollisionmask");

	lua_pushcfunction(L, entity_setsingleshapemask);
	lua_setglobal(L, "entity_setsingleshapemask");

	lua_pushcfunction(L,entity_getpos);
	lua_setglobal(L,"entity_getpos");

	lua_pushcfunction(L,entity_getcontrolpos);
	lua_setglobal(L,"entity_getcontrolpos");

	lua_pushcfunction(L,entity_setcollisiontype);
	lua_setglobal(L,"entity_setcollisiontype");

	lua_pushcfunction(L,entity_setsingleshapecollisiontype);
	lua_setglobal(L,"entity_setsingleshapecollisiontype");
	
	lua_pushcfunction(L,entity_getcollisiontype);
	lua_setglobal(L,"entity_getcollisiontype");

	lua_pushcfunction(L,entity_setelast);
	lua_setglobal(L, "entity_setelast");

	lua_pushcfunction(L,entity_setfriction);
	lua_setglobal(L, "entity_setfriction");

	lua_pushcfunction(L,entity_setmass);
	lua_setglobal(L, "entity_setmass");

	lua_pushcfunction(L,entity_setmoment);
	lua_setglobal(L,"entity_setmoment");

	lua_pushcfunction(L,entity_setmaxvel);
	lua_setglobal(L, "entity_setmaxvel");

	lua_pushcfunction(L,entity_setmaxangvel);
	lua_setglobal(L, "entity_setmaxangvel");

	lua_pushcfunction(L,entity_setvel);
	lua_setglobal(L, "entity_setvel");

	lua_pushcfunction(L,entity_setcontrolvel);
	lua_setglobal(L, "entity_setcontrolvel");

	lua_pushcfunction(L,entity_getvel);
	lua_setglobal(L, "entity_getvel");

	lua_pushcfunction(L,entity_getcontrolvel);
	lua_setglobal(L, "entity_getcontrolvel");

	lua_pushcfunction(L,entity_exist);
	lua_setglobal(L,"entity_exist");

	lua_pushcfunction(L,entity_setvisible);
	lua_setglobal(L,"entity_setvisible");

	lua_pushcfunction(L,entity_applyimpulse);
	lua_setglobal(L, "entity_applyimpulse");

	lua_pushcfunction(L,entity_movmaxbias);
	lua_setglobal(L, "entity_movmaxbias");

	lua_pushcfunction(L,entity_movmaxforce);
	lua_setglobal(L, "entity_movmaxforce");

	lua_pushcfunction(L,entity_movmaxerror);
	lua_setglobal(L,"entity_movmaxerror");

	lua_pushcfunction(L,entity_linkerrorrateleft);
	lua_setglobal(L,"entity_linkerrorrateleft");

	lua_pushcfunction(L,entity_linkmaxbias);
	lua_setglobal(L,"entity_linkmaxbias");

	lua_pushcfunction(L,entity_linkmaxforce);
	lua_setglobal(L,"entity_linkmaxforce");

	lua_pushcfunction(L,entity_setgravityenable);
	lua_setglobal(L,"entity_setgravityenable");

	lua_pushcfunction(L,entity_setcontrolpos);
	lua_setglobal(L,"entity_setcontrolpos");

	lua_pushcfunction(L,entity_setpos);
	lua_setglobal(L,"entity_setpos");

	lua_pushcfunction(L,entity_registercallback);
	lua_setglobal(L,"entity_registercallback");

	lua_pushcfunction(L,lua_entity_clearcallback);
	lua_setglobal(L,"entity_clearcallback");

	lua_pushcfunction(L,lua_entity_hascallback);
	lua_setglobal(L,"entity_hascallback");

	lua_pushcfunction(L,entity_enableconstraint);
	lua_setglobal(L,"entity_enableconstraint");

	lua_pushcfunction(L,entity_disableconstraint);
	lua_setglobal(L,"entity_disableconstraint");

	lua_pushcfunction(L,lua_entity_setanchorpoint);
	lua_setglobal(L,"entity_setanchorpoint");

	lua_pushcfunction(L,lua_entity_movetopos);
	lua_setglobal(L,"entity_movetopos");

	lua_pushcfunction(L,entity_getlinkname);
	lua_setglobal(L,"entity_getlinkname");

	lua_pushcfunction(L,lua_entity_linkto);
	lua_setglobal(L,"entity_linkto");

	lua_pushcfunction(L,entity_unlink);
	lua_setglobal(L,"entity_unlink");

	lua_pushcfunction(L,entity_relink);
	lua_setglobal(L,"entity_relink");

	lua_pushcfunction(L, lua_entity_pintoworld);
	lua_setglobal(L, "entity_pintoworld");

	lua_pushcfunction(L, lua_entity_removepin);
	lua_setglobal(L, "entity_removepin");

	lua_pushcfunction(L,entity_setframe);
	lua_setglobal(L,"entity_setframe");

	lua_pushcfunction(L,entity_fade);
	lua_setglobal(L,"entity_fade");

	lua_pushcfunction(L,entity_getfade);
	lua_setglobal(L,"entity_getfade");

	lua_pushcfunction(L,entity_endfade);
	lua_setglobal(L,"entity_endfade");

	lua_pushcfunction(L,lua_entity_startrotation);
	lua_setglobal(L,"entity_startrotation");

	lua_pushcfunction(L,lua_entity_endrotation);
	lua_setglobal(L,"entity_endrotation");

	lua_pushcfunction(L, lua_entity_getrotation);
	lua_setglobal(L, "entity_getrotation");

	lua_pushcfunction(L,lua_entity_setrotation);
	lua_setglobal(L,"entity_setrotation");

	lua_pushcfunction(L,lua_entity_setspriterotationlink);
	lua_setglobal(L,"entity_setspriterotationlink");

	lua_pushcfunction(L,lua_entity_resetrotation);
	lua_setglobal(L,"entity_resetrotation");

	lua_pushcfunction(L,entity_resetentity);
	lua_setglobal(L,"entity_resetentity");

	lua_pushcfunction(L,lua_entity_settexture);
	lua_setglobal(L,"entity_settexture");

	lua_pushcfunction(L,entity_getspritepointer);
	lua_setglobal(L,"entity_getspritepointer");

	lua_pushcfunction(L, lua_entity_getnumsprite);
	lua_setglobal(L, "entity_getnumsprite");

	lua_pushcfunction(L,entity_getmeshpointer);
	lua_setglobal(L,"entity_getmeshpointer");

	lua_pushcfunction(L,entity_setzorder);
	lua_setglobal(L,"entity_setzorder");

	lua_pushcfunction(L,lua_entity_checkrender);
	lua_setglobal(L,"entity_checkrender");

	lua_pushcfunction(L,entity_getbox);
	lua_setglobal(L,"entity_getbox");

	lua_pushcfunction(L, entity_getaabb);
	lua_setglobal(L, "entity_getaabb");

	lua_pushcfunction(L,entity_setspritefreeze);
	lua_setglobal(L,"entity_setspritefreeze");

	lua_pushcfunction(L,entity_getspritefreezepos);
	lua_setglobal(L,"entity_getspritefreezepos");
	
	lua_pushcfunction(L,entity_setspritefreezepos);
	lua_setglobal(L,"entity_setspritefreezepos");

	lua_pushcfunction(L,entity_getspriteoffset);
	lua_setglobal(L,"entity_getspriteoffset");

	lua_pushcfunction(L,entity_setspriteoffset);
	lua_setglobal(L,"entity_setspriteoffset");
	
	lua_pushcfunction(L,entity_noresetflag);
	lua_setglobal(L,"entity_noresetflag");

	lua_pushcfunction(L,lua_entity_addcontactbody);
	lua_setglobal(L,"entity_addcontactbody");

	lua_pushcfunction(L,lua_entity_removeallcontactbodies);
	lua_setglobal(L,"entity_removeallcontactbodies");

	lua_pushcfunction(L,entity_getcontactpos);
	lua_setglobal(L,"entity_getcontactpos");

	lua_pushcfunction(L,entity_getzindex);
	lua_setglobal(L,"entity_getzindex");

	lua_pushcfunction(L,lua_entity_setshaderparam_floatvalue);
	lua_setglobal(L,"entity_setshaderparam_fval");

	lua_pushcfunction(L, lua_entity_setshaderparam_intvalue);
	lua_setglobal(L, "entity_setshaderparam_ival");

	lua_pushcfunction(L, lua_entity_setshaderparam_uniformarrayfloat);
	lua_setglobal(L, "entity_setshaderparam_uniformarrayfloat");
	
	lua_pushcfunction(L,entity_getmapid);
	lua_setglobal(L,"entity_getmapid");

	lua_pushcfunction(L, entity_getsingleshapebox);
	lua_setglobal(L, "entity_getsingleshapebox");

	lua_pushcfunction(L, entity_getsingleshapeoffset);
	lua_setglobal(L, "entity_getsingleshapeoffset");

	lua_pushcfunction(L, entity_setsingleshapeoffset);
	lua_setglobal(L, "entity_setsingleshapeoffset");

	lua_pushcfunction(L, lua_entity_lockwhenanim);
	lua_setglobal(L, "entity_lockwhenanim");

	lua_pushcfunction(L, lua_entity_resetanimlock);
	lua_setglobal(L, "entity_resetanimlock");

	lua_pushcfunction(L, lua_entity_setextraparams);
	lua_setglobal(L, "entity_setextraparams");

	lua_pushcfunction(L, lua_entity_setextraptr);
	lua_setglobal(L, "entity_setextraptr");

	lua_pushcfunction(L, lua_entity_clearextraptr);
	lua_setglobal(L, "entity_clearextraptr");
	
	//trigger functions
	lua_pushcfunction(L,trigger_setpos);
	lua_setglobal(L,"trigger_setpos");

	lua_pushcfunction(L, trigger_resetpos);
	lua_setglobal(L, "trigger_resetpos");

	lua_pushcfunction(L,trigger_getpos);
	lua_setglobal(L,"trigger_getpos");

	lua_pushcfunction(L,trigger_getactivator);
	lua_setglobal(L,"trigger_getactivator");

	lua_pushcfunction(L,trigger_set_status);
	lua_setglobal(L,"trigger_set_status");

	lua_pushcfunction(L, trigger_intersectentity);
	lua_setglobal(L, "trigger_intersectentity");

	//tilemap functions
	lua_pushcfunction(L,tilemap_setframe);
	lua_setglobal(L,"tilemap_setframe");

	lua_pushcfunction(L,tilemap_setcolor);
	lua_setglobal(L,"tilemap_setcolor");

	lua_pushcfunction(L,tilemap_setlayervisibility);
	lua_setglobal(L,"tilemap_setlayervisibility");

	lua_pushcfunction(L,tilemap_settile);
	lua_setglobal(L,"tilemap_settile");

	lua_pushcfunction(L,tilemap_gettile);
	lua_setglobal(L,"tilemap_gettile");

	//text functions
	lua_pushcfunction(L,lua_text_addtext);
	lua_setglobal(L,"text_addtext");

	lua_pushcfunction(L,lua_text_fixsize);
	lua_setglobal(L,"text_setfixedsize");
	
	lua_pushcfunction(L,lua_text_removetext);
	lua_setglobal(L,"text_removetext");

	lua_pushcfunction(L,text_registercallback);
	lua_setglobal(L,"text_registercallback");

	lua_pushcfunction(L, text_getstring);
	lua_setglobal(L, "text_getstring");

	lua_pushcfunction(L,text_getstringlen);
	lua_setglobal(L,"text_getstringlen");

	lua_pushcfunction(L,text_message);
	lua_setglobal(L,"text_message");

	lua_pushcfunction(L,text_appendmessage);
	lua_setglobal(L,"text_appendmessage");

	lua_pushcfunction(L,text_append_raw);
	lua_setglobal(L,"text_append_raw");

	lua_pushcfunction(L,text_show);
	lua_setglobal(L,"text_show");

	lua_pushcfunction(L,text_hide);
	lua_setglobal(L,"text_hide");

	lua_pushcfunction(L,text_changecolor);
	lua_setglobal(L,"text_changecolor");

	lua_pushcfunction(L,text_resetcolor);
	lua_setglobal(L,"text_resetcolor");

	lua_pushcfunction(L,text_resetcontent);
	lua_setglobal(L,"text_resetcontent");

	lua_pushcfunction(L,text_getsize);
	lua_setglobal(L,"text_getsize");

	lua_pushcfunction(L,text_setposition);
	lua_setglobal(L,"text_setposition");

	lua_pushcfunction(L,text_getposition);
	lua_setglobal(L,"text_getposition");

	lua_pushcfunction(L,lua_text_clearcallback);
	lua_setglobal(L,"text_clearcallback");

	lua_pushcfunction(L,lua_text_hascallback);
	lua_setglobal(L,"text_hascallback");

	lua_pushcfunction(L,text_setzindex);
	lua_setglobal(L,"text_setzindex");

	lua_pushcfunction(L, lua_text_setrandomhide);
	lua_setglobal(L, "text_setrandomhide");

	//waypoints functions

	lua_pushcfunction(L, lua_waypoint_getpos);
	lua_setglobal(L, "waypoint_getpos");

	//colliders functions
	lua_pushcfunction(L, lua_collider_setparameters);
	lua_setglobal(L, "collider_setparameters");

	lua_pushcfunction(L, lua_collider_getcenteronground);
	lua_setglobal(L, "collider_getcenteronground");
	
}
