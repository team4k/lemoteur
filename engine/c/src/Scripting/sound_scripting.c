//TODO: lua_is* function are misleading and don't check variable type but if a variable can be converted to a type, replace
//all lua_is* function with lua_type(L,stack_pos) == <LUA_TYPE> to check the type


#ifdef _MSC_VER
    extern "C" {
#endif
        
#include "lauxlib.h"
#include "lualib.h"
#include "lua.h"
        
#ifdef _MSC_VER
 }
#endif

#if defined(_WIN32)
    #include <windows.h>
#else
	#ifndef __USE_BSD
        	#define __USE_BSD
	#endif
        #include <unistd.h>
        #include <dlfcn.h>
		#include <wchar.h>
		#include <pthread.h>
#endif

#include <stdarg.h>
#include <string.h>
#include <inttypes.h>
#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include <math.h>
#include <sqlite3.h>

#include <Engine.h>

#include <Render.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>
#include <Base/geom.h>

#include <Render/render_manager.h>

#include <Graphics/TextureLoader.h>
#include <Resx/shader_loader.h>

#include <Utils/Hash.h>
#include <Utils/hashmap.h>

#include <GameObjects/Characters.pb-c.h>

#include <Graphics/TextureRegion.h>
#include <Debug/Logprint.h>

#include <GameObjects/Level.pb-c.h>

#include <Resx/localization.h>

#include <Graphics/FontLoader.h>

#ifdef EMSCRIPTEN
	#include <SDL2/SDL_audio.h>
	#include <emscripten.h>
#else
	#include <portaudio.h>
#endif

#include <Sound/Sound.h>
#include <Sound/snd_mngr.h>

#include <Resx/Resources.h>


#include <Scripting/ScriptLuaUtils.h>
#include <Scripting/ScriptLoader.h>

static int snd_createsound(lua_State* L)
{
	const char* sample_key = lua_tostring(L,1);
	const char* id = lua_tostring(L,2);
	wbool loop = (wbool)lua_toboolean(L,3);
	wbool remove_on_ended = (wbool)lua_toboolean(L,4);
	wbool createonly = (wbool)lua_toboolean(L,5);
	int16_t sound_type = (int16_t)lua_tointeger(L,6);
	lua_Number falloff = -1;

	Vector_t sound_vector = {0.0f,0.0f};
	wbool positionning = wfalse;

	if(lua_istable(L,7))
	{
		sound_vector.x =  getluatable_value(L,"x",7);
		sound_vector.y = getluatable_value(L,"y",7);
		positionning = wtrue;
	}

	if(positionning)
	{
		falloff = lua_tonumber(L,8);
	}

	const char* map_id = NULL;

	if(lua_type(L,9) == LUA_TSTRING)
		map_id = lua_tostring(L,9);


	lua_getglobal(L,"mngr");
	snd_mngr_t* snd_mngr;
	resources_manager_t* resx_mngr;

	lua_pushstring(L,"sound_mngr");
	lua_gettable(L,-2);

	snd_mngr = (snd_mngr_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	lua_pushstring(L,"resx_mngr");
	lua_gettable(L,-2);

	resx_mngr = (resources_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	//work around because of poor 48 > 44.1 conversion in SDL audio under emscripten, use 44.1 sound version rather than 48
	#ifdef EMSCRIPTEN
		char sample_fixed_key[512];

		if(snd_mngr->fallback_to_44100)
		{
			const char* ext = get_filename_ext(sample_key);
			sprintf(sample_fixed_key,"%s441.%s",sample_key,ext);
		}
		else
		{
			strcpy(sample_fixed_key,sample_key);
		}

		sound_sample_t* tmp_sound = getsound_resx(resx_mngr,sample_fixed_key);

	#else
		sound_sample_t* tmp_sound = getsound_resx(resx_mngr,sample_key);
	#endif


	if(tmp_sound != NULL)
		snd_mngr_playsound(snd_mngr,tmp_sound,loop,id,remove_on_ended,createonly,sound_type,sound_vector,positionning,(float)falloff,map_id);

	return 1;
}

static int snd_playsound(lua_State* L)
{
	const char* id = lua_tostring(L,1);

	lua_getglobal(L,"mngr");
	snd_mngr_t* snd_mngr;

	lua_pushstring(L,"sound_mngr");
	lua_gettable(L,-2);

	snd_mngr = (snd_mngr_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	snd_mngr_rewind_sound(snd_mngr,id);

	return 1;
}

static int snd_hassound(lua_State* L)
{
	const char* id = lua_tostring(L,1);

	lua_getglobal(L,"mngr");
	snd_mngr_t* snd_mngr;

	lua_pushstring(L,"sound_mngr");
	lua_gettable(L,-2);

	snd_mngr = (snd_mngr_t*)lua_touserdata(L,-1);

	lua_pop(L,1);
	
	lua_pushboolean(L,snd_mngr_has_sound(snd_mngr,id));

	return 1;
}

static int snd_soundisplaying(lua_State* L)
{
	const char* id = lua_tostring(L,1);

	lua_getglobal(L,"mngr");
	snd_mngr_t* snd_mngr;

	lua_pushstring(L,"sound_mngr");
	lua_gettable(L,-2);

	snd_mngr = (snd_mngr_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	lua_pushboolean(L,snd_mngr_isplaying(snd_mngr,id));

	return 1;
}

static int snd_updatecenterpoint(lua_State* L)
{
	Vector_t position = {0,0};
	
	position.x = getluatable_value(L,"x",1);
	position.y = getluatable_value(L,"y",1);

	lua_getglobal(L,"mngr");
	snd_mngr_t* snd_mngr;

	lua_pushstring(L,"sound_mngr");
	lua_gettable(L,-2);

	snd_mngr = (snd_mngr_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	snd_mngr_update_gamespace(snd_mngr,snd_mngr->game_space_info,position);

	return 1;
}

static int snd_removesound(lua_State* L)
{
	const char* id = lua_tostring(L,1);

	lua_getglobal(L,"mngr");
	snd_mngr_t* snd_mngr;

	lua_pushstring(L,"sound_mngr");
	lua_gettable(L,-2);

	snd_mngr = (snd_mngr_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	snd_mngr_removesound(snd_mngr,id);

	return 1;
}


static int snd_stopsound(lua_State* L)
{
	const char* id = lua_tostring(L,1);

	lua_getglobal(L,"mngr");
	snd_mngr_t* snd_mngr;

	lua_pushstring(L,"sound_mngr");
	lua_gettable(L,-2);

	snd_mngr = (snd_mngr_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	snd_mngr_stopsound(snd_mngr,id);

	return 1;
}

static int snd_updateposition(lua_State* L)
{
	const char* id = lua_tostring(L,1);
	Vector_t position = {0,0};
	
	position.x = getluatable_value(L,"x",2);
	position.y = getluatable_value(L,"y",2);

	lua_getglobal(L,"mngr");
	snd_mngr_t* snd_mngr;

	lua_pushstring(L,"sound_mngr");
	lua_gettable(L,-2);

	snd_mngr = (snd_mngr_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	snd_mngr_update_position(snd_mngr,id,position);

	return 1;
}

static int snd_stopallsound(lua_State* L)
{
	lua_getglobal(L,"mngr");
	snd_mngr_t* snd_mngr;

	lua_pushstring(L,"sound_mngr");
	lua_gettable(L,-2);

	snd_mngr = (snd_mngr_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	snd_mngr_stopallsounds(snd_mngr,wfalse);

	return 1;
}

static int snd_setsfxvolume(lua_State* L)
{
	lua_Integer sfx_volume = lua_tointeger(L,1);

	lua_getglobal(L,"mngr");
	snd_mngr_t* snd_mngr;

	lua_pushstring(L,"sound_mngr");
	lua_gettable(L,-2);

	snd_mngr = (snd_mngr_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	snd_mngr_setvolume(snd_mngr,-1,(int16_t)sfx_volume);

	return 1;
}

static int snd_setmusicvolume(lua_State* L)
{
	lua_Integer music_volume = lua_tointeger(L,1);

	lua_getglobal(L,"mngr");
	snd_mngr_t* snd_mngr;

	lua_pushstring(L,"sound_mngr");
	lua_gettable(L,-2);

	snd_mngr = (snd_mngr_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	snd_mngr_setvolume(snd_mngr,(int16_t)music_volume,-1);

	return 1;
}

void register_sound_function(lua_State* L)
{
	//sound functions
	lua_pushcfunction(L,snd_createsound);
	lua_setglobal(L, "createsound");

	lua_pushcfunction(L,snd_playsound);
	lua_setglobal(L,"playsound");

	lua_pushcfunction(L,snd_stopsound);
	lua_setglobal(L, "stopsound");

	lua_pushcfunction(L,snd_hassound);
	lua_setglobal(L, "has_sound");

	lua_pushcfunction(L,snd_stopallsound);
	lua_setglobal(L,"stopallsounds");

	lua_pushcfunction(L,snd_setmusicvolume);
	lua_setglobal(L,"snd_setmusicvolume");

	lua_pushcfunction(L,snd_setsfxvolume);
	lua_setglobal(L,"snd_setsfxvolume");

	lua_pushcfunction(L,snd_updateposition);
	lua_setglobal(L,"snd_updateposition");

	lua_pushcfunction(L,snd_removesound);
	lua_setglobal(L,"snd_removesound");

	lua_pushcfunction(L,snd_soundisplaying);
	lua_setglobal(L,"snd_soundisplaying");

	lua_pushcfunction(L,snd_updatecenterpoint);
	lua_setglobal(L,"snd_updatecenterpoint");
}
