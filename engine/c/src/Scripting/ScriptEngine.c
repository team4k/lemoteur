#ifdef _MSC_VER
extern "C" {
#endif
 
#include "lauxlib.h"
#include "lualib.h"
#include "lua.h"
    
#ifdef _MSC_VER    
}
#endif

#include <stdarg.h>
#include <string.h>
#include <inttypes.h>

#include <Base/types.h>
#include <Engine.h>
#include <Debug/Logprint.h>
#include <GameObjects/Level.pb-c.h>
#include <Scripting/ScriptLoader.h>
#include <Scripting/ScriptEngine.h>

#ifdef USE_LUA_JIT
     #define LUA_OK 0
#endif




int32_t report (lua_State *L, int32_t status) 
{
  if (status != LUA_OK && !lua_isnil(L, -1)) 
  {
    const char *msg = lua_tostring(L, -1);
    if (msg == NULL) 
        msg = "(error object is not a string)";


    logprint("lua load : %s\n",msg);
    lua_pop(L, 1);

    /* force a complete garbage collection in case of errors */
    lua_gc(L, LUA_GCCOLLECT, 0);
  }
  else if(!lua_isnil(L, -1))
  {
      const char *msg = lua_tostring(L, -1);

      if (msg)
        luaL_traceback(L, L, msg, 1);

      logprint("lua load error : %s\n",lua_tostring(L, -1));

      lua_gc(L, LUA_GCCOLLECT, 0);
  }

  lua_settop(L,0);
  return status;
}


int script_loadfile(lua_State* L,const char* script_filename)
{
      int load_status = luaL_loadfile(L, script_filename);

       if(load_status == LUA_OK)
       {
	       if(lua_pcall(L, 0, 0, 0))//MANDATORY ! make a priming call to load script variables
		       report(L, load_status);
           else
               return 0;

		   logprint("[LUA] Loaded script file %s",script_filename);
       }
	   else
	   {
			if(!lua_isnil(L, -1))
			{
				const char *msg = lua_tostring(L, -1);

				luaL_traceback(L, L, msg, 0);

				lua_pop(L, 1);

				/* force a complete garbage collection in case of errors */
				lua_gc(L, LUA_GCCOLLECT, 0);

				logprint("[LUA] Error %s",msg);
			}

		   logprint("[LUA] Error loading script file %s",script_filename);
		   return -1;
	   }

	   lua_settop(L,0);

    return 0;
}


lua_State* script_createstate()
{
	 
     lua_State *L = luaL_newstate();
     luaL_openlibs(L);

	 register_functions(L);

	 lua_settop(L,0);

	 return L;
}

void script_destroystate(lua_State* L)
{
	lua_close(L);
}

void script_createglobalmngr(lua_State* state)
{
	lua_newtable(state);
	lua_setglobal(state,"mngr");

	lua_settop(state,0);
}

void script_pushmngr(lua_State* state,const char* mngr_name,void* p_mngr)
{
	lua_getglobal(state,"mngr");
	lua_pushlightuserdata(state,p_mngr);
	lua_setfield(state,-2,mngr_name);

	lua_settop(state,0);
}

void script_pushbooleanparam(lua_State* state, const char* param_name, wbool param_value)
{
	lua_getglobal(state, "mngr");
	lua_pushboolean(state, (int)param_value);
	lua_setfield(state, -2, param_name);

	lua_settop(state, 0);
}


void script_setcustompkgloader(lua_State* state,const lua_CFunction custompkgloader)
{
	lua_getglobal(state,"package");
			
	lua_createtable(state, 1, 0);
	lua_pushvalue(state, -2);
	lua_pushcclosure(state, custompkgloader, 1);
	lua_rawseti(state, -2, 1);
	
     #ifdef USE_LUA_JIT			
	     lua_setfield(state, -2, "loaders"); 
     #else
          lua_setfield(state, -2, "searchers"); 
     #endif
	lua_pop(state, 1);

	lua_settop(state,0);
}

void script_callgc(lua_State* state)
{
	lua_gc(state,LUA_GCCOLLECT,0);
}

static int traceback(lua_State* L)
{
	const char* lua_err = lua_tostring(L,1);
	luaL_traceback(L, L, lua_err, 0);

	return 1;
}

const char* script_execstring(lua_State* state,const char* buff,void* return_value,size_t return_size)
{
	int32_t error = 0;

	lua_pushcfunction(state,traceback);

	int32_t num_result = 0;

	if(return_value != NULL)
		num_result = 1;

	error = luaL_loadstring(state,buff) || lua_pcall(state,0,num_result,-2);

	if(error)
	{
		if(!lua_isnil(state, -1))
		{
			const char *msg = lua_tostring(state, -1);

			lua_pop(state, 2);//pop error msg and traceback function

			return msg;
		}
			
	}

	if(return_value != NULL)
	{
		if(lua_type(state,-1) != LUA_TNONE)
		{
			if(lua_type(state,-1) == LUA_TSTRING)
				w_strcpy((char*)return_value,return_size,lua_tostring(state,-1));
			else if(lua_type(state,-1) == LUA_TNUMBER)
			{
				if (lua_isinteger(state, -1)) {
					lua_Integer integer = lua_tointeger(state, -1);
					memcpy(return_value, &integer, return_size);
				}
				else {
					lua_Number num = lua_tonumber(state, -1);
					memcpy(return_value, &num, return_size);
				}
				
			}
			else if(lua_type(state,-1) == LUA_TBOOLEAN)
				(*(wbool*)return_value) = (wbool)lua_toboolean(state,-1);
			else if (lua_type(state, -1) == LUA_TTABLE)
			{
				//in case of table, special handling of script_binary_return_t

				size_t arraylen = (size_t)luaL_len(state, -1);

				script_binary_return_t* returnbin = (script_binary_return_t*)return_value;

				if (arraylen != 2)
				{
					logprint("Return array error, the returned array in LUA as more than 2 element, must conform to type script_binary_return_t");
					lua_settop(state, 0);
					return NULL;
				}

				lua_rawgeti(state, -1, 1);

				if (lua_type(state, -1) != LUA_TNUMBER) {
					logprint("Return array error, type script_binary_return_t param 1 must be binary data len / integer");
					
					lua_settop(state, 0);
					return NULL;
				}

				returnbin->binarydatalen = (size_t)lua_tointeger(state, -1);
				lua_pop(state, 1);//pop len param

				lua_rawgeti(state, -1, 2);

				if (lua_type(state, -1) != LUA_TSTRING) {
					logprint("Return array error, type script_binary_return_t param 2 must be binary data / string");
					lua_settop(state, 0);
					return NULL;
				}


				returnbin->binarydata = (unsigned char*)wf_malloc(returnbin->binarydatalen);
				w_memcpy(returnbin->binarydata, returnbin->binarydatalen, lua_tolstring(state, -1, &returnbin->binarydatalen), returnbin->binarydatalen);

				lua_pop(state, 1);//pop binary param
			}
			
		}
	}

	lua_settop(state,0);
	return NULL;
}


void script_unwindstack(lua_State* state)
{
	lua_settop(state,0);
}

const char* script_execbuffer(lua_State* const state,const char* const buffer,size_t size)
{
	int32_t error = 0;

	lua_pushcfunction(state,traceback);

	error = luaL_loadbuffer(state,buffer,size,NULL) || lua_pcall(state,0,0,-2);

	if(error)
	{
		if(!lua_isnil(state, -1))
		{
			const char *msg = lua_tostring(state, -1);

			lua_pop(state, 2);
			return msg;
		}

			
	}

	lua_settop(state,0);

	return NULL;
}

/**
** call a function or a function in a module with the parameters in variadic array
** the number of parameters is set using the params_type string
** the params_type string is setting the type of each parameters in variadic array, each char = a parameter
** parameters type s = string, i = int32_t, f = double (because of variadic promotion), b = binary array, you need to specify two parameters, the binary array and it length as a size_t param, n = nil, no need to pass any parameter
**/
const char* script_callfunc_params(lua_State* L,const char* module,const char* func,const char* params_type,...)
{

	lua_pushcfunction(L,traceback);
	int tracebackpos = -2;

	if(module)
	{
		lua_getglobal(L,module);

		if(lua_type(L,-1) != LUA_TTABLE)
		{
			logprint("module %s is not loaded!");
			lua_settop(L,0);
			return NULL;
		}


		lua_getfield(L,-1,func);

		if(lua_type(L,-1) != LUA_TFUNCTION)
		{
			logprint("the function %s doesn't exist in the module %s!",func,module);
			lua_settop(L,0);
			return NULL;
		}

		lua_replace(L,-2);
	}
	else
	{
		lua_getglobal(L,func);

		if(lua_type(L,-1) != LUA_TFUNCTION)
		{
			logprint("function %s doesn't exist!");
			lua_settop(L,0);
			return NULL;
		}
	}

	int32_t params_num = 0;

	if(params_type != NULL)
	{
		va_list args;

		va_start(args, params_type);

		params_num = strlen(params_type);

		for (int32_t i = 0; i < params_num; ++i) 
		{
			switch(params_type[i])
			{
				case 'i':
					lua_pushinteger(L,va_arg(args,int32_t));
					break;
				case 'f':
					lua_pushnumber(L,va_arg(args,double));//variadic parameters are "promoted" when passed around, so float -> double
					break;
				case 's':
					lua_pushstring(L,va_arg(args,const char*));
					break;
				case 'b':
				{
					char* binary_data = va_arg(args, char*);
					size_t len_data = va_arg(args, size_t);
					lua_pushlstring(L, (const char*)binary_data, len_data);
				}
				break;
				case 'n':
					lua_pushnil(L);
					break;
			}
		}

		va_end(args);
	}

	//set the position of the errorhandling function on the stack based on the number of parameters
	tracebackpos -= params_num;

	if(lua_pcall(L,params_num,0, tracebackpos) != 0)
	{
		const char* msg = lua_tostring(L, -1);
		lua_settop(L,0);
		return msg;
	}

	lua_settop(L,0);
	return NULL;
}

void script_callfunc_binaryparam(lua_State* L,const char* module,const char* func,unsigned char* binary_data,size_t len_data, void* return_value, size_t return_size)
{
	lua_pushcfunction(L,traceback);
	int tracebackpos = -2;


	if(module)
	{
		lua_getglobal(L,module);

		if(lua_type(L,-1) != LUA_TTABLE)
		{
			logprint("module %s is not loaded!");
			lua_settop(L,0);
			return;
		}


		lua_getfield(L,-1,func);

		if(lua_type(L,-1) != LUA_TFUNCTION)
		{
			logprint("the function %s doesn't exist in the module %s!",func,module);
			lua_settop(L,0);
			return;
		}

		lua_replace(L,-2);
	}
	else
	{
		lua_getglobal(L,func);

		if(lua_type(L,-1) != LUA_TFUNCTION)
		{
			logprint("function %s doesn't exist!");
			lua_settop(L,0);
			return;
		}
	}

	lua_pushlstring(L,(char*)binary_data,len_data);

	//one parameter pushed on the stack, the position of error handling function changed
	tracebackpos--;

	if(lua_pcall(L,1,0, tracebackpos) != 0)
	{
		logprint("error running function `%s': %s \n",
                 func, lua_tostring(L, -1));
	}

	if (return_value != NULL)
	{
		if (lua_type(L, -1) != LUA_TNONE)
		{
			if (lua_type(L, -1) == LUA_TSTRING)
			{
				w_strcpy((char*)return_value, return_size, lua_tostring(L, -1));
			}
			else if (lua_type(L, -1) == LUA_TNUMBER)
			{
				if (lua_isinteger(L, -1)) {
					lua_Integer integer = lua_tointeger(L, -1);
					memcpy(return_value, &integer, return_size);
				}
				else {
					lua_Number num = lua_tonumber(L, -1);
					memcpy(return_value, &num, return_size);
				}

			}
			else if (lua_type(L, -1) == LUA_TBOOLEAN)
			{
				(*(wbool*)return_value) = (wbool)lua_toboolean(L, -1);
			}
			else if (lua_type(L, -1) == LUA_TTABLE)
			{
				//in case of table, special handling of script_binary_return_t

				size_t arraylen = (size_t)luaL_len(L, -1);

				script_binary_return_t* returnbin = (script_binary_return_t*)return_value;

				if (arraylen != 2)
				{
					logprint("Return array error, the returned array in LUA as more than 2 element, must conform to type script_binary_return_t");
					lua_settop(L, 0);
					return;
				}

				lua_rawgeti(L, -1, 1);

				if (lua_type(L, -1) != LUA_TNUMBER) {
					logprint("Return array error, type script_binary_return_t param 1 must be binary data len / integer");

					lua_settop(L, 0);
					return;
				}

				returnbin->binarydatalen = (size_t)lua_tointeger(L, -1);
				lua_pop(L, 1);//pop len param

				lua_rawgeti(L, -1, 2);

				if (lua_type(L, -1) != LUA_TSTRING) {
					logprint("Return array error, type script_binary_return_t param 2 must be binary data / string");
					lua_settop(L, 0);
					return;
				}

				returnbin->binarydata = (unsigned char*)wf_malloc(returnbin->binarydatalen);
				w_memcpy(returnbin->binarydata, returnbin->binarydatalen, lua_tolstring(L, -1, &returnbin->binarydatalen), returnbin->binarydatalen);
				lua_pop(L, 1);//pop binary param
			}
				
		}
	}

	lua_settop(L,0);
}

void script_genscriptinitonlyctor(const char* ctorstring,const char* id,uint32_t script_id,char* output,size_t outputlen)
{
	w_sprintf(output, outputlen, "%s", ctorstring);
	
	//remove last char, must be always ')'
	output[strlen(ctorstring) - 1] = 0;

	if (output[strlen(ctorstring) - 2] == '(') {
		w_sprintf(output, outputlen, "%s'%s', %d)", output, id,script_id);
	}
	else
	{
		w_sprintf(output, outputlen, "%s,'%s', %d)", output, id, script_id);
	}
	
}