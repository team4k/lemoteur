#ifdef _MSC_VER
    extern "C" {
#endif
        
#include "lauxlib.h"
#include "lualib.h"
#include "lua.h"
        
#ifdef _MSC_VER
 }
#endif

#if defined(_WIN32)
    #include <windows.h>
#else
	#ifndef __USE_BSD
        	#define __USE_BSD
	#endif
        #include <unistd.h>
        #include <dlfcn.h>
		#include <wchar.h>
		#include <pthread.h>
#endif

#include <stdarg.h>
#include <string.h>
#include <inttypes.h>
#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include <math.h>
#include <sqlite3.h>

#include <Engine.h>

#include <Render.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>
#include <Base/geom.h>

#include <Render/render_manager.h>

#include <GameObjects/Shaders.pb-c.h>
#include <Graphics/TextureLoader.h>
#include <Resx/shader_loader.h>

#include <Utils/Hash.h>
#include <Utils/hashmap.h>

#include <GameObjects/Characters.pb-c.h>
#include <Graphics/Animation.h>
#include <Graphics/Sprite.h>
#include <Graphics/Mesh.h>

#include <Graphics/Light.h>
#include <Graphics/LightManager.h>

#include <Graphics/TextureRegion.h>
#include <Graphics/SpriteBatch.h>
#include <Debug/Logprint.h>



#include <GameObjects/Level.pb-c.h>

#include <chipmunk/chipmunk_private.h>

#include <Physics/Physics.h>

#include <Resx/localization.h>


#include <Graphics/FontLoader.h>


#include <GameObjects/entity.h>
#include <GameObjects/Trigger.h>
#include <Font/RenderText.h>
#include <GameObjects/text_entity.h>

#include <Sound/Sound.h>

#include <Resx/Resources.h>

#include <Graphics/TilemapLayer.h>
#include <GameObjects/Tilemap.h>

#include <GameObjects/entity_group.h>
#include <Graphics/Effects/particle_object.h>
#include <GameObjects/particles_entity.h>

#include <Config/config.h>
#include <GameMngr/input_mngr.h>
#include <game_functions.h>
#include <GameMngr/module_mngr.h>
#include <GameObjects/map.h>
#include <GameObjects/Collider.h>
#include <GameObjects/waypoint.h>
#include <GameObjects/Levelobject.h>
#include <GameObjects/level_base.h>

#include <GameObjects/level_stream.h>

#include <Scripting/ScriptUtils.h>
#include <Scripting/ScriptLuaUtils.h>
#include <Scripting/ScriptLoader.h>
#include <Scripting/ScriptPointers.h>


static int world_setdamping(lua_State* L)
{
	float damp = (float)lua_tonumber(L,1);

	LEVEL_TYPE type = lua_lvlbase_gettype(L);

	lua_getglobal(L, "mngr");


	lua_pushstring(L, (type == L_STATIC) ? "static_physics_mngr" : "stream_physics_mngr");
	lua_gettable(L,-2);


	cpSpace* world = ((physics_manager_t*)lua_touserdata(L,-1))->world;

	lua_pop(L,1);

	cpSpaceSetDamping(world,damp);

	 return 1;
}

static int world_setgravity(lua_State* L)
{
	if(!lua_istable(L,1))
	{
		luaL_error(L,"The first parameter of world_setgravity is not a table!"); 
		return 1;
	}

	cpVect gravity = cpv(getluatable_value(L,"x",1),getluatable_value(L,"y",1));

	cpSpace* world;
	LEVEL_TYPE type = lua_lvlbase_gettype(L);

	lua_getglobal(L, "mngr");

	lua_pushstring(L, (type == L_STATIC) ? "static_physics_mngr" : "stream_physics_mngr");
	lua_gettable(L,-2);

	world = ((physics_manager_t*)lua_touserdata(L,-1))->world;

	lua_pop(L,1);

	cpSpaceSetGravity(world,gravity);

	return 1;

}

static int world_setelasticity(lua_State* L)
{
	cpFloat elast = lua_tonumber(L,1);

	physics_manager_t* world;

	LEVEL_TYPE type = lua_lvlbase_gettype(L);

	lua_getglobal(L, "mngr");

	lua_pushstring(L, (type == L_STATIC) ? "static_physics_mngr" : "stream_physics_mngr");
	lua_gettable(L,-2);

	world = (physics_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	physics_setelasticity(world,elast);


	return 1;

}

static int world_setfriction(lua_State* L)
{
	cpFloat friction = lua_tonumber(L,1);

	physics_manager_t* world;

	LEVEL_TYPE type = lua_lvlbase_gettype(L);

	lua_getglobal(L, "mngr");


	lua_pushstring(L, (type == L_STATIC) ? "static_physics_mngr" : "stream_physics_mngr");
	lua_gettable(L,-2);

	world = (physics_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	physics_setfriction(world,friction);


	return 1;

}

static int world_dummyupdate(lua_State* L)
{
	physics_manager_t* world;

	LEVEL_TYPE type = lua_lvlbase_gettype(L);

	lua_getglobal(L, "mngr");

	lua_pushstring(L, (type == L_STATIC) ? "static_physics_mngr" : "stream_physics_mngr");
	lua_gettable(L,-2);

	world = (physics_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	update_physics(world,0.005f);

	return 1;
}

static int world_posoutofloadedbound(lua_State* L)
{
	LEVEL_TYPE ctype = L_STATIC;

	if (!lua_istable(L, 1))
	{
		luaL_error(L, "The first parameter of world_posoutofloadedbound is not a table / vector!");
		return 1;
	}

	Vector_t position = Vec(getluatable_value(L, "x", 1), getluatable_value(L, "y", 1));


	void* lvl_mngr = lua_lvlbase_getmanager(L, 2, &ctype);

	if (ctype == L_STREAM)
	{
		level_stream_t* mngr = (level_stream_t*)lvl_mngr;

		Rect_t loadedworldbound = { 0 };
		loadedworldbound.position = mngr->access_chunk[O_TOPLEFT]->bounds.position;
		loadedworldbound.width = (mngr->base_width * mngr->world_col_limit);
		loadedworldbound.height = (mngr->base_height * mngr->world_row_limit);

		lua_pushboolean(L, !vector_in_rect(&loadedworldbound, &position));
	}
	else
	{
		Levelobject_t* mngr = (Levelobject_t*)lvl_mngr;

		lua_pushboolean(L, !vector_in_rect(&mngr->current_map->map_dimension,&position));
	}

	return 1;
}



static int game_print(lua_State* L)
{
	const char* info = lua_tostring(L,1);

	logprint(info);

	return 1;
}

static int game_gettime(lua_State* L)
{
	float return_time = 0.0f;
	double ms_time = render_gettime();
	return_time = (float)ms_time / 1000;
	lua_pushnumber(L,return_time);

	return 1;
}

static int game_getcurrentlevel(lua_State* L)
{
	LEVEL_TYPE ctype = L_STATIC;

	void* lvl_mngr = lua_lvlbase_getmanager(L, 1, &ctype);

	if (ctype == L_STATIC)
	{
		lua_pushstring(L,((Levelobject_t*)lvl_mngr)->current_level);
	}
	else
	{
		lua_pushnil(L);
	}

	return 1;
}

static int game_getcurrentmap(lua_State* L)
{
	LEVEL_TYPE ctype = L_STATIC;

	void* lvl_mngr = lua_lvlbase_getmanager(L, 1, &ctype);

	if (ctype == L_STATIC)
	{
		lua_pushstring(L, ((Levelobject_t*)lvl_mngr)->current_map_id);
	}
	else
	{
		lua_pushnil(L);
	}

	return 1;
}



static int game_requestloadlevel(lua_State* L)
{
	const char* level_key = lua_tostring(L,1);

	LEVEL_TYPE ctype = L_STATIC;

	void* lvl_mngr = lua_lvlbase_getmanager(L, 2, &ctype);

	if (ctype == L_STATIC)
	{
		Levelobject_requestnewlevel((Levelobject_t*)lvl_mngr, level_key);
	}
	else
	{
		luaL_error(L, "Not implemented !");
	}

	return 1;
}

static int game_checktriggers(lua_State* L)
{

	const char* entity_id = lua_tostring(L, 1);

	LEVEL_TYPE type = L_STATIC;

	void* lvl_mngr = lua_lvlbase_getmanager(L, 2,&type);

	entity_t* entity = lua_lvlbase_get_entity(L, entity_id, 2);

	cpBB box = cpShapeGetBB(entity->shape[0]);
	lvlbase_checktriggers(lvl_mngr,type, &box, entity_id);

	return 1;
}

static int game_resetinitonlyentities(lua_State* L)
{
	LEVEL_TYPE type = L_STATIC;

	void* lvl_mngr = lua_lvlbase_getmanager(L, 1, &type);

	lvlbase_resetscriptinitonlyentities(lvl_mngr, type, L);

	return 1;
}

static int game_switchmap(lua_State* L)
{
	const char* map_name = lua_tostring(L,1);

	physics_manager_t* phy_mngr;

	LEVEL_TYPE ctype = L_STATIC;

	void* lvl_mngr = lua_lvlbase_getmanager(L, 2, &ctype);


	lua_getglobal(L, "mngr");


	lua_pushstring(L, (ctype == L_STATIC) ? "static_physics_mngr" : "stream_physics_mngr");
	lua_gettable(L,-2);

	phy_mngr = (physics_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1); 

	if (ctype == L_STATIC)
	{
		Levelobject_switchmap((Levelobject_t*)lvl_mngr, phy_mngr,L,map_name);
	}
	else
	{
		luaL_error(L, "Not implemented !");
	}


	return 1;
}

static int game_switchleveltype(lua_State* L)
{
	LEVEL_TYPE requested_type = (LEVEL_TYPE)lua_tointeger(L, 1);
	const char* requested_level = lua_tostring(L, 2);

	level_base_t* level_container = lua_getlvlbase(L);

	if (level_container)
	{
		lvlbase_requestswitchmode(level_container, requested_type,requested_level);
	}

	return 1;
}

static int game_setpause(lua_State* L)
{
	wbool pause_state = (wbool)lua_toboolean(L,1);
	wbool playsound = (wbool)lua_toboolean(L,2);

	int new_state = COMMON_RUN;

	if(pause_state)
		new_state = COMMON_PAUSE;

	LEVEL_TYPE ctype = L_STATIC;

	void* lvl_mngr = lua_lvlbase_getmanager(L, 3, &ctype);

	lua_getglobal(L,"mngr");


	module_mngr_t* module_mngr;

	lua_pushstring(L,"module_mngr");
	lua_gettable(L,-2);

	module_mngr = (module_mngr_t*)lua_touserdata(L,-1);

	lua_pop(L,1);


	int current_state = lvlbase_getcurrentstate(lvl_mngr, ctype);

	if(new_state == COMMON_PAUSE && current_state != new_state)
		module_mngr->game_pause(playsound);

	if(new_state == COMMON_RUN && current_state == COMMON_PAUSE)
		module_mngr->game_unpause();

	lvlbase_setcurrentstate(lvl_mngr, ctype, new_state);

	return 1; 
}

static int game_getlevelstate(lua_State* L)
{
	LEVEL_TYPE ctype = L_STATIC;

	void* lvl_mngr = lua_lvlbase_getmanager(L, 1, &ctype);

	int current_state = lvlbase_getcurrentstate(lvl_mngr, ctype);

	lua_pushinteger(L, current_state);

	return 1;
}


static int game_getversion(lua_State* L)
{
	lua_getglobal(L,"mngr");

	module_mngr_t* module_mngr;

	lua_pushstring(L,"module_mngr");
	lua_gettable(L,-2);

	module_mngr = (module_mngr_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	const char* version_string = module_mngr->game_getversion();

	char l_version[64];

	w_strcpy(l_version,64,version_string);
	
	lua_pushstring(L,l_version);

	return 1;
}

static int game_exit(lua_State* L)
{
	LEVEL_TYPE ctype = L_STATIC;

	void* lvl_mngr = lua_lvlbase_getmanager(L, 1, &ctype);

	lvlbase_requestexit(lvl_mngr, ctype);

	return 1;
}

static int game_getmenuptr(lua_State* L)
{
	const char* menu_id = lua_tostring(L,1);

	lua_getglobal(L,"mngr");

	Levelobject_t* menu_obj;

	lua_pushstring(L,menu_id);
	lua_gettable(L,-2);

	if(lua_type(L,-1) == LUA_TNIL || lua_type(L,-1) == LUA_TNONE)
	{
		lua_pop(L,1);
		lua_pushnil(L);
	}
	else
	{
		menu_obj = (Levelobject_t*)lua_touserdata(L,-1);

		lua_pop(L,1);

		if (menu_obj->script_id == 0) {
			menu_obj->script_id = script_registerpointer((intptr_t)menu_obj,LEVELOBJECT);
		}

		lua_pushinteger(L, menu_obj->script_id);
	}


	return 1;
}

static int game_getdefaultlvlptr(lua_State* L)
{
	LEVEL_TYPE ctype = L_STATIC;

	void* lvl_mngr = lua_lvlbase_getmanager(L, 1, &ctype);

	uint32_t scriptid = lvlbase_getcurrentscriptid(lvl_mngr, ctype);

	lua_pushinteger(L, scriptid);

	return 1;
}

static int game_getlvlptr(lua_State* L)
{
	LEVEL_TYPE requested_type = (LEVEL_TYPE)lua_tointeger(L, 1);

	level_base_t* level_container = lua_getlvlbase(L);

	if (level_container)
	{
		uint32_t script_id = lvlbase_getscriptid(level_container, requested_type);

		if (script_id > 0)
			lua_pushinteger(L, script_id);
		else
			lua_pushnil(L);
	}

	return 1;
}

static int menu_getcurrentoffset(lua_State* L)
{
	uint32_t menu_ptr = (uint32_t)lua_tointeger(L, 1);

	intptr_t number = script_getpointerfromid(menu_ptr, LEVELOBJECT);

	Levelobject_t* menu_obj = (Levelobject_t*)(number);

	if(menu_obj->current_map)
	{
		lua_newtable(L);
		lua_pushnumber(L, menu_obj->current_map->map_dimension.position.x);
		lua_setfield(L,-2,"x");
		lua_pushnumber(L,menu_obj->current_map->map_dimension.position.y);
		lua_setfield(L,-2,"y");

		lua_getglobal(L,"Vector");//we use an already defined metatable in lua, it's stored in a global in this case and not in the registry
		lua_setmetatable(L,-2);
	}

	return 1;
}

static int menu_getsize(lua_State* L)
{
	uint32_t menu_ptr = (uint32_t)lua_tointeger(L, 1);

	intptr_t number = script_getpointerfromid(menu_ptr, LEVELOBJECT);

	Levelobject_t* menu_obj = (Levelobject_t*)(number);

	if(menu_obj->current_map)
	{
		lua_newtable(L);
		lua_pushnumber(L, menu_obj->current_map->map_dimension.width);
		lua_setfield(L,-2,"width");
		lua_pushnumber(L,menu_obj->current_map->map_dimension.height);
		lua_setfield(L,-2,"height");
	}

	return 1;
}

static int menu_setzoom(lua_State* L)
{
	uint32_t menu_ptr = (uint32_t)lua_tointeger(L, 1);
	lua_Number zoom = lua_tonumber(L,2);
	wbool set_zoom = (wbool)lua_toboolean(L,3);

	intptr_t number = script_getpointerfromid(menu_ptr, LEVELOBJECT);

	Levelobject_t* menu_obj = (Levelobject_t*)(number);

	levelobject_setmenuzoom(menu_obj,(float)zoom,set_zoom);

	return 1;
}

static int menu_getzoom(lua_State* L)
{
	uint32_t menu_ptr = (uint32_t)lua_tointeger(L, 1);

	intptr_t number = script_getpointerfromid(menu_ptr, LEVELOBJECT);

	Levelobject_t* menu_obj = (Levelobject_t*)(number);

	if(menu_obj->menu_use_zoom)
	{
		lua_pushnumber(L,menu_obj->menu_zoom);
	}
	else
	{
		lua_pushnumber(L,1.0f);
	}


	return 1;
}



static int lua_config_getbool(lua_State* L)
{
	const char* config_id = NULL;

	config_id = lua_tostring(L,1);

	lua_getglobal(L,"mngr");
	config_t* config_mngr;

	lua_pushstring(L,"config");
	lua_gettable(L,-2);

	config_mngr = (config_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	wbool rval = wfalse;

	config_getbool(config_mngr,config_id,&rval);

	lua_pushboolean(L,(int)rval);

	return 1;
}


static int lua_config_setbool(lua_State* L)
{
	const char* config_id = NULL;
	wbool config_value = wfalse;

	config_id = lua_tostring(L,1);
	config_value = (wbool)lua_toboolean(L,2);

	lua_getglobal(L,"mngr");
	config_t* config_mngr;

	lua_pushstring(L,"config");
	lua_gettable(L,-2);

	config_mngr = (config_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	config_setbool(config_mngr,config_id,config_value);

	return 1;
}


static int lua_config_getstring(lua_State* L)
{
	const char* config_id = NULL;

	config_id = lua_tostring(L,1);

	lua_getglobal(L,"mngr");
	config_t* config_mngr;

	lua_pushstring(L,"config");
	lua_gettable(L,-2);

	config_mngr = (config_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	char rval[256] = {0};

	config_getstring(config_mngr,config_id,&rval[0],256);

	lua_pushstring(L,rval);

	return 1;
}


static int lua_config_setstring(lua_State* L)
{
	const char* config_id = NULL;
	const char* config_value = NULL;

	config_id = lua_tostring(L,1);
	config_value = lua_tostring(L,2);

	lua_getglobal(L,"mngr");
	config_t* config_mngr;

	lua_pushstring(L,"config");
	lua_gettable(L,-2);

	config_mngr = (config_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	config_setstring(config_mngr,config_id,config_value);

	return 1;
}



static int lua_config_getint(lua_State* L)
{
	const char* config_id = NULL;

	config_id = lua_tostring(L,1);

	lua_getglobal(L,"mngr");
	config_t* config_mngr;

	lua_pushstring(L,"config");
	lua_gettable(L,-2);

	config_mngr = (config_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	int rval = 0;

	config_getint(config_mngr,config_id,&rval);

	lua_pushinteger(L, rval);

	return 1;
}


static int lua_config_setint(lua_State* L)
{
	const char* config_id = NULL;
	lua_Integer config_value = 0;

	config_id = lua_tostring(L,1);
	config_value = lua_tointeger(L,2);

	lua_getglobal(L,"mngr");
	config_t* config_mngr;

	lua_pushstring(L,"config");
	lua_gettable(L,-2);

	config_mngr = (config_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	config_setint(config_mngr,config_id,(int32_t)config_value);

	return 1;
}


static int lua_config_getfloat(lua_State* L)
{
	const char* config_id = NULL;

	config_id = lua_tostring(L,1);

	lua_getglobal(L,"mngr");
	config_t* config_mngr;

	lua_pushstring(L,"config");
	lua_gettable(L,-2);

	config_mngr = (config_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	float rval = 0;

	config_getfloat(config_mngr,config_id,&rval);

	lua_pushnumber(L,rval);

	return 1;
}


static int lua_config_setfloat(lua_State* L)
{
	const char* config_id = NULL;
	float config_value = 0.0f;

	config_id = lua_tostring(L,1);
	config_value = (float)lua_tonumber(L,2);

	lua_getglobal(L,"mngr");
	config_t* config_mngr;

	lua_pushstring(L,"config");
	lua_gettable(L,-2);

	config_mngr = (config_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	config_setfloat(config_mngr,config_id,config_value);

	return 1;
}

static int lua_config_saveconf(lua_State* L)
{
	lua_getglobal(L,"mngr");
	config_t* config_mngr;
	const char* config_path;

	lua_pushstring(L,"config");
	lua_gettable(L,-2);

	config_mngr = (config_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	lua_pushstring(L,"config_path");
	lua_gettable(L,-2);

	config_path = (const char*)lua_touserdata(L,-1);

	lua_pop(L,1);

	config_saveconf(config_mngr,&config_path[0]);

	return 1;
}

void register_game_functions(lua_State *L)
{
	//world functions
	lua_pushcfunction(L,world_setdamping);
	lua_setglobal(L,"world_setdamping");

	lua_pushcfunction(L,world_setgravity);
	lua_setglobal(L,"world_setgravity");

	lua_pushcfunction(L,world_setelasticity);
	lua_setglobal(L,"world_setelasticity");

	lua_pushcfunction(L,world_setfriction);
	lua_setglobal(L,"world_setfriction");

	lua_pushcfunction(L,world_dummyupdate);
	lua_setglobal(L,"world_dummyupdate");

	lua_pushcfunction(L, world_posoutofloadedbound);
	lua_setglobal(L, "world_posoutofloadedbound");

	//game general functions
	lua_pushcfunction(L,game_requestloadlevel);
	lua_setglobal(L, "game_requestloadlevel");

	lua_pushcfunction(L,game_print);
	lua_setglobal(L, "game_print");

	lua_pushcfunction(L,game_switchmap);
	lua_setglobal(L,"game_switchmap");

	lua_pushcfunction(L, game_switchleveltype);
	lua_setglobal(L, "game_switchleveltype");
	
	lua_pushcfunction(L,game_setpause);
	lua_setglobal(L,"game_setpause");

	lua_pushcfunction(L, game_getlevelstate);
	lua_setglobal(L, "game_getlevelstate");
	
	lua_pushcfunction(L,game_exit);
	lua_setglobal(L,"game_exit");

	lua_pushcfunction(L,game_getversion);
	lua_setglobal(L,"game_getversion");

	lua_pushcfunction(L,game_getmenuptr);
	lua_setglobal(L,"game_getmenuptr");

	lua_pushcfunction(L,game_getdefaultlvlptr);
	lua_setglobal(L,"game_getdefaultlvlptr");

	lua_pushcfunction(L, game_getlvlptr);
	lua_setglobal(L, "game_getlvlptr");

	lua_pushcfunction(L,game_gettime);
	lua_setglobal(L,"game_gettime");

	lua_pushcfunction(L,game_getcurrentmap);
	lua_setglobal(L,"game_getcurrentmap");

	lua_pushcfunction(L,game_getcurrentlevel);
	lua_setglobal(L,"game_getcurrentlevel");

	lua_pushcfunction(L,game_checktriggers);
	lua_setglobal(L,"game_checktriggers");

	lua_pushcfunction(L, game_resetinitonlyentities);
	lua_setglobal(L, "game_resetinitonlyentities");

	//menu functions
	lua_pushcfunction(L,menu_getcurrentoffset);
	lua_setglobal(L,"menu_getcurrentoffset");

	lua_pushcfunction(L,menu_getsize);
	lua_setglobal(L,"menu_getsize");

	lua_pushcfunction(L,menu_setzoom);
	lua_setglobal(L,"menu_setzoom");

	lua_pushcfunction(L,menu_getzoom);
	lua_setglobal(L,"menu_getzoom");

	//config functions
	lua_pushcfunction(L,lua_config_getbool);
	lua_setglobal(L, "config_getbool");

	lua_pushcfunction(L,lua_config_setbool);
	lua_setglobal(L, "config_setbool");

	lua_pushcfunction(L,lua_config_getstring);
	lua_setglobal(L, "config_getstring");

	lua_pushcfunction(L,lua_config_setstring);
	lua_setglobal(L, "config_setstring");

	lua_pushcfunction(L,lua_config_getint);
	lua_setglobal(L, "config_getint");

	lua_pushcfunction(L,lua_config_setint);
	lua_setglobal(L, "config_setint");
	
	lua_pushcfunction(L,lua_config_getfloat);
	lua_setglobal(L, "config_getfloat");

	lua_pushcfunction(L,lua_config_setfloat);
	lua_setglobal(L, "config_setfloat");

	lua_pushcfunction(L,lua_config_saveconf);
	lua_setglobal(L, "config_saveconf");

}
