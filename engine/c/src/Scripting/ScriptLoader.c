//TODO: lua_is* function are misleading and don't check variable type but if a variable can be converted to a type, replace
//all lua_is* function with lua_type(L,stack_pos) == <LUA_TYPE> to check the type


#ifdef _MSC_VER
    extern "C" {
#endif
        
#include "lauxlib.h"
#include "lualib.h"
#include "lua.h"
        
#ifdef _MSC_VER
 }
#endif

#if defined(_WIN32)
    #include <windows.h>
#else
	#ifndef __USE_BSD
        	#define __USE_BSD
	#endif
        #include <unistd.h>
        #include <dlfcn.h>
		#include <wchar.h>
		#include <pthread.h>
#endif

#include <stdarg.h>
#include <string.h>
#include <inttypes.h>
#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include <math.h>
#include <sqlite3.h>

#include <Engine.h>

#include <Render.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>
#include <Base/geom.h>

#include <Render/render_manager.h>

#include <Graphics/TextureLoader.h>
#include <Resx/shader_loader.h>

#include <Utils/Hash.h>
#include <Utils/hashmap.h>

#include <GameObjects/Characters.pb-c.h>
#include <Graphics/Animation.h>
#include <Graphics/Sprite.h>

#include <Graphics/TextureRegion.h>
#include <Graphics/SpriteBatch.h>
#include <Debug/Logprint.h>

#include <GameObjects/Level.pb-c.h>
#include <Resx/localization.h>
#include <Graphics/FontLoader.h>
#include <Font/RenderText.h>

#include <Sound/Sound.h>

#ifdef EMSCRIPTEN
	#include <Sound/snd_mngr.h>
#endif

#include <Resx/Resources.h>

#include <Scripting/ScriptLoader.h>


static int keepresource(lua_State* L)
{
	const char* resxname = lua_tostring(L,1);
	const char* resxtype = get_filename_ext(resxname);
	resx_type type = texture;

	lua_getglobal(L,"mngr");

	lua_pushstring(L,"resx_mngr");
	lua_gettable(L,-2);

	resources_manager_t* resx_mngr = (resources_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	wbool notfound = wtrue;

     char** resx_ext = getresxext();

	for(uint32_t i = 0; i < getresxextsize();i++)
	{
		if (strcmp(resxtype, resx_ext[i]) == 0)
		{
			type = (resx_type)i;
			notfound = wfalse;
			break;
		}
	}

	if(notfound)
	{
		logprint("[LUA]:keepresource: Invalid resxtype %s",resxtype);
		return 1;
	}

	#ifdef EMSCRIPTEN
		if(type == sound || type == sound_uncompressed)
		{
			lua_pushstring(L,"sound_mngr");
			lua_gettable(L,-2);

			snd_mngr_t* snd_mngr = (snd_mngr_t*)lua_touserdata(L,-1);

			lua_pop(L,1);

			if(snd_mngr->fallback_to_44100)
			{
				char sample_fixed_key[512];
				w_sprintf(sample_fixed_key,512,"%s441.%s",resxname,resxtype);
				getgeneric_resx(resx_mngr,sample_fixed_key,type);
				return 1;
			}
		}
	#endif

	getgeneric_resx(resx_mngr,resxname,type);

	return 1;

}

static int lua_gettexturesize(lua_State* L)
{
	const char* resxname = lua_tostring(L,1);

	lua_getglobal(L,"mngr");

	lua_pushstring(L,"resx_mngr");
	lua_gettable(L,-2);

	resources_manager_t* resx_mngr = (resources_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);


	texture_info* txtinfo = gettexture_resx(resx_mngr,resxname);

	if(txtinfo != NULL)
	{
		lua_newtable(L);
		lua_pushnumber(L, txtinfo->width);
		lua_setfield(L,-2,"width");
		lua_pushnumber(L, txtinfo->height);
		lua_setfield(L,-2,"height");
	}
	else
	{
		lua_pushnil(L);
	}

	return 1;
}



void register_functions(lua_State *L)
{
	register_ai_functions(L);
	register_effects_functions(L);
	register_entities_functions(L);
	register_game_functions(L);
	register_light_functions(L);
	register_render_functions(L);
	register_sound_function(L);
	register_input_functions(L);


	//resources functions
	lua_pushcfunction(L,keepresource);
	lua_setglobal(L, "keepresource");

	lua_pushcfunction(L,lua_gettexturesize);
	lua_setglobal(L, "resx_gettexturesize");
	
}



