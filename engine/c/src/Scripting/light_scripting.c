//TODO: lua_is* function are misleading and don't check variable type but if a variable can be converted to a type, replace
//all lua_is* function with lua_type(L,stack_pos) == <LUA_TYPE> to check the type


#ifdef _MSC_VER
    extern "C" {
#endif
        
#include "lauxlib.h"
#include "lualib.h"
#include "lua.h"
        
#ifdef _MSC_VER
 }
#endif

#if defined(_WIN32)
    #include <windows.h>
#else
	#ifndef __USE_BSD
        	#define __USE_BSD
	#endif
        #include <unistd.h>
        #include <dlfcn.h>
		#include <wchar.h>
		#include <pthread.h>
#endif

#include <stdarg.h>
#include <string.h>
#include <inttypes.h>
#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include <math.h>
#include <sqlite3.h>

#include <Engine.h>

#include <Render.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>
#include <Base/geom.h>

#include <Render/render_manager.h>

#include <GameObjects/Shaders.pb-c.h>
#include <Graphics/TextureLoader.h>
#include <Resx/shader_loader.h>

#include <Utils/Hash.h>
#include <Utils/hashmap.h>

#include <GameObjects/Characters.pb-c.h>
#include <Graphics/Animation.h>
#include <Graphics/Sprite.h>
#include <Graphics/Mesh.h>

#include <Graphics/Light.h>
#include <Graphics/LightManager.h>

#include <Graphics/TextureRegion.h>
#include <Graphics/SpriteBatch.h>
#include <Debug/Logprint.h>


#include <Debug/geomdebug.h>

#include <GameObjects/Level.pb-c.h>
#include <Collisions/Collisions.h>

#include <chipmunk/chipmunk_private.h>

#include <Physics/Physics.h>

#include <Resx/localization.h>


#include <Graphics/FontLoader.h>


#include <GameObjects/entity.h>
#include <GameObjects/Trigger.h>
#include <Font/RenderText.h>
#include <GameObjects/text_entity.h>


#include <Sound/Sound.h>

#include <Resx/Resources.h>

#include <Graphics/TilemapLayer.h>
#include <GameObjects/Tilemap.h>

#include <Physics/physics_utils.h>

#include <GameObjects/entity_group.h>
#include <Graphics/Effects/particle_object.h>
#include <GameObjects/particles_entity.h>

#include <Config/config.h>
#include <GameMngr/input_mngr.h>
#include <game_functions.h>
#include <GameMngr/module_mngr.h>
#include <GameObjects/map.h>
#include <GameObjects/Collider.h>
#include <GameObjects/waypoint.h>
#include <GameObjects/Levelobject.h>
#include <GameObjects/level_base.h>


#include <Scripting/ScriptUtils.h>
#include <Scripting/ScriptLuaUtils.h>
#include <Scripting/ScriptLoader.h>

//light scripting
static int light_mngr_setambient(lua_State* L)
{
	lua_Number r = lua_tonumber(L,1);
	lua_Number g = lua_tonumber(L,2);
	lua_Number b = lua_tonumber(L,3);
	lua_Number a = lua_tonumber(L,4);

	lua_getglobal(L,"mngr");

	lua_pushstring(L,"light_mngr");
	lua_gettable(L,-2);

	light_manager_t* light_mngr = (light_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	ColorA_t color_a = {(float)r,(float)g,(float)b,(float)a};

	light_mngr->ambient_color = color_a;

	return 1;
}

static int light_mngr_addlight(lua_State* L)
{
	const char* light_id = lua_tostring(L,1);

	Vector_t position = vectorzero;

	if(lua_istable(L,2))
	{
		position.x =  getluatable_value(L,"x",2);
		position.y = getluatable_value(L,"y",2);
	}

	ColorA_t color = white_color;

	if(lua_istable(L,3))
	{
		color.r = getluatable_value(L,"r",3);
		color.g = getluatable_value(L,"g",3);
		color.b = getluatable_value(L,"b",3);
		color.a = getluatable_value(L,"a",3);
	}

	lua_Number radius = lua_tonumber(L,4);

	light_type l_type = (light_type)lua_tointeger(L,5);

	const char* map_id = lua_tostring(L,6);

	if(map_id == NULL)
	{
		luaL_error(L,"Can't add a light without a map_id! %s",light_id);
		return 1;
	}


	lua_getglobal(L,"mngr");

	lua_pushstring(L,"light_mngr");
	lua_gettable(L,-2);

	light_manager_t* light_mngr = (light_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);


	if(!hashmap_haskey(&light_mngr->light_hash,light_id))
		light_manager_addlight(light_mngr,position,color,(float)radius,light_id,map_id,l_type);
	else
		luaL_error(L,"A light with the id %s already exist!",light_id);

	return 1;
}

static int light_mngr_haslight(lua_State* L)
{
	const char* light_id = lua_tostring(L, 1);

	lua_getglobal(L, "mngr");

	lua_pushstring(L, "light_mngr");
	lua_gettable(L, -2);

	light_manager_t* light_mngr = (light_manager_t*)lua_touserdata(L, -1);

	lua_pop(L, 1);


	lua_pushboolean(L, hashmap_haskey(&light_mngr->light_hash, light_id));

	return 1;
}

static int light_mngr_removelight(lua_State* L)
{
	const char* light_id = lua_tostring(L,1);

	lua_getglobal(L,"mngr");

	lua_pushstring(L,"light_mngr");
	lua_gettable(L,-2);

	light_manager_t* light_mngr = (light_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	light_manager_removelight(light_mngr,light_id);

	return 1;
}

static int light_setenable(lua_State* L)
{
	const char* light_id = lua_tostring(L,1);
	wbool light_enable = (wbool)lua_toboolean(L,2);

	lua_getglobal(L,"mngr");

	lua_pushstring(L,"light_mngr");
	lua_gettable(L,-2);

	light_manager_t* light_mngr = (light_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	light_t* olight = light_manager_getlight(light_mngr,light_id);

	olight->enabled = light_enable;

	return 1;
}

static int light_setcolor(lua_State* L)
{
	const char* light_id = lua_tostring(L,1);
	lua_Number r = lua_tonumber(L,2);
	lua_Number g = lua_tonumber(L,3);
	lua_Number b = lua_tonumber(L,4);
	lua_Number a = lua_tonumber(L,5);

	ColorA_t color_a = {(float)r,(float)g,(float)b,(float)a};

	lua_getglobal(L,"mngr");

	lua_pushstring(L,"light_mngr");
	lua_gettable(L,-2);

	light_manager_t* light_mngr = (light_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	light_t* olight = light_manager_getlight(light_mngr,light_id);

	olight->color = color_a;

	return 1;
}

static int light_setradius(lua_State* L)
{
	const char* light_id = lua_tostring(L,1);
	lua_Number radius = lua_tonumber(L,2);

	lua_getglobal(L,"mngr");

	lua_pushstring(L,"light_mngr");
	lua_gettable(L,-2);

	light_manager_t* light_mngr = (light_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	light_t* olight = light_manager_getlight(light_mngr,light_id);

	olight->radius = (float)radius;

	return 1;
}

static int lua_light_setposition(lua_State* L)
{
	const char* light_id = lua_tostring(L,1);
	Vector_t position = {0,0};

	if(lua_istable(L,2))
	{
		position.x =  getluatable_value(L,"x",2);
		position.y = getluatable_value(L,"y",2);
	}


	lua_getglobal(L,"mngr");

	lua_pushstring(L,"light_mngr");
	lua_gettable(L,-2);

	light_manager_t* light_mngr = (light_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	light_t* olight = light_manager_getlight(light_mngr,light_id);

	light_setposition(olight,position);

	return 1;
}

static int lua_light_setrotationangle(lua_State* L)
{
	const char* light_id = lua_tostring(L,1);
	lua_Number new_angle = lua_tonumber(L,2);

	lua_getglobal(L,"mngr");

	lua_pushstring(L,"light_mngr");
	lua_gettable(L,-2);

	light_manager_t* light_mngr = (light_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	light_t* olight = light_manager_getlight(light_mngr,light_id);

	if(olight->l_type != light_directional)
		luaL_error(L,"Can't set start angle on a non directional light!");
	else
		light_setrotationangle(olight,(wfloat)new_angle);


	return 1;
}

static int lua_light_setopenangle(lua_State* L)
{
	const char* light_id = lua_tostring(L,1);
	lua_Number new_angle = lua_tonumber(L,2);

	lua_getglobal(L,"mngr");

	lua_pushstring(L,"light_mngr");
	lua_gettable(L,-2);

	light_manager_t* light_mngr = (light_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	light_t* olight = light_manager_getlight(light_mngr,light_id);

	if(olight->l_type != light_directional)
		luaL_error(L,"Can't set end angle on a non directional light!");
	else
		light_setopenangle(olight,(float)new_angle);

	return 1;
}

static int lua_light_setattenuation(lua_State* L)
{
	const char* light_id = lua_tostring(L,1);
	lua_Number new_constant = lua_tonumber(L,2);
	lua_Number new_linear = lua_tonumber(L,3);
	lua_Number new_quadratic = lua_tonumber(L,4);

	lua_getglobal(L,"mngr");

	lua_pushstring(L,"light_mngr");
	lua_gettable(L,-2);

	light_manager_t* light_mngr = (light_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	light_t* olight = light_manager_getlight(light_mngr,light_id);

	
	if(olight)
	{
		light_setattenuation(olight,(float)new_constant,(float)new_linear,(float)new_quadratic);
	}

	return 1;
}

static int lua_light_setpowerfactor(lua_State* L)
{
	const char* light_id = lua_tostring(L,1);
	lua_Number power_factor = lua_tonumber(L,2);

	lua_getglobal(L,"mngr");

	lua_pushstring(L,"light_mngr");
	lua_gettable(L,-2);

	light_manager_t* light_mngr = (light_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	light_t* olight = light_manager_getlight(light_mngr,light_id);

	if(olight)
	{
		olight->power_factor = (float)power_factor;
	}

	return 1;
}

static int lua_light_checkentityinside(lua_State* L)
{
	const char* light_id = lua_tostring(L,1);
	const char* entity_id = lua_tostring(L,2);

	lua_Integer ishape = -1;

	if(lua_type(L,2) == LUA_TNUMBER)
		ishape = lua_tointeger(L,3);

	lua_getglobal(L,"mngr");

	lua_pushstring(L,"light_mngr");
	lua_gettable(L,-2);

	light_manager_t* light_mngr = (light_manager_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	light_t* olight = light_manager_getlight(light_mngr,light_id);

	
	entity_t* ent_obj = lua_lvlbase_get_entity(L,entity_id,4);

	
	if(olight && ent_obj)
	{
		triangle_t bb_tri_1;
		triangle_t bb_tri_2;

		Circle_t circle_checker;

		if(ent_obj->shape)
		{
			cpBB box;

			if(ishape != -1)
				box = ent_obj->shape[ishape]->bb;
			else
				box = ent_obj->shape[0]->bb;

			cpVect vector =  cpBBCenter(box);

			bb_tri_1.point_a = convert_cpfloatpair_tovec(box.r,box.t);
			bb_tri_1.point_b = convert_cpfloatpair_tovec(box.l,box.t);
			bb_tri_1.point_c = convert_cpfloatpair_tovec(box.l,box.b);

			bb_tri_2.point_a = convert_cpfloatpair_tovec(box.r,box.t);
			bb_tri_2.point_b = convert_cpfloatpair_tovec(box.l,box.b);
			bb_tri_2.point_c = convert_cpfloatpair_tovec(box.r,box.b);

			circle_checker.position = convert_cpvec_tovec(vector);
			circle_checker.radius = fmaxf((float)(box.r - box.l),(float)(box.t - box.b));
		}
		else
		{
			
			Rect_t* rect = entity_getrenderbox(ent_obj);

			float half_width = rect->width * 0.5f;
			float half_height = rect->height * 0.5f;

			bb_tri_1.point_a = Vec(rect->position.x + half_width, rect->position.y + half_height);
			bb_tri_1.point_b = Vec(rect->position.x - half_width, rect->position.y + half_height);
			bb_tri_1.point_c = Vec(rect->position.x - half_width, rect->position.y - half_height);

			bb_tri_2.point_a = Vec(rect->position.x + half_width, rect->position.y + half_height);
			bb_tri_2.point_b = Vec(rect->position.x - half_width, rect->position.y - half_height);
			bb_tri_2.point_c = Vec(rect->position.x + half_width, rect->position.y - half_height);

			circle_checker.position = rect->position;
			circle_checker.radius = fmaxf((float)rect->width,(float)rect->height);

		}

		Circle_t light_circle;
		light_circle.position = olight->position;
		light_circle.radius = olight->radius;

		wbool collide = wfalse;

		//quick circle check before triangle collision
		if(circle_intersect_circle(circle_checker,light_circle))
		{
			triangle_t world_space_tri;
			//check triangles collisions
			for(int32_t itri = 0; itri < olight->num_light_volumes;itri++)
			{
				world_space_tri.point_a = Vec_plus(olight->position,olight->light_volumes[itri].point_a); 
				world_space_tri.point_b = Vec_plus(olight->position,olight->light_volumes[itri].point_b);
				world_space_tri.point_c = Vec_plus(olight->position,olight->light_volumes[itri].point_c);

				if(triangle_intersect_triangle(&world_space_tri,&bb_tri_1) || triangle_intersect_triangle(&world_space_tri,&bb_tri_2))
				{
					collide = wtrue;
					break;
				}

			}
		}

		lua_pushboolean(L,(int)collide);
	}

	return 1;
}


static int light_mngr_inlight(lua_State* L)
{
	const char* entity_id = lua_tostring(L,1);

	lua_Integer ishape = -1;

	if(lua_type(L,2) == LUA_TNUMBER)
		ishape = lua_tointeger(L,2);

	entity_t* ent_obj = lua_lvlbase_get_entity(L, entity_id, 3);

	if(ent_obj)
	{
		triangle_t bb_tri_1;
		triangle_t bb_tri_2;

		Circle_t circle_checker;

		if(ent_obj->shape)
		{
			cpBB box;

			if(ishape != -1)
				box = ent_obj->shape[ishape]->bb;
			else
				box = ent_obj->shape[0]->bb;

			cpVect vector =  cpBBCenter(box);

			bb_tri_1.point_a = Vec((wfloat)box.r, (wfloat)box.t);
			bb_tri_1.point_b = Vec((wfloat)box.l, (wfloat)box.t);
			bb_tri_1.point_c = Vec((wfloat)box.l, (wfloat)box.b);

			bb_tri_2.point_a = Vec((wfloat)box.r, (wfloat)box.t);
			bb_tri_2.point_b = Vec((wfloat)box.l, (wfloat)box.b);
			bb_tri_2.point_c = Vec((wfloat)box.r, (wfloat)box.b);

			circle_checker.position = Vec((wfloat)vector.x, (wfloat)vector.y);
			circle_checker.radius = fmaxf((float)(box.r - box.l),(float)(box.t - box.b));
		}
		else
		{
			Rect_t* rect = entity_getrenderbox(ent_obj);

			float half_width = rect->width * 0.5f;
			float half_height = rect->height * 0.5f;

			bb_tri_1.point_a = Vec(rect->position.x + half_width,rect->position.y + half_height);
			bb_tri_1.point_b = Vec(rect->position.x - half_width,rect->position.y + half_height);
			bb_tri_1.point_c = Vec(rect->position.x - half_width,rect->position.y - half_height);

			bb_tri_2.point_a = Vec(rect->position.x + half_width,rect->position.y + half_height);
			bb_tri_2.point_b = Vec(rect->position.x - half_width,rect->position.y - half_height);
			bb_tri_2.point_c = Vec(rect->position.x + half_width,rect->position.y - half_height);

			circle_checker.position = rect->position;
			circle_checker.radius = fmaxf((float)rect->width,(float)rect->height);

		}

		
		lua_getglobal(L,"mngr");

		lua_pushstring(L,"light_mngr");
		lua_gettable(L,-2);

		light_manager_t* light_mngr = (light_manager_t*)lua_touserdata(L,-1);

		lua_pop(L,1);

		uint32_t light_index = 0;
		uint32_t light_count = 0;

		wbool table_created = wfalse;
		int32_t array_pos = 0;

		
		for(light_t* olight = (light_t*)hashmap_iterate(&light_mngr->light_hash,&light_index,&light_count);olight != NULL;olight = (light_t*)hashmap_iterate(&light_mngr->light_hash,&light_index,&light_count))
		{
			if(!olight->enabled)
				continue;

			Circle_t light_circle;
			light_circle.position = olight->position;
			light_circle.radius = olight->radius;

			//quick circle check before triangle collision
			if(circle_intersect_circle(circle_checker,light_circle))
			{
				triangle_t world_space_tri;
				//check triangles collisions
				for(int32_t itri = 0; itri < olight->num_light_volumes;itri++)
				{
					world_space_tri.point_a = Vec_plus(olight->position,olight->light_volumes[itri].point_a); 
					world_space_tri.point_b = Vec_plus(olight->position,olight->light_volumes[itri].point_b);
					world_space_tri.point_c = Vec_plus(olight->position,olight->light_volumes[itri].point_c);

					if(triangle_intersect_triangle(&world_space_tri,&bb_tri_1) || triangle_intersect_triangle(&world_space_tri,&bb_tri_2))
					{
						if(!table_created)
						{
							lua_newtable(L);
							table_created = wtrue;
						}

						lua_pushnumber(L,++array_pos);
						lua_pushstring(L,olight->light_id);
						lua_settable(L,-3);

						break;
					}

				}
			}
		}

		if(!table_created)
			lua_pushnil(L);

	}
	else
	{
		lua_pushnil(L);
	}

	return 1;
}

void register_light_functions(lua_State* L)
{
	//lights functions
	lua_pushcfunction(L,light_mngr_inlight);
	lua_setglobal(L,"light_mngr_inlight");

	lua_pushcfunction(L,light_mngr_setambient);
	lua_setglobal(L,"light_mngr_setambient");

	lua_pushcfunction(L,light_mngr_addlight);
	lua_setglobal(L,"light_mngr_addlight");

	lua_pushcfunction(L, light_mngr_haslight);
	lua_setglobal(L, "light_mngr_haslight");

	lua_pushcfunction(L,light_mngr_removelight);
	lua_setglobal(L,"light_mngr_removelight");

	lua_pushcfunction(L,light_setenable);
	lua_setglobal(L,"light_setenable");

	lua_pushcfunction(L,light_setcolor);
	lua_setglobal(L,"light_setcolor");

	lua_pushcfunction(L,light_setradius);
	lua_setglobal(L,"light_setradius");

	lua_pushcfunction(L,lua_light_setposition);
	lua_setglobal(L,"light_setposition");

	lua_pushcfunction(L,lua_light_setrotationangle);
	lua_setglobal(L,"light_setrotationangle");

	lua_pushcfunction(L,lua_light_setopenangle);
	lua_setglobal(L,"light_setopenangle");

	lua_pushcfunction(L,lua_light_setattenuation);
	lua_setglobal(L,"light_setattenuation");

	lua_pushcfunction(L,lua_light_setpowerfactor);
	lua_setglobal(L,"light_setpowerfactor");

	lua_pushcfunction(L,lua_light_checkentityinside);
	lua_setglobal(L,"light_checkentityinside");
}
