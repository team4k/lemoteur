#ifdef _MSC_VER
    extern "C" {
#endif
        
#include "lauxlib.h"
#include "lualib.h"
#include "lua.h"
        
#ifdef _MSC_VER
 }
#endif

#if defined(_WIN32)
    #include <windows.h>
#else
	#ifndef __USE_BSD
        	#define __USE_BSD
	#endif
        #include <unistd.h>
        #include <dlfcn.h>
		#include <wchar.h>
		#include <pthread.h>
#endif

#include <stdarg.h>
#include <string.h>
#include <inttypes.h>
#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include <math.h>

#include <Engine.h>

#include <Render.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>
#include <Base/geom.h>

#include <Render/render_manager.h>

#include <Graphics/TextureLoader.h>

#include <Utils/Hash.h>
#include <Utils/hashmap.h>

#include <GameObjects/Characters.pb-c.h>
#include <Graphics/Animation.h>
#include <Graphics/Sprite.h>

#include <Graphics/TextureRegion.h>
#include <Graphics/SpriteBatch.h>
#include <Debug/Logprint.h>


#include <Debug/geomdebug.h>

#include <GameObjects/Level.pb-c.h>

#include <chipmunk/chipmunk_private.h>

#include <Physics/Physics.h>


#include <Graphics/Effects/screen_fill.h>
#include <Graphics/Effects/screen_fade.h>

#include <Graphics/compound_sprite.h>
#include <Physics/Effects/shatter_effect.h>

#include <GameObjects/level_base.h>

#include <Scripting/ScriptLuaUtils.h>
#include <Scripting/ScriptLoader.h>
#include <Scripting/ScriptPointers.h>

static int screen_fill_setactive(lua_State* L)
{
	wbool activate = (wbool)lua_toboolean(L,1);

	lua_getglobal(L,"mngr");

	screen_fill_t* screen_fill;

	lua_pushstring(L,"screen_filler");
	lua_gettable(L,-2);

	if(!lua_isnil(L,-1))
	{
		screen_fill = (screen_fill_t*)lua_touserdata(L,-1);

		lua_pop(L,1);


		screen_fill->active = activate;

		if (screen_fill->has_sibling) {
			lua_pushstring(L, "screen_filler_sibling");
			lua_gettable(L, -2);

			screen_fill = (screen_fill_t*)lua_touserdata(L, -1);

			lua_pop(L, 1);

			screen_fill->active = activate;

		}
	}
	else
	{
		luaL_error(L,"no screen filler configured, chech the filler config entry and assets directory for a .png and .awf of your filler");
	}

	return 1;
}

static int lua_screen_fill_update(lua_State* L)
{
	if(!lua_istable(L,1))
	{
		luaL_error(L,"The first parameter of screen_fill_update is not a vector!"); 
		return 1;
	}

	Vector_t pos_vector;

	pos_vector.x = getluatable_value(L,"x",1);
	pos_vector.y = getluatable_value(L,"y",1);

	wbool sibling = (wbool)lua_toboolean(L, 2);

	lua_getglobal(L,"mngr");

	screen_fill_t* screen_fill;

	
	lua_pushstring(L, (!sibling) ? "screen_filler" : "screen_filler_sibling");
	lua_gettable(L,-2);

	if(!lua_isnil(L,-1))
	{
		screen_fill = (screen_fill_t*)lua_touserdata(L,-1);

		lua_pop(L,1);

		screen_fill->base_position = pos_vector;
	}
	else
	{
		luaL_error(L,"no screen filler configured, chech the filler config entry and assets directory for a .png and .awf of your filler");
	}

	return 1;
}

static int lua_screen_fill_setzindex(lua_State* L)
{
	lua_Integer new_zindex = lua_tointeger(L,1);

	lua_getglobal(L,"mngr");

	screen_fill_t* screen_fill;

	lua_pushstring(L,"screen_filler");
	lua_gettable(L,-2);

	if(!lua_isnil(L,-1))
	{

		screen_fill = (screen_fill_t*)lua_touserdata(L,-1);

		lua_pop(L,1);

		screen_fill->z_index = (int16_t)new_zindex;

		if (screen_fill->has_sibling) {
			lua_pushstring(L, "screen_filler_sibling");
			lua_gettable(L, -2);

			screen_fill = (screen_fill_t*)lua_touserdata(L, -1);

			lua_pop(L, 1);

			screen_fill->z_index = (int16_t)new_zindex;

		}
	}
	else
	{
		luaL_error(L,"no screen filler configured, missing a filler.png and filler.awf in your assets directory");
	}

	return 1;
}

static int lua_screen_fill_setanimations(lua_State* L)
{
	const char* fill_anim = lua_tostring(L,1);
	const char* edge_anim = lua_tostring(L,2);
	lua_Integer edge_pos = lua_tointeger(L,3);

	lua_getglobal(L,"mngr");

	screen_fill_t* screen_fill;

	lua_pushstring(L,"screen_filler");
	lua_gettable(L,-2);

	if(!lua_isnil(L,-1))
	{
		screen_fill = (screen_fill_t*)lua_touserdata(L,-1);

		lua_pop(L,1);

		render_manager_t* render_mngr;

		lua_pushstring(L,"render_mngr");
		lua_gettable(L,-2);

		render_mngr = (render_manager_t*)lua_touserdata(L,-1);

		lua_pop(L,1);

		screen_fill_setanimations(screen_fill,render_mngr,fill_anim,edge_anim,(int32_t)edge_pos);

		if (screen_fill->has_sibling) {
			lua_pushstring(L, "screen_filler_sibling");
			lua_gettable(L, -2);

			screen_fill = (screen_fill_t*)lua_touserdata(L, -1);

			lua_pop(L, 1);

			screen_fill_setanimations(screen_fill, render_mngr, fill_anim, edge_anim, (int32_t)edge_pos);

		}
	}
	else
	{
		luaL_error(L,"no screen filler configured, missing a filler.png and filler.awf in your assets directory");
	}

	return 1;
}

static int screen_fill_getsize(lua_State* L)
{
	lua_getglobal(L,"mngr");

	screen_fill_t* screen_fill;

	lua_pushstring(L,"screen_filler");
	lua_gettable(L,-2);

	if(!lua_isnil(L,-1))
	{
		screen_fill = (screen_fill_t*)lua_touserdata(L,-1);

		lua_pop(L,1);

		int32_t width =  screen_fill->piece_num_width * screen_fill->piece_width;
		int32_t height = screen_fill->piece_num_height * screen_fill->piece_height;


		lua_newtable(L);
		lua_pushnumber(L, width);
		lua_setfield(L,-2,"width");
		lua_pushnumber(L,height);
		lua_setfield(L,-2,"height");
	}
	else
	{
		luaL_error(L,"no screen filler configured, missing a filler.png and filler.awf in your assets directory");
	}

	return 1;
}

static int screen_fade(lua_State* L)
{
	float duration = (float)lua_tonumber(L,1);
	float target_alpha = (float)lua_tonumber(L,2);
	lua_Integer direction = lua_tointeger(L,3);

	lua_getglobal(L,"mngr");

	screen_fade_t* screen_fade;

	lua_pushstring(L,"screen_fader");
	lua_gettable(L,-2);

	screen_fade = (screen_fade_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	screen_fade_dofade(screen_fade,duration,(int32_t)direction,target_alpha);


	return 1;
}

static int lua_screen_fade_setcolor(lua_State* L)
{
	float r = (float)lua_tonumber(L,1);
	float g = (float)lua_tonumber(L,2);
	float b = (float)lua_tonumber(L,3);

	Color_t new_color;

	new_color.r = r;
	new_color.g = g;
	new_color.b = b;

	lua_getglobal(L,"mngr");

	screen_fade_t* screen_fade;

	lua_pushstring(L,"screen_fader");
	lua_gettable(L,-2);

	screen_fade = (screen_fade_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	screen_fade_setcolor(screen_fade,new_color);


	return 1;
}

static int screen_fade_isfading(lua_State* L)
{
	lua_getglobal(L,"mngr");

	screen_fade_t* screen_fade;

	lua_pushstring(L,"screen_fader");
	lua_gettable(L,-2);

	screen_fade = (screen_fade_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	lua_pushboolean(L,(int)screen_fade->fading);

	return 1;
}

static int lua_screen_fade_resetfade(lua_State* L)
{
	wbool fadein = (wbool)lua_toboolean(L,1);

	lua_getglobal(L,"mngr");

	screen_fade_t* screen_fade;

	lua_pushstring(L,"screen_fader");
	lua_gettable(L,-2);

	screen_fade = (screen_fade_t*)lua_touserdata(L,-1);

	lua_pop(L,1);

	screen_fade_resetfade(screen_fade,fadein);

	return 1;
}


static int shatter_effect_getcompound(lua_State* L)
{
	uint32_t ptr_id = (uint32_t)lua_tointeger(L, 1);

	intptr_t ptr = script_getpointerfromid(ptr_id, SHATTER_EFFECT);

	shatter_effect_t* shatter = (shatter_effect_t*)(ptr);

	if(shatter)
	{
		if (shatter->shattered_sprite.script_id == 0)
		{
			shatter->shattered_sprite.script_id = script_registerpointer((intptr_t)&shatter->shattered_sprite, COMPOUND_SPRITE);
		}

		lua_pushinteger(L, shatter->shattered_sprite.script_id);
	}
	else
	{
		lua_pushnil(L);
	}

	return 1;
}

static int lua_shatter_effect_reinit(lua_State* L)
{
	uint32_t ptr_id = (uint32_t)lua_tointeger(L, 1);
	intptr_t ptr = script_getpointerfromid(ptr_id, SHATTER_EFFECT);

	shatter_effect_t* shatter = (shatter_effect_t*)(ptr);

	if(shatter)
	{
		lua_getglobal(L, "mngr");

		lua_pushstring(L, "level_mngr");
		lua_gettable(L, -2);

		level_base_t* lvlcontainer = NULL;

		if (lua_isuserdata(L, -1))
		{
			lvlcontainer = (level_base_t*)lua_touserdata(L, -1);
		}

		lua_pop(L, 1);

		LEVEL_TYPE type = lvlcontainer->current_level_type;

		cpSpace* world;

		lua_getglobal(L,"mngr");

		lua_pushstring(L, (type == L_STATIC) ? "static_physics_mngr":"stream_physics_mngr");
		lua_gettable(L,-2);

		world = ((physics_manager_t*)lua_touserdata(L,-1))->world;

		lua_pop(L,1);

		shatter_effect_reinit(shatter,world);
	}

	return 1;
}

void register_effects_functions(lua_State *L)
{
	//shatter effect functions
	lua_pushcfunction(L,shatter_effect_getcompound);
	lua_setglobal(L,"shatter_effect_getcompound");

	lua_pushcfunction(L,lua_shatter_effect_reinit);
	lua_setglobal(L,"shatter_effect_reinit");

		//screen effects functions
	lua_pushcfunction(L,screen_fill_setactive);
	lua_setglobal(L,"screen_fill_setactive");

	lua_pushcfunction(L,lua_screen_fill_update);
	lua_setglobal(L,"screen_fill_update");

	lua_pushcfunction(L,lua_screen_fill_setanimations);
	lua_setglobal(L,"screen_fill_setanimations");

	lua_pushcfunction(L,lua_screen_fill_update);
	lua_setglobal(L,"screen_fill_update");

	lua_pushcfunction(L,lua_screen_fill_setzindex);
	lua_setglobal(L,"screen_fill_setzindex");

	lua_pushcfunction(L,screen_fill_getsize);
	lua_setglobal(L,"screen_fill_getsize");

	lua_pushcfunction(L,screen_fade);
	lua_setglobal(L,"screen_fade");

	lua_pushcfunction(L,lua_screen_fade_setcolor);
	lua_setglobal(L,"screen_fade_setcolor");

	lua_pushcfunction(L,screen_fade_isfading);
	lua_setglobal(L,"screen_fade_isfading");

	lua_pushcfunction(L,lua_screen_fade_resetfade);
	lua_setglobal(L,"screen_fade_resetfade");
}
