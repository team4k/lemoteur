#include <stdio.h>
#include <inttypes.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#include <wchar.h>


#ifdef __APPLE__
	#include <mach-o/dyld.h>
	#include <sys/param.h>
	#include <stdlib.h>
#endif

#if defined(_WIN32)
    #include <Windows.h>
#else
    #include <unistd.h>
	#include <pthread.h>
#endif


#include <Engine.h>
#include <Base/types.h>

#include <Debug/Logprint.h>

#include <Utils/tempallocator.h>


#include <Base/functions.h>

#include <Utils/circular_array.h>


void circular_array_init(circular_array* const array,size_t element_size,uint32_t max_elements,tempallocator_t* const allocator)
{
    array->element_size = element_size;
    array->max_elements = max_elements;
    array->read_pos = 0;
    array->write_pos = 0;

    if(allocator == NULL)
    {
        array->element_ptr = (void**)wf_calloc(array->max_elements,array->element_size);
        array->use_temp_allocator = wfalse;
    }
    else
    {
        array->element_ptr = (void**)tempallocator_getmemory(allocator,array->max_elements * array->element_size);
        array->use_temp_allocator = wtrue;
    }
    
}

int32_t circular_array_check(circular_array* const p,wbool pushcheck)
{
    uint32_t wp = 0;
    uint32_t rp = 0;

    atomic_set_value(&wp,p->write_pos);
    atomic_set_value(&rp,p->read_pos);


     if(pushcheck){
          if (wp > rp) 
               return rp - wp + p->max_elements - 1;
          else if (wp < rp) 
               return rp - wp - 1;
          else 
               return p->max_elements - 1;
     }
     else {
          if (wp > rp) 
               return wp - rp;
          else if (wp < rp) 
               return wp - rp + p->max_elements;
          else 
               return 0;
     }
}

void* circular_array_get_next_element(circular_array* const p)
{
    uint32_t wp = 0;
    uint32_t rp = 0;

    atomic_set_value(&wp,p->write_pos);
    atomic_set_value(&rp,p->read_pos);


    if(rp == wp)
        return NULL;
    
    void* return_element = p->element_ptr[rp];

    if(rp < (p->max_elements - 1))
    {
        //move rp toward wp
        atomic_increment(&p->read_pos);
    }
    else
    {
        atomic_set_value(&p->read_pos,0);
    }

    return return_element;
}

void* circular_array_iterate(circular_array* const p,uint32_t* peek_read_p,wbool init)
{
    uint32_t wp = 0;


    atomic_set_value(&wp,p->write_pos);

    if(init)
        atomic_set_value(peek_read_p,p->read_pos);


    if((*peek_read_p) == wp)
        return NULL;
    
    void* return_element = p->element_ptr[(*peek_read_p)];

    if((*peek_read_p) < (p->max_elements - 1))
    {
        //move peek_read_p toward wp
        (*peek_read_p)++;
    }
    else
    {
        (*peek_read_p) = 0;
    }

    return return_element;
}


void circular_array_push_new_element(circular_array* const p, void* new_element)
{
    if(circular_array_check(p,wtrue) == 0)
    {
        logprint("No more space in circular array ! can't push element");
        return;
    }

    uint32_t wp = 0;
    uint32_t rp = 0;

    atomic_set_value(&wp,p->write_pos);
    atomic_set_value(&rp,p->read_pos);

    p->element_ptr[wp] = new_element;

    if(wp < (p->max_elements - 1))
    {
        atomic_increment(&p->write_pos);
    }
    else
    {
        atomic_set_value(&p->write_pos,0);
    }
    
}

void circular_array_free(circular_array* const p)
{
    if(!p->use_temp_allocator)
        wf_free(p->element_ptr);

}
