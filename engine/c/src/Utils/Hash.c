#include <stdio.h>
#include <inttypes.h>
#include <string.h>

#include <Engine.h>

#include <Debug/Logprint.h>

#include <Utils/Hash.h>

//djb2 by Dan Bernstein.
unsigned long hash(const char *str)
{
    unsigned long hash = 5381;
    int c;

    while ((c = *str++))
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

    return hash;
}

//berkeley db algorithm
unsigned long sdbm(const char* str)
{
	unsigned long hash = 0;
    int c;

    while ((c = *str++))
        hash = c + (hash << 6) + (hash << 16) - hash;

    return hash;
}

int hashtable_index(hashtable_t* const hashtable, const char* const key)
{
	if(hashtable->max_elem == 0)
	{
		logprint("No elements in hashtable !");
		return -1;
	}

    uint32_t index = (uint32_t)sdbm(key) % hashtable->max_elem;
    uint32_t cnt_occ = 0;

   while(cnt_occ++ < hashtable->max_elem)
   {
		if(hashtable->keys[index])
		{
			if(strcmp(hashtable->keys[index],key) == 0)
				break;
		}

		index = (index +1) % hashtable->max_elem;
   }

    if(cnt_occ > hashtable->max_elem)
    {
        logprint("can't find element %s in hashtable !",key);
        return -1;
    }
    else
    {
        return index;
    }
}

int hashtable_insertkey(hashtable_t* const hashtable, const char* const key)
{
	if(hashtable->max_elem == 0)
	{
		logprint("No elements in hashtable !");
		return -1;
	}

    uint32_t index = (uint32_t)sdbm(key) % hashtable->max_elem;
    uint32_t cnt_occ = 0;

    while(hashtable->keys[index] && strcmp(hashtable->keys[index],key) && cnt_occ++ < hashtable->max_elem)
        index = (index +1) % hashtable->max_elem;

    if(cnt_occ > hashtable->max_elem)
    {
        logprint("can't insert element %s in hashtable !",key);
        return -1;
    }
    else
    {
        return index;
    }
}

int hashtable_haskey(hashtable_t* const hashtable, const char* const key)
{
	if(hashtable->max_elem == 0)
	{
		logprint("No elements in hashtable !");
		return 0;
	}

	uint32_t index = (uint32_t)sdbm(key) % hashtable->max_elem;
    uint32_t cnt_occ = 0;

   while(cnt_occ++ < hashtable->max_elem)
   {
		if(hashtable->keys[index])
		{
			if(strcmp(hashtable->keys[index],key) == 0)
				break;
		}

		index = (index +1) % hashtable->max_elem;
   }

   if(cnt_occ > hashtable->max_elem || hashtable->keys[index] == NULL)
		return 0;
	else
		return 1;
}

int hashtable_pushkey(hashtable_t* const hashtable, char* key)
{
    int32_t index = 0;

    if(hashtable->num_elem == hashtable->max_elem)
    {
        logprint("Hashtable limit reached !, can't add new element %s",key);
        return -1;
    }

    index = hashtable_insertkey(hashtable,key);

	if (hashtable->keys[index] != NULL) {
		logprint("This key is already in use ! please use another key : %s", key);
		return -1;
	}

    hashtable->keys[index] = key;
    hashtable->num_elem++;
    return index;
}

void hashtable_removekey(hashtable_t* const hashtable,const char* const key)
{
    int32_t index = 0;

    if(hashtable->num_elem == 0)
    {
        logprint("Hashtable empty, can't remove element %s",key);
        return;
    }

     index = hashtable_index(hashtable,key);
     hashtable->keys[index] = NULL;
     hashtable->num_elem--;
}

