#if defined(WITH_CURL)

#if defined(_WIN32)
	#include <windows.h>
#elif defined(EMSCRIPTEN)
	#include <emscripten.h>
#else
	#include <pthread.h>
#endif

#ifndef EMSCRIPTEN
	#include <curl/curl.h>
#endif

#include <inttypes.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>
#include <math.h>
#include <Engine.h>

#include <Base/types.h>
#include <Base/functions.h>

#include <Debug/Logprint.h>

#include <Utils/url.h>
#include <Utils/base64.h>


#ifndef EMSCRIPTEN
static size_t write_response(void *ptr, size_t size, size_t nmemb, void *stream)
{
   url_result_data_t *result = (url_result_data_t*)stream;

    if(result->pos + size * nmemb >= result->raw_data_size - 1)
    {
		result->raw_data_size = (result->pos + size * nmemb) + URL_BUFFER_SIZE;
		//realloc buffer with bigger size
		result->raw_data = (char*)wf_realloc(result->raw_data,result->raw_data_size);
		memset(&result->raw_data[result->pos],0, size * nmemb);
    }

    memcpy(&result->raw_data[result->pos], ptr, size * nmemb);
    result->pos += size * nmemb;

    return size * nmemb;
}

#if defined(_WIN32)
DWORD WINAPI ThreadFunc(void* data) {
#else
void* ThreadFunc(void* data) {
#endif
  CURL *curl;
  CURLcode res;

  url_t* url_obj = (url_t*)data;

  struct curl_httppost* formpost = NULL;
  struct curl_httppost* lastptr = NULL;
 
  curl_global_init(CURL_GLOBAL_ALL);

  for(uint32_t i = 0; i < url_obj->count_post_params;i++)
  {
	curl_formadd(&formpost,
		&lastptr,
		CURLFORM_COPYNAME,url_obj->post_params[i].name,
		CURLFORM_COPYCONTENTS,url_obj->post_params[i].value,
		CURLFORM_END);
  }

  curl_formadd(&formpost,&lastptr,CURLFORM_COPYNAME,"submit",CURLFORM_COPYCONTENTS,"send",CURLFORM_END);

  curl = curl_easy_init();

  

  volatile uint32_t request_status = 0;

  if(curl) 
  {
		url_obj->result_data.raw_data = (char*)wf_malloc(URL_BUFFER_SIZE);
		url_obj->result_data.raw_data_size = URL_BUFFER_SIZE;
		memset(url_obj->result_data.raw_data,0,URL_BUFFER_SIZE);

		curl_easy_setopt(curl, CURLOPT_URL, url_obj->url);
		curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
		curl_easy_setopt(curl,CURLOPT_WRITEFUNCTION,write_response);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &url_obj->result_data);
		curl_easy_setopt(curl,CURLOPT_HTTPPOST,formpost);
 
		/* Perform the request, res will get the return code */ 
		res = curl_easy_perform(curl);
		/* Check for errors */ 
		if(res != CURLE_OK)
		{
			request_status = URL_REQUEST_ERROR_HTTP;
			w_strcpy(url_obj->error_string,256, curl_easy_strerror(res));
		}
		else
		{
			request_status = URL_REQUEST_SUCCESS;

			//convert from base64 to binary
			url_obj->result_data.decoded_data =  unbase64(url_obj->result_data.raw_data,url_obj->result_data.pos,&url_obj->result_data.len_decoded);


			url_obj->result_data.pos = 0;
		}

		wf_free(url_obj->result_data.raw_data);
		url_obj->result_data.raw_data = NULL;
		url_obj->result_data.raw_data_size = 0;
		/* always cleanup */ 
		curl_easy_cleanup(curl);

		curl_formfree(formpost);
  }
  else
  {
	  request_status = URL_REQUEST_ERROR_CURL;
  }

  //set request status atomically to notify the main thread
  atomic_set_value(&url_obj->request_status,request_status);
  

  return 0;
}
#endif

#if defined(EMSCRIPTEN)

void callback_url_func(unsigned int unknow, void* url,void* buffer,unsigned int size)
{
	url_t* url_obj = (url_t*)url;

	//memcpy(&result->raw_data[result->pos], ptr, size * nmemb);

	//convert from base64 to binary
	url_obj->result_data.decoded_data =  unbase64((const char*)buffer,size,&url_obj->result_data.len_decoded);

	url_obj->request_status = URL_REQUEST_SUCCESS;
	wf_free(url_obj->str_post_params);
	url_obj->str_post_params = NULL;
}

void error_url_func(unsigned int unknow,void* url,int error_code,const char* error_description)
{
	url_t* url_obj = (url_t*)url; 
	
	strcpy(url_obj->error_string, error_description);
	url_obj->request_status = URL_REQUEST_ERROR_HTTP;
}

void progress_url_func(unsigned int unknow,void* url,int loadedbytes,int totalbytes)
{

}

#endif




void url_init(url_t* const url_obj)
{
	url_obj->result_data.decoded_data = NULL;
	url_obj->result_data.pos = 0;
	url_obj->result_data.len_decoded = 0;
	url_obj->request_status = URL_REQUEST_NOTSTARTED;
	url_obj->post_params = (url_post_param_t*)wf_calloc(URL_POSTPARAM_POOL,sizeof(url_post_param_t));
	url_obj->count_post_params = 0;
	url_obj->max_post_params = URL_POSTPARAM_POOL;
	

	memset(&url_obj->url[0],0,sizeof(url_obj->url));
	memset(&url_obj->error_string[0],0,sizeof(url_obj->error_string));
	memset(&url_obj->callback[0],0,sizeof(url_obj->callback));
	url_obj->result_data.raw_data = NULL;
	url_obj->result_data.raw_data_size = 0;

	#ifdef EMSCRIPTEN
		url_obj->str_post_params = NULL;
	#else
		url_obj->thread = NULL;
	#endif
}

void url_addpostparam(url_t* const url_obj, const char* name,const char* value)
{
	if(url_obj->count_post_params == url_obj->max_post_params)
	{
		url_obj->max_post_params += URL_POSTPARAM_POOL;
		url_obj->post_params = (url_post_param_t*)wf_realloc(url_obj->post_params,sizeof(url_post_param_t) * url_obj->max_post_params);
	}

	url_post_param_t*  new_param = &url_obj->post_params[url_obj->count_post_params++];

	w_strcpy(new_param->name,128,name);

	int32_t len = strlen(value) + 1;

	new_param->value = (char*)wf_malloc(len);
	memset(new_param->value,0,len);



	memcpy(new_param->value,value,len);

}

wbool url_getbase64data(url_t* const url_obj)
{
	
	url_obj->request_status = URL_REQUEST_PENDING;

	

	#if defined(_WIN32)
		

		url_obj->thread = CreateThread(NULL, 0, ThreadFunc, url_obj, 0, NULL);

		if(!url_obj->thread)
		{
			logprint("Can't create curl thread!");
			return wfalse;
		}		
	#elif defined(EMSCRIPTEN)
		size_t full_size = 512;
		size_t resize_value = 512;
		url_obj->str_post_params = (char*)wf_malloc(full_size);
		memset(url_obj->str_post_params,0,full_size);

		size_t actual_params_size = 0;

		for(uint32_t i = 0; i < url_obj->count_post_params;i++)
		{
			if(strcmp(url_obj->str_post_params,"") != 0)
				sprintf(url_obj->str_post_params,"%s&",url_obj->str_post_params);

			size_t thisparamsize = strlen(url_obj->post_params[i].name) + strlen(url_obj->post_params[i].value) + 4;//2 null terminating char + one equal char for the post string + 1 "&" char for parameters chaining

			actual_params_size += thisparamsize;

			if(actual_params_size > full_size)
			{
				full_size += (actual_params_size - full_size) + resize_value;
				url_obj->str_post_params = (char*)wf_realloc(url_obj->str_post_params,full_size); //realloc params
			}

			sprintf(url_obj->str_post_params,"%s%s=%s",url_obj->str_post_params,url_obj->post_params[i].name,url_obj->post_params[i].value);
		}

		emscripten_async_wget2_data(&url_obj->url[0],"POST",url_obj->str_post_params,url_obj,1,callback_url_func,error_url_func,progress_url_func);

	#else
		url_obj->thread = (pthread_t*)wf_malloc(sizeof(pthread_t));
		memset(url_obj->thread,0,sizeof(pthread_t));

		if(pthread_create(url_obj->thread,NULL,ThreadFunc,url_obj) != 0)
		{
			logprint("Can't create curl thread!");
			wf_free(url_obj->thread);
			url_obj->thread = NULL;
			return wfalse;
		}
	#endif

	return wtrue;
}

void url_recycle(url_t* const url_obj,wbool clear_params)
{
	if(clear_params)
	{
		//free value parameters
		for(uint32_t iparam = 0;iparam < url_obj->count_post_params;iparam++)
		{
			if(url_obj->post_params[iparam].value != NULL)
			{
				wf_free(url_obj->post_params[iparam].value);
				url_obj->post_params[iparam].value = NULL;
			}
		}

		memset(url_obj->post_params,0,sizeof(url_post_param_t) * url_obj->max_post_params);
		url_obj->count_post_params = 0;
		memset(&url_obj->callback[0],0,sizeof(url_obj->callback));
	}


	if(url_obj->result_data.decoded_data != NULL)
	{
		wf_free(url_obj->result_data.decoded_data);
		url_obj->result_data.decoded_data = NULL;
	}

	if(url_obj->result_data.raw_data != NULL)
	{
		wf_free(url_obj->result_data.raw_data);
		url_obj->result_data.raw_data = NULL;
		url_obj->result_data.raw_data_size = 0;
	}

	#if defined(_WIN32)
		if(url_obj->thread)
			CloseHandle(url_obj->thread);

		url_obj->thread = 0;
	#elif defined(EMSCRIPTEN)
		if(url_obj->str_post_params)
			wf_free(url_obj->str_post_params);

		url_obj->str_post_params = NULL;
	#else
		if(url_obj->thread)
			wf_free(url_obj->thread);

		url_obj->thread = NULL;
	#endif

}

void url_free(url_t* const url_obj)
{
	if(url_obj->result_data.decoded_data != NULL)
	{
		wf_free(url_obj->result_data.decoded_data);
		url_obj->result_data.decoded_data = NULL;
	}

	if(url_obj->post_params != NULL)
	{
		//free value parameters
		for(uint32_t iparam = 0;iparam < url_obj->count_post_params;iparam++)
		{
			if(url_obj->post_params[iparam].value != NULL)
			{
				wf_free(url_obj->post_params[iparam].value);
				url_obj->post_params[iparam].value = NULL;
			}
		}

		wf_free(url_obj->post_params);
		url_obj->post_params = NULL;
		url_obj->max_post_params = 0;
		url_obj->count_post_params = 0;
	}

	if(url_obj->result_data.raw_data != NULL)
	{
		wf_free(url_obj->result_data.raw_data);
		url_obj->result_data.raw_data = NULL;
		url_obj->result_data.raw_data_size = 0;
	}

	#if defined(_WIN32)
		if(url_obj->thread)
		{
			CloseHandle(url_obj->thread);
			url_obj->thread = 0;
		}
	#elif defined(EMSCRIPTEN)
		if(url_obj->str_post_params)
			wf_free(url_obj->str_post_params);

		url_obj->str_post_params = NULL;
	#else
		if(url_obj->thread)
			wf_free(url_obj->thread);

		url_obj->thread = NULL;
	#endif
}
#endif
