#include <limits.h>
#include <stdlib.h>
#include <Engine.h>
#include <inttypes.h>
#include <Base/types.h>
#include <Utils/tinymt32.h>


#include <Utils/random.h>

static tinymt32_t random_cache;
static uint32_t seed_cache = 4;
static wbool initialized = wfalse;

void wf_setseed(const uint32_t seed)
{
	tinymt32_init(&random_cache,seed);
	seed_cache = seed;
	initialized = wtrue;
}

const uint32_t wf_rand()
{
	if(!initialized)
	{
		tinymt32_init(&random_cache,seed_cache);
		initialized = wtrue;
	}

	return tinymt32_generate_uint32(&random_cache);
}

const uint32_t wf_randrange(const uint32_t min_value,const uint32_t max_value)
{
	if (min_value == max_value)
	{
		return min_value;
	}

	return min_value +  wf_rand() / (WF_RAND_MAX / ((max_value - min_value) + 1) + 1);
}