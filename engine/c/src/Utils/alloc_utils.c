#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <errno.h> 
#include <stdarg.h>
#include <string.h> 
#include <Engine.h>

void* _internal_malloc(size_t size)
{
	return malloc(size);
}

void* _internal_calloc(size_t count, size_t size)
{
	return calloc(count, size);
}

void* _internal_realloc(void* ptr, size_t new_size)
{
	return realloc(ptr, new_size);
}

void _internal_free(void* ptr)
{
	free(ptr);
}