#include <stdio.h>
#include <inttypes.h>
#include <string.h>
#include <stdlib.h>

#include <Engine.h>

#include <Debug/Logprint.h>

#include <Utils/hashmap.h>

//berkeley db algorithm
static unsigned long sdbm(const char* str)
{
	unsigned long hash = 0;
    int c;

    while ((c = *str++))
        hash = c + (hash << 6) + (hash << 16) - hash;

    return hash;
}

void hashmap_initalloc(hashmap_t* const hashtable,size_t max_size,size_t value_size,size_t pool_value)
{
	hashtable->keys = (char**) wf_malloc(max_size * sizeof (char*));
	hashtable->values = (void**)wf_malloc(max_size * sizeof(void*));
	hashtable->value_size = value_size;

	if(pool_value < DEFAULT_HASHMAP_POOL)
		hashtable->pool_values = DEFAULT_HASHMAP_POOL;
	else
		hashtable->pool_values = pool_value;


    //initialise keys array
	for (uint32_t key_indx = 0; key_indx < max_size; key_indx++)
	{
		hashtable->keys[key_indx] = 0;
		hashtable->values[key_indx] = 0;
	}

	hashtable->max_elem = max_size;
	hashtable->num_elem = 0;
}

static void hashmap_resizealloc(hashmap_t* const hashtable,size_t new_size)
{
	//create a temp table with our existing keys so that we keep track of the previous assignement (and avoid pointer invalidations)
	char** old_keys = (char**)wf_calloc(hashtable->max_elem,sizeof(char*));

	memcpy(old_keys,hashtable->keys, hashtable->max_elem  * sizeof (char*));

	int32_t old_size = hashtable->max_elem;

	//set new size
	hashtable->max_elem  = new_size;

	hashtable->keys = (char**)wf_realloc(hashtable->keys, hashtable->max_elem  * sizeof (char*));

	//reinit all keys
	for(uint32_t i = 0; i < hashtable->max_elem;i++)
		hashtable->keys[i] = NULL;

	hashtable->num_elem = 0;

	//reaffect all items
	//new item list
	void** old_list = hashtable->values;
	hashtable->values = (void**)wf_calloc(hashtable->max_elem,sizeof(void*));

	//put item element from old list to new list
	for(int32_t i = 0; i < old_size;i++)
	{
		if(old_keys[i] != NULL)
		{
			//rehash keys 
			int32_t index = hashmap_getinsertindex(hashtable,old_keys[i]);
			hashtable->keys[index] = old_keys[i];
			hashtable->values[index] = old_list[i];
			hashtable->num_elem++;
		}
	}

	//clear temp var
	wf_free(old_list);
	wf_free(old_keys);
}

void hashmap_recycle(hashmap_t* const hashtable)
{
	if(hashtable->keys != NULL)
	{
		for (uint32_t key_indx = 0; key_indx < hashtable->max_elem; key_indx++)
		{
			if(hashtable->keys[key_indx] != NULL)
			{
				wf_free(hashtable->keys[key_indx]);
				wf_free(hashtable->values[key_indx]);
				hashtable->keys[key_indx] = NULL;
				hashtable->values[key_indx] = NULL;
			}
		}

		hashtable->num_elem = 0;
	}
}

void hashmap_free(hashmap_t* const hashtable)
{
	if(hashtable->keys != NULL)
	{
		for (uint32_t key_indx = 0; key_indx < hashtable->max_elem; key_indx++)
		{
			if(hashtable->keys[key_indx] != NULL)
			{
				wf_free(hashtable->keys[key_indx]);
				wf_free(hashtable->values[key_indx]);
			}
		}

		wf_free(hashtable->keys);
		wf_free(hashtable->values);
		hashtable->keys = NULL;
		hashtable->values = NULL;
		hashtable->num_elem = hashtable->max_elem = 0;
	}
}


//get the index of an existing key in the hashtable
static int hashmap_index(hashmap_t* const hashtable, const char* const key)
{
	if(hashtable->max_elem == 0)
	{
		logprint("No elements in hashtable !");
		return -1;
	}

    uint32_t index = (uint32_t)sdbm(key) % hashtable->max_elem;
    uint32_t cnt_occ = 0;

   while(cnt_occ++ < hashtable->max_elem)
   {
		if(hashtable->keys[index])
		{
			if(strcmp(hashtable->keys[index],key) == 0)
				break;
		}

		index = (index +1) % hashtable->max_elem;
   }

    if(cnt_occ > hashtable->max_elem)
    {
        logprint("can't find element %s in hashtable !",key);
        return -1;
    }
    else
    {
        return index;
    }
}

//return the index of an given key in the hashmap
int hashmap_getindexforkey(hashmap_t* const hashmap,const char* const key)
{
	if(hashmap_haskey(hashmap,key))
		return hashmap_index(hashmap,key);

	logprint("No index for the key %s in the hashmap!, returning -1",key);

	return -1;
}

char* const hashmap_getkeypointer(hashmap_t* const hashmap, const char* const key)
{
	if (hashmap_haskey(hashmap, key))
	{
		int index = hashmap_index(hashmap, key);
		return hashmap->keys[index];
	}

	logprint("No index for the key %s in the hashmap!, returning NULL", key);

	return NULL;
}

void* hashmap_getvalue(hashmap_t* const hashmap, const char* const key)
{
	return hashmap_getvalue_withlogopt(hashmap, key, 0);
}

//return the value pointer for a given key
void* hashmap_getvalue_withlogopt(hashmap_t* const hashmap,const char* const key,int silent)
{
	if(hashmap_haskey(hashmap,key))
		return hashmap->values[hashmap_index(hashmap,key)];

	if (silent == 0) {
		logprint("No value for the key %s in the hashmap!, returning NULL", key);
	}
	

	return NULL;
}

void* hashmap_getvaluefromindex(hashmap_t* const hashmap, int index)
{
	return hashmap->values[index];
}

//get the index where a given key would be inserted in the hashtable 
int hashmap_getinsertindex(hashmap_t* const hashtable, const char* const key)
{
	if(hashtable->max_elem == 0)
	{
		logprint("No elements in hashtable !");
		return -1;
	}

    uint32_t index = (uint32_t)sdbm(key) % hashtable->max_elem;
    uint32_t cnt_occ = 0;

    while(hashtable->keys[index] && strcmp(hashtable->keys[index],key) && cnt_occ++ < hashtable->max_elem)
        index = (index +1) % hashtable->max_elem;

    if(cnt_occ > hashtable->max_elem)
    {
        logprint("can't insert element %s in hashtable !",key);
        return -1;
    }
    else
    {
        return index;
    }
}

//check if the hashtable has a key
int hashmap_haskey(hashmap_t* const hashtable, const char* const key)
{
	if(hashtable->max_elem == 0)
	{
		logprint("No elements in hashtable !");
		return 0;
	}

	uint32_t index = (uint32_t)sdbm(key) % hashtable->max_elem;
    uint32_t cnt_occ = 0;

   while(cnt_occ++ < hashtable->max_elem)
   {
		if(hashtable->keys[index])
		{
			if(strcmp(hashtable->keys[index],key) == 0)
				break;
		}

		index = (index +1) % hashtable->max_elem;
   }

   if(cnt_occ > hashtable->max_elem || hashtable->keys[index] == NULL)
		return 0;
	else
		return 1;
}

//iterate on a hashmap with a for loop, use if you remove any element in the hashmap within the loop
void* hashmap_iterate_const(hashmap_t* const hashtable, uint32_t* ent_indx, uint32_t* ent_count, const uint32_t max_count)
{
	while ((*ent_count) <  max_count)
	{
		if (hashtable->keys[(*ent_indx)])
		{
			(*ent_count)++;
			return hashtable->values[(*ent_indx)++];
		}

		(*ent_indx)++;
	}

	return NULL;
}

//iterate on a hashmap with a for loop, don't use if you remove any element in the hashmap within the loop
void* hashmap_iterate(hashmap_t* const hashtable,uint32_t* ent_indx,uint32_t* ent_count)
{
	return hashmap_iterate_const(hashtable, ent_indx, ent_count, hashtable->num_elem);
}

//get the keys into this hashmap as a comma separated string, all the keys are between quote
void hashmap_getkeysstring(hashmap_t* const hashtable, char* out_string, size_t out_string_len)
{
	uint32_t index = 0;
	uint32_t cnt_occ = 0;

	size_t len_left = out_string_len;

	while (cnt_occ < hashtable->num_elem && len_left > 0)
	{
		if (hashtable->keys[index])
		{
			if (len_left + 1 < strlen(hashtable->keys[index]))
			{
				logprint("Can't store all the keys of a hashmap into the provided buffer, size of %d too small, so far %d elements of a total of %d have been stored", out_string_len, cnt_occ, hashtable->num_elem);
				break;
			}

			//comma only if not the last element in the hashmap
			if((cnt_occ + 1) < hashtable->num_elem)
				w_sprintf(out_string, len_left, "'%s',", hashtable->keys[index]);
			else
				w_sprintf(out_string, len_left, "'%s'", hashtable->keys[index]);

			size_t strprocessed = strlen(hashtable->keys[index]) + 1;
			out_string += strprocessed;
			len_left -= strprocessed;
			cnt_occ++;
		}

		index++;
	}
}

//add a new key/value pair to the hashtable
void* hashmap_push(hashmap_t* const hashtable,const char* key)
{
    int32_t index = 0;

    if(hashtable->num_elem == hashtable->max_elem)
    {
		//resize hashmap
		hashmap_resizealloc(hashtable,hashtable->max_elem + hashtable->pool_values);
    }

    index = hashmap_getinsertindex(hashtable,key);

	if (hashtable->keys[index] != NULL) {
		logprint("This key is already in use ! please use another key name : %s",key);
		return NULL;
	}

	char* new_key = (char*)wf_malloc(strlen(key) + 1);

	w_strcpy(new_key,strlen(key)+1,key);

	void* new_value = (void*)wf_malloc(hashtable->value_size);
	memset(new_value,0,hashtable->value_size);

    hashtable->keys[index] = new_key;
	hashtable->values[index] = new_value;
    hashtable->num_elem++;
    return new_value;
}

void hashmap_rename(hashmap_t* const hashtable,const char* const old_key,const char* const new_key)
{
	if(hashtable->num_elem == hashtable->max_elem)
    {
		//resize hashmap
		hashmap_resizealloc(hashtable,hashtable->max_elem + hashtable->pool_values);
    }

	int old_index = hashmap_index(hashtable,old_key);

	int new_index = hashmap_getinsertindex(hashtable,new_key);

	if (hashtable->keys[new_index] != NULL) {
		logprint("This key is already in use ! can't rename %s to %s", old_key, new_key);
		return;
	}

	if(old_index != new_index)
	{
		hashtable->values[new_index] = hashtable->values[old_index];
		hashtable->values[old_index] = NULL;
	}

	char* _new_key = (char*)wf_malloc(strlen(new_key) + 1);

	w_strcpy(_new_key,strlen(new_key)+1,new_key);

	wf_free(hashtable->keys[old_index]);
	hashtable->keys[old_index] = NULL;

	hashtable->keys[new_index] = _new_key;
}

//remove a key/value pair from the hashtable
void hashmap_remove(hashmap_t* const hashtable,const char* const key)
{
    int32_t index = 0;

    if(hashtable->num_elem == 0)
    {
        logprint("Hashtable empty, can't remove element %s",key);
        return;
    }

     index = hashmap_index(hashtable,key);

	 if(index != -1)
	 {
		 wf_free(hashtable->keys[index]);
		 wf_free(hashtable->values[index]);
		 hashtable->keys[index] = NULL;
		 hashtable->values[index] = NULL;
		 hashtable->num_elem--;
	 }
	 else{
		 logprint("Can't remove a non existant key ! %s", key);
	 }
}

