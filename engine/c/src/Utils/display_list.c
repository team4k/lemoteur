#include <inttypes.h>
#include <Base/types.h>
#include <stdlib.h>
#include <Engine.h>

#include <Utils/display_list.h>

void display_list_init(int16_t display_list[MAX_ZINDEX][MAX_DISPLAY_VALUE])
{
	//initialize our display list
	for(int16_t i = 0;i < MAX_ZINDEX;i++)
	{
		for(int16_t ielement = 0;ielement < MAX_DISPLAY_VALUE;ielement++)
			display_list[i][ielement] = -1;
	}
}

void display_list_add(int16_t display_list[MAX_ZINDEX][MAX_DISPLAY_VALUE],int16_t z_order,int16_t element_pos)
{
	for(int16_t ielement = 0;ielement < MAX_DISPLAY_VALUE;ielement++)
	{
		if(display_list[z_order][ielement] == -1)
		{
			display_list[z_order][ielement] = element_pos;
			break;
		}
	}
}

void display_list_update(int16_t display_list[MAX_ZINDEX][MAX_DISPLAY_VALUE],int16_t z_order,int16_t old_element_pos,int16_t new_element_pos)
{
	for(int16_t ielement = 0;ielement < MAX_DISPLAY_VALUE;ielement++)
	{
		if(display_list[z_order][ielement] == old_element_pos)
		{
			display_list[z_order][ielement] = new_element_pos;
			break;
		}
	}
}

void display_list_remove(int16_t display_list[MAX_ZINDEX][MAX_DISPLAY_VALUE],int16_t z_order,int16_t element_pos)
{
	wbool reorg = wfalse;

	for(int16_t ielement = 0;ielement < MAX_DISPLAY_VALUE;ielement++)
	{
		if(!reorg)
		{
			if(display_list[z_order][ielement] == element_pos)
			{
				display_list[z_order][ielement] = -1;
				reorg = wtrue;
			}
		}
		else //after we removed our npc from display list, reorganize the list so we don't have any hole in it
		{
			if(display_list[z_order][ielement] != -1)
			{
				display_list[z_order][ielement-1] = display_list[z_order][ielement];
				display_list[z_order][ielement] = -1;
			}
			else
				break;
		}
	}

}