#include <inttypes.h>
#include <stdlib.h>
#include <string.h>

#include <Engine.h>

#include <Base/types.h>

#include <Debug/Logprint.h>

#include <Utils/tempallocator.h>



void tempallocator_init(tempallocator_t* const allocator,size_t pool_size)
{
    allocator->start =  (intptr_t)wf_calloc(1,pool_size);
    allocator->end = allocator->start + pool_size;
    allocator->current = allocator->start;
    allocator->pool_size = pool_size;
}

void* tempallocator_getmemory(tempallocator_t* const allocator,size_t alloc_size)
{
    void* returned_pointer = (void*)allocator->current;

    if(allocator->current + alloc_size >= allocator->end)
    {
        logprint("Temporary allocator out of memory ! tried to get a buffer of %d, only %d left",alloc_size, allocator->end - allocator->current);
        return NULL;
    }

    allocator->current += alloc_size; 

    return returned_pointer;
}

void tempallocator_dummyfree(tempallocator_t* const allocator,void* pointer)
{

}

void* tempallocator_realloc(tempallocator_t* const allocator, void* allocated_memory,size_t old_size,size_t new_size)
{
	void* new_pointer = tempallocator_getmemory(allocator, new_size);

	if (new_size < old_size)
	{
		old_size = new_size;
	}

	w_memcpy(new_pointer, new_size, allocated_memory, old_size);

	return new_pointer;
}

void tempallocator_clear(tempallocator_t* const allocator)
{
    memset((void*)allocator->start,0,allocator->pool_size);
    allocator->current = allocator->start;
}

char* tempallocator_string_alloc(tempallocator_t* const allocator,const char* src)
{
	size_t str_size = strlen(src)+1;
	char* dest = (char*)tempallocator_getmemory(allocator,str_size);
	w_strcpy(dest,str_size,src);

	return dest;
}

void tempallocator_free(tempallocator_t* allocator)
{
    wf_free((void*)allocator->start);
    memset(allocator,0,sizeof(tempallocator_t));
}