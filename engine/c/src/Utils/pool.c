#include <stdio.h>
#include <inttypes.h>
#include <string.h>
#include <stdlib.h>

#include <Engine.h>

#include <Base/types.h>

#include <Debug/Logprint.h>

#include <Utils/pool.h>

//init allocation
void pool_initalloc(pool_t* const pool, size_t hard_limit, size_t value_size, size_t initial_pool_size)
{
	if (initial_pool_size > hard_limit)
	{
		logprint("Can't have a pool with a initial size superior to it hard limit !");
		return;
	}


	pool->values = (void**)wf_malloc(hard_limit * sizeof(void*));
	pool->used = (wbool*)wf_malloc(hard_limit * sizeof(wbool));
	pool->value_size = value_size;
	pool->hard_limit = hard_limit;
	pool->first_free = 0;
	pool->num_elem = 0;

	pool->pool_size = initial_pool_size;

	//initialise all values
	for (uint32_t indx = 0; indx < initial_pool_size; indx++)
	{
		void* new_value = (void*)wf_malloc(value_size);
		memset(new_value, 0, value_size);
		pool->values[indx] = new_value;
		pool->used[indx] = wfalse;
	}


}

//free pool
void pool_free(pool_t* const pool)
{
	for (uint32_t indx = 0; indx < pool->pool_size; indx++)
	{
		wf_free(pool->values[indx]);
	}

	wf_free(pool->values);
	wf_free(pool->used);
}


//mark the next available value as used and return it
WAFA void* pool_usenextvalue(pool_t* const pool)
{
	for (uint32_t indx = pool->first_free; indx < pool->pool_size; indx++)
	{
		if (pool->used[indx] == wfalse) {
			pool->used[indx] = wtrue;
			pool->first_free = indx + 1;
			pool->num_elem++;
			return pool->values[indx];
		}
	}

	//if the pool is filled, extend it by one element, as long as we don't go over the hard limit
	if (pool->pool_size < pool->hard_limit) 
	{	
		void* new_value = (void*)wf_malloc(pool->value_size);
		memset(new_value, 0, pool->value_size);
		pool->values[pool->pool_size] = new_value;
		pool->used[pool->pool_size] = wtrue;
		pool->pool_size++;
		pool->num_elem++;
		return new_value;
	}
	else
	{
		logprint("The pool as reached it's hard limit ! can't had another element...");
		return NULL;
	}
}

//iterator helper
void* pool_iterate(pool_t* const pool, uint32_t* ent_indx, uint32_t* ent_count,uint32_t* pool_iter)
{
	for (uint32_t indx = (*pool_iter); indx < pool->pool_size; indx++)
	{
		if (pool->used[indx] == wtrue)
		{
			(*ent_indx) = indx;
			(*ent_count)++;
			(*pool_iter)++;
			return pool->values[indx];
		}

		(*pool_iter)++;
	}

	return NULL;
}

//mark the value corresponding to the index as free
WAFA void pool_recyclevalue(pool_t* const pool, uint32_t index)
{
	if (index < pool->pool_size) {
		pool->used[index] = wfalse;
		memset(pool->values[index], 0, pool->value_size);
		
		if (index < pool->first_free) {
			pool->first_free = index;
		}

		pool->num_elem--;

	}
	else {
		logprint("can't recycle a value with an index outside of pool size !");
	}
}

//recycle the entire pool
WAFA void pool_recycle(pool_t* const pool)
{
	for (uint32_t indx = 0; indx < pool->pool_size; indx++)
	{
		pool->used[indx] = wfalse;
		memset(pool->values[indx], 0, pool->value_size);
		pool->first_free = 0;
		pool->num_elem = 0;
	}
}