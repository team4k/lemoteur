#define _BSD_SOURCE
#include <stdio.h>
#include <inttypes.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <math.h>
#include <errno.h>

#if defined(_WIN32)
	#include <Windows.h>
	#include <strsafe.h>
	#include <io.h>
#else
    #include <dirent.h>
    #include <sys/stat.h>
    #include <sys/types.h>
    #include <sys/param.h>
    #include <fcntl.h>
    #include <unistd.h>
#endif

#ifndef NO_ZIPLIB
	#include <zip.h>
#endif

#include <Render.h>

#include <Engine.h>

#include <GameObjects/Level.pb-c.h>
#include <GameObjects/Characters.pb-c.h>
#include <GameObjects/Shaders.pb-c.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/geom.h>
#include <Base/Color.h>

#include <Debug/Logprint.h>

#include <Utils/Hash.h>

#include <Render/render_manager.h>

#include <Graphics/TextureLoader.h>
#include <Resx/shader_loader.h>

#include <Sound/Sound.h>

#include <Graphics/TextureRegion.h>

#include <Graphics/FontLoader.h>

#include <Resx/Resources.h>

//resources type in order = texture / level / animation / sound / font / shader / (empty) / uncompressed sound / shader definition (for direct3d shaders) / world definition / level fragment
const char* const resx_ext[] = {"png","lwf","awf", "ogg","json","shad"," ","wav","shaddef","mwf","fwf"};
#define RESX_SIZE sizeof(resx_ext) / sizeof(char*)


size_t getresxextsize()
{
     return RESX_SIZE;
}

char** getresxext()
{
   return (char**)&resx_ext[0];
}



//load game resources informations from given path

wbool init_resources(resources_manager_t* resx_mngr, const char* const resources_path,render_manager_t* const render_mngr) {
    int32_t resx_count = 0;
    int32_t resx_indx = 0;

	resx_mngr->collect_old_resx = wfalse;
	resx_mngr->render_mngr = render_mngr;
	resx_mngr->compressed_resx = wfalse;
	
	#if defined(ANDROID_BUILD)
		resx_mngr->compressed_resx = wtrue;
          DIR* resx_dir = 0;
          struct dirent* entry;
          struct stat fileInfo;
          char fullname[MAXPATHLEN];
          int numFiles = 0;
	#else

	#ifndef NO_ZIPLIB
			//check if assets are in a zip archive
			const char* assets_ext = get_filename_ext(resources_path);
		
			if(strcmp(assets_ext,"zip") == 0)
				resx_mngr->compressed_resx = wtrue;
	#endif

		int numFiles = 0;
		#if defined(_WIN32)
			HANDLE hFind = INVALID_HANDLE_VALUE;
			wchar_t szDir[WAFA_MAX_PATH];
			WIN32_FIND_DATA ffd;

                #else
                  DIR* resx_dir = 0;
                   struct dirent* entry;
                   struct stat fileInfo;
                   char fullname[MAXPATHLEN];
                #endif

	#endif


	if(!resx_mngr->compressed_resx)
	{
	#if defined(_WIN32)
		wchar_t rs_path[WAFA_MAX_PATH] = { 0 };
		size_t ret = 0;
		mbstowcs_s(&ret, rs_path, strlen(resources_path) + 1, resources_path, WAFA_MAX_PATH);

		StringCchCopy(szDir, WAFA_MAX_PATH, rs_path);
		StringCchCat(szDir, WAFA_MAX_PATH, L"\\*");
			
		hFind = FindFirstFileEx(szDir,FindExInfoStandard,&ffd,FindExSearchNameMatch,NULL,0);
			


		if (hFind == INVALID_HANDLE_VALUE) {
			logprint("Error while listing files in resources directory %s : Directory is not valid \n", resources_path);
			return wfalse;
		}

		//first get the number of resources in directory
		do {
			if (!(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) {
				//count the number of potential resource
				char f_name[WAFA_MAX_PATH] = { 0 };
				ret = 0;
				wcstombs_s(&ret, f_name, wcslen(ffd.cFileName) + 1, ffd.cFileName, WAFA_MAX_PATH);

				const char* ext = get_filename_ext(f_name);

				 for(int i = 0; i < RESX_SIZE;i++)
				 {
					  if(strcmp(ext, resx_ext[i]) == 0)
					  {
						resx_count++;
						break;
					  }
				 }
				
			}
		} while (FindNextFile(hFind, &ffd) != 0);
	#else
		

		if (!(resx_dir = opendir(resources_path))) {
			logprint("Error while listing files in resources directory %s : Directory is not valid \n", resources_path);
			return wfalse;
		}

		while ((entry = readdir(resx_dir))) {
			sprintf(fullname, "%s/%s", resources_path, entry->d_name);

			if (lstat(fullname, &fileInfo) < 0)
				logprint("Error while reading info on file : %s \n", fullname);

			if (!S_ISDIR(fileInfo.st_mode)) {
				 const char* ext = get_filename_ext(entry->d_name);

				 for(int i = 0; i < RESX_SIZE;i++)
				 {
					  if(strcmp(ext, resx_ext[i]) == 0)
					  {
						resx_count++;
						break;
					  }
				 }
			}
		}

		closedir(resx_dir);

	#endif
	}
	else
	{
	#ifndef NO_ZIPLIB
        int error_p = 0;
		resx_mngr->zip_archive = zip_open(resources_path,0,&error_p);

		if(resx_mngr->zip_archive == NULL)
		{
            char error_string[128] = {0};
            zip_error_to_str(error_string,128,error_p,errno);
            logprint("Error while loading zip archive, path: %s, error: %s",resources_path,error_string);
            return wfalse;
		}	

                        
		numFiles = zip_get_num_files((struct zip*)resx_mngr->zip_archive);

		for (int i=0; i<numFiles; i++) 
		{
			const char* name = zip_get_name((struct zip*)resx_mngr->zip_archive, i, 0);
			
			if(!str_starts_with("assets",name))
				continue;


			if (name == NULL)
			{
				logprint("Error reading zip file name at index %i : %s", i, zip_strerror((struct zip*)resx_mngr->zip_archive));
				return wfalse;
			}

			const char* ext = get_filename_ext(name);

			 for(int i2 = 0; i2 < RESX_SIZE;i2++)
			 {
				  if(strcmp(ext, resx_ext[i2]) == 0)
				  {
					resx_count++;
					break;
				  }
			}
		}

	#endif
	}

    resx_mngr->resx_list = (resource_t*) wf_malloc(resx_count * sizeof (resource_t));
    resx_mngr->resx_hash = (hashtable_t*) wf_malloc(sizeof (hashtable_t));
    resx_mngr->resx_hash->keys = (char**) wf_malloc(resx_count * sizeof (char*));
	w_strcpy(resx_mngr->resx_path,260,resources_path);

    //initialise keys array
    for (resx_indx = 0; resx_indx < resx_count; resx_indx++)
	{
        resx_mngr->resx_hash->keys[resx_indx] = 0;
		resx_mngr->resx_list[resx_indx].type = empty;
	}

    resx_mngr->resx_hash->max_elem = resx_count;
    resx_mngr->resx_hash->num_elem = 0;

	if(!resx_mngr->compressed_resx)
	{
		//then add info to resources array
		#if defined(_WIN32)
		hFind = FindFirstFileEx(szDir, FindExInfoStandard, &ffd, FindExSearchNameMatch, NULL, 0);

		do {
			if (!(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) {

				char f_name[WAFA_MAX_PATH] = { 0 };
				mbstate_t state = { 0 };
				size_t ret = 0;
				wcstombs_s(&ret, f_name, wcslen(ffd.cFileName) + 1, ffd.cFileName, WAFA_MAX_PATH);

				const char* ext = get_filename_ext(f_name);

				 for(int i = 0; i < RESX_SIZE;i++)
				 {
						if (strcmp(ext, resx_ext[i]) == 0)
						{
							add_resx(resx_mngr, f_name,(resx_type)i,resources_path,wfalse,NULL,NULL);
							break;
						}
				 }
			}
		} while (FindNextFile(hFind, &ffd) != 0);
		#else
		resx_dir = opendir(resources_path);

		while ((entry = readdir(resx_dir))) {
			sprintf(fullname, "%s/%s", resources_path, entry->d_name);

			if (lstat(fullname, &fileInfo) < 0)
				logprint("Error while reading info on file : %s \n", fullname);

			if (!S_ISDIR(fileInfo.st_mode)) {
				const char* ext = get_filename_ext(entry->d_name);

			   for(int i = 0; i < RESX_SIZE;i++)
				{
						if (strcmp(ext, resx_ext[i]) == 0)
						{
								add_resx(resx_mngr, entry->d_name,(resx_type)i,resources_path,wfalse,NULL,NULL);
								break;
						}
				}
			}
		}

		closedir(resx_dir);

		#endif
	}
	else
	{
	  #ifndef NO_ZIPLIB
		for (int i=0; i<numFiles; i++) 
		{
			const char* name = zip_get_name((struct zip*)resx_mngr->zip_archive, i, 0);
			
			if(!str_starts_with("assets",name))
				continue;


			if (name == NULL)
			{
				logprint("Error reading zip file name at index %i : %s", i, zip_strerror((struct zip*)resx_mngr->zip_archive));
				return wfalse;
			}

			const char* ext = get_filename_ext(name);
			const char* stripped_file_name = get_file_name2(name);

			 for(int i2 = 0; i2 < RESX_SIZE;i2++)
			 {
				  if(strcmp(ext, resx_ext[i2]) == 0)
				  {
					add_resx(resx_mngr,stripped_file_name ,(resx_type)i2,"assets",wfalse,NULL,NULL);
					break;
				  }
			}
		}
	  #endif

	}



   return wtrue;

}

void start_resx_collect(resources_manager_t* resx_mngr)
{
	resx_mngr->collect_old_resx = wtrue;
}

void add_resx(resources_manager_t* resx_mngr, const char* filename, resx_type type, const char* const resources_path,wbool is_virtual,const char* linked_resx,void* virtual_data) {
   
	if(resx_mngr->resx_hash->num_elem >= resx_mngr->resx_hash->max_elem)
	{
		resx_mngr->resx_hash->max_elem++;

		resx_mngr->resx_hash->keys = (char**)wf_realloc(resx_mngr->resx_hash->keys, resx_mngr->resx_hash->max_elem * sizeof (char*));

		//reinit all keys
		for(uint32_t i = 0; i < resx_mngr->resx_hash->max_elem;i++)
			resx_mngr->resx_hash->keys[i] = NULL;

		resx_mngr->resx_hash->num_elem = 0;

		//new resx list
		resource_t* tmp_list = (resource_t*)wf_malloc(resx_mngr->resx_hash->max_elem * sizeof (resource_t));

		//put resx element from old list to new list
		for(uint32_t i = 0; i < (resx_mngr->resx_hash->max_elem - 1);i++)
		{
			if(resx_mngr->resx_list[i].type != empty)
			{
				//rehash keys
				int32_t resx_indx =  hashtable_pushkey(resx_mngr->resx_hash, resx_mngr->resx_list[i].name);
				tmp_list[resx_indx] = resx_mngr->resx_list[i];
			}
		}

		//replace old list with new list
		wf_free(resx_mngr->resx_list);
		resx_mngr->resx_list = tmp_list;
	}


	int32_t resx_indx = 0;
    char* name = (char*) wf_malloc(strlen(filename) + 1 * sizeof (char));
    w_strcpy(name,strlen(filename) + 1, filename);

    resx_indx = hashtable_pushkey(resx_mngr->resx_hash, name);
    resx_mngr->resx_list[resx_indx].type = type;

    resx_mngr->resx_list[resx_indx].name = name;
	resx_mngr->resx_list[resx_indx].survive_collect = wfalse;
    resx_mngr->resx_list[resx_indx].path = (char*) wf_malloc((strlen(filename) + strlen(resources_path) + 2) * sizeof (char));
    resx_mngr->resx_list[resx_indx].pointer = 0;
	resx_mngr->resx_list[resx_indx].is_virtual = is_virtual;

	if(linked_resx)
		w_strcpy(resx_mngr->resx_list[resx_indx].linked_resx,128,linked_resx);

	resx_mngr->resx_list[resx_indx].virtual_data = virtual_data;

	if(!resx_mngr->compressed_resx)
		w_sprintf(resx_mngr->resx_list[resx_indx].path,(strlen(filename) + strlen(resources_path) + 2), "%s%c%s", &resources_path[0],PATH_SEP,resx_mngr->resx_list[resx_indx].name);
	else
		w_sprintf(resx_mngr->resx_list[resx_indx].path,(strlen(filename) + strlen(resources_path) + 2), "%s/%s", &resources_path[0],resx_mngr->resx_list[resx_indx].name);
}

void remove_resx(resources_manager_t* resx_mngr,const char* filename, resx_type type)
{
	int32_t resx_indx = 0;
	
	if(!hashtable_haskey(resx_mngr->resx_hash,filename))
	{
		logprint("Can't remove resource %s , not found in resources list",filename);
		return;
	}

	resx_indx = hashtable_index(resx_mngr->resx_hash,filename);

	char* ptr = resx_mngr->resx_hash->keys[resx_indx];
	hashtable_removekey(resx_mngr->resx_hash,filename);
	wf_free(ptr);
	resx_mngr->resx_hash->keys[resx_indx] = NULL;

	free_resx_ptr(resx_mngr,type,resx_indx);

	wf_free(resx_mngr->resx_list[resx_indx].path);
	
	if(resx_mngr->resx_list[resx_indx].virtual_data)
		wf_free(resx_mngr->resx_list[resx_indx].virtual_data);

	memset(&resx_mngr->resx_list[resx_indx],0,sizeof(resource_t));
}

unsigned char* get_resx_content(resources_manager_t* const resx_mngr,const char* path,size_t* resx_size,const char* read_param)
{
	unsigned char* buffer_return = NULL;
	
	if(resx_mngr != NULL && resx_mngr->compressed_resx)
	{
		#ifndef NO_ZIPLIB
		struct zip_file* file = zip_fopen((struct zip*)resx_mngr->zip_archive,path,0);
		
		if(!file)
		{
		    logprint("Can't open file in archive! %s , libzip error: %s ", path,zip_strerror((struct zip*)resx_mngr->zip_archive));
		    return NULL;
		}

		 struct zip_stat st;
		 zip_stat_init(&st);
		 zip_stat((struct zip*)resx_mngr->zip_archive, path, 0, &st);

		(*resx_size) = (size_t)st.size;

		if(read_param && strcmp(read_param,"r") == 0)
			(*resx_size)++;


		buffer_return = (unsigned char*) wf_malloc((*resx_size) * sizeof (unsigned char));
		memset(buffer_return,0,(*resx_size) * sizeof (unsigned char));
		zip_fread(file,buffer_return,(*resx_size) * sizeof (unsigned char));
		zip_fclose(file);

		if(read_param && strcmp(read_param,"r") == 0)
			buffer_return[(*resx_size) - 1] = '\0';//automatically null terminate text files
	   #endif
	}
	else
	{
		#ifdef _WIN32

		HANDLE handle = INVALID_HANDLE_VALUE;
		wchar_t tmp_file[1024] = { 0 };

		size_t ret;
		mbstowcs_s(&ret, tmp_file, strlen(path) + 1, path, 1024);


		#if defined(WINAPI_FAMILY) && (WINAPI_FAMILY == WINAPI_FAMILY_PHONE_APP)
			handle = CreateFile2(tmp_file, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, OPEN_EXISTING, NULL);
		#else
			handle = CreateFile(tmp_file, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, 0,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL, NULL);
		#endif

		if (handle == INVALID_HANDLE_VALUE)
		{
			logprint("Can't open file in filesystem! %s ", path);
			return NULL;
		}

		DWORD dwFileSizeLow, dwFileSizeHigh, dwError = NO_ERROR;

		FILE_STANDARD_INFO fileInfo;
		GetFileInformationByHandleEx(handle, FileStandardInfo, &fileInfo, sizeof(fileInfo));

		dwFileSizeLow = fileInfo.EndOfFile.LowPart;
		dwFileSizeHigh = fileInfo.EndOfFile.HighPart;

		if (dwFileSizeLow == 0xFFFFFFFF)
			dwError = GetLastError();

		if (dwError != NO_ERROR)
		{
			logprint("Error while computing file %s size!", path);
			CloseHandle(handle);
			return NULL;
		}

		(*resx_size) = (size_t)(dwFileSizeLow + ((__int64)dwFileSizeHigh << 32));

		//always add one to a text file, it will serve as our null char to terminate the string
		if(read_param && strcmp(read_param,"r") == 0)
			(*resx_size)++;

		buffer_return = (unsigned char*)wf_malloc((*resx_size) * sizeof(unsigned char));
		RtlSecureZeroMemory(buffer_return, (*resx_size) * sizeof(unsigned char));

		DWORD dwNumberOfBytesRead;

		if (ReadFile(handle, buffer_return, (*resx_size) * sizeof(unsigned char), &dwNumberOfBytesRead, 0) == 0)
		{
			logprint("Error while reading file %s!", path);
			CloseHandle(handle);
			return NULL;
		}

		CloseHandle(handle);

		return buffer_return;
			

		#else
		int32_t flag = O_RDONLY;
		int32_t fd = -1;

		if (read_param && strcmp(read_param, "r") == 0)
			flag = O_RDONLY;

		fd = open(path, flag);

		if (fd == -1)
		{
			logprint("Can't open file in filesystem! %s ", path);
			return NULL;
		}

		struct stat statbuf;

		if (fstat(fd, &statbuf) == -1)
		{
			logprint("Error while computing file %s size!", path);
			close(fd);
			return NULL;
		}

		(*resx_size) = (size_t)statbuf.st_size;

		//always add one to a text file, it will serve as our null char to terminate the string
		if(read_param && strcmp(read_param,"r") == 0)
			(*resx_size)++;

		buffer_return = (unsigned char*)wf_malloc((*resx_size) * sizeof(unsigned char));
		memset(buffer_return, 0, (*resx_size) * sizeof(unsigned char));

		if (read(fd, buffer_return, (*resx_size)) == -1)
		{
			logprint("Error while reading file %s!", path);
			close(fd);
			return NULL;
		}

		close(fd);

		return buffer_return;

		#endif
	}

	//logprint("Resx %s Loaded",path);

	return buffer_return;
}


void resx_set_from_rawpacker(resources_manager_t* const resx_mngr,unsigned char* packer_data,size_t size_data)
{
	PackInfo* pack_inf = pack_info__unpack(NULL,size_data,packer_data);

	resource_t* linked_resx = get_resx(resx_mngr,pack_inf->texture_pack_id);

	if(linked_resx == NULL)
	{
		logprint("Failed to update packer, the texture_pack_id %s file doesn't exist in resources!",pack_inf->texture_pack_id);
		return;
	}

	char linked_resx_name[128];

	w_strcpy(linked_resx_name,128,linked_resx->name);

	//create / update virtual resx
	for(uint32_t i = 0; i < pack_inf->n_texture_pack_list;i++)
	{
		Rect_t* dimension = NULL;
		wbool has_resource = has_resx(resx_mngr,pack_inf->texture_pack_list[i]->texture_id);

		if(has_resource)
			dimension = gettexture_resx(resx_mngr,pack_inf->texture_pack_list[i]->texture_id)->region;
		else
			dimension = (Rect_t*)wf_malloc(sizeof(Rect_t));

		dimension->position.x = (float)pack_inf->texture_pack_list[i]->texture_rectangle->position->x;
		dimension->position.y = (float)pack_inf->texture_pack_list[i]->texture_rectangle->position->y;
		dimension->width = pack_inf->texture_pack_list[i]->texture_rectangle->width;
		dimension->height = pack_inf->texture_pack_list[i]->texture_rectangle->height;
	
		if(!has_resource)
			add_resx(resx_mngr,pack_inf->texture_pack_list[i]->texture_id,texture,"",wtrue,linked_resx_name,dimension);
	}

	//free our packing values
	pack_info__free_unpacked(pack_inf,NULL);

}

void resx_set_from_packer(resources_manager_t* const resx_mngr,const char* packer_name)
{
	//load packer file
	char packer_path[WAFA_MAX_PATH];

	if(resx_mngr->compressed_resx)
		w_sprintf(packer_path,WAFA_MAX_PATH,"assets/%s",packer_name);
	else
		w_sprintf(packer_path,WAFA_MAX_PATH,"%s%c%s",resx_mngr->resx_path,PATH_SEP,packer_name);

	size_t packer_size = 0;
	unsigned char* packer_buffer = get_resx_content(resx_mngr,packer_path,&packer_size,NULL);

	if(packer_buffer == NULL)
	{
		logprint("Failed to set packer, can't find packer file %s",packer_name);
		return;
	}

	PackInfo* pack_inf = pack_info__unpack(NULL,packer_size,packer_buffer);

	//get linked resx
	resource_t* linked_resx = get_resx(resx_mngr,pack_inf->texture_pack_id);

	if(linked_resx == NULL)
	{
		logprint("Failed to set packer, the texture_pack_id %s file doesn't exist in resources!",pack_inf->texture_pack_id);
		return;
	}

	char linked_resx_name[128];

	w_strcpy(linked_resx_name,128,linked_resx->name);

	//create virtual resx
	for(uint32_t i = 0; i < pack_inf->n_texture_pack_list;i++)
	{
		Rect_t* dimension = (Rect_t*)wf_malloc(sizeof(Rect_t));

		dimension->position.x = (float)pack_inf->texture_pack_list[i]->texture_rectangle->position->x;
		dimension->position.y = (float)pack_inf->texture_pack_list[i]->texture_rectangle->position->y;
		dimension->width = pack_inf->texture_pack_list[i]->texture_rectangle->width;
		dimension->height = pack_inf->texture_pack_list[i]->texture_rectangle->height;

		add_resx(resx_mngr,pack_inf->texture_pack_list[i]->texture_id,texture,"",wtrue,linked_resx_name,dimension);
	}

	//free our packing values
	wf_free(packer_buffer);
	pack_info__free_unpacked(pack_inf,NULL);
}

resource_t* get_resx(resources_manager_t* const resx_mngr,const char* resx_name)
{
	int32_t resx_indx = 0;
	
	if(!hashtable_haskey(resx_mngr->resx_hash,resx_name))
	{
		logprint("Can't retrieve resource %s , not found in resources list",resx_name);
		return NULL;
	}

	resx_indx = hashtable_index(resx_mngr->resx_hash,resx_name);

	return &resx_mngr->resx_list[resx_indx];
}

void free_resx_ptr(resources_manager_t* resx_mngr,resx_type type,int32_t resx_indx)
{
	switch(type)
	{
		case texture:
			{
				texture_info* ptr = (texture_info*) resx_mngr->resx_list[resx_indx].pointer;

				if(ptr && ptr->texture_pointer && !ptr->virtual_texture)//unload gl pointer
					render_delete_texture(resx_mngr->render_mngr,ptr->texture_pointer,wfalse);
				

					wf_free(ptr);
			}
			
		break;
		case animation:
			{
				AnimationList* ptr2 = (AnimationList*) resx_mngr->resx_list[resx_indx].pointer;

				if(ptr2)
					animation_list__free_unpacked(ptr2,NULL);
			}

			break;
		case level:
			{
				Level* ptr3 = (Level*) resx_mngr->resx_list[resx_indx].pointer;

				if(ptr3)
					level__free_unpacked(ptr3,NULL);
			}

			break;
		case world:
			{
				World* ptr7 = (World*)resx_mngr->resx_list[resx_indx].pointer;

				if (ptr7)
					world__free_unpacked(ptr7, NULL);
			}
			break;
        case fragment:
            {
                Fragment* ptr8 = (Fragment*)resx_mngr->resx_list[resx_indx].pointer;

                if(ptr8)
                    fragment__free_unpacked(ptr8,NULL);

				unsigned char* buffer = (unsigned char*)resx_mngr->resx_list[resx_indx].virtual_data;

				if (buffer)
					wf_free(buffer);

				resx_mngr->resx_list[resx_indx].virtual_data = NULL;
				resx_mngr->resx_list[resx_indx].virtual_data_size = 0;
            }
            break;
		case sound:
		case sound_uncompressed:
			{
				sound_sample_t* ptr4 = (sound_sample_t*) resx_mngr->resx_list[resx_indx].pointer;

				if(ptr4)
				{
					sound_close(ptr4);
					free_sound_struct(ptr4);
				}
			}
			break;
		case font:
			{
				font_resx_t* ptr5 = (font_resx_t*) resx_mngr->resx_list[resx_indx].pointer;

				if(ptr5)
				{
					wf_free(ptr5->font_info);

					wf_free(ptr5);

				}
			}
			break;
		case shader:
			{
				shader_info* ptr6 = (shader_info*)resx_mngr->resx_list[resx_indx].pointer;

				if(ptr6)
				{
					#ifdef RENDERER_DX

						if(ptr6->shader_info != NULL)
							d3_dshaderinfo__free_unpacked((D3Dshaderinfo*)ptr6->shader_info,NULL);

						ptr6->shader_info = NULL;

					#endif	

					wf_free(ptr6);
				}
			}
			break;
		case shader_d3d_def:
		case empty:
			break;
	}

	resx_mngr->resx_list[resx_indx].pointer = NULL;

	//logprint("Resx %s Freed",resx_mngr->resx_list[resx_indx].name);
}

//manually setup the content of a texture info, used mainly by editor 
void settexture_resx(resources_manager_t* resx_mngr,const char* texture_name,int16_t width,int16_t height, unsigned char* data,unsigned long data_size)
{
	//lookup the hashtable
	if(!hashtable_haskey(resx_mngr->resx_hash,texture_name))
	{
		  logprint("Texture %s doesn't exist in resources list, utiliser add_resx() ", texture_name);
		  return;
	}


    int32_t indx = hashtable_index(resx_mngr->resx_hash, texture_name);

	texture_info* ptr = (texture_info*) resx_mngr->resx_list[indx].pointer;

	if(ptr && ptr->virtual_texture)
		return;

	if(ptr && ptr->texture_pointer)//the texture have already been loaded, replace it
		render_delete_texture(resx_mngr->render_mngr,ptr->texture_pointer,wfalse);
	else
		ptr = (texture_info*) wf_malloc(sizeof (texture_info));


	ptr->width = width;
	ptr->height = height;
	ptr->region = NULL;
	ptr->virtual_texture = wfalse;
	ptr->deferredload = wfalse;
	w_strcpy(ptr->texture_name,256,texture_name);
	

	resx_mngr->resx_list[indx].pointer = ptr;

	if(!load_raw_texture(ptr,data,data_size,resx_mngr->render_mngr))
	{
		logprint("Error loading texture ! %s ", resx_mngr->resx_list[indx].name);
		return;
	}

	//update linked resx
	for (uint32_t i = 0; i < resx_mngr->resx_hash->max_elem; i++) {

		if(resx_mngr->resx_list[i].type == texture && resx_mngr->resx_list[i].pointer && resx_mngr->resx_list[i].is_virtual && strcmp(resx_mngr->resx_list[i].linked_resx,texture_name) == 0)
		{
			((texture_info*)resx_mngr->resx_list[i].pointer)->texture_pointer = ptr->texture_pointer;
		}
	}

}



wbool has_resx(resources_manager_t* resx_mngr,const char* resx_name)
{
	if(hashtable_haskey(resx_mngr->resx_hash,resx_name))
	{
		return wtrue;
	}

	return wfalse;
}

texture_info* gettexture_resx_back(resources_manager_t* resx_mngr, const char* texture_name) {
	return gettexture_resx_withfilter(resx_mngr, texture_name, resx_mngr->render_mngr->texture_filtering, wtrue);
}

texture_info* gettexture_resx(resources_manager_t* resx_mngr, const char* texture_name) {
	return gettexture_resx_withfilter(resx_mngr, texture_name, resx_mngr->render_mngr->texture_filtering,wfalse);
}

texture_info* gettexture_resx_withfilter(resources_manager_t* resx_mngr, const char* texture_name,texture_filter filter,wbool backgroundload) {
    //lookup the hashtable

	if(!hashtable_haskey(resx_mngr->resx_hash,texture_name))
	{
		  logprint("Texture %s doesn't exist in resources list", texture_name);
		return NULL;
	}

    int32_t indx = hashtable_index(resx_mngr->resx_hash, texture_name);

    texture_info* ptr = (texture_info*) resx_mngr->resx_list[indx].pointer;

    //if the texture isn't loaded, call the loadtexture function
    if (ptr == NULL) {

		if(!resx_mngr->resx_list[indx].is_virtual)
		{
			size_t texture_size = 0;
			unsigned char* texture_buffer = get_resx_content(resx_mngr, resx_mngr->resx_list[indx].path,&texture_size,0);


			if(!texture_buffer)
			{
				logprint("Can't read texture file! %s",resx_mngr->resx_list[indx].name);
				return NULL;
			}

			ptr = (texture_info*) wf_malloc(sizeof (texture_info));
			memset(ptr,0,sizeof(texture_info));
			resx_mngr->resx_list[indx].pointer = ptr;

			if (!load_texture_withfilter(ptr,texture_buffer, texture_size,resx_mngr->resx_list[indx].name,resx_mngr->render_mngr,filter, backgroundload))
			{
				logprint("Error loading Texture ! %s ", resx_mngr->resx_list[indx].name);
				wf_free(ptr);
				resx_mngr->resx_list[indx].pointer = NULL;
				wf_free(texture_buffer);
				return NULL;
			}

			w_strcpy(ptr->texture_name,256,texture_name);

			wf_free(texture_buffer);
		}
		else
		{
			//setup virtual texture from parent texture
			texture_info* parent_texture = gettexture_resx(resx_mngr,resx_mngr->resx_list[indx].linked_resx);

			ptr = (texture_info*)wf_malloc(sizeof(texture_info));
			memset(ptr,0,sizeof(texture_info));
			resx_mngr->resx_list[indx].pointer = ptr;
			ptr->virtual_texture = wtrue;
			ptr->region = (Rect_t*)resx_mngr->resx_list[indx].virtual_data;
			ptr->width = parent_texture->width;
			ptr->height = parent_texture->height;
			w_strcpy(ptr->texture_name,256,resx_mngr->resx_list[indx].name);
			w_strcpy(ptr->parent_name,256,resx_mngr->resx_list[indx].linked_resx);
			ptr->texture_pointer = parent_texture->texture_pointer;
		}
    }


	if(resx_mngr->collect_old_resx)
	{
		resx_mngr->resx_list[indx].survive_collect = wtrue;

		if(resx_mngr->resx_list[indx].is_virtual)
		{
			resource_t* lnk = get_resx(resx_mngr,resx_mngr->resx_list[indx].linked_resx);
			lnk->survive_collect = wtrue;
		}
			
	}

    return ptr;
}

Level* getlevel_resx(resources_manager_t* resx_mngr, const char* level_name) {
    //lookup the hashtable
	if(!hashtable_haskey(resx_mngr->resx_hash,level_name))
	{
		  logprint("Level %s doesn't exist in resources list", level_name);
		return NULL;
	}

    int32_t indx = hashtable_index(resx_mngr->resx_hash, level_name);
    Level* ptr = (Level*) resx_mngr->resx_list[indx].pointer;

    if (ptr == NULL) {

	size_t lSize = 0;
    unsigned char* buffer = get_resx_content(resx_mngr, resx_mngr->resx_list[indx].path,&lSize,0);

    ptr = level__unpack(NULL, lSize,buffer);

    resx_mngr->resx_list[indx].pointer = ptr;


	wf_free(buffer);
	buffer = NULL;
    }

	if(resx_mngr->collect_old_resx)
		resx_mngr->resx_list[indx].survive_collect = wtrue;

    return ptr;
}

World* getworld_resx(resources_manager_t* resx_mngr, const char* world_name) {
	if (!hashtable_haskey(resx_mngr->resx_hash, world_name))
	{
		logprint("World %s doesn't exist in resources list", world_name);
		return NULL;
	}

	int32_t indx = hashtable_index(resx_mngr->resx_hash, world_name);
	World* ptr = (World*)resx_mngr->resx_list[indx].pointer;

	if (ptr == NULL) {

		size_t lSize = 0;
		unsigned char* buffer = get_resx_content(resx_mngr, resx_mngr->resx_list[indx].path, &lSize, 0);

		ptr = world__unpack(NULL, lSize, buffer);

		resx_mngr->resx_list[indx].pointer = ptr;


		wf_free(buffer);
		buffer = NULL;
	}

	if (resx_mngr->collect_old_resx)
		resx_mngr->resx_list[indx].survive_collect = wtrue;

	return ptr;
}

//get an independent copy of a fragment, usefull when you need to add the same fragment in several place with a different position / identity
Fragment* getfragment_copy_resx(resources_manager_t* resx_mngr, const char* fragment_name, ProtobufCAllocator* temp_allocator)
{

	if (getfragment_resx(resx_mngr, fragment_name) != NULL)
	{
		int32_t indx = hashtable_index(resx_mngr->resx_hash, fragment_name);

		return fragment__unpack(temp_allocator, resx_mngr->resx_list[indx].virtual_data_size, (const unsigned char*)resx_mngr->resx_list[indx].virtual_data);
	}

	return NULL;

}

Fragment* getfragment_resx(resources_manager_t* resx_mngr, const char* fragment_name) {
    if (!hashtable_haskey(resx_mngr->resx_hash, fragment_name))
    {
        logprint("Fragment %s doesn't exist in resources list", fragment_name);
        return NULL;
    }

    int32_t indx = hashtable_index(resx_mngr->resx_hash, fragment_name);
    Fragment* ptr = (Fragment*)resx_mngr->resx_list[indx].pointer;
	unsigned char* buffer = (unsigned char*)resx_mngr->resx_list[indx].virtual_data;

	//in case of fragment we keep a buffer of our binary data in case we need to make several fragment copy
    if (ptr == NULL) 
	{

        size_t lSize = 0;
		
		if (buffer == NULL)
			buffer = get_resx_content(resx_mngr, resx_mngr->resx_list[indx].path, &lSize, 0);
		else
			lSize = resx_mngr->resx_list[indx].virtual_data_size;

        ptr = fragment__unpack(NULL, lSize, buffer);

        resx_mngr->resx_list[indx].pointer = ptr;

		resx_mngr->resx_list[indx].virtual_data = buffer;
		resx_mngr->resx_list[indx].virtual_data_size = lSize;
    }

    if (resx_mngr->collect_old_resx)
        resx_mngr->resx_list[indx].survive_collect = wtrue;

    return ptr;
}

void setanimation_resx(resources_manager_t* resx_mngr, const char* animation_name, unsigned char* animData,size_t anim_size) {
	//lookup the hashtable
	if(!hashtable_haskey(resx_mngr->resx_hash,animation_name))
	{
		 logprint("Animations data %s doesn't exist in resources list, use add_resx()  ", animation_name);
		 return;
	}

    int32_t indx = hashtable_index(resx_mngr->resx_hash, animation_name);

    AnimationList* ptr = (AnimationList*) resx_mngr->resx_list[indx].pointer;

	if(ptr)//the animation liste have already been loaded, replace it
		animation_list__free_unpacked(ptr,NULL);

	ptr = animation_list__unpack(NULL, anim_size,animData);

    resx_mngr->resx_list[indx].pointer = ptr;

}

AnimationList* getanimation_resx(resources_manager_t* resx_mngr, const char* animation_name) {
	//lookup the hashtable
	if(!hashtable_haskey(resx_mngr->resx_hash,animation_name))
	{
		 logprint("Animations data %s doesn't exist in resources list ", animation_name);
		 return NULL;
	}

    int32_t indx = hashtable_index(resx_mngr->resx_hash, animation_name);

    AnimationList* ptr = (AnimationList*) resx_mngr->resx_list[indx].pointer;

    if (ptr == NULL) {

        
        size_t lSize = 0;
        unsigned char* buffer = get_resx_content(resx_mngr, resx_mngr->resx_list[indx].path,&lSize,0);

		ptr = animation_list__unpack(NULL, lSize,buffer);

        resx_mngr->resx_list[indx].pointer = ptr;

		wf_free(buffer);
		buffer =  NULL;
    }

	if(resx_mngr->collect_old_resx)
		resx_mngr->resx_list[indx].survive_collect = wtrue;

    return ptr;

}

sound_sample_t* getsound_resx(resources_manager_t* resx_mngr,const char* sound_name)
{
	//lookup the hashtable
	if(!hashtable_haskey(resx_mngr->resx_hash,sound_name))
	{
		 logprint("Sound file %s doesn't exist in resources list", sound_name);
		 return NULL;
	}

    int32_t indx = hashtable_index(resx_mngr->resx_hash, sound_name);

	sound_sample_t* ptr = (sound_sample_t*) resx_mngr->resx_list[indx].pointer;

    if (ptr == NULL) {

        
        size_t lSize = 0;
        unsigned char* buffer = 0;
	
		ptr = alloc_sound_struct();

		wbool snd_init = wfalse;

        if(resx_mngr->compressed_resx)
		{
			#ifndef NO_ZIPLIB
			//load sound file from apk archive into memory
			buffer = get_resx_content(resx_mngr,resx_mngr->resx_list[indx].path,&lSize,0);
		
			 if(!buffer)
			 {
				 logprint("Error loading sound file! %s ", resx_mngr->resx_list[indx].path);
				 return NULL;
			 }

			  snd_init = sound_initfrombuffer(ptr,buffer,lSize);

			   char loop_file_name[256];

			   w_sprintf(loop_file_name,256,"%s.loop",resx_mngr->resx_list[indx].path);

			   long long loop_point = 0;

			   //check if we have a loop file
			   if(zip_name_locate((struct zip*)resx_mngr->zip_archive,loop_file_name,0) != -1)
			   {
				   size_t loop_size;
				   unsigned char* loop_buffer  = get_resx_content(resx_mngr,loop_file_name,&loop_size,0);

				   for(size_t loop_char = 0; loop_char < loop_size;loop_char++)
						loop_point += (long long)loop_buffer[loop_char];

				   wf_free(loop_buffer);
			   }
			   else
			   {
				   loop_point = 0;
			   }

			   sound_setlooppoint(ptr,loop_point);
			#endif
          }
		  else
		  {

			 snd_init = sound_initfromfile(ptr,resx_mngr->resx_list[indx].path);

			 char loop_file_name[256];

			w_sprintf(loop_file_name,256,"%s.loop",resx_mngr->resx_list[indx].path);

			FILE* loop_file = NULL;
			
			w_fopen(loop_file,loop_file_name,"r");
			//check if we have a loop file
			if(loop_file != NULL)
			{
				fseek(loop_file, 0, SEEK_END);
				size_t loop_size = ftell(loop_file);
				rewind(loop_file);

				char* loop_buffer = (char*) wf_malloc(loop_size * sizeof(char));
				fread(loop_buffer,1,loop_size,loop_file);
				fclose(loop_file);

				sound_setlooppoint(ptr,(long long)atoi(loop_buffer));

				wf_free(loop_buffer);
			}
			else
			{
				sound_setlooppoint(ptr,0);
			}
         }

		if(!snd_init)
		{
			logprint("Error loading sound file! %s",  resx_mngr->resx_list[indx].path);

			free_sound_struct(ptr);
			resx_mngr->resx_list[indx].pointer = NULL;
			return NULL;
		}


		

        resx_mngr->resx_list[indx].pointer = ptr;
    }

	if(resx_mngr->collect_old_resx)
		resx_mngr->resx_list[indx].survive_collect = wtrue;

    return ptr;
}

font_info* getfont_resx(resources_manager_t* resx_mngr,const char* font_name)
{
	return getfont_resx2(resx_mngr,font_name,0,0);
}

font_info* getfont_resx2(resources_manager_t* resx_mngr,const char* font_name,unsigned long char_start,unsigned long char_end)
{
	 //lookup the hashtable
	if(!hashtable_haskey(resx_mngr->resx_hash,font_name))
	{
		  logprint("Font %s doesn't exist in resources list", font_name);
		return NULL;
	}

    int32_t indx = hashtable_index(resx_mngr->resx_hash, font_name);
	font_resx_t* ptr = (font_resx_t*) resx_mngr->resx_list[indx].pointer;

	char font_texturename[256] = { 0 };
	w_strcpy(font_texturename, 256, font_name);

	get_file_without_ext(font_texturename);
	w_sprintf(font_texturename, 256, "%s.png", font_texturename);

    if (ptr == NULL) {

		size_t lSize = 0;
		//read json as text
		unsigned char* buffer = get_resx_content(resx_mngr, resx_mngr->resx_list[indx].path,&lSize,"r");

		ptr = (font_resx_t*)wf_malloc(sizeof(font_resx_t));

		w_strcpy(ptr->font_name,128,font_name);

		ptr->font_info = (font_info*)wf_calloc(1,sizeof(font_info));

		if(!load_font(ptr->font_info,font_name,buffer,lSize,resx_mngr->render_mngr))
		{
			logprint("Error with msdf font loading! %s", resx_mngr->resx_list[indx].path);
			wf_free(ptr);
			wf_free(buffer);
		    return NULL;
		}

		//msdf font texture must be loading with linear filter
		ptr->font_info->texture = gettexture_resx_withfilter(resx_mngr, font_texturename,filter_linear,wfalse);

		if (ptr->font_info->texture == NULL) {
			logprint("Error with msdf texture font loading! %s", resx_mngr->resx_list[indx].path);
			wf_free(ptr);
			wf_free(buffer);
			return NULL;
		}

		resx_mngr->resx_list[indx].pointer = ptr;
		wf_free(buffer);
    }

	if (resx_mngr->collect_old_resx)
	{
		resx_mngr->resx_list[indx].survive_collect = wtrue;
		//the associated texture must survive the collect too
		int32_t indxtexture = hashtable_index(resx_mngr->resx_hash, font_texturename);

		resx_mngr->resx_list[indxtexture].survive_collect = wtrue;
	}
		

    return ptr->font_info;
}

shader_info* getshader_resx(resources_manager_t* const resx_mngr,const char* shader_name,wbool is_fragment_shader)
{
	 //lookup the hashtable
	if(!hashtable_haskey(resx_mngr->resx_hash,shader_name))
	{
		  logprint("Shader %s doesn't exist in resources list", shader_name);
		return NULL;
	}

    int32_t indx = hashtable_index(resx_mngr->resx_hash, shader_name);

	shader_info* ptr = (shader_info*)resx_mngr->resx_list[indx].pointer;

    if (ptr == NULL) {
		//load shader buffer from file
		 size_t lSize = 0;
		ptr = (shader_info*)wf_malloc(sizeof(shader_info));

		const char* read_param = NULL; 

		#ifndef RENDERER_DX
			read_param = "r";
		#endif

		unsigned char* buffer = get_resx_content(resx_mngr, resx_mngr->resx_list[indx].path,&lSize,read_param);

		#ifdef RENDERER_DX
			//load D3D definition file
				char def_file_name[256];
				w_strcpy(def_file_name,256,resx_mngr->resx_list[indx].path);

				get_file_without_ext(def_file_name);

				char def_file_path[256];

			w_sprintf(def_file_path,256,"%s.shaddef",def_file_name);

			ptr->shader_info = NULL;

			FILE* shaderdef_file = NULL;
			
			w_fopen(shaderdef_file,def_file_path, "r");//check if we have a definition file for this shader 

			if (shaderdef_file != NULL)
			{
				size_t def_size = 0;

				unsigned char* def_buffer = get_resx_content(resx_mngr, def_file_path, &def_size, NULL);

				if (def_buffer != NULL)
				{
					ptr->shader_info = d3_dshaderinfo__unpack(NULL, def_size, def_buffer);

					wf_free(def_buffer);
				}
			}
			else
			{
				logprint("No definition file for the shader %s", resx_mngr->resx_list[indx].name);
			}
				
	   #endif


		if(!load_shader(ptr,buffer,lSize,shader_name,resx_mngr->render_mngr,is_fragment_shader))
		{
			logprint("Error while loading shader %s!", shader_name);
			wf_free(buffer);
			return NULL;
		}
		else
		{
			wf_free(buffer);
		}

		resx_mngr->resx_list[indx].pointer = ptr;

	}

	if(resx_mngr->collect_old_resx)
		resx_mngr->resx_list[indx].survive_collect = wtrue;

	return ptr;
}


void reloadshader_resx(resources_manager_t* resx_mngr, const char* shader_name)
{
	 //lookup the hashtable
	if(!hashtable_haskey(resx_mngr->resx_hash,shader_name))
	{
		logprint("Shader %s doesn't exist in resources list", shader_name);
		return;
	}

	 int32_t indx = hashtable_index(resx_mngr->resx_hash, shader_name);

	shader_info* ptr = (shader_info*)resx_mngr->resx_list[indx].pointer;

	if(ptr)
	{
		//reload shader buffer from file
		 size_t lSize = 0;

		unsigned char* buffer = get_resx_content(resx_mngr, resx_mngr->resx_list[indx].path,&lSize,"r");
		
		reload_shader(ptr,buffer,lSize,shader_name,resx_mngr->render_mngr);
		wf_free(buffer);
	}
}

void* getgeneric_resx(resources_manager_t* const resx_mngr,const char* resx_name,resx_type type,...)
{
	va_list vl;
	va_start(vl,type);
	//shader parameters
	wbool is_fragment_shader=va_arg(vl,wbool);
	va_end(vl);

	switch(type)
	{
		case texture:
			return gettexture_resx(resx_mngr,resx_name);
		case level:
			return getlevel_resx(resx_mngr,resx_name);
		case animation:
			return getanimation_resx(resx_mngr,resx_name);
		case sound:
		case sound_uncompressed:
			return getsound_resx(resx_mngr,resx_name);
		case font:
			return getfont_resx(resx_mngr,resx_name);
		case shader:
			return getshader_resx(resx_mngr,resx_name,is_fragment_shader);
		case empty:
		case shader_d3d_def:
			break;
		case fragment:
			return getfragment_resx(resx_mngr, resx_name);
			break;
		case world:
			return getworld_resx(resx_mngr, resx_name);
			break;
	}

	logprint("Invalid resx type!");
	return NULL;
}



wbool has_raw_resx(resources_manager_t* resx_mngr,const char* resx_path)
{
	#ifndef NO_ZIPLIB
	if(!resx_mngr->compressed_resx)
		return wfalse;
		
	if(zip_name_locate((struct zip*)resx_mngr->zip_archive,resx_path,0) == -1)
		return wfalse;

	return wtrue;
	#else
	return wfalse;
	#endif
}

size_t getrawdata_size(resources_manager_t* resx_mngr,const char* resx_path)
{
	#ifndef NO_ZIPLIB
	if(!resx_mngr->compressed_resx)
		return wfalse;

	struct zip_stat st;
	zip_stat_init(&st);
	zip_stat((struct zip*)resx_mngr->zip_archive, resx_path, 0, &st);
	return (size_t)st.size;
	#else
		return wfalse;
	#endif
}

int getrawdata(resources_manager_t* resx_mngr,const char* resx_path,unsigned char* buffer,long size)
{
	#ifndef NO_ZIPLIB
	if(!resx_mngr->compressed_resx)
		return wfalse;

	struct zip_file* file = zip_fopen((struct zip*)resx_mngr->zip_archive,resx_path,0);
		
	if(!file)
	{
		logprint("Error loading raw data! %s ", resx_path);
		return -1;
	}

	zip_fread(file,buffer,size);
	zip_fclose(file);
	#endif
	return 0;
}

void resx_g_clear_texture(resources_manager_t* const resx_mngr)
{
	 for (uint32_t i = 0; i < resx_mngr->resx_hash->max_elem; i++) {

		if(resx_mngr->resx_list[i].pointer && resx_mngr->resx_list[i].type == texture && !resx_mngr->resx_list[i].is_virtual)
		{
			render_delete_texture(resx_mngr->render_mngr,((texture_info*)resx_mngr->resx_list[i].pointer)->texture_pointer,wfalse);
			((texture_info*)resx_mngr->resx_list[i].pointer)->texture_pointer = NULL;
		}
	}
}


void resx_g_create_texture(resources_manager_t* const resx_mngr)
{
	 for (uint32_t i = 0; i < resx_mngr->resx_hash->max_elem; i++) {

		if(resx_mngr->resx_list[i].pointer && resx_mngr->resx_list[i].type == texture)
		{
			resource_t* c_resx = &resx_mngr->resx_list[i];

			if(resx_mngr->resx_list[i].is_virtual)
				c_resx = get_resx(resx_mngr,resx_mngr->resx_list[i].linked_resx);

			texture_info* ptr = (texture_info*)c_resx->pointer;

			if(ptr->texture_pointer == NULL)
			{
				size_t texture_size = 0;
				unsigned char* texture_buffer = get_resx_content(resx_mngr, c_resx->path,&texture_size,0);

				if(!texture_buffer)
				{
					logprint("Can't read texture file! %s",c_resx->name);
					return;
				}

				if (!load_texture(ptr,texture_buffer, texture_size,c_resx->name,resx_mngr->render_mngr,wfalse)) 
				{
					logprint("Error loading Texture ! %s ", c_resx->name);
					wf_free(texture_buffer);
					return;
				}

				wf_free(texture_buffer);
			}

			if(resx_mngr->resx_list[i].is_virtual)
				((texture_info*)resx_mngr->resx_list[i].pointer)->texture_pointer = ptr->texture_pointer;
		}
	}
}

void resx_g_clear_shader(resources_manager_t* const resx_mngr)
{
	 for (uint32_t i = 0; i < resx_mngr->resx_hash->max_elem; i++) {

		 if(resx_mngr->resx_list[i].pointer && resx_mngr->resx_list[i].type == shader)
		{
			render_delete_shader(resx_mngr->render_mngr,((shader_info*)resx_mngr->resx_list[i].pointer)->shader_pointer);
		}
	}
}

void resx_g_create_shader(resources_manager_t* const resx_mngr)
{
	for (uint32_t i = 0; i < resx_mngr->resx_hash->max_elem; i++) {

		if(resx_mngr->resx_list[i].pointer && resx_mngr->resx_list[i].type == shader)
		{
			size_t shader_size = 0;
			unsigned char* shader_buffer = get_resx_content(resx_mngr, resx_mngr->resx_list[i].path,&shader_size,"r");

			shader_info* ptr = (shader_info*)resx_mngr->resx_list[i].pointer;

			if(!shader_buffer)
			{
				logprint("Can't read shader file! %s",resx_mngr->resx_list[i].name);
				return;
			}

			if (!load_shader(ptr,shader_buffer, shader_size,resx_mngr->resx_list[i].name,resx_mngr->render_mngr,ptr->is_fragment_shader)) 
			{
				logprint("Error loading shader ! %s ", resx_mngr->resx_list[i].name);
				wf_free(shader_buffer);
				return;
			}

			wf_free(shader_buffer);
		}
	}
}

void resx_g_clear_font(resources_manager_t* const resx_mngr)
{
	 for (uint32_t i = 0; i < resx_mngr->resx_hash->max_elem; i++) {

		if(resx_mngr->resx_list[i].pointer && resx_mngr->resx_list[i].type == font)
		{
			font_resx_t* p_font = (font_resx_t*)resx_mngr->resx_list[i].pointer;

			if (p_font->font_info != NULL) {
				p_font->font_info->texture = NULL;
			}

		}
	}
}

void resx_g_create_font(resources_manager_t* const resx_mngr)
{
	for (uint32_t i = 0; i < resx_mngr->resx_hash->max_elem; i++) {

		if(resx_mngr->resx_list[i].pointer && resx_mngr->resx_list[i].type == font)
		{
			font_resx_t* p_font = (font_resx_t*)resx_mngr->resx_list[i].pointer;

			if(p_font->font_info->texture == NULL)
			{
				char font_texturename[128] = { 0 };
				w_strcpy(font_texturename, 128, p_font->font_name);

				get_file_without_ext(font_texturename);
				w_sprintf(font_texturename, 128, "%s.png", font_texturename);

				p_font->font_info->texture = gettexture_resx_withfilter(resx_mngr, font_texturename, filter_linear,wfalse);

				if (p_font->font_info->texture == NULL) {
					logprint("Error with msdf resx_g_create_font loading! %s", font_texturename);
				}
			}

		}
	}
}

void end_resx_collect(resources_manager_t* resx_mngr)
{
	resx_mngr->collect_old_resx = wfalse;

	 uint32_t i = 0;

    for (i = 0; i < resx_mngr->resx_hash->max_elem; i++) {

		if(resx_mngr->resx_list[i].pointer && !resx_mngr->resx_list[i].survive_collect)
		{
			free_resx_ptr(resx_mngr,resx_mngr->resx_list[i].type,i);
		}

		resx_mngr->resx_list[i].survive_collect = wfalse;
	}

}

void free_all_resources(resources_manager_t* resx_mngr) {
    uint32_t i = 0;

    for (i = 0; i < resx_mngr->resx_hash->max_elem; i++) {
		if(resx_mngr->resx_hash->keys[i])
		{
			if (resx_mngr->resx_list[i].pointer)
			{
				free_resx_ptr(resx_mngr,resx_mngr->resx_list[i].type,i);
			}

			wf_free(resx_mngr->resx_hash->keys[i]);
			wf_free(resx_mngr->resx_list[i].path);
			resx_mngr->resx_hash->keys[i] = NULL;
			resx_mngr->resx_list[i].path = NULL;
		}
    }

	#ifndef NO_ZIPLIB
		if(resx_mngr->compressed_resx)
			zip_close((struct zip*)resx_mngr->zip_archive);
	#endif
	

    wf_free(resx_mngr->resx_list);
	resx_mngr->resx_list = NULL;

    wf_free(resx_mngr->resx_hash->keys);
	resx_mngr->resx_hash->keys = NULL;

    wf_free(resx_mngr->resx_hash);
	resx_mngr->resx_hash = NULL;
}

