
#include <stdlib.h>
#include <inttypes.h>
#include <math.h>
#include <string.h>
#include <wchar.h>

#include <Engine.h>

#if defined(_WIN32)
	#include <Windows.h>
#endif

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/geom.h>
#include <Base/Color.h>

#include <Debug/Logprint.h>

#include <Resx/text_parser.h>


void text_parser_parse(const wchar_t* texttoparse,const ColorA_t basecolor, wchar_t* const outtextbuffer,ColorA_t* const outcolorbuffer, float* const outsizebuffer, size_t buffersize)
{

	size_t parsetextsize = a_wcslen(texttoparse);

	size_t realposchar = 0;

	char tmpcolorbuffer[COLORSIZE+1] = { 0 };
	char tmpsizebuffer[MAXTEXTSIZE+1] = { 0 };
	ColorA_t* currentcolor = NULL;
	float* currentsize = NULL;

	for (size_t ichar = 0; ichar < parsetextsize; ichar++)
	{
		ColorA_t color = basecolor;
		float size = 1.0f;

		//check if the base opening char is found
		if (wcsncmp(&texttoparse[ichar],OPENCHAR,1) == 0) {//if yes, compare with the existing markup

			//is it a single char color markup?
			if (wcsncmp(&texttoparse[ichar], COLORSINGLE, a_wcslen(COLORSINGLE)) == 0) {
				//read the existing color
				size_t endpos = ichar + a_wcslen(COLORSINGLE) + COLORSIZE;
				if (wcsncmp(&texttoparse[endpos],CLOSECHAR,1) == 0) {
					mbstate_t mbs;
					mbrlen(NULL, 0, &mbs);
					size_t retval = 0;
					const wchar_t *ptr[1];
					ptr[0] = &texttoparse[ichar + a_wcslen(COLORSINGLE)];
					w_wcsrtombs(&retval, &tmpcolorbuffer[0], (size_t)COLORSIZE+1, ptr, (size_t)COLORSIZE, &mbs);
					color = convertstring_tocolor(tmpcolorbuffer);
					ichar += a_wcslen(COLORSINGLE) + COLORSIZE + 1;
				}
			} else if (wcsncmp(&texttoparse[ichar], COLORSTART, a_wcslen(COLORSTART)) == 0) {//is it the start of a color markup?
				//read the existing color
				size_t endpos = ichar + a_wcslen(COLORSTART) + COLORSIZE;

				if (wcsncmp(&texttoparse[endpos], CLOSECHAR, 1) == 0) {
					mbstate_t mbs;
					mbrlen(NULL, 0, &mbs);
					size_t retval = 0;
					const wchar_t *ptr[1];
					ptr[0] = &texttoparse[ichar + a_wcslen(COLORSTART)];
					w_wcsrtombs(&retval, &tmpcolorbuffer[0], (size_t)COLORSIZE+1, ptr, (size_t)COLORSIZE, &mbs);
					outcolorbuffer[realposchar] = convertstring_tocolor(tmpcolorbuffer);
					currentcolor = &outcolorbuffer[realposchar];
					ichar += a_wcslen(COLORSTART) + COLORSIZE + 1;
				}
			}
			else if (wcsncmp(&texttoparse[ichar], COLOREND, a_wcslen(COLOREND)) == 0) {//is it the end of the color markup?
				//reset the current color
				currentcolor = NULL;
				ichar += a_wcslen(COLOREND);
			}
			else if (wcsncmp(&texttoparse[ichar], SIZESINGLE, a_wcslen(SIZESINGLE)) == 0) { //is it as single char textsize markup?
				//read the size
				size_t endpos = ichar + a_wcslen(SIZESINGLE);
				size_t sizesize = 0;
				wbool oksize = wfalse;
				while(wcsncmp(&texttoparse[endpos+ sizesize], CLOSECHAR, 1) != 0 || sizesize > MAXTEXTSIZE) { //lookup closing char, size can't be more than MAXTEXTSIZE char
					sizesize++;
				}

				oksize = (wbool)(wcsncmp(&texttoparse[endpos + sizesize], CLOSECHAR, 1) == 0);

				if (oksize) {
					mbstate_t mbs;
					mbrlen(NULL, 0, &mbs);
					size_t retval = 0;
					const wchar_t *ptr[1];
					ptr[0] = &texttoparse[ichar + a_wcslen(SIZESINGLE)];
					w_wcsrtombs(&retval, &tmpsizebuffer[0], (size_t)MAXTEXTSIZE+1, ptr, sizesize, &mbs);
					size = strtof(tmpsizebuffer, NULL);
				}

				ichar += a_wcslen(SIZESINGLE) + sizesize + 1;
			}
			else if (wcsncmp(&texttoparse[ichar], SIZESTART, a_wcslen(SIZESTART)) == 0) {//is it the start of a textsize markup?
																						 //read the size
				size_t endpos = ichar + a_wcslen(SIZESTART);
				size_t sizesize = 0;
				wbool oksize = wfalse;
				while (wcsncmp(&texttoparse[endpos + sizesize], CLOSECHAR, 1) != 0 || sizesize > MAXTEXTSIZE) { //lookup closing char, size can't be more than MAXTEXTSIZE char
					sizesize++;
				}

				oksize = (wbool)(wcsncmp(&texttoparse[endpos + sizesize], CLOSECHAR, 1) == 0);

				if (oksize) {
					mbstate_t mbs;
					mbrlen(NULL, 0, &mbs);
					size_t retval = 0;
					const wchar_t *ptr[1];
					ptr[0] = &texttoparse[ichar + a_wcslen(SIZESTART)];
					w_wcsrtombs(&retval, &tmpsizebuffer[0], (size_t)MAXTEXTSIZE + 1, ptr, sizesize, &mbs);
					outsizebuffer[realposchar] = strtof(tmpsizebuffer, NULL);
					currentsize = &outsizebuffer[realposchar];
				}

				ichar += a_wcslen(SIZESTART) + sizesize + 1;
			}
			else if (wcsncmp(&texttoparse[ichar], SIZEEND, a_wcslen(SIZEEND)) == 0) {//is it the end of the textsize markup?
				//reset the current size
				currentsize = NULL;
				ichar += a_wcslen(SIZEEND);
			}
		}

		if (ichar >= buffersize) {
			logprint("Position in original buffer is bigger than buffer size ! can't finish text parse...");
			return;
		}

		//the opening base char is still under the cursor, we have another thing to parse, go back at the beginning of the loop
		if (wcsncmp(&texttoparse[ichar], OPENCHAR, 1) == 0) {
			ichar--;
			continue;
		}

		//if we are not inside a color markup, use the base color
		if (currentcolor == NULL) {
			outcolorbuffer[realposchar] = color;
		}
		else {//else, use the value set in the color markup
			outcolorbuffer[realposchar] = (*currentcolor);
		}

		if (currentsize == NULL) {
			outsizebuffer[realposchar] = size;
		}
		else {
			outsizebuffer[realposchar] = (*currentsize);
		}

		outtextbuffer[realposchar] = texttoparse[ichar];
			
		realposchar++;

		if (realposchar >= buffersize) {
			logprint("Position in out buffer string is bigger thant buffer size ! can't finish text parse...");
			return;
		}
		
	}

}