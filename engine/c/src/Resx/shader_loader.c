#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <math.h>

#include <Render.h>

#include <Engine.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/geom.h>
#include <Base/Color.h>

#include "Debug/Logprint.h"

#include <Render/render_manager.h>
#include <GameObjects/Shaders.pb-c.h>
#include <Resx/shader_loader.h>


wbool load_shader(shader_info* const shader,unsigned char* shader_data,size_t shader_data_size,const char* shader_name,render_manager_t* const render_mngr,wbool is_fragment_shader)
{
	shader->is_fragment_shader = is_fragment_shader;
	w_strcpy(shader->shader_name,256,shader_name);

	shader->shader_pointer =  render_load_shader(render_mngr,shader->shader_name,(char*)shader_data,shader_data_size,shader->is_fragment_shader,shader->shader_info);

	if(shader->shader_pointer == 0)
		return wfalse;
	else
		return wtrue;
}

void reload_shader(shader_info* const shader,unsigned char* shader_data,size_t shader_data_size,const char* shader_name,render_manager_t* const render_mngr)
{
	render_reload_shader(render_mngr,shader_name,shader->shader_pointer,(char*)shader_data);
}