#include <sqlite3.h>
#include <inttypes.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>
#include <math.h>
#include <locale.h>

#if defined(_WIN32)
	#include <Windows.h>
#endif

#include <Engine.h>

#include <Base/types.h>
#include <Base/functions.h>

#include <Debug/Logprint.h>
#include <Resx/localization.h>


void localization_init(localization_t* const locale,const char* db_path)
{
    setlocale(LC_ALL, "");
    
	int sqlite_error = 0;

	w_strcpy(locale->current_locale,256,"default");

	//check file exist before opening it with sqlite, sqlite create automatically a file otherwise
    FILE* file = NULL;

	w_fopen(file,db_path,"r");
       
	if(file != NULL)
	{
		fclose(file);

		#if defined(_WIN32)
			wchar_t tmp_path[260];
			MultiByteToWideChar(CP_THREAD_ACP,0,db_path,260,tmp_path,260);

			sqlite_error = sqlite3_open16(tmp_path,&locale->locale_db);
		#else
			sqlite_error = sqlite3_open(db_path,&locale->locale_db);
		#endif

		if(sqlite_error)
		{
			logprint("Error loading localization database! :%s",sqlite3_errmsg(locale->locale_db));
			sqlite3_close(locale->locale_db);
			return;
		}
	}
	else
	{
		logprint("No locale.db file! %s",db_path);
		locale->locale_db = NULL;
	}

}

void localization_setlocale(localization_t* const locale, const char* new_locale)
{
	memset(locale->current_locale,0,sizeof(locale->current_locale));
	w_strcpy(locale->current_locale,256,new_locale);

	#ifndef ANDROID_BUILD
		setlocale(LC_NUMERIC,"C");
	#endif
}

wchar_t* localization_appendstring(localization_t* const locale,wchar_t* current_str_id,const char* str_id_toappend)
{
	size_t old_len = a_wcslen(current_str_id);

	if(!locale->locale_db)
	{
		size_t new_len = (old_len * sizeof(wchar_t)) + ( strlen(str_id_toappend) * sizeof(wchar_t))  + sizeof(wchar_t);
		current_str_id = (wchar_t*)wf_realloc(current_str_id,new_len);
		wchar_t* append_start = &current_str_id[old_len];
		size_t content_len = strlen(str_id_toappend) + 1;
		size_t converted_len = 0;
		a_mbstowcs(&converted_len,append_start, content_len,str_id_toappend,content_len);
		return current_str_id;
	}

	char select_string[2048];
	int sqlite_error = 0;

	w_sprintf(select_string,2048,"SELECT content FROM Traduction WHERE id = '%s' AND locale = '%s';",str_id_toappend,locale->current_locale);

	sqlite3_stmt* statement;

	sqlite_error = sqlite3_prepare_v2(locale->locale_db,select_string,-1,&statement,NULL);

	if(sqlite_error)
	{
		logprint("Can't prepare statement %s, sqlite error %s",select_string,sqlite3_errmsg(locale->locale_db));
		return NULL;
	}

	sqlite_error = sqlite3_step(statement);

	if(sqlite_error != SQLITE_ROW && sqlite_error != SQLITE_DONE)
	{
		logprint("Can't execute statement, sqlite error %s",sqlite3_errmsg(locale->locale_db));
		sqlite3_finalize(statement);
		return NULL;
	}

	int32_t len = sqlite3_column_bytes(statement, 0);

	if(len <= 0)
	{
		logprint("Can't find string %s for locale %s!",str_id_toappend,locale->current_locale);

		w_sprintf(select_string,2048,"SELECT content FROM Traduction WHERE id = '%s' AND locale = 'default';",str_id_toappend);

		sqlite3_finalize(statement);

		sqlite3_prepare_v2(locale->locale_db,str_id_toappend,-1,&statement,NULL);

		sqlite3_step(statement);

		len = sqlite3_column_bytes(statement, 0);

		if(len <= 0)
		{
			logprint("Can't localize string %s not found in database",str_id_toappend);

			sqlite3_finalize(statement);

			current_str_id = (wchar_t*)wf_realloc(current_str_id,(a_wcslen(current_str_id) * sizeof(wchar_t)) + (sizeof(wchar_t) * strlen(str_id_toappend))  + sizeof(wchar_t));
			wchar_t* append_start = &current_str_id[old_len];
			size_t content_len = strlen(str_id_toappend) + 1;
			size_t converted_len = 0;
			a_mbstowcs(&converted_len,append_start,content_len,str_id_toappend,content_len);
			return current_str_id;
		}
	}

        
        #if defined(_WIN32)
            const wchar_t* db_string = (const wchar_t*)sqlite3_column_text16(statement,0);
			size_t wlen = a_wcslen(db_string) + 1;
            current_str_id = (wchar_t*)wf_realloc(current_str_id, (a_wcslen(current_str_id) * sizeof(wchar_t))  + (wlen * sizeof(wchar_t)));
			wchar_t* append_start = &current_str_id[old_len];
            w_wcscpy(append_start, wlen,db_string);
        #else
            //convert utf8 value to wchar_t

			const unsigned char* db_string = (const unsigned char*)sqlite3_column_text(statement,0);

			current_str_id = (wchar_t*)wf_realloc(current_str_id, (a_wcslen(current_str_id) * sizeof(wchar_t)) + ((len * sizeof(wchar_t))  + sizeof(wchar_t)));
			wchar_t* append_start = &current_str_id[old_len];
			utf8_to_wchar(db_string,len + 1,append_start,(len * sizeof(wchar_t))  + sizeof(wchar_t),0);

           /* const uint16_t* db_string = (const uint16_t*)
            
            wchar_t* local_string = (wchar_t*)wf_malloc((len * sizeof(wchar_t))  + sizeof(wchar_t));
            
            for(int i = 0;i < len;i++)
            {
                local_string[i] = db_string[i];
            }    
            
            local_string[len] = '\0';*/
            
        #endif

	sqlite3_finalize(statement);

	return current_str_id;
}

wbool localization_isstringlocalized(localization_t* const locale,const char* string_id)
{
	if(!locale->locale_db)
		return wfalse;

	char select_string[2048];
	int sqlite_error = 0;

	w_sprintf(select_string,2048,"SELECT count(*) FROM Traduction WHERE id = '%s' AND locale = '%s';",string_id,locale->current_locale);

	sqlite3_stmt* statement;

	sqlite_error = sqlite3_prepare_v2(locale->locale_db,select_string,-1,&statement,NULL);

	if(sqlite_error)
	{
		logprint("Can't prepare statement %s, sqlite error %s",select_string,sqlite3_errmsg(locale->locale_db));
		return wfalse;
	}

	sqlite_error = sqlite3_step(statement);

	if(sqlite_error != SQLITE_ROW && sqlite_error != SQLITE_DONE)
	{
		logprint("Can't execute statement, sqlite error %s",sqlite3_errmsg(locale->locale_db));
		sqlite3_finalize(statement);
		return wfalse;
	}

	int result = sqlite3_column_int(statement,0);

	sqlite3_finalize(statement);

	return (wbool)(result > 0);
}

wchar_t* localization_getstring(localization_t* const locale,const char* string_id)
{
	if(!locale->locale_db)
	{
		wchar_t* local_string = (wchar_t*)wf_malloc(sizeof(wchar_t) * (strlen(string_id)+1));
		size_t content_len = strlen(string_id) + 1;
		size_t converted_len = 0;
		a_mbstowcs(&converted_len,local_string,content_len,string_id,content_len);
		return local_string;
	}

	char select_string[2048];
	int sqlite_error = 0;

	w_sprintf(select_string,2048,"SELECT content FROM Traduction WHERE id = '%s' AND locale = '%s';",string_id,locale->current_locale);

	sqlite3_stmt* statement;

	sqlite_error = sqlite3_prepare_v2(locale->locale_db,select_string,-1,&statement,NULL);

	if(sqlite_error)
	{
		logprint("Can't prepare statement %s, sqlite error %s",select_string,sqlite3_errmsg(locale->locale_db));
		return NULL;
	}

	sqlite_error = sqlite3_step(statement);

	if(sqlite_error != SQLITE_ROW && sqlite_error != SQLITE_DONE)
	{
		logprint("Can't execute statement, sqlite error %s",sqlite3_errmsg(locale->locale_db));
		sqlite3_finalize(statement);
		return NULL;
	}

	int32_t len = sqlite3_column_bytes(statement, 0);

	if(len <= 0)
	{
		logprint("Can't find string %s for locale %s!",string_id,locale->current_locale);

		w_sprintf(select_string,2048,"SELECT content FROM Traduction WHERE id = '%s' AND locale = 'default';",string_id);

		sqlite3_finalize(statement);

		sqlite3_prepare_v2(locale->locale_db,select_string,-1,&statement,NULL);

		sqlite3_step(statement);

		len = sqlite3_column_bytes(statement, 0);

		if(len <= 0)
		{
			logprint("Can't localize string %s not found in database",string_id);

			sqlite3_finalize(statement);

			wchar_t* local_string = (wchar_t*)wf_malloc(sizeof(wchar_t) * (strlen(string_id)+1));
			size_t content_len = strlen(string_id) + 1;
			size_t converted_len = 0;
			a_mbstowcs(&converted_len,local_string,content_len,string_id,content_len);
			return local_string;
		}
	}

        
        #if defined(_WIN32)
            const wchar_t* db_string = (const wchar_t*)sqlite3_column_text16(statement,0);
			size_t stringlen = a_wcslen(db_string) + 1;
			size_t buffer_len = (stringlen) * sizeof(wchar_t);
            wchar_t* local_string = (wchar_t*)wf_malloc(buffer_len);
			memset(local_string, 0, buffer_len);
            w_wcscpy(local_string, stringlen,db_string);
        #else
            //convert utf8 value to wchar_t

			const unsigned char* db_string = (const unsigned char*)sqlite3_column_text(statement,0);

			wchar_t* local_string = (wchar_t*)wf_malloc((len * sizeof(wchar_t))  + sizeof(wchar_t));

			utf8_to_wchar(db_string,len + 1,local_string,(len * sizeof(wchar_t))  + sizeof(wchar_t),0);

           /* const uint16_t* db_string = (const uint16_t*)
            
            wchar_t* local_string = (wchar_t*)wf_malloc((len * sizeof(wchar_t))  + sizeof(wchar_t));
            
            for(int i = 0;i < len;i++)
            {
                local_string[i] = db_string[i];
            }    
            
            local_string[len] = '\0';*/
            
        #endif

	sqlite3_finalize(statement);

	return local_string;
}

void localization_free(localization_t* const locale)
{
	if(locale->locale_db)
		sqlite3_close(locale->locale_db);
}