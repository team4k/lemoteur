#if defined WITH_NETWORK_CLIENT

#if defined _WIN32
	#include <winsock2.h>
	#include <ws2def.h>
	#include <WS2tcpip.h>
#else
	#include <sys/socket.h>
	#include <netinet/in.h>
	#include <netdb.h>
	#include <arpa/inet.h>
	#include <netinet/tcp.h>
	#include <sys/select.h>
	#include <fcntl.h>
	#include <unistd.h>
	#define SOCKET_ERROR -1
#endif

#include <Engine.h>

#include <stdio.h>
#include <inttypes.h>
#include <string.h>
#include <sys/types.h>
#include <stdlib.h>

#include <Debug/Logprint.h>

#include <Network/client.h>

#if defined EMSCRIPTEN
	#include <emscripten.h>

static void _emcc_message_callback(int fd, void* userData)
{
	//logprint("socket message %s callback", userData);  
}
	
static void _emcc_open_callback(int fd, void* userData) 
{
  //logprint("socket open %s callback", userData);  
}

static void _emcc_error_callback(int fd, int err, const char* msg, void* userData) 
{
  int error;
  socklen_t len = sizeof(error);

  int ret = getsockopt(fd, SOL_SOCKET, SO_ERROR, &error, &len);
  logprint("%s callback", userData);
  logprint("error message: %s", msg);

  /*if (err == error) 
  {
	finish(EXIT_SUCCESS);
  } 
  else 
  {
	finish(EXIT_FAILURE);
  }*/
}
#endif

int client_connect(client_t* const client,char* ipaddress, unsigned short port)
{
	//put declaration before the WSAStartup, after give us weird compilation errors
	struct sockaddr_in sock_addr;
	char i = 1;

	#if defined _WIN32
		WSAStartup(MAKEWORD(2,0), &client->WSAData);
	#endif


	memset(&sock_addr,0,sizeof(sock_addr));
	sock_addr.sin_family = AF_INET;
	inet_pton(sock_addr.sin_family, ipaddress, &sock_addr.sin_addr.s_addr);
	sock_addr.sin_port = htons(port);
	client->srv_addr = sock_addr;

	client->sock_handle = socket(AF_INET, SOCK_STREAM,IPPROTO_TCP);

	
	setsockopt(client->sock_handle, IPPROTO_TCP, TCP_NODELAY, &i, sizeof(i));


	if(client->sock_handle == -1)
		logprint("Socket error : invalid handle");
	else
		logprint("Client socket created !");

	#if defined EMSCRIPTEN
		//emscripten only support non blocking socket call (due to browser asynchronous nature)
		fcntl(client->sock_handle, F_SETFL, O_NONBLOCK);
	    emscripten_set_socket_error_callback("error", _emcc_error_callback);
		emscripten_set_socket_open_callback("open", _emcc_open_callback);
		emscripten_set_socket_message_callback("message", _emcc_message_callback);
	#endif

	int errcode = connect(client->sock_handle, (struct sockaddr *)&client->srv_addr, sizeof(client->srv_addr));
	
	#ifndef EMSCRIPTEN
		if(errcode != 0)
		{
			logprint("error connecting..  errcode: %d",errcode);
			client_disconnect(client);
			return errcode;
		}
	#endif

	return 0;
}

size_t client_rcv(client_t* const client)
{
  int sel;
  fd_set read;
  struct timeval timeout;
 
  timeout.tv_sec = 0;
  timeout.tv_usec = 10000;
 
  FD_ZERO(&read);
  FD_SET(client->sock_handle, &read);
 
  sel = select(client->sock_handle + 1, &read, NULL, NULL, &timeout);

  if(sel == SOCKET_ERROR)
  {
    logprint("select failed");
  }
  else if(sel == 0)
  {
  }
  else
  {
	if(FD_ISSET(client->sock_handle, &read))
	{   
		size_t byte_read = 0;
		byte_read = recv(client->sock_handle,client->data_buffer,sizeof(client->data_buffer),0);
		return byte_read;
	}
  }

  return 0;
}

void client_send(client_t* const client,char* buffertosend)
{
  int sel;
  fd_set write;
  struct timeval timeout;
 
  timeout.tv_sec = 0;
  timeout.tv_usec = 10000;
 
  FD_ZERO(&write);
  FD_SET(client->sock_handle, &write);
 
  sel = select(client->sock_handle + 1, &write, NULL, NULL, &timeout);

  if(sel == SOCKET_ERROR)
  {
    logprint("select failed");
  }
  else if(sel == 0)
  {

  }
  else
  {
	if(FD_ISSET(client->sock_handle, &write))
	{   
		send(client->sock_handle,buffertosend,sizeof(buffertosend),0);
	}
  }
}

void client_disconnect(client_t* const client)
{
	#if defined _WIN32
		closesocket(client->sock_handle);
		WSACleanup();
	#else
		close(client->sock_handle);
	#endif
}

#endif