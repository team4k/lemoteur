#ifndef STRIP_EDIT_CODE

#ifdef _MSC_VER
extern "C" {
#endif
#include <lua.h>

#ifdef _MSC_VER
}
#endif



#if defined(_WIN32)
#include <windows.h>
//#include <mscoree.h>
#include <eh.h>
#include <Psapi.h>
//#pragma comment(lib, "mscoree.lib")
#pragma comment(lib,"Psapi.lib")
#endif


#include <Render.h>
#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <time.h>
#include <string.h>
#include <limits.h>
#include <math.h>
#include <sqlite3.h>




#include <Engine.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>
#include <Base/geom.h>

#include <Render/render_manager.h>


#include <Debug/geomdebug.h>

#include <Graphics/TextureLoader.h>
#include <Resx/shader_loader.h>

#include <Utils/hash.h>
#include <Utils/hashmap.h>

#include <Threading/thread_func.h>
#include <Pathfinding/pathfind_worker.h>

#include <GameObjects/Characters.pb-c.h>
#include <Graphics/Animation.h>
#include <Graphics/Sprite.h>
#include <Graphics/Mesh.h>
#include <Graphics/Light.h>
#include <Graphics/LightManager.h>

#include <Graphics/TextureRegion.h>
#include <Graphics/SpriteBatch.h>

#include "GameObjects/Level.pb-c.h"

#include <portaudio.h>
#include <Sound/Sound.h>

#include <Graphics/FontLoader.h>

#include <Resx/Resources.h>
#include <Resx/localization.h>
#include <GameMngr/save_mngr.h>

#include "Debug/LogPrint.h"
#include "Graphics/TilemapLayer.h"
#include "GameObjects/Tilemap.h"

#include <Sound/snd_mngr.h>

#include <Font/RenderText.h>

#include <chipmunk/chipmunk.h>


#include <Debug/ChipmunkDebugDraw.h>

#include <Physics/Physics.h>
#include <Physics/physics_tilemap_utils.h>


#include <GameObjects/entity.h>
#include <GameObjects/Trigger.h>
#include <GameObjects/text_entity.h>

#include <GameObjects/entity_group.h>

#include <Graphics/Effects/particle_object.h>
#include <GameObjects/particles_entity.h>


#include <Config/config.h>
#include <Config/shader_config.h>
#include <GameMngr/input_mngr.h>
#include <game_functions.h>
#include <GameMngr/module_mngr.h>


#include <GameObjects/map.h>

#include <GameObjects/Collider.h>
#include <GameObjects/waypoint.h>

#include <GameObjects/Levelobject.h>


#include <Scripting/ScriptEngine.h>

#include <Graphics/Effects/screen_fill.h>
#include <Graphics/Effects/screen_fade.h>

#include <Editor/editor_selector.h>

#include <Editor/Editor.h>
#include <Editor/editor_game.h>
#include <Editor/editor_entities.h>

static editor_t* editor_ptr = NULL;

void editor_game_setptr(editor_t* const ptr)
{
	editor_ptr = ptr;
}

void editor_clean_level()
{
	snd_mngr_stopallsounds(&editor_ptr->snd_mngr,wfalse);
	editor_ptr->editor_module_mngr->game_cleanup(wfalse);
	Levelobject_clean_level(&editor_ptr->editor_level,&editor_ptr->resx_mngr,&editor_ptr->phy_mgr,editor_ptr->lua_state);
	script_callgc(editor_ptr->lua_state);

	wf_free(editor_ptr->sound_source_sprite);
	wf_free(editor_ptr->light_sprite);
	wf_free(editor_ptr->particle_sprite);

	editor_ptr->sound_source_sprite = editor_ptr->light_sprite = editor_ptr->particle_sprite = NULL;

	//recreate selector
	editor_selector_free(&editor_ptr->selector);
	editor_selector_init(&editor_ptr->selector);

	if(editor_ptr->backup_level != NULL)
	{
		level__free_unpacked(editor_ptr->backup_level,NULL);
		editor_ptr->backup_level = NULL;
	}
}

void editor_update_level_content(unsigned char* level_content,size_t level_size)
{
	if(editor_ptr->backup_level != NULL)
	{
		level__free_unpacked(editor_ptr->backup_level,NULL);
		editor_ptr->backup_level = NULL;
	}

	editor_ptr->backup_level = level__unpack(NULL,level_size,level_content);
}

void editor_level_loaded()
{
	editor_ptr->editor_module_mngr->game_level_loaded();
}

uint32_t editor_get_script_id()
{
	return editor_ptr->editor_level.script_id;
}

void editor_release_gamedll()
{
	editor_ptr->gdll_playerpos = editor_ptr->editor_module_mngr->game_get_playerpos(0);
	editor_ptr->editor_module_mngr->game_cleanup(wtrue);
	editor_ptr->editor_module_mngr->game_remove_physics(&editor_ptr->phy_mgr);
	editor_ptr->editor_module_mngr->game_g_cleanup();
	editor_ptr->editor_module_mngr->game_free();
	module_mngr_release(editor_ptr->editor_module_mngr);
}

wbool editor_load_gamedll(const char* game_dll_path)
{
	module_mngr_init(editor_ptr->editor_module_mngr,game_dll_path);

	//check if the gamedll is compiled against the correct version of the engine
	if (strcmp(editor_ptr->editor_module_mngr->game_getengineversion(), getbuildversion()) == 0 || strcmp(editor_ptr->editor_module_mngr->game_getengineversion(),"HERECOMEENGINEVERSION") == 0)
	{
		if (strcmp(editor_ptr->editor_module_mngr->game_getengineversion(), "HERECOMEENGINEVERSION") == 0)
		{
			logprint("WARNING : the game module doesn't have a defined engine version, will load anyway but it can cause crash and bad behavior");
		}

		editor_ptr->editor_module_mngr->game_init_objects(&editor_ptr->resx_mngr, (int)editor_ptr->gdll_playerpos.x, (int)editor_ptr->gdll_playerpos.y, &editor_ptr->phy_mgr, &editor_ptr->editor_lvl_base, &editor_ptr->gdll_font[0], &editor_ptr->config, &editor_ptr->render_mngr, &editor_ptr->editor_localobj, editor_ptr->lua_state, &editor_ptr->snd_mngr,&editor_ptr->input_mngr);
		editor_ptr->editor_module_mngr->game_init_physics(&editor_ptr->phy_mgr);
		editor_ptr->editor_module_mngr->game_init_scripts(editor_ptr->lua_state);
		return wtrue;
	}
	else
	{
		logprint("Error while loading the game dll !  engine version mismatch : game dll version : %s engine version : %s. Hint : The tool 'reader.exe' must be run on the Gamemain.c module source file before game module compilation, don't forget to add it in your module build toolchain", editor_ptr->editor_module_mngr->game_getengineversion(), getbuildversion());
		return wfalse;
	}
}

void editor_focus_onplayer()
{
	cpVect player_pos = editor_ptr->editor_module_mngr->game_get_playerpos(0);

	editor_ptr->editor_position.x = (float)player_pos.x;
	editor_ptr->editor_position.y = (float)player_pos.y;
}


void editor_setplayer_pos(float x,float y)
{
	editor_ptr->editor_module_mngr->game_player_reposition(cpv(x,y));
}

 //************************//
 //level functions    //
//************************//
void editor_setlevel_name(char* level_name)
{
	w_strcpy(editor_ptr->editor_level.current_level,256,level_name);
}



  //************************//
 //map switch functions    //
//************************//

void editor_switchmap(char* map_id)
{
	Levelobject_switchmap(&editor_ptr->editor_level,&editor_ptr->phy_mgr, editor_ptr->lua_state,map_id);
}

void editor_changemap_id(char* old_map,char* new_map)
{
	size_t map_id_size = (strlen(new_map) + 1);
	char* map_id = (char*)wf_malloc(map_id_size * sizeof(char));
	w_strcpy(map_id,map_id_size,new_map);

	Levelobject_changemapid(&editor_ptr->editor_level,old_map,map_id);
	editor_selector_changemapid(&editor_ptr->selector,old_map,map_id);
}


void editor_remove_map(char* map_id)
{
	int map_indx = hashtable_index(&editor_ptr->editor_level.map_hash,map_id);

	editor_clean_tilemap(&editor_ptr->editor_level.maps[map_indx].tilemap);

	Levelobject_removemap(&editor_ptr->editor_level,map_id);
}

 //************************//
 //play functions          //
//************************//

void editor_play_move(float mX,float mY)
{
	editor_ptr->editor_module_mngr->game_move(mX,mY);
}

void editor_game_input(int16_t input_event,wbool pressed)
{
	editor_ptr->editor_module_mngr->game_input(input_event,pressed,wtrue);
}

void editor_game_char(uint32_t codepoint)
{
	editor_ptr->editor_module_mngr->game_char_input(codepoint);
}

void editor_click(float targetX,float targetY,int button_id)
{
	editor_ptr->editor_module_mngr->game_target(targetX,targetY,wfalse,button_id);
}

void editor_endclick(float targetX,float targetY,int button_id)
{
	if(editor_ptr->play_mode)
		editor_ptr->editor_module_mngr->game_endtarget(targetX,targetY,wfalse,button_id);
	else
	{
		if(button_id == 0)
		{
			editor_ptr->selector.start_select.x = editor_ptr->selector.end_select.x = targetX;
			editor_ptr->selector.start_select.y = editor_ptr->selector.end_select.y = targetY;
		}
	}
}

void editor_wheelpinch(float delta)
{
	if(editor_ptr->play_mode)
		editor_ptr->editor_module_mngr->game_wheelpinch(delta,wtrue);
}

void editor_get_playerposition(int32_t* array_pos)
{
	cpVect player_pos = editor_ptr->editor_module_mngr->game_get_playerpos(0);
	array_pos[0] = (int32_t)player_pos.x;
	array_pos[1] = (int32_t)player_pos.y;
}

wbool editor_has_player()
{
	return editor_ptr->editor_module_mngr->game_has_player(0);
}

wbool editor_hadlevelpending()
{
	return editor_ptr->editor_level.request_load_level;
}

wbool editor_levelpendingisbinary()
{
	return (wbool)(editor_ptr->editor_level.request_load_level && editor_ptr->editor_level.request_load_binary);
}

void editor_getlevelpending(char* out_level_name)
{
	w_strcpy(out_level_name,256,editor_ptr->editor_level.pending_level);
}

size_t editor_getlevelpending_size()
{
	if(editor_levelpendingisbinary())
	{
		return level__get_packed_size(editor_ptr->editor_level.pending_binary);
	}

	return 0;
}

void editor_getlevelpendingbinary(unsigned char* out_level_data)
{
	level__pack(editor_ptr->editor_level.pending_binary,out_level_data);
}

void editor_clearlevelpending()
{
	editor_ptr->editor_level.request_load_level = wfalse;
	editor_ptr->editor_level.request_load_binary = wfalse;
	editor_ptr->editor_level.pending_binary = NULL;
}


  //************************//
 // collisions functions   //
//************************//
void editor_getcollisions_array(int32_t* arrayCopy,char* map_id)
{
	if(!hashtable_haskey(&editor_ptr->editor_level.map_hash,map_id))
	{
		logprint("No map with id %s!",map_id);
		return;
	}

	int map_indx = hashtable_index(&editor_ptr->editor_level.map_hash,map_id);

	if(editor_ptr->editor_level.maps[map_indx].tilemap.collisions_array == NULL)
		return;


	for(int32_t i = 0; i < (editor_ptr->editor_level.maps[map_indx].tilemap.col_count * editor_ptr->editor_level.maps[map_indx].tilemap.row_count); i++)
    {
		arrayCopy[i] = editor_ptr->editor_level.maps[map_indx].tilemap.collisions_array[i];
    }
}

void editor_getcollision(int32_t* tileid, int32_t tilepos)
{
	if(editor_ptr->editor_level.current_map->tilemap.collisions_array == NULL)
		return;

	(*tileid) = editor_ptr->editor_level.current_map->tilemap.collisions_array[tilepos];
}

void editor_change_collision(int32_t tileid, int32_t tilepos)
{
	Tilemap_update_edge(&editor_ptr->editor_level.current_map->tilemap,tilepos,(wbool)(tileid != NO_COLLISION),editor_ptr->phy_mgr.collisions_array);
	
	Tilemap_editCollision(&editor_ptr->editor_level.current_map->tilemap,tileid,tilepos,&editor_ptr->render_mngr);

	float half_size = editor_ptr->editor_level.current_map->tilemap.tile_size * 0.5f;

	int32_t tRow = (int32_t)floorf((float)(tilepos / editor_ptr->editor_level.current_map->tilemap.col_count));//ligne Y
	int32_t tCol = tilepos - (editor_ptr->editor_level.current_map->tilemap.col_count * tRow);//ligne X

	int32_t x_tile = tCol * editor_ptr->editor_level.current_map->tilemap.tile_size;
	int32_t y_tile = tRow * editor_ptr->editor_level.current_map->tilemap.tile_size;

	Circle_t col_circle = {(float)editor_ptr->editor_level.current_map->tilemap.tile_size,{(wfloat)(x_tile + half_size),(wfloat)(y_tile + half_size)}};//bigger radius than the tile size * 0.5 to avoid corner miss

	light_manager_regenlight(&editor_ptr->light_mngr,col_circle);

	editor_ptr->phy_mgr.collisions_array[tilepos] = tileid;


}

void editor_change_collision_color(int32_t tileid, int32_t tilepos, float r, float g, float b, float a)
{
	ColorA_t color = { r, g, b, a };

	Tilemap_editCollision_Colored(&editor_ptr->editor_level.current_map->tilemap, tileid, tilepos, color, &editor_ptr->render_mngr);
}



void editor_update_collisions_data(int32_t* arrayFeed)
{
	if(editor_ptr->editor_level.current_map->tilemap.collisions_array == NULL)
		return;

    int i = 0;

    for(i = 0; i < (editor_ptr->editor_level.current_map->tilemap.col_count * editor_ptr->editor_level.current_map->tilemap.row_count); i++)
    {
		editor_ptr->editor_level.current_map->tilemap.collisions_array[i] = arrayFeed[i];
    }

	Tilemap_gen_edge_list(&editor_ptr->editor_level.current_map->tilemap,wtrue, editor_ptr->editor_level.check_col_func);

	light_manager_regenall(&editor_ptr->light_mngr);
}

void editor_removecollisions()
{
	if(editor_ptr->editor_level.current_map->tilemap.collisions_array == NULL)
		return;

	for(int32_t i = 0; i < (editor_ptr->editor_level.current_map->tilemap.col_count * editor_ptr->editor_level.current_map->tilemap.row_count); i++)
		editor_ptr->editor_level.current_map->tilemap.collisions_array[i] = 0;
}


void editor_changecollisionsvisibility(wbool visible)
{
	if(editor_ptr->editor_level.current_map == NULL || editor_ptr->editor_level.current_map->tilemap.collisions_array == NULL)
		return;

	Tilemap_editor_changecollisionsvisibility(&editor_ptr->editor_level.current_map->tilemap,visible);
}


void editor_changecollisions_color(float a)
{
	if(editor_ptr->editor_level.current_map == NULL || editor_ptr->editor_level.current_map->tilemap.collisions_array == NULL)
		return;

	Tilemap_editor_changecollisions_color(&editor_ptr->editor_level.current_map->tilemap,a,&editor_ptr->render_mngr);
}

void editor_changecollisions_color_forvalue(int32_t value, float r, float g, float b, float a)
{
	if (editor_ptr->editor_level.current_map == NULL || editor_ptr->editor_level.current_map->tilemap.collisions_array == NULL)
		return;

	ColorA_t color = { 0 };
	color.r = r;
	color.g = g;
	color.b = b;
	color.a = a;

	Tilemap_editor_changecollisionscolor_forrange(&editor_ptr->editor_level.current_map->tilemap, color,value,value, &editor_ptr->render_mngr);
}

void editor_changecollisions_color_forrange(int32_t start_value,int32_t end_value, float r, float g, float b, float a)
{
	if (editor_ptr->editor_level.current_map == NULL || editor_ptr->editor_level.current_map->tilemap.collisions_array == NULL)
		return;

	ColorA_t color = { 0 };
	color.r = r;
	color.g = g;
	color.b = b;
	color.a = a;

	Tilemap_editor_changecollisionscolor_forrange(&editor_ptr->editor_level.current_map->tilemap, color, start_value, end_value, &editor_ptr->render_mngr);
}

void editor_update_collisions()
{
	if(editor_ptr->editor_level.current_map != NULL && editor_ptr->editor_level.current_map->use_physics && editor_ptr->editor_level.current_map->tilemap.collisions_array != NULL)
	{
		if(editor_ptr->phy_mgr.static_shapes)
			physics_free_tilemap_collisions(&editor_ptr->phy_mgr,wfalse);

		int32_t* destructible_data = NULL;
		
		if (editor_ptr->editor_level.current_map->tilemap.layer_destructible != NULL)
			destructible_data = editor_ptr->editor_level.current_map->tilemap.layer_destructible->tileArray;



		build_tilemap_collisions(&editor_ptr->phy_mgr, destructible_data,editor_ptr->editor_level.current_map->tilemap.collisions_array,editor_ptr->editor_level.current_map->tilemap.col_count,editor_ptr->editor_level.current_map->tilemap.row_count,editor_ptr->editor_level.current_map->tilemap.tile_size,worldrectzero,1,1, editor_ptr->editor_level.check_col_func, editor_ptr->editor_level.check_col_func_editable);

		if (!editor_ptr->path_worker.initialized)
		{
			//init pathfind worker
			pathfind_init(&editor_ptr->path_worker, 10, editor_ptr->phy_mgr.collisions_array, editor_ptr->phy_mgr.col_count, editor_ptr->phy_mgr.row_count);
		}
		else
		{
			pathfind_updategrid(&editor_ptr->path_worker, editor_ptr->phy_mgr.collisions_array, editor_ptr->phy_mgr.col_count, editor_ptr->phy_mgr.row_count);
		}

		Tilemap_gen_edge_list(&editor_ptr->editor_level.current_map->tilemap,wtrue, editor_ptr->editor_level.check_col_func);

		light_manager_regenall(&editor_ptr->light_mngr);

		light_manager_updatefloodfillarray(&editor_ptr->light_mngr, (editor_ptr->editor_level.current_map->tilemap.col_count * editor_ptr->editor_level.current_map->tilemap.row_count) * sizeof(int32_t));

	}
}



#endif