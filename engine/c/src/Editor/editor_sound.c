#ifndef STRIP_EDIT_CODE

#ifdef _MSC_VER
extern "C" {
#endif
#include <lua.h>

#ifdef _MSC_VER
}
#endif



#include <Render.h>
#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <time.h>
#include <string.h>
#include <limits.h>
#include <math.h>
#include <sqlite3.h>


#include <Engine.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>
#include <Base/geom.h>

#include <Render/render_manager.h>


#include <Graphics/TextureLoader.h>
#include <Resx/shader_loader.h>

#include <Utils/hash.h>
#include <Utils/hashmap.h>

#include <Threading/thread_func.h>
#include <Pathfinding/pathfind_worker.h>

#include <GameObjects/Characters.pb-c.h>
#include <Graphics/Animation.h>
#include <Graphics/Sprite.h>
#include <Graphics/Mesh.h>
#include <Graphics/Light.h>
#include <Graphics/LightManager.h>

#include <Graphics/TextureRegion.h>
#include <Graphics/SpriteBatch.h>

#include "GameObjects/Level.pb-c.h"

#include <portaudio.h>
#include <Sound/Sound.h>

#include <Graphics/FontLoader.h>

#include <Resx/Resources.h>
#include <Resx/localization.h>
#include <GameMngr/save_mngr.h>

#include "Debug/LogPrint.h"
#include "Graphics/TilemapLayer.h"
#include "GameObjects/Tilemap.h"

#include <Sound/snd_mngr.h>

#include <Font/RenderText.h>

#include <chipmunk/chipmunk.h>


#include <Debug/ChipmunkDebugDraw.h>

#include <Physics/Physics.h>


#include <GameObjects/entity.h>
#include <GameObjects/Trigger.h>
#include <GameObjects/text_entity.h>

#include <GameObjects/entity_group.h>

#include <Graphics/Effects/particle_object.h>
#include <GameObjects/particles_entity.h>


#include <Config/config.h>
#include <GameMngr/input_mngr.h>
#include <game_functions.h>
#include <GameMngr/module_mngr.h>


#include <GameObjects/map.h>

#include <GameObjects/Collider.h>
#include <GameObjects/waypoint.h>

#include <GameObjects/Levelobject.h>


#include <Graphics/Effects/screen_fill.h>
#include <Graphics/Effects/screen_fade.h>

#include <Editor/editor_selector.h>

#include <Editor/Editor.h>
#include <Editor/editor_sound.h>


static editor_t* editor_ptr = NULL;

void editor_sound_setptr(editor_t* const ptr)
{
	editor_ptr = ptr;
}

 //************************//
 // sound functions    //
//************************//
void editor_setsoundvolume(int16_t music_volume,int16_t sfx_volume)
{
	snd_mngr_setvolume(&editor_ptr->snd_mngr,music_volume,sfx_volume);
}

void editor_addsound(char* sound_id,unsigned char* sound_data,size_t sound_data_size,char* map_id,int32_t zorder)
{
	SoundSource* sound_obj = sound_source__unpack(NULL,sound_data_size,sound_data);

	Vector_t position = {(wfloat)sound_obj->position->x,(wfloat)sound_obj->position->y};

	snd_mngr_playsound(&editor_ptr->snd_mngr,getsound_resx(&editor_ptr->resx_mngr,sound_obj->snd_file_name),wtrue,sound_id,wfalse,wfalse,2,position,wtrue,sound_obj->falloffarea,map_id);

	Rect_t bound;
	
	bound.width = 32;
	bound.height = 32;
	bound.position.x = (wfloat)sound_obj->position->x - 16;
	bound.position.y = (wfloat)sound_obj->position->y + 16;

	int32_t zorder_sound = zorder;

	editor_selector_register_item(&editor_ptr->selector,sound_id,GT_SOUND,&bound,NULL,map_id,&zorder_sound);

	sound_source__free_unpacked(sound_obj,NULL);
}

void editor_updatesound(char* sound_id,unsigned char* sound_data,size_t sound_data_size)
{
	SoundSource* sound_obj = sound_source__unpack(NULL,sound_data_size,sound_data);

	Vector_t position = {(wfloat)sound_obj->position->x,(wfloat)sound_obj->position->y};

	snd_mngr_update_position(&editor_ptr->snd_mngr,sound_id,position);
	snd_mngr_update_falloff(&editor_ptr->snd_mngr,sound_id,sound_obj->falloffarea);

	item_t* sel_item = editor_selector_getitem(&editor_ptr->selector,sound_id);

	sel_item->bounds->position.x = (wfloat)sound_obj->position->x - 16;
	sel_item->bounds->position.y = (wfloat)sound_obj->position->y + 16;

	sound_source__free_unpacked(sound_obj,NULL);
}

void editor_updatesoundposition(char* sound_id,float x,float y)
{
	Vector_t position = {x,y};

	snd_mngr_update_position(&editor_ptr->snd_mngr,sound_id,position);

	item_t* sel_item = editor_selector_getitem(&editor_ptr->selector,sound_id);

	sel_item->bounds->position.x = x - 16;
	sel_item->bounds->position.y = y + 16;

}

void editor_removesound(char* sound_id)
{
	snd_mngr_removesound(&editor_ptr->snd_mngr,sound_id);

	editor_unregister_item(&editor_ptr->selector,sound_id,GT_SOUND);
}

#endif