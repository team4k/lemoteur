#ifndef STRIP_EDIT_CODE

#ifdef _MSC_VER
extern "C" {
#endif
#include <lua.h>

#ifdef _MSC_VER
}
#endif


#if defined(_WIN32)
#include <windows.h>
//#include <mscoree.h>
#include <eh.h>
#include <Psapi.h>
//#pragma comment(lib, "mscoree.lib")
#pragma comment(lib,"Psapi.lib")
#endif


#include <Render.h>
#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <time.h>
#include <string.h>
#include <limits.h>
#include <math.h>
#include <sqlite3.h>




#include <Engine.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>
#include <Base/geom.h>

#include <Render/render_manager.h>


#include <Debug/geomdebug.h>

#include <Graphics/TextureLoader.h>
#include <Resx/shader_loader.h>

#include <Utils/hash.h>
#include <Utils/hashmap.h>

#include <Threading/thread_func.h>
#include <Pathfinding/pathfind_worker.h>

#include <GameObjects/Characters.pb-c.h>
#include <Graphics/Animation.h>
#include <Graphics/Sprite.h>
#include <Graphics/Mesh.h>
#include <Graphics/Light.h>
#include <Graphics/LightManager.h>

#include <Graphics/TextureRegion.h>
#include <Graphics/SpriteBatch.h>

#include "GameObjects/Level.pb-c.h"

#include <portaudio.h>
#include <Sound/Sound.h>

#include <Graphics/FontLoader.h>

#include <Resx/Resources.h>
#include <Resx/localization.h>
#include <GameMngr/save_mngr.h>

#include "Debug/LogPrint.h"
#include "Graphics/TilemapLayer.h"
#include "GameObjects/Tilemap.h"

#include <Sound/snd_mngr.h>

#include <Font/RenderText.h>

#include <chipmunk/chipmunk.h>


#include <Debug/ChipmunkDebugDraw.h>

#include <Physics/Physics.h>
#include <Physics/physics_utils.h>


#include <GameObjects/entity.h>
#include <GameObjects/Trigger.h>
#include <GameObjects/text_entity.h>

#include <GameObjects/entity_group.h>

#include <Graphics/Effects/particle_object.h>
#include <GameObjects/particles_entity.h>


#include <Config/config.h>
#include <Config/shader_config.h>
#include <GameMngr/input_mngr.h>
#include <game_functions.h>
#include <GameMngr/module_mngr.h>

#include <GameObjects/map.h>

#include <GameObjects/Collider.h>
#include <GameObjects/waypoint.h>

#include <GameObjects/Levelobject.h>


#include <Scripting/ScriptEngine.h>

#include <Graphics/Effects/screen_fill.h>
#include <Graphics/Effects/screen_fade.h>

#include <Editor/editor_selector.h>

#include <Editor/Editor.h>
#include <Editor/editor_game.h>
#include <Editor/editor_light.h>


#include <string>
#include <vector>
#include <map>
#include <fstream>
#include <iostream>

using namespace std;

static editor_t* editor_ptr = NULL;

void editor_light_setptr(editor_t* const ptr)
{
	editor_ptr = ptr;
}

void editor_setenable_lighting(wbool enable)
{
	editor_ptr->enable_lighting = enable;
}

void editor_setlight_progs(const char* light_prog,const char* shadow_prog)
{
	if(editor_ptr->render_mngr.hashtable_shader_programs == NULL)
		return;

	if(render_has_program(&editor_ptr->render_mngr,light_prog))
		editor_ptr->light_mngr.prog_light = render_get_program(&editor_ptr->render_mngr,light_prog);
	else
		logprint("No program named %s",light_prog);

	if(render_has_program(&editor_ptr->render_mngr,shadow_prog))
		editor_ptr->light_mngr.prog_render = render_get_program(&editor_ptr->render_mngr,shadow_prog);
	else
		logprint("No program named %s",shadow_prog);
}

 //************************//
 // light functions    //
//************************//

void editor_addlight(char* light_id,unsigned char* light_data,size_t light_data_size,char* map_id,int32_t zorder)
{
	LightSource* light_obj = light_source__unpack(NULL,light_data_size,light_data);

	Vector_t position = {(wfloat)light_obj->position->x,(wfloat)light_obj->position->y};
	ColorA_t color = convertint_tocolor(light_obj->light_color->r,light_obj->light_color->g,light_obj->light_color->b,light_obj->light_color->a);


	light_t* data = light_manager_addlight(&editor_ptr->light_mngr,position,color,light_obj->radius,light_obj->light_id,map_id,(light_type)light_obj->l_type);

	light_setattenuation(data,light_obj->constant,light_obj->linear,light_obj->quadratic);
	data->power_factor = light_obj->power_factor;

	if(data->l_type == light_directional)
	{
		light_setrotationangle(data,light_obj->rotation_angle);
		light_setopenangle(data,light_obj->open_angle);
	}

	Rect_t bound;

	bound.width = 32;
	bound.height = 32;
	bound.position.x = (wfloat)(light_obj->position->x - 16);
	bound.position.y = (wfloat)(light_obj->position->y + 16);

	int32_t zorder_light = zorder;

	editor_selector_register_item(&editor_ptr->selector,light_id,GT_LIGHT,&bound,data,map_id,&zorder_light);

	light_source__free_unpacked(light_obj,NULL);
}

void editor_updatelight(char* light_id,unsigned char* light_data,size_t light_data_size)
{
	LightSource* light_obj = light_source__unpack(NULL,light_data_size,light_data);

	Vector_t position = {(wfloat)light_obj->position->x,(wfloat)light_obj->position->y};

	item_t* sel_item = editor_selector_getitem(&editor_ptr->selector,light_id);

	light_t* light =  (light_t*)sel_item->object;

	light->l_type = (light_type)light_obj->l_type;

	light_setattenuation(light,light_obj->constant,light_obj->linear,light_obj->quadratic);
	light->power_factor = light_obj->power_factor;

	if(light->l_type == light_directional)
	{
		light_setrotationangle(light,light_obj->rotation_angle);
		light_setopenangle(light,light_obj->open_angle);
	}


	light_setposition(light,position);

	light->radius = light_obj->radius;

	light->color = convertint_tocolor(light_obj->light_color->r,light_obj->light_color->g,light_obj->light_color->b,light_obj->light_color->a);

	sel_item->bounds->position.x = (wfloat)(light_obj->position->x - 16);
	sel_item->bounds->position.y = (wfloat)(light_obj->position->y + 16);

	light_source__free_unpacked(light_obj,NULL);
}

void editor_updatelightposition(char* light_id,float x,float y)
{
	Vector_t position = {x,y};

	item_t* sel_item = editor_selector_getitem(&editor_ptr->selector,light_id);

	light_setposition((light_t*)sel_item->object,position);

	sel_item->bounds->position.x = x - 16;
	sel_item->bounds->position.y = y + 16;
}

void editor_removelight(char* light_id)
{
	light_manager_removelight(&editor_ptr->light_mngr,light_id);
	editor_unregister_item(&editor_ptr->selector,light_id,GT_LIGHT);
}

#endif
