#ifndef STRIP_EDIT_CODE

#ifdef _MSC_VER
extern "C" {
#endif
#include <lua.h>

#ifdef _MSC_VER
}
#endif


#if defined(_WIN32)
#include <windows.h>
//#include <mscoree.h>
#include <eh.h>
#include <Psapi.h>
//#pragma comment(lib, "mscoree.lib")
#pragma comment(lib,"Psapi.lib")
#endif


#include <Render.h>
#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <time.h>
#include <string.h>
#include <limits.h>
#include <math.h>
#include <sqlite3.h>




#include <Engine.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>
#include <Base/geom.h>

#include <Render/render_manager.h>


#include <Debug/geomdebug.h>

#include <Graphics/TextureLoader.h>
#include <Resx/shader_loader.h>

#include <Utils/hash.h>
#include <Utils/hashmap.h>

#include <Threading/thread_func.h>
#include <Pathfinding/pathfind_worker.h>

#include <GameObjects/Characters.pb-c.h>
#include <Graphics/Animation.h>
#include <Graphics/Sprite.h>
#include <Graphics/Mesh.h>
#include <Graphics/Light.h>
#include <Graphics/LightManager.h>

#include <Graphics/TextureRegion.h>
#include <Graphics/SpriteBatch.h>

#include "GameObjects/Level.pb-c.h"

#include <portaudio.h>
#include <Sound/Sound.h>

#include <Graphics/FontLoader.h>

#include <Resx/Resources.h>
#include <Resx/localization.h>
#include <GameMngr/save_mngr.h>

#include "Debug/LogPrint.h"
#include "Graphics/TilemapLayer.h"
#include "GameObjects/Tilemap.h"

#include <Sound/snd_mngr.h>

#include <Font/RenderText.h>

#include <chipmunk/chipmunk.h>


#include <Debug/ChipmunkDebugDraw.h>

#include <Physics/Physics.h>
#include <Physics/physics_utils.h>


#include <GameObjects/entity.h>
#include <GameObjects/Trigger.h>
#include <GameObjects/text_entity.h>

#include <GameObjects/entity_group.h>

#include <Graphics/Effects/particle_object.h>
#include <GameObjects/particles_entity.h>


#include <Config/config.h>
#include <Config/shader_config.h>
#include <GameMngr/input_mngr.h>
#include <game_functions.h>
#include <GameMngr/module_mngr.h>


#include <GameObjects/map.h>

#include <GameObjects/Collider.h>
#include <GameObjects/waypoint.h>

#include <GameObjects/Levelobject.h>


#include <Scripting/ScriptEngine.h>

#include <Graphics/Effects/screen_fill.h>
#include <Graphics/Effects/screen_fade.h>

#include <Editor/editor_selector.h>

#include <Editor/Editor.h>
#include <Editor/editor_entities.h>
#include <Editor/editor_resx.h>
#include <Editor/editor_game.h>
#include <Editor/editor_render.h>

#include <string>
#include <vector>
#include <map>
#include <fstream>
#include <iostream>

using namespace std;






static editor_t* editor_ptr = NULL;

void editor_entities_setptr(editor_t* const ptr)
{
	editor_ptr = ptr;
}


static void editor_callback_trigger(const char* module,const char* func,const char* params,const char* entity_id,const char* trigger_id)
{
	char buff[260];

	if(params && strcmp(params,"") != 0)
		w_sprintf(buff,260,"package.loaded['%s'].%s(%s,'%s','%s');\n",editor_ptr->editor_level.current_module,func,params,entity_id, trigger_id);
	else
		w_sprintf(buff,260,"package.loaded['%s'].%s('%s','%s');\n",editor_ptr->editor_level.current_module,func,entity_id, trigger_id);

	const char* result = script_execstring(editor_ptr->lua_state,buff,NULL,0);


	if(result != NULL)
		w_strcpy(editor_ptr->lua_log_buffer,10000,result);
}

trigger_callback_t editor_entities_get_trigger_callback()
{
	return &editor_callback_trigger;
}



//************************//
 //edit tilemap functions  //
//************************//

void editor_create_tilemap(int32_t tileWidth,int32_t tileHeight,unsigned char* map_data,size_t map_size)
{
	//generate an empty texture with a single black tile

	Map* oMap = map__unpack(NULL,map_size,map_data);

	if(!editor_resx_exist(oMap->tileset_id))
	{
		logprint("Tilemap texture missing! %s",oMap->tileset_id);
		return;
	}


	//init the tilemap
	editor_ptr->editor_level.current_map = Levelobject_addmap(&editor_ptr->editor_level,tileWidth,gettexture_resx(&editor_ptr->resx_mngr,oMap->tileset_id),oMap,vectorzero,wtrue);

	w_strcpy(editor_ptr->editor_level.current_map_id,256,oMap->map_id);

	map__free_unpacked(oMap,NULL);
}

//update the texture used for the tile in GPU memory
void editor_Tilemap_TextureChanged()
{
	//update all tilemaps
	uint32_t map_indx = 0;
	uint32_t map_count = 0;

	while(map_count <  editor_ptr->editor_level.map_hash.num_elem)
	{
		if(&editor_ptr->editor_level.map_hash.keys[map_indx])
		{

			Tilemap_TextureChanged(&editor_ptr->editor_level.maps[map_indx].tilemap,&editor_ptr->render_mngr);
			map_count++;
		}

		map_indx++;
	}
	
}

void editor_Tilemap_resize(int32_t rowCount,int32_t colCount,int16_t row_direction,int16_t col_direction)
{
	Tilemap_resize(&editor_ptr->editor_level.current_map->tilemap,colCount,rowCount,row_direction,col_direction,&editor_ptr->render_mngr, editor_ptr->editor_level.check_col_func);

	editor_update_collisions();
}

void editor_tilemaps_updatetilesize(int32_t new_tile_size)
{
	//update all tilemaps
	uint32_t map_indx = 0;
	uint32_t map_count = 0;

	while(map_count <  editor_ptr->editor_level.map_hash.num_elem)
	{
		if(&editor_ptr->editor_level.map_hash.keys[map_indx])
		{

			Tilemap_update_tilesize(&editor_ptr->editor_level.maps[map_indx].tilemap,new_tile_size,&editor_ptr->render_mngr);
			map_count++;
		}

		map_indx++;
	}

	editor_update_collisions();
}

void editor_Tilemap_changeopacity(const char* layer_id,float a)
{
	int layer_indx = hashtable_index(&editor_ptr->editor_level.current_map->tilemap.layer_hash,layer_id);

	editor_ptr->editor_level.current_map->tilemap.layer_array[layer_indx].vertices_color.a = a;
}

void editor_clean_tilemap(Tilemap_t* tilemap_ptr)
{
	uint32_t key_i=0;
	uint32_t key_count = 0;

	//in editor mode, the hashmap "own" the keys strings
	while(key_count< tilemap_ptr->layer_hash.num_elem)
	{
		if(tilemap_ptr->layer_hash.keys[key_i])
		{
			wf_free(tilemap_ptr->layer_hash.keys[key_i]);
			wf_free(tilemap_ptr->layer_array[key_i].tileArray);
			key_count++;
		}

		key_i++;
	}

	if(tilemap_ptr->texturePointer)
	{
		render_delete_texture(&editor_ptr->render_mngr,tilemap_ptr->texturePointer->texture_pointer,wfalse);
		tilemap_ptr->texturePointer->texture_pointer = NULL;
	}

	if(tilemap_ptr->collisions_array != NULL)
	{
		wf_free(tilemap_ptr->collisions_array);
		tilemap_ptr->collisions_array = NULL;
	}
}



  //************************//
 //edit tiles functions    //
//************************//

//change a tile , tileId = the id of the tile in the texture, tilePos = the position of the tile in the tilemap
void editor_change_tile(int32_t tileId,int32_t tilePos,char* layer_id)
{
	Tilemap_editTile(&editor_ptr->editor_level.current_map->tilemap,tileId,tilePos,layer_id,&editor_ptr->render_mngr,wtrue);
}

static void editor_change_multi_tile_fromrectangle(int32_t* tiles_id,int32_t start_pos,Rect_t rectangle_selection,char* layer_id)
{
	Rect_t select_rectangle = rectangle_selection;

	int start_col = (int)(select_rectangle.position.x  / editor_ptr->editor_level.current_map->tilemap.tile_size);
	int start_row = (int)(select_rectangle.position.y / editor_ptr->editor_level.current_map->tilemap.tile_size)-1;

	if(start_row >=  editor_ptr->editor_level.current_map->tilemap.row_count)
		start_row = editor_ptr->editor_level.current_map->tilemap.row_count-1;
	else if(start_row < 0)
		start_row = 0;

	if(start_col >= editor_ptr->editor_level.current_map->tilemap.col_count)
		start_col = editor_ptr->editor_level.current_map->tilemap.col_count - 1;
	else if(start_col < 0)
		start_col = 0;

	//int start_tile = (start_row *  &editor_ptr->editor_level.current_map->tilemap.col_count) + start_col;
	int num_col = (select_rectangle.width / editor_ptr->editor_level.current_map->tilemap.tile_size) ;
	int num_row = (select_rectangle.height / editor_ptr->editor_level.current_map->tilemap.tile_size);

	if(start_col + num_col >= editor_ptr->editor_level.current_map->tilemap.col_count)
		num_col = editor_ptr->editor_level.current_map->tilemap.col_count - start_col;

	if(start_row - (num_row -1) < 0)
		num_row = start_row + 1;

	int num_tiles = num_col * num_row;

	int start_tile = start_pos + ((num_row - 1) * editor_ptr->editor_level.current_map->tilemap.col_count);

	int32_t i_num = 1;

	int32_t i_tile = start_tile;

	while(i_num <= num_tiles)
	{
		Tilemap_editTile(&editor_ptr->editor_level.current_map->tilemap,(*tiles_id++),i_tile++,layer_id,&editor_ptr->render_mngr,wtrue);

		if((i_num % num_col) == 0)
			i_tile -= (editor_ptr->editor_level.current_map->tilemap.col_count + num_col);

		i_num++;
	}
}

void editor_change_multi_tile(int32_t* tiles_id,int32_t start_pos,char* layer_id)
{
	editor_change_multi_tile_fromrectangle(tiles_id,start_pos,get_select_rectangle(),layer_id);
}

void editor_change_multi_tile2(int32_t* tiles_id,int32_t start_pos,int32_t* rect_values,char* layer_id)
{
	Rect_t rectangle_selection;
	rectangle_selection.position.x = (float)rect_values[0];
	rectangle_selection.position.y = (float)rect_values[1];
	rectangle_selection.width = rect_values[2];
	rectangle_selection.height = rect_values[3];

	editor_change_multi_tile_fromrectangle(tiles_id,start_pos,rectangle_selection,layer_id);
}

void editor_change_animated_tile(int32_t tileId,int32_t tilePos,char* layer_id,int32_t current_frame)
{
	Tilemap_edit_animated_tile(&editor_ptr->editor_level.current_map->tilemap,tileId,tilePos,layer_id,current_frame,&editor_ptr->render_mngr);
}

void editor_remove_animated_tile(int32_t tilePos,char* layer_id,int32_t current_frame)
{
	Tilemap_remove_animated_tile(&editor_ptr->editor_level.current_map->tilemap,tilePos,layer_id,current_frame,&editor_ptr->render_mngr);
}

void editor_update_tilemap(int32_t* arrayFeed,char* layer_id)
{
    int i = 0;

	int layer_indx = hashtable_index(&editor_ptr->editor_level.current_map->tilemap.layer_hash,layer_id);

    for(i = 0; i < (editor_ptr->editor_level.current_map->tilemap.col_count * editor_ptr->editor_level.current_map->tilemap.row_count); i++)
    {
		editor_ptr->editor_level.current_map->tilemap.layer_array[layer_indx].tileArray[i] = arrayFeed[i];
    }
       
}

void editor_goto_frame(int32_t frame,char* layer_id)
{
	int layer_indx = hashtable_index(&editor_ptr->editor_level.current_map->tilemap.layer_hash,layer_id);

	TilemapLayer_gotoframe(&editor_ptr->editor_level.current_map->tilemap.layer_array[layer_indx],frame,&editor_ptr->render_mngr);
}

  //************************//
 //edit layer functions    //
//************************//

void editor_addlayer(unsigned char* layer_data,size_t layer_size)
{
	Layer* layer_obj = layer__unpack(NULL,layer_size,layer_data);

	//objects layer aren't added to tilemap
	if(layer_obj->type == TYPE_LAYER__OBJECTS)
	{
		layer__free_unpacked(layer_obj,NULL);
		return;
	}

	if(layer_obj->type == TYPE_LAYER__COLLISIONS)
	{
		Tilemap_AddCollisions(&editor_ptr->editor_level.current_map->tilemap,layer_obj);
		Tilemap_gen_edge_list(&editor_ptr->editor_level.current_map->tilemap,wfalse, editor_ptr->editor_level.check_col_func);

		if(!editor_ptr->path_worker.initialized)
		{
			//init pathfind worker
			pathfind_init(&editor_ptr->path_worker,10,editor_ptr->editor_level.current_map->tilemap.collisions_array,editor_ptr->editor_level.current_map->tilemap.col_count,editor_ptr->editor_level.current_map->tilemap.row_count);
		}
		else
		{
			pathfind_updategrid(&editor_ptr->path_worker,editor_ptr->editor_level.current_map->tilemap.collisions_array,editor_ptr->editor_level.current_map->tilemap.col_count,editor_ptr->editor_level.current_map->tilemap.row_count);
		}

	}
	else
	{
		//get texture
		texture_info* layer_texture = NULL;

		if (layer_obj->tileset_id == NULL)
			layer_texture = editor_ptr->editor_level.current_map->tilemap.texturePointer;
		else
			layer_texture = gettexture_resx(&editor_ptr->resx_mngr, layer_obj->tileset_id);

		Tilemap_AddLayer(&editor_ptr->editor_level.current_map->tilemap, &editor_ptr->render_mngr, layer_obj, layer_texture, editor_ptr->editor_level.randomizer);
	}
		

	layer__free_unpacked(layer_obj,NULL);
}

void editor_update_layer(unsigned char* layer_data,size_t layer_size)
{
	Layer* layer_obj = layer__unpack(NULL,layer_size,layer_data);


	if(layer_obj->type != TYPE_LAYER__OBJECTS)
		Tilemap_update_layer(&editor_ptr->editor_level.current_map->tilemap,layer_obj,&editor_ptr->render_mngr);

	layer__free_unpacked(layer_obj,NULL);
}

void editor_update_layercontent(unsigned char* layer_data,size_t layer_size)
{
	Layer* layer_obj = layer__unpack(NULL,layer_size,layer_data);

	if(layer_obj->type != TYPE_LAYER__OBJECTS)
		Tilemap_update_layercontent(&editor_ptr->editor_level.current_map->tilemap,layer_obj,&editor_ptr->render_mngr);

	layer__free_unpacked(layer_obj,NULL);
}

void editor_removelayer(char* layer_id)
{
	if(!hashtable_haskey(&editor_ptr->editor_level.current_map->tilemap.layer_hash,layer_id))
		return;

	hashtable_index(&editor_ptr->editor_level.current_map->tilemap.layer_hash,layer_id);

	Tilemap_RemoveLayer(&editor_ptr->editor_level.current_map->tilemap,layer_id);
}

void editor_changelayer_order(char* layer_id,int32_t new_draw_order)
{
	if(!hashtable_haskey(&editor_ptr->editor_level.current_map->tilemap.layer_hash,layer_id))
		return;

	Tilemap_changelayer_order(&editor_ptr->editor_level.current_map->tilemap,layer_id,new_draw_order);
}

void editor_changelayer_visibility(wbool visible,char* layer_id)
{
	if(editor_ptr->editor_level.current_map && hashtable_haskey(&editor_ptr->editor_level.current_map->tilemap.layer_hash,layer_id))
		Tilemap_editor_changevisibility(&editor_ptr->editor_level.current_map->tilemap,layer_id,visible);
}

void editor_changelayer_id(char* old_layer,char* new_layer)
{
	if(!hashtable_haskey(&editor_ptr->editor_level.current_map->tilemap.layer_hash,old_layer))
		return;

	size_t layer_len = (strlen(new_layer) + 1);
	char* layer_id = (char*)wf_malloc(layer_len * sizeof(char));
	w_strcpy(layer_id,layer_len,new_layer);

	char* ptr = Tilemap_changelayer_id(&editor_ptr->editor_level.current_map->tilemap,old_layer,layer_id);

	wf_free(ptr);
}

void editor_regen_textcoord(char* layer_id)
{
	if(!hashtable_haskey(&editor_ptr->editor_level.current_map->tilemap.layer_hash,layer_id))
		return;

	Tilemap_regenTexCoord(&editor_ptr->editor_level.current_map->tilemap,layer_id,&editor_ptr->render_mngr);
}


void editor_setdestructible(char* layer_id,int32_t tile_destruct_id)
{
	if(!hashtable_haskey(&editor_ptr->editor_level.current_map->tilemap.layer_hash,layer_id))
		return;

	Tilemap_setdestructiblelayer(&editor_ptr->editor_level.current_map->tilemap,layer_id,tile_destruct_id);
}

void editor_cleardestructible()
{
	Tilemap_cleardestructiblelayer(&editor_ptr->editor_level.current_map->tilemap);
}

 //************************//
 //edit entity functions   //
//************************//


void editor_addentity(char* entity_id,unsigned char* entity_data,size_t entity_size,char* map_id)
{
	Entity* entity_obj = entity__unpack(NULL,entity_size,entity_data);

	size_t entity_len = (strlen(entity_id) + 1);
	char* _entity_id = (char*)wf_malloc(entity_len * sizeof(char));
	w_strcpy(_entity_id,entity_len,entity_id);

	entity_t* obj = Levelobject_addentity(&editor_ptr->editor_level,&editor_ptr->resx_mngr,&editor_ptr->phy_mgr,_entity_id,entity_obj,map_id,&editor_ptr->render_mngr,wtrue,vectorzero);

	Rect_t* rnd_box = entity_getrenderbox(obj);

	editor_selector_register_item(&editor_ptr->selector,_entity_id,GT_ENTITY,rnd_box,obj,map_id,&obj->z_order);

	entity__free_unpacked(entity_obj,NULL);
}

void editor_updateentity(char* entity_id,unsigned char* entity_data,size_t entity_size)
{
	Entity* entity_obj = entity__unpack(NULL,entity_size,entity_data);

	Levelobject_updateentity(&editor_ptr->editor_level,&editor_ptr->resx_mngr,&editor_ptr->phy_mgr,entity_id,entity_obj,&editor_ptr->render_mngr,wtrue);

	entity__free_unpacked(entity_obj,NULL);
}

void editor_addtrigger(char* entity_id,unsigned char* entity_data,size_t entity_size,char* map_id,int16_t z_order)
{
	Trigger* entity_obj = trigger__unpack(NULL,entity_size,entity_data);

	size_t entity_len = (strlen(entity_id) + 1);
	char* _entity_id = (char*)wf_malloc(entity_len * sizeof(char));
	w_strcpy(_entity_id,entity_len,entity_id);

	trigger_t* obj = Levelobject_addtrigger(&editor_ptr->editor_level,_entity_id,entity_obj,map_id,&editor_callback_trigger);

	int trigger_zorder = z_order;

	editor_selector_register_item(&editor_ptr->selector,_entity_id,GT_TRIGGER,&obj->render_box,obj,map_id,&trigger_zorder);

	trigger__free_unpacked(entity_obj,NULL);
}

void editor_updatetrigger(char* entity_id,unsigned char* entity_data,size_t entity_size)
{
	Trigger* entity_obj = trigger__unpack(NULL,entity_size,entity_data);

	Levelobject_updatetrigger(&editor_ptr->editor_level,entity_id,entity_obj,&editor_callback_trigger);

	trigger__free_unpacked(entity_obj,NULL);
}

void editor_addtextentity(char* entity_id,unsigned char* entity_data,size_t entity_size,char* map_id)
{
	TextObject* txtobj = text_object__unpack(NULL,entity_size,entity_data);

	size_t entity_len = (strlen(entity_id) + 1);
	char* _entity_id = (char*)wf_malloc(entity_len * sizeof(char));
	w_strcpy(_entity_id,entity_len,entity_id);

	text_entity_t* obj = levelobject_addtext(&editor_ptr->editor_level,&editor_ptr->editor_localobj,_entity_id,txtobj,map_id,&editor_ptr->resx_mngr,&editor_ptr->render_mngr);

	editor_selector_register_item(&editor_ptr->selector,_entity_id,GT_TEXT,&obj->bbox,obj,map_id,&obj->z_order);

	text_object__free_unpacked(txtobj,NULL);
}

void editor_updatetextentity(char* entity_id,unsigned char* entitydata,size_t entity_size,char* map_id)
{
	TextObject* txtobj = text_object__unpack(NULL,entity_size,entitydata);

	levelobject_updatetext(&editor_ptr->editor_level,&editor_ptr->editor_localobj,entity_id,txtobj,map_id,&editor_ptr->resx_mngr,&editor_ptr->render_mngr);

	text_object__free_unpacked(txtobj,NULL);
}

//************************//
// particles functions    //
//************************//
void editor_addparticles(char* particles_id, unsigned char* particles_data, size_t particles_data_size, char* map_id, int32_t zorder)
{
	ParticlesData* particles_obj = particles_data__unpack(NULL, particles_data_size, particles_data);

	Vector_t position = { (wfloat)particles_obj->position->x,(wfloat)particles_obj->position->y };

	particles_entity_t* part_ent = Levelobject_addparticlesentity(&editor_ptr->editor_level, particles_obj, particles_obj->id, &editor_ptr->render_mngr, editor_ptr->lua_state, &editor_ptr->phy_mgr, map_id);

	Rect_t bound;

	bound.width = 32;
	bound.height = 32;
	bound.position.x = (wfloat)(particles_obj->position->x - 16);
	bound.position.y = (wfloat)(particles_obj->position->y + 16);

	editor_selector_register_item(&editor_ptr->selector, particles_id, GT_PARTICLES, &bound, part_ent, map_id, &part_ent->z_order);

	particles_data__free_unpacked(particles_obj, NULL);

}

void editor_updateparticles(char* particles_id, unsigned char* particles_data, size_t particles_data_size, char* map_id)
{
	ParticlesData* particles_obj = particles_data__unpack(NULL, particles_data_size, particles_data);

	Vector_t position = { (wfloat)particles_obj->position->x,(wfloat)particles_obj->position->y };

	item_t* sel_item = editor_selector_getitem(&editor_ptr->selector, particles_id);

	Levelobject_updateparticlesentity(&editor_ptr->editor_level, particles_obj, particles_id, &editor_ptr->render_mngr, editor_ptr->lua_state, &editor_ptr->phy_mgr, map_id);

	particles_data__free_unpacked(particles_obj, NULL);
}

void editor_updateparticlesposition(char* particles_id, float x, float y)
{
	Vector_t position = { x,y };

	item_t* sel_item = editor_selector_getitem(&editor_ptr->selector, particles_id);

	particles_entity_setpos((particles_entity_t*)sel_item->object, position);

	sel_item->bounds->position.x = x - 16;
	sel_item->bounds->position.y = y + 16;
}

void editor_removeparticles(char* particles_id)
{
	Levelobject_removeentity(&editor_ptr->editor_level, &editor_ptr->phy_mgr, particles_id, GT_PARTICLES);
	editor_unregister_item(&editor_ptr->selector, particles_id, GT_PARTICLES);
}

//************************//
// waypoint functions    //
//************************//
void editor_addwaypoint(char* waypoint_id, unsigned char* waypoint_data, size_t waypoint_data_size, char* map_id,int32_t zorder)
{
	Waypoint* waypoint_obj = waypoint__unpack(NULL, waypoint_data_size, waypoint_data);

	Vector_t position = { (wfloat)waypoint_obj->position->x,(wfloat)waypoint_obj->position->y };

	waypoint_t* way = levelobject_addwaypoint(&editor_ptr->editor_level, waypoint_obj,map_id);

	Rect_t bound;

	bound.width = 32;
	bound.height = 32;
	bound.position.x = (wfloat)(waypoint_obj->position->x - 16);
	bound.position.y = (wfloat)(waypoint_obj->position->y + 16);

	int32_t zorder_way = zorder;

	editor_selector_register_item(&editor_ptr->selector, waypoint_id, GT_WAYPOINT, &bound, way, map_id, &zorder_way);

	waypoint__free_unpacked(waypoint_obj, NULL);
}

void editor_updatewaypointposition(char* waypoint_id, float x, float y)
{
	item_t* sel_item = editor_selector_getitem(&editor_ptr->selector, waypoint_id);

	waypoint_setpos((waypoint_t*)sel_item->object, x, y);

	sel_item->bounds->position.x = x - 16;
	sel_item->bounds->position.y = y + 16;
}

void editor_removewaypoint(char* waypoint_id)
{
	Levelobject_removeentity(&editor_ptr->editor_level, &editor_ptr->phy_mgr, waypoint_id, GT_WAYPOINT);
	editor_unregister_item(&editor_ptr->selector, waypoint_id, GT_WAYPOINT);
}


//************************//
// collider functions    //
//************************//
void editor_addcollider(char* collider_id, unsigned char* collider_data, size_t collider_data_size, char* map_id, int32_t zorder)
{
	Collider* collider_obj = collider__unpack(NULL, collider_data_size, collider_data);

	Vector_t position = { (wfloat)collider_obj->position->x,(wfloat)collider_obj->position->y };

	collider_t* col = levelobject_addcollider(&editor_ptr->editor_level, collider_obj, &editor_ptr->phy_mgr, map_id);

	Rect_t bound;

	bound.width = 32;
	bound.height = 32;
	bound.position.x = (wfloat)(collider_obj->position->x - 16);
	bound.position.y = (wfloat)(collider_obj->position->y + 16);

	int32_t zorder_col = zorder;

	editor_selector_register_item(&editor_ptr->selector, collider_id, GT_COLLIDER, &bound, col, map_id, &zorder_col);

	collider__free_unpacked(collider_obj, NULL);
}

void editor_updatecollider(char* collider_id, unsigned char* collider_data, size_t collider_data_size, char* map_id)
{
	Collider* col_obj = collider__unpack(NULL, collider_data_size, collider_data);

	Vector_t position = { (wfloat)col_obj->position->x,(wfloat)col_obj->position->y };

	item_t* sel_item = editor_selector_getitem(&editor_ptr->selector, collider_id);

	levelobject_updatecollider(&editor_ptr->editor_level, collider_id, col_obj, &editor_ptr->phy_mgr);

	collider__free_unpacked(col_obj, NULL);
}

void editor_updatecolliderposition(char* collider_id, float x, float y)
{
	item_t* sel_item = editor_selector_getitem(&editor_ptr->selector, collider_id);

	collider_setpos((collider_t*)sel_item->object, x, y);

	sel_item->bounds->position.x = x - 16;
	sel_item->bounds->position.y = y + 16;
}


void editor_updatecollidersize(char* collider_id, int16_t width, int16_t height)
{
	item_t* sel_item = editor_selector_getitem(&editor_ptr->selector, collider_id);

	collider_updaterenderbox((collider_t*)sel_item->object, width, height);
}

void editor_removecollider(char* collider_id)
{
	Levelobject_removeentity(&editor_ptr->editor_level, &editor_ptr->phy_mgr, collider_id, GT_COLLIDER);
	editor_unregister_item(&editor_ptr->selector, collider_id, GT_COLLIDER);
}





void editor_removeentity(char* entity_id,GAME_TYPE type)
{
	char* ptr = Levelobject_removeentity(&editor_ptr->editor_level,&editor_ptr->phy_mgr,entity_id,type);

	char* itemid = ptr;

	if (itemid == NULL)
	{
		itemid = entity_id;
	}

	editor_unregister_item(&editor_ptr->selector, itemid,type);

	wf_free(ptr);
}

void editor_changeentityid(char* old_id,char* new_id,GAME_TYPE type)
{
	size_t entity_len = (strlen(new_id) + 1);
	char* entity_id = (char*)wf_malloc(entity_len * sizeof(char));

	w_strcpy(entity_id,entity_len,new_id);


	Rect_t* new_rect;
	int32_t* new_z;
	void* new_entity;

	char* ptr = Levelobject_changeentityid(&editor_ptr->editor_level,old_id,entity_id,type,&new_rect,&new_z,&new_entity);

	editor_selector_changeid(&editor_ptr->selector,old_id,new_id,type,new_rect,new_z,new_entity);


	wf_free(ptr);
}

void editor_selectentity(char* select_name,GAME_TYPE type,wbool add_to_selection)
{
	editor_selector_selectentity_fromname(&editor_ptr->selector,(const char*)select_name,add_to_selection);
}

void editor_selectentity_pos(int posX,int posY,int16_t z_order,wbool add_to_selection)
{
	if(editor_ptr->selector.start_select.x == editor_ptr->selector.end_select.x && editor_ptr->selector.start_select.y == editor_ptr->selector.end_select.y)
	{
		editor_selector_selectentity_pos(&editor_ptr->selector,posX,posY,z_order,add_to_selection,editor_ptr->editor_level.current_map_id);
		editor_ptr->selector.start_select.x = editor_ptr->selector.end_select.x = (float)posX;
		editor_ptr->selector.end_select.y = editor_ptr->selector.start_select.y = (float)posY;
	}
	else
	{
		Rect_t select_rectangle;

		Vector_t diff = Vec_minus(editor_ptr->selector.end_select,editor_ptr->selector.start_select);

		if(diff.x > 0)
			select_rectangle.position.x = editor_ptr->selector.start_select.x;
		else
			select_rectangle.position.x = editor_ptr->selector.end_select.x;

		if(diff.y > 0)
			select_rectangle.position.y = editor_ptr->selector.end_select.y;
		else
			select_rectangle.position.y = editor_ptr->selector.start_select.y;

		select_rectangle.width = (int16_t)fabsf(diff.x);
		select_rectangle.height = (int16_t)fabsf(diff.y);

		editor_selector_multiselect(&editor_ptr->selector,select_rectangle,z_order,editor_ptr->editor_level.current_map_id);
	}
}

void editor_entity_unselect()
{
	editor_selector_unselect(&editor_ptr->selector);
}

wbool editor_has_entity_selected()
{
	return editor_selector_has_selection(&editor_ptr->selector);
}

int32_t editor_get_num_selection()
{
	if(editor_ptr->selector.selected_array!= NULL && editor_ptr->selector.num_selected_items > 0)
		return editor_ptr->selector.num_selected_items;

	return 0;
}

GAME_TYPE editor_entity_get_selection(char* buff,int32_t num_select,size_t buff_size)
{
	if(editor_ptr->selector.selected_array != NULL && editor_ptr->selector.num_selected_items > num_select)
	{
		w_strcpy(buff,buff_size,editor_ptr->selector.selected_array[num_select]->entity);

		return editor_ptr->selector.selected_array[num_select]->type;
	}

	return GT_ENTITY;
}

void editor_entity_update_pos(char* entity_id,int posX,int posY,GAME_TYPE type)
{
	Levelobject_update_pos(&editor_ptr->editor_level,entity_id,posX,posY,type);
	Levelobject_updaterenderbox(&editor_ptr->editor_level,&editor_ptr->render_mngr,entity_id,type);

	if(type == GT_LIGHT || type == GT_SOUND)
	{
		item_t* sel_item = editor_selector_getitem(&editor_ptr->selector,entity_id);
		sel_item->bounds->position.x = (wfloat)(posX - 16);
		sel_item->bounds->position.y = (wfloat)(posY + 16);
	}

}

void editor_entity_set_visible(char* entity_id,wbool visible,GAME_TYPE type)
{
	Levelobject_set_visible(&editor_ptr->editor_level,entity_id,visible,type);
}


void editor_entity_changezorder(char* entity_id, int new_zorder,GAME_TYPE type)
{
	wbool is_updated = Levelobject_updatezorder(&editor_ptr->editor_level,entity_id,new_zorder,type);

	if(!is_updated)
	{
		item_t* sel_item = editor_selector_getitem(&editor_ptr->selector,entity_id);

		(*sel_item->zorder) = new_zorder;
	}
}

void editor_entity_getrenderbox(char* entity_id,GAME_TYPE type,int32_t* render_box_size,float* render_box_pos)
{
	Rect_t* orig_render_box = Levelobject_getrenderbox(&editor_ptr->editor_level,entity_id,type);

	if(orig_render_box)
	{
		render_box_size[0] = orig_render_box->width;
		render_box_size[1] = orig_render_box->height;
		render_box_pos[0] = (float)orig_render_box->position.x;
		render_box_pos[1] = (float)orig_render_box->position.y;
	}
}

  //************************//
 //get tilemap content     //
//************************//

void editor_get_tilearray(int32_t* arrayCopy,char* layer_id,char* map_id)
{
	if(!hashtable_haskey(&editor_ptr->editor_level.map_hash,map_id))
	{
		logprint("No map with id %s!",map_id);
		return;
	}

    int i = 0;

	int map_indx = hashtable_index(&editor_ptr->editor_level.map_hash,map_id);

	if(!hashtable_haskey(&editor_ptr->editor_level.maps[map_indx].tilemap.layer_hash,layer_id))
	{
		logprint("No layer with id %s on map %s!",layer_id,map_id);
		return;
	}

	int layer_indx = hashtable_index(&editor_ptr->editor_level.maps[map_indx].tilemap.layer_hash,layer_id);

    for(i = 0; i < (editor_ptr->editor_level.maps[map_indx].tilemap.col_count * editor_ptr->editor_level.maps[map_indx].tilemap.row_count); i++)
    {
		arrayCopy[i] = editor_ptr->editor_level.maps[map_indx].tilemap.layer_array[layer_indx].tileArray[i];
    }

}

void editor_get_tile(int32_t* tileid, int32_t tilepos,char* layer_id)
{
	if(!hashtable_haskey(&editor_ptr->editor_level.current_map->tilemap.layer_hash,layer_id))
	{
		logprint("No layer with id %s!",layer_id);
		return;
	}

	int layer_indx = hashtable_index(&editor_ptr->editor_level.current_map->tilemap.layer_hash,layer_id);
	(*tileid) = editor_ptr->editor_level.current_map->tilemap.layer_array[layer_indx].tileArray[tilepos];
}

int editor_get_num_multi_tiles()
{
	//get the area of tile to copy
	Rect_t select_rectangle = get_select_rectangle();

	int start_col = (int)ceilf(select_rectangle.position.x  / editor_ptr->editor_level.current_map->tilemap.tile_size);
	int start_row = (int)ceilf(select_rectangle.position.y / editor_ptr->editor_level.current_map->tilemap.tile_size);

	if(start_row >=  editor_ptr->editor_level.current_map->tilemap.row_count)
		start_row = editor_ptr->editor_level.current_map->tilemap.row_count-1;
	else if(start_row < 0)
		start_row = 0;

	if(start_col >= editor_ptr->editor_level.current_map->tilemap.col_count)
		start_col = editor_ptr->editor_level.current_map->tilemap.col_count - 1;
	else if(start_col < 0)
		start_col = 0;

	int num_col = (select_rectangle.width / editor_ptr->editor_level.current_map->tilemap.tile_size);
	int num_row = (select_rectangle.height / editor_ptr->editor_level.current_map->tilemap.tile_size);

	if(start_col + num_col >= editor_ptr->editor_level.current_map->tilemap.col_count)
		num_col = editor_ptr->editor_level.current_map->tilemap.col_count - start_col;

	if(start_row - (num_row -1) < 0)
		num_row = start_row + 1;

	int num_tiles = num_col * num_row;

	return num_tiles;
}

void editor_get_multi_tiles(int32_t* tileids,char* layer_id)
{
	if(!hashtable_haskey(&editor_ptr->editor_level.current_map->tilemap.layer_hash,layer_id))
	{
		logprint("No layer with id %s!",layer_id);
		return;
	}

	int layer_indx = hashtable_index(&editor_ptr->editor_level.current_map->tilemap.layer_hash,layer_id);

	//get the area of tile to copy
	Rect_t select_rectangle = get_select_rectangle();

	int start_col = (int)(select_rectangle.position.x  / editor_ptr->editor_level.current_map->tilemap.tile_size);
	int start_row = (int)(select_rectangle.position.y / editor_ptr->editor_level.current_map->tilemap.tile_size) -1;


	if(start_row >=  editor_ptr->editor_level.current_map->tilemap.row_count)
		start_row = editor_ptr->editor_level.current_map->tilemap.row_count-1;
	else if(start_row < 0)
		start_row = 0;

	if(start_col >= editor_ptr->editor_level.current_map->tilemap.col_count)
		start_col = editor_ptr->editor_level.current_map->tilemap.col_count - 1;

	int start_tile = (start_row *  editor_ptr->editor_level.current_map->tilemap.col_count) + start_col;

	int num_col = (select_rectangle.width / editor_ptr->editor_level.current_map->tilemap.tile_size);
	int num_row = (select_rectangle.height / editor_ptr->editor_level.current_map->tilemap.tile_size);

	if(start_col + num_col >= editor_ptr->editor_level.current_map->tilemap.col_count)
		num_col = editor_ptr->editor_level.current_map->tilemap.col_count - start_col;
	else if(start_col < 0)
		start_col = 0;

	if(start_row - (num_row -1) < 0)
		num_row = start_row + 1;

	int num_tiles = num_col * num_row;	

	int32_t i_num = 1;

	int32_t i_tile = start_tile;

	while(i_num <= num_tiles)
	{
		(*tileids++) = editor_ptr->editor_level.current_map->tilemap.layer_array[layer_indx].tileArray[i_tile++];

		if((i_num % num_col) == 0)
			i_tile -= (editor_ptr->editor_level.current_map->tilemap.col_count + num_col);

		i_num++;
	}
}


#endif