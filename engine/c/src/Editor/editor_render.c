#ifndef STRIP_EDIT_CODE

#ifdef _MSC_VER
extern "C" {
#endif
#include <lua.h>

#ifdef _MSC_VER
}
#endif


#if defined(_WIN32)
#include <windows.h>
//#include <mscoree.h>
#include <eh.h>
#include <Psapi.h>
//#pragma comment(lib, "mscoree.lib")
#pragma comment(lib,"Psapi.lib")
#endif


#include <Render.h>
#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <time.h>
#include <string.h>
#include <limits.h>
#include <math.h>
#include <sqlite3.h>




#include <Engine.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>
#include <Base/geom.h>

#include <Render/render_manager.h>


#include <Debug/geomdebug.h>

#include <Graphics/TextureLoader.h>
#include <Resx/shader_loader.h>

#include <Utils/hash.h>
#include <Utils/hashmap.h>

#include <Threading/thread_func.h>
#include <Pathfinding/pathfind_worker.h>

#include <GameObjects/Characters.pb-c.h>
#include <Graphics/Animation.h>
#include <Graphics/Sprite.h>
#include <Graphics/Mesh.h>
#include <Graphics/Light.h>
#include <Graphics/LightManager.h>

#include <Graphics/TextureRegion.h>
#include <Graphics/SpriteBatch.h>
#include <Graphics/geomdraw.h>

#include "GameObjects/Level.pb-c.h"

#include <portaudio.h>
#include <Sound/Sound.h>

#include <Graphics/FontLoader.h>

#include <Resx/Resources.h>
#include <Resx/localization.h>
#include <GameMngr/save_mngr.h>

#include "Debug/LogPrint.h"
#include "Graphics/TilemapLayer.h"
#include "GameObjects/Tilemap.h"

#include <Sound/snd_mngr.h>

#include <Font/RenderText.h>

#include <chipmunk/chipmunk.h>


#include <Debug/ChipmunkDebugDraw.h>

#include <Physics/Physics.h>
#include <Physics/physics_utils.h>


#include <GameObjects/entity.h>
#include <GameObjects/Trigger.h>
#include <GameObjects/text_entity.h>

#include <GameObjects/entity_group.h>

#include <Graphics/Effects/particle_object.h>
#include <GameObjects/particles_entity.h>


#include <Config/config.h>
#include <Config/shader_config.h>
#include <GameMngr/input_mngr.h>
#include <game_functions.h>
#include <GameMngr/module_mngr.h>


#include <GameObjects/map.h>

#include <GameObjects/Collider.h>
#include <GameObjects/waypoint.h>

#include <GameObjects/Levelobject.h>


#include <Scripting/ScriptEngine.h>

#include <Graphics/Effects/screen_fill.h>
#include <Graphics/Effects/screen_fade.h>

#include <Editor/editor_selector.h>

#include <Editor/Editor.h>
#include <Editor/editor_render.h>
#include <Editor/editor_game.h>



static editor_t* editor_ptr = NULL;

void editor_render_setptr(editor_t* const ptr)
{
	editor_ptr = ptr;
}

Rect_t get_select_rectangle()
{
	Rect_t select_rectangle;

	Vector_t diff = Vec_minus(editor_ptr->end_cursor_pos,editor_ptr->cursor_pos);

	int32_t offset_width = 0;
	int32_t offset_height = 0;

	if(diff.x > 0)
	{
		select_rectangle.position.x = editor_ptr->cursor_pos.x;
		offset_width = editor_ptr->tile_size;
	}
	else if(diff.x < 0)
	{
		select_rectangle.position.x = editor_ptr->end_cursor_pos.x;
		offset_width = editor_ptr->tile_size;
	}
	else
	{
		select_rectangle.position.x = editor_ptr->cursor_pos.x;
	}

	if(diff.y > 0)
	{
		select_rectangle.position.y = editor_ptr->end_cursor_pos.y + editor_ptr->tile_size;
		offset_height = editor_ptr->tile_size;
	}
	else if(diff.y < 0)
	{
		select_rectangle.position.y = editor_ptr->cursor_pos.y + editor_ptr->tile_size;
		offset_height = editor_ptr->tile_size;
	}
	else
	{
		select_rectangle.position.y = editor_ptr->cursor_pos.y + editor_ptr->tile_size;
	}


	select_rectangle.width = (int16_t)max(editor_ptr->tile_size,fabsf(diff.x)) + offset_width;
	select_rectangle.height = (int16_t)max(editor_ptr->tile_size,fabsf(diff.y)) + offset_height;

	return select_rectangle;
}

void editor_get_select_rectangle(int32_t* rect_values)
{
	Rect_t val = get_select_rectangle();

	rect_values[0] = (int32_t)val.position.x;
	rect_values[1] = (int32_t)val.position.y;
	rect_values[2] = val.width;
	rect_values[3] = val.height;
}

void editor_resize_screen(int16_t width,int16_t height)
{
	render_resizescreen(&editor_ptr->render_mngr,width,height);

	light_manager_resizerender(&editor_ptr->light_mngr,&editor_ptr->render_mngr);

}

void editor_set_tilesize(int32_t tile_size)
{
	editor_ptr->tile_size = tile_size;
}


void editor_draw_frame()
{


	if(editor_ptr->sound_source_sprite == NULL)
	{
		editor_ptr->sound_source_sprite = (sprite_t*)wf_malloc(sizeof(sprite_t));
		memset(editor_ptr->sound_source_sprite,0,sizeof(sprite_t));

		sprite_init(editor_ptr->sound_source_sprite,32,32,32,32,gettexture_resx(&editor_ptr->resx_mngr,"sound_high_icon_32.png"),NULL,0,0,&editor_ptr->render_mngr,wfalse,0,1.0f,1.0f,wtrue);
	}

	if(editor_ptr->light_sprite == NULL)
	{
		editor_ptr->light_sprite = (sprite_t*)wf_malloc(sizeof(sprite_t));
		memset(editor_ptr->light_sprite,0,sizeof(sprite_t));

		sprite_init(editor_ptr->light_sprite,32,32,32,32,gettexture_resx(&editor_ptr->resx_mngr,"lightbulb_icon_32.png"),NULL,0,0,&editor_ptr->render_mngr,wfalse,0,1.0f,1.0f,wtrue);
	}

	if(editor_ptr->particle_sprite == NULL)
	{
		editor_ptr->particle_sprite = (sprite_t*)wf_malloc(sizeof(sprite_t));
		memset(editor_ptr->particle_sprite,0,sizeof(sprite_t));

		sprite_init(editor_ptr->particle_sprite,32,32,32,32,gettexture_resx(&editor_ptr->resx_mngr,"star_icon_32.png"),NULL,0,0,&editor_ptr->render_mngr,wfalse,0,1.0f,1.0f,wtrue);
	}

	if(editor_ptr->waypoint_sprite == NULL)
	{
		editor_ptr->waypoint_sprite = (sprite_t*)wf_malloc(sizeof(sprite_t));
		memset(editor_ptr->waypoint_sprite, 0, sizeof(sprite_t));

		sprite_init(editor_ptr->waypoint_sprite, 32, 32, 32, 32, gettexture_resx(&editor_ptr->resx_mngr, "waypoint.png"), NULL, 0, 0, &editor_ptr->render_mngr, wfalse, 0, 1.0f, 1.0f,wtrue);
	}

	if (editor_ptr->collider_sprite == NULL)
	{
		editor_ptr->collider_sprite = (sprite_t*)wf_malloc(sizeof(sprite_t));
		memset(editor_ptr->collider_sprite, 0, sizeof(sprite_t));

		sprite_init(editor_ptr->collider_sprite, 32, 32, 32, 32, gettexture_resx(&editor_ptr->resx_mngr, "collider.png"), NULL, 0, 0, &editor_ptr->render_mngr, wfalse, 0, 1.0f, 1.0f,wtrue);
	}

	TIME_PROF newTime;

	if(editor_ptr->play_mode)
	{
		time_get_time(&newTime);

		TIME_PROF ltime = time_diff(&editor_ptr->currentTime,&newTime);

		editor_ptr->time_elapsed = time_get_nsec(&ltime);

		float rnd = (float)editor_ptr->frameRenderTime / 1000000L;

		swprintf(editor_ptr->fps,260,L"FPS : [%d] Render Time : [%05.2fms]",editor_ptr->num_fps,rnd);

		editor_ptr->editor_module_mngr->game_debuginfo_update();

		if(editor_ptr->time_elapsed >= 1000000000L)
		{
			editor_ptr->num_fps = editor_ptr->count_frame;
			editor_ptr->count_frame = 0;
			time_get_time(&editor_ptr->currentTime);
		}
	}


	render_clear_screenwithcolor(&editor_ptr->render_mngr,editor_ptr->back_color);

	if(editor_ptr->play_mode)
	{
		if(editor_has_player())
			editor_ptr->editor_module_mngr->game_updatescroll();
	}
	else
	{
		editor_ptr->render_mngr.scroll_vector.x = (editor_ptr->render_mngr.screen_info.width / 2)  - (editor_ptr->editor_position.x / editor_ptr->render_mngr.zoom_factor);
		editor_ptr->render_mngr.scroll_vector.y = (editor_ptr->render_mngr.screen_info.height / 2) -  ( editor_ptr->editor_position.y / editor_ptr->render_mngr.zoom_factor);
	}

    render_update_screen(&editor_ptr->render_mngr);

	
	
	//render lights alpha mask
	if(editor_ptr->editor_level.current_map != NULL && editor_ptr->editor_level.current_map->tilemap.edge_array != NULL && editor_ptr->enable_lighting)
		light_manager_rendermask(&editor_ptr->light_mngr,editor_ptr->editor_level.current_map->tilemap.edge_array,editor_ptr->editor_level.current_map->tilemap.edge_array_size,&editor_ptr->render_mngr,wfalse,editor_ptr->editor_level.current_map_id,editor_ptr->editor_level.current_map->tilemap.tile_size,editor_ptr->editor_level.current_map->tilemap.col_count,editor_ptr->editor_level.current_map->tilemap.row_count,vectorzero);
	
	render_blend_alpha(&editor_ptr->render_mngr,wfalse);

	ColorA_t color = {1.0f, 1.0f,1.0f,1.0f};

	render_set_color(&editor_ptr->render_mngr,color);

	render_use_program(&editor_ptr->render_mngr,render_get_program(&editor_ptr->render_mngr,"prog_textured"));

	//draw on screen
	if (editor_ptr->editor_level.current_map != NULL) {

		for (int16_t z_indx = 0; z_indx < MAX_ZINDEX - 1; z_indx++)
		{

			Tilemap_Draw(&editor_ptr->editor_level.current_map->tilemap, z_indx, &editor_ptr->render_mngr, vectorzero, &editor_ptr->light_mngr, &editor_ptr->resx_mngr);


			Levelobject_Drawentities(&editor_ptr->editor_level, &editor_ptr->render_mngr, z_indx, &editor_ptr->resx_mngr, vectorzero, wtrue);


			editor_ptr->editor_module_mngr->game_draw(z_indx);


			Levelobject_DrawText(&editor_ptr->editor_level, &editor_ptr->render_mngr, z_indx, vectorzero, wtrue);

			Levelobject_DrawParticles(&editor_ptr->editor_level, &editor_ptr->render_mngr, z_indx, vectorzero, wtrue);

			//render fullscreen effects
			if (editor_ptr->has_filler && editor_ptr->screen_filler.active && editor_ptr->screen_filler.z_index == z_indx)
			{
				screen_fill_draw(&editor_ptr->screen_filler, &editor_ptr->render_mngr);

				if (editor_ptr->has_filler_sibling) {
					screen_fill_draw(&editor_ptr->screen_filler_sibling, &editor_ptr->render_mngr);
				}
			}
				
		}
	}
	

	editor_ptr->editor_module_mngr->game_debug_draw(0,0);

	if(editor_ptr->editor_level.current_map != NULL && editor_ptr->editor_level.current_map->tilemap.edge_array != NULL && editor_ptr->enable_lighting)
	{
		light_manager_draw(&editor_ptr->light_mngr,&editor_ptr->render_mngr,wfalse,editor_ptr->editor_level.current_map_id,editor_ptr->editor_level.current_map->tilemap.tile_size,editor_ptr->editor_level.current_map->tilemap.col_count,editor_ptr->editor_level.current_map->tilemap.row_count);

		#if _DEBUG
		render_unbind_texture(&editor_ptr->render_mngr,wfalse);

		render_use_program(&editor_ptr->render_mngr,render_get_program(&editor_ptr->render_mngr,"prog_no_texture"));

		render_push_matrix(&editor_ptr->render_mngr);
		//debug draw edges
		for(int32_t iedge = 0; iedge < editor_ptr->editor_level.current_map->tilemap.edge_array_size;iedge++)
		{
			if(!editor_ptr->editor_level.current_map->tilemap.edge_array[iedge].is_src || editor_ptr->editor_level.current_map->tilemap.edge_array[iedge].p_edge == NULL)
				continue;

			ColorA_t col_e = green_color;

			if(editor_ptr->editor_level.current_map->tilemap.edge_array[iedge].p_edge->type == e_top)
				col_e = white_color;
			else if(editor_ptr->editor_level.current_map->tilemap.edge_array[iedge].p_edge->type == e_left)
				col_e = blue_color;
			else if(editor_ptr->editor_level.current_map->tilemap.edge_array[iedge].p_edge->type == e_right)
				col_e = red_color;

			Debug_Drawedge(editor_ptr->editor_level.current_map->tilemap.edge_array[iedge].p_edge,col_e,&editor_ptr->render_mngr);

		}
		render_pop_matrix(&editor_ptr->render_mngr);
		#endif

		
	}
	
	if(editor_ptr->screen_fader.active)
		screen_fade_draw(&editor_ptr->screen_fader,&editor_ptr->render_mngr);

	//render fullscreen effects
	if (editor_ptr->has_filler && editor_ptr->screen_filler.active && editor_ptr->screen_filler.z_index == -1)
	{
		screen_fill_draw(&editor_ptr->screen_filler, &editor_ptr->render_mngr);

		if (editor_ptr->has_filler_sibling) {
			screen_fill_draw(&editor_ptr->screen_filler_sibling, &editor_ptr->render_mngr);
		}
	}

	//render object over filler effect if any
	Levelobject_Drawentities(&editor_ptr->editor_level,&editor_ptr->render_mngr,MAX_ZINDEX-1,&editor_ptr->resx_mngr,vectorzero,wtrue);
	Levelobject_DrawText(&editor_ptr->editor_level,&editor_ptr->render_mngr,MAX_ZINDEX-1,vectorzero,wtrue);

	if(editor_ptr->play_mode)
	{
		editor_ptr->editor_module_mngr->game_draw(-1);
	}


	if(editor_ptr->show_grid && editor_ptr->editor_level.current_map != NULL)
	{
		render_unbind_texture(&editor_ptr->render_mngr,wfalse);

		render_use_program(&editor_ptr->render_mngr,render_get_program(&editor_ptr->render_mngr,"prog_no_texture"));

		render_push_matrix(&editor_ptr->render_mngr);

		Tilemap_editor_grid_draw(&editor_ptr->editor_level.current_map->tilemap,editor_ptr->grid_color,&editor_ptr->render_mngr);

		render_pop_matrix(&editor_ptr->render_mngr);
	}

	
	
	//functions used in play mode, physics simulation, avatar movement, scripting events
	if(editor_ptr->play_mode)
	{
		if ((editor_ptr->editor_level.current_state & COMMON_PAUSE) == 0)
		{

			//update physics engine
			for (int32_t sr = 0; sr < sample_rate; sr++)
				update_physics(&editor_ptr->phy_mgr, (float)dt);


			Levelobject_update(&editor_ptr->editor_level, max_frame_time, &editor_ptr->render_mngr, editor_ptr->lua_state, wfalse);

			if (editor_ptr->call_script && editor_ptr->editor_level.current_module != NULL && strlen(editor_ptr->editor_level.current_module) > 0)
			{
				const char* result = script_callfunc_params(editor_ptr->lua_state, editor_ptr->editor_level.current_module, "doEvents", "f", max_frame_time);

				if (result != NULL)
					w_strcpy(editor_ptr->lua_log_buffer, sizeof(editor_ptr->lua_log_buffer), result);
			}
		}

		if (editor_ptr->has_filler && editor_ptr->screen_filler.active)
		{
			screen_fill_update(&editor_ptr->screen_filler, &editor_ptr->render_mngr, (float)max_frame_time);

			if (editor_ptr->has_filler_sibling) {
				screen_fill_update(&editor_ptr->screen_filler_sibling, &editor_ptr->render_mngr, (float)max_frame_time);
			}
		}
			

		if(editor_ptr->screen_fader.active)
			screen_fade_update(&editor_ptr->screen_fader,(float)max_frame_time);

		editor_ptr->editor_module_mngr->game_update((float)max_frame_time);
	}

	
	
	if(editor_ptr->play_mode)
	{
		//render FPS
		render_use_program(&editor_ptr->render_mngr,render_get_program(&editor_ptr->render_mngr,"prog_font"));

		render_setuniform_float(&editor_ptr->render_mngr, "pxRange", (float)editor_ptr->fps_text.info->distanceRange);

		render_push_matrix(&editor_ptr->render_mngr);

		int32_t text_x = (int32_t)(-editor_ptr->render_mngr.scroll_vector.x) * (int32_t)editor_ptr->render_mngr.zoom_factor;
		int32_t text_y = (int32_t)(-editor_ptr->render_mngr.scroll_vector.y ) * (int32_t)editor_ptr->render_mngr.zoom_factor;

		text_y += (int32_t)Render_Text_charheight(&editor_ptr->fps_text) + 16;
			
		Render_Text_begin(&editor_ptr->fps_text,editor_ptr->fps_color.r,editor_ptr->fps_color.g,editor_ptr->fps_color.b,editor_ptr->fps_color.a,&editor_ptr->render_mngr);

		Render_Text_Draw(&editor_ptr->fps_text,&editor_ptr->fps[0],(float)text_x,(float)text_y,16,&editor_ptr->render_mngr);
		Render_Text_end(&editor_ptr->fps_text,&editor_ptr->render_mngr);


		render_pop_matrix(&editor_ptr->render_mngr);

	}
	
	if(editor_ptr->show_debug_col)
	{
		render_unbind_texture(&editor_ptr->render_mngr,wfalse);

		ChipmunkDebugDrawImpl(&editor_ptr->render_mngr, editor_ptr->phy_mgr.world);
	}
	
	render_use_program(&editor_ptr->render_mngr,render_get_program(&editor_ptr->render_mngr,"prog_no_texture"));

	if (editor_ptr->show_colliders_triggers_misc) {
		Levelobject_Drawtriggers(&editor_ptr->editor_level, &editor_ptr->render_mngr);

		Levelobject_Drawcolliders(&editor_ptr->editor_level, &editor_ptr->render_mngr);
	}
	
	if(editor_ptr->editor_level.current_map != NULL  && editor_ptr->editor_level.current_map->tilemap.collisions_array != NULL && editor_ptr->editor_level.current_map->tilemap.show_collisions)
	{
		render_unbind_texture(&editor_ptr->render_mngr,wfalse);

		render_use_program(&editor_ptr->render_mngr,render_get_program(&editor_ptr->render_mngr,"prog_colored_no_texture"));

		Tilemap_editor_collisions_Draw(&editor_ptr->editor_level.current_map->tilemap,&editor_ptr->render_mngr,vectorzero);
	}
	
	//render entities specific items (light / sound source / waypoint / collider)

	render_use_program(&editor_ptr->render_mngr,render_get_program(&editor_ptr->render_mngr,"prog_textured"));

	if (editor_ptr->show_colliders_triggers_misc)
	{
		uint32_t ent_index = 0;
		uint32_t ent_count = 0;

		for (item_t* c_item = (item_t*)hashmap_iterate(&editor_ptr->selector.items_map, &ent_index, &ent_count); c_item != NULL; c_item = (item_t*)hashmap_iterate(&editor_ptr->selector.items_map, &ent_index, &ent_count))
		{
			if (strcmp(c_item->map_id, editor_ptr->editor_level.current_map_id) == 0)
			{
				if (c_item->type == GT_SOUND)
					sprite_draw(editor_ptr->sound_source_sprite, c_item->bounds->position.x + 16, c_item->bounds->position.y - 16, &editor_ptr->render_mngr, NULL);
				else if (c_item->type == GT_LIGHT)
					sprite_draw(editor_ptr->light_sprite, c_item->bounds->position.x + 16, c_item->bounds->position.y - 16, &editor_ptr->render_mngr, NULL);
				else if (c_item->type == GT_PARTICLES)
					sprite_draw(editor_ptr->particle_sprite, c_item->bounds->position.x + 16, c_item->bounds->position.y - 16, &editor_ptr->render_mngr, NULL);
				else if (c_item->type == GT_WAYPOINT)
					sprite_draw(editor_ptr->waypoint_sprite, c_item->bounds->position.x + 16, c_item->bounds->position.y - 16, &editor_ptr->render_mngr, NULL);
				else if (c_item->type == GT_COLLIDER)
					sprite_draw(editor_ptr->collider_sprite, c_item->bounds->position.x + 16, c_item->bounds->position.y - 16, &editor_ptr->render_mngr, NULL);
			}
		}
	}
	



	//render selected entities outlines
	if(editor_ptr->selector.selected_array != NULL && editor_ptr->selector.num_selected_items > 0 && !editor_ptr->play_mode)
	{
		render_use_program(&editor_ptr->render_mngr,render_get_program(&editor_ptr->render_mngr,"prog_no_texture"));

		render_unbind_texture(&editor_ptr->render_mngr,wfalse);

		render_set_color(&editor_ptr->render_mngr,editor_ptr->select_color);


		for(int32_t i_sel = 0; i_sel < editor_ptr->selector.num_selected_items;i_sel++)
		{
			render_push_matrix(&editor_ptr->render_mngr);

			Rect_t* render_box = editor_ptr->selector.selected_array[i_sel]->bounds;


			float vertices[] = { render_box->position.x, render_box->position.y,
									render_box->position.x +render_box->width, render_box->position.y,
									render_box->position.x + render_box->width,  render_box->position.y - render_box->height,
									render_box->position.x, render_box->position.y - render_box->height };

		

			render_start_draw_array(&editor_ptr->render_mngr,2,4,vertices,NULL);

			render_draw_lineloop(&editor_ptr->render_mngr,0,4);

			render_end_draw_array(&editor_ptr->render_mngr,wfalse);

			render_pop_matrix(&editor_ptr->render_mngr);
		}

		render_reset_color(&editor_ptr->render_mngr);
	}

	//render selection rectangle
	if(editor_ptr->show_selector && !editor_ptr->play_mode && (editor_ptr->selector.start_select.x != editor_ptr->selector.end_select.x || editor_ptr->selector.start_select.y != editor_ptr->selector.end_select.y))
	{
		render_use_program(&editor_ptr->render_mngr,render_get_program(&editor_ptr->render_mngr,"prog_no_texture"));

		render_unbind_texture(&editor_ptr->render_mngr,wfalse);

		render_set_color(&editor_ptr->render_mngr,editor_ptr->cursor_color);

		render_push_matrix(&editor_ptr->render_mngr);

		Rect_t select_rectangle;

		Vector_t diff = Vec_minus(editor_ptr->selector.end_select,editor_ptr->selector.start_select);


		if(diff.x > 0)
			select_rectangle.position.x = editor_ptr->selector.start_select.x;
		else
			select_rectangle.position.x = editor_ptr->selector.end_select.x;

		if(diff.y > 0)
			select_rectangle.position.y = editor_ptr->selector.end_select.y;
		else
			select_rectangle.position.y = editor_ptr->selector.start_select.y;


		select_rectangle.width = (int16_t)fabsf(diff.x);
		select_rectangle.height = (int16_t)fabsf(diff.y);

		float vertices[] = { select_rectangle.position.x, select_rectangle.position.y,
								select_rectangle.position.x +select_rectangle.width, select_rectangle.position.y,
								select_rectangle.position.x + select_rectangle.width,  select_rectangle.position.y - select_rectangle.height,
								select_rectangle.position.x, select_rectangle.position.y - select_rectangle.height };

		

		render_start_draw_array(&editor_ptr->render_mngr,2,4,vertices,NULL);

		render_draw_lineloop(&editor_ptr->render_mngr,0,4);

		render_end_draw_array(&editor_ptr->render_mngr,wfalse);

		render_pop_matrix(&editor_ptr->render_mngr);

		render_reset_color(&editor_ptr->render_mngr);
	}
	
    //render cursor

	if(editor_ptr->show_cursor && !editor_ptr->play_mode)
	{
		render_use_program(&editor_ptr->render_mngr,render_get_program(&editor_ptr->render_mngr,"prog_no_texture"));

		render_unbind_texture(&editor_ptr->render_mngr,wfalse);

		render_push_matrix(&editor_ptr->render_mngr);

		//float* cursor_vertices;

		Rect_t cursor_rectangle = { 0 };

		if(editor_ptr->cursor_pos.x == editor_ptr->end_cursor_pos.x && editor_ptr->cursor_pos.y == editor_ptr->end_cursor_pos.y)
		{
			cursor_rectangle.width = editor_ptr->tile_size;
			cursor_rectangle.height = editor_ptr->tile_size;
			cursor_rectangle.position = editor_ptr->cursor_pos;
			cursor_rectangle.position.y += editor_ptr->tile_size;

		}
		else
		{
			cursor_rectangle = get_select_rectangle();
		}

		drawrect(&cursor_rectangle, editor_ptr->cursor_color, &editor_ptr->render_mngr);
	}

	
	//input events
	if(editor_ptr->play_mode)
	{
		input_mngr_handle_input(&editor_ptr->input_mngr);
		input_mngr_poll(&editor_ptr->input_mngr);
	}
	
	render_swap_buffers(&editor_ptr->render_mngr);


	if(editor_ptr->play_mode)
	{
		time_get_time(&editor_ptr->end_time);
		
		TIME_PROF frame_rnd_struct = time_diff(&newTime,&editor_ptr->end_time);

		editor_ptr->frameRenderTime =  time_get_nsec(&frame_rnd_struct);

		if( editor_ptr->frameRenderTime < editor_ptr->max_nsecframe_time )
		{
			editor_ptr->sleep_time = editor_ptr->max_nsecframe_time - editor_ptr->frameRenderTime;

			if(editor_ptr->sleep_time < 0)
				editor_ptr->sleep_time = 0;
			
			if(editor_ptr->overtime > 0)
			{
				if(editor_ptr->overtime > editor_ptr->sleep_time)
				{
					editor_ptr->overtime -= editor_ptr->sleep_time;
					editor_ptr->sleep_time = 0;
				}
				else
				{
					editor_ptr->sleep_time -= editor_ptr->overtime;
					editor_ptr->overtime = 0;
				}
			}

			#ifndef EMSCRIPTEN
				nano_sleep(editor_ptr->sleep_time,wtrue);
			#endif
		}
		else //over maximum frame time, store the difference to reduce sleep time of further frames
		{
			editor_ptr->overtime += editor_ptr->frameRenderTime - editor_ptr->max_nsecframe_time;
		}
			
		editor_ptr->count_frame++;
	}
}


void editor_setzoom(int width,int height,float zoomfactor)
{
	editor_ptr->render_mngr.zoom_factor = zoomfactor;
	editor_ptr->render_mngr.screen_info.width = width;
	editor_ptr->render_mngr.screen_info.height = height;

	render_update_screen(&editor_ptr->render_mngr);
	light_manager_resizerender(&editor_ptr->light_mngr,&editor_ptr->render_mngr);
	
}

float editor_getscrollX()
{
	return  editor_ptr->render_mngr.scroll_vector.x;
}

float editor_getscrollY()
{
	return editor_ptr->render_mngr.scroll_vector.y;
}

void editor_draw_debug_collisions(wbool show)
{
	editor_ptr->show_debug_col = show;
}

void editor_draw_colliders_triggers_misc(wbool show)
{
	editor_ptr->show_colliders_triggers_misc = show;
}
  //************************//
 //draw elements functions //
//************************//

void editor_scroll(float mx,float my)
{
	editor_ptr->editor_position.x += mx;
	editor_ptr->editor_position.y += my;
}

void editor_set_scroll(float x,float y)
{
	editor_ptr->editor_position.x = x;
	editor_ptr->editor_position.y = y;
}



void editor_change_color(float r,float g,float b,float a)
{
	editor_ptr->back_color.r = r;
	editor_ptr->back_color.g = g;
	editor_ptr->back_color.b = b;
	editor_ptr->back_color.a = a;
}

void editor_grid_visibility(wbool show,float r,float g,float b,float a)
{
	editor_ptr->grid_color.r = r;
	editor_ptr->grid_color.b = b;
	editor_ptr->grid_color.g = g;
	editor_ptr->grid_color.a = a;
	editor_ptr->show_grid = show;
}
void editor_set_selectioncolor(float r,float g,float b,float a)
{
	editor_ptr->select_color.r = r;
	editor_ptr->select_color.g = g;
	editor_ptr->select_color.b = b;
	editor_ptr->select_color.a = a;
}

void editor_set_ambientcolor(float r,float g,float b,float a)
{
	editor_ptr->light_mngr.ambient_color.r = r;
	editor_ptr->light_mngr.ambient_color.g = g;
	editor_ptr->light_mngr.ambient_color.b = b;
	editor_ptr->light_mngr.ambient_color.a = a;
}



  //************************//
 //cursor functions        //
//************************//

void editor_setcursor_pos(int x,int y,wbool endcursor_only,wbool move_multi_selection)
{
	if(!move_multi_selection)
	{
		if(!endcursor_only)
		{
			editor_ptr->cursor_pos.x = (float)x;
			editor_ptr->cursor_pos.y = (float)y;
		}

		editor_ptr->end_cursor_pos.x = (float)x;
		editor_ptr->end_cursor_pos.y = (float)y;
	}
	else
	{
		if(editor_ptr->cursor_pos.x < editor_ptr->end_cursor_pos.x)
		{
			int diffx = (int)(editor_ptr->end_cursor_pos.x - editor_ptr->cursor_pos.x);
			editor_ptr->cursor_pos.x = (wfloat)x;
			editor_ptr->end_cursor_pos.x = editor_ptr->cursor_pos.x + (wfloat)diffx;
		}
		else
		{
			int diffx = (int)(editor_ptr->cursor_pos.x - editor_ptr->end_cursor_pos.x);
			editor_ptr->end_cursor_pos.x = (wfloat)x;
			editor_ptr->cursor_pos.x = editor_ptr->end_cursor_pos.x + (wfloat)diffx;
		}

		if(editor_ptr->cursor_pos.y < editor_ptr->end_cursor_pos.y)
		{
			int diffy = (int)(editor_ptr->end_cursor_pos.y - editor_ptr->cursor_pos.y);
			editor_ptr->cursor_pos.y = (wfloat)y;
			editor_ptr->end_cursor_pos.y = editor_ptr->cursor_pos.y + (wfloat)diffy;
		}
		else
		{
			int diffy = (int)(editor_ptr->cursor_pos.y - editor_ptr->end_cursor_pos.y);
			editor_ptr->end_cursor_pos.y = (wfloat)y;
			editor_ptr->cursor_pos.y = editor_ptr->end_cursor_pos.y + (wfloat)diffy;
		}
	}
}

void editor_settargetpos(int x, int y)
{
	if(editor_ptr->play_mode)
	{
		editor_ptr->editor_module_mngr->game_setcursorpos((float)x,(float)y);
		editor_ptr->editor_module_mngr->game_movetarget((float)x,(float)y,wfalse);
	}

	editor_ptr->selector.end_select.x = (wfloat)x;
	editor_ptr->selector.end_select.y = (wfloat)y;
}

void editor_setcursor_color(float r,float g,float b,float a)
{
	editor_ptr->cursor_color.r = r;
	editor_ptr->cursor_color.g = g;
	editor_ptr->cursor_color.b = b;
	editor_ptr->cursor_color.a = a;
}

void editor_setfps_color(float r,float g,float b,float a)
{
	editor_ptr->fps_color.r = r;
	editor_ptr->fps_color.g = g;
	editor_ptr->fps_color.b = b;
	editor_ptr->fps_color.a = a;
}

void editor_showcursor(wbool show_cur,wbool show_sel)
{
	editor_ptr->show_cursor = show_cur;
	editor_ptr->show_selector = show_sel;
}

 
#endif