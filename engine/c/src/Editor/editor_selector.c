#ifndef STRIP_EDIT_CODE

#include <inttypes.h>
#include <Engine.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#if defined(_WIN32)
	#include <Windows.h>
#endif

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>
#include <Base/geom.h>

#include <Collisions/Collisions.h>

#include <Utils/Hash.h>
#include <Utils/hashmap.h>

#include <Editor/editor_selector.h>



void editor_selector_init(editor_selector_t* const selector)
{
	selector->selected_array = NULL;
	selector->num_selected_items = 0;
	selector->start_select.x = selector->start_select.y = selector->end_select.x = selector->end_select.y = 0.0f;

	hashmap_initalloc(&selector->items_map,REGISTER_ITEMS_POOL,sizeof(item_t),REGISTER_ITEMS_POOL);
}

void editor_selector_free(editor_selector_t* const selector)
{
	if(selector->selected_array != NULL)
		wf_free(selector->selected_array);

	hashmap_free(&selector->items_map);
}

void editor_selector_register_item(editor_selector_t* const selector,const char* item_id,GAME_TYPE type,Rect_t* const bounds,void* data,const char* map_id,int32_t* zorder)
{
	item_t* new_element =  (item_t*)hashmap_push(&selector->items_map,item_id);

	new_element->type = type;
	new_element->object = data;

	new_element->entity = string_alloc(item_id);

	if((type == GT_SOUND || type == GT_LIGHT || type == GT_TRIGGER || type == GT_COLLIDER || type == GT_WAYPOINT) && new_element->zorder != zorder)
	{
		new_element->zorder = (int32_t*)wf_malloc(sizeof(int32_t));
		memcpy(new_element->zorder,zorder,sizeof(int32_t));
	}
	else
	{
		new_element->zorder = zorder;
	}

	if((new_element->type == GT_SOUND || new_element->type == GT_LIGHT || new_element->type == GT_PARTICLES || type == GT_COLLIDER || type == GT_WAYPOINT) && new_element->bounds != bounds)
	{
		new_element->bounds = (Rect_t*)wf_malloc(sizeof(Rect_t));
		memcpy(new_element->bounds,bounds,sizeof(Rect_t));
	}
	else
	{
		new_element->bounds = bounds;
	}

	w_strncpy(new_element->map_id,128,map_id,128);

	//return -1;
}

item_t* editor_selector_getitem(editor_selector_t* const selector, const char* item_id)
{
	return (item_t*)hashmap_getvalue(&selector->items_map,item_id);
}

void editor_unregister_item(editor_selector_t* const selector,const char* item_id,GAME_TYPE type)
{
	if(item_id != NULL && hashmap_haskey(&selector->items_map,item_id))
	{
		item_t* element = (item_t*)hashmap_getvalue(&selector->items_map,item_id);

		GAME_TYPE c_game_type = element->type;

		if(c_game_type == GT_SOUND || c_game_type == GT_LIGHT || c_game_type == GT_PARTICLES || type == GT_COLLIDER || type == GT_WAYPOINT)
			wf_free(element->bounds);

		if(c_game_type == GT_SOUND || c_game_type == GT_LIGHT || c_game_type == GT_TRIGGER || type == GT_COLLIDER || type == GT_WAYPOINT)
			wf_free(element->zorder);

		editor_unselect_item(selector,element);

		hashmap_remove(&selector->items_map,item_id);

	}
}

void editor_selector_changeid(editor_selector_t* const selector,const char* old_entity_id,const char* new_entity_id,GAME_TYPE selected_type,Rect_t* new_rect,int32_t* new_z,void* new_obj)
{
	if(selector->selected_array != NULL && selector->num_selected_items > 0)
	{
		for(int32_t i_sel = 0; i_sel < selector->num_selected_items;i_sel++)
		{
			if(strcmp(selector->selected_array[i_sel]->entity,old_entity_id) == 0)
			{
				hashmap_rename(&selector->items_map,old_entity_id,new_entity_id);

				item_t* renamed_item = (item_t*)hashmap_getvalue(&selector->items_map,new_entity_id);
				GAME_TYPE type = renamed_item->type;

				wf_free(renamed_item->entity);
				renamed_item->entity = string_alloc(new_entity_id);

				if(type == GT_SOUND || type == GT_LIGHT || type == GT_TRIGGER || type == GT_COLLIDER || type == GT_WAYPOINT)
				{
					int32_t oldz = (*renamed_item->zorder);

					wf_free(renamed_item->zorder);
					renamed_item->zorder = (int32_t*)wf_malloc(sizeof(int32_t));

					if (new_z != NULL) {
						memcpy(renamed_item->zorder, new_z, sizeof(int32_t));
					}
					else {
						memcpy(renamed_item->zorder, &oldz, sizeof(int32_t));
					}
					
				}
				else
				{
					renamed_item->zorder = new_z;
				}

				if(type == GT_SOUND || type == GT_LIGHT || type == GT_PARTICLES || type == GT_COLLIDER || type == GT_WAYPOINT)
				{
					Rect_t oldbounds = (*renamed_item->bounds);
					wf_free(renamed_item->bounds);
					renamed_item->bounds = (Rect_t*)wf_malloc(sizeof(Rect_t));

					if (new_rect != NULL) {
						memcpy(renamed_item->bounds, new_rect, sizeof(Rect_t));
					}
					else {
						memcpy(renamed_item->bounds, &oldbounds, sizeof(Rect_t));
					}
					
				}
				else
				{
					renamed_item->bounds = new_rect;
				}

				renamed_item->object = new_obj;

				selector->selected_array[i_sel] = renamed_item;

				break;
			}
		}
	}
}

void editor_selector_changemapid(editor_selector_t* const selector,const char* oldmapid,const char* newmapid)
{
	uint32_t ent_index = 0;
	uint32_t ent_count = 0;

	for(item_t* oitem = (item_t*)hashmap_iterate(&selector->items_map,&ent_index,&ent_count);oitem != NULL;oitem= (item_t*)hashmap_iterate(&selector->items_map,&ent_index,&ent_count))
	{
		if(strcmp(oitem->map_id,oldmapid) == 0)
				w_strcpy(oitem->map_id,128,newmapid);
	}
}

void editor_selector_multiselect(editor_selector_t* const selector,Rect_t selection_rectangle,int16_t z_order,const char* current_map)
{
	uint32_t ent_index = 0;
	uint32_t ent_count = 0;

	for(item_t* oitem = (item_t*)hashmap_iterate(&selector->items_map,&ent_index,&ent_count);oitem != NULL;oitem= (item_t*)hashmap_iterate(&selector->items_map,&ent_index,&ent_count))
	{
		if(strcmp(oitem->map_id,current_map) == 0 && (oitem->zorder == NULL || (*oitem->zorder) == z_order))
		{
			if(col_check2(&selection_rectangle,oitem->bounds))
				editor_selector_selectentity(selector,oitem,wtrue);
		}
	}

}

void editor_selector_selectentity_fromname(editor_selector_t* const selector,const char* selected_item,wbool add_to_selection)
{

	if(hashmap_haskey(&selector->items_map,selected_item))
	{
		item_t* oitem = (item_t*)hashmap_getvalue(&selector->items_map,selected_item);

		editor_selector_selectentity(selector,oitem,add_to_selection);
	}
}

void editor_selector_selectentity(editor_selector_t* const selector,item_t* selected_item,wbool add_to_selection)
{
	if(selector->selected_array)
	{
		if(!add_to_selection)
		{
			wf_free(selector->selected_array);
			selector->selected_array = (item_t**)wf_calloc(1,sizeof(item_t*));
			selector->num_selected_items = 0;
		}
		else
		{
			//check that the elements isn't already in selection
			for(int32_t i_sel = 0; i_sel < selector->num_selected_items;i_sel++)
			{
				if(selector->selected_array[i_sel] == selected_item)
					return;
			}

			selector->selected_array = (item_t**)wf_realloc(selector->selected_array,sizeof(item_t*) * (selector->num_selected_items + 1));
		}
	}
	else
	{
		selector->selected_array = (item_t**)wf_calloc(1,sizeof(item_t*));
		selector->num_selected_items = 0;
	}

	selector->selected_array[selector->num_selected_items] = selected_item;

	selector->num_selected_items++;
}


void editor_selector_selectentity_pos(editor_selector_t* const selector,int posX,int posY,int z_order,wbool add_to_selection,const char* current_map)
{
	Vector_t selectVect = {(float)posX,(float)posY};

	uint32_t ent_index = 0;
	uint32_t ent_count = 0;

	wbool found_item = wfalse;

	for(item_t* oitem = (item_t*)hashmap_iterate(&selector->items_map,&ent_index,&ent_count);oitem != NULL;oitem= (item_t*)hashmap_iterate(&selector->items_map,&ent_index,&ent_count))
	{
		if(strcmp(oitem->map_id,current_map) == 0 && (oitem->zorder == NULL || (*oitem->zorder) == z_order))
		{
			if(vector_in_rect(oitem->bounds,&selectVect))
			{
				editor_selector_selectentity(selector,oitem,add_to_selection);
				found_item = wtrue;
				break;
			}
		}
	}

	//if we clicked outside of a valid element and we aren't in add to selection mode, remove all selected items
	if(!add_to_selection && !found_item)
	{
		editor_selector_unselect(selector);
	}
}

void editor_unselect_item(editor_selector_t* const selector,item_t* const item)
{
	for(int32_t i_sel = 0; i_sel < selector->num_selected_items;i_sel++)
	{
		if(selector->selected_array[i_sel] == item)
		{
			selector->selected_array[i_sel] = NULL;
			return;
		}
	}
}

void  editor_selector_unselect(editor_selector_t* const selector)
{
	if(selector->selected_array)
	{
		wf_free(selector->selected_array);
	}

	selector->selected_array = NULL;
	selector->num_selected_items = 0;

	selector->start_select.x = selector->end_select.x;
	selector->start_select.y = selector->end_select.y;
}

wbool  editor_selector_has_selection(editor_selector_t* const selector)
{
	if(selector->selected_array && selector->num_selected_items  > 0)
		return wtrue;
	else
		return wfalse;
}

#endif