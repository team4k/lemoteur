#ifndef STRIP_EDIT_CODE

#ifdef _MSC_VER
extern "C" {
#endif
#include <lua.h>

#ifdef _MSC_VER
}
#endif


#if defined(_WIN32)
#include <windows.h>
//#include <mscoree.h>
#include <eh.h>
#include <Psapi.h>
//#pragma comment(lib, "mscoree.lib")
#pragma comment(lib,"Psapi.lib")
#endif


#include <Render.h>
#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <time.h>
#include <string.h>
#include <limits.h>
#include <math.h>
#include <sqlite3.h>




#include <Engine.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>
#include <Base/geom.h>

#include <Render/render_manager.h>


#include <Debug/geomdebug.h>

#include <Graphics/TextureLoader.h>
#include <Resx/shader_loader.h>

#include <Utils/hash.h>
#include <Utils/hashmap.h>

#include <Threading/thread_func.h>
#include <Pathfinding/pathfind_worker.h>

#include <GameObjects/Characters.pb-c.h>
#include <Graphics/Animation.h>
#include <Graphics/Sprite.h>
#include <Graphics/Mesh.h>
#include <Graphics/Light.h>
#include <Graphics/LightManager.h>

#include <Graphics/TextureRegion.h>
#include <Graphics/SpriteBatch.h>

#include "GameObjects/Level.pb-c.h"

#include <portaudio.h>
#include <Sound/Sound.h>

#include <Graphics/FontLoader.h>

#include <Resx/Resources.h>
#include <Resx/localization.h>
#include <GameMngr/save_mngr.h>

#include "Debug/LogPrint.h"
#include "Graphics/TilemapLayer.h"
#include "GameObjects/Tilemap.h"

#include <Sound/snd_mngr.h>

#include <Font/RenderText.h>

#include <chipmunk/chipmunk.h>


#include <Debug/ChipmunkDebugDraw.h>

#include <Physics/Physics.h>
#include <Physics/physics_utils.h>


#include <GameObjects/entity.h>
#include <GameObjects/Trigger.h>
#include <GameObjects/text_entity.h>

#include <GameObjects/entity_group.h>

#include <Graphics/Effects/particle_object.h>
#include <GameObjects/particles_entity.h>


#include <Config/config.h>
#include <Config/shader_config.h>
#include <GameMngr/input_mngr.h>
#include <game_functions.h>
#include <GameMngr/module_mngr.h>


#include <GameObjects/map.h>

#include <GameObjects/Collider.h>
#include <GameObjects/waypoint.h>

#include <GameObjects/Levelobject.h>


#include <Scripting/ScriptEngine.h>

#include <Graphics/Effects/screen_fill.h>
#include <Graphics/Effects/screen_fade.h>

#include <Editor/editor_selector.h>

#include <Editor/Editor.h>
#include <Editor/editor_game.h>
#include <Editor/editor_resx.h>

static editor_t* editor_ptr = NULL;

void editor_resx_setptr(editor_t* const ptr)
{
	editor_ptr = ptr;
}

 //************************//
 //add resources functions //
//************************//

wbool editor_resx_exist(char* resx_name)
{
	if(!hashtable_haskey(editor_ptr->resx_mngr.resx_hash,resx_name))
		return wfalse;
	else
		return wtrue;
}

void editor_addtextureresx(char* texture_name,int16_t textureWidth,int16_t textureHeight,unsigned char* image_data,unsigned long data_size)
{
	add_resx(&editor_ptr->resx_mngr,texture_name,texture,"",wfalse,NULL,NULL);
	settexture_resx(&editor_ptr->resx_mngr,texture_name,textureWidth,textureHeight,image_data,data_size);
}

void editor_addanimresx(char* anim_name,unsigned char* anim_data,size_t anim_size)
{
	add_resx(&editor_ptr->resx_mngr,anim_name,animation,"",wfalse,NULL,NULL);
	setanimation_resx(&editor_ptr->resx_mngr,anim_name,anim_data,anim_size);
}

void editor_updatetextureresx(char* texture_name,int16_t textureWidth,int16_t textureHeight,unsigned char* image_data,unsigned long data_size)
{
	settexture_resx(&editor_ptr->resx_mngr,texture_name,textureWidth,textureHeight,image_data,data_size);
}

void editor_updateanimresx(char* anim_name,unsigned char* anim_data,size_t anim_size)
{
	setanimation_resx(&editor_ptr->resx_mngr,anim_name,anim_data,anim_size);
}

void editor_updatespriteanimations(char* anim_name)
{
	Levelobject_updatesprite_animation(&editor_ptr->editor_level,&editor_ptr->resx_mngr,anim_name);
}

void editor_updatespritetextures(char* texture_name)
{
	Levelobject_updatesprite_texture(&editor_ptr->editor_level,&editor_ptr->resx_mngr,texture_name);
}

void editor_setpacker(unsigned char* packer_data,size_t packer_size)
{
	resx_set_from_rawpacker(&editor_ptr->resx_mngr,packer_data,packer_size);
}

void editor_addsoundresx(char* sound_name,const char* sound_path)
{
	add_resx(&editor_ptr->resx_mngr,sound_name,sound,sound_path,wfalse,NULL,NULL);
}

void editor_updatesoundresx(const char* sound_name)
{
	int snd_indx = hashtable_index(editor_ptr->resx_mngr.resx_hash,sound_name);


	sound_sample_t* old_ptr = (sound_sample_t*)editor_ptr->resx_mngr.resx_list[snd_indx].pointer;//get the pointer this way because getsound_resx auto load the sound data

	if(old_ptr == NULL)
		return;

	uint32_t snd_count = 0;
	uint32_t snd_index = 0;

	while(snd_count <  editor_ptr->snd_mngr.snd_hash.num_elem)
	{
		if(editor_ptr->snd_mngr.snd_hash.keys[snd_index])
		{
			snd_count++;

			sound_t* snd_tmp = &editor_ptr->snd_mngr.snd_array[snd_index];

			if(snd_tmp->sample == old_ptr)
			{
				atomic_set_value(&snd_tmp->force_ended,1);
				atomic_set_value(&snd_tmp->rewind,0);
			}
		}

		snd_index++;
	}

	volatile uint32_t add_counter = 0;

	atomic_set_value(&add_counter,editor_ptr->snd_mngr.add_counter);

	for(uint32_t a = 0; a < add_counter;a++)
	{
		if(editor_ptr->snd_mngr.snd_add_array[a].sample == old_ptr)
		{
			editor_ptr->snd_mngr.snd_add_array[a].force_ended = 1;
		}
	}


	free_resx_ptr(&editor_ptr->resx_mngr,sound,snd_indx);

	//reload sound
	sound_sample_t* new_ptr = getsound_resx(&editor_ptr->resx_mngr,sound_name);

	snd_count = 0;
	snd_index = 0;

	int sound_reloaded = 0;

	while(snd_count <  editor_ptr->snd_mngr.snd_hash.num_elem)
	{
		if(editor_ptr->snd_mngr.snd_hash.keys[snd_index])
		{
			snd_count++;

			sound_t* snd_tmp = &editor_ptr->snd_mngr.snd_array[snd_index];

			if(snd_tmp->sample == old_ptr)
			{
				snd_tmp->sample = new_ptr;
				atomic_set_value(&snd_tmp->rewind,1);
				sound_reloaded++;
			}
		}

		snd_index++;
	}

	atomic_set_value(&add_counter,editor_ptr->snd_mngr.add_counter);

	for(uint32_t a = 0; a < add_counter;a++)
	{
		if(editor_ptr->snd_mngr.snd_add_array[a].sample == old_ptr)
		{
			editor_ptr->snd_mngr.snd_add_array[a].sample = new_ptr;
			editor_ptr->snd_mngr.snd_add_array[a].force_ended = 1;
			sound_reloaded++;
		}
	}

	logprint("Reloaded %s sound file and %d sound sample",sound_name,sound_reloaded);
}

void editor_addshaderresx(char* shader_name,const char* shader_path)
{
	add_resx(&editor_ptr->resx_mngr,shader_name,shader,shader_path,wfalse,NULL,NULL);
}

void editor_updateshaderresx(const char* shader_name)
{
	reloadshader_resx(&editor_ptr->resx_mngr,shader_name);
}

#endif
