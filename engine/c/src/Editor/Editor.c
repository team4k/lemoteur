#ifndef STRIP_EDIT_CODE

#ifdef _MSC_VER
extern "C" {
#endif
#include <lua.h>

#ifdef _MSC_VER
}
#endif


#if defined(_WIN32)
#include <windows.h>
//#include <mscoree.h>
#include <eh.h>
#include <Psapi.h>
//#pragma comment(lib, "mscoree.lib")
#pragma comment(lib,"Psapi.lib")
#endif


#include <Render.h>
#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <time.h>
#include <string.h>
#include <limits.h>
#include <math.h>
#include <sqlite3.h>




#include <Engine.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>
#include <Base/geom.h>

#include <Render/render_manager.h>


#include <Debug/geomdebug.h>

#include <Graphics/TextureLoader.h>
#include <Resx/shader_loader.h>

#include <Utils/hash.h>
#include <Utils/hashmap.h>

#include <Threading/thread_func.h>
#include <Pathfinding/pathfind_worker.h>

#include <GameObjects/Characters.pb-c.h>
#include <Graphics/Animation.h>
#include <Graphics/Sprite.h>
#include <Graphics/Mesh.h>
#include <Graphics/Light.h>
#include <Graphics/LightManager.h>

#include <Graphics/TextureRegion.h>
#include <Graphics/SpriteBatch.h>

#include "GameObjects/Level.pb-c.h"

#include <portaudio.h>
#include <Sound/Sound.h>

#include <Graphics/FontLoader.h>

#include <Resx/Resources.h>
#include <Resx/localization.h>
#include <GameMngr/save_mngr.h>

#include "Debug/LogPrint.h"
#include "Graphics/TilemapLayer.h"
#include "GameObjects/Tilemap.h"

#include <Sound/snd_mngr.h>

#include <Font/RenderText.h>

#include <chipmunk/chipmunk.h>


#include <Debug/ChipmunkDebugDraw.h>

#include <Physics/Physics.h>
#include <Physics/physics_utils.h>


#include <GameObjects/entity.h>
#include <GameObjects/Trigger.h>
#include <GameObjects/text_entity.h>

#include <GameObjects/entity_group.h>

#include <Graphics/Effects/particle_object.h>
#include <GameObjects/particles_entity.h>


#include <Config/config.h>
#include <Config/shader_config.h>
#include <GameMngr/input_mngr.h>
#include <game_functions.h>
#include <GameMngr/module_mngr.h>


#include <GameObjects/map.h>

#include <GameObjects/Collider.h>
#include <GameObjects/waypoint.h>

#include <GameObjects/Levelobject.h>


#include <Scripting/ScriptEngine.h>

#include <Graphics/Effects/screen_fill.h>
#include <Graphics/Effects/screen_fade.h>

#include <Editor/editor_selector.h>

#include <Editor/Editor.h>
#include <Editor/editor_entities.h>
#include <Editor/editor_game.h>

#include <string>
#include <vector>
#include <map>
#include <fstream>
#include <iostream>

using namespace std;



static editor_t* editor_ptr = NULL;


static void editor_callback_script2(const char* id, const char* func, uint32_t parent,const char* params,void* return_value,size_t return_size)
{
	uint32_t real_parent = 0;

	if (parent == 0)
	{
		real_parent = editor_ptr->editor_level.script_id;
	}
	else
	{
		real_parent = parent;
	}

	char buff[256];

	if(params && strcmp(params,"") != 0)
		w_sprintf(buff,256,"return %s(Entities[%d]['%s'],%s);\n",func, real_parent,id,params);
	else
		w_sprintf(buff,256,"return %s(Entities[%d]['%s']);\n",func, real_parent,id);


	const char* result = script_execstring(editor_ptr->lua_state,buff,return_value,return_size);


	if(result != NULL)
		w_strcpy(editor_ptr->lua_log_buffer,10000,result);
}

static void editor_callback_texte(const char* id,const char* func, uint32_t parent,const char* params)
{
	if (parent == 0)
	{
		parent = editor_ptr->editor_level.script_id;
	}

	char buff[256];


	if(params && strcmp(params,"") != 0)
		w_sprintf(buff,256,"%s(Text[%d]['%s'],%s);\n",func,parent,id,params);
	else
		w_sprintf(buff,256,"%s(Text[%d]['%s']);\n",func,parent,id);

	const char* result = script_execstring(editor_ptr->lua_state,buff,NULL,0);

	if(result != NULL)
		w_strcpy(editor_ptr->lua_log_buffer,10000,result);

}



//handling c/c++ exception code from http://www.drdobbs.com/tools/postmortem-debugging/185300443
void   LogStackFrames(FILE* sgLogFile, PVOID FaultAddress, char *eNextBP,MODULEINFO mi)
{      
	   char *p, *pBP;                                     
       unsigned i, x, BpPassed;
       static int  CurrentlyInTheStackDump = 0;

	   int translated_address = (int)FaultAddress -  (int)mi.lpBaseOfDll;


       BpPassed = (eNextBP != NULL);
       if(! eNextBP)
       {
          _asm mov     eNextBP, eBp   
       }
       else 
           fprintf(sgLogFile, "\n  Fault Occurred At $ADDRESS:%08X\n", 
                                   translated_address);
                             // prevent infinite loops
       for(i = 0; eNextBP && i < 100; i++)
       {      
           pBP = eNextBP;           // keep current BasePointer
           eNextBP = *(char **)pBP; // dereference next BP 
           p = pBP + 8; 

		   translated_address = 0;

		   if(*(char **)(pBP + 4) != NULL)
		   {
			   char str_addr[256];
			   w_sprintf(str_addr,256,"%p",*(char **)(pBP + 4));
			   int raw_address = (int)strtol(str_addr,NULL,16);
			   translated_address = raw_address - (int)mi.lpBaseOfDll;
		   }

           // Write 20 Bytes of potential arguments
           fprintf(sgLogFile, "         with ");        
           for(x = 0; p < eNextBP && x < 20; p++, x++)
               fprintf(sgLogFile, "%02X ", *(unsigned char *)p);
           fprintf(sgLogFile, "\n\n");          
                  if(i == 1 && ! BpPassed) 
               fprintf(sgLogFile, "*************************************\n"
                                 "         Fault Occurred Here:\n");
        // Write the backjump address
        fprintf(sgLogFile, "*** %2d called from $ADDRESS:%08X\n", 
                                  i, translated_address);
        if(*(char **)(pBP + 4) == NULL)
            break; 
	   }

}


static const int   ADDRESSLENGTH = 8; 
static int has_map = -1;
typedef map<long, string>CSymbolMap;
CSymbolMap loaded_map;
static const char *ADDRESS_TAG   = "$ADDRESS:";
typedef unsigned int exception_code_t;

static unsigned long AddressStrToLong(char *str)
{   unsigned long a;
    char tx[1024];
          
    w_strncpy(tx,1024, str, ADDRESSLENGTH);
    tx[ADDRESSLENGTH] = 0;    
    a = 0;
    sscanf_s(tx, "%lx" , &a);
    return a;
}

//load generated /MAP file and associate symbol name with address value
static int LoadMapFile(const string &mapFilePath,CSymbolMap &symbolMap)
{
#ifdef WIN32
    const char *MAPSTARTLINE = "  Address";
    const char *MAPENDLINE   = " entry point at";

    const int   ADDR_START_MIN_OFF = 46;
    const int   SYMBOL_START_OFF   = 21;
#else
    const int   ADDR_START_OFF = 18;
    const int   SYMBOL_START_OFF   = 20;
#endif
    
    ifstream mapFile;
    mapFile.open(mapFilePath.c_str(), ios_base::in);  

    if(! mapFile.is_open())
    {
        cout << "Unable to open file \"" << mapFilePath << "\"\n";
		return -1;
    }

    cout << "Reading Mapfile \"" << mapFilePath << "\"\n";
    
    char buff[1024];
    int  inAddrList = 0;
    while(mapFile.getline(buff, sizeof(buff)))
    {
        if(mapFile.eof())
            break;

        char *p = buff;

#ifdef WIN32
        // Check if we are in the right section of the file
        if(! inAddrList)
        {
            if(! _strnicmp(p, MAPSTARTLINE, strlen(MAPSTARTLINE)))
                inAddrList = 1;
            continue;
        }
        if(! _strnicmp(p, MAPENDLINE  , strlen(MAPENDLINE  )))
            break;

        // Find Position of address and convert to long
        char *p2 = p + ADDR_START_MIN_OFF;
        while(*p2 && *p2 != ' ' && *p2 != '\n')
            p2++;
        while(*p2 == ' ')
            p2++;
#else
    	if(strlen(p) < ADDR_START_OFF || strncmp(p + ADDR_START_OFF - 2, "0x", 2))
	    	continue;

        *(p + ADDR_START_OFF - 3) = 0;
        if(! StrIsBlank(p))
            continue;

        char *p2 = p + ADDR_START_OFF;


#endif

        char tx[1024], tx2[1024];
        w_strncpy(tx,1024, p2, ADDRESSLENGTH);
        tx[ADDRESSLENGTH] = 0;

        long l = AddressStrToLong(tx);

        // Add Entry
		//Only register function in memory section 1 for now, TODO: take into account the memory section when registering address
        if(l && l >= 0x10000000)
        {
			l -= 0x10000000;
            p2 = p + SYMBOL_START_OFF;
            w_strcpy(tx2,1024, p2); 
            symbolMap.insert(make_pair(l, tx2));
        }
    }
    mapFile.close();

    cout << symbolMap.size() << " Symbols found\n";

    return 0;
}

//add symbol to existing log file
static int AddSymbols(const string &inFilePath, const CSymbolMap &symbolMap)

{   ifstream inFile;
    ofstream outFile;

    // Open the files
    string outFilePath = inFilePath;
    int n = outFilePath.rfind(".");
    if(n)
        outFilePath = outFilePath.substr(0, n);
    outFilePath += ".out";

    inFile.open(inFilePath.c_str(), ios_base::in);  
    if(! inFile.is_open())
    {
        cout << "Unable to open file \"" << inFilePath << "\"\n";
        exit(-1);
    }

    outFile.open(outFilePath.c_str(), ios_base::out);  
    if(! outFile.is_open())
    {
        cout << "Unable to open file \"" << inFilePath << "\"\n";
        return -1;
    }
    
    cout << "Reading Logfile \"" << inFilePath << "\"\n";
    cout << "Adding symbolnames to file \"" << outFilePath << "\"\n";

    char buff[1024];
    int count = 0;
    while(inFile.getline(buff, sizeof(buff)))
    {
        outFile << buff << "\n";
        char *p = strstr(buff, ADDRESS_TAG);

        if(p)
        {
            long l = AddressStrToLong(p + strlen(ADDRESS_TAG));
            CSymbolMap::const_iterator it;

            it = symbolMap.lower_bound(l);
            if(it != symbolMap.end())
               it--;

            if(it != symbolMap.end())
            {
                char tx1[255], tx2[255];
                w_sprintf(tx1,255, "%08X", l);
                w_sprintf(tx2,255, "%08X", it->first);
                string s(p - buff, ' ');
                outFile << s;                
                outFile << "======== " << tx1 << " == belongs to " << it->second << "\n";
                outFile << s; 
                long l2 = l - it->first;
                if(l2 >= 0)
                {
                    w_sprintf(tx1,255, "%8d", l2);
                    outFile << "       + " << tx1 << " bytes\n";
                }
                count++;
            }
        }
    }
    cout << count << " Symbols found\n";

    inFile.close();
    outFile.close();

    return 0;
}


static const char* seDescription( const exception_code_t& code )
{
    switch( code ) {
        case EXCEPTION_ACCESS_VIOLATION:         return "EXCEPTION_ACCESS_VIOLATION"         ;
        case EXCEPTION_ARRAY_BOUNDS_EXCEEDED:    return "EXCEPTION_ARRAY_BOUNDS_EXCEEDED"    ;
        case EXCEPTION_BREAKPOINT:               return "EXCEPTION_BREAKPOINT"               ;
        case EXCEPTION_DATATYPE_MISALIGNMENT:    return "EXCEPTION_DATATYPE_MISALIGNMENT"    ;
        case EXCEPTION_FLT_DENORMAL_OPERAND:     return "EXCEPTION_FLT_DENORMAL_OPERAND"     ;
        case EXCEPTION_FLT_DIVIDE_BY_ZERO:       return "EXCEPTION_FLT_DIVIDE_BY_ZERO"       ;
        case EXCEPTION_FLT_INEXACT_RESULT:       return "EXCEPTION_FLT_INEXACT_RESULT"       ;
        case EXCEPTION_FLT_INVALID_OPERATION:    return "EXCEPTION_FLT_INVALID_OPERATION"    ;
        case EXCEPTION_FLT_OVERFLOW:             return "EXCEPTION_FLT_OVERFLOW"             ;
        case EXCEPTION_FLT_STACK_CHECK:          return "EXCEPTION_FLT_STACK_CHECK"          ;
        case EXCEPTION_FLT_UNDERFLOW:            return "EXCEPTION_FLT_UNDERFLOW"            ;
        case EXCEPTION_ILLEGAL_INSTRUCTION:      return "EXCEPTION_ILLEGAL_INSTRUCTION"      ;
        case EXCEPTION_IN_PAGE_ERROR:            return "EXCEPTION_IN_PAGE_ERROR"            ;
        case EXCEPTION_INT_DIVIDE_BY_ZERO:       return "EXCEPTION_INT_DIVIDE_BY_ZERO"       ;
        case EXCEPTION_INT_OVERFLOW:             return "EXCEPTION_INT_OVERFLOW"             ;
        case EXCEPTION_INVALID_DISPOSITION:      return "EXCEPTION_INVALID_DISPOSITION"      ;
        case EXCEPTION_NONCONTINUABLE_EXCEPTION: return "EXCEPTION_NONCONTINUABLE_EXCEPTION" ;
        case EXCEPTION_PRIV_INSTRUCTION:         return "EXCEPTION_PRIV_INSTRUCTION"         ;
        case EXCEPTION_SINGLE_STEP:              return "EXCEPTION_SINGLE_STEP"              ;
        case EXCEPTION_STACK_OVERFLOW:           return "EXCEPTION_STACK_OVERFLOW"           ;
        default: return "UNKNOWN EXCEPTION" ;
    }
}


LONG Win32FaultHandler(struct _EXCEPTION_POINTERS *  ExInfo)
{   
	const char* FaultTx = seDescription(ExInfo->ExceptionRecord->ExceptionCode); 

	FILE* sgLogFile = NULL;

    w_fopen(sgLogFile,"Win32Fault.log", "w");
    int    wsFault    = ExInfo->ExceptionRecord->ExceptionCode;
    PVOID  CodeAdress = ExInfo->ExceptionRecord->ExceptionAddress;

	HMODULE hm;
	GetModuleHandleEx(GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS,(LPCWSTR)ExInfo->ExceptionRecord->ExceptionAddress,&hm);
	MODULEINFO mi;
	GetModuleInformation(GetCurrentProcess(),hm,&mi,sizeof(mi));
	char fn[MAX_PATH];
     GetModuleFileNameExA(GetCurrentProcess(), hm, fn, MAX_PATH );

	long translated_address = (int)CodeAdress - (int)mi.lpBaseOfDll;

    if(sgLogFile != NULL)
    {
       fprintf(sgLogFile, "****************************************************\n");
       fprintf(sgLogFile, "*** A Program Fault occurred:\n");
	   fprintf(sgLogFile,"*** Module path: %s raw address: %08X base address: %08X",fn,(int)CodeAdress,(int)mi.lpBaseOfDll);
       fprintf(sgLogFile, "*** Error code %08X: %s\n", wsFault, FaultTx);
       fprintf(sgLogFile, "****************************************************\n");
	   fprintf(sgLogFile, "***   Address: %08X\n", translated_address);
       fprintf(sgLogFile, "***     Flags: %08X\n", 
                                     ExInfo->ExceptionRecord->ExceptionFlags);
       LogStackFrames(sgLogFile,CodeAdress, (char *)ExInfo->ContextRecord->Ebp,mi);
       fclose(sgLogFile);
    }

	//if we have a map file, replace raw address with symbol name

	if(has_map != -1)
		AddSymbols("Win32Fault.log",loaded_map);

	MessageBox(0,L"Erreur dans le code C !",L"Erreur",MB_OK);


    return EXCEPTION_EXECUTE_HANDLER;
}

void editor_init()
{
	if(editor_ptr == NULL)
		editor_ptr = (editor_t*)wf_calloc(1,sizeof(editor_t));

	memset(&editor_ptr->phy_mgr,0,sizeof(physics_manager_t));
	memset(&editor_ptr->resx_mngr,0,sizeof(resources_manager_t));
	editor_ptr->play_mode = wfalse;
	editor_ptr->show_debug_col = wfalse;
	editor_ptr->show_colliders_triggers_misc = wfalse;
	editor_ptr->has_filler = wfalse;
	editor_ptr->has_filler_sibling = wfalse;
	editor_ptr->enable_lighting = wfalse;

	editor_ptr->lastTime = 0;
	editor_ptr->cursor_color = ColorA(0.0f,0.0f,1.0f,0.5f);
	editor_ptr->select_color = ColorA(1.0f,0.0f,0.0f,0.5f);
	editor_ptr->cursor_pos = Vec(0.0f,0.0f);
	editor_ptr->end_cursor_pos = Vec(0.0f,0.0f);
	editor_ptr->tile_size = 32;
	editor_ptr->show_cursor = wtrue;
	editor_ptr->show_selector = wtrue;

	editor_ptr->back_color = ColorA(0.0f,0.0f,0.0f,1.0f);

	editor_ptr->grid_color = ColorA(1.0f,0.0f,0.0f,1.0f);

	editor_ptr->fps_color = ColorA(0.5f,0.5f,0.5f,1.0f);

	editor_ptr->show_grid = wfalse;

	editor_ptr->overtime = 0;
	editor_ptr->count_frame = 0;

	editor_ptr->max_nsecframe_time = (long)(max_frame_time * 1000000L);

	editor_ptr->time_elapsed = 0;
	editor_ptr->editor_position = Vec(0.0f,0.0f);

	editor_ptr->call_script = wfalse;
	editor_ptr->sleep_time = 0;
	editor_ptr->frameRenderTime = 0;

	editor_ptr->backup_level = NULL;

	editor_ptr->sound_source_sprite = NULL;
	editor_ptr->light_sprite = NULL;
	editor_ptr->particle_sprite = NULL;
	editor_ptr->waypoint_sprite = NULL;
	editor_ptr->collider_sprite = NULL;

	//set ptr 
	editor_render_setptr(editor_ptr);
	editor_game_setptr(editor_ptr);
	editor_resx_setptr(editor_ptr);
	editor_entities_setptr(editor_ptr);
	editor_light_setptr(editor_ptr);
	editor_sound_setptr(editor_ptr);
}


#if defined(_WIN32)
wbool editor_start_engine(HWND hwnd,int16_t width,int16_t height,int player_start_x,int player_start_y,int startX,int startY,const char* assets_path,const char* game_dll_path,const char* font_name,const char* light_prog,const char* shadow_prog,const char* mask_prog,const char* raw_assets_path)
#else
void editor_start_engine(int width,int height,int startX,int startY,const char* assets_path)
#endif
{

	//postmortem debugging setup

	//check if we have a map file available
	string mapFilePath = "WafaEngine.map";

	has_map = LoadMapFile(mapFilePath, loaded_map);

	//set the exception handler
	SetUnhandledExceptionFilter((LPTOP_LEVEL_EXCEPTION_FILTER) Win32FaultHandler);


	editor_ptr->sound_source_sprite = NULL;
	editor_ptr->light_sprite = NULL;
	editor_ptr->particle_sprite = NULL;

	logprint("initialize engine");

	 logprint_setdir(editor_ptr->app_path);

	 editor_selector_init(&editor_ptr->selector);

	 //init scripting engine
	editor_ptr->lua_state = script_createstate();
	editor_ptr->editor_module_mngr = (module_mngr_t*)wf_calloc(1, sizeof(_module_mngr));

	const char* log_return = NULL;
	//add our script folder path to lua package path
	char package_path[500];
	char final_package_path[1000];

			#if defined(_WIN32)
				w_sprintf(package_path,500,"package.path = '%s\\?.lua;' .. package.path",raw_assets_path);

				str_double_slash(&package_path[0],final_package_path,"\\");
			#else
				sprintf(final_package_path,"package.path = '%s/?.lua;' .. package.path",raw_assets_path);
			#endif

	log_return  = script_execstring(editor_ptr->lua_state,final_package_path,NULL,0);

	if(log_return != NULL)
		logprint(log_return);
	else
		logprint("Chemin scripts lua ajoute au package.path");

	config_init(&editor_ptr->config,editor_ptr->lua_state);

	char config_path[2048];
	w_sprintf(config_path,2048,"%sconfig.lua",editor_ptr->app_path);

	config_readconf(&editor_ptr->config,&editor_ptr->resx_mngr,&config_path[0]);

	wbool force_ffp = wfalse;

	config_getbool(&editor_ptr->config,"force_ffp",&force_ffp);

	int filter_val = 0;
	config_getint(&editor_ptr->config,"texture_filtering",&filter_val);

	texture_filter filter = (texture_filter)filter_val;

	int16_t fps_limit = 60;
	config_getshort(&editor_ptr->config, "fps_limit", &fps_limit);

	render_init(&editor_ptr->render_mngr,width,height,editor_ptr->app_path,"editor",NULL,hwnd,wtrue,wfalse,filter, fps_limit);

	float mouse_sensitivity = 1.0f;
	wbool lock_cursor = wfalse;
	config_getfloat(&editor_ptr->config,"mouse_sensitivity",&mouse_sensitivity);
	config_getbool(&editor_ptr->config,"lock_cursor",&lock_cursor);

	input_mngr_init(&editor_ptr->input_mngr,&editor_ptr->render_mngr,editor_ptr->editor_module_mngr,mouse_sensitivity,lock_cursor,wtrue,wfalse);

	render_swap_buffers(&editor_ptr->render_mngr);


	//setup ressources manager
	if(strlen(assets_path) == 0)
		get_app_path(editor_ptr->resources_path);
	else
		w_strcpy(editor_ptr->resources_path,WAFA_MAX_PATH,assets_path);


	init_resources(&editor_ptr->resx_mngr,&editor_ptr->resources_path[0],&editor_ptr->render_mngr);

	char packer_file[128];

	if(config_getstring(&editor_ptr->config,"packer",packer_file,128))
		resx_set_from_packer(&editor_ptr->resx_mngr, packer_file);


	Render_Text_Init(&editor_ptr->fps_text,100,&editor_ptr->render_mngr);

	font_info* inf = getfont_resx(&editor_ptr->resx_mngr,font_name);

	Render_Text_Setinfo(&editor_ptr->fps_text,inf);
	Render_Text_setfixedsize(&editor_ptr->fps_text,wtrue);

	swprintf(editor_ptr->fps,260,L"fps : 0 ");


	//setup physics engine
	physics_init(&editor_ptr->phy_mgr,wfalse);

	editor_ptr->editor_position.x  = (float)startX;
	editor_ptr->editor_position.y = (float)startY;

	//init level object
	
	Levelobject_Init(&editor_ptr->editor_level,&editor_ptr->light_mngr,&editor_ptr->snd_mngr,&editor_ptr->path_worker);
	lvlbase_init(&editor_ptr->editor_lvl_base, &editor_ptr->editor_level, NULL, L_STATIC);

	//init sound manager
	snd_mngr_init(&editor_ptr->snd_mngr,AUDIO_FRAMES_PER_BUFFER * AUDIO_CHANNELS_NUM,0.05f);

	//init localisation
	char db_path[2048];

	w_sprintf(db_path,2048,"%slocale.db",editor_ptr->app_path);

	localization_init(&editor_ptr->editor_localobj,db_path);

	
	char locale[256];

	if(config_getstring(&editor_ptr->config,"locale",&locale[0],256))
		localization_setlocale(&editor_ptr->editor_localobj,locale);
	else
	{
		#ifndef ANDROID_BUILD
			setlocale(LC_NUMERIC, "C");//always force numeric locale to C
		#endif
	}

	char filler_name[128] = { 0 };

	if (config_getstring(&editor_ptr->config, "filler", filler_name, 128)) {

		char filler_texture[128] = { 0 };
		char filler_animation[128] = { 0 };

		w_sprintf(filler_texture, 128, "%s.png", filler_name);
		w_sprintf(filler_animation, 128, "%s.awf", filler_name);

		editor_ptr->has_filler = (wbool)(has_resx(&editor_ptr->resx_mngr, filler_texture) && has_resx(&editor_ptr->resx_mngr, filler_animation));

		if (editor_ptr->has_filler) {
			config_getbool(&editor_ptr->config, "filler_sibling", &editor_ptr->has_filler_sibling);
		}
	}

	

	save_mngr_init(&editor_ptr->editor_savemngr,&editor_ptr->app_path[0],"",0);



	script_createglobalmngr(editor_ptr->lua_state);
	script_pushmngr(editor_ptr->lua_state,"static_physics_mngr",&editor_ptr->phy_mgr);
	script_pushmngr(editor_ptr->lua_state,"level_mngr",&editor_ptr->editor_lvl_base);
	script_pushmngr(editor_ptr->lua_state,"callback_entity",&editor_callback_script2);
	script_pushmngr(editor_ptr->lua_state,"callback_texte",&editor_callback_texte);
	script_pushmngr(editor_ptr->lua_state,"sound_mngr",&editor_ptr->snd_mngr);
	script_pushmngr(editor_ptr->lua_state,"resx_mngr",&editor_ptr->resx_mngr);
	script_pushmngr(editor_ptr->lua_state,"localization",&editor_ptr->editor_localobj);
	script_pushmngr(editor_ptr->lua_state,"config",&editor_ptr->config);
	script_pushmngr(editor_ptr->lua_state,"render_mngr",&editor_ptr->render_mngr);
	script_pushmngr(editor_ptr->lua_state,"module_mngr",&editor_ptr->editor_module_mngr);
	script_pushmngr(editor_ptr->lua_state,"screen_fader",&editor_ptr->screen_fader);
	script_pushmngr(editor_ptr->lua_state,"save_mngr",&editor_ptr->editor_savemngr);
	script_pushmngr(editor_ptr->lua_state,"config_path",&config_path[0]);
	script_pushmngr(editor_ptr->lua_state,"light_mngr",&editor_ptr->light_mngr);
	script_pushmngr(editor_ptr->lua_state,"pathfind_worker",&editor_ptr->path_worker);
	script_pushmngr(editor_ptr->lua_state, "input_mngr", &editor_ptr->input_mngr);

	if(editor_ptr->has_filler)
		script_pushmngr(editor_ptr->lua_state,"screen_filler",&editor_ptr->screen_filler);

	if(editor_ptr->has_filler_sibling)
		script_pushmngr(editor_ptr->lua_state, "screen_filler_sibling", &editor_ptr->screen_filler_sibling);


	editor_ptr->gdll_playerpos.x = player_start_x;
	editor_ptr->gdll_playerpos.y = player_start_y;

	w_strcpy(&editor_ptr->gdll_font[0],260,font_name);

	if (!editor_load_gamedll(game_dll_path))
	{
		logprint("Can't initialize gamedll");
		return wfalse;
	}

	int light_t = 0;//light top down

	config_getint(&editor_ptr->config,"light_model",&light_t);

	light_model l_t = (light_model)light_t;

	//init light manager
	ColorA_t ambient = {0.0f,0.0f,0.0f,0.7f};
	light_manager_init(&editor_ptr->light_mngr,ambient,&editor_ptr->render_mngr,render_get_program(&editor_ptr->render_mngr,mask_prog),render_get_program(&editor_ptr->render_mngr,light_prog),render_get_program(&editor_ptr->render_mngr,shadow_prog),l_t,1024,1024,10,10);

	editor_ptr->editor_module_mngr->game_initlights(&editor_ptr->light_mngr);


	//init screen filler
	if(editor_ptr->has_filler)
	{
		Vector_t dir = {0,0};

		if (config_getstring(&editor_ptr->config, "filler", filler_name, 128)) {

			char filler_texture[128] = { 0 };
			char filler_animation[128] = { 0 };

			w_sprintf(filler_texture, 128, "%s.png", filler_name);
			w_sprintf(filler_animation, 128, "%s.awf", filler_name);

			screen_fill_init(&editor_ptr->screen_filler, 5, 250, dir, gettexture_resx(&editor_ptr->resx_mngr, filler_texture), 64, 64, &editor_ptr->render_mngr, getanimation_resx(&editor_ptr->resx_mngr, filler_animation), NULL, NULL);

			if (editor_ptr->has_filler_sibling) {
				screen_fill_init(&editor_ptr->screen_filler_sibling, 5, 250, dir, gettexture_resx(&editor_ptr->resx_mngr, filler_texture), 64, 64, &editor_ptr->render_mngr, getanimation_resx(&editor_ptr->resx_mngr, filler_animation), NULL, NULL);
				editor_ptr->screen_filler.has_sibling = wtrue;
			}
		}
	}

	screen_fade_init(&editor_ptr->screen_fader);

	ChipmunkDebugDrawInit(&editor_ptr->render_mngr);


	logprint("end initialize engine");
	return wtrue;
}

void editor_load_level(unsigned char* level_data, size_t level_size)
{
	//clean up previous loaded level
	if (editor_ptr->editor_level.pending_binary != NULL) {
		editor_ptr->editor_module_mngr->game_cleanup(wfalse);
		Levelobject_clean_level(&editor_ptr->editor_level, &editor_ptr->resx_mngr, &editor_ptr->phy_mgr, editor_ptr->lua_state);
		script_callgc(editor_ptr->lua_state);

		level__free_unpacked(editor_ptr->editor_level.pending_binary, NULL);
		editor_ptr->editor_level.pending_binary = NULL;
	}

	Level* level_obj = level__unpack(NULL, level_size, level_data);

	start_resx_collect(&editor_ptr->resx_mngr);

	gettexture_resx(&editor_ptr->resx_mngr, "sound_high_icon_32.png");
	gettexture_resx(&editor_ptr->resx_mngr, "lightbulb_icon_32.png");
	gettexture_resx(&editor_ptr->resx_mngr, "star_icon_32.png");
	gettexture_resx(&editor_ptr->resx_mngr, "collider.png");
	gettexture_resx(&editor_ptr->resx_mngr, "waypoint.png");
	getfont_resx(&editor_ptr->resx_mngr, "visitor1.json");

	editor_ptr->editor_module_mngr->game_keepresources();

	char filler_name[128] = { 0 };

	if (config_getstring(&editor_ptr->config, "filler", filler_name, 128))
	{
		char filler_texture[128] = { 0 };
		char filler_animation[128] = { 0 };

		w_sprintf(filler_texture, 128, "%s.png", filler_name);
		w_sprintf(filler_animation, 128, "%s.awf", filler_name);

		gettexture_resx(&editor_ptr->resx_mngr, filler_texture);
		getanimation_resx(&editor_ptr->resx_mngr, filler_animation);
	}


	Levelobject_load_level2(&editor_ptr->editor_level, &editor_ptr->resx_mngr, editor_ptr->editor_module_mngr, &editor_ptr->phy_mgr, editor_ptr->lua_state, &editor_ptr->editor_localobj, &editor_ptr->render_mngr, level_obj, editor_entities_get_trigger_callback(), vectorzero, NULL);

	end_resx_collect(&editor_ptr->resx_mngr);
		

	editor_ptr->editor_level.pending_binary = level_obj;
}


void editor_load_localedb()
{
	//init localisation
	char db_path[2048];

	w_sprintf(db_path,2048,"%slocale.db",editor_ptr->app_path);

	localization_init(&editor_ptr->editor_localobj,db_path);

	char locale[256];

	if(config_getstring(&editor_ptr->config,"locale",&locale[0],256))
		localization_setlocale(&editor_ptr->editor_localobj,locale);
}

void editor_release_localedb()
{
	localization_free(&editor_ptr->editor_localobj);
}

void editor_shutdown_engine()
{
	 //clean context

	render_stop(&editor_ptr->render_mngr);
	input_mngr_free(&editor_ptr->input_mngr);

	physics_free(&editor_ptr->phy_mgr);

    //exit program
    exit( EXIT_SUCCESS );
}

void editor_change_mode(wbool pmode)
{
	editor_ptr->play_mode = pmode;
	wbool repos = wfalse;

	//reset selector
	editor_ptr->selector.start_select.x = editor_ptr->selector.end_select.x;
	editor_ptr->selector.start_select.y = editor_ptr->selector.end_select.y;

	//reset cursor
	editor_ptr->cursor_pos.x = editor_ptr->end_cursor_pos.x;
	editor_ptr->cursor_pos.y = editor_ptr->end_cursor_pos.y;

	editor_ptr->editor_level.current_state = COMMON_RUN;

	if(!editor_ptr->play_mode)
	{

		//first, update position
		Levelobject_move_tostart(&editor_ptr->editor_level,&editor_ptr->phy_mgr);

		//next, update physic engine to get updated collisions boxes
		for(int32_t sr = 0; sr < sample_rate;sr++)
			update_physics(&editor_ptr->phy_mgr,(float)dt);

		//then, update render boxes
		Levelobject_updaterenderboxes(&editor_ptr->editor_level,&editor_ptr->render_mngr);
	}


	cpVect player_pos = editor_ptr->editor_module_mngr->game_get_playerpos(0);

	
	
	//reposition our character so it's not out of map
	if(player_pos.x == 0 && player_pos.y == 0)//at startup
	{
		repos = wtrue;
	}
	else
	{

		if(editor_ptr->editor_level.current_map->tilemap.collisions_array != NULL)
		{
			//check if we are on a collidable tile
			int32_t tile_col = (int32_t)floorf((float)(player_pos.x  / editor_ptr->editor_level.current_map->tilemap.tile_size));
			int32_t tile_row = (int32_t)floorf((float)(player_pos.y  / editor_ptr->editor_level.current_map->tilemap.tile_size));

			int32_t col_indx = (tile_row * editor_ptr->editor_level.current_map->tilemap.col_count) + tile_col;

			if(editor_ptr->editor_level.current_map->tilemap.collisions_array[col_indx] != 0)
				repos = wtrue;
		}

	}

	if(repos)
	{
		if(editor_ptr->editor_level.current_map->tilemap.collisions_array != NULL)
		{
			for(int32_t col_indx = 0; col_indx < (editor_ptr->editor_level.current_map->tilemap.col_count * editor_ptr->editor_level.current_map->tilemap.row_count) ;col_indx++)
			{
				if(editor_ptr->editor_level.current_map->tilemap.collisions_array[col_indx] == 0)//change player position to the first non collidable tile
				{
					int32_t tile_row = (col_indx / editor_ptr->editor_level.current_map->tilemap.col_count);//ligne Y
					int32_t tile_col = col_indx - (editor_ptr->editor_level.current_map->tilemap.col_count * tile_row);//ligne X

					cpVect newpos = cpv((tile_col * editor_ptr->editor_level.current_map->tilemap.tile_size) + (editor_ptr->editor_level.current_map->tilemap.tile_size / 2),(tile_row * editor_ptr->editor_level.current_map->tilemap.tile_size) + (editor_ptr->editor_level.current_map->tilemap.tile_size / 2));

					editor_ptr->editor_module_mngr->game_player_reposition(newpos);

					break;
				}
			}
		}

		
	}

	editor_ptr->editor_module_mngr->game_editor_reset();

}




void editor_send_inputmessage(uint32_t msg,uint32_t wparam,long lparam)
{
	input_mngr_sendinputmessage(&editor_ptr->input_mngr,msg,wparam,lparam);
}



  //************************//
 // scripting functions    //
//************************//

void editor_exec_string(const char* buffer,char* return_value,size_t return_buffer_size)
{
	const char* result = script_execstring(editor_ptr->lua_state,buffer, return_value, return_buffer_size);

	if(result != NULL)
	{
		w_strcpy(return_value, return_buffer_size,result);
	}
}

void editor_set_module(const char* buffer)
{
	w_strcpy(editor_ptr->editor_level.current_module,256,buffer);
}

void editor_get_lua_log(char* return_value,size_t return_size)
{
	if(strlen(editor_ptr->lua_log_buffer) != 0)
	{
		w_strcpy(return_value,return_size,editor_ptr->lua_log_buffer);
		w_strcpy(editor_ptr->lua_log_buffer,1,"\0");
	}
}

void editor_set_call_script(wbool pcall_script)
{
	editor_ptr->call_script = pcall_script;
}

void editor_reloadconfig()
{
	config_reload(&editor_ptr->config,"config.lua");
}

void editor_reloadshaderconfig()
{
	shader_config_load(&editor_ptr->render_mngr,editor_ptr->lua_state,&editor_ptr->resx_mngr);
}


#endif

