#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <stdlib.h>
#include <inttypes.h>
#include <math.h>
#include <time.h>

#if defined(_WIN32)
    #include <Windows.h>
#endif

#include <Engine.h>


#include <Base/types.h>
#include <Base/functions.h>


#include <Base/geom.h>
#include <Collisions/Collisions.h>



//check if rec_1 interesect with rec_2, if true return intersection rectangle
wbool col_check(const Rect_t* rec_1, const Rect_t* rec_2, Rect_t* intersect_result)
{
    float intersectMinX = wfmax(recget_minx(rec_1),recget_minx(rec_2));
    float intersectMinY = wfmax(recget_miny(rec_1),recget_miny(rec_2));
	float intersectMaxX = wfmin(recget_maxx(rec_1),recget_maxx(rec_2));
	float intersectMaxY = wfmin(recget_maxy(rec_1),recget_maxy(rec_2));


	// if the rects intersect, return our intersect result
	if (intersectMinX < intersectMaxX && intersectMinY < intersectMaxY)
	{
		intersect_result->width = (int16_t)(intersectMaxX - intersectMinX);
		intersect_result->height = (int16_t)(intersectMaxY - intersectMinY);
		
		if(intersect_result->width < COL_MIN_PUSHBACK)
			intersect_result->width = COL_MIN_PUSHBACK;

		if(intersect_result->height < COL_MIN_PUSHBACK)
			intersect_result->height = COL_MIN_PUSHBACK;

		intersect_result->position.x = intersectMinX + (intersect_result->width / 2);
		intersect_result->position.y = intersectMinY +  (intersect_result->height / 2);
        return wtrue;
	}
	
	return wfalse;
}

//check if rec_1 interesect with rec_2, true if intersection, false otherwise
wbool col_check2(const Rect_t* rec_1, const Rect_t* rec_2)
{
	float intersectMinX = wfmax(recget_minx(rec_1),recget_minx(rec_2));
    float intersectMinY = wfmax(recget_miny(rec_1),recget_miny(rec_2));
	float intersectMaxX = wfmin(recget_maxx(rec_1),recget_maxx(rec_2));
	float intersectMaxY = wfmin(recget_maxy(rec_1),recget_maxy(rec_2));

	// if the rects intersect, return our intersect result
	return (wbool)(intersectMinX < intersectMaxX && intersectMinY < intersectMaxY);
}

//merge 2 bounding boxes
void col_merge(Rect_t* const destination,const Rect_t* const src)
{
	destination->position.x = wfmin(recget_minx(destination),recget_minx(src));
	destination->position.y = wfmax(recget_maxy(destination),recget_maxy(src));
	destination->width = (int16_t) (wfmax(recget_maxx(destination),recget_maxx(src)) - destination->position.x);
	destination->height = (int16_t) (destination->position.y + wfmin(recget_miny(destination),recget_miny(src)));
}

//get the reflection vector between rec_1 and rec_2 - usually rec_1 is the static rectangle and rec_2 is the moving rectangle
void col_getreflectvector(const Rect_t* intersect,Vector_t* reflection)
{
	reflection->x = reflection->y = 1.0f;

	if(intersect->width < intersect->height)
		reflection->x = -1.0f;
	else if(intersect->width > intersect->height)
		reflection->y = -1.0f;
	else
		reflection->x = reflection->y = -1.0f;

}