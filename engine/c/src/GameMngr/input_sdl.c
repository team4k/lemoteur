#ifdef USE_SDL_INPUT

#ifdef _MSC_VER
       extern "C" {
#endif
	#include "lauxlib.h"
#include "lualib.h"
	#include <lua.h>

#ifdef _MSC_VER
}
#endif


#if defined(_WIN32)
        #include <windows.h>
#else
	#ifndef __USE_BSD
	        #define __USE_BSD
	#endif
        #include <unistd.h>
        #include <dlfcn.h>
#endif

#include <Render.h>

#include <inttypes.h>


#include <emscripten.h>
#include <SDL2/SDL.h>

#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <time.h>
#include <wchar.h>
#include <math.h>
#include <string.h>
#include <sqlite3.h>

#include <Engine.h>
#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>

#include <Base/geom.h>
#include <Utils/Hash.h>
#include <Utils/hashmap.h>

#include <Render/render_manager.h>

#include <GameObjects/Shaders.pb-c.h>
#include <Graphics/TextureLoader.h>
#include <Resx/shader_loader.h>

#include <GameObjects/Characters.pb-c.h>
#include <Graphics/Animation.h>
#include <Graphics/Sprite.h>
#include <Graphics/Mesh.h>

#include <Graphics/Light.h>
#include <Graphics/LightManager.h>

#include <Debug/Logprint.h>


#include <Graphics/TextureRegion.h>
#include <Graphics/SpriteBatch.h>
#include <Graphics/FontLoader.h>

#include "GameObjects/Level.pb-c.h"

#include <Sound/Sound.h>
       

#include <Resx/Resources.h>
#include <Resx/localization.h>

#include <Graphics/TilemapLayer.h>
#include <GameObjects/Tilemap.h>

#include <chipmunk/chipmunk.h>
#include <Physics/Physics.h>


#include <GameObjects/entity.h>
#include <GameObjects/Trigger.h>

#include <Config/config.h>

#include <GameMngr/input_mngr.h>
#include <game_functions.h>
#include <GameMngr/module_mngr.h>


void input_sdl_event(input_mngr_t* const input_mngr)
{
	SDL_Event sdlEvent;
	render_manager_t* rnd_manager = input_mngr->render_manager;
	module_mngr_t* mod_manager = input_mngr->module_manager;

    while( SDL_PollEvent( &sdlEvent ) ) {
        switch( sdlEvent.type ) {

        case SDL_CONTROLLERDEVICEADDED:
			for(int32_t ijoy = 0;ijoy < SDL_NumJoysticks();ijoy++)
			{
				if(SDL_IsGameController(ijoy))	
				{
					input_mngr->first_found_joystick = ijoy;
					input_mngr->joystick = SDL_GameControllerOpen(input_mngr->first_found_joystick);

					if(input_mngr->joystick == NULL)
					{
						logprint("Could not open gamecontroller %d %s",input_mngr->first_found_joystick,SDL_GetError());
					}
					else
					{
						const char* name = SDL_GameControllerName(input_mngr->joystick);

						logprint("Found joystick %d named %s",input_mngr->first_found_joystick,name);
						input_mngr->module_manager->game_changeinputmode("joystick");
					}
				}
			}
            break;

        case SDL_CONTROLLERDEVICEREMOVED:
			if(input_mngr->first_found_joystick == sdlEvent.cdevice.which)
			{
				if(input_mngr->joystick != NULL)
				{
					const char* name = SDL_GameControllerName(input_mngr->joystick);
					logprint("remove joystick named %s",name);
					SDL_GameControllerClose(input_mngr->joystick);
					input_mngr->joystick = NULL;
					input_mngr->first_found_joystick = -1;
					input_mngr->module_manager->game_changeinputmode("keyboard");
				}

				if(input_mngr->button_pressed != NULL)
				{
					wf_free(input_mngr->button_pressed);
					input_mngr->button_pressed = NULL;
				}
			}

            break;

		case SDL_MOUSEBUTTONDOWN:
		case SDL_MOUSEBUTTONUP:
			{
				int valx = (int)sdlEvent.button.x;
				int valy = (int)sdlEvent.button.y;

				Vector_t dest = input_getglobalcoords(input_mngr, valx, valy);

				input_mngr->click_vector = dest;

				if (sdlEvent.button.state == SDL_PRESSED)
					mod_manager->game_target((float)dest.x, (float)dest.y, wfalse, sdlEvent.button.button);
				else if (sdlEvent.button.state == SDL_RELEASED)
					mod_manager->game_endtarget((float)dest.x, (float)dest.y, wfalse, sdlEvent.button.button);
			}
			break;
		case SDL_MOUSEMOTION:
			{
				Vector_t dest = input_getglobalcoords(input_mngr, (int)sdlEvent.motion.x, (int)sdlEvent.motion.y);

				int16_t halfwidth = (int16_t)(rnd_manager->screen_info.width * 0.5f);
				int16_t halfheight = (int16_t)(rnd_manager->screen_info.height * 0.5f);

				input_mngr->move_vector = dest;

				mod_manager->game_setcursorpos(dest.x, dest.y);
				mod_manager->game_movetarget(dest.x, dest.y, wfalse);

				if (input_mngr->lock_cursor)
				{
					mod_manager->game_offset_target((float)((sdlEvent.motion.x - halfwidth)*input_mngr->mouse_sensitivity), (float)((sdlEvent.motion.y - halfheight)*input_mngr->mouse_sensitivity));
				}
			}
			break;

		case SDL_KEYUP:
			input_mngr->module_manager->game_input(sdlEvent.key.keysym.sym, wfalse, wtrue);
			break;

        }
    }

	int numkeys = 0;
	const Uint8* keys = SDL_GetKeyboardState(&numkeys);

	for (int i = 0; i < numkeys; i++)
	{
		if (keys[i]) {
			input_mngr->module_manager->game_input(SDL_GetKeyFromScancode(i), wtrue, wtrue);
		}
	}

	if(input_mngr->first_found_joystick != -1)
			SDL_GameControllerUpdate();

	if(input_mngr->first_found_joystick != -1)
	{
		int axes_count = SDL_CONTROLLER_AXIS_MAX;
		int button_count = SDL_CONTROLLER_BUTTON_MAX;

		if(input_mngr->button_pressed == NULL)
			input_mngr->button_pressed = (unsigned char*)wf_calloc(button_count,sizeof(unsigned char));

		for(int16_t ax = 0; ax < axes_count;ax++)
		{
			Sint16 tmp_axe =  SDL_GameControllerGetAxis(input_mngr->joystick,ax);
			float axe_value = (float)(tmp_axe / 32767.0f);
			input_mngr->module_manager->game_joystick_axe( input_mngr->first_found_joystick,ax,axe_value);
		}

		for(int16_t ab = 0; ab < button_count;ab++)
		{
			unsigned char button_value =  SDL_GameControllerGetButton(input_mngr->joystick,ab);

			if(button_value == 1)
			{
				input_mngr->module_manager->game_joystick_button(input_mngr->first_found_joystick,ab,wtrue);
				input_mngr->button_pressed[ab] = 1;
			}
			else if(button_value == 0)
			{
				if(input_mngr->button_pressed[ab] == 1)
				{
					input_mngr->module_manager->game_joystick_button(input_mngr->first_found_joystick,ab,wfalse);
					input_mngr->button_pressed[ab] = 0;
				}

			}

		}

	}

}

void input_sdl_init(input_mngr_t* const input_mngr)
{
	logprint("SDL : Init joystick and game controller...");
	SDL_Init(SDL_INIT_JOYSTICK | SDL_INIT_GAMECONTROLLER | SDL_INIT_AUDIO);
	input_mngr->joystick = NULL;

	if (input_mngr->lock_cursor) {
		SDL_SetRelativeMouseMode(SDL_TRUE);
	}

}

void input_sdl_poll()
{
	//SDL_PumpEvents();
}

const char* input_sdl_getcontrollername(input_mngr_t* const input_mngr)
{
	if (input_mngr->joystick == NULL)
	{
		return NULL;
	}

	return SDL_GameControllerName(input_mngr->joystick);
}

const char* input_sdl_getkeyname(input_mngr_t* const input_mngr, int key)
{
	switch (key) {
		case 80:
			return "LEFT";
		case 82:
			return "UP";
		case 79:
			return "RIGHT";
		case 81:
			return "DOWN";
		default:
			return (const char*)SDL_GetKeyName(key);
	}
}

gamepad_scheme input_sdl_getscheme(input_mngr_t* const input_mngr)
{
	const char* name = SDL_GameControllerName(input_mngr->joystick);

	if (strcmp(name, "Xbox Controller") == 0 || strcmp(name, "Xbox One Controller") == 0 || strcmp(name, "Xbox 360 Controller") == 0)
	{
		return XBOX_INPUT;
	}


	if (strcmp(name, "PS4 Controller") == 0 || strcmp(name, "PS5 Controller") == 0 || strcmp(name, "PS3 Controller") == 0)
	{
		return PLAYSTATION_INPUT;
	}

	return GENERIC_INPUT;
}

int input_sdl_getkey(input_mngr_t* const input_mngr, int key)
{
	const Uint8* keys = SDL_GetKeyboardState(NULL);
	return (int)keys[SDL_GetScancodeFromKey(key)];
}

const char* input_sdl_getbuttonname(input_mngr_t* const input_mngr, int16_t button, gamepad_scheme scheme)
{
	if (scheme == PLAYSTATION_INPUT)
	{
		switch (button)
		{
		case SDL_CONTROLLER_BUTTON_A:
			return "CROSS";
		case SDL_CONTROLLER_BUTTON_B:
			return "CIRCLE";
		case SDL_CONTROLLER_BUTTON_X:
			return "SQUARE";
		case SDL_CONTROLLER_BUTTON_Y:
			return "TRIANGLE";
		case SDL_CONTROLLER_BUTTON_LEFTSHOULDER:
			return "BTN_L1";
		case SDL_CONTROLLER_BUTTON_RIGHTSHOULDER:
			return "BTN_R1";
		case SDL_CONTROLLER_BUTTON_BACK:
			return "BTN_SHARE";
		case SDL_CONTROLLER_BUTTON_START:
			return "BTN_OPTIONS";
		case SDL_CONTROLLER_BUTTON_GUIDE:
			return "BTN_PLAYSTATION";
		case SDL_CONTROLLER_BUTTON_LEFTSTICK:
			return "BTN_L3";
		case SDL_CONTROLLER_BUTTON_RIGHTSTICK:
			return "BTN_R3";
		case SDL_CONTROLLER_BUTTON_DPAD_UP:
			return "DPAD_UP";
		case SDL_CONTROLLER_BUTTON_DPAD_RIGHT:
			return "DPAD_RIGHT";
		case SDL_CONTROLLER_BUTTON_DPAD_DOWN:
			return "DPAD_DOWN";
		case SDL_CONTROLLER_BUTTON_DPAD_LEFT:
			return "DPAD_LEFT";
		}
	}
	else if (scheme == XBOX_INPUT || scheme == GENERIC_INPUT)
	{
		switch (button)
		{
		case SDL_CONTROLLER_BUTTON_A:
			return "BTN_A";
		case SDL_CONTROLLER_BUTTON_B:
			return "BTN_B";
		case SDL_CONTROLLER_BUTTON_X:
			return "BTN_X";
		case SDL_CONTROLLER_BUTTON_Y:
			return "BTN_Y";
		case SDL_CONTROLLER_BUTTON_LEFTSHOULDER:
			return "BTN_LB";
		case SDL_CONTROLLER_BUTTON_RIGHTSHOULDER:
			return "BTN_RB";
		case SDL_CONTROLLER_BUTTON_LEFTSTICK:
			return "BTN_LJT";
		case SDL_CONTROLLER_BUTTON_RIGHTSTICK:
			return "BTN_RJB";
		case SDL_CONTROLLER_BUTTON_BACK:
			return "BTN_BACK";
		case SDL_CONTROLLER_BUTTON_START:
			return "BTN_START";
		case SDL_CONTROLLER_BUTTON_DPAD_UP:
			return "DPAD_UP";
		case SDL_CONTROLLER_BUTTON_DPAD_RIGHT:
			return "DPAD_RIGHT";
		case SDL_CONTROLLER_BUTTON_DPAD_DOWN:
			return "DPAD_DOWN";
		case SDL_CONTROLLER_BUTTON_DPAD_LEFT:
			return "DPAD_LEFT";
		}
	}

	return NULL;
}

const char* input_sdl_getaxisname(input_mngr_t* const input_mngr, int16_t axis, gamepad_scheme scheme)
{
	if (scheme == PLAYSTATION_INPUT)
	{
		switch (axis)
		{
		case SDL_CONTROLLER_AXIS_LEFTX:
			return "AXIS_LEFT_X";
		case SDL_CONTROLLER_AXIS_LEFTY:
			return "AXIS_LEFT_Y";
		case SDL_CONTROLLER_AXIS_RIGHTX:
			return "AXIS_RIGHT_X";
		case SDL_CONTROLLER_AXIS_RIGHTY:
			return "AXIS_RIGHT_Y";
		case SDL_CONTROLLER_AXIS_TRIGGERLEFT:
			return "BTN_L2";
		case SDL_CONTROLLER_AXIS_TRIGGERRIGHT:
			return "BTN_R2";
		}
	}
	else if (scheme == XBOX_INPUT || scheme == GENERIC_INPUT)
	{
		switch (axis)
		{
		case SDL_CONTROLLER_AXIS_LEFTX:
			return "AXIS_LEFT_X";
		case SDL_CONTROLLER_AXIS_LEFTY:
			return "AXIS_LEFT_Y";
		case SDL_CONTROLLER_AXIS_RIGHTX:
			return "AXIS_RIGHT_X";
		case SDL_CONTROLLER_AXIS_RIGHTY:
			return "AXIS_RIGHT_Y";
		case SDL_CONTROLLER_AXIS_TRIGGERLEFT:
			return "BTN_LT";
		case SDL_CONTROLLER_AXIS_TRIGGERRIGHT:
			return "BTN_RT";
		}
	}

	return NULL;
}


wbool input_sdl_is_trigger(input_mngr_t* const input_mngr, int16_t axis)
{
	switch (axis)
	{
		case SDL_CONTROLLER_AXIS_TRIGGERLEFT:
		case SDL_CONTROLLER_AXIS_TRIGGERRIGHT:
			return wtrue;
	}

	return wfalse;

}



#endif
