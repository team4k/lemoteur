#ifdef _MSC_VER
       extern "C" {
#endif
	#include "lauxlib.h"
#include "lualib.h"
	#include <lua.h>

#ifdef _MSC_VER
}
#endif


#if defined(_WIN32)
        #include <windows.h>
#else
	#ifndef __USE_BSD
	        #define __USE_BSD
	#endif
        #include <unistd.h>
        #include <dlfcn.h>
#endif

#include <Render.h>

#include <inttypes.h>

#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <time.h>
#include <wchar.h>
#include <math.h>
#include <string.h>
#include <sqlite3.h>

#include <Engine.h>
#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>

#include <Base/geom.h>
#include <Utils/Hash.h>
#include <Utils/hashmap.h>

#include <Render/render_manager.h>

#include <GameObjects/Shaders.pb-c.h>
#include <Graphics/TextureLoader.h>
#include <Resx/shader_loader.h>

#include <GameObjects/Characters.pb-c.h>
#include <Graphics/Animation.h>
#include <Graphics/Sprite.h>
#include <Graphics/Mesh.h>

#include <Graphics/Light.h>
#include <Graphics/LightManager.h>

#include <Debug/Logprint.h>


#include <Graphics/TextureRegion.h>
#include <Graphics/SpriteBatch.h>
#include <Graphics/FontLoader.h>

#include "GameObjects/Level.pb-c.h"

#include <Sound/Sound.h>
       

#include <Resx/Resources.h>
#include <Resx/localization.h>

#include <Graphics/TilemapLayer.h>
#include <GameObjects/Tilemap.h>

#include <chipmunk/chipmunk.h>
#include <Physics/Physics.h>


#include <GameObjects/entity.h>
#include <GameObjects/Trigger.h>

#include <Config/config.h>

#include <GameMngr/input_mngr.h>
#include <game_functions.h>
#include <GameMngr/module_mngr.h>


void input_mngr_init(input_mngr_t* const input_mngr,render_manager_t* const render_manager,module_mngr_t* const module_manager,float mouse_sensitivity,wbool lock_cursor,wbool editor_mode,wbool show_cursor)
{
	input_mngr->mouse_sensitivity = mouse_sensitivity;

	input_mngr->started_offset = wfalse;
	input_mngr->click_vector = vectorzero;
	input_mngr->move_vector = vectorzero;

	input_mngr->first_found_joystick = -1;
	input_mngr->has_lookup_joystick = wfalse;
	input_mngr->button_pressed = NULL;
	input_mngr->has_focus = wtrue;
	input_mngr->lock_cursor = lock_cursor;
	input_mngr->request_close = wfalse;
	input_mngr->module_manager = module_manager;
	input_mngr->render_manager = render_manager;
	input_mngr->editor_mode = editor_mode;
	input_mngr->show_cursor = show_cursor;
	
	#ifdef USE_SDL_INPUT
		input_sdl_init(input_mngr);
	#endif

	#if USE_GLFW_INPUT
		input_glfw_init(input_mngr,render_manager);
	#endif

	#ifdef USE_MOBILE_INPUT
		input_mobile_init(input_mngr);
	#endif
}

Vector_t input_getglobalcoords(input_mngr_t* const input_mngr,int local_coordx,int local_coordy)
{
	Vector_t globalcoords = vectorzero;
	globalcoords.x = ((float)(local_coordx - input_mngr->render_manager->scroll_vector.x)) * input_mngr->render_manager->zoom_factor;
	globalcoords.y = ((float)(input_mngr->render_manager->screen_info.height- (local_coordy +  input_mngr->render_manager->scroll_vector.y))) * input_mngr->render_manager->zoom_factor;

	return globalcoords;
}

void input_mngr_handle_input(input_mngr_t* const input_mngr)
{
	//FIXME glfw focus callback is buggy ? investigate
	//if(!input_mngr->has_focus || input_mngr->module_manager->game_disableinput())
		//return;

	//FIXEME : disable input shouldn't disable all input, like esc or space for restart
	if(input_mngr->module_manager->game_disableinput())
		return;

	#if defined(USE_GLFW_INPUT)
		input_glfw_joystickevent(input_mngr);
	#endif

	#ifdef USE_SDL_INPUT
		input_sdl_event(input_mngr);
	#endif

	#if defined(USE_GLFW_INPUT)
		input_glfw_keyboardevent(input_mngr);
	#endif

	#ifdef USE_MOBILE_INPUT
		input_mobile_handleinput(input_mngr);
	#endif
}

void input_mngr_sendinputmessage(input_mngr_t* const input_mngr,uint32_t uMsg,uint32_t wParam,long lParam)
{
	#if defined(USE_GLFW_INPUT)
		input_glfw_sendinputmessage(input_mngr,uMsg,wParam,lParam);
	#endif
}

void input_mngr_poll(input_mngr_t* const input_mngr)
{
	if(!input_mngr->editor_mode)
	{
		#if defined(USE_GLFW_INPUT)
			input_glfw_poll();
		#endif

		#if defined(USE_SDL_INPUT)
			input_sdl_poll();
		#endif
	}
		
}

int input_mngr_getkey(input_mngr_t* const input_mngr,int key)
{
	#if defined(USE_GLFW_INPUT)
		return input_glfw_getkey(input_mngr,key);
	#endif

	#if defined(USE_SDL_INPUT)
		return input_sdl_getkey(input_mngr,key);
	#endif

    return 0;
}


void input_mngr_touch(input_mngr_t* const input_mngr,wfloat posx,wfloat posy,int action)
{
	#if defined(USE_MOBILE_INPUT)
		input_mobile_touch(input_mngr,posx,posy,action);
	#endif
}

void input_mngr_backbutton(input_mngr_t* const input_mngr)
{
	#if defined(USE_MOBILE_INPUT)
		input_mobile_backbutton(input_mngr);
	#endif
}

const char* input_mngr_getkeyname(input_mngr_t* const input_mngr, int key)
{
	#if defined(USE_GLFW_INPUT)
		return input_glfw_getkeyname(input_mngr, key);
	#endif

	#if defined(USE_SDL_INPUT)
		return input_sdl_getkeyname(input_mngr, key);
	#endif

	return NULL;
}

gamepad_scheme input_mngr_getscheme(input_mngr_t* const input_mngr)
{
	#if defined(USE_GLFW_INPUT)
		return input_glfw_getscheme(input_mngr);
	#endif

	#if defined(USE_SDL_INPUT)
		return input_sdl_getscheme(input_mngr);
	#endif

	return GENERIC_INPUT;
}

const char* input_mngr_getcontrollername(input_mngr_t* const input_mngr)
{
	#if defined(USE_GLFW_INPUT)
		return input_glfw_getcontrollername(input_mngr);
	#endif

	#if defined(USE_SDL_INPUT)
		return input_sdl_getcontrollername(input_mngr);
	#endif

	return NULL;
}

const char* input_mngr_getbuttonname(input_mngr_t* const input_mngr, int16_t button, gamepad_scheme scheme)
{
	#if defined(USE_GLFW_INPUT)
		return input_glfw_getbuttonname(input_mngr, button, scheme);
	#endif

	#if defined(USE_SDL_INPUT)
		return input_sdl_getbuttonname(input_mngr, button, scheme);
	#endif

	return NULL;
}

const char* input_mngr_getaxisname(input_mngr_t* const input_mngr, int16_t axis, gamepad_scheme scheme)
{
	#if defined(USE_GLFW_INPUT)
		return input_glfw_getaxisname(input_mngr, axis, scheme);
	#endif

	#if defined(USE_SDL_INPUT)
		return input_sdl_getaxisname(input_mngr, axis, scheme);
	#endif

	return NULL;
}

wbool input_mngr_is_trigger(input_mngr_t* const input_mngr, int16_t axis)
{
	#if defined(USE_GLFW_INPUT)
		return input_glfw_is_trigger(input_mngr, axis);
	#endif

	#if defined(USE_SDL_INPUT)
		return input_sdl_is_trigger(input_mngr, axis);
	#endif

	return wfalse;
}


void input_mngr_free(input_mngr_t* input_mngr)
{
	input_mngr->first_found_joystick = -1;
	input_mngr->has_lookup_joystick = wfalse;

	if(input_mngr->button_pressed != NULL)
	{
		wf_free(input_mngr->button_pressed);
		input_mngr->button_pressed = NULL;
	}
}
