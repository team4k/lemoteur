#ifdef _MSC_VER
       extern "C" {
#endif
	#include "lauxlib.h"
#include "lualib.h"
	#include <lua.h>

#ifdef _MSC_VER
}
#endif


#if defined(_WIN32)
        #include <windows.h>
#else
	#ifndef __USE_BSD
        	#define __USE_BSD
	#endif
        #include <unistd.h>
        #include <dlfcn.h>
#endif

#include <Render.h>

#include <inttypes.h>

#ifndef EMSCRIPTEN
	#include <portaudio.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <time.h>
#include <wchar.h>
#include <math.h>
#include <string.h>
#include <sqlite3.h>

#include <Engine.h>
#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>

#include <Base/geom.h>
#include <Utils/Hash.h>
#include <Utils/hashmap.h>

#include <Render/render_manager.h>

#include <GameObjects/Shaders.pb-c.h>
#include <Graphics/TextureLoader.h>
#include <Resx/shader_loader.h>

#include <GameObjects/Characters.pb-c.h>
#include <Graphics/Animation.h>
#include <Graphics/Sprite.h>
#include <Graphics/Mesh.h>

#include <Graphics/Light.h>
#include <Graphics/LightManager.h>

#include <Debug/Logprint.h>


#include <Graphics/TextureRegion.h>
#include <Graphics/SpriteBatch.h>
#include <Graphics/FontLoader.h>

#include "GameObjects/Level.pb-c.h"

#include <Sound/Sound.h>
       

#include <Resx/Resources.h>
#include <Resx/localization.h>

#include <Graphics/TilemapLayer.h>
#include <GameObjects/Tilemap.h>

#include <chipmunk/chipmunk.h>
#include <Physics/Physics.h>


#include <GameObjects/entity.h>
#include <GameObjects/Trigger.h>

#include <Config/config.h>

#include <GameMngr/input_mngr.h>
#include <game_functions.h>
#include <GameMngr/module_mngr.h>

#if defined(_WIN32)
	#define ADDRESS_FUNC(module,func_name) GetProcAddress(module,func_name)
#else
	#define ADDRESS_FUNC(module,func_name) dlsym(module,func_name);
#endif


void module_mngr_init(module_mngr_t* mngr,const char* module_path)
{
	w_strcpy(mngr->game_dll_path,260,module_path);

	#if defined(_WIN32)

		#if defined(WINAPI_FAMILY) && (WINAPI_FAMILY == WINAPI_FAMILY_PHONE_APP)
			wchar_t lib_name[260] = { 0 };
			size_t ret;
			mbstowcs_s(&ret, lib_name, strlen(mngr->game_dll_path) + 1, mngr->game_dll_path, 260);

			mngr->game_module = LoadPackagedLibrary(lib_name, 0);
		#else
			mngr->game_module = LoadLibraryA(mngr->game_dll_path);
		#endif
	#else
		mngr->game_module = dlopen(mngr->game_dll_path, RTLD_LAZY);
	#endif

	if(mngr->game_module == NULL)
	{
		logprint("Can't load game dll ! %s",mngr->game_dll_path);
		return;
	}

	logprint("[Game module] Locating functions...");


	//resolve game functions
	mngr->game_init_objects = (game_init_objects_t)ADDRESS_FUNC(mngr->game_module,"game_init_objects");
	mngr->game_init_physics = (game_init_physics_t)ADDRESS_FUNC(mngr->game_module,"game_init_physics");
	mngr->game_changephysicsmanager = (game_changephysicsmanager_t)ADDRESS_FUNC(mngr->game_module, "game_changephysicsmanager");
	mngr->game_init_scripts = (game_init_scripts_t)ADDRESS_FUNC(mngr->game_module,"game_init_scripts");
	mngr->game_get_playerpos = (game_get_playerpos_t)ADDRESS_FUNC(mngr->game_module,"game_get_playerpos");
	mngr->game_player_reposition = (game_player_reposition_t)ADDRESS_FUNC(mngr->game_module,"game_player_reposition");
	mngr->game_debuginfo_update = (game_debuginfo_update_t)ADDRESS_FUNC(mngr->game_module,"game_debuginfo_update");
	mngr->game_draw = (game_draw_t)ADDRESS_FUNC(mngr->game_module,"game_draw");
	mngr->game_debug_draw = (game_debug_draw_t)ADDRESS_FUNC(mngr->game_module,"game_debug_draw");
	mngr->game_update = (game_update_t)ADDRESS_FUNC(mngr->game_module,"game_update");
	mngr->game_move = (game_move_t)ADDRESS_FUNC(mngr->game_module,"game_move");
	mngr->game_target = (game_target_t)ADDRESS_FUNC(mngr->game_module,"game_target");
	mngr->game_get_playerbox = (game_get_playerbox_t)ADDRESS_FUNC(mngr->game_module,"game_get_playerbox");
	mngr->game_editor_reset = (game_editor_reset_t)ADDRESS_FUNC(mngr->game_module,"game_editor_reset");
	mngr->game_endtarget = (game_endtarget_t)ADDRESS_FUNC(mngr->game_module,"game_endtarget");
	mngr->game_movetarget = (game_movetarget_t)ADDRESS_FUNC(mngr->game_module,"game_movetarget");
	mngr->game_cleanup = (game_cleanup_t)ADDRESS_FUNC(mngr->game_module,"game_cleanup");
	mngr->game_updatescroll = (game_updatescroll_t)ADDRESS_FUNC(mngr->game_module,"game_updatescroll");
	mngr->game_keepresources = (game_keepresources_t)ADDRESS_FUNC(mngr->game_module,"game_keepresources");
	mngr->game_offset_target = (game_offset_target_t)ADDRESS_FUNC(mngr->game_module,"game_offset_target");
	mngr->game_update_cursorpos = (game_update_cursorpos_t)ADDRESS_FUNC(mngr->game_module,"game_update_cursorpos");
    mngr->game_setcursorpos = (game_setcursorpos_t)ADDRESS_FUNC(mngr->game_module,"game_setcursorpos");
	mngr->game_has_player = (game_has_player_t)ADDRESS_FUNC(mngr->game_module,"game_has_player");
	mngr->game_config_updated = (game_config_updated_t)ADDRESS_FUNC(mngr->game_module,"game_config_updated");
	mngr->game_remove_physics = (game_remove_physics_t)ADDRESS_FUNC(mngr->game_module,"game_remove_physics");
	mngr->game_free = (game_free_t)ADDRESS_FUNC(mngr->game_module,"game_free");
	mngr->game_g_cleanup = (game_g_cleanup_t)ADDRESS_FUNC(mngr->game_module,"game_g_cleanup");
	mngr->game_g_create = (game_g_create_t)ADDRESS_FUNC(mngr->game_module,"game_g_create");
	mngr->game_pause= (game_pause_t)ADDRESS_FUNC(mngr->game_module,"game_pause");
	mngr->game_unpause = (game_unpause_t)ADDRESS_FUNC(mngr->game_module,"game_unpause");
	mngr->game_chunk_loaded = (game_chunk_loaded_t)ADDRESS_FUNC(mngr->game_module, "game_chunk_loaded");
	mngr->game_level_loaded = (game_level_loaded_t)ADDRESS_FUNC(mngr->game_module,"game_level_loaded");
	mngr->game_getversion = (game_getversion_t)ADDRESS_FUNC(mngr->game_module,"game_getversion");
	mngr->game_disableinput = (game_disableinput_t)ADDRESS_FUNC(mngr->game_module,"game_disableinput");
	mngr->game_input = (game_input_t)ADDRESS_FUNC(mngr->game_module,"game_input");
	mngr->game_setexternapifunc = (game_setexternapifunc_t)ADDRESS_FUNC(mngr->game_module,"game_setexternapifunc");
	mngr->game_joystick_axe = (game_joystick_axe_t)ADDRESS_FUNC(mngr->game_module,"game_joystick_axe");
	mngr->game_joystick_button = (game_joystick_button_t)ADDRESS_FUNC(mngr->game_module,"game_joystick_button");
	mngr->game_changeinputmode = (game_changeinputmode_t)ADDRESS_FUNC(mngr->game_module,"game_changeinputmode");
	mngr->game_char_input = (game_char_input_t)ADDRESS_FUNC(mngr->game_module,"game_char_input");
	mngr->game_wheelpinch = (game_wheelpinch_t)ADDRESS_FUNC(mngr->game_module,"game_wheelpinch");
	mngr->game_getpausemenu = (game_getpausemenu_t)ADDRESS_FUNC(mngr->game_module, "game_getpausemenu");
	mngr->game_handlebackbutton = (game_handlebackbutton_t)ADDRESS_FUNC(mngr->game_module,"game_handlebackbutton");
	mngr->game_initlevelstream = (game_initlevelstream_t)ADDRESS_FUNC(mngr->game_module, "game_initlevelstream");
	mngr->game_initlights = (game_init_lights_t)ADDRESS_FUNC(mngr->game_module, "game_initlights");
	mngr->game_getengineversion = (game_getengineversion_t)ADDRESS_FUNC(mngr->game_module, "game_getengineversion");
	
	

	module_mngr_check(mngr);
}

void module_mngr_check(module_mngr_t* const mngr)
{
	if(!mngr->game_init_objects)
		logprint("Can't locate game_init_objects!");

	if(!mngr->game_init_physics)
		logprint("Can't locate game_init_physics!");

	if (!mngr->game_changephysicsmanager)
		logprint("Can't locate game_changephysicsmanager!");

	if(!mngr->game_init_scripts)
		logprint("Can't locate game_init_scripts!");

	if(!mngr->game_get_playerpos)
		logprint("Can't locate game_get_playerpos!");

	if(!mngr->game_player_reposition)
		logprint("Can't locate game_player_reposition!");

	if(!mngr->game_debuginfo_update)
		logprint("Can't locate game_debuginfo_update!");

	if(!mngr->game_draw)
		logprint("Can't locate game_draw!");

	if(!mngr->game_debug_draw)
		logprint("Can't locate game_debug_draw!");

	if(!mngr->game_update)
		logprint("Can't locate game_update!");

	if(!mngr->game_move)
		logprint("Can't locate game_move!");

	if(!mngr->game_target)
		logprint("Can't locate game_target!");

	
	if(!mngr->game_get_playerbox)
		logprint("Can't locate game_get_playerbox!");

	if(!mngr->game_editor_reset)
		logprint("Can't locate game_editor_reset!");

	if(!mngr->game_endtarget)
		logprint("Can't locate game_endtarget!");

	if(!mngr->game_movetarget)
		logprint("Can't locate game_movetarget!");

	if(!mngr->game_cleanup)
		logprint("Can't locate game_cleanup!");

	if(!mngr->game_updatescroll)
		logprint("Can't locate game_updatescroll!");

	if(!mngr->game_keepresources)
		logprint("Can't locate game_keepresources!");

	if(!mngr->game_offset_target)
		logprint("Can't locate game_offset_target!");

	if(!mngr->game_update_cursorpos)
		logprint("Can't locate mngr->game_update_cursorpos!");
                
        if(!mngr->game_setcursorpos)
            logprint("Can't locate mngr->game_setcursorpos!");

	if(!mngr->game_has_player)
		logprint("Can't locate mngr->game_has_player!");

	if(!mngr->game_config_updated)
		logprint("Can't locate mngr->game_config_updated!");

	if(!mngr->game_remove_physics)
		logprint("Can't locate mngr->game_remove_physics!");

	if(!mngr->game_free)
		logprint("Can't locate mngr->game_free!");

	if(!mngr->game_g_cleanup)
		logprint("Can't locate mngr->game_g_cleanup!");

	if(!mngr->game_g_create)
		logprint("Can't locate mngr->game_g_create!");

	if(!mngr->game_pause)
		logprint("Can't locate mngr->game_pause!");

	if(!mngr->game_unpause)
		logprint("Can't locate mngr->game_unpause!");

	if (!mngr->game_chunk_loaded)
		logprint("Can't locate mngr->game_chunk_loaded");

	if(!mngr->game_level_loaded)
		logprint("Can't locate game_level_loaded!");

	if(!mngr->game_getversion)
		logprint("Can't locate game_getversion!");

	if(!mngr->game_disableinput)
		logprint("Can't locate game_disableinput!");

	if(!mngr->game_input)
		logprint("Can't locate game_input!");

	if(!mngr->game_setexternapifunc)
		logprint("Can't locate game_setexternapifunc!");

	if(!mngr->game_joystick_axe)
		logprint("Can't locate game_joystick_axe!");

	if(!mngr->game_joystick_button)
		logprint("Can't locate game_joystick_button!");

	if(!mngr->game_changeinputmode)
		logprint("Can't locate game_changeinputmode!");

	if(!mngr->game_char_input)
		logprint("Can't locate game_char_input!");

	if(!mngr->game_wheelpinch)
		logprint("Can't locate game_wheelpinch!");

	if (!mngr->game_getpausemenu)
		logprint("Can't locate game_getpausemenu!");

	if(!mngr->game_handlebackbutton)
		logprint("Can't locate game_handlebackbutton");

	if (!mngr->game_initlevelstream)
		logprint("Can't locate game_initlevelstream");

	if (!mngr->game_initlights)
		logprint("Can't locate game_initlights");

	if(!mngr->game_getengineversion)
		logprint("Can't locate game_getengineversion");
}

void module_mngr_release(module_mngr_t* const module_mngr)
{
	if(module_mngr->game_module)
	{
		#if defined(_WIN32)
			FreeLibrary(module_mngr->game_module);
		#else
			dlclose(module_mngr->game_module);
		#endif


		module_mngr->game_module = NULL;
		module_mngr->game_init_objects = NULL;
		module_mngr->game_init_physics = NULL;
		module_mngr->game_init_scripts = NULL;
		module_mngr->game_get_playerpos = NULL;
		module_mngr->game_player_reposition = NULL;
		module_mngr->game_debuginfo_update = NULL;
		module_mngr->game_draw = NULL;
		module_mngr->game_debug_draw =NULL;
		module_mngr->game_update = NULL;
		module_mngr->game_move = NULL;
		module_mngr->game_target = NULL;
		module_mngr->game_get_playerbox = NULL;
		module_mngr->game_editor_reset = NULL;
		module_mngr->game_endtarget = NULL;
		module_mngr->game_movetarget = NULL;
		module_mngr->game_cleanup = NULL;
		module_mngr->game_updatescroll = NULL;
		module_mngr->game_keepresources = NULL;
		module_mngr->game_offset_target = NULL;
		module_mngr->game_update_cursorpos = NULL;
        module_mngr->game_setcursorpos = NULL;
		module_mngr->game_has_player = NULL;
		module_mngr->game_config_updated = NULL;
		module_mngr->game_remove_physics = NULL;
		module_mngr->game_free = NULL;
		module_mngr->game_g_cleanup = NULL;
		module_mngr->game_g_create = NULL;
		module_mngr->game_pause= NULL;
		module_mngr->game_unpause = NULL;
		module_mngr->game_level_loaded = NULL;
		module_mngr->game_getversion = NULL;
		module_mngr->game_disableinput = NULL;
		module_mngr->game_setexternapifunc = NULL;
		module_mngr->game_joystick_axe = NULL;
		module_mngr->game_joystick_button = NULL;
		module_mngr->game_changeinputmode = NULL;
		module_mngr->game_char_input = NULL;
		module_mngr->game_wheelpinch = NULL;
		module_mngr->game_getpausemenu = NULL;
		module_mngr->game_initlevelstream = NULL;
		module_mngr->game_initlights = NULL;
		module_mngr->game_chunk_loaded = NULL;
		module_mngr->game_getengineversion = NULL;
	}
}
