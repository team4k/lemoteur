#ifdef USE_WINDOWS_INPUT
extern "C" {
	#include "lauxlib.h"
	#include "lualib.h"
	#include <lua.h>
}


#include <windows.h>

#include <Render.h>

#include <inttypes.h>

#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <time.h>
#include <wchar.h>
#include <math.h>
#include <string.h>
#include <sqlite3.h>

#include <Engine.h>
#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>

#include <Base/geom.h>
#include <Utils/Hash.h>
#include <Utils/hashmap.h>

#include <Render/render_manager.h>

#include <GameObjects/Shaders.pb-c.h>
#include <Graphics/TextureLoader.h>
#include <Resx/shader_loader.h>

#include <GameObjects/Characters.pb-c.h>
#include <Graphics/Animation.h>
#include <Graphics/Sprite.h>
#include <Graphics/Mesh.h>

#include <Graphics/Light.h>
#include <Graphics/LightManager.h>

#include <Debug/Logprint.h>


#include <Graphics/TextureRegion.h>
#include <Graphics/SpriteBatch.h>
#include <Graphics/FontLoader.h>

#include "GameObjects/Level.pb-c.h"

#include <Sound/Sound.h>
       

#include <Resx/Resources.h>
#include <Resx/localization.h>

#include <Graphics/TilemapLayer.h>
#include <GameObjects/Tilemap.h>

#include <chipmunk/chipmunk.h>
#include <Physics/Physics.h>


#include <GameObjects/entity.h>
#include <GameObjects/Trigger.h>

#include <Config/config.h>

#include <GameMngr/input_mngr.h>
#include <game_functions.h>
#include <GameMngr/module_mngr.h>


LRESULT CALLBACK WndProc(HWND hWnd,
			 UINT msg,
			 WPARAM wParam,
			 LPARAM lParam)
{
	switch( msg )
	{

		case WM_KEYDOWN:
			if( wParam == VK_ESCAPE ){
				if(MessageBox(0, L"Are you sure you want to exit?",
                		L"Really?", MB_YESNO | MB_ICONQUESTION) == IDYES)
				DestroyWindow(hWnd);
			}

		return 0;

		case WM_DESTROY:
			PostQuitMessage(0);
		return 0;
	}

	return DefWindowProc(hWnd,
			 msg,
			 wParam,
			 lParam);
}


void input_windows_init(input_mngr_t* const input_mngr)
{
	input_mngr->render_manager->wc.lpfnWndProc = WndProc;
}

void input_windows_poll(input_mngr_t* const input_mngr)
{
	MSG msg;

	if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
	{
		//if (msg.message == WM_QUIT)
		//	running = wfalse;

		TranslateMessage(&msg);	
		DispatchMessage(&msg);
	}
}

#endif

