#include <sqlite3.h>
#include <inttypes.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>
#include <math.h>
#include <locale.h>

#if defined(_WIN32)
	#include <Windows.h>
	#include <strsafe.h>
	#include <io.h>
#endif

#ifdef EMSCRIPTEN
	#include <emscripten.h>
#endif

#include <Engine.h>

#include <Base/types.h>
#include <Base/functions.h>

#include <Debug/Logprint.h>
#include <GameMngr/save_mngr.h>

void save_mngr_init(save_mngr_t* const save_mngr,const char* save_file,const char* home_dir,uint32_t savepoint)
{
	int sqlite_error = 0;

	//if savepoint > 0 , copy the save file to the current save data
	if (savepoint > 0) {

		char savepoint_file[WAFA_MAX_PATH] = { 0 };
		w_sprintf(savepoint_file, "%s%csave_%d.db", home_dir, PATH_SEP, savepoint);

		copy_file(savepoint_file, save_file, wtrue); 
	}
	
	w_strcpy(save_mngr->homedir, WAFA_MAX_PATH, home_dir);
	w_strcpy(save_mngr->save_file, WAFA_MAX_PATH, save_file);
	

	//check file exist before opening it with sqlite, sqlite create automatically a file otherwise
     FILE* file = NULL;

	 w_fopen(file,save_file,"r");
       
	if(file != NULL)
	{
		fclose(file);

		//convert to wide char (utf-16) under windows

		#if defined(_WIN32)
			wchar_t tmp_path[260];
			MultiByteToWideChar(CP_THREAD_ACP,0, save_file,260,tmp_path,260);

			sqlite_error = sqlite3_open16(tmp_path,&save_mngr->save_db);
		#else
			sqlite_error = sqlite3_open(save_path,&save_mngr->save_db);
		#endif

		if(sqlite_error)
		{
			logprint("Error loading save game database! :%s",sqlite3_errmsg(save_mngr->save_db));
			sqlite3_close(save_mngr->save_db);
			return;
		}
	}
	else
	{
		logprint("No save.db file! %s", save_file);
		save_mngr->save_db = NULL;
	}

	save_mngr->ready = wtrue;
}

static void _sync_change()
{
	#ifdef EMSCRIPTEN
		EM_ASM(
				//persist change
				FS.syncfs(false,function (err) {
					assert(!err);
				});
		);
	#endif
}


wbool save_mngr_checkdata(save_mngr_t* const save_mngr,const char* table,const char* key,const char* keyvalue)
{
	char select_string[2048];
	int sqlite_error = 0;

	w_sprintf(select_string,2048,"SELECT count(*) FROM %s WHERE %s = '%s';",table,key,keyvalue);

	sqlite3_stmt* statement;

	sqlite_error = sqlite3_prepare_v2(save_mngr->save_db,select_string,-1,&statement,NULL);

	if(sqlite_error)
	{
		logprint("Can't prepare statement %s, sqlite error %s",select_string,sqlite3_errmsg(save_mngr->save_db));
		return wfalse;
	}

	sqlite_error = sqlite3_step(statement);

	if(sqlite_error != SQLITE_ROW && sqlite_error != SQLITE_DONE)
	{
		logprint("Can't execute statement, sqlite error %s",sqlite3_errmsg(save_mngr->save_db));
		sqlite3_finalize(statement);
		return wfalse;
	}

	int result = sqlite3_column_int(statement,0);

	sqlite3_finalize(statement);

	return (wbool)(result > 0);
}

wbool save_mngr_checkdata2(save_mngr_t* const save_mngr,const char* table,const char* key,const char* keyvalue,const char* key2,const char* keyvalue2)
{
	char select_string[2048];
	int sqlite_error = 0;

	w_sprintf(select_string,2048,"SELECT count(*) FROM %s WHERE %s = '%s' AND %s = '%s';",table,key,keyvalue,key2,keyvalue2);

	sqlite3_stmt* statement;

	sqlite_error = sqlite3_prepare_v2(save_mngr->save_db,select_string,-1,&statement,NULL);

	if(sqlite_error)
	{
		logprint("Can't prepare statement %s, sqlite error %s",select_string,sqlite3_errmsg(save_mngr->save_db));
		return wfalse;
	}

	sqlite_error = sqlite3_step(statement);

	if(sqlite_error != SQLITE_ROW && sqlite_error != SQLITE_DONE)
	{
		logprint("Can't execute statement, sqlite error %s",sqlite3_errmsg(save_mngr->save_db));
		sqlite3_finalize(statement);
		return wfalse;
	}

	int result = sqlite3_column_int(statement,0);

	sqlite3_finalize(statement);

	return (wbool)(result > 0);
}

int32_t save_mngr_getint(save_mngr_t* const save_mngr,const char* table,const char* key,const char* keyvalue,const char* column)
{
	char select_string[2048];
	int sqlite_error = 0;

	w_sprintf(select_string,2048,"SELECT %s FROM %s WHERE %s = '%s';",column,table,key,keyvalue);

	sqlite3_stmt* statement;

	sqlite_error = sqlite3_prepare_v2(save_mngr->save_db,select_string,-1,&statement,NULL);

	if(sqlite_error)
	{
		logprint("Can't prepare statement %s, sqlite error %s",select_string,sqlite3_errmsg(save_mngr->save_db));
		return -1;
	}

	sqlite_error = sqlite3_step(statement);

	if(sqlite_error != SQLITE_ROW && sqlite_error != SQLITE_DONE)
	{
		logprint("Can't execute statement, sqlite error %s",sqlite3_errmsg(save_mngr->save_db));
		sqlite3_finalize(statement);
		return -1;
	}

	int32_t result = sqlite3_column_int(statement,0);

	sqlite3_finalize(statement);

	return result;
}

int32_t save_mngr_getint2(save_mngr_t* const save_mngr,const char* table,const char* key,const char* keyvalue,const char* key2,const char* value2,const char* column)
{
	char select_string[2048];
	int sqlite_error = 0;

	w_sprintf(select_string,2048,"SELECT %s FROM %s WHERE %s = '%s' AND %s = '%s';",column,table,key,keyvalue,key2,value2);

	sqlite3_stmt* statement;

	sqlite_error = sqlite3_prepare_v2(save_mngr->save_db,select_string,-1,&statement,NULL);

	if(sqlite_error)
	{
		logprint("Can't prepare statement %s, sqlite error %s",select_string,sqlite3_errmsg(save_mngr->save_db));
		return -1;
	}

	sqlite_error = sqlite3_step(statement);

	if(sqlite_error != SQLITE_ROW && sqlite_error != SQLITE_DONE)
	{
		logprint("Can't execute statement, sqlite error %s",sqlite3_errmsg(save_mngr->save_db));
		sqlite3_finalize(statement);
		return -1;
	}

	int32_t result = sqlite3_column_int(statement,0);

	sqlite3_finalize(statement);

	return result;
}


double save_mngr_getdouble(save_mngr_t* const save_mngr,const char* table,const char* key,const char* keyvalue,const char* column)
{
	char select_string[2048];
	int sqlite_error = 0;

	w_sprintf(select_string,2048,"SELECT %s FROM %s WHERE %s = '%s';",column,table,key,keyvalue);

	sqlite3_stmt* statement;

	sqlite_error = sqlite3_prepare_v2(save_mngr->save_db,select_string,-1,&statement,NULL);

	if(sqlite_error)
	{
		logprint("Can't prepare statement %s, sqlite error %s",select_string,sqlite3_errmsg(save_mngr->save_db));
		return -1.0;
	}

	sqlite_error = sqlite3_step(statement);

	if(sqlite_error != SQLITE_ROW && sqlite_error != SQLITE_DONE)
	{
		logprint("Can't execute statement, sqlite error %s",sqlite3_errmsg(save_mngr->save_db));
		sqlite3_finalize(statement);
		return -1.0;
	}

	double result = sqlite3_column_double(statement,0);

	sqlite3_finalize(statement);

	return result;
}

double save_mngr_getdouble2(save_mngr_t* const save_mngr,const char* table,const char* key,const char* keyvalue,const char* key2,const char* value2,const char* column)
{
	char select_string[2048];
	int sqlite_error = 0;

	w_sprintf(select_string,2048,"SELECT %s FROM %s WHERE %s = '%s' AND %s = '%s';",column,table,key,keyvalue,key2,value2);

	sqlite3_stmt* statement;

	sqlite_error = sqlite3_prepare_v2(save_mngr->save_db,select_string,-1,&statement,NULL);

	if(sqlite_error)
	{
		logprint("Can't prepare statement %s, sqlite error %s",select_string,sqlite3_errmsg(save_mngr->save_db));
		return -1.0;
	}

	sqlite_error = sqlite3_step(statement);

	if(sqlite_error != SQLITE_ROW && sqlite_error != SQLITE_DONE)
	{
		logprint("Can't execute statement, sqlite error %s",sqlite3_errmsg(save_mngr->save_db));
		sqlite3_finalize(statement);
		return -1.0;
	}

	double result = sqlite3_column_double(statement,0);

	sqlite3_finalize(statement);

	return result;
}

char* save_mngr_getstring(save_mngr_t* const save_mngr,const char* table,const char* key,const char* keyvalue,const char* column)
{
	char select_string[2048];
	int sqlite_error = 0;

	w_sprintf(select_string,2048,"SELECT %s FROM %s WHERE %s = '%s';",column,table,key,keyvalue);

	sqlite3_stmt* statement;

	sqlite_error = sqlite3_prepare_v2(save_mngr->save_db,select_string,-1,&statement,NULL);

	if(sqlite_error)
	{
		logprint("Can't prepare statement %s, sqlite error %s",select_string,sqlite3_errmsg(save_mngr->save_db));
		return NULL;
	}

	sqlite_error = sqlite3_step(statement);

	if(sqlite_error != SQLITE_ROW && sqlite_error != SQLITE_DONE)
	{
		logprint("Can't execute statement, sqlite error %s",sqlite3_errmsg(save_mngr->save_db));
		sqlite3_finalize(statement);
		return NULL;
	}

	const char* db_result = (const char*)sqlite3_column_text(statement,0);
	size_t str_l = strlen(db_result)+1;
	char* result = (char*)wf_malloc(str_l);
	w_strcpy(result,str_l,db_result);

	sqlite3_finalize(statement);

	return result;
}

wbool save_mngr_new_data(save_mngr_t* const save_mngr,const char* table,int count,...)
{
	char select_string[2048];
	int sqlite_error = 0;

	w_sprintf(select_string,2048,"INSERT INTO %s VALUES(",table);

	va_list args;
    va_start(args, count);

    for (int i = 0; i < count; ++i) 
	{
		char var_string[256] = {0};
		const char* type =  va_arg(args,const char*);

		if(strcmp(type,"int") == 0)
			w_sprintf(var_string,256,"%d,",va_arg(args,int32_t));
		else if(strcmp(type,"string") == 0)
			w_sprintf(var_string,256,"'%s',",va_arg(args,const char*));
		else if(strcmp(type,"float") == 0)
		{
			w_sprintf(var_string,256,"%.2f,",va_arg(args,double));//variadic parameters are "promoted" when passed around, so float -> double

			for (uint32_t ci = 0; ci < strlen(var_string) - 1; ci++)
			{
				if (var_string[ci] == ',')
					var_string[ci] = '.';
			}
			
		}

		w_strcat(select_string,sizeof(select_string),var_string);
    }

    va_end(args);


	select_string[strlen(select_string)-1] = '\0';//remove the last character


	w_strcat(select_string,sizeof(select_string),");");

	sqlite3_stmt* statement;

	sqlite_error = sqlite3_prepare_v2(save_mngr->save_db,select_string,-1,&statement,NULL);

	if(sqlite_error)
	{
		logprint("Can't prepare statement %s, sqlite error %s",select_string,sqlite3_errmsg(save_mngr->save_db));
		return wfalse;
	}

	sqlite_error = sqlite3_step(statement);
	
	if(sqlite_error != SQLITE_DONE)
	{
		logprint("Can't execute statement, sqlite error %s",sqlite3_errmsg(save_mngr->save_db));
		sqlite3_finalize(statement);
		return wfalse;
	}

	sqlite3_finalize(statement);

	_sync_change();

	return wtrue;

}

wbool save_mngr_update_data(save_mngr_t* const save_mngr,const char* table,const char* key,const char* keyval,int count,...)
{
	char select_string[2048];
	int sqlite_error = 0;

	w_sprintf(select_string,2048,"UPDATE %s SET ",table);

	va_list args;
    va_start(args, count);

    for (int i = 0; i < count; ++i) 
	{
		char var_string[256] = {0};
		const char* field = va_arg(args,const char*);
		const char* type =  va_arg(args,const char*);

		if(strcmp(type,"int") == 0)
			w_sprintf(var_string,256,"%s = %d,",field,va_arg(args,int32_t));
		else if(strcmp(type,"string") == 0)
			w_sprintf(var_string,256,"%s = '%s',",field,va_arg(args,const char*));
		else if(strcmp(type,"float") == 0)
		{
			w_sprintf(var_string,256,"%s = %.2f,",field,va_arg(args,double));//variadic parameters are "promoted" when passed around, so float -> double

			for(uint32_t ci = 0; ci < strlen(var_string)-1;ci++)
			{
				if(var_string[ci] == ',')
					var_string[ci] = '.';
			}
		}

		w_strcat(select_string,sizeof(select_string),var_string);
    }

    va_end(args);


	select_string[strlen(select_string)-1] = '\0';//remove the last character

	char cond_string[256];

	w_sprintf(cond_string,256," WHERE %s='%s';",key,keyval);

	w_strcat(select_string,sizeof(select_string),cond_string);

	sqlite3_stmt* statement;

	sqlite_error = sqlite3_prepare_v2(save_mngr->save_db,select_string,-1,&statement,NULL);

	if(sqlite_error)
	{
		logprint("Can't prepare statement %s, sqlite error %s",select_string,sqlite3_errmsg(save_mngr->save_db));
		return wfalse;
	}

	sqlite_error = sqlite3_step(statement);
	
	if(sqlite_error != SQLITE_DONE)
	{
		logprint("Can't execute statement, sqlite error %s",sqlite3_errmsg(save_mngr->save_db));
		sqlite3_finalize(statement);
		return wfalse;
	}

	sqlite3_finalize(statement);

	
	_sync_change();

	return wtrue;
}


wbool save_mngr_update_data2(save_mngr_t* const save_mngr,const char* table,const char* key,const char* keyval,const char* key2,const char* keyval2,int count,...)
{
	char select_string[2048];
	int sqlite_error = 0;

	w_sprintf(select_string,2048,"UPDATE %s SET ",table);

	va_list args;
    va_start(args, count);

    for (int i = 0; i < count; ++i) 
	{
		char var_string[256] = {0};
		const char* field = va_arg(args,const char*);
		const char* type =  va_arg(args,const char*);

		if(strcmp(type,"int") == 0)
			w_sprintf(var_string,256,"%s = %d,",field,va_arg(args,int32_t));
		else if(strcmp(type,"string") == 0)
			w_sprintf(var_string,256,"%s = '%s',",field,va_arg(args,const char*));
		else if(strcmp(type,"float") == 0)
		{
			w_sprintf(var_string,256,"%s = %.2f,",field,va_arg(args,double));//variadic parameters are "promoted" when passed around, so float -> double

			for(uint32_t ci = 0; ci < strlen(var_string)-1;ci++)
			{
				if(var_string[ci] == ',')
					var_string[ci] = '.';
			}
		}

		w_strcat(select_string,sizeof(select_string),var_string);
    }

    va_end(args);


	select_string[strlen(select_string)-1] = '\0';//remove the last character

	char cond_string[256];

	w_sprintf(cond_string,256," WHERE %s='%s' AND %s = '%s';",key,keyval,key2,keyval2);

	w_strcat(select_string,sizeof(select_string),cond_string);

	sqlite3_stmt* statement;

	sqlite_error = sqlite3_prepare_v2(save_mngr->save_db,select_string,-1,&statement,NULL);

	if(sqlite_error)
	{
		logprint("Can't prepare statement %s, sqlite error %s",select_string,sqlite3_errmsg(save_mngr->save_db));
		return wfalse;
	}

	sqlite_error = sqlite3_step(statement);
	
	if(sqlite_error != SQLITE_DONE)
	{
		logprint("Can't execute statement, sqlite error %s",sqlite3_errmsg(save_mngr->save_db));
		sqlite3_finalize(statement);
		return wfalse;
	}

	sqlite3_finalize(statement);

	_sync_change();

	return wtrue;
}

uint32_t save_mngr_getmaxsavepoint(const char* homedir,const char* savebasename)
{


	#if defined(_WIN32)
		wchar_t rs_path[WAFA_MAX_PATH] = { 0 };
		HANDLE hFind = INVALID_HANDLE_VALUE;
		wchar_t szDir[WAFA_MAX_PATH];
		WIN32_FIND_DATA ffd;
		MultiByteToWideChar(CP_THREAD_ACP, 0, homedir, WAFA_MAX_PATH, rs_path, WAFA_MAX_PATH);
		//mbstowcs_s(&ret, rs_path, strlen(save_mngr->) + 1, resources_path, WAFA_MAX_PATH);

		StringCchCopy(szDir, WAFA_MAX_PATH, rs_path);
		StringCchCat(szDir, WAFA_MAX_PATH, L"\\*");

		hFind = FindFirstFileEx(szDir, FindExInfoStandard, &ffd, FindExSearchNameMatch, NULL, 0);


		if (hFind == INVALID_HANDLE_VALUE) {
			logprint("Error while listing files in homepath directory %s : Directory is not valid \n", homedir);
			return 0;
		}

		uint32_t savepoint = 0;
		//get the max save info
		do {
			if (!(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) {

				char f_name[WAFA_MAX_PATH] = { 0 };
				size_t ret = 0;
				wcstombs_s(&ret, f_name, wcslen(ffd.cFileName) + 1, ffd.cFileName, WAFA_MAX_PATH);

				if (str_starts_with(savebasename, f_name)) {
					get_file_without_ext(f_name);
					uint32_t tmpsavepoint = (uint32_t)strtoul(&f_name[5], NULL, 10);

					if (tmpsavepoint > savepoint) {
						savepoint = tmpsavepoint;
					}
				}
			}
		} while (FindNextFile(hFind, &ffd) != 0);

		return savepoint;
	#else
		DIR* home_dir = 0;
		struct dirent* entry;
		struct stat fileInfo;
		char fullname[MAXPATHLEN];

		if (!(home_dir = opendir(homedir))) {
			logprint("Error while listing files in homepath directory %s : Directory is not valid \n", save_mngr->homedir);
			return wfalse;
		}

		while ((entry = readdir(home_dir))) {

			w_sprintf(fullname, MAXPATHLEN, "%s/%s", homedir, entry->d_name);

			lstat(fullname, &fileInfo);

			if (!S_ISDIR(fileInfo.st_mode)) {
				char f_name[WAFA_MAX_PATH] = { 0 };
				w_strcpy(f_name, WAFA_MAX_PATH, entry->d_name);

				if (str_starts_with(savebasename, f_name)) {
					get_file_without_ext(f_name);
					uint32_t tmpsavepoint = (uint32_t)strtoul(&f_name[5], NULL, 10);

					if (tmpsavepoint > savepoint) {
						savepoint = tmpsavepoint;
					}
				}
			}
		}

		closedir(resx_dir);
		return savepoint;
	#endif

}

void save_mgnr_savepoint(save_mngr_t* const save_mngr)
{
	//find max save point value
	uint32_t maxsavepoint = save_mngr_getmaxsavepoint(save_mngr->homedir,"save_");
	maxsavepoint++;

	//save file to savepoint
	char savepoint_file[WAFA_MAX_PATH] = { 0 };
	w_sprintf(savepoint_file, "%s%csave_%d.db", save_mngr->homedir, PATH_SEP, maxsavepoint);



	copy_file(save_mngr->save_file, savepoint_file, wfalse);
}


void save_mngr_free(save_mngr_t* const save_mngr)
{
	if(save_mngr->save_db)
		sqlite3_close(save_mngr->save_db);
}