#ifdef USE_GLFW_INPUT

#ifdef _MSC_VER
       extern "C" {
#endif
	#include "lauxlib.h"
#include "lualib.h"
	#include <lua.h>

#ifdef _MSC_VER
}
#endif


#if defined(_WIN32)
        #include <windows.h>
#else
	#ifndef __USE_BSD
	        #define __USE_BSD
	#endif
        #include <unistd.h>
        #include <dlfcn.h>
#endif

#include <Render.h>

#include <inttypes.h>

#ifndef EMSCRIPTEN
	#include <portaudio.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <time.h>
#include <wchar.h>
#include <math.h>
#include <string.h>
#include <sqlite3.h>

#include <Engine.h>
#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>

#include <Base/geom.h>
#include <Utils/Hash.h>
#include <Utils/hashmap.h>

#include <Render/render_manager.h>

#include <GameObjects/Shaders.pb-c.h>
#include <Graphics/TextureLoader.h>
#include <Resx/shader_loader.h>

#include <GameObjects/Characters.pb-c.h>
#include <Graphics/Animation.h>
#include <Graphics/Sprite.h>
#include <Graphics/Mesh.h>

#include <Graphics/Light.h>
#include <Graphics/LightManager.h>

#include <Debug/Logprint.h>


#include <Graphics/TextureRegion.h>
#include <Graphics/SpriteBatch.h>
#include <Graphics/FontLoader.h>

#include "GameObjects/Level.pb-c.h"

#include <Sound/Sound.h>
       

#include <Resx/Resources.h>
#include <Resx/localization.h>

#include <Graphics/TilemapLayer.h>
#include <GameObjects/Tilemap.h>

#include <chipmunk/chipmunk.h>
#include <Physics/Physics.h>


#include <GameObjects/entity.h>
#include <GameObjects/Trigger.h>

#include <Config/config.h>

#include <GameMngr/input_mngr.h>
#include <game_functions.h>
#include <GameMngr/module_mngr.h>


static void Mouse_callback_click(GLFWwindow* window, int id, int action,int mods)
{
	input_mngr_t* input_mngr =  (input_mngr_t*)glfwGetWindowUserPointer(window);
	render_manager_t* rnd_manager = input_mngr->render_manager;
	module_mngr_t* mod_manager = input_mngr->module_manager;

	if(!input_mngr->has_focus)
		return;

	double x = 0,y = 0;

	glfwGetCursorPos(rnd_manager->oglWindow,&x,&y);

	Vector_t dest = input_getglobalcoords(input_mngr,(int)x,(int)y);

	input_mngr->click_vector = dest;

	if(action == GLFW_PRESS)
		mod_manager->game_target((float)dest.x,(float)dest.y,wfalse,id);
	else if(action == GLFW_RELEASE)
		mod_manager->game_endtarget((float)dest.x,(float)dest.y,wfalse,id);
}

static void Mouse_callback_move(GLFWwindow* window, double x,double y)
{
	input_mngr_t* input_mngr =  (input_mngr_t*)glfwGetWindowUserPointer(window);
	render_manager_t* rnd_manager = input_mngr->render_manager;
	module_mngr_t* mod_manager = input_mngr->module_manager;

	if(!input_mngr->has_focus)
		return;

	Vector_t dest = input_getglobalcoords(input_mngr,(int)x,(int)y);

	int16_t halfwidth = (int16_t)(rnd_manager->screen_info.width * 0.5f);
	int16_t halfheight = (int16_t)(rnd_manager->screen_info.height * 0.5f);

	input_mngr->move_vector = dest;

	mod_manager->game_setcursorpos(dest.x,dest.y);
	mod_manager->game_movetarget(dest.x,dest.y,wfalse);

	if(input_mngr->lock_cursor) 
	{
		mod_manager->game_offset_target((float)((x - halfwidth)*input_mngr->mouse_sensitivity),(float)((y - halfheight)*input_mngr->mouse_sensitivity));
		glfwSetCursorPos(window,halfwidth,halfheight);
	}

}

static void Window_close_callback(GLFWwindow* window)
{
	input_mngr_t* input_mngr =  (input_mngr_t*)glfwGetWindowUserPointer(window);
	input_mngr->request_close = wtrue;
}

static void Window_focus_callback(GLFWwindow* window,int focus)
{
	input_mngr_t* input_mngr =  (input_mngr_t*)glfwGetWindowUserPointer(window);
	render_manager_t* rnd_manager = input_mngr->render_manager;
	module_mngr_t* mod_manager = input_mngr->module_manager;

	if(focus == 1)
	{
		if(input_mngr->lock_cursor)
		{
			int16_t halfwidth = (int16_t)(rnd_manager->screen_info.width * 0.5f);
			int16_t halfheight = (int16_t)(rnd_manager->screen_info.height * 0.5f);
			glfwSetCursorPos(window,halfwidth,halfheight);
			Vector_t dest = input_getglobalcoords(input_mngr,halfwidth,halfheight);

			mod_manager->game_setcursorpos((float)dest.x,(float)dest.y);
		}
	}
	else
	{

	}

	input_mngr->has_focus = (wbool)focus;
}


static void Keyboard_keycallback(GLFWwindow* windows,int key,int scancode,int action,int mods)
{
	input_mngr_t* input_mngr =  (input_mngr_t*)glfwGetWindowUserPointer(windows);
	module_mngr_t* mod_manager = input_mngr->module_manager;

	if(action == GLFW_RELEASE)
		mod_manager->game_input((int16_t)key,wfalse,wtrue);

	if (key == GLFW_KEY_BACKSPACE && (action == GLFW_PRESS || action == GLFW_REPEAT))
		mod_manager->game_char_input(8);
}

static void Keyboard_charcallback(GLFWwindow* window, unsigned int codepoint)
{
	input_mngr_t* input_mngr =  (input_mngr_t*)glfwGetWindowUserPointer(window);
	module_mngr_t* mod_manager = input_mngr->module_manager;

	mod_manager->game_char_input(codepoint);
}

static void show_cursor(render_manager_t* const render_mngr,wbool show)
{
	if(!show)
		glfwSetInputMode(render_mngr->oglWindow,GLFW_CURSOR,GLFW_CURSOR_DISABLED);
	else
		glfwSetInputMode(render_mngr->oglWindow,GLFW_CURSOR,GLFW_CURSOR_NORMAL);
}

static void Cursor_enter_callback(GLFWwindow* windows,int enter)
{
	input_mngr_t* input_mngr =  (input_mngr_t*)glfwGetWindowUserPointer(windows);

	if(enter == 1)
	{
		show_cursor(input_mngr->render_manager,input_mngr->show_cursor);
	}
	else
	{
		//outside of the client area and when on top bar, use normal cursor mode, this way the player can move the window without trouble
		show_cursor(input_mngr->render_manager,wtrue);
	}
}

void Wheel_callback(GLFWwindow* windows,double offsetx,double offsety)
{
	input_mngr_t* input_mngr =  (input_mngr_t*)glfwGetWindowUserPointer(windows);
	module_mngr_t* mod_manager = input_mngr->module_manager;
	
	if (mod_manager->game_wheelpinch) {
		mod_manager->game_wheelpinch((float)offsety, wtrue);
	}
	
}


void Joystick_Callback(int joystick, int joyevent)
{
	//always using the same user pointer for all joystick
	input_mngr_t* input_mngr = (input_mngr_t*)glfwGetJoystickUserPointer(GLFW_JOYSTICK_1);

	if (joyevent == GLFW_CONNECTED && input_mngr->first_found_joystick == -1)
	{
		input_mngr->first_found_joystick = joystick;
		input_mngr->module_manager->game_changeinputmode("joystick");
	}
	else if (joyevent == GLFW_DISCONNECTED && input_mngr->first_found_joystick != -1 && input_mngr->first_found_joystick == joystick) {
		input_mngr->first_found_joystick = -1;

		if (input_mngr->button_pressed != NULL)
		{
			wf_free(input_mngr->button_pressed);
			input_mngr->button_pressed = NULL;
		}

		input_mngr->module_manager->game_changeinputmode("keyboard");
	}
}



void input_glfw_init(input_mngr_t* const input_mngr,render_manager_t* const render_manager)
{
	glfwSetInputMode(render_manager->oglWindow,GLFW_CURSOR,GLFW_CURSOR_DISABLED);

	glfwSetWindowUserPointer(render_manager->oglWindow,input_mngr);

	glfwSetJoystickUserPointer(GLFW_JOYSTICK_1, input_mngr);

	glfwSetMouseButtonCallback(render_manager->oglWindow,Mouse_callback_click);
	glfwSetCursorPosCallback(render_manager->oglWindow,Mouse_callback_move);
	glfwSetWindowCloseCallback(render_manager->oglWindow,Window_close_callback);
	glfwSetWindowFocusCallback(render_manager->oglWindow,Window_focus_callback);
	glfwSetKeyCallback(render_manager->oglWindow,Keyboard_keycallback);
	glfwSetCharCallback(render_manager->oglWindow,Keyboard_charcallback);
	glfwSetCursorEnterCallback(render_manager->oglWindow,Cursor_enter_callback);
	glfwSetScrollCallback(render_manager->oglWindow,Wheel_callback);
	glfwSetJoystickCallback(Joystick_Callback);
}

void input_glfw_keyboardevent(input_mngr_t* const input_mngr)
{
	for (int32_t i = 0;  i <= GLFW_KEY_LAST;  i++)
    {
		int32_t get_state = glfwGetKey(input_mngr->render_manager->oglWindow,i);

        if(get_state == GLFW_PRESS || get_state == GLFW_REPEAT)
            input_mngr->module_manager->game_input((int16_t)i,wtrue,wtrue);
    }
}


void input_glfw_joystickevent(input_mngr_t* const input_mngr)
{
	//if no joystick yet, lookup if any connected, in case of connection / deconnection while the game is running is handled by joystick event
	if (!input_mngr->has_lookup_joystick) {

		wbool no_joystick = wtrue;

		for (int i = GLFW_JOYSTICK_1; i < GLFW_JOYSTICK_LAST; i++)
		{
			if (glfwJoystickPresent(i))
			{
				input_mngr->first_found_joystick = i;
				no_joystick = wfalse;
				input_mngr->module_manager->game_changeinputmode("joystick");
				break;
			}
		}

		if (no_joystick && input_mngr->first_found_joystick != -1)
		{
			input_mngr->first_found_joystick = -1;

			if (input_mngr->button_pressed != NULL)
			{
				wf_free(input_mngr->button_pressed);
				input_mngr->button_pressed = NULL;
			}

			input_mngr->module_manager->game_changeinputmode("keyboard");
		}

		input_mngr->has_lookup_joystick = wtrue;
	}

	if(input_mngr->first_found_joystick != -1)
	{
		int axes_count = 0;
		int button_count = 0;
		const float* axes_values = NULL;
		const unsigned char* button_states = NULL;
		GLFWgamepadstate state;

		if (glfwJoystickIsGamepad(input_mngr->first_found_joystick) && glfwGetGamepadState(input_mngr->first_found_joystick, &state))
		{
			axes_count = sizeof(state.axes) / sizeof(float);
			button_count = sizeof(state.buttons);

			axes_values = &state.axes[0];
			button_states = &state.buttons[0];
		}
		else 
		{
			axes_values = glfwGetJoystickAxes(input_mngr->first_found_joystick, &axes_count);
			button_states = glfwGetJoystickButtons(input_mngr->first_found_joystick, &button_count);
		}
		
		if (input_mngr->button_pressed == NULL)
			input_mngr->button_pressed = (unsigned char*)wf_calloc(button_count, sizeof(unsigned char));

		for (int16_t ax = 0; ax < axes_count; ax++)
		{
			float axe_value = axes_values[ax];
			input_mngr->module_manager->game_joystick_axe((int16_t)input_mngr->first_found_joystick, ax, axe_value);
		}

		for (int16_t ab = 0; ab < button_count; ab++)
		{
			unsigned char button_value = button_states[ab];

			if (button_value == GLFW_PRESS)
			{
				input_mngr->module_manager->game_joystick_button((int16_t)input_mngr->first_found_joystick, ab, wtrue);
				input_mngr->button_pressed[ab] = GLFW_PRESS;
			}
			else if (button_value == GLFW_RELEASE)
			{
				if (input_mngr->button_pressed[ab] == GLFW_PRESS)
				{
					input_mngr->module_manager->game_joystick_button((int16_t)input_mngr->first_found_joystick, ab, wfalse);
					input_mngr->button_pressed[ab] = GLFW_RELEASE;
				}
			}

		}

	}
}

void input_glfw_sendinputmessage(input_mngr_t* const input_mngr,uint32_t uMsg,uint32_t wParam,long lParam)
{
	#if defined(_WIN32)
		glfwCustomPoll(input_mngr->render_manager->oglWindow,uMsg,wParam,lParam);
	#endif
}

void input_glfw_poll()
{
	glfwPollEvents();
}

const char* input_glfw_getcontrollername(input_mngr_t* const input_mngr)
{
	if (input_mngr->first_found_joystick != -1)
	{
		if (glfwJoystickIsGamepad(input_mngr->first_found_joystick)) {
			return glfwGetGamepadName(input_mngr->first_found_joystick);
		}
		else {
			return glfwGetJoystickName(input_mngr->first_found_joystick);
		}
	}

	return NULL;
}

int input_glfw_getkey(input_mngr_t* const input_mngr,int key)
{
	return glfwGetKey(input_mngr->render_manager->oglWindow,key);
}

const char* input_glfw_getkeyname(input_mngr_t* const input_mngr, int key)
{
	int scancode = glfwGetKeyScancode(key);
	const char* glfwret = glfwGetKeyName(key, scancode);
	
	if (glfwret != NULL) {
		return glfwret;
	}

	switch (key)
	{
		case GLFW_KEY_SPACE:
			return "SPACE";
		case GLFW_KEY_UP:
			return "UP";
		case GLFW_KEY_DOWN:
			return "DOWN";
		case GLFW_KEY_LEFT:
			return "LEFT";
		case GLFW_KEY_RIGHT:
			return "RIGHT";
		case GLFW_KEY_ESCAPE:
			return "ESC";
		case GLFW_KEY_TAB:
			return "TAB";
		case GLFW_KEY_ENTER:
			return "ENTER";
		case GLFW_KEY_RIGHT_CONTROL:
			return "RCTRL";
		case GLFW_KEY_LEFT_CONTROL:
			return "LCTRL";
		case GLFW_KEY_LEFT_ALT:
			return "LALT";
		case GLFW_KEY_RIGHT_ALT:
			return "RALT";
		case GLFW_KEY_LEFT_SHIFT:
			return "LSHIFT";
		case GLFW_KEY_RIGHT_SHIFT:
			return "RSHIFT";
		case GLFW_KEY_BACKSPACE:
			return "BCK";
		case GLFW_KEY_PAGE_DOWN:
			return "PGDOWN";
		case GLFW_KEY_PAGE_UP:
			return "PGUP";
		default:
			return NULL;

	}

}

gamepad_scheme input_glfw_getscheme(input_mngr_t* const input_mngr)
{
	const char* name = glfwGetJoystickName(input_mngr->first_found_joystick);
	const char* name2 = glfwGetGamepadName(input_mngr->first_found_joystick);

	if (strcmp(name, "Xbox Controller") == 0 || strcmp(name2, "Xbox Controller") == 0 || strcmp(name, "Xbox One Controller") == 0 || strcmp(name2, "Xbox One Controller") == 0 || strcmp(name, "Xbox 360 Controller") == 0 || strcmp(name2, "Xbox 360 Controller") == 0)
	{
		return XBOX_INPUT;
	}


	if (strcmp(name, "PS4 Controller") == 0 || strcmp(name2, "PS4 Controller") == 0 || strcmp(name, "PS5 Controller") == 0 || strcmp(name2, "PS5 Controller") == 0 || strcmp(name, "PS3 Controller") == 0 || strcmp(name2, "PS3 Controller") == 0)
	{
		return PLAYSTATION_INPUT;
	}

	return GENERIC_INPUT;
}


const char* input_glfw_getbuttonname(input_mngr_t* const input_mngr, int16_t button, gamepad_scheme scheme)
{
	if (scheme == PLAYSTATION_INPUT)
	{
		switch (button)
		{
		case GLFW_GAMEPAD_BUTTON_CROSS:
			return "CROSS";
		case GLFW_GAMEPAD_BUTTON_CIRCLE:
			return "CIRCLE";
		case GLFW_GAMEPAD_BUTTON_SQUARE:
			return "SQUARE";
		case GLFW_GAMEPAD_BUTTON_TRIANGLE:
			return "TRIANGLE";
		case GLFW_GAMEPAD_BUTTON_LEFT_BUMPER:
			return "BTN_L1";
		case GLFW_GAMEPAD_BUTTON_RIGHT_BUMPER:
			return "BTN_R1";
		case GLFW_GAMEPAD_BUTTON_BACK:
			return "BTN_SHARE";
		case GLFW_GAMEPAD_BUTTON_START:
			return "BTN_OPTIONS";
		case GLFW_GAMEPAD_BUTTON_GUIDE:
			return "BTN_PLAYSTATION";
		case GLFW_GAMEPAD_BUTTON_LEFT_THUMB:
			return "BTN_L3";
		case GLFW_GAMEPAD_BUTTON_RIGHT_THUMB:
			return "BTN_R3";
		case GLFW_GAMEPAD_BUTTON_DPAD_UP:
			return "DPAD_UP";
		case GLFW_GAMEPAD_BUTTON_DPAD_RIGHT:
			return "DPAD_RIGHT";
		case GLFW_GAMEPAD_BUTTON_DPAD_DOWN:
			return "DPAD_DOWN";
		case GLFW_GAMEPAD_BUTTON_DPAD_LEFT:
			return "DPAD_LEFT";
		}
	}
	else if (scheme == XBOX_INPUT || scheme == GENERIC_INPUT)
	{
		switch (button)
		{
			case GLFW_GAMEPAD_BUTTON_A:
				return "BTN_A";
			case GLFW_GAMEPAD_BUTTON_B:
				return "BTN_B";
			case GLFW_GAMEPAD_BUTTON_X:
				return "BTN_X";
			case GLFW_GAMEPAD_BUTTON_Y:
				return "BTN_Y";
			case GLFW_GAMEPAD_BUTTON_LEFT_BUMPER:
				return "BTN_LB";
			case GLFW_GAMEPAD_BUTTON_RIGHT_BUMPER:
				return "BTN_RB";
			case GLFW_GAMEPAD_BUTTON_BACK:
				return "BTN_BACK";
			case GLFW_GAMEPAD_BUTTON_START:
				return "BTN_START";
			case GLFW_GAMEPAD_BUTTON_LEFT_THUMB:
				return "BTN_LJT";
			case GLFW_GAMEPAD_BUTTON_RIGHT_THUMB:
				return "BTN_RJB";
			case GLFW_GAMEPAD_BUTTON_DPAD_UP:
				return "DPAD_UP";
			case GLFW_GAMEPAD_BUTTON_DPAD_RIGHT:
				return "DPAD_RIGHT";
			case GLFW_GAMEPAD_BUTTON_DPAD_DOWN:
				return "DPAD_DOWN";
			case GLFW_GAMEPAD_BUTTON_DPAD_LEFT:
				return "DPAD_LEFT";
		}
	}

	return NULL;
}

const char* input_glfw_getaxisname(input_mngr_t* const input_mngr, int16_t axis, gamepad_scheme scheme)
{
	if (scheme == PLAYSTATION_INPUT)
	{
		switch (axis)
		{
			case GLFW_GAMEPAD_AXIS_LEFT_X:
				return "AXIS_LEFT_X";
			case GLFW_GAMEPAD_AXIS_LEFT_Y:
				return "AXIS_LEFT_Y";
			case GLFW_GAMEPAD_AXIS_RIGHT_X:
				return "AXIS_RIGHT_X";
			case GLFW_GAMEPAD_AXIS_RIGHT_Y:
				return "AXIS_RIGHT_Y";
			case GLFW_GAMEPAD_AXIS_LEFT_TRIGGER:
				return "BTN_L2";
			case GLFW_GAMEPAD_AXIS_RIGHT_TRIGGER:
				return "BTN_R2";
		}
	}
	else if (scheme == XBOX_INPUT || scheme == GENERIC_INPUT)
	{
		switch (axis)
		{
			case GLFW_GAMEPAD_AXIS_LEFT_X:
				return "AXIS_LEFT_X";
			case GLFW_GAMEPAD_AXIS_LEFT_Y:
				return "AXIS_LEFT_Y";
			case GLFW_GAMEPAD_AXIS_RIGHT_X:
				return "AXIS_RIGHT_X";
			case GLFW_GAMEPAD_AXIS_RIGHT_Y:
				return "AXIS_RIGHT_Y";
			case GLFW_GAMEPAD_AXIS_LEFT_TRIGGER:
				return "BTN_LT";
			case GLFW_GAMEPAD_AXIS_RIGHT_TRIGGER:
				return "BTN_RT";
		}
	}

	return NULL;
}

wbool input_glfw_is_trigger(input_mngr_t* const input_mngr, int16_t axis)
{
	switch (axis)
	{
		case GLFW_GAMEPAD_AXIS_LEFT_TRIGGER:
		case GLFW_GAMEPAD_AXIS_RIGHT_TRIGGER:
			return wtrue;
	}

	return wfalse;

}

#endif
