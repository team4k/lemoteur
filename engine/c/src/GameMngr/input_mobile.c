#ifdef USE_MOBILE_INPUT

#ifdef _MSC_VER
       extern "C" {
#endif
	#include "lauxlib.h"
#include "lualib.h"
	#include <lua.h>

#ifdef _MSC_VER
}
#endif


#if defined(_WIN32)
        #include <windows.h>
#else
	#ifndef __USE_BSD
	        #define __USE_BSD
	#endif
        #include <unistd.h>
        #include <dlfcn.h>
#endif

#include <Render.h>

#include <inttypes.h>


#ifndef EMSCRIPTEN
	#include <portaudio.h>
#else
	#include <emscripten.h>
	#include <SDL2/SDL.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <time.h>
#include <wchar.h>
#include <math.h>
#include <string.h>
#include <sqlite3.h>

#include <Engine.h>
#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>

#include <Base/geom.h>
#include <Utils/Hash.h>
#include <Utils/hashmap.h>

#include <Render/render_manager.h>

#include <GameObjects/Shaders.pb-c.h>
#include <Graphics/TextureLoader.h>
#include <Resx/shader_loader.h>

#include <GameObjects/Characters.pb-c.h>
#include <Graphics/Animation.h>
#include <Graphics/Sprite.h>
#include <Graphics/Mesh.h>

#include <Graphics/Light.h>
#include <Graphics/LightManager.h>

#include <Debug/Logprint.h>


#include <Graphics/TextureRegion.h>
#include <Graphics/SpriteBatch.h>
#include <Graphics/FontLoader.h>

#include "GameObjects/Level.pb-c.h"

#include <Sound/Sound.h>
       

#include <Resx/Resources.h>
#include <Resx/localization.h>

#include <Graphics/TilemapLayer.h>
#include <GameObjects/Tilemap.h>

#include <chipmunk/chipmunk.h>
#include <Physics/Physics.h>


#include <GameObjects/entity.h>
#include <GameObjects/Trigger.h>

#include <Config/config.h>

#include <GameMngr/input_mngr.h>
#include <game_functions.h>
#include <GameMngr/module_mngr.h>



static int checkspace_input_buffer(input_buffer_t *p, int writeCheck)
{
	 int wp = p->wp, rp = p->rp;
     if(writeCheck){
          if (wp > rp) 
               return rp - wp + INPUT_BUFFER_SIZE - 1;
          else if (wp < rp) 
               return rp - wp - 1;
          else 
               return INPUT_BUFFER_SIZE - 1;
     }
     else {
          if (wp > rp) 
               return wp - rp;
          else if (wp < rp) 
               return wp - rp + INPUT_BUFFER_SIZE;
          else 
               return 0;
     }
}

static input_entry_t* read_input_buffer_bytes(input_buffer_t *p)
{
	 int remaining;
     int input_read, size = INPUT_BUFFER_SIZE;
     int i=0, rp = p->rp;
	 input_entry_t* out;
	 
     if ((remaining = checkspace_input_buffer(p, 0)) == 0) {
          return NULL;
     }
	 
	 out = &p->buffer[rp++];
	 
	 if(rp == size) 
		   rp = 0;
		   
	  p->rp = rp;
     
     return out;
}

static int write_input_buffer(input_buffer_t *p, const input_entry_t* in)
{
	int remaining;
	 int input_read, size = INPUT_BUFFER_SIZE;
	 int i=0, wp = p->wp;
	 
	 if ((remaining = checkspace_input_buffer(p, 1)) == 0) {
		  return 0;
	 }
	 
	p->buffer[wp].input_data.x = in->input_data.x;
	p->buffer[wp].input_data.y = in->input_data.y;
	p->buffer[wp++].action = in->action;
	
	
	if(wp == size) 
		   wp = 0;
		   
	 p->wp = wp;
	 return 1;
}

void input_mobile_init(input_mngr_t* const input_mngr)
{
	input_mngr->input_buffer = (input_buffer_t){0};
}

void input_mobile_touch(input_mngr_t* const input_mngr,wfloat posx,wfloat posy,int action)
{
	input_entry_t entry;
	entry.input_data.x = posx;
	entry.input_data.y = posy;
	entry.action = action;
	
	if(write_input_buffer(&input_mngr->input_buffer,&entry) == 0)
		logprint("No more space in input buffer!");
}

void input_mobile_backbutton(input_mngr_t* const input_mngr)
{
	input_entry_t entry;
	entry.input_data.x = 0;
	entry.input_data.y = 0;
	entry.action = ACTION_BACKBUTTON;
	
	if(write_input_buffer(&input_mngr->input_buffer,&entry) == 0)
		logprint("No more space in input buffer!");
}

void input_mobile_handleinput(input_mngr_t* const input_mngr)
{
	input_entry_t* tmp_input_entry;
	Vector_t tmp_coord = vectorzero;
	
	while((tmp_input_entry = read_input_buffer_bytes(&input_mngr->input_buffer)) != NULL)
	{
		if(tmp_input_entry->action == ACTION_BACKBUTTON)
		{
			input_mngr->module_manager->game_handlebackbutton();
		}
		else
		{
			tmp_coord = input_getglobalcoords(input_mngr,tmp_input_entry->input_data.x,tmp_input_entry->input_data.y);
			
			if(tmp_input_entry->action == ACTION_DOWN || tmp_input_entry->action == ACTION_POINTER_DOWN)
				input_mngr->module_manager->game_target(tmp_coord.x,tmp_coord.y,wtrue,0);
			else if(tmp_input_entry->action == ACTION_UP || tmp_input_entry->action == ACTION_POINTER_UP)
				input_mngr->module_manager->game_endtarget(tmp_coord.x,tmp_coord.y,wtrue,0);
			else if(tmp_input_entry->action == ACTION_MOVE)
				input_mngr->module_manager->game_movetarget(tmp_coord.x,tmp_coord.y,wtrue);
		}
	}
}

#endif