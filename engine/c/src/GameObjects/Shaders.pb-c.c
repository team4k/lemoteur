/* Generated by the protocol buffer compiler.  DO NOT EDIT! */
/* Generated from: Shaders.proto */

/* Do not generate deprecated warnings for self */
#ifndef PROTOBUF_C__NO_DEPRECATED
#define PROTOBUF_C__NO_DEPRECATED
#endif

#include "GameObjects/Shaders.pb-c.h"
void   d3_dshaderparameter__init
                     (D3Dshaderparameter         *message)
{
  static const D3Dshaderparameter init_value = D3_DSHADERPARAMETER__INIT;
  *message = init_value;
}
size_t d3_dshaderparameter__get_packed_size
                     (const D3Dshaderparameter *message)
{
  assert(message->base.descriptor == &d3_dshaderparameter__descriptor);
  return protobuf_c_message_get_packed_size ((const ProtobufCMessage*)(message));
}
size_t d3_dshaderparameter__pack
                     (const D3Dshaderparameter *message,
                      uint8_t       *out)
{
  assert(message->base.descriptor == &d3_dshaderparameter__descriptor);
  return protobuf_c_message_pack ((const ProtobufCMessage*)message, out);
}
size_t d3_dshaderparameter__pack_to_buffer
                     (const D3Dshaderparameter *message,
                      ProtobufCBuffer *buffer)
{
  assert(message->base.descriptor == &d3_dshaderparameter__descriptor);
  return protobuf_c_message_pack_to_buffer ((const ProtobufCMessage*)message, buffer);
}
D3Dshaderparameter *
       d3_dshaderparameter__unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const uint8_t       *data)
{
  return (D3Dshaderparameter *)
     protobuf_c_message_unpack (&d3_dshaderparameter__descriptor,
                                allocator, len, data);
}
void   d3_dshaderparameter__free_unpacked
                     (D3Dshaderparameter *message,
                      ProtobufCAllocator *allocator)
{
  if(!message)
    return;
  assert(message->base.descriptor == &d3_dshaderparameter__descriptor);
  protobuf_c_message_free_unpacked ((ProtobufCMessage*)message, allocator);
}
void   d3_dshaderconstantbuffer__init
                     (D3Dshaderconstantbuffer         *message)
{
  static const D3Dshaderconstantbuffer init_value = D3_DSHADERCONSTANTBUFFER__INIT;
  *message = init_value;
}
size_t d3_dshaderconstantbuffer__get_packed_size
                     (const D3Dshaderconstantbuffer *message)
{
  assert(message->base.descriptor == &d3_dshaderconstantbuffer__descriptor);
  return protobuf_c_message_get_packed_size ((const ProtobufCMessage*)(message));
}
size_t d3_dshaderconstantbuffer__pack
                     (const D3Dshaderconstantbuffer *message,
                      uint8_t       *out)
{
  assert(message->base.descriptor == &d3_dshaderconstantbuffer__descriptor);
  return protobuf_c_message_pack ((const ProtobufCMessage*)message, out);
}
size_t d3_dshaderconstantbuffer__pack_to_buffer
                     (const D3Dshaderconstantbuffer *message,
                      ProtobufCBuffer *buffer)
{
  assert(message->base.descriptor == &d3_dshaderconstantbuffer__descriptor);
  return protobuf_c_message_pack_to_buffer ((const ProtobufCMessage*)message, buffer);
}
D3Dshaderconstantbuffer *
       d3_dshaderconstantbuffer__unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const uint8_t       *data)
{
  return (D3Dshaderconstantbuffer *)
     protobuf_c_message_unpack (&d3_dshaderconstantbuffer__descriptor,
                                allocator, len, data);
}
void   d3_dshaderconstantbuffer__free_unpacked
                     (D3Dshaderconstantbuffer *message,
                      ProtobufCAllocator *allocator)
{
  if(!message)
    return;
  assert(message->base.descriptor == &d3_dshaderconstantbuffer__descriptor);
  protobuf_c_message_free_unpacked ((ProtobufCMessage*)message, allocator);
}
void   d3_dshaderlayout__init
                     (D3Dshaderlayout         *message)
{
  static const D3Dshaderlayout init_value = D3_DSHADERLAYOUT__INIT;
  *message = init_value;
}
size_t d3_dshaderlayout__get_packed_size
                     (const D3Dshaderlayout *message)
{
  assert(message->base.descriptor == &d3_dshaderlayout__descriptor);
  return protobuf_c_message_get_packed_size ((const ProtobufCMessage*)(message));
}
size_t d3_dshaderlayout__pack
                     (const D3Dshaderlayout *message,
                      uint8_t       *out)
{
  assert(message->base.descriptor == &d3_dshaderlayout__descriptor);
  return protobuf_c_message_pack ((const ProtobufCMessage*)message, out);
}
size_t d3_dshaderlayout__pack_to_buffer
                     (const D3Dshaderlayout *message,
                      ProtobufCBuffer *buffer)
{
  assert(message->base.descriptor == &d3_dshaderlayout__descriptor);
  return protobuf_c_message_pack_to_buffer ((const ProtobufCMessage*)message, buffer);
}
D3Dshaderlayout *
       d3_dshaderlayout__unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const uint8_t       *data)
{
  return (D3Dshaderlayout *)
     protobuf_c_message_unpack (&d3_dshaderlayout__descriptor,
                                allocator, len, data);
}
void   d3_dshaderlayout__free_unpacked
                     (D3Dshaderlayout *message,
                      ProtobufCAllocator *allocator)
{
  if(!message)
    return;
  assert(message->base.descriptor == &d3_dshaderlayout__descriptor);
  protobuf_c_message_free_unpacked ((ProtobufCMessage*)message, allocator);
}
void   d3_dshaderinfo__init
                     (D3Dshaderinfo         *message)
{
  static const D3Dshaderinfo init_value = D3_DSHADERINFO__INIT;
  *message = init_value;
}
size_t d3_dshaderinfo__get_packed_size
                     (const D3Dshaderinfo *message)
{
  assert(message->base.descriptor == &d3_dshaderinfo__descriptor);
  return protobuf_c_message_get_packed_size ((const ProtobufCMessage*)(message));
}
size_t d3_dshaderinfo__pack
                     (const D3Dshaderinfo *message,
                      uint8_t       *out)
{
  assert(message->base.descriptor == &d3_dshaderinfo__descriptor);
  return protobuf_c_message_pack ((const ProtobufCMessage*)message, out);
}
size_t d3_dshaderinfo__pack_to_buffer
                     (const D3Dshaderinfo *message,
                      ProtobufCBuffer *buffer)
{
  assert(message->base.descriptor == &d3_dshaderinfo__descriptor);
  return protobuf_c_message_pack_to_buffer ((const ProtobufCMessage*)message, buffer);
}
D3Dshaderinfo *
       d3_dshaderinfo__unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const uint8_t       *data)
{
  return (D3Dshaderinfo *)
     protobuf_c_message_unpack (&d3_dshaderinfo__descriptor,
                                allocator, len, data);
}
void   d3_dshaderinfo__free_unpacked
                     (D3Dshaderinfo *message,
                      ProtobufCAllocator *allocator)
{
  if(!message)
    return;
  assert(message->base.descriptor == &d3_dshaderinfo__descriptor);
  protobuf_c_message_free_unpacked ((ProtobufCMessage*)message, allocator);
}
static const ProtobufCFieldDescriptor d3_dshaderparameter__field_descriptors[3] =
{
  {
    "name",
    1,
    PROTOBUF_C_LABEL_REQUIRED,
    PROTOBUF_C_TYPE_STRING,
    0,   /* quantifier_offset */
    offsetof(D3Dshaderparameter, name),
    NULL,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "type_param",
    2,
    PROTOBUF_C_LABEL_REQUIRED,
    PROTOBUF_C_TYPE_INT32,
    0,   /* quantifier_offset */
    offsetof(D3Dshaderparameter, type_param),
    NULL,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "semantic",
    3,
    PROTOBUF_C_LABEL_OPTIONAL,
    PROTOBUF_C_TYPE_INT32,
    offsetof(D3Dshaderparameter, has_semantic),
    offsetof(D3Dshaderparameter, semantic),
    NULL,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
};
static const unsigned d3_dshaderparameter__field_indices_by_name[] = {
  0,   /* field[0] = name */
  2,   /* field[2] = semantic */
  1,   /* field[1] = type_param */
};
static const ProtobufCIntRange d3_dshaderparameter__number_ranges[1 + 1] =
{
  { 1, 0 },
  { 0, 3 }
};
const ProtobufCMessageDescriptor d3_dshaderparameter__descriptor =
{
  PROTOBUF_C__MESSAGE_DESCRIPTOR_MAGIC,
  "D3Dshaderparameter",
  "D3Dshaderparameter",
  "D3Dshaderparameter",
  "",
  sizeof(D3Dshaderparameter),
  3,
  d3_dshaderparameter__field_descriptors,
  d3_dshaderparameter__field_indices_by_name,
  1,  d3_dshaderparameter__number_ranges,
  (ProtobufCMessageInit) d3_dshaderparameter__init,
  NULL,NULL,NULL    /* reserved[123] */
};
static const ProtobufCFieldDescriptor d3_dshaderconstantbuffer__field_descriptors[2] =
{
  {
    "name",
    1,
    PROTOBUF_C_LABEL_REQUIRED,
    PROTOBUF_C_TYPE_STRING,
    0,   /* quantifier_offset */
    offsetof(D3Dshaderconstantbuffer, name),
    NULL,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "param_list",
    2,
    PROTOBUF_C_LABEL_REPEATED,
    PROTOBUF_C_TYPE_MESSAGE,
    offsetof(D3Dshaderconstantbuffer, n_param_list),
    offsetof(D3Dshaderconstantbuffer, param_list),
    &d3_dshaderparameter__descriptor,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
};
static const unsigned d3_dshaderconstantbuffer__field_indices_by_name[] = {
  0,   /* field[0] = name */
  1,   /* field[1] = param_list */
};
static const ProtobufCIntRange d3_dshaderconstantbuffer__number_ranges[1 + 1] =
{
  { 1, 0 },
  { 0, 2 }
};
const ProtobufCMessageDescriptor d3_dshaderconstantbuffer__descriptor =
{
  PROTOBUF_C__MESSAGE_DESCRIPTOR_MAGIC,
  "D3Dshaderconstantbuffer",
  "D3Dshaderconstantbuffer",
  "D3Dshaderconstantbuffer",
  "",
  sizeof(D3Dshaderconstantbuffer),
  2,
  d3_dshaderconstantbuffer__field_descriptors,
  d3_dshaderconstantbuffer__field_indices_by_name,
  1,  d3_dshaderconstantbuffer__number_ranges,
  (ProtobufCMessageInit) d3_dshaderconstantbuffer__init,
  NULL,NULL,NULL    /* reserved[123] */
};
static const ProtobufCFieldDescriptor d3_dshaderlayout__field_descriptors[1] =
{
  {
    "param_list",
    1,
    PROTOBUF_C_LABEL_REPEATED,
    PROTOBUF_C_TYPE_MESSAGE,
    offsetof(D3Dshaderlayout, n_param_list),
    offsetof(D3Dshaderlayout, param_list),
    &d3_dshaderparameter__descriptor,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
};
static const unsigned d3_dshaderlayout__field_indices_by_name[] = {
  0,   /* field[0] = param_list */
};
static const ProtobufCIntRange d3_dshaderlayout__number_ranges[1 + 1] =
{
  { 1, 0 },
  { 0, 1 }
};
const ProtobufCMessageDescriptor d3_dshaderlayout__descriptor =
{
  PROTOBUF_C__MESSAGE_DESCRIPTOR_MAGIC,
  "D3Dshaderlayout",
  "D3Dshaderlayout",
  "D3Dshaderlayout",
  "",
  sizeof(D3Dshaderlayout),
  1,
  d3_dshaderlayout__field_descriptors,
  d3_dshaderlayout__field_indices_by_name,
  1,  d3_dshaderlayout__number_ranges,
  (ProtobufCMessageInit) d3_dshaderlayout__init,
  NULL,NULL,NULL    /* reserved[123] */
};
static const ProtobufCFieldDescriptor d3_dshaderinfo__field_descriptors[3] =
{
  {
    "shader_name",
    1,
    PROTOBUF_C_LABEL_REQUIRED,
    PROTOBUF_C_TYPE_STRING,
    0,   /* quantifier_offset */
    offsetof(D3Dshaderinfo, shader_name),
    NULL,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "constant_def",
    3,
    PROTOBUF_C_LABEL_OPTIONAL,
    PROTOBUF_C_TYPE_MESSAGE,
    0,   /* quantifier_offset */
    offsetof(D3Dshaderinfo, constant_def),
    &d3_dshaderconstantbuffer__descriptor,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "layout_def",
    4,
    PROTOBUF_C_LABEL_OPTIONAL,
    PROTOBUF_C_TYPE_MESSAGE,
    0,   /* quantifier_offset */
    offsetof(D3Dshaderinfo, layout_def),
    &d3_dshaderlayout__descriptor,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
};
static const unsigned d3_dshaderinfo__field_indices_by_name[] = {
  1,   /* field[1] = constant_def */
  2,   /* field[2] = layout_def */
  0,   /* field[0] = shader_name */
};
static const ProtobufCIntRange d3_dshaderinfo__number_ranges[2 + 1] =
{
  { 1, 0 },
  { 3, 1 },
  { 0, 3 }
};
const ProtobufCMessageDescriptor d3_dshaderinfo__descriptor =
{
  PROTOBUF_C__MESSAGE_DESCRIPTOR_MAGIC,
  "D3Dshaderinfo",
  "D3Dshaderinfo",
  "D3Dshaderinfo",
  "",
  sizeof(D3Dshaderinfo),
  3,
  d3_dshaderinfo__field_descriptors,
  d3_dshaderinfo__field_indices_by_name,
  2,  d3_dshaderinfo__number_ranges,
  (ProtobufCMessageInit) d3_dshaderinfo__init,
  NULL,NULL,NULL    /* reserved[123] */
};
static const ProtobufCEnumValue d3_dshadertype__enum_values_by_number[5] =
{
  { "MATRIX4X4", "D3_DSHADERTYPE__MATRIX4X4", 1 },
  { "FLOAT4", "D3_DSHADERTYPE__FLOAT4", 2 },
  { "FLOAT3", "D3_DSHADERTYPE__FLOAT3", 3 },
  { "FLOAT2", "D3_DSHADERTYPE__FLOAT2", 4 },
  { "FLOAT", "D3_DSHADERTYPE__FLOAT", 5 },
};
static const ProtobufCIntRange d3_dshadertype__value_ranges[] = {
{1, 0},{0, 5}
};
static const ProtobufCEnumValueIndex d3_dshadertype__enum_values_by_name[5] =
{
  { "FLOAT", 4 },
  { "FLOAT2", 3 },
  { "FLOAT3", 2 },
  { "FLOAT4", 1 },
  { "MATRIX4X4", 0 },
};
const ProtobufCEnumDescriptor d3_dshadertype__descriptor =
{
  PROTOBUF_C__ENUM_DESCRIPTOR_MAGIC,
  "D3Dshadertype",
  "D3Dshadertype",
  "D3Dshadertype",
  "",
  5,
  d3_dshadertype__enum_values_by_number,
  5,
  d3_dshadertype__enum_values_by_name,
  1,
  d3_dshadertype__value_ranges,
  NULL,NULL,NULL,NULL   /* reserved[1234] */
};
static const ProtobufCEnumValue d3_dshadersemantic__enum_values_by_number[4] =
{
  { "POSITION", "D3_DSHADERSEMANTIC__POSITION", 1 },
  { "TEXCOORD", "D3_DSHADERSEMANTIC__TEXCOORD", 2 },
  { "NORMAL", "D3_DSHADERSEMANTIC__NORMAL", 3 },
  { "COLOR", "D3_DSHADERSEMANTIC__COLOR", 4 },
};
static const ProtobufCIntRange d3_dshadersemantic__value_ranges[] = {
{1, 0},{0, 4}
};
static const ProtobufCEnumValueIndex d3_dshadersemantic__enum_values_by_name[4] =
{
  { "COLOR", 3 },
  { "NORMAL", 2 },
  { "POSITION", 0 },
  { "TEXCOORD", 1 },
};
const ProtobufCEnumDescriptor d3_dshadersemantic__descriptor =
{
  PROTOBUF_C__ENUM_DESCRIPTOR_MAGIC,
  "D3Dshadersemantic",
  "D3Dshadersemantic",
  "D3Dshadersemantic",
  "",
  4,
  d3_dshadersemantic__enum_values_by_number,
  4,
  d3_dshadersemantic__enum_values_by_name,
  1,
  d3_dshadersemantic__value_ranges,
  NULL,NULL,NULL,NULL   /* reserved[1234] */
};
