#include <inttypes.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>

#include <Engine.h>

#include <Render.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>
#include <Base/geom.h>
#include <Render/render_manager.h>

#include <Graphics/geomdraw.h>

#include <chipmunk/chipmunk.h>

#include "GameObjects/Level.pb-c.h"

#include <Collisions/Collisions.h>

#include <GameObjects/Trigger.h>

void trigger_init(trigger_t* const otrigger,const Trigger* const trigger_data, trigger_callback_t trigger_func,Vector_t offset)
{
	otrigger->activated = wfalse;
	otrigger->width = abs(trigger_data->width);
	otrigger->height = abs(trigger_data->height);
	

	float halfwidth = otrigger->width * 0.5f;
	float halfheight = otrigger->height * 0.5f;

	if(trigger_data->params && strcmp(trigger_data->params,"") != 0)
		w_strcpy(otrigger->params,256,trigger_data->params);
	else
		memset(otrigger->params,0,256);

	otrigger->box = cpBBNew((trigger_data->position->x + offset.x) - halfwidth,(trigger_data->position->y + offset.y) - halfheight,(trigger_data->position->x + offset.x) + halfwidth,(trigger_data->position->y + offset.y) + halfheight);
	otrigger->render_box.position.x = (wfloat)otrigger->box.l;
	otrigger->render_box.position.y =  (wfloat)otrigger->box.t;

	otrigger->origpos = Vec((wfloat)otrigger->box.l, (wfloat)otrigger->box.t);

	otrigger->render_box.width = otrigger->width;
	otrigger->render_box.height = otrigger->height;
	otrigger->trigger_func = trigger_func;
	otrigger->repeat = (wbool)trigger_data->repeat;
	otrigger->is_visible = wtrue;
	memset(otrigger->activator,0,sizeof(otrigger->activator));
	w_strcpy(otrigger->callback_func,256,trigger_data->callback_func);
}

void trigger_update(trigger_t* const otrigger,const cpBB entity_bb,const char* entity_id,const char* module)
{
	if(otrigger->activated && !otrigger->repeat)
		return;

	if(cpBBIntersects(otrigger->box,entity_bb))
	{
		if(!otrigger->activated || strcmp(otrigger->activator,entity_id) != 0)
		{
			otrigger->activated = wtrue;
			otrigger->trigger_func(module,otrigger->callback_func,otrigger->params,entity_id,otrigger->trigger_id);
			w_strcpy(otrigger->activator,256,entity_id);
		}
	}
	else if(otrigger->activated && strcmp(otrigger->activator,entity_id) == 0)
	{
		otrigger->activated = wfalse;
		memset(otrigger->activator,0,sizeof(otrigger->activator));
	}
}

wbool trigger_checkbb(trigger_t* const otrigger, const cpBB entity_bb)
{
	return (wbool)cpBBIntersects(otrigger->box, entity_bb);
}

void trigger_draw(trigger_t* const otrigger,render_manager_t* const render_mngr)
{
	if(!otrigger->is_visible)
		return;

	ColorA_t trigger_color = {1.0f,0.0f,0.9f,0.3f};
	//Rect_t draw_rect = {otrigger->width,otrigger->height,{(float)otrigger->box.l,(float)otrigger->box.t}};
	drawrect(&otrigger->render_box,trigger_color,render_mngr);
}