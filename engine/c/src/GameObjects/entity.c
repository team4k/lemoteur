#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <stdlib.h>
#include <inttypes.h>
#define __USE_MISC 1
#define _USE_MATH_DEFINES 1
#include <math.h>

#include <Render.h>

#include <Engine.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>
#include <Base/geom.h>

#include <Render/render_manager.h>

#include <Debug/Logprint.h>

#include <Utils/Hash.h>


#include <Debug/geomdebug.h>


#include <Graphics/TextureLoader.h>

#include <GameObjects/Level.pb-c.h>
#include <Collisions/Collisions.h>

#include <chipmunk/chipmunk.h>
#include <Physics/Physics.h>


#include <GameObjects/Characters.pb-c.h>
#include <Graphics/Animation.h>
#include <Graphics/Mesh.h>
#include <Graphics/Sprite.h>
#include <GameObjects/entity.h>


void entity_init(entity_t* oentity, const Entity* const  entity_def, texture_info* const texture_inf, physics_manager_t* physicsWorld, const AnimationList* const animation_def, render_manager_t* const render_manager, wbool in_group)
{
	oentity->is_visible = wtrue;
	oentity->in_group = in_group;
	oentity->dynamic = wfalse;
	oentity->set_sprite_rotation = wtrue;
	oentity->current_pass = 0;
	oentity->mesh_obj = NULL;
	oentity->sprite = NULL;
	oentity->start_position = cpvzero;
	oentity->lockedanim = NULL;
	oentity->lockedanimsize = 0;
	oentity->lockedspritepart = 0;
	oentity->nodraw = (wbool)(entity_def->has_nodraw &&  entity_def->nodraw);
	oentity->extraparam1 = oentity->extraparam2 = 0;
	oentity->extraptr1 = NULL;

	if (entity_def->source != NULL)
	{
		w_strcpy(oentity->source, 256, entity_def->source);
	}

	if (entity_def->source_id != NULL)
	{
		w_strcpy(oentity->source_id, 256, entity_def->source_id);
	}

	if (entity_def->script_module != NULL)
	{
		w_strcpy(oentity->script_module, 128, entity_def->script_module);
	}

	if (entity_def->script_ctor != NULL)
	{
		w_strcpy(oentity->script_ctor, 512, entity_def->script_ctor);
	}

	if (animation_def != NULL)
	{
		w_strcpy(oentity->animation_file, 128, entity_def->animation_file);
	}

	float scale_x = 1.0f;
	float scale_y = 1.0f;

	if (entity_def->has_scale_x)
		scale_x = entity_def->scale_x;

	if (entity_def->has_scale_y)
		scale_y = entity_def->scale_y;

	if (!entity_def->has_has_mesh || !entity_def->has_mesh)
	{
		if (entity_def->has_sprite_count) {
			oentity->sprite_count = (uint8_t)entity_def->sprite_count;
		}
		else {
			oentity->sprite_count = 1;
		}


		oentity->sprite = (sprite_t**)wf_calloc(oentity->sprite_count, sizeof(sprite_t*));

		size_t max_anim_name_size = 0;

		for (uint8_t ispr = 0; ispr < oentity->sprite_count; ispr++) {

			oentity->sprite[ispr] = (sprite_t*)wf_calloc(1, sizeof(sprite_t));

			wbool duplicate_texture = wtrue;

			if (entity_def->has_duplicate_texture_only) {
				duplicate_texture = (wbool)entity_def->duplicate_texture_only;
			}

			sprite_init(oentity->sprite[ispr], entity_def->width, entity_def->height, texture_inf->width, texture_inf->height, texture_inf, animation_def, entity_def->repeat_x, entity_def->repeat_y, render_manager, oentity->in_group, entity_def->start_frame, scale_x, scale_y, duplicate_texture);

			if (entity_def->has_mat_flag)
				oentity->sprite[ispr]->mat_flag = entity_def->mat_flag;

			if (oentity->sprite[ispr]->max_anim_name_size > max_anim_name_size) {
				max_anim_name_size = oentity->sprite[ispr]->max_anim_name_size;
			}
		}

		oentity->lockedanim = (char*)wf_calloc(max_anim_name_size, sizeof(char));
		oentity->lockedanimsize = max_anim_name_size;
	}
	else
	{
		oentity->mesh_obj = (Mesh_t*)wf_calloc(1, sizeof(Mesh_t));

		mesh_template_type templ = (mesh_template_type)entity_def->mesh_obj;

		mesh_init(oentity->mesh_obj, entity_def->width, entity_def->height, entity_def->height, templ, render_manager);

		if (entity_def->has_mat_flag)
			oentity->mesh_obj->mat_flag = entity_def->mat_flag;
	}


	oentity->current_texture = texture_inf;

	if (entity_def->shader_program != NULL && strcmp(entity_def->shader_program, "") != 0)
	{
		oentity->program_id = render_get_program(render_manager, entity_def->shader_program);

		w_strcpy(oentity->prog_name, 128, entity_def->shader_program);

		if (entity_def->parameters != NULL && entity_def->n_parameters > 0)
		{
			//copy protobuf parameters
			oentity->num_parameters = entity_def->n_parameters;
			oentity->shader_parameters = (ShadersParameters**)wf_calloc(oentity->num_parameters, sizeof(ShadersParameters*));

			unsigned char* param_buffer = NULL;

			for (uint32_t i_param = 0; i_param < oentity->num_parameters; i_param++)
			{
				size_t size_param = shaders_parameters__get_packed_size(entity_def->parameters[i_param]);

				param_buffer = (unsigned char*)wf_realloc(param_buffer, size_param);
				memset(param_buffer, 0, size_param);
				shaders_parameters__pack(entity_def->parameters[i_param], param_buffer);

				oentity->shader_parameters[i_param] = shaders_parameters__unpack(NULL, size_param, param_buffer);
			}

			if (param_buffer != NULL)
				wf_free(param_buffer);

		}
		else
		{
			oentity->shader_parameters = NULL;
			oentity->num_parameters = 0;
		}
	}
	else
	{
		oentity->program_id = render_get_emptyprogvalue();
		oentity->shader_parameters = NULL;
		oentity->num_parameters = 0;
		memset(oentity->prog_name, 0, sizeof(oentity->prog_name));
	}

	if (entity_def->n_extra_pass > 0 && entity_def->extra_pass != NULL)
	{
		//copy protobuf parameters
		oentity->num_pass = entity_def->n_extra_pass;
		oentity->render_pass = (RenderPass**)wf_calloc(oentity->num_pass, sizeof(RenderPass*));

		unsigned char* pass_buffer = NULL;

		for (uint32_t i_pass = 0; i_pass < entity_def->n_extra_pass; i_pass++)
		{
			size_t size_pass = render_pass__get_packed_size(entity_def->extra_pass[i_pass]);

			pass_buffer = (unsigned char*)wf_realloc(pass_buffer, size_pass);
			memset(pass_buffer, 0, size_pass);
			render_pass__pack(entity_def->extra_pass[i_pass], pass_buffer);

			oentity->render_pass[i_pass] = render_pass__unpack(NULL, size_pass, pass_buffer);
		}

		if (pass_buffer != NULL)
			wf_free(pass_buffer);
	}

	oentity->callback_hash = NULL;
	oentity->callback_keys = NULL;
	oentity->callback_scriptfuncs = NULL;
	oentity->z_order = entity_def->z_order;
	oentity->last_touched_surface = 0;
	oentity->freeze_pos = cpv((float)entity_def->position->x, oentity->y = (float)entity_def->position->y);
	oentity->sprite_freezed = wfalse;
	oentity->no_reset = wfalse;


	oentity->offsetx = entity_def->offsetx;
	oentity->offsety = entity_def->offsety;

	if (oentity->sprite)
	{
		for (uint8_t ispr = 0; ispr < oentity->sprite_count; ispr++)
		{
			oentity->sprite[ispr]->render_box.width = entity_def->width;
			oentity->sprite[ispr]->render_box.height = entity_def->height;
		}

	}
	else if (oentity->mesh_obj)
	{
		oentity->mesh_obj->render_box.width = entity_def->width;
		oentity->mesh_obj->render_box.height = entity_def->height;
	}


	if (oentity->sprite)
	{
		for (uint8_t ispr = 0; ispr < oentity->sprite_count; ispr++)
		{
			oentity->sprite[ispr]->render_box.position.x = (entity_def->position->x - (oentity->sprite[0]->render_box.width * 0.5f)) + oentity->offsetx;
			oentity->sprite[ispr]->render_box.position.y = (entity_def->position->y + (oentity->sprite[0]->render_box.height * 0.5f)) + oentity->offsety;
		}
	}
	else if (oentity->mesh_obj)
	{
		oentity->mesh_obj->render_box.position.x = (entity_def->position->x - (oentity->mesh_obj->render_box.width * 0.5f)) + oentity->offsetx;
		oentity->mesh_obj->render_box.position.y = (entity_def->position->y + (oentity->mesh_obj->render_box.height * 0.5f)) + oentity->offsety;
	}


	oentity->constraint_x = wfalse;
	oentity->constraint_y = wfalse;
	oentity->link_constraint = NULL;
	oentity->link_constraint_activated = wfalse;
	oentity->dorotation = wfalse;
	oentity->shape_num = 0;
	oentity->pin_constraint = NULL;

	oentity->entity_parent = 0;

	if (entity_def->has_callback)
	{

		oentity->callback_hash = (hashtable_t*)wf_malloc(sizeof(hashtable_t));
		oentity->callback_keys = (char**)wf_calloc(ENTITY_MAX_CALLBACK, sizeof(char*));
		oentity->callback_hash->keys = oentity->callback_keys;
		oentity->callback_hash->max_elem = ENTITY_MAX_CALLBACK;
		oentity->callback_hash->num_elem = 0;

		oentity->callback_scriptfuncs = (char**)wf_calloc(ENTITY_MAX_CALLBACK, sizeof(char*));


		for (int32_t call_indx = 0; call_indx < ENTITY_MAX_CALLBACK; call_indx++)
		{
			oentity->callback_keys[call_indx] = 0;
			oentity->callback_scriptfuncs[call_indx] = 0;
		}
	}

	if (entity_def->no_collisions)
	{
		oentity->body = NULL;
		oentity->shape = NULL;
		oentity->x = (float)entity_def->position->x;
		oentity->y = (float)entity_def->position->y;
		oentity->static_ent = wtrue;
		oentity->start_position = cpv(oentity->x, oentity->y);

		oentity->parallax.x = entity_def->parallax_x;
		oentity->parallax.y = entity_def->parallax_y;

		return;
	}

	oentity->in_physics_simulation = wtrue;
	oentity->static_ent = wfalse;

	cpBody* entityBody = NULL;

	if (entity_def->kinematic_body) {
		entityBody = cpBodyNewKinematic();
	}
	else {
		entityBody = cpBodyNew(10.0f, INFINITY);
	}

	oentity->body = cpSpaceAddBody(physicsWorld->world, entityBody);
	cpBodySetPosition(oentity->body, cpv(entity_def->position->x, entity_def->position->y));
	oentity->start_position = cpv(entity_def->position->x, entity_def->position->y);



	switch (entity_def->shape_type)
	{
		case TYPE_PHYSICS__BOX:
		{
			oentity->shape = (cpShape**)wf_malloc(sizeof(cpShape*));

			cpBB box_bb;

			if (entity_def->custom_collisions)
			{
				cpFloat hw = (float)entity_def->col_width / 2.0f;
				cpFloat hh = (float)entity_def->col_height / 2.0f;

				box_bb = cpBBNew(-hw, -hh, hw, hh);
			}
			else
			{
				cpFloat hw = (float)entity_def->width / 2.0f;
				cpFloat hh = (float)entity_def->height / 2.0f;

				box_bb = cpBBNew(-hw, -hh, hw, hh);
			}

			oentity->shape[0] = cpBoxShapeNew2(oentity->body, box_bb, entity_def->col_radius);

			oentity->shape_num = 1;
		}

		break;
		case TYPE_PHYSICS__CIRCLE:
		{
			oentity->shape = (cpShape**)wf_malloc(sizeof(cpShape*));

			if (entity_def->custom_collisions)
				oentity->shape[0] = cpCircleShapeNew(oentity->body, fmax((float)entity_def->col_width, (float)entity_def->col_height) * 0.5f, cpvzero);
			else
				oentity->shape[0] = cpCircleShapeNew(oentity->body, fmax((float)entity_def->width, (float)entity_def->height) * 0.5f, cpvzero);

			oentity->shape_num = 1;
		}

		break;
		case TYPE_PHYSICS__U_SHAPE:
		{
			oentity->shape = (cpShape**)wf_malloc(sizeof(cpShape*) * 3);

			cpVect u_poly[4];

			u_poly[0] = cpv(8, 32);
			u_poly[1] = cpv(8, -32);
			u_poly[2] = cpv(-8, -32);
			u_poly[3] = cpv(-8, 16);


			oentity->shape[0] = cpPolyShapeNew(oentity->body, 4, &u_poly[0], cpTransformTranslate(cpv(-40, 0)), 0.0f);

			u_poly[0] = cpv(32, 16);
			u_poly[1] = cpv(32, -16);
			u_poly[2] = cpv(-32, -16);
			u_poly[3] = cpv(-32, 16);

			oentity->shape[1] = cpPolyShapeNew(oentity->body, 4, &u_poly[0], cpTransformTranslate(cpv(0, -16)), 0.0f);

			u_poly[0] = cpv(8, 16);
			u_poly[1] = cpv(8, -32);
			u_poly[2] = cpv(-8, -32);
			u_poly[3] = cpv(-8, 32);


			oentity->shape[2] = cpPolyShapeNew(oentity->body, 4, &u_poly[0], cpTransformTranslate(cpv(40, 0)), 0.0f);
			oentity->shape_num = 3;
		}

		break;

		case TYPE_PHYSICS__MULTI_SHAPES:
		{
			oentity->shape = (cpShape**)wf_malloc(sizeof(cpShape*) * entity_def->n_custom_shapes_def);
			oentity->shape_num = entity_def->n_custom_shapes_def;

			for (uint32_t i_mshape = 0; i_mshape < entity_def->n_custom_shapes_def; i_mshape++)
			{
				switch (entity_def->custom_shapes_def[i_mshape]->type)
				{
				case TYPE_PHYSICS__BOX:
				{
					cpFloat hw = (float)entity_def->custom_shapes_def[i_mshape]->width / 2.0f;
					cpFloat hh = (float)entity_def->custom_shapes_def[i_mshape]->height / 2.0f;

					cpBB box_bb = cpBBNew(-hw, -hh, hw, hh);
					box_bb.l += entity_def->custom_shapes_def[i_mshape]->offset->x;
					box_bb.r += entity_def->custom_shapes_def[i_mshape]->offset->x;

					box_bb.t += entity_def->custom_shapes_def[i_mshape]->offset->y;
					box_bb.b += entity_def->custom_shapes_def[i_mshape]->offset->y;

					oentity->shape[i_mshape] = cpBoxShapeNew2(oentity->body, box_bb, 0.0f);

				}
				break;

				case TYPE_PHYSICS__CIRCLE:
				{
					oentity->shape[i_mshape] = cpCircleShapeNew(oentity->body, fmax((float)entity_def->custom_shapes_def[i_mshape]->width, (float)entity_def->custom_shapes_def[i_mshape]->height) * 0.5f, cpv(entity_def->custom_shapes_def[i_mshape]->offset->x, entity_def->custom_shapes_def[i_mshape]->offset->y));
				}
				break;
				}
			}
		}

		break;
	}

	oentity->shape_type = entity_def->shape_type;

	if (!oentity->shape && oentity->shape_type != TYPE_PHYSICS__CUSTOM_SHAPE)
	{
		logprint("No shape type defined for entity %s", entity_def->entity_id);

		cpSpaceRemoveBody(physicsWorld->world, oentity->body);
		cpBodyFree(oentity->body);

		return;
	}
	else
	{
		for (int32_t ishape = 0; ishape < oentity->shape_num; ishape++)
			cpShapeSetUserData(oentity->shape[ishape], oentity);

		cpBodySetUserData(oentity->body, oentity);
	}

	if (entity_def->has_controller)
	{
		//add control body for entity
		oentity->controlBody = cpBodyNewKinematic();
		cpBodySetPosition(oentity->controlBody, cpv(entity_def->position->x, entity_def->position->y));
		//
		//default max force infinity
		//default max bias infinity
		oentity->move_constraint = cpSpaceAddConstraint(physicsWorld->world, cpPivotJointNew2(oentity->controlBody, oentity->body, cpvzero, cpvzero));
		//cpConstraintSetMaxBias(oentity->move_constraint, 0); // disable joint correction
		cpConstraintSetErrorBias(oentity->move_constraint, cpfpow(1.0f - 0.15f, 60.0f));
		oentity->move_constraint_activated = wtrue;

	}

	oentity->ori_mask = LAYER_PLAYER | LAYER_ENTITY | LAYER_STATIC;
	oentity->ori_layer = LAYER_ENTITY;

	for (int32_t ishape = 0; ishape < oentity->shape_num; ishape++)
	{
		cpSpaceAddShape(physicsWorld->world, oentity->shape[ishape]);

		cpShapeFilter filter = cpShapeFilterNew(CP_NO_GROUP, oentity->ori_layer, oentity->ori_mask);

		cpShapeSetFilter(oentity->shape[ishape], filter);
		cpShapeSetElasticity(oentity->shape[ishape], 0.0f);
		cpShapeSetFriction(oentity->shape[ishape], 2.0f);
	}


}


void entity_setcustomshape(entity_t* const avt, Vector_t* verts, int count_verts, wbool set_sprite_verts, physics_manager_t* const physicsWorld, render_manager_t* const render_mngr)
{
	if (avt->shape_type != TYPE_PHYSICS__CUSTOM_SHAPE)
	{
		logprint("can't set custom shape for entity with type_physics not set to TYPE_PHYSICS__CUSTOM_SHAPE!");
		return;
	}

	//clean existing shape before
	if (avt->shape != NULL)
	{
		cpSpaceRemoveShape(physicsWorld->world, avt->shape[0]);
		cpShapeFree(avt->shape[0]);

		wf_free(avt->shape);

		avt->shape = NULL;

		avt->shape_num = 0;

	}



	avt->shape = (cpShape**)wf_malloc(sizeof(cpShape*));
	avt->shape_num = 1;

	cpVect* poly = (cpVect*)wf_calloc(count_verts, sizeof(cpVect));

	int io = 0;

	for (int i_v = count_verts - 1; i_v >= 0; i_v--)
	{
		poly[io].x = verts[i_v].x;
		poly[io].y = verts[i_v].y;
		io++;
	}


	avt->shape[0] = cpPolyShapeNew(avt->body, count_verts, poly, cpTransformIdentity, 0.0f);

	cpShapeSetUserData(avt->shape[0], avt);

	cpSpaceAddShape(physicsWorld->world, avt->shape[0]);

	cpShapeFilter filter = cpShapeFilterNew(CP_NO_GROUP, avt->ori_layer, avt->ori_mask);

	cpShapeSetFilter(avt->shape[0], filter);

	cpShapeSetElasticity(avt->shape[0], 0.0f);
	cpShapeSetFriction(avt->shape[0], 2.0f);


	wf_free(poly);

	if (set_sprite_verts && avt->sprite)
		sprite_setvertices(avt->sprite[0], verts, count_verts, render_mngr);
}

void entity_freezesprite(entity_t* const avt)
{
	avt->freeze_pos = cpBodyGetPosition(avt->body);
	avt->sprite_freezed = wtrue;
}

void entity_unfreezesprite(entity_t* const avt)
{
	avt->sprite_freezed = wfalse;
}

cpVect entity_getposition(const entity_t* const oentity)
{
	if (oentity->sprite_freezed)
		return oentity->freeze_pos;
	else if (oentity->body)
		return cpBodyGetPosition(oentity->body);
	else
		return cpv(oentity->x, oentity->y);
}


wbool entity_checkrender(const entity_t* const oentity, const Rect_t screen_info)
{
	wbool visible = wtrue;

	if (oentity->sprite)
		visible = col_check2(&screen_info, &oentity->sprite[0]->render_box);
	else if (oentity->mesh_obj)
		visible = col_check2(&screen_info, &oentity->mesh_obj->render_box);

	return visible;
}

void entity_setshaderparam_intvalue(entity_t* const oentity, const char* param_name, int32_t param_value)
{
	if (oentity->shader_parameters == NULL || oentity->num_parameters == 0)
		return;

	for (uint32_t i_param = 0; i_param < oentity->num_parameters; i_param++)
	{
		if (strcmp(oentity->shader_parameters[i_param]->param_name, param_name) == 0)
		{
			if (oentity->shader_parameters[i_param]->uniform_int != NULL)
				oentity->shader_parameters[i_param]->uniform_int->v1 = param_value;
		}
	}
}

void entity_setshaderparam_floatvalue(entity_t* const oentity, const char* param_name, float param_values[4])
{
	if (oentity->shader_parameters == NULL || oentity->num_parameters == 0)
		return;

	for (uint32_t i_param = 0; i_param < oentity->num_parameters; i_param++)
	{
		if (strcmp(oentity->shader_parameters[i_param]->param_name, param_name) == 0)
		{
			if (oentity->shader_parameters[i_param]->uniform_float != NULL)
			{
				oentity->shader_parameters[i_param]->uniform_float->v1 = param_values[0];

				if (oentity->shader_parameters[i_param]->uniform_float->has_v2)
					oentity->shader_parameters[i_param]->uniform_float->v2 = param_values[1];

				if (oentity->shader_parameters[i_param]->uniform_float->has_v3)
					oentity->shader_parameters[i_param]->uniform_float->v3 = param_values[2];

				if (oentity->shader_parameters[i_param]->uniform_float->has_v4)
					oentity->shader_parameters[i_param]->uniform_float->v4 = param_values[3];
			}

			break;
		}
	}
}

void entity_setshaderparam_uniformarrayfloat(entity_t* const oentity, const char* param_name, const float* const array_values,size_t array_size)
{
	if (oentity->shader_parameters == NULL || oentity->num_parameters == 0)
		return;

	for (uint32_t i_param = 0; i_param < oentity->num_parameters; i_param++)
	{
		if (strcmp(oentity->shader_parameters[i_param]->param_name, param_name) == 0 && oentity->shader_parameters[i_param]->param_type == TYPE_PARAM_SHADER__uniform_array)
		{
			if (oentity->shader_parameters[i_param]->attribute_array == NULL)
			{
				ShaderArray* sub_array_param = (ShaderArray*)wf_malloc(sizeof(ShaderArray));
				shader_array__init(sub_array_param);

				sub_array_param->array_id = (char*)wf_malloc(strlen(param_name) + 1);
				w_strcpy(sub_array_param->array_id, strlen(param_name) + 1, param_name);

				sub_array_param->array_content = (float*)wf_malloc(sizeof(float) * array_size);

				w_memcpy(sub_array_param->array_content, array_size * sizeof(float), array_values, array_size * sizeof(float));
				sub_array_param->n_array_content = array_size;
				oentity->shader_parameters[i_param]->attribute_array = sub_array_param;
			}
			else {
				if (array_size != oentity->shader_parameters[i_param]->attribute_array->n_array_content) {
					logprint("can't resize an uniform array after initial allocation ! .. size provided %d size allocated %d", array_size, oentity->shader_parameters[i_param]->attribute_array->n_array_content);
					break;
				}

				w_memcpy(oentity->shader_parameters[i_param]->attribute_array->array_content, array_size * sizeof(float), array_values, array_size * sizeof(float));
			}

			break;
		}
	}

}




void entity_draw(entity_t* oentity, render_manager_t* const render_mngr, const Vector_t* const offset, void(*ondraw_func)(entity_t* const currententity))
{

	if (ondraw_func != NULL)
		ondraw_func(oentity);

	if (oentity->sprite_freezed)
	{
		cpVect pos_f = oentity->freeze_pos;

		pos_f.x += oentity->offsetx;
		pos_f.y += oentity->offsety;

		if (offset)
		{
			pos_f.x += offset->x;
			pos_f.y += offset->y;
		}

		if (oentity->sprite)
		{
			for (uint8_t ispr = 0; ispr < oentity->sprite_count; ispr++)
			{
				sprite_draw_withzindex(oentity->sprite[ispr], (wfloat)pos_f.x, (wfloat)pos_f.y, oentity->z_order, render_mngr, NULL);
			}

		}
		else if (oentity->mesh_obj)
		{
			Vector3_t mesh_pos = { (wfloat)pos_f.x,(wfloat)pos_f.y,oentity->mesh_obj->position.z };
			mesh_setposition(oentity->mesh_obj, mesh_pos);
			mesh_draw(oentity->mesh_obj, render_mngr);
		}
	}
	else
	{
		cpVect pos = entity_getposition(oentity);

		if (oentity->static_ent)
		{
			pos.x = ((-render_mngr->scroll_vector.x) + pos.x) - (oentity->parallax.x * -render_mngr->scroll_vector.x);
			pos.y = (pos.y + (-render_mngr->scroll_vector.y)) - (oentity->parallax.y * -render_mngr->scroll_vector.y);
		}

		pos.x += oentity->offsetx;
		pos.y += oentity->offsety;

		if (offset)
		{
			pos.x += offset->x;
			pos.y += offset->y;
		}

		if (oentity->sprite)
		{
			for (uint8_t ispr = 0; ispr < oentity->sprite_count; ispr++)
			{
				render_setuniform_int(render_mngr, "sprite_num", ispr);
				sprite_draw_withzindex(oentity->sprite[ispr], (wfloat)pos.x, (wfloat)pos.y, oentity->z_order + ispr, render_mngr, NULL);
			}
		}
		else if (oentity->mesh_obj)
		{
			Vector3_t mesh_pos = { (wfloat)pos.x,(wfloat)pos.y,oentity->mesh_obj->position.z };
			mesh_setposition(oentity->mesh_obj, mesh_pos);
			mesh_draw(oentity->mesh_obj, render_mngr);
		}
	}

	if (oentity->num_pass > 0)
		oentity->current_pass++;

	if (oentity->current_pass > oentity->num_pass)
		oentity->current_pass = 0;
}

void entity_lockwhenanim(entity_t* const oentity, const char* anim, uint8_t sprite_part)
{
	if (oentity->lockedanim == NULL) {
		logprint("Can't lock to an animation on a non animated sprite...");
		return;
	}

	if (sprite_part >= oentity->sprite_count) {
		logprint("Can't lock to a non existant sprite part...");
		return;
	}

	w_strcpy(oentity->lockedanim, oentity->lockedanimsize, anim);
	oentity->lockedspritepart = sprite_part;
}

void entity_resetanimlock(entity_t* const oentity)
{
	if (oentity->lockedanim == NULL) {
		logprint("Can't reset anim lock on a non animated sprite...");
		return;
	}

	memset(oentity->lockedanim, 0, oentity->lockedanimsize);
	oentity->lockedspritepart = 0;
}

void entity_playanimation3(entity_t* const oentity, const char* anim, uint8_t sprite_part, wbool reset_flicker, wbool reverse)
{
	if (oentity->sprite)
	{
		uint8_t ispr = sprite_part;

		if (ispr >= oentity->sprite_count)
		{
			ispr = oentity->sprite_count - 1;
		}

		//if the locked animation is not finished, don't start any new animation
		if (oentity->lockedanim != NULL && oentity->sprite[oentity->lockedspritepart]->current_anim != NULL && strcmp(oentity->lockedanim, oentity->sprite[oentity->lockedspritepart]->current_anim->anim_def->name) == 0 && !oentity->sprite[oentity->lockedspritepart]->current_anim->ended) {
			return;
		}

		if (oentity->sprite[ispr]->animated) {
			sprite_playanimation3(oentity->sprite[ispr], anim, reset_flicker,reverse);
		}
	}
}

void entity_playanimation2(entity_t* const oentity, const char* anim, uint8_t sprite_part, wbool reset_flicker)
{
	entity_playanimation3(oentity, anim, sprite_part, reset_flicker, wfalse);

}

void entity_playanimation(entity_t* const oentity, const char* anim, uint8_t sprite_part)
{
	entity_playanimation2(oentity, anim, sprite_part, wtrue);
}

void entity_stopanimation(entity_t* const oentity, uint8_t sprite_part)
{
	if (oentity->sprite)
	{
		uint8_t ispr = sprite_part;

		if (ispr >= oentity->sprite_count)
		{
			ispr = oentity->sprite_count - 1;
		}

		if (oentity->sprite[ispr]->animated)
			sprite_stopanimation(oentity->sprite[ispr]);
	}
}

sprite_t* entity_getsprite(entity_t* const oentity, const uint8_t ispr)
{
	if (oentity->sprite_count > ispr) {
		return oentity->sprite[ispr];
	}
	else {
		return NULL;
	}
}

uint8_t entity_getnumsprite(entity_t* const oentity)
{
	return oentity->sprite_count;
}

void entity_updaterenderbox(entity_t* oentity, render_manager_t* const render_mngr, wbool no_phy_update)
{
	if (oentity->body)
	{
		cpVect pos = entity_getposition(oentity);

		if (oentity->sprite)
		{
			for (uint8_t ispr = 0; ispr < oentity->sprite_count; ispr++)
			{
				oentity->sprite[ispr]->render_box.position.x = (float)((pos.x - (oentity->sprite[ispr]->render_box.width * 0.5f)) + oentity->offsetx);
				oentity->sprite[ispr]->render_box.position.y = (float)((pos.y + (oentity->sprite[ispr]->render_box.height * 0.5f)) + oentity->offsety);
			}

		}
		else
		{
			oentity->mesh_obj->render_box.position.x = (float)((pos.x - (oentity->mesh_obj->render_box.width * 0.5f)) + oentity->offsetx);
			oentity->mesh_obj->render_box.position.y = (float)((pos.y + (oentity->mesh_obj->render_box.height * 0.5f)) + oentity->offsety);
		}

	}
	else
	{
		cpVect pos = entity_getposition(oentity);
		pos.x = ((-render_mngr->scroll_vector.x) + pos.x) - (oentity->parallax.x * -render_mngr->scroll_vector.x);
		pos.y = (pos.y + (-render_mngr->scroll_vector.y)) - (oentity->parallax.y * -render_mngr->scroll_vector.y);

		if (oentity->sprite)
		{
			for (uint8_t ispr = 0; ispr < oentity->sprite_count; ispr++)
			{
				oentity->sprite[ispr]->render_box.position.x = (float)((pos.x - (oentity->sprite[ispr]->render_box.width * 0.5f)) + oentity->offsetx);
				oentity->sprite[ispr]->render_box.position.y = (float)((pos.y + (oentity->sprite[ispr]->render_box.height * 0.5f)) +oentity->offsety);
			}
		}
		else if (oentity->mesh_obj)
		{
			oentity->mesh_obj->render_box.position.x = (float)((pos.x - (oentity->mesh_obj->render_box.width * 0.5f)) + oentity->offsetx);
			oentity->mesh_obj->render_box.position.y = (float)((pos.y + (oentity->mesh_obj->render_box.height * 0.5f)) + oentity->offsety);
		}
	}
}

void entity_update(entity_t* oentity, float elapsed, render_manager_t* const render_mngr, void(*onupdate_func)(entity_t* const currententity))
{
	wbool updatesprite = wfalse;
	wbool rot1 = wfalse;
	wbool rot2 = wfalse;
	wfloat rot1val = WFLOAT0;
	wfloat rot2val = WFLOAT0;

	//update sprite
	if (oentity->sprite) {
		updatesprite = wtrue;
	}
	else if (oentity->mesh_obj)
		mesh_update(oentity->mesh_obj, elapsed);

	if (oentity->body && oentity->set_sprite_rotation && oentity->sprite)
	{
		rot1 = wtrue;
		rot1val = rad2deg((float)cpBodyGetAngle(oentity->body));
	}

	entity_updaterenderbox(oentity, render_mngr, wfalse);

	//do rotation if needed
	if (oentity->dorotation)
	{
		oentity->rotation_amount += oentity->rotation_step / elapsed;
		//oentity->rotation_step;
		if (oentity->controlBody)
		{
			cpVect bodypos;
			bodypos.x = oentity->rotation_radius * cosf(oentity->rotation_amount) + oentity->rotation_point.x;
			bodypos.y = oentity->rotation_radius * sinf(oentity->rotation_amount) + oentity->rotation_point.y;
			cpBodySetPosition(oentity->controlBody, bodypos);
		}
		else if (oentity->body)
		{
			cpVect bodypos;
			bodypos.x = oentity->rotation_radius * cosf(oentity->rotation_amount) + oentity->rotation_point.x;
			bodypos.y = oentity->rotation_radius * sinf(oentity->rotation_amount) + oentity->rotation_point.y;
			cpBodySetPosition(oentity->body, bodypos);
		}

		if (oentity->controlBody)
			cpBodySetAngle(oentity->controlBody, oentity->rotation_amount);
		else if (oentity->body)
		{
			cpBodySetAngle(oentity->body, oentity->rotation_amount);

			cpShapeCacheBB(oentity->shape[0]);

			if (oentity->shape_num > 1)
			{
				for (int32_t ishape = 1; ishape < oentity->shape_num; ishape++)
					cpShapeCacheBB(oentity->shape[ishape]);
			}
		}
		else if (oentity->set_sprite_rotation && oentity->sprite)
		{
			rot2 = wtrue;
			rot2val = rad2deg(oentity->rotation_amount);
		}


		if ((oentity->rotation_step > 0 && oentity->rotation_amount >= (M_PI * 2)) || (oentity->rotation_step < 0 && oentity->rotation_amount <= -(M_PI * 2)))//check against max radian value
			oentity->rotation_amount = 0.0f;
	}

	for (uint8_t ispr = 0; ispr < oentity->sprite_count; ispr++)
	{
		if (updatesprite)
			sprite_update(oentity->sprite[ispr], elapsed);

		if (rot1)
			sprite_setrotation(oentity->sprite[ispr], rot1val);

		if (rot2)
			sprite_setrotation(oentity->sprite[ispr], rot2val);
	}

	entity_updatecontactbodies(oentity);

	if (onupdate_func != NULL) {
		onupdate_func(oentity);
	}
}


void entity_startrotation(entity_t* const oentity, cpVect base_point, float rot_radius, int direction, float start_rotation, float speed)
{
	oentity->dorotation = wtrue;
	oentity->rotation_point = base_point;
	oentity->rotation_step = (float)(((M_PI * 2) * direction) * speed);
	oentity->rotation_radius = rot_radius;
	oentity->rotation_amount = (float)deg2rad(start_rotation);
	oentity->start_rotation = oentity->rotation_amount;

	if (oentity->rotation_amount != 0.0f)
	{
		if (oentity->controlBody)
			cpBodySetAngle(oentity->controlBody, oentity->rotation_amount);

		if (oentity->body)
			cpBodySetAngle(oentity->body, oentity->rotation_amount);

		if (oentity->set_sprite_rotation && oentity->sprite)
		{
			for (uint8_t ispr = 0; ispr < oentity->sprite_count; ispr++)
			{
				sprite_setrotation(oentity->sprite[ispr], rad2deg(oentity->rotation_amount));
			}

		}

	}
}

void entity_endrotation(entity_t* const oentity)
{
	oentity->dorotation = wfalse;

	if (oentity->controlBody)
		cpBodySetAngle(oentity->controlBody, 0.0f);

	if (oentity->body)
		cpBodySetAngle(oentity->body, 0.0f);

	oentity->rotation_amount = 0.0f;

	if (oentity->set_sprite_rotation && oentity->sprite)
	{
		for (uint8_t ispr = 0; ispr < oentity->sprite_count; ispr++)
		{
			sprite_setrotation(oentity->sprite[ispr], 0.0f);
		}
	}


	oentity->start_rotation = 0.0f;
}

float entity_getrotation(entity_t* const oentity)
{
	if (oentity->controlBody)
		return rad2deg((float)cpBodyGetAngle(oentity->controlBody));

	if (oentity->body)
		return rad2deg((float)cpBodyGetAngle(oentity->body));


	if (oentity->set_sprite_rotation && oentity->sprite)
		return sprite_getrotation(oentity->sprite[0]);

	return 0.0f;
}


void entity_setrotation(entity_t* const oentity, float rotation)
{
	if (oentity->dorotation)
	{
		logprint("Can't set rotation manually while automatic rotation is on! use endrotation before");
		return;
	}

	if (oentity->controlBody)
		cpBodySetAngle(oentity->controlBody, deg2rad(rotation));

	if (oentity->body)
	{
		cpBodySetAngle(oentity->body, deg2rad(rotation));
		cpShapeCacheBB(oentity->shape[0]);

		if (oentity->shape_num > 1)
		{
			for (int32_t ishape = 1; ishape < oentity->shape_num; ishape++)
				cpShapeCacheBB(oentity->shape[ishape]);
		}
	}


	if (oentity->set_sprite_rotation && oentity->sprite)
	{
		for (uint8_t ispr = 0; ispr < oentity->sprite_count; ispr++)
		{
			sprite_setrotation(oentity->sprite[ispr], rotation);
		}
	}

}

void entity_settexture(entity_t* const oentity, texture_info* new_texture)
{
	oentity->current_texture = new_texture;

	if (oentity->sprite)
	{
		for (uint8_t ispr = 0; ispr < oentity->sprite_count; ispr++)
		{
			sprite_settexture(oentity->sprite[ispr], new_texture);
		}
	}

}

void entity_resetrotation(entity_t* const oentity)
{
	if (oentity->controlBody)
		cpBodySetAngle(oentity->controlBody, 0.0f);

	if (oentity->body)
		cpBodySetAngle(oentity->body, 0.0f);

	oentity->rotation_amount = oentity->start_rotation;

	if (oentity->set_sprite_rotation && oentity->sprite)
	{
		for (uint8_t ispr = 0; ispr < oentity->sprite_count; ispr++)
		{
			sprite_setrotation(oentity->sprite[ispr], 0.0f);
		}
	}

}

void entity_setposition(entity_t* oentity, wfloat posX, wfloat posY)
{
	if (oentity->controlBody)
		cpBodySetPosition(oentity->controlBody, cpv(posX, posY));

	if (oentity->body)
		cpBodySetPosition(oentity->body, cpv(posX, posY));
	else
	{
		oentity->x = posX;
		oentity->y = posY;
	}

}

void entity_setextraparams(entity_t* const oentity, uint32_t extrap1, uint32_t extrap2)
{
	oentity->extraparam1 = extrap1;
	oentity->extraparam2 = extrap2;
}

void entity_getextraparams(const entity_t* const oentity, uint32_t* outextrap1, uint32_t* outextrap2)
{
	(*outextrap1) = oentity->extraparam1;
	(*outextrap2) = oentity->extraparam2;
}

void entity_setextraptr(entity_t* const oentity, intptr_t extraptr)
{
	oentity->extraptr1 = extraptr;
}

intptr_t entity_getextraptr(entity_t* const oentity)
{
	return oentity->extraptr1;
}

void entity_setanchorpoint(entity_t* oentity, wfloat X, wfloat Y)
{
	if (oentity->move_constraint)
	{
		cpPivotJointSetAnchorB(oentity->move_constraint, cpBodyWorldToLocal(oentity->body, cpv(X, Y)));
	}
}


void entity_movetopos(entity_t* oentity, wfloat posX, wfloat posY)
{
	if (oentity->constraint_x)
		posX = (wfloat)oentity->start_position.x;

	if (oentity->constraint_y)
		posY = (wfloat)oentity->start_position.y;

	if (oentity->controlBody)
	{
		cpVect basepoint = cpv(posX, posY);
		cpVect velocity = cpvmult(cpvsub(basepoint, cpBodyGetPosition(oentity->body)), cpBodyGetMass(oentity->body));

		cpBodySetVelocity(oentity->controlBody, velocity);

		cpBodySetPosition(oentity->controlBody, basepoint);

		if (oentity->sprite)
		{
			for (uint8_t ispr = 0; ispr < oentity->sprite_count; ispr++)
			{
				oentity->sprite[ispr]->render_box.position.x = (wfloat)((basepoint.x - (oentity->sprite[0]->render_box.width * 0.5f)) + oentity->offsetx);
				oentity->sprite[ispr]->render_box.position.y = (wfloat)((basepoint.y + (oentity->sprite[0]->render_box.height * 0.5f)) + oentity->offsety);
			}
		}
		else if (oentity->mesh_obj)
		{
			oentity->mesh_obj->render_box.position.x = (wfloat)((basepoint.x - (oentity->sprite[0]->render_box.width * 0.5f)) + oentity->offsetx);
			oentity->mesh_obj->render_box.position.y = (wfloat)((basepoint.y + (oentity->sprite[0]->render_box.height * 0.5f)) + oentity->offsety);
		}
	}
	else
		entity_setposition(oentity, posX, posY);
}

void entity_set_constraint(entity_t* const oentity, physics_manager_t* const physicsWorld, wbool activate)
{
	if (!oentity->move_constraint)
	{
		logprint("No Move Contraint !");
		return;
	}

	//in all case, set the anchor point back at it original values
	if (oentity->move_constraint)
	{
		cpPivotJointSetAnchorA(oentity->move_constraint, cpvzero);
		cpPivotJointSetAnchorB(oentity->move_constraint, cpvzero);
	}

	if (!oentity->move_constraint_activated && activate)
	{
		cpSpaceAddConstraint(physicsWorld->world, oentity->move_constraint);
		oentity->move_constraint_activated = wtrue;
	}
	else if (oentity->move_constraint_activated && !activate)
	{
		cpSpaceRemoveConstraint(physicsWorld->world, oentity->move_constraint);
		oentity->move_constraint_activated = wfalse;
	}

}

void entity_linkto(entity_t* const oentity, entity_t* const link_to_entity, physics_manager_t* const physicsWorld, wbool activate, float mindistance, float maxdistance, Vector_t offset_anchr1, Vector_t offset_anchr2)
{
	cpVect offset = cpv(offset_anchr1.x, offset_anchr1.y);
	cpVect offset2 = cpv(offset_anchr2.x, offset_anchr2.y);

	if (oentity->link_constraint == NULL) //create constraint
	{

		oentity->link_constraint = cpSpaceAddConstraint(physicsWorld->world, cpSlideJointNew(oentity->body, link_to_entity->body, offset, offset2, mindistance, maxdistance));
		oentity->link_constraint_activated = wtrue;
		w_strcpy(&oentity->linked_to_id[0], 256, link_to_entity->entity_id);

	}
	else
	{
		if (link_to_entity != NULL)
		{
			//if the link already exist, but the name is different, recreate the link
			if (strcmp(oentity->linked_to_id, link_to_entity->entity_id) != 0)
			{
				if (oentity->link_constraint_activated)
					cpSpaceRemoveConstraint(physicsWorld->world, oentity->link_constraint);

				cpConstraintFree(oentity->link_constraint);


				oentity->link_constraint = cpSpaceAddConstraint(physicsWorld->world, cpSlideJointNew(oentity->body, link_to_entity->body, offset, offset2, mindistance, maxdistance));
				oentity->link_constraint_activated = wtrue;
				w_strcpy(&oentity->linked_to_id[0], 256, link_to_entity->entity_id);
			}
		}


		if (!oentity->link_constraint_activated && activate)
		{
			cpSpaceAddConstraint(physicsWorld->world, oentity->link_constraint);
			oentity->link_constraint_activated = wtrue;
		}
		else if (oentity->link_constraint_activated && !activate)
		{
			cpSpaceRemoveConstraint(physicsWorld->world, oentity->link_constraint);
			oentity->link_constraint_activated = wfalse;
		}

		if (oentity->link_constraint_activated  && activate)
		{
			if (mindistance != -1)
				cpSlideJointSetMin(oentity->link_constraint, mindistance);

			if (maxdistance != -1)
				cpSlideJointSetMax(oentity->link_constraint, maxdistance);

			cpSlideJointSetAnchorA(oentity->link_constraint, offset);
			cpSlideJointSetAnchorB(oentity->link_constraint, offset2);
		}
	}

}

wbool entity_haspin(entity_t* const oentity)
{
	return (wbool)(oentity->pin_constraint != NULL);
}

cpConstraint* entity_getpin(entity_t* const oentity)
{
	return oentity->pin_constraint;
}

//pin the entity in a specific place in world coordinate, always pinned to the world static body
cpConstraint* entity_pintoworld(entity_t* const oentity, physics_manager_t* const physicsWorld, cpVect offset_anchr1, cpVect offset_anchr2, wbool addtoworld)
{

	if (!oentity->in_physics_simulation) {
		logprint("the entity %s is not in physics simulation, can't add a pin constraint...", oentity->entity_id);
		return NULL;
	}

	if (oentity->pin_constraint == NULL) //create constraint
	{
		cpBody* staticbody = cpSpaceGetStaticBody(physicsWorld->world);
		oentity->pin_constraint = cpPinJointNew(staticbody, oentity->body, offset_anchr1, offset_anchr2);

		if (addtoworld) {
			cpSpaceAddConstraint(physicsWorld->world, oentity->pin_constraint);
		}
	}
	else {
		logprint("A pin constraint already exist for the entity %s please remove it first !", oentity->entity_id);
	}

	return oentity->pin_constraint;
}


void entity_removepin(entity_t* const oentity, physics_manager_t* const physicsWorld)
{
	if (oentity->pin_constraint != NULL) {
		cpSpaceRemoveConstraint(physicsWorld->world, oentity->pin_constraint);
		cpConstraintFree(oentity->pin_constraint);
		oentity->pin_constraint = NULL;
	}
}

void entity_addcallback(entity_t* const oentity, const char* func_id, const char* script_func, void(*callback_func)(const char* id, const char* func, uint32_t parent, const char* params, void* return_value, size_t return_size), uint32_t parent)
{
	if (oentity->callback_hash == NULL)
	{
		logprint("Can't add callback to an entity without has_callback properties set to true!");
		return;
	}

	size_t len = strlen(func_id) + 1;
	char* ptr = (char*)wf_malloc(sizeof(char) * len);
	w_strcpy(ptr, len, func_id);
	int pos = hashtable_pushkey(oentity->callback_hash, ptr);

	size_t script_len = (strlen(script_func) + 1);

	char* ptr2 = (char*)wf_malloc(sizeof(char) * script_len);
	w_strcpy(ptr2, script_len, script_func);

	oentity->callback_scriptfuncs[pos] = ptr2;
	oentity->callback_func = callback_func;

	if (parent != 0)
		oentity->entity_parent = parent;
}

void entity_removecallback(entity_t* const oentity, const char* func_id)
{
	if (oentity->callback_hash == NULL)
	{
		logprint("Can't remove callback from an entity without has_callback properties set to true!");
		return;
	}

	int indx = hashtable_index(oentity->callback_hash, func_id);

	wf_free(oentity->callback_scriptfuncs[indx]);
	oentity->callback_scriptfuncs[indx] = NULL;

	hashtable_removekey(oentity->callback_hash, func_id);

	wf_free(oentity->callback_keys[indx]);
	oentity->callback_keys[indx] = NULL;


}

void entity_clearcallback(entity_t* const oentity)
{
	if (oentity->callback_hash == NULL)
	{
		logprint("Can't remove callback from an entity without has_callback properties set to true!");
		return;
	}

	for (int callback_indx = 0; callback_indx < ENTITY_MAX_CALLBACK; callback_indx++)
	{
		if (oentity->callback_keys[callback_indx] != NULL)
		{
			wf_free(oentity->callback_scriptfuncs[callback_indx]);
			oentity->callback_scriptfuncs[callback_indx] = NULL;

			wf_free(oentity->callback_keys[callback_indx]);
			oentity->callback_keys[callback_indx] = NULL;

		}
	}

	oentity->callback_hash->num_elem = 0;


}


void entity_callback(entity_t* const oentity, const char* callback, const char* params)
{
	if (oentity->callback_func != NULL && hashtable_haskey(oentity->callback_hash, callback))
		oentity->callback_func(oentity->entity_id, oentity->callback_scriptfuncs[hashtable_index(oentity->callback_hash, callback)], oentity->entity_parent, params, NULL, 0);
}

void entity_callback2(entity_t* const oentity, const char* callback, const char* params, void* return_value, size_t return_size)
{
	if (oentity->callback_func != NULL && hashtable_haskey(oentity->callback_hash, callback))
		oentity->callback_func(oentity->entity_id, oentity->callback_scriptfuncs[hashtable_index(oentity->callback_hash, callback)], oentity->entity_parent, params, return_value, return_size);
}

void entity_removefromspace(physics_manager_t* const physicsWorld, entity_t* const oentity)
{
	for (int32_t ishape = 0; ishape < oentity->shape_num; ishape++)
		cpSpaceRemoveShape(physicsWorld->world, oentity->shape[ishape]);

	cpSpaceRemoveBody(physicsWorld->world, oentity->body);
	oentity->in_physics_simulation = wfalse;

	if (oentity->contact_shape_list != NULL)
	{
		for (uint32_t c_s = 0; c_s < oentity->contact_shape_num; c_s++)
		{
			if (oentity->contact_shape_list[c_s])
			{
				cpBody* tp_bod = cpShapeGetBody(oentity->contact_shape_list[c_s]);
				cpSpaceRemoveShape(physicsWorld->world, oentity->contact_shape_list[c_s]);
				cpSpaceRemoveBody(physicsWorld->world, tp_bod);
			}
		}
	}
}

void entity_addtospace(physics_manager_t* const physicsWorld, entity_t* const oentity)
{
	for (int32_t ishape = 0; ishape < oentity->shape_num; ishape++)
		cpSpaceAddShape(physicsWorld->world, oentity->shape[ishape]);

	cpSpaceAddBody(physicsWorld->world, oentity->body);
	oentity->in_physics_simulation = wtrue;

	if (oentity->contact_shape_list != NULL)
	{
		for (uint32_t c_s = 0; c_s < oentity->contact_shape_num; c_s++)
		{
			if (oentity->contact_shape_list[c_s])
			{
				cpBody* tp_bod = cpShapeGetBody(oentity->contact_shape_list[c_s]);
				cpSpaceAddShape(physicsWorld->world, oentity->contact_shape_list[c_s]);
				cpSpaceAddBody(physicsWorld->world, tp_bod);
			}
		}
	}
}

void entity_freelinkconstraint(physics_manager_t* const physicsWorld, entity_t* oentity)
{
	if (oentity->link_constraint)
	{
		if (oentity->link_constraint_activated)
			cpSpaceRemoveConstraint(physicsWorld->world, oentity->link_constraint);

		cpConstraintFree(oentity->link_constraint);
		oentity->link_constraint = NULL;

		oentity->link_constraint_activated = wfalse;
	}
}



static void no_gravity(cpBody *body, cpVect gravity, cpFloat damping, cpFloat dt)
{
	cpBodyUpdateVelocity(body, cpvzero, 0.0f, dt);
}


int32_t entity_addcontactbody(entity_t* const oentity, int16_t width, int16_t height, const char* contact_name, physics_manager_t* const physicsWorld, cpCollisionType col_type, cpBitmask col_layer, cpBitmask mask)
{
	if (oentity->static_ent)
	{
		logprint("Can't add contact body on static entity!");
		return -1;
	}

	if (oentity->in_group)
	{
		logprint("Can't add contact body on grouped entity!");
		return -1;
	}

	if (!oentity->sprite)
	{
		logprint("Can't add contact body on an entity without a sprite!");
		return -1;
	}

	if (!oentity->sprite[0]->animated)
	{
		logprint("Can't add contact body with non animated sprite!");
		return -1;
	}

	if (oentity->sprite[0]->current_anim == NULL)
	{
		logprint("play an animation before adding a contact body!");
		return -1;
	}


	int32_t return_value = animation_getcontactpointindx(oentity->sprite[0]->current_anim, contact_name);

	if (return_value == -1)
	{
		logprint("a contact point named %s doesn't exist!", contact_name);
		return return_value;
	}

	if (oentity->contact_shape_list == NULL)
	{
		oentity->contact_shape_list = (cpShape**)wf_calloc(oentity->sprite[0]->current_anim->anim_def->n_attachpoint_id, sizeof(cpShape*));
		oentity->contact_shape_num = oentity->sprite[0]->current_anim->anim_def->n_attachpoint_id;
	}

	cpBody* contact_body = cpSpaceAddBody(physicsWorld->world, cpBodyNewKinematic());
	cpBodySetVelocityUpdateFunc(contact_body, no_gravity);

	contact_point_ptr* contact_indx = (contact_point_ptr*)wf_malloc(sizeof(contact_point_ptr));
	contact_indx->contact_indx = return_value;
	contact_indx->ref_entity = oentity;
	cpBodySetUserData(contact_body, contact_indx);

	oentity->contact_shape_list[return_value] = cpBoxShapeNew(contact_body, width, height, 0.0f);

	cpShapeFilter filter = cpShapeFilterNew(CP_NO_GROUP, col_layer, mask);//LAYER_INACTIVE 0

	cpShapeSetFilter(oentity->contact_shape_list[return_value], filter);

	cpShapeSetCollisionType(oentity->contact_shape_list[return_value], col_type);

	cpVect pos = entity_getposition(oentity);

	cpBodySetPosition(contact_body, pos);

	cpSpaceAddShape(physicsWorld->world, oentity->contact_shape_list[return_value]);

	return return_value;
}

void entity_updatecontactbodies(entity_t* const ent)
{
	if (ent->contact_shape_list != NULL && ent->sprite && ent->sprite[0]->current_anim != NULL)
	{
		for (uint32_t ics = 0; ics < ent->contact_shape_num; ics++)
		{
			if (ent->contact_shape_list[ics] != NULL)
			{
				cpBody* c_bod = cpShapeGetBody(ent->contact_shape_list[ics]);
				contact_point_ptr* c_indx = (contact_point_ptr*)cpBodyGetUserData(c_bod);

				if (animation_iscontactactive_atframe(ent->sprite[0]->current_anim, c_indx->contact_indx))
				{
					cpShapeFilter filter = cpShapeGetFilter(ent->contact_shape_list[ics]);

					if (filter.mask == LAYER_INACTIVE)
					{
						filter.categories = filter.mask = LAYER_FORCE;
						cpShapeSetFilter(ent->contact_shape_list[ics], filter);
					}

					Vector_t pos = animation_getcontactpos_atframe(ent->sprite[0]->current_anim, c_indx->contact_indx);


					pos.x *= ent->sprite[0]->scale_x;
					pos.y *= ent->sprite[0]->scale_y;

					//rotate if needed

					if (sprite_getrotation(ent->sprite[0]) != 0.0f)
					{
						Vector_t pos_center = { (ent->sprite[0]->texture_width * ent->sprite[0]->scale_x) * 0.5f,(ent->sprite[0]->texture_height * ent->sprite[0]->scale_y) * 0.5f };

						float rad_angle = atan2f(pos.y - pos_center.y, pos.x - pos_center.x);

						float radius = Vec_dist(pos_center, pos);

						float rad = deg2rad(sprite_getrotation(ent->sprite[0]));

						float final_rot = rad + rad_angle;

						if (final_rot >= M_PI * 2)
							final_rot = ((float)M_PI * 2.0f) - final_rot;
						else if (final_rot <= 0)
							final_rot = final_rot + ((float)M_PI * 2.0f);

						pos.x = radius * cosf(final_rot) + pos_center.x;
						pos.y = radius * sinf(final_rot) + pos_center.y;
					}

					if (ent->sprite[0]->mirror_texture_x)
						pos.x = (ent->sprite[0]->width * ent->sprite[0]->scale_x) - pos.x;

					if (ent->sprite[0]->mirror_texture_y)
						pos.y = (ent->sprite[0]->height * ent->sprite[0]->scale_y) - pos.y;


					cpVect ent_pos = entity_getposition(ent);

					ent_pos.x += ent->offsetx + ent->sprite[0]->current_anim->anim_def->sprite_offsetx;
					ent_pos.y += ent->offsety + ent->sprite[0]->current_anim->anim_def->sprite_offsety;


					cpVect fpos = cpv((ent_pos.x + pos.x) - ((ent->sprite[0]->width * ent->sprite[0]->scale_x) * 0.5f), (ent_pos.y + pos.y) - ((ent->sprite[0]->height * ent->sprite[0]->scale_y) * 0.5f));


					cpBodySetPosition(c_bod, fpos);
				}
				else //disable contact point for now
				{
					cpShapeFilter filter = cpShapeGetFilter(ent->contact_shape_list[ics]);

					if (filter.mask == LAYER_FORCE)
					{
						filter.categories = LAYER_INACTIVE;
						filter.mask = 0;
						cpShapeSetFilter(ent->contact_shape_list[ics], filter);
					}
				}
			}
		}
	}
}

void entity_removeallcontactbodies(entity_t* const oentity, physics_manager_t* const physicsWorld)
{
	if (oentity->contact_shape_list != NULL)
	{
		//clear existing contact points
		for (uint32_t c_s = 0; c_s < oentity->contact_shape_num; c_s++)
		{
			if (oentity->contact_shape_list[c_s])
			{
				cpBody* tp_bod = cpShapeGetBody(oentity->contact_shape_list[c_s]);
				cpSpaceRemoveShape(physicsWorld->world, oentity->contact_shape_list[c_s]);
				cpSpaceRemoveBody(physicsWorld->world, tp_bod);
			}
		}

		for (uint32_t c_s = 0; c_s < oentity->contact_shape_num; c_s++)
		{
			if (oentity->contact_shape_list[c_s])
			{
				cpBody* tp_bod = cpShapeGetBody(oentity->contact_shape_list[c_s]);
				contact_point_ptr* indx = (contact_point_ptr*)cpBodyGetUserData(tp_bod);
				wf_free(indx);
				cpShapeFree(oentity->contact_shape_list[c_s]);
				cpBodyFree(tp_bod);
			}
		}

		wf_free(oentity->contact_shape_list);
		oentity->contact_shape_num = 0;
		oentity->contact_shape_list = NULL;
	}
}

Rect_t* entity_getrenderbox(entity_t* const oentity)
{
	if (oentity->sprite)
		return &oentity->sprite[0]->render_box;
	else if (oentity->mesh_obj)
		return &oentity->mesh_obj->render_box;

	return NULL;
}

Rect_t entity_getphysicsbox(entity_t* const oentity)
{
	Rect_t rectangle = { 0 };

	if (oentity->in_physics_simulation)
	{
		if (oentity->shape)
		{
			
			cpBB bball = cpBBNew(0,0,0,0);

			wint minx = 0, miny = 0, maxx = 0, maxy = 0;

			for (int32_t ishape = 0; ishape < oentity->shape_num; ishape++)
			{
				cpBB shapebb = cpShapeGetBB(oentity->shape[ishape]);

				if (shapebb.l < minx || minx == 0)
					minx = shapebb.l;

				if (shapebb.r > maxx || maxx == 0)
					maxx = shapebb.r;

				if (shapebb.b < miny || miny == 0)
					miny = shapebb.b;
				if (shapebb.t > maxy || maxy == 0)
					maxy = shapebb.b;
			}

			rectangle.position = Vec(minx, miny);
			rectangle.width = maxx - minx;
			rectangle.height = maxy - miny;

			return rectangle;
		}
		else
		{
			logprint("no physics shape for entity %s, use entity_getrenderbox instead, returning empty box...", oentity->entity_id);
		}
	}
	else {
		logprint("entity %s not in physics simulation, use entity_getrenderbox instead, returning empty box...", oentity->entity_id);
	}

	return rectangle;
	
}

void entity_g_clean(entity_t* const oentity)
{
	if (oentity->sprite)
	{
		for (uint8_t ispr = 0; ispr < oentity->sprite_count; ispr++)
		{
			sprite_g_clean(oentity->sprite[ispr]);
		}
	}
	else if (oentity->mesh_obj)
		mesh_g_clean(oentity->mesh_obj);

	if (oentity->shader_parameters != NULL)
	{
		for (uint32_t i_param = 0; i_param < oentity->num_parameters; i_param++)
		{
			if (oentity->shader_parameters[i_param]->param_type == TYPE_PARAM_SHADER__attribute_array)
			{
				if (oentity->shader_parameters[i_param]->attribute_array->has_array_ref)
				{
					VBO_ID vbo;

					memcpy(&vbo, oentity->shader_parameters[i_param]->attribute_array->array_ref.data, sizeof(VBO_ID));

					render_delete_buffer(vbo);

					oentity->shader_parameters[i_param]->attribute_array->has_array_ref = (protobuf_c_boolean)0;

					wf_free(oentity->shader_parameters[i_param]->attribute_array->array_ref.data);
					oentity->shader_parameters[i_param]->attribute_array->array_ref.len = 0;

					oentity->shader_parameters[i_param]->attribute_array->array_ref.data = NULL;
				}
			}
		}
	}

	oentity->program_id = render_get_emptyprogvalue();
}

void entity_g_create(entity_t* const oentity, render_manager_t* const render_mngr)
{
	if (oentity->sprite)
	{
		for (uint8_t ispr = 0; ispr < oentity->sprite_count; ispr++)
		{
			sprite_g_create(oentity->sprite[ispr], render_mngr);
		}
	}
	else if (oentity->mesh_obj)
		mesh_g_create(oentity->mesh_obj, render_mngr);

	if (strlen(oentity->prog_name) != 0)
		oentity->program_id = render_get_program(render_mngr, oentity->prog_name);
}


void entity_free(physics_manager_t* const physicsWorld, entity_t* oentity)
{
	if (oentity->controlBody)
	{
		if (oentity->move_constraint_activated)
			cpSpaceRemoveConstraint(physicsWorld->world, oentity->move_constraint);

		cpConstraintFree(oentity->move_constraint);
		oentity->move_constraint = NULL;
		cpBodyFree(oentity->controlBody);
		oentity->controlBody = NULL;

		oentity->move_constraint_activated = wfalse;
	}

	if (oentity->link_constraint && oentity->link_constraint_activated)
	{
		logprint("Link constraint of entity %s not freed before body! risk of memory corruption", oentity->entity_id);
	}

	if (oentity->lockedanim != NULL)
		wf_free(oentity->lockedanim);

	oentity->lockedanim = NULL;

	if (oentity->in_physics_simulation)
	{
		//pin contraint are ALWAYS pinned to the world static body
		entity_removepin(oentity, physicsWorld);

		if (oentity->shape)
		{
			for (int32_t ishape = 0; ishape < oentity->shape_num; ishape++)
				cpSpaceRemoveShape(physicsWorld->world, oentity->shape[ishape]);
		}

		if (oentity->body)
			cpSpaceRemoveBody(physicsWorld->world, oentity->body);

		if (oentity->contact_shape_list != NULL)
		{
			for (uint32_t c_s = 0; c_s < oentity->contact_shape_num; c_s++)
			{
				if (oentity->contact_shape_list[c_s])
				{
					cpBody* tp_bod = cpShapeGetBody(oentity->contact_shape_list[c_s]);
					cpSpaceRemoveShape(physicsWorld->world, oentity->contact_shape_list[c_s]);
					cpSpaceRemoveBody(physicsWorld->world, tp_bod);
				}
			}
		}
	}

	if (oentity->contact_shape_list != NULL)
	{
		for (uint32_t c_s = 0; c_s < oentity->contact_shape_num; c_s++)
		{
			if (oentity->contact_shape_list[c_s])
			{
				cpBody* tp_bod = cpShapeGetBody(oentity->contact_shape_list[c_s]);
				int32_t* indx = (int32_t*)cpBodyGetUserData(tp_bod);
				wf_free(indx);
				cpShapeFree(oentity->contact_shape_list[c_s]);
				cpBodyFree(tp_bod);
			}
		}

		wf_free(oentity->contact_shape_list);
		oentity->contact_shape_num = 0;
		oentity->contact_shape_list = NULL;
	}


	if (oentity->shape)
	{
		for (int32_t ishape = 0; ishape < oentity->shape_num; ishape++)
			cpShapeFree(oentity->shape[ishape]);

		wf_free(oentity->shape);

		oentity->shape = NULL;

		oentity->shape_num = 0;
	}

	if (oentity->body)
	{
		cpBodyFree(oentity->body);
		oentity->body = NULL;
	}


	if (oentity->sprite != NULL)
	{
		for (uint8_t ispr = 0; ispr < oentity->sprite_count; ispr++) {
			sprite_free(oentity->sprite[ispr]);
			wf_free(oentity->sprite[ispr]);
		}

		wf_free(oentity->sprite);
		oentity->sprite = NULL;
	}
	else if (oentity->mesh_obj != NULL)
	{
		mesh_free(oentity->mesh_obj);
		wf_free(oentity->mesh_obj);
		oentity->mesh_obj = NULL;
	}


	if (oentity->callback_keys != NULL)
	{
		for (int callback_indx = 0; callback_indx < ENTITY_MAX_CALLBACK; callback_indx++)
		{
			if (oentity->callback_keys[callback_indx] != NULL)
			{
				wf_free(oentity->callback_scriptfuncs[callback_indx]);
				oentity->callback_scriptfuncs[callback_indx] = NULL;

				wf_free(oentity->callback_keys[callback_indx]);
				oentity->callback_keys[callback_indx] = NULL;
			}
		}

		wf_free(oentity->callback_keys);
		oentity->callback_hash->num_elem = 0;
	}

	if (oentity->callback_hash)
		wf_free(oentity->callback_hash);

	if (oentity->callback_scriptfuncs)
		wf_free(oentity->callback_scriptfuncs);


	oentity->callback_keys = NULL;
	oentity->callback_hash = NULL;
	oentity->callback_scriptfuncs = NULL;
	oentity->callback_func = NULL;

	if (oentity->shader_parameters != NULL)
	{
		for (uint32_t i_param = 0; i_param < oentity->num_parameters; i_param++)
		{
			if (oentity->shader_parameters[i_param]->param_type == TYPE_PARAM_SHADER__attribute_array)
			{
				if (oentity->shader_parameters[i_param]->attribute_array->has_array_ref)
				{
					VBO_ID vbo;

					memcpy(&vbo, oentity->shader_parameters[i_param]->attribute_array->array_ref.data, sizeof(VBO_ID));

					render_delete_buffer(vbo);
				}
			}

			shaders_parameters__free_unpacked(oentity->shader_parameters[i_param], NULL);
		}

		wf_free(oentity->shader_parameters);
	}

	oentity->shader_parameters = NULL;

	if (oentity->render_pass != NULL)
	{
		for (uint32_t i_pass = 0; i_pass < oentity->num_pass; i_pass++)
		{
			render_pass__free_unpacked(oentity->render_pass[i_pass], NULL);
		}

		wf_free(oentity->render_pass);
	}

	oentity->render_pass = NULL;
	oentity->extraptr1 = NULL;

	oentity->program_id = render_get_emptyprogvalue();
	oentity->num_parameters = 0;
}
