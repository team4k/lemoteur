

#ifdef _MSC_VER
extern "C" {
#endif
#include "lauxlib.h"
#include "lualib.h"
#include <lua.h>

#ifdef _MSC_VER
}
#endif

#if defined(_WIN32)
#include <windows.h>

#if defined(WINAPI_FAMILY) && (WINAPI_FAMILY == WINAPI_FAMILY_PHONE_APP)
#include <thread>
#endif

#else
#ifndef __USE_BSD
#define __USE_BSD
#endif
#include <unistd.h>
#include <dlfcn.h>
#include <pthread.h>
#endif



#include <inttypes.h>
#include <sqlite3.h>

#include <Render.h>

#include <math.h>
#include <string.h>
//#include <stdlib.h>
#include <Engine.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>
#include <Base/geom.h>

#include <Utils/Hash.h>
#include <Utils/hashmap.h>
#include <Utils/display_list.h>
#include <Utils/tempallocator.h>
#include <Utils/circular_array.h>

#include <Render/render_manager.h>

#include <Graphics/TextureLoader.h>
#include <GameObjects/Shaders.pb-c.h>
#include <Resx/shader_loader.h>


#include <GameObjects/Characters.pb-c.h>
#include <Graphics/Animation.h>
#include <Graphics/Sprite.h>
#include <Graphics/Mesh.h>
#include <Graphics/Light.h>
#include <Graphics/LightManager.h>

#include <Graphics/TextureRegion.h>
#include <Graphics/SpriteBatch.h>
#include <Graphics/FontLoader.h>

#include "GameObjects/Level.pb-c.h"

#include <Debug/Logprint.h>

#include <Sound/Sound.h>

#include <Resx/Resources.h>
#include <Resx/localization.h>

#include <Collisions/Collisions.h>
#include <Graphics/TilemapLayer.h>
#include <GameObjects/Tilemap.h>

#include <chipmunk/chipmunk.h>
#include <Physics/Physics.h>
#include <Physics/physics_utils.h>



#include <GameObjects/entity.h>
#include <GameObjects/Trigger.h>
#include <Config/config.h>
#include <GameMngr/input_mngr.h>
#include <game_functions.h>
#include <GameMngr/module_mngr.h>

#include <Font/RenderText.h>

#include <GameObjects/text_entity.h>

#include <GameObjects/entity_group.h>

#ifndef EMSCRIPTEN
#include <portaudio.h>
#else
#include <emscripten.h>
#endif

#include <Graphics/Effects/particle_object.h>
#include <GameObjects/particles_entity.h>

#include <Sound/snd_mngr.h>

#include <Threading/thread_func.h>

#include <Pathfinding/pathfind_worker.h>

#include <Threading/threads.h>

#include <Render/render_shader_utils.h>
#include <GameObjects/map.h>
#include <GameObjects/Collider.h>
#include <GameObjects/waypoint.h>

#include <GameObjects/Levelobject.h>
#include <GameObjects/level_stream.h>
#include <GameObjects/level_stream_chunks.h>
#include <Scripting/ScriptEngine.h>

#include <GameObjects/level_base.h>


void lvlbase_init(level_base_t* const lvl_base, void* const static_level_obj, void* const stream_level_obj, LEVEL_TYPE startup_type)
{
	lvl_base->static_level_obj = static_level_obj;
	lvl_base->stream_level_obj = stream_level_obj;
	lvl_base->current_level_type = startup_type;
	lvl_base->pending_level_type = startup_type;
	lvl_base->level_switch_callback = NULL;
	memset(lvl_base->pending_level, 0, sizeof(char) * 100); 
}

void lvlbase_setswitchcallback(level_base_t* const lvl_base,level_type_switch_t callback)
{
	lvl_base->level_switch_callback = callback;
}

void lvlbase_requestswitchmode(level_base_t* const lvl_base, LEVEL_TYPE switch_to_type,const char* pending_level)
{
	Levelobject_t* static_level = (Levelobject_t*)lvl_base->static_level_obj;
	level_stream_t* stream_level = (level_stream_t*)lvl_base->stream_level_obj;

	static_level->current_state = COMMON_SWITCH;
	stream_level->current_state = COMMON_SWITCH;

	lvl_base->pending_level_type = switch_to_type;

	w_strcpy(lvl_base->pending_level, 100, pending_level);
}

void lvlbase_doswitchmode(level_base_t* const lvl_base)
{
	Levelobject_t* static_level = (Levelobject_t*)lvl_base->static_level_obj;
	level_stream_t* stream_level = (level_stream_t*)lvl_base->stream_level_obj;

	lvl_base->current_level_type = lvl_base->pending_level_type;

	if (lvl_base->current_level_type == L_STATIC) {
		static_level->current_state = COMMON_RUN;
		Levelobject_requestnewlevel(static_level, lvl_base->pending_level);
	}
	else if (lvl_base->current_level_type == L_STREAM) {
		stream_level->current_state = COMMON_RUN;
		level_stream_doswitch(stream_level);
	}

	if (lvl_base->level_switch_callback != NULL) {
		lvl_base->level_switch_callback(lvl_base->current_level_type);
	}
}

void lvlbase_requestexit(const void* const level_obj, LEVEL_TYPE type)
{
	if (type == L_STATIC)
		Levelobject_requestexit((Levelobject_t*)level_obj);
	else
		((level_stream_t*)level_obj)->request_exit = wtrue;
}

void* lvlbase_getcurrentlevelobj(level_base_t* const lvl_base)
{
	if (lvl_base->current_level_type == L_STATIC) {
		return lvl_base->static_level_obj;
	}
	else {
		return lvl_base->stream_level_obj;
	}
}

void lvlbase_updatezorder(const void* const level_obj, LEVEL_TYPE type,const char* entity_key,int new_zorder,GAME_TYPE gtype)
{
	if (type == L_STATIC)
		Levelobject_updatezorder((Levelobject_t*)level_obj, entity_key, new_zorder, gtype);
	else
		logprint("lvlbase_updatezorder is not implemented in stream mode");

}



uint32_t lvlbase_getcurrentscriptid(const void* const level_obj, LEVEL_TYPE type)
{
	if (type == L_STATIC)
		return ((Levelobject_t*)level_obj)->script_id;
	else
		return ((level_stream_t*)level_obj)->script_id;
}

uint32_t lvlbase_getscriptid(level_base_t* const lvl_base, LEVEL_TYPE type)
{
	if (type == L_STATIC)
		return ((Levelobject_t*)lvl_base->static_level_obj)->script_id;
	else
	{
		if (lvl_base->stream_level_obj != NULL)
			return ((level_stream_t*)lvl_base->stream_level_obj)->script_id;
		else
			return 0;
	}
}

int32_t lvlbase_getrowcount(const void* const level_obj,LEVEL_TYPE type, wbool global)
{
	if (type == L_STATIC)
		return ((Levelobject_t*)level_obj)->current_map->tilemap.row_count;
	else
	{
		if (!global)
			return ((level_stream_t*)level_obj)->base_row_count;
		else
			return ((level_stream_t*)level_obj)->base_row_count * CHUNK_ROW;
	}
		
}

int32_t lvlbase_getcolcount(const void* const level_obj,LEVEL_TYPE type,wbool global)
{
	if (type == L_STATIC)
		return ((Levelobject_t*)level_obj)->current_map->tilemap.col_count;
	else
	{
		if(!global)
			return ((level_stream_t*)level_obj)->base_col_count;
		else
			return ((level_stream_t*)level_obj)->base_col_count * CHUNK_COL;
	}
		
}

int32_t lvlbase_gettilesize(const void* const level_obj, LEVEL_TYPE type)
{
	if (type == L_STATIC)
		return ((Levelobject_t*)level_obj)->current_map->tilemap.tile_size;
	else
		return ((level_stream_t*)level_obj)->tile_size;
}

WorldRect_t lvlbase_getdimension(const void* const level_obj, LEVEL_TYPE type)
{
	WorldRect_t map_dimension = worldrectzero;
	if (type == L_STATIC)
	{
		Levelobject_t* lvl_mngr = (Levelobject_t*)level_obj;
		/*map_dimension.position.x = lvl_mngr->current_map;
		map_dimension.position.y = 0;
		map_dimension.width = lvl_mngr->current_map->tilemap.col_count * lvl_mngr->current_map->tilemap.tile_size;
		map_dimension.height = lvl_mngr->current_map->tilemap.row_count * lvl_mngr->current_map->tilemap.tile_size;*/
		map_dimension.loadeddimension = lvl_mngr->current_map->map_dimension;
		map_dimension.worldwidth = map_dimension.loadeddimension.width;
		map_dimension.worldheight = map_dimension.loadeddimension.height;
		return map_dimension;
	}
	else
	{
		level_stream_t* lvl_mngr = (level_stream_t*)level_obj;

		map_dimension.loadeddimension.position = lvl_mngr->access_chunk[O_BOTTOMLEFT]->bounds.position;
		map_dimension.loadeddimension.width = (lvl_mngr->base_col_count * CHUNK_COL) *  lvl_mngr->tile_size;
		map_dimension.loadeddimension.height = (lvl_mngr->base_row_count * CHUNK_ROW) * lvl_mngr->tile_size;
		map_dimension.worldwidth = (lvl_mngr->base_col_count * lvl_mngr->world_col_limit) * lvl_mngr->tile_size;
		map_dimension.worldheight = (lvl_mngr->base_row_count * lvl_mngr->world_row_limit) * lvl_mngr->tile_size;
	}

	return map_dimension;
}

wbool lvlbase_hasdestructiblelayer(const void* const level_obj,LEVEL_TYPE type, int32_t tilePos)
{
	if (type == L_STATIC)
	{
		return (wbool)(((Levelobject_t*)level_obj)->current_map != NULL && ((Levelobject_t*)level_obj)->current_map->tilemap.layer_destructible != NULL);
	}
	else
	{
		level_stream_t* tmp = (level_stream_t*)level_obj;

		//get the chunk from the tile pos

		int32_t row = (int32_t)floorf((float)(tilePos / (tmp->base_col_count * CHUNK_COL)));
		int32_t col = tilePos - ((tmp->base_col_count * CHUNK_COL) * row);

		int32_t chunk_col = (int32_t)floorf((float)col / tmp->base_col_count);
			
		int32_t chunk_row = (int32_t)floorf((float)row / tmp->base_row_count);

		int32_t chunk_pos = (chunk_row * CHUNK_COL) + chunk_col;

		return (wbool)(tmp->access_chunk[chunk_pos]->map.layer_destructible != NULL);
	}
		
}

void* lvlbase_getdestructiblelayer(const void* const level_obj,LEVEL_TYPE type, int32_t tilePos)
{
	if (type == L_STATIC)
		return ((Levelobject_t*)level_obj)->current_map->tilemap.layer_destructible;
	else
	{
		level_stream_t* tmp = (level_stream_t*)level_obj;

		//get the chunk from the tile pos

		int32_t row = (int32_t)floorf((float)(tilePos / (tmp->base_col_count * CHUNK_COL)));
		int32_t col = tilePos - ((tmp->base_col_count * CHUNK_COL) * row);

		int32_t chunk_col = (int32_t)floorf((float)col / tmp->base_col_count);

		int32_t chunk_row = (int32_t)floorf((float)row / tmp->base_row_count);

		int32_t chunk_pos = (chunk_row * CHUNK_COL) + chunk_col;

		return tmp->access_chunk[chunk_pos]->map.layer_destructible;
	}
	
}

void* lvlbase_gettilemap(const void* const level_obj,LEVEL_TYPE type, int32_t tilePos)
{
	if (type == L_STATIC)
		return &((Levelobject_t*)level_obj)->current_map->tilemap;
	else
	{
		level_stream_t* tmp = (level_stream_t*)level_obj;

		//get the chunk from the tile pos

		int32_t row = (int32_t)floorf((float)(tilePos / (tmp->base_col_count * CHUNK_COL)));
		int32_t col = tilePos - ((tmp->base_col_count * CHUNK_COL) * row);

		int32_t chunk_col = (int32_t)floorf((float)col / tmp->base_col_count);

		int32_t chunk_row = (int32_t)floorf((float)row / tmp->base_row_count);

		int32_t chunk_pos = (chunk_row * CHUNK_COL) + chunk_col;

		return &tmp->access_chunk[chunk_pos]->map;
	}
}

int32_t* lvlbase_getcollisionsdata(const void* const level_obj, LEVEL_TYPE type)
{
	if (type == L_STATIC)
		return ((Levelobject_t*)level_obj)->current_map->tilemap.collisions_array;
	else
		return ((level_stream_t*)level_obj)->collisions_array;
}

void lvlbase_setcheckcolfunc(level_base_t* const lvl_base, check_col_t check_col_func,check_col_t check_col_func_editable)
{
	((Levelobject_t*)lvl_base->static_level_obj)->check_col_func = check_col_func;
	((Levelobject_t*)lvl_base->static_level_obj)->check_col_func_editable = check_col_func_editable;

	if (lvl_base->stream_level_obj != NULL) {
		((level_stream_t*)lvl_base->stream_level_obj)->check_col_func = check_col_func;
		((level_stream_t*)lvl_base->stream_level_obj)->check_col_func_editable = check_col_func_editable;
	}
}

void lvlbase_setrandomizer(level_base_t* const lvl_base, tlayer_randomizer_t randomizer)
{
	Levelobject_setrandomizer_func((Levelobject_t*)lvl_base->static_level_obj, randomizer);

	if (lvl_base->stream_level_obj != NULL) {
		level_stream_setrandomizer_func((level_stream_t*)lvl_base->stream_level_obj, randomizer);
	}
}

/*
void lvlbase_setentitycallback(level_base_t* const lvl_base, void(*ondraw_func)(entity_t* const currententity), void(*onupdate_func)(entity_t* const currententity), void(*ondelete_func)(entity_t* const currententity))
{
	levelobject_setentitycallback(((Levelobject_t*)lvl_base->static_level_obj), ondraw_func, onupdate_func, ondelete_func);

	if (lvl_base->stream_level_obj != NULL) {
		level_stream_setentitycallback(((level_stream_t*)lvl_base->stream_level_obj), ondraw_func, onupdate_func, ondelete_func);
	}
}*/

void lvlbase_genedgelist(const void* const level_obj,LEVEL_TYPE type,  wbool regen)
{
	if (type == L_STATIC)
	{
		Levelobject_t* tmp = (Levelobject_t*)level_obj;
		Tilemap_gen_edge_list(&tmp->current_map->tilemap, regen,tmp->check_col_func);
	}
	else
	{
		level_stream_t* tmp = (level_stream_t*)level_obj;
		gen_edges_list(tmp->base_row_count * CHUNK_ROW,tmp->base_col_count * CHUNK_COL, tmp->tile_size, tmp->collisions_array, tmp->edges_array, &tmp->preallocated_edges, tmp->check_col_func, tmp->access_chunk[O_BOTTOMLEFT]->bounds.position);
	}
		
}

void lvlbase_updateedge(const void* const level_obj,LEVEL_TYPE type,  int32_t tilepos, wbool addtile,const int32_t* const collisions_array)
{
	if (type == L_STATIC)
	{
		Tilemap_update_edge(&((Levelobject_t*)level_obj)->current_map->tilemap, tilepos, addtile, collisions_array);
	}
	else
	{
		level_stream_t* tmp = (level_stream_t*)level_obj;

		update_edges(tmp->base_col_count * CHUNK_COL, tmp->base_row_count * CHUNK_ROW, tmp->tile_size, tmp->edges_array, tmp->edges_array_size, collisions_array, tilepos, addtile,tmp->preallocated_edges, tmp->access_chunk[O_BOTTOMLEFT]->bounds.position);
	}
	
}

void* lvlbase_getlightmanager(const void* const level_obj, LEVEL_TYPE type)
{
	if (type == L_STATIC)
	{
		return (light_manager_t*)((Levelobject_t*)level_obj)->light_manager;
	}
	else
	{
		return (light_manager_t*)((level_stream_t*)level_obj)->light_manager;
	}

}

int lvlbase_getcurrentstate(const void* const level_obj, LEVEL_TYPE type)
{
	if (type == L_STATIC)
	{
		return ((Levelobject_t*)level_obj)->current_state;
	}
	else
	{
		return ((level_stream_t*)level_obj)->current_state;
	}
}

void lvlbase_setcurrentstate(const void* const level_obj,LEVEL_TYPE type, int state)
{
	if (type == L_STATIC)
	{
		((Levelobject_t*)level_obj)->current_state = state;
	}
	else
	{
		((level_stream_t*)level_obj)->current_state = state;
	}
}

wbool lvlbase_hasentity(const void* const level_obj,LEVEL_TYPE type,  const char* entity_name)
{
	if (type == L_STATIC)
	{
		return (wbool)hashtable_haskey(&((Levelobject_t*)level_obj)->entities_hash, entity_name);
	}
	else
	{
		level_stream_t* lvl = (level_stream_t*)level_obj;

		wbool result_entity = (wbool)hashmap_haskey(&lvl->entities, entity_name);

		if (!result_entity) 
		{
			for (uint32_t ichunk = 0; ichunk < CHUNK_NUMBER; ichunk++)
			{
				volatile uint32_t chunk_state = S_EMPTY;

				atomic_set_value(&chunk_state, lvl->chunks[ichunk].state);

				if ((chunk_state == S_LOADED || chunk_state == S_SCRIPTEXEC || chunk_state == S_UNLOADING || chunk_state == S_FRONTUNLOADING) && lvl->chunks[ichunk].visible && lvl->chunks[ichunk].entities.num_elem > 0)
				{
					result_entity = (wbool)hashmap_haskey(&lvl->chunks[ichunk].entities, entity_name);

					if (result_entity)
					{
						break;
					}
				}
			}

		}

		return result_entity;
	}
}

int lvlbase_getentitypos(const void* const level_obj,LEVEL_TYPE type,  const char* entity_name)
{
	if (type == L_STATIC)
	{
		int pos = hashtable_index(&((Levelobject_t*)level_obj)->entities_hash, entity_name);

		return pos;
	}
	else
	{
		level_stream_t* lvl = (level_stream_t*)level_obj;

		int result_pos = hashmap_getindexforkey(&lvl->entities, entity_name);

		if (result_pos == -1)
		{
			for (uint32_t ichunk = 0; ichunk < CHUNK_NUMBER; ichunk++)
			{
				volatile uint32_t chunk_state = S_EMPTY;

				atomic_set_value(&chunk_state, lvl->chunks[ichunk].state);

				if ((chunk_state == S_LOADED || chunk_state == S_SCRIPTEXEC || chunk_state == S_UNLOADING || chunk_state == S_FRONTUNLOADING) && lvl->chunks[ichunk].visible && lvl->chunks[ichunk].entities.num_elem > 0)
				{
					result_pos = hashmap_getindexforkey(&lvl->chunks[ichunk].entities, entity_name);

					if (result_pos != -1) {
						break;
					}
				}
			}

		}

		return result_pos;
	}
}


void* lvlbase_getentity(const void* const level_obj,LEVEL_TYPE type,  const char* entity_name)
{
	if (type == L_STATIC)
	{

		int pos = hashtable_index(&((Levelobject_t*)level_obj)->entities_hash, entity_name);

		return &((Levelobject_t*)level_obj)->entities[pos];
	}
	else
	{
		level_stream_t* lvl = (level_stream_t*)level_obj;

		entity_t* result_entity = (entity_t*)hashmap_getvalue_withlogopt(&lvl->entities, entity_name,1);

		if (result_entity == NULL)
		{
			for (uint32_t ichunk = 0; ichunk < CHUNK_NUMBER; ichunk++)
			{

				volatile uint32_t chunk_state = S_EMPTY;

				atomic_set_value(&chunk_state, lvl->chunks[ichunk].state);

				if ((chunk_state == S_LOADED || chunk_state == S_SCRIPTEXEC || chunk_state == S_UNLOADING || chunk_state == S_FRONTUNLOADING) && lvl->chunks[ichunk].visible && lvl->chunks[ichunk].entities.num_elem > 0)
				{
					result_entity = (entity_t*)hashmap_getvalue_withlogopt(&lvl->chunks[ichunk].entities, entity_name,1);

					if (result_entity != NULL)
					{
						break;
					}
				}
			}

		}

		return result_entity;
	}
}

void* lvlbase_getentity_fromindex(const void* const level_obj,LEVEL_TYPE type,  int entity_index)
{
	if (type == L_STATIC)
	{

		return &((Levelobject_t*)level_obj)->entities[entity_index];
	}
	else
	{
		level_stream_t* lvl = (level_stream_t*)level_obj;

		entity_t* result_entity = (entity_t*)hashmap_getvaluefromindex(&lvl->entities, entity_index);

		if (result_entity == NULL)
		{
			for (uint32_t ichunk = 0; ichunk < CHUNK_NUMBER; ichunk++)
			{

				volatile uint32_t chunk_state = S_EMPTY;

				atomic_set_value(&chunk_state, lvl->chunks[ichunk].state);

				if ((chunk_state == S_LOADED || chunk_state == S_SCRIPTEXEC || chunk_state == S_UNLOADING || chunk_state == S_FRONTUNLOADING) && lvl->chunks[ichunk].visible && lvl->chunks[ichunk].entities.num_elem > 0)
				{
					result_entity = (entity_t*)hashmap_getvaluefromindex(&lvl->chunks[ichunk].entities, entity_index);

					if (result_entity != NULL)
					{
						break;
					}
				}
			}

		}

		return result_entity;
	}
}

void lvlbase_callbackonallentities(const void* const level_obj,LEVEL_TYPE type,  const char* callback)
{
	if (type == L_STATIC)
	{
		Levelobject_t* local_lvl_mngr = (Levelobject_t*)level_obj;

		uint32_t ent_indx = 0;
		uint32_t ent_count = 0;

		while (ent_count <  local_lvl_mngr->entities_hash.num_elem)
		{
			if (local_lvl_mngr->entities_hash.keys[ent_indx])
			{
				if (strcmp(local_lvl_mngr->entities[ent_indx].ref_map_id, local_lvl_mngr->current_map_id) == 0)
				{
					if (local_lvl_mngr->entities[ent_indx].callback_func != NULL)
					{
						entity_callback(&local_lvl_mngr->entities[ent_indx], callback, NULL);
					}
				}

				ent_count++;
			}

			ent_indx++;
		}
	}
	else
	{
		uint32_t o_index = 0;
		uint32_t o_count = 0;

		level_stream_t* lvl = (level_stream_t*)level_obj;

		for (entity_t* oent = (entity_t*)hashmap_iterate(&lvl->entities, &o_index, &o_count); oent != NULL; oent = (entity_t*)hashmap_iterate(&lvl->entities, &o_index, &o_count))
		{
			entity_callback(oent, callback, NULL);
		}
	}
}

void lvlbase_resetscriptinitonlyentities(const void* const level_obj,LEVEL_TYPE type, lua_State* state)
{
	if (type == L_STATIC)
	{
		Levelobject_t* local_lvl_mngr = (Levelobject_t*)level_obj;

		uint32_t ent_indx = 0;
		uint32_t ent_count = 0;

		while (ent_count <  local_lvl_mngr->entities_hash.num_elem)
		{
			if (local_lvl_mngr->entities_hash.keys[ent_indx])
			{
				if (strcmp(local_lvl_mngr->entities[ent_indx].ref_map_id, local_lvl_mngr->current_map_id) == 0)
				{
					if (!local_lvl_mngr->entities[ent_indx].has_script && strlen(local_lvl_mngr->entities[ent_indx].script_ctor) != 0)
					{
						script_execbuffer(state, local_lvl_mngr->entities[ent_indx].script_ctor, strlen(local_lvl_mngr->entities[ent_indx].script_ctor));
					}
				}

				ent_count++;
			}

			ent_indx++;
		}
	}
	else
	{
		uint32_t o_index = 0;
		uint32_t o_count = 0;

		level_stream_t* lvl = (level_stream_t*)level_obj;

		for (entity_t* oent = (entity_t*)hashmap_iterate(&lvl->entities, &o_index, &o_count); oent != NULL; oent = (entity_t*)hashmap_iterate(&lvl->entities, &o_index, &o_count))
		{
			if (!oent->has_script && strlen(oent->script_ctor) != 0)
			{
				script_execbuffer(state, oent->script_ctor, strlen(oent->script_ctor));
			}
		}
	}
}


wbool lvlbase_hastrigger(const void* const level_obj,LEVEL_TYPE type,  const char* trigger_name)
{
	if (type == L_STATIC)
	{
		return (wbool)hashtable_haskey(&((Levelobject_t*)level_obj)->triggers_hash, trigger_name);
	}
	else
	{

		level_stream_t* lvl = (level_stream_t*)level_obj;
		wbool result_trigger = wfalse;

		for (uint32_t ichunk = 0; ichunk < CHUNK_NUMBER; ichunk++)
		{

			volatile uint32_t chunk_state = S_EMPTY;

			atomic_set_value(&chunk_state, lvl->chunks[ichunk].state);

			if ((chunk_state == S_LOADED || chunk_state == S_SCRIPTEXEC || chunk_state == S_UNLOADING || chunk_state == S_FRONTUNLOADING) && lvl->chunks[ichunk].visible && lvl->chunks[ichunk].triggers.num_elem > 0)
			{

				result_trigger = (wbool)hashmap_haskey(&lvl->chunks[ichunk].triggers, trigger_name);

				if (result_trigger) {
					break;
				}
			}
		}

		return result_trigger;

		
	}
}

void* lvlbase_gettrigger(const void* const level_obj,LEVEL_TYPE type,  const char* trigger_name)
{
	if (type == L_STATIC)
	{

		int pos = hashtable_index(&((Levelobject_t*)level_obj)->triggers_hash, trigger_name);

		return &((Levelobject_t*)level_obj)->triggers[pos];
	}
	else
	{
		level_stream_t* lvl = (level_stream_t*)level_obj;
		trigger_t* result_trigger = NULL;

		for (uint32_t ichunk = 0; ichunk < CHUNK_NUMBER; ichunk++)
		{

			volatile uint32_t chunk_state = S_EMPTY;

			atomic_set_value(&chunk_state, lvl->chunks[ichunk].state);

			if ((chunk_state == S_LOADED || chunk_state == S_SCRIPTEXEC || chunk_state == S_UNLOADING || chunk_state == S_FRONTUNLOADING) && lvl->chunks[ichunk].visible)
			{
				result_trigger = (trigger_t*)hashmap_getvalue(&lvl->chunks[ichunk].triggers, trigger_name);

				if (result_trigger != NULL) {
					break;
				}
			}
		}

		return result_trigger;

	
	}
}

void lvlbase_checktriggers(const void* const level_obj,LEVEL_TYPE type,  void* entity_box, const char* entity_id)
{

	if (type == L_STATIC)
	{
		Levelobject_t* local_lvl_mngr = (Levelobject_t*)level_obj;

		Levelobject_checktriggers(local_lvl_mngr,(*(cpBB*)entity_box), entity_id);
	}
	else
	{
		level_stream_t* lvl = (level_stream_t*)level_obj;
		level_stream_checktriggers(lvl, (*(cpBB*)entity_box), entity_id);
	}
}

wbool lvlbase_hastext(const void* const level_obj,LEVEL_TYPE type,  const char* text_name)
{
	if (type == L_STATIC)
	{
		return (wbool)hashtable_haskey(&((Levelobject_t*)level_obj)->text_hash, text_name);
	}
	else
	{

		level_stream_t* lvl = (level_stream_t*)level_obj;
		wbool result_text = wfalse;

		for (uint32_t ichunk = 0; ichunk < CHUNK_NUMBER; ichunk++)
		{

			volatile uint32_t chunk_state = S_EMPTY;

			atomic_set_value(&chunk_state, lvl->chunks[ichunk].state);

			if ((chunk_state == S_LOADED || chunk_state == S_SCRIPTEXEC || chunk_state == S_UNLOADING || chunk_state == S_FRONTUNLOADING) && lvl->chunks[ichunk].visible && lvl->chunks[ichunk].text.num_elem > 0)
			{
				result_text = (wbool)hashmap_haskey(&lvl->chunks[ichunk].text, text_name);

				if (result_text) {
					break;
				}
			}
		}

		return result_text;


	}
}

void* lvlbase_gettext(const void* const level_obj,LEVEL_TYPE type,  const char* text_name)
{
	if (type == L_STATIC)
	{

		int pos = hashtable_index(&((Levelobject_t*)level_obj)->text_hash, text_name);

		return &((Levelobject_t*)level_obj)->text_objects[pos];
	}
	else
	{
		level_stream_t* lvl = (level_stream_t*)level_obj;
		text_entity_t* result_text = NULL;

		for (uint32_t ichunk = 0; ichunk < CHUNK_NUMBER; ichunk++)
		{

			volatile uint32_t chunk_state = S_EMPTY;

			atomic_set_value(&chunk_state, lvl->chunks[ichunk].state);

			if ((chunk_state == S_LOADED || chunk_state == S_SCRIPTEXEC || chunk_state == S_UNLOADING || chunk_state == S_FRONTUNLOADING) && lvl->chunks[ichunk].visible)
			{
				result_text = (text_entity_t*)hashmap_getvalue(&lvl->chunks[ichunk].text, text_name);

				if (result_text != NULL) {
					break;
				}
			}
		}

		return result_text;


	}
}

wbool lvlbase_haswaypoint(const void* const level_obj,LEVEL_TYPE type,  const char* waypoint_id)
{
	if (type == L_STATIC)
	{
		return (wbool)hashmap_haskey(&((Levelobject_t*)level_obj)->waypoints_list, waypoint_id);
	}
	else
	{

		level_stream_t* lvl = (level_stream_t*)level_obj;
		wbool result_way = wfalse;

		for (uint32_t ichunk = 0; ichunk < CHUNK_NUMBER; ichunk++)
		{

			volatile uint32_t chunk_state = S_EMPTY;

			atomic_set_value(&chunk_state, lvl->chunks[ichunk].state);

			if ((chunk_state == S_LOADED || chunk_state == S_SCRIPTEXEC || chunk_state == S_UNLOADING || chunk_state == S_FRONTUNLOADING) && lvl->chunks[ichunk].visible && lvl->chunks[ichunk].waypoints.num_elem > 0)
			{
				result_way = (wbool)hashmap_haskey(&lvl->chunks[ichunk].waypoints, waypoint_id);

				if (result_way) {
					break;
				}
			}
		}

		return result_way;


	}
}

void* lvlbase_getwaypoint(const void* const level_obj,LEVEL_TYPE type,  const char* waypoint_id)
{
	if (type == L_STATIC)
	{

		return hashmap_getvalue(&((Levelobject_t*)level_obj)->waypoints_list, waypoint_id);
	}
	else
	{
		level_stream_t* lvl = (level_stream_t*)level_obj;
		waypoint_t* result_way = NULL;

		for (uint32_t ichunk = 0; ichunk < CHUNK_NUMBER; ichunk++)
		{

			volatile uint32_t chunk_state = S_EMPTY;

			atomic_set_value(&chunk_state, lvl->chunks[ichunk].state);

			if ((chunk_state == S_LOADED || chunk_state == S_SCRIPTEXEC || chunk_state == S_UNLOADING || chunk_state == S_FRONTUNLOADING) && lvl->chunks[ichunk].visible)
			{
				result_way = (waypoint_t*)hashmap_getvalue(&lvl->chunks[ichunk].waypoints, waypoint_id);

				if (result_way != NULL) {
					break;
				}
			}
		}

		return result_way;


	}
}


wbool lvlbase_hascollider(const void* const level_obj,LEVEL_TYPE type,  const char* collider_id)
{
	if (type == L_STATIC)
	{
		return (wbool)hashmap_haskey(&((Levelobject_t*)level_obj)->colliders_list, collider_id);
	}
	else
	{

		level_stream_t* lvl = (level_stream_t*)level_obj;
		wbool result_col = wfalse;

		for (uint32_t ichunk = 0; ichunk < CHUNK_NUMBER; ichunk++)
		{

			volatile uint32_t chunk_state = S_EMPTY;

			atomic_set_value(&chunk_state, lvl->chunks[ichunk].state);

			if ((chunk_state == S_LOADED || chunk_state == S_SCRIPTEXEC || chunk_state == S_UNLOADING || chunk_state == S_FRONTUNLOADING) && lvl->chunks[ichunk].visible && lvl->chunks[ichunk].colliders.num_elem > 0)
			{
				result_col = (wbool)hashmap_haskey(&lvl->chunks[ichunk].colliders, collider_id);

				if (result_col) {
					break;
				}
			}
		}

		return result_col;


	}
}

void* lvlbase_getcollider(const void* const level_obj,LEVEL_TYPE type,  const char* collider_id)
{
	if (type == L_STATIC)
	{

		return hashmap_getvalue(&((Levelobject_t*)level_obj)->colliders_list, collider_id);
	}
	else
	{
		level_stream_t* lvl = (level_stream_t*)level_obj;
		collider_t* result_col = NULL;

		for (uint32_t ichunk = 0; ichunk < CHUNK_NUMBER; ichunk++)
		{

			volatile uint32_t chunk_state = S_EMPTY;

			atomic_set_value(&chunk_state, lvl->chunks[ichunk].state);

			if ((chunk_state == S_LOADED || chunk_state == S_SCRIPTEXEC || chunk_state == S_UNLOADING || chunk_state == S_FRONTUNLOADING) && lvl->chunks[ichunk].visible)
			{
				result_col = (collider_t*)hashmap_getvalue(&lvl->chunks[ichunk].colliders, collider_id);

				if (result_col != NULL) {
					break;
				}
			}
		}

		return result_col;


	}
}


void lvlbase_movetostart(const void* const level_obj,LEVEL_TYPE type, void* const phy_mngr)
{
	if (type == L_STATIC)
	{
		Levelobject_move_tostart((Levelobject_t*)level_obj,(physics_manager_t*)phy_mngr);
	}
	else
	{
		logprint("Not implemented");
	}
}

const char* lvlbase_getcurrentmodule(const void* const level_obj, LEVEL_TYPE type)
{
	if (type == L_STATIC)
	{
		return ((Levelobject_t*)level_obj)->current_module;
	}
	else
	{
		return  ((level_stream_t*)level_obj)->current_module;
	}
}

void* lvlbase_getcurrenttilemap(const void* const level_obj, LEVEL_TYPE type)
{
	if (type == L_STATIC)
	{
		return &((Levelobject_t*)level_obj)->current_map->tilemap;
	}
	else
	{

		level_stream_t* lvl = (level_stream_t*)level_obj;

		stream_chunk_t* chunk = level_stream_get_center_chunk(lvl);

		if (chunk != NULL)
		{
			return &chunk->map;
		}
		else
		{
			return NULL;
		}
	}
}

const char* lvlbase_getcurrentmapid(const void* const level_obj, LEVEL_TYPE type)
{
	if (type == L_STATIC)
	{
		return ((Levelobject_t*)level_obj)->current_map_id;
	}
	else
	{
		return NULL;
	}
}

int32_t lvlbase_convert_worldpos_to_tilepos(const void* const level_obj,LEVEL_TYPE type, int32_t worldPos)
{
	if (type == L_STATIC)
		 return worldPos;
	else
	{
		level_stream_t* tmp = (level_stream_t*)level_obj;
		return convert_worldpos_to_tilepos(worldPos, CHUNK_COL, tmp->base_col_count, tmp->base_row_count);
	}
	
}

uint32_t lvlbase_gettilefromvector(const void* const level_obj, LEVEL_TYPE type, Vector_t pos)
{
	if (type == L_STATIC) {
		Levelobject_t* lvl = (Levelobject_t*)level_obj;
		uint32_t current_col = (uint32_t)floorf(pos.x / lvl->current_map->tilemap.tile_size);
		uint32_t	current_row = (uint32_t)floorf(pos.y / lvl->current_map->tilemap.tile_size);

		return (current_row * lvl->current_map->tilemap.col_count) + current_col;
	}
	else
	{
		level_stream_t* tmp = (level_stream_t*)level_obj;

		uint32_t current_col = (uint32_t)floorf((pos.x - tmp->access_chunk[O_BOTTOMLEFT]->bounds.position.x) / tmp->tile_size);
		uint32_t	current_row = (uint32_t)floorf((pos.y - tmp->access_chunk[O_BOTTOMLEFT]->bounds.position.y) / tmp->tile_size);

		return (current_row * (tmp->base_col_count * CHUNK_COL)) + current_col;
	}
}


/*
 R�cup�re la position global d'une tile � partir de l'index local de cette tile sur un chunk, le param�tre ichunk correspond � la position
 en m�moire du chunk (et non � la position logique de ce chunk)
*/
int32_t lvlbase_convert_tilepos_to_worldpos(const void* const level_obj,LEVEL_TYPE type, uint32_t tilepos,uint16_t ichunk)
{
	if (type == L_STATIC)
		return tilepos;
	else
	{
		level_stream_t* tmp = (level_stream_t*)level_obj;

		return convert_tilepos_to_worldpos(tilepos, (uint16_t)tmp->chunks[ichunk].pos, CHUNK_COL, tmp->base_col_count, tmp->base_row_count);
	}
}

int16_t lvlbase_getmemorychunk_from_worldpos(const void* const level_obj,LEVEL_TYPE type, int32_t worldPos)
{
	if (type == L_STATIC)
		return 0;
	else
	{
		level_stream_t* tmp = (level_stream_t*)level_obj;

		int32_t row = (int32_t)floorf((float)(worldPos / (tmp->base_col_count * CHUNK_COL)));
		int32_t col = worldPos - ((tmp->base_col_count * CHUNK_COL) * row);

		int32_t chunk_col = (int32_t)floorf((float)col / tmp->base_col_count);

		int32_t chunk_row = (int32_t)floorf((float)row / tmp->base_row_count);

		int32_t chunk_pos = (chunk_row * CHUNK_COL) + chunk_col;

		return tmp->access_chunk[(STREAM_POS)chunk_pos]->mem_pos;
	}
}

/*
Get the index in memory of the first collision shape (4 shape per tile) corresponding to a tile position on a chunk
tilepos = position of a tile local to a chunk
mem_chunk = the position in memory of the chunk

*/
int32_t lvlbase_getmemoryshapeindex_from_tilepos(const void* const level_obj,LEVEL_TYPE type, int32_t tilepos,  uint16_t mem_chunk)
{
	if (type == L_STATIC)
		return tilepos * 4;
	else
	{
		level_stream_t* tmp = (level_stream_t*)level_obj;

		int32_t tile_row = (int32_t)floorf((float)(tilepos / (tmp->base_col_count)));
		int32_t tile_col = tilepos - (tmp->base_col_count * tile_row);



		int32_t mem_chunk_row = (int32_t)(mem_chunk / CHUNK_COL);
		int32_t mem_chunk_col = mem_chunk - (mem_chunk_row * CHUNK_COL);

		int32_t mem_tile_row = tile_row + (mem_chunk_row * tmp->base_row_count);
		int32_t mem_tile_col = tile_col + (mem_chunk_col * tmp->base_col_count);

		return ((mem_tile_row * (tmp->base_col_count * CHUNK_COL)) + mem_tile_col) * 4;
	}
}

