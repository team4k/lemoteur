#include <inttypes.h>

#include <Render.h>

#include <math.h>
#include <Engine.h>
#include <string.h>
#include <stdlib.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>
#include <Base/geom.h>

#include <Debug/Logprint.h>
#include <Render/render_manager.h>

#include <Graphics/TextureLoader.h>

#include <Utils/Hash.h>
#include <Utils/hashmap.h>
#include <Utils/display_list.h>

#include <GameObjects/Shaders.pb-c.h>
#include <Resx/shader_loader.h>
#include <GameObjects/Characters.pb-c.h>
#include <GameObjects/Level.pb-c.h>
#include <Graphics/Animation.h>
#include <Graphics/Sprite.h>
#include <Graphics/Light.h>
#include <Graphics/LightManager.h>

#include <Graphics/TextureRegion.h>
#include <Graphics/SpriteBatch.h>
#include <Graphics/FontLoader.h>

#include <Sound/Sound.h>

#include <Resx/Resources.h>

#include <Debug/geomdebug.h>


#include <Render/render_shader_utils.h>

#include <Collisions/Collisions.h>
#include <Graphics/TilemapLayer.h>
#include <GameObjects/Tilemap.h>

#include <chipmunk/chipmunk.h>
#include <Physics/physics_utils.h>



void Tilemap_Init(Tilemap_t* const tilemap,const int32_t tile_size,texture_info* const texturePointer,const char* texture_name,const Map* const oMap)
{
	int layer_indx = 0;
	tilemap->collisions_array = NULL;
    tilemap->collision_color.r =  tilemap->collision_color.g = tilemap->collision_color.b = 0.0f;
    tilemap->collision_color.a = 0.5f;

	tilemap->edge_array = NULL;
	tilemap->edge_array_size = 0;

	tilemap->preallocated_edges = NULL;

	tilemap->slope_color.r = 0.5f;
	tilemap->slope_color.g = 0.5f;
    tilemap->slope_color.b = 0.0f;
	tilemap->slope_color.a = 0.5f;

	tilemap->slope_inverse_color.r = tilemap->slope_inverse_color.b = 0.0f;
	tilemap->slope_inverse_color.g = 1.0f;
	tilemap->slope_inverse_color.a  = 0.5f;

	tilemap->destructible_color.r = 0.0f;
	tilemap->destructible_color.g = 0.5f;
	tilemap->destructible_color.b = 0.5f;
	tilemap->destructible_color.a = 0.5f;

	tilemap->editable_color.r = 0.89f;
	tilemap->editable_color.g = 1.0f;
	tilemap->editable_color.b = 0.0f;
	tilemap->editable_color.a = 0.5f;
	
	tilemap->editable_color_2.r = 0.64f;
	tilemap->editable_color_2.g = 0.0f;
	tilemap->editable_color_2.b = 1.0f;
	tilemap->editable_color_2.a = 0.5f;

	tilemap->editable_color_3.r = 0.0f;
	tilemap->editable_color_3.g = 0.0f;
	tilemap->editable_color_3.b = 1.0f;
	tilemap->editable_color_3.a = 0.5f;

	tilemap->kill_color.r = 1.0f;
	tilemap->kill_color.g = 0.0f;
	tilemap->kill_color.b = 0.0f;
	tilemap->kill_color.a = 0.5f;

	w_strcpy(&tilemap->texture_name[0],256,texture_name);
    tilemap->tile_size = tile_size;
	tilemap->col_count = oMap->col_count;
	tilemap->row_count = oMap->row_count;
    tilemap->texturePointer = texturePointer;
    tilemap->show_collisions = wtrue;

	tilemap->layer_hash.keys = (char**) wf_malloc(MAX_LAYER * sizeof (char*));

    //initialise keys array
    for (layer_indx = 0; layer_indx < MAX_LAYER; layer_indx++)
        tilemap->layer_hash.keys[layer_indx] = 0;

    tilemap->layer_hash.max_elem = MAX_LAYER;
	tilemap->layer_hash.num_elem = 0;
	tilemap->layer_destructible = NULL;

	tilemap->editor_collisions_colors = NULL;


	tilemap->edge_array = alloc_edges_list(tilemap->row_count, tilemap->col_count, tilemap->edge_array, &tilemap->edge_array_size,wfalse,&tilemap->preallocated_edges);
}


void Tilemap_AddCollisions(Tilemap_t* const tilemap,Layer* layer_data)
{
	tilemap->collisions_array = (int32_t*)wf_malloc(tilemap->row_count * tilemap->col_count * sizeof(int32_t));
	memcpy(tilemap->collisions_array,layer_data->tile_array,tilemap->row_count * tilemap->col_count * sizeof(int32_t));

	tilemap->collision_color.a = tilemap->destructible_color.a = tilemap->slope_color.a = tilemap->slope_inverse_color.a = tilemap->editable_color.a  =  tilemap->editable_color_2.a  = tilemap->editable_color_3.a = tilemap->kill_color.a =  layer_data->opacity;
}

void Tilemap_AddLayer(Tilemap_t* const tilemap, render_manager_t* const render_mngr, Layer* layer_data, texture_info* const texture_pointer, tlayer_randomizer_t randomizer)
{
	Tilemap_AddLayer_withback(tilemap, render_mngr, layer_data, texture_pointer, randomizer, wfalse);
}


void Tilemap_AddLayer_withback(Tilemap_t* const tilemap, render_manager_t* const render_mngr, Layer* layer_data, texture_info* const texture_pointer, tlayer_randomizer_t randomizer, wbool backgroundload)
{
	//copy our layer id into a safe string buffer (one that won't get modified after the stack unwind)

	//check if a layer with this name as already be added
	if(hashtable_haskey(&tilemap->layer_hash,layer_data->layer_id))
	{
		logprint("A layer with the id %s has already been added!",layer_data->layer_id);
		return;
	}

	char* layer_id = (char*)wf_malloc((strlen(layer_data->layer_id) + 1) * sizeof(char));
	w_strcpy(layer_id,(strlen(layer_data->layer_id) + 1),layer_data->layer_id);

	int layer_indx = hashtable_pushkey(&tilemap->layer_hash, layer_id);
		
	TilemapLayer_Init_withback(&tilemap->layer_array[layer_indx],tilemap->row_count,tilemap->col_count,tilemap->tile_size,tilemap->tile_size,texture_pointer,layer_data,render_mngr, randomizer, backgroundload);

	if(layer_data->order > tilemap->max_zindex)
		tilemap->max_zindex = layer_data->order;

	if(layer_data->animated)
		TilemapLayer_setanimated(&tilemap->layer_array[layer_indx],layer_data->frames,layer_data->n_frames);

	logprint("Layer %s added!",layer_id);
}

void Tilemap_frontload_layer(Tilemap_t* const tilemap, render_manager_t* const render_mngr,const char* layer_id)
{
	if (!hashtable_haskey(&tilemap->layer_hash, layer_id))
	{
		logprint("Invalid layer id!");
		return;
	}

	int layer_indx = hashtable_index(&tilemap->layer_hash, layer_id);

	TilemapLayer_frontload(&tilemap->layer_array[layer_indx], render_mngr);
}

void Tilemap_update_layer(Tilemap_t* const tilemap,Layer* layer_data,render_manager_t* const rendermngr)
{
	if(!hashtable_haskey(&tilemap->layer_hash,layer_data->layer_id))
	{
		logprint("Invalid layer id!");
		return;
	}

	int layer_indx = hashtable_index(&tilemap->layer_hash,layer_data->layer_id);

	if(layer_data->order > tilemap->max_zindex)
		tilemap->max_zindex = layer_data->order;


	tilemap->layer_array[layer_indx].parallax_x = layer_data->parallax_x;
	tilemap->layer_array[layer_indx].parallax_y = layer_data->parallax_y;

	TilemapLayer_set_shader_program(&tilemap->layer_array[layer_indx],layer_data,rendermngr);


	if(layer_data->animated)
		TilemapLayer_setanimated(&tilemap->layer_array[layer_indx],layer_data->frames,layer_data->n_frames);
	else
		TilemapLayer_clearanimated(&tilemap->layer_array[layer_indx]);
}

void Tilemap_update_layercontent(Tilemap_t* const tilemap,Layer* layer_data,render_manager_t* const render_mngr)
{
	if(!hashtable_haskey(&tilemap->layer_hash,layer_data->layer_id))
	{
		logprint("Invalid layer id!");
		return;
	}

	int layer_indx = hashtable_index(&tilemap->layer_hash,layer_data->layer_id);

	memcpy(tilemap->layer_array[layer_indx].tileArray,layer_data->tile_array,tilemap->row_count * tilemap->col_count * sizeof(int32_t));

	TilemapLayer_regenTexCoord(&tilemap->layer_array[layer_indx],render_mngr);

}

//add/remove columns to the top of the map row_direction = 0, add/remove columns to the bottom of the map  row_direction = 1 /  add / remove columns to the right of the map col_direction = 0,  add / remove columns to the left of the map col_direction = 1
void Tilemap_resize(Tilemap_t* const tilemap,const int32_t col_count, const int32_t row_count,const int16_t row_direction, const int16_t col_direction,render_manager_t* const render_mngr,check_col_t check_col_func)
{
	uint32_t layer_indx = 0;
	uint32_t layer_count = 0;

	int32_t old_col = tilemap->col_count;
	int32_t old_row = tilemap->row_count;
	tilemap->col_count = col_count;
    tilemap->row_count = row_count;

	if(tilemap->collisions_array != NULL)
	{
		//resize collision array
	
		wbool skip_row = wfalse;
		wbool skip_col = wfalse;

		//
		int32_t diff_row = abs(row_count - old_row);
		int32_t diff_col = abs(col_count - old_col);

		short row_sign = (row_count > old_row) ? 1 : -1;
		short col_sign = (col_count > old_col) ? 1 : -1;

		
		int32_t remove_col = 0;


		if(col_count < old_col)
		{
			remove_col = diff_col;
		}

		//reallocate cell to keep the existing tile position
		if(diff_col > 0 || diff_row > 0)
		{
			int32_t* new_collision_array = (int32_t*)wf_malloc((row_count * col_count) * sizeof(int32_t));

			int32_t old_id = 0;

			if (row_direction == 1 && row_sign == -1)
			{
				old_id += col_count * diff_row;
			}


			for(int ri1 = 0; ri1 < row_count;ri1++)
			{
				skip_row = (wbool)(row_direction == 1 && ri1 < diff_row && row_sign == 1);

				if(col_direction == 1)
					old_id += remove_col;

				for(int ri = 0; ri < col_count;ri++)
				{
					if(col_direction == 1 && remove_col == 0)
						skip_col = (wbool)(ri < diff_col);
					else
						skip_col = (wbool)(ri >= old_col);

					if(!skip_col && !skip_row)
					{
						if(old_id < (old_col * old_row))
							new_collision_array[ri + (ri1 * col_count)] = tilemap->collisions_array[old_id++];
						else
							new_collision_array[ri + (ri1 * col_count)] = 0;
					}
					else
					{
						new_collision_array[ri + (ri1 * col_count)] = 0;
					}
				}

				if(col_direction == 0)
					old_id += remove_col;
			}

			wf_free(tilemap->collisions_array);
			tilemap->collisions_array = new_collision_array;
		}
		else
		{
			tilemap->collisions_array = (int32_t*)wf_realloc(tilemap->collisions_array,(row_count * col_count) * sizeof(int32_t));
		}

		if(tilemap->editor_collisions_colors != NULL)
		{
			wf_free(tilemap->editor_collisions_colors);
			render_delete_buffer(tilemap->buffer_collisions_colors);
			tilemap->editor_collisions_colors  = NULL;
		}

		//regen edge list 
		Tilemap_gen_edge_list(tilemap,wtrue, check_col_func);
	}


	while(layer_count <  tilemap->layer_hash.num_elem)
	{
		if(tilemap->layer_hash.keys[layer_indx])
		{
			TilemapLayer_resize(&tilemap->layer_array[layer_indx],row_count,col_count,row_direction,col_direction,render_mngr);
			layer_count++;
		}

		layer_indx++;
	}
	
}

void Tilemap_update_tilesize(Tilemap_t* const tilemap,const int32_t tile_size,render_manager_t* const rendermngr)
{
	tilemap->tile_size = tile_size;


	uint32_t layer_indx = 0,layer_count = 0;

	while(layer_count <  tilemap->layer_hash.num_elem)
	{
		if(tilemap->layer_hash.keys[layer_indx])
		{
			TilemapLayer_update_tilesize(&tilemap->layer_array[layer_indx],tile_size,rendermngr);
			layer_count++;
		}

		layer_indx++;
	}

}



//used only by editor
void Tilemap_RemoveLayer(Tilemap_t* const tilemap,char* layer_id)
{
	int layer_indx =  hashtable_index(&tilemap->layer_hash,layer_id);

	TilemapLayer_Free(&tilemap->layer_array[layer_indx]);

	memset(&tilemap->layer_array[layer_indx],0,sizeof(TilemapLayer_t));

	char* ptr = tilemap->layer_hash.keys[layer_indx];
	hashtable_removekey(&tilemap->layer_hash,layer_id);

	wf_free(ptr);
}

char* Tilemap_changelayer_id(Tilemap_t* const tilemap, char* old_layer_id,char* new_layer_id)
{
	int layer_indx = hashtable_index(&tilemap->layer_hash,old_layer_id);

	char* ptr = tilemap->layer_hash.keys[layer_indx];
	hashtable_removekey(&tilemap->layer_hash,old_layer_id);

	int new_layer_indx = hashtable_pushkey(&tilemap->layer_hash,new_layer_id);

	if(new_layer_indx != layer_indx)
	{
		tilemap->layer_array[new_layer_indx] = tilemap->layer_array[layer_indx];

		memset(&tilemap->layer_array[layer_indx],0,sizeof(TilemapLayer_t));
	}

	return ptr;//return the old char ptr to let the calling class freeing it up
}

void Tilemap_changelayer_order(Tilemap_t* const tilemap, char* layer_id,int32_t draw_order)
{
	int layer_indx = hashtable_index(&tilemap->layer_hash,layer_id);

	tilemap->layer_array[layer_indx].draw_order = draw_order;

	if(draw_order > tilemap->max_zindex)
		tilemap->max_zindex = draw_order;
}

void Tilemap_destroytile(Tilemap_t* const tilemap,int32_t tile_pos,render_manager_t* const render_mngr)
{
	if(tilemap->layer_destructible)
	{
		TilemapLayer_editTile(tilemap->layer_destructible,tilemap->layer_destructible->destruct_tile_id,tile_pos,render_mngr,wtrue);
	}
}

void Tilemap_setdestructiblelayer(Tilemap_t* const tilemap, char* layer_id,int32_t tile_destruct)
{
	int layer_indx = hashtable_index(&tilemap->layer_hash,layer_id);

	TilemapLayer_setDestructible(&tilemap->layer_array[layer_indx],wtrue,tile_destruct);

	tilemap->layer_destructible = &tilemap->layer_array[layer_indx];
}

void Tilemap_cleardestructiblelayer(Tilemap_t* const tilemap)
{
	if(tilemap->layer_destructible)
	{
		TilemapLayer_setDestructible(tilemap->layer_destructible,wfalse,0);
		tilemap->layer_destructible = NULL;
	}
}

void Tilemap_TextureChanged(Tilemap_t* const tilemap,render_manager_t* const render_mngr)
{
	uint32_t layer_indx = 0;
	uint32_t layer_count = 0;

	while(layer_count <  tilemap->layer_hash.num_elem)
	{
		if(tilemap->layer_hash.keys[layer_indx])
		{
			TilemapLayer_TextureChanged(&tilemap->layer_array[layer_indx],render_mngr);
			layer_count++;
		}

		layer_indx++;
	}
	
}

void Tilemap_regenTexCoord(Tilemap_t* const tilemap,char* layer_id,render_manager_t* const render_mngr)
{
	int layer_indx = hashtable_index(&tilemap->layer_hash,layer_id);

	TilemapLayer_regenTexCoord(&tilemap->layer_array[layer_indx],render_mngr);
}

void Tilemap_g_clean(Tilemap_t* const tilemap)
{
	uint32_t layer_indx = 0;
	uint32_t layer_count = 0;


	while(layer_count <  tilemap->layer_hash.num_elem)
	{
		if(tilemap->layer_hash.keys[layer_indx])
		{
			render_delete_buffer(tilemap->layer_array[layer_indx].VBOvertices);
			render_delete_buffer(tilemap->layer_array[layer_indx].VBOtexcoord);
			tilemap->layer_array[layer_indx].program_id = render_get_emptyprogvalue();

			//remove shader params that need recreation (VBOs)
			if (tilemap->layer_array[layer_indx].shader_parameters != NULL)
			{
				for (uint32_t i_param = 0; i_param < tilemap->layer_array[layer_indx].num_parameters; i_param++)
				{
					if (tilemap->layer_array[layer_indx].shader_parameters[i_param]->param_type == TYPE_PARAM_SHADER__attribute_array)
					{
						if (tilemap->layer_array[layer_indx].shader_parameters[i_param]->attribute_array->has_array_ref)
						{
							VBO_ID vbo;

							memcpy(&vbo, tilemap->layer_array[layer_indx].shader_parameters[i_param]->attribute_array->array_ref.data, sizeof(VBO_ID));

							render_delete_buffer(vbo);

							tilemap->layer_array[layer_indx].shader_parameters[i_param]->attribute_array->has_array_ref = (protobuf_c_boolean)0;

							wf_free(tilemap->layer_array[layer_indx].shader_parameters[i_param]->attribute_array->array_ref.data);
							tilemap->layer_array[layer_indx].shader_parameters[i_param]->attribute_array->array_ref.len = 0;

							tilemap->layer_array[layer_indx].shader_parameters[i_param]->attribute_array->array_ref.data = NULL;
						}
					}
				}
			}


			layer_count++;
		}

		layer_indx++;
	}
}

void Tilemap_g_create(Tilemap_t* const tilemap,render_manager_t* const render_mngr)
{
	uint32_t layer_indx = 0;
	uint32_t layer_count = 0;


	while(layer_count <  tilemap->layer_hash.num_elem)
	{
		if(tilemap->layer_hash.keys[layer_indx])
		{
			tilemap->layer_array[layer_indx].VBOvertices = render_gen_buffer(render_mngr,tilemap->layer_array[layer_indx].vertices,tilemap->layer_array[layer_indx].verticeArraySize * sizeof(float),wfalse,wfalse);
			tilemap->layer_array[layer_indx].VBOtexcoord = render_gen_buffer(render_mngr,tilemap->layer_array[layer_indx].textureCoord,tilemap->layer_array[layer_indx].textureArraySize * sizeof(float),(wbool)tilemap->layer_array[layer_indx].animated,(wbool)tilemap->layer_array[layer_indx].animated);
			
			if(strlen(tilemap->layer_array[layer_indx].prog_name) != 0)
				tilemap->layer_array[layer_indx].program_id = render_get_program(render_mngr,tilemap->layer_array[layer_indx].prog_name);

			layer_count++;
		}

		layer_indx++;
	}
}

void tilemap_keepresources(Tilemap_t* const tilemap, resources_manager_t* const resx_mngr)
{
	uint32_t layer_indx = 0;
	uint32_t layer_count = 0;


	while (layer_count <  tilemap->layer_hash.num_elem)
	{
		if (tilemap->layer_hash.keys[layer_indx])
		{
			gettexture_resx(resx_mngr, tilemap->layer_array[layer_indx].textPointer->texture_name);

			layer_count++;
		}

		layer_indx++;
	}
}

wbool Tilemap_Draw(const Tilemap_t* const tilemap,int z_indx,render_manager_t* const render_mngr,Vector_t offset,light_manager_t* const light_manager,resources_manager_t* const resx_manager)
{
	if(z_indx > tilemap->max_zindex)
		return wfalse;

	uint32_t layer_indx = 0;
	uint32_t layer_count = 0;

	SHADERPROGRAM_ID old_prog = render_mngr->current_program;

	render_push_matrix(render_mngr);

	while(layer_count <  tilemap->layer_hash.num_elem)
	{
		if(tilemap->layer_hash.keys[layer_indx])
		{
			if(tilemap->layer_array[layer_indx].visible == wtrue && tilemap->layer_array[layer_indx].draw_order == z_indx)
			{
				if(tilemap->layer_array[layer_indx].program_id != render_get_emptyprogvalue() && old_prog != tilemap->layer_array[layer_indx].program_id)
					render_use_program(render_mngr,tilemap->layer_array[layer_indx].program_id);

				
				Vector_t offset_texture = vectorzero;

				if(tilemap->texturePointer->virtual_texture)
				{
					offset_texture = tilemap->texturePointer->region->position;
				}

				if(tilemap->layer_array[layer_indx].shader_parameters != NULL && tilemap->layer_array[layer_indx].num_parameters != 0)
				{
					bind_shaders_params(light_manager,render_mngr,tilemap->layer_array[layer_indx].shader_parameters,tilemap->layer_array[layer_indx].num_parameters,resx_manager,NULL,NULL,0.0f,offset_texture,NULL,0);
				}

				TilemapLayer_Draw(&tilemap->layer_array[layer_indx],render_mngr,offset);

				if(tilemap->layer_array[layer_indx].shader_parameters != NULL && tilemap->layer_array[layer_indx].num_parameters != 0)
					unbind_shaders_params(render_mngr,tilemap->layer_array[layer_indx].shader_parameters,tilemap->layer_array[layer_indx].num_parameters,resx_manager,light_manager);

				break;
			}

			layer_count++;

		}

		layer_indx++;
	}

	render_pop_matrix(render_mngr);
	render_use_program(render_mngr,old_prog);

	return wfalse;
}

//generate initial edge list
void Tilemap_gen_edge_list(Tilemap_t* const tilemap,wbool regen,check_col_t check_col_func)
{
	if (regen) {
		tilemap->edge_array = alloc_edges_list(tilemap->row_count, tilemap->col_count, tilemap->edge_array, &tilemap->edge_array_size, wtrue, &tilemap->preallocated_edges);
	}

	gen_edges_list(tilemap->row_count, tilemap->col_count, tilemap->tile_size, tilemap->collisions_array, tilemap->edge_array, &tilemap->preallocated_edges, check_col_func,vectorzero);
}

//TODO: optimisation
//pour optimiser la maj de collisions et enlever les realloc, ainsi que pour simplifier les limite pour le nombre de murs, on peut modifier la structure du tableau de collisions comme suit
//taille du tableau = numcol * numrow * nbsegment(4)
//chaque case du tableau contient soit l'adresse du segment de collision (m�me si le segment est pr�sent sur plusieurs case) soit null (pas de collision)
//ce qui permet de r�cup�rer les bon segment directement via la position de la tile
//pour la g�n�ration de l'�clairage, il faudrait ajouter la gestion de la valeur NULL, la gestion des segment contigu (pour �viter de prendre en compte le m�me segment plusieurs fois)
//et la limitation de la recherche par rapport au rayon de la lumi�re

//update neighbour edge given a tilepos
void Tilemap_update_edge(Tilemap_t* const tilemap,int32_t tilePos, wbool add_tile,const int32_t* const collisions_array)
{
	update_edges(tilemap->col_count, tilemap->row_count, tilemap->tile_size, tilemap->edge_array, tilemap->edge_array_size, collisions_array, tilePos, add_tile, tilemap->preallocated_edges,vectorzero);
}

void Tilemap_update(Tilemap_t* const tilemap,const float deltaTime,render_manager_t* const render_mngr)
{
	uint32_t layer_indx = 0;
	uint32_t layer_count = 0;

	while(layer_count <  tilemap->layer_hash.num_elem)
	{
		if(tilemap->layer_hash.keys[layer_indx])
		{
			if(tilemap->layer_array[layer_indx].animated)
				TilemapLayer_Update(&tilemap->layer_array[layer_indx],deltaTime,render_mngr);

			layer_count++;

		}

		layer_indx++;
	}
}

#ifndef STRIP_EDIT_CODE


static ColorA_t* Tilemap_editor_getcollisioncolor(Tilemap_t* const tilemap,int32_t collision_value)
{
	if(collision_value == COLLISION)
		return &tilemap->collision_color;
	else if(collision_value == COLLISION_SLOPE)
		return &tilemap->slope_color;
	else if(collision_value == COLLISION_SLOPE_INVERSE)
		return &tilemap->slope_inverse_color;
	else if(collision_value == COLLISION_DESTRUCTIBLE)
		return &tilemap->destructible_color;
	else if(collision_value == COLLISION_EDITABLE)
		return &tilemap->editable_color;
	else if(collision_value == COLLISION_EDITABLE_2)
		return &tilemap->editable_color_2;
	else if(collision_value == COLLISION_EDITABLE_3)
		return &tilemap->editable_color_3;
	else if(collision_value == COLLISION_KILL)
		return &tilemap->kill_color;

	return NULL;
}

void Tilemap_editor_changecollisionscolor_forrange(Tilemap_t* const tilemap, ColorA_t color, int32_t start_value,int32_t end_value, render_manager_t* const render_mngr)
{

	if (tilemap->editor_collisions_colors != NULL)
	{
		int32_t colors_i = 0;
		int32_t pos_collide = 0;

		ColorA_t empty_color = { 1.0,1.0,1.0,0.0 };

		for (int32_t i = 0; i < tilemap->row_count; i++)
		{
			for (int32_t j = 0; j < tilemap->col_count; j++)
			{
				pos_collide = j + (i * tilemap->col_count);

				//store colors for each vertices
				if (tilemap->collisions_array[pos_collide] >= start_value && tilemap->collisions_array[pos_collide] <= end_value)
				{


					for (int32_t vert = 0; vert < 6; vert++)
					{
						tilemap->editor_collisions_colors[colors_i++] = color.r;
						tilemap->editor_collisions_colors[colors_i++] = color.g;
						tilemap->editor_collisions_colors[colors_i++] = color.b;
						tilemap->editor_collisions_colors[colors_i++] = color.a;
					}
				}
				else
				{
					colors_i += 24;
				}

			}
		}

		size_t colors_sizes = ((tilemap->row_count * tilemap->col_count) * 24);


		render_update_buffer(render_mngr, tilemap->buffer_collisions_colors, tilemap->editor_collisions_colors, 0, colors_sizes * sizeof(float));
	}
}



void Tilemap_editor_changecollisions_color(Tilemap_t* const tilemap,float a,render_manager_t* const render_mngr)
{
	tilemap->collision_color.a = tilemap->destructible_color.a = tilemap->slope_color.a = tilemap->slope_inverse_color.a = tilemap->editable_color.a  =  tilemap->editable_color_2.a  = tilemap->editable_color_3.a = tilemap->kill_color.a = a;

	if(tilemap->editor_collisions_colors != NULL)
	{
		int32_t colors_i = 0;
		int32_t pos_collide = 0;

		ColorA_t empty_color = {1.0,1.0,1.0,0.0};

		for(int32_t i = 0; i < tilemap->row_count ;i++)
		{
			for(int32_t j = 0; j < tilemap->col_count;j++)
			{
				pos_collide = j + (i * tilemap->col_count); 
            
				//store colors for each vertices
				ColorA_t* selected_color = Tilemap_editor_getcollisioncolor(tilemap,tilemap->collisions_array[pos_collide]);

				if(selected_color == NULL)
					selected_color = &empty_color;


				for(int32_t vert = 0; vert < 6;vert++)
				{
					tilemap->editor_collisions_colors[colors_i++] = selected_color->r;
					tilemap->editor_collisions_colors[colors_i++] = selected_color->g;
					tilemap->editor_collisions_colors[colors_i++] = selected_color->b;
					tilemap->editor_collisions_colors[colors_i++] = selected_color->a;
				}
			}
		}

		size_t colors_sizes = ((tilemap->row_count * tilemap->col_count)  * 24) ;


		render_update_buffer(render_mngr,tilemap->buffer_collisions_colors,tilemap->editor_collisions_colors,0,colors_sizes * sizeof(float));
	}


}

void Tilemap_editor_collisions_Draw(Tilemap_t* const tilemap,render_manager_t* const render_mngr, Vector_t offset)
{
	if(tilemap->collisions_array == NULL)
		return;

	int32_t i = 0;
	int32_t j = 0;
    int32_t pos_collide = 0;

	int layer_indx = 0;
	uint32_t layer_count = 0;

	int backgroundLayer = -1;

	//get collisions layer
	while(layer_count <  tilemap->layer_hash.num_elem)
	{
		if(tilemap->layer_hash.keys[layer_indx])
		{

			layer_count++;

			if(backgroundLayer == -1)
			{
				backgroundLayer = layer_indx;
				break;
			}
		}

		layer_indx++;
	}



	if(backgroundLayer == -1)
		return;

	
	ColorA_t empty_color = {1.0,1.0,1.0,0.0};

	if(tilemap->editor_collisions_colors == NULL)
	{
		size_t colors_sizes = ((tilemap->row_count * tilemap->col_count)  * 24) ;

		tilemap->editor_collisions_colors = (float*)wf_malloc(colors_sizes * sizeof(float));
		memset(tilemap->editor_collisions_colors, 0, colors_sizes * sizeof(float));

		int32_t colors_i = 0;

		for(i = 0; i < tilemap->row_count ;i++)
		{
			for(j = 0; j < tilemap->col_count;j++)
			{
				pos_collide = j + (i * tilemap->col_count); 
            
				//store colors for each vertices
				ColorA_t* selected_color = Tilemap_editor_getcollisioncolor(tilemap,tilemap->collisions_array[pos_collide]);

				
				if(selected_color == NULL)
					selected_color = &empty_color;
				
				for(int32_t vert = 0; vert < 6;vert++)
				{
					tilemap->editor_collisions_colors[colors_i++] = selected_color->r;
					tilemap->editor_collisions_colors[colors_i++] = selected_color->g;
					tilemap->editor_collisions_colors[colors_i++] = selected_color->b;
					tilemap->editor_collisions_colors[colors_i++] = selected_color->a;
				}
			}
		}

		tilemap->buffer_collisions_colors = render_gen_buffer(render_mngr,tilemap->editor_collisions_colors,colors_sizes * sizeof(float),wfalse,wfalse);

	}

	if(tilemap->editor_collisions_colors != NULL)
	{
		render_push_matrix(render_mngr);

		Vector_t cpos = render_mngr->scroll_vector;

		int32_t scroll_x = (int32_t)(((cpos.x * 1.0f) - cpos.x) + offset.x);
		int32_t scroll_y = (int32_t)(((cpos.y * 1.0f) - cpos.y) + offset.y);

		cpos.x = (wfloat)scroll_x;
		cpos.y = (wfloat)scroll_y;

		render_set_position(render_mngr, cpos);

		render_start_draw_buffer(render_mngr,NULL,tilemap->layer_array[backgroundLayer].VBOvertices,NULL,tilemap->buffer_collisions_colors,2);

		render_draw_buffer(render_mngr,0,tilemap->row_count * tilemap->col_count * 6);

		render_end_draw_buffer(render_mngr,wfalse,wtrue);
	
	   render_pop_matrix(render_mngr);
	}

}

void Tilemap_editor_grid_draw(const Tilemap_t* const tilemap,const ColorA_t grid_color,render_manager_t* const render_mngr)
{

	int layer_indx = 0;
	uint32_t layer_count = 0;

	int backgroundLayer = -1;

	//get background layer
	while(layer_count <  tilemap->layer_hash.num_elem)
	{
		if(tilemap->layer_hash.keys[layer_indx])
		{

			layer_count++;

			if(backgroundLayer == -1)
			{
				backgroundLayer = layer_indx;
				break;
			}
		}

		layer_indx++;
	}
	

	if(backgroundLayer == -1)
		return;


	render_set_color(render_mngr,grid_color);

	render_start_draw_buffer(render_mngr,NULL,tilemap->layer_array[backgroundLayer].VBOvertices,NULL,NULL, 2);

	render_draw_line(render_mngr,0,tilemap->row_count * tilemap->col_count * 6);

	render_end_draw_buffer(render_mngr,wfalse,wfalse);
}

void Tilemap_editCollision(Tilemap_t* const tmap,int32_t tileId,int32_t tilePos,render_manager_t* const render_mngr)
{
	if(tmap->collisions_array == NULL)
		return;

	tmap->collisions_array[tilePos] = tileId;// ? 1 : 0;

	if(tmap->editor_collisions_colors != NULL)
	{
		ColorA_t empty_color = {1.0,1.0,1.0,0.0};

		ColorA_t* selected_color = Tilemap_editor_getcollisioncolor(tmap,tileId);

		if(selected_color == NULL)
			selected_color = &empty_color;

		int32_t start_update = (tilePos * 24);

		for(int32_t vert = 0; vert < 6;vert++)
		{
			tmap->editor_collisions_colors[start_update] = selected_color->r;
			tmap->editor_collisions_colors[start_update+1] = selected_color->g;
			tmap->editor_collisions_colors[start_update+2] = selected_color->b;
			tmap->editor_collisions_colors[start_update+3] = selected_color->a;

			start_update += 4;
		}

		render_update_buffer(render_mngr,tmap->buffer_collisions_colors,&tmap->editor_collisions_colors[(tilePos * 24)],(tilePos * 24) * sizeof(float),24 * sizeof(float));
	}

}

void Tilemap_editCollision_Colored(Tilemap_t* const tmap, int32_t tileId, int32_t tilePos,ColorA_t selected_color, render_manager_t* const render_mngr)
{
	if (tmap->collisions_array == NULL)
		return;

	tmap->collisions_array[tilePos] = tileId;// ? 1 : 0;

	if (tmap->editor_collisions_colors != NULL)
	{

		int32_t start_update = (tilePos * 24);

		for (int32_t vert = 0; vert < 6; vert++)
		{
			tmap->editor_collisions_colors[start_update] = selected_color.r;
			tmap->editor_collisions_colors[start_update + 1] = selected_color.g;
			tmap->editor_collisions_colors[start_update + 2] = selected_color.b;
			tmap->editor_collisions_colors[start_update + 3] = selected_color.a;

			start_update += 4;
		}

		render_update_buffer(render_mngr, tmap->buffer_collisions_colors, &tmap->editor_collisions_colors[(tilePos * 24)], (tilePos * 24) * sizeof(float), 24 * sizeof(float));
	}

}

#endif

void Tilemap_editTile(Tilemap_t* const tmap,int32_t tileId,int32_t tilePos,const char* layer_id,render_manager_t* const render_mngr,wbool edit_gpubuffer)
{
	int layer_indx = hashtable_index(&tmap->layer_hash, layer_id);
	TilemapLayer_editTile(&tmap->layer_array[layer_indx],tileId,tilePos,render_mngr,edit_gpubuffer);
}

void Tilemap_edit_animated_tile(Tilemap_t* const tmap,int32_t tileId,int32_t tilePos,char* layer_id,int32_t current_frame,render_manager_t* const render_mngr)
{
	int layer_indx = hashtable_index(&tmap->layer_hash, layer_id);
	TilemapLayer_edit_tile_animated(&tmap->layer_array[layer_indx],tileId,tilePos,current_frame,render_mngr);
}

void Tilemap_remove_animated_tile(Tilemap_t* const tmap,int32_t tilePos,char* layer_id,int32_t current_frame,render_manager_t* const render_mngr)
{
	int layer_indx = hashtable_index(&tmap->layer_hash, layer_id);
	TilemapLayer_remove_tile_animation(&tmap->layer_array[layer_indx],tilePos,current_frame,render_mngr);
}

void Tilemap_editor_changecollisionsvisibility(Tilemap_t* const tmap,wbool visible)
{
	tmap->show_collisions = visible;
}

void Tilemap_editor_changevisibility(Tilemap_t* const tmap,char* layer_id,wbool visible)
{
	int layer_indx = hashtable_index(&tmap->layer_hash, layer_id);
	tmap->layer_array[layer_indx].visible = visible;
}

void Tilemap_Free(Tilemap_t* const tilemap)
{

	if(tilemap->editor_collisions_colors != NULL)
	{
		wf_free(tilemap->editor_collisions_colors);
		render_delete_buffer(tilemap->buffer_collisions_colors);
		tilemap->editor_collisions_colors  = NULL;
	}

	uint32_t layer_indx = 0;
	uint32_t layer_count = 0;

	//cleanup layers
	while(layer_count <  tilemap->layer_hash.num_elem)
	{
		if(tilemap->layer_hash.keys[layer_indx])
		{
			//remove tilemaps
			TilemapLayer_Free(&tilemap->layer_array[layer_indx]);
			logprint("Layer %s freed!",tilemap->layer_hash.keys[layer_indx]);
			wf_free(tilemap->layer_hash.keys[layer_indx]);
			layer_count++;
		}

		layer_indx++;
	}

	wf_free(tilemap->layer_hash.keys);
	tilemap->layer_hash.keys = NULL;

	if(tilemap->collisions_array != NULL)
	{
		wf_free(tilemap->collisions_array);
		tilemap->collisions_array = NULL;
	}

	if(tilemap->edge_array != NULL)
	{
		wf_free(tilemap->edge_array);
		tilemap->edge_array = NULL;
		tilemap->edge_array_size = 0;
	}

	if (tilemap->preallocated_edges != NULL) {
		wf_free(tilemap->preallocated_edges);
		tilemap->preallocated_edges = NULL;
	}

	tilemap->layer_hash.num_elem = 0;
	tilemap->layer_hash.max_elem = 0;
}
