#include <inttypes.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sqlite3.h>
#include <wchar.h>

#include <Base/types.h>
#include <Render.h>
#include <Engine.h>
#include <Base/functions.h>

#include <Base/Color.h>
#include <Base/geom.h>

#include <Utils/Hash.h>

#include <Render/render_manager.h>

#include <Collisions/Collisions.h>

#include <Graphics/TextureLoader.h>

#include <GameObjects/Level.pb-c.h>

#include <GameObjects/Characters.pb-c.h>
#include <Graphics/Animation.h>
#include <Graphics/Sprite.h>

#include <Graphics/TextureRegion.h>
#include <Graphics/SpriteBatch.h>
#include <Graphics/FontLoader.h>

#include <Font/RenderText.h>

#include <Resx/localization.h>

#include <GameObjects/text_entity.h>

const char* const align_type[] = {"left","center","right"};
#define ALIGN_SIZE sizeof(align_type) / sizeof(char*)

static void text_entity_realign(text_entity_t* const text_obj,const Rect_t alignement_box)
{
	//based on alignement, reposition our text element
	switch(text_obj->align)
	{
		case font_center:
			text_obj->bbox.position.x = (alignement_box.position.x + (alignement_box.width * 0.5f)) - (text_obj->bbox.width * 0.5f);
			text_obj->orig_pos.x = text_obj->bbox.position.x;
			break;
		/*case font_left:
			text_obj->bbox.position.x = alignement_box.position.x;
			text_obj->orig_pos.x = text_obj->bbox.position.x;
			break;*/
		case font_right:
			text_obj->bbox.position.x = (alignement_box.position.x + alignement_box.width) - text_obj->bbox.width;
			text_obj->orig_pos.x = text_obj->bbox.position.x;
			break;
		case font_left:
		case font_none:
			break;
	}
}


void text_entity_init(text_entity_t* const text_obj,localization_t* const local_obj,TextObject* text_data,const char* map_id,char* text_id,const font_info* font_data,const Rect_t alignement_box,render_manager_t* const render_mngr)
{
	text_obj->align = (font_alignement)text_data->font_align;
	text_obj->z_order = text_data->z_order;
	text_obj->dynamic = wfalse;

	text_obj->bbox.position.x = (float)text_data->position->x;
	text_obj->bbox.position.y = (float)text_data->position->y;
	text_obj->orig_pos.x = (float)text_data->position->x;
	text_obj->orig_pos.y = (float)text_data->position->y;
	text_obj->orig_width = text_data->width;
	text_obj->orig_height = text_data->height;

	text_obj->parallax.x = text_data->parallax_x;
	text_obj->parallax.y = text_data->parallax_y;
	text_obj->no_cull = (wbool)text_data->no_culling;
	text_obj->no_translation = (wbool)text_data->no_translation;
	text_obj->fontsize = (uint8_t)text_data->font_size;
	text_obj->charreplacer = 0;

	text_obj->entity_parent = 0;


	//translate the content
	if(!text_data->no_translation)
		text_obj->text_content = localization_getstring(local_obj,text_data->text_content);
	else
	{
		size_t text_len = (strlen(text_data->text_content) + 1);
		text_obj->text_content = (wchar_t*)wf_malloc(text_len * sizeof(wchar_t));
		size_t convertion = 0;
		a_mbstowcs(&convertion,text_obj->text_content,text_len,text_data->text_content,text_len);
	}

	//choose a size for our text
	int32_t batch_size = a_wcslen(text_obj->text_content);
	if(batch_size < 100)
		batch_size = 100;
	else
		batch_size *= 2;


	Render_Text_Init(&text_obj->font_object,batch_size,render_mngr);
	Render_Text_Setinfo(&text_obj->font_object,font_data);

	Render_Text_setfixedsize(&text_obj->font_object,(wbool)(text_data->has_fixed_size && text_data->fixed_size));

	text_obj->original_content = (char*)wf_malloc((strlen(text_data->text_content) + 1) * sizeof(char));
	w_strcpy(text_obj->original_content,(strlen(text_data->text_content) + 1),text_data->text_content);

	text_obj->bbox.width = (uint32_t)Render_Text_strlen(&text_obj->font_object,text_obj->text_content);

	text_obj->bbox.height = (uint32_t)Render_Text_charheight(&text_obj->font_object);

	text_obj->diff_width = text_data->width - text_obj->bbox.width;

	if(text_obj->diff_width < 0)
		text_obj->diff_width = 0;

	//resize the box if needed
	if(text_data->width > (int32_t)text_obj->bbox.width)
		text_obj->bbox.width = text_data->width;

	text_obj->diff_height= text_data->height - text_obj->bbox.height;

	if(text_obj->diff_height < 0)
		text_obj->diff_height = 0;

	if(text_data->height >(int32_t)text_obj->bbox.height)
		text_obj->bbox.height = text_data->height;

	text_obj->has_sprite = wfalse;

	text_entity_realign(text_obj,alignement_box);


	w_strcpy(text_obj->ref_map_id,256,map_id);

	text_obj->text_id = text_id;


	text_obj->original_color.r = text_obj->color.r = (text_data->text_color->r != 0) ? (float)(text_data->text_color->r / 255.0f) : 0.0f;
	text_obj->original_color.g = text_obj->color.g = (text_data->text_color->g != 0) ? (float)(text_data->text_color->g / 255.0f) : 0.0f;
	text_obj->original_color.b = text_obj->color.b = (text_data->text_color->b != 0) ? (float)(text_data->text_color->b / 255.0f) : 0.0f;
	text_obj->original_color.a = text_obj->color.a = (text_data->text_color->a != 0) ? (float)(text_data->text_color->a / 255.0f) : 0.0f;

	

	if(text_data->has_callback)
	{

		text_obj->callback_hash = (hashtable_t*)wf_malloc(sizeof(hashtable_t));
		text_obj->callback_keys = (char**)wf_calloc(TEXT_MAX_CALLBACK,sizeof(char*));
		text_obj->callback_hash->keys = text_obj->callback_keys;
		text_obj->callback_hash->max_elem = TEXT_MAX_CALLBACK;
		text_obj->callback_hash->num_elem = 0;

		text_obj->callback_scriptfuncs = (char**)wf_calloc(TEXT_MAX_CALLBACK,sizeof(char*));
	}

	text_obj->is_visible = wtrue;
}

void text_entity_updaterenderbox(text_entity_t* const text_obj)
{
	text_obj->bbox.position.x = text_obj->orig_pos.x;
	text_obj->bbox.position.y = text_obj->orig_pos.y;

	text_obj->bbox.width = (int16_t)Render_Text_strlen(&text_obj->font_object,text_obj->text_content);
	text_obj->bbox.height = (int16_t)Render_Text_charheight(&text_obj->font_object);
}

void text_entity_updatecontent(text_entity_t* const text_obj,localization_t* const local_obj,const char* new_content,const Rect_t alignement_box,const wbool updateoriginalcontent)
{
	wf_free(text_obj->text_content);

	if (updateoriginalcontent) {
		text_obj->original_content = (char*)wf_realloc(text_obj->original_content, (strlen(new_content) + 1) * sizeof(char*));
		w_strcpy(text_obj->original_content, (strlen(new_content) + 1), new_content);
	}
	

	if(!text_obj->no_translation)
		text_obj->text_content = localization_getstring(local_obj,new_content);
	else
	{
		size_t content_len = (strlen(new_content) + 1);
		text_obj->text_content = (wchar_t*)wf_malloc(content_len * sizeof(wchar_t));
		size_t convertion = 0;
		a_mbstowcs(&convertion,text_obj->text_content, content_len,new_content, content_len);
	}

	text_obj->bbox.width = (int16_t)Render_Text_strlen(&text_obj->font_object,text_obj->text_content);

	text_obj->diff_width = text_obj->orig_width - text_obj->bbox.width;

	if(text_obj->diff_width < 0)
		text_obj->diff_width = 0;

	//resize the box if needed
	if(text_obj->orig_width > text_obj->bbox.width)
		text_obj->bbox.width = text_obj->orig_width;

	text_entity_realign(text_obj,alignement_box);
}

void text_entity_resetcontent(text_entity_t* const text_obj,localization_t* const local_obj,const Rect_t alignement_box)
{
	wf_free(text_obj->text_content);

	if(!text_obj->no_translation)
		text_obj->text_content = localization_getstring(local_obj,text_obj->original_content);
	else
	{
		size_t content_len = (strlen(text_obj->original_content) + 1);
		text_obj->text_content = (wchar_t*)wf_malloc(content_len * sizeof(wchar_t));
		size_t converted_char = 0;
		a_mbstowcs(&converted_char,text_obj->text_content, content_len, text_obj->original_content, strlen(text_obj->original_content) + 1);
	}

	text_obj->bbox.width = (int16_t)Render_Text_strlen(&text_obj->font_object,text_obj->text_content);

	text_obj->diff_width = text_obj->orig_width - text_obj->bbox.width;

	if(text_obj->diff_width < 0)
		text_obj->diff_width = 0;

	//resize the box if needed
	if(text_obj->orig_width > text_obj->bbox.width)
		text_obj->bbox.width = text_obj->orig_width;

	text_entity_realign(text_obj,alignement_box);
}

void text_entity_appendcontent(text_entity_t* const text_obj,localization_t* const local_obj,const char* new_content,const Rect_t alignement_box)
{
	if(text_obj->no_translation)
	{
		text_entity_append_raw(text_obj,new_content,alignement_box);
		return;
	}

	wchar_t* new_ptr = localization_appendstring(local_obj,text_obj->text_content,new_content);

	if(new_ptr != NULL)
	{
		text_obj->text_content = new_ptr;
		text_obj->bbox.width = (int16_t)Render_Text_strlen(&text_obj->font_object,text_obj->text_content);

		text_obj->diff_width = text_obj->orig_width - text_obj->bbox.width;

		if(text_obj->diff_width < 0)
			text_obj->diff_width = 0;

		//resize the box if needed
		if(text_obj->orig_width > text_obj->bbox.width)
			text_obj->bbox.width = text_obj->orig_width;
	}

	text_entity_realign(text_obj,alignement_box);
}

void text_entity_append_raw(text_entity_t* const text_obj,const char* raw_content,const Rect_t alignement_box)
{
	size_t old_len = wcslen(text_obj->text_content);
	size_t new_len = (old_len * sizeof(wchar_t)) + (strlen(raw_content) * sizeof(wchar_t)) + sizeof(wchar_t);
	text_obj->text_content = (wchar_t*)wf_realloc(text_obj->text_content,new_len);
	text_obj->bbox.width = (int16_t)Render_Text_strlen(&text_obj->font_object,text_obj->text_content);

	text_obj->diff_width = text_obj->orig_width - text_obj->bbox.width;

	if(text_obj->diff_width < 0)
		text_obj->diff_width = 0;

	//resize the box if needed
	if(text_obj->orig_width > text_obj->bbox.width)
		text_obj->bbox.width = text_obj->orig_width;
	
	wchar_t* append_start = &text_obj->text_content[old_len];
	size_t content_len = strlen(raw_content) + 1;
	size_t converted = 0;
	a_mbstowcs(&converted,append_start,content_len,raw_content,content_len);

	text_entity_realign(text_obj,alignement_box);
}


void text_entity_addcallback(text_entity_t* const oentity, const char* func_id,const char* script_func,void  (*callback_func)(const char* id,const char* func, uint32_t parent,const char* params), uint32_t parent)
{
	char* ptr = (char*)wf_malloc(sizeof(char) * (strlen(func_id)+1));
	w_strcpy(ptr,(strlen(func_id)+1),func_id);
	int pos = hashtable_pushkey(oentity->callback_hash,ptr);


	char* ptr2 = (char*)wf_malloc(sizeof(char) * (strlen(script_func)+1));
	w_strcpy(ptr2,(strlen(script_func)+1),script_func);

	oentity->callback_scriptfuncs[pos] = ptr2;
	oentity->callback_func = callback_func;

	if (parent != 0)
		oentity->entity_parent = parent;
}

void text_entity_removecallback(text_entity_t* const oentity,const char* func_id)
{
	int indx =  hashtable_index(oentity->callback_hash,func_id);

	wf_free(oentity->callback_scriptfuncs[indx]);
	oentity->callback_scriptfuncs[indx] = NULL;

	hashtable_removekey(oentity->callback_hash,func_id);

	wf_free(oentity->callback_keys[indx]);
	oentity->callback_keys[indx] = NULL;


}

void text_entity_clearcallback(text_entity_t* const oentity)
{
	if(oentity->callback_hash == NULL)
	{
		return;
	}

	for (int callback_indx = 0; callback_indx < TEXT_MAX_CALLBACK;callback_indx++)
	{
		if(oentity->callback_keys[callback_indx] != NULL)
		{
			wf_free(oentity->callback_scriptfuncs[callback_indx]);
			oentity->callback_scriptfuncs[callback_indx] = NULL;

			wf_free(oentity->callback_keys[callback_indx]);
			oentity->callback_keys[callback_indx] = NULL;
			
		}
	}

	oentity->callback_hash->num_elem = 0;


}

void text_entity_setposition(text_entity_t* const oentity,float x,float y)
{
	oentity->orig_pos.x = x;
	oentity->orig_pos.y = y;
}

Vector_t text_entity_getposition(text_entity_t* const oentity)
{
	return oentity->bbox.position;
}

void text_entity_setrandomhide(text_entity_t* const oentity, uint32_t hidechance, int charreplacer)
{
	oentity->hidechance = hidechance;
	oentity->charreplacer = charreplacer;
}

void text_entity_callback(text_entity_t* const oentity,const char* callback,const char* params)
{
	if(oentity->callback_func != NULL && hashtable_haskey(oentity->callback_hash,callback))
		oentity->callback_func(oentity->text_id,oentity->callback_scriptfuncs[hashtable_index(oentity->callback_hash,callback)],oentity->entity_parent,params);
}

wbool text_entity_checkrender(const text_entity_t* const oentity,const Rect_t screen_info)
{
	if(oentity->no_cull)
		return wtrue;

	return col_check2(&screen_info,&oentity->bbox);
}

void text_entity_draw(text_entity_t* const text_obj,render_manager_t* const render_mngr,Vector_t offset)
{
	if(!text_obj->is_visible)
		return;

	SHADERPROGRAM_ID current_prog = render_mngr->current_program;

	render_use_program(render_mngr, render_get_program(render_mngr, "prog_font"));

	text_obj->bbox.position.x = ((-render_mngr->scroll_vector.x) + text_obj->orig_pos.x) -  (text_obj->parallax.x * -render_mngr->scroll_vector.x);

	text_obj->bbox.position.y = (text_obj->orig_pos.y + (-render_mngr->scroll_vector.y)) -  (text_obj->parallax.y * -render_mngr->scroll_vector.y);
	
	render_setuniform_float(render_mngr, "pxRange", (float)text_obj->font_object.info->distanceRange);

	Render_Text_begin(&text_obj->font_object,text_obj->color.r,text_obj->color.g,text_obj->color.b,text_obj->color.a,render_mngr);
	Render_Text_Draw(&text_obj->font_object,text_obj->text_content,text_obj->bbox.position.x + offset.x + (text_obj->diff_width * 0.5f),(text_obj->bbox.position.y + offset.y) - (text_obj->diff_height * 0.5f), text_obj->fontsize,render_mngr);

	Render_Text_end(&text_obj->font_object,render_mngr);

	render_use_program(render_mngr, current_prog);
}



void text_entity_free(text_entity_t* text_obj)
{
	if(text_obj->callback_keys != NULL)
	{
		for (int callback_indx = 0; callback_indx < TEXT_MAX_CALLBACK;callback_indx++)
		{
			if(text_obj->callback_keys[callback_indx] != NULL)
			{
				wf_free(text_obj->callback_keys[callback_indx]);
				text_obj->callback_keys[callback_indx] = NULL;
				wf_free(text_obj->callback_scriptfuncs[callback_indx]);
				text_obj->callback_scriptfuncs[callback_indx] = NULL;
			}
		}

		wf_free(text_obj->callback_keys);
	}

	if(text_obj->callback_hash)
		wf_free(text_obj->callback_hash);

	if(text_obj->callback_scriptfuncs)
		wf_free(text_obj->callback_scriptfuncs);
	

	text_obj->callback_keys = NULL;
	text_obj->callback_hash = NULL;
	text_obj->callback_scriptfuncs = NULL;
	text_obj->callback_func = NULL;

	if(text_obj->text_content)
		wf_free(text_obj->text_content);

	text_obj->text_content = NULL;

	if(text_obj->original_content)
		wf_free(text_obj->original_content);

	text_obj->original_content = NULL;

	Render_Text_Free(&text_obj->font_object);
	wf_free(text_obj->text_content);
}
