#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <stdlib.h>
#include <inttypes.h>
#include <math.h>

#include <Render.h>

#include <Engine.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>
#include <Base/geom.h>

#include <Render/render_manager.h>

#include <Debug/Logprint.h>

#include <Utils/Hash.h>


#include <Debug/geomdebug.h>


#include <Graphics/TextureLoader.h>

#include <GameObjects/Level.pb-c.h>
#include <Collisions/Collisions.h>

#include <chipmunk/chipmunk.h>
#include <Physics/Physics.h>

#include <GameObjects/Characters.pb-c.h>
#include <Graphics/Animation.h>
#include <Graphics/Sprite.h>
#include <Graphics/Mesh.h>
#include <Graphics/TextureRegion.h>
#include <Graphics/SpriteBatch.h>

#include <GameObjects/entity.h>
#include <GameObjects/entity_group.h>


void entity_group_init(entity_group_t* const entity_group,const char* group_id,size_t max_num_entities,int16_t zindex)
{
	entity_group->linked_entities = (sprite_t**)wf_malloc(max_num_entities * sizeof(entity_t*));
	memset(entity_group->linked_entities,0,max_num_entities * sizeof(sprite_t*));

	w_strcpy(entity_group->group_id,128,group_id);

	entity_group->num_entities = 0;
	entity_group->sprite_batch_iscreated = wfalse;
	entity_group->zindex = zindex;

	memset(&entity_group->render_box,0,sizeof(Rect_t));
}

void entity_group_addentity(entity_group_t* const entity_group,sprite_t* const new_sprite)
{
	entity_group->linked_entities[entity_group->num_entities++] = new_sprite;

	col_merge(&entity_group->render_box,&new_sprite->render_box);
}

void entity_group_createspritebatch(entity_group_t* const entity_group,render_manager_t* const render_manager,const texture_info* texture)
{
	//count the number of sprites
	int32_t num_sprite = 0;

	for(int32_t i = 0; i < entity_group->num_entities;i++)
		num_sprite += (entity_group->linked_entities[i]->repeat_x + 1) * (entity_group->linked_entities[i]->repeat_y + 1);

	SpriteBatch_Init(&entity_group->sprite_batch,num_sprite,render_manager);

	for(int32_t i = 0; i < entity_group->num_entities;i++)
	{
		SpriteBatch_AddSprite2(&entity_group->sprite_batch,entity_group->linked_entities[i]->render_box.position.x + (entity_group->linked_entities[i]->render_box.width * 0.5f),entity_group->linked_entities[i]->render_box.position.y - (entity_group->linked_entities[i]->render_box.height * 0.5f),entity_group->linked_entities[i],render_manager);
	}

	//#ifdef ANDROID_BUILD
		SpriteBatch_scheduleupdatebuffer(&entity_group->sprite_batch, BUFFER_UPDATE_VERTICES | BUFFER_UPDATE_TEXCOORDS);
//	#else
	//	SpriteBatch_updatebuffer_fromdirty(&entity_group->sprite_batch,render_manager,BUFFER_UPDATE_TEXCOORDS | BUFFER_UPDATE_VERTICES);
	//#endif

	entity_group->texture = texture;
	entity_group->sprite_batch_iscreated = wtrue;
}

wbool entity_group_checkrender(entity_group_t* const entity_group,const Rect_t screen_info)
{
	for(int32_t i = 0; i < entity_group->num_entities;i++)
	{
		if(col_check2(&entity_group->linked_entities[i]->render_box,&screen_info))
		{
			return wtrue;
		}
	}

	return wfalse;
	//return col_check2(&screen_info,&entity_group->render_box);
}

void entity_group_draw(entity_group_t* const entity_group,render_manager_t* const render_mngr,wbool visibility_test,Vector_t offset)
{
	if(!entity_group->sprite_batch_iscreated)
	{
		logprint("Can't draw an entity group with no sprite batch initialized! group:%s",entity_group->group_id);
		return;
	}

	entity_group->sprite_batch.position = offset;

	SpriteBatch_draw(&entity_group->sprite_batch,render_mngr,entity_group->texture->texture_pointer);

	Rect_t screen_info = render_mngr->screen_info;

	if(render_mngr->zoom_factor != 1.0f)
	{
		screen_info.width = (int16_t)(screen_info.width * render_mngr->zoom_factor);
		screen_info.height = (int16_t)(screen_info.height * render_mngr->zoom_factor);
	}

	//check which sprite need update
	int32_t indx_sprite = 0;
	wbool update_buffer = wfalse;
	for(int32_t i = 0; i < entity_group->num_entities;i++)
	{
		if(entity_group->linked_entities[i]->tex_coord_needupdate)
		{
			//update only if the entity is visible or in the update offset range
			Rect_t render_box = entity_group->linked_entities[i]->render_box;

			render_box.position.x -= GROUP_RENDER_UPDATE_OFFSET;
			render_box.position.y += GROUP_RENDER_UPDATE_OFFSET;
			render_box.width += GROUP_RENDER_UPDATE_OFFSET;
			render_box.height += GROUP_RENDER_UPDATE_OFFSET;

			if(col_check2(&render_box,&screen_info) || !visibility_test)
			{

				sprite_gentexturesvbo(entity_group->linked_entities[i],render_mngr);

				SpriteBatch_updateSprite2(&entity_group->sprite_batch,indx_sprite,entity_group->linked_entities[i]->render_box.position.x,entity_group->linked_entities[i]->render_box.position.y,entity_group->linked_entities[i],render_mngr);
				update_buffer = wtrue;
			}
		}

		indx_sprite += (entity_group->linked_entities[i]->repeat_x + 1) * (entity_group->linked_entities[i]->repeat_y + 1);
	}

	if(update_buffer)
	{
		//#ifdef ANDROID_BUILD
			SpriteBatch_scheduleupdatebuffer(&entity_group->sprite_batch, BUFFER_UPDATE_VERTICES | BUFFER_UPDATE_TEXCOORDS);
		//#else
		//	SpriteBatch_updatebuffer_fromdirty(&entity_group->sprite_batch,render_mngr,BUFFER_UPDATE_TEXCOORDS);
	//	#endif
	}
}


void entity_group_free(entity_group_t* const entity_group)
{
	wf_free(entity_group->linked_entities);

	if(entity_group->sprite_batch_iscreated)
		SpriteBatch_Free(&entity_group->sprite_batch);
}