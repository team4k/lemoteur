#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <stdlib.h>
#include <inttypes.h>
#define __USE_MISC 1
#define _USE_MATH_DEFINES 1
#include <math.h>

#include <Render.h>

#include <Engine.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>
#include <Base/geom.h>

#include <Render/render_manager.h>

#include <Debug/Logprint.h>

#include <Utils/Hash.h>


#include <Debug/geomdebug.h>


#include <Graphics/TextureLoader.h>

#include <GameObjects/Level.pb-c.h>
#include <Collisions/Collisions.h>

#include <chipmunk/chipmunk.h>
#include <Physics/Physics.h>


#include <GameObjects/Characters.pb-c.h>
#include <Graphics/Animation.h>
#include <Graphics/Mesh.h>
#include <Graphics/Sprite.h>
#include <GameObjects/entity.h>

#include <GameObjects/waypoint.h>

void waypoint_init(waypoint_t* const waypoint, Waypoint* data, const char* map_id)
{
	waypoint->position = Vec((wfloat)data->position->x, (wfloat)data->position->y);
	w_strcpy(waypoint->ref_map_id, 256, map_id);
}

Vector_t waypoint_getpos(waypoint_t* const waypoint)
{
	return waypoint->position;
}

void waypoint_setpos(waypoint_t* const waypoint,wfloat x,wfloat y)
{
	waypoint->position.x = x;
	waypoint->position.y = y;
}


