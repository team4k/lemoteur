#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <stdlib.h>
#include <inttypes.h>
#define __USE_MISC 1
#define _USE_MATH_DEFINES 1
#include <math.h>

#include <Render.h>
#include <Engine.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>
#include <Base/geom.h>
#include <Render/render_manager.h>
#include <Graphics/geomdraw.h>

#include <chipmunk/chipmunk.h>

#include <Physics/Physics.h>
#include <GameObjects/Level.pb-c.h>

#include <GameObjects/Collider.h>

void collider_init(collider_t* ocollider, const Collider* const  collider_def, physics_manager_t* physicsWorld,const char* map_id,char* const keyptr)
{
	cpBody* colliderbody = cpBodyNewStatic();

	float halfwidth = collider_def->width * 0.5f;
	float halfheight = collider_def->height * 0.5f;
	w_strcpy(ocollider->ref_map_id, 256, map_id);


	ocollider->keyptr = keyptr;
	ocollider->bbox = cpBBNew(-halfwidth,-halfheight,halfwidth,halfheight);

	ocollider->body = cpSpaceAddBody(physicsWorld->world, colliderbody);
	
	ocollider->shape = cpBoxShapeNew2(ocollider->body, ocollider->bbox, WFLOAT1);

	ocollider->render_box.width = collider_def->width;
	ocollider->render_box.height = collider_def->height;


	cpSpaceAddShape(physicsWorld->world, ocollider->shape);
	cpBodySetUserData(ocollider->body, ocollider);
	
	cpShapeSetCollisionType(ocollider->shape, collider_def->collision_type);

	cpShapeFilter filter = cpShapeFilterNew(CP_NO_GROUP, collider_def->collision_layer, collider_def->collision_mask);

	cpShapeSetFilter(ocollider->shape, filter);
	cpBodySetPosition(ocollider->body, cpv(collider_def->position->x, collider_def->position->y));

	ocollider->render_box.position.x = (wfloat)(ocollider->bbox.l + collider_def->position->x);
	ocollider->render_box.position.y = (wfloat)(ocollider->bbox.t + collider_def->position->y);

	cpShapeSetElasticity(ocollider->shape, 1.0f);
	cpShapeSetFriction(ocollider->shape, 0.0f);

	ocollider->in_physics_simulation = wtrue;

}

void collider_setpos(collider_t* ocollider, wfloat x,wfloat y)
{
	cpBodySetPosition(ocollider->body, cpv(x, y));
	ocollider->render_box.position.x = (wfloat)(ocollider->bbox.l +x);
	ocollider->render_box.position.y = (wfloat)(ocollider->bbox.t + y);
}

Vector_t collider_getpos(collider_t* ocollider)
{
	cpVect pos = cpBodyGetPosition(ocollider->body);

	return Vec((wfloat)pos.x, (wfloat)pos.y);
}

Rect_t collider_getbox(collider_t* ocollider)
{
	return ocollider->render_box;
}

void collider_setparameters(collider_t* ocollider, wfloat elasticity, wfloat friction)
{
	cpShapeSetElasticity(ocollider->shape, 1.0f);
	cpShapeSetFriction(ocollider->shape, 0.0f);
}

void collider_draw(collider_t* const ocollider, render_manager_t* const render_mngr)
{
	ColorA_t collider_color = { 1.0f,0.65f,0.28f,0.3f };
	drawrect(&ocollider->render_box, collider_color, render_mngr);
}

void collider_updaterenderbox(collider_t* const ocollider, int16_t width, int16_t height)
{
	ocollider->render_box.width = width;
	ocollider->render_box.height = height;
}

void collider_addtospace(collider_t* const ocollider, physics_manager_t* const physicsWorld)
{
	cpSpaceAddShape(physicsWorld->world, ocollider->shape);
	cpSpaceAddBody(physicsWorld->world, ocollider->body);
	ocollider->in_physics_simulation = wtrue;
}

void collider_removefromspace(collider_t* const ocollider, physics_manager_t* const physicsWorld)
{
	cpSpaceRemoveShape(physicsWorld->world, ocollider->shape);
	cpSpaceRemoveBody(physicsWorld->world, ocollider->body);
	ocollider->in_physics_simulation = wfalse;
}

void collider_free(collider_t* ocollider, physics_manager_t* const physicsWorld)
{
	if (ocollider->in_physics_simulation)
	{
		cpSpaceRemoveShape(physicsWorld->world, ocollider->shape);
		cpSpaceRemoveBody(physicsWorld->world, ocollider->body);
	}

	cpShapeFree(ocollider->shape);
	cpBodyFree(ocollider->body);

	ocollider->shape = NULL;
	ocollider->body = NULL;
	ocollider->keyptr = NULL;
}