
#include <time.h>
#include <GameObjects/level_stream.h>
#include <GameObjects/level_stream_chunks.h>
#include <GameObjects/level_stream_dynamic.h>

//save the world entities into the database 
uint32_t level_stream_world_entities_save(level_stream_t* const level)
{
	volatile uint32_t world_state = 0;

	atomic_set_value(&world_state, level->world_entity_pool_state);

	uint32_t o_index = 0;
	uint32_t o_count = 0;

	if (world_state == S_EMPTY)
	{

		atomic_set_value(&level->world_entity_pool_state, S_SAVING);

		for (entity_t* c_entity = (entity_t*)hashmap_iterate(&level->entities, &o_index, &o_count); c_entity != NULL; c_entity = (entity_t*)hashmap_iterate(&level->entities, &o_index, &o_count))
		{
			if (strlen(c_entity->source) > 0)
			{
				level_stream_save_world_entity(level, c_entity,NULL,NULL,0,wfalse);
			}

		}

		atomic_set_value(&level->world_entity_pool_state, S_SAVED);
		world_state = S_SAVED;
	}

	return world_state;
}

void level_stream_getworldentitydbpos(level_stream_t* const level, const char* id, int64_t* out_xpos, int64_t* out_ypos)
{
	if (level->chunks_db == NULL || !level->chunks_db_ready)
	{
		logprint("Error while loading world entities, chunks db not loaded or not ready");
		return;
	}

	char select_string[512];
	int sqlite_error = 0;

	w_sprintf(select_string, 512, "SELECT Xpos,Ypos FROM world_entities WHERE id = '%s'", id);

	sqlite3_stmt* statement;

	sqlite_error = sqlite3_prepare_v2(level->chunks_db, select_string, -1, &statement, NULL);

	if (sqlite_error)
	{
		logprint("Can't prepare statement %s, sqlite error %s", select_string, sqlite3_errmsg(level->chunks_db));
		return;
	}

	sqlite_error = sqlite3_step(statement);

	if (sqlite_error != SQLITE_ROW)
	{
		sqlite3_finalize(statement);
		logprint("No row found for entity %s", id);
		return;
	}

	//fetch data from database
	(*out_xpos) = sqlite3_column_int64(statement, 0);
	(*out_ypos) = sqlite3_column_int64(statement, 1);

	sqlite3_finalize(statement);
}

wbool level_stream_has_worldentitydb(level_stream_t* const level, const char* id)
{
	if (level->chunks_db == NULL || !level->chunks_db_ready)
	{
		logprint("Error while loading world entities, chunks db not loaded or not ready");
		return wfalse;
	}

	char select_string[512];
	int sqlite_error = 0;

	w_sprintf(select_string, 512, "SELECT count(*) FROM world_entities WHERE id = '%s'", id);

	sqlite3_stmt* statement;

	sqlite_error = sqlite3_prepare_v2(level->chunks_db, select_string, -1, &statement, NULL);

	if (sqlite_error)
	{
		logprint("Can't prepare statement %s, sqlite error %s", select_string, sqlite3_errmsg(level->chunks_db));
		return wfalse;
	}

	sqlite_error = sqlite3_step(statement);

	if (sqlite_error != SQLITE_ROW)
	{
		sqlite3_finalize(statement);
		return wfalse;
	}

	//fetch data from database

	int count = sqlite3_column_int(statement, 0);

	sqlite3_finalize(statement);


	if (count == 1) {
		return wtrue;
	}

	if (count > 1) {
		logprint("More than one entity with the id %s", id);
	}

	return wfalse;
}


//load into temporary world entities circular array the world entities in range in the database
//load only the entities that aren't already loaded
//TODO optimize loading, one array with entities from DB and load everything in one go
void level_stream_load_world_entities_inrange(level_stream_t* const level,wbool simul)
{
	uwint xminpos = 0, xmaxpos = 0, yminpos = 0, ymaxpos = 0;

	if (simul)
	{
		xminpos = level->access_chunk[O_BOTTOMLEFT]->bounds.position.x;
		xmaxpos = xminpos + (level->base_width * CHUNK_COL);
		yminpos = level->access_chunk[O_BOTTOMLEFT]->bounds.position.y;
		ymaxpos = yminpos + (level->base_height * CHUNK_ROW);

		xminpos += (level->tile_size * 2);
		xmaxpos -= (level->tile_size * 2);
		yminpos += (level->tile_size * 2);
		ymaxpos -= (level->tile_size * 2);
	}

	atomic_set_value(&level->world_entity_pool_state, S_LOADING);


	if (level->chunks_db_background == NULL || !level->chunks_db_ready)
	{
		logprint("Error while loading world entities, chunks db not loaded or not ready");
		atomic_set_value(&level->world_entity_pool_state, S_EMPTY);
		return;
	}

	char select_string[1536] = { 0 };
	int sqlite_error = 0;

	if (simul)
	{
		w_sprintf(select_string, 1536, "SELECT id,frag_file,source_id,script_ctor,Xpos,Ypos,simuldata FROM world_entities WHERE Xpos >= %lld AND Xpos <=%lld AND Ypos >= %lld AND Ypos <= %lld AND simul = true",(uint64_t)xminpos, (uint64_t)xmaxpos, (uint64_t)yminpos, (uint64_t)ymaxpos, convertbooltostring(simul));
	}
	else {
		//tout ce qui n'est pas simul� est forc�ment dans l'espace du jeu
		w_sprintf(select_string, 1536, "SELECT id,frag_file,source_id,script_ctor,Xpos,Ypos,simuldata FROM world_entities WHERE simul = false", convertbooltostring(simul));
	}
	

	sqlite3_stmt* statement;

	sqlite_error = sqlite3_prepare_v2(level->chunks_db_background, select_string, -1, &statement, NULL);

	if (sqlite_error)
	{
		logprint("Can't prepare statement %s, sqlite error %s", select_string, sqlite3_errmsg(level->chunks_db_background));
		atomic_set_value(&level->world_entity_pool_state, S_EMPTY);
		return;
	}

	do
	{
		sqlite_error = sqlite3_step(statement);

		if (sqlite_error != SQLITE_ROW)
		{
			break;
		}

		//fetch data from database
		const char* id = (const char*)sqlite3_column_text(statement, 0);
		const char* frag_file = (const char*)sqlite3_column_text(statement, 1);
		const char* source_id = (const char*)sqlite3_column_text(statement, 2);
		const char* script_ctor = (const char*)sqlite3_column_text(statement, 3);

		int64_t posX = sqlite3_column_int64(statement, 4);
		int64_t posY = sqlite3_column_int64(statement, 5);

		const void* binary_data = sqlite3_column_blob(statement, 6);
		size_t binary_data_size = sqlite3_column_bytes(statement, 6);

		ProtobufCAllocator temp_allocator;

		typedef void *(*tmpalloc)(void *allocator_data, size_t size);
		typedef void(*tmpfree)(void *allocator_data, void *pointer);

		temp_allocator.alloc = (tmpalloc)tempallocator_getmemory;
		temp_allocator.allocator_data = level->world_entity_pool;
		temp_allocator.free = (tmpfree)tempallocator_dummyfree;


		//get the source file
		Fragment* source = getfragment_copy_resx(level->resx_manager, frag_file, &temp_allocator);

		if (source == NULL)
		{
			logprint("Can't find source %s for entity %s, skipping...", frag_file, id);
			continue;
		}

		//get the entity into the source file
		Entity* source_entity = NULL;

		for (uint32_t ient = 0; ient < source->n_entity_array; ient++)
		{
			if (strcmp(source->entity_array[ient]->entity_id, source_id) == 0)
			{
				source_entity = source->entity_array[ient];
				source_entity->entity_id = tempallocator_string_alloc(level->world_entity_pool, id);
				source_entity->source = tempallocator_string_alloc(level->world_entity_pool, frag_file);
				source_entity->source_id = tempallocator_string_alloc(level->world_entity_pool, source_id);
				source_entity->position->x = (int32_t)posX;
				source_entity->position->y = (int32_t)posY;
				source_entity->script_ctor = tempallocator_string_alloc(level->world_entity_pool, script_ctor);

				if (binary_data_size > 0) {
					source_entity->has_simul_data = (protobuf_c_boolean)1;
					source_entity->simul_data.data = (uint8_t*)tempallocator_getmemory(level->world_entity_pool, binary_data_size);
					source_entity->simul_data.len = binary_data_size;
					w_memcpy(source_entity->simul_data.data, binary_data_size, binary_data, binary_data_size);
				}
				

				break;
			}
		}

		if (source_entity != NULL)
		{
			circular_array_push_new_element(&level->temp_world_entities, source_entity);
		}
		else
		{
			logprint("Can't find entity %s in source %s", source_id, frag_file);
		}


	} while (sqlite_error == SQLITE_ROW);

	sqlite3_finalize(statement);

	//update entity to remove simul flag

	char update_string[512] = { 0 };
	w_sprintf(update_string, 512, "UPDATE world_entities SET simul = false WHERE id = @id");

	wbool init_load = wtrue;
	uint32_t read_p = 0;
	Entity* world_ent = NULL;

	sqlite_error = sqlite3_prepare_v2(level->chunks_db_background, update_string, -1, &statement, NULL);

	do
	{
		world_ent = (Entity*)circular_array_iterate(&level->temp_world_entities, &read_p, init_load);
		init_load = wfalse;

		if (world_ent != NULL) {
			sqlite3_bind_text(statement, 1, world_ent->entity_id, -1, SQLITE_STATIC);

			sqlite_error = level_stream_handlelock(statement);

			if (sqlite_error != SQLITE_DONE)
			{
				logprint("Can't update world entity, sqlite error %s", sqlite3_errmsg(level->chunks_db_background));
			}

			sqlite3_reset(statement);
			sqlite3_clear_bindings(statement);
		}

	} while (world_ent != NULL);

	sqlite3_finalize(statement);

	atomic_set_value(&level->world_entity_pool_state, S_FRONTLOADING);
}

void level_stream_unload_world_entities_outofrange(level_stream_t* const mngr)
{
	atomic_set_value(&mngr->world_entity_pool_state, S_UNLOADING);

	if (mngr->chunks_db == NULL || !mngr->chunks_db_ready)
	{
		logprint("Error while unloading world entities, chunks db not loaded or not ready");
		atomic_set_value(&mngr->world_entity_pool_state, S_EMPTY);
		return;
	}

	volatile uint32_t chunk_state = S_EMPTY;
	volatile uint32_t center_chunk_state = S_EMPTY;

	atomic_set_value(&chunk_state, mngr->access_chunk[O_TOPLEFT]->state);
	atomic_set_value(&center_chunk_state, mngr->access_chunk[O_CENTER]->state);

	//pas de d�chargement tant que notre chunk en haut � gauche et notre chunk central ne sont pas charg�s
	if (chunk_state != S_LOADED && center_chunk_state != S_LOADED) {
		return;
	}


	//Rect
	Rect_t loadedworldbound = { 0 };
	//chunk rect position is on origine bottom left, change it to top left
	loadedworldbound.position = mngr->access_chunk[O_TOPLEFT]->bounds.position;
	loadedworldbound.position.y += mngr->base_height;
	loadedworldbound.width = (mngr->base_width * mngr->world_col_limit);
	loadedworldbound.height = (mngr->base_height * mngr->world_row_limit);

	uint32_t o_index = 0, o_count = 0;

	const char* log_return = NULL;
	char script_remove[512] = { 0 };

	const char* delete_ids[512];
	uint32_t delete_counts = 0;


	for (entity_t* c_entity = (entity_t*)hashmap_iterate(&mngr->entities, &o_index, &o_count); c_entity != NULL; c_entity = (entity_t*)hashmap_iterate(&mngr->entities, &o_index, &o_count))
	{
		if (c_entity->static_ent || !c_entity->dynamic) {
			continue;
		}

		cpVect position = entity_getposition(c_entity);
		Rect_t* box = entity_getrenderbox(c_entity);

		Vector_t pos = Vec(position.x, position.y);

		Vector_t lbound = Vec(pos.x - box->width, position.y);
		Vector_t rbound = Vec(pos.x + box->width, position.y);
		Vector_t tbound = Vec(pos.x, position.y + box->height);
		Vector_t bbound = Vec(pos.x, position.y - box->height);

		wbool lboundres = vector_in_rect(&loadedworldbound, &lbound);
		wbool rboundres = vector_in_rect(&loadedworldbound, &rbound);
		wbool tboundres = vector_in_rect(&loadedworldbound, &tbound);
		wbool bboundres = vector_in_rect(&loadedworldbound, &bbound);

		if (!lboundres || !rboundres || !tboundres || !bboundres) {

			if (delete_counts >= 512) {
				logprint("can't delete more than 512 dynamic entities in one go, stop unloading...");
				break;
			}

			//select outbound position
			Vector_t* outboundpos = NULL;

			if (!lboundres)
			{
				outboundpos = &lbound;
			}
			else if (!rboundres)
			{
				outboundpos = &rbound;
			}
			else if (!tboundres)
			{
				outboundpos = &tbound;
			}
			else if (!bboundres)
			{
				outboundpos = &bbound;
			}

			char simulgenstr[256] = { 0 };

			w_sprintf(simulgenstr, 256, "return get_entity_simulgen('%s')", c_entity->entity_id);

			script_binary_return_t returndata = { 0 };

			log_return = script_execstring(mngr->script_mngr, simulgenstr, &returndata, sizeof(script_binary_return_t));

			if (log_return != NULL)
				logprint(log_return);

			level_stream_save_world_entity(mngr, c_entity, outboundpos,returndata.binarydata,returndata.binarydatalen,wtrue);

			//unload this entity
			if (returndata.binarydata != NULL)
			{
				wf_free(returndata.binarydata);
				returndata.binarydata = NULL;
			}

			//remove script data
			if (c_entity->has_script)
			{
				w_sprintf(script_remove, 512, "remove_entity('%s',%d)", c_entity->entity_id, mngr->script_id);

				log_return = script_execstring(mngr->script_mngr, script_remove, NULL, 0);

				if (log_return != NULL)
					logprint(log_return);
			}

			//free entities data
			entity_freelinkconstraint(mngr->phy_manager, c_entity);

			if (mngr->ondelete_func != NULL) {
				mngr->ondelete_func(c_entity);
			}

			entity_free(mngr->phy_manager, c_entity);

			delete_ids[delete_counts] = c_entity->entity_id;
			delete_counts++;
		}

	}


	//remove old ids from hashmap
	for (uint32_t icount = 0; icount < delete_counts; icount++)
	{
		hashmap_remove(&mngr->entities, delete_ids[icount]);
	}

	

	atomic_set_value(&mngr->world_entity_pool_state, S_UNLOADED);

}

void level_stream_save_world_entity(level_stream_t* const level, entity_t* const world_entity,Vector_t* const possave,unsigned char* databin,size_t databinlen,wbool simul)
{
	//check if the entity exist in db
	char select_string[512];
	int sqlite_error = 0;
	w_sprintf(select_string, 512, "SELECT count(*) FROM world_entities WHERE id = '%s'", world_entity->entity_id);

	sqlite3_stmt* statement;

	sqlite_error = sqlite3_prepare_v2(level->chunks_db, select_string, -1, &statement, NULL);

	if (sqlite_error)
	{
		logprint("Can't prepare statement %s, sqlite error %s", select_string, sqlite3_errmsg(level->chunks_db));
		return;
	}

	sqlite_error = sqlite3_step(statement);

	if (sqlite_error != SQLITE_ROW && sqlite_error != SQLITE_DONE)
	{
		logprint("Can't execute statement, sqlite error %s", sqlite3_errmsg(level->chunks_db));
		sqlite3_finalize(statement);
		return;
	}

	int32_t count = sqlite3_column_int(statement, 0);

	sqlite3_finalize(statement);

	sqlite3_stmt* statement_insert;

	cpVect pos = entity_getposition(world_entity);

	if (possave != NULL)
	{
		pos.x = possave->x;
		pos.y = possave->y;
	}
	

	cpBB box = cpShapeGetBB(world_entity->shape[0]);

	//check if the position is not in a trigger, otherwise move it outside of trigger area
	trigger_t* intersect = level_stream_get_trigger_intersect(level, box, world_entity->entity_id);

	if (intersect != NULL) {
		intersect->box.l;

		cpFloat xcenter = intersect->box.l + (intersect->box.r - intersect->box.l);

		if (pos.x <= xcenter) { //position to the left
			pos.x = (intersect->box.l - (box.r - box.l)) - level->tile_size;
		}
		else if (pos.x > xcenter) { //position to the right
			pos.x = (intersect->box.r + (box.r - box.l)) + level->tile_size;
		}

		pos.y = intersect->box.b + (intersect->box.t - intersect->box.b);

		entity_setposition(world_entity, (wfloat)pos.x, (wfloat)pos.y);
	}


	//entity doesn't exist, insert it
	if (sqlite_error == SQLITE_ROW && count == 0)
	{
		w_sprintf(select_string, 512, "INSERT INTO world_entities VALUES(@id,@frag_file,@source_id,@script_ctor,@Xpos,@Ypos,@simul,@simuldata,@simullastupdate)");

		sqlite_error = sqlite3_prepare_v2(level->chunks_db, select_string, -1, &statement_insert, NULL);

		if (sqlite_error)
		{
			logprint("Can't prepare statement %s, sqlite error %s", select_string, sqlite3_errmsg(level->chunks_db));
			return;
		}

		sqlite3_bind_text(statement_insert, 1, world_entity->entity_id, -1, SQLITE_STATIC);
		sqlite3_bind_text(statement_insert, 2, world_entity->source, -1, SQLITE_STATIC);
		sqlite3_bind_text(statement_insert, 3, world_entity->source_id, -1, SQLITE_STATIC);
		sqlite3_bind_text(statement_insert, 4, world_entity->script_ctor, -1, SQLITE_STATIC);
		sqlite3_bind_int64(statement_insert, 5, (int64_t)pos.x);
		sqlite3_bind_int64(statement_insert, 6, (int64_t)pos.y);
		sqlite3_bind_int(statement_insert, 7, (int)simul);

		if(databin != NULL)
			sqlite3_bind_blob(statement_insert, 8, (const void*)databin, databinlen, SQLITE_STATIC);
		else
			sqlite3_bind_null(statement_insert, 8);

		time_t timenow;

		time(&timenow);

		sqlite3_bind_int64(statement_insert, 9, (intmax_t)timenow);

		sqlite_error = level_stream_handlelock(statement_insert);

		if (sqlite_error != SQLITE_DONE)
		{
			logprint("Can't insert world entity, sqlite error %s", sqlite3_errmsg(level->chunks_db));
		}

		sqlite_error = sqlite3_finalize(statement_insert);

		if (sqlite_error != SQLITE_OK)
		{
			logprint("Error finalizing insert statement", sqlite3_errmsg(level->chunks_db));
		}

		return;
	}
	else if (sqlite_error == SQLITE_ROW && count == 1)
	{
		//entity exist, update it
		w_sprintf(select_string, 512, "UPDATE world_entities SET Xpos = @Xpos, Ypos = @Ypos,simul = @simul,simuldata = @simuldata,simullastupdate = @simullastupdate WHERE id = @id");

		sqlite_error = sqlite3_prepare_v2(level->chunks_db, select_string, -1, &statement_insert, NULL);

		if (sqlite_error)
		{
			logprint("Can't prepare statement %s, sqlite error %s", select_string, sqlite3_errmsg(level->chunks_db));
			return;
		}

		sqlite3_bind_int64(statement_insert, 1, (int64_t)pos.x);
		sqlite3_bind_int64(statement_insert, 2, (int64_t)pos.y);
		sqlite3_bind_int(statement_insert, 3, (int)simul);

		if (databin != NULL)
			sqlite3_bind_blob(statement_insert, 4, (const void*)databin, databinlen, SQLITE_STATIC);
		else
			sqlite3_bind_null(statement_insert, 4);

		time_t timenow;

		time(&timenow);

		sqlite3_bind_int64(statement_insert, 5, (intmax_t)timenow);


		sqlite3_bind_text(statement_insert, 6, world_entity->entity_id, -1, SQLITE_STATIC);

		sqlite_error = level_stream_handlelock(statement_insert);

		if (sqlite_error != SQLITE_DONE)
		{
			logprint("Can't update world entity, sqlite error %s", sqlite3_errmsg(level->chunks_db));
		}

		sqlite_error = sqlite3_finalize(statement_insert);

		if (sqlite_error != SQLITE_OK)
		{
			logprint("Error finalizing update statement", sqlite3_errmsg(level->chunks_db));
		}

		return;
	}
	else
	{
		if (count > 1)
		{
			logprint("More Than one world entity with the id %s", world_entity->entity_id);
		}
		else
		{
			logprint("The count(*) query for a world entity didn't return any value %s", world_entity->entity_id);
		}
	}
}

//!BACKGROUND THREAD!
//simulation de d�placement pour les entit�s en dehors du monde charg�
//r�cup�re dans un type simulcontainer de toutes les entit�s � mettre � jour
//envoie les �l�ments � la fonction de simulation c�t� module de jeu
//met � jour les donn�es en BDD
wbool level_stream_simul_world_entity(level_stream_t* const level,time_t lastupdate)
{
	if (level->chunks_db_background == NULL || !level->chunks_db_ready || level->simul_update == NULL)
	{
		return wfalse;
	}

	//check if the entity exist in db
	char select_string[512];
	int sqlite_error = 0;
	w_sprintf(select_string, 512, "SELECT COUNT(id) FROM world_entities WHERE simul = true");

	sqlite3_stmt* statement;

	sqlite_error = sqlite3_prepare_v2(level->chunks_db_background, select_string, -1, &statement, NULL);

	if (sqlite_error)
	{
		logprint("Can't prepare statement %s, sqlite error %s", select_string, sqlite3_errmsg(level->chunks_db_background));
		return wfalse;
	}

	sqlite_error = sqlite3_step(statement);

	if (sqlite_error != SQLITE_ROW && sqlite_error != SQLITE_DONE)
	{
		logprint("Can't execute statement, sqlite error %s", sqlite3_errmsg(level->chunks_db_background));
		sqlite3_finalize(statement);
		return wfalse;
	}

	int32_t count = sqlite3_column_int(statement, 0);

	sqlite3_finalize(statement);

	//nothing to update
	if (count == 0)
	{
		return wfalse;
	}

	simul_container_t* containers = (simul_container_t*)wf_calloc(count, sizeof(simul_container_t));

	w_sprintf(select_string, 512, "SELECT id,frag_file,Xpos,Ypos,simuldata,simullastupdate FROM world_entities WHERE simul = true");

	sqlite_error = sqlite3_prepare_v2(level->chunks_db_background, select_string, -1, &statement, NULL);

	if (sqlite_error)
	{
		logprint("Can't prepare statement %s, sqlite error %s", select_string, sqlite3_errmsg(level->chunks_db_background));
		return wfalse;
	}

	int32_t icontainer = 0;

	time_t now;
	time(&now);

	do
	{

		sqlite_error = sqlite3_step(statement);

		if (sqlite_error != SQLITE_ROW && sqlite_error != SQLITE_DONE)
		{
			logprint("Can't execute statement, sqlite error %s", sqlite3_errmsg(level->chunks_db_background));
			sqlite3_finalize(statement);
			return wfalse;
		}

		//fetch data from database
		const unsigned char* idsim = sqlite3_column_text(statement, 0);

		w_strcpy(containers[icontainer].id, 255, (const char*)idsim);

		const unsigned char* fragsim = sqlite3_column_text(statement, 1);

		w_strcpy(containers[icontainer].frag_file, 255, (const char*)fragsim);

		containers[icontainer].xpos = sqlite3_column_int64(statement, 2);
		containers[icontainer].ypos = sqlite3_column_int64(statement, 3);

		const void* simdata = sqlite3_column_blob(statement, 4);

		size_t simdatalen = sqlite3_column_bytes(statement, 4);

		containers[icontainer].binarydata = (unsigned char*)wf_malloc(simdatalen);

		w_memcpy(containers[icontainer].binarydata, simdatalen, simdata, simdatalen);

		containers[icontainer].binarydatalen = simdatalen;

		containers[icontainer].lastupdate = (intmax_t)sqlite3_column_int64(statement, 5);

		//first update, we must update all the simulated data epoch to avoid any simulation overcompensation
		if (lastupdate == 0)
		{
			containers[icontainer].lastupdate = now;
		}

		icontainer++;

	} while (sqlite_error == SQLITE_ROW && icontainer < count);

	sqlite3_finalize(statement);

	//don't call simulation func if first update, update epoch values first
	if (lastupdate != 0)
	{
		//call simulation func
		level->simul_update(containers, count, level->base_width * level->world_col_limit, level->base_height * level->world_row_limit, level->tile_size);
	}
	
	//update data in BDD

	char update_string[512];

	w_sprintf(update_string, 512, "UPDATE world_entities SET Xpos = @Xpos, Ypos = @Ypos,simuldata = @simuldata,simullastupdate = @simullastupdate WHERE id = @id AND simul = true");

	sqlite_error = sqlite3_prepare_v2(level->chunks_db_background, update_string, -1, &statement, NULL);

	for (icontainer = 0; icontainer < count; icontainer++)
	{

		sqlite3_bind_int64(statement, 1, (int64_t)containers[icontainer].xpos);
		sqlite3_bind_int64(statement, 2, (int64_t)containers[icontainer].ypos);

		sqlite3_bind_blob(statement, 3, (const void*)containers[icontainer].binarydata, containers[icontainer].binarydatalen, SQLITE_STATIC);

		time_t timenow;

		time(&timenow);

		sqlite3_bind_int64(statement, 4, (intmax_t)timenow);
		sqlite3_bind_text(statement, 5, containers[icontainer].id, - 1, SQLITE_STATIC);

		sqlite_error = level_stream_handlelock(statement);

		if (sqlite_error != SQLITE_DONE)
		{
			logprint("Can't update world entity, sqlite error %s", sqlite3_errmsg(level->chunks_db_background));
		}

		sqlite3_reset(statement);
		sqlite3_clear_bindings(statement);

		wf_free(containers[icontainer].binarydata);
		containers[icontainer].binarydata = NULL;
		containers[icontainer].binarydatalen = 0;
	}

	sqlite3_finalize(statement);

	wf_free(containers);
	return wtrue;
}
