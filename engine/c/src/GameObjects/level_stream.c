﻿
#include <time.h>
#include <GameObjects/level_stream.h>
#include <GameObjects/level_stream_chunks.h>
#include <GameObjects/level_stream_dynamic.h>


//this function is called in a background thread to load chunk from disk asynchronously
static void _level_stream_background_update(void* level_obj)
{
    level_stream_t* level = (level_stream_t*)level_obj;

	if (level->level_thread_first_run)
	{
		level->level_thread_first_run = wfalse;
	}

     //load / unload chunk
    for(uint32_t ichunk = 0;ichunk < CHUNK_NUMBER;ichunk++)
    {
        volatile uint32_t chunk_state = S_EMPTY;

        atomic_set_value(&chunk_state,level->chunks[ichunk].state);


        if(chunk_state == S_EMPTY || chunk_state == S_UNLOADED || chunk_state == S_OUTOFBOUND)
        {
			if (level->chunks[ichunk].world_row < 0 || level->chunks[ichunk].world_col < 0 || level->chunks[ichunk].world_row > level->world_row_limit - 1 || level->chunks[ichunk].world_col > level->world_col_limit - 1) {
				level->chunks[ichunk].visible = wfalse;
				atomic_set_value(&level->chunks[ichunk].state, S_OUTOFBOUND);
				continue;
			}

            atomic_set_value(&level->chunks[ichunk].state,S_LOADING);
			level->chunks[ichunk].visible = wtrue;

			int64_t posX = 0;
			int64_t posY = 0;

            Map* map_data = level_stream_load_chunkdb(level,level->chunks[ichunk].world_row,level->chunks[ichunk].world_col,&level->chunks[ichunk].chunk_id[0],&posX,&posY);

            wbool free_data = wtrue;

            if(map_data == NULL)
            {
                //use the default chunk generation function if no custom generation routine defined
                if(level->custom_chunk_gen_func == NULL)
                    map_data = level_stream_gen_chunk(level->gen_data_pool,level->base_row_count,level->base_col_count,"prog_common",level->base_num_tiles,&level->chunks[ichunk].chunk_id[0]);
                else
                    map_data = level->custom_chunk_gen_func(level->gen_data_pool,level->world_entity_pool,&level->temp_world_entities,level->base_row_count,level->base_col_count,"prog_common",level->base_num_tiles,&level->chunks[ichunk].chunk_id[0], level->chunks[ichunk].world_row, level->chunks[ichunk].world_col, level->user_data_gen_info);

				//error generating map data, go to next chunk
				if (map_data == NULL)
				{
					logprint("Error generating map data on chunk %d, go to next chunk...", ichunk);
					continue;
				}

                free_data = wfalse;

				//save map chunk
				level_stream_save_chunk(level, &level->chunks[ichunk], map_data, level->chunks[ichunk].world_row, level->chunks[ichunk].world_col);

				atomic_set_value(&level->chunks[ichunk].state, S_SAVED);
            }

			level_stream_backload_chunkgraph(level, &level->chunks[ichunk], map_data, ichunk);

			
			size_t content_size = map__get_packed_size(map_data);
			
			level->chunks[ichunk].content_buffer = (unsigned char*)tempallocator_getmemory(level->chunks[ichunk].content_pool,content_size);
			
			map__pack(map_data,level->chunks[ichunk].content_buffer);
			
			level->chunks[ichunk].content_buffer_size = content_size;

            if(free_data)
            {
                map__free_unpacked(map_data,NULL);
            }

            tempallocator_clear(level->gen_data_pool);

            atomic_set_value(&level->chunks[ichunk].state, S_FRONTLOADING);

            atomic_set_value(&level->world_entity_pool_state, S_FRONTLOADING);
        }
		else if (chunk_state == S_GAMECHUNKBACKLOADING)
		{
			module_mngr_t* module_manager = (module_mngr_t*)level->module_manager;

			if (module_manager->game_chunk_loaded)
				module_manager->game_chunk_loaded(&level->chunks[ichunk],wtrue);

			atomic_set_value(&level->chunks[ichunk].state, S_GAMECHUNKFRONTLOADING);
		}
        else if(chunk_state == S_UNLOADING)
        {
			Map* map_data = map__unpack(NULL,level->chunks[ichunk].content_buffer_size,level->chunks[ichunk].content_buffer);
			
            level_stream_sync_chunkcontent(level,&level->chunks[ichunk],map_data);
			
			level_stream_save_chunk(level,&level->chunks[ichunk],map_data,level->chunks[ichunk].world_row,level->chunks[ichunk].world_col);
			
			tempallocator_clear(level->chunks[ichunk].content_pool);
			map__free_unpacked(map_data,NULL);
			
			atomic_set_value(&level->chunks[ichunk].state, S_FRONTUNLOADING);
        }
       
    }

	volatile uint32_t world_state = S_EMPTY;

	atomic_set_value(&world_state, level->world_entity_pool_state);

	if (world_state == S_EMPTY)
	{
		//update simulated entities before loading what's left in range
		time_t now;
		time(&now);

		//update each 2 seconds
		if (level->simul_last_update == 0 || (now - level->simul_last_update) >= 2)
		{
			//update simulated world entities (out of bounds)
			level_stream_simul_world_entity(level, level->simul_last_update);
			level->simul_last_update = now;
		}

		//load world entities in range
		level_stream_load_world_entities_inrange(level, wtrue);
	}
}

void level_stream_map_updated_script(level_stream_t* const level, lua_State* script_mngr)
{
	//call script init function if any
	if (strlen(level->current_module) > 0)
	{
		char buff[260];

		w_sprintf(buff, 260, "%s.map_updated(%d)\n", level->current_module, level->script_id);

		const char* result = script_execstring(script_mngr, buff, NULL, 0);

		if (result != NULL)
		{
			logprint(result);
		}
	}
}

void level_stream_init_script_pointer(level_stream_t* const level, lua_State* script_mngr)
{
	const char* log_return = NULL;
	char level_script_path[512];

	//init Default level ptr
	w_sprintf(level_script_path, 512, "init_default_pointer(%d)", level->script_id);

	log_return = script_execstring(script_mngr, level_script_path, NULL, 0);

	if (log_return != NULL)
		logprint(log_return);
	else
	{
		logprint("Pointer par défaut initialisé");
	}
}

void level_stream_init_script_after_switch(level_stream_t* const level, lua_State* script_mngr)
{
	//call script init after switch function if any
	if (strlen(level->current_module) > 0)
	{
		char buff[260] = { 0 };

		w_sprintf(buff, 260, "%s.init_after_switch(%d)\n", level->current_module, level->script_id);

		const char* result = script_execstring(script_mngr, buff, NULL, 0);

		if (result != NULL)
		{
			logprint(result);
		}
	}
}


//db structure for chunk
//row_num (signed int64), col_num (signed int64), chunkid (guid), content (binary protobuf content, stored as a level protobuf object)
//the rownum / colnum represent the position of the chunk in the world map
//a world chunk have a constant width / height and represent the minimum size of a level_stream chunk
//a level stream chunk have a size that is a power of the world chunk size, so we can't have a incoherent number of world chunk in a level_stream chunk. a level stream chunk is decided from the player screen size and the power of the player system. (more power > bigger chunk)

void level_stream_init(level_stream_t* const map,int32_t chunk_width,int32_t chunk_height,uint64_t world_spawn_pos,uint64_t world_row_limit,uint64_t world_col_limit,const int32_t tile_size,texture_info* const texturePointer,const char* texture_name,render_manager_t* const render_mngr,const char* prog_name,resources_manager_t* const resx_mngr, void* module_manager, trigger_callback_t trigger_func,localization_t* const local_obj,lua_State* script_mngr,physics_manager_t* const phy_manager,gen_chunk_t custom_chunk_gen_func,const char* script_module,void* pathfind_worker,void* user_data_gen_info,check_col_t check_col_func,check_col_t check_col_func_editable)
{
    //first, we need to check if on the initial position we have saved chunk availables, if yes load them, if no, generate the chunk that doesn't exists
    map->resx_manager = resx_mngr;
    map->render_mngr = render_mngr;
	map->phy_manager = phy_manager;
	map->module_manager = module_manager;
    memset(&map->level_thread,0,sizeof(thread_t));
    map->custom_chunk_gen_func = custom_chunk_gen_func;
	map->simul_update = NULL;
	map->simul_last_update = 0;
	map->user_data_gen_info = user_data_gen_info;
	map->script_id = script_registerpointer((intptr_t)map, LEVELOBJECT);
	map->chunk_loaded_callback = NULL;
	map->world_row_limit = world_row_limit;
	map->world_col_limit = world_col_limit;
	map->onupdate_func = NULL;
	map->ondelete_func = NULL;
	map->ondraw_func = NULL;
	map->randomizer = NULL;
	map->level_thread_first_run = wtrue;
	map->pending_new_chunk_loaded = wfalse;
	map->first_load = wtrue;

	map->pathfind_worker = pathfind_worker;

    map->gen_data_pool = (tempallocator_t*)wf_calloc(1,sizeof(tempallocator_t));

    tempallocator_init(map->gen_data_pool,TEMPALLOCATOR_SIZE);

    //initialize entity hashmap
    hashmap_initalloc(&map->entities,100,sizeof(entity_t),20);

    map->tile_size = tile_size;
    map->trigger_func = trigger_func;
    map->local_obj = local_obj;
    map->script_mngr = script_mngr;
    map->world_entity_pool_state = S_EMPTY;
	
	int32_t tilemap_width = chunk_width;
	int32_t tilemap_height = chunk_height;

	map->preallocated_edges = NULL;
	map->edges_array = NULL;
	map->edges_array_size = 0;

    map->base_col_count = tilemap_width / tile_size;
	map->base_row_count = tilemap_height / tile_size;

    map->base_num_tiles = map->base_col_count * map->base_row_count;
	map->base_width = tilemap_width;
	map->base_height = tilemap_height;
	map->check_col_func = check_col_func;
	map->check_col_func_editable = check_col_func_editable;

	map->collisions_array_count = map->base_num_tiles * CHUNK_NUMBER;

	map->collisions_array = (int32_t*)wf_calloc(map->collisions_array_count, sizeof(int32_t));
    
	uint64_t spawn_col = (world_spawn_pos % world_col_limit);
	uint64_t spawn_row = (uint64_t)(world_spawn_pos / world_col_limit);

    uint64_t cworld_row = (spawn_row > 0) ? spawn_row-1 : spawn_row;
    uint64_t cworld_col = (spawn_col > 0) ? spawn_col-1 : spawn_col;

	Vector_t center_pos = Vec((spawn_col * tilemap_width) + (tilemap_width * WFLOAT05), (spawn_row * tilemap_height) + (tilemap_height * WFLOAT05));

	wfloat posX = center_pos.x - (wfloat)(tilemap_width * 1.5);
	wfloat posY = center_pos.y - (wfloat)(tilemap_height * 1.5);



	if (script_module != NULL)
	{
		const char* log_return;
		char level_script_path[512];
		w_strcpy(map->current_module, 256, script_module);
		get_file_without_ext(map->current_module);
		//load script file
		w_sprintf(level_script_path, 512, "package.loaded['%s'] = nil \n require('%s')", map->current_module, map->current_module);


		log_return = script_execstring(script_mngr, level_script_path, NULL, 0);

		if (log_return != NULL)
			logprint(log_return);
		else
			logprint("Module %s chargé", map->current_module);

		//init Default level ptr
		level_stream_init_script_pointer(map, script_mngr);
	}

    map->world_entity_pool = (tempallocator_t*)wf_calloc(1, sizeof(tempallocator_t));

    tempallocator_init(map->world_entity_pool, WORLDENTITYALLOCATOR_SIZE);
    
    circular_array_init(&map->temp_world_entities,sizeof(Entity*),MAX_TEMP_WORLD_ENTITIES,NULL);
    
	
	for(int32_t itmap = 0;itmap < CHUNK_NUMBER;itmap++)
	{
        map->chunks[itmap].phy_manager = (physics_manager_t*)wf_calloc(1,sizeof(physics_manager_t));

	    create_slave_manager(map->chunks[itmap].phy_manager,phy_manager);

		 map->chunks[itmap].pos = (STREAM_POS)itmap;
		 map->chunks[itmap].state = S_EMPTY;
		 map->chunks[itmap].unload = wfalse;
		 map->chunks[itmap].bounds.width = tilemap_width;
         map->chunks[itmap].bounds.height = tilemap_height;
         map->chunks[itmap].bounds.position = Vec(posX,posY);
         map->chunks[itmap].visible = wtrue;
         map->chunks[itmap].world_row = cworld_row;
         map->chunks[itmap].world_col = cworld_col;
		 map->chunks[itmap].content_buffer = NULL;
		 map->chunks[itmap].mem_pos = itmap;
		 map->chunks[itmap].content_pool = (tempallocator_t*)wf_calloc(1,sizeof(tempallocator_t));
		 map->chunks[itmap].lights.keys = NULL;
		 map->chunks[itmap].lights.max_elem = 0;
		 map->chunks[itmap].lights.num_elem = 0;
	
		tempallocator_init(map->chunks[itmap].content_pool,MAPALLOCATOR_SIZE);
         
         hashmap_initalloc(&map->chunks[itmap].text,MAX_TEXT_OBJ,sizeof(text_entity_t),10);
         hashmap_initalloc(&map->chunks[itmap].triggers,MAX_TRIGGER,sizeof(trigger_t),10);
         hashmap_initalloc(&map->chunks[itmap].group_objects,MAX_GROUP,sizeof(entity_group_t),10);
		 hashmap_initalloc(&map->chunks[itmap].particles_list, POOL_PARTICLES_OBJ, sizeof(particle_object_t), POOL_PARTICLES_OBJ);
		 hashmap_initalloc(&map->chunks[itmap].colliders, POOL_COLLIDER_OBJ, sizeof(collider_t), POOL_COLLIDER_OBJ);
		 hashmap_initalloc(&map->chunks[itmap].waypoints, POOL_WAYPOINT_OBJ, sizeof(waypoint_t), POOL_WAYPOINT_OBJ);
		 hashmap_initalloc(&map->chunks[itmap].entities, MAX_ENTITIES, sizeof(entity_t), 10);

         
		 map->access_chunk[itmap] = &map->chunks[itmap];
		 
		 posX += tilemap_width;

         cworld_col++;

         if(cworld_col > spawn_col + 1)
         {
             cworld_row++;
             cworld_col = spawn_col - 1;
         }
		 
		 if(((itmap + 1) % 3) == 0)
		 {
			posX = center_pos.x - (wfloat)(tilemap_width * WFLOAT15);
			posY += tilemap_height;
		 }
	}

	physics_alloc(phy_manager,map->base_col_count * CHUNK_COL,map->base_row_count * CHUNK_ROW,wtrue);

	map->edges_array = alloc_edges_list(map->base_row_count * CHUNK_ROW, map->base_col_count * CHUNK_COL, map->edges_array, &map->edges_array_size, wfalse, &map->preallocated_edges);
	
	build_tilemap_collisions(phy_manager, map->collisions_array, map->collisions_array, map->base_col_count * CHUNK_COL, map->base_row_count * CHUNK_ROW, map->tile_size, worldrectzero,CHUNK_COL,CHUNK_ROW, map->check_col_func, map->check_col_func_editable);

	if (map->pathfind_worker != NULL)
	{
		pathfind_worker_t* worker = (pathfind_worker_t*)map->pathfind_worker;

		if (!worker->initialized)
			pathfind_init(worker, 10, phy_manager->collisions_array, phy_manager->col_count, phy_manager->row_count);
		else
			pathfind_updategrid(worker, phy_manager->collisions_array, phy_manager->col_count, phy_manager->row_count);
	}
	
	map->center_pos = center_pos;
}

void level_stream_setsimulupdatecallback(level_stream_t* const level,simul_update_world_entity_t callback)
{
	level->simul_update = callback;
}

void level_stream_setlightmngr(level_stream_t* const  level, light_manager_t* const light_mngr)
{
	level->light_manager = light_mngr;
}

void level_stream_setchunkloadedcallback(level_stream_t* const level,new_chunk_loaded_t callback)
{
	level->chunk_loaded_callback = callback;
}

void level_stream_setentitycallback(level_stream_t* const level, void(*ondraw_func)(entity_t* const currententity), void(*onupdate_func)(entity_t* const currententity), void(*ondelete_func)(entity_t* const currententity))
{
	level->ondraw_func = ondraw_func;
	level->onupdate_func = onupdate_func;
	level->ondelete_func = ondelete_func;
}

//load from a sqlite database the existing chunks, if no chunks exist in db, generate the required number of chunks

//thread strategy
//((loading))
//-1- (main thread)get the chunk that need to be loaded (transmit to thread row / col number as well as id)
//-2- (back thread)read from disk the chunk data and load the binary content in memory, deserialize the data as a protobuf content
//-3- (main thread) load game specific entities
//((unloading / saving))
//-1- (main thread) mark chunk as unloaded and send to back thread updated protobuf content with id row / col number
//-2- (back thread) serialise protobuf content and save it to disk
//-3- (main thread) free unneeded chunk
//((generating))
//-1- (main thread) send chunk basic info (row / col)
//-2- (back thread) generate a chunk based on procedural generation and from fragment informations (only load protobuf data), create a protobuf object 
//-3- (main thread) load newly created chunk based on protobuf data

//load / unload strategy
// a chunk must be loaded if we change of center chunk, but we must respect rules to avoid constant loading / unloading
//no new center_map as long as there is chunk in the process of loading / unloading
// 

void level_stream_initchunksdb(level_stream_t* const  level,const char* base_chunk_db_path,const char* temp_chunk_db_path)
{
    int sqlite_error = 0;

	//check file exist before opening it with sqlite, sqlite create automatically a file otherwise
     FILE* file = NULL;

	 w_fopen(file, temp_chunk_db_path,"r");

	 //no db found, copy base path
	 if (file == NULL)
	 {
		 if (!copy_file(base_chunk_db_path, temp_chunk_db_path,wfalse)) {
			 logprint("Error while copying base chunk database to home directory %s %s", base_chunk_db_path, temp_chunk_db_path);
			 return;
		 }

		w_fopen(file, temp_chunk_db_path, "r");
	 }

       
	if(file != NULL)
	{
		fclose(file);

		//wchar_t tmp_path[260];
		//MultiByteToWideChar(CP_THREAD_ACP,0, temp_chunk_db_path,260,tmp_path,260);

		//UTF8_IGNORE_ERROR

		sqlite_error = sqlite3_open_v2(temp_chunk_db_path, &level->chunks_db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_NOMUTEX, NULL);

		if(sqlite_error)
		{
			logprint("Error loading chunks database! :%s",sqlite3_errmsg(level->chunks_db));
			sqlite3_close(level->chunks_db);
			return;
		}

		//background thread connection
		sqlite_error = sqlite3_open_v2(temp_chunk_db_path, &level->chunks_db_background, SQLITE_OPEN_READWRITE | SQLITE_OPEN_NOMUTEX, NULL);

		if (sqlite_error)
		{
			logprint("Error loading chunks database / background connection! :%s", sqlite3_errmsg(level->chunks_db_background));
			sqlite3_close(level->chunks_db_background);
			return;
		}
	}
	else
	{
		logprint("No chunks db file! %s", temp_chunk_db_path);
		level->chunks_db = NULL;
		level->chunks_db_background = NULL;
		return;
	}

	level->chunks_db_ready = wtrue;
}

void level_stream_start_backgroundthread(level_stream_t* const level)
{
    thread_create(&level->level_thread,_level_stream_background_update,(void*)level,NULL,NULL);
}


static void _sync_change()
{
	#ifdef EMSCRIPTEN
		EM_ASM(
				//persist change
				FS.syncfs(false,function (err) {
					assert(!err);
				});
		);
	#endif
}


void level_stream_setrandomizer_func(level_stream_t* const level, tlayer_randomizer_t randomizer)
{
	level->randomizer = randomizer;
}

/*
    Store the binary representation of an entity before we load it into the main thread and create an entity_t
    Used only for world entity, since they are dynamic and not linked to a chunk
    this function is made to be called from a procedural generation func, so it MUST be thread-safe
*/

void level_stream_preload_world_entity(level_stream_t* const level_obj,unsigned char* entity_data,size_t entity_data_size)
{
    
}

/*
	Add an entity to our level stream : 
		if the entity is dynamic (the entity can move or be moved), it will be added to the world entity hashtable, so it can be loaded / unloaded independently of the chunk it is associated with
		if the entity is static (then entity postion / size NEVER change), it will be added to the chunk entity hashtable, and will be loaded / unloaded / saved with it associated chunk
	DON'T change a static entity while the game is running, as it is supposed to be linked with a chunk
	DON'T put a dynamic entity into a chunk, as it will cause entity to cease to exist and reset an existing state

	All static entities positions are relative to the chunk position, never use a world position for a static entity
*/
entity_t* level_stream_add_entity(level_stream_t* const level_obj, stream_chunk_t* const chunk_obj, resources_manager_t* const resx_mngr, physics_manager_t* const phy_mngr, char* entity_id, Entity* entity_data, render_manager_t* const render_manager, wbool dynamic)
{

	entity_t* new_entity = NULL;
	hashmap_t* hashmap = NULL;

	if (dynamic)
	{
		if (chunk_obj != NULL) {
			logprint("Can't add a dynamic entity on a stream_chunk ! ");
			return NULL;
		}

		new_entity = (entity_t*)hashmap_push(&level_obj->entities, entity_id);
		hashmap = &level_obj->entities;
	}
	else
	{
		if (chunk_obj == NULL) {
			new_entity = (entity_t*)hashmap_push(&level_obj->entities, entity_id);
			hashmap = &level_obj->entities;
		}
		else
		{
			new_entity = (entity_t*)hashmap_push(&chunk_obj->entities, entity_id);
			hashmap = &chunk_obj->entities;
		}
	}

	wbool animated = wfalse;
	AnimationList* anim_list = NULL;

	texture_info* ent_tex = gettexture_resx(resx_mngr, entity_data->tileset);

	animated = (entity_data->start_animation && strlen(entity_data->start_animation) > 0) ? wtrue : wfalse;

	if (animated)
	{
		anim_list = getanimation_resx(resx_mngr, entity_data->animation_file);

		if (anim_list == NULL)
			logprint("Can't load animation file %s, entity %s won't be animated!", entity_data->animation_file, entity_id);
	}

	if (entity_data->script_ctor && strcmp(entity_data->script_ctor, "") != 0 && !entity_data->script_init_only)
		new_entity->has_script = wtrue;
	else
		new_entity->has_script = wfalse;

	//we don't manage group in editor mode, keep that for runtime mode
	wbool in_group = (wbool)(entity_data->group_id != NULL && strcmp(entity_data->group_id, "") != 0);

	//if there is no physics handling

	Vector_t orig_pos = { (wfloat)entity_data->position->x,(wfloat)entity_data->position->y };


	entity_init(new_entity, entity_data, ent_tex, phy_mngr, anim_list, render_manager, in_group);

	int index = hashmap_getindexforkey(hashmap, entity_id);
	new_entity->entity_id = hashmap->keys[index];

	if (chunk_obj != NULL)
	{
		w_strcpy(new_entity->ref_map_id, 256, chunk_obj->map_id);
	}
	else
	{
		w_strcpy(new_entity->ref_map_id, 256, "world");
	}
	

	new_entity->dynamic = dynamic;

	if (chunk_obj != NULL) {
		entity_movetopos(new_entity, orig_pos.x + chunk_obj->bounds.position.x, orig_pos.y + chunk_obj->bounds.position.y);
	}

	if (animated && anim_list != NULL)
		entity_playanimation(new_entity, entity_data->start_animation,0);
	

	return new_entity;
}



static void level_stream_execscripts(level_stream_t* const level, script_container_t container)
{
	char level_script_path[256] = { 0 };
	char module_name[256] = { 0 };

	const char* log_return = NULL;

	//create entity script objects
	for (int32_t ise = 0; ise < container.entity_script_len; ise++)
	{
		if (container.entity_script[ise])
		{
			//load entity module
			w_strcpy(module_name, 256, container.entity_script[ise]->script_module);
			w_sprintf(level_script_path, 256, "package.loaded['%s'] = nil \n require('%s')", module_name, module_name);

			log_return = script_execstring(level->script_mngr, level_script_path, NULL, 0);

			if (log_return != NULL)
				logprint(log_return);

			if (!container.entity_script[ise]->script_init_only) {
				w_sprintf(level_script_path, 256, "add_entity(%s,'%s',%d)", container.entity_script[ise]->script_ctor, container.entity_script[ise]->entity_id, level->script_id);
			}
			else {
				//add the id as an extra param
				script_genscriptinitonlyctor(container.entity_script[ise]->script_ctor, container.entity_script[ise]->entity_id, level->script_id, level_script_path, 256);
			}

			//create entity object
			log_return = script_execstring(level->script_mngr, level_script_path, NULL, 0);

			if (log_return != NULL)
				logprint(log_return);
		}
	}

	//create text object scripts
	for (int32_t ist = 0; ist < container.text_script_len; ist++)
	{
		if (container.text_script[ist])
		{
			//load entity module
			w_strcpy(module_name, 256, container.text_script[ist]->script_module);
			w_sprintf(level_script_path, 256, "package.loaded['%s'] = nil \n require('%s')", module_name, module_name);

			log_return = script_execstring(level->script_mngr, level_script_path, NULL, 0);

			if (log_return != NULL)
				logprint(log_return);

			if (!container.text_script[ist]->script_init_only) {
				w_sprintf(level_script_path, 256, "add_text(%s,'%s',%d)", container.text_script[ist]->script_ctor, container.text_script[ist]->text_id, level->script_id);
			}
			else {
				//add the id as an extra param
				script_genscriptinitonlyctor(container.text_script[ist]->script_ctor, container.text_script[ist]->text_id, level->script_id, level_script_path, 256);
			}

			//create text object
			log_return = script_execstring(level->script_mngr, level_script_path, NULL, 0);

			if (log_return != NULL)
				logprint(log_return);
		}
	}


	//cleanup containers
	wf_free(container.entity_script);
	container.entity_script = NULL;
	wf_free(container.text_script);
	container.text_script = NULL;
}

void level_stream_main_script_init(level_stream_t* const level, lua_State* script_mngr)
{
	//call script init function if any
	if (strlen(level->current_module) > 0)
	{
		char buff[260];

		w_sprintf(buff, 260, "%s.init(%d)\n", level->current_module, level->script_id);

		const char* result = script_execstring(script_mngr, buff, NULL, 0);

		if (result != NULL)
		{
			logprint(result);
		}
		else
		{
			logprint("module %s initialized!", level->current_module);
		}
	}
}


static void level_stream_drawentities(level_stream_t* const  level, hashmap_t* const hashmap, light_manager_t* const light_mngr, render_manager_t* const render_mngr, resources_manager_t* const resx_mngr, wbool check_visible, int z_index)
{
	Rect_t screen_info = render_mngr->screen_info;

	if (render_mngr->zoom_factor != 1.0f)
	{
		screen_info.width = (int16_t)(screen_info.width * render_mngr->zoom_factor);
		screen_info.height = (int16_t)(screen_info.height * render_mngr->zoom_factor);
	}

	render_push_matrix(render_mngr);

	uint32_t o_index = 0;
	uint32_t o_count = 0;


	for (entity_t* c_entity = (entity_t*)hashmap_iterate(hashmap, &o_index, &o_count); c_entity != NULL; c_entity = (entity_t*)hashmap_iterate(hashmap, &o_index, &o_count))
	{
		SHADERPROGRAM_ID old_prog = render_mngr->current_program;

		if ((!check_visible || entity_checkrender(c_entity, screen_info)) && c_entity->is_visible && !c_entity->in_group && !c_entity->nodraw && c_entity->z_order == z_index)
		{
			cpVect vec = entity_getposition(c_entity);
			Vector_t vec_p = Vec((wfloat)vec.x, (wfloat)vec.y);
			light_t* light_arr[MAX_LIGHT_PER_ELEMENT];
			Rect_t* renderbox = entity_getrenderbox(c_entity);

			int32_t num_light_a = 0;

			if (light_mngr != NULL)
				num_light_a = light_manager_getinrangelight(light_mngr, renderbox, vec_p, render_mngr, light_arr);

			if (c_entity->program_id != render_get_emptyprogvalue())
			{
				if (render_get_numsubprogram(render_mngr, c_entity->program_id) > 0 && num_light_a > 0)
				{
					render_use_subprogram(render_mngr, c_entity->program_id, num_light_a - 1);
				}
				else
					render_use_program(render_mngr, c_entity->program_id);
			}
			else
				render_use_program(render_mngr, old_prog);


			if (c_entity->program_id != render_get_emptyprogvalue() && c_entity->shader_parameters != NULL && c_entity->num_parameters > 0)
			{

				Vector_t mirror_val = Vec(1.0, 1.0);
				float rotation = 0.0f;

				sprite_t* fsprite = entity_getsprite(c_entity, 0);

				if (c_entity->sprite)
					mirror_val = Vec((fsprite->mirror_texture_x) ? WFLOAT1 : -WFLOAT1, (fsprite->mirror_texture_y) ? WFLOAT1 : -WFLOAT1);

				if (c_entity->sprite)
					rotation = (float)deg2rad(sprite_getrotation(fsprite));

				Vector_t offset = vectorzero;

				if (c_entity->sprite && fsprite->texture_pointer->virtual_texture)
				{
					offset = fsprite->texture_pointer->region->position;
				}

				bind_shaders_params(light_mngr, render_mngr, c_entity->shader_parameters, c_entity->num_parameters, resx_mngr, &vec_p, &mirror_val, rotation, offset, light_arr, num_light_a);

				if (render_has_passparam(render_mngr))
					render_setuniform_float(render_mngr, "pass", (float)c_entity->current_pass);
			}

			entity_draw(c_entity, render_mngr, NULL, level->ondraw_func);

			if (c_entity->program_id != render_get_emptyprogvalue() && c_entity->shader_parameters != NULL && c_entity->num_parameters > 0)
				unbind_shaders_params(render_mngr, c_entity->shader_parameters, c_entity->num_parameters, resx_mngr, light_mngr);

		}

		render_use_program(render_mngr, old_prog);
	}

	render_pop_matrix(render_mngr);

}


static void level_stream_drawtriggers(hashmap_t* const hashmap, render_manager_t* const render_mngr)
{
	uint32_t o_index = 0;
	uint32_t o_count = 0;

	for (trigger_t* c_trigger = (trigger_t*)hashmap_iterate(hashmap, &o_index, &o_count); c_trigger != NULL; c_trigger = (trigger_t*)hashmap_iterate(hashmap, &o_index, &o_count))
	{
		trigger_draw(c_trigger, render_mngr);
	}
}

void level_stream_draw(level_stream_t* const  level,render_manager_t* const render_mngr,resources_manager_t* const resx_manager,int z_index)
{

	//draw tilemaps that are visible and in state S_LOADED
	for(uint32_t ichunk = 0;ichunk < CHUNK_NUMBER;ichunk++)
	{
        volatile uint32_t chunk_state = S_EMPTY;

        atomic_set_value(&chunk_state,level->chunks[ichunk].state);

		if(chunk_state == S_LOADED && level->chunks[ichunk].visible)
		{
			Tilemap_Draw(&level->chunks[ichunk].map, z_index,render_mngr,level->chunks[ichunk].bounds.position, level->light_manager,resx_manager);

			level_stream_drawentities(level,&level->chunks[ichunk].entities, level->light_manager, render_mngr, resx_manager, wtrue, z_index);

			//draw trigger in debug mode
			#if _DEBUG
				SHADERPROGRAM_ID old_prog = render_mngr->current_program;

				render_use_program(render_mngr, render_get_program(render_mngr, "prog_no_texture"));

				level_stream_drawtriggers(&level->chunks[ichunk].triggers, render_mngr);

				render_use_program(render_mngr, old_prog);
			#endif
		}
	}

	//draw world entities
	level_stream_drawentities(level,&level->entities,level->light_manager, render_mngr, resx_manager, wtrue, z_index);
}

static void inverse_cpy(int32_t* const array, int32_t dest, int32_t src,int32_t count)
{
	int32_t ic = count - 1;
	
	while (ic >= 0)
	{
		array[dest + ic] = array[src + ic];
		ic--;
	}

}


//move collisions data before changing chunk position
//depending of the new center map, the collisions copy scheme is different
//TODO : move edge array data (for lighting system) in the same way than collisions data
static void _move_collisions_data(level_stream_t* const  level, int32_t src_col_start, int32_t src_row_start, int32_t src_row_end, int32_t dest_row_start, int32_t dest_col_start, int32_t len_copy, wbool inverse_copy)
{
	int32_t dir = (src_row_start  > src_row_end) ? -1 : 1;

	for (int32_t ir = src_row_start, dr = dest_row_start; ir != src_row_end; ir += dir, dr += dir)
	{
		int32_t src_cpy = (ir * (level->base_col_count * CHUNK_COL)) + src_col_start;

		int32_t dest_cpy = (dr * (level->base_col_count * CHUNK_COL)) + dest_col_start;

		if (dir < 0)
		{
			src_cpy -= len_copy;
			dest_cpy -= len_copy;
		}

		if (!inverse_copy) {
			w_memcpy(&level->collisions_array[dest_cpy],  len_copy * sizeof(int32_t), &level->collisions_array[src_cpy], len_copy * sizeof(int32_t));
			w_memcpy(&level->phy_manager->collisions_array[dest_cpy], len_copy * sizeof(int32_t), &level->phy_manager->collisions_array[src_cpy], len_copy * sizeof(int32_t));
		}
		else {
			inverse_cpy(&level->collisions_array[0], dest_cpy, src_cpy, len_copy);
			inverse_cpy(&level->phy_manager->collisions_array[0], dest_cpy, src_cpy, len_copy);
		}


	}
}


static void _level_stream_objects_update(level_stream_t* const mngr, double elapsed, render_manager_t* const render_mngr, lua_State* script_mngr, wbool call_script)
{
	uint32_t o_index = 0;
	uint32_t o_count = 0;

	//update chunk bound entities first
	for (uint32_t ichunk = 0; ichunk < CHUNK_NUMBER; ichunk++)
	{
		volatile uint32_t chunk_state = S_EMPTY;

		atomic_set_value(&chunk_state, mngr->chunks[ichunk].state);

		if (chunk_state == S_LOADED && mngr->chunks[ichunk].visible)
		{

			hashmap_t* hashmap = &mngr->chunks[ichunk].entities;

			for (entity_t* c_entity = (entity_t*)hashmap_iterate(hashmap, &o_index, &o_count); c_entity != NULL; c_entity = (entity_t*)hashmap_iterate(hashmap, &o_index, &o_count))
			{
				entity_update(c_entity, (float)elapsed, render_mngr, mngr->onupdate_func);
			}

			o_index = o_count = 0;

			for (particles_entity_t* opart = (particles_entity_t*)hashmap_iterate(&mngr->chunks[ichunk].particles_list, &o_index, &o_count); opart != NULL; opart = (particles_entity_t*)hashmap_iterate(&mngr->chunks[ichunk].particles_list, &o_index, &o_count))
			{
				particles_entity_update(opart, (float)elapsed);
			}


			Tilemap_update(&mngr->chunks[ichunk].map, (float)elapsed, render_mngr);
		}
	}

	o_index = o_count = 0;
	//update world entities 

	for (entity_t* c_entity = (entity_t*)hashmap_iterate(&mngr->entities, &o_index, &o_count); c_entity != NULL; c_entity = (entity_t*)hashmap_iterate(&mngr->entities, &o_index, &o_count))
	{
		entity_update(c_entity, (float)elapsed, render_mngr,mngr->onupdate_func);
	}
	
	//call scripts events
	if (call_script && strlen(mngr->current_module) > 0 && strcmp(mngr->current_module, "") != 0)
	{
		const char* result = script_callfunc_params(script_mngr, mngr->current_module, "doEvents", "fi", elapsed, (int32_t)mngr->script_id);

		if (result != NULL)
			logprint(result);
	}


}


void level_stream_g_clean_level(level_stream_t* const level_mngr)
{
	uint32_t o_index = 0;
	uint32_t o_count = 0;

	//clean chunk bound entities first
	for (uint32_t ichunk = 0; ichunk < CHUNK_NUMBER; ichunk++)
	{
		volatile uint32_t chunk_state = S_EMPTY;

		atomic_set_value(&chunk_state, level_mngr->chunks[ichunk].state);

		if (chunk_state == S_LOADED && level_mngr->chunks[ichunk].visible)
		{

			hashmap_t* hashmap = &level_mngr->chunks[ichunk].entities;

			for (entity_t* c_entity = (entity_t*)hashmap_iterate(hashmap, &o_index, &o_count); c_entity != NULL; c_entity = (entity_t*)hashmap_iterate(hashmap, &o_index, &o_count))
			{
				entity_g_clean(c_entity);
			}

			o_index = o_count = 0;

			for (particles_entity_t* opart = (particles_entity_t*)hashmap_iterate(&level_mngr->chunks[ichunk].particles_list, &o_index, &o_count); opart != NULL; opart = (particles_entity_t*)hashmap_iterate(&level_mngr->chunks[ichunk].particles_list, &o_index, &o_count))
			{
				particles_entity_g_clean(opart);
			}

			Tilemap_g_clean(&level_mngr->chunks[ichunk].map);
		}
	}

	o_index = o_count = 0;
	//clean world entities 

	for (entity_t* c_entity = (entity_t*)hashmap_iterate(&level_mngr->entities, &o_index, &o_count); c_entity != NULL; c_entity = (entity_t*)hashmap_iterate(&level_mngr->entities, &o_index, &o_count))
	{
		entity_g_clean(c_entity);
	}
}

void level_stream_g_create_level(level_stream_t* const level_mngr, render_manager_t* const render_mngr)
{
	uint32_t o_index = 0;
	uint32_t o_count = 0;

	//clean chunk bound entities first
	for (uint32_t ichunk = 0; ichunk < CHUNK_NUMBER; ichunk++)
	{
		volatile uint32_t chunk_state = S_EMPTY;

		atomic_set_value(&chunk_state, level_mngr->chunks[ichunk].state);

		if (chunk_state == S_LOADED && level_mngr->chunks[ichunk].visible)
		{

			hashmap_t* hashmap = &level_mngr->chunks[ichunk].entities;

			for (entity_t* c_entity = (entity_t*)hashmap_iterate(hashmap, &o_index, &o_count); c_entity != NULL; c_entity = (entity_t*)hashmap_iterate(hashmap, &o_index, &o_count))
			{
				entity_g_create(c_entity, render_mngr);
			}

			o_index = o_count = 0;

			for (particles_entity_t* opart = (particles_entity_t*)hashmap_iterate(&level_mngr->chunks[ichunk].particles_list, &o_index, &o_count); opart != NULL; opart = (particles_entity_t*)hashmap_iterate(&level_mngr->chunks[ichunk].particles_list, &o_index, &o_count))
			{
				particles_entity_g_create(opart, render_mngr);
			}

			Tilemap_g_create(&level_mngr->chunks[ichunk].map, render_mngr);
		}
	}

	o_index = o_count = 0;
	//clean world entities 

	for (entity_t* c_entity = (entity_t*)hashmap_iterate(&level_mngr->entities, &o_index, &o_count); c_entity != NULL; c_entity = (entity_t*)hashmap_iterate(&level_mngr->entities, &o_index, &o_count))
	{
		entity_g_clean(c_entity);
	}
}

void level_stream_update(level_stream_t* const  level,Vector_t current_pos,double elapsed, render_manager_t* const render_mngr,lua_State* script_mngr, wbool call_script)
{
	//update our stream_map based on the current pos
	
	//-4- unload async and save to a chunk file the maps that need to move to a new position
	//-5- load async the map that need to move to a new position
	//-6- when the map that had to move are loaded, move them to the new position
	
	//-7- check which loaded map are visible / not visible and mark them
	
	stream_chunk_t* center_chunk = NULL;
	
	for(uint32_t ichunk = 0;ichunk < CHUNK_NUMBER;ichunk++)
	{
		if(level->chunks[ichunk].pos == O_CENTER)
		{
			center_chunk = &level->chunks[ichunk];
		}
	}
	
	if(center_chunk == NULL)
	{
		logprint("no center chunk!");
		return;
	}

	if (level->level_thread.initialized)
	{
		thread_run(&level->level_thread);
	}
	


	//-1- check if the current_pos is within bound of the center map
	if(!vector_in_rect_originbottomleft(&center_chunk->bounds,&current_pos) &&  !Vec_isfuzzyequal(&current_pos,&vectorzero,0.5f))
	{
		//check if load / unload operation are finished before updating the center map
		wbool stream_op_ok = wtrue;
		for (uint32_t ichunk = 0; ichunk < CHUNK_NUMBER; ichunk++)
		{
			volatile uint32_t chunk_state = S_EMPTY;

			atomic_set_value(&chunk_state, level->chunks[ichunk].state);

			if (chunk_state != S_LOADED && chunk_state != S_OUTOFBOUND) {
				stream_op_ok = wfalse;
				break;
			}
		}

		if (stream_op_ok)
		{
			center_chunk = NULL;
			//-2- if not, find the new center map
			for (uint32_t ichunk = 0; ichunk < CHUNK_NUMBER; ichunk++)
			{
				if (vector_in_rect_originbottomleft(&level->chunks[ichunk].bounds, &current_pos))
				{
					center_chunk = &level->chunks[ichunk];
					break;
				}
			}
		}
		
	}
	
	if(center_chunk == NULL)
	{
		logprint("current_pos is out of bound!");
		return;
	}

	//the center chunk has changed
	if (center_chunk->pos != O_CENTER)
	{
		int32_t src_row_start = 0;
		int32_t src_row_end = 0;
		int32_t src_col_start = 0;
		int32_t dest_row_start = 0;
		int32_t dest_col_start = 0;
		wbool inverse_copy = wfalse;
		int32_t len_cpy = level->base_col_count * 2;

		//-3- based on the new center map, prepare to reassign the stream_pos of each map
		switch (center_chunk->pos)
		{
			case O_BOTTOMLEFT:
			{

				src_row_start = (2 * level->base_row_count);
				src_col_start = 0;
				src_row_end = src_row_start - (2 * level->base_row_count);
				dest_row_start = src_row_start + level->base_row_count;
				dest_col_start = src_col_start + level->base_col_count;
				inverse_copy = wfalse;


				level->access_chunk[O_TOPLEFT]->pos = O_BOTTOMLEFT;
				level->access_chunk[O_TOP]->pos = O_BOTTOM;
				level->access_chunk[O_TOPRIGHT]->pos = O_BOTTOMRIGHT;

				level->access_chunk[O_LEFT]->pos = O_TOP;
				level->access_chunk[O_CENTER]->pos = O_TOPRIGHT;
				level->access_chunk[O_RIGHT]->pos = O_TOPLEFT;

				level->access_chunk[O_BOTTOM]->pos = O_RIGHT;
				level->access_chunk[O_BOTTOMRIGHT]->pos = O_LEFT;

				//set the chunks to unload
				level->access_chunk[O_TOPLEFT]->unload = level->access_chunk[O_TOP]->unload = level->access_chunk[O_TOPRIGHT]->unload = level->access_chunk[O_RIGHT]->unload = level->access_chunk[O_BOTTOMRIGHT]->unload = wtrue;

				break;
			}
			case O_BOTTOMRIGHT:
			{
				src_row_start = (2 * level->base_row_count);
				src_col_start = level->base_col_count;
				src_row_end = src_row_start - (2 * level->base_row_count);
				dest_row_start = src_row_start + level->base_row_count;
				dest_col_start = src_col_start - level->base_col_count;
				inverse_copy = wfalse;



				level->access_chunk[O_TOPLEFT]->pos = O_BOTTOMLEFT;
				level->access_chunk[O_TOP]->pos = O_BOTTOM;
				level->access_chunk[O_TOPRIGHT]->pos = O_BOTTOMRIGHT;

				level->access_chunk[O_LEFT]->pos = O_RIGHT;
				level->access_chunk[O_CENTER]->pos = O_TOPLEFT;
				level->access_chunk[O_RIGHT]->pos = O_TOP;

				level->access_chunk[O_BOTTOM]->pos = O_LEFT;
				level->access_chunk[O_BOTTOMLEFT]->pos = O_TOPRIGHT;

				//set the chunks to unload
				level->access_chunk[O_TOPRIGHT]->unload = level->access_chunk[O_TOP]->unload = level->access_chunk[O_TOPLEFT]->unload = level->access_chunk[O_LEFT]->unload = level->access_chunk[O_BOTTOMLEFT]->unload = wtrue;

				break;
			}
			case O_TOPRIGHT:
			{
				src_row_start = level->base_row_count;
				src_row_end = src_row_start + (2 * level->base_row_count);
				src_col_start = level->base_col_count;
				dest_row_start = 0;
				dest_col_start = 0;
				inverse_copy = wfalse;

				level->access_chunk[O_TOP]->pos = O_LEFT;
				level->access_chunk[O_TOPLEFT]->pos = O_RIGHT;

				level->access_chunk[O_LEFT]->pos = O_BOTTOMRIGHT;
				level->access_chunk[O_CENTER]->pos = O_BOTTOMLEFT;
				level->access_chunk[O_RIGHT]->pos = O_BOTTOM;

				level->access_chunk[O_BOTTOMLEFT]->pos = O_TOPLEFT;
				level->access_chunk[O_BOTTOM]->pos = O_TOP;
				level->access_chunk[O_BOTTOMRIGHT]->pos = O_TOPRIGHT;

				//set the chunks to unload
				level->access_chunk[O_TOPLEFT]->unload = level->access_chunk[O_LEFT]->unload = level->access_chunk[O_BOTTOMLEFT]->unload = level->access_chunk[O_BOTTOM]->unload = level->access_chunk[O_BOTTOMRIGHT]->unload = wtrue;
				break;
			}
			case O_TOPLEFT:
			{
				src_row_start = level->base_row_count;
				src_row_end = src_row_start + (2 * level->base_row_count);
				src_col_start = 0;
				dest_row_start = 0;
				dest_col_start = level->base_col_count;
				inverse_copy = wfalse;


				level->access_chunk[O_TOPRIGHT]->pos = O_LEFT;
				level->access_chunk[O_TOP]->pos = O_RIGHT;

				level->access_chunk[O_LEFT]->pos = O_BOTTOM;
				level->access_chunk[O_CENTER]->pos = O_BOTTOMRIGHT;
				level->access_chunk[O_RIGHT]->pos = O_BOTTOMLEFT;

				level->access_chunk[O_BOTTOMLEFT]->pos = O_TOPLEFT;
				level->access_chunk[O_BOTTOM]->pos = O_TOP;
				level->access_chunk[O_BOTTOMRIGHT]->pos = O_TOPRIGHT;

				//set the chunks to unload
				level->access_chunk[O_TOPRIGHT]->unload = level->access_chunk[O_RIGHT]->unload = level->access_chunk[O_BOTTOMRIGHT]->unload = level->access_chunk[O_BOTTOM]->unload = level->access_chunk[O_BOTTOMLEFT]->unload  = wtrue;

				break;
			}
			case O_LEFT:
			{
				src_row_start = 0;
				src_row_end = src_row_start + (3 * level->base_row_count);
				src_col_start = 0;
				dest_row_start = 0;
				dest_col_start = level->base_col_count;
				inverse_copy = wtrue;


				level->access_chunk[O_TOPRIGHT]->pos = O_TOPLEFT;
				level->access_chunk[O_TOPLEFT]->pos = O_TOP;
				level->access_chunk[O_TOP]->pos = O_TOPRIGHT;

				level->access_chunk[O_CENTER]->pos = O_RIGHT;
				level->access_chunk[O_RIGHT]->pos = O_LEFT;

				level->access_chunk[O_BOTTOM]->pos = O_BOTTOMRIGHT;
				level->access_chunk[O_BOTTOMLEFT]->pos = O_BOTTOM;
				level->access_chunk[O_BOTTOMRIGHT]->pos = O_BOTTOMLEFT;

				//set the chunks to unload
				level->access_chunk[O_TOPRIGHT]->unload = level->access_chunk[O_RIGHT]->unload = level->access_chunk[O_BOTTOMRIGHT]->unload = wtrue;
				break;
			}
			case O_RIGHT:
			{
				src_row_start = 0;
				src_row_end = src_row_start + (3 * level->base_row_count);
				src_col_start = level->base_col_count;
				dest_row_start = 0;
				dest_col_start = 0;
				inverse_copy = wfalse;


				level->access_chunk[O_TOP]->pos = O_TOPLEFT;
				level->access_chunk[O_TOPLEFT]->pos = O_TOPRIGHT;
				level->access_chunk[O_TOPRIGHT]->pos = O_TOP;

				level->access_chunk[O_CENTER]->pos = O_LEFT;
				level->access_chunk[O_LEFT]->pos = O_RIGHT;

				level->access_chunk[O_BOTTOM]->pos = O_BOTTOMLEFT;
				level->access_chunk[O_BOTTOMRIGHT]->pos = O_BOTTOM;
				level->access_chunk[O_BOTTOMLEFT]->pos = O_BOTTOMRIGHT;

				//set the chunks to unload
				level->access_chunk[O_TOPLEFT]->unload = level->access_chunk[O_LEFT]->unload = level->access_chunk[O_BOTTOMLEFT]->unload = wtrue;
				break;
			}
			case O_TOP:
			{
				src_row_start = level->base_row_count;
				src_row_end = src_row_start + (2 * level->base_row_count);
				src_col_start = 0;
				dest_row_start = 0;
				dest_col_start = 0;
				inverse_copy = wfalse;
				len_cpy = level->base_col_count * 3;

				level->access_chunk[O_TOPLEFT]->pos = O_LEFT;
				level->access_chunk[O_TOPRIGHT]->pos = O_RIGHT;

				level->access_chunk[O_LEFT]->pos = O_BOTTOMLEFT;
				level->access_chunk[O_CENTER]->pos = O_BOTTOM;
				level->access_chunk[O_RIGHT]->pos = O_BOTTOMRIGHT;

				level->access_chunk[O_BOTTOMLEFT]->pos = O_TOPLEFT;
				level->access_chunk[O_BOTTOM]->pos = O_TOP;
				level->access_chunk[O_BOTTOMRIGHT]->pos = O_TOPRIGHT;


				//set the chunks to unload
				level->access_chunk[O_BOTTOMLEFT]->unload = level->access_chunk[O_BOTTOM]->unload = level->access_chunk[O_BOTTOMRIGHT]->unload = wtrue;

				break;
			}
			case O_BOTTOM:
			{
				src_row_start = (2 * level->base_row_count);
				src_row_end = 0;
				src_col_start = 0;
				dest_row_start = (3 * level->base_row_count);
				dest_col_start = 0;
				inverse_copy = wfalse;
				len_cpy = level->base_col_count * 3;

				level->access_chunk[O_TOPLEFT]->pos = O_BOTTOMLEFT;
				level->access_chunk[O_TOP]->pos = O_BOTTOM;
				level->access_chunk[O_TOPRIGHT]->pos = O_BOTTOMRIGHT;

				level->access_chunk[O_LEFT]->pos = O_TOPLEFT;
				level->access_chunk[O_CENTER]->pos = O_TOP;
				level->access_chunk[O_RIGHT]->pos = O_TOPRIGHT;

				level->access_chunk[O_BOTTOMLEFT]->pos = O_LEFT;
				level->access_chunk[O_BOTTOMRIGHT]->pos = O_RIGHT;


				//set the chunks to unload
				level->access_chunk[O_TOP]->unload = level->access_chunk[O_TOPLEFT]->unload = level->access_chunk[O_TOPRIGHT]->unload = wtrue;
				break;
			}

		}

		center_chunk->pos = O_CENTER;

		if (level->pathfind_worker != NULL)
		{
			pathfind_worker_t* worker = (pathfind_worker_t*)level->pathfind_worker;

			if (worker->initialized)
				pathfind_startgridchange(worker);
		}
		
		//before moving collisions data, refresh level stream data from physics manager data
		w_memcpy(level->collisions_array, (level->phy_manager->col_count * level->phy_manager->row_count) * sizeof(int32_t), level->phy_manager->collisions_array, (level->phy_manager->col_count * level->phy_manager->row_count) * sizeof(int32_t));

		//move collisions data for chunk that stay loaded
		_move_collisions_data(level, src_col_start, src_row_start, src_row_end, dest_row_start, dest_col_start, len_cpy, inverse_copy);

		//reassign positions
		for (uint32_t ichunk = 0; ichunk < CHUNK_NUMBER; ichunk++)
			level->access_chunk[level->chunks[ichunk].pos] = &level->chunks[ichunk];


		//move elements around
		level_stream_reposchunks(level, center_chunk);

		level_stream_map_updated_script(level, level->script_mngr);

		if (level->pathfind_worker != NULL)
		{
			pathfind_worker_t* worker = (pathfind_worker_t*)level->pathfind_worker;

			if (worker->initialized)
				pathfind_endgridchange(worker);
		}
	}
	


    //we are loading physics data on main thread since chipmunk is not thread safe, tilemaps (texture / vertex buffers / etc.) are loaded on the secondary opengl context, on the background thread
	//only load one chunk at a time to prevent slowdown

	wbool frontloaddone = wfalse;
	wbool allloadded = wtrue;

	for (uint32_t ichunk = 0; ichunk < CHUNK_NUMBER; ichunk++)
	{
		volatile uint32_t chunk_state = S_EMPTY;

		atomic_set_value(&chunk_state, level->chunks[ichunk].state);

		if (chunk_state == S_FRONTLOADING)
		{
			if (frontloaddone && !level->first_load) {
				allloadded = wfalse;
				break;//one chunk in the same frame
			}

			Map* map_data = map__unpack(NULL, level->chunks[ichunk].content_buffer_size, level->chunks[ichunk].content_buffer);

			script_container_t container = level_stream_frontload_chunkgraph(level, &level->chunks[ichunk], map_data, ichunk);

			if (container.entity_script_len > 0 || container.text_script_len > 0) {
				atomic_set_value(&level->chunks[ichunk].state, S_SCRIPTEXEC);
				level_stream_execscripts(level, container);
			}

			atomic_set_value(&level->chunks[ichunk].state, S_GAMECHUNKBACKLOADING);
			map__free_unpacked(map_data, NULL);
			frontloaddone = wtrue;
		}
		else if (chunk_state == S_GAMECHUNKFRONTLOADING)
		{
			if (frontloaddone && !level->first_load) {
				allloadded = wfalse;
				break;//one chunk in the same frame
			}

			module_mngr_t* module_manager = (module_mngr_t*)level->module_manager;

			if (module_manager->game_chunk_loaded)
			{
				module_manager->game_chunk_loaded(&level->chunks[ichunk], wfalse);
			}

			atomic_set_value(&level->chunks[ichunk].state, S_LOADED);
			frontloaddone = wtrue;
			level->pending_new_chunk_loaded = wtrue;
		}
		else if (chunk_state == S_FRONTUNLOADING)
		{
			level_stream_unload_chunk(level,&level->chunks[ichunk]);
			atomic_set_value(&level->chunks[ichunk].state, S_UNLOADED);
		}
	}

	if (allloadded && level->pending_new_chunk_loaded) {

		level->first_load = wfalse;

		if (level->chunk_loaded_callback != NULL) {
			level->chunk_loaded_callback();
		}

		//update edges with new collisions data from loaded chunk
		gen_edges_list(level->base_row_count * CHUNK_ROW, level->base_col_count * CHUNK_COL, level->tile_size, level->collisions_array, level->edges_array, &level->preallocated_edges, level->check_col_func, level->access_chunk[O_BOTTOMLEFT]->bounds.position);

		if (level->light_manager != NULL) {
			light_manager_regenall(level->light_manager);
		}

		level->pending_new_chunk_loaded = wfalse;
	}
	

    //after all the static content is loaded, load the dynamic content (world entities)

    volatile uint32_t world_ent_state = S_EMPTY;

    atomic_set_value(&world_ent_state,level->world_entity_pool_state);

    
    if(world_ent_state == S_FRONTLOADING)
    {
        Entity* world_ent = NULL;
        wbool init_load = wtrue;       
        uint32_t read_p = 0;     


        do
        {

            if(world_ent != NULL) {
               entity_t* new_ent = level_stream_add_entity(level,NULL, level->resx_manager, level->phy_manager,world_ent->entity_id,world_ent,level->render_mngr, wtrue);

               new_ent->has_script = (wbool)(world_ent->script_ctor != NULL && strlen(world_ent->script_ctor) > 0 && !world_ent->script_init_only);
            }

           world_ent = (Entity*)circular_array_iterate(&level->temp_world_entities,&read_p,init_load);
           init_load = wfalse;
		} while (world_ent != NULL);

        //second pass to load scripts this time
        char level_script_path[256];
        char module_name[256];

        do
        {


            if(world_ent != NULL && world_ent->script_ctor != NULL && strlen(world_ent->script_ctor) > 0) {
                const char* log_return;
        	    //create entity script objects
			    //load entity module
			    w_strcpy(module_name,256,world_ent->script_module);
			    w_sprintf(level_script_path,256,"package.loaded['%s'] = nil \n require('%s')",module_name,module_name);

			    log_return  = script_execstring(level->script_mngr,level_script_path,NULL,0);

			    if(log_return != NULL)
				    logprint(log_return);


				if (!world_ent->script_init_only) {
					w_sprintf(level_script_path, 256, "add_entity(%s,'%s',%d)", world_ent->script_ctor, world_ent->entity_id,level->script_id);
				}
				else {
					//add the id as an extra param
					script_genscriptinitonlyctor(level_script_path, world_ent->entity_id, level->script_id, level_script_path, 256);
				}
			    

			    //create entity object
			    log_return = script_execstring(level->script_mngr,level_script_path,NULL,0);

			    if(log_return != NULL)
				    logprint(log_return);

				//if the object has some simulation data, we need to call it custom setup to pass binary data around, and call module init function (the init function is delayed in this case)
				if (world_ent->has_simul_data) {
					script_callfunc_params(level->script_mngr, NULL, "set_entity_simuldata", "sb", world_ent->entity_id, world_ent->simul_data.data, world_ent->simul_data.len);
				}
				else {
					//call the set_entity_simuldata function even is no simulation data, since we still have the delayed init function to call
					script_callfunc_params(level->script_mngr, NULL, "set_entity_simuldata", "sn", world_ent->entity_id);
				}

                    
             }

            world_ent = (Entity*)circular_array_get_next_element(&level->temp_world_entities);

		} while (world_ent != NULL);
    


        tempallocator_clear(level->world_entity_pool);

        atomic_set_value(&level->world_entity_pool_state,S_EMPTY);
    }
	else if (world_ent_state == S_EMPTY)
	{
		//world entities management => unload any entity that is out of bound, save it position / put into temporary world entities any entity that is now on bound
		level_stream_unload_world_entities_outofrange(level);
		atomic_set_value(&level->world_entity_pool_state, S_EMPTY);
	}


	//finally, update existing entities
	_level_stream_objects_update(level, elapsed, render_mngr, script_mngr, call_script);

}

//TODO : voir si toujours utile ?
void level_stream_doswitch(level_stream_t* stream_level)
{
	//level_stream_chunks_doswitch(stream_level);

	//if (stream_level->light_manager != NULL) {
		//update floodlight map
	//	light_manager_updatefloodfillarray(stream_level->light_manager, stream_level->collisions_array_count * sizeof(int32_t));
	//}
}

int level_stream_handlelock(sqlite3_stmt* const stmt)
{
	int sqlite_error = 0;

	sqlite_error = sqlite3_step(stmt);

	uint8_t errnum = 0;

	while (sqlite_error == SQLITE_BUSY || sqlite_error == SQLITE_LOCKED) {
		//retry
		sqlite_error = sqlite3_step(stmt);
		errnum++;

		if (errnum >= MAX_SQLITE_RETRY)
		{
			logprint("Database busy or locked, aborting after %d retry", errnum);
			return sqlite_error;
		}
	}

	return sqlite_error;
}

void level_stream_persist(level_stream_t* const level,wbool resetchunkstate)
{

	//wait for world state to be ready for saving
	volatile uint32_t world_ent_state = S_EMPTY;

	atomic_set_value(&world_ent_state, level->world_entity_pool_state);

	uint32_t iter = 0;

	while (world_ent_state != S_EMPTY && iter < 100) {
		atomic_set_value(&world_ent_state, level->world_entity_pool_state);
		iter++;
	}

	if (iter >= 100) {
		logprint("Error while attempting to save scene content / world entity pool state locked to non empty state : %d", world_ent_state);
		return;
	}

	for (uint32_t ichunk = 0; ichunk < CHUNK_NUMBER; ichunk++)
	{
		volatile uint32_t chunk_state = S_EMPTY;

		atomic_set_value(&chunk_state, level->chunks[ichunk].state);

		if (chunk_state == S_LOADED)
		{
			chunk_state = S_SAVING;
			atomic_set_value(&level->chunks[ichunk].state, chunk_state);

			Map* map_data = map__unpack(NULL, level->chunks[ichunk].content_buffer_size, level->chunks[ichunk].content_buffer);

			level_stream_sync_chunkcontent(level, &level->chunks[ichunk], map_data);

			level->chunks[ichunk].content_buffer_size = map__get_packed_size(map_data);

			level->chunks[ichunk].content_buffer = level_stream_save_chunk(level, &level->chunks[ichunk], map_data, level->chunks[ichunk].world_row, level->chunks[ichunk].world_col);
			
			if (level->chunks[ichunk].content_buffer == NULL)
			{
				logprint("Error while saving chunk %d output buffer null", ichunk);
			}

			map__free_unpacked(map_data,NULL);

			chunk_state = S_SAVED;
			atomic_set_value(&level->chunks[ichunk].state, chunk_state);
		}
	}

	//save world entities
	uint32_t worldstate = level_stream_world_entities_save(level);


	if (worldstate != S_SAVED) {
		logprint("Error while attempting to save world entities, state returned %d", worldstate);
		return;
	}


	//set chunk state back to S_LOADED so it's again available for rendering
	//set world entity pool state to empty so it's available again for loading world entities
	if (resetchunkstate)
	{
		atomic_set_value(&level->world_entity_pool_state, S_EMPTY);

		for (uint32_t ichunk = 0; ichunk < CHUNK_NUMBER; ichunk++)
		{
			volatile uint32_t chunk_state = S_LOADED;
			atomic_set_value(&level->chunks[ichunk].state, chunk_state);
		}
	}


}

//clean level stream state in case of level switching (no unloading, but change of chunk and world entities state)
void level_stream_clean_state(level_stream_t* const level,Vector_t current_pos,double simelapsed,render_manager_t* const rnd_manager,lua_State* const script_mngr)
{
	//call level update as long as we need to load world entity
	while (level->world_entity_pool_state == S_FRONTLOADING) {
		level_stream_update(level, current_pos, simelapsed, rnd_manager, script_mngr, wtrue);
	}

}

void level_stream_keepresources(level_stream_t* const level, resources_manager_t* const resx_mngr)
{
	for (uint32_t ichunk = 0; ichunk < CHUNK_NUMBER; ichunk++)
	{
		uint32_t o_index = 0;
		uint32_t o_count = 0;

		for (entity_t* c_entity = (entity_t*)hashmap_iterate(&level->chunks[ichunk].entities, &o_index, &o_count); c_entity != NULL; c_entity = (entity_t*)hashmap_iterate(&level->chunks[ichunk].entities, &o_index, &o_count))
		{
			gettexture_resx(resx_mngr, c_entity->current_texture->texture_name);

			if (c_entity->sprite)
			{
				sprite_t* fsprite = entity_getsprite(c_entity, 0);

				if (fsprite->animated)
					getanimation_resx(resx_mngr, c_entity->animation_file);

			}
		}


		o_index = 0;
		o_count = 0;

		for (text_entity_t* c_entity = (text_entity_t*)hashmap_iterate(&level->chunks[ichunk].text, &o_index, &o_count); c_entity != NULL; c_entity = (text_entity_t*)hashmap_iterate(&level->chunks[ichunk].text, &o_index, &o_count))
		{
			getfont_resx(resx_mngr, c_entity->font_object.info->font_name);
		}

		tilemap_keepresources(&level->chunks[ichunk].map, resx_mngr);
	}

	uint32_t o_index = 0;
	uint32_t o_count = 0;

	for (entity_t* c_entity = (entity_t*)hashmap_iterate(&level->entities, &o_index, &o_count); c_entity != NULL; c_entity = (entity_t*)hashmap_iterate(&level->entities, &o_index, &o_count))
	{
		gettexture_resx(resx_mngr, c_entity->current_texture->texture_name);

		if (c_entity->sprite)
		{
			sprite_t* fsprite = entity_getsprite(c_entity, 0);

			if (fsprite->animated)
				getanimation_resx(resx_mngr, c_entity->animation_file);

		}
	}
}

//since lights are a shared resources, disable or enable the lights used by this level stream rather than removing them
void level_stream_setenablelights(level_stream_t* const level,wbool enable)
{
	if (level->light_manager == NULL)
		return;

	light_manager_t* light_manager = (light_manager_t*)level->light_manager;

	uint32_t light_index = 0;
	uint32_t light_count = 0;

	for (uint32_t ichunk = 0; ichunk < CHUNK_NUMBER; ichunk++)
	{
		light_index = 0;
		light_count = 0;

		for (light_t* olight = (light_t*)hashmap_iterate(&light_manager->light_hash, &light_index, &light_count); olight != NULL; olight = (light_t*)hashmap_iterate(&light_manager->light_hash, &light_index, &light_count))
		{
			if (strcmp(olight->parent_id, level->chunks[ichunk].map_id) == 0) {
				olight->enabled = enable;
			}
		}
	}

	light_index = 0;
	light_count = 0;

	//from world entities
	for (light_t* olight = (light_t*)hashmap_iterate(&light_manager->light_hash, &light_index, &light_count); olight != NULL; olight = (light_t*)hashmap_iterate(&light_manager->light_hash, &light_index, &light_count))
	{
		if (strcmp(olight->parent_id, "world") == 0) {
			olight->enabled = enable;
		}
	}

}


void level_stream_free(level_stream_t* level)
{
	char script_remove[1000] = { 0 };
	const char* log_return = NULL;

	for (uint32_t ichunk = 0; ichunk < CHUNK_NUMBER; ichunk++)
	{
		uint32_t o_index = 0;
		uint32_t o_count = 0;
		
		for (entity_t* c_entity = (entity_t*)hashmap_iterate(&level->chunks[ichunk].entities, &o_index, &o_count); c_entity != NULL; c_entity = (entity_t*)hashmap_iterate(&level->chunks[ichunk].entities, &o_index, &o_count))
		{
			entity_freelinkconstraint(level->phy_manager, c_entity);
		}

		o_index = 0;
		o_count = 0;

		for (entity_t* c_entity = (entity_t*)hashmap_iterate(&level->chunks[ichunk].entities, &o_index, &o_count); c_entity != NULL; c_entity = (entity_t*)hashmap_iterate(&level->chunks[ichunk].entities, &o_index, &o_count))
		{	
			//remove script data
			if (c_entity->has_script)
			{
				w_sprintf(script_remove, 1000, "remove_entity('%s',%d)", c_entity->entity_id, level->script_id);

				log_return = script_execstring(level->script_mngr, script_remove, NULL, 0);

				if (log_return != NULL)
					logprint(log_return);
			}

			//free rogue char pointer (not in level data file) in case of dynamic entities
			//if (c_entity->dynamic)
				//wf_free(c_entity->entity_id);

			//free entities data

			if (level->ondelete_func != NULL) {
				level->ondelete_func(c_entity);
			}

			entity_free(level->phy_manager, c_entity);
		}

		o_index = 0;
		o_count = 0;

		for (text_entity_t* c_entity = (text_entity_t*)hashmap_iterate(&level->chunks[ichunk].text, &o_index, &o_count); c_entity != NULL; c_entity = (text_entity_t*)hashmap_iterate(&level->chunks[ichunk].text, &o_index, &o_count))
		{
				//remove script data
				if (c_entity->has_script)
				{
					w_sprintf(script_remove, 1000, "remove_text('%s',%d)", c_entity->text_id, level->script_id);

					log_return = script_execstring(level->script_mngr, script_remove, NULL, 0);

					if (log_return != NULL)
						logprint(log_return);
				}

				//free rogue char pointer (not in level data file) in case of dynamic entities
				//if (c_entity->dynamic)
					//wf_free(c_entity->text_id);

				//free text entities data
				text_entity_free(c_entity);
		}

		hashmap_free(&level->chunks[ichunk].entities);
		hashmap_free(&level->chunks[ichunk].triggers);
		hashmap_free(&level->chunks[ichunk].text);
	
	}

	uint32_t o_index = 0;
	uint32_t o_count = 0;

	for (entity_t* c_entity = (entity_t*)hashmap_iterate(&level->entities, &o_index, &o_count); c_entity != NULL; c_entity = (entity_t*)hashmap_iterate(&level->entities, &o_index, &o_count))
	{
		entity_freelinkconstraint(level->phy_manager, c_entity);
	}

	o_index = 0;
	o_count = 0;

	for (entity_t* c_entity = (entity_t*)hashmap_iterate(&level->entities, &o_index, &o_count); c_entity != NULL; c_entity = (entity_t*)hashmap_iterate(&level->entities, &o_index, &o_count))
	{
		//remove script data
		if (c_entity->has_script)
		{
			w_sprintf(script_remove, 1000, "remove_entity('%s',%d)", c_entity->entity_id, level->script_id);

			log_return = script_execstring(level->script_mngr, script_remove, NULL, 0);

			if (log_return != NULL)
				logprint(log_return);
		}

		//free rogue char pointer (not in level data file) in case of dynamic entities
		//if (c_entity->dynamic)
			//wf_free(c_entity->entity_id);

		//free entities data
		if (level->ondelete_func != NULL) {
			level->ondelete_func(c_entity);
		}

		entity_free(level->phy_manager, c_entity);
	}


	if (level->script_id != 0) {
		script_unregisterpointer(level->script_id);
		level->script_id = 0;
	}

	physics_free_tilemap_collisions(level->phy_manager,wfalse);

	if (level->edges_array != NULL) {
		wf_free(level->edges_array);
	}

	if(level->preallocated_edges != NULL)
		wf_free(level->preallocated_edges);

    if(level->chunks_db != NULL && level->chunks_db_ready)
    {
        sqlite3_close(level->chunks_db);
		sqlite3_close(level->chunks_db_background);
        level->chunks_db = NULL;
		level->chunks_db_background = NULL;
    }

    hashmap_free(&level->entities);
	wf_free(level->collisions_array);

	for(int32_t itmap = 0;itmap < CHUNK_NUMBER;itmap++)
	{
		Tilemap_Free(&level->chunks[itmap].map);
        hashmap_free(&level->chunks[itmap].text);
        hashmap_free(&level->chunks[itmap].triggers);
        hashmap_free(&level->chunks[itmap].group_objects);
		wf_free(level->chunks[itmap].phy_manager);
		tempallocator_free(level->chunks[itmap].content_pool);
		wf_free(level->chunks[itmap].content_pool);
	}

	if (level->pathfind_worker != NULL)
	{
		pathfind_worker_t* worker = (pathfind_worker_t*)level->pathfind_worker;

		if (worker->initialized)
			pathfind_freeallrequest(worker);
	}

    if(level->level_thread.initialized)
    {
        thread_free(&level->level_thread);
    }

	tempallocator_free(level->gen_data_pool);
	wf_free(level->gen_data_pool);

    circular_array_free(&level->temp_world_entities);

    tempallocator_free(level->world_entity_pool);
	wf_free(level->world_entity_pool);

	level->onupdate_func = NULL;
	level->ondraw_func = NULL;
	level->ondelete_func = NULL;
}