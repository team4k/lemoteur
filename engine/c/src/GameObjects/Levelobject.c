﻿#ifdef _MSC_VER
       extern "C" {
#endif
	#include "lauxlib.h"
	#include "lualib.h"
	#include <lua.h>

#ifdef _MSC_VER
}
#endif

#if defined(_WIN32)
    #include <windows.h>

#else
	#ifndef __USE_BSD
        	#define __USE_BSD
	#endif
        #include <unistd.h>
        #include <dlfcn.h>
	    #include <pthread.h>
#endif



#include <inttypes.h>
#include <sqlite3.h>

#include <Render.h>

#include <math.h>
#include <string.h>
//#include <stdlib.h>
#include <Engine.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>
#include <Base/geom.h>

#include <Utils/Hash.h>
#include <Utils/hashmap.h>
#include <Utils/display_list.h>

#include <Render/render_manager.h>

#include <Graphics/TextureLoader.h>
#include <GameObjects/Shaders.pb-c.h>
#include <Resx/shader_loader.h>


#include <GameObjects/Characters.pb-c.h>
#include <Graphics/Animation.h>
#include <Graphics/Sprite.h>
#include <Graphics/Mesh.h>
#include <Graphics/Light.h>
#include <Graphics/LightManager.h>

#include <Graphics/TextureRegion.h>
#include <Graphics/SpriteBatch.h>
#include <Graphics/FontLoader.h>

#include "GameObjects/Level.pb-c.h"

#include <Debug/Logprint.h>

#include <Sound/Sound.h>

#include <Resx/Resources.h>
#include <Resx/localization.h>

#include <Collisions/Collisions.h>
#include <Graphics/TilemapLayer.h>
#include <GameObjects/Tilemap.h>

#include <chipmunk/chipmunk.h>
#include <Physics/Physics.h>
#include <Physics/physics_tilemap_utils.h>



#include <GameObjects/entity.h>
#include <GameObjects/Trigger.h>
#include <Config/config.h>
#include <GameMngr/input_mngr.h>
#include <game_functions.h>
#include <GameMngr/module_mngr.h>

#include <Font/RenderText.h>

#include <GameObjects/text_entity.h>

#include <GameObjects/entity_group.h>

#ifndef EMSCRIPTEN
	#include <portaudio.h>
#else
	#include <emscripten.h>
#endif

#include <Graphics/Effects/particle_object.h>
#include <GameObjects/particles_entity.h>

#include <GameObjects/Collider.h>
#include <GameObjects/waypoint.h>

#include <Sound/snd_mngr.h>

#include <Threading/thread_func.h>

#include <Pathfinding/pathfind_worker.h>

#include <Render/render_shader_utils.h>
#include <GameObjects/map.h>

#include <GameObjects/Levelobject.h>
#include <Scripting/ScriptEngine.h>
#include <Scripting/ScriptPointers.h>

void Levelobject_Init(Levelobject_t* const level_obj,void* const p_light_manager,void* const p_sound_manager,void* const pathfind_worker)
{
	if(p_light_manager != NULL)
		level_obj->light_manager = p_light_manager;

	if(p_sound_manager != NULL)
		level_obj->sound_manager = p_sound_manager;

	if(pathfind_worker != NULL)
		level_obj->pathfind_worker = pathfind_worker;
	
	level_obj->map_hash.keys = (char**) wf_malloc(MAX_MAPS * sizeof (char*));
	level_obj->onupdate_func = NULL;
	level_obj->ondraw_func = NULL;
	level_obj->ondelete_func = NULL;

    //initialise keys array
    for (int32_t map_indx = 0; map_indx < MAX_MAPS; map_indx++)
        level_obj->map_hash.keys[map_indx] = 0;
		
    level_obj->map_hash.max_elem = MAX_MAPS;
	level_obj->map_hash.num_elem = 0;
	level_obj->menu_zoom = 1.0f;
	level_obj->menu_use_zoom = wfalse;
	level_obj->script_id = script_registerpointer((intptr_t)level_obj, LEVELOBJECT);

	level_obj->entities_hash.keys = (char**) wf_malloc(MAX_ENTITIES * sizeof (char*));

	//initialise entities keys array
	for(int32_t ent_indx = 0; ent_indx < MAX_ENTITIES;ent_indx++)
		 level_obj->entities_hash.keys[ent_indx] = 0;

	level_obj->entities_hash.max_elem = MAX_ENTITIES;
	level_obj->entities_hash.num_elem = 0;
	
	level_obj->triggers_hash.keys = (char**) wf_malloc(MAX_TRIGGER * sizeof (char*));

		//initialise triggers keys array
	for(int32_t trig_indx = 0; trig_indx < MAX_TRIGGER;trig_indx++)
		 level_obj->triggers_hash.keys[trig_indx] = 0;

	level_obj->triggers_hash.max_elem = MAX_TRIGGER;
	level_obj->triggers_hash.num_elem = 0;

	//initialise text objects keys array
	level_obj->text_hash.keys = (char**)wf_malloc(MAX_TEXT_OBJ * sizeof(char*));

	for(int32_t text_indx = 0; text_indx < MAX_TEXT_OBJ;text_indx++)
		level_obj->text_hash.keys[text_indx] = 0;

	level_obj->text_hash.max_elem = MAX_TEXT_OBJ;
	level_obj->text_hash.num_elem = 0;

	//initialise group objects key array
	level_obj->group_hash.keys = (char**)wf_malloc(MAX_GROUP * sizeof(char*));

	for(int32_t group_indx = 0; group_indx < MAX_GROUP;group_indx++)
		level_obj->group_hash.keys[group_indx] = 0;

	level_obj->group_hash.max_elem = MAX_GROUP;
	level_obj->group_hash.num_elem = 0;

	//initialise particles data
	
	hashmap_initalloc(&level_obj->particles_list,POOL_PARTICLES_OBJ,sizeof(particles_entity_t),POOL_PARTICLES_OBJ);

	//initialise waypoint data
	hashmap_initalloc(&level_obj->waypoints_list, POOL_WAYPOINT_OBJ, sizeof(waypoint_t), POOL_WAYPOINT_OBJ);

	//load collider 
	hashmap_initalloc(&level_obj->colliders_list, POOL_COLLIDER_OBJ, sizeof(collider_t), POOL_COLLIDER_OBJ);
	
	level_obj->request_load_level = wfalse;
	level_obj->request_load_binary = wfalse;
	level_obj->pending_binary = NULL;
	level_obj->request_exit = wfalse;
	level_obj->current_state = COMMON_RUN;

	level_obj->current_map = NULL;
	level_obj->slave_manager = NULL;

	level_obj->check_col_func = NULL;
	level_obj->check_col_func_editable = NULL;
	level_obj->randomizer = NULL;

	//null initialise elements in array
	memset(&level_obj->maps,0,sizeof(map_t) * MAX_MAPS);
	memset(&level_obj->entities,0,sizeof(entity_t) * MAX_ENTITIES);
	memset(&level_obj->triggers,0,sizeof(trigger_t) * MAX_TRIGGER);
	memset(&level_obj->text_objects,0,sizeof(text_entity_t) * MAX_TEXT_OBJ);
	memset(&level_obj->group_objects,0,sizeof(entity_group_t) * MAX_GROUP);

	display_list_init(level_obj->entity_display_list);
	display_list_init(level_obj->text_display_list);
	display_list_init(level_obj->entitygroup_display_list);
	display_list_init(level_obj->particles_display_list);
}

//manage maps

map_t* Levelobject_addmap(Levelobject_t* const level_obj,const int32_t tile_size,texture_info* const texturePointer,Map* oMap,Vector_t offset,wbool use_physics)
{
	char* map_id = (char*)wf_malloc((strlen(oMap->map_id) + 1) * sizeof(char));

	w_strcpy(map_id,strlen(oMap->map_id) + 1,oMap->map_id);

	int map_indx = hashtable_pushkey(&level_obj->map_hash, map_id);


	Rect_t map_info;
	map_info.width = oMap->col_count * tile_size;
	map_info.height = oMap->row_count * tile_size;
	map_info.position.x = offset.x;
	map_info.position.y = offset.y;

	Tilemap_Init(&level_obj->maps[map_indx].tilemap,tile_size,texturePointer,texturePointer->texture_name,oMap);

	level_obj->maps[map_indx].map_dimension = map_info;
	level_obj->maps[map_indx].use_physics = use_physics;


	return &level_obj->maps[map_indx];
}

void Levelobject_requestnewlevel(Levelobject_t* const level_obj,const char* new_level)
{
	w_strcpy(level_obj->pending_level,256,new_level);

	level_obj->request_load_level = wtrue;
}

void Levelobject_switchmap(Levelobject_t* const level_obj,physics_manager_t* const phy_mngr,lua_State* const script_mngr,const char* new_map)
{
	if(!hashtable_haskey(&level_obj->map_hash,new_map)) 
	{
		logprint("The current level %s doesn't contain a map named %s",level_obj->current_level,new_map);
		return;
	}

	physics_manager_t* c_phy_mngr = (level_obj->slave_manager) ? level_obj->slave_manager : phy_mngr;

	if(level_obj->current_map != NULL && level_obj->current_map->use_physics)
	{
		physics_free_tilemap_collisions(c_phy_mngr,wfalse);
	}

	int map_indx = hashtable_index(&level_obj->map_hash,new_map);

	level_obj->current_map = &level_obj->maps[map_indx];

	w_strcpy(level_obj->current_map_id,256,new_map);

	uint32_t entity_indx = 0;
	uint32_t entity_count = 0;

	while(entity_count <  level_obj->entities_hash.num_elem)
	{
		if(level_obj->entities_hash.keys[entity_indx])
		{
			if(!level_obj->entities[entity_indx].static_ent)
			{
				if(strcmp(level_obj->entities[entity_indx].ref_map_id,level_obj->current_map_id) == 0)
				{
					if(!level_obj->entities[entity_indx].in_physics_simulation)
						entity_addtospace(c_phy_mngr,&level_obj->entities[entity_indx]);

				}
				else
				{
					if(level_obj->entities[entity_indx].in_physics_simulation)
						entity_removefromspace(c_phy_mngr,&level_obj->entities[entity_indx]);
				}
			}
	
			entity_count++;
		}

		entity_indx++;
	}

	entity_indx = 0;
	entity_count = 0;

	//ajout / suppression collider à l'espace chipmunk au changement de map
	for (collider_t* ocol = (collider_t*)hashmap_iterate(&level_obj->colliders_list, &entity_indx, &entity_count); ocol != NULL; ocol = (collider_t*)hashmap_iterate(&level_obj->colliders_list, &entity_indx, &entity_count))
	{
		if (strcmp(ocol->ref_map_id, level_obj->current_map_id) == 0)
		{
			if(!ocol->in_physics_simulation)
				collider_addtospace(ocol, c_phy_mngr);
		}
		else if (ocol->in_physics_simulation)
		{
			collider_removefromspace(ocol, c_phy_mngr);
		}
	}

	if(level_obj->current_map->use_physics && level_obj->current_map->tilemap.collisions_array)
	{
		int32_t* destructible_data = NULL;

		if (level_obj->current_map->tilemap.layer_destructible != NULL)
			destructible_data = level_obj->current_map->tilemap.layer_destructible->tileArray;

		WorldRect_t dim = lvlbase_getdimension(level_obj, L_STATIC);

		build_tilemap_collisions(c_phy_mngr, destructible_data,level_obj->current_map->tilemap.collisions_array,level_obj->current_map->tilemap.col_count,level_obj->current_map->tilemap.row_count,level_obj->current_map->tilemap.tile_size,dim,1,1, level_obj->check_col_func, level_obj->check_col_func_editable);

		if(level_obj->pathfind_worker != NULL)
		{
			pathfind_worker_t* worker = (pathfind_worker_t*)level_obj->pathfind_worker;

			if(!worker->initialized)
				pathfind_init(worker,10, phy_mngr->collisions_array, phy_mngr->col_count, phy_mngr->row_count);
			else
				pathfind_updategrid(worker, phy_mngr->collisions_array, phy_mngr->col_count, phy_mngr->row_count);
		}

		if (level_obj->light_manager != NULL)
		{
			light_manager_updatefloodfillarray((light_manager_t*)level_obj->light_manager, (phy_mngr->col_count * phy_mngr->row_count) * sizeof(uint32_t));
		}

	}

	if(level_obj->sound_manager != NULL)
	{
		snd_mngr_setmapid((snd_mngr_t*)level_obj->sound_manager,new_map);
	}

	

	//call script init function if any
	if (strlen(level_obj->current_module) > 0)
	{
		char buff[260];

		w_sprintf(buff, 260, "%s.map_updated(%d)\n", level_obj->current_module, level_obj->script_id);

		const char* result = script_execstring(script_mngr, buff, NULL, 0);

		if (result != NULL)
		{
			logprint(result);
		}
	}

}

void Levelobject_requestexit(Levelobject_t* const level_obj)
{
	level_obj->request_exit = wtrue;
}

void levelobject_setmenuzoom(Levelobject_t* const level_obj,float menu_zoom,wbool menu_use_zoom)
{
	level_obj->menu_zoom = menu_zoom;
	level_obj->menu_use_zoom = menu_use_zoom;
}

void levelobject_setentitycallback(Levelobject_t* const level_obj, void(*ondraw_func)(entity_t* const currententity),void(*onupdate_func)(entity_t* const currententity), void(*ondelete_func)(entity_t* const currententity))
{
	level_obj->ondraw_func = ondraw_func;
	level_obj->onupdate_func = onupdate_func;
	level_obj->ondelete_func = ondelete_func;
}

void Levelobject_changemapid(Levelobject_t* const level_obj,char* old_map_id,char* new_map_id)
{
	int map_indx = hashtable_index(&level_obj->map_hash,old_map_id);

	char* ptr = level_obj->map_hash.keys[map_indx];
	hashtable_removekey(&level_obj->map_hash,old_map_id);
	

	int new_map_indx = hashtable_pushkey(&level_obj->map_hash,new_map_id);

	if(strcmp(level_obj->current_map_id,old_map_id) == 0)
		w_strcpy(level_obj->current_map_id,256,new_map_id);

	if(level_obj->current_map == &level_obj->maps[map_indx])
		level_obj->current_map = &level_obj->maps[new_map_indx];

	if(new_map_indx != map_indx)
	{
		level_obj->maps[new_map_indx] = level_obj->maps[map_indx];

		memset(&level_obj->maps[map_indx],0,sizeof(map_t));
	}

	

	uint32_t ent_count = 0;
	uint32_t ent_indx = 0;

	//change map id for all entities
	while(ent_count <  level_obj->entities_hash.num_elem)
	{
		if(level_obj->entities_hash.keys[ent_indx])
		{
			if(strcmp(level_obj->entities[ent_indx].ref_map_id,old_map_id) == 0)
				w_strcpy(level_obj->entities[ent_indx].ref_map_id,256,new_map_id);

			ent_count++;
		}

		ent_indx++;
	}

	ent_count = ent_indx = 0;

	while(ent_count <  level_obj->group_hash.num_elem)
	{
		if(level_obj->group_hash.keys[ent_indx])
		{
			if(strcmp(level_obj->group_objects[ent_indx].ref_map_id,old_map_id) == 0)
				w_strcpy(level_obj->group_objects[ent_indx].ref_map_id,256,new_map_id);

			ent_count++;
		}

		ent_indx++;
	}

	ent_count = ent_indx = 0;

	while(ent_count <  level_obj->text_hash.num_elem)
	{
		if(level_obj->text_hash.keys[ent_indx])
		{
			if(strcmp(level_obj->text_objects[ent_indx].ref_map_id,old_map_id) == 0)
				w_strcpy(level_obj->text_objects[ent_indx].ref_map_id,256,new_map_id);

			ent_count++;
		}

		ent_indx++;
	}

	ent_count = ent_indx = 0;

	while(ent_count <  level_obj->triggers_hash.num_elem)
	{
		if(level_obj->triggers_hash.keys[ent_indx])
		{
			if(strcmp(level_obj->triggers[ent_indx].ref_map_id,old_map_id) == 0)
				w_strcpy(level_obj->triggers[ent_indx].ref_map_id,256,new_map_id);

			ent_count++;
		}

		ent_indx++;
	}

	uint32_t part_count = 0;
	uint32_t part_index  = 0;

	for(particles_entity_t* opart = (particles_entity_t*)hashmap_iterate(&level_obj->particles_list,&part_index,&part_count);opart != NULL;opart =  (particles_entity_t*)hashmap_iterate(&level_obj->particles_list,&part_index,&part_count))
	{
		if(strcmp(opart->ref_map_id,old_map_id) == 0)
			w_strcpy(opart->ref_map_id,256,new_map_id);

	}


	wf_free(ptr);
}

void Levelobject_removemap(Levelobject_t* const level_obj,char* map_id)
{
	int map_indx =  hashtable_index(&level_obj->map_hash,map_id);

	Tilemap_Free(&level_obj->maps[map_indx].tilemap);

	memset(&level_obj->maps[map_indx],0,sizeof(map_t));

	char* ptr = level_obj->map_hash.keys[map_indx];
	hashtable_removekey(&level_obj->map_hash,map_id);
	wf_free(ptr);

	if(strcmp(level_obj->current_map_id,map_id) == 0)
	{
		level_obj->current_map = NULL;
		memset(level_obj->current_map_id,0,256);
	}

}

void Levelobject_setrandomizer_func(Levelobject_t* const level_obj, tlayer_randomizer_t randomizer)
{
	level_obj->randomizer = randomizer;
}

//manage entity group
entity_group_t* Levelobject_addentitygroup(Levelobject_t* const level_obj,GroupEntity* group_data,const char* map_id,int16_t zorder,render_manager_t* const render_manager)
{
	//check if our group have at least an entity
	if(group_data->n_id_array == 0)
	{
		logprint("Can't add a group with no entities! %s",group_data->group_id);
		return NULL;
	}

	int pos = hashtable_pushkey(&level_obj->group_hash,group_data->group_id);

	w_strcpy(level_obj->group_objects[pos].ref_map_id,128,map_id);

	entity_group_init(&level_obj->group_objects[pos],group_data->group_id,group_data->n_id_array,zorder);

	const texture_info* group_texture = NULL;

	//add our entities pointer based on the group id array
	for(uint32_t ientity = 0; ientity < group_data->n_id_array;ientity++)
	{
		char* entity_id = group_data->id_array[ientity];

		if(!hashtable_haskey(&level_obj->entities_hash,entity_id))
		{
			logprint("Can't find entity %s defined in entity group %s!",entity_id,group_data->group_id);
			continue;
		}

		int pos2 = hashtable_index(&level_obj->entities_hash,entity_id);

		if(group_texture == NULL)
			group_texture = level_obj->entities[pos2].current_texture;

		for (uint8_t ispr = 0; ispr < level_obj->entities[pos2].sprite_count; ispr++) {
			entity_group_addentity(&level_obj->group_objects[pos], level_obj->entities[pos2].sprite[ispr]);
		}
		
	}

	//finally, build our spritebatch, use the texture of the first entity linked (all entities must have the same texture when grouped)
	entity_group_createspritebatch(&level_obj->group_objects[pos],render_manager,group_texture);

	display_list_add(level_obj->entitygroup_display_list,zorder,pos);

	return &level_obj->group_objects[pos];
}

//manage entities

entity_t* Levelobject_addentity(Levelobject_t* const level_obj,resources_manager_t* const resx_mngr, physics_manager_t* const phy_mngr,char* entity_id,Entity* entity_data,const char* map_id,render_manager_t* const render_manager,wbool editor_mode,Vector_t offset)
{
	return Levelobject_addentity2(level_obj,resx_mngr,phy_mngr,entity_id,entity_data,map_id,render_manager,offset,editor_mode,wfalse);
}

entity_t* Levelobject_addentity2(Levelobject_t* const level_obj,resources_manager_t* const resx_mngr, physics_manager_t* const phy_mngr,char* entity_id,Entity* entity_data,const char* map_id,render_manager_t* const render_manager,Vector_t offset,wbool editor_mode,wbool dynamic)
{
	int pos = hashtable_pushkey(&level_obj->entities_hash,entity_id);
	wbool animated = wfalse;
	AnimationList* anim_list = NULL;

	texture_info* npc_tex = gettexture_resx(resx_mngr,entity_data->tileset);

	animated = (entity_data->start_animation && strlen(entity_data->start_animation) > 0) ? wtrue : wfalse;

	if(animated)
	{
		anim_list = getanimation_resx(resx_mngr,entity_data->animation_file);

		if(anim_list == NULL)
			logprint("Can't load animation file %s, entity %s won't be animated!",entity_data->animation_file,entity_id);
	}

	w_strcpy(level_obj->entities[pos].ref_map_id,256,map_id);



	//we don't manage group in editor mode, keep that for runtime mode
	wbool in_group = wfalse;

	if(!editor_mode)
		 in_group = (wbool)(entity_data->group_id != NULL && strcmp(entity_data->group_id,"") != 0);

	//if there is no physics handling

	Vector_t orig_pos = {(wfloat)entity_data->position->x,(wfloat)entity_data->position->y};

	if(!entity_data->no_collisions)
	{
		entity_data->position->x += (int32_t)offset.x;
		entity_data->position->y += (int32_t)offset.y;
	}

	physics_manager_t* c_phy_mngr = (level_obj->slave_manager) ? level_obj->slave_manager : phy_mngr;

	entity_init(&level_obj->entities[pos],entity_data,npc_tex,c_phy_mngr,anim_list,render_manager,in_group);


	if (entity_data->script_ctor && strcmp(entity_data->script_ctor, "") != 0 && !entity_data->script_init_only)
		level_obj->entities[pos].has_script = wtrue;
	else
	{
		level_obj->entities[pos].has_script = wfalse;

		if (entity_data->script_init_only && strlen(entity_data->script_ctor) > 0) {
			script_genscriptinitonlyctor(entity_data->script_ctor, entity_id, level_obj->script_id, level_obj->entities[pos].script_ctor, 512);
		}

	}

	level_obj->entities[pos].entity_id = entity_id;
	level_obj->entities[pos].dynamic = dynamic;

	if(!entity_data->no_collisions)
	{
		entity_data->position->x = (int32_t)orig_pos.x;
		entity_data->position->y = (int32_t)orig_pos.y;
	}

	if(animated && anim_list != NULL)
		entity_playanimation(&level_obj->entities[pos],entity_data->start_animation,0);

	if (!entity_data->has_nodraw || !entity_data->nodraw) {
		display_list_add(level_obj->entity_display_list, level_obj->entities[pos].z_order, pos);

		if (level_obj->entities[pos].render_pass != NULL && level_obj->entities[pos].num_pass > 0)
		{
			for (uint32_t ipass = 0; ipass < level_obj->entities[pos].num_pass; ipass++)
				display_list_add(level_obj->entity_display_list, level_obj->entities[pos].render_pass[ipass]->pass_zindex, pos);
		}
	}
		

	return &level_obj->entities[pos];
}


void Levelobject_keepresources(Levelobject_t* const obj,resources_manager_t* const resx_mngr)
{
	uint32_t txt_indx = 0;
	uint32_t txt_count = 0;

	while(txt_count <  obj->text_hash.num_elem)
	{
		if(obj->text_hash.keys[txt_indx])
		{
			getfont_resx(resx_mngr,obj->text_objects[txt_indx].font_object.info->font_name);

			txt_count++;
		}

		txt_indx++;
	}

	uint32_t ent_indx = 0;
	uint32_t ent_count = 0;

	while(ent_count <  obj->entities_hash.num_elem)
	{
		if(obj->entities_hash.keys[ent_indx])
		{
			gettexture_resx(resx_mngr,obj->entities[ent_indx].current_texture->texture_name);

			if (obj->entities[ent_indx].sprite)
			{
				sprite_t* fsprite = entity_getsprite(&obj->entities[ent_indx], 0);

				if(fsprite->animated)
					getanimation_resx(resx_mngr, obj->entities[ent_indx].animation_file);

			}

			ent_count++;
		}

		ent_indx++;
	}


	uint32_t map_indx = 0;
	uint32_t map_count = 0;

	while(map_count < obj->map_hash.num_elem)
	{
		if(obj->map_hash.keys[map_indx])
		{
			gettexture_resx(resx_mngr,obj->maps[map_indx].tilemap.texturePointer->texture_name);

			map_count++;
		}
	
		map_indx++;
	}

}

void Levelobject_updateentity(Levelobject_t* const level_obj,resources_manager_t* const resx_mngr, physics_manager_t* const phy_mngr,char* entity_id,Entity* entity_data,render_manager_t* const render_manager,wbool editor_mode)
{
	int pos = hashtable_index(&level_obj->entities_hash,entity_id);

	wbool animated = wfalse;
	AnimationList* anim_list = NULL;

	texture_info* npc_tex = gettexture_resx(resx_mngr,entity_data->tileset);

	animated = (entity_data->start_animation && strcmp(entity_data->start_animation,"") != 0) ? wtrue : wfalse;

	if(animated)
		anim_list = getanimation_resx(resx_mngr,entity_data->animation_file);


	//check constraint validity
	if(level_obj->entities[pos].link_constraint)
	{
		if(hashtable_haskey(&level_obj->entities_hash,level_obj->entities[pos].linked_to_id))
		{
			int pos_link = hashtable_index(&level_obj->entities_hash,level_obj->entities[pos].linked_to_id);

            if(cpConstraintGetBodyB(level_obj->entities[pos].link_constraint) != level_obj->entities[pos_link].body)
				level_obj->entities[pos].link_constraint_activated = wfalse;
		}
		else
		{
			level_obj->entities[pos].link_constraint_activated = wfalse;
		}
	}

	if(level_obj->entities[pos].render_pass != NULL && level_obj->entities[pos].num_pass > 0 && !level_obj->entities[pos].nodraw)
	{
		for(uint32_t ipass = 0; ipass < level_obj->entities[pos].num_pass;ipass++)
			display_list_remove(level_obj->entity_display_list,level_obj->entities[pos].render_pass[ipass]->pass_zindex,pos);
	}

	physics_manager_t* c_phy_mngr = (level_obj->slave_manager) ? level_obj->slave_manager : phy_mngr;

	entity_freelinkconstraint(c_phy_mngr,&level_obj->entities[pos]);
	entity_free(c_phy_mngr,&level_obj->entities[pos]);


	//we don't manage group in editor mode, keep that for runtime mode
	wbool in_group = wfalse;

	if(!editor_mode)
		 in_group = (wbool)(entity_data->group_id != NULL && strcmp(entity_data->group_id,"") != 0);

	entity_init(&level_obj->entities[pos],entity_data,npc_tex,c_phy_mngr,anim_list,render_manager,in_group);

	if(level_obj->entities[pos].render_pass != NULL && level_obj->entities[pos].num_pass > 0 && (!entity_data->has_nodraw || !entity_data->nodraw))
	{
		for(uint32_t ipass = 0; ipass < level_obj->entities[pos].num_pass;ipass++)
			display_list_add(level_obj->entity_display_list,level_obj->entities[pos].render_pass[ipass]->pass_zindex,pos);
	}

	if(animated)
		entity_playanimation(&level_obj->entities[pos],entity_data->start_animation,0);

}

void Levelobject_updatesprite_texture(Levelobject_t* const level_obj,resources_manager_t* const resx_mngr,char* updated_texture)
{
	uint32_t ent_indx = 0;
	uint32_t ent_count = 0;

	texture_info* texture_inf = gettexture_resx(resx_mngr,updated_texture);

	while(ent_count <  level_obj->entities_hash.num_elem)
	{
		if(level_obj->entities_hash.keys[ent_indx])
		{
			if(strcmp(level_obj->entities[ent_indx].current_texture->texture_name,updated_texture) == 0 || strcmp(level_obj->entities[ent_indx].current_texture->parent_name,updated_texture) == 0)
				entity_settexture(&level_obj->entities[ent_indx],texture_inf);

			ent_count++;
		}

		ent_indx++;
	}
}

void Levelobject_updatesprite_animation(Levelobject_t* const level_obj,resources_manager_t* const resx_mngr,char* updated_animation)
{
	uint32_t ent_indx = 0;
	uint32_t ent_count = 0;

	AnimationList* anim_list = getanimation_resx(resx_mngr,updated_animation);

	char texture_name[256];
    char tmp_texture_name[256];

	w_strcpy(texture_name,256,updated_animation);

	get_file_without_ext(texture_name);
    w_strcpy(tmp_texture_name,256,texture_name);
        

	w_sprintf(texture_name,256,"%s.png",tmp_texture_name);

	while(ent_count <  level_obj->entities_hash.num_elem)
	{
		if(level_obj->entities_hash.keys[ent_indx])
		{
			if (level_obj->entities[ent_indx].sprite != NULL && strcmp(level_obj->entities[ent_indx].current_texture->texture_name, texture_name) == 0)
			{
				for (uint8_t ispr = 0; ispr < level_obj->entities[ent_indx].sprite_count; ispr++) {
					sprite_updateanimation(level_obj->entities[ent_indx].sprite[ispr], anim_list);
				}

				
			}
				

			ent_count++;
		}

		ent_indx++;
	}
	
}

void Levelobject_update_pos(Levelobject_t* const level_obj,char* entity_id,int posX,int posY,GAME_TYPE type)
{

	switch(type)
	{
		case GT_ENTITY:
			if(hashtable_haskey(&level_obj->entities_hash,entity_id))
			{
				int pos = hashtable_index(&level_obj->entities_hash,entity_id);

				level_obj->entities[pos].start_position.x = posX;
				level_obj->entities[pos].start_position.y = posY;

				entity_setposition(&level_obj->entities[pos],(float)posX,(float)posY);

			}
		break;

		case GT_TRIGGER:
			if(hashtable_haskey(&level_obj->triggers_hash,entity_id))
			{

				int pos = hashtable_index(&level_obj->triggers_hash,entity_id);

				float halfwidth = abs(level_obj->triggers[pos].width) * 0.5f;
				float halfheight = abs(level_obj->triggers[pos].height) * 0.5f;

				level_obj->triggers[pos].box = cpBBNew(posX - halfwidth,posY - halfheight,posX + halfwidth,posY + halfheight);
			}
		break;

		case GT_TEXT:
			if(hashtable_haskey(&level_obj->text_hash,entity_id))
			{

				int pos = hashtable_index(&level_obj->text_hash,entity_id);

				level_obj->text_objects[pos].orig_pos.x = (wfloat)posX;
				level_obj->text_objects[pos].orig_pos.y = (wfloat)posY;
			}
			break;

		case GT_SOUND:
			if(level_obj->sound_manager != NULL)
			{
				snd_mngr_update_position((snd_mngr_t*)level_obj->sound_manager,entity_id,Vec((wfloat)posX,(wfloat)posY));
			}
		case GT_LIGHT:
			if(level_obj->light_manager != NULL)
			{
				light_t* olight =  light_manager_getlight((light_manager_t*)level_obj->light_manager,entity_id);

				if(olight != NULL)
					light_setposition(olight,Vec((wfloat)posX,(wfloat)posY));
			}
			break;
		case GT_PARTICLES:
			if(hashmap_haskey(&level_obj->particles_list,entity_id))
			{
				particles_entity_t* opart = (particles_entity_t*)hashmap_getvalue(&level_obj->particles_list,entity_id);

				particles_entity_setpos(opart,Vec((wfloat)posX,(wfloat)posY));
			}
		case GT_WAYPOINT:
			if (hashmap_haskey(&level_obj->waypoints_list, entity_id))
			{
				waypoint_t* oway = (waypoint_t*)hashmap_getvalue(&level_obj->waypoints_list, entity_id);
				waypoint_setpos(oway, (wfloat)posX, (wfloat)posY);
			}
			break;
		case GT_COLLIDER:
			if (hashmap_haskey(&level_obj->colliders_list, entity_id))
			{
				collider_t* ocol = (collider_t*)hashmap_getvalue(&level_obj->colliders_list, entity_id);
				collider_setpos(ocol, (wfloat)posX, (wfloat)posY);
			}
			break;
	}
}

void Levelobject_set_visible(Levelobject_t* const level_obj,char* entity_id,wbool visible,GAME_TYPE type)
{
	switch(type)
	{
		case GT_ENTITY:
			if(hashtable_haskey(&level_obj->entities_hash,entity_id))
			{
				int pos = hashtable_index(&level_obj->entities_hash,entity_id);

				level_obj->entities[pos].is_visible = visible;
			}
		break;

		case GT_TRIGGER:
			if(hashtable_haskey(&level_obj->triggers_hash,entity_id))
			{
				int pos = hashtable_index(&level_obj->triggers_hash,entity_id);

				level_obj->triggers[pos].is_visible = visible;
			}
		break;

		case GT_TEXT:
			if(hashtable_haskey(&level_obj->text_hash,entity_id))
			{
				int pos = hashtable_index(&level_obj->text_hash,entity_id);

				level_obj->text_objects[pos].is_visible = visible;
			}
			break;

		case GT_SOUND:
		case GT_LIGHT:
		case GT_COLLIDER:
		case GT_WAYPOINT:
			break;
		case GT_PARTICLES:
			if(hashmap_haskey(&level_obj->particles_list,entity_id))
			{
				particles_entity_t* opart = (particles_entity_t*)hashmap_getvalue(&level_obj->particles_list,entity_id);
				opart->is_visible = visible;
			}
			break;
	}

}

wbool Levelobject_updatezorder(Levelobject_t* const level_obj,const char* entity_id,int zorder,GAME_TYPE type)
{
	switch(type)
	{
		case GT_ENTITY:
			if(hashtable_haskey(&level_obj->entities_hash,entity_id))
			{
				int pos = hashtable_index(&level_obj->entities_hash,entity_id);

				if (!level_obj->entities[pos].nodraw) {
					display_list_remove(level_obj->entity_display_list, level_obj->entities[pos].z_order, pos);

					display_list_add(level_obj->entity_display_list, zorder, pos);
				}
				
				level_obj->entities[pos].z_order = zorder;

			}
		return wtrue;

		case GT_TRIGGER:
			return wfalse;
		case GT_LIGHT:
			return wfalse;
		case GT_SOUND:
			return wfalse;
		case GT_COLLIDER:
			return wfalse;
		case GT_WAYPOINT:
			return wfalse;

		case GT_TEXT:
			if(hashtable_haskey(&level_obj->text_hash,entity_id))
			{
				int pos = hashtable_index(&level_obj->text_hash,entity_id);

				display_list_remove(level_obj->text_display_list,level_obj->text_objects[pos].z_order,pos);

				display_list_add(level_obj->text_display_list,zorder,pos);

				level_obj->text_objects[pos].z_order = zorder;
			}
			return wtrue;
		case GT_PARTICLES:
			if(hashmap_haskey(&level_obj->particles_list,entity_id))
			{
				particles_entity_t* opart = (particles_entity_t*)hashmap_getvalue(&level_obj->particles_list,entity_id);
				opart->z_order = zorder;
			}
			return wtrue;
	}

	return wfalse;
}

void Levelobject_move_tostart(Levelobject_t* const level_obj, physics_manager_t* const phy_mngr)
{
	uint32_t ent_indx = 0;
	uint32_t ent_count = 0;

	physics_manager_t* c_phy_mngr = (level_obj->slave_manager) ? level_obj->slave_manager : phy_mngr;

	while(ent_count <  level_obj->entities_hash.num_elem)
	{
		if(level_obj->entities_hash.keys[ent_indx])
		{
			if(strcmp(level_obj->entities[ent_indx].ref_map_id,level_obj->current_map_id) == 0)
			{
				if(!level_obj->entities[ent_indx].static_ent && !level_obj->entities[ent_indx].no_reset)
				{
                    cpBodySetPosition(level_obj->entities[ent_indx].body,level_obj->entities[ent_indx].start_position);
					cpBodySetAngle(level_obj->entities[ent_indx].body,0);
                    cpBodySetAngularVelocity(level_obj->entities[ent_indx].body,0);
                    cpBodySetVelocity(level_obj->entities[ent_indx].body,cpvzero);


                    for(int32_t ishape = 0; ishape < level_obj->entities[ent_indx].shape_num;ishape++){
						cpShapeFilter filter = cpShapeFilterNew(CP_NO_GROUP, level_obj->entities[ent_indx].ori_layer, level_obj->entities[ent_indx].ori_mask);

                        cpShapeSetFilter(level_obj->entities[ent_indx].shape[ishape],filter);

                    }


					if (level_obj->entities[ent_indx].set_sprite_rotation && level_obj->entities[ent_indx].sprite != NULL)
					{
						for (uint8_t ispr = 0; ispr < level_obj->entities[ent_indx].sprite_count; ispr++)
						{
							sprite_setrotation(level_obj->entities[ent_indx].sprite[ispr], 0.0f);
						}
					}
						

					
					level_obj->entities[ent_indx].is_visible = wtrue;

					entity_setposition(&level_obj->entities[ent_indx],(float)level_obj->entities[ent_indx].start_position.x,(float)level_obj->entities[ent_indx].start_position.y);

					if(level_obj->entities[ent_indx].controlBody)
					{
                        cpBodySetVelocity(level_obj->entities[ent_indx].controlBody,cpvzero);
                        cpBodySetPosition(level_obj->entities[ent_indx].controlBody,level_obj->entities[ent_indx].start_position);

						entity_set_constraint(&level_obj->entities[ent_indx],c_phy_mngr,wtrue);
					}
				}
			}

			ent_count++;
		}

		ent_indx++;
	}

	ent_indx = 0;
	ent_count = 0;

	while(ent_count <  level_obj->triggers_hash.num_elem)
	{
		if(level_obj->triggers_hash.keys[ent_indx])
		{
			if(strcmp(level_obj->triggers[ent_indx].ref_map_id,level_obj->current_map_id) == 0)
			{
				level_obj->triggers[ent_indx].activated = wfalse;
			}

			ent_count++;
		}

		ent_indx++;
	}


	ent_indx = 0;
	ent_count = 0;

	while(ent_count <  level_obj->text_hash.num_elem)
	{
		if(level_obj->text_hash.keys[ent_indx])
		{
			if(strcmp(level_obj->text_objects[ent_indx].ref_map_id,level_obj->current_map_id) == 0)
			{
				level_obj->text_objects[ent_indx].bbox.position.x = level_obj->text_objects[ent_indx].orig_pos.x;
				level_obj->text_objects[ent_indx].bbox.position.y = level_obj->text_objects[ent_indx].orig_pos.y;
			}

			ent_count++;
		}

		ent_indx++;
	}

}

char* Levelobject_removeentity(Levelobject_t* const level_obj,physics_manager_t* const phy_mngr,const char* entity_id,GAME_TYPE type)
{
	char* ptr = NULL;

	physics_manager_t* c_phy_mngr = (level_obj->slave_manager) ? level_obj->slave_manager : phy_mngr;

	switch(type)
	{
		case GT_ENTITY:
			if(hashtable_haskey(&level_obj->entities_hash,entity_id))
			{
				int av_indx =  hashtable_index(&level_obj->entities_hash,entity_id);

				display_list_remove(level_obj->entity_display_list,level_obj->entities[av_indx].z_order,av_indx);

				if(level_obj->entities[av_indx].render_pass != NULL && level_obj->entities[av_indx].num_pass > 0)
				{
					for(uint32_t ipass = 0; ipass < level_obj->entities[av_indx].num_pass;ipass++)
						display_list_remove(level_obj->entity_display_list,level_obj->entities[av_indx].render_pass[ipass]->pass_zindex,av_indx);
				}

				if (level_obj->ondelete_func != NULL) {
					level_obj->ondelete_func(&level_obj->entities[av_indx]);
				}

				entity_freelinkconstraint(c_phy_mngr,&level_obj->entities[av_indx]);
				entity_free(c_phy_mngr,&level_obj->entities[av_indx]);

				memset(&level_obj->entities[av_indx],0,sizeof(entity_t));

				ptr = level_obj->entities_hash.keys[av_indx];
				hashtable_removekey(&level_obj->entities_hash,entity_id);

				
			}
		break;

		case GT_TRIGGER:
			if(hashtable_haskey(&level_obj->triggers_hash,entity_id))
			{
				int av_indx =  hashtable_index(&level_obj->triggers_hash,entity_id);


				memset(&level_obj->triggers[av_indx],0,sizeof(trigger_t));

				ptr = level_obj->triggers_hash.keys[av_indx];
				hashtable_removekey(&level_obj->triggers_hash,entity_id);
			}
		break;

		case GT_TEXT:
			if(hashtable_haskey(&level_obj->text_hash,entity_id))
			{
				int av_indx =  hashtable_index(&level_obj->text_hash,entity_id);

				display_list_remove(level_obj->text_display_list,level_obj->text_objects[av_indx].z_order,av_indx);

				text_entity_free(&level_obj->text_objects[av_indx]);

				memset(&level_obj->text_objects[av_indx],0,sizeof(text_entity_t));

				ptr = level_obj->text_hash.keys[av_indx];
				hashtable_removekey(&level_obj->text_hash,entity_id);
			}
			break;
		case GT_SOUND:
			if(level_obj->sound_manager != NULL)
				snd_mngr_removesound((snd_mngr_t*)level_obj->sound_manager, entity_id);
				
			break;
		case GT_LIGHT:
			if(level_obj->light_manager != NULL)
				light_manager_removelight((light_manager_t*) level_obj->light_manager,entity_id);
			break;
		case GT_PARTICLES:
			if(hashmap_haskey(&level_obj->particles_list,entity_id))
			{
				particles_entity_t* opart = (particles_entity_t*)hashmap_getvalue(&level_obj->particles_list,entity_id);
				int index = hashmap_getindexforkey(&level_obj->particles_list,entity_id);

				display_list_remove(level_obj->text_display_list,opart->z_order,index);

				particles_entity_free(opart,c_phy_mngr);
				hashmap_remove(&level_obj->particles_list,entity_id);
			}
			break;
		case GT_COLLIDER:
			if (hashmap_haskey(&level_obj->colliders_list, entity_id))
			{
				collider_t* ocol = (collider_t*)hashmap_getvalue(&level_obj->colliders_list, entity_id);
				collider_free(ocol,c_phy_mngr);
				hashmap_remove(&level_obj->colliders_list, entity_id);
			}
			break;

		case GT_WAYPOINT:
			if (hashmap_haskey(&level_obj->waypoints_list, entity_id))
			{
				hashmap_remove(&level_obj->waypoints_list, entity_id);
			}
	}

	return ptr;
}


//manage particles
particles_entity_t* Levelobject_addparticlesentity(Levelobject_t* const level_obj,ParticlesData* data,const char* entity_id,render_manager_t* const render_manager,lua_State* script_mngr, physics_manager_t* const phy_mngr,const char* map_id)
{

	int32_t pos = hashmap_getinsertindex(&level_obj->particles_list,entity_id);
	particles_entity_t* entity =  (particles_entity_t*)hashmap_push(&level_obj->particles_list,entity_id);

	physics_manager_t* c_phy_mngr = (level_obj->slave_manager) ? level_obj->slave_manager : phy_mngr;

	particles_entity_init(entity,data,render_manager,"prog_no_texture",NULL,script_mngr,c_phy_mngr,map_id);

	display_list_add(level_obj->particles_display_list,entity->z_order,pos);

	return entity;
}

void Levelobject_updateparticlesentity(Levelobject_t* const level_obj,ParticlesData* data,const char* entity_id,render_manager_t* const render_manager,lua_State* script_mngr, physics_manager_t* const phy_mngr,const char* map_id)
{
	particles_entity_t* entity =  (particles_entity_t*)hashmap_getvalue(&level_obj->particles_list,entity_id);

	physics_manager_t* c_phy_mngr = (level_obj->slave_manager) ? level_obj->slave_manager : phy_mngr;

	particles_entity_free(entity,c_phy_mngr);

	particles_entity_init(entity,data,render_manager,"prog_no_texture",NULL,script_mngr,c_phy_mngr,map_id);
}


char* Levelobject_changeentityid(Levelobject_t* const level_obj,char* old_entity_id,char* new_entity_id,GAME_TYPE type,Rect_t** out_rectangle,int32_t** out_zorder,void** out_entity)
{
	char* ptr = NULL;

	hashtable_t* g_hash = NULL;

	switch(type)
	{
		case GT_ENTITY:
			g_hash = &level_obj->entities_hash;
			break;
		case GT_TRIGGER:
			g_hash = &level_obj->triggers_hash;
			break;
		case GT_TEXT:
			g_hash = &level_obj->text_hash;
			break;
		case GT_LIGHT:
			if(level_obj->light_manager != NULL)
			{
				hashmap_rename(&((light_manager_t*)level_obj->light_manager)->light_hash,old_entity_id,new_entity_id);
				(*out_entity) = hashmap_getvalue(&((light_manager_t*)level_obj->light_manager)->light_hash,new_entity_id);
				ptr = new_entity_id;
				(*out_rectangle) = NULL;
				(*out_zorder) = NULL;
			}
			break;
		case GT_SOUND:
			if(level_obj->sound_manager != NULL)
			{
				g_hash = &((snd_mngr_t*)level_obj->sound_manager)->snd_hash;
			}
			break;
		case GT_PARTICLES:
		{
			hashmap_rename(&level_obj->particles_list, old_entity_id, new_entity_id);
			particles_entity_t* opart = (particles_entity_t*)hashmap_getvalue(&level_obj->particles_list, new_entity_id);
			(*out_entity) = opart;
			ptr = new_entity_id;
			(*out_rectangle) = NULL;
			(*out_zorder) = &opart->z_order;
		}
			break;
		case GT_COLLIDER:
		{
			hashmap_rename(&level_obj->colliders_list, old_entity_id, new_entity_id);
			collider_t* ocol = (collider_t*)hashmap_getvalue(&level_obj->colliders_list, new_entity_id);
			(*out_entity) = ocol;
			ptr = new_entity_id;
			(*out_rectangle) = NULL;
			(*out_zorder) = NULL;
		}
			
			break;
		case GT_WAYPOINT:
		{
			hashmap_rename(&level_obj->waypoints_list, old_entity_id, new_entity_id);
			waypoint_t* oway = (waypoint_t*)hashmap_getvalue(&level_obj->waypoints_list, new_entity_id);
			(*out_entity) = oway;
			ptr = new_entity_id;
			(*out_rectangle) = NULL;
			(*out_zorder) = NULL;
		}
			
			break;
	}


	if(g_hash != NULL && hashtable_haskey(g_hash,old_entity_id))
	{
		int av_indx = hashtable_index(g_hash,old_entity_id);

		ptr = g_hash->keys[av_indx];
		hashtable_removekey(g_hash,old_entity_id);

		//TODO callback to update entity id
	

		int new_av_indx = hashtable_pushkey(g_hash,new_entity_id);

		switch(type)
		{
			case GT_ENTITY:
				if(new_av_indx != av_indx)
				{
					level_obj->entities[new_av_indx] = level_obj->entities[av_indx];

					memset(&level_obj->entities[av_indx],0,sizeof(entity_t));
				}

				(*out_rectangle) = entity_getrenderbox(&level_obj->entities[new_av_indx]);

				(*out_zorder) = &level_obj->entities[new_av_indx].z_order;
				(*out_entity) = &level_obj->entities[new_av_indx];

				display_list_update(level_obj->entity_display_list,level_obj->entities[new_av_indx].z_order,av_indx,new_av_indx);

				level_obj->entities[new_av_indx].entity_id = new_entity_id;
				break;
			case GT_TRIGGER:
				if(new_av_indx != av_indx)
				{
					level_obj->triggers[new_av_indx] = level_obj->triggers[av_indx];

					memset(&level_obj->triggers[av_indx],0,sizeof(trigger_t));
				}

				(*out_rectangle) = &level_obj->triggers[new_av_indx].render_box;
				(*out_zorder) = NULL;
				(*out_entity) = &level_obj->triggers[new_av_indx];

				level_obj->triggers[new_av_indx].trigger_id = new_entity_id;
				break;
			case GT_TEXT:
				if(new_av_indx != av_indx)
				{
					level_obj->text_objects[new_av_indx] = level_obj->text_objects[av_indx];

					memset(&level_obj->text_objects[av_indx],0,sizeof(text_entity_t));
				}

				display_list_update(level_obj->text_display_list,level_obj->text_objects[new_av_indx].z_order,av_indx,new_av_indx);

				(*out_rectangle) = &level_obj->text_objects[new_av_indx].bbox;
				(*out_zorder) = &level_obj->text_objects[new_av_indx].z_order;
				(*out_entity) = &level_obj->text_objects[new_av_indx];


				level_obj->text_objects[new_av_indx].text_id = new_entity_id;
				break;
			case GT_SOUND:
				if(new_av_indx != av_indx)
				{
					((snd_mngr_t*)level_obj->sound_manager)->snd_array[new_av_indx] = ((snd_mngr_t*)level_obj->sound_manager)->snd_array[av_indx];
					memset(&((snd_mngr_t*)level_obj->sound_manager)->snd_array[av_indx],0,sizeof(sound_t));
				}

				(*out_entity) = &((snd_mngr_t*)level_obj->sound_manager)->snd_array[new_av_indx];
				(*out_rectangle) = NULL;
				(*out_zorder) = NULL;

				break;
			case GT_LIGHT:
			case GT_PARTICLES:
			case GT_WAYPOINT:
			case GT_COLLIDER:
				break;
		}

		
	}

	return ptr;
}

void Levelobject_drawallasmenu(Levelobject_t* const obj,render_manager_t* const render_mngr,Vector_t offset,resources_manager_t* const resx_mngr,wbool check_visible,menu_positions position)
{

	float menu_x = truncf(-render_mngr->scroll_vector.x);
	float menu_y = truncf(-render_mngr->scroll_vector.y);

	float real_screen_width = (float)render_mngr->screen_info.width;
	float real_screen_height = (float)render_mngr->screen_info.height;

	float menu_width = (float)obj->current_map->map_dimension.width;
	float menu_height = (float)obj->current_map->map_dimension.height;

	switch(position)
	{
		case MENU_TOPLEFT:
			menu_y += (real_screen_height - menu_height);
		break;

		case MENU_TOPCENTER:
			menu_x += (real_screen_width * 0.5f) - (menu_width * 0.5f);
			menu_y += (real_screen_height - menu_height);
		break;

		case MENU_TOPRIGHT:
			menu_x += (real_screen_width) - (menu_width);
			menu_y += (real_screen_height - menu_height);
		break;

		case MENU_LEFTCENTER:
			menu_y += ((real_screen_height * 0.5f) - (menu_height * 0.5f));
		break;

		case MENU_CENTER:
			menu_x += ((real_screen_width * 0.5f) - (menu_width * 0.5f));
			menu_y += ((real_screen_height * 0.5f) - (menu_height * 0.5f));
		break;

		case MENU_RIGHTCENTER:
			menu_x += (real_screen_width) - (menu_width);
			menu_y += ((real_screen_height * 0.5f) - (menu_height * 0.5f));
		break;

		case MENU_BOTTOMLEFT:
			//by default, the menu is bottom left, so no change to coordinates
		break;

		case MENU_BOTTOMCENTER:
			menu_x += ((real_screen_width * 0.5f) - (menu_width * 0.5f));
		break;

		case MENU_BOTTOMRIGHT:
			menu_x += (real_screen_width) - (menu_width);
		break;
	}

	menu_x += (float)offset.x;
	menu_y += (float)offset.y;

	Vector_t drawoffset = Vec(menu_x, menu_y);

	//obj->current_map->map_dimension.position.x = menu_x;
	//obj->current_map->map_dimension.position.y = menu_y;


	if(obj->menu_use_zoom && obj->menu_zoom != render_mngr->zoom_factor)
	{
		render_setzoomproj(render_mngr,obj->menu_zoom);
	}

	

	//draw on screen
	for(int16_t z_indx = 0; z_indx < MAX_ZINDEX;z_indx++)
	{
		//draw tilemap
		Tilemap_Draw(&obj->current_map->tilemap,z_indx,render_mngr, drawoffset,NULL,resx_mngr);

		if(obj->entity_display_list[z_indx][0] == -1 && obj->text_display_list[z_indx][0] == -1 && obj->entitygroup_display_list[z_indx][0] == -1)
			continue;

		Levelobject_DrawText(obj,render_mngr,z_indx, drawoffset,check_visible);
		Levelobject_Drawentities(obj,render_mngr,z_indx,resx_mngr, drawoffset,check_visible);
		Levelobject_Drawentitiesgroup(obj,render_mngr,z_indx, drawoffset,check_visible);
		Levelobject_DrawParticles(obj,render_mngr,z_indx, drawoffset,check_visible);

	}

	if(obj->menu_use_zoom && obj->menu_zoom != render_mngr->zoom_factor)
	{
		render_setzoomproj(render_mngr,render_mngr->zoom_factor);
	}


}

void Levelobject_Drawentities(Levelobject_t* const mngr,render_manager_t* const render_mngr,int16_t z_index,resources_manager_t* const resx_mngr,Vector_t offset,wbool check_visible)
{
	if(mngr->entity_display_list[z_index][0] == -1)
		return;

	Rect_t screen_info = render_mngr->screen_info;

	if(render_mngr->zoom_factor != 1.0f)
	{
		screen_info.width = (int16_t)(screen_info.width * render_mngr->zoom_factor);
		screen_info.height = (int16_t)(screen_info.height * render_mngr->zoom_factor);
	}

	light_manager_t* light_mngr = NULL;

	if(mngr->light_manager != NULL)
		light_mngr = (light_manager_t*)mngr->light_manager;

	render_push_matrix(render_mngr);

	for(int16_t ientity = 0; ientity < MAX_ENTITIES;ientity++)
	{
		if(mngr->entity_display_list[z_index][ientity] == -1)
			break;

		entity_t* c_entity = &mngr->entities[mngr->entity_display_list[z_index][ientity]];

		SHADERPROGRAM_ID old_prog = render_mngr->current_program;

		//check if this entity is on the current map
		if(strcmp(c_entity->ref_map_id,mngr->current_map_id) == 0)
		{
			if(c_entity->is_visible && !c_entity->in_group && (!check_visible || entity_checkrender(c_entity, screen_info)))
			{
				cpVect vec = entity_getposition(c_entity);
				Vector_t vec_p = Vec((wfloat)vec.x,(wfloat)vec.y);
				light_t* light_arr[MAX_LIGHT_PER_ELEMENT];
				Rect_t* renderbox = entity_getrenderbox(c_entity);

				int32_t num_light_a	= 0;
				
				if(light_mngr != NULL)
					num_light_a = light_manager_getinrangelight(light_mngr, renderbox,vec_p,render_mngr,light_arr);

				if(c_entity->program_id != render_get_emptyprogvalue())
				{
					if(render_get_numsubprogram(render_mngr,c_entity->program_id) > 0 && num_light_a > 0)
					{
						render_use_subprogram(render_mngr,c_entity->program_id,num_light_a-1);
					}
					else
						render_use_program(render_mngr,c_entity->program_id);
				}
				else
					render_use_program(render_mngr,old_prog);


				if(c_entity->program_id != render_get_emptyprogvalue() && c_entity->shader_parameters != NULL && c_entity->num_parameters > 0)
				{

					Vector_t mirror_val = Vec(1.0,1.0);
					float rotation = 0.0f;

					sprite_t* fsprite = entity_getsprite(c_entity, 0);

					if(c_entity->sprite)
						mirror_val = Vec((fsprite->mirror_texture_x)?WFLOAT1:-WFLOAT1,(fsprite->mirror_texture_y)?WFLOAT1:-WFLOAT1);

					if(c_entity->sprite)
						rotation = (float)deg2rad(sprite_getrotation(fsprite));

					Vector_t offset_texture = vectorzero;

					if(c_entity->sprite && fsprite->texture_pointer->virtual_texture)
					{
						offset_texture = fsprite->texture_pointer->region->position;
					}

					bind_shaders_params(light_mngr,render_mngr,c_entity->shader_parameters,c_entity->num_parameters,resx_mngr,&vec_p,&mirror_val,rotation, offset_texture,light_arr,num_light_a);

					if(render_has_passparam(render_mngr))
						render_setuniform_float(render_mngr,"pass",(float)c_entity->current_pass);
				}

				if(c_entity->static_ent || c_entity->sprite_freezed)
					entity_draw(c_entity,render_mngr,&offset,mngr->ondraw_func);
				else
					entity_draw(c_entity,render_mngr,NULL, mngr->ondraw_func);

				if(c_entity->program_id != render_get_emptyprogvalue() && c_entity->shader_parameters != NULL && c_entity->num_parameters > 0)
					unbind_shaders_params(render_mngr,c_entity->shader_parameters,c_entity->num_parameters,resx_mngr,light_mngr);
				
			}
		}

		render_use_program(render_mngr,old_prog);
	}

	render_pop_matrix(render_mngr);

}

void Levelobject_Drawentitiesgroup(Levelobject_t* const mngr,render_manager_t* const render_mngr,int16_t z_index,Vector_t offset, wbool check_visible)
{
	if(mngr->entitygroup_display_list[z_index][0] == -1)
		return;

	Rect_t screen_info = render_mngr->screen_info;
	render_reset_color(render_mngr);

	if(render_mngr->zoom_factor != 1.0f)
	{
		screen_info.width = (int16_t)(screen_info.width * render_mngr->zoom_factor);
		screen_info.height = (int16_t)(screen_info.height * render_mngr->zoom_factor);
	}

	render_push_matrix(render_mngr);

	for(int16_t ientity = 0; ientity < MAX_GROUP;ientity++)
	{
		if(mngr->entitygroup_display_list[z_index][ientity] == -1)
			break;

		entity_group_t* c_group = &mngr->group_objects[mngr->entitygroup_display_list[z_index][ientity]];

		//check if this group is on the current map
		if(strcmp(c_group->ref_map_id,mngr->current_map_id) == 0)
		{
			if(!check_visible || entity_group_checkrender(c_group,screen_info))
				entity_group_draw(c_group,render_mngr,check_visible,offset);
		}
	}

	render_pop_matrix(render_mngr);
}

void Levelobject_Drawtriggers(Levelobject_t* const mngr,render_manager_t* const render_mngr)
{
	uint32_t ent_indx = 0;
	uint32_t ent_count = 0;

	render_push_matrix(render_mngr);

	while(ent_count <  mngr->triggers_hash.num_elem)
	{
		if(mngr->triggers_hash.keys[ent_indx])
		{
			//check if this trigger is on the current map
			if(strcmp(mngr->triggers[ent_indx].ref_map_id,mngr->current_map_id) == 0)
			{
				trigger_draw(&mngr->triggers[ent_indx],render_mngr);
			}

			ent_count++;
		}

		ent_indx++;
	}
	render_pop_matrix(render_mngr);

}

void Levelobject_Drawcolliders(Levelobject_t* const mngr, render_manager_t* const render_mngr)
{
	Rect_t screen_info = render_mngr->screen_info;

	if (render_mngr->zoom_factor != 1.0f)
	{
		screen_info.width = (int16_t)(screen_info.width  * render_mngr->zoom_factor);
		screen_info.height = (int16_t)(screen_info.height * render_mngr->zoom_factor);
	}

	uint32_t ent_indx = 0;
	uint32_t ent_count = 0;

	render_push_matrix(render_mngr);

	for (collider_t* ocol = (collider_t*)hashmap_iterate(&mngr->colliders_list, &ent_indx, &ent_count); ocol != NULL; ocol = (collider_t*)hashmap_iterate(&mngr->colliders_list, &ent_indx, &ent_count))
	{
		if (strcmp(ocol->ref_map_id, mngr->current_map_id) == 0)
		{
			collider_draw(ocol, render_mngr);
		}
	}

	render_pop_matrix(render_mngr);
}

void Levelobject_DrawText(Levelobject_t* const mngr,render_manager_t* const render_mngr,int16_t z_index,Vector_t offset,wbool check_visible)
{
	if(mngr->text_display_list[z_index][0] == -1)
		return;

	Rect_t screen_info = render_mngr->screen_info;

	if(render_mngr->zoom_factor != 1.0f)
	{
		screen_info.width = (int16_t)(screen_info.width  * render_mngr->zoom_factor);
		screen_info.height = (int16_t)(screen_info.height * render_mngr->zoom_factor);
	}

	render_push_matrix(render_mngr);

	for(int16_t ientity = 0; ientity < MAX_TEXT_OBJ;ientity++)
	{
		if(mngr->text_display_list[z_index][ientity] == -1)
			break;

		text_entity_t* txt_ent = &mngr->text_objects[mngr->text_display_list[z_index][ientity]];

		//check if this text is on the current map
		if(strcmp(txt_ent->ref_map_id,mngr->current_map_id) == 0)
		{
			if(!check_visible || text_entity_checkrender(txt_ent,screen_info))
				text_entity_draw(txt_ent,render_mngr,offset);
		}
	}

	render_pop_matrix(render_mngr);
}

void Levelobject_DrawParticles(Levelobject_t* const mngr,render_manager_t* const render_mngr,int16_t z_index,Vector_t offset,wbool check_visible)
{
	if(mngr->particles_display_list[z_index][0] == -1)
		return;

	Rect_t screen_info = render_mngr->screen_info;

	if(render_mngr->zoom_factor != 1.0f)
	{
		screen_info.width = (int16_t)(screen_info.width  * render_mngr->zoom_factor);
		screen_info.height = (int16_t)(screen_info.height * render_mngr->zoom_factor);
	}

	render_push_matrix(render_mngr);

	for(uint32_t ipart = 0;ipart < mngr->particles_list.num_elem;ipart++)
	{
		if(mngr->particles_display_list[z_index][ipart] == -1)
			break;

		particles_entity_t* opart = (particles_entity_t*)mngr->particles_list.values[mngr->particles_display_list[z_index][ipart]];

		if(strcmp(opart->ref_map_id,mngr->current_map_id) == 0)
		{
			if(!check_visible || particles_entity_checkrender(opart,screen_info))
				particles_entity_draw(opart,render_mngr,offset);
		}
	}

	render_pop_matrix(render_mngr);
}

void Levelobject_checktriggers(Levelobject_t* const mngr,const cpBB entity_box,const char* entity_id)
{
	uint32_t ent_indx = 0;
	uint32_t ent_count = 0;

	while(ent_count <  mngr->triggers_hash.num_elem)
	{
		if(mngr->triggers_hash.keys[ent_indx])
		{
			if(strcmp(mngr->triggers[ent_indx].ref_map_id,mngr->current_map_id) == 0)
				trigger_update(&mngr->triggers[ent_indx],entity_box,entity_id,&mngr->current_module[0]);
	
			ent_count++;
		}

		ent_indx++;
	}
}

void Levelobject_update(Levelobject_t* const mngr,double elapsed,render_manager_t* const render_mngr,lua_State* script_mngr,wbool call_script)
{
	uint32_t ent_indx = 0;
	uint32_t ent_count = 0;

	while(ent_count <  mngr->entities_hash.num_elem)
	{
		if(mngr->entities_hash.keys[ent_indx])
		{
			if(strcmp(mngr->entities[ent_indx].ref_map_id,mngr->current_map_id) == 0)
				entity_update(&mngr->entities[ent_indx],(float)elapsed,render_mngr, mngr->onupdate_func);
	
			ent_count++;
		}

		ent_indx++;
	}

	ent_indx = ent_count = 0;

	for(particles_entity_t* opart = (particles_entity_t*)hashmap_iterate(&mngr->particles_list,&ent_indx,&ent_count);opart != NULL;opart =  (particles_entity_t*)hashmap_iterate(&mngr->particles_list,&ent_indx,&ent_count))
	{
		if(strcmp(opart->ref_map_id,mngr->current_map_id) == 0)
			particles_entity_update(opart,(float)elapsed);

	}

	
	if(mngr->current_map)
	{
		Tilemap_update(&mngr->current_map->tilemap,(float)elapsed,render_mngr);
	}


	//call scripts events
	if(call_script && strlen(mngr->current_module) > 0 && strcmp(mngr->current_module,"") != 0)
	{
		const char* result = script_callfunc_params(script_mngr,mngr->current_module,"doEvents","fi",elapsed, (int32_t)mngr->script_id);

		if(result != NULL)
				logprint(result);
	}


}

void Levelobject_updaterenderboxes(Levelobject_t* const mngr,render_manager_t* const render_mngr)
{
	uint32_t ent_indx = 0;
	uint32_t ent_count = 0;

	while(ent_count <  mngr->entities_hash.num_elem)
	{
		if(mngr->entities_hash.keys[ent_indx])
		{
			if(strcmp(mngr->entities[ent_indx].ref_map_id,mngr->current_map_id) == 0)
				entity_updaterenderbox(&mngr->entities[ent_indx],render_mngr,wfalse);
	
			ent_count++;
		}

		ent_indx++;
	}
}


void* levelobject_getfrompos(Levelobject_t* const mngr,float posX,float posY,GAME_TYPE selected_type)
{
	uint32_t ent_indx = 0;
    uint32_t text_count = 0;

	cpVect selectVect = cpv(posX,posY);

	switch(selected_type)
	{
		case GT_ENTITY:
			//pick entity base on z position
			for(int16_t z_indx = MAX_ZINDEX - 1; z_indx >= 0;z_indx--)
			{
				if(mngr->entity_display_list[z_indx][0] == -1)
					continue;

				for(int16_t ientity = 0; ientity < MAX_ENTITIES;ientity++)
				{
					if(mngr->entity_display_list[z_indx][ientity] == -1)
						break;

					entity_t* c_entity = &mngr->entities[mngr->entity_display_list[z_indx][ientity]];

					//check if this entity is on the current map
					if(strcmp(c_entity->ref_map_id,mngr->current_map_id) == 0 &&  c_entity->is_visible)
					{
						cpBB bound;

						Rect_t* rnd_box = entity_getrenderbox(c_entity);

						bound.l = rnd_box->position.x + mngr->current_map->map_dimension.position.x;
						bound.t = rnd_box->position.y + mngr->current_map->map_dimension.position.y;
						bound.r = bound.l + rnd_box->width;
						bound.b = bound.t - rnd_box->height;	

						if(cpBBContainsVect(bound,selectVect))
						{
							return c_entity;
						}
					}
				}
			}
		break;

		case GT_TEXT:

			ent_indx = 0;

			while(text_count <  mngr->text_hash.num_elem)
			{
				if(mngr->text_hash.keys[ent_indx])
				{
			
					if(strcmp(mngr->text_objects[ent_indx].ref_map_id,mngr->current_map_id) == 0 && mngr->text_objects[ent_indx].is_visible)
					{
						cpBB bound;
						bound.l = mngr->text_objects[ent_indx].bbox.position.x + mngr->current_map->map_dimension.position.x;
						bound.t = mngr->text_objects[ent_indx].bbox.position.y + mngr->current_map->map_dimension.position.y;
						bound.r = bound.l + mngr->text_objects[ent_indx].bbox.width;
						bound.b = bound.t - mngr->text_objects[ent_indx].bbox.height;	

						if(cpBBContainsVect(bound,selectVect))
						{
							return &mngr->text_objects[ent_indx];
						}
					}
					text_count++;
				}

				ent_indx++;
			}
		break;
		case GT_TRIGGER:
		case GT_LIGHT:
		case GT_SOUND:
		case GT_PARTICLES:
		case GT_WAYPOINT:
		case GT_COLLIDER:
			break;
	}

	return NULL;
}

void Levelobject_updaterenderbox(Levelobject_t* const mngr,render_manager_t* const render_mngr,const char* entity_id,GAME_TYPE type)
{
	switch(type)
	{
		case GT_ENTITY:
			if(hashtable_haskey(&mngr->entities_hash,entity_id))
			{
				int pos = hashtable_index(&mngr->entities_hash,entity_id);
				entity_updaterenderbox(&mngr->entities[pos],render_mngr,wtrue);

			}
		break;

		case GT_TRIGGER:
			if(hashtable_haskey(&mngr->triggers_hash,entity_id))
			{
				int pos = hashtable_index(&mngr->triggers_hash,entity_id);

				trigger_t* otrigger = &mngr->triggers[pos];

				otrigger->render_box.position.x = (float)otrigger->box.l;
				otrigger->render_box.position.y =  (float)otrigger->box.t;
				otrigger->render_box.width = otrigger->width;
				otrigger->render_box.height = otrigger->height;
			}
		break;

		case GT_TEXT:
			if(hashtable_haskey(&mngr->text_hash,entity_id))
			{
				int pos = hashtable_index(&mngr->text_hash,entity_id);
				text_entity_updaterenderbox(&mngr->text_objects[pos]);
			}
			break;
		case GT_LIGHT:
		case GT_SOUND:
		case GT_PARTICLES:
		case GT_COLLIDER:
		case GT_WAYPOINT: 
			break;
	}
}

Rect_t* Levelobject_getrenderbox(Levelobject_t* const mngr,const char* entity_id,GAME_TYPE type)
{
	switch(type)
	{
		case GT_ENTITY:
			if(hashtable_haskey(&mngr->entities_hash,entity_id))
			{
				int pos = hashtable_index(&mngr->entities_hash,entity_id);

				return entity_getrenderbox(&mngr->entities[pos]);
			}
		break;

		case GT_TRIGGER:
			if(hashtable_haskey(&mngr->triggers_hash,entity_id))
			{
				int pos = hashtable_index(&mngr->triggers_hash,entity_id);
				return &mngr->triggers[pos].render_box;
			}
		break;

		case GT_TEXT:
			if(hashtable_haskey(&mngr->text_hash,entity_id))
			{
				int pos = hashtable_index(&mngr->text_hash,entity_id);

				return &mngr->text_objects[pos].bbox;
			}
			break;
		case GT_LIGHT:
		case GT_SOUND:
		case GT_PARTICLES:
		case GT_COLLIDER:
		case GT_WAYPOINT:
			break;
	}

	return NULL;
}


//manage triggers

trigger_t* Levelobject_addtrigger(Levelobject_t* const level_obj,char* trigger_id,Trigger* trigger_data,const char* map_id,trigger_callback_t trigger_func)
{
	int pos = hashtable_pushkey(&level_obj->triggers_hash,trigger_id);

	w_strcpy(level_obj->triggers[pos].ref_map_id,256,map_id);

	trigger_init(&level_obj->triggers[pos],trigger_data,trigger_func,vectorzero);

	level_obj->triggers[pos].trigger_id = trigger_id;

	return &level_obj->triggers[pos];
}

void Levelobject_updatetrigger(Levelobject_t* const level_obj,char* trigger_id,Trigger* trigger_data,trigger_callback_t trigger_func)
{
	int pos = hashtable_index(&level_obj->triggers_hash,trigger_id);

	trigger_init(&level_obj->triggers[pos],trigger_data,trigger_func,vectorzero);
}

text_entity_t* levelobject_addtext(Levelobject_t* const level_obj,localization_t* const local_obj,char* text_id,TextObject* text_data,const char* map_id,resources_manager_t* const resx_mngr,render_manager_t* const render_manager)
{
	 return levelobject_addtext2(level_obj,local_obj,text_id,text_data,map_id,resx_mngr,render_manager,wfalse);
}


//manage text objects
text_entity_t* levelobject_addtext2(Levelobject_t* const level_obj,localization_t* const local_obj,char* text_id,TextObject* text_data,const char* map_id,resources_manager_t* const resx_mngr,render_manager_t* const render_manager,wbool dynamic)
{
	//get font data

	wbool antialias = wfalse;

	if(text_data->has_anti_aliasing)
		antialias = (wbool)text_data->anti_aliasing;

	font_info* finfo = getfont_resx(resx_mngr,text_data->font_name);

	if(finfo != NULL)
	{
		int pos = hashtable_pushkey(&level_obj->text_hash,text_id);

		int pos_map = hashtable_index(&level_obj->map_hash,map_id);

		Rect_t map_dimension;
		map_dimension.position.x = 0;
		map_dimension.position.y = 0;
		map_dimension.width = level_obj->maps[pos_map].map_dimension.width;
		map_dimension.height = level_obj->maps[pos_map].map_dimension.height;

		text_entity_init(&level_obj->text_objects[pos],local_obj,text_data,map_id,text_id,finfo,map_dimension,render_manager);

		level_obj->entities[pos].has_script = wfalse;

		if (text_data->script_init_only && strlen(text_data->script_ctor) > 0) {
			script_genscriptinitonlyctor(text_data->script_ctor, text_id, level_obj->script_id, level_obj->text_objects[pos].script_ctor, 512);
		}

		display_list_add(level_obj->text_display_list,level_obj->text_objects[pos].z_order,pos);

		level_obj->text_objects[pos].dynamic = dynamic;

		return &level_obj->text_objects[pos];
	}
	else
	{
		logprint("the font %s doesn't exist in resources list!",text_data->font_name);
		return NULL;
	}
}

void levelobject_updatetext(Levelobject_t* const level_obj,localization_t* const local_obj,char* text_id,TextObject* text_data,const char* map_id,resources_manager_t* const resx_mngr,render_manager_t* const render_mngr)
{
	if(hashtable_haskey(&level_obj->text_hash,text_id))
	{
		int pos = hashtable_index(&level_obj->text_hash,text_id);

		wbool antialias = wfalse;

		if(text_data->has_anti_aliasing)
			antialias = (wbool)text_data->anti_aliasing;

		//get font data
		font_info* finfo = getfont_resx(resx_mngr,text_data->font_name);

		if(finfo == NULL)
		{
			logprint("the font %s doesn't exist in resources list!",text_data->font_name);
			return;
		}

		text_entity_free(&level_obj->text_objects[pos]);

		int pos_map = hashtable_index(&level_obj->map_hash,map_id);

		Rect_t map_dimension;
		map_dimension.position.x = 0;
		map_dimension.position.y = 0;
		map_dimension.width = level_obj->maps[pos_map].tilemap.col_count * level_obj->maps[pos_map].tilemap.tile_size;
		map_dimension.height = level_obj->maps[pos_map].tilemap.row_count * level_obj->maps[pos_map].tilemap.tile_size;

		text_entity_init(&level_obj->text_objects[pos],local_obj,text_data,map_id,level_obj->text_hash.keys[pos],finfo,map_dimension,render_mngr);
	}
	else
	{
		levelobject_addtext(level_obj,local_obj,text_id,text_data,map_id,resx_mngr,render_mngr);
	}
}

//manage collider
collider_t* levelobject_addcollider(Levelobject_t* const level_obj, Collider* collider_data, physics_manager_t* phy_mngr,const char* map_id)
{
	collider_t* tmp_collider = (collider_t*)hashmap_push(&level_obj->colliders_list, collider_data->id);
	char* const ptr = hashmap_getkeypointer(&level_obj->colliders_list, collider_data->id);
	collider_init(tmp_collider, collider_data, phy_mngr,map_id,ptr);
	return tmp_collider;
}

void levelobject_updatecollider(Levelobject_t* const level_obj, const char* collider_id, Collider* collider_data, physics_manager_t* phy_mngr)
{
	collider_t* tmp_collider = (collider_t*)hashmap_getvalue(&level_obj->colliders_list, collider_id);

	collider_free(tmp_collider, phy_mngr);

	char* const ptr = hashmap_getkeypointer(&level_obj->colliders_list, collider_id);

	collider_init(tmp_collider, collider_data, phy_mngr, tmp_collider->ref_map_id,ptr);
}

//manage waypoint
waypoint_t* levelobject_addwaypoint(Levelobject_t* const level_obj, Waypoint* waypoint_data,const char* map_id)
{
	waypoint_t* allocated_ptr = (waypoint_t*)hashmap_push(&level_obj->waypoints_list, waypoint_data->id);
	waypoint_init(allocated_ptr, waypoint_data,map_id);
	return allocated_ptr;
}

wbool levelobject_haswaypoint(Levelobject_t* const level_obj, const char* waypointid)
{
	return (wbool)hashmap_haskey(&level_obj->waypoints_list, waypointid);
}

Vector_t levelobject_getwaypoint(Levelobject_t* const level_obj, const char* waypointid)
{
	if (hashmap_haskey(&level_obj->waypoints_list, waypointid))
	{
		waypoint_t* tmp_way = (waypoint_t*)hashmap_getvalue(&level_obj->waypoints_list, waypointid);
		return waypoint_getpos(tmp_way);
	}

	return vectorzero;
}

void levelobject_updatewaypoint(Levelobject_t* const level_obj, const char* waypoint_id, Waypoint* waypoint_data)
{
	waypoint_t* tmp_way = (waypoint_t*)hashmap_getvalue(&level_obj->waypoints_list, waypoint_id);
	waypoint_setpos(tmp_way, (wfloat)waypoint_data->position->x, (wfloat)waypoint_data->position->y);
}


void Levelobject_Free(Levelobject_t* level_obj)
{
	if (level_obj->script_id != 0) {
		script_unregisterpointer(level_obj->script_id);
		level_obj->script_id = 0;
	}

	level_obj->onupdate_func = NULL;
	level_obj->ondraw_func = NULL;
	level_obj->ondelete_func = NULL;

	wf_free(level_obj->map_hash.keys);
	level_obj->map_hash.keys = NULL;
	level_obj->map_hash.num_elem = 0;
	level_obj->map_hash.max_elem = 0;

	wf_free(level_obj->entities_hash.keys);
	level_obj->entities_hash.keys = NULL;
	level_obj->entities_hash.num_elem = 0;
	level_obj->entities_hash.max_elem = 0;

	wf_free(level_obj->triggers_hash.keys);
	level_obj->triggers_hash.keys = NULL;
	level_obj->triggers_hash.num_elem = 0;
	level_obj->triggers_hash.max_elem = 0;

	wf_free(level_obj->group_hash.keys);
	level_obj->group_hash.keys = NULL;
	level_obj->group_hash.num_elem = 0;
	level_obj->group_hash.max_elem = 0;

	//initialise text objects keys array
	wf_free(level_obj->text_hash.keys);
	level_obj->text_hash.keys = NULL;
	level_obj->text_hash.num_elem = 0;
	level_obj->text_hash.max_elem = 0;

	hashmap_free(&level_obj->particles_list);

	hashmap_free(&level_obj->waypoints_list);
	hashmap_free(&level_obj->colliders_list);

	memset(&level_obj->maps,0,sizeof(map_t) * MAX_MAPS);
	memset(&level_obj->entities,0,sizeof(entity_t) * MAX_ENTITIES);
	memset(&level_obj->triggers,0,sizeof(trigger_t) * MAX_TRIGGER);
	memset(&level_obj->text_objects,0,sizeof(text_entity_t) * MAX_TEXT_OBJ);
	memset(&level_obj->group_objects,0,sizeof(entity_group_t) * MAX_GROUP);

	display_list_init(level_obj->entity_display_list);
	display_list_init(level_obj->text_display_list);
	display_list_init(level_obj->entitygroup_display_list);

	memset(level_obj->current_level,0,sizeof(level_obj->current_level));
	memset(level_obj->current_map_id,0,sizeof(level_obj->current_map_id));
	level_obj->current_map = NULL;
	level_obj->light_manager = NULL;

	if(level_obj->slave_manager)
	{
		physics_free(level_obj->slave_manager);
		wf_free(level_obj->slave_manager);
		level_obj->slave_manager = NULL;
	}

}

int Levelobject_load_level(Levelobject_t* const level_mngr,
							resources_manager_t* const resx_mngr,
							module_mngr_t* module_mngr,
							physics_manager_t* const phy_mngr,  
							lua_State* script_mngr,
							localization_t* const local_obj,
							render_manager_t* const render_mngr,
							const char* level_name,
							trigger_callback_t trigger_func)
{
	Level* pb_level = getlevel_resx(resx_mngr,level_name);

	  //check level
    if(!pb_level)
    {
        logprint("Erreur chargement niveau ! %s ",level_name);
        return 0;
    }

	int result = Levelobject_load_level2(level_mngr,resx_mngr,module_mngr,phy_mngr,script_mngr,local_obj,render_mngr,pb_level,trigger_func,vectorzero,NULL);

	return result;
}

int Levelobject_load_level_as_menu(Levelobject_t* const level_mngr,
							resources_manager_t* const resx_mngr,
							module_mngr_t* module_mngr,
							physics_manager_t* const phy_mngr,  
							lua_State* script_mngr,
							localization_t* const local_obj,
							render_manager_t* const render_mngr,
							Level* pb_level,
							trigger_callback_t trigger_func,
							Vector_t offset,
							const char* start_map)
{
	if(level_mngr->slave_manager == NULL)
		level_mngr->slave_manager = (physics_manager_t*)wf_calloc(1,sizeof(physics_manager_t));

	create_slave_manager(level_mngr->slave_manager,phy_mngr);

	return Levelobject_load_level2(level_mngr,resx_mngr,module_mngr,level_mngr->slave_manager,script_mngr,local_obj,render_mngr,pb_level,trigger_func,offset,start_map);
	
}


int Levelobject_load_level2(Levelobject_t* const level_mngr,
							resources_manager_t* const resx_mngr,
							module_mngr_t* module_mngr,
							physics_manager_t* const phy_mngr,  
							lua_State* script_mngr,
							localization_t* const local_obj,
							render_manager_t* const render_mngr,
							Level* pb_level,
							trigger_callback_t trigger_func,
							Vector_t offset,
							const char* start_map)
{

	if(module_mngr != NULL)
		module_mngr->game_player_reposition(cpv(pb_level->map_array[0]->editor_player_pos->x,pb_level->map_array[0]->editor_player_pos->y));

	//we always set the first map as the current map
	char first_map[256];
	w_strcpy(first_map,256,pb_level->map_array[0]->map_id);

	w_strcpy(level_mngr->current_level,256,pb_level->level_id);

	light_manager_t* c_light_manager = NULL;

	if(level_mngr->light_manager != NULL)
		c_light_manager = (light_manager_t*)level_mngr->light_manager;

	snd_mngr_t* c_sound_manager = NULL;

	if(level_mngr->sound_manager != NULL)
		c_sound_manager = (snd_mngr_t*)level_mngr->sound_manager;

	pathfind_worker_t* c_path_worker = NULL;

	if(level_mngr->pathfind_worker != NULL)
		c_path_worker = (pathfind_worker_t*)level_mngr->pathfind_worker;

	
    //array to store script functions
	Entity** entity_script = NULL;
	int32_t entity_script_len = 0;
	int32_t entity_script_indx = 0;

	TextObject** text_script = NULL;
	int32_t text_script_len = 0;
	int32_t text_script_indx = 0;

	int32_t tile_size = pb_level->tile_size;

	if(tile_size <= 0)
		tile_size = 32;

	 //create level based on protobuf data
	for(uint32_t imap = 0; imap < pb_level->n_map_array;imap++)
	{
		//get map texture name
		char* file_name = (char*)wf_malloc((strlen(pb_level->map_array[imap]->tileset_id) + 1) * sizeof(char));

		get_file_name(pb_level->map_array[imap]->tileset_id,file_name,wfalse);
                
        char tmp_file_name[256];
        w_strcpy(tmp_file_name,256,file_name);

		w_sprintf(file_name,(strlen(pb_level->map_array[imap]->tileset_id) + 1),"%s%s",&tmp_file_name[0],".png");
	
		logprint("Adding Map %s with tileset %s",pb_level->map_array[imap]->map_id,file_name);

		texture_info* tileset = gettexture_resx(resx_mngr,&file_name[0]);

		//check tileset texture
		if(!tileset)
		{
			logprint("Error loading tileset %s for tilemap %s!",file_name,pb_level->map_array[imap]->map_id);
			wf_free(file_name);
			file_name = NULL;
			return 0;
		}

		//add map to level object
		map_t* tmap =  Levelobject_addmap(level_mngr,tile_size,tileset,pb_level->map_array[imap],offset,wfalse);
		
		//the first map is our current map
		if(imap == 0)
			level_mngr->current_map = tmap;

		wf_free(file_name);
		file_name = NULL;


		//add layers to map
		for(uint32_t i = 0; i < pb_level->map_array[imap]->n_layer_array; i++)
		{
			//layer texture
			texture_info* layer_texture = NULL;

			if(pb_level->map_array[imap]->layer_array[i]->tileset_id == NULL)
				layer_texture = tileset;
			else
			{
				char layer_texture_name[256];
                char tmp_layer_texture_name[256];

				if (str_has_slash(pb_level->map_array[imap]->layer_array[i]->tileset_id))
				{
					get_file_name(pb_level->map_array[imap]->layer_array[i]->tileset_id, layer_texture_name, wfalse);

					w_strcpy(tmp_layer_texture_name, 256, layer_texture_name);

					w_sprintf(layer_texture_name, 256, "%s%s", &tmp_layer_texture_name[0], ".png");
				}
				else
				{
					w_strcpy(layer_texture_name, 256, pb_level->map_array[imap]->layer_array[i]->tileset_id);
				}

				layer_texture = gettexture_resx(resx_mngr,layer_texture_name);
			}


			//normal layer contains our tilemap data (background and foreground static or animated data)
			if(pb_level->map_array[imap]->layer_array[i]->type == TYPE_LAYER__NORMAL)
				Tilemap_AddLayer(&tmap->tilemap,render_mngr,pb_level->map_array[imap]->layer_array[i],layer_texture, level_mngr->randomizer);
			else if(pb_level->map_array[imap]->layer_array[i]->type == TYPE_LAYER__COLLISIONS && !pb_level->map_array[imap]->layer_array[i]->editor_only)
			{
				Tilemap_AddCollisions(&tmap->tilemap,pb_level->map_array[imap]->layer_array[i]);
					tmap->use_physics = wtrue;
			}
			else if(pb_level->map_array[imap]->layer_array[i]->type == TYPE_LAYER__OBJECTS)//object layer contains trigger / npc / entities and text objects
			{

				//loading trigger
				for(uint32_t itrig = 0; itrig < pb_level->map_array[imap]->layer_array[i]->n_trigger_array; itrig++)
				{
					Trigger* tmp_trig = pb_level->map_array[imap]->layer_array[i]->trigger_array[itrig];
					Levelobject_addtrigger(level_mngr,tmp_trig->trigger_id,tmp_trig,pb_level->map_array[imap]->map_id,trigger_func);
				}

				 entity_script_len += pb_level->map_array[imap]->layer_array[i]->n_entity_array;

				if(entity_script == NULL)
				{
					if(entity_script_len > 0)
						entity_script = (Entity**)wf_malloc(sizeof(Entity*) * entity_script_len);
				}
				else
				{
					entity_script = (Entity**)wf_realloc(entity_script,sizeof(Entity*) * entity_script_len);
				}


				//loading entities
				for(uint32_t ient = 0; ient <  pb_level->map_array[imap]->layer_array[i]->n_entity_array;ient++)
				{
					Entity* tmp_ent =  pb_level->map_array[imap]->layer_array[i]->entity_array[ient];
					entity_t* new_entity = Levelobject_addentity(level_mngr,resx_mngr,phy_mngr,tmp_ent->entity_id,tmp_ent,pb_level->map_array[imap]->map_id,render_mngr,wfalse,tmap->map_dimension.position);

					if(tmp_ent->script_ctor != NULL && strlen(tmp_ent->script_ctor) > 0)
					{
						entity_script[entity_script_indx++] = tmp_ent;
						new_entity->has_script = (wbool)!tmp_ent->script_init_only;
						w_strcpy(new_entity->script_ctor, 512, tmp_ent->script_ctor);

						if(!new_entity->has_script) {
							script_genscriptinitonlyctor(tmp_ent->script_ctor, tmp_ent->entity_id,level_mngr->script_id, new_entity->script_ctor, 512);
						}
						
					}
					else
					{
						entity_script[entity_script_indx++] = NULL;
						new_entity->has_script = wfalse;
						memset(new_entity->script_ctor, 0, 512);
					}
				}

				//add our entity group after all entities have been processed
				for(uint32_t igroup = 0; igroup < pb_level->map_array[imap]->layer_array[i]->n_group_array;igroup++)
				{
					GroupEntity* tmp_group = pb_level->map_array[imap]->layer_array[i]->group_array[igroup];
					Levelobject_addentitygroup(level_mngr,tmp_group,pb_level->map_array[imap]->map_id,pb_level->map_array[imap]->layer_array[i]->order,render_mngr);
				}

				text_script_len += pb_level->map_array[imap]->layer_array[i]->n_text_array;

				if(text_script == NULL)
				{
					if(text_script_len > 0)
						text_script = (TextObject**)wf_malloc(sizeof(TextObject*) * text_script_len);
				}
				else
				{
					text_script = (TextObject**)wf_realloc(text_script,sizeof(TextObject*) * text_script_len);
				}

				//loading text objects
				for(uint32_t itxt = 0; itxt <  pb_level->map_array[imap]->layer_array[i]->n_text_array;itxt++)
				{
					TextObject* tmp_txt =  pb_level->map_array[imap]->layer_array[i]->text_array[itxt];
					text_entity_t* new_txt = levelobject_addtext(level_mngr,local_obj,tmp_txt->text_id,tmp_txt,pb_level->map_array[imap]->map_id,resx_mngr,render_mngr);

					if(tmp_txt->script_ctor != NULL && strlen(tmp_txt->script_ctor) > 0)
					{
						text_script[text_script_indx++] = tmp_txt;
						new_txt->has_script = (wbool)!tmp_txt->script_init_only;
						w_strcpy(new_txt->script_ctor, 512, tmp_txt->script_ctor);

						if (!new_txt->has_script) {
							script_genscriptinitonlyctor(tmp_txt->script_ctor, tmp_txt->text_id, level_mngr->script_id, new_txt->script_ctor, 512);
						}
						

					}
					else
					{
						text_script[text_script_indx++] = NULL;
						new_txt->has_script = wfalse;
						memset(new_txt->script_ctor, 0, 512);
					}
				}

				//loading particles objects
				for(uint32_t i_part = 0;i_part < pb_level->map_array[imap]->layer_array[i]->n_particles_array;i_part++)
				{
					ParticlesData* tmp_part = pb_level->map_array[imap]->layer_array[i]->particles_array[i_part];
					Levelobject_addparticlesentity(level_mngr,tmp_part,tmp_part->id,render_mngr,script_mngr,phy_mngr,pb_level->map_array[imap]->map_id);
				}

				//keep track waypoint data
				for (uint32_t i_way = 0; i_way < pb_level->map_array[imap]->layer_array[i]->n_waypoints; i_way++)
				{
					Waypoint* tmp_way = pb_level->map_array[imap]->layer_array[i]->waypoints[i_way];
					
					levelobject_addwaypoint(level_mngr, tmp_way, pb_level->map_array[imap]->map_id);
				}

				//load collider
				for (uint32_t i_way = 0; i_way < pb_level->map_array[imap]->layer_array[i]->n_colliders; i_way++)
				{
					levelobject_addcollider(level_mngr, pb_level->map_array[imap]->layer_array[i]->colliders[i_way], phy_mngr, pb_level->map_array[imap]->map_id);
				}


				if(c_sound_manager != NULL)
				{
					//loading sound object
					for(uint32_t isnd = 0;isnd < pb_level->map_array[imap]->layer_array[i]->n_sound_array;isnd++)
					{
						SoundSource* tmp_snd = pb_level->map_array[imap]->layer_array[i]->sound_array[isnd];
						Vector_t position = {(wfloat)tmp_snd->position->x,(wfloat)tmp_snd->position->y};
						snd_mngr_playsound(c_sound_manager,getsound_resx(resx_mngr,tmp_snd->snd_file_name),wtrue,tmp_snd->sound_id,wtrue,wfalse,2,position,wtrue,tmp_snd->falloffarea,pb_level->map_array[imap]->map_id);
					}
				}

				if(c_light_manager != NULL)
				{
					//loading light object
					for(uint32_t ilight = 0;ilight < pb_level->map_array[imap]->layer_array[i]->n_light_array;ilight++)
					{
						LightSource* tmp_light = pb_level->map_array[imap]->layer_array[i]->light_array[ilight];

						Vector_t position = {(wfloat)tmp_light->position->x,(wfloat)tmp_light->position->y};
						ColorA_t color = convertint_tocolor(tmp_light->light_color->r,tmp_light->light_color->g,tmp_light->light_color->b,tmp_light->light_color->a);
						
						light_t* new_light = light_manager_addlight(c_light_manager,position,color,tmp_light->radius,tmp_light->light_id,pb_level->map_array[imap]->map_id,(light_type)tmp_light->l_type);

						light_setattenuation(new_light,tmp_light->constant,tmp_light->linear,tmp_light->quadratic);
						new_light->power_factor = tmp_light->power_factor;


						if(new_light->l_type == light_directional)
						{
							light_setrotationangle(new_light,tmp_light->rotation_angle);
							light_setopenangle(new_light,tmp_light->open_angle);
						}

					}
				}

				
			}
		 
			if(pb_level->map_array[imap]->layer_array[i]->destructible)
				Tilemap_setdestructiblelayer(&tmap->tilemap,pb_level->map_array[imap]->layer_array[i]->layer_id,pb_level->map_array[imap]->layer_array[i]->destruct_tile_id);

		
		}


		//generate edge list
		if(tmap->tilemap.collisions_array != NULL)
			Tilemap_gen_edge_list(&tmap->tilemap,wfalse,level_mngr->check_col_func);

	}


	
	Levelobject_move_tostart(level_mngr,phy_mngr);

	//tell on which map we are
	Levelobject_switchmap(level_mngr,phy_mngr,script_mngr,(start_map) ? start_map : first_map);

	if (c_light_manager != NULL) {
		//update floodlight map
		light_manager_updatefloodfillarray(c_light_manager, (level_mngr->current_map->tilemap.col_count * level_mngr->current_map->tilemap.row_count) * sizeof(int32_t));
	}
		

	char level_script_path[1000];
	
	//-3- if a script file exist, load it

	if(pb_level->script_file != NULL && strlen(pb_level->script_file) > 0)
	{
		const char* log_return;
		
		char module_name[260];
		w_strcpy(module_name,260,pb_level->script_file);
		get_file_without_ext(module_name);
		//load script file
		w_sprintf(level_script_path,1000,"package.loaded['%s'] = nil \n require('%s')",module_name,module_name);


		log_return  = script_execstring(script_mngr,level_script_path,NULL,0);

		if(log_return != NULL)
			logprint(log_return);
		else
		{
			logprint("Module %s chargé",module_name);
			w_strcpy(level_mngr->current_module,256,module_name);
		}

		//init Default level ptr if not in menu context
		if (level_mngr->slave_manager == NULL) {
			
			w_sprintf(level_script_path, 1000, "init_default_pointer(%d)", level_mngr->script_id);


			log_return = script_execstring(script_mngr, level_script_path, NULL, 0);

			if (log_return != NULL)
				logprint(log_return);
			else
			{
				logprint("Pointer par défaut initialisé");
			}
		}

		//create entity script objects
		for(int ise = 0; ise < entity_script_len; ise++)
		{
			if(entity_script[ise])
			{
				//load entity module
				w_strcpy(module_name,260,entity_script[ise]->script_module);
				w_sprintf(level_script_path,1000,"package.loaded['%s'] = nil \n require('%s')",module_name,module_name);

				log_return  = script_execstring(script_mngr,level_script_path,NULL,0);

				if(log_return != NULL)
					logprint(log_return);


				if (!entity_script[ise]->script_init_only) {
					w_sprintf(level_script_path, 1000, "add_entity(%s,'%s',%d)", entity_script[ise]->script_ctor, entity_script[ise]->entity_id, level_mngr->script_id);
				}
				else {
					script_genscriptinitonlyctor(entity_script[ise]->script_ctor, entity_script[ise]->entity_id, level_mngr->script_id, level_script_path, 1000);
				}
				

				//create entity object
				log_return = script_execstring(script_mngr,level_script_path,NULL,0);

				if(log_return != NULL)
					logprint(log_return);
			}
		}

		//create text object scripts
		for(int ist = 0; ist < text_script_len; ist++)
		{
			if(text_script[ist])
			{
				//load entity module
				w_strcpy(module_name,260,text_script[ist]->script_module);
				w_sprintf(level_script_path,1000,"package.loaded['%s'] = nil \n require('%s')",module_name,module_name);

				log_return  = script_execstring(script_mngr,level_script_path,NULL,0);

				if(log_return != NULL)
					logprint(log_return);

				if (!text_script[ist]->script_init_only) {
					w_sprintf(level_script_path, 1000, "add_text(%s,'%s',%d)", text_script[ist]->script_ctor, text_script[ist]->text_id, level_mngr->script_id);
				}
				else {
					//add the id as an extra param
					script_genscriptinitonlyctor(text_script[ist]->script_ctor, text_script[ist]->text_id, level_mngr->script_id, level_script_path, 1000);
				}
				

				//create text object
				log_return = script_execstring(script_mngr,level_script_path,NULL,0);

				if(log_return != NULL)
					logprint(log_return);
			}
		}
	}


	//cleanup containers
	wf_free(entity_script);
	entity_script = NULL;
	wf_free(text_script);
	text_script = NULL;

	if(module_mngr != NULL)
		module_mngr->game_level_loaded();

	//call script init function if any
	if(pb_level->script_file != NULL && strlen(pb_level->script_file) > 0)
	{
		char module_name[260];
		w_strcpy(module_name,260,pb_level->script_file);
		get_file_without_ext(module_name);

		char buff[260];

		w_sprintf(buff,260,"%s.init(%d)\n",module_name, level_mngr->script_id);

		const char* result = script_execstring(script_mngr,buff,NULL,0);

		if(result != NULL)
		{
			logprint(result);
		}
		else
		{
			logprint("module %s initialized!",module_name);
		}
	}


	return 1;
}


void Levelobject_g_clean_level(Levelobject_t* const level_mngr)
{
	uint32_t map_indx = 0;
	uint32_t map_count = 0;

	while(map_count <  level_mngr->map_hash.num_elem)
	{
		if(level_mngr->map_hash.keys[map_indx])
		{

			Tilemap_g_clean(&level_mngr->maps[map_indx].tilemap);
	
			map_count++;
		}

		map_indx++;
	}

	uint32_t ent_indx = 0;
	uint32_t ent_count = 0;

	while(ent_count <  level_mngr->entities_hash.num_elem)
	{
		if(level_mngr->entities_hash.keys[ent_indx])
		{
			entity_g_clean(&level_mngr->entities[ent_indx]);

			ent_count++;
		}

		ent_indx++;
	}

	uint32_t txt_indx = 0;
	uint32_t txt_count = 0;

	while(txt_count <  level_mngr->text_hash.num_elem)
	{
		if(level_mngr->text_hash.keys[txt_indx])
		{
			SpriteBatch_g_clean(&level_mngr->text_objects[txt_indx].font_object.batch);
			txt_count++;
		}

		txt_indx++;
	}

	uint32_t group_indx = 0;
	uint32_t group_count = 0;

	while(group_count <  level_mngr->group_hash.num_elem)
	{
		if(level_mngr->group_hash.keys[group_indx])
		{
			SpriteBatch_g_clean(&level_mngr->group_objects[group_indx].sprite_batch);

			group_count++;
		}
		group_indx++;
	}
}

void Levelobject_g_create_level(Levelobject_t* const level_mngr,render_manager_t* const render_mngr)
{
	uint32_t map_indx = 0;
	uint32_t map_count = 0;

	while(map_count <  level_mngr->map_hash.num_elem)
	{
		if(level_mngr->map_hash.keys[map_indx])
		{
			Tilemap_g_create(&level_mngr->maps[map_indx].tilemap,render_mngr);
	
			map_count++;
		}

		map_indx++;
	}

	uint32_t ent_indx = 0;
	uint32_t ent_count = 0;

	while(ent_count <  level_mngr->entities_hash.num_elem)
	{
		if(level_mngr->entities_hash.keys[ent_indx])
		{
			entity_g_create(&level_mngr->entities[ent_indx],render_mngr);

			ent_count++;
		}

		ent_indx++;
	}

	uint32_t txt_indx = 0;
	uint32_t txt_count = 0;

	while(txt_count <  level_mngr->text_hash.num_elem)
	{
		if(level_mngr->text_hash.keys[txt_indx])
		{
			SpriteBatch_g_create(&level_mngr->text_objects[txt_indx].font_object.batch,render_mngr);
			txt_count++;
		}

		txt_indx++;
	}

	uint32_t group_indx = 0;
	uint32_t group_count = 0;

	while(group_count <  level_mngr->group_hash.num_elem)
	{
		if(level_mngr->group_hash.keys[group_indx])
		{
			SpriteBatch_g_create(&level_mngr->group_objects[group_indx].sprite_batch,render_mngr);

			group_count++;
		}
		group_indx++;
	}
}

int Levelobject_clean_level(Levelobject_t* const level_mngr,
							resources_manager_t* const resx_mngr,
							physics_manager_t* const phy_mngr,  
							lua_State* script_mngr)
{
	physics_manager_t* c_phy_mngr = (level_mngr->slave_manager)?level_mngr->slave_manager : phy_mngr;

	//remove collisions data
	physics_free_tilemap_collisions(c_phy_mngr,wfalse);

	const char* log_return = NULL;
	char script_remove[1000] = { 0 };


	uint32_t ent_indx = 0;
	uint32_t ent_count = 0;


	//cleanup entities
	//remove link constraint at first, since these reference body across different entity
	while(ent_count <  level_mngr->entities_hash.num_elem)
	{
		if(level_mngr->entities_hash.keys[ent_indx])
		{
			//free entities constraints
			entity_freelinkconstraint(c_phy_mngr,&level_mngr->entities[ent_indx]);
			ent_count++;
		}

		ent_indx++;
	}


	ent_count = ent_indx = 0;

	while(ent_count <  level_mngr->entities_hash.num_elem)
	{
		if(level_mngr->entities_hash.keys[ent_indx])
		{
			//remove script data
			if(level_mngr->entities[ent_indx].has_script)
			{
				w_sprintf(script_remove,1000,"remove_entity('%s',%d)",level_mngr->entities[ent_indx].entity_id,level_mngr->script_id);

				log_return = script_execstring(script_mngr,script_remove,NULL,0);

				if(log_return != NULL)
						logprint(log_return);
			}

			//free rogue char pointer (not in level data file) in case of dynamic entities
			if(level_mngr->entities[ent_indx].dynamic)
				wf_free(level_mngr->entities_hash.keys[ent_indx]);

			//free entities data
			entity_free(phy_mngr,&level_mngr->entities[ent_indx]);
			memset(&level_mngr->entities[ent_indx],0,sizeof(entity_t));


			ent_count++;
		}

		ent_indx++;
	}

	memset(level_mngr->entities_hash.keys,0,sizeof(char*) * MAX_ENTITIES);
	level_mngr->entities_hash.num_elem = 0;

	//cleanup entity group
	uint32_t group_indx = 0;
	uint32_t group_count = 0;

	while(group_count <  level_mngr->group_hash.num_elem)
	{
		if(level_mngr->group_hash.keys[group_indx])
		{
			entity_group_free(&level_mngr->group_objects[group_indx]);

			memset(&level_mngr->group_objects[group_indx],0,sizeof(entity_group_t));

			group_count++;
		}
		group_indx++;
	}

	memset(level_mngr->group_hash.keys,0,sizeof(char*) * MAX_GROUP);
	level_mngr->group_hash.num_elem = 0;


	uint32_t trig_indx = 0;
	uint32_t trig_count = 0;

	//cleanup triggers
	while(trig_count <  level_mngr->triggers_hash.num_elem)
	{
		if(level_mngr->triggers_hash.keys[trig_indx])
		{
			memset(&level_mngr->triggers[trig_indx],0,sizeof(trigger_t));
			trig_count++;			
		}

		trig_indx++;
	}

	memset(level_mngr->triggers_hash.keys,0,sizeof(char*) * MAX_TRIGGER);
	level_mngr->triggers_hash.num_elem = 0;

	//cleanup text objects


	uint32_t txt_indx = 0;
	uint32_t txt_count = 0;

	while(txt_count <  level_mngr->text_hash.num_elem)
	{
		if(level_mngr->text_hash.keys[txt_indx])
		{
			//remove script data
			if(level_mngr->text_objects[txt_indx].has_script)
			{
				w_sprintf(script_remove,1000,"remove_text('%s',%d)",level_mngr->text_objects[txt_indx].text_id,level_mngr->script_id);

				log_return = script_execstring(script_mngr,script_remove,NULL,0);

				if(log_return != NULL)
						logprint(log_return);
			}

			//free rogue char pointer (not in level data file) in case of dynamic entities
			if(level_mngr->text_objects[txt_indx].dynamic)
				wf_free(level_mngr->text_hash.keys[txt_indx]);

			//free text entities data
			text_entity_free(&level_mngr->text_objects[txt_indx]);
			memset(&level_mngr->text_objects[txt_indx],0,sizeof(text_entity_t));
			txt_count++;
		}

		txt_indx++;
	}

	memset(level_mngr->text_hash.keys,0,sizeof(char*) * MAX_TEXT_OBJ);
	level_mngr->text_hash.num_elem = 0;

	ent_count = ent_indx = 0;

	//cleanup particles
	for(particles_entity_t* opart = (particles_entity_t*)hashmap_iterate(&level_mngr->particles_list,&ent_indx,&ent_count);opart != NULL;opart =  (particles_entity_t*)hashmap_iterate(&level_mngr->particles_list,&ent_indx,&ent_count))
	{
		particles_entity_free(opart,c_phy_mngr);
	}

	hashmap_recycle(&level_mngr->particles_list);

	//cleanup waypoint
	hashmap_recycle(&level_mngr->waypoints_list);

	ent_count = ent_indx = 0;

	//cleanup collider
	for (collider_t* ocol = (collider_t*)hashmap_iterate(&level_mngr->colliders_list, &ent_indx, &ent_count); ocol != NULL; ocol = (collider_t*)hashmap_iterate(&level_mngr->colliders_list, &ent_indx, &ent_count))
	{
		collider_free(ocol, c_phy_mngr);
	}

	hashmap_recycle(&level_mngr->colliders_list);

	if(level_mngr->pathfind_worker != NULL)
	{
		pathfind_worker_t* worker = (pathfind_worker_t*)level_mngr->pathfind_worker;

		if(worker->initialized)
			pathfind_freeallrequest(worker);
	}

	if(level_mngr->sound_manager != NULL)
	{
		snd_mngr_stopallsounds((snd_mngr_t*)level_mngr->sound_manager,wtrue);//since the sound objects on this level are the only one with the positionning and the removeonended flag is set to true, they will be automatically stopped and removed by the sound manager
	}


	
	uint32_t map_indx = 0;
	uint32_t map_count = 0;


	//cleanup maps
	while(map_count <  level_mngr->map_hash.num_elem)
	{
		if(level_mngr->map_hash.keys[map_indx])
		{
			//remove lights of this map
			if (level_mngr->light_manager != NULL)
			{
				uint32_t light_index = 0;
				uint32_t light_count = 0;

				light_manager_t* light_manager = (light_manager_t*)level_mngr->light_manager;

				uint32_t max_count = light_manager->light_hash.num_elem;

				for (light_t* olight = (light_t*)hashmap_iterate_const(&light_manager->light_hash, &light_index, &light_count, max_count); olight != NULL; olight = (light_t*)hashmap_iterate_const(&light_manager->light_hash, &light_index, &light_count, max_count))
				{
					if (strcmp(olight->parent_id, level_mngr->map_hash.keys[map_indx]) == 0) {
						light_manager_removelight(light_manager, olight->light_id);
					}
				}
			}

			//remove tilemaps
			Tilemap_Free(&level_mngr->maps[map_indx].tilemap);
			memset(&level_mngr->maps[map_indx],0,sizeof(map_t));
			map_count++;

			logprint("Map %s freed!",level_mngr->map_hash.keys[map_indx]);
		}

		map_indx++;
	}

	memset(level_mngr->map_hash.keys,0,sizeof(char*) * MAX_MAPS);
	level_mngr->map_hash.num_elem = 0;
	

	//unload script lua module


	if(strlen(level_mngr->current_module) > 0 && strcmp(level_mngr->current_module,"") != 0)
	{
		//load script file
		w_sprintf(script_remove,1000,"package.loaded['%s'] = nil",level_mngr->current_module);

		log_return  = script_execstring(script_mngr,script_remove,NULL,0);

		if(log_return != NULL)
			logprint(log_return);
		else
			logprint("Module %s unloaded",level_mngr->current_module);

		
	}
	
	memset(level_mngr->current_module,0,sizeof(level_mngr->current_module));
	
	memset(level_mngr->current_map_id,0,sizeof(level_mngr->current_map_id));
	memset(level_mngr->current_level,0,sizeof(level_mngr->current_level));

	display_list_init(level_mngr->entity_display_list);
	display_list_init(level_mngr->text_display_list);
	display_list_init(level_mngr->entitygroup_display_list);
	display_list_init(level_mngr->particles_display_list);

	level_mngr->current_map = NULL;

	return 1;
}
