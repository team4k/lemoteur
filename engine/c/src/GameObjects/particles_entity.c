#ifdef _MSC_VER
       extern "C" {
#endif
	#include "lauxlib.h"
	#include "lualib.h"
	#include <lua.h>

#ifdef _MSC_VER
}
#endif

#include <inttypes.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

#include <Render.h>

#include <Engine.h>
#include <Base/types.h>
#include <Base/functions.h>
#include <Base/geom.h>
#include <Base/Color.h>

#include <Debug/Logprint.h>

#include <Render/render_manager.h>
#include <Graphics/TextureLoader.h>
#include <GameObjects/Level.pb-c.h>

#include <chipmunk/chipmunk.h>
#include <chipmunk/chipmunk_unsafe.h>
#include <Physics/Physics.h>

#include <Graphics/Effects/particle_object.h>
#include <GameObjects/particles_entity.h>

#include <Scripting/ScriptEngine.h>

static void _particle_created_callback(particle_object_t* const ref_obj,particle_t* const part_obj)
{
	if(ref_obj->ref_physics && ref_obj->ref_entity)
	{
		physics_manager_t* phy_mngr = (physics_manager_t*)ref_obj->ref_physics;
		particles_entity_t* this_ent = (particles_entity_t*)ref_obj->ref_entity;
		//select the first available physics object
		uint32_t i_phy = 0;

		while(i_phy < this_ent->physics_obj_num && this_ent->physics_obj_array[i_phy].in_use)
			i_phy++;

		if(i_phy < this_ent->physics_obj_num)
		{
			this_ent->physics_obj_array[i_phy].in_use = wtrue;

			cpFloat hw = (float)part_obj->width * 0.5f;
			cpFloat hh =  (float)part_obj->height * 0.5f;

			cpBB box = cpBBNew(-hw, -hh, hw, hh);

			cpVect verts[] = {
				cpv(box.l, box.b),
				cpv(box.l, box.t),
				cpv(box.r, box.t),
				cpv(box.r, box.b),
			};

            cpPolyShapeSetVerts(this_ent->physics_obj_array[i_phy].shape,4,verts,cpTransformIdentity);

			cpSpaceAddBody(phy_mngr->world,this_ent->physics_obj_array[i_phy].body);
			cpSpaceAddShape(phy_mngr->world,this_ent->physics_obj_array[i_phy].shape);

			if(this_ent->physics_obj_array[i_phy].constraint_particle)
				cpSpaceAddConstraint(phy_mngr->world,this_ent->physics_obj_array[i_phy].move_constraint);

			part_obj->ref_physics_body = &this_ent->physics_obj_array[i_phy];

		}
	}

}

static void _particle_destroyed_callback(particle_object_t* const ref_obj,particle_t* const part_obj)
{
	if(ref_obj->ref_physics && ref_obj->ref_entity && part_obj->ref_physics_body)
	{
		physics_manager_t* phy_mngr = (physics_manager_t*)ref_obj->ref_physics;

		particle_physic_object_t* ref_o = (particle_physic_object_t*)part_obj->ref_physics_body;
			
		cpSpaceRemoveBody(phy_mngr->world,ref_o->body);
		cpSpaceRemoveShape(phy_mngr->world,ref_o->shape);
		
		if(ref_o->constraint_particle)
			cpSpaceRemoveConstraint(phy_mngr->world,ref_o->move_constraint);

		ref_o->in_use = wfalse;
		part_obj->ref_physics_body = NULL;
	}
}

static void _particle_script_callback(particle_object_t* const ref_obj,particle_t* const part_obj)
{
	char part_ptr[128];
	w_sprintf(part_ptr,128,"%p",part_obj);

	script_callfunc_params((lua_State*)ref_obj->ref_script,ref_obj->ref_module,"update_particle","s",part_ptr);

	if(part_obj->ref_physics_body)
	{
		particle_physic_object_t* ref_o = (particle_physic_object_t*)part_obj->ref_physics_body;

		if(ref_o->constraint_particle)
		{
            cpBodySetPosition(ref_o->controller,cpv(part_obj->position.x,part_obj->position.y));

            cpVect c_pos = cpBodyGetPosition(ref_o->body);

			part_obj->position = Vec((wfloat)c_pos.x,(wfloat)c_pos.y);

		}
		else
            cpBodySetPosition(ref_o->body,cpv(part_obj->position.x,part_obj->position.y));
	}

}

void particles_entity_init(particles_entity_t* const part_entity,ParticlesData* pb_data,render_manager_t* const render_manager,const char* prog_id,texture_info* texture,lua_State* script_ref,physics_manager_t* const phy_ref,const char* map_id)
{
	particle_mov_callback_t mov_callback = NULL;
	part_entity->physics_obj_array = NULL;
	part_entity->physics_obj_num = 0;

	if(script_ref != NULL && pb_data->script_module != NULL)
		mov_callback = _particle_script_callback;
		
	particle_object_init(&part_entity->graph_obj,render_manager,prog_id,texture,mov_callback,_particle_created_callback,_particle_destroyed_callback);

	part_entity->graph_obj.ref_script = script_ref;
	part_entity->graph_obj.ref_entity = part_entity;
	part_entity->is_visible = wtrue;
	part_entity->z_order = pb_data->z_order;

	w_strcpy(part_entity->ref_map_id,256,map_id);


	if(pb_data->script_module != NULL)
		w_strcpy(part_entity->graph_obj.ref_module,128,pb_data->script_module);

	if(pb_data->use_physics)
		part_entity->graph_obj.ref_physics = phy_ref;

	//setup our particle entity based on protobuf content
	part_entity->entity_id = string_alloc(pb_data->id);
	size_t num_physics_obj = 0;

	Vector_t center_pos = Vec((wfloat)pb_data->position->x, (wfloat)pb_data->position->y);

	for(uint32_t i_emit = 0;i_emit < pb_data->n_emitters;i_emit++)
	{
		particle_object_addemitter(&part_entity->graph_obj,pb_data->emitters[i_emit],render_manager,center_pos);

		num_physics_obj += pb_data->emitters[i_emit]->max_particle;
	}

	if(pb_data->n_emitters == 0)
		logprint("No emitters defined of %s particle entity!",part_entity->entity_id);
	else
		particle_object_genvbo(&part_entity->graph_obj,render_manager);

	//allocate default physics object
	if(pb_data->use_physics && num_physics_obj > 0)
	{
		part_entity->physics_obj_array = (particle_physic_object_t*)wf_calloc(num_physics_obj,sizeof(particle_physic_object_t));
		part_entity->physics_obj_num = num_physics_obj;

		for(uint32_t i_phy = 0;i_phy < part_entity->physics_obj_num;i_phy++)
		{
			particle_physic_object_t* c_obj = &part_entity->physics_obj_array[i_phy];
			
			c_obj->body =  cpBodyNew(10.0f, INFINITY);
            cpBodySetPosition(c_obj->body,cpvzero);

			cpFloat hw = 8.0f;
			cpFloat hh = 8.0f;

			cpBB box_bb = cpBBNew(-hw, -hh, hw, hh);

            c_obj->shape =  cpBoxShapeNew2(c_obj->body,box_bb,0.0f);
			c_obj->in_use = wfalse;

			c_obj->controller = cpBodyNewKinematic();
            cpBodySetPosition(c_obj->controller,cpvzero);

			c_obj->constraint_particle = (wbool)pb_data->constraint_particle;

			if(pb_data->constraint_particle)
			{
				c_obj->move_constraint = cpPivotJointNew2(c_obj->controller, c_obj->body, cpvzero,cpvzero);
                cpConstraintSetErrorBias(c_obj->move_constraint,cpfpow(1.0f - 0.15f, 60.0f));
				
			}

			
		}
	}

}

void particles_entity_setpos(particles_entity_t* const part_entity,Vector_t new_pos)
{
	for(int32_t i_emit = 0;i_emit < part_entity->graph_obj.num_emitter;i_emit++)
	{
		part_entity->graph_obj.emitter_array[i_emit].position = new_pos;
	}
}

wbool particles_entity_checkrender(particles_entity_t* const part_entity,Rect_t view_rect)
{
	return wtrue;
}

void particles_entity_draw(particles_entity_t* const part_entity,render_manager_t* const render_manager,Vector_t offset)
{
	particle_object_draw(&part_entity->graph_obj,render_manager,offset);
}

void particles_entity_update(particles_entity_t* const part_entity,float elapsed)
{
	particle_object_update(&part_entity->graph_obj,elapsed);
}

void particles_entity_g_clean(particles_entity_t* part_entity)
{
	particle_object_g_free(&part_entity->graph_obj);
}

void particles_entity_g_create(particles_entity_t* part_entity, render_manager_t* const render_manager)
{
	particle_object_g_create(&part_entity->graph_obj, render_manager);
}

void particles_entity_free(particles_entity_t* part_entity,physics_manager_t* const phy_ref)
{
	if(part_entity->physics_obj_array != NULL)
	{
		for(uint32_t i_phy = 0;i_phy < part_entity->physics_obj_num;i_phy++)
		{
			if(part_entity->physics_obj_array[i_phy].in_use)
			{
				cpSpaceRemoveBody(phy_ref->world,part_entity->physics_obj_array[i_phy].body);
				cpSpaceRemoveShape(phy_ref->world,part_entity->physics_obj_array[i_phy].shape);

				if(part_entity->physics_obj_array[i_phy].constraint_particle)
					cpSpaceRemoveConstraint(phy_ref->world,part_entity->physics_obj_array[i_phy].move_constraint);
			}

			if(part_entity->physics_obj_array[i_phy].constraint_particle)
				cpConstraintFree(part_entity->physics_obj_array[i_phy].move_constraint);

			cpBodyFree(part_entity->physics_obj_array[i_phy].controller);
			cpBodyFree(part_entity->physics_obj_array[i_phy].body);
			cpShapeFree(part_entity->physics_obj_array[i_phy].shape);
		}

		//TODO : clear physics object here
		wf_free(part_entity->physics_obj_array);
	}

	part_entity->physics_obj_num = 0;

	wf_free(part_entity->entity_id);
	particle_object_free(&part_entity->graph_obj);


}
