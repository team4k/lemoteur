
#include <GameObjects/level_stream.h>
#include <GameObjects/level_stream_chunks.h>

//chunk load, load a chunk from disk
//there is 2 load phase : 
// -1- load the data from disk et prepare them in a separate thread
Map* level_stream_load_chunkdb(level_stream_t* const level, int64_t world_row, int64_t world_column, char* out_guid, int64_t* out_posX, int64_t* out_posY)
{
	if (level->chunks_db_background == NULL || !level->chunks_db_ready)
	{
		logprint("Error while loading chunk, chunks db not loaded or not ready");
		return NULL;
	}


	char select_string[512];
	int sqlite_error = 0;

	w_sprintf(select_string, 512, "SELECT chunkid,content,posX,posY FROM chunks WHERE row_num=%lld AND col_num=%lld", world_row, world_column);

	sqlite3_stmt* statement;

	sqlite_error = sqlite3_prepare_v2(level->chunks_db_background, select_string, -1, &statement, NULL);

	if (sqlite_error)
	{
		logprint("Can't prepare statement %s, sqlite error %s", select_string, sqlite3_errmsg(level->chunks_db_background));
		return NULL;
	}

	sqlite_error = sqlite3_step(statement);

	if (sqlite_error != SQLITE_ROW && sqlite_error != SQLITE_DONE)
	{
		logprint("Can't execute statement, sqlite error %s", sqlite3_errmsg(level->chunks_db_background));
		sqlite3_finalize(statement);
		return NULL;
	}

	//no data to fetch, return null
	if (sqlite_error == SQLITE_DONE)
	{
		sqlite3_finalize(statement);
		return NULL;
	}
	else
	{
		//fetch data from database

		const unsigned char* id = sqlite3_column_text(statement, 0);

		w_sprintf(out_guid, 64, "%s", id);

		const void* binary_result = sqlite3_column_blob(statement, 1);

		size_t bytes_len = sqlite3_column_bytes(statement, 1);

		Map* map_data = map__unpack(NULL, bytes_len, (const unsigned char*)binary_result);

		(*out_posX) = sqlite3_column_int64(statement, 2);
		(*out_posY) = sqlite3_column_int64(statement, 3);

		sqlite3_finalize(statement);
		return map_data;
	}
}

wbool level_stream_getchunkdbpos(level_stream_t* const level, int64_t row_pos, int64_t col_pos, int64_t* out_xpos, int64_t* out_ypos)
{
	if (level->chunks_db == NULL || !level->chunks_db_ready)
	{
		logprint("Error while loading chunk, chunks db not loaded or not ready");
		return wfalse;
	}

	char select_string[512];
	int sqlite_error = 0;

	w_sprintf(select_string, 512, "SELECT posX, posY FROM chunks WHERE row_num = %lld AND col_num = %lld", row_pos, col_pos);

	sqlite3_stmt* statement;

	sqlite_error = sqlite3_prepare_v2(level->chunks_db, select_string, -1, &statement, NULL);

	if (sqlite_error)
	{
		logprint("Can't prepare statement %s, sqlite error %s", select_string, sqlite3_errmsg(level->chunks_db));
		return wfalse;
	}

	sqlite_error = sqlite3_step(statement);

	if (sqlite_error != SQLITE_ROW)
	{
		sqlite3_finalize(statement);
		logprint("No chunk found for pos row = %lld col = %lld", row_pos, col_pos);
		return wfalse;
	}

	//fetch data from database
	(*out_xpos) = sqlite3_column_int64(statement, 0);
	(*out_ypos) = sqlite3_column_int64(statement, 1);

	sqlite3_finalize(statement);
	return wtrue;
}

// generate a new chunk based on random levels fragments and map procedural generation
Map* level_stream_gen_chunk(tempallocator_t* const gen_data_pool, int32_t row_count, int32_t col_count, const char* prog_name, uint32_t num_tiles, char* out_guid)
{
	char gen_id[64] = { 0 };

	gen_guid(&gen_id[0]);

	w_sprintf(out_guid, 64, "%s", gen_id);

	char map_id[256] = { 0 };

	w_sprintf(map_id, 256, "mapchunk_%s", gen_id);

	Map* map_data = (Map*)tempallocator_getmemory(gen_data_pool, sizeof(Map));

	map__init(map_data);

	map_data->map_id = tempallocator_string_alloc(gen_data_pool, map_id);
	map_data->editor_player_pos = (Wvector*)tempallocator_getmemory(gen_data_pool, sizeof(Wvector));
	wvector__init(map_data->editor_player_pos);
	map_data->editor_pos = (Wvector*)tempallocator_getmemory(gen_data_pool, sizeof(Wvector));
	wvector__init(map_data->editor_pos);

	map_data->col_count = col_count;
	map_data->row_count = row_count;

	map_data->tileset_id = tempallocator_string_alloc(gen_data_pool, "\\underground_tileset.gal");

	map_data->n_layer_array = 2;
	map_data->layer_array = (Layer**)tempallocator_getmemory(gen_data_pool, sizeof(Layer*) *  map_data->n_layer_array);

	int32_t debug_tile = 1;

	Layer* layer_data = (Layer*)tempallocator_getmemory(gen_data_pool, sizeof(Layer));
	Layer* collision_gen = (Layer*)tempallocator_getmemory(gen_data_pool, sizeof(Layer));

	//create protobuf layer
	layer__init(layer_data);
	layer_data->layer_id = tempallocator_string_alloc(gen_data_pool, "layer_normal");

	layer_data->order = 0;
	layer_data->type = TYPE_LAYER__NORMAL;
	layer_data->editor_order = 0;
	layer_data->has_opacity = 1;
	layer_data->opacity = 1.0f;
	layer_data->has_destructible = 1;
	layer_data->destructible = 1;
	layer_data->has_destruct_tile_id = 1;
	layer_data->destruct_tile_id = 0;
	layer_data->has_visible = 1;
	layer_data->visible = 1;
	layer_data->shader_program = tempallocator_string_alloc(gen_data_pool, prog_name);

	layer_data->n_tile_array = num_tiles;
	layer_data->tile_array = (int32_t*)tempallocator_getmemory(gen_data_pool, layer_data->n_tile_array * sizeof(int32_t));

	for (uint32_t itile = 0; itile < layer_data->n_tile_array; itile++)
	{
		layer_data->tile_array[itile] = debug_tile;
	}

	map_data->layer_array[0] = layer_data;

	//generate collisions layer
	layer__init(collision_gen);

	collision_gen->layer_id = tempallocator_string_alloc(gen_data_pool, "collisions");

	collision_gen->order = 1;
	collision_gen->type = TYPE_LAYER__COLLISIONS;
	collision_gen->editor_order = 1;
	collision_gen->has_opacity = 1;
	collision_gen->opacity = 1.0f;
	collision_gen->destructible = 0;
	collision_gen->has_visible = 1;
	collision_gen->visible = 0;

	collision_gen->n_tile_array = num_tiles;
	collision_gen->tile_array = (int32_t*)tempallocator_getmemory(gen_data_pool, collision_gen->n_tile_array * sizeof(int32_t));

	map_data->layer_array[1] = collision_gen;

	return map_data;
}

/*
Load data that are shared with other level objects, and that are unload / removed between level switches (like lights)
*/
static void _level_stream_loadgeneralgraph(level_stream_t* const level, stream_chunk_t* const chunk, const Map* const data, uint32_t ilayer, wbool firstload)
{
	light_manager_t* c_light_manager = NULL;

	if (level->light_manager != NULL)
		c_light_manager = (light_manager_t*)level->light_manager;

	if (c_light_manager != NULL)
	{
		//loading light object
		for (uint32_t ilight = 0; ilight < data->layer_array[ilayer]->n_light_array; ilight++)
		{
			LightSource* tmp_light = data->layer_array[ilayer]->light_array[ilight];

			Vector_t position = { (wfloat)tmp_light->position->x + chunk->bounds.position.x,(wfloat)tmp_light->position->y + chunk->bounds.position.y };
			ColorA_t color = convertint_tocolor(tmp_light->light_color->r, tmp_light->light_color->g, tmp_light->light_color->b, tmp_light->light_color->a);

			light_t* new_light = light_manager_addlight(c_light_manager, position, color, tmp_light->radius, tmp_light->light_id, data->map_id, (light_type)tmp_light->l_type);

			light_setattenuation(new_light, tmp_light->constant, tmp_light->linear, tmp_light->quadratic);
			new_light->power_factor = tmp_light->power_factor;

			if (firstload) {
				size_t len = strlen(tmp_light->light_id) + 1;
				char* ptr = (char*)wf_malloc(sizeof(char) * len);
				w_strcpy(ptr, len, tmp_light->light_id);
				hashtable_pushkey(&chunk->lights, ptr);
			}

			if (new_light->l_type == light_directional)
			{
				light_setrotationangle(new_light, tmp_light->rotation_angle);
				light_setopenangle(new_light, tmp_light->open_angle);
			}

		}
	}
}

/*
the input map data are either generated data or loaded from chunk db data, never from a level file, since
the generation func create a unique id for entities / particles / other elements

chunk specific data are considered non existent, and always loaded

data that are loaded on the stream map (like dynamic entities), get theirs id checked against existing elements,
there is three state :
- not loaded and not tracked : the element has never been loaded -> load it
- not loaded and tracked : the element has been loaded and unloaded previously, it's not in memory but it state reside in the world tracker manager -> load it and initialise it state from the world tracker
-loaded : the element is already loaded, nothing to do

//load the graphicals data on the main thread, load the entities on the world entity hashmap, set the chunk ready to be used

//NB : chunk_pos : the position of the chunk in array
// ichunkpos : the position of the first tile of the chunk in the global tile array (based on the logical position in access_chunk)
//mem_chunk_pos : the position of the first tile of the chunk in memory
*/
void level_stream_backload_chunkgraph(level_stream_t* const level, stream_chunk_t* const chunk, const Map* const data, const int32_t chunk_pos)
{
	resources_manager_t* resx_mngr = level->resx_manager;

	module_mngr_t* module_mngr = (module_mngr_t*)level->module_manager;

	int32_t tile_size = level->tile_size;

	//create level based on protobuf data
	//get map texture name
	char* file_name = (char*)wf_malloc((strlen(data->tileset_id) + 1) * sizeof(char));

	get_file_name(data->tileset_id, file_name, wfalse);

	char tmp_file_name[256];
	w_strcpy(tmp_file_name, 256, file_name);

	w_sprintf(file_name, (strlen(data->tileset_id) + 1), "%s%s", &tmp_file_name[0], ".png");

	logprint("Adding Map %s with tileset %s", data->map_id, file_name);

	texture_info* tileset = gettexture_resx_back(resx_mngr, &file_name[0]);

	w_strcpy(chunk->map_id, 256, data->map_id);


	//! the chunks MUST have the same size (same row_count / col count) otherwise the below calculation doesn't work, it doesn't need to be square (colcount and rowcount can be different)
	//but it need to be the same acrosse all chunks
	int32_t row_in_chunk = (int32_t)(chunk->pos / CHUNK_COL);
	int32_t col_in_chunk = chunk->pos - (row_in_chunk * CHUNK_COL);

	int32_t ichunkpos = ((row_in_chunk * CHUNK_COL)  * (data->col_count * data->row_count)) + (data->col_count * col_in_chunk);

	int32_t row_jump = (int32_t)((CHUNK_COL - 1) * data->col_count);


	//position in memory for shape data (data that we don't more around when a the center chunk change)
	int32_t mem_row_in_chunk = (int32_t)(chunk_pos / CHUNK_COL);
	int32_t mem_col_in_chunk = chunk_pos - (mem_row_in_chunk * CHUNK_COL);

	int32_t mem_chunk_pos = ((mem_row_in_chunk * CHUNK_COL)  * (data->col_count * data->row_count)) + (data->col_count * mem_col_in_chunk);

	//check tileset texture
	if (!tileset)
	{
		logprint("Error loading tileset %s for tilemap %s!", file_name, data->map_id);
		wf_free(file_name);
		file_name = NULL;
		return;
	}

	Tilemap_Init(&chunk->map, tile_size, tileset, tileset->texture_name, data);

	wf_free(file_name);
	file_name = NULL;

	Rect_t map_dimension;
	map_dimension.position = chunk->bounds.position;
	map_dimension.width = data->col_count * tile_size;
	map_dimension.height = data->row_count * tile_size;

	//add layers to map
	for (uint32_t i = 0; i < data->n_layer_array; i++)
	{
		if (data->layer_array[i]->type != TYPE_LAYER__NORMAL)
			continue;

		//layer texture
		texture_info* layer_texture = NULL;

		if (data->layer_array[i]->tileset_id == NULL)
			layer_texture = tileset;
		else
		{
			char layer_texture_name[256];
			char tmp_layer_texture_name[256];

			if (str_has_slash(data->layer_array[i]->tileset_id))
			{
				get_file_name(data->layer_array[i]->tileset_id, layer_texture_name, wfalse);

				w_strcpy(tmp_layer_texture_name, 256, layer_texture_name);

				w_sprintf(layer_texture_name, 256, "%s%s", &tmp_layer_texture_name[0], ".png");
			}
			else
			{
				w_strcpy(layer_texture_name, 256, data->layer_array[i]->tileset_id);
			}

			layer_texture = gettexture_resx_back(resx_mngr, layer_texture_name);
		}


		//normal layer contains our tilemap data (background and foreground static or animated data)
		Tilemap_AddLayer_withback(&chunk->map, level->render_mngr, data->layer_array[i], layer_texture, level->randomizer,wtrue);

		if (data->layer_array[i]->destructible)
			Tilemap_setdestructiblelayer(&chunk->map, data->layer_array[i]->layer_id, data->layer_array[i]->destruct_tile_id);
	}
}

static int32_t _get_neigh_mem_index(level_stream_t* const level, stream_chunk_t* const chunk, STREAM_POS neigh_pos)
{

	STREAM_POS chunk_pos = chunk->pos;

	int32_t imemindex = -1;

	switch (neigh_pos)
	{
	case O_LEFT:
		if (chunk_pos == O_TOPLEFT || chunk_pos == O_LEFT || chunk_pos == O_BOTTOMLEFT)
		{
			return -1;
		}
		else
		{
			if (chunk_pos == O_TOP) {
				imemindex = level->access_chunk[O_TOPLEFT]->mem_pos;
			}
			else if (chunk_pos == O_CENTER) {
				imemindex = level->access_chunk[O_LEFT]->mem_pos;
			}
			else if (chunk_pos == O_BOTTOM) {
				imemindex = level->access_chunk[O_BOTTOMLEFT]->mem_pos;
			}
			else if (chunk_pos == O_TOPRIGHT) {
				imemindex = level->access_chunk[O_TOP]->mem_pos;
			}
			else if (chunk_pos == O_RIGHT) {
				imemindex = level->access_chunk[O_CENTER]->mem_pos;
			}
			else if (chunk_pos == O_BOTTOMRIGHT) {
				imemindex = level->access_chunk[O_BOTTOM]->mem_pos;
			}
		}
		break;
	case O_TOP:
		if (chunk_pos == O_TOPLEFT || chunk_pos == O_TOP || chunk_pos == O_TOPRIGHT)
		{
			return -1;
		}
		else
		{
			if (chunk_pos == O_LEFT) {
				imemindex = level->access_chunk[O_TOPLEFT]->mem_pos;
			}
			else if (chunk_pos == O_CENTER) {
				imemindex = level->access_chunk[O_TOP]->mem_pos;
			}
			else if (chunk_pos == O_RIGHT) {
				imemindex = level->access_chunk[O_TOPRIGHT]->mem_pos;
			}
			else if (chunk_pos == O_BOTTOMLEFT) {
				imemindex = level->access_chunk[O_LEFT]->mem_pos;
			}
			else if (chunk_pos == O_BOTTOM) {
				imemindex = level->access_chunk[O_CENTER]->mem_pos;
			}
			else if (chunk_pos == O_BOTTOMRIGHT) {
				imemindex = level->access_chunk[O_RIGHT]->mem_pos;
			}
		}
		break;
	case O_RIGHT:
		if (chunk_pos == O_TOPRIGHT || chunk_pos == O_RIGHT || chunk_pos == O_BOTTOMRIGHT)
		{
			return -1;
		}
		else
		{
			if (chunk_pos == O_TOPLEFT) {
				imemindex = level->access_chunk[O_TOP]->mem_pos;
			}
			else if (chunk_pos == O_LEFT) {
				imemindex = level->access_chunk[O_CENTER]->mem_pos;
			}
			else if (chunk_pos == O_BOTTOMLEFT) {
				imemindex = level->access_chunk[O_BOTTOM]->mem_pos;
			}
			else if (chunk_pos == O_TOP) {
				imemindex = level->access_chunk[O_TOPRIGHT]->mem_pos;
			}
			else if (chunk_pos == O_CENTER) {
				imemindex = level->access_chunk[O_RIGHT]->mem_pos;
			}
			else if (chunk_pos == O_BOTTOM) {
				imemindex = level->access_chunk[O_BOTTOMRIGHT]->mem_pos;
			}
		}
		break;
	case O_BOTTOM:
		if (chunk_pos == O_BOTTOMLEFT || chunk_pos == O_BOTTOM || chunk_pos == O_BOTTOMRIGHT)
		{
			return -1;
		}
		else
		{
			if (chunk_pos == O_TOPLEFT) {
				imemindex = level->access_chunk[O_LEFT]->mem_pos;
			}
			else if (chunk_pos == O_TOP) {
				imemindex = level->access_chunk[O_CENTER]->mem_pos;
			}
			else if (chunk_pos == O_TOPRIGHT) {
				imemindex = level->access_chunk[O_RIGHT]->mem_pos;
			}
			else if (chunk_pos == O_LEFT) {
				imemindex = level->access_chunk[O_BOTTOMLEFT]->mem_pos;
			}
			else if (chunk_pos == O_CENTER) {
				imemindex = level->access_chunk[O_BOTTOM]->mem_pos;
			}
			else if (chunk_pos == O_RIGHT) {
				imemindex = level->access_chunk[O_BOTTOMRIGHT]->mem_pos;
			}
		}
		break;

	}

	int32_t row_in_chunk = (int32_t)(imemindex / CHUNK_COL);
	int32_t col_in_chunk = imemindex - (row_in_chunk * CHUNK_COL);

	return ((row_in_chunk * CHUNK_COL)  * (level->base_col_count * level->base_row_count)) + (level->base_col_count * col_in_chunk);
}

script_container_t level_stream_frontload_chunkgraph(level_stream_t* const level, stream_chunk_t* const chunk, const Map* const data, const int32_t chunk_pos)
{

	light_manager_t* c_light_manager = NULL;

	if (level->light_manager != NULL)
	{
		size_t num_lights = 0;
		c_light_manager = (light_manager_t*)level->light_manager;

		for (uint32_t i = 0; i < data->n_layer_array; i++)
		{
			if (data->layer_array[i]->type == TYPE_LAYER__OBJECTS && !data->layer_array[i]->editor_only) {
				num_lights += data->layer_array[i]->n_light_array;
			}

		}

		chunk->lights.keys = (char**)wf_malloc(num_lights * sizeof(char*));

		//initialise keys array
		for (uint32_t light_indx = 0; light_indx < num_lights; light_indx++)
			chunk->lights.keys[light_indx] = 0;

		chunk->lights.max_elem = num_lights;
		chunk->lights.num_elem = 0;
	}



	snd_mngr_t* c_sound_manager = NULL;

	if (level->sound_manager != NULL)
		c_sound_manager = (snd_mngr_t*)level->sound_manager;

	pathfind_worker_t* c_path_worker = NULL;

	if (level->pathfind_worker != NULL)
		c_path_worker = (pathfind_worker_t*)level->pathfind_worker;

	resources_manager_t* resx_mngr = level->resx_manager;

	module_mngr_t* module_mngr = (module_mngr_t*)level->module_manager;

	script_container_t container;

	container.entity_script = NULL;

	//array to store script functions
	container.entity_script = NULL;
	container.entity_script_len = 0;
	int32_t entity_script_indx = 0;

	container.text_script = NULL;
	container.text_script_len = 0;
	int32_t text_script_indx = 0;


	//! the chunks MUST have the same size (same row_count / col count) otherwise the below calculation doesn't work, it doesn't need to be square (colcount and rowcount can be different)
	//but it need to be the same acrosse all chunks
	int32_t row_in_chunk = (int32_t)(chunk->pos / CHUNK_COL);
	int32_t col_in_chunk = chunk->pos - (row_in_chunk * CHUNK_COL);

	int32_t ichunkpos = ((row_in_chunk * CHUNK_COL)  * (data->col_count * data->row_count)) + (data->col_count * col_in_chunk);

	int32_t row_jump = (int32_t)((CHUNK_COL - 1) * data->col_count);


	//position in memory for shape data (data that we don't more around when a the center chunk change)
	int32_t mem_row_in_chunk = (int32_t)(chunk_pos / CHUNK_COL);
	int32_t mem_col_in_chunk = chunk_pos - (mem_row_in_chunk * CHUNK_COL);

	int32_t mem_chunk_pos = ((mem_row_in_chunk * CHUNK_COL)  * (data->col_count * data->row_count)) + (data->col_count * mem_col_in_chunk);

	int32_t tile_size = level->tile_size;

	Rect_t map_dimension;
	map_dimension.position = chunk->bounds.position;
	map_dimension.width = data->col_count * tile_size;
	map_dimension.height = data->row_count * tile_size;

	load_texture_deferred(chunk->map.texturePointer, level->render_mngr);
	

	//add layers to map
	for (uint32_t i = 0; i < data->n_layer_array; i++)
	{
		if (data->layer_array[i]->type == TYPE_LAYER__NORMAL)
		{
			Tilemap_frontload_layer(&chunk->map, level->render_mngr,data->layer_array[i]->layer_id);
			//Tilemaplay
		}
		else if (data->layer_array[i]->type == TYPE_LAYER__COLLISIONS)
		{
			//TODO reprendre le calcul de copie des �l�ments depuis les donn�es de niveau vers notre tableau de collisions
			//voir si toujours d'actualit� suite correction bug calcul nombre de tile

			int32_t row_count = chunk->map.row_count;
			int32_t row_cpy = 0;
			int32_t index_cpy = ichunkpos;
			int32_t src_cpy = 0;

			//update collisions data
			while (row_cpy++ < row_count)
			{
				w_memcpy(&level->collisions_array[index_cpy], chunk->map.col_count * sizeof(int32_t), &data->layer_array[i]->tile_array[src_cpy], chunk->map.col_count * sizeof(int32_t));

				index_cpy += (chunk->map.col_count + row_jump);
				src_cpy += chunk->map.col_count;
			}

			Vector_t offsetv = Vec_minus(level->access_chunk[O_BOTTOMLEFT]->bounds.position, level->access_chunk[O_CENTER]->bounds.position);

			offsetv.x += level->access_chunk[O_CENTER]->bounds.position.x;
			//level->tile_size * (level->base_col_count * WFLOAT05);
			offsetv.y += level->access_chunk[O_CENTER]->bounds.position.y;
			//level->tile_size * (level->base_row_count * WFLOAT05);

			cpVect offset = cpv(offsetv.x, offsetv.y);

			WorldRect_t dimension = lvlbase_getdimension(level, L_STREAM);

			update_tilemap_ranged(level->phy_manager, data->layer_array[i]->tile_array, level->collisions_array, dimension, ichunkpos, chunk->map.col_count, chunk->map.row_count, chunk->map.tile_size, row_jump, mem_chunk_pos * 4, _get_neigh_mem_index(level, chunk, O_LEFT), _get_neigh_mem_index(level, chunk, O_RIGHT), _get_neigh_mem_index(level, chunk, O_TOP), _get_neigh_mem_index(level, chunk, O_BOTTOM), CHUNK_COL, CHUNK_ROW, chunk_pos, level->check_col_func, level->check_col_func_editable);

			Tilemap_AddCollisions(&chunk->map, data->layer_array[i]);

		}
		else if (data->layer_array[i]->type == TYPE_LAYER__OBJECTS && !data->layer_array[i]->editor_only)//object layer contains trigger / npc / entities and text objects
		{

			//loading trigger
			for (uint32_t itrig = 0; itrig < data->layer_array[i]->n_trigger_array; itrig++)
			{
				Trigger* tmp_trig = data->layer_array[i]->trigger_array[itrig];

				trigger_t* o_trig = (trigger_t*)hashmap_push(&chunk->triggers, tmp_trig->trigger_id);
				trigger_init(o_trig, tmp_trig, level->trigger_func, chunk->bounds.position);
				w_strcpy(o_trig->ref_map_id, 256, chunk->map_id);

				int indx = hashmap_getindexforkey(&chunk->triggers, tmp_trig->trigger_id);

				o_trig->trigger_id = chunk->triggers.keys[indx];
			}

			container.entity_script_len += data->layer_array[i]->n_entity_array;

			if (container.entity_script == NULL)
			{
				if (container.entity_script_len > 0)
					container.entity_script = (Entity**)wf_malloc(sizeof(Entity*) * container.entity_script_len);
			}
			else
			{
				container.entity_script = (Entity**)wf_realloc(container.entity_script, sizeof(Entity*) * container.entity_script_len);
			}


			//loading entities
			for (uint32_t ient = 0; ient < data->layer_array[i]->n_entity_array; ient++)
			{
				Entity* tmp_ent = data->layer_array[i]->entity_array[ient];


				//every entities from a map are static, no matter what. dynamic entities are removed from map after generation and saved / loaded separatly
				entity_t* new_entity = level_stream_add_entity(level, chunk, resx_mngr, chunk->phy_manager, tmp_ent->entity_id, tmp_ent, level->render_mngr, wfalse);

				if (tmp_ent->script_ctor != NULL && strlen(tmp_ent->script_ctor) > 0)
				{
					container.entity_script[entity_script_indx++] = tmp_ent;

					new_entity->has_script = (wbool)!tmp_ent->script_init_only;

					w_strcpy(new_entity->script_ctor, 512, tmp_ent->script_ctor);

					if (!new_entity->has_script) {
						script_genscriptinitonlyctor(tmp_ent->script_ctor, tmp_ent->entity_id, level->script_id, new_entity->script_ctor, 1000);
					}

				}
				else
				{
					container.entity_script[entity_script_indx++] = NULL;
					new_entity->has_script = wfalse;
				}
			}

			/*
			//add our entity group after all entities have been processed
			for(uint32_t igroup = 0; igroup < data->layer_array[i]->n_group_array;igroup++)
			{
			GroupEntity* tmp_group = data->layer_array[i]->group_array[igroup];
			Levelobject_addentitygroup(level_mngr,tmp_group,data->map_id,data->layer_array[i]->order,render_mngr);
			}
			*/
			container.text_script_len += data->layer_array[i]->n_text_array;

			if (container.text_script == NULL)
			{
				if (container.text_script_len > 0)
					container.text_script = (TextObject**)wf_malloc(sizeof(TextObject*) * container.text_script_len);
			}
			else
			{
				container.text_script = (TextObject**)wf_realloc(container.text_script, sizeof(TextObject*) * container.text_script_len);
			}

			//loading text objects
			for (uint32_t itxt = 0; itxt < data->layer_array[i]->n_text_array; itxt++)
			{
				TextObject* text_data = data->layer_array[i]->text_array[itxt];

				wbool antialias = wfalse;

				if (text_data->has_anti_aliasing)
					antialias = (wbool)text_data->anti_aliasing;

				font_info* finfo = getfont_resx(resx_mngr, text_data->font_name);

				text_entity_t* o_text = (text_entity_t*)hashmap_push(&chunk->text, text_data->text_id);

				if (finfo != NULL)
				{
					text_entity_init(o_text, level->local_obj, text_data, data->map_id, text_data->text_id, finfo, map_dimension, level->render_mngr);

					o_text->dynamic = wfalse;
				}
				else
				{
					logprint("the font %s doesn't exist in resources list!", text_data->font_name);
				}

				if (text_data->script_ctor != NULL && strlen(text_data->script_ctor) > 0)
				{
					container.text_script[text_script_indx++] = text_data;

					o_text->has_script = (wbool)!text_data->script_init_only;

					w_strcpy(o_text->script_ctor, 512, text_data->script_ctor);

					if (!o_text->has_script) {
						script_genscriptinitonlyctor(text_data->script_ctor, text_data->text_id, level->script_id, o_text->script_ctor, 1000);
					}

				}
				else
				{
					container.text_script[text_script_indx++] = NULL;
					o_text->has_script = wfalse;
				}
			}

			//loading particles objects
			for (uint32_t i_part = 0; i_part < data->layer_array[i]->n_particles_array; i_part++)
			{
				ParticlesData* tmp_part = data->layer_array[i]->particles_array[i_part];

				particles_entity_t* entity = (particles_entity_t*)hashmap_push(&chunk->particles_list, tmp_part->id);
				tmp_part->position->x += (int32_t)chunk->bounds.position.x;
				tmp_part->position->y += (int32_t)chunk->bounds.position.y;

				particles_entity_init(entity, tmp_part, level->render_mngr, "prog_no_texture", NULL, level->script_mngr, chunk->phy_manager, data->map_id);

			}

			//loading waypoints
			for (uint32_t i_way = 0; i_way < data->layer_array[i]->n_waypoints; i_way++)
			{
				Waypoint* tmp_way = data->layer_array[i]->waypoints[i_way];

				waypoint_t* entity = (waypoint_t*)hashmap_push(&chunk->waypoints, tmp_way->id);

				tmp_way->position->x += (int32_t)chunk->bounds.position.x;
				tmp_way->position->y += (int32_t)chunk->bounds.position.y;

				waypoint_init(entity, tmp_way, data->map_id);
			}

			//loading colliders
			for (uint32_t i_col = 0; i_col < data->layer_array[i]->n_colliders; i_col++)
			{
				Collider* tmp_col = data->layer_array[i]->colliders[i_col];

				collider_t* entity = (collider_t*)hashmap_push(&chunk->colliders, tmp_col->id);

				tmp_col->position->x += (int32_t)chunk->bounds.position.x;
				tmp_col->position->y += (int32_t)chunk->bounds.position.y;

				char* const ptr = hashmap_getkeypointer(&chunk->colliders, tmp_col->id);

				collider_init(entity, tmp_col, chunk->phy_manager, data->map_id, ptr);
			}

			_level_stream_loadgeneralgraph(level, chunk, data, i, wtrue);

			if (c_sound_manager != NULL)
			{
				//loading sound object
				for (uint32_t isnd = 0; isnd < data->layer_array[i]->n_sound_array; isnd++)
				{
					SoundSource* tmp_snd = data->layer_array[i]->sound_array[isnd];
					Vector_t position = { (wfloat)tmp_snd->position->x + chunk->bounds.position.x,(wfloat)tmp_snd->position->y + chunk->bounds.position.y };
					snd_mngr_playsound(c_sound_manager, getsound_resx(resx_mngr, tmp_snd->snd_file_name), wtrue, tmp_snd->sound_id, wtrue, wfalse, 2, position, wtrue, tmp_snd->falloffarea, data->map_id);
				}
			}




		}

	}


	if (c_light_manager != NULL) {
		//update floodlight map
		light_manager_updatefloodfillarray(c_light_manager, level->collisions_array_count * sizeof(int32_t));
	}

	return container;
}


void level_stream_sync_chunkcontent(level_stream_t* const level, stream_chunk_t* const chunk, Map* const data)
{
	//synchronize the Map object with any change made to tilemap and object to prepare for chunk saving to disk
	//chunk->map
	//  data

	for (uint32_t ilayer = 0; ilayer < data->n_layer_array; ilayer++)
	{
		if (data->layer_array[ilayer]->type == TYPE_LAYER__OBJECTS)
			continue;

		if (data->layer_array[ilayer]->type == TYPE_LAYER__COLLISIONS)
		{
			w_memcpy(data->layer_array[ilayer]->tile_array, (chunk->map.col_count * chunk->map.row_count) * sizeof(int32_t), chunk->map.collisions_array, (chunk->map.col_count * chunk->map.row_count) * sizeof(int32_t));
			continue;
		}


		int layer_indx = hashtable_index(&chunk->map.layer_hash, data->layer_array[ilayer]->layer_id);

		if (layer_indx == -1)
			continue;

		TilemapLayer_t* layer = &chunk->map.layer_array[layer_indx];

		w_memcpy(data->layer_array[ilayer]->tile_array, (chunk->map.col_count * chunk->map.row_count) * sizeof(int32_t), layer->tileArray, (chunk->map.col_count * chunk->map.row_count) * sizeof(int32_t));
	}

}


unsigned char* level_stream_save_chunk(level_stream_t* const level, stream_chunk_t* const chunk, const Map* const data, int64_t world_row, int64_t world_column)
{

	//check if the chunk exist in db
	char select_string[512];
	int sqlite_error = 0;
	w_sprintf(select_string, 512, "SELECT count(*) FROM chunks WHERE chunkid = '%s'", chunk->chunk_id);


	sqlite3_stmt* statement;

	sqlite_error = sqlite3_prepare_v2(level->chunks_db_background, select_string, -1, &statement, NULL);

	if (sqlite_error)
	{
		logprint("Can't prepare statement %s, sqlite error %s", select_string, sqlite3_errmsg(level->chunks_db_background));
		return NULL;
	}

	sqlite_error = sqlite3_step(statement);

	if (sqlite_error != SQLITE_ROW && sqlite_error != SQLITE_DONE)
	{
		logprint("Can't execute statement, sqlite error %s", sqlite3_errmsg(level->chunks_db_background));
		sqlite3_finalize(statement);
		return NULL;
	}

	int32_t count = sqlite3_column_int(statement, 0);

	sqlite3_finalize(statement);

	sqlite3_stmt* statement_insert;

	size_t save_size = map__get_packed_size(data);

	tempallocator_clear(chunk->content_pool);

	unsigned char* save_blob = (unsigned char*)tempallocator_getmemory(chunk->content_pool, save_size);

	map__pack(data, save_blob);

	//chunk doesn't exist, insert it
	if (sqlite_error == SQLITE_ROW && count == 0)
	{
		w_sprintf(select_string, 512, "INSERT INTO chunks VALUES(@row,@col,@chunkid,@content,@Xpos,@Ypos)");

		sqlite_error = sqlite3_prepare_v2(level->chunks_db_background, select_string, -1, &statement_insert, NULL);

		if (sqlite_error)
		{
			logprint("Can't prepare statement %s, sqlite error %s", select_string, sqlite3_errmsg(level->chunks_db_background));
			return save_blob;
		}

		sqlite3_bind_int64(statement_insert, 1, world_row);
		sqlite3_bind_int64(statement_insert, 2, world_column);
		sqlite3_bind_text(statement_insert, 3, chunk->chunk_id, -1, SQLITE_STATIC);
		sqlite3_bind_blob(statement_insert, 4, (const void*)save_blob, save_size, SQLITE_STATIC);
		sqlite3_bind_int64(statement_insert, 5, (int64_t)chunk->bounds.position.x);
		sqlite3_bind_int64(statement_insert, 6, (int64_t)chunk->bounds.position.y);

		sqlite_error = level_stream_handlelock(statement_insert);

		if (sqlite_error != SQLITE_DONE)
		{
			logprint("Can't insert chunk, sqlite error %s", sqlite3_errmsg(level->chunks_db_background));
		}

		sqlite3_finalize(statement_insert);

		return save_blob;
	}
	else if (sqlite_error == SQLITE_ROW && count == 1)
	{
		//chunk exist, update it
		w_sprintf(select_string, 512, "UPDATE chunks SET content = @content WHERE chunkid = @chunkid");

		sqlite_error = sqlite3_prepare_v2(level->chunks_db_background, select_string, -1, &statement_insert, NULL);

		if (sqlite_error)
		{
			logprint("Can't prepare statement %s, sqlite error %s", select_string, sqlite3_errmsg(level->chunks_db_background));
			return save_blob;
		}

		sqlite3_bind_text(statement_insert, 2, chunk->chunk_id, -1, SQLITE_STATIC);
		sqlite3_bind_blob(statement_insert, 1, (const void*)save_blob, save_size, SQLITE_STATIC);

		sqlite_error = level_stream_handlelock(statement_insert);

		if (sqlite_error != SQLITE_DONE)
		{
			logprint("Can't update chunk, sqlite error %s", sqlite3_errmsg(level->chunks_db_background));
		}

		sqlite3_finalize(statement_insert);
		return save_blob;
	}
	else
	{
		if (count > 1)
		{
			logprint("More than one chunk with id %s, can't save chunk", chunk->chunk_id);
		}
		else
		{
			logprint("Count(*) query didn't return any row for id %s", chunk->chunk_id);
		}

	}

	return save_blob;


}

/*
like load, unload separate level stream linked and chunk linked data :

chunk linked (static entities / grouped entities / text / triggers / particles) => always unloaded

level linked (dynamic entities) => if still in stream zone, keep in memory , if not in stream zone anymore, send to world tracker
in case an entity from an unloaded chunk is suddenly out of stream zone > send to world tracker and unload it
*/

static void _unload_entities(stream_chunk_t* const chunk)
{
	uint32_t o_index = 0;
	uint32_t o_count = 0;

	for (entity_t* oent = (entity_t*)hashmap_iterate(&chunk->entities, &o_index, &o_count); oent != NULL; oent = (entity_t*)hashmap_iterate(&chunk->entities, &o_index, &o_count))
	{
		//free entities data
		entity_free(chunk->phy_manager, oent);
	}

	hashmap_recycle(&chunk->entities);
}

//unload physics in main thread
static void post_step_unload_callback(cpSpace* space, void* obj, void* data)
{
	logprint("unload in main thread");
	_unload_entities((stream_chunk_t*)obj);
}

stream_chunk_t* level_stream_get_center_chunk(level_stream_t* const  level)
{
	stream_chunk_t* center_chunk = NULL;

	for (uint32_t ichunk = 0; ichunk < CHUNK_NUMBER; ichunk++)
	{
		if (level->chunks[ichunk].pos == O_CENTER)
		{
			center_chunk = &level->chunks[ichunk];
			break;
		}
	}

	return center_chunk;
}

void level_stream_unload_chunk(level_stream_t* const level, stream_chunk_t* const chunk)
{
	const char* log_return = NULL;
	char script_remove[512] = { 0 };

	uint32_t o_index = 0;
	uint32_t o_count = 0;

	for (entity_group_t* ogroup = (entity_group_t*)hashmap_iterate(&chunk->group_objects, &o_index, &o_count); ogroup != NULL; ogroup = (entity_group_t*)hashmap_iterate(&chunk->group_objects, &o_index, &o_count))
	{
		entity_group_free(ogroup);
	}

	hashmap_recycle(&chunk->group_objects);

	hashmap_recycle(&chunk->triggers);

	o_index = o_count = 0;

	for (entity_t* oent = (entity_t*)hashmap_iterate(&chunk->entities, &o_index, &o_count); oent != NULL; oent = (entity_t*)hashmap_iterate(&chunk->entities, &o_index, &o_count))
	{
		//remove script data
		if (oent->has_script)
		{
			w_sprintf(script_remove, 512, "remove_entity('%s',%d)", oent->entity_id, level->script_id);

			log_return = script_execstring(level->script_mngr, script_remove, NULL, 0);

			if (log_return != NULL)
				logprint(log_return);
		}
	}

	//depending of the cpspace state, unload entities directly or do it in a poststepcallback
	//if (cpSpaceIsLocked(chunk->phy_manager->world)) {
		//cpSpaceAddPostStepCallback(chunk->phy_manager->world, post_step_unload_callback, chunk, NULL);
	//}
	//else {
		//atomic_set_value(&chunk->phy_manager->unload_inprogress, 1);
		
		//atomic_set_value(&chunk->phy_manager->unload_inprogress, 0);
	//}
	_unload_entities(chunk);


	o_index = o_count = 0;

	for (text_entity_t* otext = (text_entity_t*)hashmap_iterate(&chunk->text, &o_index, &o_count); otext != NULL; otext = (text_entity_t*)hashmap_iterate(&chunk->text, &o_index, &o_count))
	{
		//remove script data
		if (otext->has_script)
		{
			w_sprintf(script_remove, 1000, "remove_text('%s',%d)", otext->text_id, level->script_id);

			log_return = script_execstring(level->script_mngr, script_remove, NULL, 0);

			if (log_return != NULL)
				logprint(log_return);
		}

		//free text entities data
		text_entity_free(otext);
	}

	hashmap_recycle(&chunk->text);

	o_index = o_count = 0;

	for (particles_entity_t* opart = (particles_entity_t*)hashmap_iterate(&chunk->particles_list, &o_index, &o_count); opart != NULL; opart = (particles_entity_t*)hashmap_iterate(&chunk->particles_list, &o_index, &o_count))
	{
		particles_entity_free(opart, chunk->phy_manager);
	}

	hashmap_recycle(&chunk->particles_list);

	o_index = o_count = 0;

	for (collider_t* ocol = (collider_t*)hashmap_iterate(&chunk->colliders, &o_index, &o_count); ocol != NULL; ocol = (collider_t*)hashmap_iterate(&chunk->colliders, &o_index, &o_count))
	{
		collider_free(ocol, chunk->phy_manager);
	}

	hashmap_recycle(&chunk->colliders);

	hashmap_recycle(&chunk->waypoints);

	Tilemap_Free(&chunk->map);

	//remove lights 
	if (level->light_manager != NULL)
	{
		light_manager_t* c_light_manager = (light_manager_t*)level->light_manager;

		uint32_t l_indx = 0;
		uint32_t l_count = 0;
		while (l_count <  chunk->lights.num_elem)
		{
			if (chunk->lights.keys[l_indx])
			{
				light_manager_removelight(c_light_manager, chunk->lights.keys[l_indx]);
				wf_free(chunk->lights.keys[l_indx]);
				l_count++;
			}

			l_indx++;
		}

		wf_free(chunk->lights.keys);
		chunk->lights.keys = NULL;
		chunk->lights.max_elem = chunk->lights.num_elem = 0;
	}

}


static void _assign_new_state(stream_chunk_t* const chunk, Vector_t new_pos, int64_t col, int64_t row)
{
	if (!Vec_isfuzzyequal(&chunk->bounds.position, &new_pos, 0.1f) && chunk->unload)
	{
		chunk->bounds.position = new_pos;

		volatile uint32_t chunk_state = S_EMPTY;

		atomic_set_value(&chunk_state, chunk->state);

		if (chunk_state != S_EMPTY && chunk_state != S_UNLOADED && chunk_state != S_UNLOADING && chunk_state != S_OUTOFBOUND && chunk_state != S_FRONTUNLOADING)
		{
			
			atomic_set_value(&chunk->state, S_UNLOADING);
		}
			

		chunk->world_col = col;
		chunk->world_row = row;
		chunk->unload = wfalse;
	}
}

void level_stream_reposchunks(level_stream_t* const level, stream_chunk_t* const center_chunk)
{
	_assign_new_state(level->access_chunk[O_BOTTOMLEFT], Vec(center_chunk->bounds.position.x - center_chunk->bounds.width, center_chunk->bounds.position.y - center_chunk->bounds.height), center_chunk->world_col - 1, center_chunk->world_row - 1);

	_assign_new_state(level->access_chunk[O_BOTTOM], Vec(center_chunk->bounds.position.x, center_chunk->bounds.position.y - center_chunk->bounds.height), center_chunk->world_col, center_chunk->world_row - 1);

	_assign_new_state(level->access_chunk[O_BOTTOMRIGHT], Vec(center_chunk->bounds.position.x + center_chunk->bounds.width, center_chunk->bounds.position.y - center_chunk->bounds.height), center_chunk->world_col + 1, center_chunk->world_row - 1);

	_assign_new_state(level->access_chunk[O_LEFT], Vec(center_chunk->bounds.position.x - center_chunk->bounds.width, center_chunk->bounds.position.y), center_chunk->world_col - 1, center_chunk->world_row);

	_assign_new_state(level->access_chunk[O_RIGHT], Vec(center_chunk->bounds.position.x + center_chunk->bounds.width, center_chunk->bounds.position.y), center_chunk->world_col + 1, center_chunk->world_row);

	_assign_new_state(level->access_chunk[O_TOPLEFT], Vec(center_chunk->bounds.position.x - center_chunk->bounds.width, center_chunk->bounds.position.y + center_chunk->bounds.height), center_chunk->world_col - 1, center_chunk->world_row + 1);

	_assign_new_state(level->access_chunk[O_TOP], Vec(center_chunk->bounds.position.x, center_chunk->bounds.position.y + center_chunk->bounds.height), center_chunk->world_col, center_chunk->world_row + 1);

	_assign_new_state(level->access_chunk[O_TOPRIGHT], Vec(center_chunk->bounds.position.x + center_chunk->bounds.width, center_chunk->bounds.position.y + center_chunk->bounds.height), center_chunk->world_col + 1, center_chunk->world_row + 1);
}

void level_stream_update_chunk_worldpos(level_stream_t* const level, Vector_t center_pos)
{
	//get the world chunk pos from the vector in parameter

	int64_t center_world_col = (int64_t)wfloor(center_pos.x / level->base_width);
	int64_t center_world_row = (int64_t)wfloor(center_pos.y / level->base_height);

	level->first_load = wtrue;//load all chunk on the same frame, no need to avoid frame slowdown in case of teleport
	level->access_chunk[O_CENTER]->world_row = center_world_row;
	level->access_chunk[O_CENTER]->world_col = center_world_col;

	volatile uint32_t chunk_state = S_EMPTY;

	atomic_set_value(&chunk_state, level->access_chunk[O_CENTER]->state);

	if (chunk_state != S_EMPTY && chunk_state != S_UNLOADED && chunk_state != S_UNLOADING && chunk_state != S_OUTOFBOUND && chunk_state != S_FRONTUNLOADING)
		atomic_set_value(&level->access_chunk[O_CENTER]->state, S_UNLOADING);

	//force reload all chunk
	for (uint32_t ichunk = 0; ichunk < CHUNK_NUMBER; ichunk++)
	{
		level->chunks[ichunk].unload = wtrue;
	}


	int64_t xpos = 0;
	int64_t ypos = 0;

	wbool found = level_stream_getchunkdbpos(level, center_world_row, center_world_col, &xpos, &ypos);

	if (!found) {
		//compute value from world info
		xpos = center_world_col * level->base_width;
		ypos = center_world_row * level->base_height;
	}


	level->access_chunk[O_CENTER]->bounds.position = Vec((wfloat)xpos, (wfloat)ypos);

	level_stream_reposchunks(level, level->access_chunk[O_CENTER]);

}

void level_stream_chunks_doswitch(level_stream_t* stream_level)
{
	//iterate on all loaded chunk and recreate element lost between static and stream switch
	for (uint32_t ichunk = 0; ichunk < CHUNK_NUMBER; ichunk++)
	{
		Map* map_data = map__unpack(NULL, stream_level->chunks[ichunk].content_buffer_size, stream_level->chunks[ichunk].content_buffer);
		//level_stream_load_chunkgraph(level, &level->chunks[ichunk], map_data, ichunk);

		for (uint32_t i = 0; i < map_data->n_layer_array; i++)
		{
			_level_stream_loadgeneralgraph(stream_level, &stream_level->chunks[ichunk], map_data, i, wfalse);
		}

		map__free_unpacked(map_data, NULL);
	}
}


void level_stream_checktriggers(level_stream_t* const level, const cpBB entity_box, const char* entity_id)
{
	for (uint32_t ichunk = 0; ichunk < CHUNK_NUMBER; ichunk++)
	{
		uint32_t o_index = 0;
		uint32_t o_count = 0;

		volatile uint32_t chunk_state = S_EMPTY;

		atomic_set_value(&chunk_state, level->chunks[ichunk].state);

		if (chunk_state == S_LOADED && level->chunks[ichunk].visible)
		{
			for (trigger_t* oent = (trigger_t*)hashmap_iterate(&level->chunks[ichunk].triggers, &o_index, &o_count); oent != NULL; oent = (trigger_t*)hashmap_iterate(&level->chunks[ichunk].triggers, &o_index, &o_count))
			{
				trigger_update(oent, entity_box, entity_id, &level->current_module[0]);
			}
		}
	}
}

void level_stream_resettriggers(level_stream_t* const level)
{
	for (uint32_t ichunk = 0; ichunk < CHUNK_NUMBER; ichunk++)
	{
		uint32_t o_index = 0;
		uint32_t o_count = 0;

		volatile uint32_t chunk_state = S_EMPTY;

		atomic_set_value(&chunk_state, level->chunks[ichunk].state);

		if (chunk_state == S_LOADED && level->chunks[ichunk].visible)
		{
			for (trigger_t* oent = (trigger_t*)hashmap_iterate(&level->chunks[ichunk].triggers, &o_index, &o_count); oent != NULL; oent = (trigger_t*)hashmap_iterate(&level->chunks[ichunk].triggers, &o_index, &o_count))
			{
				oent->activated = wfalse;
			}
		}
	}
}

trigger_t* level_stream_get_trigger_intersect(level_stream_t* const level, const cpBB entity_box, const char* entity_id)
{
	for (uint32_t ichunk = 0; ichunk < CHUNK_NUMBER; ichunk++)
	{
		uint32_t o_index = 0;
		uint32_t o_count = 0;

		volatile uint32_t chunk_state = S_EMPTY;

		atomic_set_value(&chunk_state, level->chunks[ichunk].state);

		if ((chunk_state == S_LOADED || chunk_state == S_SAVED) && level->chunks[ichunk].visible)
		{
			for (trigger_t* oent = (trigger_t*)hashmap_iterate(&level->chunks[ichunk].triggers, &o_index, &o_count); oent != NULL; oent = (trigger_t*)hashmap_iterate(&level->chunks[ichunk].triggers, &o_index, &o_count))
			{
				if (trigger_checkbb(oent, entity_box)) {
					return oent;
				}
			}
		}
	}

	return NULL;
}