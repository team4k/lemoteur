#ifndef __THREAD_FUNC_H__

#define __THREAD_FUNC_H__ 1

#include <Engine.h>

#if defined(_WIN32)
#include <windows.h>

#if defined(WINAPI_FAMILY) && (WINAPI_FAMILY == WINAPI_FAMILY_PHONE_APP)
#include <thread>
#endif

#elif defined(EMSCRIPTEN)
#include <emscripten.h>
#else
#include <pthread.h>
#endif

#if defined(_WIN32)
typedef struct
{
	CRITICAL_SECTION cr_sec;
	CONDITION_VARIABLE cr_cond;
	volatile uint32_t pred;

} cond_wait_t;
#elif defined (EMSCRIPTEN)
typedef struct
{
	volatile uint32_t pred;
} cond_wait_t;
#else
typedef struct
{
	pthread_cond_t wait_cond;
	pthread_mutex_t wait_mutex;
	volatile uint32_t pred;
} cond_wait_t;
#endif


WAFA void thread_cond_init(cond_wait_t* const thread_cond);
WAFA void thread_cond_wait(cond_wait_t* const thread_cond);
WAFA void thread_cond_wakeup(cond_wait_t* const thread_cond);
WAFA void thread_cond_free(cond_wait_t* const thread_cond);

#endif