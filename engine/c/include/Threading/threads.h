#ifndef __THREADS_H__

#define __THREADS_H__ 1

#if defined(_WIN32)
    typedef void (*thread_func_t)(void* param);
#else
    typedef void (*thread_func_t)(void* param);
#endif

#define THREAD_NOTSTARTED -1
#define THREAD_WORKING 0 
#define THREAD_WAITING 1

typedef struct
{
    #if defined(_WIN32)
            #if defined(WINAPI_FAMILY) && (WINAPI_FAMILY == WINAPI_FAMILY_PHONE_APP)
                    std::thread* thread;
            #else
                    HANDLE thread;
            #endif
    #elif defined(EMSCRIPTEN)
            char* worker_data;
            size_t worker_data_size;
            worker_handle thread;
            char func_name[128];
    #else
            pthread_t* thread;
    #endif

    thread_func_t func;
    void* func_param;
    cond_wait_t thread_cond;
    volatile uint32_t thread_state;
    wbool initialized;

} thread_t;

//create thread object
WAFA void thread_create(thread_t* const thread_obj,thread_func_t func,void* func_param,const char* worker_name,const char* func_name);

//run or wake up the thread
WAFA void thread_run(thread_t* const thread_obj);

#ifdef EMSCRIPTEN
    //run the emscripten worker
    WAFA void thread_simupdate(thread_t* const thread_obj);
#endif

//stop and cleanup the thread object
WAFA void thread_free(thread_t* thread_obj);

#endif
