#ifndef __FUNCTIONS__

#define __FUNCTIONS__ 1

#include <Base/types.h>
#include <Engine.h>
#include <math.h>


#if defined(_WIN32)
	#include <Windows.h>
	#define TIME_PROF LARGE_INTEGER
#else
	#include <time.h>
	#define TIME_PROF struct timespec

        # ifndef __u_char_defined
                typedef unsigned char u_char;
                #define __u_char_defined
        #endif
#endif

//get bit from a char variable (b = the variable, p = the index of the bit inside the variable (0-7), n = the number of bit we want to return (>=1)
#define TAKE_N_BITS_FROM(b, p, n) (((b) >> (p)) & ((1 << (n)) - 1))

//
//MATH FUNCTIONS
//

//this function are not defined prior to VC 2012, add a specific check for windows phone 8.0 (as opposed to WP 8.1 which has these functions)
#if defined(_MSC_VER) && ((_MSC_VER < 1700) || WINVER == 0x0602) 
W_INLINE float fminf(float val1,float val2) { return (val1 < val2) ? val1 : val2; }
W_INLINE float fmaxf(float val1,float val2) { return (val1 > val2) ? val1 : val2; }
W_INLINE float roundf(float number){ return number < 0.0f ? ceilf(number - 0.5f) : floorf(number +0.5f);}
W_INLINE double fmin(double val1,double val2) { return (val1 < val2) ? val1 : val2; }
W_INLINE double fmax(double val1,double val2) { return (val1 > val2) ? val1 : val2; }
#endif

#ifndef min
W_INLINE int min(int val1,int val2) { return (val1 < val2) ? val1 : val2; }
#endif

#ifndef max
W_INLINE int max(int val1,int val2) { return (val1 > val2) ? val1 : val2; }
#endif


#ifdef MATH_DOUBLE
	#define wceil ceil
	#define wfloor floor
	#define wfabs fabs
	#define wfmin fmin
	#define wfmax fmax
#else
	#define wceil ceilf
	#define wfloor floorf
	#define wfabs fabsf
	#define wfmin fminf
	#define wfmax fmaxf
#endif

W_INLINE wfloat fround(wfloat f) {

	if(f > 0)
		return wfloor(f + WFLOAT05);
	else
		return wceil(f - WFLOAT05);
}


//def
///tolerance value example : 0.001
W_INLINE wbool fuzzyEquals(wfloat a, wfloat b, wfloat tolerance) {
    const wfloat absdiff = wfabs(a - b);
    const wbool rtn = (wbool)((a == WFLOAT0 && b == WFLOAT0) || absdiff < tolerance);
    return rtn;
}

W_INLINE wfloat deg2rad(wfloat degree) { return (degree * (WM_PI / WFLOAT180)); };

W_INLINE wfloat rad2deg(wfloat radian) { return (radian * (WFLOAT180/WM_PI)); };

//
//FILEPATH AND STRING FUNCTIONS
//

W_INLINE const char* get_filename_ext(const char *filename) {
    const char *dot = strrchr(filename, '.');
    if(!dot || dot == filename) return "";
    return dot + 1;
}


WAFA void get_app_path(char* app_path);
WAFA void get_user_home_dir(wchar_t* userprofilepath);
WAFA wbool directory_exist(const wchar_t* directory);
WAFA wbool create_directory(const wchar_t* directory);
WAFA wbool copy_file(const char* src, const char* dst,wbool overwrite);
WAFA void get_file_name(const char* file_path,char* file_name,wbool with_ext);
WAFA const char* get_file_name2(const char* file_name);
WAFA void get_file_without_ext(char* file_name);
WAFA void str_double_slash(const char* src,char* dest,const char* slash);
WAFA wbool str_has_slash(const char* file_path);
WAFA wbool str_starts_with(const char *pre, const char *str);
WAFA wbool str_end_with(const char* pre, const char* str);
WAFA void combine_rel_path(char* base_path, const char* base_rel_path, size_t size_base_path);
WAFA char* string_alloc(const char* src);

#define _NXT	0x80
#define _SEQ2	0xc0
#define _SEQ3	0xe0
#define _SEQ4	0xf0
#define _SEQ5	0xf8
#define _SEQ6	0xfc

#define _BOM	0xfeff

#define UTF8_IGNORE_ERROR		0x01
#define UTF8_SKIP_BOM			0x02


//unicode functions
//convert from / to utf-8 on non windows platform
//convert from / to current ansi codepage on windows
WAFA size_t mb_to_wchar(const unsigned char *in, size_t insize, wchar_t *out,size_t outsize, int flags);
WAFA size_t wchar_to_mb(const wchar_t *in, size_t insize, char *out, size_t outsize,int flags);


#ifdef ANDROID_BUILD
//copy manually char to wide char
	size_t a_wcslen(const wchar_t *s);
	void a_mbstowcs(wchar_t* dest,size_t dest_size,const char* src,size_t count);
	int a_wcscmp(const wchar_t* str1,const wchar_t* str2);
	wchar_t* a_wcscpy(wchar_t* dest,size_t copy_size,const wchar_t* src);
#else
	#define a_wcslen wcslen
	#define a_mbstowcs w_mbstowcs
	#define a_wcscmp wcscmp
	#define a_wcscpy w_wcscpy
#endif

//
//THREADING AND TIME FUNCTIONS
//
WAFA void atomic_set_value(volatile uint32_t* target,uint32_t value);
WAFA void atomic_increment(volatile uint32_t* target);

#if defined(_WIN32)	
WAFA void usleep(double waitTime);
#endif

WAFA void nano_sleep(long waitTime,wbool focus);

//profiling functions
WAFA TIME_PROF time_diff(TIME_PROF* start, TIME_PROF* end);
WAFA void time_get_char_diff(char* content, TIME_PROF* start, TIME_PROF* end, size_t content_size);
WAFA void time_get_time(TIME_PROF* prof_value);
WAFA long time_get_nsec(TIME_PROF* prof_value);

//guid functions
WAFA void gen_guid(char* out_guid);

#endif

