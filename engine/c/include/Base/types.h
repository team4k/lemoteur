#ifndef __TYPES__

#define __TYPES__ 1

#include <inttypes.h>

#ifdef _MSC_VER
#define W_INLINE __forceinline /* use __forceinline (VC++ specific) */
#else
#define W_INLINE inline       /* use standard inline */
#endif

typedef enum { wfalse, wtrue} wbool;
typedef wbool(*check_col_t)(int32_t collision_value);
W_INLINE const char* convertbooltostring(wbool value) { if (value) { return "true"; } else { return "false"; } }

typedef struct
{
	int* pathresult;
	int flagsresult;
} pathfind_response_t;

typedef void(*tlayer_randomizer_t)(int32_t* const grid_to_fill, uint16_t col_count, uint16_t row_count, uint16_t tilesize, int16_t texture_width, int16_t texture_height);

#ifdef MATH_DOUBLE
	#define wfloat double
	#define WFLOAT0 0.0
	#define WFLOAT05 0.5
	#define WFLOAT180 180.0
	#define WFLOAT1 1.0
	#define WFLOAT15 1.5
	#define WFLOAT2 2.0
	#define wsqrt sqrt
	#define wround round
	#define uwint uint64_t
	#define wint int64_t
#else
	#define wfloat float
	#define WFLOAT0 0.0f
	#define WFLOAT05 0.5f
	#define WFLOAT180 180.0f
	#define WFLOAT1 1.0f
	#define WFLOAT15 1.5f
	#define WFLOAT2 2.0f
	#define wsqrt sqrtf
    #define wround roundf
	#define uwint uint32_t
	#define wint int32_t
#endif

#endif

