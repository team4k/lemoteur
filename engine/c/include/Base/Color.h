#ifndef __COLOR__

#define __COLOR__ 1

typedef struct
{
    float r;
    float g;
    float b;

} Color_t;

typedef struct
{
	float r;
    float g;
    float b;
	float a;

} ColorA_t;


static const ColorA_t red_color = {1.0f,0.0f,0.0f,1.0f};
static const ColorA_t green_color = {0.0f,1.0f,0.0f,1.0f};
static const ColorA_t blue_color = {0.0f,0.0f,1.0f,1.0f};
static const ColorA_t white_color = {1.0f,1.0f,1.0f,1.0f};
static const ColorA_t black_color = {0.0f,0.0f,0.0f,1.0f};

W_INLINE float convertcolorint_tofloat(int32_t color_value) { return (color_value != 0) ? (float)(color_value / 255.0f) : 0.0f;}

W_INLINE ColorA_t convertint_tocolor(int32_t r,int32_t g,int32_t b,int32_t a)
{
	ColorA_t conv_col;
	conv_col.r = (r != 0) ? (float)(r / 255.0f) : 0.0f;
	conv_col.g =  (g != 0) ? (float)(g / 255.0f) : 0.0f;
	conv_col.b = (b != 0) ? (float)(b / 255.0f) : 0.0f;
	conv_col.a =  (a != 0) ? (float)(a / 255.0f) : 0.0f;

	return conv_col;
}

W_INLINE ColorA_t convertstring_tocolor(const char* string)
{
	ColorA_t conv_col;
	uint8_t r,g,b,a;

	char rval[3] = { 0 };
	char gval[3] = { 0 };
	char bval[3] = { 0 };
	char aval[3] = { 0 };

	w_strncpy(rval, 3, string,2);
	w_strncpy(gval, 3, &string[2],2);
	w_strncpy(bval, 3, &string[4],2);
	w_strncpy(aval, 3, &string[6],2);

	r = (uint8_t)strtol(rval, NULL, 16);
	g = (uint8_t)strtol(gval, NULL, 16);
	b = (uint8_t)strtol(bval, NULL, 16);
	a = (uint8_t)strtol(aval, NULL, 16);

	conv_col.r = (r != 0) ? (float)(r / 255.0f) : 0.0f;
	conv_col.g = (g != 0) ? (float)(g / 255.0f) : 0.0f;
	conv_col.b = (b != 0) ? (float)(b / 255.0f) : 0.0f;
	conv_col.a = (a != 0) ? (float)(a / 255.0f) : 0.0f;

	return conv_col;
}

W_INLINE ColorA_t ColorA(float r,float g,float b,float a)
{
	ColorA_t conv_col;
	conv_col.r = r;
	conv_col.g = g;
	conv_col.b = b;
	conv_col.a = a;

	return conv_col;
}

#endif