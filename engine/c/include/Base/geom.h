#ifndef __GEOM__

#define __GEOM__ 1

#include <inttypes.h>
#include <Engine.h>
//geometry types

//Vector
typedef struct
{
    wfloat x;
    wfloat y;
    
} Vector_t;

static const Vector_t vectorzero = {0.0f,0.0f};

typedef struct
{
    wfloat x;
    wfloat y;
    wfloat z;
} Vector3_t;

static const Vector3_t vector3zero = {0.0f,0.0f,0.0f};


WAFA void Vec_add(Vector_t* src,const Vector_t add);
WAFA void Vec_sub(Vector_t* src, const Vector_t sub);
WAFA Vector_t Vec_mulvec_f(const Vector_t vec1,const wfloat fmul);
WAFA Vector_t Vec_mulvec(const Vector_t vec1,const Vector_t vec2);
WAFA void Vec_mulsrc_f(Vector_t* src,const wfloat mul);
WAFA void Vec_mulsrc(Vector_t* src,const Vector_t mul);
WAFA Vector_t Vec_div_f(const Vector_t vec1,const wfloat div);
WAFA Vector_t Vec_div(const Vector_t vec1,const Vector_t vec2);
WAFA void Vec_divsrc_f(Vector_t* src,const wfloat div);
WAFA void Vec_divsrc(Vector_t* src,const Vector_t div);
WAFA char Vec_isequal(const Vector_t vec1,const Vector_t vec2);
WAFA Vector_t Vec_plus(const Vector_t vec1,const Vector_t vec2);
WAFA Vector_t Vec_minus(const Vector_t vec1,const Vector_t vec2);
WAFA wfloat Vec_length(Vector_t* vec);
WAFA wfloat Vec_lengthsquared(const Vector_t vec);
WAFA wfloat Vec_normalise(Vector_t* vec);
WAFA wfloat Vec_dot(Vector_t vec1, Vector_t vec2);
WAFA Vector_t Vec_rotate(const Vector_t vec1,const wfloat degAngle);
WAFA void Vec_ApplyAngle(Vector_t* src,const wfloat degAngle);
WAFA Vector_t Vec_project(const Vector_t* const point,const Vector_t* const src,const wfloat project_distance);
WAFA wfloat Vec_dist(const Vector_t vec1,const Vector_t vec2);
WAFA Vector_t Vec(wfloat x,wfloat y);
WAFA wbool Vec_isfuzzyequal(const Vector_t* const vec1,const Vector_t* const vec2,wfloat tolerance);

//Rectangle
//position is by defaut in the top left corner of the rectangle
typedef struct
{
	wint width;
	wint height;
    Vector_t position;
} Rect_t;

//World rectangle
typedef struct
{
	wint worldwidth;
	wint worldheight;
	Rect_t loadeddimension;
} WorldRect_t;

static const Rect_t rectzero = { 0,0,{ 0.0f,0.0f } };
static const WorldRect_t worldrectzero = { 0,0,{0,0, { 0.0f,0.0f } } };


typedef struct
{
	float radius;
	Vector_t position;
} Circle_t;

WAFA wbool circle_intersect_circle(const Circle_t circle1,const Circle_t circle2);

W_INLINE wfloat recget_minx(const Rect_t* rec) { return rec->position.x; }
W_INLINE wfloat recget_maxx(const Rect_t* rec) { return rec->position.x  + rec->width; }
W_INLINE wfloat recget_miny(const Rect_t* rec) { return rec->position.y - rec->height ; }
W_INLINE wfloat recget_maxy(const Rect_t* rec) { return rec->position.y; }
W_INLINE wfloat sign(const Vector_t* const v1,const Vector_t* const v2,const Vector_t* const v3) {return (v1->x - v3->x) * (v2->y  - v3->y) - (v2->x - v3->x) * (v1->y - v3->y) ;}

W_INLINE wbool vector_in_rect(const Rect_t* rec,const Vector_t* vec) { return (wbool)((vec->x >= rec->position.x && vec->x < rec->position.x +  rec->width) && (vec->y < rec->position.y && vec->y >= rec->position.y - rec->height)); }

W_INLINE wbool vector_in_rect_originbottomleft(const Rect_t* rec,const Vector_t* vec) { return (wbool)((vec->x >= rec->position.x && vec->x < rec->position.x +  rec->width) && (vec->y >= rec->position.y && vec->y < rec->position.y + rec->height)); }

WAFA wbool circle_intersect_rect(const Rect_t rec,const Circle_t circ);

typedef enum {e_bottom,e_top,e_left,e_right,e_slope} edge_type;

//edge
typedef struct
{
	Vector_t start; //start of the edge
	Vector_t end; //end of the edge
	edge_type type; //position of the edge on a tile 
} edge_t;

typedef struct
{
	edge_t* p_edge;
	wbool is_src;
	int32_t row_num;
	int32_t col_num;
} edge_container_t;

WAFA wbool edge_intersect_circle(const edge_t* const edge,const Vector_t circle_center,const wfloat circle_radius);


typedef struct
{
	Vector_t point_a;
	Vector_t point_b;
	Vector_t point_c;
} triangle_t;

WAFA wbool Vec_in_triangle(const Vector_t* const vec,const triangle_t* const triangle);
WAFA wbool circle_intersect_triangle(const Circle_t circle,const triangle_t triangle);
WAFA wbool triangle_intersect_triangle(const triangle_t* const triangle1,const triangle_t* const triangle2);

typedef struct
{
	Vector_t point_a;
	Vector_t point_b;
	Vector_t point_c;
	Vector_t point_d;

} shadow_volume_t;

//wbool edge_intersect_shadowvolume(const edge_t* const edge,const shadow_volume_t* const volume);
#endif

