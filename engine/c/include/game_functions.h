#ifndef __GAME_FUNCTIONS_H__

#define __GAME_FUNCTIONS_H__ 1

#include <GameObjects/level_base.h>

//function to call external api functions (achievements / ads / etc)
typedef void (*game_unlock_achievement_t)(const char* id);
typedef void (*game_show_achievements_t)();
typedef void (*game_show_ads_t)();
typedef void (*game_load_ads_t)();

//game dll function declaration
typedef void (*game_init_objects_t)(resources_manager_t* resx_mngr,int player_start_x,int player_start_y,physics_manager_t* phy_mgr,level_base_t* lvl_mngr,const char* font_path,config_t* config,render_manager_t* render_mngr,localization_t* localization_obj,lua_State* script_mngr,void* sound_mngr,input_mngr_t* input_mngr);
typedef void (*game_init_physics_t)(physics_manager_t* mngr);
typedef void (*game_changephysicsmanager_t)(physics_manager_t* const mngr);
typedef void (*game_init_scripts_t)(lua_State* lua_state);
typedef cpVect (*game_get_playerpos_t)(int player_num);
typedef void (*game_player_reposition_t)(cpVect pos);
typedef void (*game_debuginfo_update_t)();
typedef void (*game_draw_t)(int zindex);
typedef void (*game_debug_draw_t)(float text_x,float text_y);
typedef void (*game_update_t)(float elapsedTime);
typedef void (*game_move_t)(float mX,float mY);
typedef void (*game_target_t)(float targetX,float targetY,wbool pointer_control,int button_id);
typedef cpBB (*game_get_playerbox_t)(int player_num);
typedef void (*game_editor_reset_t)();
typedef void (*game_endtarget_t)(float targetX,float targetY,wbool pointer_control,int button_id);
typedef void (*game_movetarget_t)(float targetX,float targetY,wbool pointer_control);
typedef void (*game_offset_target_t)(float offsetX,float offsetY);
typedef void (*game_update_cursorpos_t)(float x,float y);
typedef void (*game_setcursorpos_t)(float x,float y);
typedef void (*game_cleanup_t)(wbool closing);
typedef void (*game_updatescroll_t)();
typedef void (*game_keepresources_t)();
typedef wbool (*game_has_player_t)(int player_num);
typedef void (*game_config_updated_t)();
typedef void (*game_remove_physics_t)(physics_manager_t* const phy_mgr);
typedef void  (*game_free_t)();
typedef void  (*game_g_cleanup_t)();
typedef void  (*game_g_create_t)();
typedef void  (*game_pause_t)(wbool playsound);
typedef void  (*game_unpause_t)();
typedef void (*game_chunk_loaded_t)(void* chunk_data,wbool backgroundloading);
typedef void (*game_level_loaded_t)();
typedef const char* (*game_getversion_t)();
typedef wbool (*game_disableinput_t)();
typedef void (*game_input_t)(int16_t input_event,wbool pressed,wbool from_keyboard);
typedef void (*game_setexternapifunc_t)(game_unlock_achievement_t func1,game_show_achievements_t func2,game_show_ads_t func3,game_load_ads_t func4);
typedef void (*game_joystick_axe_t)(int16_t joystick_id,int16_t axe,float axe_value);
typedef void (*game_joystick_button_t)(int16_t joystick_id,int16_t button,wbool pressed);
typedef void (*game_changeinputmode_t)(const char* new_mode);
typedef void (*game_char_input_t)(uint32_t codepoint);
typedef void (*game_wheelpinch_t)(float delta_value,wbool pointer_control);
typedef void* (*game_getpausemenu_t)();
typedef void (*game_handlebackbutton_t)();
typedef void (*game_initlevelstream_t)(const char* app_path, const char* homepath, void* module_manager);
typedef void (*game_init_lights_t)(void* light_manager);
typedef const char* (*game_getengineversion_t)();

#endif
