#ifndef __RENDER_TEXT_H__

#define __RENDER_TEXT_H__ 1

#include <inttypes.h>
#include <Base/types.h>
#include <Base/Color.h>
#include <Base/geom.h>
#include <Graphics/FontLoader.h>
#include <Graphics/SpriteBatch.h>
#include <Render/render_manager.h>

typedef enum {TP_CENTER,TP_TOPCENTER,TP_TOPLEFT,TP_TOPRIGHT,TP_BOTTOMCENTER,TP_BOTTOMLEFT,TP_BOTTOMRIGHT,TP_RIGHTCENTER,TP_LEFTCENTER} render_text_positions;

typedef struct
{
   //AssetManager assets;                               // Asset Manager
   SpriteBatch_t batch;                                 // Batch Renderer
   
   wchar_t* message; //message to draw
   ColorA_t* colors; //characters individual color (same size than message array)
   uint8_t* char_sizes; //characters individual size (same size than message array)
   uint16_t drawmessage; //flag indicate if the message is visible
   const font_info* info;

   wbool fixed_size;//flag to indicate if the text size change depending of the zoom value
	
} Render_Text_t;

#define CHAR_SPACE 32 //Space character

WAFA int Render_Text_Init(Render_Text_t* text,int32_t batch_size,render_manager_t* const render_mngr);
WAFA void Render_Text_setfixedsize(Render_Text_t* const text,wbool fix_size);
WAFA void Render_Text_setscale(Render_Text_t* text,float scale_x,float scale_y);
WAFA void Render_Text_Setinfo(Render_Text_t* text,const font_info* info);
WAFA void Render_Text_begin(Render_Text_t* text,float red,float green, float blue, float alpha,render_manager_t* const render_mngr);
WAFA void Render_Text_end(Render_Text_t* text,render_manager_t* const render_mngr);
WAFA void Render_Text_Draw(Render_Text_t* const text, const wchar_t* message, float x_orig, float y_orig, uint8_t fontsize, render_manager_t* const render_mngr);
WAFA void Render_Text_Draw_withcolornsize(Render_Text_t* const text, const wchar_t* message, float x_orig, float y_orig, uint8_t fontsize, render_manager_t* const render_mngr, const ColorA_t* colors, const float* char_factor);
WAFA void Render_Text_Draw2(Render_Text_t* const text, const wchar_t* message, float x_orig, float y_orig, uint8_t fontsize, render_manager_t* const render_mngr, uint32_t hidechance, int32_t charreplacer, const ColorA_t* colors, const float* char_factor);
WAFA void Render_Text_Draw_centered(Render_Text_t* text, const wchar_t* message, render_manager_t* const render_mngr, render_text_positions position, SHADERPROGRAM_ID text_prog, ColorA_t text_color, Vector_t offset, uint8_t fontsize);
WAFA float Render_Text_strlen(Render_Text_t* text,const wchar_t* message);
WAFA float Render_Text_charheight(Render_Text_t* text);
WAFA float Render_Text_charwidth(Render_Text_t* text);
WAFA void Render_Text_drawtexture(Render_Text_t* text,float x,float y,render_manager_t* const render_mngr);
WAFA void Render_Text_Free(Render_Text_t* text);

#endif // !__RENDER_TEXT_H__