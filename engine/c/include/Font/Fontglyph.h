#ifndef FONT_FONTGLYPH

#define FONT_FONTGLYPH 1

typedef struct
{
	float u1;  // Top/Left U,V Coordinates
	float v1;
	float u2;// Bottom/Right U,V Coordinates
	float v2;
	unsigned long char_code;
	float atlasleft;
	float atlasbottom;
	float atlasright;
	float atlastop;
	float advance;
	float planeleft;
	float planebottom;
	float planeright;
	float planetop;
	wbool emptychar;
} Fontglyph_t;

void Fontglyph_Init(Fontglyph_t* const fontglyph, const float u1, const float v1, const float u2, const float v2, const unsigned long char_code,
	const float atlasleft, const float atlasbottom, const float atlasright, const float atlastop,
	const float planeleft, const float planebottom, const float planeright, const float planetop,
	const float advance);
void Fontglyph_Init_empty(Fontglyph_t* const fontglyph, const unsigned long char_code, const float advance);
#endif // !FONT_FONTGLYPH