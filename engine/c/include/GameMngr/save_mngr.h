typedef struct
{
	sqlite3* save_db;
	wbool ready;
	char homedir[WAFA_MAX_PATH];
	char save_file[WAFA_MAX_PATH];
} save_mngr_t;

WAFA void save_mngr_init(save_mngr_t* const save_mngr, const char* save_file, const char* home_dir, uint32_t savepoint);
WAFA wbool save_mngr_checkdata(save_mngr_t* const save_mngr,const char* table,const char* key,const char* keyvalue);
WAFA int32_t save_mngr_getint(save_mngr_t* const save_mngr,const char* table,const char* key,const char* keyvalue,const char* column);
WAFA int32_t save_mngr_getint2(save_mngr_t* const save_mngr,const char* table,const char* key,const char* keyvalue,const char* key2,const char* value2,const char* column);
WAFA wbool save_mngr_checkdata2(save_mngr_t* const save_mngr,const char* table,const char* key,const char* keyvalue,const char* key2,const char* keyvalue2);
WAFA double save_mngr_getdouble(save_mngr_t* const save_mngr,const char* table,const char* key,const char* keyvalue,const char* column);
WAFA double save_mngr_getdouble2(save_mngr_t* const save_mngr,const char* table,const char* key,const char* keyvalue,const char* key2,const char* value2,const char* column);
WAFA char* save_mngr_getstring(save_mngr_t* const save_mngr,const char* table,const char* key,const char* keyvalue,const char* column);
WAFA wbool save_mngr_new_data(save_mngr_t* const save_mngr,const char* table,int count,...);
WAFA wbool save_mngr_update_data(save_mngr_t* const save_mngr,const char* table,const char* key,const char* keyval,int count,...);
WAFA wbool save_mngr_update_data2(save_mngr_t* const save_mngr,const char* table,const char* key,const char* keyval,const char* key2,const char* keyval2,int count,...);
WAFA uint32_t save_mngr_getmaxsavepoint(const char* homedir, const char* savebasename);
WAFA void save_mgnr_savepoint(save_mngr_t* const save_mngr);
WAFA void save_mngr_free(save_mngr_t* const save_mngr);