//define key values
#define KEY_DOWN GLFW_KEY_DOWN
#define KEY_UP GLFW_KEY_UP
#define KEY_LEFT GLFW_KEY_LEFT
#define KEY_RIGHT GLFW_KEY_RIGHT
#define KEY_ESC GLFW_KEY_ESCAPE
#define KEY_E GLFW_KEY_E
#define KEY_D GLFW_KEY_D
#define KEY_SPACE GLFW_KEY_SPACE
#define KEY_U GLFW_KEY_U
#define KEY_I GLFW_KEY_I
#define KEY_O GLFW_KEY_O
#define KEY_ENTER GLFW_KEY_ENTER
#define KEY_PAGE_UP GLFW_KEY_PAGE_UP
#define KEY_PAGE_DOWN GLFW_KEY_PAGE_DOWN

#define KEY_PRESS GLFW_PRESS
#define KEY_RELEASE GLFW_RELEASE
#define KEY_REPEAT GLFW_REPEAT

#define MOUSE_BUTTON_LEFT GLFW_MOUSE_BUTTON_LEFT

void input_glfw_init(input_mngr_t* const input_mngr,render_manager_t* const render_manager);
void input_glfw_keyboardevent(input_mngr_t* const input_mngr);
void input_glfw_joystickevent(input_mngr_t* const input_mngr);
void input_glfw_sendinputmessage(input_mngr_t* const input_mngr,uint32_t uMsg,uint32_t wParam,long lParam);
void input_glfw_poll();
int input_glfw_getkey(input_mngr_t* const input_mngr,int key);
const char* input_glfw_getkeyname(input_mngr_t* const input_mngr, int key);
gamepad_scheme input_glfw_getscheme(input_mngr_t* const input_mngr);
const char* input_glfw_getbuttonname(input_mngr_t* const input_mngr, int16_t button, gamepad_scheme scheme);
const char* input_glfw_getaxisname(input_mngr_t* const input_mngr, int16_t axis, gamepad_scheme scheme);
const char* input_glfw_getcontrollername(input_mngr_t* const input_mngr);
wbool input_glfw_is_trigger(input_mngr_t* const input_mngr, int16_t axis);