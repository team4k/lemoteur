#ifndef __INPUT_MNGR_H__

#define __INPUT_MNGR_H__ 1

//input buffer type
#define INPUT_BUFFER_SIZE 20

typedef enum { XBOX_INPUT = 1,PLAYSTATION_INPUT = 2, GENERIC_INPUT = 3 } gamepad_scheme;

typedef struct {
	Vector_t input_data;
	int32_t action;
} input_entry_t;

//input buffer type
typedef struct {
 input_entry_t buffer[INPUT_BUFFER_SIZE];
 int wp;
 int rp;
} input_buffer_t;

#ifdef USE_SDL_INPUT
	#include <SDL2/SDL.h>
#endif

#ifndef _MODULE_MNGR_DECLARE_
	typedef struct _module_mngr module_mngr_t;
	#define _MODULE_MNGR_DECLARE_
#endif


typedef struct
{
	float mouse_sensitivity;
	wbool started_offset;
	Vector_t old_vector;
	Vector_t move_vector;
	Vector_t click_vector;
	int32_t first_found_joystick;
	wbool has_lookup_joystick;
	unsigned char* button_pressed;
	wbool has_focus;
	wbool lock_cursor;
	wbool show_cursor;
	render_manager_t* render_manager;
	module_mngr_t* module_manager;
	wbool request_close;
	wbool editor_mode;

	//only used with buffered input platform (android / ios / windowsphone)
	input_buffer_t input_buffer;

	#ifdef USE_SDL_INPUT
		SDL_GameController* joystick;
	#endif

} input_mngr_t;

#ifdef USE_GLFW_INPUT
	#include <GameMngr/input_glfw.h>
#endif

#ifdef USE_SDL_INPUT
	#include <GameMngr/input_sdl.h>
#endif

#ifdef USE_MOBILE_INPUT
	#include <GameMngr/input_mobile.h>
#endif

#ifdef USE_WINDOWS_INPUT
	#include <GameMngr/input_windows.h>
#endif

WAFA void input_mngr_init(input_mngr_t* const input_mngr,render_manager_t* const render_manager,module_mngr_t* const module_manager,float mouse_sensitivity,wbool lock_cursor,wbool editor_mode,wbool show_cursor);

WAFA void input_mngr_handle_input(input_mngr_t* const input_mngr);

WAFA Vector_t input_getglobalcoords(input_mngr_t* const input_mngr,int local_coordx,int local_coordy);

WAFA void input_mngr_sendinputmessage(input_mngr_t* const input_mngr,uint32_t uMsg,uint32_t wParam,long lParam);

WAFA void input_mngr_poll(input_mngr_t* const input_mngr);

WAFA int input_mngr_getkey(input_mngr_t* const input_mngr,int key);

WAFA void input_mngr_touch(input_mngr_t* const input_mngr,wfloat posx,wfloat posy,int action);

WAFA void input_mngr_backbutton(input_mngr_t* const input_mngr);

WAFA const char* input_mngr_getcontrollername(input_mngr_t* const input_mngr);

WAFA const char* input_mngr_getkeyname(input_mngr_t* const input_mngr, int key);

WAFA gamepad_scheme input_mngr_getscheme(input_mngr_t* const input_mngr);
WAFA const char* input_mngr_getbuttonname(input_mngr_t* const input_mngr, int16_t button, gamepad_scheme scheme);
WAFA const char* input_mngr_getaxisname(input_mngr_t* const input_mngr, int16_t axis, gamepad_scheme scheme);
WAFA wbool input_mngr_is_trigger(input_mngr_t* const input_mngr, int16_t axis);


WAFA void input_mngr_free(input_mngr_t* input_mngr);

#endif