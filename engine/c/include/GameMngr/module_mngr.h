#ifndef _MODULE_MNGR_DECLARE_
typedef struct _module_mngr module_mngr_t;
#define _MODULE_MNGR_DECLARE_
#endif

#ifndef __MODULE_MNGR_H__

#define __MODULE_MNGR_H__ 1


struct _module_mngr
{
	char game_dll_path[260];

	//function pointer to game dll code
	game_init_objects_t game_init_objects;
	game_init_physics_t game_init_physics;
	game_init_scripts_t game_init_scripts;
	game_changephysicsmanager_t game_changephysicsmanager;
	game_get_playerpos_t game_get_playerpos;
	game_player_reposition_t game_player_reposition;
	game_debuginfo_update_t game_debuginfo_update;
	game_draw_t game_draw;
	game_debug_draw_t game_debug_draw;
	game_update_t game_update;
	game_move_t game_move;
	game_target_t game_target;
	game_get_playerbox_t game_get_playerbox;
	game_editor_reset_t game_editor_reset;
	game_endtarget_t game_endtarget;
	game_movetarget_t game_movetarget;
	game_cleanup_t game_cleanup;
	game_updatescroll_t game_updatescroll;
	game_keepresources_t game_keepresources;
	game_offset_target_t game_offset_target;
	game_update_cursorpos_t game_update_cursorpos;
    game_setcursorpos_t game_setcursorpos;
	game_has_player_t game_has_player;
	game_config_updated_t game_config_updated;
	game_remove_physics_t game_remove_physics;
	game_free_t game_free;
	game_g_cleanup_t game_g_cleanup;
	game_g_create_t game_g_create;
	game_pause_t game_pause;
	game_unpause_t game_unpause;
	game_chunk_loaded_t game_chunk_loaded;
	game_level_loaded_t game_level_loaded;
	game_getversion_t game_getversion;
	game_disableinput_t game_disableinput;
	game_input_t game_input;
	game_setexternapifunc_t game_setexternapifunc;
	game_joystick_axe_t game_joystick_axe;
	game_joystick_button_t game_joystick_button;
	game_changeinputmode_t game_changeinputmode;
	game_char_input_t game_char_input;
	game_wheelpinch_t game_wheelpinch;
	game_getpausemenu_t game_getpausemenu;
	game_handlebackbutton_t game_handlebackbutton;
	game_initlevelstream_t game_initlevelstream;
	game_init_lights_t game_initlights;
	game_getengineversion_t game_getengineversion;

	#if defined(_WIN32)
	HMODULE game_module;
	#else
		void* game_module;
	#endif


};


WAFA void module_mngr_init(module_mngr_t* mngr,const char* module_path);
WAFA void module_mngr_check(module_mngr_t* const mngr);
WAFA void module_mngr_release(module_mngr_t* const module_mngr);

#endif