//dummy define
#define KEY_DOWN 0
#define KEY_UP 0
#define KEY_LEFT 0
#define KEY_RIGHT 0
#define KEY_ESC 0
#define KEY_E 0
#define KEY_D 0
#define KEY_SPACE 0
#define KEY_U 0
#define KEY_I 0
#define KEY_O 0
#define KEY_ENTER 0
#define KEY_PAGE_UP 0
#define KEY_PAGE_DOWN 0

#define KEY_PRESS 0
#define KEY_RELEASE 0
#define KEY_REPEAT 0

#define MOUSE_BUTTON_LEFT 0

//touch screen actions
#define ACTION_BACKBUTTON -1

#define ACTION_DOWN 0
#define ACTION_UP 1
#define ACTION_MOVE 2
#define ACTION_POINTER_DOWN 5
#define ACTION_POINTER_UP 6

WAFA void input_mobile_init(input_mngr_t* const input_mngr);

WAFA void input_mobile_touch(input_mngr_t* const input_mngr,wfloat posx,wfloat posy,int action);

WAFA void input_mobile_backbutton(input_mngr_t* const input_mngr);

WAFA void input_mobile_handleinput(input_mngr_t* const input_mngr);