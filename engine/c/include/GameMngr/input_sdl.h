//define key values
#define KEY_DOWN SDLK_DOWN
#define KEY_UP SDLK_UP
#define KEY_LEFT SDLK_LEFT
#define KEY_RIGHT SDLK_RIGHT
#define KEY_ESC SDLK_ESCAPE
#define KEY_E SDLK_e
#define KEY_D SDLK_d
#define KEY_SPACE  SDLK_SPACE
#define KEY_U SDLK_u
#define KEY_I SDLK_i
#define KEY_O SDLK_o
#define KEY_ENTER SDLK_RETURN
#define KEY_PAGE_UP SDLK_PAGEUP
#define KEY_PAGE_DOWN SDLK_PAGEDOWN

#define KEY_PRESS SDL_PRESSED 
#define KEY_RELEASE SDL_RELEASED
#define KEY_REPEAT SDL_PRESSED

#define MOUSE_BUTTON_LEFT SDL_BUTTON_LEFT

void input_sdl_init(input_mngr_t* const input_mngr);
void input_sdl_event(input_mngr_t* const input_mngr);
int input_sdl_getkey(input_mngr_t* const input_mngr, int key);
gamepad_scheme input_sdl_getscheme(input_mngr_t* const input_mngr);
const char* input_sdl_getcontrollername(input_mngr_t* const input_mngr);
const char* input_sdl_getkeyname(input_mngr_t* const input_mngr,int key);
const char* input_sdl_getbuttonname(input_mngr_t* const input_mngr, int16_t button, gamepad_scheme scheme);
const char* input_sdl_getaxisname(input_mngr_t* const input_mngr, int16_t axis, gamepad_scheme scheme);
wbool input_sdl_is_trigger(input_mngr_t* const input_mngr, int16_t axis);
void input_sdl_poll();