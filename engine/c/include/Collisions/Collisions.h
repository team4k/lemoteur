#ifndef __COLLISIONS_H__

#define __COLLISIONS_H__ 1

#define COL_MIN_PUSHBACK 3

WAFA wbool col_check(const Rect_t* rec_1, const Rect_t* rec_2, Rect_t* intersect_result);
WAFA wbool col_check2(const Rect_t* rec_1, const Rect_t* rec_2);
WAFA void col_merge(Rect_t* const destination,const Rect_t* const src);
WAFA void col_getreflectvector(const Rect_t* intersect,Vector_t* reflection);

#endif