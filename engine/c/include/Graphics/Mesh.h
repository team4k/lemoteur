#ifndef __MESH__

#define __MESH__ 1

typedef enum {MT_NONE,MT_TETRAHEDRON,MT_CUBE,MT_CYLINDER} mesh_template_type; 

typedef struct
{
	int32_t width;
	int32_t height;
	int32_t depth;

	ColorA_t color;

	mesh_template_type m_template;

	float* vertices_data;
	VBO_ID vertices_id;

	uint16_t* indices_data;
	VBO_ID indices_id;

	float* normal_data;
	VBO_ID normal_id;

	size_t num_indices;

	Vector3_t position;
	render_mode mesh_mode;

	float rotation_x;
	float rotation_y;
	float rotation_z;

	float scale_x;
	float scale_y;
	float scale_z;

	Rect_t render_box;

	//flicker variables
	float flicker_time;
	float flicker_step_time;
	wbool skip_draw;

	int mat_flag;
	uint32_t script_id;

} Mesh_t;

#define M_FLICKER_DRAW_STEP 20.0f
#define M_FLICKER_HIDE_STEP 20.0f

WAFA void mesh_init(Mesh_t* const mesh_obj,const int32_t width,const int32_t height,const int32_t depth,mesh_template_type template_type,render_manager_t* const render_mngr);


WAFA void mesh_setposition(Mesh_t* const mesh_obj,Vector3_t position);

WAFA void mesh_setrotation(Mesh_t* const mesh_obj,float rot_x,float rot_y,float rot_z);

WAFA void mesh_setscale(Mesh_t* const mesh_obj, float scale_x, float scale_y, float scale_z);

WAFA void mesh_setrendermode(Mesh_t* const mesh_obj,render_mode mesh_mode);

WAFA void mesh_setcolor(Mesh_t* const mesh_obj,ColorA_t color);

WAFA void mesh_flicker(Mesh_t* const mesh,float flicker_time);

WAFA void mesh_draw(Mesh_t* const mesh_obj,render_manager_t* const render_mngr);

WAFA void mesh_update(Mesh_t* const mesh_obj,float elapsed);

WAFA void mesh_g_create(Mesh_t* const mesh_obj,render_manager_t* const render_mngr);

WAFA void mesh_g_clean(Mesh_t* const mesh_obj);

WAFA void mesh_free(Mesh_t* mesh_obj);

#endif