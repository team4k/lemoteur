#ifndef __ANIMATION__

#define __ANIMATION__ 1

#include <inttypes.h>
#include <Base/types.h>
#include <GameObjects/Characters.pb-c.h>

typedef void (*new_frame_callback_t)(void* data);

typedef struct
{
    const AnimationDef* anim_def;
	int current_frame;
    uint32_t frame_counter;
	wbool reverse;
    float current_step_time;
    wbool ended;
	new_frame_callback_t new_frame;
	void* new_frame_data;
} animation_t;


WAFA void animation_init(animation_t* const p_anim,AnimationDef* const anim_def,new_frame_callback_t new_frame_callback,void* callback_data);

WAFA void animation_update(animation_t* const p_anim,const float elapsed);

WAFA void animation_reset(animation_t* const p_anim);

WAFA void animation_setnewframecallback(animation_t* const p_anim,new_frame_callback_t new_frame_callback,void* new_frame_data);

WAFA void animation_set_pause(animation_t* const p_anim,const wbool pause);

WAFA wbool animation_iscontactactive_atframe(animation_t* const p_anim,int32_t contact_indx);

WAFA wbool animation_hascontactpoints(animation_t* const p_anim);

WAFA int32_t animation_getcontactpointindx(animation_t* const p_anim,const char* contact_pointname);

WAFA Vector_t animation_getcontactpos_atframe(animation_t* const p_anim,int32_t contact_indx);

#endif // ! __ANIMATION__