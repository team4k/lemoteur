#ifndef __SPRITE_BATCH__

#define __SPRITE_BATCH__ 1

#include <inttypes.h>
#include <Base/types.h>
#include <Base/Color.h>
#include <Base/geom.h>
#include <Graphics/TextureRegion.h>
#include <Render/render_manager.h>
#include <Graphics/Sprite.h>

typedef struct
{
   float* vertexBuffer;     
   float* textcoordBuffer;
   float* colorBuffer;


   //buffers used to store vertices, textcord and colors in GPU memory, alternate between them to avoid synchronisation in case buffer orphaning doesn't work (which is the case in webgl)
   VBO_ID vertices[3];
   VBO_ID textcoord[3];
   VBO_ID colors[3];

   FENCEID buffersync[3];

   wbool bufferupdated[3];

   int16_t pendingupdateflags;

   int32_t maxSprites;                                    // Maximum Sprites Allowed in Buffer
   int32_t numSprites;                                    // Number of Sprites Currently in Buffer
   wbool* dirty_sprite;
   Vector_t position;
   float rotation;
   float scale_x;
   float scale_y;
} SpriteBatch_t;


#define VERTEX_SIZE 2 // Vertex Size (in Components) ie. (X,Y)
#define COLOR_SIZE 4 // color size (in componentes) ie. (R,G,B,A)
#define VERTICES_PER_SPRITE 6

#define BUFFER_UPDATE_VERTICES 1 
#define BUFFER_UPDATE_TEXCOORDS 2
#define BUFFER_UPDATE_COLORS 4
#define BUFFER_UPDATE_FULL (BUFFER_UPDATE_VERTICES | BUFFER_UPDATE_TEXCOORDS | BUFFER_UPDATE_COLORS)

WAFA void SpriteBatch_Init(SpriteBatch_t* const spritebatch,int32_t maxSprites,render_manager_t* const render_mngr);
WAFA void SpriteBatch_Free(SpriteBatch_t * spritebatch);
WAFA void SpriteBatch_recycle(SpriteBatch_t * spritebatch, render_manager_t* const render_mngr);
WAFA void SpriteBatch_draw(SpriteBatch_t * spritebatch, render_manager_t* const render_mngr, TEXTURE_ID texture_id);
WAFA void SpriteBatch_scheduleupdatebuffer(SpriteBatch_t * spritebatch, int16_t update_flag);
//void SpriteBatch_updatebuffer_fromdirty(SpriteBatch_t * spritebatch,render_manager_t* const render_mngr,int16_t update_flag);
WAFA void SpriteBatch_updateSprite(SpriteBatch_t* spritebatch,int32_t isprite,wfloat x, wfloat y, wfloat width, wfloat height,const TextureRegion_t* region, const ColorA_t color, render_manager_t* const render_mngr);
WAFA void SpriteBatch_updateSprite2(SpriteBatch_t* const spritebatch,int32_t isprite,wfloat x, wfloat y, sprite_t* const sprite,render_manager_t* const render_mngr);
WAFA void SpriteBatch_cleanupsprite(SpriteBatch_t * const spritebatch, int32_t sprite);
WAFA void SpriteBatch_cleanupbuffer(SpriteBatch_t* const spritebatch, int32_t startsprite);
WAFA int32_t SpriteBatch_AddSprite(SpriteBatch_t * spritebatch,wfloat x, wfloat y, wfloat width, wfloat height,const TextureRegion_t* region, const ColorA_t color,render_manager_t* const render_mngr);
WAFA int32_t SpriteBatch_AddSprite2(SpriteBatch_t * spritebatch,wfloat x,wfloat y,sprite_t* const sprite,render_manager_t* const render_mngr);
WAFA int32_t SpriteBatch_AddSprite3(SpriteBatch_t* const spritebatch,Vector_t* const a,Vector_t* const b,Vector_t* const c,Vector_t* const d, const ColorA_t color);
WAFA void SpriteBatch_RemoveSprite2(SpriteBatch_t * spritebatch, sprite_t* const sprite, int32_t index, render_manager_t* const render_mngr);

WAFA void SpriteBatch_g_clean(SpriteBatch_t* const spritebatch);
WAFA void SpriteBatch_g_create(SpriteBatch_t* const spritebatch,render_manager_t* const render_mngr);

#endif
