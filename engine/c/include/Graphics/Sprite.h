#ifndef __SPRITE__

#define __SPRITE__ 1

#include <inttypes.h>
#include <Base/types.h>
#include <Base/Color.h>
#include <Base/geom.h>
#include <Utils/Hash.h>
#include <Graphics/TextureLoader.h>
#include <Graphics/Animation.h>
#include <Render/render_manager.h>
#include <GameObjects/Characters.pb-c.h>

typedef struct
{
    int32_t width;
    int32_t height;
    int32_t texture_width;
    int32_t texture_height;
    texture_info* texture_pointer;
    wbool animated;
	hashtable_t anim_hash;
    animation_t* anim_array;
	animation_t* current_anim;
	size_t max_anim_name_size;
    float coordUnitX;
    float coordUnitY;
	float _rotation;
	wbool mirror_texture_x;
	wbool mirror_texture_y;
	float scale_x;
	float scale_y;

	int32_t txtColCount;

	//repeat texture / tesselate sprite
	//if repeat_texture = wtrue then the texture is repeated across the repeat_x / y value based on the sprite size
	//if repeat_texture = wfalse then the texture is mapped across the entire sprite, segmented on the number of sprite chunk based on repeat_x / y values and the sprite size
	int32_t repeat_x;
	int32_t repeat_y;
	wbool repeat_texture;

	ColorA_t color;

	//flicker variables
	float flicker_time;
	float flicker_step_time;
	wbool skip_draw;

	//fade in / out variables
	float fade_amount;
	float fade_amout_step;
	wbool dofade;

	int32_t* frame; //array of frame values

	float* tex_coords;
	float* vertices;

	size_t vertices_buffer_size;
	size_t tex_coords_buffer_size;

	int32_t num_vertices;

	VBO_ID tex_coords_vbo;
	VBO_ID vertices_vbo;

	wbool has_tex_vbo;
	wbool has_vert_vbo;


	wbool tex_coord_needupdate;
	wbool vertices_needupdate;
	wbool in_group;

	Rect_t render_box;

	Vector_t storedoffset;

	int mat_flag;

	uint32_t script_id;

} sprite_t;

#define FLICKER_DRAW_STEP 20.0f
#define FLICKER_HIDE_STEP 20.0f

WAFA void sprite_init(sprite_t* const sprite,const int32_t sprite_width,const int32_t sprite_height, const int32_t texture_width, const int32_t texture_height,texture_info* const texture_pointer,const AnimationList* const animation_def,int32_t repeat_x,int32_t repeat_y,render_manager_t* const render_manager,wbool in_group,int32_t start_frame,float scale_x,float scale_y,wbool repeat_texture);

WAFA void sprite_updateanimation(sprite_t* const sprite,const AnimationList* const animation_def);

WAFA void sprite_settexture(sprite_t* const sprite,texture_info* const texture_pointer);

WAFA void sprite_setcolor(sprite_t* const sprite,ColorA_t new_color);

WAFA float sprite_getrotation(sprite_t* const sprite);

WAFA void sprite_setrotation(sprite_t* const sprite,float rotation);

WAFA void sprite_setsize(sprite_t* const sprite,int32_t width,int32_t height);

WAFA void sprite_flicker(sprite_t* const sprite,float flicker_time);

WAFA void sprite_fade(sprite_t* const sprite,float fade_time,int fade_direction);

WAFA void sprite_setframe(sprite_t* const sprite,int frame,int position);

WAFA void sprite_setrepeat(sprite_t* const sprite,int32_t repeatx,int32_t repeaty);

WAFA void sprite_setstoredoffset(sprite_t* const sprite, Vector_t offset);

WAFA Vector_t sprite_getstoredoffset(sprite_t* const sprite);

WAFA void sprite_draw(sprite_t* const sprite, float x,float y,render_manager_t* const render_mngr,void  (*ondraw_func)(render_manager_t* const render_mngr));

WAFA void sprite_draw_withzindex(sprite_t* const sprite, float x, float y, int z_index, render_manager_t* const render_mngr, void(*ondraw_func)(render_manager_t* const render_mngr));

WAFA void sprite_playanimation(sprite_t* const sprite,const char* anim_name);

WAFA void sprite_playanimation2(sprite_t* const sprite, const char* anim_name, wbool reset_flicker);

WAFA void sprite_playanimation3(sprite_t* const sprite, const char* anim_name, wbool reset_flicker, wbool reverse);

WAFA void sprite_stopanimation(sprite_t* const sprite);

WAFA void sprite_update(sprite_t* const sprite, const float elapsed);

WAFA void sprite_g_create(sprite_t* const sprite,render_manager_t* const render_mngr);

WAFA void sprite_genvertices_vbo(sprite_t* const sprite,render_manager_t* const render_manager);

WAFA void sprite_setvertices(sprite_t* const sprite,Vector_t* verts,size_t vert_count,render_manager_t* const render_manager);

WAFA void sprite_gentexturesvbo(sprite_t* const sprite,render_manager_t* const render_manager); 

WAFA void sprite_g_clean(sprite_t* const sprite);

WAFA void sprite_free(sprite_t* const sprite);

#endif // !__SPRITE__