#ifndef __LIGHT_MANAGER_H__
#define __LIGHT_MANAGER_H__ 1

#include <inttypes.h>
#include <Base/types.h>
#include <Base/Color.h>
#include <Base/geom.h>
#include <GameObjects/level_base.h>
#include <Graphics/Light.h>
#include <Render/render_manager.h>

#define POOL_LIGHT_NUM 15
#define SHADOW_NUM_ELEMENT_PER_VOLUME 6
#define SHADOW_SIZE_VERTICES 2

typedef wbool(*check_flood_t)(const int32_t grid_value);

typedef struct
{
	ColorA_t ambient_color;
	hashmap_t light_hash;

	FBO_ID fbo;
	TEXTURE_ID shadow_texture;

	SHADERPROGRAM_ID prog_mask;
	SHADERPROGRAM_ID prog_light;
	SHADERPROGRAM_ID prog_render;

	light_model light_gen_type;

	int32_t width_texture;
	int32_t height_texture;

	float texture_render_vertices[12];
	float texture_render_texcoords[12];

	VBO_ID r_vert_buff;
	VBO_ID r_tex_buff;

	uint32_t* lightfloodfillarray;
	size_t lightfloodfillarray_size;

	check_flood_t check_flood;

} light_manager_t;


WAFA void light_manager_init(light_manager_t* const light_manager,ColorA_t ambient_color,render_manager_t* const render_mngr,SHADERPROGRAM_ID prog_mask,SHADERPROGRAM_ID prog_light,SHADERPROGRAM_ID prog_render,
	light_model light_gen_type,int32_t width_texture,int32_t height_texture, uint32_t col_count,uint32_t row_count);

WAFA void light_manager_setshaderprograms(light_manager_t* const light_manager, SHADERPROGRAM_ID prog_mask, SHADERPROGRAM_ID prog_light, SHADERPROGRAM_ID prog_render);

WAFA void light_manager_resizerender(light_manager_t* const light_manager,render_manager_t* const render_mngr);

WAFA light_t* light_manager_addlight(light_manager_t* const light_manager,Vector_t position,ColorA_t color,float radius,const char* light_id,const char* parent_id,light_type l_type);

WAFA light_t* light_manager_getlight(light_manager_t* const light_manager,const char* light_id);

WAFA light_t* light_manager_getnearestlight(light_manager_t* const light_manager,Vector_t position,render_manager_t* const render_mngr);

WAFA void light_manager_removelight(light_manager_t* const light_manager,const char* light_id);

WAFA int32_t light_manager_getinrangelight(light_manager_t* const light_manager, const Rect_t* const render_box, Vector_t position, render_manager_t* const render_mngr, light_t* light_array[MAX_LIGHT_PER_ELEMENT]);

WAFA void light_manager_regenall(light_manager_t* const light_manager);

WAFA void light_manager_regenlight(light_manager_t* const light_manager,const Circle_t circ_regen);

WAFA void light_manager_setfloodcheckfunc(light_manager_t* const light_manager, check_flood_t floodcheck);

///realloc and update floodfillarray size
WAFA void light_manager_updatefloodfillarray(light_manager_t* const light_manager, size_t collisions_array_size);

///set which lights are visible by player based on the level collisions, avoid showing light which are on screen but obscured by existing collisions
WAFA void light_manager_floodlight(light_manager_t* const light_manager, level_base_t* const level_container, const int32_t* const collisions_array, size_t collisions_array_size, const uint32_t tile_size, const uint32_t col_count, const uint32_t row_count, const uint32_t start_tile, const char* map_id);

WAFA void light_manager_rendermask(light_manager_t* const light_manager, edge_container_t* const edge_array, int32_t edge_count, render_manager_t* const render_mngr, wbool draw_direct, const char* parent_id, int32_t tile_size, int32_t col_count, int32_t row_count, Vector_t offset);

WAFA void light_manager_draw(light_manager_t* const light_manager, render_manager_t* const render_mngr,wbool draw_direct,const char* parent_id,int32_t tile_size,int32_t col_count,int32_t row_count);

WAFA void light_manager_recycle(light_manager_t* const light_manager);

WAFA void light_manager_g_create(light_manager_t* const light_manager, render_manager_t* const render_mngr);

WAFA void light_manager_g_clean(light_manager_t* const light_manager, render_manager_t* const render_mngr);

WAFA void light_manager_free(light_manager_t* const light_manager,render_manager_t* const render_mngr);

#endif