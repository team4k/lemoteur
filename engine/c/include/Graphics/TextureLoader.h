#ifndef GRAPHICS_TEXTURELOADER

#define GRAPHICS_TEXTURELOADER 1

#include <inttypes.h>
#include <Base/types.h>
#include <Render.h>
#include <png.h>
#include <Base/geom.h>
#include <Render/render_manager.h>

typedef struct
{
    TEXTURE_ID texture_pointer;
    int16_t width;
    int16_t height;
	char parent_name[256];
	char texture_name[256];
	wbool virtual_texture;
	Rect_t* region;
	png_bytep tmpbuffer;
	png_imagep tmpimage;
	texture_filter filter;
	wbool deferredload;
} texture_info;


WAFA wbool load_texture(texture_info* texture,unsigned char* png_buffer,size_t buffer_size,const char* filePath,render_manager_t* const render_mngr, wbool backgrounload);

WAFA wbool load_texture_deferred(texture_info* texture, render_manager_t* const render_mngr);

WAFA wbool load_texture_withfilter(texture_info* texture, unsigned char* png_buffer, size_t buffer_size, const char* filePath, render_manager_t* const render_mngr, texture_filter filter, wbool backgrounload);

wbool load_raw_texture(texture_info* texture,unsigned char* textureData,unsigned long texturedata_size,render_manager_t* const render_mngr);
#endif

