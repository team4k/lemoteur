#ifndef __LIGHT__

#define __LIGHT__ 1

#include <inttypes.h>
#include <Base/types.h>
#include <Base/geom.h>
#include <Base/Color.h>
#include <Render/render_manager.h>

#define POOL_SHADOWS_VOLUMES 6
#define POOL_LIGHT_VOLUMES 20

typedef enum {light_topdown,light_jrpg} light_model;
typedef enum {light_omni,light_directional} light_type;

typedef struct
{
	Vector_t position;
	float radius;
	ColorA_t color;

	shadow_volume_t* shadows_volumes;
	int32_t num_shadow_volumes;
	int32_t max_shadow_volumes;

	triangle_t* light_volumes;
	int32_t num_light_volumes;
	int32_t max_light_volumes;

	wbool enabled;
	wbool visiblebyplayer;

	light_model gen_type;
	light_type l_type;

	char parent_id[64];

	char light_id[128];

	float* vertexBuffer;
	Vector_t baseratio;
	VBO_ID vertices[3];
	size_t num_volumes_in_buffer[3];
	FENCEID buffersync[3];

	wbool bufferupdated[3];

	wbool build_volumes;


	//for directional lights (in radian)
	float rotation_angle;
	float open_angle;

	//attenuation values
	float constant;
	float linear;
	float quadratic;

	//light power value (only used when rendering light on top of scene, for sprite light intensity, the alpha value of the light color is used)
	float power_factor;

} light_t;


WAFA void light_init(light_t* const light,Vector_t position,float radius,ColorA_t color,light_model gen_type,const char* light_id,const char* parent_id,light_type l_type);

//WAFA void light_genshadown_volumes(light_t* const light,edge_t** const edge_array,int32_t edge_array_count,const Rect_t screen_space);

WAFA void light_genlight_volumes(light_t* const light, render_manager_t* const render_mngr, edge_container_t* const edge_array, int32_t edge_array_count, int32_t tile_size, int32_t col_count, int32_t row_count, Vector_t ratio, Vector_t offset);

WAFA void light_setposition(light_t* const light,Vector_t new_pos);

WAFA void light_draw(light_t* const light,render_manager_t* const render_mngr,wbool mask_mode,Vector_t offset,Vector_t ratio,float ambient_alpha,int32_t width_render,int32_t height_render);

//set rotation angle in degree (converted in radian internally)
WAFA void light_setrotationangle(light_t* const light,float deg_angle);

//set open angle in degree (converted in radian internally)
WAFA void light_setopenangle(light_t* const light,float deg_angle);

//set light attenuation values (typically used in shader to compute light falloff)
WAFA void light_setattenuation(light_t* const light,float constant,float linear,float quadratic);

WAFA void light_g_create(light_t* const light);

WAFA void light_g_clean(light_t* const light);

WAFA void light_free(light_t* const light);

#endif
