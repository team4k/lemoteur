typedef struct
{
	texture_info* fill_texture;
	Vector_t direction_fill;
	float move_per_tick;
	float move_timeout;
	wbool active;
	float* vertices;
	float* texture_coords;
	float YpixelAdjust;
	float XpixelAdjust;
	int16_t piece_width;
	int16_t piece_height;
	int16_t piece_num_width;
	int16_t piece_num_height;

	float coordUnitX;
	float coordUnitY;

	int32_t tile_per_row;

	VBO_ID VBOvertices;
	VBO_ID VBOtexcoord;

	//animations variables
	hashtable_t anim_hash;
    animation_t* anim_array;
	animation_t* fill_anim;
	animation_t* edge_anim;

	int32_t current_fill_frame;
	int32_t current_edge_frame;

	int32_t edge_position;


	Vector_t base_position;

	int16_t z_index;

	wbool has_sibling;
	
} screen_fill_t;

#define EDGE_ON_TOP 1
#define EDGE_ON_BOTTOM 2
#define EDGE_ON_RIGHT 3
#define EDGE_ON_LEFT 4

WAFA void screen_fill_init(screen_fill_t* const screen_obj,float move_per_tick,float move_timeout,Vector_t direction_fill,texture_info* const fill_texture,int16_t piece_width,int16_t piece_height,render_manager_t* const render_mngr,const AnimationList* const animation_def,const char* fill_anim,const char* edge_anim);

void screen_fill_updatetxtcoord(screen_fill_t* const screen_obj,render_manager_t* const render_mngr);

WAFA void screen_fill_setanimations(screen_fill_t* const screen_obj,render_manager_t* const render_mngr,const char* fill_anim,const char* edge_anim,int32_t edge_pos);

WAFA void screen_fill_setgraphics(screen_fill_t* const screen_obj,render_manager_t* const render_mngr,texture_info* const new_texture,const AnimationList* const new_animations,const char* fill_anim,const char* edge_anim);

WAFA void screen_fill_draw(screen_fill_t* const screen_obj,render_manager_t* const render_mngr);

WAFA void screen_fill_update(screen_fill_t* const screen_obj,render_manager_t* const render_mngr,const float elapsed);

WAFA void screen_fill_g_clean(screen_fill_t* const screen_obj);

WAFA void screen_fill_g_create(screen_fill_t* const screen_obj,render_manager_t* const render_mngr);

WAFA void screen_fill_free(screen_fill_t* const screen_obj);