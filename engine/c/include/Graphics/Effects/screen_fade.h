typedef struct
{
	float current_alpha;
	float target_alpha;
	wbool active;
	wbool fading;

	//fade in / out variables
	float fade_amout_step;
	int fade_direction;

	Color_t fade_color;

} screen_fade_t;


WAFA void screen_fade_init(screen_fade_t* const scr_fade);
WAFA void screen_fade_draw(screen_fade_t* const scr_fade,render_manager_t* const render_mngr);
WAFA void screen_fade_setcolor(screen_fade_t* const scr_fade,Color_t new_color);
WAFA void screen_fade_dofade(screen_fade_t* const scr_fade,float duration,int direction,float target_alpha);
WAFA void screen_fade_resetfade(screen_fade_t* const scr_fade,wbool fadein);
WAFA void screen_fade_update(screen_fade_t* const scr_fade,const float elapsed);