#ifndef __PARTICLE_OBJECT_H__

#define __PARTICLE_OBJECT_H__ 1

typedef enum {PART_MOV_LINEAR,PART_MOV_SCRIPT} part_mov_type_t;

typedef struct
{
  float life_time;
  wbool updated;
  wbool alive;
  Vector_t position;
  Vector_t direction;
  int16_t width;
  int16_t height;
  float speed;
  part_mov_type_t particule_movement;
  void* ref_physics_body;
} particle_t;

typedef struct _particle_obj particle_object_t;

typedef void (*particle_mov_callback_t)(particle_object_t* const ref_object,particle_t* const particule_ptr);
typedef void (*particle_created_callback_t)(particle_object_t* const ref_object,particle_t* const particule_ptr);
typedef void (*particle_destroyed_callback_t)(particle_object_t* const ref_object,particle_t* const particule_ptr);

typedef struct
{
	int32_t rate; //number of particle emitted per seconds
	ColorA_t color; //color of the particles
	int16_t texture_frame;//for texcoord generation if any
	int16_t width_particle; //width of a particle
	int16_t height_particle; //height of a particle
	int16_t max_particle; //maximum number of particle visible at once
	int32_t total_particle; //total number of particle to emit, 0 if no limit
	float life_time;//life time of a particle in ms
	Vector_t direction;//direction of the particle emission {0,0} if the particle must be emitted in all directions equally
	Vector_t position;//position of the emitter
	float speed;
	int32_t num_alive_particles;

	float rate_prop;//time slice for a particle to appear

	float rate_track;
	int32_t total_particle_track;
	particle_t* particles;
} emitter_t;

struct _particle_obj
{
	emitter_t* emitter_array;
	int32_t num_emitter;
	size_t pool_size;

	SHADERPROGRAM_ID prog_id;
	texture_info* texture;
	VBO_ID vertices_vbo;

	float* all_vertices;
	size_t num_vertices;
	size_t spawned_particles;
	particle_mov_callback_t particle_mov_callback;
	particle_created_callback_t particle_created_callback;
	particle_destroyed_callback_t particle_destroyed_callback;

	void* ref_script;
	void* ref_physics;
	void* ref_entity;
	char ref_module[128];

};


#define PARTICLE_EMITTER_POOLSIZE 3


WAFA void particle_object_init(particle_object_t* const obj,render_manager_t* const render_manager,const char* prog_id,texture_info* texture,particle_mov_callback_t move_callback,particle_created_callback_t created_callback,particle_destroyed_callback_t destroyed_callback);

WAFA void particle_object_addemitter(particle_object_t* const obj,ParticleEmitter* emitter_data,render_manager_t* const render_manager,const Vector_t center_position);

WAFA void particle_object_genvbo(particle_object_t* const obj,render_manager_t* const render_manager);

WAFA void particle_object_draw(particle_object_t* const obj,render_manager_t* const render_manager,Vector_t offset);

WAFA void particle_object_update(particle_object_t* const obj, float delta_time);

WAFA void particle_object_recycle(particle_object_t* const obj);

WAFA void particle_object_g_create(particle_object_t* const obj,render_manager_t* const render_manager);

WAFA void particle_object_g_free(particle_object_t* const obj);

WAFA void particle_object_free(particle_object_t* const obj);

#endif