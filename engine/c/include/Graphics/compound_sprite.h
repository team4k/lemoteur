
#define COMPOUND_MAX_SHAPE 32
#define COMPOUND_MAX_VERTICES 16
#define COMPOUND_MAX_INDICES 32 

typedef struct
{
	void* sprite;
	int16_t shape_indx;
	int16_t shape_id;
	wbool gravity_inverted;

} csprite_pointer_t;

typedef struct
{
    int32_t width;
    int32_t height;
    int32_t texture_width;
    int32_t texture_height;
	texture_info* texture_pointer;
    float coordUnitX;
    float coordUnitY;
    int txtColCount;
	float rotation;
	int16_t shape_size[COMPOUND_MAX_SHAPE];
	int16_t shape_indices_size[COMPOUND_MAX_SHAPE];
	float  shape_rotation[COMPOUND_MAX_SHAPE];
	Vector_t shape_position[COMPOUND_MAX_SHAPE];
	float draw_vertices[COMPOUND_MAX_SHAPE * COMPOUND_MAX_VERTICES]; //base vertices
     float buffer_vertices[COMPOUND_MAX_SHAPE * COMPOUND_MAX_VERTICES]; //buffer vertices are updated each time position and / or rotation are updated
	float draw_texcoord[COMPOUND_MAX_SHAPE * COMPOUND_MAX_VERTICES]; //texture coordinates
	uint16_t draw_indices[COMPOUND_MAX_SHAPE * COMPOUND_MAX_INDICES];
	csprite_pointer_t shape_pointer[COMPOUND_MAX_SHAPE];
	int32_t shape_indx;
	int32_t indices_indx;
	int32_t indices_val;
	float shape_minx;
	float shape_miny;
	int32_t tile_index;
	float alpha_value;
	int32_t tile_size;

	VBO_ID hardware_buffer_vertices;
	VBO_ID hardware_buffer_texcoords;
	VBO_ID hardware_buffer_indices;

	wbool has_hwd_buffer_vertices;
	wbool has_hwd_buffer_texcoords;
	wbool has_hwd_buffer_indices;
    /* int32_t buffer_updateoffset;
     int32_t buffer_updatesize;*/
     wbool buffer_needupdate;

	int32_t z_index;
	uint32_t script_id;

} compound_sprite_t;



WAFA void compound_sprite_init(compound_sprite_t* const sprite,const int32_t sprite_width,const int32_t sprite_height, texture_info* const texture_inf,const int32_t tile_size,render_manager_t* const render_mngr);

WAFA csprite_pointer_t* compound_sprite_addshape(compound_sprite_t* const sprite,float** vertices,const int16_t size,Vector_t position,float rotation,wbool invert_gravity);

WAFA void compound_sprite_update_size(compound_sprite_t* const sprite,const int32_t sprite_width,const int32_t sprite_height,const int32_t tilesize);

WAFA void compound_sprite_updatetexture(compound_sprite_t* const sprite,texture_info* const new_tex);

WAFA void compound_sprite_compute_textcoord(compound_sprite_t* const sprite,float basex,float basey,int32_t tile_index,wbool gen_hardware_buffer,wbool mirror_x,wbool mirror_y,render_manager_t* const render_mngr);

WAFA void compound_sprite_updateshape(compound_sprite_t* const sprite,Vector_t position,float rotation, int16_t isprite);

WAFA void compound_sprite_clearshape(compound_sprite_t* const sprite);

WAFA void compound_sprite_draw(compound_sprite_t* const sprite,render_manager_t* const render_mngr);

WAFA void compound_sprite_create_VBO(compound_sprite_t* const sprite,render_manager_t* const render_mngr);

WAFA void compound_sprite_free(compound_sprite_t* sprite);

