#ifndef __TEXTURE_REGION__

#define __TEXTURE_REGION__ 1

typedef struct
{
	float u1;  // Top/Left U,V Coordinates
	float v1;
	float u2;// Bottom/Right U,V Coordinates
	float v2;
	//unsigned long char_code;
	//float aspect_ratio;

} TextureRegion_t;


WAFA void TextureRegion_Init(TextureRegion_t* texRegion,float texWidth,float textHeight, float x, float y, float width, float height);

void TextureRegion_Init2(TextureRegion_t* texRegion, float u1, float v1, float u2, float v2);

#endif