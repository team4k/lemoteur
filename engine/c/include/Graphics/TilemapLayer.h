#ifndef __TILEMAPLAYER_H__

#define __TILEMAPLAYER_H__ 1

typedef struct
{
	uint16_t tilesize;

	//size of base graph on texture
	uint16_t graph_width;
	uint16_t graph_height;

	//position of base graph on texture
	uint32_t posx;
	uint32_t posy;

	//chance of being picked up by the randomizer (0 = never, 100 = always)
	uint8_t random_gen_chance;

} tlayer_rand_config_t;


//store tile change info for animated tilemap
typedef struct 
{
	int index;//id in layer array of tile
	int id_tile; //id in tileset
	wbool draw;//is this update in screen space?

} tile_info_t;

typedef struct
{
	tile_info_t** tile_update;//array of tile update
	size_t num_update;
	float time_step;
} tilemap_frame_t;


typedef struct
{
	wbool visible;
	uint16_t rowCount;
	uint16_t colCount;
	int32_t draw_order;
	
	uint16_t tileWidth;
	uint16_t tileHeight;
	
	float* textureCoord;//array
	float* vertices;//array
	
	VBO_ID VBOvertices;
	VBO_ID VBOtexcoord;
	
	texture_info* textPointer;

	SHADERPROGRAM_ID program_id;
	char prog_name[128];
	ShadersParameters** shader_parameters;
	size_t num_parameters;

	float coordUnitX;
	float coordUnitY;
	int32_t txtColCount;

    VBO_ID buffIndex[1];

	int32_t* tileArray;
	size_t tileArraySize;
	size_t textureArraySize;
	size_t verticeArraySize;
    float XpixelAdjust;
    float YpixelAdjust;

	float parallax_x;
	float parallax_y;

	ColorA_t vertices_color;

	//destruction vars
	wbool destructible;
	int32_t destruct_tile_id;

	//for animated layer
	wbool animated;
	tilemap_frame_t** frames;
	size_t num_frames;
	uint32_t current_frame;
	float current_step_time;
	int32_t* tileFrameArray;

} TilemapLayer_t;

#define TILEDRAW_EXTRA_TILE 1
#define TILEUPDATE_EXTRA_TILE 4
#define TILEUPDATE_EXTRA_TILE_HALF TILEUPDATE_EXTRA_TILE / 2

WAFA void TilemapLayer_Init(TilemapLayer_t* tmap,int32_t rowCount,int32_t colCount,int32_t tileWidth, int32_t tileHeight,texture_info* const texturePointer,Layer* layer_data,render_manager_t* const rendermngr, tlayer_randomizer_t randomizer);

WAFA void TilemapLayer_Init_withback(TilemapLayer_t* tmap, int32_t rowCount, int32_t colCount, int32_t tileWidth, int32_t tileHeight, texture_info* const texturePointer, Layer* layer_data, render_manager_t* const rendermngr, tlayer_randomizer_t randomizer, wbool backgroundload);

WAFA void TilemapLayer_frontload(TilemapLayer_t* const tmap, render_manager_t* const rendermngr);

void TilemapLayer_genTileMap(TilemapLayer_t* tmap,const int32_t* tileArray, const size_t tileArraySize,wbool regen);

void TilemapLayer_regenTexCoord(TilemapLayer_t* tmap,render_manager_t* const rendermngr);

void TilemapLayer_resize(TilemapLayer_t* tmap,const int32_t rowCount,const int32_t colCount,const int16_t row_direction,const int16_t col_direction,render_manager_t* const rendermngr);
void TilemapLayer_update_tilesize(TilemapLayer_t* tmap,const int32_t tile_size,render_manager_t* const rendermngr);
void TilemapLayer_setanimated(TilemapLayer_t* const tmap,TilemapFrame** array_of_frames,size_t num_frames);
void TilemapLayer_clearanimated(TilemapLayer_t* const tmap);

WAFA void TilemapLayer_setcolor(TilemapLayer_t* const tmap, ColorA_t new_color);

WAFA void TilemapLayer_set_shader_program(TilemapLayer_t* const tmap,Layer* layer_data,render_manager_t* const rendermngr);
WAFA void TilemapLayer_addshaderparam(TilemapLayer_t* const tmap,const ShadersParameters* const new_param);

WAFA void TilemapLayer_fullupdateattributearray_shader(TilemapLayer_t* const tmap,const char* attribute_name,const float* const array_update);
WAFA void TilemapLayer_updateattributearray_shader(TilemapLayer_t* const tmap,const char* attribute_name,float value_update,int32_t value_index);
WAFA uint32_t TilemapLayer_getshaderparamindex(TilemapLayer_t* const tmap, const char* attribute_name);
WAFA void TilemapLayer_bitaddtoattributearray_shader(TilemapLayer_t* const tmap, uint32_t i_param, int32_t value_add, int32_t value_index);
WAFA void TilemapLayer_bitremovefromattributearray_shader(TilemapLayer_t* const tmap, uint32_t i_param, int32_t value_remove, int32_t value_index);
WAFA float TilemapLayer_getshaderattributarrayvalue(TilemapLayer_t* const tmap, uint32_t i_param, int32_t value_index);
WAFA void TilemapLayer_updateparambuffer(TilemapLayer_t* const tmap, uint32_t i_param);

WAFA void TilemapLayer_Draw(const TilemapLayer_t* tmap,render_manager_t* const render_mngr,Vector_t offset);

WAFA void TilemapLayer_editTile(TilemapLayer_t* tmap,int32_t tileId,int32_t tilePos,render_manager_t* const rendermngr,wbool edit_gpubuffer);
WAFA void TilemapLayer_Update(TilemapLayer_t* const tmap,const float deltaTime,render_manager_t* const render_mngr);

WAFA void TilemapLayer_clearframe(TilemapLayer_t* const tmap,render_manager_t* const render_mngr);
WAFA void TilemapLayer_gotoframe(TilemapLayer_t* const tmap,uint32_t frame,render_manager_t* const render_mngr);

WAFA void TilemapLayer_edit_tile_animated(TilemapLayer_t* const tmap,const int32_t tileId,const int32_t tilePos,const int32_t current_frame,render_manager_t* const render_mngr);
WAFA void TilemapLayer_remove_tile_animation(TilemapLayer_t* const tmap,const int32_t tile_pos,const int32_t current_frame,render_manager_t* const rendermngr);

WAFA void TilemapLayer_update_texcoord(TilemapLayer_t* const tmap,int32_t idtx,int32_t txtCol,int32_t txtRow,render_manager_t* const rendermngr,wbool update_gpu_buffer);
WAFA void TilemapLayer_synctexcoord(TilemapLayer_t* const tmap,render_manager_t* const rendermngr);

WAFA void TilemapLayer_setDestructible(TilemapLayer_t* const tmap,const wbool destructible, const int32_t tile_destruct);

WAFA void TilemapLayer_TextureChanged(TilemapLayer_t* tmap,render_manager_t* const render_mngr);

WAFA void TilemapLayer_Free(TilemapLayer_t* tmap);

#endif

