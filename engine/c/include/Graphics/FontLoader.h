#ifndef __FONT_LOADER__

#define __FONT_LOADER__ 1

#include <inttypes.h>
#include <Base/types.h>
#include <Font/Fontglyph.h>
#include <Graphics/TextureLoader.h>
#include <Graphics/TextureRegion.h>
#include <Font/Fontglyph.h>
#include <Render/render_manager.h>

typedef struct
{
    texture_info* texture;
    int16_t width;
    int16_t height;
	float fontAscent;
	float fontDescent;
	uint8_t distanceRange;
	uint8_t emSize;
	float lineHeight;

   int textureSize;                                   // Texture Size for Font (Square) [NOTE: Public for Testing Purposes Only!]
   TextureRegion_t textureRgn;                          // Full Texture Region

   Fontglyph_t* glyphs;// info about each characters

   char font_name[128];
   int32_t font_size;

   wbool antialias;

   unsigned long char_start;
   unsigned long char_end;
   uint32_t char_cnt;

} font_info;


#define FONT_PAD_X 2
#define FONT_PAD_Y 2
#define FONT_RESOLUTION 72

#define CHAR_SPACE 32 //Space character
#define CHAR_START 32 // First Character (ASCII Code)
#define CHAR_END 254  // Last Character (ASCII Code)
#define CHAR_CNT ( ( CHAR_END - CHAR_START ) + 1 )// Character Count (Including Character to use for Unknown)
#define FONT_SIZE_MIN 6 // Minumum Font Size (Pixels)
#define FONT_SIZE_MAX 180 // Maximum Font Size (Pixels)

wbool load_font(font_info* font,const char* font_name,unsigned char* font_data,long lSize,render_manager_t* const render_mngr);
wbool load_font2(font_info* font, const char* font_name, unsigned char* font_data, long lSize, render_manager_t* const render_mngr, unsigned long char_start, unsigned long char_end);
void free_font(render_manager_t* const render_mngr, font_info* font);

#endif