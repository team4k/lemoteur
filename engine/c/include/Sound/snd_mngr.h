#ifndef __SND_MNGR_H__


#define __SND_MNGR_H__ 1

#define AUDIO_CHANNELS_NUM 2
#ifdef EMSCRIPTEN
	#define AUDIO_SAMPLE_RATE 48000
#else
	#define AUDIO_SAMPLE_RATE 48000
#endif

#define MAX_BUFFER_SIZE 32768 //32k is our max buffer
#define AUDIO_FRAMES_PER_BUFFER 2048 //64

typedef struct
{
  #ifdef EMSCRIPTEN
	int16_t num_channels;
	int32_t audio_device;
	wbool fallback_to_44100;
  #else
	 PaStream *stream;
	 PaError error;
	 PaStreamParameters outputParameters;
  #endif
  sound_t snd_array[MAX_SOUND_SAMPLE];//array of sound sample to play
  hashtable_t snd_hash;
  wbool stream_opened;

  #ifdef EMSCRIPTEN
  float snd_buffer[MAX_BUFFER_SIZE];
  #else
  int16_t snd_buffer[MAX_BUFFER_SIZE];
  #endif

  int audio_buffersize;

  char* rm_array[MAX_SOUND_SAMPLE];//elements to remove
  char* add_array[MAX_SOUND_SAMPLE];//elements to add
  sound_t snd_add_array[MAX_SOUND_SAMPLE];

  volatile uint32_t add_counter;
  int rm_counter;

  volatile uint32_t mapid_can_read;
  char current_map_id[128];

  float music_volume;//value between [0.0, 1.0]
  float sfx_volume;//value between [0.0 1.0]

  Rect_t game_space_info;
  Vector_t center_point;

  volatile uint32_t do_update_centerpoint;

  Vector_t temp_centerpoint;

} snd_mngr_t;

WAFA void snd_mngr_init(snd_mngr_t* snd_mngr,int audio_buffersize,float suggestedLatency);

WAFA void snd_mngr_setvolume(snd_mngr_t* const snd_mngr,int16_t music_volume,int16_t sfx_volume);

WAFA void snd_mngr_playsound(snd_mngr_t* const snd_mngr,sound_sample_t* const snd_to_play,wbool loop,const char* sample_key,wbool remove_on_ended,wbool createonly,int16_t sound_type,Vector_t game_position,wbool positionning,float falloffdistance,const char* map_id);

WAFA void snd_mngr_setmapid(snd_mngr_t* const snd_mngr,const char* new_mapid);

WAFA void snd_mngr_update_position(snd_mngr_t* const snd_mngr,const char* sound_key,Vector_t new_pos);

WAFA void snd_mngr_update_falloff(snd_mngr_t* const snd_mngr,const char* sound_key,float newfalloff);

WAFA void snd_mngr_update_gamespace(snd_mngr_t* const snd_mngr,const Rect_t new_gamespace,const Vector_t center_point);

WAFA void snd_mngr_rewind_sound(snd_mngr_t* snd_mngr,const char* sample_key);

WAFA wbool snd_mngr_isplaying(snd_mngr_t* const snd_mngr,const char* sample_key);

WAFA void snd_mngr_stopsound(snd_mngr_t* const snd_mngr,const char* sample_key);

WAFA void snd_mngr_removesound(snd_mngr_t* const snd_mngr,const char* sample_key);

WAFA void snd_mngr_stopallsounds(snd_mngr_t* const snd_mngr,wbool only_with_positioning);

WAFA void snd_mngr_start(snd_mngr_t* snd_mngr);

WAFA void snd_mngr_stop(snd_mngr_t* snd_mngr);

WAFA wbool snd_mngr_has_sound(snd_mngr_t* const snd_mngr,const char* sample_key);

WAFA void snd_mngr_free(snd_mngr_t* snd_mngr);

#endif