#ifndef __SOUND_H__

#define __SOUND_H__ 1

typedef struct _internal_sound_sample sound_sample_t;

typedef struct
{
	sound_sample_t* sample;
	long long position;
	volatile uint32_t rewind;
	volatile uint32_t ended;
	volatile uint32_t force_ended;
	wbool loop;
	volatile uint32_t remove_on_ended;
	int16_t sound_type;
	Vector_t game_position;
	wbool positionning;
	char map_id[128];
	float falloffdistance;
} sound_t;

#define sound_size_t long long

#define MUSIC_TYPE 1
#define SFX_TYPE 2

sound_sample_t* alloc_sound_struct();
wbool sound_initfrombuffer(sound_sample_t* const ptr,unsigned char* buffer,size_t buffer_size);
wbool sound_initfromfile(sound_sample_t* const ptr,const char* path);
void sound_seek(sound_sample_t* const ptr,sound_size_t position);
sound_size_t sound_getframes_left(sound_sample_t* const ptr,sound_size_t position);
void sound_setlooppoint(sound_sample_t* const ptr,sound_size_t point);
sound_size_t sound_getlooppoint(sound_sample_t* const ptr);
sound_size_t sound_read_short(sound_sample_t* const ptr,int16_t* buffer,sound_size_t read_len);
sound_size_t sound_read_float(sound_sample_t* const ptr,float* buffer,sound_size_t read_len);
void sound_close(sound_sample_t* const ptr);
void free_sound_struct(sound_sample_t* ptr);

#endif // !__SOUND_H__
