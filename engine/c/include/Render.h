#ifndef __RENDER__

#define __RENDER__ 1

	#ifdef _WIN32
		#include <Windows.h>
	#endif

	#if defined(RENDERER_OPENGL) || defined(RENDERER_OPENGL_ES)

	#ifdef ANDROID_BUILD
		 #include <GLES/gl.h>
		 #include <GLES2/gl2.h>
		 #define GLdouble     GLfloat
		 #define GL_CLAMP     GL_CLAMP_TO_EDGE
		 #define glClearDepth glClearDepthf
		 #define glOrtho      glOrthof
		 #define glFrustum    glFrustumf
		 #define glGetDoublev glGetFloatv
     
		 //no 3D texture support under android / GLES
		 #define GL_TEXTURE_3D GL_TEXTURE_2D
		 #define GL_TEXTURE_WRAP_R GL_TEXTURE_WRAP_T
		 //#define GL_STREAM_DRAW GL_DYNAMIC_DRAW
		#include <EGL/egl.h>
	#else
		 #include <GL/glew.h>


		#ifdef USE_SDL_RENDER
			#include <SDL2/SDL.h>
			#include "SDL2/SDL_opengl.h"
		#endif

		#ifdef USE_GLFW_RENDER
			#include <GLFW/glfw3.h>
		#endif


		 #ifdef __APPLE__
			 #include <OpenGL/glu.h>
		 #else
			 #include <GL/glu.h>
		 #endif
    
	#endif

	#ifndef ISHADER

		typedef struct
		{
			GLuint shader_id;
			size_t num_sub_shader;
			GLuint* sub_shader_ids;
			char is_fragment_shader;
		} GLShader;

		#define ISHADER
	#endif

	#ifndef IPROGRAM

		typedef struct
		{
			GLuint prog_id;
			size_t num_sub_program;
			GLuint* sub_prog_ids;
			GLShader* vertex_shader;
			GLShader* pixel_shader;
			GLuint active_prog_id;
			char has_pass;
		} GLProgram;

		#define IPROGRAM
	#endif


	#define TEXTURE_ID GLuint*
	#define VBO_ID GLuint
	#define SHADER_ID GLShader*
	#define SHADERPROGRAM_ID GLProgram*
	#define VAO_ID GLuint
	#define FBO_ID GLuint*
	#define RBO_ID GLuint*
	#define SHADER_ATTRIB GLint
	#define FENCEID GLsync

	#elif defined(RENDERER_DX)

	#include <d3d11.h>

	#if defined(WINAPI_FAMILY) && (WINAPI_FAMILY == WINAPI_FAMILY_PHONE_APP)
		#include <DirectXMath.h>
		#include <DXGI1_2.h>
		#include <D3D11_1.h>
	#else
		#include <d3dx11.h>
		#include <d3dx10.h>
		#include <xnamath.h>
	#endif

	#ifndef ITEXTURE
		typedef struct
		{
			ID3D11Texture2D* texture;
			ID3D11ShaderResourceView* texturerv;
		} D3DTexture;
		#define ITEXTURE
	#endif


	#ifndef ISHADER

		typedef struct
		{
			size_t size_element;
			size_t offset_element;
		} element_def_t;

		typedef struct
		{
			void* buffer_element_def;
			char* buffer_content;
		} constant_buffer_t;

		typedef struct
		{
			ID3D11VertexShader* vertex_shader;
			ID3D11PixelShader* pixel_shader;
			ID3D11Buffer* constant_buffer;
			ID3D11InputLayout* input_layout;

			constant_buffer_t* const_buf_def;

			int is_fragment_shader;
		} D3DShader;
		#define ISHADER
	#endif

	#ifndef IPROGRAM
		typedef struct
		{
			D3DShader* vertex_shader;
			D3DShader* pixel_shader;
		} D3DProgram;
		#define IPROGRAM
	#endif

	#ifndef IFBO
		typedef struct
		{
			D3DTexture* m_renderTexture;
			ID3D11RenderTargetView* m_renderTargetView;
		} D3DFbo;
		#define IFBO
	#endif
	

	#define TEXTURE_ID D3DTexture*
	#define VBO_ID ID3D11Buffer*
	#define FBO_ID D3DFbo*
	#define RBO_ID ID3D11RenderTargetView*
	#define VAO_ID ID3D11Buffer*
	#define SHADER_ID D3DShader*
	#define SHADERPROGRAM_ID D3DProgram*



	#endif
#endif