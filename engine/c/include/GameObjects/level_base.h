#ifndef  __LEVEL_BASE_H__

#define __LEVEL_BASE_H__ 1

#ifdef _MSC_VER
extern "C" {
#endif

#include "lauxlib.h"
#include "lualib.h"
#include "lua.h"

#ifdef _MSC_VER
}
#endif

#include <inttypes.h>
#include <Base/types.h>
#include <Base/geom.h>
#include <Engine.h>

typedef enum { L_STATIC = 0, L_STREAM = 1} LEVEL_TYPE;

typedef void(*level_type_switch_t)(LEVEL_TYPE switchto);

typedef struct
{
	void* static_level_obj;
	void* stream_level_obj;
	LEVEL_TYPE current_level_type;
	LEVEL_TYPE pending_level_type;
	char pending_level[100];
	level_type_switch_t level_switch_callback;
} level_base_t;

WAFA void lvlbase_init(level_base_t* const lvl_base, void* const static_level_obj, void* const stream_level_obj, LEVEL_TYPE startup_type);

WAFA void lvlbase_setswitchcallback(level_base_t* const lvl_base, level_type_switch_t callback);

WAFA void lvlbase_requestswitchmode(level_base_t* const lvl_base, LEVEL_TYPE switch_to_type, const char* pending_level);

WAFA void lvlbase_doswitchmode(level_base_t* const lvl_base);

WAFA void lvlbase_requestexit(const void* const level_obj, LEVEL_TYPE type);

WAFA void* lvlbase_getcurrentlevelobj(level_base_t* const lvl_base);

WAFA void lvlbase_updatezorder(const void* const level_obj, LEVEL_TYPE type, const char* entity_key, int new_zorder, GAME_TYPE gtype);

WAFA uint32_t lvlbase_getcurrentscriptid(const void* const level_obj, LEVEL_TYPE type);

WAFA uint32_t lvlbase_getscriptid(level_base_t* const lvl_base, LEVEL_TYPE type);

WAFA int32_t lvlbase_getrowcount(const void* const level_obj,LEVEL_TYPE type, wbool global);

WAFA int32_t lvlbase_getcolcount(const void* const level_obj,LEVEL_TYPE type, wbool global);

WAFA int32_t lvlbase_gettilesize(const void* const level_obj, LEVEL_TYPE type);

WAFA wbool lvlbase_hasdestructiblelayer(const void* const level_obj,LEVEL_TYPE type,int32_t tilePos);

WAFA void* lvlbase_getdestructiblelayer(const void* const level_obj,LEVEL_TYPE type, int32_t tilePos);

WAFA int32_t* lvlbase_getcollisionsdata(const void* const level_obj, LEVEL_TYPE type);

WAFA void lvlbase_setcheckcolfunc(level_base_t* const lvl_base, check_col_t check_col_func, check_col_t check_col_func_editable);

WAFA void lvlbase_setrandomizer(level_base_t* const lvl_base, tlayer_randomizer_t randomizer);

WAFA void lvlbase_genedgelist(const void* const level_obj,LEVEL_TYPE type,  wbool regen);

WAFA void lvlbase_updateedge(const void* const level_obj, LEVEL_TYPE type, int32_t tilepos, wbool addtile, const int32_t* const collisions_array);

WAFA void* lvlbase_getlightmanager(const void* const level_obj, LEVEL_TYPE type);

WAFA int lvlbase_getcurrentstate(const void* const level_obj, LEVEL_TYPE type);

WAFA void lvlbase_setcurrentstate(const void* const level_obj,LEVEL_TYPE type,  int state);

WAFA wbool lvlbase_hasentity(const void* const level_obj,LEVEL_TYPE type,  const char* entity_name);

WAFA wbool lvlbase_hastrigger(const void* const level_obj,LEVEL_TYPE type,  const char* trigger_name);

WAFA wbool lvlbase_hastext(const void* const level_obj,LEVEL_TYPE type,  const char* text_name);

WAFA wbool lvlbase_haswaypoint(const void* const level_obj,LEVEL_TYPE type,  const char* waypoint_id);

WAFA wbool lvlbase_hascollider(const void* const level_obj,LEVEL_TYPE type,  const char* collider_id);

WAFA void* lvlbase_gettrigger(const void* const level_obj,LEVEL_TYPE type,  const char* trigger_name);

WAFA int lvlbase_getentitypos(const void* const level_obj,LEVEL_TYPE type,  const char* entity_name);

WAFA void* lvlbase_getentity_fromindex(const void* const level_obj,LEVEL_TYPE type,  int entity_index);

WAFA void* lvlbase_getentity(const void* const level_obj,LEVEL_TYPE type,  const char* entity_name);

WAFA void* lvlbase_gettext(const void* const level_obj,LEVEL_TYPE type,  const char* text_name);

WAFA void* lvlbase_getwaypoint(const void* const level_obj,LEVEL_TYPE type,  const char* waypoint_id);

WAFA void* lvlbase_getcollider(const void* const level_obj,LEVEL_TYPE type,  const char* collider_id);

WAFA void lvlbase_callbackonallentities(const void* const level_obj,LEVEL_TYPE type,  const char* callback);

WAFA void lvlbase_resetscriptinitonlyentities(const void* const level_obj,LEVEL_TYPE type,  lua_State* state);

WAFA void lvlbase_checktriggers(const void* const level_obj,LEVEL_TYPE type,  void* entity_box, const char* entity_id);

WAFA void lvlbase_movetostart(const void* const level_obj,LEVEL_TYPE type,  void* const phy_mngr);

WAFA const char* lvlbase_getcurrentmodule(const void* const level_obj, LEVEL_TYPE type);

WAFA void* lvlbase_gettilemap(const void* const level_obj,LEVEL_TYPE type, int32_t tilePos);

WAFA void* lvlbase_getcurrenttilemap(const void* const level_obj, LEVEL_TYPE type);

WAFA const char* lvlbase_getcurrentmapid(const void* const level_obj, LEVEL_TYPE type);

WAFA WorldRect_t lvlbase_getdimension(const void* const level_obj, LEVEL_TYPE type);

WAFA int32_t lvlbase_convert_worldpos_to_tilepos(const void* const level_obj,LEVEL_TYPE type, int32_t worldPos);

WAFA int32_t lvlbase_convert_tilepos_to_worldpos(const void* const level_obj,LEVEL_TYPE type, uint32_t tilepos, uint16_t ichunk);

WAFA uint32_t lvlbase_gettilefromvector(const void* const level_obj, LEVEL_TYPE type, Vector_t pos);

WAFA int16_t lvlbase_getmemorychunk_from_worldpos(const void* const level_obj,LEVEL_TYPE type, int32_t worldPos);

WAFA int32_t lvlbase_getmemoryshapeindex_from_tilepos(const void* const level_obj,LEVEL_TYPE type, int32_t tilepos,  uint16_t mem_chunk);

#endif