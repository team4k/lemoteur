typedef struct
{
	Rect_t map_dimension;
	Tilemap_t tilemap;
	wbool use_physics;
} map_t;