//state define
#define COMMON_RUN 1
#define COMMON_PAUSE 2
#define COMMON_NODRAW 4
#define COMMON_SWITCH 8
#define COMMON_NOPHYSICS 16

typedef enum {MENU_CENTER,MENU_TOPCENTER,MENU_TOPLEFT,MENU_TOPRIGHT,MENU_BOTTOMCENTER,MENU_BOTTOMLEFT,MENU_BOTTOMRIGHT,MENU_RIGHTCENTER,MENU_LEFTCENTER} menu_positions;


typedef struct 
{
	map_t maps[MAX_MAPS];
	hashtable_t map_hash;

	entity_t entities[MAX_ENTITIES];
	hashtable_t entities_hash;

	text_entity_t text_objects[MAX_TEXT_OBJ];
	hashtable_t text_hash;

	entity_group_t group_objects[MAX_GROUP];
	hashtable_t group_hash;

	hashmap_t particles_list;
	hashmap_t waypoints_list;
	hashmap_t colliders_list;


	int current_state;
	char current_map_id[256];
	char current_level[256];
	char pending_level[256];
	char current_module[256]; 

	Level* pending_binary;

	trigger_t triggers[MAX_TRIGGER];
	hashtable_t triggers_hash;

	map_t* current_map;
	wbool request_load_level;
	wbool request_load_binary;
	wbool request_exit;

	int16_t entity_display_list[MAX_ZINDEX][MAX_DISPLAY_VALUE];
	int16_t entitygroup_display_list[MAX_ZINDEX][MAX_DISPLAY_VALUE];
	int16_t text_display_list[MAX_ZINDEX][MAX_DISPLAY_VALUE];
	int16_t particles_display_list[MAX_ZINDEX][MAX_DISPLAY_VALUE];

	void* light_manager;
	void* sound_manager;
	void* pathfind_worker;

	physics_manager_t* slave_manager;

	float menu_zoom;
	wbool menu_use_zoom;

	check_col_t check_col_func;
	check_col_t check_col_func_editable;

	uint32_t script_id;

	//entity optional callback
	void(*ondraw_func)(entity_t* const currententity);
	void(*onupdate_func)(entity_t* const currententity);
	void(*ondelete_func)(entity_t* const currententity);

	//randomizer function
	tlayer_randomizer_t randomizer;

} Levelobject_t;

WAFA void Levelobject_Init(Levelobject_t* const level_obj,void* const p_light_manager,void* const p_sound_manager,void* const pathfind_worker);
WAFA void Levelobject_Free(Levelobject_t* level_obj);

//editor function for adding / removing maps
WAFA map_t* Levelobject_addmap(Levelobject_t* const level_obj,const int32_t tile_size,texture_info* const texturePointer,Map* oMap,Vector_t offset,wbool use_physics);

WAFA void Levelobject_changemapid(Levelobject_t* const level_obj,char* old_map_id,char* new_map_id);

WAFA void Levelobject_requestnewlevel(Levelobject_t* const level_obj,const char* new_level);

WAFA void levelobject_setmenuzoom(Levelobject_t* const level_obj,float menu_zoom,wbool menu_use_zoom);

WAFA void levelobject_setentitycallback(Levelobject_t* const level_obj, void(*ondraw_func)(entity_t* const currententity), void(*onupdate_func)(entity_t* const currententity), void(*ondelete_func)(entity_t* const currententity));

WAFA void Levelobject_switchmap(Levelobject_t* const level_obj, physics_manager_t* const phy_mngr, lua_State* const script_mngr, const char* new_map);

WAFA void Levelobject_requestexit(Levelobject_t* const level_obj);

WAFA void Levelobject_removemap(Levelobject_t* const level_obj,char* map_id);

//set the function used to generate tile on tilemap layer set as random_tiles = true
WAFA void Levelobject_setrandomizer_func(Levelobject_t* const level_obj, tlayer_randomizer_t randomizer);

WAFA entity_group_t* Levelobject_addentitygroup(Levelobject_t* const level_obj,GroupEntity* group_data,const char* map_id,int16_t zorder,render_manager_t* const render_manager);

WAFA entity_t* Levelobject_addentity(Levelobject_t* const level_obj,resources_manager_t* const resx_mngr, physics_manager_t* const phy_mngr,char* entity_id,Entity* entity_data,const char* map_id,render_manager_t* const render_manager,wbool editor_mode,Vector_t offset);

WAFA entity_t* Levelobject_addentity2(Levelobject_t* const level_obj,resources_manager_t* const resx_mngr, physics_manager_t* const phy_mngr,char* entity_id,Entity* entity_data,const char* map_id,render_manager_t* const render_manager,Vector_t offset,wbool editor_mode,wbool dynamic);

WAFA void Levelobject_updateentity(Levelobject_t* const level_obj,resources_manager_t* const resx_mngr, physics_manager_t* const phy_mngr,char* entity_id,Entity* entity_data,render_manager_t* const render_manager,wbool editor_mode);

WAFA void Levelobject_keepresources(Levelobject_t* const obj,resources_manager_t* const resx_mngr);

WAFA void Levelobject_updatesprite_texture(Levelobject_t* const level_obj,resources_manager_t* const resx_mngr,char* updated_texture);

WAFA void Levelobject_updatesprite_animation(Levelobject_t* const level_obj,resources_manager_t* const resx_mngr,char* updated_animation);

WAFA void Levelobject_update_pos(Levelobject_t* const level_obj,char* entity_id,int posX,int posY,GAME_TYPE type);

WAFA void Levelobject_set_visible(Levelobject_t* const level_obj,char* entity_id,wbool visible,GAME_TYPE type);

WAFA wbool Levelobject_updatezorder(Levelobject_t* const level_obj,const char* entity_id,int zorder,GAME_TYPE type);

WAFA void Levelobject_updaterenderboxes(Levelobject_t* const mngr,render_manager_t* const render_mngr);

WAFA void Levelobject_updaterenderbox(Levelobject_t* const mngr,render_manager_t* const render_mngr,const char* entity_id,GAME_TYPE type);

WAFA Rect_t* Levelobject_getrenderbox(Levelobject_t* const mngr,const char* entity_id,GAME_TYPE type);

WAFA void Levelobject_move_tostart(Levelobject_t* const level_obj, physics_manager_t* const phy_mngr);

WAFA char* Levelobject_removeentity(Levelobject_t* const level_obj,physics_manager_t* const phy_mngr,const char* entity_id,GAME_TYPE type);

WAFA char* Levelobject_changeentityid(Levelobject_t* const level_obj,char* old_entity_id,char* new_entity_id,GAME_TYPE type,Rect_t** out_rectangle,int32_t** out_zorder,void** out_entity);

WAFA particles_entity_t* Levelobject_addparticlesentity(Levelobject_t* const level_obj,ParticlesData* data,const char* entity_id,render_manager_t* const render_manager,lua_State* script_mngr, physics_manager_t* const phy_mngr,const char* map_id);

WAFA void Levelobject_updateparticlesentity(Levelobject_t* const level_obj,ParticlesData* data,const char* entity_id,render_manager_t* const render_manager,lua_State* script_mngr, physics_manager_t* const phy_mngr,const char* map_id);

WAFA void Levelobject_drawallasmenu(Levelobject_t* const obj,render_manager_t* const render_mngr,Vector_t offset,resources_manager_t* const resx_mngr,wbool check_visible,menu_positions position);

WAFA void Levelobject_Drawentitiesgroup(Levelobject_t* const mngr,render_manager_t* const render_mngr,int16_t z_index,Vector_t offset, wbool check_visible);

WAFA void Levelobject_Drawentities(Levelobject_t* const mngr,render_manager_t* const render_mngr,int16_t z_index,resources_manager_t* const resx_mngr,Vector_t offset,wbool check_visible);

WAFA void Levelobject_Drawtriggers(Levelobject_t* const mngr,render_manager_t* const render_mngr);

WAFA void Levelobject_Drawcolliders(Levelobject_t* const mngr, render_manager_t* const render_mngr);

WAFA void Levelobject_DrawText(Levelobject_t* const mngr,render_manager_t* const render_mngr,int16_t z_index,Vector_t offset,wbool check_visible);

WAFA void Levelobject_DrawParticles(Levelobject_t* const mngr,render_manager_t* const render_mngr,int16_t z_index,Vector_t offset,wbool check_visible);

WAFA void Levelobject_checktriggers(Levelobject_t* const mngr,const cpBB entity_box,const char* entity_id);

WAFA void Levelobject_update(Levelobject_t* const mngr,double elapsed,render_manager_t* const render_mngr,lua_State* script_mngr,wbool call_script);

WAFA void* levelobject_getfrompos(Levelobject_t* const mngr,float posX,float posY,GAME_TYPE selected_type);

WAFA trigger_t* Levelobject_addtrigger(Levelobject_t* const level_obj,char* trigger_id,Trigger* trigger_data,const char* map_id,trigger_callback_t trigger_func);

WAFA void Levelobject_updatetrigger(Levelobject_t* const level_obj,char* trigger_id,Trigger* trigger_data,trigger_callback_t trigger_func);

text_entity_t* levelobject_addtext(Levelobject_t* const level_obj,localization_t* const local_obj,char* text_id,TextObject* text_data,const char* map_id,resources_manager_t* const resx_mngr,render_manager_t* const render_manager);

text_entity_t* levelobject_addtext2(Levelobject_t* const level_obj,localization_t* const local_obj,char* text_id,TextObject* text_data,const char* map_id,resources_manager_t* const resx_mngr,render_manager_t* const render_manager,wbool dynamic);

void levelobject_updatetext(Levelobject_t* const level_obj,localization_t* const local_obj,char* text_id,TextObject* text_data,const char* map_id,resources_manager_t* const resx_mngr,render_manager_t* const render_mngr);

collider_t* levelobject_addcollider(Levelobject_t* const level_obj, Collider* collider_data, physics_manager_t* phy_mngr,const char* map_id);

void levelobject_updatecollider(Levelobject_t* const level_obj, const char* collider_id, Collider* collider_data, physics_manager_t* phy_mngr);

waypoint_t* levelobject_addwaypoint(Levelobject_t* const level_obj, Waypoint* waypoint_data,const char* map_id);

wbool levelobject_haswaypoint(Levelobject_t* const level_obj, const char* waypointid);

Vector_t levelobject_getwaypoint(Levelobject_t* const level_obj, const char* waypointid);

void levelobject_updatewaypoint(Levelobject_t* const level_obj, const char* waypoint_id, Waypoint* waypoint_data);



WAFA int Levelobject_load_level(Levelobject_t* const level_mngr,
							resources_manager_t* const resx_mngr,
							module_mngr_t* module_mngr,
							physics_manager_t* const phy_mngr,  
							lua_State* script_mngr,
							localization_t* const local_obj,
							render_manager_t* const render_mngr,
							const char* level_name,
							trigger_callback_t trigger_func);

WAFA int Levelobject_load_level_as_menu(Levelobject_t* const level_mngr,
							resources_manager_t* const resx_mngr,
							module_mngr_t* module_mngr,
							physics_manager_t* const phy_mngr,  
							lua_State* script_mngr,
							localization_t* const local_obj,
							render_manager_t* const render_mngr,
							Level* pb_level,
							trigger_callback_t trigger_func,
							Vector_t offset,
							const char* start_map);

WAFA int Levelobject_load_level2(Levelobject_t* const level_mngr,
							resources_manager_t* const resx_mngr,
							module_mngr_t* module_mngr,
							physics_manager_t* const phy_mngr,  
							lua_State* script_mngr,
							localization_t* const local_obj,
							render_manager_t* const render_mngr,
							Level* pb_level,
							trigger_callback_t trigger_func,
							Vector_t offset,
							const char* start_map);

WAFA int Levelobject_clean_level(Levelobject_t* const level_mngr,
							resources_manager_t* const resx_mngr,
							physics_manager_t* const phy_mngr,  
							lua_State* script_mngr);

//use to clear VBO
WAFA void Levelobject_g_clean_level(Levelobject_t* const level_mngr);
//use to recreate VBO
WAFA void Levelobject_g_create_level(Levelobject_t* const level_mngr,render_manager_t* const render_mngr);