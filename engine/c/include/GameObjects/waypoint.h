#ifndef __WAYPOINT_H__

#define __WAYPOINT_H__ 1

typedef struct
{
	Vector_t position;
	char ref_map_id[256];
} waypoint_t;

WAFA void waypoint_init(waypoint_t* const waypoint, Waypoint* data, const char* map_id);

WAFA Vector_t waypoint_getpos(waypoint_t* const waypoint);

WAFA void waypoint_setpos(waypoint_t* const waypoint, wfloat x, wfloat y);

#endif
