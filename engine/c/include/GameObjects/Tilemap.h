#ifndef __TILEMAP_H__

#define __TILEMAP_H__ 1

#include <inttypes.h>
#include <Base/types.h>
#include <Base/geom.h>
#include <Base/Color.h>
#include <Graphics/TilemapLayer.h>
#include <Graphics/TextureLoader.h>
#include <GameObjects/Level.pb-c.h>
#include <Render/render_manager.h>
#include <Graphics/LightManager.h>
#include <Resx/Resources.h>

#include <Render.h>

typedef struct
{
	Rect_t intersection;
	int16_t col_index;
	int16_t col_type;
	Vector_t contact_reflect;
}tilemapcolresponse_t;

#define MAX_LAYER 32

typedef struct
{
    TilemapLayer_t layer_array[MAX_LAYER];
	hashtable_t layer_hash;

    int32_t* collisions_array;
    int32_t tile_size;
    int32_t col_count;
    int32_t row_count;

	edge_container_t* edge_array;
	int32_t edge_array_size;

	edge_t* preallocated_edges;

    texture_info* texturePointer;
	
	char texture_name[256];

	int16_t max_zindex;

	TilemapLayer_t* layer_destructible;
	
	//for editor purpose
	wbool show_collisions;
	ColorA_t collision_color;
	ColorA_t destructible_color;
	ColorA_t slope_color;
	ColorA_t slope_inverse_color;
	ColorA_t editable_color;
	ColorA_t editable_color_2;
	ColorA_t editable_color_3;
	ColorA_t kill_color;

	float* editor_collisions_colors;
	VBO_ID buffer_collisions_colors;

}Tilemap_t;




//base function
WAFA void Tilemap_Init(Tilemap_t* const tilemap,const int32_t tile_size,texture_info* const texturePointer,const char* texture_name,const Map* const oMap);
WAFA void Tilemap_AddCollisions(Tilemap_t* const tilemap,Layer* layer_data);
WAFA void Tilemap_AddLayer(Tilemap_t* const tilemap,render_manager_t* const render_mngr,Layer* layer_data,texture_info* const texture_pointer, tlayer_randomizer_t randomizer);
WAFA void Tilemap_AddLayer_withback(Tilemap_t* const tilemap, render_manager_t* const render_mngr, Layer* layer_data, texture_info* const texture_pointer, tlayer_randomizer_t randomizer, wbool backgroundload);

WAFA void Tilemap_frontload_layer(Tilemap_t* const tilemap, render_manager_t* const render_mngr, const char* layer_id);

WAFA void Tilemap_RemoveLayer(Tilemap_t* const tilemap,char* layer_id);



//editor functions
WAFA void Tilemap_editCollision(Tilemap_t* const tmap,int32_t tileId,int32_t tilePos,render_manager_t* const render_mngr);
WAFA void Tilemap_editTile(Tilemap_t* const tmap,int32_t tileId,int32_t tilePos,const char* layer_id,render_manager_t* const render_mngr,wbool edit_gpubuffer);
WAFA void Tilemap_update_layer(Tilemap_t* const tilemap,Layer* layer_data,render_manager_t* const rendermngr);
WAFA void Tilemap_update_tilesize(Tilemap_t* const tilemap,const int32_t tile_size,render_manager_t* const rendermngr);
WAFA void Tilemap_edit_animated_tile(Tilemap_t* const tmap,int32_t tileId,int32_t tilePos,char* layer_id,int32_t current_frame,render_manager_t* const render_mngr);
WAFA void Tilemap_remove_animated_tile(Tilemap_t* const tmap,int32_t tilePos,char* layer_id,int32_t current_frame,render_manager_t* const render_mngr);
WAFA void Tilemap_editor_changecollisionsvisibility(Tilemap_t* const tmap,wbool visible);
WAFA void Tilemap_editor_changevisibility(Tilemap_t* const tmap,char* layer_id,wbool visible);
WAFA void Tilemap_TextureChanged(Tilemap_t* const tilemap,render_manager_t* const render_mngr);
WAFA wbool Tilemap_Draw(const Tilemap_t* const tilemap,int z_indx,render_manager_t* const render_mngr,Vector_t offset,light_manager_t* const light_manager,resources_manager_t* const resx_manager);
WAFA void Tilemap_update(Tilemap_t* const tilemap,const float deltaTime,render_manager_t* const render_mngr);

#ifndef STRIP_EDIT_CODE
	WAFA void Tilemap_editor_collisions_Draw(Tilemap_t* const tilemap, render_manager_t* const render_mngr, Vector_t offset);
	void Tilemap_editor_grid_draw(const Tilemap_t* const tilemap,const ColorA_t grid_color,render_manager_t* const render_mngr);
	void Tilemap_editor_changecollisions_color(Tilemap_t* const tilemap,float a,render_manager_t* const render_mngr);
	void Tilemap_editor_changecollisionscolor_forrange(Tilemap_t* const tilemap, ColorA_t color, int32_t start_value, int32_t end_value, render_manager_t* const render_mngr);
	void Tilemap_editCollision_Colored(Tilemap_t* const tmap, int32_t tileId, int32_t tilePos, ColorA_t selected_color, render_manager_t* const render_mngr);
#endif

WAFA char* Tilemap_changelayer_id(Tilemap_t* const tilemap, char* old_layer_id,char* new_layer_id);
WAFA void Tilemap_changelayer_order(Tilemap_t* const tilemap, char* layer_id,int32_t draw_order);
WAFA void Tilemap_resize(Tilemap_t* const tilemap, const int32_t col_count, const int32_t row_count, const int16_t row_direction, const int16_t col_direction, render_manager_t* const render_mngr, check_col_t check_col_func);
WAFA void Tilemap_regenTexCoord(Tilemap_t* const tilemap,char* layer_id,render_manager_t* const render_mngr);
WAFA void Tilemap_update_layercontent(Tilemap_t* const tilemap,Layer* layer_data,render_manager_t* const render_mngr);
WAFA void Tilemap_gen_edge_list(Tilemap_t* const tilemap,wbool regen,check_col_t check_col_func);
WAFA void Tilemap_update_edge(Tilemap_t* const tilemap, int32_t tilePos, wbool add_tile, const int32_t* const collisions_array);

WAFA void Tilemap_destroytile(Tilemap_t* const tilemap,int32_t tile_pos,render_manager_t* const render_mngr);
WAFA void Tilemap_setdestructiblelayer(Tilemap_t* const tilemap, char* layer_id,int32_t tile_destruct);
WAFA void Tilemap_cleardestructiblelayer(Tilemap_t* const tilemap);
WAFA void Tilemap_g_clean(Tilemap_t* const tilemap);
WAFA void Tilemap_g_create(Tilemap_t* const tilemap,render_manager_t* const render_mngr);
WAFA void tilemap_keepresources(Tilemap_t* const tilemap, resources_manager_t* const resx_mngr);



//cleanup functions
WAFA void Tilemap_Free(Tilemap_t* const tilemap);

#endif