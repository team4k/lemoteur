#ifndef __LEVEL_STREAM_DYNAMIC__

#define __LEVEL_STREAM_DYNAMIC__ 1

WAFA uint32_t level_stream_world_entities_save(level_stream_t* const level);

WAFA void level_stream_getworldentitydbpos(level_stream_t* const level, const char* id, int64_t* out_xpos, int64_t* out_ypos);

WAFA wbool level_stream_has_worldentitydb(level_stream_t* const level, const char* id);

WAFA void level_stream_load_world_entities_inrange(level_stream_t* const level, wbool simul);

WAFA void level_stream_save_world_entity(level_stream_t* const level, entity_t* const world_entity, Vector_t* const possave, unsigned char* databin, size_t databinlen, wbool simul);

WAFA void level_stream_unload_world_entities_outofrange(level_stream_t* const mngr);

WAFA wbool level_stream_simul_world_entity(level_stream_t* const level, time_t lastupdate);

#endif
