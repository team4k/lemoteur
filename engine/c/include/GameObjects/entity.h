#ifndef __ENTITY_H__

#define __ENTITY_H__ 1

#include <inttypes.h>
#include <Base/types.h>
#include <Base/geom.h>
#include <chipmunk/chipmunk.h>
#include <Graphics/TextureLoader.h>
#include <Graphics/Sprite.h>
#include <Graphics/Mesh.h>
#include <GameObjects/Level.pb-c.h>
#include <GameObjects/Characters.pb-c.h>
#include <Utils/Hash.h>
#include <Render/render_manager.h>
#include <Physics/Physics.h>


///game entity object
typedef struct
{
    wbool is_visible;
    sprite_t** sprite;
	uint8_t sprite_count;

	Mesh_t* mesh_obj;

	cpBody* body;
	cpShape** shape;
	int32_t shape_num;
	cpVect start_position;
	char ref_map_id[256];
	char* entity_id;
	int shape_type;
	cpBody* controlBody;
	cpConstraint* move_constraint;
	wbool move_constraint_activated;
	wbool has_script;
	float x;
	float y;
	wbool static_ent;

	int32_t offsetx;
	int32_t offsety;

	int z_order;
	Vector_t parallax;

	wbool constraint_x;
	wbool constraint_y;

	wbool in_physics_simulation;

	cpConstraint* link_constraint;
	wbool link_constraint_activated;
	char linked_to_id[256];

	cpConstraint* pin_constraint;

	const texture_info* current_texture;
	SHADERPROGRAM_ID program_id;
	char prog_name[128];
	ShadersParameters** shader_parameters;
	size_t num_parameters;
	char animation_file[128];


	//rotation variables
	cpVect rotation_point;
	float rotation_radius;
	float rotation_amount;
	float rotation_step;
	float start_rotation;
	wbool dorotation;
	wbool set_sprite_rotation;


	//callback variables
	void  (*callback_func)(const char* id,const char* func, uint32_t parent,const char* params,void* return_value,size_t return_size);
	hashtable_t* callback_hash;
	uint32_t entity_parent;
	char** callback_keys;
	char** callback_scriptfuncs;

    cpBitmask ori_layer;
	cpBitmask ori_mask;

	wbool in_group;
	wbool dynamic;
	wbool nodraw;

	Vector_t offset_glob;

	cpCollisionType last_touched_surface;

	cpVect freeze_pos;
	wbool sprite_freezed;

	wbool no_reset;

	cpShape** contact_shape_list;
	size_t contact_shape_num;

	RenderPass** render_pass;
	size_t num_pass;

	uint32_t current_pass;

	//used by level stream system
	char source[256];
	char source_id[256];
	char script_ctor[512];
	char script_module[128];

	char* lockedanim;
	uint8_t lockedspritepart;
	size_t lockedanimsize;

	//extra info used by game module to store game specific data
	uint32_t extraparam1;
	uint32_t extraparam2;
	intptr_t extraptr1;

} entity_t;


typedef struct
{
	int32_t contact_indx;
	entity_t* ref_entity;
} contact_point_ptr;

#define ENTITY_MAX_CALLBACK 20
#define ROTATE_STEP 0.05f

///init a new entity
WAFA void entity_init(entity_t* oentity, const Entity* const  entity_def,texture_info* const texture_inf,physics_manager_t* physicsWorld,const AnimationList* const animation_def,render_manager_t* const render_manager,wbool in_group);

///set the texture pointer of an entity
WAFA void entity_settexture(entity_t* const oentity,texture_info* new_texture);

///check if an entity is visible within the supplied bounds
WAFA wbool entity_checkrender(const entity_t* const oentity,const Rect_t screen_info);

///set an existing float shader param value
WAFA void entity_setshaderparam_floatvalue(entity_t* const oentity,const char* param_name,float param_values[4]);

///set an existing shader integer param value
WAFA void entity_setshaderparam_intvalue(entity_t* const oentity, const char* param_name, int32_t param_value);

///set an existing shader uniform array float param (memory allocation done on first call)
WAFA void entity_setshaderparam_uniformarrayfloat(entity_t* const oentity, const char* param_name, const float* const array_values, size_t array_size);

///draw an entity
WAFA void entity_draw(entity_t* oentity, render_manager_t* const render_mngr, const Vector_t* const offset, void(*ondraw_func)(entity_t* const currententity));

///update an entity animation, renderbox, rotation and contact bodies, take an optional callback function in parameter
WAFA void entity_update(entity_t* oentity, float elapsed, render_manager_t* const render_mngr, void(*onupdate_func)(entity_t* const currententity));

///freeze an entity in it current position, the entity won't move anymore unless you call unfreeze
WAFA void entity_freezesprite(entity_t* const avt);

///unfreeze an entity previously frozen with entity_freezesprite
WAFA void entity_unfreezesprite(entity_t* const avt); 

///set a custom collision shape for an entity, if set_sprite_verts, underlying sprite vertices will map the collision shape
WAFA void entity_setcustomshape(entity_t* const avt,Vector_t* verts,int count_verts,wbool set_sprite_verts,physics_manager_t* const physicsWorld,render_manager_t* const render_mngr);

///get the position of the entity
WAFA cpVect entity_getposition(const entity_t* const oentity);

///update the entity render box, if no phy update is set to true, don't use the physics object to update the box
WAFA void entity_updaterenderbox(entity_t* oentity,render_manager_t* const render_mngr,wbool no_phy_update);

///as long as the locked animation is playing on a specific sprite part, don't play any other animation
WAFA void entity_lockwhenanim(entity_t* const oentity, const char* anim, uint8_t sprite_part);

///reset the lock
WAFA void entity_resetanimlock(entity_t* const oentity);

///play a new animation
WAFA void entity_playanimation(entity_t* const avt, const char* anim, uint8_t sprite_part);

///play a new animation, and control if we reset flicker effect
WAFA void entity_playanimation2(entity_t* const oentity, const char* anim, uint8_t sprite_part, wbool reset_flicker);

///play a new animation, and control if we reset flicker effect and if we play the animation in reverse
void entity_playanimation3(entity_t* const oentity, const char* anim, uint8_t sprite_part, wbool reset_flicker, wbool reverse);

///stop the current playing animation 
WAFA void entity_stopanimation(entity_t* const avt, uint8_t sprite_part);

///get a sprite object
WAFA sprite_t* entity_getsprite(entity_t* const oentity, const uint8_t ispr);

///get the nomber of sprite object for this entity
WAFA uint8_t entity_getnumsprite(entity_t* const oentity);

///set an entity position, if the entity has a controller, it will set the controller position instead
WAFA void entity_setposition(entity_t* avt, wfloat posX, wfloat posY);

///set extra game parameters
WAFA void entity_setextraparams(entity_t* const oentity, uint32_t extrap1, uint32_t extrap2);

//get extra game parameters
WAFA void entity_getextraparams(const entity_t* const oentity, uint32_t* outextrap1, uint32_t* outextrap2);

WAFA void entity_setextraptr(entity_t* const oentity, intptr_t extraptr);

WAFA intptr_t entity_getextraptr(entity_t* const oentity);

///move to a new postion, if a controller exist, it velocity will be adjusted based on this position, otherwise it just call entity_setposition
WAFA void entity_movetopos(entity_t* oentity, wfloat posX, wfloat posY);

///set the anchor point of the controller movemement constraint
WAFA void entity_setanchorpoint(entity_t* oentity, wfloat X, wfloat Y);

///activate or deactivate the controller movement constraints
WAFA void entity_set_constraint(entity_t* const oentity,physics_manager_t* const physicsWorld,wbool activate);

///link this entity to another entity using a SlideJoint constraint 
WAFA void entity_linkto(entity_t* const oentity,entity_t* const link_to_entity,physics_manager_t* const physicsWorld,wbool activate,float mindistance,float maxdistance,Vector_t offset_anchr1,Vector_t offset_anchr2);

///pin this entity to a specific point relative to our world staticBody
WAFA cpConstraint* entity_pintoworld(entity_t* const oentity, physics_manager_t* const physicsWorld, cpVect offset_anchr1, cpVect offset_anchr2, wbool addtoworld);

///remove the existing pin constraint of this entity
WAFA void entity_removepin(entity_t* const oentity, physics_manager_t* const physicsWorld);

///check existence of a pin constraint
WAFA wbool entity_haspin(entity_t* const oentity);

///return pin constraint
WAFA cpConstraint* entity_getpin(entity_t* const oentity);

///add a callback to a script function to this entity
WAFA void entity_addcallback(entity_t* const oentity, const char* func_id,const char* script_func,void (*callback_func)(const char* id,const char* func, uint32_t parent,const char* params,void* return_value,size_t return_size),uint32_t parent);

///remove a script callback from this entity
WAFA void entity_removecallback(entity_t* const oentity,const char* func_id);

///remove all script callbacks from this entity
WAFA void entity_clearcallback(entity_t* const oentity);

///run a callback function
WAFA void entity_callback(entity_t* const oentity,const char* callback,const char* params);

///run a callback function with an extra return value parameter
WAFA void entity_callback2(entity_t* const oentity,const char* callback,const char* params,void* return_value,size_t return_size);

///remove an entity from the physics space, no collisions will be handled after this call
void entity_removefromspace(physics_manager_t* const physicsWorld,entity_t* const oentity);

///add an entity to the physics spacen, collisions will be automatically handled after this call
void entity_addtospace(physics_manager_t* const physicsWorld,entity_t* const oentity);

///start a rotational movement on an entity
void entity_startrotation(entity_t* const oentity,cpVect base_point,float rot_radius,int direction,float start_rotation,float speed);

///end the rotational movement on an entity
void entity_endrotation(entity_t* const oentity);

///get entity current rotation value (from physics body or sprite depending of the entity settings)
float entity_getrotation(entity_t* const oentity);

///set a specific rotation on an entity (won't work if a rotational movement is used at the same time)
void entity_setrotation(entity_t* const oentity,float rotation);

///reset an entity rotation to zero
void entity_resetrotation(entity_t* const oentity);

///add a new contact body, a contact body is a specific physics object that follow a contact point set in the underlying sprite animation, useful to check attacks hits in a fighting game for example.
WAFA int32_t entity_addcontactbody(entity_t* const oentity,int16_t width,int16_t height,const char* contact_name,physics_manager_t* const physicsWorld,cpCollisionType col_type,cpBitmask col_layer,cpBitmask mask);

///update contact bodies positions based on the underlying sprite animation data
void entity_updatecontactbodies(entity_t* const ent);

///remove all contact bodies on a sprite
WAFA void entity_removeallcontactbodies(entity_t* const oentity,physics_manager_t* const physicsWorld);


///get a pointer to an entity render box
WAFA Rect_t* entity_getrenderbox(entity_t* const oentity);

///return the rectangle encompassing all the physics objects of this entity
///return an empty rectangle if this entity has no shape or is out of the physics simulation
WAFA Rect_t entity_getphysicsbox(entity_t* const oentity);

///cleanup / free hardware graphics buffer of an entity
WAFA void entity_g_clean(entity_t* const oentity);

///create all the hardware graphics buffer of an entity
WAFA void entity_g_create(entity_t* const oentity,render_manager_t* const render_mngr);

///free the link constraint of an entity
WAFA void entity_freelinkconstraint(physics_manager_t* const physicsWorld,entity_t* oentity);

///free an entity and all it's underlying objects (sprite / mesh)
WAFA void entity_free(physics_manager_t* const physicsWorld,entity_t* avt);

#endif