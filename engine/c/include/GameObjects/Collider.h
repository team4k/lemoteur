#ifndef __COLLIDER_H__

#define __COLLIDER_H__ 1
typedef struct
{
	cpBody* body;
	cpShape* shape;
	char ref_map_id[256];
	Rect_t render_box;
	cpBB bbox;
	char* keyptr;
	wbool in_physics_simulation;
} collider_t;


WAFA void collider_init(collider_t* ocollider, const Collider* const  collider_def, physics_manager_t* physicsWorld,const char* map_id, char* const keyptr);

WAFA void collider_setpos(collider_t* ocollider, wfloat x, wfloat y);

WAFA Vector_t collider_getpos(collider_t* ocollider);

WAFA Rect_t collider_getbox(collider_t* ocollider);

WAFA void collider_setparameters(collider_t* ocollider, wfloat elasticity, wfloat friction);

WAFA void collider_updaterenderbox(collider_t* const ocollider, int16_t width, int16_t height);

WAFA void collider_draw(collider_t* const ocollider, render_manager_t* const render_mngr);

WAFA void collider_addtospace(collider_t* const ocollider, physics_manager_t* const physicsWorld);

WAFA void collider_removefromspace(collider_t* const ocollider, physics_manager_t* const physicsWorld);

WAFA void collider_free(collider_t* ocollider, physics_manager_t* const physicsWorld);

#endif