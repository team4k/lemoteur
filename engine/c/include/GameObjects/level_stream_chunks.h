#ifndef __LEVEL_STREAM_CHUNKS__

#define __LEVEL_STREAM_CHUNKS__ 1

WAFA Map* level_stream_load_chunkdb(level_stream_t* const level, int64_t world_row, int64_t world_column, char* out_guid, int64_t* out_posX, int64_t* out_posY);

WAFA wbool level_stream_getchunkdbpos(level_stream_t* const level, int64_t row_pos, int64_t col_pos, int64_t* out_xpos, int64_t* out_ypos);

WAFA Map* level_stream_gen_chunk(tempallocator_t* const gen_data_pool, int32_t row_count, int32_t col_count, const char* prog_name, uint32_t num_tiles, char* out_guid);

WAFA void level_stream_sync_chunkcontent(level_stream_t* const level, stream_chunk_t* const chunk, Map* const data);

WAFA void level_stream_backload_chunkgraph(level_stream_t* const level, stream_chunk_t* const chunk, const Map* const data, const int32_t chunk_pos);

WAFA script_container_t level_stream_frontload_chunkgraph(level_stream_t* const level, stream_chunk_t* const chunk, const Map* const data, const int32_t chunk_pos);

WAFA unsigned char* level_stream_save_chunk(level_stream_t* const level, stream_chunk_t* const chunk, const Map* const data, int64_t world_row, int64_t world_column);

WAFA void level_stream_unload_chunk(level_stream_t* const level, stream_chunk_t* const chunk);

WAFA stream_chunk_t* level_stream_get_center_chunk(level_stream_t* const  level);

//update chunk world position from a new center position
WAFA void level_stream_update_chunk_worldpos(level_stream_t* const level, Vector_t center_pos);

WAFA void level_stream_chunks_doswitch(level_stream_t* stream_level);

WAFA void level_stream_reposchunks(level_stream_t* const level, stream_chunk_t* const center_chunk);

WAFA void level_stream_checktriggers(level_stream_t* const level, const cpBB entity_box, const char* entity_id);

WAFA void level_stream_resettriggers(level_stream_t* const level);

WAFA trigger_t* level_stream_get_trigger_intersect(level_stream_t* const level, const cpBB entity_box, const char* entity_id);

#endif