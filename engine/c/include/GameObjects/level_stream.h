#ifndef __LEVEL_STREAM__

#define __LEVEL_STREAM__ 1

#ifdef _MSC_VER
extern "C" {
#endif
#include "lauxlib.h"
#include "lualib.h"
#include <lua.h>

#ifdef _MSC_VER
}
#endif

#if defined(_WIN32)
#include <windows.h>

#if defined(WINAPI_FAMILY) && (WINAPI_FAMILY == WINAPI_FAMILY_PHONE_APP)
#include <thread>
#endif

#else
#ifndef __USE_BSD
#define __USE_BSD
#endif
#include <unistd.h>
#include <dlfcn.h>
#include <pthread.h>
#endif

#include <inttypes.h>
#include <sqlite3.h>

#include <Render.h>

#include <math.h>
#include <Engine.h>
#include <string.h>
#include <stdlib.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>
#include <Base/geom.h>

#include <Debug/Logprint.h>
#include <Render/render_manager.h>

#include <Graphics/TextureLoader.h>

#include <Utils/Hash.h>
#include <Utils/hashmap.h>
#include <Utils/display_list.h>
#include <Utils/tempallocator.h>
#include <Utils/circular_array.h>

#include <GameObjects/Shaders.pb-c.h>
#include <Resx/shader_loader.h>
#include <GameObjects/Characters.pb-c.h>
#include <GameObjects/Level.pb-c.h>
#include <Graphics/Animation.h>
#include <Graphics/Mesh.h>
#include <Graphics/Sprite.h>
#include <Graphics/Light.h>
#include <Graphics/LightManager.h>

#include <Graphics/TextureRegion.h>
#include <Graphics/SpriteBatch.h>
#include <Graphics/FontLoader.h>

#include <Sound/Sound.h>

#include <Resx/Resources.h>
#include <Resx/localization.h>

#include <Debug/geomdebug.h>


#include <Render/render_shader_utils.h>

#include <Collisions/Collisions.h>
#include <Graphics/TilemapLayer.h>
#include <GameObjects/Tilemap.h>

#include <chipmunk/chipmunk.h>
#include <Physics/Physics.h>

#include <Physics/physics_tilemap_utils.h>
#include <Physics/physics_utils.h>

#include <Font/RenderText.h>
#include <GameObjects/entity.h>
#include <GameObjects/entity_group.h>
#include <GameObjects/text_entity.h>
#include <GameObjects/Trigger.h>
#include <GameObjects/Collider.h>
#include <GameObjects/waypoint.h>

#ifndef EMSCRIPTEN
#include <portaudio.h>
#else
#include <emscripten.h>
#endif

#include <Graphics/Effects/particle_object.h>
#include <GameObjects/particles_entity.h>

#include <Graphics/LightManager.h>

#include <Sound/snd_mngr.h>

#include <Threading/thread_func.h>

#include <Pathfinding/pathfind_worker.h>

#include <Scripting/ScriptEngine.h>
#include <Scripting/ScriptPointers.h>

#include <Threading/threads.h>

#include <Config/config.h>
#include <GameMngr/input_mngr.h>
#include <game_functions.h>
#include <GameMngr/module_mngr.h>

typedef enum {O_BOTTOMLEFT,O_BOTTOM,O_BOTTOMRIGHT,O_LEFT,O_CENTER,O_RIGHT,O_TOPLEFT,O_TOP,O_TOPRIGHT} STREAM_POS;
//usual chunk lifecycle : S_LOADING (start loading chunk from disk, load graph in background thread) -> S_SAVING (start saving chunk to disk) -> S_SAVED (finished saving chunk) -> S_FRONTLOADING (request loading on main thread for non thread safe data) -> S_GAMECHUNKBACKLOADING (request loading game specific data on background thread) -> S_GAMECHUNKFRONTLOADING (request loading remaining data on main thread) -> S_LOADED (chunk loaded and available for rendering) -> S_UNLOADING (request chunk unload on background thread, chunk no longer available for rendering) -> S_UNLOADED (chunk unloaded and free to be reused)
//possible chunk state -> S_OUTOFBOUND (the chunk is out of the world bound and can't be loaded, not necessarily an error, could happend in case we are on the border of the world) -> S_SCRIPTEXEC (the chunk data are loaded, the initialisation lua scripts are running, notify that the chunk entities can be safely requested)
typedef enum {S_EMPTY = 0,S_LOADED = 1,S_SAVING = 2,S_SAVED = 3,S_UNLOADING = 4,S_UNLOADED = 5,S_LOADING = 6,S_FRONTLOADING = 7,S_GAMECHUNKBACKLOADING = 8, S_GAMECHUNKFRONTLOADING = 9,S_OUTOFBOUND = 10,S_SCRIPTEXEC = 11,S_FRONTUNLOADING = 12} STREAM_STATE;

#define CHUNK_ROW 3
#define CHUNK_COL 3
#define CHUNK_NUMBER (CHUNK_ROW * CHUNK_COL)
#define TEMPALLOCATOR_SIZE 52428800 //50Mo
#define MAPALLOCATOR_SIZE TEMPALLOCATOR_SIZE
#define WORLDENTITYALLOCATOR_SIZE TEMPALLOCATOR_SIZE
#define MAX_TEMP_WORLD_ENTITIES 200
#define MAX_SQLITE_RETRY 200

typedef struct _level_stream_t level_stream_t;

typedef Map* (*gen_chunk_t)(tempallocator_t* const gen_data_pool,tempallocator_t* const world_data_pool,circular_array* const world_entities_array,int32_t row_count,int32_t col_count,const char* prog_name,uint32_t num_tiles,char* out_guid,int64_t world_row,int64_t world_col,void* user_data_gen_info);

typedef struct
{
	char id[255];
	char frag_file[255];
	unsigned char* binarydata;
	size_t binarydatalen;
	int64_t xpos;
	int64_t ypos;
	time_t lastupdate;

} simul_container_t;

typedef void(*simul_update_world_entity_t)(simul_container_t* const containers, size_t containerslen, uint64_t worldwidth, uint64_t worldheight, uint32_t tilesize);

typedef void(*new_chunk_loaded_t)();

typedef struct
{
	Entity** entity_script;
	int32_t entity_script_len;

	TextObject** text_script;
	int32_t text_script_len;

} script_container_t;

typedef struct
{
	Tilemap_t map;
	STREAM_POS pos;
	int32_t mem_pos;
	volatile uint32_t state;
	wbool visible;
	//origin is bottom left
	Rect_t bounds;
	wbool unload;
	wint world_col;
	wint world_row;
    
    //these data are limited to a chunk context, and are loaded / saved / unloaded with a chunk
    hashmap_t text;
    hashmap_t triggers;
    hashmap_t group_objects;
    hashmap_t particles_list;
    hashmap_t entities;
	hashmap_t colliders;
	hashmap_t waypoints;

	hashtable_t lights;

    physics_manager_t* phy_manager;
	
	tempallocator_t* content_pool;
	unsigned char* content_buffer;
	size_t content_buffer_size;

    char chunk_id[64];
	char map_id[256];
    
} stream_chunk_t;

struct _level_stream_t
{
	stream_chunk_t chunks[CHUNK_NUMBER];
	Vector_t center_pos;
	stream_chunk_t* access_chunk[CHUNK_NUMBER];
    int32_t tile_size;

	wint world_row_limit;
	wint world_col_limit;
    
    //these entities live with the stream map context, they are individually managed
    hashmap_t entities;
    
    
	light_manager_t* light_manager;
	void* sound_manager;
	void* pathfind_worker;
	resources_manager_t* resx_manager;
	void* module_manager;
    render_manager_t* render_mngr;
	physics_manager_t* phy_manager;
    trigger_callback_t trigger_func;
    localization_t* local_obj;
    lua_State* script_mngr;

    sqlite3* chunks_db;
	sqlite3* chunks_db_background;
    wbool chunks_db_ready;
	wbool pending_new_chunk_loaded;
	wbool first_load;

    tempallocator_t* gen_data_pool;

	//width / length and num tiles of a SINGLE chunk, multiply by the number of chunk to have the total size
    uint32_t base_row_count;
    uint32_t base_col_count;
    uint32_t base_num_tiles;
	uint32_t base_width;
	uint32_t base_height;

    thread_t level_thread;
	wbool level_thread_first_run;

    gen_chunk_t custom_chunk_gen_func;

	simul_update_world_entity_t simul_update;
	time_t simul_last_update;
	
	int current_state;
	char current_module[256]; 
	wbool request_exit;

	//collisions data
	int32_t* collisions_array;
	int32_t collisions_array_count;
	edge_container_t* edges_array;
	int32_t edges_array_size;
	edge_t* preallocated_edges;
	check_col_t check_col_func;
	check_col_t check_col_func_editable;

	//user data info passed to custom chunk generation func
	void* user_data_gen_info;

    //used to generate world entities
    volatile uint32_t world_entity_pool_state;

	tempallocator_t* world_entity_pool;

    circular_array temp_world_entities;

	uint32_t script_id;

	new_chunk_loaded_t chunk_loaded_callback;

	//entity optional callback
	void(*ondraw_func)(entity_t* const currententity);
	void(*onupdate_func)(entity_t* const currententity);
	void(*ondelete_func)(entity_t* const currententity);

	//randomizer function
	tlayer_randomizer_t randomizer;
};


WAFA void level_stream_init_script_pointer(level_stream_t* const level, lua_State* script_mngr);

WAFA void level_stream_init_script_after_switch(level_stream_t* const level, lua_State* script_mngr);

WAFA void level_stream_init(level_stream_t* const map, int32_t chunk_width, int32_t chunk_height, uint64_t world_spawn_pos, uint64_t world_row_limit, uint64_t world_col_limit, const int32_t tile_size, texture_info* const texturePointer, const char* texture_name, render_manager_t* const render_mngr, const char* prog_name, resources_manager_t* const resx_mngr, void* module_manager, trigger_callback_t trigger_func, localization_t* const local_obj, lua_State* script_mngr, physics_manager_t* const phy_manager, gen_chunk_t custom_chunk_gen_func, const char* script_module, void* pathfind_worker, void* user_data_gen_info, check_col_t check_col_func, check_col_t check_col_func_editable);

WAFA void level_stream_setlightmngr(level_stream_t* const  level, light_manager_t* const light_mngr);

WAFA void level_stream_setchunkloadedcallback(level_stream_t* const level, new_chunk_loaded_t callback);

WAFA void level_stream_setsimulupdatecallback(level_stream_t* const level, simul_update_world_entity_t callback);

WAFA void level_stream_setentitycallback(level_stream_t* const level, void(*ondraw_func)(entity_t* const currententity), void(*onupdate_func)(entity_t* const currententity), void(*ondelete_func)(entity_t* const currententity));

WAFA void level_stream_initchunksdb(level_stream_t* const  level, const char* base_chunk_db_path, const char* temp_chunk_db_path);

WAFA void level_stream_start_backgroundthread(level_stream_t* const level);

//set the function used to generate tile on tilemap layer set as random_tiles = true
WAFA void level_stream_setrandomizer_func(level_stream_t* const level, tlayer_randomizer_t randomizer);

WAFA entity_t* level_stream_add_entity(level_stream_t* const level_obj, stream_chunk_t* const chunk_obj, resources_manager_t* const resx_mngr, physics_manager_t* const phy_mngr, char* entity_id, Entity* entity_data, render_manager_t* const render_manager, wbool dynamic);

WAFA void level_stream_main_script_init(level_stream_t* const level, lua_State* script_mngr);

WAFA void level_stream_doswitch(level_stream_t* stream_level);

WAFA int level_stream_handlelock(sqlite3_stmt* const stmt);

//persist to database the entire content of the level stream
//usually called before exiting the game / or before any major scene change
WAFA void level_stream_persist(level_stream_t* const level, wbool resetchunkstate);

WAFA void level_stream_clean_state(level_stream_t* const level, Vector_t current_pos, double simelapsed, render_manager_t* const rnd_manager, lua_State* const script_mngr);

WAFA void level_stream_draw(level_stream_t* const  level, render_manager_t* const render_mngr, resources_manager_t* const resx_manager, int z_index);

WAFA void level_stream_map_updated_script(level_stream_t* const level, lua_State* script_mngr);

WAFA void level_stream_g_clean_level(level_stream_t* const level_mngr);

WAFA void level_stream_g_create_level(level_stream_t* const level_mngr, render_manager_t* const render_mngr);

WAFA void level_stream_update(level_stream_t* const  level, Vector_t current_pos, double elapsed, render_manager_t* const render_mngr, lua_State* script_mngr, wbool call_script);

WAFA void level_stream_keepresources(level_stream_t* const level, resources_manager_t* const resx_mngr);

WAFA void level_stream_setenablelights(level_stream_t* const level, wbool enable);

WAFA void level_stream_free(level_stream_t* level);

#endif
