#ifndef __ENTITY_GROUP_H__

#define __ENTITY_GROUP_H__ 1

typedef struct
{
	char group_id[128];
	char ref_map_id[128];
	sprite_t** linked_entities;
	SpriteBatch_t sprite_batch;
	int16_t num_entities;
	wbool sprite_batch_iscreated;
	int16_t zindex;
	Rect_t render_box;
	const texture_info* texture;

} entity_group_t;

#define GROUP_RENDER_UPDATE_OFFSET 25


WAFA void entity_group_init(entity_group_t* const entity_group,const char* group_id,size_t max_num_entities,int16_t zindex);

WAFA void entity_group_addentity(entity_group_t* const entity_group,sprite_t* const new_entity);

WAFA void entity_group_createspritebatch(entity_group_t* const entity_group,render_manager_t* const render_manager,const texture_info* texture);

WAFA wbool entity_group_checkrender(entity_group_t* const entity_group,const Rect_t screen_info);

WAFA void entity_group_draw(entity_group_t* const entity_group,render_manager_t* const render_mngr,wbool visibility_test,Vector_t offset);

WAFA void entity_group_free(entity_group_t* const entity_group);

#endif