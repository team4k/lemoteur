#ifndef __PARTICLES_ENTITY_H__

#define __PARTICLES_ENTITY_H__ 1

typedef struct
{
	cpBody* controller;
	cpConstraint* move_constraint;
	cpBody* body;
	cpShape* shape;
	wbool in_use;
	wbool constraint_particle; //forcibly set the particle position based on collision response
} particle_physic_object_t;

typedef struct
{
	char* entity_id;
	particle_object_t graph_obj;
	particle_physic_object_t* physics_obj_array;
	size_t physics_obj_num;
	wbool constraint_particle;
	char ref_map_id[256];
	int32_t z_order;
	wbool is_visible;

} particles_entity_t;


WAFA void particles_entity_init(particles_entity_t* const part_entity,ParticlesData* pb_data,render_manager_t* const render_manager,const char* prog_id,texture_info* texture,lua_State* script_ref,physics_manager_t* const phy_ref,const char* map_id);

WAFA wbool particles_entity_checkrender(particles_entity_t* const part_entity,Rect_t view_rect);

WAFA void particles_entity_draw(particles_entity_t* const part_entity,render_manager_t* const render_manager,Vector_t offset);

WAFA void particles_entity_setpos(particles_entity_t* const part_entity,Vector_t new_pos);

WAFA void particles_entity_update(particles_entity_t* const part_entity,float elapsed);

WAFA void particles_entity_g_clean(particles_entity_t* part_entity);

WAFA void particles_entity_g_create(particles_entity_t* part_entity, render_manager_t* const render_manager);

WAFA void particles_entity_free(particles_entity_t* part_entity,physics_manager_t* const phy_ref);

#endif