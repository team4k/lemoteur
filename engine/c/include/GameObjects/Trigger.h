#ifndef __TRIGGER_H__

#define __TRIGGER_H__ 1

typedef void(*trigger_callback_t)(const char* module, const char* func, const char* params, const char* entity_id, const char* trigger_id);

typedef struct
{
	Rect_t render_box;
	cpBB box;
	int32_t width;
	int32_t height;
	char callback_func[256];
	char params[256];
	wbool repeat;
	wbool activated;
	char activator[256];
	trigger_callback_t  trigger_func;
	char ref_map_id[256];
	char* trigger_id;
	wbool is_visible;
	Vector_t origpos;
} trigger_t;

WAFA void trigger_init(trigger_t* const otrigger,const Trigger* const trigger_data,trigger_callback_t trigger_func,Vector_t offset);
WAFA void trigger_update(trigger_t* const otrigger,const cpBB entity_bb,const char* entity_id,const char* module);
wbool trigger_checkbb(trigger_t* const otrigger, const cpBB entity_bb);
WAFA void trigger_draw(trigger_t* const otrigger,render_manager_t* const render_mngr);

#endif // !__TRIGGER_H__