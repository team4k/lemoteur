#ifndef __TEXT_ENTITY_H__

#define __TEXT_ENTITY_H__ 1

#include <Base/Color.h>
#include <Font/RenderText.h>
#include <Resx/localization.h>
#include <Render/render_manager.h>


typedef enum { font_left=1, font_center=2, font_right=3, font_none=4} font_alignement;

typedef struct
{
	Render_Text_t font_object;
	Rect_t bbox;
	Vector_t orig_pos;
	font_alignement align;
	wbool has_sprite;
	char sprite_corner[256];
	char sprite_background[256];
	char sprite_side[256];
	char ref_map_id[256];
	char* text_id;
	char* original_content;
	ColorA_t original_color;
	ColorA_t color;
	wbool has_script;
	int z_order;
	Vector_t parallax;
	wbool no_cull;
	wbool dynamic;

	uint16_t orig_width;
	uint16_t orig_height;

	int16_t diff_width;
	int16_t diff_height;
	uint8_t fontsize;

	wchar_t* text_content;

	//callback variables
	uint32_t entity_parent;
	void  (*callback_func)(const char* id,const char* func,uint32_t parent,const char* params);
	hashtable_t* callback_hash;
	char** callback_keys;
	char** callback_scriptfuncs;

	wbool is_visible;
	wbool no_translation;

	char script_ctor[512];

	uint32_t hidechance;
	int charreplacer;

}text_entity_t;

#define TEXT_MAX_CALLBACK 8

WAFA void text_entity_init(text_entity_t* const text_obj,localization_t* const local_obj,TextObject* text_data,const char* map_id,char* text_id,const font_info* font_data,const Rect_t alignement_box,render_manager_t* const render_mngr);

WAFA void text_entity_updaterenderbox(text_entity_t* const text_obj);

WAFA void text_entity_addcallback(text_entity_t* const oentity, const char* func_id,const char* script_func,void  (*callback_func)(const char* id,const char* func,uint32_t parent,const char* params), uint32_t parent);
void text_entity_removecallback(text_entity_t* const oentity,const char* func_id);
void text_entity_clearcallback(text_entity_t* const oentity);
WAFA void text_entity_callback(text_entity_t* const oentity,const char* callback,const char* params);

void text_entity_updatecontent(text_entity_t* const text_obj,localization_t* const local_obj,const char* new_content,const Rect_t alignement_box,const wbool updateoriginalcontent);
void text_entity_appendcontent(text_entity_t* const text_obj,localization_t* const local_obj,const char* new_content,const Rect_t alignement_box);
void text_entity_append_raw(text_entity_t* const text_obj,const char* raw_content,const Rect_t alignement_box);
void text_entity_resetcontent(text_entity_t* const text_obj,localization_t* const local_obj,const Rect_t alignement_box);

void text_entity_setposition(text_entity_t* const oentity,float x,float y);
Vector_t text_entity_getposition(text_entity_t* const oentity);

void text_entity_setrandomhide(text_entity_t* const oentity, uint32_t hidechance, int charreplacer);

wbool text_entity_checkrender(const text_entity_t* const oentity,const Rect_t screen_info);

void text_entity_draw(text_entity_t* const text_obj,render_manager_t* const render_mngr,Vector_t offset);

WAFA void text_entity_free(text_entity_t* text_obj);

#endif // !__TEXT_ENTITY_H__