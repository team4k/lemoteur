#ifndef __SCRIPTPOINTERS_H__

#define __SCRIPTPOINTERS_H__ 1

typedef enum { UNDEFINED = 0, SPRITE = 1, MESH = 2, LEVELOBJECT = 3, COMPOUND_SPRITE = 4, SHATTER_EFFECT = 5, CUSTOM_1 = 6 } script_type;

WAFA uint32_t script_registerpointer(intptr_t pointervalue, script_type type);
WAFA void script_unregisterpointer(uint32_t pointerid);
WAFA intptr_t script_getpointerfromid(uint32_t pointerid, script_type type);

#endif
