wbool luatable_hasvalue(lua_State* L,const char* key,int stack_pos);
float getluatable_value(lua_State* L,const char* key,int stack_pos);
const char* getluatable_stringvalue(lua_State* L,const char* key,int stack_pos);
int32_t getluatableint_value(lua_State* L,const char* key,int stack_pos);
wbool getluatablebool_value(lua_State* L,const char* key,int stack_pos);
size_t getluatable_nestedtable(lua_State* L,const char* key,int stack_pos);
void getlua_rawtable(lua_State* L,int stack_pos,int raw_pos);
void printlua_stack(lua_State* L);