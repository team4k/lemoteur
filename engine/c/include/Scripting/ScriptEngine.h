#ifndef __SCRIPTENGINE_H__

#define __SCRIPTENGINE_H__ 1

typedef struct
{
	unsigned char* binarydata;
	size_t binarydatalen;
} script_binary_return_t;

WAFA int script_loadfile(lua_State* L,const char* script_filename);

WAFA lua_State* script_createstate();
WAFA void script_createglobalmngr(lua_State* state);
WAFA void script_pushmngr(lua_State* state,const char* mngr_name,void* p_mngr);
WAFA void script_pushbooleanparam(lua_State* state, const char* param_name, wbool param_value);
WAFA const char* script_execstring(lua_State* state,const char* buff,void* return_value,size_t return_size);
WAFA void script_unwindstack(lua_State* state);
WAFA const char* script_execbuffer(lua_State* const state,const char* const buffer,size_t size);
WAFA void script_setcustompkgloader(lua_State* state, const lua_CFunction custompkgloader);
WAFA const char* script_callfunc_params(lua_State* L,const char* module,const char* func,const char* params_type,...);
WAFA void script_callfunc_binaryparam(lua_State* L, const char* module, const char* func, unsigned char* binary_data, size_t len_data, void* return_value, size_t return_size);
WAFA void script_genscriptinitonlyctor(const char* ctorstring, const char* id, uint32_t script_id, char* output, size_t outputlen);
WAFA void script_callgc(lua_State* state);
WAFA void script_destroystate(lua_State* L);

#endif