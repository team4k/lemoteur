#ifndef __SCRIPTUTILS_H__

#define __SCRIPTUTILS_H__ 1

#include <chipmunk/chipmunk.h>
#include "GameObjects/Level.pb-c.h"
#include <GameObjects/text_entity.h>
#include <GameObjects/waypoint.h>
#include <GameObjects/entity.h>
#include <GameObjects/trigger.h>
#include <GameObjects/Tilemap.h>
#include <GameObjects/Collider.h>

WAFA level_base_t* lua_getlvlbase(lua_State* L);
WAFA LEVEL_TYPE lua_lvlbase_gettype(lua_State* L);
WAFA void* lua_lvlbase_getmanager(lua_State* L, int stack_pos, LEVEL_TYPE* type);
WAFA text_entity_t* lua_lvlbase_get_text(lua_State* L,const char* entity_id,int stack_pos);
WAFA waypoint_t* lua_lvlbase_getwaypoint(lua_State* L, const char* waypoint_id, int stack_pos);
WAFA entity_t* lua_lvlbase_get_entity(lua_State* L,const char* entity_id,int stack_pos);
WAFA int lua_lvlbase_get_entitypos(lua_State* L, const char* entity_id, int stack_pos);
WAFA wbool lua_lvlbase_has_entity(lua_State* L, const char* entity_id, int stack_pos);
WAFA trigger_t* lua_lvlbase_get_trigger(lua_State* L,const char* trigger_id,int stack_pos);
WAFA Tilemap_t* lua_lvlbase_get_tilemap(lua_State* L, int stack_pos);
WAFA WorldRect_t lua_lvlbase_getdimension(lua_State* L, int stack_pos);
WAFA int32_t lua_lvlbase_gettilesize(lua_State* L, int stack_pos);
WAFA collider_t* lua_lvlbase_getcollider(lua_State* L, const char* collider_id, int stack_pos);

void no_gravity(cpBody *body, cpVect gravity, cpFloat damping, cpFloat dt);
WAFA void _protobuf_create_entity(lua_State* L,Entity* const entity_data);

#endif