#ifndef ASTAR_H_
#define ASTAR_H_

typedef struct coord {
	int32_t x;
	int32_t y;
} coord_t;



/* Run A* pathfinding over uniform-cost 2d grids using jump point search.

   grid: >=1 if obstructed, 0 if non-obstructed (the value is ignored beyond that).
   solLength: out-parameter, will contain the solution length
   boundX: width of the grid
   boundY: height of the grid
   start: index of the starting node in the grid
   end: index of the ending node in the grid
   enterable_value: value for an non obstructed element in the grid


   return value: Array of node indexes making up the solution, of length solLength, in reverse order
 */

#ifdef EMSCRIPTEN
	WAFA void worker_astar(char* data,int size);
#endif

pathfind_response_t astar_compute (const int32_t* grid,
			int32_t *solLength,
			int32_t boundX,
			int32_t boundY,
		    int32_t start, 
		    int32_t end,
			int32_t enterable_value,
			int32_t secondary_value,
			int32_t flags);

WAFA pathfind_response_t astar_unopt_compute (const int32_t* grid,
		    int32_t *solLength, 
		    int32_t boundX, 
		    int32_t boundY, 
		    int32_t start, 
		    int32_t end,
			int32_t enterable_value,
			int32_t secondary_value,
			int32_t flags);


/* Compute cell indexes from cell coordinates and the grid width */
int32_t astar_getIndexByWidth (int32_t width, int32_t x, int32_t y);

/* Compute coordinates from a cell index and the grid width */
void astar_getCoordByWidth (int32_t width, int32_t node, int32_t *x, int32_t *y);
#endif
