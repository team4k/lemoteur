#ifndef LOMBRIC_H_
#define LOMBRIC_H_


//flags to change the pathfind behavior
#define LOMBRIC_NO_STICK_CEIL 1 //no movement on ceiling possible
#define LOMBRIC_ONLY_LEFT 2 //only find on the left side of the start tile
#define LOMBRIC_ONLY_RIGHT 4 //only find on the right side of the start tile
#define LOMBRIC_MOVETHROUGHROCK 8 //can move through undestructible collisions from bottom to top
#define LOMBRIC_NOSAFEZONE 16 //can't move through safe zone

typedef struct coord_lomb {
	int x;
	int y;
} coord_lomb_t;


#ifdef EMSCRIPTEN
WAFA void worker_lombric(char* data, int size);
#endif

/**
* return the shortest path between to point by check at the same time path to the left side and path to the right side given these constraint
* we can't go up unless we are climbing a wall
* we can go down
* we can dig through walls that are diggable
**/
pathfind_response_t lombric_compute(const int32_t* grid,
	int32_t* solLength,
	int32_t boundX,
	int32_t boundY,
	int32_t start,
	int32_t end,
	int32_t enterable_value,
	int32_t diggable_value,
	int32_t flags);



#endif
