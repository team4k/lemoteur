#ifndef __PATHFIND_WORKER_H__

#define __PATHFIND_WORKER_H__ 1

#include <inttypes.h>
#include <Engine.h>
#include <Base/types.h>
#include <Utils/hashmap.h>
#include <Threading/thread_func.h>

#define PATHFIND_NOTSTARTED 888
#define PATHFIND_PENDING 0
#define PATHFIND_PROCESSED 1


#define WORKER_SAFE 1
#define WORKER_ADDINGELEMENTS 2
#define WORKER_ELEMENTS_ADDED 3
#define WORKER_UNSAFE 4

#define NO_SIGNAL 0
#define PATHFIND_GRIDCHANGE 1
#define PATHFIND_RERUN 2

#define DEFAULT_POOL_REQUEST 10

typedef enum { astar_jps, lombric } pathfind_algo;

typedef pathfind_response_t (*pathfind_compute_t)(const int32_t* grid,
	int *solLength,
	int boundX,
	int boundY,
	int start,
	int end,
	int enterable_value,
	int secondary_value,
	int flags
	);

typedef struct
{
	char result_id[128];
	int32_t cell_start;
	int32_t cell_goal;
	int32_t flags;
	int32_t* path_result;
	int32_t path_result_size;
	wbool path_computed;
	wbool result_send;
	int32_t flagsresult;
	wbool recycled;
	pathfind_algo algo;
} pathfind_result_t;


typedef struct
{
	#if defined(_WIN32)
		#if defined(WINAPI_FAMILY) && (WINAPI_FAMILY == WINAPI_FAMILY_PHONE_APP)
			std::thread* thread;
		#else
			HANDLE thread;
		#endif
	#elif defined(EMSCRIPTEN)
		char* worker_data;
		size_t grid_size;
		size_t worker_data_size;
		worker_handle thread;
		char custom_worker[256];
	#else
		pthread_t* thread;
	#endif			

	hashmap_t request_array;
	volatile uint32_t state;
	volatile uint32_t thread_state;
	volatile uint32_t outside_signal;
	pathfind_algo algo;

	
	const int32_t* grid;
	int32_t grid_width;
	int32_t grid_height;

	cond_wait_t thread_cond;

	wbool initialized;

} pathfind_worker_t;


WAFA void pathfind_init(pathfind_worker_t* const pathwork_obj,int32_t request_max,const int32_t* grid, int32_t grid_width, int32_t grid_height);
WAFA void pathfind_setalgo(pathfind_worker_t* const pathwork_obj, pathfind_algo algo);
WAFA void pathfind_startgridchange(pathfind_worker_t* const pathwork_obj);
WAFA void pathfind_endgridchange(pathfind_worker_t* const pathwork_obj);
WAFA pathfind_result_t* pathfind_dorequest(pathfind_worker_t* const pathwork_obj,const char* request_id, int32_t cell_start, int32_t cell_goal,wbool recycle_request, int32_t flags);
WAFA pathfind_result_t* pathfind_dorequest_withalgo(pathfind_worker_t* const pathwork_obj, const char* request_id, int32_t cell_start, int32_t cell_goal, wbool recycle_request, int32_t flags, pathfind_algo algo);

#ifdef EMSCRIPTEN
WAFA void pathfind_simupdate(pathfind_worker_t* const pathwork_obj);
#endif

WAFA void pathfind_updategrid(pathfind_worker_t* const pathwork_obj,const int32_t* grid, int32_t grid_width, int32_t grid_height);

WAFA void pathfind_freerequest(pathfind_worker_t* const pathwork_obj,const char* request_id);
WAFA void pathfind_freeallrequest(pathfind_worker_t* const pathwork_obj);
WAFA void pathfind_free(pathfind_worker_t* pathwork_obj);

#endif