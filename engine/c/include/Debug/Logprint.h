#ifndef __LOGPRINT__

#define __LOGPRINT__ 1
#define MAX_LOG_SIZE 1048576

WAFA void logprint_setdir(const char* dir);
WAFA int8_t logprint_c(float line,const char* filename,const char* function,const char* message,...);


#if defined(_WIN32)
        #define logprint(message,...) logprint_c((float)__LINE__,__FILE__,__func__,message, __VA_ARGS__)
#else
        #define logprint(message,...) logprint_c((float)__LINE__,__FILE__,__func__,message, ## __VA_ARGS__)
#endif

#endif

#if defined(DEBUG)
	#if defined(_WIN32)
	#define logprintdebug(message,...) logprint_c((float)__LINE__,__FILE__,__func__,message, __VA_ARGS__)
	#else
	#define logprintdebug(message,...) logprint_c((float)__LINE__,__FILE__,__func__,message, ## __VA_ARGS__)
	#endif
#else
	#define logprintdebug(message,...)
#endif
