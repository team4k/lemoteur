/* Copyright (c) 2007 Scott Lembcke
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __CHIPMUNK_DEBUG_DRAW__

#define __CHIPMUNK_DEBUG_DRAW__ 1


typedef struct Color {
	float r, g, b, a;
} Color;

static inline cpSpaceDebugColor RGBAColor(float r, float g, float b, float a) {
	cpSpaceDebugColor color = { r, g, b, a };
	return color;
}

static inline cpSpaceDebugColor LAColor(float l, float a) {
	cpSpaceDebugColor color = { l, l, l, a };
	return color;
}

WAFA void ChipmunkDebugSetDrawMatrix(cpTransform drawmatrix);

WAFA void ChipmunkDebugSetPointScale(float pointscale);


WAFA void ChipmunkDebugDrawInit(render_manager_t* const render_mngr);

WAFA void ChipmunkDebugDrawCircle(cpVect pos, cpFloat angle, cpFloat radius, cpSpaceDebugColor outlineColor, cpSpaceDebugColor fillColor);
WAFA void ChipmunkDebugDrawSegment(cpVect a, cpVect b, cpSpaceDebugColor color);
WAFA void ChipmunkDebugDrawFatSegment(cpVect a, cpVect b, cpFloat radius, cpSpaceDebugColor outlineColor, cpSpaceDebugColor fillColor);
WAFA void ChipmunkDebugDrawPolygon(int count, const cpVect *verts, cpFloat radius, cpSpaceDebugColor outlineColor, cpSpaceDebugColor fillColor);
WAFA void ChipmunkDebugDrawDot(cpFloat size, cpVect pos, cpSpaceDebugColor fillColor);
WAFA void ChipmunkDebugDrawBB(cpBB bb, cpSpaceDebugColor outlineColor);
WAFA cpSpaceDebugColor ColorForShape(cpShape *shape, cpDataPointer data);

WAFA cpSpaceDebugDrawOptions ChipmunkDebugDrawGetDefaultOptions();

// Call this at the beginning of the frame to clear out any ChipmunkDebugDraw*() commands.
WAFA void ChipmunkDebugDrawClearRenderer();
// Call this at the end of the frame to draw the ChipmunkDebugDraw*() commands to the screen.
WAFA void ChipmunkDebugDrawFlushRenderer(render_manager_t* const render_mngr);

WAFA void ChipmunkDebugDrawImpl(render_manager_t* const render_mngr, cpSpace* const space);
WAFA void ChipmunkDebugDrawImplWithOptions(render_manager_t* const render_mngr, cpSpace* const space, cpSpaceDebugDrawOptions drawOptions);

// Save the current contents of the renderer.
WAFA void ChipmunkDebugDrawPushRenderer();
// Reset the renderer back to it's last pushed state.
WAFA void ChipmunkDebugDrawPopRenderer();

#endif // !__CHIPMUNK_DEBUG_DRAW__
