#ifdef ALLOC_TRACER

void* malloc_tracer(float line,const char* filename,size_t bytes);
void free_tracer(float line,const char* filename,void* ptr);
void realloc_tracer(float line,const char* filename,size_t bytes,void* ptr);


#define wf_malloc(size) malloc_tracer((float)__LINE__,__FILE__,size)
#define wf_free(ptr) free_tracer((float)__LINE__,__FILE__,ptr)
#define wf_realloc(ptr,size) realloc_tracer((float)__LINE__,__FILE__,size,ptr)

#endif