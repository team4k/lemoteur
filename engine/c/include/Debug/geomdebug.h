#ifndef __GEOMDEBUG_H__

#define __GEOMDEBUG_H__ 1

WAFA void Debug_Drawedge(const edge_t* edge,ColorA_t color,render_manager_t* const render_mngr);
WAFA void Debug_Drawshadowvolume(const shadow_volume_t* shadow_volume,ColorA_t color,render_manager_t* const render_mngr);
WAFA void vector_tostring(const Vector_t* vector,char* string, size_t size_string);
WAFA void rect_tostring(const Rect_t* rect,char* string, size_t size_string);
WAFA void Debug_Drawpath(const Vector_t* const vector_array,size_t array_size,render_manager_t* const render_mngr,ColorA_t color);

#endif