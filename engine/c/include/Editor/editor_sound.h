
//sound functions
WAFA void editor_setsoundvolume(int16_t music_volume,int16_t sfx_volume);
WAFA void editor_addsound(char* sound_id,unsigned char* sound_data,size_t sound_data_size,char* map_id,int32_t zorder);
WAFA void editor_updatesound(char* sound_id,unsigned char* sound_data,size_t sound_data_size);
WAFA void editor_updatesoundposition(char* sound_id,float x,float y); 
WAFA void editor_removesound(char* sound_id);