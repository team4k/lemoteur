

WAFA void editor_setenable_lighting(wbool enable);
//light functions
WAFA void editor_addlight(char* light_id,unsigned char* light_data,size_t light_data_size,char* map_id,int32_t zorder);
WAFA void editor_updatelight(char* light_id,unsigned char* light_data,size_t light_data_size);
WAFA void editor_updatelightposition(char* light_id,float x,float y);
WAFA void editor_removelight(char* light_id);
WAFA void editor_setlight_progs(const char* light_prog,const char* shadow_prog);