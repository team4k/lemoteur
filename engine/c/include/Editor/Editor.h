typedef struct
{
	char app_path[WAFA_MAX_PATH];
	char resources_path[WAFA_MAX_PATH];

	//game objects
	physics_manager_t phy_mgr;
	resources_manager_t resx_mngr;

	//Level objects
	level_base_t editor_lvl_base;
	Levelobject_t editor_level;
	lua_State* lua_state;
	module_mngr_t* editor_module_mngr;
	snd_mngr_t snd_mngr;
	light_manager_t light_mngr;
	localization_t editor_localobj;
	save_mngr_t editor_savemngr;
	config_t config;
	render_manager_t render_mngr;
	screen_fill_t screen_filler;
	screen_fill_t screen_filler_sibling;
	screen_fade_t screen_fader;
	editor_selector_t selector;
	pathfind_worker_t path_worker;
	input_mngr_t input_mngr;

	wbool play_mode;
	wbool show_debug_col;
	//draw other items (colliders / triggers / light icons etc.) 
	wbool show_colliders_triggers_misc;


	wbool has_filler;
	wbool has_filler_sibling;
	wbool enable_lighting;

	//cursor objects
	double lastTime;
	ColorA_t cursor_color;
	ColorA_t select_color;
	Vector_t cursor_pos;
	Vector_t end_cursor_pos;
	int32_t tile_size;
	wbool show_cursor;
	wbool show_selector;

	ColorA_t back_color;

	ColorA_t grid_color;

	ColorA_t fps_color;

	wbool show_grid;

	//frame draw objects

	TIME_PROF currentTime;
	
	long overtime;
	int32_t count_frame;

	long max_nsecframe_time;

	long time_elapsed;


	wchar_t fps[260];
	Render_Text_t fps_text;

	Vector_t editor_position;


	wbool call_script;


	int32_t num_fps;

	long sleep_time;
	long frameRenderTime;
	TIME_PROF end_time;

	char lua_log_buffer[10000];

	char gdll_font[260];
	cpVect gdll_playerpos;


	Level* backup_level;

	sprite_t* sound_source_sprite;
	sprite_t* light_sprite;
	sprite_t* particle_sprite;

	sprite_t* waypoint_sprite;
	sprite_t* collider_sprite;


} editor_t;

  //************************//
 //global editor functions //
//************************//

const size_t pixel_bytes = 4;
const float MAX_FPS = 60.0f;
//collisions delta time
const float sample_rate = 3;
const double dt = 1.0f/MAX_FPS/sample_rate;
const double max_frame_time = 1000.0f / MAX_FPS;

//global editor functions
WAFA void editor_init();

void editor_render_setptr(editor_t* const ptr);
void editor_game_setptr(editor_t* const ptr);
void editor_resx_setptr(editor_t* const ptr);
void editor_entities_setptr(editor_t* const ptr);
void editor_light_setptr(editor_t* const ptr);
void editor_sound_setptr(editor_t* const ptr);

trigger_callback_t editor_entities_get_trigger_callback();

#if defined(_WIN32)
WAFA wbool editor_start_engine(HWND hwnd,int16_t width,int16_t height,int player_start_x,int player_start_y,int startX,int startY,const char* assets_path,const char* game_dll_path,const char* font_name,const char* light_prog,const char* shadow_prog,const char* mask_prog,const char* raw_assets_path);
#else
WAFA wbool editor_start_engine(int width,int height,int startX,int startY,const char* assets_path,const char* game_dll_path);
#endif

WAFA void editor_load_level(unsigned char* level_data, size_t level_size);
WAFA void editor_load_localedb();
WAFA void editor_release_localedb();

WAFA void editor_shutdown_engine();
WAFA void editor_change_mode(wbool pmode);


WAFA void editor_send_inputmessage(uint32_t msg,uint32_t wparam,long lparam);


//scripting functions
WAFA void editor_exec_string(const char* buffer,char* return_value,size_t return_size); 
WAFA void editor_set_module(const char* buffer);
WAFA void editor_set_call_script(wbool pcall_script);
WAFA void editor_get_lua_log(char* return_value, size_t return_size);
WAFA void editor_reloadconfig();
WAFA void editor_reloadshaderconfig();

