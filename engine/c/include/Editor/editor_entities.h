
//edit tilemap functions

WAFA void editor_create_tilemap(int32_t tileWidth,int32_t tileHeight,unsigned char* map_data,size_t map_size);
WAFA void editor_Tilemap_TextureChanged();
WAFA void editor_clean_tilemap(Tilemap_t* tilemap_ptr);
WAFA void editor_Tilemap_resize(int32_t rowCount,int32_t colCount,int16_t row_direction,int16_t col_direction);
WAFA void editor_Tilemap_changeopacity(const char* layer_id,float a);
WAFA void editor_tilemaps_updatetilesize(int32_t new_tile_size);

//edit tiles functions
WAFA void editor_change_tile(int32_t tileId,int32_t tilePos,char* layer_id);
WAFA void editor_change_multi_tile(int32_t* tiles_id,int32_t start_pos,char* layer_id);
WAFA void editor_change_multi_tile2(int32_t* tiles_id,int32_t start_pos,int32_t* rect_values,char* layer_id);
WAFA void editor_update_tilemap(int32_t* arrayFeed,char* layer_id);
WAFA void editor_change_animated_tile(int32_t tileId,int32_t tilePos,char* layer_id,int32_t current_frame);
WAFA void editor_remove_animated_tile(int32_t tilePos,char* layer_id,int32_t current_frame);
WAFA void editor_goto_frame(int32_t frame,char* layer_id);


//edit layer functions
WAFA void editor_changelayer_visibility(wbool visible,char* layer_id);
WAFA void editor_changelayer_id(char* old_layer,char* new_layer);
WAFA void editor_addlayer(unsigned char* layer_data,size_t layer_size);
WAFA void editor_update_layer(unsigned char* layer_data,size_t layer_size);
WAFA void editor_update_layercontent(unsigned char* layer_data,size_t layer_size);
WAFA void editor_changelayer_order(char* layer_id,int32_t new_draw_order);
WAFA void editor_removelayer(char* layer_id);
WAFA void editor_setdestructible(char* layer_id,int32_t tile_destruct_id);
WAFA void editor_cleardestructible();
WAFA void editor_regen_textcoord(char* layer_id);

//edit entities functions
WAFA void editor_addentity(char* entity_id,unsigned char* entity_data,size_t entity_size,char* map_id);
WAFA void editor_updateentity(char* entity_id,unsigned char* entity_data,size_t entity_size);
WAFA void editor_removeentity(char* entity_id,GAME_TYPE type);
WAFA void editor_changeentityid(char* old_id,char* new_id,GAME_TYPE type);
WAFA void editor_selectentity(char* select_name,GAME_TYPE type,wbool add_to_selection);
WAFA void editor_selectentity_pos(int posX,int posY,int16_t z_order,wbool add_to_selection);
WAFA void editor_entity_unselect();
WAFA wbool editor_has_entity_selected();
WAFA int32_t editor_get_num_selection();
WAFA GAME_TYPE editor_entity_get_selection(char* buff,int32_t num_select,size_t buff_size);
WAFA void editor_entity_update_pos(char* entity_id,int posX,int posY,GAME_TYPE type);
WAFA void editor_entity_set_visible(char* entity_id,wbool visible,GAME_TYPE type);
WAFA void editor_addtrigger(char* entity_id,unsigned char* entity_data,size_t entity_size,char* map_id,int16_t z_order);
WAFA void editor_updatetrigger(char* entity_id,unsigned char* entity_data,size_t entity_size);
WAFA void editor_addtextentity(char* entity_id,unsigned char* entity_data,size_t entity_size,char* map_id);
WAFA void editor_updatetextentity(char* entity_id,unsigned char* entitydata,size_t entity_size,char* map_id);
WAFA void editor_entity_changezorder(char* entity_id, int new_zorder,GAME_TYPE type);
WAFA void editor_entity_getrenderbox(char* entity_id,GAME_TYPE type,int32_t* render_box_size,float* render_box_pos);

//particles functions
WAFA void editor_addparticles(char* particles_id, unsigned char* particles_data, size_t particles_data_size, char* map_id, int32_t zorder);
WAFA void editor_updateparticles(char* particles_id, unsigned char* particles_data, size_t particles_data_size, char* map_id);
WAFA void editor_updateparticlesposition(char* particles_id, float x, float y);
WAFA void editor_removeparticles(char* particles_id);

//waypoints functions
WAFA void editor_addwaypoint(char* waypoint_id, unsigned char* waypoint_data, size_t waypoint_data_size, char* map_id, int32_t zorder);
WAFA void editor_updatewaypointposition(char* waypoint_id, float x, float y);
WAFA void editor_removewaypoint(char* waypoint_id);

//colliders functions
WAFA void editor_addcollider(char* collider_id, unsigned char* collider_data, size_t collider_data_size, char* map_id, int32_t zorder);
WAFA void editor_updatecollider(char* collider_id, unsigned char* collider_data, size_t collider_data_size, char* map_id);
WAFA void editor_updatecolliderposition(char* collider_id, float x, float y);
WAFA void editor_updatecollidersize(char* collider_id, int16_t width, int16_t height);
WAFA void editor_removecollider(char* collider_id);

//get tilemap content
WAFA void editor_get_tilearray(int32_t* arrayCopy,char* layer_id,char* map_id);
WAFA void editor_get_tile(int32_t* tileid, int32_t tilepos,char* layer_id);
WAFA int editor_get_num_multi_tiles();
WAFA void editor_get_multi_tiles(int32_t* tileids,char* layer_id);