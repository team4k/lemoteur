
WAFA wbool editor_load_gamedll(const char* game_dll_path);
WAFA void editor_release_gamedll();
WAFA void editor_focus_onplayer();
WAFA void editor_setplayer_pos(float x,float y);
WAFA void editor_clean_level();
WAFA void editor_level_loaded();
WAFA void editor_update_level_content(unsigned char* level_content,size_t level_size);
WAFA uint32_t editor_get_script_id();

//level functions
WAFA void editor_setlevel_name(char* level_name);

//map functions
WAFA void editor_switchmap(char* map_id);
WAFA void editor_changemap_id(char* old_map,char* new_map);

//play functions
WAFA void editor_play_move(float mX,float mY);
WAFA void editor_game_input(int16_t input_event,wbool pressed);
WAFA void editor_game_char(uint32_t codepoint);
WAFA void editor_click(float targetX,float targetY,int button_id);
WAFA void editor_endclick(float targetX,float targetY,int button_id);
WAFA void editor_wheelpinch(float delta);
WAFA void editor_get_playerposition(int32_t* array_pos);
WAFA wbool editor_has_player();
WAFA wbool editor_hadlevelpending();
WAFA wbool editor_levelpendingisbinary();
WAFA void editor_getlevelpending(char* out_level_name);
WAFA size_t editor_getlevelpending_size();
WAFA void editor_getlevelpendingbinary(unsigned char* out_level_data);
WAFA void editor_clearlevelpending();

//collisions functions
WAFA void editor_getcollisions_array(int32_t* arrayCopy,char* map_id);
WAFA void editor_getcollision(int32_t* tileid, int32_t tilepos);
WAFA void editor_update_collisions_data(int32_t* arrayFeed);
WAFA void editor_update_collisions();
WAFA void editor_removecollisions();
WAFA void editor_change_collision(int32_t tileid, int32_t tilepos);
WAFA void editor_changecollisions_color(float a);
WAFA void editor_changecollisions_color_forvalue(int32_t value, float r, float g, float b, float a);
WAFA void editor_changecollisions_color_forrange(int32_t start_value, int32_t end_value, float r, float g, float b, float a);
WAFA void editor_changecollisionsvisibility(wbool visible);
WAFA void editor_change_collision_color(int32_t tileid, int32_t tilepos, float r, float g, float b, float a);

