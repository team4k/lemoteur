
//add resources functions
WAFA wbool editor_resx_exist(char* resx_name);
WAFA void editor_addtextureresx(char* texture_name,int16_t textureWidth,int16_t textureHeight,unsigned char* image_data,unsigned long data_size);
WAFA void editor_addanimresx(char* anim_name,unsigned char* anim_data,size_t anim_size);
WAFA void editor_updatetextureresx(char* texture_name,int16_t textureWidth,int16_t textureHeight,unsigned char* image_data,unsigned long data_size);
WAFA void editor_updateanimresx(char* anim_name,unsigned char* anim_data,size_t anim_size);
WAFA void editor_updatespriteanimations(char* anim_name);
WAFA void editor_updatespritetextures(char* texture_name);
WAFA void editor_setpacker(unsigned char* packer_data,size_t packer_size);
WAFA void editor_addsoundresx(char* sound_name,const char* sound_path);
WAFA void editor_updatesoundresx(const char* sound_name);
WAFA void editor_addshaderresx(char* shader_name,const char* shader_path);
WAFA void editor_updateshaderresx(const char* shader_name);