Rect_t get_select_rectangle();

WAFA void editor_get_select_rectangle(int32_t* rect_values);
WAFA void editor_resize_screen(int16_t width,int16_t height);
WAFA void editor_set_tilesize(int32_t tile_size);
WAFA void editor_draw_frame();
WAFA void editor_setzoom(int width,int height,float zoomfactor);
WAFA float editor_getscrollX();
WAFA float editor_getscrollY();
WAFA void editor_draw_debug_collisions(wbool show);
WAFA void editor_draw_colliders_triggers_misc(wbool show);
WAFA void editor_scroll(float mx,float my);
WAFA void editor_set_scroll(float x,float y);
WAFA void editor_change_color(float r,float g,float b,float a);
WAFA void editor_grid_visibility(wbool show,float r,float g,float b,float a);
WAFA void editor_set_selectioncolor(float r,float g,float b,float a);
WAFA void editor_set_ambientcolor(float r,float g,float b,float a);

//cursor functions
WAFA void editor_setcursor_pos(int x,int y,wbool endcursor_only,wbool move_multi_selection);
WAFA void editor_settargetpos(int x, int y);
WAFA void editor_setcursor_color(float r,float g,float b,float a);
WAFA void editor_showcursor(wbool show_cur,wbool show_sel);
WAFA void editor_setfps_color(float r,float g,float b,float a);


