typedef struct
{
	char* entity;
	GAME_TYPE type;
	Rect_t* bounds;
	void* object;
	char map_id[128];
	int32_t* zorder;
} item_t;


typedef struct
{
	item_t** selected_array;
	int32_t num_selected_items;

	hashmap_t items_map;
	//hashtable_t items_hash;
	//item_t* registered_items;

	//multi selection
	Vector_t start_select;
	Vector_t end_select;

} editor_selector_t;

#define REGISTER_ITEMS_POOL 100


WAFA void editor_selector_init(editor_selector_t* const selector);
WAFA void editor_selector_free(editor_selector_t* const selector);
WAFA void editor_selector_register_item(editor_selector_t* const selector,const char* item_id,GAME_TYPE type,Rect_t* const  bounds,void* data,const char* map_id,int32_t* zorder);
WAFA item_t* editor_selector_getitem(editor_selector_t* const selector, const char* item_id);
WAFA void editor_unregister_item(editor_selector_t* const selector,const char* item_id,GAME_TYPE type);
WAFA void editor_selector_changeid(editor_selector_t* const selector,const char* old_entity_id,const char* new_entity_id,GAME_TYPE selected_type,Rect_t* new_rect,int32_t* new_z,void* new_obj);
WAFA void editor_selector_changemapid(editor_selector_t* const selector,const char* oldmapid,const char* newmapid);
WAFA void editor_selector_multiselect(editor_selector_t* const selector,Rect_t selection_rectangle,int16_t z_order,const char* current_map);
WAFA void editor_selector_selectentity(editor_selector_t* const selector,item_t* selected_item,wbool add_to_selection);
WAFA void editor_selector_selectentity_pos(editor_selector_t* const selector,int posX,int posY,int z_order,wbool add_to_selection,const char* current_map);
WAFA void editor_selector_selectentity_fromname(editor_selector_t* const selector,const char* selected_item,wbool add_to_selection);
WAFA void editor_unselect_item(editor_selector_t* const selector,item_t* const item);
WAFA void  editor_selector_unselect(editor_selector_t* const selector);
WAFA wbool  editor_selector_has_selection(editor_selector_t* const selector);
