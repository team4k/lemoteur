#ifndef __CONFIG_H__

#define __CONFIG_H__ 1

typedef void (*config_saved_t)();

typedef struct
{
	lua_State* lua_state;
	config_saved_t config_saved;
} config_t;


WAFA void config_init(config_t* const config,lua_State* ref_state);
WAFA wbool config_readconf(config_t* const config,void* resx_mngr,const char* config_path);
WAFA void config_reload(config_t* const config,const char* config_path);
WAFA void config_saveconf(config_t* const config,const char* config_path); 
WAFA wbool config_getint(config_t* const config,const char* key,int32_t* ret);
WAFA wbool config_getshort(config_t* const config,const char* key,int16_t* ret);
WAFA wbool config_setint(config_t* const config,const char* key,int32_t in);
WAFA wbool config_setshort(config_t* const config,const char* key,int16_t in);
WAFA wbool config_getfloat(config_t* const config,const char* key,float* ret);
WAFA wbool config_setfloat(config_t* const config,const char* key,float in);
WAFA wbool config_getstring(config_t* const config,const char* key,char* return_string,size_t size_string);
WAFA wbool config_setstring(config_t* const config,const char* key, const char* in_str);
WAFA wbool config_getbool(config_t* const config,const char* key,wbool* return_bool);
WAFA wbool config_setbool(config_t* const config, const char* key, wbool in_bool);

#endif