#ifndef __HASH__

#define __HASH__ 1
typedef struct {
    char** keys;
    uint32_t num_elem;
    uint32_t max_elem;
}hashtable_t;

WAFA int hashtable_index(hashtable_t* const hashtable, const char* const key);

WAFA int hashtable_pushkey(hashtable_t* const hashtable,char* key);

WAFA int hashtable_insertkey(hashtable_t* const hashtable, const char* const key);

WAFA void hashtable_removekey(hashtable_t* const hashtable,const char* const key);

WAFA int hashtable_haskey(hashtable_t* const hashtable, const char* const key);

#endif