#define WF_RAND_MAX UINT_MAX

WAFA void wf_setseed(const uint32_t seed);

WAFA const uint32_t wf_rand();

WAFA const uint32_t wf_randrange(const uint32_t min_value,const uint32_t max_value);