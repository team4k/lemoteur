#if defined(WITH_CURL)

#define URL_REQUEST_NOTSTARTED -1
#define URL_REQUEST_PENDING 0
#define URL_REQUEST_SUCCESS 1
#define URL_REQUEST_ERROR_CURL 2
#define URL_REQUEST_ERROR_HTTP 3
#define URL_REQUEST_PROCESSED 4

#define URL_BUFFER_SIZE 4096

#define URL_POSTPARAM_POOL 4

typedef struct 
{
   char* raw_data;
   size_t raw_data_size;
   unsigned char* decoded_data; 
   int pos;
   int len_decoded;
} url_result_data_t;

typedef struct
{
	char name[128];
	char* value;

} url_post_param_t;

typedef struct
{
	#if defined(_WIN32)
		HANDLE thread;
	#elif defined(EMSCRIPTEN)
		char* str_post_params;
	#else
		pthread_t* thread;
	#endif
		//request_status possible values: 0 = processing, 1 = success, 2 = can't init curl, 3 = can't perform request
	volatile uint32_t request_status;
	char url[256];
	char error_string[256];
	char callback[128];

	url_post_param_t* post_params;
	size_t max_post_params;
	size_t count_post_params;

	url_result_data_t result_data;

} url_t;

WAFA void url_init(url_t* const url_obj);

WAFA wbool url_getbase64data(url_t* const url_obj);

WAFA void url_addpostparam(url_t* const url_obj, const char* name,const char* value);

WAFA void url_recycle(url_t* const url_obj,wbool clear_params);

WAFA void url_free(url_t* const url_obj);

#endif