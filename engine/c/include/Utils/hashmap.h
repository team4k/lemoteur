#ifndef __HASHMAP__
#define __HASHMAP__ 1
#define DEFAULT_HASHMAP_POOL 10

typedef struct {
    char** keys;
    uint32_t num_elem;
    uint32_t max_elem;
	void** values;
	size_t value_size;
	size_t pool_values;
} hashmap_t;

WAFA void hashmap_initalloc(hashmap_t* const hashtable,size_t max_size,size_t value_size,size_t pool_value);

WAFA void hashmap_free(hashmap_t* const hashtable);

WAFA int hashmap_getindexforkey(hashmap_t* const hashmap,const char* const key);

WAFA char* const hashmap_getkeypointer(hashmap_t* const hashmap, const char* const key);

WAFA void* hashmap_getvalue(hashmap_t* const hashmap,const char* const key);

WAFA void* hashmap_getvalue_withlogopt(hashmap_t* const hashmap, const char* const key, int silent);

WAFA void* hashmap_getvaluefromindex(hashmap_t* const hashmap, int index);

WAFA void* hashmap_iterate_const(hashmap_t* const hashtable, uint32_t* ent_indx, uint32_t* ent_count, const uint32_t max_count);

WAFA void* hashmap_iterate(hashmap_t* const hashtable,uint32_t* ent_indx,uint32_t* ent_count);

//get the keys into this hashmap as a comma separated string, all the keys are between quote
WAFA void hashmap_getkeysstring(hashmap_t* const hashtable, char* out_string, size_t out_string_len);

WAFA void* hashmap_push(hashmap_t* const hashtable,const char* key);

WAFA int hashmap_getinsertindex(hashmap_t* const hashtable, const char* const key);

WAFA void hashmap_rename(hashmap_t* const hashtable,const char* const old_key,const char* const new_key);

WAFA void hashmap_remove(hashmap_t* const hashtable,const char* const key);

WAFA void hashmap_recycle(hashmap_t* const hashtable);

WAFA int hashmap_haskey(hashmap_t* const hashtable, const char* const key);

#endif