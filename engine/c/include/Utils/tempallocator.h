#ifndef __TEMPALLOCATOR__

#define __TEMPALLOCATOR__ 1
/**
* temporary memory allocator, DOESN'T REPLACE SYSTEM MEMORY ALLOCATOR, ONLY FOR TEMPORY MEMORY USE!!
* this allocator is used where stack allocation / static allocation is not practical and where heap allocation would generate too much allocation call
* this rudimentary allocator doesn't keep track of allocated range, must be cleared after each use
**/
typedef struct
{
    uintptr_t start;
    uintptr_t end;
    uintptr_t current;
    size_t pool_size;

} tempallocator_t;


WAFA void tempallocator_init(tempallocator_t* const allocator,size_t pool_size);

WAFA void* tempallocator_getmemory(tempallocator_t* const allocator,size_t alloc_size);

WAFA void tempallocator_dummyfree(tempallocator_t* const allocator, void* pointer);

WAFA void* tempallocator_realloc(tempallocator_t* const allocator, void* allocated_memory, size_t old_size, size_t new_size);

WAFA char* tempallocator_string_alloc(tempallocator_t* const allocator,const char* src);

WAFA void tempallocator_clear(tempallocator_t* const allocator);

WAFA void tempallocator_free(tempallocator_t* allocator);

#endif