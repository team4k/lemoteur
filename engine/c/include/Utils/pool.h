/**
* pool array, allocate a base number of element, and grow based on needs, put an hard limit to make sure that the pool never grow over it
* used in place of hashmap when we don't need to track and element based on a name
**/

typedef struct {
	uint32_t num_elem;
	uint32_t hard_limit;
	void** values;
	wbool* used;
	size_t value_size;
	size_t pool_size;
	uint32_t first_free;
} pool_t;

//init allocation
WAFA void pool_initalloc(pool_t* const pool, size_t hard_limit, size_t value_size, size_t initial_pool_size);

//free pool
WAFA void pool_free(pool_t* const pool);

//mark the next available value as used and return it
WAFA void* pool_usenextvalue(pool_t* const pool);

//iterator helper
WAFA void* pool_iterate(pool_t* const pool, uint32_t* ent_indx, uint32_t* ent_count,uint32_t* pool_iter);

//mark the value corresponding to the index as free
WAFA void pool_recyclevalue(pool_t* const pool, uint32_t index);

//recycle the entire pool
WAFA void pool_recycle(pool_t* const pool);
