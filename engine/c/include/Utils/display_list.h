#ifndef __DISPLAY_LIST__

#define __DISPLAY_LIST__ 1

void display_list_init(int16_t display_list[MAX_ZINDEX][MAX_DISPLAY_VALUE]);
void display_list_add(int16_t display_list[MAX_ZINDEX][MAX_DISPLAY_VALUE],int16_t z_order,int16_t element_pos);
void display_list_update(int16_t display_list[MAX_ZINDEX][MAX_DISPLAY_VALUE],int16_t z_order,int16_t old_element_pos,int16_t new_element_pos);
void display_list_remove(int16_t display_list[MAX_ZINDEX][MAX_DISPLAY_VALUE],int16_t z_order,int16_t element_pos);

#endif