#ifndef __CIRCULAR_ARRAY__

#define __CIRCULAR_ARRAY__ 1

typedef struct _circular_array {
 void** element_ptr;
 size_t element_size;
 uint32_t max_elements;
 volatile uint32_t write_pos;
 volatile uint32_t read_pos;
 wbool use_temp_allocator;
} circular_array;

WAFA void circular_array_init(circular_array* const array,size_t element_size,uint32_t max_elements,tempallocator_t* const allocator);
WAFA int32_t circular_array_check(circular_array* const p,wbool pushcheck);
WAFA void* circular_array_get_next_element(circular_array* const p);
WAFA void* circular_array_iterate(circular_array* const p,uint32_t* peek_read_p,wbool init);
WAFA void circular_array_push_new_element(circular_array* const p, void* new_element);
WAFA void circular_array_free(circular_array* const p);

#endif
