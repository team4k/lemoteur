#ifndef __LOCALIZATION_H__

#define __LOCALIZATION_H__ 1

#include <sqlite3.h>

typedef struct
{
	sqlite3* locale_db;
	char current_locale[256];
} localization_t;

WAFA void localization_init(localization_t* const locale,const char* db_path);
WAFA void localization_setlocale(localization_t* const locale, const char* new_locale);
WAFA wchar_t* localization_appendstring(localization_t* const locale,wchar_t* current_str_id,const char* str_id_toappend);
WAFA wchar_t* localization_getstring(localization_t* const locale,const char* string_id);
WAFA wbool localization_isstringlocalized(localization_t* const locale,const char* string_id);
WAFA void localization_free(localization_t* const locale);

#endif