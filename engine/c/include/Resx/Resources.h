#ifndef __RESOURCES_H__

#define __RESOURCES_H__ 1

#include <inttypes.h>
#include <Base/types.h>
#include <GameObjects/Level.pb-c.h>
#include <GameObjects/Characters.pb-c.h>
#include <GameObjects/Shaders.pb-c.h>
#include <Sound/Sound.h>
#include <Graphics/TextureLoader.h>
#include <Resx/shader_loader.h>
#include <Graphics/TextureRegion.h>
#include <Graphics/FontLoader.h>
#include <Render/render_manager.h>

typedef enum { texture, level, animation, sound, font, shader,empty,sound_uncompressed,shader_d3d_def,world, fragment} resx_type;


typedef struct resource_t
{
    char* name;
    char* path;
    resx_type type;
	wbool survive_collect;
	void* pointer;

	//for virtual resx
	wbool is_virtual;
	char linked_resx[128];
	void* virtual_data;
	size_t virtual_data_size;
} resource_t;

typedef struct
{
	font_info* font_info;
	char font_name[128];

} font_resx_t;

typedef struct
{
    resource_t* resx_list;
    hashtable_t* resx_hash;

	wbool collect_old_resx;

	void* zip_archive;
	
	wbool compressed_resx;

	render_manager_t* render_mngr;
	char resx_path[WAFA_MAX_PATH];

} resources_manager_t;


WAFA wbool init_resources(resources_manager_t* resx_mngr,const char* const resources_path,render_manager_t* const render_mngr);

WAFA size_t getresxextsize();

WAFA char** getresxext();

WAFA void start_resx_collect(resources_manager_t* resx_mngr);

WAFA void end_resx_collect(resources_manager_t* resx_mngr);

WAFA void settexture_resx(resources_manager_t* resx_mngr,const char* texture_name,int16_t width,int16_t height, unsigned char* data,unsigned long data_size);

WAFA wbool has_resx(resources_manager_t* resx_mngr,const char* resx_name);

//get the content of a resource in the manager. if called with a null resx_mngr, it will load data from the absolute path on the file system
WAFA unsigned char* get_resx_content(resources_manager_t* const resx_mngr,const char* path,size_t* resx_size,const char* read_param);

WAFA texture_info* gettexture_resx_back(resources_manager_t* resx_mngr, const char* texture_name);

WAFA texture_info* gettexture_resx(resources_manager_t* resx_mngr,const char* texture_name);

WAFA texture_info* gettexture_resx_withfilter(resources_manager_t* resx_mngr, const char* texture_name, texture_filter filter, wbool backgroundload);

//delete the texture data in VRAM
WAFA void resx_g_clear_texture(resources_manager_t* const resx_mngr);

//create the texture in VRAM
WAFA void resx_g_create_texture(resources_manager_t* const resx_mngr) ;

WAFA void resx_set_from_packer(resources_manager_t* const resx_mngr,const char* packer_name);

WAFA void resx_set_from_rawpacker(resources_manager_t* const resx_mngr,unsigned char* packer_data,size_t size_data);

WAFA Level* getlevel_resx(resources_manager_t* resx_mngr,const char* level_name);

WAFA World* getworld_resx(resources_manager_t* resx_mngr, const char* world_name);

WAFA Fragment* getfragment_resx(resources_manager_t* resx_mngr, const char* fragment_name);

WAFA Fragment* getfragment_copy_resx(resources_manager_t* resx_mngr, const char* fragment_name, ProtobufCAllocator* temp_allocator);

WAFA void setanimation_resx(resources_manager_t* resx_mngr, const char* animation_name, unsigned char* animData,size_t anim_size);

WAFA AnimationList* getanimation_resx(resources_manager_t* resx_mngr, const char* animation_name);

WAFA sound_sample_t* getsound_resx(resources_manager_t* resx_mngr,const char* sound_name);

WAFA font_info* getfont_resx(resources_manager_t* resx_mngr,const char* font_name);

WAFA font_info* getfont_resx2(resources_manager_t* resx_mngr,const char* font_name,unsigned long char_start,unsigned long char_end);

WAFA shader_info* getshader_resx(resources_manager_t* const resx_mngr,const char* shader_name,wbool is_fragment_shader);

WAFA void reloadshader_resx(resources_manager_t* resx_mngr, const char* shader_name);

WAFA void resx_g_clear_font(resources_manager_t* const resx_mngr);

WAFA void resx_g_create_font(resources_manager_t* const resx_mngr);

WAFA void resx_g_clear_shader(resources_manager_t* const resx_mngr);

WAFA void resx_g_create_shader(resources_manager_t* const resx_mngr);

WAFA void* getgeneric_resx(resources_manager_t* const resx_mngr,const char* resx_name,resx_type type,...);

WAFA wbool has_raw_resx(resources_manager_t* resx_mngr,const char* resx_path);

WAFA size_t getrawdata_size(resources_manager_t* resx_mngr,const char* resx_path);

WAFA int getrawdata(resources_manager_t* resx_mngr,const char* resx_path,unsigned char* buffer,long size);

WAFA void add_resx(resources_manager_t* resx_mngr, const char* filename, resx_type type, const char* const resources_path,wbool is_virtual,const char* linked_resx,void* virtual_data);

WAFA void remove_resx(resources_manager_t* resx_mngr,const char* filename, resx_type type);

WAFA void resx_set_from_packer(resources_manager_t* const resx_mngr,const char* packer_name);

WAFA resource_t* get_resx(resources_manager_t* const resx_mngr,const char* resx_name);

WAFA void free_resx_ptr(resources_manager_t* resx_mngr,resx_type type,int32_t resx_indx);

WAFA void free_all_resources(resources_manager_t* resx_mngr);

#endif


