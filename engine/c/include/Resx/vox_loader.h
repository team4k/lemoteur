//================
// RGBA
//================
typedef struct
{
    unsigned char r;
	unsigned char g;
	unsigned char b;
	unsigned char a;
} MV_RGBA;

//================
// Voxel
//================
typedef struct 
{
    unsigned char x;
	unsigned char y;
	unsigned char z;
	unsigned char colorIndex;
} MV_Voxel;

typedef struct 
{
        int id;
        int contentSize;
        int childrenSize;
        long end;
} chunk_t;

typedef struct  {
    // size
    int sizex;
	int sizey;
	int sizez;
    
    // voxels
    int numVoxels;
    MV_Voxel *voxels;

    // palette
    wbool isCustomPalette;
    MV_RGBA palette[ 256 ];
    
    // version
    int version;
} MV_Model;
    
WAFA void MV_Model_init(MV_Model* const model);
WAFA void MV_Model_free(MV_Model* const model);
void Free(MV_Model* const model);
WAFA wbool LoadModel( MV_Model* const model,const char *path );
wbool ReadModelFile(MV_Model* const model, FILE *fp ) ;
void ReadChunk( FILE *fp, chunk_t* const chunk ) ;
int ReadInt( FILE *fp );
void Error( const char *info );

    
