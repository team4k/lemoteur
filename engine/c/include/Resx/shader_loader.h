#ifndef __SHADER_LOADER__

#define __SHADER_LOADER__ 1

typedef struct
{
	SHADER_ID shader_pointer;
	char shader_name[256];
	wbool is_fragment_shader;

	void* shader_info;

} shader_info;

WAFA wbool load_shader(shader_info* const shader,unsigned char* shader_data,size_t shader_data_size,const char* shader_name,render_manager_t* const render_mngr,wbool is_fragment_shader);
void reload_shader(shader_info* const shader,unsigned char* shader_data,size_t shader_data_size,const char* shader_name,render_manager_t* const render_mngr);

#endif