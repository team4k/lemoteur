#define COLORSINGLE L"{C="
#define COLORSTART L"{CD="
#define COLOREND  L"{CF}"
#define SIZESINGLE L"{T="
#define SIZESTART L"{TD="
#define SIZEEND L"{TF}"
#define OPENCHAR L"{"
#define CLOSECHAR L"}"
#define COLORSIZE 8
#define MAXTEXTSIZE 5 //00.00


///parse the texttoparse parameters to a cleaned text outtextbuffer without markup, store markup info into outcolorbuffer parameter and outsizebuffer parameter
WAFA void text_parser_parse(const wchar_t* texttoparse, const ColorA_t basecolor, wchar_t* const outtextbuffer, ColorA_t* const outcolorbuffer, float* const outsizebuffer, size_t buffersize);