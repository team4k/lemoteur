#ifndef __ENGINE__

#define __ENGINE__ 1

#if defined(_WIN32)
    #define WAFA  extern "C" __declspec( dllexport ) 
    #define PATH_SEP '\\'
	#define __func__ __FUNCTION__
	#pragma warning(disable : 4995)
#else
   #define WAFA  __attribute__((visibility("default"))) 
   #define PATH_SEP '/'
#endif


#ifndef GAME_TYPE_DEF

	typedef enum
	{
		GT_ENTITY,
		GT_TRIGGER,
		GT_TEXT,
		GT_SOUND,
		GT_LIGHT,
		GT_PARTICLES,
		GT_WAYPOINT,
		GT_COLLIDER
	} GAME_TYPE;

#define GAME_TYPE_DEF
#endif

#ifndef WM_PI
	#ifdef MATH_DOUBLE
		#define WM_PI 3.14159265358979323846264338327950288
	#else
		#define WM_PI 3.14159265358979323846f
	#endif
#endif

#ifndef WM_2_PI
    #ifdef MATH_DOUBLE
        #define WM_2_PI 0.636619772367581343075535053490057448
    #else
        #define WM_2_PI 0.63661977236758134307f
    #endif
#endif

#ifndef WM_PI_2
    #ifdef MATH_DOUBLE
        #define WM_PI_2 1.57079632679489661923132169163975144
    #else
        #define WM_PI_2 1.570796326794896619231f
    #endif
#endif


#ifndef UINT_MAX
	#define UINT_MAX 0xffffffff
#endif

#ifndef errno
	extern int errno;
#endif

WAFA void _internal_free(void* ptr);
WAFA void* _internal_malloc(size_t size);
WAFA void* _internal_calloc(size_t count, size_t size);
WAFA void* _internal_realloc(void* ptr,size_t new_size);

#ifndef ALLOC_TRACER
	#define wf_malloc(size) _internal_malloc(size)
	#define wf_free(ptr) _internal_free(ptr)
	#define wf_realloc(ptr,size) _internal_realloc(ptr,size)
	#define wf_calloc(count,size) _internal_calloc(count,size)
#else
	void* malloc_tracer(float line,const char* filename,size_t bytes);
	void free_tracer(float line,const char* filename,void* ptr);
	void* realloc_tracer(float line,const char* filename,size_t bytes,void* ptr);
	void* calloc_tracer(float line,const char* filename,size_t count,size_t size);


	#define wf_malloc(size) malloc_tracer((float)__LINE__,__FILE__,size)
	#define wf_free(ptr) free_tracer((float)__LINE__,__FILE__,ptr)
	#define wf_realloc(ptr,size) realloc_tracer((float)__LINE__,__FILE__,size,ptr)
	#define wf_calloc(count,size) calloc_tracer((float)__LINE__,__FILE__,count,size)

#endif


#ifdef _WIN32
	#define w_sprintf sprintf_s
	#define w_strcpy strcpy_s
	#define w_wcsncpy wcsncpy_s
	#define w_strncpy strncpy_s
	#define w_fopen(file,path,mode) fopen_s(&file,path,mode)
	#define w_memcpy memcpy_s
	#define w_wcsrtombs wcsrtombs_s
	#define w_localtime localtime_s
	#define w_strerror strerror_s
    #define w_wcscpy wcscpy_s
	#define w_strcat strcat_s
	#define w_mbstowcs mbstowcs_s
#else
	#define w_sprintf snprintf
	#define w_strcpy(dest, size, src) (strncpy((dest), (src), (size))) //USE -D_FORTIFY_SOURCE=2 in addition with GCC
	#define w_wcsncpy wcsncpy
	#define w_strncpy(dest,size,src,maxsize) (strncpy((dest),(src),(size)))
	#define w_fopen(file,path,mode) (file = fopen(path,mode))
	#define w_memcpy(dest,destsz,src,srcsz) memcpy((dest),(src),(destsz))
	#define w_wcsrtombs(_Retval, _Dst, _SizeInBytes, _Src, _Size,_State) wcsrtombs((_Dst),(_Src),(_Size),(_State))
	#define w_localtime(tm,time) (tm = localtime(time))
	#define w_strerror(buff,size,errn) (buff = strerror(errn))
	#define w_wcscpy(dest,size,src) (wcscpy((dest),(src)))
	#define w_strcat(dest,size,src) (strcat((dest),(src)))
	#define w_mbstowcs(converted_len,dest,dest_len,src,count) (mbstowcs((dest),(src),(count)))
#endif

#ifdef MEM_TRACE
	WAFA void init_mem_trace();
	WAFA void snapshot_mem_trace(const char* snap_name);
	WAFA void shutdown_mem_trace();
	WAFA void frameend_mem_trace();
#endif

#ifdef _MSC_VER
	#if (_MSC_VER == 1900)
		#define PROTOBUF_C_SKIP_INTTYPES_H 1
	#endif // 0

#endif

WAFA const char* getbuildversion();



#define NO_COLLISION 0
#define COLLISION 1
#define COLLISION_DESTRUCTIBLE 2
#define COLLISION_AI 3
#define COLLISION_PLAYER 4
#define COLLISION_SLOPE 5
#define COLLISION_SLOPE_INVERSE 6
#define COLLISION_EDITABLE 7
#define COLLISION_EDITABLE_2 8
#define COLLISION_EDITABLE_3 9
#define COLLISION_KILL 10
//value use in light manager to check which light is visible by the player
#define FLOODLIGHT 100

#define COL_CHECK_MAX 4

#define MAX_MAPS 10
#define MAX_ENTITIES 160
#define MAX_GROUP 30
#define MAX_TRIGGER 70
#define MAX_TEXT_OBJ 50
#define MAX_SOUND_SAMPLE 200
#define MAX_ZINDEX 20
#define MAX_DISPLAY_VALUE 160
#define WAFA_MAX_PATH 260
#define MAX_SHADER_PROGRAMS 20
#define MAX_LIGHT_PER_ELEMENT 16
#define MAX_ALLOWED_SCRIPT_POINTERS 1000
#define POOL_PARTICLES_OBJ 8
#define POOL_WAYPOINT_OBJ 10
#define POOL_COLLIDER_OBJ 10

#define POOL_DYNAMIC_STREAM_ENTITIES 100

#endif



