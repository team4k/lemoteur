#ifndef __PHYSICS_TILEMAP_UTILS_H__

#define __PHYSICS_TILEMAP_UTILS_H__ 1

WAFA void rebuild_tilemap_fromcache(physics_manager_t* const  manager, Tilemap_t* const ctmap, render_manager_t* const render_mngr, check_col_t check_col_func);

WAFA void build_tilemap_collisions(physics_manager_t* const  manager, int32_t* const destructible_data, const int32_t* const collisions_array, const int32_t col_count, const int32_t row_count, const int32_t tile_size, WorldRect_t dim, uint16_t num_chunk_col, uint16_t num_chunk_row, check_col_t check_col_func, check_col_t check_col_func_editable);

WAFA void update_tilemap_ranged(physics_manager_t* const  manager, int32_t* const destructible_data, const int32_t* const collisions_array, WorldRect_t dim, int32_t index_start, int32_t col_count, int32_t row_count, const int32_t tile_size, const int32_t index_jump, int32_t shape_index, int32_t left_neigh_index, int32_t right_neigh_index, int32_t top_neigh_index, int32_t bottom_neigh_index, uint16_t num_chunk_col, uint16_t num_chunk_row, uint16_t chunk_mem, check_col_t check_col_func, check_col_t check_col_func_editable);

#endif
