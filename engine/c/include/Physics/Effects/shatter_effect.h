typedef struct
{
	compound_sprite_t shattered_sprite;
	cpBody* frag_list[COMPOUND_MAX_SHAPE];
	cpShape* shape_frag_list[COMPOUND_MAX_SHAPE];
	int32_t frag_count;
	float cleanup_delay;
	short status;//0 = inactive, 1 = active
	int shatter_collision_type;
	wbool sprite_onlymode;
	uint32_t script_id;
} shatter_effect_t;


typedef struct  {
	uint32_t seed;
	cpFloat cellSize;
	int width, height;
	cpBB bb;
	cpVect focus;
} WorleyContex;

#define SHE_MAX_VERTEXES_PER_VORONOI 16
#define SHE_DENSITY (1.0/10000.0)
#define CLEANUP_DELAY 100.0f
#define IMPULSE_POWER 100.0f
#define ALPHA_STEP 0.05f

WAFA void shatter_effect_init(shatter_effect_t* const oshatter,const int32_t effect_width,const int32_t effect_height,texture_info* const base_texture,const int32_t tile_size,render_manager_t* const render_mngr,int collision_type);

WAFA void shatter_effect_draw(shatter_effect_t* const oshatter,render_manager_t* const render_mngr);

WAFA void shatter_effect_updatesize(shatter_effect_t* const oshatter,const int32_t sprite_width,const int32_t sprite_height,const int32_t tile_size);

WAFA void shatter_effect_updatetexture(shatter_effect_t* const oshatter,texture_info* const new_texture);

WAFA void shatter_effect_create(shatter_effect_t* const oshatter,const int32_t tile_index,cpVect* base_vertices,int16_t size,cpSpace* const world,float mass,cpVect pos,cpVect impactPoint,float cellSize,cpVect shatter_velocity,cpFloat rotation,wbool mirror_x,wbool mirror_y,render_manager_t* const render_mngr,wbool sprite_onlymode,wbool invert_gravity);

WAFA void shatter_effect_update(shatter_effect_t* const oshatter,float delta,cpSpace* const world);

WAFA void shatter_effect_reinit(shatter_effect_t* const oshatter,cpSpace* const world);