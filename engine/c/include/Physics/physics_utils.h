#ifndef __PHYSICS_UTILS_H__

#define __PHYSICS_UTILS_H__ 1

WAFA Vector_t convert_cpvec_tovec(cpVect vec);

WAFA Vector_t convert_cpfloatpair_tovec(cpFloat x, cpFloat y);

WAFA void update_edges(const int32_t col_count, const int32_t row_count, const int32_t tile_size, edge_container_t* const edge_array, const int32_t edge_array_size, const int32_t* const collisions_array, int32_t tilePos, wbool add_tile, edge_t*  preallocated_edge_array, Vector_t edge_offset);

WAFA edge_container_t* alloc_edges_list(const int32_t row_count, const int32_t col_count, edge_container_t* dst_edge_array, int32_t* const edge_array_size, wbool regen, edge_t**  preallocated_edge_arrayp);

WAFA void gen_edges_list(const int32_t row_count, const int32_t col_count, const int32_t tile_size, const int32_t* const src_collisions_array, edge_container_t* const segment, edge_t**  preallocated_edge_arrayp, check_col_t check_col_func, Vector_t edge_offset);

WAFA int32_t convert_worldpos_to_tilepos(uint32_t worldPos, uint32_t num_chunk_col, uint32_t base_col_count, uint32_t base_row_count);

WAFA int32_t convert_tilepos_to_worldpos(uint32_t tilepos, uint16_t ichunk, uint32_t num_chunk_col, uint32_t base_col_count, uint32_t base_row_count);

WAFA uint16_t get_chunk_pos_from_tilepos(uint32_t worldtilepos, uint32_t num_chunk_col, uint32_t base_col_count, uint32_t base_row_count);

#endif