
WAFA void convert_cpvect_to_float(cpVect* src,float** dest,size_t array_size);

WAFA void convert_cpfloat_to_float(cpFloat* src, float* dest, size_t array_size);