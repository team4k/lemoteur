#ifndef __PHYSICS_H__

#define __PHYSICS_H__ 1

typedef enum {PTILE_TOP,PTILE_LEFT,PTILE_RIGHT,PTILE_BOTTOM,PTILE_SLOPE} segment_type;

typedef struct
{
	int32_t shape_index;
	int32_t tilemap_index;
	int32_t world_index;
	cpVect contact_point;
	cpVect contact_normal;
	segment_type tile_type;
	int16_t ichunk;
	cpCollisionType base_typ_col;
} callback_ref_t;

typedef struct
{
	cpVect start_segment;
	cpVect end_segment;
	cpCollisionType typ_col;
	cpDataPointer data;
	cpVect neighbour1;
	cpVect neighbour2;
} segment_t;

typedef struct
{
	cpSpace* world;
	cpShape** static_shapes;
	callback_ref_t* callback_refs;
	int32_t* collisions_array;
	int32_t* tilemap_array;

	//keep row and col count locally in case of tilemap resize
	int32_t row_count;
	int32_t col_count;

	wbool is_slave;

	//prealloc all physics manager data before use
	wbool pool_alloc;

	segment_t** collisions_segment;
	segment_t* allocated_segment;
	cpSegmentShape** allocated_shape;
	cpBitmask defaultwallmask;

	volatile uint32_t unload_inprogress;

} physics_manager_t;



#define LAYER_STATIC 1
#define LAYER_PLAYER 2
#define LAYER_ENTITY 4
#define LAYER_FORCE 8
#define LAYER_INACTIVE 16


WAFA void physics_init(physics_manager_t* const  manager, wbool use_spatial_hash);

WAFA void physics_init2(physics_manager_t* const  manager, wbool use_spatial_hash, wbool pool_alloc);

WAFA void physics_alloc(physics_manager_t* const  manager, const int32_t col_count, const int32_t row_count, const wbool has_destructible_data);

WAFA void create_slave_manager(physics_manager_t* const slave_manager,physics_manager_t* const master_manager);

WAFA void update_physics(physics_manager_t* const manager,float delta);

//remove and free tilemap collisions shape, if removeonly is true, the shape are kept in memory
WAFA void physics_free_tilemap_collisions(physics_manager_t* const manager,wbool removeonly);

WAFA void physics_setelasticity(physics_manager_t* const manager,cpFloat elasticity);

WAFA void physics_setfriction(physics_manager_t* const manager,cpFloat friction);

wbool default_check_col(int32_t collision_value);

WAFA void physics_create_collision_shape(physics_manager_t* const  manager, int32_t tile_col, int32_t tile_row, int32_t tile_size, int32_t shape_index, int32_t index, cpCollisionType col_type, const int32_t col_count, const int32_t row_count, const int32_t tile_index, const int32_t* const collisions_array, segment_t** segment, WorldRect_t dim, int16_t ichunk, check_col_t check_col_func, check_col_t check_col_func_editable,int32_t num_chunk_col);

WAFA void physics_set_shape_layer(physics_manager_t* const  manager,int32_t shape_index,int32_t layer_id,int32_t mask);

WAFA void physics_free(physics_manager_t* manager);

#endif