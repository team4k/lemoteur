#if  defined(RENDERER_OPENGL) || defined(RENDERER_OPENGL_ES)

GLfloat glMaxf(const GLfloat value0, const GLfloat value1);

GLfloat glMinf(const GLfloat value0, const GLfloat value1);

GLfloat glRadToDegf(const GLfloat radians);

GLfloat glDegToRadf(const GLfloat degrees);

GLfloat glMixf(const GLfloat value0, const GLfloat value1, const GLfloat t);

GLfloat glClampf(const GLfloat value, const GLfloat min, const GLfloat max);

GLfloat glLengthf(const GLfloat x, const GLfloat y, const GLfloat z);

void glVector3Normalizef(GLfloat vector[3]);


#endif