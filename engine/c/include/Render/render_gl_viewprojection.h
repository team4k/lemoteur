#if  defined(RENDERER_OPENGL) || defined(RENDERER_OPENGL_ES)

wbool w_glOrthof(GLfloat result[16], const GLfloat left, const GLfloat right, const GLfloat bottom, const GLfloat top, const GLfloat nearVal, const GLfloat farVal);
void w_glLookat(GLfloat result[16],GLfloat eyePosition[3],GLfloat FocusPosition[3],GLfloat upDirection[3]);
void w_glPerspective(GLfloat result[16],GLfloat fovy,GLfloat aspect,GLfloat zNear,GLfloat zFar);

#endif