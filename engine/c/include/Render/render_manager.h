#ifndef __RENDER_MANAGER__

#define __RENDER_MANAGER__ 1

#include <inttypes.h>
#include <Base/types.h>
#include <Base/Color.h>
#include <Render.h>

typedef enum {filter_pixelart,filter_linear} texture_filter;
typedef enum {blend_min,blend_max,blend_add} blend_eq;
typedef enum {blend_alpha,blend_replace,blend_additive} blend_op;
typedef enum {R_FILL,R_WIREFRAME} render_mode;
typedef enum {R_PORTRAIT,R_LANDSCAPE,R_PORTRAIT_FLIPPED,R_LANDSCAPE_FLIPPED} orientation_mode;
typedef enum {AR_4_3=0,AR_16_9=1,AR_16_10=2,AR_21_9=3,AR_UNKOWN=4} aspectratio;

#define SET_WORLD_MAT 1
#define SET_VIEW_MAT 2
#define SET_PROJ_MAT 4

typedef struct
{
	int width;
	int height;
	int refreshrate;
	aspectratio aspectratio;
} videomode_t;


typedef struct
{
	Vector_t scroll_vector;
	Rect_t screen_info;
	float zoom_factor;
	float near_value;
	float far_value;
	wbool fullscreen;
	int16_t refreshrate;
	const char* window_title;
	int fullscreen_width;
	int fullscreen_height;
	int fullscreen_offsetx;
	int fullscreen_offsety;
	TEXTURE_ID current_bind_texture; 
	texture_filter texture_filtering; //pixelart,linear
	int mat_flag;
	orientation_mode screen_orientation;
	videomode_t** video_resolutions;
	int countresolutions;
	int32_t frame_number;


	#if  defined(RENDERER_OPENGL) || defined(RENDERER_OPENGL_ES)
		
		#ifdef ANDROID_BUILD
			EGLDisplay oglDisplay;
			EGLSurface oglSurface;
			EGLContext oglContext;
		#endif

		#ifdef USE_GLFW_RENDER
			GLFWwindow* oglWindow;
		#endif

		#ifdef USE_SDL_RENDER
			SDL_Window* oglWindow;
			SDL_GLContext oglcontext;
		#endif

		//used by FFP context
		GLint viewport[4];
		GLint viewPortMatrix[6];
		//GLdouble ProjMatrix[16];
		//GLdouble ViewMatrix[16];
		wbool enabled_vertex_array;
		wbool enabled_texcoord_array;
		ColorA_t current_color;

		GLfloat ProjMatrix[16];
		GLfloat ViewMatrix[16];
		GLfloat WorldMatrix[16];
		FBO_ID current_framebuffer;

		//shader attribute cache
		GLint inColor_attrib;
		GLint inPos_attrib;
		GLint inTex_attrib;
		GLint inPos3_attrib;
		GLint inNormal_attrib;

		GLint uniColor;
		GLint uniTrans;
		GLint uniWorld;
		GLint uniView;
		GLint uniProj;

		GLfloat WVP[16];

		int major_opengl_version;
		int minor_opengl_version;

	
	#endif

		wbool enabled_blend;
		blend_eq c_blend_eq;
		blend_op c_blend_op;

		//used by programmable context
		SHADERPROGRAM_ID current_program;

		//hashtable to keep track of shader programs
		void* hashtable_shader_programs;
		SHADERPROGRAM_ID shader_programs[MAX_SHADER_PROGRAMS];


		//temporary buffer object
		VBO_ID tmp_vertices_vbo;
		VBO_ID tmp_texcoord_vbo;
		size_t last_size_vert_vbo;
		size_t last_size_texcoord_vbo;

		VAO_ID global_vao;

	

	#ifdef RENDERER_DX

		WNDCLASSEX wc;
		IDXGISwapChain* SwapChain;

		IDXGIOutput* dxgiOutput;

		ID3D11Device* d3d11Device;
		ID3D11DeviceContext* d3d11DevCon;
		ID3D11RenderTargetView* renderTargetView;
		ID3D11RasterizerState* m_rasterState;
		ID3D11SamplerState* texSamplerState;

		ID3D11RenderTargetView* currentTargetView;


		XMMATRIX projection;
		XMMATRIX view;
		XMMATRIX world;

		XMMATRIX scale_matrix;
		XMMATRIX rotation_matrix;
		XMMATRIX position_matrix;



		ID3D11Buffer* tmpvertexBuffer;
		ID3D11Buffer* tmptexCoordBuffer;

		ID3D11BlendState* Transparency;

		XMMATRIX Orientation2D;
		XMMATRIX Orientation3D;

		int16_t orig_screen_width;
		int16_t orig_screen_height;



	#endif


} render_manager_t;


//implemented in separate files depending of render preprocessor macro
WAFA int render_init_window_render(render_manager_t* const rendermngr, wbool editor_mode, wbool fullscreen, const char* window_title, void* app_instance, void* app_handle, int16_t refreshrate);
WAFA wbool render_recreate_window(render_manager_t* const rendermngr, wbool fullscreen, void* app_handle, int16_t refreshrate);
WAFA void render_set_swap_interval(render_manager_t* const rendermngr);
WAFA void render_swap_buffers(render_manager_t* const rendermngr);
WAFA void render_destroy_window(render_manager_t* const rendermngr);
WAFA int render_getrefreshrate(render_manager_t* const rendermngr);



WAFA int render_init(render_manager_t* const rendermngr, int16_t width, int16_t height, const char* app_path, const char* window_title, void* app_instance, void* app_handle, wbool editor_mode, wbool fullscreen, texture_filter texture_filtering, int16_t refreshrate);
WAFA void render_set_backfacecull(render_manager_t* const rendermngr, wbool cull);
WAFA void render_setup_config(render_manager_t* const rendermngr);
WAFA void render_setup_debugconfig(render_manager_t* const rendermngr);
WAFA void render_recreatescreen(render_manager_t* const rendermngr, wbool fullscreen, void* app_handle, int16_t new_width, int16_t new_height, int16_t refreshrate);
WAFA void render_resizescreen(render_manager_t* const rendermngr,int16_t new_width,int16_t new_height);
WAFA void render_destroyscreen(render_manager_t* const rendermngr);

TEXTURE_ID render_gen_texture(render_manager_t* const rendermngr,uint32_t width,uint32_t height,unsigned char* buffer,unsigned long buffer_size,wbool luminance);
TEXTURE_ID render_gen_texture_withfilter(render_manager_t* const rendermngr, uint32_t width, uint32_t height, unsigned char* buffer, unsigned long buffer_size, wbool luminance, texture_filter filter);
WAFA void render_delete_texture(render_manager_t* const rendermngr,TEXTURE_ID texture,wbool is_3d);
void render_delete_shader(render_manager_t* const rendermngr,SHADER_ID shader);
WAFA void render_bind_texture(render_manager_t* const rendermngr,TEXTURE_ID texture,wbool repeat_texture,wbool is_3d);
WAFA void render_unbind_texture(render_manager_t* const rendermngr,wbool is_3d);

WAFA TEXTURE_ID render_gen_texture3D(render_manager_t* const rendermngr,uint32_t width,uint32_t height,uint32_t depth,unsigned char* buffer,unsigned long buffer_size);

WAFA void render_set_color(render_manager_t* const render_mngr,ColorA_t color);
WAFA void render_reset_color(render_manager_t* const render_mngr);

WAFA VBO_ID render_gen_buffer(render_manager_t* const rendermngr,void* buffer_data,size_t buffer_size,wbool dynamic,wbool stream);
WAFA VBO_ID render_gen_bufferindices(render_manager_t* const rendermngr,const uint16_t* const indices,size_t buffer_size,wbool dynamic,wbool stream);
WAFA void render_delete_buffer(VBO_ID buffer);
WAFA void render_start_draw_buffer(render_manager_t* const render_mngr,TEXTURE_ID texture,VBO_ID vertices_buffer,VBO_ID texcoord_buffer,VBO_ID color_buffer,int32_t size_vertice);
WAFA void render_draw_buffer(render_manager_t* const render_mngr,int32_t start_index,int32_t length);
WAFA void render_end_draw_buffer(render_manager_t* const render_mngr,wbool has_textcoord,wbool has_colors);

WAFA void render_resize_updatebuffer(render_manager_t* const rendermngr, VBO_ID buffer, float* buffer_data, size_t buffer_size, wbool dynamic, wbool stream);

WAFA void render_update_buffer(render_manager_t* const rendermngr,VBO_ID buffer,void* buffer_data,size_t offset,size_t size);

//update buffer asynchronously by "orphaning" the buffer (setting the buffer data to null before updating data) which avoid synchronisation, in case of incompatible platform, use render_unsync_updatebuffer
WAFA void render_discard_updatebuffer(render_manager_t* const rendermngr,VBO_ID buffer,float* buffer_data,size_t buffer_size,wbool dynamic,wbool stream);

WAFA FENCEID render_addfencesync(render_manager_t* const rendermngr);
WAFA void render_removefencesync(FENCEID syncid);
WAFA wbool render_fencesyncisready(render_manager_t* const rendermngr, FENCEID syncid);

//update buffer asynchronously, the buffer MUST NOT be in use / queued in the command buffer, use the render_addfencesync / removefencesync / fencesyncisready to check if the buffer is in use
WAFA void render_unsync_updatebuffer(render_manager_t* const rendermngr, VBO_ID buffer, float* buffer_data, size_t buffer_size);

WAFA void render_bind_buffer(render_manager_t* const rendermngr,VBO_ID buffer);
WAFA void render_step_update_buffer(render_manager_t* const rendermngr,float* buffer_data,size_t offset,size_t size,VBO_ID buffer);
WAFA void render_unbind_buffer(render_manager_t* const rendermngr,VBO_ID buffer);

WAFA void render_update_indices_buffer(render_manager_t* const rendermngr,VBO_ID buffer,void* buffer_data,size_t offset,size_t size);
WAFA void render_discard_update_indicesbuffer(render_manager_t* const rendermngr,VBO_ID buffer,uint16_t* buffer_data,size_t buffer_size,wbool dynamic,wbool stream);


WAFA void render_set_position(render_manager_t* const render_mngr,Vector_t position);
WAFA void render_set_position3(render_manager_t* const render_mngr,Vector3_t position);
WAFA void render_set_rotation(render_manager_t* const render_mngr,float angle);
WAFA void render_set_scale(render_manager_t* const render_mngr,float scaleX,float scaleY,float scaleZ);
WAFA void render_set_rotation2(render_manager_t* const render_mngr,float angle_x,float angle_y,float angle_z);

WAFA void render_start_draw_array(render_manager_t* const render_mngr, int32_t size_vertice,uint32_t numVertices,float* vertices_array,float* tex_coord_array);
WAFA void render_draw_array(render_manager_t* const render_mngr,int32_t offset, int32_t numVertices);
WAFA void render_draw_line(render_manager_t* const render_mngr,int32_t offset, int32_t numVertices);
WAFA void render_draw_lineloop(render_manager_t* const render_mngr,int32_t offset, int32_t numVertices);
WAFA void render_draw_fan(render_manager_t* const render_mngr,int32_t offset, int32_t numVertices);
WAFA void render_draw_points(render_manager_t* const render_mngr,int32_t offset, int32_t numVertices,float pointsize);
WAFA void render_draw_indices(render_manager_t* const render_mngr, VBO_ID indices, size_t num_indices);
WAFA void render_end_draw_array(render_manager_t* const render_mngr,wbool has_textcoord);
WAFA void render_mult_matrix(render_manager_t* const render_mngr,const float* matrix);


void render_draw_compound_textured_array(render_manager_t* const render_mngr,TEXTURE_ID texture,float* vertices,float* texture_coord,wbool repeat_texture,ColorA_t color,int16_t* sprite_sizes,int32_t num_sprite,int32_t size_vertice);
void render_draw_compound_textured_buffer(render_manager_t* const render_mngr,VBO_ID vertices_id,VBO_ID text_coord,VBO_ID indices,Vector_t* positions,float* rotations,int32_t num_sprite,int16_t* sprite_sizes,int16_t* sprite_indices_size);
void render_draw_bufferindices(render_manager_t* const render_mngr, VBO_ID vertices, VBO_ID indices, size_t num_indices);
WAFA void render_draw_textured_array(render_manager_t* const render_mngr,TEXTURE_ID texture,float* vertices,float* texture_coord,wbool repeat_texture,float rotation,int32_t num_vertices,int32_t size_vertice,float x,float y,ColorA_t* const color,wbool is_3d_texture);

WAFA void render_draw_3Dbufferindices(render_manager_t* const render_mngr,VBO_ID vertices,VBO_ID indices,VBO_ID normals,size_t num_indices,render_mode mesh_mode,wbool has_normals);

WAFA void render_blend_alpha(render_manager_t* const render_mngr, wbool force);
WAFA void render_setblend(render_manager_t* const render_mngr,blend_eq eq,blend_op op);
WAFA void render_setzoomproj(render_manager_t* const render_mngr,float zoom);
WAFA void render_update_screen(render_manager_t* const render_mngr);
WAFA void render_clear_screen(render_manager_t* const render_mngr);
WAFA void render_clear_screenwithcolor(render_manager_t* const rendermngr,ColorA_t back_color);
WAFA void render_push_matrix(render_manager_t* const render_mngr);
WAFA void render_pop_matrix(render_manager_t* const render_mngr);

WAFA double render_gettime();
WAFA float render_gettime2(wbool msec);
WAFA void render_stop(render_manager_t* const rendermngr);
WAFA const videomode_t** const render_getvideoresolutions(render_manager_t* const rendermngr, int* outcountresolutions);
WAFA void render_freevideoresolutions(render_manager_t* const rendermngr);

WAFA  SHADER_ID render_load_shader(render_manager_t* const rendermngr,const char* shader_name,char* shader_buffer,size_t shader_data_size,wbool is_fragment_shader,void* shader_info);
WAFA void render_reload_shader(render_manager_t* const rendermngr,const char* shader_name,SHADER_ID shader_id,char* shader_buffer);
WAFA SHADERPROGRAM_ID render_create_program(render_manager_t* const rendermngr,const char* program_id,SHADER_ID vertex_shader,SHADER_ID fragment_shader);
WAFA void render_relink_program(render_manager_t* const rendermngr,const char* program_id,SHADERPROGRAM_ID program);
WAFA void render_use_program(render_manager_t* const rendermngr,SHADERPROGRAM_ID program);
WAFA int32_t render_get_numsubprogram(render_manager_t* const rendermngr,SHADERPROGRAM_ID prog_o);
WAFA void render_use_subprogram(render_manager_t* const rendermngr,SHADERPROGRAM_ID prog_o,int32_t i_sub);
WAFA wbool render_has_program(render_manager_t* const rendermngr,const char* program_id);
WAFA SHADERPROGRAM_ID render_get_program(render_manager_t* const rendermngr,const char* program_id);
WAFA wbool render_delete_program(render_manager_t* const rendermngr,const char* program_id);
WAFA SHADERPROGRAM_ID render_get_emptyprogvalue();
WAFA VBO_ID render_get_emptybuffervalue();
WAFA wbool render_has_passparam(render_manager_t* const rendermngr);
WAFA void render_setshader_attribute_float(render_manager_t* const rendermngr,SHADERPROGRAM_ID prog_id,const char* name,int32_t size,int32_t stride,void* offset);
WAFA void render_setshader_attribute_uint(render_manager_t* const rendermngr, SHADERPROGRAM_ID prog_id, const char* name, int32_t size, int32_t stride, void* offset);
WAFA void render_setshader_attribute_ubyte(render_manager_t* const rendermngr, SHADERPROGRAM_ID prog_id, const char* name, int32_t size, int32_t stride, void* offset, wbool normalize);
WAFA void render_setuniform_int(render_manager_t* const rendermngr,const char* uniform_name,int value);
WAFA void render_setuniform_float(render_manager_t* const rendermngr,const char* uniform_name,float value);
WAFA void render_setuniform_float2(render_manager_t* const rendermngr,const char* uniform_name,float vector[2]);
WAFA void render_setuniform_float3(render_manager_t* const rendermngr,const char* uniform_name,float vector[3]);
WAFA void render_setuniform_float4(render_manager_t* const rendermngr,const char* uniform_name,float vector[4]);
WAFA void render_setuniform_arrayfloat(render_manager_t* const rendermngr, const char* uniform_name, float* data, int32_t count);
WAFA void render_setuniform_arrayfloat3(render_manager_t* const rendermngr,const char* uniform_name,float* data,int32_t count);
WAFA void render_setuniform_arrayfloat4(render_manager_t* const rendermngr,const char* uniform_name,float* data,int32_t count);
WAFA void render_setuniform_matrix4(render_manager_t* const rendermngr, const char* uniform_name, float matrix[16]);
WAFA void render_bindattribute_floatVBO(render_manager_t* const render_mngr,const char* attribute_name,VBO_ID vbo,size_t size_element);

WAFA VAO_ID render_gen_vao(render_manager_t* const rendermngr);
WAFA void render_bind_vao(render_manager_t* const rendermngr,VAO_ID vao);
WAFA void render_unbind_vao(render_manager_t* const rendermngr);
WAFA void render_delete_vao(render_manager_t* const rendermngr,VAO_ID vao);

WAFA FBO_ID render_alloc_fbo(render_manager_t* const rendermngr);
WAFA RBO_ID render_alloc_rbo(render_manager_t* const rendermngr);
WAFA TEXTURE_ID render_alloc_texture(render_manager_t* const rendermngr);
WAFA void render_create_fbo(render_manager_t* const rendermngr,FBO_ID fbo_id,TEXTURE_ID fbo_texture,int16_t fbo_width,int16_t fbo_height);
WAFA void render_bind_framebuffer(render_manager_t* const rendermngr,FBO_ID framebuffer,TEXTURE_ID texture,int16_t fbo_width,int16_t fbo_height);
WAFA void render_unbind_framebuffer(render_manager_t* const rendermngr,FBO_ID framebuffer);
WAFA void render_delete_fbo(render_manager_t* const rendermngr,FBO_ID framebuffer);
WAFA void render_delete_rbo(render_manager_t* const rendermngr,RBO_ID renderbuffer);

WAFA void render_bindtexture_tosampler(render_manager_t* const rendermngr,TEXTURE_ID texture,const char* sampler_id,int32_t texture_unit_id,wbool is_3d);
WAFA void render_unbindtexture_tosampler(render_manager_t* const rendermngr,TEXTURE_ID texture,const char* sampler_id,int32_t texture_unit_id,wbool is_3d);

WAFA const char* render_get_error(render_manager_t* const render_mngr);

#endif