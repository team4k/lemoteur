#ifndef __RENDER_SHADER_UTILS_H__

#define __RENDER_SHADER_UTILS_H__ 1

WAFA void bind_shaders_params(light_manager_t* light_mngr,render_manager_t* const render_mngr, ShadersParameters** const params,size_t num_params,resources_manager_t* const resx_mngr,const Vector_t* const obj_pos,const Vector_t* const mirror_val,float rotation_value,Vector_t offset_coords,light_t** light_arr,int32_t num_a_light);

WAFA void unbind_shaders_params(render_manager_t* const render_mngr, ShadersParameters** const params,size_t num_params,resources_manager_t* const resx_mngr,light_manager_t* light_mngr);

#endif // !__RENDER_SHADER_UTILS_H__