#if  defined(RENDERER_OPENGL) || defined(RENDERER_OPENGL_ES)

void  glMatrix4x4Identityf(GLfloat matrix[16]);

void  glMatrix3x3Identityf(GLfloat matrix[9]);

void  glMatrix2x2Identityf(GLfloat matrix[4]);

void  glMatrix4x4Initf(GLfloat matrix[16], const GLfloat column0[4], const GLfloat column1[4], const GLfloat column2[4], const GLfloat column3[4]);

void  glMatrix3x3Initf(GLfloat matrix[9], const GLfloat column0[3], const GLfloat column1[3], const GLfloat column2[3]);

void  glMatrix2x2Initf(GLfloat matrix[4], const GLfloat column0[2], const GLfloat column1[2]);

void  glMatrix4x4Copyf(GLfloat matrix[16], const GLfloat source[16], const wbool rotationOnly);

void  glMatrix3x3Copyf(GLfloat matrix[9], const GLfloat source[9], const wbool rotationOnly);

void  glMatrix2x2Copyf(GLfloat matrix[4], const GLfloat source[4]);

void  glMatrix4x4ExtractMatrix3x3f(GLfloat matrix[9], const GLfloat source[16]);

void  glMatrix4x4ExtractMatrix2x2f(GLfloat matrix[4], const GLfloat source[16]);

void  glMatrix3x3ExtractMatrix2x2f(GLfloat matrix[4], const GLfloat source[9]);

void  glMatrix3x3CreateMatrix4x4f(GLfloat matrix[16], const GLfloat source[9]);

void  glMatrix2x2CreateMatrix4x4f(GLfloat matrix[16], const GLfloat source[4]);

void  glMatrix2x2CreateMatrix3x3f(GLfloat matrix[9], const GLfloat source[4]);

void  glMatrix4x4Addf(GLfloat matrix[16], const GLfloat matrix0[16], const GLfloat matrix1[16]);

void  glMatrix3x3Addf(GLfloat matrix[9], const GLfloat matrix0[9], const GLfloat matrix1[9]);

void  glMatrix2x2Addf(GLfloat matrix[4], const GLfloat matrix0[4], const GLfloat matrix1[4]);

void  glMatrix4x4Subtractf(GLfloat matrix[16], const GLfloat matrix0[16], const GLfloat matrix1[16]);

void  glMatrix3x3Subtractf(GLfloat matrix[9], const GLfloat matrix0[9], const GLfloat matrix1[9]);

void  glMatrix2x2Subtractf(GLfloat matrix[4], const GLfloat matrix0[4], const GLfloat matrix1[4]);

void  glMatrix2x2Multiplyf(GLfloat matrix[4], const GLfloat matrix0[4], const GLfloat matrix1[4]);

void  glMatrix3x3Multiplyf(GLfloat matrix[9], const GLfloat matrix0[9], const GLfloat matrix1[9]);

void glMatrix4x4Multiplyf(GLfloat matrix[16], const GLfloat matrix0[16], const GLfloat matrix1[16]);

//void glMatrix4x4Multiplyf3(GLfloat matrix[16], const GLfloat matrix0[16], const GLfloat matrix1[16],const GLfloat matrix2[16]);

GLfloat  glMatrix4x4Determinantf(const GLfloat matrix[16]);

GLfloat  glMatrix3x3Determinantf(const GLfloat matrix[9]);

GLfloat  glMatrix2x2Determinantf(const GLfloat matrix[4]);

wbool  glMatrix4x4Inversef(GLfloat matrix[16]);

wbool  glMatrix3x3Inversef(GLfloat matrix[9]);

wbool  glMatrix2x2Inversef(GLfloat matrix[4]);


void  glMatrix4x4InverseRigidBodyf(GLfloat matrix[16]);

void  glMatrix3x3InverseRigidBodyf(GLfloat matrix[9], const wbool is2D);

void  glMatrix2x2InverseRigidBodyf(GLfloat matrix[4]);

void  glMatrix4x4Transposef(GLfloat matrix[16]);

void  glMatrix3x3Transposef(GLfloat matrix[9]);

void  glMatrix2x2Transposef(GLfloat matrix[4]);

void  glMatrix4x4Translatef(GLfloat matrix[16], const GLfloat x, const GLfloat y, const GLfloat z);

void  glMatrix3x3Translatef(GLfloat matrix[9], const GLfloat x, const GLfloat y);

void  glMatrix4x4Rotatef(GLfloat matrix[16], const GLfloat angle, const GLfloat x, const GLfloat y, const GLfloat z);

void  glMatrix3x3Rotatef(GLfloat matrix[9], const GLfloat angle, const GLfloat x, const GLfloat y, const GLfloat z);

void  glMatrix2x2Rotatef(GLfloat matrix[4], const GLfloat angle);

void  glMatrix4x4RotateRxf(GLfloat matrix[16], const GLfloat angle);

void  glMatrix3x3RotateRxf(GLfloat matrix[9], const GLfloat angle);

void  glMatrix4x4RotateRyf(GLfloat matrix[16], const GLfloat angle);

void  glMatrix3x3RotateRyf(GLfloat matrix[9], const GLfloat angle);

void  glMatrix4x4RotateRzf(GLfloat matrix[16], const GLfloat angle);

void  glMatrix3x3RotateRzf(GLfloat matrix[9], const GLfloat angle);

void  glMatrix4x4RotateRzRxRyf(GLfloat matrix[16], const GLfloat anglez, const GLfloat anglex, const GLfloat angley);

void  glMatrix4x4RotateRzRyRxf(GLfloat matrix[16], const GLfloat anglez, const GLfloat angley, const GLfloat anglex);

void  glMatrix3x3RotateRzRxRyf(GLfloat matrix[9], const GLfloat anglez, const GLfloat anglex, const GLfloat angley);

void  glMatrix3x3RotateRzRyRxf(GLfloat matrix[9], const GLfloat anglez, const GLfloat angley, const GLfloat anglex);

void  glMatrix4x4Scalef(GLfloat matrix[16], const GLfloat x, const GLfloat y, const GLfloat z);

void  glMatrix3x3Scalef(GLfloat matrix[9], const GLfloat x, const GLfloat y, const GLfloat z);

void  glMatrix2x2Scalef(GLfloat matrix[4], const GLfloat x, const GLfloat y);

void  glMatrix4x4Shearf(GLfloat matrix[16], const GLfloat shxy, const GLfloat shxz, const GLfloat shyx, const GLfloat shyz, const GLfloat shzx, const GLfloat shzy);

void  glMatrix3x3Shearf(GLfloat matrix[9], const GLfloat shxy, const GLfloat shxz, const GLfloat shyx, const GLfloat shyz, const GLfloat shzx, const GLfloat shzy);

void  glMatrix2x2Shearf(GLfloat matrix[4], const GLfloat shx, const GLfloat shy);

void  glMatrix4x4MultiplyVector3f(GLfloat result[3], const GLfloat matrix[16], const GLfloat vector[3]);

void  glMatrix4x4MultiplyVector2f(GLfloat result[2], const GLfloat matrix[16], const GLfloat vector[2]);

void  glMatrix3x3MultiplyVector3f(GLfloat result[3], const GLfloat matrix[9], const GLfloat vector[3]);

void  glMatrix3x3MultiplyVector2f(GLfloat result[2], const GLfloat matrix[9], const GLfloat vector[2]);

void  glMatrix2x2MultiplyVector2f(GLfloat result[2], const GLfloat matrix[4], const GLfloat vector[2]);

void  glMatrix4x4MultiplyPoint4f(GLfloat result[4], const GLfloat matrix[16], const GLfloat point[4]);

void  glMatrix3x3MultiplyPoint3f(GLfloat result[3], const GLfloat matrix[9], const GLfloat point[3]);

void  glMatrix4x4MultiplyPlanef(GLfloat result[4], const GLfloat matrix[16], const GLfloat plane[4]);


void  glMatrix4x4GetEulerRzRxRyf(GLfloat angles[3], const GLfloat matrix[16]);

void  glMatrix4x4GetEulerRzRyRxf(GLfloat angles[3], const GLfloat matrix[16]);

void  glMatrix3x3GetEulerRzRxRyf(GLfloat angles[3], const GLfloat matrix[9]);

void  glMatrix3x3GetEulerRzRyRxf(GLfloat angles[3], const GLfloat matrix[9]);

void  glMatrix3x3GetAnglef(GLfloat* angle, const GLfloat matrix[9]);

void  glMatrix2x2GetAnglef(GLfloat* angle, const GLfloat matrix[4]);

void  glMatrix4x4GetScale(GLfloat scales[3], const GLfloat matrix[16]);

void  glMatrix3x3GetScale(GLfloat scales[3], const GLfloat matrix[9]);

void  glMatrix2x2GetScale(GLfloat scales[2], const GLfloat matrix[4]);

void  glMatrix4x4GetTranslate(GLfloat translates[3], const GLfloat matrix[16]);

void  glMatrix3x3GetTranslate(GLfloat translates[2], const GLfloat matrix[9]);

#endif