#if defined WITH_NETWORK_CLIENT
typedef struct
{
	int cl_handle;
	struct sockaddr_in address;
	struct hostent *host_nfo;
	struct sockaddr_in srv_addr;
	
	#if defined _WIN32
		WSADATA WSAData;
	#endif
	
	int sock_handle;
	char data_buffer[1024];//a buffer of 1Ko for all our message, we can't have a bigger network message
} client_t;



WAFA int client_connect(client_t* const client,char* ipaddress, unsigned short port);
WAFA size_t client_rcv(client_t* const client);
WAFA void client_send(client_t* const client,char* buffertosend);
WAFA void client_disconnect(client_t* const client);
#endif