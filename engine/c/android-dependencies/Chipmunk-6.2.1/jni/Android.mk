LOCAL_HOME := $(call my-dir) 
LOCAL_PATH := $(call my-dir)


#build chipmunk 
include $(CLEAR_VARS) 

LOCAL_MODULE := chipmunk
# using floats for convenience: GL_DOUBLE is not available on opengl es
# allowing private access for chipmunk-draw library
LOCAL_CFLAGS := -std=c99 -DCP_USE_DOUBLES=0 -DCP_ALLOW_PRIVATE_ACCESS=1 -DNDEBUG -ffast-math
LOCAL_EXPORT_CFLAGS := -DCP_USE_DOUBLES=0 -DCP_ALLOW_PRIVATE_ACCESS=1 -DNDEBUG -ffast-math
LOCAL_C_INCLUDES := $(LOCAL_PATH)/../include/chipmunk
-DCP_ALLOW_PRIVATE_ACCESS=1
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/../../include/chipmunk
LOCAL_SRC_FILES := ../src/chipmunk.c \
../src/cpBody.c \
../src/cpSpace.c \
../src/cpSpaceStep.c \
../src/cpBBTree.c \
../src/cpHashSet.c \
../src/cpShape.c \
../src/cpBB.c \
../src/cpCollision.c \
../src/cpSweep1D.c \
../src/cpPolyShape.c \
../src/cpSpaceQuery.c \
../src/cpVect.c \
../src/cpArray.c \
../src/cpArbiter.c \
../src/cpSpaceComponent.c \
../src/cpSpaceHash.c \
../src/cpSpatialIndex.c \
../src/constraints/cpPinJoint.c \
../src/constraints/cpGearJoint.c \
../src/constraints/cpSlideJoint.c \
../src/constraints/cpRotaryLimitJoint.c \
../src/constraints/cpRatchetJoint.c \
../src/constraints/cpConstraint.c \
../src/constraints/cpSimpleMotor.c \
../src/constraints/cpGrooveJoint.c \
../src/constraints/cpDampedSpring.c \
../src/constraints/cpPivotJoint.c \
../src/constraints/cpDampedRotarySpring.c
 
include $(BUILD_STATIC_LIBRARY)
