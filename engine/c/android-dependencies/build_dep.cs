using System;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;

public class EntryPoint
{
    static void Main(string[] args)
    {
        string host="armeabi-v7a";
		
        Console.WriteLine("Build Android dependencies");
		//build dependencies
		foreach(string dep in Directory.GetDirectories(AppDomain.CurrentDomain.BaseDirectory))
        {
            DirectoryInfo inf = new DirectoryInfo(dep);
            
            if(inf.Name == "old")
                continue;
            
            Console.WriteLine("Build " + inf.Name);
            ProcessStartInfo startinfo = new ProcessStartInfo("cmd.exe",@"/c D:\devel\common\android-ndk\ndk-build.cmd -C " + dep);
            startinfo.UseShellExecute = false;
            startinfo.RedirectStandardOutput = true;
            Process build_proc = Process.Start(startinfo);
            build_proc.OutputDataReceived += (sender, e) => Console.WriteLine(e.Data);
            build_proc.BeginOutputReadLine();
            build_proc.WaitForExit();

            //copy output file if it exist
            List<string> libList = new List<string>();
            string libpath = dep + "\\obj\\local\\"+host;
            string dllPath = dep + "\\libs\\" + host;

            if(Directory.Exists(libpath))
                libList.AddRange(Directory.GetFiles(libpath,"*.a"));

           if(Directory.Exists(dllPath))
               libList.AddRange(Directory.GetFiles(dllPath, "*.so"));

            foreach (string file in libList)
            {
                FileInfo inf2 = new FileInfo(file);
                File.Copy(file, AppDomain.CurrentDomain.BaseDirectory + "\\" + inf2.Name,true);
            }
        }

        Console.WriteLine("Finished Building Android dependencies");
        Console.ReadLine();
	}
}