LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := protobuf-c
LOCAL_C_INCLUDES := $(LOCAL_PATH)/../src/google/protobuf-c
LOCAL_SRC_FILES := ../src/google/protobuf-c/protobuf-c.c ../src/google/protobuf-c/protobuf-c-data-buffer.c ../src/google/protobuf-c/protobuf-c-dispatch.c ../src/google/protobuf-c/protobuf-c-rpc.c

LOCAL_CFLAGS := -DHAVE_PROTOBUF_C_CONFIG_H=1

include $(BUILD_STATIC_LIBRARY)
