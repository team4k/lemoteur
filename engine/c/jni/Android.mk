#set prebuild libraries
LOCAL_PATH := $(call my-dir)

#libpng
include $(CLEAR_VARS)
LOCAL_MODULE := png
LOCAL_SRC_FILES := ../android-dependencies/libpng.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/../android-dependencies/libpng/jni
include $(PREBUILT_STATIC_LIBRARY)

#zlib
include $(CLEAR_VARS)
LOCAL_MODULE := zlib
LOCAL_SRC_FILES := ../android-dependencies/libzlib.a
include $(PREBUILT_STATIC_LIBRARY)

#liblua
include $(CLEAR_VARS)
LOCAL_MODULE := lua
LOCAL_SRC_FILES := ../android-dependencies/liblua.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/../android-dependencies/lua-5.3.1/src
include $(PREBUILT_STATIC_LIBRARY)

#libfreetype
include $(CLEAR_VARS)
LOCAL_MODULE := freetype
LOCAL_SRC_FILES := ../android-dependencies/libfreetype2-static.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/../android-dependencies/libfreetype/include
include $(PREBUILT_STATIC_LIBRARY)

#libprotobuf-c
include $(CLEAR_VARS)
LOCAL_MODULE := protobuf-c
LOCAL_SRC_FILES := ../android-dependencies/libprotobuf-c.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/../android-dependencies/protobuf-c-0.15/src
include $(PREBUILT_STATIC_LIBRARY)


#libchipmunk
include $(CLEAR_VARS)
LOCAL_MODULE := chipmunk
LOCAL_EXPORT_CFLAGS := -DCP_USE_DOUBLES=0 -DCP_ALLOW_PRIVATE_ACCESS=1 -DNDEBUG=1
LOCAL_SRC_FILES := ../android-dependencies/libchipmunk.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/../android-dependencies/Chipmunk-6.2.1/include

include $(PREBUILT_STATIC_LIBRARY)

#libzip
include $(CLEAR_VARS)
LOCAL_MODULE := libzip
LOCAL_SRC_FILES := ../android-dependencies/libzip.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/../android-dependencies/libzip/jni
include $(PREBUILT_STATIC_LIBRARY)

#libsndfile
include $(CLEAR_VARS)
LOCAL_MODULE := sndfile
LOCAL_SRC_FILES := ../android-dependencies/libsndfile.so
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/../android-dependencies/libsndfile/jni
include $(PREBUILT_SHARED_LIBRARY)

#libportaudio
include $(CLEAR_VARS)
LOCAL_MODULE := portaudio
LOCAL_EXPORT_CFLAGS := -DPA_LITTLE_ENDIAN -DPA_USE_OPENSLES
LOCAL_SRC_FILES := ../android-dependencies/libportaudio.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/../android-dependencies/portaudio_android/include
include $(PREBUILT_STATIC_LIBRARY)

#libsqlite
include $(CLEAR_VARS)
LOCAL_MODULE := sqlite
LOCAL_SRC_FILES := ../android-dependencies/libsqlite.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/../android-dependencies/sqlite3/include
include $(PREBUILT_STATIC_LIBRARY)

#build wafa engine

include $(CLEAR_VARS)
LOCAL_MODULE    := wafaengine
LOCAL_C_INCLUDES := $(LOCAL_PATH)/../include
LOCAL_STATIC_LIBRARIES := png lua freetype protobuf-c zlib libzip chipmunk sndfile portaudio sqlite
LOCAL_SRC_FILES := ../src/Base/functions.c ../src/Base/geom.c ../src/Base/geom3D.c ../src/Collisions/Collisions.c ../src/Config/config.c ../src/Config/shader_config.c ../src/Debug/geomdebug.c ../src/Debug/Logprint.c ../src/Font/RenderText.c ../src/GameObjects/Levelobject.c ../src/GameMngr/module_mngr.c ../src/GameMngr/save_mngr.c ../src/GameObjects/Trigger.c ../src/GameObjects/entity.c ../src/GameObjects/text_entity.c ../src/GameObjects/Characters.pb-c.c ../src/GameObjects/Level.pb-c.c ../src/GameObjects/Tilemap.c ../src/Graphics/SpriteBatch.c ../src/Graphics/TextureRegion.c ../src/Graphics/Animation.c ../src/Graphics/compound_sprite.c ../src/Graphics/Sprite.c ../src/Graphics/TextureLoader.c ../src/Graphics/FontLoader.c ../src/Graphics/TilemapLayer.c ../src/Graphics/Effects/screen_fill.c ../src/Physics/convert.c ../src/Physics/Physics.c ../src/Physics/Effects/shatter_effect.c ../src/Resx/Resources.c ../src/Scripting/ScriptEngine.c ../src/Scripting/ScriptLoader.c ../src/Sound/snd_mngr.c ../src/Utils/Hash.c ../src/Engine.c ../src/Resx/localization.c ../src/Render/render_gl_math.c ../src/Render/render_gl_matrix.c ../src/Render/render_gl_viewprojection.c ../src/Graphics/Effects/screen_fade.c ../src/Render/render_manager_gl.c ../src/Utils/display_list.c ../src/Physics/chipmunk_wrap.c ../src/Scripting/lua_wrap.c ../src/Resx/shader_loader.c ../src/GameObjects/entity_group.c ../src/Graphics/Light.c ../src/Graphics/geomdraw.c ../src/Graphics/LightManager.c ../src/Utils/url.c ../src/Utils/base64.c ../src/Pathfinding/AStar.c ../src/Pathfinding/IndexPriorityQueue.c ../src/Pathfinding/pathfind_worker.c ../src/Threading/thread_func.c ../src/Utils/hashmap.c ../src/Utils/random.c ../src/Utils/tinymt32.c ../src/Utils/tinymt64.c ../src/Physics/physics_utils.c ../src/Render/render_shader_utils.c ../src/Graphics/Mesh.c ../src/GameObjects/Shaders.pb-c.c ../src/Utils/alloc_utils.c ../src/GameObjects/particles_entity.c ../src/Scripting/AI_scripting.c ../src/Scripting/effects_scripting.c ../src/Scripting/entities_scripting.c ../src/Scripting/game_scripting.c ../src/Scripting/light_scripting.c ../src/Scripting/render_scripting.c ../src/Scripting/sound_scripting.c ../src/Scripting/ScriptUtils.c ../src/Sound/sound.c ../src/Graphics/Effects/particle_object.c

LOCAL_CFLAGS := -std=c99 -DRENDERER_OPENGL_ES -DSTRIP_EDIT_CODE -DANDROID_BUILD -DCP_ALLOW_PRIVATE_ACCESS=1 -DNDEBUG -DANDROID_NDK
LOCAL_LDLIBS := -llog -lOpenSLES -lEGL -landroid -lGLESv2


include $(BUILD_SHARED_LIBRARY)

