@SETLOCAL EnableDelayedExpansion
@ECHO OFF
@for %%x in (*.proto) do (
	@set file=%%x
	@set file=!file:.proto=!
	
	@protogen --csharp_out=..\..\..\editor\WafaEditorV2\ %%x 
	
	@protoc.exe --c_out=. --c_opt=base_include=Engine,export_func=WAFA,include_folder=GameObjects %%x 
	
	@move /Y !file!.pb-c.c ..\src\GameObjects\!file!.pb-c.c
	@move /Y !file!.pb-c.h ..\include\GameObjects\!file!.pb-c.h
	
	REM gen php code from proto definition
	REM @php.exe protoc-php.php %%x
)
@ECHO ON
@endlocal 
pause