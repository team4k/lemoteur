project(WafaEngine)
cmake_minimum_required ( VERSION 3.1 )

file(GLOB_RECURSE wafa_src_files "src/*.c")

if(EMSCRIPTEN)
	list(REMOVE_ITEM wafa_src_files "src/Pathfinding/IndexPriorityQueue.c")
	list(REMOVE_ITEM wafa_src_files "src/Pathfinding/AStar.c")
endif()

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_CURRENT_SOURCE_DIR}/cmake_modules) 

#add include directory
include_directories("include" "../../dependencies/output/include/Freetype" "../../dependencies/output/include/libpng" "../../dependencies/output/include/lua" "../../dependencies/output/include/libsndfile" "../../dependencies/output/include/libzip" "../../dependencies/output/include/protobuf_c")

option(WITH_CURL "build with curl support" OFF)

option(USE_D3D "build with direct3D renderer" OFF)
#by default, use opengl
option(USE_OPENGL "build with opengl renderer" ON)


#for non mobile and non web platforms we use glfw and glew
if(NOT ANDROID AND NOT EMSCRIPTEN)
	if(MSVC)
		include_directories("../../dependencies/output/include/glewwin")
	else()
		include_directories("../../dependencies/output/include/glew")
	endif()
	
	include_directories("../../dependencies/output/include/glfw")
    
    list(APPEND PREDEF USE_GLFW_INPUT)
	list(APPEND PREDEF USE_GLFW_RENDER)
endif()

include_directories("../../dependencies/output/include/sqlite")

#different chipmunk version between emscripten and other platforms
if(EMSCRIPTEN)
	include_directories("../../dependencies/output/include/ChipmunkEmscripten")
else()
	include_directories("../../dependencies/output/include/Chipmunk")
endif()


#curl support
if(WITH_CURL)
	list(APPEND PREDEF WITH_CURL)
endif()

#use specific msvc headers
if(MSVC)
	include_directories("include-msvc")
    add_definitions(-DUNICODE -D_UNICODE)
	
	#if((MSVC_VERSION GREATER 1800) OR (MSVC_VERSION EQUAL 1800))
	#	add_definitions(-D_XKEYCHECK_H)
	#endif()
	#enable compiler warning C4296 in warnlevel 3
	#set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} /w34005 ")
	#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /w34005 ")
	
	
    list(APPEND PREDEF __STDC_CONSTANT_MACROS)
	
	link_directories("C:/Program Files (x86)/Microsoft SDKs/Windows/v7.1A/Lib/")
	link_directories("C:/Program Files (x86)/Microsoft SDKs/Windows/v7.0A/Lib/")
endif()


#build a shared library for all platform except emscripten and iOS
if(EMSCRIPTEN OR IOS)
    add_library(WafaEngine ${wafa_src_files})
else()
    add_library(WafaEngine SHARED ${wafa_src_files})
endif()

option (USE_DEBUG_DEPEND "Use debug version of dependencies libraries" OFF )

if(USE_DEBUG_DEPEND)
    set(CMAKE_BUILD_TYPE Debug)
endif()

#set the library root path
if(WIN32)
    if(USE_DEBUG_DEPEND)
        set(wafa_lib_root_debug "../../dependencies/output/libs-msvc-debug")
    endif()

	set( wafa_lib_root "../../dependencies/output/libs-msvc")
endif()

if(ANDROID)
	set(wafa_lib_root "../../dependencies/output/libs-android")
    list(APPEND PREDEF ANDROID_BUILD)
    list(APPEND PREDEF ANDROID_NDK)
    list(APPEND PREDEF USE_MOBILE_INPUT)
    
    
    #set android specific libraries
     list(APPEND LIBS_OBJ log )
     list(APPEND LIBS_OBJ android)
endif()

if(EMSCRIPTEN)
	set(wafa_lib_root "../../dependencies/output/libs-emscripten")
    
    list(APPEND PREDEF USE_SDL_INPUT)
	list(APPEND PREDEF USE_SDL_RENDER)
	
	#use double for math type
	list(APPEND PREDEF MATH_DOUBLE)
endif()

if(APPLE)
    if(IOS)
        set(wafa_lib_root "../../dependencies/output/libs-ios")
        list(APPEND PREDEF USE_MOBILE_INPUT)
    else()
        set(wafa_lib_root "../../dependencies/output/libs-macosx")
    endif()
endif()

if(UNIX AND NOT APPLE AND NOT ANDROID AND NOT EMSCRIPTEN)
	set(wafa_lib_root "../../dependencies/output/libs-linux")
    if(USE_DEBUG_DEPEND)
        set(wafa_lib_root_debug "../../dependencies/output/libs-linux-debug")
    endif()
endif()


#use opengl on all platforms
if(USE_OPENGL)
	
	if(ANDROID OR EMSCRIPTEN OR IOS)
       if(ANDROID)
        list(APPEND LIBS_OBJ EGL)
        list(APPEND LIBS_OBJ GLESv2)
       endif()
        
    
		#set_target_properties(WafaEngine PROPERTIES COMPILE_DEFINITIONS RENDERER_OPENGL_ES)
        list(APPEND PREDEF RENDERER_OPENGL_ES)
	else()
        find_package(OpenGL REQUIRED)
        
        list(APPEND LIBS_OBJ ${OPENGL_gl_LIBRARY}) 
		#set_target_properties(WafaEngine PROPERTIES COMPILE_DEFINITIONS RENDERER_OPENGL)
        #set_target_properties(WafaEngine PROPERTIES COMPILE_DEFINITIONS GLFW_DLL)
        list(APPEND PREDEF RENDERER_OPENGL)
        list(APPEND PREDEF GLFW_DLL)
		
		FIND_LIBRARY(GLFW_LIB NAMES glfw glfw3 glfw3dll PATHS ${wafa_lib_root} NO_DEFAULT_PATH)
		FIND_LIBRARY(GLEW_LIB NAMES libGLEW GLEW glew glew32 PATHS ${wafa_lib_root} NO_DEFAULT_PATH)
        
        if(USE_DEBUG_DEPEND)
            #debug version
            FIND_LIBRARY(GLFW_D_LIB NAMES glfw glfw3 glfw3dll PATHS ${wafa_lib_root_debug} NO_DEFAULT_PATH)
            FIND_LIBRARY(GLEW_D_LIB NAMES libGLEW GLEW glew glew32 PATHS ${wafa_lib_root_debug} NO_DEFAULT_PATH)
            list(APPEND LIBS_OBJ debug)
            list(APPEND LIBS_OBJ ${OPENGL_glu_LIBRARY})
            list(APPEND LIBS_OBJ debug)
            list(APPEND LIBS_OBJ ${GLEW_D_LIB})
            list(APPEND LIBS_OBJ debug)
            list(APPEND LIBS_OBJ ${GLFW_D_LIB})
         endif()
        
		#FIND_LIBRARY(GLU_LIB glu ${wafa_lib_root})

        list(APPEND LIBS_OBJ ${OPENGL_glu_LIBRARY})
        if(USE_DEBUG_DEPEND)
            list(APPEND LIBS_OBJ optimized)
        endif()
        list(APPEND LIBS_OBJ ${GLFW_LIB})
        if(USE_DEBUG_DEPEND)
            list(APPEND LIBS_OBJ optimized)
        endif()
        list(APPEND LIBS_OBJ ${GLEW_LIB})
	endif()
	
endif()

if(USE_D3D)
   list(APPEND PREDEF RENDERER_DX)
   
   #temporary hard set the directx SDK, TODO use FindDirectX.cmake
	include_directories("D:/devel/common/Microsoft DirectX SDK (June 2010)/Include")
	list(APPEND PREDEF STRIP_EDIT_CODE)
	set(DX_ROOT "D:/devel/common/Microsoft DirectX SDK (June 2010)/Lib/x86")
	FIND_LIBRARY(D3DLIB NAMES d3d11 PATHS ${DX_ROOT} NO_DEFAULT_PATH)
	FIND_LIBRARY(D3DX11LIB NAMES d3dx11 PATHS ${DX_ROOT} NO_DEFAULT_PATH)
	FIND_LIBRARY(D3DX10LIB NAMES d3dx10 PATHS ${DX_ROOT} NO_DEFAULT_PATH)

	list(APPEND LIBS_OBJ ${D3DLIB})
	list(APPEND LIBS_OBJ ${D3DX11LIB})
	list(APPEND LIBS_OBJ ${D3DX10LIB})
   
endif()


#strip editor code under all platforms but msvc (for now)
if(NOT MSVC)
    list(APPEND PREDEF STRIP_EDIT_CODE)
endif()

if(ANDROID)
	list(APPEND PREDEF CP_USE_DOUBLES=0)
	list(APPEND PREDEF CP_ALLOW_PRIVATE_ACCESS=1)
endif()

#set_target_properties ( WafaEngine PROPERTIES COMPILE_DEFINITIONS ${PREDEF})
target_compile_definitions(WafaEngine PUBLIC ${PREDEF})

#under MSVC compile as c++
if(MSVC)
    set_source_files_properties(${wafa_src_files} PROPERTIES LANGUAGE CXX)
    set_target_properties(WafaEngine PROPERTIES LINKER_LANGUAGE CXX)
    
    #generate a map file used for unhandled exception reports
	target_link_options(WafaEngine PRIVATE "/MAP")
	
	#work around to fix LNK1104 error VC bug with map generation
	#set_target_properties(WafaEngine PROPERTIES LINK_FLAGS "/INCREMENTAL:NO")
	
	if(USE_DEBUG_DEPEND)
		set_target_properties(WafaEngine PROPERTIES LINK_FLAGS "/NODEFAULTLIB:msvcrt.lib")
	else()
		set_target_properties(WafaEngine PROPERTIES LINK_FLAGS "/NODEFAULTLIB:msvcrtd.lib")
	endif()
	
	#generate a pdb file for all configs
	set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} /Zi")
	set(CMAKE_SHARED_LINKER_FLAGS_RELEASE "${CMAKE_SHARED_LINKER_FLAGS_RELEASE} /DEBUG /OPT:REF /OPT:ICF /NODEFAULTLIB:libcmt.lib /NODEFAULTLIB:libcmtd.lib")
	set(CMAKE_SHARED_LINKER_FLAGS_DEBUG "${CMAKE_SHARED_LINKER_FLAGS_DEBUG} /DEBUG /NODEFAULTLIB:libcmt.lib /NODEFAULTLIB:libcmtd.lib")
	 
else()
    #other platform, compile as c99
    #set_property(TARGET WafaEngine PROPERTY C_STANDARD 99)

	
	
	if(ANDROID)
		set (CMAKE_C_FLAGS "-std=c99 ${CMAKE_C_FLAGS}")
	elseif(EMSCRIPTEN) #emscripten in version 1.38 doesn't like std=c99 with em_asm macro
		set (CMAKE_C_FLAGS "-std=gnu99 ${CMAKE_C_FLAGS}")
	else()
		#for some reason, the previous line doesn't work with cmake 3.1.0 on ubuntu even though it's referenced in the documentation 
		set (CMAKE_C_FLAGS "--std=c99 ${CMAKE_C_FLAGS}")
	endif()
    #set math library
    list(APPEND LIBS_OBJ m)
	#set uuid library (used to generate guid)
	list(APPEND LIBS_OBJ uuid)
endif()



#android, emscripten and IOS use custom cmake toolchain, set the path directly under these platforms
if(ANDROID OR EMSCRIPTEN OR IOS)
     get_filename_component(SQLITE_LIB ${wafa_lib_root}/libsqlite3-static${CMAKE_STATIC_LIBRARY_SUFFIX} ABSOLUTE)
     get_filename_component(ZIP_LIB ${wafa_lib_root}/libzip${CMAKE_STATIC_LIBRARY_SUFFIX} ABSOLUTE)
     get_filename_component(LUA_LIB ${wafa_lib_root}/liblua${CMAKE_STATIC_LIBRARY_SUFFIX} ABSOLUTE)
     get_filename_component(FREETYPE_LIB ${wafa_lib_root}/libfreetype${CMAKE_STATIC_LIBRARY_SUFFIX} ABSOLUTE)
     get_filename_component(CHIPMUNK_LIB ${wafa_lib_root}/libchipmunk${CMAKE_STATIC_LIBRARY_SUFFIX} ABSOLUTE)
     get_filename_component(PROTO_LIB ${wafa_lib_root}/libprotobuf-c${CMAKE_STATIC_LIBRARY_SUFFIX} ABSOLUTE)
	 get_filename_component(PNG_LIB ${wafa_lib_root}/libpng16${CMAKE_STATIC_LIBRARY_SUFFIX} ABSOLUTE)
     
     #android build use built-in zlib
	if(ANDROID)
		set(ZLIB_LIB z)
    else()
		get_filename_component(ZLIB_LIB ${wafa_lib_root}/libz.${CMAKE_STATIC_LIBRARY_SUFFIX} ABSOLUTE)
	endif()
     #no libsndfile for iOS
    if(ANDROID)
        get_filename_component(SNDFILE_LIB ${wafa_lib_root}/libsndfile.so ABSOLUTE)
    elseif(EMSCRIPTEN)
        get_filename_component(SNDFILE_LIB ${wafa_lib_root}/libsndfile${CMAKE_STATIC_LIBRARY_SUFFIX} ABSOLUTE)
    endif()
else()
    FIND_LIBRARY(SQLITE_LIB NAMES sqlite3 sqlite3-static PATHS ${wafa_lib_root} NO_DEFAULT_PATH)
    FIND_LIBRARY(ZLIB_LIB NAMES libz zlib zlibstatic z PATHS ${wafa_lib_root} NO_DEFAULT_PATH)
    FIND_LIBRARY(ZIP_LIB NAME zip PATHS ${wafa_lib_root} NO_DEFAULT_PATH)
    FIND_LIBRARY(LUA_LIB NAMES lua liblua PATHS ${wafa_lib_root} NO_DEFAULT_PATH)
    FIND_LIBRARY(FREETYPE_LIB NAMES freetype freetype2411 libfreetype PATHS ${wafa_lib_root} NO_DEFAULT_PATH)
    FIND_LIBRARY(CHIPMUNK_LIB NAMES chipmunk libchipmunk PATHS ${wafa_lib_root} NO_DEFAULT_PATH)
    FIND_LIBRARY(PROTO_LIB NAMES protobuf-c libprotobuf-c PATHS ${wafa_lib_root} NO_DEFAULT_PATH)
    FIND_LIBRARY(PNG_LIB NAMES libpng libpng16_static libpng16 png16 PATHS ${wafa_lib_root} NO_DEFAULT_PATH)
    FIND_LIBRARY(SNDFILE_LIB NAMES libsndfile sndfile PATHS ${wafa_lib_root} NO_DEFAULT_PATH)
endif()

if(USE_DEBUG_DEPEND)
    list(APPEND LIBS_OBJ optimized)
endif()
list(APPEND LIBS_OBJ ${LUA_LIB})
if(USE_DEBUG_DEPEND)
    list(APPEND LIBS_OBJ optimized)
endif()
list(APPEND LIBS_OBJ ${FREETYPE_LIB})
if(USE_DEBUG_DEPEND)
    list(APPEND LIBS_OBJ optimized)
endif()
list(APPEND LIBS_OBJ ${CHIPMUNK_LIB})
if(USE_DEBUG_DEPEND)
    list(APPEND LIBS_OBJ optimized)
endif()
list(APPEND LIBS_OBJ ${PROTO_LIB})
if(USE_DEBUG_DEPEND)
    list(APPEND LIBS_OBJ optimized)
endif()
list(APPEND LIBS_OBJ ${PNG_LIB})
if(USE_DEBUG_DEPEND)
    list(APPEND LIBS_OBJ optimized)
endif()
list(APPEND LIBS_OBJ ${SNDFILE_LIB})

if(USE_DEBUG_DEPEND)
    FIND_LIBRARY(LUA_D_LIB lua ${wafa_lib_root_debug})
    FIND_LIBRARY(FREETYPE_D_LIB NAMES freetype freetype2411_D PATHS ${wafa_lib_root_debug} NO_DEFAULT_PATH)
    FIND_LIBRARY(CHIPMUNK_D_LIB NAMES chipmunk PATHS ${wafa_lib_root_debug} NO_DEFAULT_PATH)
    FIND_LIBRARY(PROTO_D_LIB NAMES protobuf-c libprotobuf-c PATHS ${wafa_lib_root_debug} NO_DEFAULT_PATH)
    FIND_LIBRARY(PNG_D_LIB NAMES libpng libpng16_staticd libpng16_static libpng16 png16 PATHS ${wafa_lib_root_debug} NO_DEFAULT_PATH)
    FIND_LIBRARY(SNDFILE_D_LIB NAMES libsndfile sndfile PATHS ${wafa_lib_root_debug} NO_DEFAULT_PATH)

    list(APPEND LIBS_OBJ debug)
    list(APPEND LIBS_OBJ ${LUA_D_LIB})
    list(APPEND LIBS_OBJ debug)
    list(APPEND LIBS_OBJ ${FREETYPE_D_LIB})
    list(APPEND LIBS_OBJ debug)
    list(APPEND LIBS_OBJ ${CHIPMUNK_D_LIB})
    list(APPEND LIBS_OBJ debug)
    list(APPEND LIBS_OBJ ${PROTO_D_LIB})
    list(APPEND LIBS_OBJ debug)
    list(APPEND LIBS_OBJ ${PNG_D_LIB})
    list(APPEND LIBS_OBJ debug)
    list(APPEND LIBS_OBJ ${SNDFILE_D_LIB})
endif()


#emscripten use sdl_audio rather than portaudio
if(NOT EMSCRIPTEN)
    include_directories("../../dependencies/output/include/portaudio")
    
    if(ANDROID OR IOS)
        get_filename_component(PORTAUDIO_LIB ${wafa_lib_root}/libportaudio${CMAKE_STATIC_LIBRARY_SUFFIX} ABSOLUTE)
    else()
        FIND_LIBRARY(PORTAUDIO_LIB NAMES portaudio portaudio_static_x86 PATHS ${wafa_lib_root} NO_DEFAULT_PATH)
    endif()
    
    if(USE_DEBUG_DEPEND)
    list(APPEND LIBS_OBJ optimized)
    endif()
	
    list(APPEND LIBS_OBJ ${PORTAUDIO_LIB})
    
	
    if(USE_DEBUG_DEPEND)
        FIND_LIBRARY(PORTAUDIO_D_LIB NAMES portaudio portaudio_static_x86 PATHS ${wafa_lib_root_debug} NO_DEFAULT_PATH)
        list(APPEND LIBS_OBJ debug)
        list(APPEND LIBS_OBJ ${PORTAUDIO_D_LIB})
    endif()
else()
SET( CMAKE_C_FLAGS  "${CMAKE_C_FLAGS} -O2 -s USE_SDL=2" )
#set SDL 2 flag
#target_compile_options(WafaEngine PUBLIC " ")
endif()

#android use OpenSLES backend with portaudio
if(ANDROID)
    list(APPEND LIBS_OBJ OpenSLES)
	
	#debug definition
	#include(AndroidNdkGdb)
    #android_ndk_gdb_enable()
    #android_ndk_gdb_debuggable(WafaEngine) 
endif()


#add windows socket and rpc lib
if(MSVC)
	list(APPEND LIBS_OBJ WS2_32.lib)
	list(APPEND LIBS_OBJ Rpcrt4.lib)
endif()

#set Macosx/iOS specific framework to link against
if(APPLE)
FIND_LIBRARY(AUDIO_UNIT AudioUnit)
FIND_LIBRARY(AUDIO_TOOLBOX AudioToolbox)
FIND_LIBRARY(CORE_AUDIO CoreAudio)
FIND_LIBRARY(CORE_FOUNDATION CoreFoundation)

    if(NOT IOS)
        FIND_LIBRARY(CORE_SERVICES CoreServices)
        list(APPEND LIBS_OBJ ${CORE_SERVICES})
    endif()

list(APPEND LIBS_OBJ ${AUDIO_UNIT})
list(APPEND LIBS_OBJ ${AUDIO_TOOLBOX})
list(APPEND LIBS_OBJ ${CORE_AUDIO})
list(APPEND LIBS_OBJ ${CORE_FOUNDATION})

endif()




if(USE_DEBUG_DEPEND)
    list(APPEND LIBS_OBJ optimized)
endif()
list(APPEND LIBS_OBJ ${SQLITE_LIB})
if(USE_DEBUG_DEPEND)
    list(APPEND LIBS_OBJ optimized)
endif()
list(APPEND LIBS_OBJ ${ZIP_LIB})
if(USE_DEBUG_DEPEND)
    list(APPEND LIBS_OBJ optimized)
endif()
list(APPEND LIBS_OBJ ${ZLIB_LIB})


if(USE_DEBUG_DEPEND)
    FIND_LIBRARY(SQLITE_D_LIB NAMES sqlite3 sqlite3-static PATHS ${wafa_lib_root_debug} NO_DEFAULT_PATH)
    FIND_LIBRARY(ZIP_D_LIB NAMES zip PATHS ${wafa_lib_root_debug} NO_DEFAULT_PATH)
    FIND_LIBRARY(ZLIB_D_LIB NAMES libz zlib zlibstaticd zlibstatic z  PATHS ${wafa_lib_root_debug} NO_DEFAULT_PATH)
    list(APPEND LIBS_OBJ debug)
    list(APPEND LIBS_OBJ ${SQLITE_D_LIB})
    list(APPEND LIBS_OBJ debug)
    list(APPEND LIBS_OBJ ${ZIP_D_LIB})
    list(APPEND LIBS_OBJ debug)
    list(APPEND LIBS_OBJ ${ZLIB_D_LIB})
endif()

#this custom target use CMAKE touch to force the rebuild of the Engine.c file (which update the engine version string every build)
add_custom_target(invalidate_files ALL COMMAND ${CMAKE_COMMAND} -E touch ${PROJECT_SOURCE_DIR}/src/Engine.c)

#add a dependency on the invalidate_files target to our engine so the invalidation is done before any build
add_dependencies(WafaEngine invalidate_files)

target_link_libraries(WafaEngine ${LIBS_OBJ})

#separate astar building with emscripten
if(EMSCRIPTEN)
	execute_process(COMMAND "python" "emcc_build_worker_release.py" WORKING_DIRECTORY ${PROJECT_SOURCE_DIR})
endif()





