using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;


//compile with : csc reader.cs -platform:x86
class Reader
{
	public const string enginePath = "WafaEngine";
	
	[SuppressUnmanagedCodeSecurity]
	[DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
	public static extern IntPtr getbuildversion();
	
	public static void Main(string[] args)
	{
		if(args.Length == 0)
		{
			Console.WriteLine("No GameMain.c file path in parameter list, aborting...");
			return;
		}
		
		string gamemainfile = args[0];
		
		if(!File.Exists(args[0]))
		{
			Console.WriteLine("The GameMain.c file parameter doesn't exist on disk, aborting...");
			return;
		}
		
		string filecontent = File.ReadAllText(gamemainfile);
		
		string version = Marshal.PtrToStringAnsi(getbuildversion());
		
		string basestr = "#define ENGINEVERSION";
		
		int basei = 0;
		
		while(basei != -1)
		{
			basei = filecontent.IndexOf(basestr,basei);
			
			if(basei == -1) {
				break;
			}
			
			int endi = filecontent.IndexOf(Environment.NewLine,basei);
			
			filecontent = filecontent.Remove(basei,endi - basei);
			
			string strinsert = basestr+" \""+version+"\"";
			filecontent = filecontent.Insert(basei,strinsert);
			
			basei += strinsert.Length;
		}
		
		
		//write engine version in gamemain.c file
		
	   File.WriteAllText(gamemainfile,filecontent,Encoding.UTF8);
	}
}