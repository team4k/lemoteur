//common functions
int init_render(void* app_instance,void* app_handle,const char* window_title,wbool fullscreen);
void get_world_coords(Vector_t* world_vector, int x, int y);
void get_cursor_pos(double* x,double* y);
int stop_render();
int get_run();
void g_init();
void gameloop();
void renderoneframe();
void g_cleanup();
void init_gamemodule_path(const char* game_module);
int init_path();
int init_mngr();
int init_resx();
int init_path();
void free_mngr();
void init_text();
void load_level(const char* name);
void end_run();
void target(float x,float y,int button_id);
void movetarget(float x,float y);
void setcursorpos(float x,float y);
void offsettarget(float x,float y);
void endtarget(float x,float y,int button_id);
void lockrunning();
void unlockrunning();
void config_saved();
wbool disableinput();

void refresh_shader_program();
Levelobject_t* get_pausemenu();
const char* get_config_path();

void checkpause();
void endrenderthread();
void setcustompkgloader(lua_State* state);

resources_manager_t* get_resx_mngr();
module_mngr_t* get_module_mngr();
screen_fade_t* get_screenfade();
physics_manager_t* get_phymngr();
config_t* get_config();
input_mngr_t* get_input();
render_manager_t* get_render();
void* get_levelmngr();
wbool get_stream_mode();
snd_mngr_t* get_soundmngr();

void set_screen_size(int16_t width,int16_t height);

//sprite_t* get_cursor();
void set_app_path(const char* path);
void set_lib_path(const char* path);
void set_db_path(const char* path);
void set_save_path(const char* path);
void set_config_path(const char* path);
void set_locale(const char* locale);
void set_maxfps(float max_fps);
void set_state(int new_state);
void set_focus(wbool focused);
void set_audio_buffersize(int audio_buffsize);
void configure();
void show_cursor(wbool show);




