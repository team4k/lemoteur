#include <common_include.h>

#ifdef EMSCRIPTEN
	#include <Gamemain.h>
	int timing = 1;
#endif

//common variables
int running = wtrue;


int audio_buffer_size = AUDIO_FRAMES_PER_BUFFER * AUDIO_CHANNELS_NUM;

resources_manager_t resx_mngr = {0};
physics_manager_t phy_mgr = {0};
snd_mngr_t sound_mngr = {0};
screen_fade_t screen_fader = {0};
input_mngr_t input_mngr = {0};
screen_fill_t screen_filler = { 0 };
wbool has_filler = wfalse;

//set the level system used, if false, the level system is a per level file system (with a regular system that load the entire world in memory with loading between world)
//if true, the world is streamed using a sqlite database containing chunk and entities that are loaded / unloaded seamlessly in a background thread)
wbool stream_mode = wfalse;


//Rect_t screen_info = {0};

double lastTime = 0;
double rate = 0;

float MAX_FPS = 60.0f;

char* result = NULL;

wbool focus = wtrue;


render_manager_t render_mngr = {0};


char app_path[WAFA_MAX_PATH]  = {0};

//Level objects
Levelobject_t level_obj = {0};
level_stream_t level_stream_obj;
lua_State* lua_state = NULL;
struct _module_mngr module_mngr = {0};
localization_t local_obj = {0};
save_mngr_t save_mngr  = {0};
light_manager_t light_mngr = {0};
pathfind_worker_t path_worker = {0};

config_t config = {0};

char resources_path[260] = {0};
char script_path[260] = {0};
char font_path[260] = {0};
char lib_path[260] = {0};
char game_dll_path[260] = {0};
char db_path[260] = {0};
char save_path[260] = {0};
char config_path[260] = {0};
char current_locale[260] = {0};

Render_Text_t fps_text = {0};



//access function to game managers
resources_manager_t* get_resx_mngr()
{
	return &resx_mngr;
}

module_mngr_t* get_module_mngr()
{
	return &module_mngr;
}

config_t* get_config()
{
	return &config;
}

input_mngr_t* get_input()
{
	return &input_mngr;
}

render_manager_t* get_render()
{
	return &render_mngr;
}

void* get_levelmngr()
{
    if(stream_mode)
        return &level_stream_obj;
    else
	    return &level_obj;
}

int levelmngr_getcurrentstate()
{
    if(stream_mode)
        return level_stream_obj.current_state;
    else
        return level_obj.current_state;
}

void levelmngr_setcurrentstate(int current_state)
{
    if(stream_mode)
        level_stream_obj.current_state = current_state;
    else
        level_obj.current_state = current_state;
}

const char* levelmngr_getcurrentmodule()
{
     if(stream_mode)
        return level_stream_obj.current_module;
    else
        return level_obj.current_module;
}

wbool levelmngr_getrequestexit()
{
    if(stream_mode)
    {
        return level_stream_obj.request_exit;
    }
    else
    {
        return level_obj.request_exit;
    }
}

void levelmngr_draw(int z_indx)
{
    if(stream_mode)
    {
        level_stream_draw(&level_stream_obj,&render_mngr,&light_mngr,&resx_mngr);
    }
    else
    {
         //draw bottom map layer
	    if(level_obj.current_map != NULL)
		    Tilemap_Draw(&level_obj.current_map->tilemap,z_indx,&render_mngr,vectorzero,&light_mngr,&resx_mngr);

	    //draw entities
	    Levelobject_Drawentities(&level_obj,&render_mngr,z_indx,&resx_mngr,vectorzero,wtrue);

	    //draw entity groups
	    render_reset_color(&render_mngr);
	    Levelobject_Drawentitiesgroup(&level_obj,&render_mngr,z_indx,vectorzero,wtrue);

	    Levelobject_DrawText(&level_obj,&render_mngr,z_indx,vectorzero,wtrue);
    }
}

void levelmngr_update(float frame_time)
{
    if(stream_mode)
    {
		Vector_t player_pos = convert_cpvec_tovec(module_mngr.game_get_playerpos(0));
		
        level_stream_update(&level_stream_obj, player_pos,frame_time,&render_mngr,lua_state,wtrue);
    }
    else
    {
       Levelobject_update(&level_obj,frame_time,&render_mngr,lua_state, wtrue);
    }
   
}

void levelmngr_scriptevents(float frame_time)
{
	const char* current_module = NULL;
	if (stream_mode)
	{
		current_module = level_stream_obj.current_module;
	}
	else
	{
		current_module = level_obj.current_module;
	}


	//do scripts events
	if (current_module != NULL && strlen(current_module) > 0)
	{
		char buff[260];

		w_sprintf(buff, 260, "package.loaded['%s'].doEvents(%f)", current_module, frame_time);

		const char* result = script_execstring(lua_state, buff, NULL, 0);

		if (result != NULL)
			logprint(result);
	}
}

void levelmngr_renderlightmask()
{
	if (stream_mode)
	{

		if (level_stream_obj.edges_array != NULL)
			light_manager_rendermask(&light_mngr, level_stream_obj.edges_array, level_stream_obj.edges_array_size, &render_mngr, wfalse, NULL, level_stream_obj.tile_size, level_stream_obj.base_col_count * CHUNK_COL, level_stream_obj.base_row_count * CHUNK_ROW);
		
	}
	else
	{
		//render light mask first
		if (level_obj.current_map != NULL && level_obj.current_map->tilemap.edge_array != NULL)
			light_manager_rendermask(&light_mngr, level_obj.current_map->tilemap.edge_array, level_obj.current_map->tilemap.edge_array_size, &render_mngr, wfalse, level_obj.current_map_id, level_obj.current_map->tilemap.tile_size, level_obj.current_map->tilemap.col_count, level_obj.current_map->tilemap.row_count);
	}

}

void levelmngr_drawlight()
{
	if (stream_mode)
	{
		//draw lights
		if (level_stream_obj.edges_array != NULL)
			light_manager_draw(&light_mngr, &render_mngr, wfalse, NULL, level_stream_obj.tile_size, level_stream_obj.base_col_count * CHUNK_COL, level_stream_obj.base_row_count * CHUNK_ROW);
	}
	else
	{
		//draw lights
		if (level_obj.current_map != NULL && level_obj.current_map->tilemap.edge_array != NULL)
			light_manager_draw(&light_mngr, &render_mngr, wfalse, level_obj.current_map_id, level_obj.current_map->tilemap.tile_size, level_obj.current_map->tilemap.col_count, level_obj.current_map->tilemap.row_count);

	}
}

wbool get_stream_mode()
{
    return stream_mode;
}

screen_fade_t* get_screenfade()
{
	return &screen_fader;
}

screen_fill_t* get_screenfill()
{
	return &screen_filler;
}


physics_manager_t* get_phymngr()
{
	return &phy_mgr;
}


snd_mngr_t* get_soundmngr()
{
	return &sound_mngr;
}

void g_clean_text()
{
	SpriteBatch_g_clean(&fps_text.batch);
}

void g_create_text()
{
	SpriteBatch_g_create(&fps_text.batch,&render_mngr);
}


void set_maxfps(float max_fps)
{
	MAX_FPS = max_fps;
}

void set_state(int new_state)
{
	if(new_state == COMMON_PAUSE && levelmngr_getcurrentstate() != new_state)
		module_mngr.game_pause(wtrue);

	if(new_state == COMMON_RUN && levelmngr_getcurrentstate() == COMMON_PAUSE)
		module_mngr.game_unpause();

    levelmngr_setcurrentstate(new_state);
}

void set_focus(wbool focused)
{
	focus = focused;
}

void set_screen_size(int16_t width,int16_t height)
{
	render_mngr.screen_info.width = width;
	render_mngr.screen_info.height = height;
	render_mngr.zoom_factor = 1.0f;
}

void set_app_path(const char* path)
{
	w_strcpy(app_path,260,path);
}

void set_db_path(const char* path)
{
	w_strcpy(db_path,260,path);
}

void set_save_path(const char* path)
{
	w_strcpy(save_path,260,path);
}

void set_config_path(const char* path)
{
	w_strcpy(config_path,260,path);
}

const char* get_config_path()
{
	return &config_path[0];
}

void set_locale(const char* locale)
{
	w_strcpy(current_locale,260,locale);
}

void set_lib_path(const char* path)
{
	w_strcpy(lib_path,260,path);
}

int get_run()
{
	int retval = 0;
	
	lockrunning();
	
	retval = running;
	
	unlockrunning();
	
	return retval;
}

void end_run()
{
	running = wfalse;
}

void target(float x,float y,int button_id)
{
	module_mngr.game_target(x,y,wfalse,button_id);
}

void input(int16_t key_event)
{
	module_mngr.game_input(key_event,wtrue,wtrue);
}

void movetarget(float x,float y)
{
	module_mngr.game_movetarget(x,y,wfalse);
}

void setcursorpos(float x,float y)
{
    module_mngr.game_setcursorpos(x,y);
}

void offsettarget(float x,float y)
{
	module_mngr.game_offset_target(x,y);
}

wbool disableinput()
{
	return module_mngr.game_disableinput();
}

void endtarget(float x,float y,int button_id)
{
	module_mngr.game_endtarget(x,y,wfalse,button_id);
}

void init_text()
{
	Render_Text_Init(&fps_text,100,&render_mngr);

	font_info* inf = getfont_resx(&resx_mngr,"visitor1.ttf",28,wfalse);

	Render_Text_Setinfo(&fps_text,inf);
	Render_Text_setfixedsize(&fps_text,wtrue);
}

static void callback_trigger(const char* module,const char* func,const char* params,const char* entity_id,const char* trigger_id)
{
	char buff[260];

	if(params && strcmp(params,"") != 0)
		w_sprintf(buff,260,"package.loaded['%s'].%s(%s,'%s','%s');\n",levelmngr_getcurrentmodule(),func,params,entity_id, trigger_id);
	else
		w_sprintf(buff,260,"package.loaded['%s'].%s('%s','%s');\n",levelmngr_getcurrentmodule(),func,entity_id, trigger_id);

	const char* result = script_execstring(lua_state,buff,NULL,0);


	if(result != NULL)
		logprint(result);
}

void start_level_stream()
{
    //start the chunk generation / chunk loading from disk background thread
    level_stream_start_backgroundthread(&level_stream_obj);
}


void load_level_binary(Level* pb_level)
{
	start_resx_collect(&resx_mngr);//function to start collection of unused resx for this level, we position a flag telling if the resx should survive this collect, and 
	//clean everything else
	
	//load  level
	Levelobject_load_level2(&level_obj,&resx_mngr,&module_mngr,&phy_mgr,lua_state,&local_obj,&render_mngr,pb_level,&callback_trigger,vectorzero,NULL);

	getfont_resx(&resx_mngr,"visitor1.ttf",28,wfalse);
	module_mngr.game_keepresources();
	
	char filler_name[128] = { 0 };

	if (config_getstring(&config, "filler", filler_name, 128))
	{
		char filler_texture[128] = { 0 };
		char filler_animation[128] = { 0 };

		w_sprintf(filler_texture, 128, "%s.png", filler_name);
		w_sprintf(filler_animation, 128, "%s.awf", filler_name);

		gettexture_resx(&resx_mngr, filler_texture);
		getanimation_resx(&resx_mngr, filler_animation);
	}
	
	//end resx collect, this free up any pointed resources that haven't been used in this level loading
	end_resx_collect(&resx_mngr);


	#ifdef MEM_TRACE
		char LevelName[512];
		sprintf(LevelName,"Level %s Loaded",name);
		snapshot_mem_trace(LevelName);
	#endif
}

void load_level(const char* name)
{

	start_resx_collect(&resx_mngr);//function to start collection of unused resx for this level, we position a flag telling if the resx should survive this collect, and 
	//clean everything else
	
	//load  level
	
	Levelobject_load_level(&level_obj,&resx_mngr,&module_mngr,&phy_mgr,lua_state,&local_obj,&render_mngr,name,&callback_trigger);

	getfont_resx(&resx_mngr,"visitor1.ttf",28,wfalse);
	module_mngr.game_keepresources();
	
	char filler_name[128] = { 0 };

	if (config_getstring(&config, "filler", filler_name, 128))
	{
		char filler_texture[128] = { 0 };
		char filler_animation[128] = { 0 };

		w_sprintf(filler_texture, 128, "%s.png", filler_name);
		w_sprintf(filler_animation, 128, "%s.awf", filler_name);

		gettexture_resx(&resx_mngr, filler_texture);
		getanimation_resx(&resx_mngr, filler_animation);
	}
	
	//end resx collect, this free up any pointed resources that haven't been used in this level loading
	end_resx_collect(&resx_mngr);


	#ifdef MEM_TRACE
		char LevelName[512];
		sprintf(LevelName,"Level %s Loaded",name);
		snapshot_mem_trace(LevelName);
	#endif


}

int init_render(void* app_instance,void* app_handle,const char* window_title,wbool fullscreen)
{
	int filter_val = 0;
	config_getint(&config,"texture_filtering",&filter_val);

	texture_filter filter = (texture_filter)filter_val;

	render_init(&render_mngr,render_mngr.screen_info.width,render_mngr.screen_info.height,app_path,window_title,app_instance,app_handle,wfalse,fullscreen,filter);
	
    return 0;
}



int stop_render()
{
	render_stop(&render_mngr);
	
	return 0;
}


static void callback_entity(const char* id,const char* func,const char* parent,const char* params,void* return_value,size_t return_size)
{
	const char* real_parent = NULL;
	char r_parent[128] = {0};

	if(parent == NULL || strcmp(parent,"") == 0)
	{
		w_sprintf(r_parent,128,"%p",get_levelmngr());
		real_parent = &r_parent[0];
	}
	else
	{
		real_parent = parent;
	}

	char buff[260] = {0};


	if(params && strcmp(params,"") != 0)
		w_sprintf(buff,260,"return %s(Entities['%s']['%s'],%s);\n",func,real_parent,id,params);
	else
		w_sprintf(buff,260,"return %s(Entities['%s']['%s']);\n",func,real_parent,id);

	const char* result = script_execstring(lua_state,buff,return_value,return_size);


	if(result != NULL)
		logprint(result);
}

static void callback_texte(const char* id,const char* func,const char* parent,const char* params)
{
	char r_parent[128] = {0};

	if(parent == NULL || strcmp(parent,"") == 0)
	{
		w_sprintf(r_parent,128,"%p",get_levelmngr());
		parent = &r_parent[0];
	}

	char buff[260];


	if(params && strcmp(params,"") != 0)
		w_sprintf(buff,260,"%s(Text['%s']['%s'],%s);\n",func,parent,id,params);
	else
		w_sprintf(buff,260,"%s(Text['%s']['%s']);\n",func,parent,id);

	const char* result = script_execstring(lua_state,buff,NULL,0);

	if(result != NULL)
		logprint(result);
}

void callback_config_saved()
{
	//notify bridge app that config as updated
	config_saved();
	//notify game dll that config as updated
	module_mngr.game_config_updated();
}


TIME_PROF currentTime;
wbool show_fps = wtrue;

wbool use_cursor = wfalse;

SHADERPROGRAM_ID prog_id = 0;
SHADERPROGRAM_ID text_prog = 0;

//collisions delta time
const float sample_rate = 3;
float dt = 0;
	
double oversleep = 0.0;
int32_t count_frame = 0;

float max_frame_time = 0;

long max_nsecframe_time = 0;

long time_elapsed = 0;
wchar_t fps[260];
	
#ifdef ANDROID_BUILD
	char fps_android[260];
#endif

int32_t num_fps = 0;

long sleep_time = 0;
long frameRenderTime = 0;
TIME_PROF end;
long overtime = 0;

#ifdef PERFORMANCE_TEST
		TIME_PROF start_render, end_render, start_physics, end_physics,start_entities,end_entities,start_script,end_script,start_game_update,end_game_update,start_input,end_input,start_swap,end_swap,start_event,end_event,eps_micro;

#endif

void renderoneframe()
{
	TIME_PROF newTime;
	time_get_time(&newTime);

	TIME_PROF ltime = time_diff(&currentTime,&newTime);
	 
	time_elapsed = time_get_nsec(&ltime);
	  
	float rnd = (float)frameRenderTime / 1000000L;

	#ifdef ANDROID_BUILD
		//wchar implementation is broken under android, copy ourselves characters to wide char string
		sprintf(fps_android,"FPS : [%d] Render Time : [%05.2f ms]",num_fps,rnd);
			
		for(int i =0; i < 260;i++)
		{
			fps[i] = (wchar_t)fps_android[i];
		}
	#else
		swprintf(fps,260,L"FPS : [%d] Render Time : [%05.2f ms] ",num_fps,rnd);
	#endif

		 
		 
	if(time_elapsed >= 1000000000L)
	{
		num_fps = count_frame;
		count_frame = 0;
		time_get_time(&currentTime);
	}

	#ifdef PERFORMANCE_TEST
		time_get_time(&start_render);
	#endif

	

	//render_clear_screen(&render_mngr);
	ColorA_t back_col = {0.7f,0.7f,0.7f,1.0f};
	//ColorA_t back_col = { 1.0f,0.0f,0.0f,1.0f };
	render_clear_screenwithcolor(&render_mngr,back_col);

	module_mngr.game_updatescroll();


	render_update_screen(&render_mngr);


    if((levelmngr_getcurrentstate() & COMMON_NODRAW) == 0)
	{
		levelmngr_renderlightmask();
	  
        render_use_program(&render_mngr,text_prog);

		ColorA_t color = {1.0f,1.0f,1.0f,1.0f};

		render_set_color(&render_mngr,color);
    
		//draw on screen
		for(int16_t z_indx = 0; z_indx < MAX_ZINDEX - 1;z_indx++)
		{
            levelmngr_draw(z_indx);

			//draw game specific objects
			module_mngr.game_draw(z_indx);
			
			if (screen_filler.active && screen_filler.z_index == z_indx)
			{
				render_use_program(&render_mngr, text_prog);
				screen_fill_draw(&screen_filler, &render_mngr);
			}

		}

		levelmngr_drawlight();
		
		if (screen_filler.active && (screen_filler.z_index == MAX_ZINDEX - 1))
		{
			render_use_program(&render_mngr, text_prog);
			screen_fill_draw(&screen_filler, &render_mngr);
		}

		
		//draw top elements
		if(screen_fader.active)
		{
			render_use_program(&render_mngr,prog_id);
			screen_fade_draw(&screen_fader,&render_mngr);
			screen_fade_update(&screen_fader,max_frame_time);
		}

        //draw topmost
        levelmngr_draw(MAX_ZINDEX - 1);

		if (screen_filler.active && (screen_filler.z_index == -1) )
		{
			render_use_program(&render_mngr, text_prog);
			screen_fill_draw(&screen_filler, &render_mngr);
		}
		
		module_mngr.game_draw(-1);//top most game draw
	}
	 
	 
	
	
	if(show_fps)
	{
			
		int32_t text_x = (int32_t)-(render_mngr.scroll_vector.x * render_mngr.zoom_factor);
		int32_t text_y = (int32_t)-(render_mngr.scroll_vector.y * render_mngr.zoom_factor);

		render_push_matrix(&render_mngr);
		text_y += (int32_t)Render_Text_charheight(&fps_text);

		//select shader program
		render_use_program(&render_mngr,text_prog);

		Render_Text_begin(&fps_text,0.5f,0.5f,0.5f,1.0f,&render_mngr);
		Render_Text_Draw(&fps_text,&fps[0],(float)text_x,(float)text_y,&render_mngr);
		Render_Text_end(&fps_text,&render_mngr);

		render_pop_matrix(&render_mngr);
		//logprint("%ls",fps);
	}

	#ifdef PERFORMANCE_TEST
		time_get_time(&end_render);
	#endif

		
	//update game object - handle physics

	if((level_obj.current_state & COMMON_PAUSE) == 0)
	{
		#ifdef PERFORMANCE_TEST
			time_get_time(&start_physics);
		#endif

		//update physics engine
		for(int32_t sr = 0; sr < sample_rate;sr++)
			update_physics(&phy_mgr,dt);

		#ifdef PERFORMANCE_TEST
			time_get_time(&end_physics);
			time_get_time(&start_entities);
		#endif

		//update entities
        levelmngr_update(max_frame_time);
			
		if (screen_filler.active)
			screen_fill_update(&screen_filler, &render_mngr, max_frame_time);


		#ifdef PERFORMANCE_TEST
			time_get_time(&end_entities);
		#endif

	}
	 
	 #ifdef EMSCRIPTEN
		if(path_worker.initialized)
			pathfind_simupdate(&path_worker);
	#endif
	 
	#ifdef PERFORMANCE_TEST
			time_get_time(&start_game_update);
	#endif
	//update game specific object, use fixed frame time
	module_mngr.game_update(max_frame_time);
	 
	#ifdef PERFORMANCE_TEST
		time_get_time(&end_game_update);
		time_get_time(&start_input);
	#endif

	input_mngr_handle_input(&input_mngr);

	#ifdef PERFORMANCE_TEST
		time_get_time(&end_input);
		time_get_time(&start_event);
	#endif

	input_mngr_poll(&input_mngr);

	#ifdef PERFORMANCE_TEST
		time_get_time(&end_event);
		time_get_time(&start_swap);
	#endif

	//swap front and back buffer
	render_swap_buffers(&render_mngr);

	//glFlush();

	#ifdef PERFORMANCE_TEST
		time_get_time(&end_swap);
	#endif

	time_get_time(&end);
		
	TIME_PROF frame_rnd_struct = time_diff(&newTime,&end);

	frameRenderTime =  time_get_nsec(&frame_rnd_struct);


	#ifdef PERFORMANCE_TEST
	//performance counter

	//if(frameRenderTime > 16700000L && frameRenderTime < 60000000L)
	if(frameRenderTime > 30700000L && frameRenderTime < 60000000L)
	{
			
		float rnd = (float)frameRenderTime / 1000000L;
		logprint("Frame render longer than max frame time! time = [%05.2f]",rnd);
		logprint("Perf report");

		char time_df[256] = {0};
			
		time_get_char_diff(time_df,&start_render,&end_render,256);
			
		logprint("render time = [%s]",time_df);
			
		memset(time_df,0,256);

		time_get_char_diff(time_df,&start_physics,&end_physics,256);
			
		logprint("physics time = [%s]",time_df);
			
		memset(time_df,0,256);
			
		time_get_char_diff(time_df,&start_entities,&end_entities,256);

		logprint("entities time = [%s]",time_df);
			
		memset(time_df,0,256);

		time_get_char_diff(time_df,&start_script,&end_script,256);

		logprint("script time = [%s]",time_df);
			
		memset(time_df,0,256);

		time_get_char_diff(time_df,&start_game_update,&end_game_update,256);

		logprint("game update time = [%s]",time_df);

		memset(time_df,0,256);

		time_get_char_diff(time_df,&start_input,&end_input,256);

		logprint("input time = [%s]",time_df);
			
		memset(time_df,0,256);

		time_get_char_diff(time_df,&start_swap,&end_swap,256);

		logprint("swap time = [%s]",time_df);
			
		#ifndef ANDROID_BUILD
			memset(time_df,0,256);

			time_get_char_diff(time_df,&start_event,&end_event,256);

			logprint("poll event time = [%s]",time_df);
		#endif

			logprint("End Perf report");

	}


	#endif


	//sleep to force our frame to render in 16.7 ms
	if( frameRenderTime < max_nsecframe_time )
	{
		sleep_time = max_nsecframe_time - frameRenderTime;
			
		if(sleep_time < 0)
			sleep_time = 0;
			
		if(overtime > 0)
		{
			if(overtime > sleep_time)
			{
				overtime -= sleep_time;
				sleep_time = 0;
			}
			else
			{
				sleep_time -= overtime;
				overtime = 0;
			}
		}

		#if !defined(EMSCRIPTEN) && (!defined(WINAPI_FAMILY) || (WINAPI_FAMILY != WINAPI_FAMILY_PHONE_APP))
			nano_sleep(sleep_time,focus);
		#endif

		#if defined(EMSCRIPTEN)
			//change timing based on observed fps number / refresh rate

			if (num_fps >= 120 && timing != 2) { //we are over hundred fps, so around 120hz 
				emscripten_set_main_loop_timing(EM_TIMING_RAF, 2);
				timing = 2;
			}

			/*if (num_fps <= 60 && timing != 1) { //60 hz and less
				emscripten_set_main_loop_timing(EM_TIMING_RAF, 1);
				timing = 1;
			}*/
			
			
		#endif	
				
	}
	else //over maximum frame time, store the difference to reduce sleep time of further frames
	{
		overtime += frameRenderTime - max_nsecframe_time;
	}


	count_frame++;
		
	#if defined(ANDROID_BUILD)
		checkpause();
	#endif

	//unload existing level and load new level if needed
    if(level_obj.request_load_level && !stream_mode)
	{
		module_mngr.game_cleanup(wfalse);
		Levelobject_clean_level(&level_obj,&resx_mngr,&phy_mgr,lua_state);
		script_callgc(lua_state);

		if(level_obj.request_load_binary)
			load_level_binary(level_obj.pending_binary);
		else
			load_level(level_obj.pending_level);

		level_obj.request_load_level = wfalse;
		level_obj.request_load_binary = wfalse;
		level_obj.pending_binary = NULL;
	}

	//quit game
    if(levelmngr_getrequestexit() || input_mngr.request_close)
	{
		//save level stream content before exit
		if (stream_mode)
		{
			level_stream_persist(&level_stream_obj, wfalse);
		}

		end_run();
	}

	#ifdef MEM_TRACE
		frameend_mem_trace();
	#endif

}

void refresh_shader_program()
{
	prog_id = render_get_program(&render_mngr,"prog_no_texture");
	text_prog = render_get_program(&render_mngr,"prog_textured");
}

Levelobject_t* get_pausemenu()
{
	return (Levelobject_t*)module_mngr.game_getpausemenu();
}

void g_init()
{
	time_get_time(&currentTime);

	config_getfloat(&config, "fps_limit", &MAX_FPS);
	config_getbool(&config, "show_fps", &show_fps);
	config_getbool(&config, "use_cursor", &use_cursor);

	prog_id = render_get_program(&render_mngr, "prog_no_texture");

	//collisions delta time
	dt = 1.0f / MAX_FPS / sample_rate;

	max_frame_time = 1000.0f / MAX_FPS;
	max_nsecframe_time = max_frame_time * 1000000L;

	text_prog = render_get_program(&render_mngr, "prog_textured");

	render_blend_alpha(&render_mngr);
}

void g_cleanup()
{
	module_mngr.game_cleanup(wtrue);

    if(stream_mode)
    {

    }
    else
    {
        Levelobject_clean_level(&level_obj, &resx_mngr, &phy_mgr, lua_state);
    }

	
	script_callgc(lua_state);

	snd_mngr_stop(&sound_mngr);

	free_all_resources(&resx_mngr);
	
	if (has_filler)
		screen_fill_free(&screen_filler);

	stop_render();

	endrenderthread();
	free_mngr();
	
	#if (!defined(WINAPI_FAMILY) || (WINAPI_FAMILY != WINAPI_FAMILY_PHONE_APP))
		//exit program
		exit(EXIT_SUCCESS);
	#endif
}

void gameloop()
{
	g_init();

	#ifdef EMSCRIPTEN
		emscripten_set_main_loop(renderoneframe,0,1);
		emscripten_set_main_loop_timing(EM_TIMING_RAF, 1);
	#elif !defined(WINAPI_FAMILY) || (WINAPI_FAMILY != WINAPI_FAMILY_PHONE_APP)
		while( get_run() )
		{
			renderoneframe();
		}
		g_cleanup();
	#endif
}

int init_resx()
{
	//setup ressources manager
	init_resources(&resx_mngr,&resources_path[0],&render_mngr);
	return 0;
}



int init_mngr()
{

	char packer_file[128];

	//set our texture packer
	if(config_getstring(&config,"packer",&packer_file[0],128))
		resx_set_from_packer(&resx_mngr,packer_file);
	
	//no dlopen in emscripten, set function pointers directly
	#ifdef EMSCRIPTEN
		module_mngr.game_init_objects = (game_init_objects_t)game_init_objects;
		module_mngr.game_init_physics = (game_init_physics_t)game_init_physics;
		module_mngr.game_init_scripts = (game_init_scripts_t)game_init_scripts;
		module_mngr.game_get_playerpos = (game_get_playerpos_t)game_get_playerpos;
		module_mngr.game_player_reposition = (game_player_reposition_t)game_player_reposition;
		module_mngr.game_debuginfo_update = (game_debuginfo_update_t)game_debuginfo_update;
		module_mngr.game_draw = (game_draw_t)game_draw;
		module_mngr.game_debug_draw = (game_debug_draw_t)game_debug_draw;
		module_mngr.game_update = (game_update_t)game_update;
		module_mngr.game_move = (game_move_t)game_move;
		module_mngr.game_target = (game_target_t)game_target;
		module_mngr.game_get_playerbox = (game_get_playerbox_t)game_get_playerbox;
		module_mngr.game_editor_reset = (game_editor_reset_t)game_editor_reset;
		module_mngr.game_endtarget = (game_endtarget_t)game_endtarget;
		module_mngr.game_movetarget = (game_movetarget_t)game_movetarget;
		module_mngr.game_cleanup = (game_cleanup_t)game_cleanup;
		module_mngr.game_updatescroll = (game_updatescroll_t)game_updatescroll;
		module_mngr.game_keepresources = (game_keepresources_t)game_keepresources;
		module_mngr.game_offset_target = (game_offset_target_t)game_offset_target;
		module_mngr.game_update_cursorpos = (game_update_cursorpos_t)game_update_cursorpos;
        module_mngr.game_setcursorpos = (game_setcursorpos_t)game_setcursorpos;
		module_mngr.game_has_player = (game_has_player_t)game_has_player;
		module_mngr.game_config_updated = (game_config_updated_t)game_config_updated;
		module_mngr.game_remove_physics = (game_remove_physics_t)game_remove_physics;
		module_mngr.game_free = (game_free_t)game_free;
		module_mngr.game_g_cleanup = (game_g_cleanup_t)game_g_cleanup;
		module_mngr.game_g_create = (game_g_create_t)game_g_create;
		module_mngr.game_pause= (game_pause_t)game_pause;
		module_mngr.game_unpause = (game_unpause_t)game_unpause;
		module_mngr.game_chunk_loaded = (game_chunk_loaded_t)game_chunk_loaded;
		module_mngr.game_level_loaded = (game_level_loaded_t)game_level_loaded;
		module_mngr.game_getversion = (game_getversion_t)game_getversion;
		module_mngr.game_disableinput = (game_disableinput_t)game_disableinput;
		module_mngr.game_input = (game_input_t)game_input;
		module_mngr.game_joystick_axe = (game_joystick_axe_t)game_joystick_axe;
		module_mngr.game_joystick_button = (game_joystick_button_t)game_joystick_button;
		module_mngr.game_changeinputmode = (game_changeinputmode_t)game_changeinputmode;
		module_mngr.game_char_input = (game_char_input_t)game_char_input;
		module_mngr.game_setexternapifunc = (game_setexternapifunc_t)game_setexternapifunc;
		module_mngr.game_wheelpinch = (game_wheelpinch_t)game_wheelpinch;
		module_mngr_check(&module_mngr);
	#else
		module_mngr_init(&module_mngr,game_dll_path);
	#endif
	
    config_getbool(&config,"stream_mode",&stream_mode);
		
    if(!stream_mode)
    {
	    //init level object
	    Levelobject_Init(&level_obj,&light_mngr,&sound_mngr,&path_worker);
    }

	//setup physics engine
	physics_init2(&phy_mgr, wfalse, stream_mode);

	float suggestedLatency = 0.05f;

	#ifdef EMSCRIPTEN
		audio_buffer_size = 1024;
	#endif	
	

	int16_t music_volume = 10;
	int16_t sfx_volume = 10;

	config_getshort(&config,"music_volume",&music_volume);
	config_getshort(&config,"sfx_volume",&sfx_volume);

	//init sound manager
	snd_mngr_init(&sound_mngr,audio_buffer_size,suggestedLatency);


	snd_mngr_setvolume(&sound_mngr,music_volume,sfx_volume);


	//init script mngr and register managers
	script_createglobalmngr(lua_state);

	
	#ifdef ANDROID_BUILD
	
		char tmp_db_path[260];
		
		//copy database content from apk to applications file folder
		sprintf(tmp_db_path,"assets/locale.db");

		long db_size = getrawdata_size(&resx_mngr,tmp_db_path);
		unsigned char* db_buffer = (unsigned char*)wf_malloc(db_size * sizeof(unsigned char));

		if(getrawdata(&resx_mngr,tmp_db_path,db_buffer,db_size) == -1)
		{
			logprint("Can't find locale.db file!");
			wf_free(db_buffer);
			db_buffer = NULL;
			return 0;
		}
		
		FILE* file = fopen(db_path,"w");//remove previous db file if any
		 
		if(file != NULL)
		{
			fwrite(db_buffer,sizeof(unsigned char),db_size,file);
			fclose(file);
		}
		else
		{
			logprint("Failed to write locale.db file content to file system %s",db_path);
			wf_free(db_buffer);
			db_buffer = NULL;
			return 0;
		}
		
		wf_free(db_buffer);
		db_buffer = NULL;
		
		char tmp_save_path[260];
		
		//copy save db file from apk to application file folder 
		sprintf(tmp_save_path,"assets/save.db");

		db_size = getrawdata_size(&resx_mngr,tmp_save_path);
		db_buffer = (unsigned char*)wf_malloc(db_size * sizeof(unsigned char));

		if(getrawdata(&resx_mngr,tmp_save_path,db_buffer,db_size) == -1)
		{
			logprint("Can't find save.db file!");
			wf_free(db_buffer);
			return 0;
		}
		
		//write a save db file ONLY if no save.db found
		file = fopen(save_path,"r");
		
		if(file == NULL)
		{  
		   file = fopen(save_path,"w");
		   
		   if(file != NULL)
		   {
				fwrite(db_buffer,sizeof(unsigned char),db_size,file);
				fclose(file);
		   }
		   else
		   {
				logprint("Failed to write save.db file content to file system %s",save_path);
				wf_free(db_buffer);
				db_buffer = NULL;
				return 0;
		   }
		}
		else
		{
			fclose(file);
		}
		
		wf_free(db_buffer);
		db_buffer = NULL;
		
		
		
	#endif

		
	save_mngr_init(&save_mngr,save_path);
	
	char filler_name[256] = { 0 };

	if (config_getstring(&config, "filler", filler_name, 256))
	{
		char filler_texture[128] = { 0 };
		char filler_animation[128] = { 0 };

		w_sprintf(filler_texture, 128, "%s.png", filler_name);
		w_sprintf(filler_animation, 128, "%s.awf", filler_name);

		Vector_t dir = { 0,0 };
		screen_fill_init(&screen_filler, 5, 250, dir, gettexture_resx(&resx_mngr, filler_texture), 64, 64, &render_mngr, getanimation_resx(&resx_mngr, filler_animation), "lava_fill", "lava_edge");

		script_pushmngr(lua_state, "screen_filler", &screen_filler);
		has_filler = wtrue;
	}
	
	screen_fade_init(&screen_fader);
	
	script_pushmngr(lua_state,"physics_mngr",&phy_mgr);
	script_pushmngr(lua_state,"callback_entity",&callback_entity);
	script_pushmngr(lua_state,"callback_texte",&callback_texte);
	script_pushmngr(lua_state,"level_mngr",get_levelmngr());
	script_pushmngr(lua_state,"sound_mngr",&sound_mngr);
	script_pushmngr(lua_state,"resx_mngr",&resx_mngr);
	script_pushmngr(lua_state,"render_mngr",&render_mngr);
	script_pushmngr(lua_state,"localization",&local_obj);
	script_pushmngr(lua_state,"config",&config);
	script_pushmngr(lua_state,"render_mngr",&render_mngr);
	script_pushmngr(lua_state,"module_mngr",&module_mngr);
	script_pushmngr(lua_state,"screen_fader",&screen_fader);
	script_pushmngr(lua_state,"save_mngr",&save_mngr);
	script_pushmngr(lua_state,"config_path",&config_path[0]);
	script_pushmngr(lua_state,"light_mngr",&light_mngr);
	script_pushmngr(lua_state,"pathfind_worker",&path_worker);
	script_pushbooleanparam(lua_state,"stream_mode",stream_mode);
	
	const char* log_return;

	#ifdef ANDROID_BUILD
		setcustompkgloader(lua_state);
	#else
		const char* script_ext = get_filename_ext(script_path);

		if(strcmp(script_ext,"zip") == 0)
		{
			setcustompkgloader(lua_state);
			logprint("Custom lua script loader set");
		}
		else
		{
			//add our script folder path to lua package path
			char package_path[500];
			char final_package_path[1000];

					#if defined(_WIN32)
						w_sprintf(package_path,500,"package.path = '%s\\?.lua;' .. package.path",&script_path[0]);

						str_double_slash(&package_path[0],final_package_path,"\\");
					#else
						 sprintf(final_package_path,"package.path = '%s/?.lua;' .. package.path",&script_path[0]);
					#endif

			log_return  = script_execstring(lua_state,final_package_path,NULL,0);

			if(log_return != NULL)
				logprint(log_return);
			else
				logprint("Chemin scripts lua ajoute au package.path");
		}
			
	#endif


	logprint("game init locale");
	//init localization
	localization_init(&local_obj,db_path);

	char locale[256] = {0};


	if(config_getstring(&config,"locale",&locale[0],256))
	{
		if(strcmp(locale,"") == 0)
		{
			#ifdef ANDROID_BUILD
			
			const char* fr_exist = strstr(current_locale,"fr");
			
			#else
				//get locale from system
				setlocale(LC_ALL,"");
				
				const char* new_locale = setlocale(LC_ALL, NULL);

				#if defined(_WIN32)
					const char* fr_exist = strstr(new_locale,"French");
				#else
					const char* fr_exist = strstr(new_locale,"fr");
				#endif
			#endif
			
			if(fr_exist != NULL)
			{
				w_strcpy(locale,256,"fr");
			}
			else
			{
				w_strcpy(locale,256,"en");
			}
			

			config_setstring(&config,"locale",locale);

			config_saveconf(&config,&config_path[0]);
		}


		localization_setlocale(&local_obj,locale);
	}

	logprint("game init objects");

	module_mngr.game_init_objects(&resx_mngr,0,0,&phy_mgr,get_levelmngr(),"visitor1.ttf",&config,&render_mngr,&local_obj,lua_state,&sound_mngr,&input_mngr);

	logprint("game init physics");

	module_mngr.game_init_physics(&phy_mgr);

	logprint("game init scripts");

	module_mngr.game_init_scripts(lua_state);
	
	ColorA_t ambient = {0.0f,0.0f,0.0f,0.7f};

	int light_t = 0;

	config_getint(&config,"light_model",&light_t);

	light_model l_t = (light_model)light_t;
	
	light_manager_init(&light_mngr,ambient,&render_mngr,render_get_program(&render_mngr,"prog_no_texture"),render_get_program(&render_mngr,"prog_no_texture"),render_get_program(&render_mngr,"prog_shadows"),l_t,512,512);


	if (stream_mode)
	{

		//init level stream in module, if no module, print a warning message
		if (module_mngr.game_initlevelstream != NULL)
		{
			module_mngr.game_initlevelstream(app_path,&module_mngr);
		}
		else
		{
			logprint("No game_initlevelstream set in game module even though stream_mode is ON, please check game module / config");
		}
	}

	return 1;
}

void free_mngr()
{
	light_manager_free(&light_mngr,&render_mngr);
	input_mngr_free(&input_mngr);
	localization_free(&local_obj);
	save_mngr_free(&save_mngr);
	script_destroystate(lua_state);
	lua_state = NULL;

    if(stream_mode)
    {
        level_stream_free(&level_stream_obj);
    }
    else
    {
	    Levelobject_Free(&level_obj);
    }

	snd_mngr_free(&sound_mngr);
	physics_free(&phy_mgr);
	module_mngr_release(&module_mngr);
}

void init_gamemodule_path(const char* game_module)
{
	
	#ifdef ANDROID_BUILD
		sprintf(game_dll_path,"%s/lib%s.so",&lib_path[0],game_module);
	#else
		#if defined(_WIN32)
			#if defined(WINAPI_FAMILY) && (WINAPI_FAMILY == WINAPI_FAMILY_PHONE_APP)
				w_sprintf(game_dll_path, 260, "%s.dll", game_module);
			#else
				w_sprintf(game_dll_path,260,"%s%s.dll",&app_path[0],game_module);
			#endif
		#elif defined(__APPLE__)
			sprintf(game_dll_path,"%slib%s.dylib",&app_path[0],game_module);
		#else
			sprintf(game_dll_path,"%s%s.so",&app_path[0],game_module);
		#endif
	#endif
}

int init_path()
{
	#ifdef ANDROID_BUILD
		//in android mode, app path is already defined, and contains the apk file path
		sprintf(resources_path,"%s",&app_path[0]);
	#else
		#ifdef EMSCRIPTEN
			sprintf(&app_path[0],"");
		#elif !defined(WINAPI_FAMILY) || (WINAPI_FAMILY != WINAPI_FAMILY_PHONE_APP)
			get_app_path(app_path);
			set_db_path(app_path);
			set_config_path(app_path);
			set_save_path(app_path);
			logprint_setdir(app_path);
		#endif
		
		w_sprintf(resources_path,260,"%sassets",&app_path[0]);
		w_sprintf(script_path,260,"%sscripts",&app_path[0]);
	#endif

	
	#ifdef ANDROID_BUILD
		sprintf(font_path,"assets/visitor1.ttf");
	#else
		#if defined(_WIN32)
			w_sprintf(font_path,260,"%sassets\\%s",&app_path[0],"visitor1.ttf");
		#elif defined(__APPLE__)
			sprintf(font_path,"%sassets/%s",&app_path[0],"visitor1.ttf");
		#else
			sprintf(font_path,"%sassets/%s",&app_path[0],"visitor1.ttf");
		#endif
	#endif


	
	char tmp_db_path[260];
	char tmp_save_path[260];
	char tmp_config_path[260];
	w_strcpy(tmp_save_path,260,save_path);
	w_strcpy(tmp_db_path,260,db_path);
	w_strcpy(tmp_config_path,260,config_path);


	if (tmp_db_path[strlen(tmp_db_path) - 1] != PATH_SEP)
		w_sprintf(tmp_db_path, 260, "%s%c", db_path,PATH_SEP);

	if (tmp_save_path[strlen(tmp_save_path) - 1] != PATH_SEP)
		w_sprintf(tmp_save_path, 260, "%s%c", save_path, PATH_SEP);

	if (tmp_config_path[strlen(tmp_config_path) - 1] != PATH_SEP)
		w_sprintf(tmp_config_path, 260, "%s%c", config_path, PATH_SEP);
    
	w_sprintf(db_path,260,"%slocale.db",&tmp_db_path[0]);
	w_sprintf(save_path,260,"%ssave.db",&tmp_save_path[0]);
	w_sprintf(config_path,260, "%sconfig.lua", &tmp_config_path[0]);

	return 1;
}

void set_audio_buffersize(int audio_buffsize)
{
	//keep our buffer big enough and under our max buffer size
	if(audio_buffsize < 4096)
		audio_buffer_size = 4096;
	else if (audio_buffsize > MAX_BUFFER_SIZE)
		audio_buffer_size = MAX_BUFFER_SIZE;
	else
		audio_buffer_size = audio_buffsize;
}

void configure()
{
	//if android build, copy the config from the apk first
	//to application file folder 

	char tmp_config_path[260];

	size_t config_size = 0;
	unsigned char* config_buffer = NULL;

	#ifdef ANDROID_BUILD
	w_sprintf(tmp_config_path, 260, "assets/config_android.lua");

	config_size = getrawdata_size(&resx_mngr, tmp_config_path);
	config_buffer = (unsigned char*)wf_malloc(config_size * sizeof(unsigned char));

	if (getrawdata(&resx_mngr, tmp_config_path, config_buffer, config_size) == -1)
	{
		logprint("Can't find config.lua file!");
		wf_free(config_buffer);
		return;
	}
	#else
	w_sprintf(tmp_config_path, 260, "%sconfig.lua", app_path);

	if (strcmp(config_path, tmp_config_path) != 0)
	{
		config_buffer = get_resx_content(&resx_mngr, tmp_config_path, &config_size,NULL);
	}

	#endif

	if (config_buffer != NULL)
	{
		wbool dev_mode = wtrue;

		//write a config file ONLY if no config_android.lua found or 
		//if we are in dev mode
		FILE* file = NULL;

		if (!dev_mode)
			w_fopen(file,config_path, "r");
		else
			logprint("DEV MODE: don't forget to set dev_mode variable to wfalse in common.c file before release");

		if (file == NULL)
		{
			w_fopen(file,config_path, "w");

			if (file != NULL)
			{
				fwrite(config_buffer, sizeof(unsigned char), config_size, file);
				fclose(file);
			}
			else
			{
				logprint("Failed to write config.lua file content to file system %s", config_path);
				wf_free(config_buffer);
				config_buffer = NULL;
				return;
			}
		}
		else
		{
			fclose(file);
		}

		wf_free(config_buffer);
		config_buffer = NULL;
	}


	//load lua config file, (if any)
	lua_state =  script_createstate();
	config_init(&config,lua_state);
	
	config_readconf(&config,(void*)&resx_mngr,&config_path[0]);

	config.config_saved = callback_config_saved;

	//update resources path if needed (PC builds only)
	#ifndef ANDROID_BUILD

	char c_asset_path[260];
	char c_script_path[260];
	char new_resx_path[260];
	char new_script_path[260];


	if(config_getstring(&config,"asset_path",&c_asset_path[0],260))
	{
		w_strcpy(new_resx_path,260,app_path);

		//remove last slash in path
		if(new_resx_path[strlen(new_resx_path)-1] == PATH_SEP)
			new_resx_path[strlen(new_resx_path)-1] = '\0';

		combine_rel_path(new_resx_path,&c_asset_path[0],260);

		w_sprintf(resources_path,260,"%s",new_resx_path);
	}

	if(config_getstring(&config,"script_path",&c_script_path[0],260))
	{
		w_strcpy(new_script_path,260,app_path);

		//remove last slash in path
		if(new_script_path[strlen(new_script_path)-1] == PATH_SEP)
			new_script_path[strlen(new_script_path)-1] = '\0';

		combine_rel_path(new_script_path,&c_script_path[0],260);

		w_sprintf(script_path,260,"%s",new_script_path);
	}
        
        


	#endif
}
