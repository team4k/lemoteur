#include <common_include.h>

#include <jni.h>
#include <android/native_window.h> // requires ndk r5 or newer
#include <android/native_window_jni.h> // requires ndk r5 or newer
#include <pthread.h>
#include <EGL/egl.h>	


ANativeWindow* window;
pthread_t gameloop_thread;
static pthread_mutex_t touch_mutex = PTHREAD_MUTEX_INITIALIZER;

char level_start[256] = {0};

JNIEXPORT void JNICALL Java_wafa_game_MainActivity_touchscreen(JNIEnv *env,jobject self,jfloat posx,jfloat posy,jint action)
{
	pthread_mutex_lock(&touch_mutex);
	
	input_mngr_touch(get_input(),posx,posy,action);
	
	pthread_mutex_unlock(&touch_mutex);
}

JNIEXPORT void JNICALL Java_wafa_game_MainActivity_backbutton(JNIEnv *env,jobject self)
{
	pthread_mutex_lock(&touch_mutex);

	input_mngr_backbutton(get_input());
	
	pthread_mutex_unlock(&touch_mutex);
}


static int checkload (lua_State *L, int stat, const char *filename) {
  if (stat) {  /* module loaded successfully? */
    lua_pushstring(L, filename);  /* will be 2nd argument to module */
    return 2;  /* return open function and file name */
  }
  else
    return luaL_error(L, "error loading module " LUA_QS
                         " from file " LUA_QS ":\n\t%s",
                          lua_tostring(L, 1), filename, lua_tostring(L, -1));
}

static int lua_custom_loader(lua_State* state,const char* module_key,const char* module_path)
{
	long script_size = getrawdata_size(get_resx_mngr(),module_path);
	unsigned char* script_buffer = (unsigned char*)malloc(script_size * sizeof(unsigned char));
	getrawdata(get_resx_mngr(),module_path,script_buffer,script_size);
	luaL_loadbuffer(state,script_buffer,script_size,module_key) ;
	free(script_buffer);
	return 0;
}

static int lua_custom_searcher(lua_State* state)
{
	const char* module_key = luaL_checkstring(state, 1);
	char module_path[1000];
	
	sprintf(module_path,"assets/%s.lua",module_key);
	
	if(!has_raw_resx(get_resx_mngr(),module_path))
		return 1;//1 = not found in path 
	
	return checkload(state, (lua_custom_loader(state, module_key, module_path) == 0), module_key);
}


static pthread_mutex_t running_mutex = PTHREAD_MUTEX_INITIALIZER;

volatile uint32_t suspend_gameloop = 0;
volatile uint32_t destroyegl = 0;
volatile uint32_t createegl = 0;
volatile uint32_t adsinfront = 0;

void lockrunning()
{
	pthread_mutex_lock(&running_mutex);
}

void unlockrunning()
{
	pthread_mutex_unlock(&running_mutex);
}

void config_saved()
{
}

void checkpause()
{
	//check for pause state
	volatile uint32_t local_suspend_gameloop = 0;
	volatile uint32_t local_adsinfront = 0;
	
	atomic_set_value(&local_suspend_gameloop,suspend_gameloop);
	atomic_set_value(&local_adsinfront,adsinfront);
	
	if(local_suspend_gameloop == 1)
	{
		if(strcmp(get_levelmngr()->current_level,level_start) != 0 && get_levelmngr()->current_state != COMMON_PAUSE && local_adsinfront == 0) //if we are on a regular level
		{
			if(!get_module_mngr()->game_disableinput()) //if input are enabled
			{
				screen_fade_dofade(get_screenfade(),500,1,0.8f); //show pause
				set_state(COMMON_PAUSE);
			}
		}
	}
	
	while(local_suspend_gameloop == 1)
	{
		usleep(200000);//wait for 200ms
		volatile uint32_t local_destroygl = 0;
		atomic_set_value(&local_destroygl,destroyegl);
		
		if(local_destroygl == 1)
		{
			//kill native window
			ANativeWindow_release( window );
			window = NULL;
			//destroy textures
			resx_g_clear_texture(get_resx_mngr());
			//destroy font textures
			resx_g_clear_font(get_resx_mngr());
			//game specific cleanup (destroy shader programs here)
			get_module_mngr()->game_g_cleanup();
			//destroy shader
			resx_g_clear_shader(get_resx_mngr());

			Levelobject_g_clean_level(get_levelmngr());

			//screen_fill_g_clean(get_screenfill());

			//destroy egl context and surface
			render_destroyscreen(get_render());
			atomic_set_value(&destroyegl,0);
			//destroyegl = 0;
			logprint("egl context, surface and display destroyed");
		}

		atomic_set_value(&local_suspend_gameloop,suspend_gameloop);
	}
	
	volatile uint32_t local_creategl = 0;
	
	atomic_set_value(&local_creategl,createegl);
	
	if(local_creategl == 1)
	{
		if(window == NULL)
		{
			logprint("can't create egl context without a valid native window!, please call createegl");
		}
		else
		{
			//recreate egl context and surface
			render_recreatescreen(get_render(),wfalse,window);
			//recreate textures
			resx_g_create_texture(get_resx_mngr());
			//recreate font textures
			resx_g_create_font(get_resx_mngr());
			//reload shaders
			resx_g_create_shader(get_resx_mngr());
			//recreate shader programs here
			get_module_mngr()->game_g_create();
			
			refresh_shader_program();
			
			Levelobject_g_create_level(get_levelmngr(),get_render());
			
			//screen_fill_g_create(get_screenfill(),get_render());
		}
		atomic_set_value(&createegl,0);
	}
	
}


void endrenderthread()
{
	pthread_mutex_destroy(&running_mutex);
	pthread_mutex_destroy(&touch_mutex);
	pthread_exit(0);
}

void setcustompkgloader(lua_State* state)
{
	script_setcustompkgloader(state,lua_custom_searcher);
}

void handle_input()
{
	//movement using touch control
	pthread_mutex_lock(&touch_mutex);
	
	input_mngr_handle_input(get_input());
			
	pthread_mutex_unlock(&touch_mutex);
}

static JavaVM* global_jvm;
static jobject java_activity;


static void google_api_show_ads()
{
	JNIEnv* env;
	
	jint rs = (*global_jvm)->AttachCurrentThread(global_jvm,&env,NULL);
	
	if(rs != JNI_OK)
	{
		logprint("Can't attach JVM google_api_show_ads! %d",rs);
		return;
	}

	jclass class_activity = (*env)->GetObjectClass(env, java_activity);
	jmethodID show_ads = (*env)->GetMethodID(env,class_activity, "show_ads", "()V");
	(*env)->CallVoidMethod(env,java_activity,show_ads);
	
	(*global_jvm)->DetachCurrentThread(global_jvm);
}

static void google_api_load_ads()
{
	JNIEnv* env;
	
	jint rs = (*global_jvm)->AttachCurrentThread(global_jvm,&env,NULL);
	
	if(rs != JNI_OK)
	{
		logprint("Can't attach JVM google_api_load_ads! %d",rs);
		return;
	}

	jclass class_activity = (*env)->GetObjectClass(env, java_activity);
	jmethodID load_ads = (*env)->GetMethodID(env,class_activity, "load_ads", "()V");
	(*env)->CallVoidMethod(env,java_activity,load_ads);
	
	(*global_jvm)->DetachCurrentThread(global_jvm);
}

//these functions directly call google api functions in java activity code
static void google_api_show_achievements()
{
	JNIEnv* env;
	
	jint rs = (*global_jvm)->AttachCurrentThread(global_jvm,&env,NULL);
	
	if(rs != JNI_OK)
	{
		logprint("Can't attach JVM google_api_show_achievements! %d",rs);
		return;
	}

	jclass class_activity = (*env)->GetObjectClass(env, java_activity);
	jmethodID show_achiev = (*env)->GetMethodID(env,class_activity, "show_achievements", "()V");
	(*env)->CallVoidMethod(env,java_activity,show_achiev);
	
	(*global_jvm)->DetachCurrentThread(global_jvm);
}

static void google_api_unlockachievement(const char* achiev_id)
{
	JNIEnv* env;
	
	jint rs = (*global_jvm)->AttachCurrentThread(global_jvm,&env,NULL);
	
	if(rs != JNI_OK)
	{
		logprint("Can't attach JVM google_api_unlockachievement! %d",rs);
		return;
	}

	jclass class_activity = (*env)->GetObjectClass(env, java_activity);
	
	jmethodID unlock_achiev = (*env)->GetMethodID(env,class_activity, "unlock_achievement", "(Ljava/lang/String;)V");
	
	jstring achiev_jid = (*env)->NewStringUTF(env, achiev_id);
	(*env)->CallVoidMethod(env,java_activity,unlock_achiev,achiev_jid);
	
	(*global_jvm)->DetachCurrentThread(global_jvm);
}

void* gameloop_ptr(void* unused)
{
	//set path
	init_path();
    
    //init resources manager
    logprint("Init resx manager...");
	init_resx();
    
    //read configuration
	configure();
    
    //init game module path
    logprint("Init game module...");
	char game_module[128];

	if(config_getstring(get_config(),"gamemodule",&game_module[0],128))
		init_gamemodule_path(game_module);
	else
	{
		logprint("No gamemodule set! aborting...");
		return 0;
	}
	
	
    
    char game_name[128];

	w_sprintf(game_name,128,"Wafa template");

	config_getstring(get_config(),"gamename",&game_name[0],128);
	
	
	//initialize render window
    logprint("Init render window...");
	get_render()->zoom_factor = 1.0f;
	init_render(NULL,window,game_name,wfalse);
	
	input_mngr_init(get_input(),get_render(),get_module_mngr(),1.0f,wfalse,wfalse,wfalse);

	//initialise manager (resources / physics / etc.)
    logprint("Init managers...");
	init_mngr();
	
	//set google api functions pointers
	get_module_mngr()->game_setexternapifunc(google_api_unlockachievement,google_api_show_achievements,google_api_show_ads,google_api_load_ads);
	
	init_text();

    logprint("End main init");
	if(config_getstring(get_config(),"start_level",&level_start[0],256))
	{
		load_level(level_start);
	}
	else
	{
		logprint("Can't load start_level! please check config");
	}

	gameloop();
	
	return NULL;
}  

JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM *jvm, void *reserved) {
	
	JNIEnv *env;
	
	if ((*jvm)->GetEnv(jvm, (void**) &env, JNI_VERSION_1_6) != JNI_OK)
		return -1;


	return JNI_VERSION_1_6;
}


JNIEXPORT void JNICALL Java_wafa_game_MainActivity_initengine(JNIEnv *env,jobject self,jobject surface,jstring apkPath,jstring libpath,jstring filepath,jstring locale) {
	
	 jint rs = (*env)->GetJavaVM(env,&global_jvm);
	 
	 if(rs != JNI_OK)
	 {
		logprint("Can't get java VM!");
		return;
	}
	
	java_activity = (*env)->NewGlobalRef(env,self);

	logprint("get window reference");
	//get the surface reference from java
	window = ANativeWindow_fromSurface(env, surface);
	
	const char* str = (*env)->GetStringUTFChars(env,apkPath, 0);
	 
	set_app_path(str);
	
	(*env)->ReleaseStringUTFChars(env, apkPath, str);
	
	const char* str2 = (*env)->GetStringUTFChars(env,libpath, 0);
	 
	set_lib_path(str2);
	
	(*env)->ReleaseStringUTFChars(env, libpath, str2);
	
	const char* str3 = (*env)->GetStringUTFChars(env,filepath,0);
	
	set_db_path(str3);
	
	set_save_path(str3);
	
	set_config_path(str3);
	
    (*env)->ReleaseStringUTFChars(env, filepath, str3);
	
	const char* str4 =  (*env)->GetStringUTFChars(env,locale,0);
	
	set_locale(str4);
	
	(*env)->ReleaseStringUTFChars(env, locale, str4);
	
	if(window == NULL)
	{
		logprint("Can't get native window from surface!");
		return;
	}

	
	logprint("start thread");
	//start thread for gameloop
	 pthread_create(&gameloop_thread,NULL,gameloop_ptr,NULL);
}


JNIEXPORT void JNICALL Java_wafa_game_MainActivity_stopengine(JNIEnv *env,jobject self)
{
	pthread_mutex_lock(&running_mutex);
	end_run();
	eglMakeCurrent (get_render()->oglDisplay, EGL_NO_SURFACE, EGL_NO_SURFACE,EGL_NO_CONTEXT);
	snd_mngr_setvolume(get_soundmngr(),0,0);
	pthread_mutex_unlock(&running_mutex);
	
	(*env)->DeleteGlobalRef(env,java_activity);
	
	logprint("stop engine");
	
	atomic_set_value(&suspend_gameloop,0);
	
}

JNIEXPORT void JNICALL Java_wafa_game_MainActivity_createegl(JNIEnv* env,jobject self,jobject surface)
{
	if(window != NULL)
	{
		logprint("native window already exist! call destroyegl before createegl");
		return;
	}
	
	window = ANativeWindow_fromSurface(env, surface);
	atomic_set_value(&createegl,1);
}

JNIEXPORT jint JNICALL Java_wafa_game_MainActivity_checkdestroyegl(JNIEnv* env,jobject self)
{
	volatile uint32_t local_destroystate = 0;
	atomic_set_value(&local_destroystate,destroyegl);
	

	return local_destroystate;
}

JNIEXPORT void JNICALL Java_wafa_game_MainActivity_destroyegl(JNIEnv* env,jobject self)
{
	atomic_set_value(&destroyegl,1);

}

JNIEXPORT void JNICALL Java_wafa_game_MainActivity_pauseengine(JNIEnv *env,jobject self)
{
	atomic_set_value(&suspend_gameloop,1);
	
	if(get_soundmngr()->stream_opened)
		snd_mngr_setvolume(get_soundmngr(),0,0);
		
	logprint("pause engine");
}

JNIEXPORT void JNICALL Java_wafa_game_MainActivity_adsvisible(JNIEnv* env,jobject self,jint visible)
{
	if(visible == 1)
		atomic_set_value(&adsinfront,1);
	else
		atomic_set_value(&adsinfront,0);
	
}

JNIEXPORT void JNICALL Java_wafa_game_MainActivity_resumeengine(JNIEnv *env,jobject self)
{
	if(get_soundmngr()->stream_opened)
	{
		int32_t music_vol = 0;
		int32_t sfx_vol = 0;
		config_getint(get_config(),"music_volume",&music_vol);
		config_getint(get_config(),"sfx_volume",&sfx_vol);
		snd_mngr_setvolume(get_soundmngr(),music_vol,sfx_vol);
	}
	
	atomic_set_value(&suspend_gameloop,0);
	atomic_set_value(&adsinfront,0);

	logprint("resume engine");
}

JNIEXPORT void Java_wafa_game_MainActivity_setaudiobuffer(JNIEnv *env,jobject self,jint audio_buffersize)
{
	set_audio_buffersize(audio_buffersize);
}