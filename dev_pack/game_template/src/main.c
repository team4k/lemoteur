#include <common_include.h>


wbool has_focus = wtrue;
wbool lock_cursor;

float mouse_sensitivity = 1.0f;
char level_start[256];


void lockrunning()
{

}

void unlockrunning()
{
	
}

void config_saved()
{
	wbool fullscreen = wfalse;
	config_getbool(get_config(),"fullscreen",&fullscreen);        
	config_getfloat(get_config(),"mouse_sensitivity",&mouse_sensitivity);

	if(fullscreen != get_render()->fullscreen)
	{
		resx_g_clear_texture(get_resx_mngr());
		resx_g_clear_font(get_resx_mngr());
		get_module_mngr()->game_g_cleanup();

        if(get_stream_mode())
        {

        }
        else
        {
            Levelobject_g_clean_level((Levelobject_t*)get_levelmngr());
        }
		

		render_recreatescreen(get_render(),fullscreen,NULL);

		resx_g_create_texture(get_resx_mngr());
		resx_g_create_font(get_resx_mngr());

         if(get_stream_mode())
        {

        }
        else
        {
		    Levelobject_g_create_level((Levelobject_t*)get_levelmngr(),get_render());
        }

		get_module_mngr()->game_g_create();

		//TODO rebind input events
		/*glfwSetMouseButtonCallback(get_render()->oglWindow,Mouse_callback_click);
		glfwSetCursorPosCallback(get_render()->oglWindow,Mouse_callback_move);
		glfwSetWindowCloseCallback(get_render()->oglWindow,Window_close_callback);
		glfwSetWindowFocusCallback(get_render()->oglWindow,Window_focus_callback);*/

	}
}

void checkpause()
{
}

void endrenderthread()
{
	
}


//lua custom script loading function
static int checkload (lua_State *L, int stat, const char *filename) {
  if (stat) {  /* module loaded successfully? */
    w_lua_pushstring(L, filename);  /* will be 2nd argument to module */
    return 2;  /* return open function and file name */
  }
  else
    return w_luaL_error(L, "error loading module %s from file %s : \n\t%s",
                          w_lua_tostring(L, 1), filename, w_lua_tostring(L, -1));
}

static int lua_custom_loader(lua_State* state,const char* module_key,const char* module_path)
{
	long script_size = getrawdata_size(get_resx_mngr(),module_path);
	unsigned char* script_buffer = (unsigned char*)wf_malloc(script_size * sizeof(unsigned char));
	getrawdata(get_resx_mngr(),module_path,script_buffer,script_size);
	w_luaL_loadbuffer(state,(const char*)script_buffer,script_size,module_key) ;
	free(script_buffer);
	return 0;
}

static int lua_custom_searcher(lua_State* state)
{
	const char* module_key = w_luaL_checkstring(state, 1);
	char module_path[256] = { 0 };
	
	w_sprintf(module_path,256,"scripts/%s.lua",module_key);
	
	if(!has_raw_resx(get_resx_mngr(),module_path))
		return 1;//1 = not found in path 
	
	return checkload(state, (lua_custom_loader(state, module_key, module_path) == 0), module_key);
}

void setcustompkgloader(lua_State* state)
{
	script_setcustompkgloader(state,lua_custom_searcher);
}


int main( void )
{
	#ifdef MEM_TRACE
		init_mem_trace();
	#endif

	init_path();

	//load configuration
	configure();

	//init game module path

	char game_module[128];

	if(config_getstring(get_config(),"gamemodule",&game_module[0],128))
		init_gamemodule_path(game_module);
	else
	{
		logprint("No gamemodule set! aborting...");
		return 0;
	}

        
    //initialize resources manager
	init_resx();


	wbool fullscreen = wfalse;
	config_getfloat(get_config(),"mouse_sensitivity",&mouse_sensitivity);
	config_getbool(get_config(),"fullscreen",&fullscreen);
	config_getbool(get_config(),"lock_cursor",&lock_cursor);

	int16_t screen_w = 1024;
	int16_t screen_h = 768;
	
	config_getshort(get_config(),"res_x",&screen_w);
	config_getshort(get_config(),"res_y",&screen_h);

	set_screen_size(screen_w,screen_h);

	char game_name[128];

	w_sprintf(game_name,128,"Wafa template");

	config_getstring(get_config(),"gamename",&game_name[0],128);

	//initialize render window
	init_render(NULL,NULL,game_name,fullscreen);

	//initialize input
	input_mngr_init(get_input(),get_render(),get_module_mngr(),mouse_sensitivity,lock_cursor,wfalse,wtrue);

	#ifdef EMSCRIPTEN
		emscripten_run_script("showGame()");
	#endif

	//windows specific icon set
	/*#if defined(_WIN32)
		HWND handle = glfwGetWin32Window(get_render()->oglWindow);

		HICON icon = LoadIcon((HINSTANCE)GetWindowLong(handle, GWL_HINSTANCE),MAKEINTRESOURCE(101));

		SendMessage(handle,WM_SETICON,ICON_SMALL,(LPARAM)icon);
		SendMessage(handle,WM_SETICON,ICON_BIG,(LPARAM)icon);
	#endif*/

	set_audio_buffersize(16384);
	
	//initialize manager
	init_mngr();

	//initialize global informations text
	init_text();


	#ifdef MEM_TRACE
		snapshot_mem_trace("Managers");
	#endif

	
	wbool stream_mode = wfalse;
	config_getbool(get_config(), "stream_mode", &stream_mode);


	if (!stream_mode)
	{
		if (config_getstring(get_config(), "start_level", &level_start[0], 256))
		{
			load_level(level_start);
		}
		else
		{
			logprint("Can't load start_level! please check config");
		}
	}


	gameloop();

	#ifdef MEM_TRACE
		shutdown_mem_trace();
	#endif

   return 0;

}
