cd ..\animation_converter
REM first param = animated assets input dir, second param = static 	asset input dir, third param = assets output dir, fourth param = scripts output dir, fifth param = exclude filename config file
@AnimationConverter.exe ..\game_template\assets\animated_assets ..\game_template\assets ..\game_template\bin\assets ..\game_template\bin\scripts ..\game_template\assets\exclude_pc.txt

cd ..
copy /Y game_template\assets\config.lua game_template\bin\Debug\config.lua
copy /y game_template\assets\config.lua game_template\bin\Release\config.lua
pause