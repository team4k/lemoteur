LOCAL_PATH := $(call my-dir)

#sndfile dependency
include $(CLEAR_VARS)
LOCAL_MODULE := sndfile
LOCAL_SRC_FILES := ../../../libs-android/libsndfile.so
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/../../../depends_inc/libsndfile
include $(PREBUILT_SHARED_LIBRARY)

#wafa dependency
include $(CLEAR_VARS)
LOCAL_MODULE := wafaengine
LOCAL_SRC_FILES := ../../../libs-android/libWafaEngine.so
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/../../../include
include $(PREBUILT_SHARED_LIBRARY)

#build game dll
include $(CLEAR_VARS)
LOCAL_MODULE := dummygame
LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../../depends_inc/lua $(LOCAL_PATH)/../../../depends_inc/Chipmunk $(LOCAL_PATH)/../../../depends_inc/libpng $(LOCAL_PATH)/../../../depends_inc/libsndfile $(LOCAL_PATH)/../../../depends_inc/portaudio $(LOCAL_PATH)/../../../depends_inc/sqlite $(LOCAL_PATH)/../../../depends_inc/protobuf $(LOCAL_PATH)/..
LOCAL_SHARED_LIBRARIES := wafaengine
LOCAL_SRC_FILES := ../../dummygame/Gamemain.c ../../dummygame/script_funcs.c
LOCAL_CFLAGS := -DRENDERER_OPENGL_ES -DSTRIP_EDIT_CODE -DANDROID_BUILD -DCP_USE_DOUBLES=0 -DCP_ALLOW_PRIVATE_ACCESS=1 -DNDEBUG=1 -DUSE_MOBILE_INPUT
LOCAL_LDLIBS := -lGLESv2

include $(BUILD_SHARED_LIBRARY)

#build bridge c code
include $(CLEAR_VARS)
LOCAL_MODULE := wafabridge
LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../../depends_inc/lua $(LOCAL_PATH)/../../../depends_inc/Chipmunk $(LOCAL_PATH)/../../../libpng $(LOCAL_PATH)/../../../include $(LOCAL_PATH)/../../../depends_inc/libsndfile $(LOCAL_PATH)/../../../depends_inc/portaudio $(LOCAL_PATH)/../../../depends_inc/sqlite $(LOCAL_PATH)/../../../depends_inc/protobuf $(LOCAL_PATH)/../../include
LOCAL_SHARED_LIBRARIES := wafaengine sndfile
LOCAL_SRC_FILES := ../../src/common.c ../../src/main_android.cpp

LOCAL_CFLAGS := -DRENDERER_OPENGL_ES -DSTRIP_EDIT_CODE -DANDROID_BUILD -DANDROID_NDK -DCP_USE_DOUBLES=0 -DCP_ALLOW_PRIVATE_ACCESS=1 -DUSE_MOBILE_INPUT
LOCAL_LDLIBS := -lGLESv2 -ldl -landroid -lEGL

include $(BUILD_SHARED_LIBRARY)



