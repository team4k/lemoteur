if(WINDOWSPHONE)
    option(USE_D3D "build with direct3D renderer" ON)
    option(USE_OPENGL "build with opengl renderer" OFF)
else()
    option(USE_D3D "build with direct3D renderer" OFF)
    #by default, use opengl
    option(USE_OPENGL "build with opengl renderer" ON)
endif()

option(USE_DEBUG_DEPEND "build with debug libs" OFF)

#
# game DLL
#
project(dummygame)
cmake_minimum_required( VERSION 3.1 )

file(GLOB_RECURSE dummy_src_files "dummygame/*.c")

include_directories("dummygame" "../include" "../depends_inc/lua" "../depends_inc/protobuf" "../depends_inc/libsndfile" "../depends_inc/protobuf_c" "../depends_inc/libpng" "../depends_inc/sqlite")


if(MSVC)
	include_directories("../include-msvc")
	include_directories("../depends_inc/glewwin")
else()
	include_directories("../depends_inc/glew")
endif()

if(EMSCRIPTEN)
	include_directories("../depends_inc/ChipmunkEmscripten")
	list(APPEND PREDEF USE_SDL_INPUT)
    list(APPEND PREDEF USE_SDL_RENDER)
	list(APPEND PREDEF MATH_DOUBLE)
	list(APPEND PREDEF STRIP_EDIT_CODE)
	list(APPEND PREDEF ZIP_STATIC)
else()
	include_directories("../depends_inc/Chipmunk")
	list(APPEND PREDEF USE_GLFW_INPUT)
	list(APPEND PREDEF USE_GLFW_RENDER)
endif()


if(USE_OPENGL)
	if(EMSCRIPTEN)
		list(APPEND PREDEF RENDERER_OPENGL_ES)
	else()
		list(APPEND PREDEF RENDERER_OPENGL)
	endif()
endif()


#build a shared library for all platform except emscripten and iOS
if(EMSCRIPTEN OR IOS)
    add_library(dummygame ${dummy_src_files})
else()
    add_library(dummygame SHARED ${dummy_src_files})
endif()

if(EMSCRIPTEN)
	#target_compile_options(gamegame PUBLIC "-O2")
	SET( CMAKE_C_FLAGS  "${CMAKE_C_FLAGS} -O2 -s USE_SDL=2 -std=gnu99 -s FORCE_FILESYSTEM=1" )
endif()

if(MSVC)
	set(wafa_lib_root "../libs-msvc")
endif()
if(UNIX AND NOT APPLE AND NOT ANDROID AND NOT EMSCRIPTEN)
	set(wafa_lib_root "../libs-linux")
    if(USE_DEBUG_DEPEND)
        set(wafa_lib_root "../libs-linux-debug")
    endif()
endif()												 

target_compile_definitions(dummygame PUBLIC ${PREDEF})
	 
if(NOT EMSCRIPTEN AND NOT IOS)
    FIND_LIBRARY(WAFAENGINE_LIB NAMES WafaEngine PATHS ${wafa_lib_root} NO_DEFAULT_PATH)
	list(APPEND LIBS_OBJ ${WAFAENGINE_LIB})
	target_link_libraries(dummygame ${LIBS_OBJ})
endif()


#
# Game exe
#
project(game)
cmake_minimum_required( VERSION 3.1 )


list(APPEND game_src_files "src/common.c")

include_directories("include" "../include" "../depends_inc/lua" "../depends_inc/libpng" "../depends_inc/glfw"  "../depends_inc/sqlite" "../depends_inc/protobuf")


if(MSVC)
	include_directories("../include-msvc")
	include_directories("../depends_inc/glewwin")
else()
    include_directories("../depends_inc/glew")										  
endif()

if(EMSCRIPTEN)
	include_directories("../depends_inc/ChipmunkEmscripten")
	list(APPEND PREDEF STRIP_EDIT_CODE)
	list(APPEND PREDEF ZIP_STATIC)
	list(APPEND PREDEF RENDERER_OPENGL_ES)
	list(APPEND PREDEF MATH_DOUBLE)
	set(wafa_lib_root "../libs-emscripten")
	list(APPEND PREDEF USE_SDL_INPUT)
    list(APPEND PREDEF USE_SDL_RENDER)
else()
	list(APPEND PREDEF USE_GLFW_INPUT)
	list(APPEND PREDEF USE_GLFW_RENDER)
	include_directories("../depends_inc/Chipmunk")
	include_directories("../depends_inc/portaudio")
endif()

if(ANDROID)
	list(APPEND game_src_files "src/main_android.cpp")
else()
	list(APPEND game_src_files "src/main.c")
endif()

if(MSVC)
	set(wafa_lib_root "../libs-msvc")
endif()

if(UNIX AND NOT APPLE AND NOT ANDROID AND NOT EMSCRIPTEN)
	set(wafa_lib_root "../libs-linux")
    if(USE_DEBUG_DEPEND)
        set(wafa_lib_root "../libs-linux-debug")
    endif()
endif()
if(USE_OPENGL)
	if(EMSCRIPTEN)
		list(APPEND PREDEF RENDERER_OPENGL_ES)
	else()
		list(APPEND PREDEF RENDERER_OPENGL)
	endif()
endif()


add_executable(game ${game_src_files})

target_compile_definitions(game PUBLIC ${PREDEF})

if(EMSCRIPTEN)
	#target_compile_options(game PUBLIC "-O2")
	SET( CMAKE_C_FLAGS  "${CMAKE_C_FLAGS} -std=gnu99 -s FORCE_FILESYSTEM=1 -O2 -s USE_SDL=2 -s LINKABLE=1 -s EXPORT_ALL=1 -s SAFE_HEAP=1 -s ASSERTIONS=2 -s TOTAL_MEMORY=67108864 -s NO_EXIT_RUNTIME=1 -s MAX_WEBGL_VERSION=2 -s USE_WEBGL2=1  -s FULL_ES3=1" )
	
	#set_target_properties(game PROPERTIES LINK_FLAGS "")
endif()


if(ANDROID OR EMSCRIPTEN OR IOS)
     get_filename_component(DUMMYGAME_LIB libdummygame${CMAKE_STATIC_LIBRARY_SUFFIX} ABSOLUTE)
	 get_filename_component(WAFAENGINE_LIB ${wafa_lib_root}/libWafaEngine${CMAKE_STATIC_LIBRARY_SUFFIX} ABSOLUTE)
     get_filename_component(SQLITE_LIB ${wafa_lib_root}/libsqlite3-static${CMAKE_STATIC_LIBRARY_SUFFIX} ABSOLUTE)
     get_filename_component(ZIP_LIB ${wafa_lib_root}/libzip${CMAKE_STATIC_LIBRARY_SUFFIX} ABSOLUTE)
     get_filename_component(LUA_LIB ${wafa_lib_root}/liblua${CMAKE_STATIC_LIBRARY_SUFFIX} ABSOLUTE)
     get_filename_component(FREETYPE_LIB ${wafa_lib_root}/libfreetype${CMAKE_STATIC_LIBRARY_SUFFIX} ABSOLUTE)
     get_filename_component(CHIPMUNK_LIB ${wafa_lib_root}/libchipmunk${CMAKE_STATIC_LIBRARY_SUFFIX} ABSOLUTE)
     get_filename_component(PROTO_LIB ${wafa_lib_root}/libprotobuf-c${CMAKE_STATIC_LIBRARY_SUFFIX} ABSOLUTE)
	 get_filename_component(PNG_LIB ${wafa_lib_root}/libpng16${CMAKE_STATIC_LIBRARY_SUFFIX} ABSOLUTE)

	 
	if(EMSCRIPTEN)
		list(APPEND LIBS_OBJ ${DUMMYGAME_LIB})
		list(APPEND LIBS_OBJ ${WAFAENGINE_LIB})
	endif()
	
     
     #android build use built-in zlib
	if(ANDROID)
		set(ZLIB_LIB z)
    else()
		get_filename_component(ZLIB_LIB ${wafa_lib_root}/libz${CMAKE_STATIC_LIBRARY_SUFFIX} ABSOLUTE)
	endif()
     #no libsndfile for iOS
    if(ANDROID)
        get_filename_component(SNDFILE_LIB ${wafa_lib_root}/libsndfile.so ABSOLUTE)
    elseif(EMSCRIPTEN)
	    get_filename_component(OGG_LIB ${wafa_lib_root}/libogg${CMAKE_STATIC_LIBRARY_SUFFIX} ABSOLUTE)
		list(APPEND LIBS_OBJ ${OGG_LIB})
	    get_filename_component(VORBIS_LIB ${wafa_lib_root}/libvorbis${CMAKE_STATIC_LIBRARY_SUFFIX} ABSOLUTE)
		list(APPEND LIBS_OBJ ${VORBIS_LIB})
        get_filename_component(SNDFILE_LIB ${wafa_lib_root}/libsndfile${CMAKE_STATIC_LIBRARY_SUFFIX} ABSOLUTE)
    endif()
	
	list(APPEND LIBS_OBJ ${SQLITE_LIB})
	list(APPEND LIBS_OBJ ${ZIP_LIB})
	list(APPEND LIBS_OBJ ${ZLIB_LIB})
	list(APPEND LIBS_OBJ ${LUA_LIB})
	list(APPEND LIBS_OBJ ${FREETYPE_LIB})
	list(APPEND LIBS_OBJ ${CHIPMUNK_LIB})
	list(APPEND LIBS_OBJ ${PROTO_LIB})
	list(APPEND LIBS_OBJ ${PNG_LIB})
else()
	  FIND_LIBRARY(WAFAENGINE_LIB NAMES WafaEngine PATHS ${wafa_lib_root} NO_DEFAULT_PATH)
	  FIND_LIBRARY(SNDFILE_LIB NAMES libsndfile sndfile PATHS ${wafa_lib_root} NO_DEFAULT_PATH)
	  #FIND_LIBRARY(DUMMYGAME_LIB NAMES dummygame PATHS "." NO_DEFAULT_PATH)
endif()


list(APPEND LIBS_OBJ ${SNDFILE_LIB})

if(NOT EMSCRIPTEN)
	  list(APPEND LIBS_OBJ ${DUMMYGAME_LIB})
      list(APPEND LIBS_OBJ ${WAFAENGINE_LIB})
endif()




target_link_libraries(game ${LIBS_OBJ})


