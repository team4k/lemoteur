LOCAL_PATH := $(call my-dir)

#wafa dependency
include $(CLEAR_VARS)
LOCAL_MODULE := wafaengine
LOCAL_SRC_FILES := ../../../libs-android/libWafaEngine.so
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/../../../include
include $(PREBUILT_SHARED_LIBRARY)

#include $(CLEAR_VARS)
#LOCAL_MODULE    := GLES_mgd
#LOCAL_SRC_FILES := ../libGLES_mgd.so
#include $(PREBUILT_SHARED_LIBRARY)

#build bridge c code
include $(CLEAR_VARS)
LOCAL_MODULE := wafabridge
LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../../depends_inc/lua $(LOCAL_PATH)/../../../depends_inc/Chipmunk $(LOCAL_PATH)/../../../libpng $(LOCAL_PATH)/../../../include $(LOCAL_PATH)/../../../depends_inc/libsndfile $(LOCAL_PATH)/../../../depends_inc/portaudio $(LOCAL_PATH)/../../../depends_inc/sqlite $(LOCAL_PATH)/../../../depends_inc/protobuf $(LOCAL_PATH)/../../include
LOCAL_SHARED_LIBRARIES := wafaengine
LOCAL_SRC_FILES := ../../src/common.c ../../src/main_android.c

LOCAL_CFLAGS := -std=c99 -DRENDERER_OPENGL_ES -DSTRIP_EDIT_CODE -DANDROID_BUILD -DANDROID_NDK -DCP_USE_DOUBLES=0 -DCP_ALLOW_PRIVATE_ACCESS=1 -DUSE_MOBILE_INPUT
LOCAL_LDLIBS := -lGLESv2 -ldl -landroid -lEGL

include $(BUILD_SHARED_LIBRARY)



