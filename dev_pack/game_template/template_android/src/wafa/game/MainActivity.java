package wafa.game;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import java.util.Locale;
import android.support.v4.view.MotionEventCompat;

import com.google.android.gms.ads.*;
import com.google.android.gms.common.*;



public class MainActivity extends Activity
{
   private eglSurfaceView oglSurface;
    public String apkPath = null;
    public String libPath = null;
    public String filePath = null;
    public String locale = null;
    
    public boolean gamepaused = false;
    public boolean windowsFocused = false;
    
    public boolean adsAskedLoad = false;
    
    private InterstitialAd interstitial;
    private AdRequest adRequest;
    
    public boolean itchioversion = true;
    
     //declare jni functions to access our native c library
    public native void initengine(Surface oSurface,String apkPath,String libpath,String filepath,String locale);
    public native void setaudiobuffer(int audio_buffersize);
    public native void stopengine();
    public native void destroyegl();
    public native int checkdestroyegl();
    public native void createegl(Surface oSurface);
    
    private native void pauseengine();
    public native void resumeengine();
    public native void adsvisible(int visible);
    
    private native void touchscreen(float posx,float posy,int action);
    
    private native void backbutton();
    
   private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    
    
   private View.OnTouchListener mTouchListener = new View.OnTouchListener() {
		
		public boolean onTouch(View v,MotionEvent event)
		{
                    int action = MotionEventCompat.getActionMasked(event);
                    int index = MotionEventCompat.getActionIndex(event);
                    
                    touchscreen(MotionEventCompat.getX(event, index),MotionEventCompat.getY(event, index),action);
                    Log.d("WAFA", "action" + event.getAction());
                    return true;
                }      
              
   };
   
   
   
     @Override
    protected void onCreate(Bundle savedInstanceState) {
         //load native libraries
        System.loadLibrary("sndfile");//sound file loader (dynamic linking because of LGPL license)
        System.loadLibrary("wafaengine"); //game engine
        System.loadLibrary("wafabridge"); //gameloop / jni bridge
        
        //System.loadLibrary("GLES_mgd");//Mali debugger lib

         
        super.onCreate(savedInstanceState);

        ApplicationInfo appInfo = null;
        PackageManager packMgmr = this.getPackageManager();
        
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        
        //set android specific configuration
     
        int buffer_size =  AudioTrack.getMinBufferSize(44100, AudioFormat.CHANNEL_CONFIGURATION_STEREO, AudioFormat.ENCODING_PCM_16BIT);
        
        setaudiobuffer(buffer_size);
        
        try {
	    appInfo = packMgmr.getApplicationInfo("wafa.game", 0);
        } catch (NameNotFoundException e) {
                e.printStackTrace();
                throw new RuntimeException("Unable to locate assets, aborting...");
        }
        
         requestWindowFeature(Window.FEATURE_NO_TITLE);
         getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
         getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        
        apkPath = appInfo.sourceDir;
        libPath = appInfo.nativeLibraryDir;
        filePath = this.getFilesDir().getAbsolutePath();
        locale = Locale.getDefault().getLanguage();
        

        
        oglSurface = new eglSurfaceView(this.getApplicationContext(),this);
        oglSurface.setOnTouchListener(mTouchListener);
        setContentView(oglSurface);
        gamepaused = false;
        
        if(!itchioversion)
        {
            int service_available = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

            if(service_available == ConnectionResult.SUCCESS)
            {
                // Créez l'interstitiel.
               interstitial = new InterstitialAd(this);
               interstitial.setAdUnitId("ca-app-pub-6335120819045352/8295032821");

               // Créez la demande d'annonce.
               adRequest = new AdRequest.Builder()
                       .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                       .addTestDevice("382574A921A15732368A26EA122EBA04")
                       .addTestDevice("A4040E77335F596CC030C501253E1C8F")
                       .build();

                //Lancez le chargement de l'interstitiel.
                interstitial.loadAd(adRequest);
                adsAskedLoad = true;
            }
            else
            {
                 if (GooglePlayServicesUtil.isUserRecoverableError(service_available)) 
                 {
                    GooglePlayServicesUtil.getErrorDialog(service_available, this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
                 }
            }
        }
        
    }
     
     @Override
     public void onBackPressed()
     {
         backbutton();
     }
     
     @Override
     protected void onStart()
     {
         super.onStart();
     }
     
     @Override
    public void onAttachedToWindow()
    {
        super.onAttachedToWindow();
    }

    @Override
    protected void onPause() {
        super.onPause();
        
        if(!gamepaused)
        {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            pauseengine();
            gamepaused = true;
        }
    }
    
    @Override
     public void onWindowFocusChanged(boolean hasFocus)
     {
         super.onWindowFocusChanged(hasFocus);
         
         Log.d("WAFA", "onWindowsFocusChanged");
         this.windowsFocused = hasFocus;
         
          if(hasFocus && gamepaused)
          {
              getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
              
              resumeengine();
              gamepaused = false;
              
          }
          
          if(!hasFocus && !gamepaused)
          {
              getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
              pauseengine();
              gamepaused = true;
          }
              
     }
    

    @Override
    protected void onResume() {
        super.onResume();
        
        if(gamepaused)
        {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            
            Log.d("WAFA", "onResume");
            if(!oglSurface.surfaceDestroyed && this.windowsFocused)
            {
                resumeengine();
                gamepaused = false;
            }
        }
    }
     @Override
    protected void onDestroy() {
         super.onDestroy();
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
         stopengine();

     }
     
     
     //google api call
     public void show_achievements() {
     }
     
     public void unlock_achievement(String achievement_id)
     {
     }
     
     public void showInterstitial()
     {
         if(itchioversion)
             return;
         
        if (interstitial != null && interstitial.isLoaded()) {
            adsvisible(1);
            interstitial.show();
            adsAskedLoad = false;
         }
     }
     
     public void loadInterstitial()
     {
          if(itchioversion)
             return;
         
         if(interstitial != null && !adsAskedLoad)
         {
            interstitial.loadAd(adRequest);
            adsAskedLoad = true;
         }
     }
     
     public void load_ads()
     {
          if(itchioversion)
             return;
         
         final MainActivity ref = this;
         this.runOnUiThread(new Runnable() {
            @Override public void run() {
                ref.loadInterstitial();
            }
        });
     }
     
     public void show_ads() {
          if(itchioversion)
             return;
         
         final MainActivity ref = this;
         this.runOnUiThread(new Runnable() {
            @Override public void run() {
                ref.showInterstitial();
            }
        });
     }
     
     
}
