        /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wafa.game;

import android.content.Context;
import android.graphics.Canvas;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.SlidingDrawer;
    
public class eglSurfaceView extends SurfaceView implements SurfaceHolder.Callback {
    
    private MainActivity _activityref;
    
    private boolean EngineInitialized = false;
    public boolean surfaceDestroyed = false;
    
    public boolean resumeaftersurface = false;
    
    public SurfaceHolder mHolder;
    
 public eglSurfaceView(Context context,MainActivity activityref) {
    super(context);
    _activityref = activityref;
    mHolder = getHolder();
    mHolder.addCallback(this);
   
}
   



public void surfaceChanged(SurfaceHolder holder, int format, int width,
        int height) {
   Log.d("WAFA", "Surface changed");
   
}

public void surfaceCreated(SurfaceHolder holder) {
    
        if(!EngineInitialized)
        {
            _activityref.initengine(mHolder.getSurface(), _activityref.apkPath,_activityref.libPath,_activityref.filePath,_activityref.locale);
            EngineInitialized = true;
        }
        else if(this.surfaceDestroyed)
        {
            _activityref.createegl(mHolder.getSurface());
            this.surfaceDestroyed = false;
        }
        
        Log.d("WAFA", "Surface created");
}

public void surfaceDestroyed(SurfaceHolder holder) {
    //ask the native thread to destroy gl data
    _activityref.destroyegl();
    
    //wait for the native part of the surface to be destroyed
    while(_activityref.checkdestroyegl() == 1)
    {
        try 
        {
            Thread.sleep(1);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    this.surfaceDestroyed = true;
    Log.d("WAFA", "Surface destroyed");
}   
}
