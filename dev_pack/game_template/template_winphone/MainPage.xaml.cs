﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Core;
using WafaBridge;
using Windows.UI.ViewManagement;
using System.Threading.Tasks;
using Windows.UI.Input;
using Windows.Phone.UI.Input;
using Microsoft.Advertising.WinRT.UI;


namespace Reckpunk_app
{
    /// <summary>
    ///Main xaml page containing our renderpanel
    /// </summary>
    public sealed partial class MainPage : Page
    {
        //private SomaInterstitialAd _ads;
        private InterstitialAd _msads;

        private bool _adsAskedLoad = false;
        private bool _adsIsLoaded = false;
        private DispatcherTimer dispatcherTimer;
        private bool doResumeGame = false;

        // private string IA_APPID = "d25517cb-12d4-4699-8bdc-52040c712cab";
        //private string IA_ADUNITID = "11389925";
        private string IA_APPID = "2e645a9f-e04d-4547-9af3-664ebb1bf0e0";
        private string IA_ADUNITID = "11613423";

        public MainPage()
        {
            this.InitializeComponent();

            _msads = new InterstitialAd();
            _msads.AdReady += _msads_AdReady;
            _msads.Cancelled += _msads_Cancelled;
            _msads.Completed += _msads_Completed;
            _msads.ErrorOccurred += _msads_ErrorOccurred;
            _msads.RequestAd(AdType.Video, IA_APPID, IA_ADUNITID);
            _adsAskedLoad = true;


            dispatcherTimer =  new DispatcherTimer();
            dispatcherTimer.Tick += DispatcherTimer_Tick;

            //check for ads request every seconds
            TimeSpan time = new TimeSpan(1000);
            dispatcherTimer.Interval = time;

            dispatcherTimer.Start();

            HardwareButtons.BackPressed += HardwareButtons_BackPressed;

            this.NavigationCacheMode = NavigationCacheMode.Required;
        }

        private void _msads_ErrorOccurred(object sender, AdErrorEventArgs e)
        {
            //eventually log why the ad failed
        }

        private void _msads_Completed(object sender, object e)
        {
            oWafaBridge.ResumeGame();
        }

        private void _msads_Cancelled(object sender, object e)
        {
            oWafaBridge.ResumeGame();
        }

        private void _msads_AdReady(object sender, object e)
        {
            _adsIsLoaded = true;
        }

        private void DispatcherTimer_Tick(object sender, object e)
        {
            if (doResumeGame)
            {
                doResumeGame = !oWafaBridge.ResumeGame();
            }

            //handle ads requests
            if (oWafaBridge.DoShowAds)
            {
                showInterstitial();
                oWafaBridge.DoShowAds = false;
            }

            if(oWafaBridge.DoLoadAds)
            {
                loadInterstitial();
                oWafaBridge.DoLoadAds = false;
            }
            
        }

        private void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            e.Handled = oWafaBridge.OnBackButtonPressed();

            if (!e.Handled)
            {
                doResumeGame = false;
                dispatcherTimer.Stop();
                Application.Current.Exit();
            }
                
        }

        private void _ads_AdClosed(object sender, EventArgs e)
        {
           
        }

        private void _ads_NewAdAvailable(object sender, EventArgs e)
        {
            _adsIsLoaded = true;
        }

 
         public bool showInterstitial()
         {
            try
            {
                if (_msads != null && _adsIsLoaded && _msads.State == InterstitialAdState.Ready)
                {
                    _msads.Show();
                    _adsAskedLoad = false;
                    _adsIsLoaded = false;
                    return true;
                }
            }
            catch (Exception ex)
            {
                string ex_text = ex.ToString();
            }


            doResumeGame = !oWafaBridge.ResumeGame();
            return false;
         }

         public bool loadInterstitial()
         {
            if (_msads != null && !_adsAskedLoad)
            {
                _msads.RequestAd(AdType.Video, IA_APPID, IA_ADUNITID);
                _adsAskedLoad = true;
                _adsIsLoaded = false;
                return true;
            }

            return false;
         }


        protected override void OnNavigatedTo(NavigationEventArgs e)
        {

            // Start rendering animated DirectX content on a background thread, which reduces load on the UI thread
            // and can help keep the app responsive to user input.
            // oWafaBridge.StartRenderThread();
            oWafaBridge.ResumeGame();
        }

        protected override void OnPointerPressed(PointerRoutedEventArgs e)
        {
             PointerPoint args = e.GetCurrentPoint(this);
             oWafaBridge.OnPointerPressed(args);

            base.OnPointerPressed(e);
        }

        protected override void OnPointerMoved(PointerRoutedEventArgs e)
        {
            PointerPoint args = e.GetCurrentPoint(this);
            oWafaBridge.OnPointerMoved(args);
            base.OnPointerMoved(e);
        }

        protected override void OnPointerReleased(PointerRoutedEventArgs e)
        {
            PointerPoint args = e.GetCurrentPoint(this);
            oWafaBridge.OnPointerReleased(args);
            base.OnPointerReleased(e);
        }

        protected override void OnGotFocus(RoutedEventArgs e)
        {
            base.OnGotFocus(e);
        }

        protected override void OnLostFocus(RoutedEventArgs e)
        {
            base.OnLostFocus(e);
        }

        protected override void OnPointerCaptureLost(PointerRoutedEventArgs e)
        {
            base.OnPointerCaptureLost(e);
        }


        /// <summary>
        /// Invoked when this page will no longer be displayed in a Frame.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            // Stop rendering DirectX content when it will no longer be on screen.
           oWafaBridge.StopRenderThread();
           oWafaBridge.SuspendGame();
        }

       
    }
}
