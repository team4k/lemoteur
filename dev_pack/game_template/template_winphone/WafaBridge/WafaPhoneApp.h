﻿#pragma once

#include <wrl/client.h>
#include <d3d11_1.h>
#include <DirectXMath.h>
#include <memory>
#include <agile.h>
#include <ppl.h>
#include <ppltasks.h>
#include "StepTimer.h"

namespace WafaBridge
{

	public ref class WafaPhoneApp sealed : public Windows::UI::Xaml::Controls::SwapChainPanel
	{

	public:
		WafaPhoneApp();
		
		property bool DoShowAds;
		property bool DoLoadAds;


		bool StartRenderThread();
		void StopRenderThread();

		void SuspendGame();
		bool ResumeGame();

		void OnPointerPressed(Windows::UI::Input::PointerPoint^ args);
		void OnPointerMoved(Windows::UI::Input::PointerPoint^ args);
		void OnPointerReleased(Windows::UI::Input::PointerPoint^ args);

		bool OnBackButtonPressed();

	protected private:
		virtual void CreateGameObjects();

		virtual void OnSuspending(Platform::Object^ sender, Windows::ApplicationModel::SuspendingEventArgs^ e);
		virtual void OnResuming(Platform::Object^ sender, Platform::Object^ args);

		
		void WafaPhoneApp::OnOrientationChanged(Windows::Graphics::Display::DisplayInformation^ info, Platform::Object^ sender);
		float ConvertDipsToPixels(float dips);
		Windows::Foundation::Point ConvertRawPoint(Windows::Foundation::Point point);

		//game render thread properties
		Windows::Foundation::IAsyncAction^					m_renderLoopWorker;
		DX::StepTimer										mtimer;
		HANDLE												m_frameLatencyWaitableObject;
		float                                                               m_renderTargetHeight;
		float                                                               m_renderTargetWidth;

		float                                                               m_compositionScaleX;
		float                                                               m_compositionScaleY;

		float                                                               m_height;
		float                                                               m_width;
	private:
		~WafaPhoneApp();

	};
}
