﻿#include "common_include.h"
#include "common.h"
#include "WafaPhoneApp.h"
#include <windows.ui.xaml.media.dxinterop.h>
#include <d3d11_2.h>

using namespace Windows::ApplicationModel;
using namespace Windows::ApplicationModel::Core;
using namespace Windows::ApplicationModel::Activation;
using namespace Windows::UI::Core;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Interop;
using namespace Windows::UI::Input;
using namespace Windows::UI::ViewManagement;
using namespace Windows::System;
using namespace Windows::Foundation;
using namespace Windows::Graphics::Display;
using namespace Windows::System::Threading;
using namespace concurrency;
using namespace Platform;
using namespace Microsoft::WRL;


using namespace WafaBridge;


static const float m_dipsPerInch = 96.0f;
static char level_start[256] = { 0 };
static char level_start_id[256] = { 0 };


static wbool request_showads = wfalse;
static wbool request_loadads = wfalse;
static wbool can_closegame = wtrue;

//C functions implementations
#define ACTION_BACKBUTTON -1

#define ACTION_DOWN 0
#define ACTION_UP 1
#define ACTION_MOVE 2

#define INPUT_BUFFER_SIZE 20

typedef struct {
	Vector_t input_data;
	int32_t action;
} input_entry_t;

//input buffer type
typedef struct {
	input_entry_t buffer[INPUT_BUFFER_SIZE];
	int wp;
	int rp;
} input_buffer_t;

static input_buffer_t input_buffer = { 0 };

static int checkspace_input_buffer(input_buffer_t *p, int writeCheck)
{
	int wp = p->wp, rp = p->rp;
	if (writeCheck) {
		if (wp > rp)
			return rp - wp + INPUT_BUFFER_SIZE - 1;
		else if (wp < rp)
			return rp - wp - 1;
		else
			return INPUT_BUFFER_SIZE - 1;
	}
	else {
		if (wp > rp)
			return wp - rp;
		else if (wp < rp)
			return wp - rp + INPUT_BUFFER_SIZE;
		else
			return 0;
	}
}

static input_entry_t* read_input_buffer_bytes(input_buffer_t *p)
{
	int remaining;
	int input_read, size = INPUT_BUFFER_SIZE;
	int i = 0, rp = p->rp;
	input_entry_t* out;

	if ((remaining = checkspace_input_buffer(p, 0)) == 0) {
		return NULL;
	}

	out = &p->buffer[rp++];

	if (rp == size)
		rp = 0;

	p->rp = rp;

	return out;
}

static int write_input_buffer(input_buffer_t *p, const input_entry_t* in)
{
	int remaining;
	int input_read, size = INPUT_BUFFER_SIZE;
	int i = 0, wp = p->wp;

	if ((remaining = checkspace_input_buffer(p, 1)) == 0) {
		return 0;
	}

	p->buffer[wp].input_data.x = in->input_data.x;
	p->buffer[wp].input_data.y = in->input_data.y;
	p->buffer[wp++].action = in->action;


	if (wp == size)
		wp = 0;

	p->wp = wp;
	return 1;
}


//ads functions are executed on the ui thread
static void smaato_load_ads()
{
	request_loadads = wtrue;	
}

static void smaato_show_ads()
{
	request_showads = wtrue;
}

void poll_events()
{

}

void lockrunning()
{

}

void unlockrunning()
{

}



void handle_input()
{
	//movement using touch control

	input_entry_t* tmp_input_entry;
	Vector_t tmp_coord = { 0 };

	while ((tmp_input_entry = read_input_buffer_bytes(&input_buffer)) != NULL)
	{
		module_mngr_t* mngr_ptr = get_module_mngr();

		if (tmp_input_entry->action == ACTION_BACKBUTTON)
		{
			if (get_levelmngr()->current_state == COMMON_PAUSE) //press on back button in pause menu let us exit 
			{
				Levelobject_t* pause_menu = get_pausemenu();

				if (pause_menu != NULL)
				{
					if (strcmp(pause_menu->current_map_id, "pause") == 0) //if we are on the first pause menu
					{
						Levelobject_requestexit(get_levelmngr());//quit the game
					}
					else //write config and go back to first pause menu
					{
						config_saveconf(get_config(), get_config_path());

						w_strcpy(pause_menu->current_map_id, 256, "pause");
						Levelobject_switchmap(pause_menu, get_phymngr(), "pause");
						can_closegame = wtrue;
					}
				}
			}
			else
			{
				mngr_ptr->game_input(256, wtrue, wfalse); //simulate esc button

				if (strcmp(get_levelmngr()->current_level, level_start_id) != 0) //if we are on a regular level
				{
					if (!mngr_ptr->game_disableinput()) //if input are enabled
					{
						screen_fade_dofade(get_screenfade(), 500, 1, 0.8f); //show pause
						set_state(COMMON_PAUSE);
						can_closegame = wtrue;
					}
				}
				else //if we are on menu level
				{
					can_closegame = wtrue;

					//go back to main map
					if (strcmp(get_levelmngr()->current_map_id, "menu_mob") != 0) //if we are not on first menu, write config and go back to it
					{
						config_saveconf(get_config(), get_config_path());
						Levelobject_switchmap(get_levelmngr(), get_phymngr(), "menu_mob");
					}
					else //if we are on first menu, exit
					{
						Levelobject_requestexit(get_levelmngr());
					}
				}
			}
		}
		else
		{
			if (strcmp(get_levelmngr()->current_map_id, "menu_mob") != 0)
			{
				can_closegame = wfalse;
			}
			
			get_world_coords(&tmp_coord, tmp_input_entry->input_data.x, tmp_input_entry->input_data.y);

			if (tmp_input_entry->action == ACTION_DOWN)
				mngr_ptr->game_target(tmp_coord.x, tmp_coord.y, wtrue, 0);
			else if (tmp_input_entry->action == ACTION_UP)
				mngr_ptr->game_endtarget(tmp_coord.x, tmp_coord.y, wtrue, 0);
			else if (tmp_input_entry->action == ACTION_MOVE)
				mngr_ptr->game_movetarget(tmp_coord.x, tmp_coord.y, wtrue);	

			if (strcmp(get_levelmngr()->pending_level, "menu_mob.lwf") == 0)
			{
				can_closegame = wtrue;
			}
		}
	}

}

void endrenderthread()
{

}

void setcustompkgloader(lua_State* state)
{

}

void config_saved()
{

}


//c++ implementation of WafaPhoneApp
WafaPhoneApp::WafaPhoneApp() :
	m_compositionScaleX(1.0f),
	m_compositionScaleY(1.0f),
	m_height(1.0f),
	m_width(1.0f)
{
	Application::Current->Suspending += ref new SuspendingEventHandler(this, &WafaPhoneApp::OnSuspending);
	Application::Current->Resuming += ref new EventHandler<Object^>(this, &WafaPhoneApp::OnResuming);

	this->DoShowAds = false;
	this->DoLoadAds = false;
	

	CreateGameObjects();
}


WafaPhoneApp::~WafaPhoneApp()
{
}

bool WafaPhoneApp::StartRenderThread()
{
	// If the animation render loop is already running then do not start another thread.
	if (m_renderLoopWorker != nullptr && m_renderLoopWorker->Status == AsyncStatus::Started)
	{
		return false;
	}

	// Create a task that will be run on a background thread.
	auto workItemHandler = ref new WorkItemHandler([this](IAsyncAction ^ action)
	{
		// Calculate the updated frame and render once per vertical blanking interval.
		while (action->Status == AsyncStatus::Started)
		{
			mtimer.Tick([&]()
			{
				renderoneframe();
			});
			
			//handle ads request outside of rendering
			if (request_showads)
			{
				this->DoShowAds = true;
				request_showads = wfalse;
				SuspendGame();
				return;//end thread*/
			}
				

			if (request_loadads)
			{
				this->DoLoadAds = true;
				request_loadads = wfalse;
			}

			if (get_levelmngr()->request_exit)
			{
				g_cleanup();
				return;//end thread
			}
				
		}
	});

	// Run task on a dedicated high priority background thread.
	m_renderLoopWorker = ThreadPool::RunAsync(workItemHandler, WorkItemPriority::High, WorkItemOptions::TimeSliced);

	return true;
}

void WafaPhoneApp::StopRenderThread()
{

	// Cancel the asynchronous task and let the render thread exit.
	m_renderLoopWorker->Cancel();
}

void WafaPhoneApp::SuspendGame()
{

	if (get_soundmngr()->stream_opened)
		snd_mngr_setvolume(get_soundmngr(), 0, 0);

	snd_mngr_stop(get_soundmngr());
}

bool WafaPhoneApp::ResumeGame()
{

	snd_mngr_start(get_soundmngr());

	if (get_soundmngr()->stream_opened)
	{
		int32_t music_vol = 0;
		int32_t sfx_vol = 0;
		config_getint(get_config(), "music_volume", &music_vol);
		config_getint(get_config(), "sfx_volume", &sfx_vol);
		snd_mngr_setvolume(get_soundmngr(), music_vol, sfx_vol);
	}

	return StartRenderThread();
}

//input event 
void WafaPhoneApp::OnPointerPressed(PointerPoint^ args)
{
	Point oriPoint = ConvertRawPoint(args->Position);

	input_entry_t entry;
	entry.input_data.x = oriPoint.X;
	entry.input_data.y = oriPoint.Y;
	entry.action = ACTION_DOWN;

	if (write_input_buffer(&input_buffer, &entry) == 0)
		logprint("No more space in input buffer!");

}

void WafaPhoneApp::OnPointerMoved(PointerPoint^ args)
{
	Point oriPoint = ConvertRawPoint(args->Position);

	input_entry_t entry;
	entry.input_data.x = oriPoint.X;
	entry.input_data.y = oriPoint.Y;
	entry.action = ACTION_MOVE;

	if (write_input_buffer(&input_buffer, &entry) == 0)
		logprint("No more space in input buffer!");
}

void WafaPhoneApp::OnPointerReleased(PointerPoint^ args)
{
	Point oriPoint = ConvertRawPoint(args->Position);

	input_entry_t entry;
	entry.input_data.x = oriPoint.X;
	entry.input_data.y = oriPoint.Y;
	entry.action = ACTION_UP;

	if (write_input_buffer(&input_buffer, &entry) == 0)
		logprint("No more space in input buffer!");
}

bool WafaPhoneApp::OnBackButtonPressed()
{
	input_entry_t entry;
	entry.input_data.x = 0.0f;
	entry.input_data.y = 0.0f;
	entry.action = ACTION_BACKBUTTON;

	if (write_input_buffer(&input_buffer, &entry) == 0)
		logprint("No more space in input buffer!");

	if (can_closegame == wtrue)
		return false;
	else
		return true;
}

void WafaPhoneApp::OnOrientationChanged(DisplayInformation^ info, Platform::Object^ sender)
{
	DisplayOrientations ori = DisplayInformation::GetForCurrentView()->CurrentOrientation;
}



Point WafaPhoneApp::ConvertRawPoint(Point point)
{
	CoreWindow^ app_window = CoreWindow::GetForCurrentThread();

	Point returnValue;

	returnValue = Point(ConvertDipsToPixels(point.X), ConvertDipsToPixels(point.Y));
	
	return returnValue;
}

void WafaPhoneApp::CreateGameObjects()
{
	Platform::String^ app_path = Windows::ApplicationModel::Package::Current->InstalledLocation->Path + L"\\Assets\\";
	char c_app_path[WAFA_MAX_PATH];
	size_t convert = 0;
	wcstombs_s(&convert, c_app_path, WAFA_MAX_PATH, app_path->Data(), WAFA_MAX_PATH);

	Platform::String^ storage_path = Windows::Storage::ApplicationData::Current->LocalFolder->Path + L"\\";
	char c_store_path[WAFA_MAX_PATH];
	wcstombs_s(&convert, c_store_path, WAFA_MAX_PATH, storage_path->Data(), WAFA_MAX_PATH);

	//intialise engine

	//set paths
	set_app_path(c_app_path);

	set_lib_path(c_app_path);

	set_db_path(c_app_path);

	set_save_path(c_store_path);

	set_config_path(c_store_path);

	logprint_setdir(c_store_path);

	init_path();

	logprint("Init resx manager...");

	init_resx();
	configure();
	logprint("Init game module...");

	char game_module[128];

	if (config_getstring(get_config(), "gamemodule", &game_module[0], 128))
		init_gamemodule_path(game_module);
	else
	{
		logprint("No gamemodule set! aborting...");
		return;
	}

	char game_name[128];

	w_sprintf(game_name, 128, "Wafa template");

	config_getstring(get_config(), "gamename", &game_name[0], 128);




	CoreWindow^ app_window = CoreWindow::GetForCurrentThread();

	//get the phone physicale size independantly of orientation
	int16_t phone_screen_width = ConvertDipsToPixels(app_window->Bounds.Width);
	int16_t phone_screen_height = ConvertDipsToPixels(app_window->Bounds.Height);

	DisplayInformation::GetForCurrentView()->AutoRotationPreferences = DisplayOrientations::Landscape;
	DisplayInformation::GetForCurrentView()->OrientationChanged += ref new Windows::Foundation::TypedEventHandler<Windows::Graphics::Display::DisplayInformation ^, Platform::Object ^>(this, &WafaPhoneApp::OnOrientationChanged);

	

	//initialize render window
	logprint("Init render window...");
	get_render()->zoom_factor = 1.0f;

	m_compositionScaleX = this->CompositionScaleX;
	m_compositionScaleY = this->CompositionScaleY;


	//landscape orientation, so we invert the width / height values
	get_render()->screen_info.width = phone_screen_height;
	get_render()->screen_info.height = phone_screen_width;


	init_render(NULL, reinterpret_cast<IUnknown*>(app_window), game_name, wfalse);

	//associate SwapChain to swappanel
	Dispatcher->RunAsync(CoreDispatcherPriority::Normal, ref new DispatchedHandler([=]()
	{
		//Get backing native interface for SwapChainPanel.
		ComPtr<ISwapChainPanelNative> panelNative;
		reinterpret_cast<IUnknown*>(this)->QueryInterface(IID_PPV_ARGS(&panelNative));

		// Associate swap chain with SwapChainPanel.  This must be done on the UI thread.
		panelNative->SetSwapChain(get_render()->SwapChain);
	}, CallbackContext::Any));


	// Ensure the physical pixel size of the swap chain takes into account both the XAML SwapChainPanel's logical layout size and 
	// any cumulative composition scale applied due to zooming, render transforms, or the system's current scaling plateau.
	// For example, if a 100x100 SwapChainPanel has a cumulative 2x scale transform applied, we instead create a 200x200 swap chain 
	// to avoid artifacts from scaling it up by 2x, then apply an inverse 1/2x transform to the swap chain to cancel out the 2x transform.

	DXGI_MATRIX_3X2_F inverseScale = { 0 };
	inverseScale._11 = 1.0f / m_compositionScaleX;
	inverseScale._22 = 1.0f / m_compositionScaleY;

	IDXGISwapChain2* tmpswapchain = (IDXGISwapChain2*)get_render()->SwapChain;

	tmpswapchain->SetMatrixTransform(&inverseScale);

	//initialise manager (resources / physics / etc.)
	logprint("Init managers...");
	init_mngr();

	get_module_mngr()->game_setexternapifunc(NULL, NULL, smaato_show_ads, smaato_load_ads);


	init_text();

	logprint("End main init");


	if (config_getstring(get_config(), "start_level", &level_start[0], 256))
	{
		load_level(level_start);

		w_strcpy(level_start_id, 256, get_levelmngr()->current_level);
	}
	else
	{
		logprint("Can't load start_level! please check config");
	}

	g_init();
}



float WafaPhoneApp::ConvertDipsToPixels(float dips)
{
	static const float dipsPerInch = 96.0f;
	
	return floor(dips * DisplayInformation::GetForCurrentView()->LogicalDpi / dipsPerInch + 0.5f); // Arrondir à l’entier le plus proche.
}

void WafaPhoneApp::OnResuming(Platform::Object^ sender, Platform::Object^ args)
{
	ResumeGame();
}

void WafaPhoneApp::OnSuspending(Object^ sender, SuspendingEventArgs^ e)
{
	SuspendGame();
}