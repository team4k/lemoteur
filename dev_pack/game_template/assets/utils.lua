utils = {}


function utils.shallowcopy(orig)
    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in pairs(orig) do
            copy[orig_key] = orig_value
        end
    else -- number, string, boolean, etc
        copy = orig
    end
    return copy
end

function utils.reloadmod(modname)
   if package.loaded[modname] ~= nil then
      package.loaded[modname] = nil
   end
   
   require(modname)
   print(modname.. ' reloaded!')
end


function utils.arraycontains(arr,value)
  
  if arr == nil then
    print(debug.traceback())
  end
  
  for i=1,#arr do
    
    if arr[i] == value then
      return i
    end
    
  end
  
  return -1
  
end

function utils.fuzzyequals(a,b,tolerance)
  local absdiff = math.abs(a - b)
  
  return ((a == 0.0 and b == 0.0) or absdiff < tolerance)
end

function utils.setangle(base_angle,add_angle)
  
  if base_angle > 360 or add_angle > 360 or base_angle < 0 or add_angle < -360 then
    print("invalid base angle "..tostring(base_angle).." / add angle "..tostring(add_angle).." !")
    return
  end
  
  if base_angle + add_angle > 360 then
     add_angle = (base_angle + add_angle) - 360 
     base_angle = 0
  elseif base_angle + add_angle < 0 then
    add_angle = (base_angle + add_angle)
    base_angle = 360
  end
  
  return base_angle + add_angle
end

function utils.print_r(arr, indentLevel,limitLevel)
    local str = ""
    local indentStr = "#"

    if(indentLevel == nil) then
        print(utils.print_r(arr, 0))
        return
    end

    for i = 0, indentLevel do
        indentStr = indentStr.."\t"
    end

    for index,value in pairs(arr) do
        if type(value) == "table" then
          if limitLevel == nil then
            str = str..indentStr..index..": \n"..utils.print_r(value, (indentLevel + 1))
          else
            
            if limitLevel == 1 then
              str = str..indentStr..index..": \n"
            else
              str = str..indentStr..index..": \n"..utils.print_r(value, (indentLevel + 1),limitLevel -1)
            end
          end
        else 
            str = str..indentStr..index..": "..tostring(value).."\n"
        end
    end

    return str
end

local Chars = {}
for Loop = 0, 255 do
   Chars[Loop+1] = string.char(Loop)
end
local String = table.concat(Chars)

local Built = {['.'] = Chars}

local AddLookup = function(CharSet)
   local Substitute = string.gsub(String, '[^'..CharSet..']', '')
   local Lookup = {}
   for Loop = 1, string.len(Substitute) do
       Lookup[Loop] = string.sub(Substitute, Loop, Loop)
   end
   Built[CharSet] = Lookup

   return Lookup
end

function utils.random(Length, pCharSet)
   -- Length (number)
   -- CharSet (string, optional); e.g. %l%d for lower case letters and digits

   local CharSet = pCharSet or '.'

   if CharSet == '' then
      return ''
   else
      local Result = {}
      local Lookup = Built[CharSet] or AddLookup(CharSet)
      local Range = #Lookup
      
      for Loop = 1,Length do
         Result[Loop] = Lookup[math.random(1, Range)]
      end

      return table.concat(Result)
   end
end

startsWith = function(self, piece)
  return string.sub(self, 1, string.len(piece)) == piece
end

rawset(_G.string, "startsWith", startsWith)


return utils;