config = {
fullscreen=false,
mouse_sensitivity=0.9,
fps_limit=60,
locale="fr",
music_volume=4,
sfx_volume=2,
editor=false,
asset_path="..\\assets",
script_path="..\\scripts",
show_fps=true,
texture_filtering = 0,
gamemodule = "WafaDummyGame",
gamename = "GameTemplate"
}