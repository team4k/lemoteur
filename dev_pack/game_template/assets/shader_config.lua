shader_config = {
    {prog="prog_textured",vertex_shader="vertex_shader.shad",pixel_shader="pixel_shader.shad"},
    {prog="prog_no_texture",vertex_shader="vertex_shader_notex.shad",pixel_shader="pixel_shader_notex.shad"},
    {prog="prog_colored_no_texture",vertex_shader="vertex_shader_notex_colored.shad",pixel_shader="pixel_shader_notex.shad"},
    {prog="prog_shadows",vertex_shader="vertex_shader.shad",pixel_shader="ps_shadows.shad"}
}

return shader_config