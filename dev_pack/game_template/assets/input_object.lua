input_object = {}
require("lib_vector")
require("entities_data")
require("utils")

local OFFSET_TEXT_OBJ_X = 5
local OFFSET_TEXT_OBJ_Y = -5

function input_object.new(params)
	return {modulename = "input_object",focuscallback = params.focuscallback or nil,focused = false,input_content = {},maxlength = params.maxlength or 18,text_obj = params.text_obj}
end

function input_object.init(obj,id)
	obj.id = id
  
  --set background frames
  local num_repeat = sprite_getrepeat(entity_getspritepointer(obj.id))
  
  for i = 0,num_repeat.repeatx do
    
    if i == 0 then
      entity_setframe(obj.id,0,i)
    elseif i == num_repeat.repeatx then
      entity_setframe(obj.id,2,i)
    else
      entity_setframe(obj.id,1,i)
    end
  end
  
  --set text obj pos
  local field_pos = entity_getpos(obj.id)
  local field_size = entity_getbox(obj.id)
  
  field_pos.x = (field_pos.x - (field_size.width * 0.5)) + OFFSET_TEXT_OBJ_X
  field_pos.y = (field_pos.y + (field_size.height * 0.5)) + OFFSET_TEXT_OBJ_Y 
  
  text_setposition(obj.text_obj,field_pos)
end

function input_object.onhover(obj)
  if not obj.focused then
    sprite_setcolor(entity_getspritepointer(obj.id),1.0,0.0,0.0,1.0)
  end
end

function input_object.onout(obj)
  if not obj.focused then
    sprite_setcolor(entity_getspritepointer(obj.id),0.0,0.0,1.0,1.0)
  end
end

function input_object.addchar(obj,chartoadd)
  local len = text_getstringlen(obj.text_obj)

  if len >= obj.maxlength then
    return
  end
    
  if len == 0 then
    text_message(obj.text_obj,chartoadd)
  else
    text_appendmessage(obj.text_obj,chartoadd)
  end
  
  table.insert(obj.input_content,chartoadd)
end

function input_object.getcontent(obj)
  
  return  table.concat(obj.input_content)
end

function input_object.removechar(obj)
  local len = text_getstringlen(obj.text_obj)
  
  if #obj.input_content > 0 and len > 0 then
    table.remove(obj.input_content)
    
    local str = table.concat(obj.input_content)
    --recreate text content
    text_message(obj.text_obj,str)
  end
end

function input_object.outfocus(obj)
  obj.focused = false
  sprite_setcolor(entity_getspritepointer(obj.id),0.0,0.0,1.0,1.0)
end

function input_object.onclick(obj)
  
  obj.focused = true
  sprite_setcolor(entity_getspritepointer(obj.id),0.0,1.0,0.0,1.0)
  
  if obj.focuscallback ~= nil then
    obj.focuscallback(obj.id)
  end
end

function input_object.doEvents(obj,elapsed)
end

function input_object.onreset(obj)
end


return input_object