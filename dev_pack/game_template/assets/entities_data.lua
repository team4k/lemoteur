require("utils")

Entities = {}
Text = {}

Default_ptr = nil

function check_ptr(parent_ptr)
    if parent_ptr == nil then --get default level object ptr
        if Default_ptr == nil then
            Default_ptr = game_getdefaultlvlptr()
            return Default_ptr
        end
        
        return Default_ptr
    end
    
    return parent_ptr
end

function add_entity(obj,id,parent_ptr)

    parent_ptr = check_ptr(parent_ptr)


  if Entities[parent_ptr] == nil then
    Entities[parent_ptr] = {}
  end
  
  if Entities[parent_ptr][id] ~= nil then
    game_print("[LUA] An entity with the id "..id.." already exist!")
  end

   Entities[parent_ptr][id] = obj
   
   if parent_ptr ~= Default_ptr then
        obj.parent = parent_ptr
   end
	
	--initialise native object values
    require(obj.modulename).init(obj,id)
    
    --register function for this entity
  for key,value in pairs(require(obj.modulename)) do
    if key:startsWith("on") and type(value) == "function" then
      entity_registercallback(id,obj.modulename.."."..key,key,obj.parent)
    end
  end
	   
end

function get_entity(id,parent_ptr)
    
    if parent_ptr ~= nil and Entities[parent_ptr] == nil then
       return nil
    end

    parent_ptr = check_ptr(parent_ptr)

    return Entities[parent_ptr][id]
end

function update_entities(elapsed,parent_ptr)
  parent_ptr = check_ptr(parent_ptr)
    
  if Entities[parent_ptr] == nil then
     return
  end

  for entityid,entityobj in pairs(Entities[parent_ptr]) do
     require(Entities[parent_ptr][entityid].modulename).doEvents(Entities[parent_ptr][entityid],elapsed)
  end
end

function update_text(elapsed,parent_ptr)
  parent_ptr = check_ptr(parent_ptr)
    
    
  if Text[parent_ptr] == nil then
       return
  end

  for textid,textobj in pairs(Text[parent_ptr]) do
    require(Text[parent_ptr][textid].modulename).doEvents(Text[parent_ptr][textid],elapsed)
  end
end

function get_text(id,parent_ptr)
   if parent_ptr ~= nil and Text[parent_ptr] == nil then
      return nil
   end

   parent_ptr = check_ptr(parent_ptr)

  return Text[parent_ptr][id]
end

function remove_entity(id,parent_ptr)

    if parent_ptr ~= nil and Entities[parent_ptr] == nil then
      return nil
   end

   parent_ptr = check_ptr(parent_ptr)
 
  if entity_hascallback(Entities[parent_ptr][id].id,Entities[parent_ptr][id].parent) then
    entity_clearcallback(Entities[parent_ptr][id].id,Entities[parent_ptr][id].parent)
  end
  
 --cleanup entities data
  if  require(Entities[parent_ptr][id].modulename).clean ~= nil then
    require(Entities[parent_ptr][id].modulename).clean(Entities[parent_ptr][id])
  end
  
  Entities[parent_ptr][id] = nil
end

function add_text(obj,id,parent_ptr)

    parent_ptr = check_ptr(parent_ptr)

  if Text[parent_ptr] == nil then
    Text[parent_ptr] = {}
  end

   Text[parent_ptr][id] = obj
   
   if parent_ptr ~= Default_ptr then
        obj.parent = parent_ptr
   end
	
	--initialise native object values
    require(obj.modulename).init(obj,id)
    
    --register function for this entity
  for key,value in pairs(require(obj.modulename)) do
    if key:startsWith("on") and type(value) == "function" then
      text_registercallback(id,obj.modulename.."."..key,key,obj.parent)
    end
  end
end


function remove_text(id,parent_ptr)

   if parent_ptr ~= nil and Text[parent_ptr] == nil then
      return nil
   end

   parent_ptr = check_ptr(parent_ptr)

   if text_hascallback(Text[parent_ptr][id].id,Text[parent_ptr][id].parent) then
    text_clearcallback(Text[parent_ptr][id].id,Text[parent_ptr][id].parent)
  end
  
  --cleanup text data
  if  require(Text[parent_ptr][id].modulename).clean ~= nil then
    require(Text[parent_ptr][id].modulename).clean(Text[parent_ptr][id])
  end
  
   Text[parent_ptr][id] = nil
end