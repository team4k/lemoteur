sprite_move = {}


require("lib_vector")
require("entities_data")

sprite_move.MOVE_NONE = 0
sprite_move.MOVE_STRAIGHT = 1
sprite_move.MOVE_CIRCLE = 2
sprite_move.MOVE_CONST_CIRCLE = 3
sprite_move.MOVE_PARABOLIC = 4

function sprite_move.new(params)
   return {modulename='sprite_move',speed = params.speed or 100,move_type = sprite_move.MOVE_NONE,move_entity = params.move_entity or 1,use_physics = params.use_physics or 0}
end

function sprite_move.init(obj,id)
  obj.id = id
end

function sprite_move.gotodest(obj,dest,speed)
  obj.dest = dest
  obj.dist = nil
  
  if obj.speed ~= nil then
    obj.speed = speed
  end
  
  obj.move_type = sprite_move.MOVE_STRAIGHT 
  
    
  if obj.move_entity == 1 then
    if obj.use_physics == 1 then
      entity_setcontrolpos(obj.id,entity_getpos(obj.id))
    end
  end
  
end

function sprite_move.circletodest(obj,startangle,endangle,circle_radius,move_step,base_point)
  obj.anglevalue = startangle
  obj.endangle = endangle
  obj.circle_radius=circle_radius
  
  obj.base_point = base_point
  
  obj.move_type = sprite_move.MOVE_CIRCLE
  
  if startangle < endangle then
    obj.move_step = move_step
  else
    obj.move_step = -move_step
  end

end


function sprite_move.constcircle(obj,startangle,circle_radius,move_step,base_point,direction)
  obj.anglevalue = startangle
  obj.circle_radius=circle_radius
  
  obj.base_point = base_point
  
  obj.move_type = sprite_move.MOVE_CONST_CIRCLE
  
  if direction >= 0 then
    obj.move_step = move_step
  else
    obj.move_step = -move_step
  end
 
end

function sprite_move.parabolic(obj,end_para,height_para,speed)
  obj.end_para = end_para
  obj.start_para = entity_getpos(obj.id)
  obj.speed_para = speed
  obj.height_para = height_para
  
  obj.move_type = sprite_move.MOVE_PARABOLIC
  obj.time_para = 0
end

function sprite_move.ismoving(obj)
  
  if obj.move_type == sprite_move.MOVE_NONE then
    return false
  end
  
  if obj.move_type == sprite_move.MOVE_STRAIGHT then 
    
    if obj.move_entity == 1 then
      obj.position = entity_getpos(obj.id)
    end
    
    return (obj.dest ~= nil and obj.dest ~= obj.position)
    
  elseif obj.move_type == sprite_move.MOVE_CIRCLE then 
   local ret_val = (obj.move_step > 0 and obj.anglevalue >= obj.endangle) or (obj.move_step < 0 and obj.anglevalue <= obj.endangle)
   return not ret_val
  elseif obj.move_type == sprite_move.MOVE_PARABOLIC then
    return (obj.time_para < 1.0)
  end
  
  return false
end

function sprite_move.doEvents(obj,elapsed)
    if obj.move_type == sprite_move.MOVE_STRAIGHT then 
      sprite_move.move_straight(obj,elapsed)
    elseif obj.move_type == sprite_move.MOVE_CIRCLE then
      sprite_move.move_circle(obj,elapsed)
    elseif obj.move_type == sprite_move.MOVE_CONST_CIRCLE then
      sprite_move.move_const_circle(obj,elapsed)
    elseif obj.move_type == sprite_move.MOVE_PARABOLIC then
      sprite_move.move_parabolic(obj,elapsed)
    end
end

function sprite_move.move_parabolic(obj,elapsed)
  
  if obj.time_para < 1.0 then
    
    obj.time_para = obj.time_para + (elapsed * (0.001 * obj.speed_para) )
      
     local result = nil
    
    if math.abs(obj.start_para.y - obj.end_para.y) < 0.1 then
      local dir = obj.end_para - obj.start_para
      result = obj.start_para + obj.time_para * dir
      
      result.y = result.y + math.sin(obj.time_para * math.pi) * obj.height_para
    else
      
     local dir =  obj.end_para - obj.start_para
     local up = dir:normal()
      
      
      if obj.end_para.x < obj.start_para.x then
        up.x = -up.x
        up.y = -up.y
      end
      
    
      result = obj.start_para + obj.time_para * dir
      result = result + ((math.sin(obj.time_para * math.pi) * obj.height_para) * up:normalized())
      
    end
    
     obj.position = result
    
    if obj.move_entity == 1 then
      
      if obj.use_physics == 1 then
        entity_setcontrolpos(obj.id,obj.position)
      else
        entity_setpos(obj.id,obj.position)
      end
    end
    
  end
  
end

function sprite_move.move_straight(obj,elapsed)
  
  if obj.move_entity == 1 then
    if obj.use_physics == 1 then
       obj.position = entity_getcontrolpos(obj.id)
    else
      obj.position = entity_getpos(obj.id)
    end
  end
  
  if obj.dest ~= nil and obj.dest ~= obj.position then
    local mov = 0.0
    local mov_vector = nil
    
    if obj.dist == nil then
      obj.direction = obj.dest - obj.position
      obj.dist = obj.direction:len()
      obj.direction:Normalize()
    end
    
    
    
    if obj.dist > 0 then
      mov = obj.speed * (elapsed * 0.001)
      mov_vector = obj.direction * mov
      
      obj.position = obj.position + mov_vector
      
      if obj.move_entity == 1 then
        
         if obj.use_physics == 1 then
            entity_setcontrolpos(obj.id,obj.position)
         else
            entity_setpos(obj.id,obj.position)
         end
      end
     
      obj.dist = obj.dist - mov
    end
    
    if obj.dist <= 0 then
      obj.dist = nil
      obj.position = obj.dest
      
      if obj.move_entity == 1 then
        
         if obj.use_physics == 1 then
           entity_setcontrolpos(obj.id,obj.dest)
         else
           entity_setpos(obj.id,obj.dest)
         end
      end
      
      obj.dest = nil
    end
  end
end

function sprite_move.move_circle(obj,elapsed)
  if (obj.move_step > 0 and obj.anglevalue >= obj.endangle) or (obj.move_step < 0 and obj.anglevalue <= obj.endangle) then
    return
  end
  
  obj.anglevalue = obj.anglevalue + obj.move_step
  
  local new_x = obj.circle_radius * math.cos(obj.anglevalue) + obj.base_point.x
  local new_y = obj.circle_radius * math.sin(obj.anglevalue) + obj.base_point.y
  
  obj.position = Vector.new(new_x,new_y)
  
  if obj.move_entity == 1 then
    
    if obj.use_physics == 1 then
      entity_setcontrolpos(obj.id,obj.position)
    else
      entity_setpos(obj.id,obj.position)
    end
  end
  
end


function sprite_move.move_const_circle(obj,elapsed)  
  obj.anglevalue = obj.anglevalue + obj.move_step
  
  if obj.move_step > 0 and obj.anglevalue >= math.pi * 2 then
    obj.anglevalue = 0
  end
  
  if obj.move_step < 0 and obj.anglevalue <= 0 then
    obj.anglevalue = math.pi * 2
  end
  
  local new_x = obj.circle_radius * math.cos(obj.anglevalue) + obj.base_point.x
  local new_y = obj.circle_radius * math.sin(obj.anglevalue) + obj.base_point.y
  
  obj.position = Vector.new(new_x,new_y)
  
  if obj.move_entity == 1 then
    if obj.use_physics == 1 then
      entity_setcontrolpos(obj.id,obj.position)
    else
      entity_setpos(obj.id,obj.position)
    end
  end
  
end

return sprite_move

