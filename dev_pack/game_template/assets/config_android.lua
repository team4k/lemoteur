config = {
fps_limit=60,
locale="",
music_volume=4,
sfx_volume=2,
editor=false,
asset_path="..\\assets",
script_path="..\\scripts",
show_fps=true,
texture_filtering = 0,
mobile_device=true,
use_cursor=false,
gamemodule = "dummygame",
gamename = "GameTemplate"
}