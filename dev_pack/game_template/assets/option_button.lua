option_button = {}


function option_button.new(params)
  return {modulename="option_button",optiontype=params.optiontype, configentry=params.configentry, configlist=params.configlist or {},lockcallback = params.lockcallback or nil}
end


function option_button.init(obj,id)
  obj.id = id
    
  text_resetcontent(obj.id,obj.parent)--set the content back to it original state
  --set the text of the object
  text_append_raw(obj.id," : ",obj.parent)
  
  if obj.optiontype == "bool" then
 
    local config_value = config_getbool(obj.configentry)
   
    obj.current_config_value = config_value
    
   if config_value then
      text_appendmessage(obj.id,"true",obj.parent)
    else
      text_appendmessage(obj.id,"false",obj.parent)
    end 
  elseif obj.optiontype == "string" then
    local config_value = config_getstring(obj.configentry)
    
    text_appendmessage(obj.id,config_value,obj.parent)
    obj.current_config_value = config_value
  end
    
end


function option_button.onhover(obj)
  text_changecolor(obj.id,1.0,0.0,0.0,1.0,obj.parent)
end

function option_button.onout(obj)
  text_resetcolor(obj.id,obj.parent) 
  
  if obj.optiontype == "bool" then
   obj.current_config_value = config_getbool(obj.configentry)

    text_resetcontent(obj.id,obj.parent)
    text_append_raw(obj.id," : ",obj.parent)
    
    if obj.current_config_value then
      text_appendmessage(obj.id,"true",obj.parent)
    else
      text_appendmessage(obj.id,"false",obj.parent)
    end
  elseif obj.optiontype == "string" then
     obj.current_config_value = config_getstring(obj.configentry)
    text_resetcolor(obj.id,obj.parent)
    text_resetcontent(obj.id,obj.parent)--set the content back to it original state
    text_append_raw(obj.id," : ",obj.parent)
    text_appendmessage(obj.id,obj.current_config_value,obj.parent)
  end
end

function option_button.onclick(obj)
  
  local locked = false
  
  local tmp_value = obj.current_config_value
  
  if obj.optiontype == "bool" then
    tmp_value = not tmp_value
  elseif obj.optiontype == "string" then
     if option_button.listhasvalue(obj.configlist,tmp_value) then
         tmp_value = option_button.listnextitem(obj.configlist,tmp_value)
      else
         tmp_value = obj.configlist[1]
      end
  end
  
  if obj.lockcallback ~= nil then
      locked = obj.lockcallback(tmp_value)
  end
  
  if obj.optiontype == "bool" then

    if not locked then
      obj.current_config_value = tmp_value
      
      config_setbool(obj.configentry,obj.current_config_value)
    end
    
    text_resetcontent(obj.id,obj.parent)--set the content back to it original state
    text_append_raw(obj.id," : ",obj.parent)
    
    if tmp_value then
      text_appendmessage(obj.id,"true",obj.parent)
    else
      text_appendmessage(obj.id,"false",obj.parent)
    end
    
    if locked then
      text_changecolor(obj.id,0.5,0.5,0.5,1.0,obj.parent)
      text_appendmessage(obj.id," (locked)",obj.parent)
    else
      text_changecolor(obj.id,1.0,0.0,0.0,1.0,obj.parent)
    end
      
       
  elseif obj.optiontype == "string" then
    
     obj.current_config_value = tmp_value
     
    if not locked then
      config_setstring(obj.configentry,obj.current_config_value)
    end
    
    text_resetcontent(obj.id,obj.parent)--set the content back to it original state
    text_append_raw(obj.id," : ",obj.parent)
    text_appendmessage(obj.id,tmp_value,obj.parent)
    
    if locked then
      text_changecolor(obj.id,0.5,0.5,0.5,1.0,obj.parent)
      text_appendmessage(obj.id," (locked)",obj.parent)
    else
      text_changecolor(obj.id,1.0,0.0,0.0,1.0,obj.parent)
    end
  end
  
end

function option_button.listhasvalue(list,config_value)
  
  for i = 1 , #list do
    if list[i] == config_value then
      return true
    end
  end
  
  return false
end

function option_button.listnextitem(list,config_value)
  for i = 1 , #list do
    if list[i] == config_value then
      
      if i < #list then
        return list[i+1]
      else
        return list[1]
      end
    end
  end
  
  return list[0]
end


function option_button.doEvents(obj,elapsed)
  
end  
  
return option_button