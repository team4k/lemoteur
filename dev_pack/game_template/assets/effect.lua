effect = {}
require("lib_vector")
require("entities_data")

EFFECT_EXPLODE = 1
EFFECT_ENERGYWAVE = 2

--effect local constant
local ENERGY_WAVE_TIMEOUT = 50.0
local ENERGY_WAVE_STEP = 0.04
local ENERGY_WAVE_MIN_SIZE = 0.01
local ENERGY_WAVE_MAX_SIZE = 0.27

local MIN_EXPLO = 0.05
local MAX_EXPLO = 0.15
local STEP_EXPLO = 0.04
local TIMEOUT_EXPLO = 20.0

local STEP_OUTPLO = 0.01
local TIMEOUT_OUTPLO = 50.0
local HIDE_VAL_EXPLO = 0.08

function effect.new(params)
	return {modulename = "effect",active=true,e_type=params.e_type,shader_state=0,damage=params.damage,shader_step=0,shader_timeout=0.0,width=params.width,height=params.height,center=params.initial_pos,effect_callback = params.callback or nil,template = params.template}
end

function effect.init(obj,id)
	obj.id = id
  
  if obj.e_type == EFFECT_EXPLODE then
    obj.shader_step = MIN_EXPLO
    obj.shader_timeout = TIMEOUT_EXPLO
  end
  
  entity_addentity(obj.template,game_getcurrentmap())
  
  entity_setpos(obj.id,obj.center)
  
end

function effect.doEvents(obj,elapsed)
  
  if not obj.active then
    return
  end
  
  local scroll = camera_getscroll()
  
  local pos = obj.center + scroll
  
 
  
  
  if obj.e_type == EFFECT_EXPLODE then
     entity_setshaderparam_fval(obj.id,"inCircleStep",obj.shader_step)
    
    if obj.shader_state == 0 then
      
      if obj.shader_step < MAX_EXPLO then
        
        if obj.shader_timeout <= 0 then
          
          if (obj.shader_step + STEP_EXPLO) < MAX_EXPLO then
            obj.shader_step = obj.shader_step + STEP_EXPLO
          else
            obj.shader_step = MAX_EXPLO
          end
          
          obj.shader_timeout = TIMEOUT_EXPLO
          
        else
           obj.shader_timeout = obj.shader_timeout  - elapsed
        end
        
        --routine callback
        if obj.effect_callback ~= nil then
          obj.effect_callback(obj)
        end
        
      else
        obj.shader_state = 1
      end
      
    end
    
    if obj.shader_state == 1 then
      
      if obj.shader_step > HIDE_VAL_EXPLO then
        
        if obj.shader_timeout <= 0 then
            if (obj.shader_step - STEP_OUTPLO) > HIDE_VAL_EXPLO then
              obj.shader_step = obj.shader_step - STEP_OUTPLO
            else
              obj.shader_step = HIDE_VAL_EXPLO
            end
            
            obj.shader_timeout = TIMEOUT_OUTPLO
          
        else
           obj.shader_timeout = obj.shader_timeout  - elapsed
        end
        
      else
         obj.active = false
      end
      
    end
    
    
    
  elseif obj.e_type == EFFECT_ENERGYWAVE then
    if obj.shader_step < ENERGY_WAVE_MAX_SIZE then
      
      if obj.shader_timeout <= 0 then
        
        if (obj.shader_step + ENERGY_WAVE_STEP) < ENERGY_WAVE_MAX_SIZE then
          obj.shader_step =  obj.shader_step + ENERGY_WAVE_STEP
          obj.width = math.floor(obj.shader_step * 740)
          obj.height = math.floor(obj.shader_step * 740)
          sprite_setsize(entity_getspritepointer(obj.id),obj.width,obj.height)
          --entity_setpos(obj.id,obj.center)
        else
           obj.shader_step = obj.shader_step + ENERGY_WAVE_MAX_SIZE
        end
        
        obj.shader_timeout = ENERGY_WAVE_TIMEOUT
        
      else
        obj.shader_timeout = obj.shader_timeout - elapsed
      end
      
      --routine callback
      if obj.effect_callback ~= nil then
        obj.effect_callback(obj)
      end
      
    else
      obj.active = false
    end
  end
  
  
  print(obj.width)
  
   entity_setshaderparam_fval(obj.id,"inSize",obj.width)
   entity_setshaderparam_fval(obj.id,"inCenter",pos.x,pos.y)
  
  
end

function effect.onreset(obj)
end

function effect.clean(obj)
  
  entity_removeentity(obj.id)
  
end

return effect