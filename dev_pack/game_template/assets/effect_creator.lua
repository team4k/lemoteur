effect_creator = {}

require("effect")
require("entities_data")

effect_creator.effect_array = {}

effect_creator.template_effect = {
  {entity_id = "explode",script_module = "effect", script_ctor="effect.new({})",start_animation="",animation_file="",col_width=64,col_height=64,tileset="rock_base.png",pos_x=0,pos_y=0,width=64,height=64,shape_type=1,_type=2,has_controller=false,no_collisions=true,has_callback=false,z_order=6,custom_collisions=false,offsetx=0,offsety=0,repeat_x=0,repeat_y=0,start_frame=0,parallax_x=1,parallax_y=1,col_radius = 0.00,shader_program="prog_explo",shader_params={
      {param_name='inResolution',param_type=4,const_name='resolution'},
      {param_name='inCircleStep',param_type=1,num_component=1,v1=0.0},
      {param_name='inGlobalTime',param_type=4,const_name='time'},
      {param_name='inCenter',param_type=1,num_component=2,v1=0.0,v2=0.0},
      {param_name='inSize',param_type=1,num_component=1,v1=64}
    }
  },
  {entity_id = "ring",script_module = "effect", script_ctor="effect.new({})",start_animation="",animation_file="",col_width=64,col_height=64,tileset="rock_base.png",pos_x=0,pos_y=0,width=64,height=64,shape_type=1,_type=2,has_controller=false,no_collisions=true,has_callback=false,z_order=6,custom_collisions=false,offsetx=0,offsety=0,repeat_x=0,repeat_y=0,start_frame=0,parallax_x=1,parallax_y=1,col_radius = 0.00,shader_program="prog_ring",shader_params={
      {param_name='inCenter',param_type=1,num_component=2,v1=0.0,v2=0.0},
      {param_name='inSize',param_type=1,num_component=1,v1=64}
    }
  }
}


function effect_creator.createeffect(e_type,width,height,effect_name,damage,initial_pos)
  
    local cloneeffect = utils.shallowcopy(effect_creator.template_effect[e_type])
    cloneeffect.entity_id = effect_name
    cloneeffect.width = width
    cloneeffect.height = height
    
    local array_params = {e_type=e_type,damage=damage,width=width,height=height,initial_pos=initial_pos,template = cloneeffect}
    
    effect_creator.effect_array[effect_name] = effect.new(array_params)
    
    add_entity(effect_creator.effect_array[effect_name],cloneeffect.entity_id)

end

function effect_creator.effectactive(name)
  
  return  effect_creator.effect_array[name] and effect_creator.effect_array[name].active
  
end

function effect_creator.update()
  
  for key,value in pairs(effect_creator.effect_array) do
    
    if not value.active then
      
      remove_entity(value.id)
      
    end
    
  end
  
end





return effect_creator