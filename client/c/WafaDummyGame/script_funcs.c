#include "common_include_game.h"

#include "Gamemain.h"
#include "script_funcs.h"

static float getluatable_value(lua_State* L,const char* key,int stack_pos)
{
	float result;
	w_lua_pushstring(L,key);
	w_lua_gettable(L, stack_pos);

	if(!w_lua_isnumber(L,-1))
		w_luaL_error(L,"can't found / invalid component for parameter %s!",key);

	result = (float)w_lua_tonumber(L,-1);
	w_lua_pop(L,1);

	return result;
}


static entity_t* internal_get_entity(lua_State* L,const char* entity_id)
{
	w_lua_getglobal(L,"mngr");

	Levelobject_t* ent_mngr;

	w_lua_pushstring(L,"level_mngr");
	w_lua_gettable(L,-2);

	ent_mngr = (Levelobject_t*)w_lua_touserdata(L,-1);

	w_lua_pop(L,1);

	if(hashtable_haskey(&ent_mngr->entities_hash,entity_id))
	{
		int pos = hashtable_index(&ent_mngr->entities_hash,entity_id);

		return &ent_mngr->entities[pos];
	}
	else
	{
		w_luaL_error(L,"The entity '%s' doesn't exist !",entity_id); 
		return NULL;
	}
}

static physics_manager_t* internal_get_physics(lua_State* L)
{
	LEVEL_TYPE type = lua_lvlbase_gettype(L);

	w_lua_getglobal(L, "mngr");

	physics_manager_t* phy_mngr;

	w_lua_pushstring(L, (type == L_STATIC) ? "static_physics_mngr" : "stream_physics_mngr");
	w_lua_gettable(L,-2);

	phy_mngr = (physics_manager_t*)w_lua_touserdata(L,-1);

	w_lua_pop(L,1);

	return phy_mngr;
}

static render_manager_t* internal_get_render(lua_State* L)
{
	w_lua_getglobal(L,"mngr");

	render_manager_t* render_mngr;

	w_lua_pushstring(L,"render_mngr");
	w_lua_gettable(L,-2);

	render_mngr = (render_manager_t*)w_lua_touserdata(L,-1);

	w_lua_pop(L,1);

	return render_mngr;
}

static Tilemap_t* internal_get_tilemap(lua_State* L)
{
	w_lua_getglobal(L,"mngr");

	Levelobject_t* ent_mngr;

	w_lua_pushstring(L,"level_mngr");
	w_lua_gettable(L,-2);

	ent_mngr = (Levelobject_t*)w_lua_touserdata(L,-1);

	w_lua_pop(L,1);

	return &ent_mngr->current_map->tilemap;
}


static game_state_t* get_state(lua_State* L)
{
	w_lua_getglobal(L,"mngr");


	w_lua_pushstring(L,"game_state");
	w_lua_gettable(L,-2);

	game_state_t* ret =  (game_state_t*)w_lua_touserdata(L,-1);

	w_lua_pop(L,1);

	return ret;
}


static int dummy_registerhandle(lua_State* L)
{
	const char* callback = w_lua_tostring(L,1);
	game_state_t* state = get_state(L);
	  
	state->script_handle_event = wtrue;

	w_strcpy(state->script_handle,260,callback);

	return 1;

}

static int dummy_registermodule(lua_State* L)
{
	const char* module = w_lua_tostring(L,1);
	game_state_t* state = get_state(L);

	w_strcpy(state->script_module,260,module);

	return 1;
}

static int dummy_getregisteredmodule(lua_State* L)
{
	game_state_t* state = get_state(L);

	w_lua_pushstring(L,state->script_module);
	
	return 1;
}

static int dummy_gettmapinfo(lua_State* L)
{
	Tilemap_t* c_tilemap = internal_get_tilemap(L);

	if(c_tilemap != NULL)
	{
		 w_lua_newtable(L);
		 w_lua_pushnumber(L,c_tilemap->col_count);
		 w_lua_setfield(L,-2,"colcount");
		 w_lua_pushnumber(L,c_tilemap->row_count);
		 w_lua_setfield(L,-2,"rowcount");
		 w_lua_pushnumber(L,c_tilemap->tile_size);
		 w_lua_setfield(L,-2,"tilesize");
	}
	else
	{
		w_lua_pushnil(L);
	}

	return 1;
}

static int dummy_getcollisionvalue(lua_State* L)
{
	int32_t row_pos = (int32_t)w_lua_tonumber(L,1);
	int32_t col_pos = (int32_t)w_lua_tonumber(L,2);

	physics_manager_t* c_phy = internal_get_physics(L);


	
	if(c_phy->collisions_array != NULL && row_pos >= 0 && row_pos < c_phy->row_count && col_pos >= 0 && col_pos < c_phy->col_count)
	{
		int32_t index = (row_pos * c_phy->col_count) + col_pos;
		w_lua_pushnumber(L,c_phy->collisions_array[index]);
	}
	else
	{
		w_lua_pushnumber(L,0);
	}

	return 1;
}

//
//fonctions gestion input
//
static int dummy_disableinput(lua_State* L)
{
	game_state_t* state = get_state(L);
	state->disable_input = (wbool)w_lua_toboolean(L,1);

	return 1;
}

static int dummy_focusinput(lua_State* L)
{
	game_state_t* state = get_state(L);
	state->focus_input = (wbool)w_lua_toboolean(L,1);

	return 1;
}


//game events related functions
static int dummy_setgamestate(lua_State* L)
{
	game_state_t* state = get_state(L);
	state->level_state = (char)w_lua_tointeger(L,1);

	return 1;
}


void script_register_func(lua_State* lua_state)
{
	w_lua_pushcfunction(lua_state,dummy_setgamestate);
	w_lua_setglobal(lua_state,"dummy_setgamestate");

	w_lua_pushcfunction(lua_state,dummy_registerhandle);
	w_lua_setglobal(lua_state,"dummy_registerhandle");

	w_lua_pushcfunction(lua_state,dummy_registermodule);
	w_lua_setglobal(lua_state,"dummy_registermodule");

	w_lua_pushcfunction(lua_state,dummy_getregisteredmodule);
	w_lua_setglobal(lua_state,"dummy_getregisteredmodule");
	
	w_lua_pushcfunction(lua_state,dummy_disableinput);
	w_lua_setglobal(lua_state,"dummy_disableinput");

	w_lua_pushcfunction(lua_state,dummy_focusinput);
	w_lua_setglobal(lua_state,"dummy_focusinput");

	w_lua_pushcfunction(lua_state,dummy_getcollisionvalue);
	w_lua_setglobal(lua_state,"dummy_getcollisionvalue");

	w_lua_pushcfunction(lua_state,dummy_gettmapinfo);
	w_lua_setglobal(lua_state,"dummy_gettmapinfo");
}