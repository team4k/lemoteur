LOCAL_PATH := $(call my-dir)

#libwafaengine
include $(CLEAR_VARS)
LOCAL_MODULE := wafaengine
LOCAL_SRC_FILES := ../../../libs-android/libWafaEngine.so
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/../../../include
include $(PREBUILT_SHARED_LIBRARY)

#build game dll
include $(CLEAR_VARS)
LOCAL_MODULE := dummygame
LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../../depends_inc/lua $(LOCAL_PATH)/../../../depends_inc/Chipmunk $(LOCAL_PATH)/../../../depends_inc/libpng $(LOCAL_PATH)/../../../depends_inc/libsndfile $(LOCAL_PATH)/../../../depends_inc/portaudio $(LOCAL_PATH)/../../../depends_inc/sqlite $(LOCAL_PATH)/../../../depends_inc/protobuf $(LOCAL_PATH)/..
LOCAL_SHARED_LIBRARIES := wafaengine
LOCAL_SRC_FILES := ../Gamemain.c ../level_proc_gen.c ../script_funcs.c ../rock_gen.c ../collapse_gen.c
LOCAL_CFLAGS := -std=c99 -DRENDERER_OPENGL_ES -DSTRIP_EDIT_CODE -DANDROID_BUILD -DCP_USE_DOUBLES=0 -DCP_ALLOW_PRIVATE_ACCESS=1 -DNDEBUG=1 -DUSE_MOBILE_INPUT
LOCAL_LDLIBS := -lGLESv2 

include $(BUILD_SHARED_LIBRARY)
