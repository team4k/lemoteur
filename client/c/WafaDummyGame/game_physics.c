
#include <common_include_game.h>
#include <Gamemain.h>
#include <game_physics.h>


//weight informations
static int* collapse_values = NULL;
static level_base_t* local_base_lvl = NULL;
static lua_State* local_lua_state = NULL;
static physics_manager_t* local_phy_mngr = NULL;
static LEVEL_TYPE lvl_type;
callback_script_t callback_script = NULL;
static wbool editor = wfalse;
static cpBitmask wallmask = 0;

static entity_t* const _getentity(const char* entity)
{
	if (lvlbase_hasentity(lvlbase_getcurrentlevelobj(local_base_lvl), local_base_lvl->current_level_type, entity))
	{
		return (entity_t*)lvlbase_getentity(lvlbase_getcurrentlevelobj(local_base_lvl), local_base_lvl->current_level_type, entity);
	}

	return NULL;
}


static void g_cpSpaceAddCollisionHandler(
	cpSpace *space,
	cpCollisionType a, cpCollisionType b,
	cpCollisionBeginFunc begin,
	cpCollisionPreSolveFunc preSolve,
	cpCollisionPostSolveFunc postSolve,
	cpCollisionSeparateFunc separate,
	void *data
)
{
	cpCollisionHandler* handler = cpSpaceAddCollisionHandler(space, a, b);

	if (begin != NULL) {
		handler->beginFunc = begin;
	}

	if (preSolve != NULL) {
		handler->preSolveFunc = preSolve;
	}

	if (postSolve != NULL) {
		handler->postSolveFunc = postSolve;
	}

	if (separate != NULL) {
		handler->separateFunc = separate;
	}

	handler->userData = data;
}


void gp_setpointers(level_base_t* const pbaselevel, LEVEL_TYPE ptype, callback_script_t pcallback,lua_State* pluastate, physics_manager_t* const  mngr, wbool peditor)
{
	local_base_lvl = pbaselevel;
	lvl_type = ptype;
	callback_script = pcallback;
	local_lua_state = pluastate;
	local_phy_mngr = mngr;
	editor = peditor;
}

void gp_setcollisionshandlers(physics_manager_t* const  mngr,cpBitmask pwallmask)
{
	wallmask = pwallmask;
}