﻿#include <common_include_game.h>
#include <script_funcs.h>
#include <Gamemain.h>
#include <Physics/physics_utils.h>
#include <Physics/physics_tilemap_utils.h>
#include <Utils/random.h>
#include <wctype.h>
#include <game_physics.h>


//#define  "HERECOMEENGINEVERSION"

#ifdef _DEBUG
	#define ENGINEVERSION "HERECOMEENGINEVERSION"
#else
	#define ENGINEVERSION "HERECOMEENGINEVERSION"
#endif

//local manager pointer
static render_manager_t* local_renderer;
static resources_manager_t* local_resx_mngr;
static physics_manager_t* local_phy_mngr;
static config_t* local_config;
static level_base_t* local_base_lvl;
static lua_State* local_lua_state;
static localization_t* local_locale;
static screen_fade_t* local_screen_fader;
static save_mngr_t* local_save_mngr;
static snd_mngr_t* local_sound_mngr;
static pathfind_worker_t* local_worker;
static input_mngr_t* local_input_mngr;

static game_state_t game_state;

static LEVEL_TYPE lvl_type;

//physics
static cpBitmask wallmask = (LAYER_PLAYER | LAYER_ENTITY | LAYER_FORCE);

//action bind
static int move_left;
static int move_right;
static int move_down;
static int move_up;
static int inventaire; 
static int pause;

						 //input key bind
static int key_move_left;
static int key_move_right;
static int key_move_down;
static int key_move_up;
static int key_inventaire; 
static int key_pause; 

//joystick bind
static int joy_move_left;
static int joy_move_right;
static int joy_move_down;
static int joy_move_up;
static int joy_inventaire; //menu lombrics
static int joy_pause; //menu luciole

static wbool control_joystick;//true si le paramétrage dans la config est avec le joystick, false sinon

static wbool use_joystick = wfalse;
static wbool mobile_version = wfalse;
static wbool has_joystick = wfalse;

static wbool editor = wfalse;

static void callback_script(const char* evnt, const char* param, void* return_value, size_t return_size, wbool string_param)
{
	if (game_state.script_handle_event)
	{
		char buff[512];

		if (string_param)
			w_sprintf(buff, 512, "return %s('%s','%s')", game_state.script_handle, evnt, param);
		else
			w_sprintf(buff, 512, "return %s('%s',%s)", game_state.script_handle, evnt, param);

		const char* result = script_execstring(local_lua_state, buff, return_value, return_size);

		if (result != NULL)
			logprint(result);
	}
}


void game_init_physics(physics_manager_t* mngr)
{
	gp_setcollisionshandlers(mngr, wallmask);
}

void game_remove_physics(physics_manager_t* const mngr)
{
	

}

static void callback_trigger(const char* module, const char* func, const char* params, const char* entity_id, const char* trigger_id)
{
	char buff[260];

	if (params && strcmp(params, "") != 0)
		w_sprintf(buff, 260, "package.loaded['%s'].%s(%s,'%s','%s');\n", module, func, params, entity_id, trigger_id);
	else
		w_sprintf(buff, 260, "package.loaded['%s'].%s('%s','%s');\n", module, func, entity_id, trigger_id);

	const char* result = script_execstring(local_lua_state, buff, NULL, 0);

	if (result != NULL)
		logprint(result);
}

static void _input_rebind(config_t* const config) {
	//affectation touches / bouton par defaut
	config_getbool(config, "control_joystick", &control_joystick);

	if (local_input_mngr->first_found_joystick != -1) {
		has_joystick = wtrue;
	}

	if (control_joystick) {
		joy_move_left = -100;//horiz joystick, - 100 pour différencier des boutons + le signe pour donner la direction
		joy_move_right = 100;//horiz joystick + 100 pour différencier des boutons et donner la direction
		joy_move_down = 400;//joystick gauche bas
		joy_move_up = -400;//joystick gauche haut
		joy_pause = 257;//start
		joy_inventaire = 0;//A


		//get joy mapping
		config_getint(config, "joy_move_left", &joy_move_left);
		config_getint(config, "joy_move_right", &joy_move_right);
		config_getint(config, "joy_move_up", &joy_move_up);
		config_getint(config, "joy_pause", &joy_pause);
		config_getint(config, "joy_inventaire", &joy_inventaire);


		int32_t schemeint = -1;

		if (!config_getint(config, "joy_gamepad_scheme", &schemeint))
		{
			gamepad_scheme scheme = input_mngr_getscheme(local_input_mngr);
			config_setint(config, "joy_gamepad_scheme", (int32_t)scheme);
		}

		move_left = joy_move_left;
		move_right = joy_move_right;
		move_down = joy_move_down;
		move_up = joy_move_down;
		pause = joy_pause;
		inventaire = joy_inventaire;
	}
	else {
		key_move_left = 65;//q
		key_move_right = 68;//d
		key_move_down = 83;//s
		key_move_up = 87; //z
		key_pause = 256;//echap
		key_inventaire = 32;//space

							  //get key mapping
		config_getint(config, "key_move_left", &key_move_left);
		config_getint(config, "key_move_right", &key_move_right);
		config_getint(config, "key_move_down", &key_move_down);
		config_getint(config, "key_move_up", &key_move_up);
		config_getint(config, "key_pause", &key_move_up);
		config_getint(config, "key_inventaire", &key_move_up);


		move_left = key_move_left;
		move_right = key_move_right;
		move_down = key_move_down;
		move_up = key_move_up;
		pause = key_pause;
		inventaire = key_inventaire;
	}



}

static SHADERPROGRAM_ID prog_tex_id;
static SHADERPROGRAM_ID prog_no_tex;

void game_init_objects(resources_manager_t* resx_mngr, int player_start_x, int player_start_y, physics_manager_t* phy_mgr, level_base_t* lvl_mngr, const char* font_name, config_t* config, render_manager_t* render_mngr, localization_t* localization_obj, lua_State* script_mngr, void* sound_mngr, input_mngr_t* input_mngr)
{
	phy_mgr->defaultwallmask = wallmask;
	local_renderer = render_mngr;
	game_state.level_state = STATE_NORMAL;

	wf_setseed((uint32_t)render_gettime());

	//set near / far to show 3D rendering
	local_renderer->near_value = -100.0f;
	local_renderer->far_value = 100.0f;
	local_resx_mngr = resx_mngr;
	local_phy_mngr = phy_mgr;
	local_config = config;
	local_base_lvl = lvl_mngr;
	local_locale = localization_obj;
	local_sound_mngr = (snd_mngr_t*)sound_mngr;
	local_lua_state = script_mngr;
	local_input_mngr = input_mngr;

	wbool stream_mode = wfalse;

	config_getbool(config, "stream_mode", &stream_mode);

	if (stream_mode)
		lvl_type = L_STREAM;
	else
		lvl_type = L_STATIC;


	config_getbool(config, "editor", &editor);

	gp_setpointers(local_base_lvl, lvl_type, callback_script, local_lua_state, local_phy_mngr,editor);

	//create texture shader
    shader_config_load(local_renderer,script_mngr,resx_mngr);

	prog_tex_id = render_get_program(render_mngr,"prog_textured");

	prog_no_tex = render_get_program(render_mngr,"prog_no_texture");

	render_use_program(render_mngr,prog_tex_id);

	_input_rebind(config);

}

void game_free()
{
	
}

void game_keepresources()
{

	
}

void game_init_scripts(lua_State* lua_state)
{
	script_pushmngr(lua_state,"game_state", &game_state);
	script_register_func(lua_state);
}

wbool game_has_player(int player_num)
{

	return wfalse;
}

cpVect game_get_playerpos(int player_num)
{
	return cpvzero;
}

cpBB game_get_playerbox(int player_num)
{
	return cpBBNew(0,0,0,0);
}


void game_player_reposition(cpVect pos)
{


}

void game_setexternapifunc(game_unlock_achievement_t func1,game_show_achievements_t func2,game_show_ads_t func3,game_load_ads_t func4)
{
}

//joystick release handling
static int joy_state[5] = { 0 }; //0 = horiz axe, 1 = vert axe , 2 = horiz axe joy droit, 3 = vert axe joy droit, 4 = dive

void game_joystick_axe(int16_t joystick_id,int16_t axe,float axe_value)
{
	if (!use_joystick || game_state.disable_input)
		return;

	float axe_up_limit = 0.8f;
	float axe_down_limit = -0.8f;

	//gachette manette xbox
	if (axe == 4 || axe == 5) {
		axe_down_limit = -1.0f;
	}

	wbool pressed = (wbool)(axe_value >axe_up_limit || axe_value < axe_down_limit);


	switch (axe)
	{
		case 0:
		{
			//int joy_k_id =  (axe_value > 0.0f)?0:1;

			if (pressed)
			{
				game_input((axe_value > 0.0f) ? move_right : move_left, pressed, wfalse);
				joy_state[0] = 1;
			}
			else
			{
				if (joy_state[0] == 1)
				{
					game_input(move_right, wfalse, wfalse);
					game_input(move_left, wfalse, wfalse);
					joy_state[0] = 0;
				}
			}
		}
		break;

		case 1:
		{
			if (pressed)
			{
				game_input((axe_value > 0.0f) ? move_down : move_up, pressed, wfalse);
				joy_state[1] = 1;
			}
			else
			{
				if (joy_state[1] == 1)
				{
					game_input(move_down, wfalse, wfalse);
					game_input(move_up, wfalse, wfalse);
					joy_state[1] = 0;
				}
			}
		}
		break;

	}

}

void game_joystick_button(int16_t joystick_id,int16_t button,wbool pressed)
{

	if (!has_joystick || game_state.disable_input)
		return;

	if (!use_joystick && pressed) {
		config_setbool(local_config, "control_joystick", wtrue);
		_input_rebind(local_config);
		use_joystick = wtrue;
	}


	game_input(button, pressed, wfalse);
}

void game_chunk_loaded(void* chunk_data)
{

}

void game_changeinputmode(const char* new_mode)
{
}

void game_level_loaded()
{

}

void game_editor_reset()
{
	
}




void game_updatescroll()
{
	

}






void game_debuginfo_update()
{

}

//called when the config file is updated
void game_config_updated()
{
	_input_rebind(local_config);
}

wbool game_disableinput()
{
	return wfalse;
	//return game_state.disable_input;
}


void game_draw(int zindex)
{

	
}


void game_debug_draw(float text_x,float text_y)
{

}

void game_update(float elapsedTime)
{
	

}




void game_move(float mX,float mY)
{

}

void game_target(float targetX,float targetY,wbool pointer_control,int button_id)
{
	
}

void game_input(int16_t input_event,wbool pressed,wbool from_keyboard)
{
}

void game_char_input(uint32_t codepoint)
{

}

void game_movetarget(float targetX,float targetY,wbool pointer_control)
{

	
}

void game_pause(wbool playsound)
{
	int cstate = lvlbase_getcurrentstate(lvlbase_getcurrentlevelobj(local_base_lvl), local_base_lvl->current_level_type);

	if ((cstate & COMMON_PAUSE) == 0)
	{
		cstate ^= COMMON_RUN;
		cstate |= COMMON_PAUSE;

		lvlbase_setcurrentstate(lvlbase_getcurrentlevelobj(local_base_lvl), local_base_lvl->current_level_type, cstate);
		screen_fade_dofade(local_screen_fader, 500, 1, 0.8f);
	}
}

void game_unpause()
{
	int cstate = lvlbase_getcurrentstate(lvlbase_getcurrentlevelobj(local_base_lvl), local_base_lvl->current_level_type);

	if ((cstate & COMMON_PAUSE) > 0)
	{
		cstate |= COMMON_RUN;
		cstate ^= COMMON_PAUSE;
		lvlbase_setcurrentstate(lvlbase_getcurrentlevelobj(local_base_lvl), local_base_lvl->current_level_type, cstate);
		screen_fade_dofade(local_screen_fader, 500, -1, 0.0f);
	}

}

void game_offset_target(float offsetX,float offsetY)
{

}

void game_update_cursorpos(float x,float y)
{
	
}

void game_setcursorpos(float x,float y)
{

}

void game_endtarget(float targetX,float targetY,wbool pointer_control,int button_id)
{
	game_state.focus_input = wfalse;

	game_state.level_state = STATE_NORMAL;

	game_state.script_handle_event = wfalse;
	memset(game_state.script_handle, 0, sizeof(game_state.script_handle));
}

void game_wheelpinch(float delta_value, wbool pointer_control)
{

}

void game_initlevelstream(const char* app_path, void* module_manager)
{

}


//call before each level loading
void game_cleanup(wbool closing)
{

	
}

void game_g_cleanup()
{

}

void game_g_create()
{

}

const char* game_getversion()
{
	return "dummy 1.0";
}

const char* game_getengineversion()
{
	return ENGINEVERSION;
}