#if defined(_WIN32)
    #define GMEXP  extern "C" __declspec( dllexport ) 
#else
   #define GMEXP  __attribute__((visibility("default"))) 
#endif

typedef struct
{
	int32_t tile_index;
	float distance;
} query_result;

#include "game_struct.h"

typedef struct
{
	entity_t* nearest_entity;
	float distance;
} entity_result;

#define STATE_NORMAL 1
#define STATE_GAMEOVER 2
#define STATE_GAMECOMPLETE 4

typedef void(*callback_script_t)(const char* evnt, const char* param, void* return_value, size_t return_size, wbool string_param);

GMEXP void game_init_objects(resources_manager_t* resx_mngr, int player_start_x, int player_start_y, physics_manager_t* phy_mgr, level_base_t* lvl_mngr, const char* font_path, config_t* config, render_manager_t* render_mngr, localization_t* localization_obj, lua_State* script_mngr, void* sound_mngr, input_mngr_t* input_mngr);
GMEXP void game_init_physics(physics_manager_t* mngr);
GMEXP void game_init_scripts(lua_State* lua_state);
GMEXP cpVect game_get_playerpos(int player_num);
GMEXP void game_player_reposition(cpVect pos);
GMEXP void game_debuginfo_update();
GMEXP void game_draw(int zindex);
GMEXP void game_debug_draw(float text_x, float text_y);
GMEXP void game_update(float elapsedTime);
GMEXP void game_move(float mX, float mY);
GMEXP void game_target(float targetX, float targetY, wbool pointer_control, int button_id);
GMEXP cpBB  game_get_playerbox(int player_num);
GMEXP void game_editor_reset();
GMEXP void game_movetarget(float targetX, float targetY, wbool pointer_control);
GMEXP void game_endtarget(float targetX, float targetY, wbool pointer_control, int button_id);
GMEXP void game_offset_target(float offsetX, float offsetY);
GMEXP void game_cleanup(wbool closing);
GMEXP void game_updatescroll();
GMEXP void game_keepresources();
GMEXP void game_update_cursorpos(float x, float y);
GMEXP void game_setcursorpos(float x, float y);
GMEXP wbool game_has_player(int player_num);
GMEXP void game_config_updated();
GMEXP void game_remove_physics(physics_manager_t* const phy_mgr);
GMEXP void game_free();
GMEXP void game_g_cleanup();
GMEXP void game_g_create();
GMEXP void game_pause(wbool playsound);
GMEXP void game_unpause();
//void internal_reset_state();
GMEXP void game_chunk_loaded(void* chunk_data);
GMEXP void game_level_loaded();
GMEXP const char* game_getversion();
GMEXP wbool game_disableinput();
GMEXP void game_input(int16_t input_event, wbool pressed, wbool from_keyboard);
GMEXP void game_setexternapifunc(game_unlock_achievement_t func1, game_show_achievements_t func2, game_show_ads_t func3, game_load_ads_t func4);
GMEXP void game_joystick_axe(int16_t joystick_id, int16_t axe, float axe_value);
GMEXP void game_joystick_button(int16_t joystick_id, int16_t button, wbool pressed);
GMEXP void game_changeinputmode(const char* new_mode);
GMEXP void game_char_input(uint32_t codepoint);
GMEXP void game_wheelpinch(float delta_value, wbool pointer_control);
GMEXP void game_getpausemenu();
GMEXP void game_initlevelstream(const char* app_path, void* module_manager);
GMEXP const char* game_getengineversion();
//callback script functions