
#include <GameObjects/level_base.h>

void gp_setpointers(level_base_t* const pbaselevel, LEVEL_TYPE ptype, callback_script_t pcallback,lua_State* pluastate, physics_manager_t* const  mngr,wbool peditor);

void gp_setcollisionshandlers(physics_manager_t* const  mngr, cpBitmask pwallmask);

