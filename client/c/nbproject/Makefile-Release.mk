#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux-x86
CND_DLIB_EXT=so
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/src/animation_demo.o \
	${OBJECTDIR}/src/chipmunk_demo.o \
	${OBJECTDIR}/src/collision_demo.o \
	${OBJECTDIR}/src/custom_vertices.o \
	${OBJECTDIR}/src/fireball_demo.o \
	${OBJECTDIR}/src/font_demo.o \
	${OBJECTDIR}/src/game_state.o \
	${OBJECTDIR}/src/joystick_demo.o \
	${OBJECTDIR}/src/main.o \
	${OBJECTDIR}/src/normal_mapping_demo.o \
	${OBJECTDIR}/src/rectangle_demo.o \
	${OBJECTDIR}/src/screen_effect_demo.o \
	${OBJECTDIR}/src/shadow_demo.o \
	${OBJECTDIR}/src/shatter_demo.o \
	${OBJECTDIR}/src/sound_demo.o \
	${OBJECTDIR}/src/tilemap_demo.o \
	${OBJECTDIR}/src/url_demo.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/c

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/c: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.c} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/c ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/src/animation_demo.o: src/animation_demo.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/animation_demo.o src/animation_demo.c

${OBJECTDIR}/src/chipmunk_demo.o: src/chipmunk_demo.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/chipmunk_demo.o src/chipmunk_demo.c

${OBJECTDIR}/src/collision_demo.o: src/collision_demo.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/collision_demo.o src/collision_demo.c

${OBJECTDIR}/src/custom_vertices.o: src/custom_vertices.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/custom_vertices.o src/custom_vertices.c

${OBJECTDIR}/src/fireball_demo.o: src/fireball_demo.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/fireball_demo.o src/fireball_demo.c

${OBJECTDIR}/src/font_demo.o: src/font_demo.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/font_demo.o src/font_demo.c

${OBJECTDIR}/src/game_state.o: src/game_state.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/game_state.o src/game_state.c

${OBJECTDIR}/src/joystick_demo.o: src/joystick_demo.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/joystick_demo.o src/joystick_demo.c

${OBJECTDIR}/src/main.o: src/main.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/main.o src/main.c

${OBJECTDIR}/src/normal_mapping_demo.o: src/normal_mapping_demo.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/normal_mapping_demo.o src/normal_mapping_demo.c

${OBJECTDIR}/src/rectangle_demo.o: src/rectangle_demo.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/rectangle_demo.o src/rectangle_demo.c

${OBJECTDIR}/src/screen_effect_demo.o: src/screen_effect_demo.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/screen_effect_demo.o src/screen_effect_demo.c

${OBJECTDIR}/src/shadow_demo.o: src/shadow_demo.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/shadow_demo.o src/shadow_demo.c

${OBJECTDIR}/src/shatter_demo.o: src/shatter_demo.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/shatter_demo.o src/shatter_demo.c

${OBJECTDIR}/src/sound_demo.o: src/sound_demo.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/sound_demo.o src/sound_demo.c

${OBJECTDIR}/src/tilemap_demo.o: src/tilemap_demo.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/tilemap_demo.o src/tilemap_demo.c

${OBJECTDIR}/src/url_demo.o: src/url_demo.c 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/url_demo.o src/url_demo.c

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/c

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
