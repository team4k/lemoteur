﻿#include <common_include.h>
#include <Resx/text_parser.h>
#include <demo_list.h>


static Render_Text_t text_dialog;
static localization_t local_obj;
static wchar_t* test_string = L"abcdefghijklmnopqrstuvwxyz";
static wchar_t* test_parser_string = L"{TD=2.0}{C=FF0000FF}a{C=00FF00FF}b{C=0000FFFF}c{CD=FFCC35FF}def{TF}ghijk{CF}lmn{T=4.0}op{T=0.5}qrstuvwxyz";

wchar_t* outbuffer = NULL;
ColorA_t* outcolorbuffer = NULL;
float* outsizebuffer = NULL;

//static float zoom = 0.0625;
static float zoom = 1.0;
static float xtext = 0;
static float ytext = 0;
static float step = 4.0f;
//static uint8_t font_size = 12;
//static uint8_t font_size = 56;
static uint8_t font_size = 32;
static ColorA_t background = blue_color;

static void trashcache()
{
	if (text_dialog.message != NULL) {
		wf_free(text_dialog.message);
		text_dialog.message = NULL;
		SpriteBatch_cleanupbuffer(&text_dialog.batch, 0);
		text_dialog.batch.numSprites = 0;
	}
}

static void KeyboardCallback(int16_t input_event, wbool pressed, wbool from_keyboard)
{
	if (input_event == KEY_SPACE && !pressed)
	{
		//test_string = L"abcdefghijklmnopqrstuvwxyz";
		zoom /= 2;
	}

	if (input_event == KEY_ENTER && !pressed) {
		zoom *= 2;
	}

	if (input_event == KEY_D && !pressed) {
		font_size += 2;
		trashcache();
	}

	if (input_event == KEY_E && !pressed) {
		font_size -= 2;
		trashcache();
	}

	if (input_event == KEY_LEFT && !pressed) {
		xtext -= step;
	}
	else if (input_event == KEY_RIGHT) {
		xtext += step;
	}

	if (input_event == KEY_UP) {
		ytext += step;
	}
	else if (input_event == KEY_DOWN) {
		ytext -= step;
	}

	if (input_event == KEY_I)
	{
		background = (background.r == white_color.r) ? blue_color : white_color;
	}
}

static int draw_function(gamestate_t* const game_state)
{
	game_state->render_mngr.zoom_factor = zoom;
	render_clear_screenwithcolor(&game_state->render_mngr, background);
	render_setuniform_float(&game_state->render_mngr, "pxRange", (float)text_dialog.info->distanceRange);
	render_setuniform_float(&game_state->render_mngr, "inzoom", zoom);
	render_push_matrix(&game_state->render_mngr);
	Render_Text_begin(&text_dialog,1.0f,1.0f,1.0f,1.0f,&game_state->render_mngr);


	//Render_Text_Draw(&text_dialog,test_string, xtext, ytext, font_size,&game_state->render_mngr);
	//Render_Text_Draw(&text_dialog, test_string, xtext, ytext, font_size, &game_state->render_mngr);
	Render_Text_Draw_withcolornsize(&text_dialog, outbuffer, xtext, ytext, font_size, &game_state->render_mngr, outcolorbuffer, outsizebuffer);

	Render_Text_end(&text_dialog,&game_state->render_mngr);
	render_pop_matrix(&game_state->render_mngr);
	return 0;
}

static int update_func(gamestate_t* const game_state, float elapsed)
{

	return 1;
}

int font_demo(gamestate_t* const game_state)
{
		//bordure
	//https://www.redblobgames.com/x/2404-distance-field-effects/
//https://stackoverflow.com/questions/26155614/outlining-a-font-with-a-shader-and-using-distance-field#26161741

	//pour les bordure, l'important est la génération de la texture msdf avec les paramètres emrange et la taille de de police
	//il arrive qu'avec des taille de police plus petite, l'intégralité du caractère sera recouvert par une ombre noire, cela vient du paramètre emrange en fonction de la taille de la police lors de la génération de la texture 

	//I can fix this problem by a combination of:

	//switching from pxrange to emrange
		//embiggening the emrange from 0.1 to something higher
		//shrinking the outline width from 1px to 0.75px
		//setting an outline relative to the font size instead of in absolute units

	//Through some experimentation I decided to set the default absolute outline width to 1 / 3px, and the relative outline width to 1 / 16th the font size.At a small font size, the absolute width contributes significantly.At a large font size, the relative width contributes significantly.I think this combination is a pleasing default.

	//But because I want to play with the sliders in this demo app, I decided to set the emrange to 0.5 and font resolution to ~48 to accomodate larger absolute widths. For a real project I would probably use 0.2 and ~36.


	game_state->demo_draw_func = draw_function;
	game_state->demo_update_func = update_func;

	SHADERPROGRAM_ID prog_font = render_get_program(&game_state->render_mngr, "prog_font_outline");

	Render_Text_Init(&text_dialog,100,&game_state->render_mngr);

	font_info* info = getfont_resx(&game_state->resx_mngr,"GelPenUprightLight_outline.json");//VCR_OSD_MONO
	

	Render_Text_Setinfo(&text_dialog,info);

	size_t bufferlen = a_wcslen(test_parser_string);

	 outbuffer = (wchar_t*)wf_calloc(bufferlen, sizeof(wchar_t));
	 outcolorbuffer = (ColorA_t*)wf_calloc(bufferlen, sizeof(ColorA_t));
	 outsizebuffer = (float*)wf_calloc(bufferlen, sizeof(float));

	text_parser_parse(test_parser_string, white_color, outbuffer, outcolorbuffer, outsizebuffer, bufferlen);


	char db_path[WAFA_MAX_PATH] = { 0 };
	char app_path[WAFA_MAX_PATH] = { 0 };
	get_app_path(&app_path[0]);


	sprintf(db_path,"%slocale.db",app_path);



	localization_init(&local_obj,db_path);

	localization_setlocale(&local_obj,"fr");

		//localization_getstring(&local_obj,"test_unicode");
	//test_string = L"Wa";
	//test_string = L"ABCDEFGHIJKLWabcdefghijklw";
	test_string = L"Voix ambiguë d'un coeur qui au zéphyr préfère les jattes de kiwis. 1234567890";
	//test_string = L"ëêéè";

	//test_string = L"Hello,world is .not true";


	render_use_program(&game_state->render_mngr, prog_font);
	game_state->module_mngr.game_input = KeyboardCallback;
	

	game_state->gameloop_func(game_state);

	return 0;
}