#include <common_include.h>
#include <demo_list.h>


static Tilemap_t level_tilemap;
static Vector_t mouse_position;
static int mouse_tile;

static SHADERPROGRAM_ID prog_tex;
static SHADERPROGRAM_ID prog_notex;

static render_manager_t* render_mngr;

static wbool has_start = wfalse;
static Rect_t start_cell = {32,32,{0,0}};
static int start = -1;
static wbool has_end = wfalse;
static Rect_t goal_cell = {32,32,{0,0}};
static int end = -1;

static wbool walk_grid = wfalse;

static render_manager_t* get_render()
{
	return render_mngr;
}

static void Mouse_callback_click(float targetX,float targetY,wbool pointer_control,int button_id)
{
	
	if(button_id == MOUSE_BUTTON_LEFT)
	{
		if(has_start && has_end)
			has_start = has_end = wfalse;


		if(!has_start)
		{
			start_cell.position = mouse_position;
			start = mouse_tile;
			has_start= wtrue;
		}
		else if(!has_end)
		{
			goal_cell.position = mouse_position;
			end = mouse_tile;
			has_end = wtrue;
		}
			
	}
}

static void Mouse_callback_move(float targetX,float targetY,wbool pointer_control)
{
	int32_t tile_x = (targetX / level_tilemap.tile_size);
	int32_t tile_y  = (targetY / level_tilemap.tile_size);

	float pos_x = tile_x * level_tilemap.tile_size;
	float pos_y = (tile_y + 1) * level_tilemap.tile_size;

	mouse_position = Vec(pos_x,pos_y);
	mouse_tile = (tile_y * level_tilemap.col_count) + tile_x;

}

//grid walk variable
int32_t start_row = 0;
int32_t start_col = 0;
int32_t current_row = 0;
int32_t current_col = 0;
int32_t goal_row = 0;
int32_t goal_col = 0;
Vector_t grid_dir = {0,-1};

wbool change_dir = wtrue;

int32_t ignore_step = 0; 
int32_t ignore_counter = 0;
int32_t max_ignore = 0;

int32_t counter_acc = 1;
int32_t counter_acc2 = 1;
int32_t counter_acc3 = 1;
int32_t counter_acc4 = 1;

int32_t ignore_adder = 0;
int32_t step_adder = 0;


int* grid_walk_array = NULL;
int32_t grid_walk_array_indx = 0;

float timeout = 0;

static void  KeyboardCallback(int16_t input_event,wbool pressed,wbool from_keyboard)
{
	if(input_event == KEY_SPACE && !pressed && has_start && has_end)
	{
		walk_grid = wtrue;

		current_row = (int32_t)floorf((float)(start / level_tilemap.col_count));//ligne Y
		current_col = start - (level_tilemap.col_count * current_row);//ligne X

		start_row = current_row;
		start_col = current_col;

		current_row--;


		if(current_row < 0)
			current_row = 0;
		else if(current_row >= level_tilemap.row_count)
			current_row = level_tilemap.row_count - 1;

		if(current_col < 0)
			current_col = 0;
		else if(current_col >= level_tilemap.col_count)
			current_col = level_tilemap.col_count -1;


		goal_row = (int32_t)floorf((float)(end / level_tilemap.col_count));//ligne Y
		goal_col = end - (level_tilemap.col_count * goal_row);//ligne X

		if(goal_row < 0)
			goal_row = 0;
		else if(goal_row >= level_tilemap.row_count)
			goal_row = level_tilemap.row_count - 1;

		if(goal_col < 0)
			goal_col = 0;
		else if(goal_col >= level_tilemap.col_count)
			goal_col = level_tilemap.col_count -1;


		grid_dir.x = 0;
		grid_dir.y = -1;

		if(grid_walk_array)
			wf_free(grid_walk_array);

		grid_walk_array_indx = 0;

		grid_walk_array = (int*)wf_calloc((level_tilemap.row_count * level_tilemap.col_count) , sizeof(int));

		change_dir = wtrue;
		ignore_step = 0;
		ignore_counter = 0;
		max_ignore = 0;
		ignore_adder = 0;
	}

}



static int draw_func(gamestate_t* const game_state)
{
	render_use_program(&game_state->render_mngr,prog_tex);
	Tilemap_Draw(&level_tilemap,0,&game_state->render_mngr,vectorzero,NULL,&game_state->resx_mngr);

	//draw cursor
	Rect_t rec = {level_tilemap.tile_size,level_tilemap.tile_size,mouse_position};

	render_use_program(&game_state->render_mngr,prog_notex);
	drawrect(&rec,blue_color,&game_state->render_mngr);

	if(has_start)
		drawrect(&start_cell,green_color,&game_state->render_mngr);

	if(has_end)
		drawrect(&goal_cell,red_color,&game_state->render_mngr);

	//draw grid walking
	if(walk_grid)
	{
		for(int32_t i_res = 0; i_res < grid_walk_array_indx;i_res++)
		{
			int32_t tRow = (int32_t)floorf((float)(grid_walk_array[i_res] / level_tilemap.col_count));//ligne Y
			int32_t tCol = grid_walk_array[i_res]  - (level_tilemap.col_count * tRow);//ligne X

			int32_t x_tile = tCol * level_tilemap.tile_size;
			int32_t y_tile = (tRow+1) * level_tilemap.tile_size;

			Rect_t rec_res = {32,32,{x_tile,y_tile}};

			ColorA_t colour = black_color;

			if(i_res == grid_walk_array_indx -1)
				colour = white_color;

			drawrect(&rec_res,colour,&game_state->render_mngr);
		}

		if(timeout <= 0)
		{
			//add new index
			grid_walk_array[grid_walk_array_indx++] = (current_row * level_tilemap.col_count) + current_col;



			if(ignore_counter == 0)
			{
				if(grid_dir.y != 0.0f)
				{
					grid_dir.x = grid_dir.y * -1;
					grid_dir.y = 0.0f;
				
				}
				else if(grid_dir.x != 0.0f)
				{
					grid_dir.y = grid_dir.x;
					grid_dir.x = 0.0f;
				}

				ignore_counter = max_ignore;
				ignore_step++;
			}
			else
			{
				ignore_counter--;
			}


			if(ignore_step == 2)
			{
				max_ignore++;
				max_ignore += ignore_adder;

				ignore_adder = 0;

				ignore_counter = max_ignore;
				ignore_step = 0;
			}

			current_row += grid_dir.y;
			current_col += grid_dir.x;

			if(current_col < 0)
			{
				//if(max_ignore == 0)
					//ignore_counter = 1;
				//else
					//ignore_counter = max_ignore - ((max_ignore - ignore_counter) - 2);

				ignore_counter = max_ignore + counter_acc;
				counter_acc--;

				ignore_adder = 1;

				grid_dir.x = -grid_dir.x;
				grid_dir.y = 0.0f;
				current_col = 0;
				current_row -= ((current_row - start_row) * 2) + 1;
			}

			if(current_col >= level_tilemap.col_count)
			{
				ignore_counter = max_ignore + counter_acc;
				counter_acc--;

				ignore_adder = 1;

				grid_dir.x = -grid_dir.x;
				grid_dir.y = 0.0f;
				current_col = level_tilemap.col_count - 1;
				current_row += ((start_row - current_row) * 2);
			}

			/*if(current_row < 0)
			{
				ignore_counter = max_ignore + counter_acc;
				counter_acc--;

				ignore_adder = 1;

				grid_dir.y = -grid_dir.y;
				grid_dir.x = 0.0f;
				current_row = 0;
				current_col += ((current_col - start_col) * 2 ) + 1; 
			}
			*/

			if(current_row == goal_row && current_col == goal_col)
				walk_grid = wfalse;
		}

	}


	/*if(result_obj != NULL && result_obj->path_computed && has_start && has_end)
	{
		

		 
	}*/


	return 1;
}

static int update_func(gamestate_t* const game_state,float elapsed)
{
	float mov_value = 0.32f * elapsed;

	if(input_mngr_getkey(&game_state->input_mngr,KEY_DOWN) == KEY_PRESS)
			game_state->render_mngr.scroll_vector.y += (int)mov_value;
        
	if(input_mngr_getkey(&game_state->input_mngr,KEY_UP) == KEY_PRESS)
			game_state->render_mngr.scroll_vector.y -= (int)mov_value;

	if(input_mngr_getkey(&game_state->input_mngr,KEY_LEFT) == KEY_PRESS)
		game_state->render_mngr.scroll_vector.x += (int)mov_value;

	if(input_mngr_getkey(&game_state->input_mngr,KEY_RIGHT) == KEY_PRESS)
			game_state->render_mngr.scroll_vector.x -= (int)mov_value;


	if(walk_grid)
	{
		if(timeout <= 0)
			timeout = 200;
		else
			timeout -= elapsed;
	}


		return 1;
}


int test_grid_walk_demo(gamestate_t* const game_state)
{
	game_state->demo_draw_func = draw_func;
	game_state->demo_update_func = update_func;

	render_mngr = &game_state->render_mngr;

	prog_tex = render_get_program(&game_state->render_mngr,"prog_common");
	prog_notex = render_get_program(&game_state->render_mngr,"prog_notex");

	Level* pathfind_level = getlevel_resx(&game_state->resx_mngr,"test_pathfinding.lwf");

	 //check level
    if(!pathfind_level)
    {
        logprint("Erreur chargement niveau ! %s ","test_pathfinding.lwf");
        return 0;
    }

	//get first map texture name
	char* file_name = (char*)wf_malloc(strlen(pathfind_level->map_array[0]->tileset_id) * sizeof(char));

	get_file_name(pathfind_level->map_array[0]->tileset_id,file_name,wfalse);

	sprintf(file_name,"%s%s",&file_name[0],".png");

	texture_info* tileset = gettexture_resx(&game_state->resx_mngr,&file_name[0]);

    //check tileset texture
    if(!tileset)
    {
        logprint("Erreur chargement Texture niveau ! %s ",file_name);
        return 0;
    }


    //-2- create tilemap based on protobuf data
	Tilemap_Init(&level_tilemap,32,tileset,tileset->texture_name,pathfind_level->map_array[0]);
	Tilemap_AddLayer(&level_tilemap,&game_state->render_mngr,pathfind_level->map_array[0]->layer_array[0],tileset,NULL);
	Tilemap_AddCollisions(&level_tilemap,pathfind_level->map_array[0]->layer_array[1]);


	game_state->module_mngr.game_input = KeyboardCallback;
	game_state->module_mngr.game_movetarget = Mouse_callback_move;
	game_state->module_mngr.game_endtarget = Mouse_callback_click;
		




	game_state->gameloop_func(game_state);

	wf_free(file_name);

	return 0;
}