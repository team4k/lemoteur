#include <common_include.h>

#include <demo_list.h>


static float start = 0;

static float delta = 15.0f;

static float del = 0;

static int draw_custom_vert(gamestate_t* const game_state)
{
	render_push_matrix(&game_state->render_mngr);

	 float radius = 20;
	 float x2=0,y2=0;
	 int cache = 0;

	 cpVect p1 = cpv(50,50);
	 cpVect p2 = cpv(500,150);

	 float d = cpvdist(p1,p2);

	#ifdef __linux__
	 float a = atan2(p2.y - p1.y,p2.x-p1.x);
	#else
	 float a = atan2f(p2.y - p1.y,p2.x-p1.x);
	#endif

	 float vertices[8192];
	 int32_t elements_num = 0;
	 float freq = 10;
	 float amp = 16;

	 float const_freq = 0.055f;

	 for(float i = delta;i<= d+delta;i+=1)
	 {
         if(elements_num >= 8192)
				break;

		 vertices[elements_num] = i-delta;
		 vertices[elements_num+1] = sin(i * WM_PI_2 * const_freq) * amp;
         vertices[elements_num+2] = (i+1)-delta;
		 vertices[elements_num+3] = sinf((i+1) * WM_PI_2 * const_freq) * amp;

		 elements_num+=4;
	 }


	 Vector_t pos = {50,50};

	 render_set_position(&game_state->render_mngr,pos);

 
	 render_set_rotation(&game_state->render_mngr,rad2deg(a));


	 render_start_draw_array(&game_state->render_mngr,2,elements_num * 0.5f,vertices,NULL);

	 render_draw_line(&game_state->render_mngr,0,elements_num * 0.5f);
	 render_end_draw_array(&game_state->render_mngr,wfalse);

	 render_pop_matrix(&game_state->render_mngr);

	return 0;
}

static int update_custom_vert(gamestate_t* const game_state,float elapsed)
{		
	//update screen filler position
	if(del <= 0.0f)
	{
			if(start >360) 
			{
			start = 0;
			}
			else
			{
				start += 1.0f;
			}

		del = delta;
	}

	del -= elapsed;

	return 0;
}


int custom_vertices_demo(gamestate_t* const game_state)
{
	game_state->demo_draw_func = draw_custom_vert;
	game_state->demo_update_func = update_custom_vert;
	del = delta;

	Vector_t pos = {50,50};

	render_use_program(&game_state->render_mngr,render_get_program(&game_state->render_mngr,"prog_notex"));


	game_state->gameloop_func(game_state);

	return 0;
}
