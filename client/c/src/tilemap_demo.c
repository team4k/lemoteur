#include <common_include.h>

#include <demo_list.h>

static sprite_t avatar;
static Vector_t avatar_position;
static render_manager_t* render_manager;
static Tilemap_t level_tilemap;


static int demo_draw(gamestate_t* const game_state)
{
    //draw elements       
	Tilemap_Draw(&level_tilemap,1,&game_state->render_mngr,vectorzero,NULL,&game_state->resx_mngr);

    //avatar_update(&avatar,elapsed);
	sprite_draw(&avatar,avatar_position.x,avatar_position.y,&game_state->render_mngr,NULL);

	return 0;

}

static int demo_update(gamestate_t* const game_state, float elapsed)
{

	float mov_value = 0.2f * elapsed;

	if(input_mngr_getkey(&game_state->input_mngr,KEY_DOWN) == KEY_PRESS)
            avatar_position.y -= mov_value;
        
    if(input_mngr_getkey(&game_state->input_mngr,KEY_UP) == KEY_PRESS)
        avatar_position.y += mov_value;

    if(input_mngr_getkey(&game_state->input_mngr,KEY_LEFT) == KEY_PRESS)
        avatar_position.x -= mov_value;

    if(input_mngr_getkey(&game_state->input_mngr,KEY_RIGHT) == KEY_PRESS)
       avatar_position.x += mov_value;


	render_manager->scroll_vector.x =   ((render_manager->screen_info.width / 2) -  ((float)avatar_position.x / render_manager->zoom_factor));

	render_manager->scroll_vector.y =  ((render_manager->screen_info.height / 2) -  ((float)avatar_position.y / render_manager->zoom_factor));

	return 0;
}


int tilemap_demo(gamestate_t* const game_state)
{
	game_state->demo_draw_func = demo_draw;
	game_state->demo_update_func = demo_update;

	render_manager = &game_state->render_mngr;

	texture_info* player_texture = gettexture_resx(&game_state->resx_mngr,"hero.png");
    //
    Level* pb_level = getlevel_resx(&game_state->resx_mngr,"niveau1.lwf");

    //check player texture
    if(!player_texture)
    {
        logprint("Erreur chargement Texture joueur ! %s ","hero.png");
        return 0;
    }

     //check level
    if(!pb_level)
    {
        logprint("Erreur chargement niveau ! %s ","niveau1.lwf");
        return 0;
    }


	//get first map texture name
	char* file_name = (char*)wf_malloc(strlen(pb_level->map_array[0]->tileset_id) * sizeof(char));;

	get_file_name(pb_level->map_array[0]->tileset_id,file_name,wfalse);

	sprintf(file_name,"%s%s",&file_name[0],".png");

	texture_info* tileset = gettexture_resx(&game_state->resx_mngr,&file_name[0]);

    //check tileset texture
    if(!tileset)
    {
        logprint("Erreur chargement Texture niveau ! %s ","Tileset.png");
        return 0;
    }


    //-2- create tilemap based on protobuf data
	Tilemap_Init(&level_tilemap,32,tileset,tileset->texture_name,pb_level->map_array[0]);
	pb_level->map_array[0]->layer_array[0]->opacity = 1.0f;
	Tilemap_AddLayer(&level_tilemap,&game_state->render_mngr,pb_level->map_array[0]->layer_array[0],tileset,NULL);

    //-3- create avatar gameobject
	sprite_init(&avatar,32,32,32,32,player_texture,NULL,0,0,&game_state->render_mngr,wfalse,0,1.0f,1.0f,wtrue);

    level_tilemap.show_collisions = wtrue;

	game_state->gameloop_func(game_state);

	wf_free(file_name);


    return 0;

}