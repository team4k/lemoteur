#include <common_include.h>
#include <demo_list.h>


static Tilemap_t level_tilemap;
static Vector_t mouse_position;
static int mouse_tile;

static SHADERPROGRAM_ID prog_tex;
static SHADERPROGRAM_ID prog_notex;

static render_manager_t* render_mngr;

static const int tilesize = 64;

static wbool has_start = wfalse;
static Rect_t start_cell = { tilesize ,tilesize ,{0,0}};
static int start = -1;
static wbool has_end = wfalse;
static Rect_t goal_cell = { tilesize ,tilesize ,{0,0}};
static int end = -1;

static pathfind_worker_t worker;
static pathfind_result_t* result_obj = NULL;

static const ColorA_t path_color = { 0.0f,0.0f,0.0f,0.5f };

#define LOMBRIC_NO_STICK_CEIL 1 //no movement on ceiling possible
#define LOMBRIC_ONLY_LEFT 2 //only find on the left side of the start tile
#define LOMBRIC_ONLY_RIGHT 4 //only find on the right side of the start tile
#define LOMBRIC_MOVETHROUGHROCK 8 //can move through undestructible collisions from bottom to top
#define LOMBRIC_NOSAFEZONE 16 //can't move through safe zone


static render_manager_t* get_render()
{
	return render_mngr;
}

static void mouse_endclick_callback(float targetX,float targetY,wbool pointer_control,int button_id)
{
	if(button_id == MOUSE_BUTTON_LEFT)
	{
		if(has_start && has_end)
			has_start = has_end = wfalse;


		if(!has_start)
		{
			start_cell.position = mouse_position;
			start = mouse_tile;
			has_start= wtrue;
		}
		else if(!has_end)
		{
			goal_cell.position = mouse_position;
			end = mouse_tile;
			has_end = wtrue;
		}
			
	}
}


static void mouse_move_callback(float targetX,float targetY,wbool pointer_control)
{
	int32_t tile_x = (targetX / level_tilemap.tile_size);
	int32_t tile_y = (targetY / level_tilemap.tile_size);


	float pos_x = tile_x * level_tilemap.tile_size;
	float pos_y = (tile_y + 1) * level_tilemap.tile_size;


	mouse_position = Vec(pos_x,pos_y);
	mouse_tile = (tile_y * level_tilemap.col_count) + tile_x;

}

static void keyboard_callback(int16_t input_event,wbool pressed,wbool from_keyboard)
{

	//(LOMBRIC_NO_STICK_CEIL | LOMBRIC_MOVETHROUGHROCK )
	if (input_event == KEY_SPACE && !pressed && has_start && has_end) {
		result_obj = pathfind_dorequest(&worker, "test_request", start, end, (wbool)(result_obj != NULL), (LOMBRIC_NO_STICK_CEIL | LOMBRIC_MOVETHROUGHROCK | LOMBRIC_NOSAFEZONE));
		//
	}
}

static int draw_func(gamestate_t* const game_state)
{
	render_use_program(&game_state->render_mngr,prog_tex);
	Tilemap_Draw(&level_tilemap,1,&game_state->render_mngr,vectorzero,NULL,&game_state->resx_mngr);

	//draw cursor
	Rect_t rec = {level_tilemap.tile_size,level_tilemap.tile_size,mouse_position};

	render_use_program(&game_state->render_mngr,prog_notex);
	drawrect(&rec,blue_color,&game_state->render_mngr);

	if(has_start)
		drawrect(&start_cell,green_color,&game_state->render_mngr);


	if(has_end)
		drawrect(&goal_cell,red_color,&game_state->render_mngr);

	//draw result when request ended
	if(result_obj != NULL && result_obj->path_computed && has_start && has_end)
	{
		for(int32_t i_res = 0; i_res < result_obj->path_result_size;i_res++)
		{
			int32_t tRow = (int32_t)floorf((float)(result_obj->path_result[i_res] / level_tilemap.col_count));//ligne Y
			int32_t tCol = result_obj->path_result[i_res]  - (level_tilemap.col_count * tRow);//ligne X

			int32_t x_tile = tCol * level_tilemap.tile_size;
			int32_t y_tile = (tRow+1) * level_tilemap.tile_size;

			Rect_t rec_res = { tilesize ,tilesize ,{x_tile,y_tile}};

			drawrect(&rec_res,path_color,&game_state->render_mngr);
		}

	}




	return 1;
}

static int update_func(gamestate_t* const game_state,float elapsed)
{
	float mov_value = 0.32f * elapsed;

	if(input_mngr_getkey(&game_state->input_mngr,KEY_DOWN) == KEY_PRESS)
		game_state->render_mngr.scroll_vector.y += (int)mov_value;
        
	if(input_mngr_getkey(&game_state->input_mngr,KEY_UP) == KEY_PRESS)
		game_state->render_mngr.scroll_vector.y -= (int)mov_value;

	if(input_mngr_getkey(&game_state->input_mngr,KEY_LEFT) == KEY_PRESS)
		game_state->render_mngr.scroll_vector.x += (int)mov_value;

	if(input_mngr_getkey(&game_state->input_mngr,KEY_RIGHT) == KEY_PRESS)
		game_state->render_mngr.scroll_vector.x -= (int)mov_value;

	#ifdef EMSCRIPTEN
		pathfind_simupdate(&worker);
	#endif

	input_mngr_handle_input(&game_state->input_mngr);

		return 1;
}


int pathfind_demo(gamestate_t* const game_state)
{
	game_state->demo_draw_func = draw_func;
	game_state->demo_update_func = update_func;

	render_mngr = &game_state->render_mngr;
	render_mngr->zoom_factor = 4.0f;
	prog_tex = render_get_program(&game_state->render_mngr,"prog_common");
	prog_notex = render_get_program(&game_state->render_mngr,"prog_notex");

	//Level* pathfind_level = getlevel_resx(&game_state->resx_mngr,"test_lombric.lwf");
	Level* pathfind_level = getlevel_resx(&game_state->resx_mngr, "test_lombric_2.lwf");
	

	//Level* pathfind_level = getlevel_resx(&game_state->resx_mngr, "test_pathfinding.lwf");

	 //check level
    if(!pathfind_level)
    {
        logprint("Erreur chargement niveau ! %s ","test_lombric.lwf");
        return 0;
    }

	//get first map texture name
	char* file_name = (char*)wf_malloc(strlen(pathfind_level->map_array[0]->tileset_id) * sizeof(char));

	get_file_name(pathfind_level->map_array[0]->tileset_id,file_name,wfalse);

	sprintf(file_name,"%s%s",&file_name[0],".png");

	texture_info* tileset = gettexture_resx(&game_state->resx_mngr,&file_name[0]);

    //check tileset texture
    if(!tileset)
    {
        logprint("Erreur chargement Texture niveau ! %s ",file_name);
        return 0;
    }


    //-2- create tilemap based on protobuf data
	Tilemap_Init(&level_tilemap, tilesize,tileset,tileset->texture_name,pathfind_level->map_array[0]);
	Tilemap_AddLayer(&level_tilemap,&game_state->render_mngr,pathfind_level->map_array[0]->layer_array[0],tileset,NULL);
	Tilemap_AddCollisions(&level_tilemap,pathfind_level->map_array[0]->layer_array[1]);

	//setup pathfinder worker
	pathfind_init(&worker,10,level_tilemap.collisions_array,level_tilemap.col_count,level_tilemap.row_count);

	pathfind_setalgo(&worker, lombric);


	game_state->module_mngr.game_input = keyboard_callback;
	game_state->module_mngr.game_movetarget = mouse_move_callback;
	game_state->module_mngr.game_endtarget = mouse_endclick_callback;

	game_state->gameloop_func(game_state);

	wf_free(file_name);

	return 0;
}