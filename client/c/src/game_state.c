#include <common_include.h>

void get_world_coords(Vector_t* world_vector, int x, int y)
{
    world_vector->x = (float)x;
    world_vector->y = (float)y;
}

static wbool running = wtrue;

static double frameRenderTime = 0;
static double rate = 0;

double oversleep = 0.0;
double overtime;

#define MAX_FPS 60.0f

#define max_frame_time 1000.0f / MAX_FPS

double sleep_time = 0;


static void loop(void* arg)
{
	gamestate_t* gamestate = (gamestate_t*)arg;
	double newTime = render_gettime();

	render_clear_screen(&gamestate->render_mngr);
	//render_clear_screenwithcolor(&gamestate->render_mngr,blue_color);

	render_update_screen(&gamestate->render_mngr);

	if(gamestate->demo_draw_func)
		gamestate->demo_draw_func(gamestate);

	if(gamestate->demo_update_func)
		gamestate->demo_update_func(gamestate,max_frame_time);



	render_swap_buffers(&gamestate->render_mngr);

	input_mngr_poll(&gamestate->input_mngr);

	if(input_mngr_getkey(&gamestate->input_mngr,KEY_ESC) == KEY_PRESS)
		running = wfalse;


	double end = render_gettime();
		
	frameRenderTime = end - newTime;

	if(frameRenderTime < max_frame_time )
	{
		sleep_time = (max_frame_time  - frameRenderTime) - oversleep;
			
		if(sleep_time < 0)
			sleep_time = 0;
			
		if(overtime > 0)
		{
			if(overtime > sleep_time)
			{
				overtime -= sleep_time;
				sleep_time = 0;
			}
			else
			{
				sleep_time -= overtime;
				overtime = 0;
			}
		}

	#ifndef EMSCRIPTEN
		usleep(sleep_time * 1000);
	#endif
				
	}
	else //over maximum frame time, store the difference to reduce sleep time of further frames
	{
		overtime += frameRenderTime - max_frame_time;
	}
		
	oversleep =  render_gettime() - end - sleep_time;//take into account OS scheduling, the end value should be zero

		
	if(oversleep < 0)
		oversleep = 0;
	
}

wbool base_disable_input()
{
	return wfalse;
}

void base_game_target(float targetX,float targetY,wbool pointer_control,int button_id)
{

}

void base_game_end_target(float targetX,float targetY,wbool pointer_control,int button_id)
{

}
void base_setcursor_pos(float x,float y)
{

}
void base_move_target(float targetX,float targetY,wbool pointer_control)
{
}
void base_offset_target(float offsetX,float offsetY)
{
}
void base_game_input(int16_t input_event,wbool pressed,wbool from_keyboard)
{
}
void base_game_char_input(uint32_t codepoint)
{
}
void base_joystick_axe(int16_t joystick_id,int16_t axe,float axe_value)
{
}
void base_joystick_button(int16_t joystick_id,int16_t button,wbool pressed)
{
}
void base_change_input_mode(const char* new_mode)
{
}


int gameloop(gamestate_t* const gamestate)
{
	frameRenderTime = 16.7f;

	#ifdef EMSCRIPTEN
		emscripten_set_main_loop_arg(loop,gamestate,0,1);
		emscripten_set_main_loop_timing(EM_TIMING_RAF, 1);
	#else

		while( running )
		{
			loop(gamestate);
		}
	#endif

	return 1;
}