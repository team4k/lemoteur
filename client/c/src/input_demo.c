#include <common_include.h>

#include <demo_list.h>

static input_mngr_t* input_mngr = NULL;


static void callback_joystick_axe(int16_t joystick_id,int16_t axe,float axe_value)
{
	if (fabsf(axe_value) != 0.0f)
	{
		float axe_up_limit = 0.8f;
		float axe_down_limit = -0.8f;

		//gachette manette xbox
		if (axe == 4 || axe == 5) {
			axe_down_limit = -1.0f;
		}

		wbool pressed = (wbool)(axe_value >axe_up_limit || axe_value < axe_down_limit);

		if (pressed) {
			gamepad_scheme scheme = input_mngr_getscheme(input_mngr);

			const char* axisname = input_mngr_getaxisname(input_mngr, axe, scheme);

			if (axisname != NULL)
			{
				logprint("axis name is %s, with value %5.2f", axisname, axe_value);
			}
		}
			
	}
		

}

static void callback_joystick_button(int16_t joystick_id,int16_t button,wbool pressed)
{
	if (pressed)
	{

		logprint("button %d is pressed", button);

		gamepad_scheme scheme =  input_mngr_getscheme(input_mngr);

		const char* buttonname = input_mngr_getbuttonname(input_mngr, button, scheme);

		if (buttonname != NULL)
		{
			logprint("button name is %s", buttonname);
		}
	}
		

}


static void callback_changeinputmode(const char* newmode)
{
	logprint("input mode is %s", newmode);

	if (strcmp(newmode, "joystick") == 0) {
		logprint("joystick connected is %d", input_mngr->first_found_joystick);

		/*if (glfwJoystickIsGamepad(input_mngr->first_found_joystick))
		{
			const char* name = glfwGetJoystickName(input_mngr->first_found_joystick);
			const char* name2 = glfwGetGamepadName(input_mngr->first_found_joystick);
			logprint("joystick is gamepad named %s %s", name,name2);
		}*/
	}
}

static void callback_input(int16_t input_event, wbool pressed, wbool from_keyboard)
{
	if(pressed && from_keyboard)
		logprint("key %d is pressed %s", input_event, input_mngr_getkeyname(input_mngr,input_event));
}


static int draw_input(gamestate_t* const gamestate)
{
	

	return 0;
}

static int update_input(gamestate_t* const gamestate,float elapsed)
{

	input_mngr_handle_input(&gamestate->input_mngr);



	return 0;
}

int input_demo(gamestate_t* const gamestate)
{
	size_t lSize = 0;

	//const char* mappings = (const char*)get_resx_content(&gamestate->resx_mngr,"gamecontrollerdb.txt", &lSize, NULL);

	//glfwUpdateGamepadMappings(mappings);


	gamestate->demo_draw_func = draw_input;
	gamestate->demo_update_func = update_input;

	gamestate->module_mngr.game_joystick_axe = callback_joystick_axe;
	gamestate->module_mngr.game_joystick_button = callback_joystick_button;
	gamestate->module_mngr.game_changeinputmode = callback_changeinputmode;
	gamestate->module_mngr.game_input = callback_input;

	input_mngr = &gamestate->input_mngr;
	

	gamestate->gameloop_func(gamestate);

	return 0;
}