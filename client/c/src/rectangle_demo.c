#include <common_include.h>
#include <demo_list.h>


static Rect_t test_rectangle = {300,300,{200,480}};
static Rect_t test_rectangle2 = {64,64,{250,250}};
static ColorA_t color = {1.0f,0.0f,0.0f,1.0f};
static Circle_t test_circle = {50.0f,{400,300}};
static ColorA_t color_circ = {0.0f,0.0f,1.0f,1.0f};


static int draw_func(gamestate_t* const game_state)
{
	drawrect(&test_rectangle,color,&game_state->render_mngr);

	drawcircle(&test_circle,color_circ,&game_state->render_mngr);
	return 1;
}

static int update_func(gamestate_t* const gamestate,float elapsed)
{
	float mov_value = 0.5f * elapsed;

	
	if(input_mngr_getkey(&gamestate->input_mngr,KEY_DOWN) == KEY_PRESS)
			test_rectangle.position.y -= mov_value;
        
	if(input_mngr_getkey(&gamestate->input_mngr,KEY_UP) == KEY_PRESS)
		test_rectangle.position.y += mov_value;

	if(input_mngr_getkey(&gamestate->input_mngr,KEY_LEFT) == KEY_PRESS)
		test_rectangle.position.x  -= mov_value;

	if(input_mngr_getkey(&gamestate->input_mngr,KEY_RIGHT) == KEY_PRESS)
		test_rectangle.position.x += mov_value;


	return 1;
}


int rectangle_demo(gamestate_t* const game_state)
{
	/*wchar_t userpath[WAFA_MAX_PATH] = { 0 };

	get_user_home_dir(&userpath[0]);

	wchar_t gamepath[WAFA_MAX_PATH] = { 0 };

	swprintf(gamepath, WAFA_MAX_PATH, L"%s%c%s", userpath, PATH_SEP, L"lombric");
	

	if (directory_exist(gamepath)) {
		logprint("Répertoire existe");
	}
	else {
		logprint("Répertoire n'existe pas !");
		create_directory(gamepath);
	}*/

	game_state->demo_draw_func = draw_func;
	game_state->demo_update_func = update_func;

	render_use_program(&game_state->render_mngr,render_get_program(&game_state->render_mngr,"prog_notex"));

	game_state->gameloop_func(game_state);


	return 1;
}
