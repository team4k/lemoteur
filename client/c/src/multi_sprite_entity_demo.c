#include <common_include.h>
#include <demo_list.h>


static entity_t avatar_phy;
static SHADERPROGRAM_ID prog_tex = 0;

char* canim_tail = NULL;
char* canim_body = NULL;
char* canim_head = NULL;

char anim_tail[64] = "tail_idle";
char anim_body[64] = "body_idle";
char anim_head[64] = "head_idle";

char anim_tail2[64] = "tail_move";
char anim_body2[64] = "body_move";
char anim_head2[64] = "head_move";

static uint8_t body_part = 0;

static void KeyboardCallback(int16_t input_event, wbool pressed, wbool from_keyboard)
{
	if (input_event == KEY_LEFT && !pressed)
	{
		if (body_part > 0) {
			body_part--;
		}
		else {
			body_part = 2;
		}

	}

	if (input_event == KEY_RIGHT && !pressed)
	{
		if (body_part < 2) {
			body_part++;
		}
		else {
			body_part = 0;
		}
	}

	wbool switch_anim = wfalse;

	if (input_event == KEY_ENTER && !pressed)
	{

		switch_anim = wtrue;
	}


	if (switch_anim)
	{
		char* anim = NULL;

		switch (body_part)
		{
		case 0:
			if (strcmp(canim_tail, anim_tail) == 0) {
				canim_tail = anim_tail2;
			}
			else {
				canim_tail = anim_tail;
			}

			anim = canim_tail;

			break;

		case 1:
			if (strcmp(canim_body, anim_body) == 0) {
				canim_body = anim_body2;
			}
			else {
				canim_body = anim_body;
			}

			anim = canim_body;

			break;

		case 2:
			if (strcmp(canim_head, anim_head) == 0) {
				canim_head = anim_head2;
			}
			else {
				canim_head = anim_head;
			}

			anim = canim_head;

			break;
		}


		entity_playanimation(&avatar_phy, anim, body_part);
	}
}


static int draw_func(gamestate_t* const game_state)
{
	render_use_program(&game_state->render_mngr, prog_tex);

	entity_draw(&avatar_phy, &game_state->render_mngr, NULL,NULL);

	return 1;
}

static int update_func(gamestate_t* const gamestate, float elapsed)
{
	entity_update(&avatar_phy, elapsed, &gamestate->render_mngr,NULL);

	input_mngr_handle_input(&gamestate->input_mngr);

	return 1;
}


int multi_sprite_entity_demo(gamestate_t* const game_state)
{
	game_state->demo_draw_func = draw_func;
	game_state->demo_update_func = update_func;

	prog_tex = render_get_program(&game_state->render_mngr, "prog_common");

	Entity entity_data;
	Wvector entity_pos;

	entity__init(&entity_data);

	const char* entity_id = "lomb";
	entity_data.entity_id = (char*)wf_malloc((strlen(entity_id) + 1) * sizeof(char));
	strcpy(entity_data.entity_id, entity_id);

	entity_data.script_module = NULL;

	entity_data.script_ctor = NULL;

	const char* start_animation = "tail_idle";
	char tmp_anim[256];
	entity_data.start_animation = &tmp_anim[0];
	strcpy(entity_data.start_animation, start_animation);

	const char* tileset = "lomb.png";
	char tmp_tileset[256];
	entity_data.tileset = &tmp_tileset[0];
	strcpy(entity_data.tileset, tileset);

	const char* animation_file = "lomb.awf";
	char tmp_anim2[256];
	entity_data.animation_file = &tmp_anim2[0];
	strcpy(entity_data.animation_file, animation_file);

	entity_data.position = &entity_pos;
	wvector__init(entity_data.position);

	entity_pos.x = 200;
	entity_pos.y = 100;



	entity_data.width = 64;
	entity_data.height = 96;
	entity_data.shape_type = TYPE_PHYSICS__BOX;
	entity_data.type = 1;
	entity_data.has_controller = (protobuf_c_boolean)0;
	entity_data.no_collisions = (protobuf_c_boolean)1;
	entity_data.has_callback = (protobuf_c_boolean)0;
	entity_data.z_order = 1;
	entity_data.custom_collisions = (protobuf_c_boolean)0;
	entity_data.offsetx = 0;
	entity_data.offsety = 0;
	entity_data.col_height = 128;
	entity_data.col_width = 256;
	entity_data.repeat_x = 0;
	entity_data.repeat_y = 0;
	entity_data.start_frame = 0;
	entity_data.parallax_x = 1.0f;
	entity_data.parallax_y = 1.0f;
	entity_data.sprite_count = 3;
	entity_data.has_sprite_count = (protobuf_c_boolean)1;


	texture_info* npc_tex = gettexture_resx(&game_state->resx_mngr, entity_data.tileset);

	AnimationList* anim_list = getanimation_resx(&game_state->resx_mngr, entity_data.animation_file);

	if (anim_list == NULL)
		logprint("Can't load animation file %s, entity %s won't be animated!", entity_data.animation_file, entity_id);



	avatar_phy.has_script = wfalse;


	entity_init(&avatar_phy, &entity_data, npc_tex, &game_state->phy_mngr, anim_list, &game_state->render_mngr, wfalse);

	canim_tail = anim_tail;
	canim_body = anim_body;
	canim_head = anim_head;


	avatar_phy.entity_id = entity_data.entity_id;

	canim_tail = anim_tail;
	canim_body = anim_body;
	canim_head = anim_head;

	entity_playanimation(&avatar_phy, canim_tail, 0);

	entity_playanimation(&avatar_phy, canim_body, 1);

	entity_playanimation(&avatar_phy, canim_head, 2);

	game_state->module_mngr.game_input = KeyboardCallback;


	game_state->gameloop_func(game_state);


	return 1;
}
