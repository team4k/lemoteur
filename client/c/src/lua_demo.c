#include <common_include.h>

#include <demo_list.h>


static int draw_lua(gamestate_t* const gamestate)
{
	

	return 0;
}

static int update_lua(gamestate_t* const gamestate,float elapsed)
{
	//if(glfwGetKey(game_state->render_mngr.oglWindow,GLFW_KEY_DOWN) == GLFW_PRESS)

	return 0;
}

int lua_demo(gamestate_t* const gamestate)
{
	gamestate->demo_draw_func = draw_lua;
	gamestate->demo_update_func = update_lua;

	script_loadfile(gamestate->lua_state,"test.lua");

	//script_execstring(gamestate->lua_state,"testvalue()");


	gamestate->gameloop_func(gamestate);

	return 0;
}
