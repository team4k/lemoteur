
#include <jni.h>
#include <android/native_window.h> // requires ndk r5 or newer
#include <android/native_window_jni.h> // requires ndk r5 or newer
#include <EGL/egl.h>

extern "C" {
    #include <common_include.h>
    #include <demo_list.h>
}



gamestate_t game_state;
char resources_path[260];
char config_path[260];
char script_path[260];
char app_path[WAFA_MAX_PATH];

const double SCREEN_WIDTH = 640.0;
const double SCREEN_HEIGHT = 480.0;

ANativeWindow* window;
pthread_t gameloop_thread;
static pthread_mutex_t touch_mutex = PTHREAD_MUTEX_INITIALIZER;

char level_start[256] = {0};


void configure()
{
	//if android build, copy the config from the apk first
	//to application file folder 

	char tmp_config_path[260];

	size_t config_size = 0;
	unsigned char* config_buffer = NULL;

	#ifdef ANDROID_BUILD
	w_sprintf(tmp_config_path, 260, "assets/config.lua");

	config_size = getrawdata_size(&game_state.resx_mngr, tmp_config_path);
	config_buffer = (unsigned char*)wf_malloc(config_size * sizeof(unsigned char));

	if (getrawdata(&game_state.resx_mngr, tmp_config_path, config_buffer, config_size) == -1)
	{
		logprint("Can't find config.lua file!");
		wf_free(config_buffer);
		return;
	}
	#else
	w_sprintf(tmp_config_path, 260, "%sconfig.lua", app_path);

	if (strcmp(config_path, tmp_config_path) != 0)
	{
		config_buffer = get_resx_content(&game_state.resx_mngr, tmp_config_path, &config_size,NULL);
	}

	#endif

	if (config_buffer != NULL)
	{
		wbool dev_mode = wfalse;

		//write a config file ONLY if no config_android.lua found or 
		//if we are in dev mode
		FILE* file = NULL;

		if (!dev_mode)
			w_fopen(file,config_path, "r");
		else
			logprint("DEV MODE: don't forget to set dev_mode variable to wfalse in common.c file before release");

		if (file == NULL)
		{
			w_fopen(file,config_path, "w");

			if (file != NULL)
			{
				fwrite(config_buffer, sizeof(unsigned char), config_size, file);
				fclose(file);
			}
			else
			{
				logprint("Failed to write config.lua file content to file system %s", config_path);
				wf_free(config_buffer);
				config_buffer = NULL;
				return;
			}
		}
		else
		{
			fclose(file);
		}

		wf_free(config_buffer);
		config_buffer = NULL;
	}


	//load lua config file, (if any)
	game_state.lua_state =  script_createstate();

	game_state.module_mngr.game_disableinput = base_disable_input;
	game_state.module_mngr.game_target = base_game_target;
	game_state.module_mngr.game_endtarget = base_game_end_target;
	game_state.module_mngr.game_setcursorpos = base_setcursor_pos;
	game_state.module_mngr.game_movetarget = base_move_target;
	game_state.module_mngr.game_offset_target = base_offset_target;
	game_state.module_mngr.game_input = base_game_input;
	game_state.module_mngr.game_char_input = base_game_char_input;
	game_state.module_mngr.game_joystick_axe = base_joystick_axe;
	game_state.module_mngr.game_joystick_button = base_joystick_button;
	game_state.module_mngr.game_changeinputmode = base_change_input_mode;

	config_init(&game_state.config,game_state.lua_state);
	config_readconf(&game_state.config,(void*)&game_state.resx_mngr,&config_path[0]);

	//config.config_saved = callback_config_saved;

	#ifndef ANDROID_BUILD
	//update resources path if needed (PC builds only)

	char c_asset_path[260];
	char c_script_path[260];
	char new_resx_path[260];
	char new_script_path[260];


	if(config_getstring(&game_state.config,"asset_path",&c_asset_path[0],260))
	{
		strcpy(new_resx_path,app_path);

		//remove last slash in path
		if(new_resx_path[strlen(new_resx_path)-1] == PATH_SEP)
			new_resx_path[strlen(new_resx_path)-1] = '\0';

		combine_rel_path(new_resx_path,&c_asset_path[0],260);

		sprintf(resources_path,"%s",new_resx_path);
	}

	if(config_getstring(&game_state.config,"script_path",&c_script_path[0],260))
	{
		strcpy(new_script_path,app_path);

		//remove last slash in path
		if(new_script_path[strlen(new_script_path)-1] == PATH_SEP)
			new_script_path[strlen(new_script_path)-1] = '\0';

		combine_rel_path(new_script_path,&c_script_path[0],260);

		sprintf(script_path,"%s",new_script_path);
	}
	#endif


	//add our script folder path to lua package path
	char package_path[500];
	char final_package_path[1000];

			#if defined(_WIN32)
				sprintf(package_path,"package.path = '%s\\?.lua;' .. package.path",&script_path[0]);

				str_double_slash(&package_path[0],final_package_path,"\\");
			#else
					sprintf(final_package_path,"package.path = '%s/?.lua;' .. package.path",&script_path[0]);
			#endif

	const char* log_return  = script_execstring(game_state.lua_state,final_package_path,NULL,0);

	if(log_return != NULL)
		logprint(log_return);
	else
		logprint("Chemin scripts lua ajouté au package.path");
}
	


int init_render(render_manager_t* const render_mngr,void* app_instance,void* app_handle)
{
	render_init(render_mngr,SCREEN_WIDTH,SCREEN_HEIGHT,&app_path[0],"Demo Wafa engine",app_instance,app_handle,wfalse,wfalse,wfalse,filter_pixelart);

	//load shader
	
	if(!render_mngr->fixed_func_pipeline)
	{
		shader_info* vert_shader = getshader_resx(&game_state.resx_mngr,"vertex_shader.shad",wfalse);
		shader_info* pix_shader = getshader_resx(&game_state.resx_mngr,"pixel_shader.shad",wtrue);
		
		shader_info* vert_notex_shader = getshader_resx(&game_state.resx_mngr,"vertex_shader_notex.shad",wfalse);
		shader_info* pixel_notex_shader = getshader_resx(&game_state.resx_mngr,"pixel_shader_notex.shad",wtrue); 

		SHADERPROGRAM_ID prog_id =  render_create_program(&game_state.render_mngr,"prog_common",vert_shader->shader_pointer,pix_shader->shader_pointer);

		SHADERPROGRAM_ID prog_no_tex_id = render_create_program(&game_state.render_mngr,"prog_notex",vert_notex_shader->shader_pointer,pixel_notex_shader->shader_pointer);

		render_create_program(&game_state.render_mngr,"prog_mesh",getshader_resx(&game_state.resx_mngr,"vs_mesh.shad",wfalse)->shader_pointer,getshader_resx(&game_state.resx_mngr,"ps_mesh.shad",wtrue)->shader_pointer);


		//render_create_program(&game_state.render_mngr,"prog_test",getshader_resx(&game_state.resx_mngr,"vs_charlighting.shad",wfalse)->shader_pointer,getshader_resx(&game_state.resx_mngr,"ps_charlighting.shad",wtrue)->shader_pointer);



		render_use_program(&game_state.render_mngr,prog_id);	
	}

    return 0;
}

int (*demo_ptr[])(gamestate_t* const game_state) = {rectangle_demo};





extern "C"
void Java_dlan_wafasamples_MainActivity_touchscreen(JNIEnv *env,jobject self,jfloat posx,jfloat posy,jint action)
{
	pthread_mutex_lock(&touch_mutex);
	
	input_mngr_touch(&game_state.input_mngr,posx,posy,action);
	
	pthread_mutex_unlock(&touch_mutex);
}

extern "C"
void Java_dlan_wafasamples_MainActivity_backbutton(JNIEnv *env,jobject self)
{
	pthread_mutex_lock(&touch_mutex);

	input_mngr_backbutton(&game_state.input_mngr);
	
	pthread_mutex_unlock(&touch_mutex);
}


static int checkload (lua_State *L, int stat, const char *filename) {
  if (stat) {  /* module loaded successfully? */
    lua_pushstring(L, filename);  /* will be 2nd argument to module */
    return 2;  /* return open function and file name */
  }
  else
    return luaL_error(L, "error loading module " LUA_QS
                         " from file " LUA_QS ":\n\t%s",
                          lua_tostring(L, 1), filename, lua_tostring(L, -1));
}

static int lua_custom_loader(lua_State* state,const char* module_key,const char* module_path)
{
	long script_size = getrawdata_size(&game_state.resx_mngr,module_path);
	unsigned char* script_buffer = (unsigned char*)malloc(script_size * sizeof(unsigned char));
	getrawdata(&game_state.resx_mngr,module_path,script_buffer,script_size);
	luaL_loadbuffer(state,(const char*)script_buffer,script_size,module_key) ;
	free(script_buffer);
	return 0;
}

static int lua_custom_searcher(lua_State* state)
{
	const char* module_key = luaL_checkstring(state, 1);
	char module_path[1000];
	
	sprintf(module_path,"assets/%s.lua",module_key);
	
	if(!has_raw_resx(&game_state.resx_mngr,module_path))
		return 1;//1 = not found in path 
	
	return checkload(state, (lua_custom_loader(state, module_key, module_path) == 0), module_key);
}


static pthread_mutex_t running_mutex = PTHREAD_MUTEX_INITIALIZER;

volatile uint32_t suspend_gameloop = 0;
volatile uint32_t destroyegl = 0;
volatile uint32_t createegl = 0;
volatile uint32_t adsinfront = 0;

void lockrunning()
{
	pthread_mutex_lock(&running_mutex);
}

void unlockrunning()
{
	pthread_mutex_unlock(&running_mutex);
}

void config_saved()
{
}



void endrenderthread()
{
	pthread_mutex_destroy(&running_mutex);
	pthread_mutex_destroy(&touch_mutex);
	pthread_exit(0);
}

void setcustompkgloader(lua_State* state)
{
	script_setcustompkgloader(state,lua_custom_searcher);
}

void handle_input()
{
	//movement using touch control
	pthread_mutex_lock(&touch_mutex);
	
	input_mngr_handle_input(&game_state.input_mngr);
			
	pthread_mutex_unlock(&touch_mutex);
}

static JavaVM* global_jvm;
static jobject java_activity;


void* gameloop_ptr(void* unused)
{
   configure();

   init_resources(&game_state.resx_mngr,&resources_path[0],&game_state.render_mngr);

   
   init_render(&game_state.render_mngr,NULL,window);

   input_mngr_init(&game_state.input_mngr,&game_state.render_mngr,&game_state.module_mngr,1.0f,wfalse,wfalse,wtrue);

   game_state.gameloop_func = gameloop;

   (*demo_ptr[0])(&game_state);

   render_stop(&game_state.render_mngr);
	
	return NULL;
}


extern "C"
void Java_dlan_wafasamples_MainActivity_initengine(JNIEnv *env,jobject self,jobject surface,jstring apkPath,jstring libpath,jstring filepath,jstring locale) {
	
	 jint rs = env->GetJavaVM(&global_jvm);
	 
	 if(rs != JNI_OK)
	 {
		logprint("Can't get java VM!");
		return;
	}
	
	java_activity = env->NewGlobalRef(self);

	logprint("get window reference");
	//get the surface reference from java
	window = ANativeWindow_fromSurface(env, surface);
	
	const char* str = env->GetStringUTFChars(apkPath, 0);
	 
	sprintf(app_path,"%s",&str[0]);
	sprintf(resources_path,"%s",&app_path[0]);
	
	
	env->ReleaseStringUTFChars(apkPath, str);
	
	const char* str3 = env->GetStringUTFChars(filepath,0);
	
	 sprintf(config_path,"%sconfig.lua",&str3[0]);
	
    env->ReleaseStringUTFChars(filepath, str3);
		
	if(window == NULL)
	{
		logprint("Can't get native window from surface!");
		return;
	}

	
	logprint("start thread");
	//start thread for gameloop
	 pthread_create(&gameloop_thread,NULL,gameloop_ptr,NULL);
}


extern "C"
void JNICALL Java_dlan_wafasamples_MainActivity_stopengine(JNIEnv *env,jobject self)
{
	pthread_mutex_lock(&running_mutex);
	eglMakeCurrent (game_state.render_mngr.oglDisplay, EGL_NO_SURFACE, EGL_NO_SURFACE,EGL_NO_CONTEXT);
	//snd_mngr_setvolume(get_soundmngr(),0,0);
	pthread_mutex_unlock(&running_mutex);
	
	env->DeleteGlobalRef(java_activity);
	
	logprint("stop engine");
	
	atomic_set_value(&suspend_gameloop,0);
	
}

extern "C"
void JNICALL Java_dlan_wafasamples_MainActivity_createegl(JNIEnv* env,jobject self,jobject surface)
{
	if(window != NULL)
	{
		logprint("native window already exist! call destroyegl before createegl");
		return;
	}
	
	window = ANativeWindow_fromSurface(env, surface);
	atomic_set_value(&createegl,1);
}

extern "C"
jint Java_dlan_wafasamples_MainActivity_checkdestroyegl(JNIEnv* env,jobject self)
{
	volatile uint32_t local_destroystate = 0;
	atomic_set_value(&local_destroystate,destroyegl);
	

	return local_destroystate;
}

extern "C"
void Java_dlan_wafasamples_MainActivity_destroyegl(JNIEnv* env,jobject self)
{
	atomic_set_value(&destroyegl,1);

}

extern "C"
void Java_dlan_wafasamples_MainActivity_pauseengine(JNIEnv *env,jobject self)
{
	/*atomic_set_value(&suspend_gameloop,1);
	
	if(get_soundmngr()->stream_opened)
		snd_mngr_setvolume(get_soundmngr(),0,0);
		
	logprint("pause engine");*/
}


extern "C"
void Java_dlan_wafasamples_MainActivity_resumeengine(JNIEnv *env,jobject self)
{
	/*if(get_soundmngr()->stream_opened)
	{
		int32_t music_vol = 0;
		int32_t sfx_vol = 0;
		config_getint(get_config(),"music_volume",&music_vol);
		config_getint(get_config(),"sfx_volume",&sfx_vol);
		snd_mngr_setvolume(get_soundmngr(),music_vol,sfx_vol);
	}
	
	atomic_set_value(&suspend_gameloop,0);
	atomic_set_value(&adsinfront,0);

	logprint("resume engine");*/
}

extern "C"
void Java_dlan_wafasamples_MainActivity_setaudiobuffer(JNIEnv *env,jobject self,jint audio_buffersize)
{
	//set_audio_buffersize(audio_buffersize);
}