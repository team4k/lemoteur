#include <common_include.h>

#include <demo_list.h>

static save_mngr_t save_mngr;
const char* mdp_base = "AdKqeYSuqmJskaPAESHI";
const char* mdp_persist = "new_mdp_persist";
const char* save_path = "/persistent_data/save.db";
static wbool ready_savedata = wfalse;
static wbool updated_data = wfalse;

static int savemngr_draw_func(gamestate_t* const game_state)
{

	return 1;
}

static int savemngr_update_func(gamestate_t* const game_state,float elapsed)
{
	#ifdef EMSCRIPTEN
	//check if save.db file exist
	if(emscripten_run_script_int("Module.syncdone") == 1 && !ready_savedata)
	{
		FILE* file = fopen(save_path,"r");
       
		if(file == NULL)
		{
			printf("save.db file doesn't exist in file system, copying it...\n");
			size_t size;
			//copy file
			unsigned char* buffer = get_resx_content(&game_state->resx_mngr,"/base_data/save.db",&size,NULL);

			file = fopen(save_path,"w");

			fwrite(buffer,sizeof(unsigned char),size,file);
			fclose(file);

			//persist emscripten current data to persistent source data
			EM_ASM(
				Module.print("Start File sync..");
				Module.syncdone = 0;
				FS.syncfs(false, function(err) {
					assert(!err);
					Module.print("End File sync..");
					Module.syncdone = 1;
				});
			);
		}
		else
		{
			fclose(file);
		}

		save_mngr_init(&save_mngr,save_path);

		printf("Start check data \n");
		if(!save_mngr_checkdata(&save_mngr,"avatar","guid","2C54959F-89D6-158B-B022-0CF3308379B0"))
		{
			printf("Data doesn't exist, insert new line\n");
			save_mngr_new_data(&save_mngr,"avatar",3,"string","2C54959F-89D6-158B-B022-0CF3308379B0","string",mdp_base,"int",0);
		}
		else
		{
			printf("Password before change \n");
			char* ret_val = save_mngr_getstring(&save_mngr,"avatar","guid","2C54959F-89D6-158B-B022-0CF3308379B0","mdp");

			printf("%s \n",ret_val);
			wf_free(ret_val);
			const char* tmp;

			if(strcmp(mdp_base,ret_val) == 0)
				tmp = mdp_persist;
			else
				tmp = mdp_base;

			save_mngr_update_data(&save_mngr,"avatar","guid","2C54959F-89D6-158B-B022-0CF3308379B0",1,"mdp","string",tmp);

			printf("Password after change \n");
			printf("%s \n",tmp);
		}

		printf("End check data \n");
		ready_savedata = wtrue;
	}


	#endif
		

	return 1;
}

int save_mngr_demo(gamestate_t* const game_state)
{
	game_state->demo_draw_func = savemngr_draw_func;
	game_state->demo_update_func = savemngr_update_func;


	#ifdef EMSCRIPTEN
	//mount read/write filesystem 
		EM_ASM(
			FS.mkdir('/persistent_data');
			FS.mount(IDBFS,{},'/persistent_data');//mount persistent directory as IDBFS
			Module.syncdone = 0;
			Module.print("start file sync..");
			//populate emscripten file system with existing persistent source data
			FS.syncfs(true, function(err) {
				assert(!err);
				Module.print("end file sync..");
				Module.syncdone = 1;
			});
		);

	#endif


	game_state->gameloop_func(game_state);
  
	return 1;

}
