#include <common_include.h>
#include <demo_list.h>


static Rect_t test_rectangle = {300,300,{200,480}};
static Rect_t test_rectangle2 = {64,64,{250,250}};
static ColorA_t color = {1.0f,0.0f,0.0f,1.0f};

static int draw_func(gamestate_t* const game_state)
{
	float center[] = {test_rectangle.position.x + (test_rectangle.width * 0.5f),test_rectangle.position.y - (test_rectangle.height * 0.5f)};
	render_setuniform_float2(&game_state->render_mngr,"inCenter",center);

	render_setuniform_float(&game_state->render_mngr,"inCircleStep",0.3f);
	render_setuniform_float(&game_state->render_mngr,"inGlobalTime",(float)(render_gettime() / 1000.0));

	drawrect(&test_rectangle,color,&game_state->render_mngr);

	return 1;
}

static int update_func(gamestate_t* const gamestate,float elapsed)
{
	float mov_value = 0.5f * elapsed;

	if(input_mngr_getkey(&gamestate->input_mngr,KEY_DOWN) == KEY_PRESS)
			test_rectangle.position.y -= mov_value;
        
	if(input_mngr_getkey(&gamestate->input_mngr,KEY_UP) == KEY_PRESS)
		test_rectangle.position.y += mov_value;

	if(input_mngr_getkey(&gamestate->input_mngr,KEY_LEFT) == KEY_PRESS)
		test_rectangle.position.x  -= mov_value;

	if(input_mngr_getkey(&gamestate->input_mngr,KEY_RIGHT) == KEY_PRESS)
		test_rectangle.position.x += mov_value;


	return 1;
}


int fireball_demo(gamestate_t* const game_state)
{
	game_state->demo_draw_func = draw_func;
	game_state->demo_update_func = update_func;

	shader_info* vert_effect_shader = getshader_resx(&game_state->resx_mngr,"vertex_shader_effect.shad",wfalse);
	shader_info* pixel_effect_shader = getshader_resx(&game_state->resx_mngr,"pixel_shader_explosion.shad",wtrue); 

	SHADERPROGRAM_ID prog_id =  render_create_program(&game_state->render_mngr,"prog_effect",vert_effect_shader->shader_pointer,pixel_effect_shader->shader_pointer);

	render_use_program(&game_state->render_mngr,prog_id);


	float resolution[] = {game_state->render_mngr.screen_info.width,game_state->render_mngr.screen_info.height};
	render_setuniform_float2(&game_state->render_mngr,"inResolution",resolution);


	game_state->gameloop_func(game_state);

	render_delete_program(&game_state->render_mngr,"prog_effect");

	return 1;
}