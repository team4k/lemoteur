#include <common_include.h>

#include <demo_list.h>

#include <Debug/ChipmunkDebugDraw.h>

static sprite_t avatar;
static Vector_t avatar_position;
static render_manager_t* render_manager;
static level_stream_t level_stream;
static light_manager_t light_mngr;
static const int32_t map_size = 512;
static const int32_t tile_size = 32;

static SHADERPROGRAM_ID prog_notex = NULL;
static SHADERPROGRAM_ID prog_common = NULL;
static SHADERPROGRAM_ID prog_colored_notex = NULL;

static localization_t local_obj;

static void KeyboardCallback(int16_t input_event,wbool pressed,wbool from_keyboard)
{
    if(input_event == KEY_SPACE && !pressed)
    {
        int index = -1;

        for(int li = 0;li < level_stream.access_chunk[O_CENTER]->map.layer_hash.max_elem;li++)
        {
                if(level_stream.access_chunk[O_CENTER]->map.layer_hash.keys[li])
                {
                    index = li;
                    break;
                }
         }

        if(index != -1)
        {
            //switch all tiles of the current chunk
            for(int it = 0;it <  level_stream.access_chunk[O_CENTER]->map.col_count *  level_stream.access_chunk[O_CENTER]->map.row_count;it++)
            {
                 TilemapLayer_editTile(&level_stream.access_chunk[O_CENTER]->map.layer_array[index],2,it,render_manager,wtrue);

            }
        }
       
    }
}

static float* editor_collisions_colors = NULL;
static GLuint buffer_collisions_colors;
static GLuint VBOvertices;
static float* vertices = NULL;


static void
DrawCircle(cpVect p, cpFloat a, cpFloat r, cpSpaceDebugColor outline, cpSpaceDebugColor fill, cpDataPointer data)
{
	ChipmunkDebugDrawCircle(p, a, r, outline, fill);
}

static void
DrawSegment(cpVect a, cpVect b, cpSpaceDebugColor color, cpDataPointer data)
{
	ChipmunkDebugDrawSegment(a, b, color);
}

static void
DrawFatSegment(cpVect a, cpVect b, cpFloat r, cpSpaceDebugColor outline, cpSpaceDebugColor fill, cpDataPointer data)
{
	ChipmunkDebugDrawFatSegment(a, b, r, outline, fill);
}

static void
DrawPolygon(int count, const cpVect *verts, cpFloat r, cpSpaceDebugColor outline, cpSpaceDebugColor fill, cpDataPointer data)
{
	ChipmunkDebugDrawPolygon(count, verts, r, outline, fill);
}

static void
DrawDot(cpFloat size, cpVect pos, cpSpaceDebugColor color, cpDataPointer data)
{
	ChipmunkDebugDrawDot(size, pos, color);
}

static int demo_draw(gamestate_t* const game_state)
{

	render_manager->zoom_factor = 4.0f;

	ColorA_t back_col = { 1.0f,0.0f,0.80,1.0f };
	render_clear_screenwithcolor(render_manager, back_col);

	render_use_program(render_manager, prog_common);
    //draw elements       
	//level_stream_draw(&level_stream,render_manager,&light_mngr,&game_state->resx_mngr);

    //avatar_update(&avatar,elapsed);
	sprite_draw(&avatar,avatar_position.x,avatar_position.y,&game_state->render_mngr,NULL);


	

	render_unbind_texture(render_manager, wfalse);
	render_use_program(render_manager, prog_colored_notex);

	int32_t tottile = (level_stream.base_row_count * CHUNK_ROW)  * (level_stream.base_col_count * CHUNK_COL);

	size_t colors_sizes = tottile * 24;

	if (game_state->phy_mngr.collisions_array  != NULL && vertices != NULL)
	{
		ColorA_t empty_color = { 1.0,1.0,1.0,0.0 };

	
		if(editor_collisions_colors == NULL)
			editor_collisions_colors = (float*)wf_malloc(colors_sizes * sizeof(float));

		int32_t vert_i = 0;
		int32_t colors_i = 0;

		for (int io = 0; io < tottile; io++) {

			//ColorA_t* selected_color = Tilemap_editor_getcollisioncolor(tilemap, tilemap->collisions_array[pos_collide]);

			ColorA_t selected_color = { 0.0,0.0,0.0,1.0 };

			if (game_state->phy_mngr.collisions_array[io] == NO_COLLISION) {
				selected_color = empty_color;
			}


			for (int32_t vert = 0; vert < 6; vert++)
			{
				editor_collisions_colors[colors_i++] = selected_color.r;
				editor_collisions_colors[colors_i++] = selected_color.g;
				editor_collisions_colors[colors_i++] = selected_color.b;
				editor_collisions_colors[colors_i++] = selected_color.a;
			}

		}


		buffer_collisions_colors = render_gen_buffer(render_manager, editor_collisions_colors, colors_sizes * sizeof(float), wfalse, wfalse);
	}

	if (editor_collisions_colors != NULL)
	{
		render_push_matrix(render_manager);

		Vector_t cpos = render_manager->scroll_vector;
		
		Vector_t offset = level_stream.access_chunk[O_BOTTOMLEFT]->bounds.position;

		int32_t scroll_x = (int32_t)(((cpos.x * 1.0f) - cpos.x) + offset.x);
		int32_t scroll_y = (int32_t)(((cpos.y * 1.0f) - cpos.y) + offset.y);

		cpos.x = (wfloat)scroll_x;
		cpos.y = (wfloat)scroll_y;

		render_set_position(render_manager, cpos);

		render_start_draw_buffer(render_manager, 0, VBOvertices, 0, buffer_collisions_colors, 2);

		render_draw_buffer(render_manager, 0, colors_sizes * 6);

		render_end_draw_buffer(render_manager, wfalse, wtrue);

		render_pop_matrix(render_manager);
	}
	


	render_use_program(render_manager, prog_notex);

	ChipmunkDebugDrawImpl(&game_state->render_mngr, game_state->phy_mngr.world);

	return 0;

}

static int demo_update(gamestate_t* const game_state, float elapsed)
{
	ChipmunkDebugDrawClearRenderer();

	float mov_value = 0.2f * elapsed;

	if(input_mngr_getkey(&game_state->input_mngr,KEY_DOWN) == KEY_PRESS)
            avatar_position.y -= mov_value;
        
    if(input_mngr_getkey(&game_state->input_mngr,KEY_UP) == KEY_PRESS)
        avatar_position.y += mov_value;

    if(input_mngr_getkey(&game_state->input_mngr,KEY_LEFT) == KEY_PRESS)
        avatar_position.x -= mov_value;

    if(input_mngr_getkey(&game_state->input_mngr,KEY_RIGHT) == KEY_PRESS)
       avatar_position.x += mov_value;


	render_manager->scroll_vector.x =   ((render_manager->screen_info.width / 2) -  ((float)avatar_position.x / render_manager->zoom_factor));

	render_manager->scroll_vector.y =  ((render_manager->screen_info.height / 2) -  ((float)avatar_position.y / render_manager->zoom_factor));

    level_stream_update(&level_stream,avatar_position,elapsed,render_manager, game_state->lua_state,wfalse);

	update_physics(&game_state->phy_mngr, 0.01f);

	return 0;
}


int level_stream_demo(gamestate_t* const game_state)
{
	game_state->demo_draw_func = demo_draw;
	game_state->demo_update_func = demo_update;

	render_manager = &game_state->render_mngr;

    prog_notex = render_get_program(&game_state->render_mngr,"prog_notex");
	prog_common = render_get_program(&game_state->render_mngr,"prog_common");
	prog_colored_notex = render_get_program(&game_state->render_mngr, "prog_colored_no_texture");

	physics_init2(&game_state->phy_mngr, wfalse,wtrue);

    ColorA_t color = {0.0f,0.0f,0.0f,0.7f};
    

    light_manager_init(&light_mngr,color,&game_state->render_mngr,prog_notex,prog_notex,prog_common,light_topdown,512,512,10,10);

	texture_info* player_texture = gettexture_resx(&game_state->resx_mngr,"hero.png");


    //check player texture
    if(!player_texture)
    {
        logprint("Erreur chargement Texture joueur ! %s ","hero.png");
        return 0;
    }

    texture_info* level_texture = gettexture_resx(&game_state->resx_mngr,"underground_tileset.png");

   /* shader_info* vert_ground_shader = getshader_resx(&game_state->resx_mngr,"vs_groundtmap_v2.shad",wfalse);
	shader_info* pixel_ground_shader = getshader_resx(&game_state->resx_mngr,"ps_groundtmap_v2.shad",wtrue); 

	SHADERPROGRAM_ID prog_ground =  render_create_program(&game_state->render_mngr,"prog_ground",vert_ground_shader->shader_pointer,pixel_ground_shader->shader_pointer);*/

    char db_path[WAFA_MAX_PATH] = { 0 };
	char app_path[WAFA_MAX_PATH] = { 0 };
	get_app_path(&app_path[0]);

	sprintf(db_path,"%slocale.db",app_path);

	localization_init(&local_obj,db_path);

	localization_setlocale(&local_obj,"fr");

    level_stream_init(&level_stream,map_size,map_size,72,4,4,tile_size,level_texture,"underground_tileset.png",render_manager,"prog_common",&game_state->resx_mngr, &game_state->module_mngr,NULL,&local_obj,game_state->lua_state,&game_state->phy_mngr,NULL,NULL,NULL,NULL,NULL,NULL);




    char chunkdb_path[2048];

    sprintf(chunkdb_path,"%steststream.db",app_path);

	char chunkdb_path2[2048];

	sprintf(chunkdb_path2, "%steststreamtmp.db", app_path);

    level_stream_initchunksdb(&level_stream,chunkdb_path,chunkdb_path2);

    level_stream_start_backgroundthread(&level_stream);

	size_t tileArraySize = (level_stream.base_row_count * CHUNK_ROW)  * (level_stream.base_col_count * CHUNK_COL);
	size_t verticesArraySize = tileArraySize * 12;

	int32_t idTile, idvrt, tileRow, tileCol;

	vertices = (float*)wf_malloc(sizeof(float) * tileArraySize * 12);


	for (idTile = 0, idvrt = 0; idTile < tileArraySize; idTile++, idvrt += 12)
	{

		tileRow = (int32_t)floorf((float)(idTile / (level_stream.base_col_count * CHUNK_COL)));//ligne Y
		tileCol = idTile - ((level_stream.base_col_count * CHUNK_COL) * tileRow);//ligne X

																				 //compute vertices positions

		vertices[idvrt] = (float)(tileCol * level_stream.tile_size);
		vertices[idvrt + 1] = (float)(tileRow * level_stream.tile_size);

		vertices[idvrt + 2] = (float)level_stream.tile_size + (tileCol * level_stream.tile_size);
		vertices[idvrt + 3] = (float)(tileRow * level_stream.tile_size);

		vertices[idvrt + 4] = (float)level_stream.tile_size + (tileCol * level_stream.tile_size);
		vertices[idvrt + 5] = (float)level_stream.tile_size + (tileRow * level_stream.tile_size);

		vertices[idvrt + 6] = (float)level_stream.tile_size + (tileCol * level_stream.tile_size);
		vertices[idvrt + 7] = (float)level_stream.tile_size + (tileRow * level_stream.tile_size);

		vertices[idvrt + 8] = (float)(tileCol * level_stream.tile_size);
		vertices[idvrt + 9] = (float)level_stream.tile_size + (tileRow * level_stream.tile_size);

		vertices[idvrt + 10] = (float)(tileCol * level_stream.tile_size);
		vertices[idvrt + 11] = (float)(tileRow * level_stream.tile_size);

	}

	VBOvertices = render_gen_buffer(&game_state->render_mngr, vertices, verticesArraySize * sizeof(float), wfalse, wfalse);
    

    
    //-3- create avatar gameobject
	sprite_init(&avatar,32,32,32,32,player_texture,NULL,0,0,&game_state->render_mngr,wfalse,0,1.0f,1.0f,wtrue);


    game_state->module_mngr.game_input = KeyboardCallback;

	ChipmunkDebugDrawInit(&game_state->render_mngr);

	game_state->gameloop_func(game_state);


    return 0;

}
