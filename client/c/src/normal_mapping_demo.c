#include <common_include.h>
#include <demo_list.h>


static sprite_t sprite_o;
static texture_info* base_texture;
static texture_info* normal_texture;

static Vector_t light_pos = {250.0f,250.0f};

static void normal_draw(render_manager_t* const render_mngr)
{
	//render_bindtexture_tosampler(render_mngr,base_texture->texture_pointer,"u_texture",0);
	render_bindtexture_tosampler(render_mngr,normal_texture->texture_pointer,"u_normals",2,wfalse);

}

static int draw_func(gamestate_t* const game_state)
{
	render_bindtexture_tosampler(&game_state->render_mngr,normal_texture->texture_pointer,"u_normals",2,wfalse);
	
	sprite_draw(&sprite_o,200,200,&game_state->render_mngr,NULL);
	render_unbindtexture_tosampler(&game_state->render_mngr,normal_texture->texture_pointer,"u_normals",2,wfalse);
	//render_unbindtexture_tosampler(&game_state->render_mngr,normal_texture->texture_pointer,"u_texture",0);

	return 1;
}

static int update_func(gamestate_t* const gamestate,float elapsed)
{
	float mov_value =  0.08f * elapsed;

	if(input_mngr_getkey(&gamestate->input_mngr,KEY_DOWN) == KEY_PRESS)
		light_pos.y -= mov_value;
        
	if(input_mngr_getkey(&gamestate->input_mngr,KEY_UP) == KEY_PRESS)
		light_pos.y += mov_value;

	if(input_mngr_getkey(&gamestate->input_mngr,KEY_LEFT) == KEY_PRESS)
		light_pos.x  -= mov_value;

	if(input_mngr_getkey(&gamestate->input_mngr,KEY_RIGHT) == KEY_PRESS)
		light_pos.x += mov_value;

	float light[] = {light_pos.x / 640.0f,light_pos.y / 480.0f,0.075f}; //normalized light position

	render_setuniform_float3(&gamestate->render_mngr,"LightPos",light);

	return 1;
}



int normal_map_demo(gamestate_t* const game_state)
{
	game_state->demo_draw_func = draw_func;
	game_state->demo_update_func = update_func;

	shader_info* vert_normal_shader = getshader_resx(&game_state->resx_mngr,"vs_normalmap.shad",wfalse);
	shader_info* pixel_normal_shader = getshader_resx(&game_state->resx_mngr,"ps_normalmap.shad",wtrue); 

	SHADERPROGRAM_ID prog_id =  render_create_program(&game_state->render_mngr,"prog_normal",vert_normal_shader->shader_pointer,pixel_normal_shader->shader_pointer);

	render_use_program(&game_state->render_mngr,prog_id);

	float light[] = {light_pos.x / 640.0f,light_pos.y / 480.0f,0.075f}; //normalized light position

	render_setuniform_float3(&game_state->render_mngr,"LightPos",light);

	float ambient[] = {0.1f,0.1f,0.1f,1.0f};

	render_setuniform_float4(&game_state->render_mngr,"AmbientColor",ambient);

	float resolution[] = {640.0f,480.0f};

	render_setuniform_float2(&game_state->render_mngr,"Resolution",resolution);

	float light_color[] = {0.0f,0.0f,1.0f,50.0f};

	render_setuniform_float4(&game_state->render_mngr,"LightColor",light_color);

	float attenuation[] = {0.4f,3.0f,20.0f};//constant, linear, quadratic

	render_setuniform_float3(&game_state->render_mngr,"Falloff",attenuation);
	

	base_texture = gettexture_resx(&game_state->resx_mngr,"casque_malef.png");
	normal_texture = gettexture_resx(&game_state->resx_mngr,"casque_malef_NORMALS.png");

	sprite_init(&sprite_o,200,258,200,258,base_texture,NULL,0,0,&game_state->render_mngr,wfalse,0,1.0f,1.0f,wtrue);

	game_state->gameloop_func(game_state);

	return 1;
}