#include <common_include.h>
#include <demo_list.h>


static sprite_t sprite_curve;
static SHADERPROGRAM_ID prog_tex = 0;
static SHADERPROGRAM_ID prog_notex = 0;

float rotations_sprite[16] = { 0.0f };
int irot = 14;

#define ANIM_DELAY 20.0f

float delay = ANIM_DELAY;

float posx = 200.0f;

float maxangle = 0.0f;

#define ANGLE_STEP 5.625f
#define MAX_ANGLE 90.0f

wbool doanim = wfalse;

float posheady = 0.0f;
int tilesize = 64;

float possplinex = 256.0f;
float posspliney = 102.0f;

float wallx = 388.0f;
float ceily = 238.0f;
float floory = 102.0f;

int ianimpoint = 0;


static void KeyboardCallback(int16_t input_event, wbool pressed, wbool from_keyboard)
{
	//augmentation progressive de l'angle de rotation
	/*if (input_event == KEY_RIGHT) {
		posheady += 2.5f;
		//1 - calculer l'angle en prenant la taille d'une tile comme r�f�rence pour la rotation � 90 degr�e
		//exemple : tile = 64 alors si �loignement de 64 en Y, rotation � 90 du premier segment
		//on enl�ve le pas (5.625) pour chaque segment suivant
		//si �loignement de 128 en Y alors rotation � (128/64 = 2, 90* 2 = 180, mais comme la limite est � 90, on enl�ve le pas de 180, mais on limite la rotation � 90)
		maxangle = (posheady / tilesize) * MAX_ANGLE;
	}*/

	if (input_event == KEY_RIGHT) {

		if (possplinex <= wallx && posspliney < ceily)
		{
			possplinex += 2.5f;
		}
		else
		{
			posspliney += 2.5f;
		}
	}


	if (input_event == KEY_SPACE && !pressed)
		doanim = wtrue;

}

static int draw_test(gamestate_t* const game_state)
{
	render_use_program(&game_state->render_mngr, prog_notex);

	cpVect p1_pos;
	p1_pos.x = 100;
	p1_pos.y = 100;

	cpVect p2_pos;
	p2_pos.x = 200;
	p2_pos.y = 100;

	float min_distance = 80;

	float link_delta = 20;

	float alpha = 1.0;

	cpFloat len = cpvdist(p2_pos, p1_pos);


	cpFloat color_len = len - min_distance;

	float link_angle = atan2f(p2_pos.y - p1_pos.y, p2_pos.x - p1_pos.x);

	float vertices[8192];
	int32_t elements_num = 0;
	//float freq = 10;
	float amp = 10;

	float const_freq = 0.055f;//usually freq / len

	float m_pi_2 = WM_2_PI;

	for (float i = link_delta; i <= len + link_delta; i += 1)
	{
		if (elements_num >= 8192)
			break;

		vertices[elements_num] = i - link_delta;
		vertices[elements_num + 1] = sinf(i * m_pi_2 * const_freq) * amp;
		vertices[elements_num + 2] = (i + 1) - link_delta;
		vertices[elements_num + 3] = sinf((i + 1) * m_pi_2 * const_freq) * amp;

		elements_num += 4;
	}


	Vector_t pos = { p1_pos.x,p1_pos.y };

	render_push_matrix(&game_state->render_mngr);

	render_set_position(&game_state->render_mngr, pos);


	render_set_rotation(&game_state->render_mngr, rad2deg(link_angle));
	render_start_draw_array(&game_state->render_mngr, 2, elements_num * 0.5f, vertices, NULL);
	ColorA_t link_color = { (float)color_len * 0.5f,1.0f - ((float)color_len *0.5f),0.0f,alpha };
	render_set_color(&game_state->render_mngr, link_color);
	render_draw_line(&game_state->render_mngr, 0, elements_num * 0.5f);
	render_end_draw_array(&game_state->render_mngr, wfalse);


	link_color.r = link_color.b = link_color.g = link_color.a = 1.0f;
	render_set_color(&game_state->render_mngr, link_color);

	render_pop_matrix(&game_state->render_mngr);

	return 1;
}

static float b0(float u)
{
	//return powf((1 - u),3);
	return ((1 - u) * (1 - u) * (1 - u)) / 6;
}

static float b1(float u)
{
	return ((3 * (u * u * u)) - (6 * (u * u)) + 4) / 6;
}


static float b2(float u)
{
	return ((-3 * (u * u * u)) + (3 * (u * u)) + (3 * u) + 1) / 6;
}


static float b3(float u)
{
	return (u * u * u) / 6;
}


//le code de la d�riv� est � rev�rifier, soit le calcul n'est pas bon, soit le calcul de v�locit� n'est pas bon, utilisation a atan entre deux point pour calculer la direction � la place
static float b0prime(float u)
{
	return (-3 * (u  * u))  * (1.0f / 6);
}

static float b1prime(float u)
{
	return ((9 * (u * u)) - (12 * u)) * (1.0f / 6);
}


static float b2prime(float u)
{
	return ((-9 * (u * u)) + (6 * u) + 3) * (1.0f / 6);
}


static float b3prime(float u)
{
	return (3 * (u * u)) * (1.0f / 6);
}

//https://www2.cs.uregina.ca/~anima/UniformBSpline.pdf
//https://www.methodemaths.fr/derivee/

int cvel = 0;

static int draw_uni_b_spline(gamestate_t* const game_state)
{
	int max_steps = 10; // nombre de noeuds
	int nctrlpoints = 6; //m

	//courbe en S
	Vector_t ctrlpoints[6];
	ctrlpoints[0].x = 100;
	ctrlpoints[0].y = 100;

	ctrlpoints[1].x = 300;
	ctrlpoints[1].y = 100;

	ctrlpoints[2].x = 320;
	ctrlpoints[2].y = 170;

	ctrlpoints[3].x = 320;
	ctrlpoints[3].y = 330;

	ctrlpoints[4].x = 340;
	ctrlpoints[4].y = 350;

	ctrlpoints[5].x = 600;
	ctrlpoints[5].y = 400;

	//courbe avec 4 points de controles
	/*int nctrlpoints = 4; //m
	Vector_t ctrlpoints[4];
	ctrlpoints[0].x = 100;
	ctrlpoints[0].y = 100;

	ctrlpoints[1].x = 300;
	ctrlpoints[1].y = 100;

	ctrlpoints[2].x = 300;
	ctrlpoints[2].y = 400;

	ctrlpoints[3].x = 600;
	ctrlpoints[3].y = 400;*/

	float vertices[8192];
	int32_t elements_num = 0;
	int extra = 0;

	Vector_t velocities[8192];
	int32_t vector_nums = 0;

	vertices[elements_num] = 100;
	vertices[elements_num + 1] = 100;
	elements_num += 2;

	float rotations[8192];
	int32_t rot_nums = 0;

	for (int i = 0; i < nctrlpoints - 3; i++)
	{
		for (int j = 0; j < max_steps; j++)
		{
			double u = (double)j / (double)max_steps;
			Vector_t pointcourbe;

			pointcourbe.x = b0(u) * ctrlpoints[i].x + b1(u) * ctrlpoints[i+1].x + b2(u) * ctrlpoints[i+2].x + b3(u) * ctrlpoints[i+3].x;
			pointcourbe.y = b0(u) * ctrlpoints[i].y + b1(u) * ctrlpoints[i + 1].y + b2(u) * ctrlpoints[i + 2].y + b3(u) * ctrlpoints[i + 3].y;

			if (elements_num > 0) {
				float opp = pointcourbe.y - vertices[elements_num - 1];
				float adj = pointcourbe.x - vertices[elements_num - 2];

				float radangle = atan2f(opp, adj);
				rotations[rot_nums++] = radangle * (180.0f / WM_PI);
			}
			else {
				rotations[rot_nums++] = 0.0f;
			}
			


			vertices[elements_num] = pointcourbe.x;
			vertices[elements_num + 1] = pointcourbe.y;
			elements_num += 2;
		}
	}

	vertices[elements_num] = 600;
	vertices[elements_num + 1] = 400;
	elements_num += 2;

	render_use_program(&game_state->render_mngr, prog_notex);

	render_push_matrix(&game_state->render_mngr);

	Vector_t pos = { 0, 0};

	render_set_position(&game_state->render_mngr, pos);

	//rendu de la courbe
	render_start_draw_array(&game_state->render_mngr, 2, elements_num * 0.5f, vertices, NULL);
	ColorA_t link_color = { (float)1.0f,0.0f,0.0f,1.0f };
	render_set_color(&game_state->render_mngr, link_color);
	render_draw_line(&game_state->render_mngr, 0, elements_num * 0.5f);
	render_end_draw_array(&game_state->render_mngr, wfalse);

	link_color.r = link_color.b = link_color.g = link_color.a = 1.0f;

	render_set_color(&game_state->render_mngr, link_color);

	render_pop_matrix(&game_state->render_mngr);

	render_push_matrix(&game_state->render_mngr);

	if (cvel >= rot_nums) {
		cvel = 0;
	}

	//rendu du vecteur de direction
	float vel_vert[4];

	Vector_t pointvel = Vec(vertices[(cvel * 2)], vertices[(cvel * 2) + 1]);

	vel_vert[0] = pointvel.x;
	vel_vert[1] = pointvel.y;

	Vector_t direction = Vec_rotate(Vec(100, 0), rotations[cvel]);


	Vector_t endvel = Vec_plus(pointvel, direction);
	//Vec_rotate()
	//Vector_t endvel = Vec_plus(pointvel, velvec);

	vel_vert[2] = endvel.x;
	vel_vert[3] = endvel.y;

	link_color.r = 0.0f; 
	link_color.g = 1.0f;

	render_start_draw_array(&game_state->render_mngr, 2, 2, vel_vert, NULL);

	render_set_color(&game_state->render_mngr, link_color);

	render_draw_line(&game_state->render_mngr, 0, 2);
	render_end_draw_array(&game_state->render_mngr, wfalse);

	render_pop_matrix(&game_state->render_mngr);

	//rendu du vecteur de normal
	float normal_vert[4];

	Vector_t normalvec = Vec_rotate(direction, 90.0f);

	normal_vert[0] = pointvel.x;
	normal_vert[1] = pointvel.y;

	Vector_t endnormal = Vec_plus(pointvel, normalvec);

	normal_vert[2] = endnormal.x;
	normal_vert[3] = endnormal.y;

	link_color.r = 1.0f;
	link_color.g = 1.0f;

	render_start_draw_array(&game_state->render_mngr, 2, 2, normal_vert, NULL);

	render_set_color(&game_state->render_mngr, link_color);

	render_draw_line(&game_state->render_mngr, 0, 2);
	render_end_draw_array(&game_state->render_mngr, wfalse);

	render_pop_matrix(&game_state->render_mngr);


	return 1;
}

static int draw_sprite_spline(gamestate_t* const game_state)
{
	int max_steps = 16; // nombre de noeuds
	int nctrlpoints = 6; //m

	//ligne plate au d�but
	Vector_t ctrlpoints[6];

	float width = 256;
	float step = 32;

	ctrlpoints[5].x = possplinex;//100 - 64 / x de d�part - 64
	ctrlpoints[5].y = posspliney;

	if (posspliney > floory + step) {
		ctrlpoints[4].x = ctrlpoints[5].x;
		ctrlpoints[4].y = floory + step;

		ctrlpoints[3].x = ctrlpoints[4].x;
		ctrlpoints[3].y = floory + step;
	}
	else {
		ctrlpoints[4].x = ctrlpoints[5].x - step;
		ctrlpoints[4].y = posspliney;

		ctrlpoints[3].x = ctrlpoints[4].x - step;
		ctrlpoints[3].y = posspliney;
	}
	

	if (posspliney > floory + step) {
		ctrlpoints[2].x = ctrlpoints[3].x - step;
		ctrlpoints[2].y = floory;

		ctrlpoints[1].x = ctrlpoints[2].x;
		ctrlpoints[1].y = floory;
	}
	else {
		ctrlpoints[2].x = ctrlpoints[3].x - step;
		ctrlpoints[2].y = posspliney;

		ctrlpoints[1].x = ctrlpoints[2].x - step;
		ctrlpoints[1].y = posspliney;
	}
	
	if (posspliney > floory + step) {
		ctrlpoints[0].x = ctrlpoints[5].x - (step * 6);
		ctrlpoints[0].y = floory;
	}
	else {
		ctrlpoints[0].x = ctrlpoints[1].x - step;
		ctrlpoints[0].y = posspliney;
	}
	

	float vertices[8192];
	int32_t elements_num = 0;
	int extra = 0;


	float rotations[8192];
	int32_t rot_nums = 0;

	for (int i = 0; i < nctrlpoints - 3; i++)
	{
		for (int j = 0; j < max_steps; j++)
		{
			double u = (double)j / (double)max_steps;
			Vector_t pointcourbe;

			pointcourbe.x = b0(u) * ctrlpoints[i].x + b1(u) * ctrlpoints[i + 1].x + b2(u) * ctrlpoints[i + 2].x + b3(u) * ctrlpoints[i + 3].x;
			pointcourbe.y = b0(u) * ctrlpoints[i].y + b1(u) * ctrlpoints[i + 1].y + b2(u) * ctrlpoints[i + 2].y + b3(u) * ctrlpoints[i + 3].y;

			if (elements_num > 0) {
				float opp = pointcourbe.y - vertices[elements_num - 1];
				float adj = pointcourbe.x - vertices[elements_num - 2];

				float radangle = atan2f(opp, adj);
				rotations[rot_nums++] = radangle * (180.0f / WM_PI);
			}
			else {
				rotations[rot_nums++] = 0.0f;
			}



			vertices[elements_num] = pointcourbe.x;
			vertices[elements_num + 1] = pointcourbe.y;
			elements_num += 2;
		}
	}


	render_use_program(&game_state->render_mngr, prog_notex);

	render_push_matrix(&game_state->render_mngr);

	Vector_t pos = { 0, 0 };

	render_set_position(&game_state->render_mngr, pos);

	//rendu de la courbe
	render_start_draw_array(&game_state->render_mngr, 2, elements_num * 0.5f, vertices, NULL);
	ColorA_t link_color = { (float)1.0f,0.0f,0.0f,1.0f };
	render_set_color(&game_state->render_mngr, link_color);
	render_draw_line(&game_state->render_mngr, 0, elements_num * 0.5f);
	render_end_draw_array(&game_state->render_mngr, wfalse);


	link_color.r = link_color.b = link_color.g = link_color.a = 1.0f;

	render_set_color(&game_state->render_mngr, link_color);

	render_pop_matrix(&game_state->render_mngr);

	//rendu du terrain

	render_push_matrix(&game_state->render_mngr);

	render_set_position(&game_state->render_mngr, Vec(0, 0));

	float vertices_terrain[8192];
	int32_t elements_terrain = 0;

	float stx = 100.0f;
	float sty = 100.0f;

	for (int it = 0; it < 4; it++) {
		vertices_terrain[elements_terrain] = stx;
		vertices_terrain[elements_terrain + 1] = sty;

		vertices_terrain[elements_terrain + 2] = stx;
		vertices_terrain[elements_terrain + 3] = sty - 64.0f;

		vertices_terrain[elements_terrain + 4] = stx + 64.0f;
		vertices_terrain[elements_terrain + 5] = sty;

		vertices_terrain[elements_terrain + 6] = stx;
		vertices_terrain[elements_terrain + 7] = sty - 64.0f;

		vertices_terrain[elements_terrain + 8] = stx + 64.0f;
		vertices_terrain[elements_terrain + 9] = sty - 64.0f;

		vertices_terrain[elements_terrain + 10] = stx + 64.0f;
		vertices_terrain[elements_terrain + 11] = sty;

		elements_terrain += 12;
		stx += 64.0f;
	}

	

	for (int it = 0; it < 2; it++) {
		vertices_terrain[elements_terrain] = stx;
		vertices_terrain[elements_terrain + 1] = sty;

		vertices_terrain[elements_terrain + 2] = stx;
		vertices_terrain[elements_terrain + 3] = sty - 64.0f;

		vertices_terrain[elements_terrain + 4] = stx + 64.0f;
		vertices_terrain[elements_terrain + 5] = sty;

		vertices_terrain[elements_terrain + 6] = stx;
		vertices_terrain[elements_terrain + 7] = sty - 64.0f;

		vertices_terrain[elements_terrain + 8] = stx + 64.0f;
		vertices_terrain[elements_terrain + 9] = sty - 64.0f;

		vertices_terrain[elements_terrain + 10] = stx + 64.0f;
		vertices_terrain[elements_terrain + 11] = sty;

		elements_terrain += 12;
		sty += 64.0f;
	}


	for (int it = 0; it < 4; it++) {
		vertices_terrain[elements_terrain] = stx;
		vertices_terrain[elements_terrain + 1] = sty;

		vertices_terrain[elements_terrain + 2] = stx;
		vertices_terrain[elements_terrain + 3] = sty - 64.0f;

		vertices_terrain[elements_terrain + 4] = stx + 64.0f;
		vertices_terrain[elements_terrain + 5] = sty;

		vertices_terrain[elements_terrain + 6] = stx;
		vertices_terrain[elements_terrain + 7] = sty - 64.0f;

		vertices_terrain[elements_terrain + 8] = stx + 64.0f;
		vertices_terrain[elements_terrain + 9] = sty - 64.0f;

		vertices_terrain[elements_terrain + 10] = stx + 64.0f;
		vertices_terrain[elements_terrain + 11] = sty;

		elements_terrain += 12;
		stx += 64.0f;
	}
	



	render_start_draw_array(&game_state->render_mngr, 2, elements_terrain * 0.5f, vertices_terrain, NULL);

	render_set_color(&game_state->render_mngr, white_color);

	render_draw_array(&game_state->render_mngr, 0, elements_terrain * 0.5f);

	render_end_draw_array(&game_state->render_mngr, wfalse);

	render_pop_matrix(&game_state->render_mngr);


	return 1;
}


static int draw_func(gamestate_t* const game_state)
{

	float vertices_terrain[8192];
	int32_t elements_terrain = 0;

	float stx = 100.0f;
	float sty = 100.0f;

	for (int it = 0; it < 4; it++) {
		vertices_terrain[elements_terrain] = stx;
		vertices_terrain[elements_terrain + 1] = sty;

		vertices_terrain[elements_terrain + 2] = stx;
		vertices_terrain[elements_terrain + 3] = sty - 64.0f;

		vertices_terrain[elements_terrain + 4] = stx + 64.0f;
		vertices_terrain[elements_terrain + 5] = sty;

		vertices_terrain[elements_terrain + 6] = stx;
		vertices_terrain[elements_terrain + 7] = sty - 64.0f;

		vertices_terrain[elements_terrain + 8] = stx + 64.0f;
		vertices_terrain[elements_terrain + 9] = sty - 64.0f;

		vertices_terrain[elements_terrain + 10] = stx + 64.0f;
		vertices_terrain[elements_terrain + 11] = sty;

		elements_terrain += 12;
		stx += 64.0f;
	}



	for (int it = 0; it < 2; it++) {
		vertices_terrain[elements_terrain] = stx;
		vertices_terrain[elements_terrain + 1] = sty;

		vertices_terrain[elements_terrain + 2] = stx;
		vertices_terrain[elements_terrain + 3] = sty - 64.0f;

		vertices_terrain[elements_terrain + 4] = stx + 64.0f;
		vertices_terrain[elements_terrain + 5] = sty;

		vertices_terrain[elements_terrain + 6] = stx;
		vertices_terrain[elements_terrain + 7] = sty - 64.0f;

		vertices_terrain[elements_terrain + 8] = stx + 64.0f;
		vertices_terrain[elements_terrain + 9] = sty - 64.0f;

		vertices_terrain[elements_terrain + 10] = stx + 64.0f;
		vertices_terrain[elements_terrain + 11] = sty;

		elements_terrain += 12;
		sty += 64.0f;
	}


	for (int it = 0; it < 4; it++) {
		vertices_terrain[elements_terrain] = stx;
		vertices_terrain[elements_terrain + 1] = sty;

		vertices_terrain[elements_terrain + 2] = stx;
		vertices_terrain[elements_terrain + 3] = sty - 64.0f;

		vertices_terrain[elements_terrain + 4] = stx + 64.0f;
		vertices_terrain[elements_terrain + 5] = sty;

		vertices_terrain[elements_terrain + 6] = stx;
		vertices_terrain[elements_terrain + 7] = sty - 64.0f;

		vertices_terrain[elements_terrain + 8] = stx + 64.0f;
		vertices_terrain[elements_terrain + 9] = sty - 64.0f;

		vertices_terrain[elements_terrain + 10] = stx + 64.0f;
		vertices_terrain[elements_terrain + 11] = sty;

		elements_terrain += 12;
		stx += 64.0f;
	}

	render_push_matrix(&game_state->render_mngr);

	render_use_program(&game_state->render_mngr, prog_notex);

	render_start_draw_array(&game_state->render_mngr, 2, elements_terrain * 0.5f, vertices_terrain, NULL);

	render_set_color(&game_state->render_mngr, white_color);

	render_draw_array(&game_state->render_mngr, 0, elements_terrain * 0.5f);

	render_end_draw_array(&game_state->render_mngr, wfalse);

	render_pop_matrix(&game_state->render_mngr);

	int max_steps = 16; // nombre de noeuds
	int nctrlpoints = 6; //m

	Vector_t ctrlpoints[6];

	float width = 256;
	float step = 32;

	ctrlpoints[0].x = 100;//100 - 64 / x de d�part - 64
	ctrlpoints[0].y = 100;

	ctrlpoints[1].x = wallx - 32.0f;
	ctrlpoints[1].y = 100;

	ctrlpoints[2].x = wallx - 32.0f;
	ctrlpoints[2].y = 100 + 32.0f;

	ctrlpoints[3].x = wallx - 32.0f;
	ctrlpoints[3].y = 100 + 128.0f;

	ctrlpoints[4].x = wallx - 32.0f;
	ctrlpoints[4].y = 100 + 160.0f;

	ctrlpoints[5].x = 512;
	ctrlpoints[5].y = 100 + 128.0f;


	float vertices[8192];
	int32_t elements_num = 0;
	int extra = 0;


	float rotations[8192];
	int32_t rot_nums = 0;

	for (int i = 0; i < nctrlpoints - 3; i++)
	{
		for (int j = 0; j < max_steps; j++)
		{
			double u = (double)j / (double)max_steps;
			Vector_t pointcourbe;

			pointcourbe.x = b0(u) * ctrlpoints[i].x + b1(u) * ctrlpoints[i + 1].x + b2(u) * ctrlpoints[i + 2].x + b3(u) * ctrlpoints[i + 3].x;
			pointcourbe.y = b0(u) * ctrlpoints[i].y + b1(u) * ctrlpoints[i + 1].y + b2(u) * ctrlpoints[i + 2].y + b3(u) * ctrlpoints[i + 3].y;

			if (elements_num > 0) {
				float opp = pointcourbe.y - vertices[elements_num - 1];
				float adj = pointcourbe.x - vertices[elements_num - 2];

				float radangle = atan2f(opp, adj);
				rotations[rot_nums++] = radangle * (180.0f / WM_PI);
			}
			else {
				rotations[rot_nums++] = 0.0f;
			}



			vertices[elements_num] = pointcourbe.x;
			vertices[elements_num + 1] = pointcourbe.y;
			elements_num += 2;
		}
	}


	render_use_program(&game_state->render_mngr, prog_tex);

	//modification des rotations en fonction de la courbe
	//rotations

	//copie des 16 premi�res rotations, en fonction du point o� nous sommes situ�

	if (ianimpoint >= elements_num / 2) {
		ianimpoint = 0;

		for (int irot = 0; irot < 16; irot++)
		{
			rotations_sprite[irot] = 0.0f;
		}

	}

	rotations_sprite[15] = -rotations[ianimpoint];

	for (int irot = 1; irot < 16; irot++)
	{
		if ((ianimpoint - irot) > 0)
		{
			rotations_sprite[15-irot] = -rotations[ianimpoint - irot];
		}
		
	}
	

	render_setuniform_arrayfloat(&game_state->render_mngr, "rotations", rotations_sprite, 16);

	sprite_draw(&sprite_curve, vertices[ianimpoint * 2] - 128.0f, vertices[(ianimpoint * 2)+1], &game_state->render_mngr, NULL);

	render_use_program(&game_state->render_mngr, prog_notex);

	render_push_matrix(&game_state->render_mngr);

	Vector_t pos = { 0, 0 };

	render_set_position(&game_state->render_mngr, pos);

	//rendu de la courbe
	render_start_draw_array(&game_state->render_mngr, 2, elements_num * 0.5f, vertices, NULL);
	ColorA_t link_color = { (float)1.0f,0.0f,0.0f,1.0f };
	render_set_color(&game_state->render_mngr, link_color);
	render_draw_line(&game_state->render_mngr, 0, elements_num * 0.5f);
	render_end_draw_array(&game_state->render_mngr, wfalse);


	link_color.r = link_color.b = link_color.g = link_color.a = 1.0f;

	render_set_color(&game_state->render_mngr, link_color);

	render_pop_matrix(&game_state->render_mngr);

	return 1;
}

float timing = 0;
float MTIME = 100;

static int update_func(gamestate_t* const gamestate, float elapsed)
{
	if (doanim)
	{
		if (timing >= MTIME)
		{
			ianimpoint++;
			timing = 0;
		}
		else {
			timing += elapsed;
		}
	}
	

	sprite_update(&sprite_curve, elapsed);

	input_mngr_handle_input(&gamestate->input_mngr);




	return 1;
}


static int update_spline_func(gamestate_t* const gamestate, float elapsed)
{
	if (timing >= MTIME)
	{
		cvel++;
		timing = 0;
	}
	else {
		timing += elapsed;
	}
		

	input_mngr_handle_input(&gamestate->input_mngr);

	return 1;
}

static int update_sprite_spline(gamestate_t* const gamestate, float elapsed)
{

	input_mngr_handle_input(&gamestate->input_mngr);

	return 1;
}

int sprite_bezier_curve_demo(gamestate_t* const game_state)
{
	game_state->demo_draw_func = draw_func;
	game_state->demo_update_func = update_func;

	prog_tex = render_get_program(&game_state->render_mngr, "prog_deform");

	prog_notex = render_get_program(&game_state->render_mngr, "prog_notex");

	
	texture_info* npc_tex = gettexture_resx(&game_state->resx_mngr, "hhworm.png");
	//0.0, -11.25, -22.5, -22.5, -45.0, -45.0, -67.5, -67.5);
	/*rotations[0] = 0.0f;
	rotations[1] = -5.625f;
	rotations[2] = -11.25f;
	rotations[3] = -16.875f;
	rotations[4] = -22.5f;
	rotations[5] = -28.125f;
	rotations[6] = -33.75f;
	rotations[7] = -39.375f;
	rotations[8] = -45.0f;
	rotations[9] = -50.625f;
	rotations[10] = -56.25f;
	rotations[11] = -61.875f;
	rotations[12] = -67.5f;
	rotations[13] = -73.125f;
	rotations[14] = -78.75f;
	rotations[15] = -84.375f;*/

	/*rotations[0] = 0.0f;
	rotations[1] = 0.0f;
	rotations[2] = 0.0f;
	rotations[3] = 0.0f;
	rotations[4] = 0.0f;
	rotations[5] = 0.0f;
	rotations[6] = 0.0f;
	rotations[7] = 0.0f;
	rotations[8] = -5.625f;
	rotations[9] = -11.25f;
	rotations[10] = -16.875f;
	rotations[11] = -22.5f;
	rotations[12] = -28.125f;
	rotations[13] = -33.75f;
	rotations[14] = -39.375f;
	rotations[15] = -45.0f;*/

	//S -> monte une marche
	/*rotations[0] = -33.75f;
	rotations[1] = -33.75f;
	rotations[2] = -33.75f;
	rotations[3] = -45.0f;
	rotations[4] = -56.25f;
	rotations[5] = -67.5f;
	rotations[6] = -78.75f;
	rotations[7] = -90.00f;
	rotations[8] = -90.00f;
	rotations[9] = -78.75f;
	rotations[10] = -67.5f;
	rotations[11] = -56.25f;
	rotations[12] = -45.0f;
	rotations[13] = -33.75f;
	rotations[14] = -33.75f;
	rotations[15] = -33.75f;*/

	//monte une marche2, sans la forme S
	/*rotations[0] = -90.00f;
	rotations[1] = -90.00f;
	rotations[2] = -90.00f;
	rotations[3] = -90.00f;
	rotations[4] = -90.00f;
	rotations[5] = -90.00f;
	rotations[6] = -90.00f;
	rotations[7] = -90.00f;
	rotations[8] = -90.00f;
	rotations[9] = -78.75f;
	rotations[10] = -67.5f;
	rotations[11] = -56.25f;
	rotations[12] = -45.0f;
	rotations[13] = -33.75f;
	rotations[14] = -33.75f;
	rotations[15] = -33.75f;*/


	sprite_init(&sprite_curve, 256, 64, 256, 64, npc_tex, NULL, 15, 0, &game_state->render_mngr, wfalse, 0, WFLOAT1, WFLOAT1,wfalse);

	game_state->module_mngr.game_input = KeyboardCallback;


	game_state->gameloop_func(game_state);

	//utilisation des b-splines pour faire le rendu du lombric
	// rappel : une b-spline est une g�n�ralisation de la courbe de b�zier
	// on a besoin d'un nombre de noeud + de points de controle
	// dans notre cas, nous allons partir sur une b-spline uniforme car nos noeuds vont �tre espac�s avec un pas de m�me valeur tout au long de la courbe
	// voir aussi Uniform Cubic B - Spline Curves https://www2.cs.uregina.ca/~anima/UniformBSpline.pdf


	return 1;
}
