#include <common_include.h>
#include <demo_list.h>


static particle_object_t particle_obj = {0};
static Vector_t particle_target = {0.0,0.0};
static float rotate_step = 0.0f;
static Vector_t particle_pos = {320.0f,200.0f};

static int draw_function(gamestate_t* const game_state)
{
	particle_object_draw(&particle_obj,&game_state->render_mngr,vectorzero);
	return 0;
}

static int update_function(gamestate_t* const game_state,float elapsed)
{
	particle_object_update(&particle_obj,elapsed);

	particle_target.x = 100.0f * cosf(rotate_step) + particle_pos.x;
	particle_target.y = 100.0f * sinf(rotate_step) + particle_pos.y;

	rotate_step += 0.05f;

	Vector_t dir = Vec_minus(particle_target,particle_pos);
	Vec_normalise(&dir);

	particle_obj.emitter_array[0].direction = dir;
	

	return 0;
}


int particle_demo(gamestate_t* const game_state)
{
	game_state->demo_draw_func = draw_function;
	game_state->demo_update_func = update_function;

	particle_object_init(&particle_obj,&game_state->render_mngr,"prog_notex",NULL,NULL,NULL,NULL);

	ParticleEmitter* emitter = (ParticleEmitter*)wf_calloc(1,sizeof(ParticleEmitter));

	particle_emitter__init(emitter);

	emitter->rate = 4;
	
	emitter->color = (Wcolor*)wf_calloc(1,sizeof(Wcolor));
	wcolor__init(emitter->color);

	emitter->color->r = emitter->color->a = 1.0f;
	emitter->color->g = emitter->color->b = 0.0f;

	emitter->width_particle = 8;
	emitter->height_particle = 8;

	emitter->max_particle = 40;

	emitter->life_time = 2000.0f;
	emitter->total_particle = 700;
	emitter->offset =  (Wvector*)wf_calloc(1,sizeof(Wvector));
	wvector__init(emitter->offset);
	emitter->offset->x = particle_pos.x;
	emitter->offset->y = particle_pos.y;

	emitter->direction = (Wvector*)wf_calloc(1,sizeof(Wvector));
	wvector__init(emitter->direction);
	emitter->direction->x = 0.0f;
	emitter->direction->y = 1.0f;

	emitter->speed = 80.0f;

	
	particle_object_addemitter(&particle_obj,emitter,&game_state->render_mngr,vectorzero);

	particle_object_genvbo(&particle_obj,&game_state->render_mngr);

	game_state->gameloop_func(game_state);

	return 0;
}
