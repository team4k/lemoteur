#include <common_include.h>

#include <demo_list.h>


static sprite_t avatar;
static Vector_t avtar_position;

static int draw_func(gamestate_t* const game_state)
{
	sprite_draw(&avatar,avtar_position.x,avtar_position.y,&game_state->render_mngr,NULL);

	   return 1;
}

static int update_func(gamestate_t* const game_state,float elapsed)
{
	avatar.scale_x = 2.0f;
	avatar.scale_y = 2.0f;

	sprite_update(&avatar,elapsed);



		/*if(glfwGetKey(game_state->render_mngr.oglWindow,GLFW_KEY_DOWN) == GLFW_PRESS)
		{
			avatar_playanimation(&avatar,"rush");
		}

		if(glfwGetKey(game_state->render_mngr.oglWindow,GLFW_KEY_UP) == GLFW_PRESS)
		{
			avatar_playanimation(&avatar,"kick");
		}*/

		return 1;
}

//
//basic gameplay
//
int animation_demo(gamestate_t* const game_state)
{
	game_state->demo_draw_func = draw_func;
	game_state->demo_update_func = update_func;


	texture_info* player_text = gettexture_resx(&game_state->resx_mngr,"anzu2.png");

    //load player texture
    if(!player_text)
    {
        logprint("Erreur chargement Texture joueur ! %s ","anzu2.png");
        return 0;
    }

	AnimationList* player_animation = getanimation_resx(&game_state->resx_mngr,"anzu2.awf");

	if(!player_animation)
	{
		logprint("Erreur chargement animation ! %s","anzu2.awf");
		return 0;
	}

	avtar_position.x = game_state->render_mngr.screen_info.width * WFLOAT05;
	avtar_position.y = game_state->render_mngr.screen_info.height * WFLOAT05;

	sprite_init(&avatar,32,32,player_text->width,player_text->height,player_text,player_animation,0,0,&game_state->render_mngr,wfalse,0,1.0f,1.0f,wtrue);

	sprite_playanimation(&avatar,"walk_down");

	game_state->gameloop_func(game_state);

    return 0;
}
