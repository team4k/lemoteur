#include <common_include.h>

#include <demo_list.h>

#include <Debug/ChipmunkDebugDraw.h>


static shatter_effect_t shatter_test;

static Vector_t tmp_sh;
static cpVect ChipmunkDemoMouse;

static SHADERPROGRAM_ID prog_common = NULL;
static SHADERPROGRAM_ID prog_notex = NULL;

static void Mouse_move_callback(float targetX,float targetY,wbool pointer_control)
{
	ChipmunkDemoMouse = cpv(targetX,targetY);
}

static int draw_func(gamestate_t* const game_state)
{

	ChipmunkDebugDrawClearRenderer();

	render_use_program(&game_state->render_mngr, prog_common);

	shatter_effect_draw(&shatter_test,&game_state->render_mngr);

	render_use_program(&game_state->render_mngr, prog_notex);

	ChipmunkDebugDrawImpl(&game_state->render_mngr, game_state->phy_mngr.world);


	 return 1;
}

static int update_func(gamestate_t* const game_state,float elapsed)
{
	//shatter_effect_update(&shatter_test,elapsed,game_state->phy_mngr.world);

	update_physics(&game_state->phy_mngr,0.01f);
	 
	return 1;
}


int shatter_demo(gamestate_t* const game_state)
{
	game_state->demo_draw_func = draw_func;
	game_state->demo_update_func = update_func; 

	prog_common = render_get_program(&game_state->render_mngr, "prog_common");
	prog_notex = render_get_program(&game_state->render_mngr, "prog_notex");

	shatter_effect_init(&shatter_test,128,128,gettexture_resx(&game_state->resx_mngr,"bidon2.png"),-1,&game_state->render_mngr,1);
	cpVect box_verts[4]  = {0};

	float halfWidth = 128.0f * 0.5f;
	float halfHeight =  128.0f * 0.5f;

	cpVect pos = cpv(250,250);
	
	box_verts[0] = cpv((-halfWidth +pos.x),(-halfHeight + pos.y));
	box_verts[1] = cpv(halfWidth + pos.x ,(-halfHeight + pos.y));
	box_verts[2] = cpv(halfWidth + pos.x,halfHeight +pos.y);
	box_verts[3] = cpv((-halfWidth + pos.x),halfHeight +pos.y);

	physics_init(&game_state->phy_mngr,wfalse);


	shatter_effect_updatesize(&shatter_test,128,128,-1);

	shatter_effect_updatetexture(&shatter_test, gettexture_resx(&game_state->resx_mngr,"bidon2.png"));

	shatter_effect_create(&shatter_test,0,box_verts,4,game_state->phy_mngr.world,50,cpv(pos.x,pos.y + halfHeight),pos,20.0f,cpvzero,0.0f,wfalse,wfalse,&game_state->render_mngr,wfalse,wfalse);

	cpSpaceSetGravity(game_state->phy_mngr.world,cpv(0,-5));
    
	game_state->module_mngr.game_movetarget = Mouse_move_callback;

	ChipmunkDebugDrawInit(&game_state->render_mngr);

	game_state->gameloop_func(game_state);
	
	return 1;
}