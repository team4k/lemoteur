#include <common_include.h>

#include <demo_list.h>

static screen_fill_t screen_filler;
static float delta = 400.0f;
static float del = 0;


static int draw_effect(gamestate_t* const game_state)
{
	render_push_matrix(&game_state->render_mngr);
	screen_fill_draw(&screen_filler,&game_state->render_mngr);		
	render_pop_matrix(&game_state->render_mngr);

	return 0;
}

static int update_effect(gamestate_t* const game_state,float elapsed)
{
	screen_fill_update(&screen_filler,&game_state->render_mngr,elapsed);

	//update screen filler position
	if(del <= 0.0f)
	{
		screen_filler.base_position.y -= 5;
		del = delta;
	}

	del -= elapsed;
		

	return 0;
}

int screen_effect_demo(gamestate_t* const game_state)
{
	game_state->demo_draw_func = draw_effect;
	game_state->demo_update_func = update_effect;
	del = delta;
	Vector_t dir = {0,0};

	screen_fill_init(&screen_filler,5,250,dir,gettexture_resx(&game_state->resx_mngr,"lava.png"),64,64,&game_state->render_mngr,getanimation_resx(&game_state->resx_mngr,"lava.awf"),"laval_fill","lava_edge");

	screen_filler.base_position.x = 0;
	screen_filler.base_position.y = game_state->render_mngr.screen_info.height;



	game_state->gameloop_func(game_state);

	return 0;
}