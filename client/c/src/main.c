#include <common_include.h>

#include <demo_list.h>


gamestate_t game_state;
char resources_path[260];
char config_path[260];
char script_path[260];
char app_path[WAFA_MAX_PATH];

const double SCREEN_WIDTH = 640.0;
const double SCREEN_HEIGHT = 480.0;
//const double SCREEN_WIDTH = 1280.0;
//const double SCREEN_HEIGHT = 720.0;


void configure()
{
	//load lua config file, (if any)
	game_state.lua_state =  script_createstate();

	game_state.module_mngr.game_disableinput = base_disable_input;
	game_state.module_mngr.game_target = base_game_target;
	game_state.module_mngr.game_endtarget = base_game_end_target;
	game_state.module_mngr.game_setcursorpos = base_setcursor_pos;
	game_state.module_mngr.game_movetarget = base_move_target;
	game_state.module_mngr.game_offset_target = base_offset_target;
	game_state.module_mngr.game_input = base_game_input;
	game_state.module_mngr.game_char_input = base_game_char_input;
	game_state.module_mngr.game_joystick_axe = base_joystick_axe;
	game_state.module_mngr.game_joystick_button = base_joystick_button;
	game_state.module_mngr.game_changeinputmode = base_change_input_mode;

	config_init(&game_state.config,game_state.lua_state);
	config_readconf(&game_state.config,(void*)&game_state.resx_mngr,&config_path[0]);

	//config.config_saved = callback_config_saved;

	//update resources path if needed (PC builds only)

	char c_asset_path[260];
	char c_script_path[260];
	char new_resx_path[260];
	char new_script_path[260];


	if(config_getstring(&game_state.config,"asset_path",&c_asset_path[0],260))
	{
		strcpy(new_resx_path,app_path);

		//remove last slash in path
		if(new_resx_path[strlen(new_resx_path)-1] == PATH_SEP)
			new_resx_path[strlen(new_resx_path)-1] = '\0';

		combine_rel_path(new_resx_path,&c_asset_path[0],260);

		sprintf(resources_path,"%s",new_resx_path);
	}

	if(config_getstring(&game_state.config,"script_path",&c_script_path[0],260))
	{
		strcpy(new_script_path,app_path);

		//remove last slash in path
		if(new_script_path[strlen(new_script_path)-1] == PATH_SEP)
			new_script_path[strlen(new_script_path)-1] = '\0';

		combine_rel_path(new_script_path,&c_script_path[0],260);

		sprintf(script_path,"%s",new_script_path);
	}


	//add our script folder path to lua package path
	char package_path[500];
	char final_package_path[1000];

			#if defined(_WIN32)
				sprintf(package_path,"package.path = '%s\\?.lua;' .. package.path",&script_path[0]);

				str_double_slash(&package_path[0],final_package_path,"\\");
			#else
					sprintf(final_package_path,"package.path = '%s/?.lua;' .. package.path",&script_path[0]);
			#endif

	const char* log_return  = script_execstring(game_state.lua_state,final_package_path,NULL,0);

	if(log_return != NULL)
		logprint(log_return);
	else
		logprint("Chemin scripts lua ajouté au package.path");
}
	


int init_render(render_manager_t* const render_mngr,void* app_instance,void* app_handle)
{
	render_init(render_mngr,SCREEN_WIDTH,SCREEN_HEIGHT,&app_path[0],"Demo Wafa engine",app_instance,app_handle,wfalse,wfalse,filter_linear,60);

	//load shader
	shader_info* vert_shader = getshader_resx(&game_state.resx_mngr,"vertex_shader.shad",wfalse);
	shader_info* pix_shader = getshader_resx(&game_state.resx_mngr,"pixel_shader.shad",wtrue);
		
	shader_info* vert_notex_shader = getshader_resx(&game_state.resx_mngr,"vertex_shader_notex.shad",wfalse);
	shader_info* pixel_notex_shader = getshader_resx(&game_state.resx_mngr,"pixel_shader_notex.shad",wtrue); 

	shader_info* vertex_shader_notex_colored = getshader_resx(&game_state.resx_mngr, "vertex_shader_notex_colored.shad", wfalse);

	shader_info* vs_font = getshader_resx(&game_state.resx_mngr, "vs_fontmsdf.shad", wfalse);
	shader_info* ps_font = getshader_resx(&game_state.resx_mngr, "ps_fontmsdf.shad", wtrue);

	shader_info* vs_font_outline = getshader_resx(&game_state.resx_mngr, "vs_fontmsdf_outline.shad", wfalse);
	shader_info* ps_font_outline = getshader_resx(&game_state.resx_mngr, "ps_fontmsdf_outline.shad", wtrue);


	SHADERPROGRAM_ID prog_id =  render_create_program(&game_state.render_mngr,"prog_common",vert_shader->shader_pointer,pix_shader->shader_pointer);

	SHADERPROGRAM_ID prog_no_tex_id = render_create_program(&game_state.render_mngr,"prog_notex",vert_notex_shader->shader_pointer,pixel_notex_shader->shader_pointer);

	render_create_program(&game_state.render_mngr,"prog_mesh",getshader_resx(&game_state.resx_mngr,"vs_mesh.shad",wfalse)->shader_pointer,getshader_resx(&game_state.resx_mngr,"ps_mesh.shad",wtrue)->shader_pointer);

	render_create_program(&game_state.render_mngr, "prog_colored_no_texture", vertex_shader_notex_colored->shader_pointer, pixel_notex_shader->shader_pointer);

	render_create_program(&game_state.render_mngr, "prog_font", vs_font->shader_pointer, ps_font->shader_pointer);

	render_create_program(&game_state.render_mngr, "prog_font_outline", vs_font_outline->shader_pointer, ps_font_outline->shader_pointer);

	render_create_program(&game_state.render_mngr, "prog_deform", getshader_resx(&game_state.resx_mngr, "vs_deform.shad", wfalse)->shader_pointer, getshader_resx(&game_state.resx_mngr, "ps_deform.shad", wtrue)->shader_pointer);

	render_create_program(&game_state.render_mngr, "prog_debug_chipmunk", getshader_resx(&game_state.resx_mngr, "vs_debugchipmunk.shad", wfalse)->shader_pointer, getshader_resx(&game_state.resx_mngr, "ps_debugchipmunk.shad", wtrue)->shader_pointer);

	ChipmunkDebugDrawInit(&game_state.render_mngr);


	//render_create_program(&game_state.render_mngr,"prog_test",getshader_resx(&game_state.resx_mngr,"vs_charlighting.shad",wfalse)->shader_pointer,getshader_resx(&game_state.resx_mngr,"ps_charlighting.shad",wtrue)->shader_pointer);



	render_use_program(&game_state.render_mngr,prog_id);	

    return 0;
}

int (*demo_ptr[])(gamestate_t* const game_state) = {rectangle_demo,tilemap_demo,animation_demo,chipmunk_demo,shatter_demo,sound_demo,font_demo,screen_effect_demo,custom_vertices_demo,shadow_demo,input_demo,url_demo,normal_map_demo,fireball_demo,save_mngr_demo,pathfind_demo,test_grid_walk_demo,mesh_demo,particle_demo,level_stream_demo,multi_sprite_entity_demo,sprite_bezier_curve_demo};



void em_main(void* val)
{
	int option = 0;

	 if(option >= 0 && option < (sizeof(demo_ptr) / 4))
		 (*demo_ptr[option])((gamestate_t*)val);
}

#ifdef RENDERER_DX
int WINAPI WinMain(HINSTANCE hInstance,	//Main windows function
		   HINSTANCE hPrevInstance, 
		   LPSTR lpCmdLine,
		   int nShowCmd)
#else
int main( void )
#endif
{

   int option = 1;
	#ifdef EMSCRIPTEN
	   sprintf(&app_path[0], "");
	#elif !defined(WINAPI_FAMILY) || (WINAPI_FAMILY != WINAPI_FAMILY_PHONE_APP)
	   get_app_path(app_path);
	   logprint_setdir(app_path);
	#endif

	w_sprintf(config_path,260,"%sconfig.lua",&app_path[0]);


	configure();

	init_resources(&game_state.resx_mngr,&resources_path[0],&game_state.render_mngr);

	init_render(&game_state.render_mngr,NULL,NULL);

	input_mngr_init(&game_state.input_mngr,&game_state.render_mngr,&game_state.module_mngr,1.0f,wfalse,wfalse,wtrue);

	game_state.gameloop_func = gameloop;

	printf("refresh rate : %d",render_getrefreshrate(&game_state.render_mngr));

	#ifdef  EMSCRIPTEN
		//input demo
		(*demo_ptr[10])(&game_state);
	#else
		printf("Enter number to run demo : \n");
		printf("(1) Geom Demo \n");
		printf("(2) Level with tilemap Demo \n");
		printf("(3) Animation Demo \n");
		printf("(4) Chipmunk fighter character Demo \n");
		printf("(5) Chipmunk Shatter tile Demo \n");
		printf("(6) Sound Demo \n");
		printf("(7) Font Demo \n");
		printf("(8) screen effects Demo \n");
		printf("(9) draw custom vertices Demo \n");
		printf("(10) shadows Demo \n");
		printf("(11) input Demo \n");
		printf("(12) url Demo \n");
		printf("(13) normal Demo \n");
		printf("(14) fireball Demo \n");
		printf("(15) save manager Demo \n");
		printf("(16) A* Demo \n");
		printf("(17) test grid walk \n");
		printf("(18) mesh demo \n");
		printf("(19) particle demo \n");
		printf("(20) level stream demo \n");
		printf("(21) multi sprite entity demo \n");
		printf("(22) sprite bezier curve demo \n");

		scanf("%d", &option);
		option--;

	   if(option >= 0 && option < (sizeof(demo_ptr) / 4))
			(*demo_ptr[option])(&game_state);


	   render_stop(&game_state.render_mngr);

	   system("PAUSE");

	   return 0;
	  
	#endif



}




