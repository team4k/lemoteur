#include <common_include.h>

#include <demo_list.h>

static Mesh_t mesh_obj;
static SHADERPROGRAM_ID mesh_prog;
static Vector3_t mesh_position;
static Rect_t rectangle_test = {50,50,{100,240}};

static int draw_func(gamestate_t* const game_state)
{
	render_use_program(&game_state->render_mngr,mesh_prog);

	mesh_draw(&mesh_obj,&game_state->render_mngr);

	return 1;

}

static int update_func(gamestate_t* const gamestate,float elapsed)
{
	float mov_value = 0.5f * elapsed;

	float rot_y = mesh_obj.rotation_y;
	float rot_z = mesh_obj.rotation_z;
	float rot_x = mesh_obj.rotation_x;

	if(input_mngr_getkey(&gamestate->input_mngr,KEY_DOWN) == KEY_PRESS)
		rot_z -= 5.0f;
        
	if(input_mngr_getkey(&gamestate->input_mngr,KEY_UP) == KEY_PRESS)
		rot_z += 5.0f;

	if(input_mngr_getkey(&gamestate->input_mngr,KEY_LEFT) == KEY_PRESS)
		rot_x -= 5.0f;

	if(input_mngr_getkey(&gamestate->input_mngr,KEY_RIGHT) == KEY_PRESS)
		rot_x += 5.0f;

	if(input_mngr_getkey(&gamestate->input_mngr,KEY_D) == KEY_PRESS)
		rot_y += 5.0f;


	if(input_mngr_getkey(&gamestate->input_mngr,KEY_E) == KEY_PRESS)
		rot_y -= 5.0f;


	if(rot_y >= 360.0f)
		rot_y = rot_y - 360.0f;

	if(rot_y < 0.0f)
		rot_y = 360.0f - (rot_y * -1.0f);


	if (rot_x >= 360.0f)
		rot_x = rot_x - 360.0f;

	if (rot_x < 0.0f)
		rot_x = 360.0f - (rot_x * -1.0f);

	if (rot_z >= 360.0f)
		rot_z = rot_z - 360.0f;

	if (rot_z < 0.0f)
		rot_z = 360.0f - (rot_z * -1.0f);

	mesh_setrotation(&mesh_obj, rot_x, rot_y, rot_z);

	//mesh_setposition(&mesh_obj, mesh_position);


	return 1;
}

int mesh_demo(gamestate_t* const game_state)
{
	game_state->demo_draw_func = draw_func;
	game_state->demo_update_func = update_func;

	mesh_position.x = 320.0f;
	mesh_position.y = 240.0f;
	mesh_position.z -= 20.0f;

	game_state->render_mngr.near_value = -100.0f;
	game_state->render_mngr.far_value = 100.0f;

	//mesh_init(&mesh_obj,50,50,50,MT_TETRAHEDRON,&game_state->render_mngr);
	//mesh_init(&mesh_obj, 50, 50, 50, MT_CUBE, &game_state->render_mngr);
	mesh_init(&mesh_obj, 50, 50, 50, MT_CYLINDER, &game_state->render_mngr);


	mesh_setposition(&mesh_obj,mesh_position);
	mesh_setrotation(&mesh_obj,10.0f,0.0f,0.0f);
	ColorA_t color = { 1.0f,0.0f,0.0f,1.0f };
	mesh_setcolor(&mesh_obj, color);
	//mesh_setrendermode(&mesh_obj,R_WIREFRAME);


	mesh_obj.mat_flag = (SET_WORLD_MAT | SET_VIEW_MAT);

	mesh_prog = render_get_program(&game_state->render_mngr,"prog_mesh");

	game_state->gameloop_func(game_state);


	return 0;
}