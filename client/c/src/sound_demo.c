#include <common_include.h>

#include <demo_list.h>

static Rect_t rec_static = {64,64,{100,100}};
static ColorA_t color = {1.0,0.0,0.0,1.0};
	
static wbool added = wfalse; 
static wbool is_playing = wfalse;

static int draw_func(gamestate_t* const game_state)
{
	   drawrect(&rec_static,color,&game_state->render_mngr);
	   return 1;
} 

static int update_func(gamestate_t* const game_state,float elapsed)
{
		if(input_mngr_getkey(&game_state->input_mngr,KEY_SPACE) == KEY_PRESS && !is_playing)
        {
            snd_mngr_rewind_sound(&game_state->sound_mngr,"grab");  
            is_playing = wtrue;
        }
                
        if(input_mngr_getkey(&game_state->input_mngr,KEY_ENTER) == KEY_PRESS && is_playing)
        {
            is_playing = wfalse;
        }
                

       

		if(input_mngr_getkey(&game_state->input_mngr,KEY_DOWN) == KEY_PRESS)
            rec_static.position.y -= 2;
        
		if(input_mngr_getkey(&game_state->input_mngr,KEY_UP) == KEY_PRESS)
            rec_static.position.y += 2;

		if(input_mngr_getkey(&game_state->input_mngr,KEY_LEFT) == KEY_PRESS)
            rec_static.position.x -= 2;

		if(input_mngr_getkey(&game_state->input_mngr,KEY_RIGHT) == KEY_PRESS)
            rec_static.position.x += 2;

			//snd_mngr_update_position(&sound_mngr,"btnsmp",rec_static.position);

	return 1;
}


int sound_demo(gamestate_t* const game_state)
{
	game_state->demo_draw_func = draw_func;
	game_state->demo_update_func = update_func;
	 
	#ifdef EMSCRIPTEN
		SDL_Init(SDL_INIT_AUDIO);
	#endif

    snd_mngr_init(&game_state->sound_mngr,1024,0.05f);

	 
	 
	//snd_mngr_playsound(&sound_mngr,getsound_resx(&resx_mngr,"jump.ogg"),wtrue,"jumpsmp");

	//snd_mngr_update_gamespace(&sound_mngr,render_mngr->screen_info,render_);
        
        Vector_t vec= {100,100};

		 
      
         
       // snd_mngr_playsound(&game_state->sound_mngr,getsound_resx(&game_state->resx_mngr,"sfx_select.ogg"),wfalse,"grab",wfalse,wfalse,1,vec,wfalse);
	//snd_mngr_playsound(&sound_mngr,getsound_resx(&resx_mngr,"jump.ogg"),wtrue,"btnsmp",wfalse,wfalse,1,rec_static.position,wtrue);

	snd_mngr_playsound(&game_state->sound_mngr,getsound_resx(&game_state->resx_mngr,"boss.ogg"),wfalse,"music",wfalse,wfalse,MUSIC_TYPE,vectorzero,wfalse,0.0f,NULL);

	//snd_mngr_playsound(&game_state->sound_mngr,getsound_resx(&game_state->resx_mngr,"sfx_select.ogg"),wtrue,"sfx_select",wfalse,wfalse,SFX_TYPE,vectorzero,wfalse);

	snd_mngr_setvolume(&game_state->sound_mngr,6,6);

	game_state->gameloop_func(game_state);

	snd_mngr_free(&game_state->sound_mngr);

	return 0;
}