#include <common_include.h>

#include <demo_list.h>

#ifdef WITH_CURL
static url_t test_url;
#endif

int url_draw(gamestate_t* const game_state)
{



	return 1;
}

int url_update(gamestate_t* const game_state,float elapsed)
{
	#ifdef WITH_CURL
	volatile uint32_t request_status = URL_REQUEST_PENDING;

	atomic_set_value(&request_status,test_url.request_status);

	if(request_status != URL_REQUEST_PENDING)
	{
		switch(request_status)
		{
			case URL_REQUEST_ERROR_CURL:
				logprint("Erreur init curl %s",test_url.error_string);
			break;

			case URL_REQUEST_ERROR_HTTP:
				logprint("Erreur http %s",test_url.error_string);
				break;

			case URL_REQUEST_SUCCESS:
				
				test_url.request_status = URL_REQUEST_PROCESSED;

				if(test_url.result_data.decoded_data != NULL)
					printf((const char*)test_url.result_data.decoded_data);

				break;
		}
	}

	#endif
	return 1;
}


int  url_demo(gamestate_t* const game_state)
{
	#ifdef WITH_CURL
		game_state->demo_draw_func = url_draw;
		game_state->demo_update_func = url_update;

		memset(&test_url,0,sizeof(url_t));

		sprintf(&test_url.url[0],"http://gamecity.reckpunk.com/test_base64.php");

		url_getbase64data(&test_url);

		game_state->gameloop_func(game_state);
	#endif

	return 1;
}