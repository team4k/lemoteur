#include <common_include.h>

#include <demo_list.h>


static light_manager_t light_mngr;
static Tilemap_t level_tilemap;
static Vector_t pos_light = {140,128};
static ColorA_t light_color = {1.0f,1.0f,1.0f,0.2f};

static Vector_t pos_light2 = {240,128};
static ColorA_t light2_color = {1.0f,0.0f,0.0f,0.5f};

static light_t* olight2 = NULL;

static SHADERPROGRAM_ID prog_notex;
static SHADERPROGRAM_ID prog_common;
static light_t* olight = NULL;
static wbool debug_mode = wfalse;


static void KeyboardCallback(int16_t input_event,wbool pressed,wbool from_keyboard)
{
	if(input_event == KEY_SPACE && !pressed)
			debug_mode = (wbool)!debug_mode;

	if(input_event == KEY_U && !pressed)
		olight->enabled = (wbool)!olight->enabled;

	if(input_event == KEY_I && !pressed)
	{
		if(olight != NULL)
		{
			light_manager_removelight(&light_mngr,"main_light");
			olight = NULL;
		}
		else
		{
			olight = light_manager_addlight(&light_mngr,pos_light,light_color,100.0f,"main_light","main",light_omni);
		}

	}

	if(input_event == KEY_O && !pressed)
	{
		if(olight2 != NULL)
		{
			light_manager_removelight(&light_mngr,"second_light");
			olight2 = NULL;
		}
		else
		{
			olight2 = light_manager_addlight(&light_mngr,pos_light2,light2_color,50.0f,"second_light","main",light_directional);
		}
	}
}

static int draw_shadows(gamestate_t* const gamestate)
{

	//draw background 
	render_use_program(&gamestate->render_mngr,prog_notex);

	light_manager_rendermask(&light_mngr, level_tilemap.edge_array, level_tilemap.edge_array_size, &gamestate->render_mngr, wfalse, "main", level_tilemap.tile_size, level_tilemap.col_count, level_tilemap.row_count,vectorzero);

	render_blend_alpha(&gamestate->render_mngr,wfalse);

	ColorA_t color = { 1.0f, 1.0f,1.0f,1.0f };

	render_set_color(&gamestate->render_mngr, color);

	render_use_program(&gamestate->render_mngr, prog_common);
	Tilemap_Draw(&level_tilemap, 0, &gamestate->render_mngr, vectorzero, &light_mngr, &gamestate->resx_mngr);


	if(debug_mode)
	{
		render_use_program(&gamestate->render_mngr, prog_notex);

		//debug draw edges
		for(int32_t iedge = 0; iedge < level_tilemap.edge_array_size;iedge++)
		{
			if(!level_tilemap.edge_array[iedge].is_src || level_tilemap.edge_array[iedge].p_edge == NULL)
				continue;

			ColorA_t col_e = green_color;

			if(level_tilemap.edge_array[iedge].p_edge->type == e_top)
				col_e = white_color;
			else if(level_tilemap.edge_array[iedge].p_edge->type == e_left)
				col_e = blue_color;
			else if(level_tilemap.edge_array[iedge].p_edge->type == e_right)
				col_e = red_color;

			Debug_Drawedge(level_tilemap.edge_array[iedge].p_edge,col_e,&gamestate->render_mngr);

		}

		light_manager_draw(&light_mngr,&gamestate->render_mngr, wtrue,"main",level_tilemap.tile_size,level_tilemap.col_count,level_tilemap.row_count);

		uint32_t light_index = 0;
		uint32_t light_count = 0;
		light_t* olight = NULL;

		while((olight = (light_t*)hashmap_iterate(&light_mngr.light_hash,&light_index,&light_count)))
		{
			for(int32_t ishadow_vol = 0; ishadow_vol < olight->num_shadow_volumes;ishadow_vol++)
			{
				shadow_volume_t* shadow_volume =  &olight->shadows_volumes[ishadow_vol];

				Debug_Drawshadowvolume(shadow_volume,green_color,&gamestate->render_mngr);
			
			}

		}
	}
	else
	{

		//gen shadow on fbo
		light_manager_draw(&light_mngr,&gamestate->render_mngr,wfalse,"main",level_tilemap.tile_size,level_tilemap.col_count,level_tilemap.row_count);
	}
	
	return 0;
}

static int update_shadows(gamestate_t* const gamestate,float elapsed)
{
	if(olight == NULL)
		return 0;

	float mov_value = 0.08f * elapsed;

	if(input_mngr_getkey(&gamestate->input_mngr,KEY_DOWN) == KEY_PRESS)
		light_setposition(olight,Vec(olight->position.x,olight->position.y - mov_value));
		   
	if(input_mngr_getkey(&gamestate->input_mngr,KEY_UP) == KEY_PRESS)
		light_setposition(olight,Vec(olight->position.x,olight->position.y + mov_value));

	if(input_mngr_getkey(&gamestate->input_mngr,KEY_LEFT) == KEY_PRESS)
		light_setposition(olight,Vec(olight->position.x - mov_value,olight->position.y));

	if(input_mngr_getkey(&gamestate->input_mngr,KEY_RIGHT) == KEY_PRESS)
		light_setposition(olight,Vec(olight->position.x + mov_value,olight->position.y));

	return 0;
}

int shadow_demo(gamestate_t* const gamestate)
{
	gamestate->demo_draw_func = draw_shadows;
	gamestate->demo_update_func = update_shadows;

	prog_notex = render_get_program(&gamestate->render_mngr,"prog_notex");
	prog_common = render_get_program(&gamestate->render_mngr,"prog_common");

	ColorA_t color = {0.0f,0.0f,0.0f,0.7f};

	light_manager_init(&light_mngr,color,&gamestate->render_mngr,prog_notex,prog_notex,prog_common,light_topdown,512,512,10,10);

	olight = light_manager_addlight(&light_mngr,pos_light,light_color,100.0f,"main_light","main",light_omni);

	Level* shadow_level = getlevel_resx(&gamestate->resx_mngr,"test_shadow.lwf");

	 //check level
    if(!shadow_level)
    {
        logprint("Erreur chargement niveau ! %s ","test_shadow.lwf");
        return 0;
    }

	//get first map texture name
	char* file_name = (char*)wf_malloc(strlen(shadow_level->map_array[0]->tileset_id) * sizeof(char));;

	get_file_name(shadow_level->map_array[0]->tileset_id,file_name,wfalse);

	sprintf(file_name,"%s%s",&file_name[0],".png");

	texture_info* tileset = gettexture_resx(&gamestate->resx_mngr,&file_name[0]);

    //check tileset texture
    if(!tileset)
    {
        logprint("Erreur chargement Texture niveau ! %s ",file_name);
        return 0;
    }


    //-2- create tilemap based on protobuf data
	Tilemap_Init(&level_tilemap,32,tileset,tileset->texture_name,shadow_level->map_array[0]);
	Tilemap_AddLayer(&level_tilemap,&gamestate->render_mngr,shadow_level->map_array[0]->layer_array[0],tileset,NULL);
	Tilemap_AddCollisions(&level_tilemap,shadow_level->map_array[0]->layer_array[1]);

	Tilemap_gen_edge_list(&level_tilemap,wfalse,NULL);

	render_use_program(&gamestate->render_mngr,prog_common);

	gamestate->module_mngr.game_input = KeyboardCallback;

	gamestate->gameloop_func(gamestate);

	wf_free(file_name);

	return 0;
}