#include <common_include.h>

#include <demo_list.h>

static entity_t avatar_phy;
const float SPEED = 500.0f;
static SHADERPROGRAM_ID prog_tex = 0;


static int draw_func(gamestate_t* const game_state)
{
	ChipmunkDebugDrawClearRenderer();
	
	render_use_program(&game_state->render_mngr,prog_tex);

	entity_draw(&avatar_phy,&game_state->render_mngr,NULL,NULL);


	ChipmunkDebugDrawImpl(&game_state->render_mngr, game_state->phy_mngr.world);


	return 1;
}

static int update_func(gamestate_t* const game_state,float elapsed)
{
	cpVect dir = cpv(0,0);

	if(input_mngr_getkey(&game_state->input_mngr,KEY_DOWN) == KEY_PRESS)
	{
		entity_playanimation(&avatar_phy,"rush",0);
	}

	if(input_mngr_getkey(&game_state->input_mngr,KEY_DOWN) == KEY_PRESS)
	{
		entity_playanimation(&avatar_phy,"kick",0);
	}

	
	entity_update(&avatar_phy,elapsed,&game_state->render_mngr,NULL);
	update_physics(&game_state->phy_mngr,0.01f);
	return 1;
}

//
//physics example
//
int chipmunk_demo(gamestate_t* const game_state)
{
	game_state->demo_draw_func = draw_func;
	game_state->demo_update_func = update_func;

	prog_tex = render_get_program(&game_state->render_mngr,"prog_common");

	physics_init(&game_state->phy_mngr,wfalse);

	Entity entity_data;
	Wvector entity_pos;
	MultiShapeInfo** entity_shapes = (MultiShapeInfo**)wf_calloc(3,sizeof(MultiShapeInfo*));

	entity_shapes[0] = (MultiShapeInfo*)wf_malloc(sizeof(MultiShapeInfo));
	entity_shapes[1] = (MultiShapeInfo*)wf_malloc(sizeof(MultiShapeInfo));
	entity_shapes[2] = (MultiShapeInfo*)wf_malloc(sizeof(MultiShapeInfo));

	multi_shape_info__init(entity_shapes[0]);
	multi_shape_info__init(entity_shapes[1]);
	multi_shape_info__init(entity_shapes[2]);

	entity_shapes[0]->width = 32;
	entity_shapes[0]->height = 32;
	entity_shapes[0]->type = TYPE_PHYSICS__CIRCLE;

	entity_shapes[0]->offset = (Wvector*)wf_malloc(sizeof(Wvector));
	wvector__init(entity_shapes[0]->offset);
	entity_shapes[0]->offset->x = 0;
	entity_shapes[0]->offset->y = -32;

	entity_shapes[1]->width = 32;
	entity_shapes[1]->height = 32;
	entity_shapes[1]->type = TYPE_PHYSICS__BOX;

	entity_shapes[1]->offset = (Wvector*)wf_malloc(sizeof(Wvector));
	wvector__init(entity_shapes[1]->offset);
	entity_shapes[1]->offset->x = 0;
	entity_shapes[1]->offset->y = 0;

	entity_shapes[2]->width = 32;
	entity_shapes[2]->height = 32;
	entity_shapes[2]->type = TYPE_PHYSICS__CIRCLE;

	entity_shapes[2]->offset = (Wvector*)wf_malloc(sizeof(Wvector));
	wvector__init(entity_shapes[2]->offset);
	entity_shapes[2]->offset->x = 0;
	entity_shapes[2]->offset->y = 32;


	entity__init(&entity_data);

	const char* entity_id =  "kim";
	entity_data.entity_id = (char*)wf_malloc((strlen(entity_id) + 1) * sizeof(char));
	strcpy(entity_data.entity_id,entity_id);

	entity_data.script_module = NULL;

	entity_data.script_ctor = NULL;

	const char* start_animation = "kick";
	char tmp_anim[256];
	entity_data.start_animation = &tmp_anim[0];
	strcpy(entity_data.start_animation,start_animation);

	const char* tileset = "kim.png";
	char tmp_tileset[256];
	entity_data.tileset = &tmp_tileset[0];
	strcpy(entity_data.tileset,tileset);

	const char* animation_file = "kim.awf";
	char tmp_anim2[256];
	entity_data.animation_file = &tmp_anim2[0];
	strcpy(entity_data.animation_file,animation_file);

	entity_data.position = &entity_pos;
	wvector__init(entity_data.position);

	entity_pos.x = 200;
	entity_pos.y = 100;


	
	entity_data.width = 64;
	entity_data.height = 96;
	entity_data.shape_type = TYPE_PHYSICS__MULTI_SHAPES;
	entity_data.type = 1;
	entity_data.has_controller = (protobuf_c_boolean)1;
	entity_data.no_collisions = (protobuf_c_boolean)0;
	entity_data.has_callback = (protobuf_c_boolean)0;
	entity_data.z_order = 1;
	entity_data.custom_collisions = (protobuf_c_boolean)0;
	entity_data.offsetx = 0;
	entity_data.offsety = 0;
	entity_data.col_height = 96;
	entity_data.col_width = 64;
	entity_data.repeat_x = 0;
	entity_data.repeat_y = 0;
	entity_data.start_frame = 0;
	entity_data.parallax_x = 1.0f;
	entity_data.parallax_y = 1.0f;

	entity_data.custom_shapes_def = entity_shapes;
	entity_data.n_custom_shapes_def = 3;


	texture_info* npc_tex = gettexture_resx(&game_state->resx_mngr,entity_data.tileset);

	AnimationList* anim_list = getanimation_resx(&game_state->resx_mngr,entity_data.animation_file);

	if(anim_list == NULL)
		logprint("Can't load animation file %s, entity %s won't be animated!",entity_data.animation_file,entity_id);



	avatar_phy.has_script = wfalse;


	entity_init(&avatar_phy,&entity_data,npc_tex,&game_state->phy_mngr,anim_list,&game_state->render_mngr,wfalse);



	avatar_phy.entity_id = entity_data.entity_id;

	entity_playanimation(&avatar_phy,entity_data.start_animation,0);

	entity_addcontactbody(&avatar_phy,10,10,"poing1",&game_state->phy_mngr,0,LAYER_INACTIVE,0);
	entity_addcontactbody(&avatar_phy,10,10,"poing2",&game_state->phy_mngr,0,LAYER_INACTIVE, 0);
	entity_addcontactbody(&avatar_phy,10,10,"pied1",&game_state->phy_mngr,0, LAYER_INACTIVE, 0);
	entity_addcontactbody(&avatar_phy,10,10,"pied2",&game_state->phy_mngr,0, LAYER_INACTIVE, 0);

	ChipmunkDebugDrawInit(&game_state->render_mngr);

	game_state->gameloop_func(game_state);


	return  1;
}