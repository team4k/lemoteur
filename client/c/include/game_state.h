struct gamestate_t;

typedef int (*gameloop_t)(struct gamestate_t* const gamestate);
typedef int (*demo_draw_func_t)(struct gamestate_t* const game_state);
typedef int (*demo_update_func_t)(struct gamestate_t* const game_state,float elapsed);

//input callbacks


typedef struct gamestate_t
{
	gameloop_t gameloop_func;
	demo_draw_func_t demo_draw_func;
	demo_update_func_t demo_update_func;

	lua_State* lua_state;
	config_t config;
	resources_manager_t resx_mngr;
	render_manager_t render_mngr;

	physics_manager_t phy_mngr;
	snd_mngr_t sound_mngr;
	input_mngr_t input_mngr;

	module_mngr_t module_mngr;

} gamestate_t;

//default module func

wbool base_disable_input();
void base_game_target(float targetX,float targetY,wbool pointer_control,int button_id);

void base_game_end_target(float targetX,float targetY,wbool pointer_control,int button_id);
void base_setcursor_pos(float x,float y);
void base_move_target(float targetX,float targetY,wbool pointer_control);
void base_offset_target(float offsetX,float offsetY);
void base_game_input(int16_t input_event,wbool pressed,wbool from_keyboard);
void base_game_char_input(uint32_t codepoint);
void base_joystick_axe(int16_t joystick_id,int16_t axe,float axe_value);
void base_joystick_button(int16_t joystick_id,int16_t button,wbool pressed);
void base_change_input_mode(const char* new_mode);


void get_world_coords(Vector_t* world_vector, int x, int y);
int gameloop(gamestate_t* const gamestate);