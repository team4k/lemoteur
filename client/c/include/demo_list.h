int rectangle_demo(gamestate_t* const game_state);
int tilemap_demo(gamestate_t* const game_state);
int animation_demo(gamestate_t* const game_state);
int chipmunk_demo(gamestate_t* const game_state);
int shatter_demo(gamestate_t* const game_state);
int sound_demo(gamestate_t* const game_state);
int font_demo(gamestate_t* const game_state);
int screen_effect_demo(gamestate_t* const game_state);
int custom_vertices_demo(gamestate_t* const game_state);
int shadow_demo(gamestate_t* const game_state);
int input_demo(gamestate_t* const game_state);
int url_demo(gamestate_t* const game_state);
int normal_map_demo(gamestate_t* const game_state);
int fireball_demo(gamestate_t* const game_state);
int save_mngr_demo(gamestate_t* const game_state);
int pathfind_demo(gamestate_t* const game_state);
int test_grid_walk_demo(gamestate_t* const game_state);
int mesh_demo(gamestate_t* const game_state);
int particle_demo(gamestate_t* const game_state);
int level_stream_demo(gamestate_t* const game_state);
int multi_sprite_entity_demo(gamestate_t* const game_state);
int sprite_bezier_curve_demo(gamestate_t* const game_state);


