"use strict";
/** @suppress {duplicate}*/var NetworkMsg;
if (typeof(NetworkMsg)=="undefined") {NetworkMsg = {};}

NetworkMsg.Avatar = PROTO.Message("NetworkMsg.Avatar",{
	id: {
		options: {},
		multiplicity: PROTO.required,
		type: function(){return PROTO.string;},
		id: 1
	},
	x: {
		options: {},
		multiplicity: PROTO.optional,
		type: function(){return PROTO.Float;},
		id: 2
	},
	y: {
		options: {},
		multiplicity: PROTO.optional,
		type: function(){return PROTO.Float;},
		id: 3
	},
	clickx: {
		options: {},
		multiplicity: PROTO.optional,
		type: function(){return PROTO.Float;},
		id: 4
	},
	clicky: {
		options: {},
		multiplicity: PROTO.optional,
		type: function(){return PROTO.Float;},
		id: 5
	},
	mouseDown: {
		options: {},
		multiplicity: PROTO.optional,
		type: function(){return PROTO.bool;},
		id: 6
	}});
NetworkMsg.WorldState = PROTO.Message("NetworkMsg.WorldState",{
	player_array: {
		options: {},
		multiplicity: PROTO.repeated,
		type: function(){return NetworkMsg.Avatar;},
		id: 1
	}});
NetworkMsg.DiscState = PROTO.Message("NetworkMsg.DiscState",{
	disc_guid: {
		options: {},
		multiplicity: PROTO.repeated,
		type: function(){return PROTO.string;},
		id: 1
	}});
