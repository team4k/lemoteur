function Vector2(x,y)
{
	this.x = x || 0;
	this.y = y || 0;
}


Vector2.prototype.add = function(vadd)
{
	this.x += vadd.x;
	this.y += vadd.y;
}

Vector2.prototype.sub = function(vsub)
{
	this.x -= vsub.x;
	this.y -= vsub.y;
}


Vector2.prototype.mul = function(vmul)
{
	if(typeof(vmul) == "number")
		return new Vector2(this.x * vmul,this.y * vmul);
	else if(vmul instanceof Vector2)
		return new Vector2(this.x * vmul.x,this.y * vmul.y);
}

Vector2.prototype.multhis = function(vmul)
{
	if(typeof(vmul) == "number")
	{
		this.x *= vmul;
		this.y *= vmul;
	}
	else if(vmul instanceof Vector2)
	{
		this.x *= vmul.x;
		this.y *= vmul.y;
	}
}

Vector2.prototype.div = function(vdiv)
{
	if(typeof(vmul) == "number")
		return new Vector2(this.x / vdiv,this.y / vdiv);
	else if(vmul instanceof Vector2)
		return new Vector2(this.x / vdiv.x,this.y / vdiv.y);
}

Vector2.prototype.divthis = function(vdiv)
{
	if(typeof(vmul) == "number")
	{
		this.x /= vmul;
		this.y /= vmul;
	}
	else if(vmul instanceof Vector2)
	{
		this.x /= vmul.x;
		this.y /= vmul.y;
	}
}

Vector2.prototype.isequal = function(v)
{
	return (v.x == this.x && v.y == this.y);
}



Vector2.prototype.plus = function(vplus)
{
	return new Vector2(this.x + vplus.x,this.y + vplus.y);
}

Vector2.prototype.minus = function(vminus)
{
	return new Vector2(this.x - vminus.x,this.y - vminus.y)
}

Vector2.prototype.length = function()
{
	return Math.sqrt(this.x * this.x + this.y * this.y);
}

Vector2.prototype.lengthsquared = function()
{
	return this.x * this.x + this.y * this.y;
}

Vector2.prototype.normalise = function()
{
	var len = this.length();
	
	var fInvLength = 1.0  / len;
	
	this.multhis(fInvLength);
	
	return len;
}

Vector2.prototype.dot = function(vec)
{
	return (this.x * vec.x + this.y * vec.y);
}

Vector2.prototype.copyTo = function(vec)
{
	vec.x = this.x;
	vec.y = this.y;
}
