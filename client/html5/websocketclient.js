function Shape(x,y,w,h,fill,id) {
	this.position = new Vector2(x,y);
	this.w = w || 1;
	this.h = h ||1;
	this.destination = new Vector2(x,y);
	this.olddestination = new Vector2(x,y);
	this.dist = 0;
	this.direction = new Vector2(0,0);
	this.fill = fill || "#AAAAAA";
	this.id = id;
}

Shape.prototype.draw = function(ctx)
{
	ctx.fillStyle = this.fill;
	ctx.fillRect(this.position.x,this.position.y,this.w,this.h);
}

var myState = null;

function CanvasState(canvas)
{
	this.canvas = canvas;
	this.width = canvas.width
	this.height = canvas.height
	this.ctx = canvas.getContext('2d')
	
	var stylePaddingLeft, stylePaddingTop, styleBorderLeft, styleBorderTop;
	
	if(document.defaultView && document.defaultView.getComputedStyle)
	{
		this.stylePaddingLeft = parseInt(document.defaultView.getComputedStyle(canvas, null)["paddingLeft"],10) || 0;
		this.stylePaddingTop = parseInt(document.defaultView.getComputedStyle(canvas, null)["paddingTop"],10) || 0;
		this.styleBorderLeft = parseInt(document.defaultView.getComputedStyle(canvas, null)["borderLeftWidth"],10) || 0;
		this.styleBorderTop = parseInt(document.defaultView.getComputedStyle(canvas, null)["borderTopWidth"],10) || 0;
	}
	

	var html = document.body.parentNode
	this.htmlTop = html.offsetTop
	this.htmlLeft = html.offsetLeft
	
	this.valid = false;
	this.shapes = [];
	
	myState = this;
	
	
	canvas.addEventListener("mousedown", function(e) {
		if(Object.size(myState.shapes) > 0)
		{
			var mouse = myState.getMouse(e);
			
			var mx = mouse.x;
			var my = mouse.y;
			//move our shape to the clicked position
			var shape = myState.shapes[player.id];
			
			shape.destination.x = mx - (shape.w / 2);
			shape.destination.y = my - (shape.h / 2);
		}
	});
}

var lastTime = 0;

var speed = 200;

function tick() 
{
	var timeNow = new Date().getTime();
	var elapsed = timeNow - lastTime;
	
	if(Object.size(myState.shapes) == 0)
	{
		requestAnimFrame(tick);
		lastTime = timeNow;
		return;
	}
	
	var pShape = myState.shapes[player.id];
	
	moveShape(pShape,true, elapsed);
	
	//move the other player based on value sended
	for(var i = 0; i < extplayers.length;i++)
	{
		//get the shape
		var extShape = myState.shapes[extplayers[i].id];
		
		//update shape values base on ext player value
		if(extShape.destination.x != extplayers[i].clickx || extShape.destination.y != extplayers[i].clicky)
		{
			extShape.destination.x = extplayers[i].clickx;
			extShape.destination.y = extplayers[i].clicky;
			
			//snap back to the position at the time of the click
			//TODO : smooth the snap back movement
			//extShape.x =  extplayers[i].x;
			//extShape.y = extplayers[i].y;
		}
		
		//then move the shape
		moveShape(extShape,false, elapsed);
	}
	
	//check new players
	if(spawnedplayers.length > 0)
	{
		for(var i = 0; i < spawnedplayers.length; i++)
		{
			s.addShape(new Shape(spawnedplayers[i].x,spawnedplayers[i].y,50,50, 'red',spawnedplayers[i].id));
			extplayers.push(spawnedplayers[i]);
		}
		
		spawnedplayers = [];
	}
	
	//check removed players
	if(removedplayers.length > 0)
	{
		
		for(var i = 0; i < removedplayers.length; i++)
		{
			var rmindex = -1;
			
			for(var j =0; j < extplayers.length; j++)
			{
				if(removedplayers[i] == extplayers[j].id)
				{
					s.removeShape(removedplayers[i]);
					rmindex = j;
					break;
				}
			}
			
			extplayers.splice(rmindex, 1);//remove the player
		}
		
		removedplayers = [];
	}
	
    requestAnimFrame(tick);
	myState.draw();
	
	
	lastTime = timeNow;
}

function moveShape(pShape,isClient,elapsed)
{
	if(!pShape.destination.isequal(pShape.position))
	{
		//call redraw
		myState.valid = false;
		
		//move the shape to the selected point
		if(pShape.dist == 0 || !pShape.destination.isequal(pShape.olddestination))
		{
			pShape.direction = pShape.position.minus(pShape.destination);
			pShape.dist = pShape.direction.normalise();
			pShape.destination.copyTo(pShape.olddestination);
			
			if(isClient)
			{
				//send to server our new position
				player.clickx = pShape.destination.x;
				player.clicky = pShape.destination.y;
				
				//get base64 value
				var serialized = new PROTO.Base64Stream;
				player.SerializeToStream(serialized);
				socket.send(USER_CLICK + serialized.getString());
			}
		}
		
		var mov = 0;
		
		if(pShape.dist > 0)
		{
			mov = speed * (elapsed * 0.001);
			var moveVec = pShape.direction.mul(mov);
			pShape.position.sub(moveVec);
			pShape.dist -= mov;
		}
	

		if(pShape.dist <= 0)
		{
			pShape.dist = 0;
			pShape.destination.copyTo(pShape.position);
		}
	}
}


CanvasState.prototype.getMouse = function(e)
{
	var element = this.canvas, offsetX = 0, offsetY = 0, mx,  my;
	
	if(element.offsetParent !== undefined)
	{
		do
		{
			offsetX += element.offsetLeft;
			offsetY += element.offsetTop;
		}
		while((element = element.offsetParent));
	}
	
	offsetX += this.stylePaddingLeft + this.styleBorderLeft + this.htmlLeft;
	offsetY += this.stylePaddingTop + this.styleBorderTop + this.htmlTop;
	
	mx = e.pageX - offsetX;
	my = e.pageY - offsetY;
	
	return {x: mx, y: my};
}

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
}

CanvasState.prototype.addShape = function(shape) 
{
	this.shapes[shape.id] = shape;
	this.valid = false
}

CanvasState.prototype.removeShape = function(id) 
{
	this.shapes[id] = null;
	
	this.valid = false
}

CanvasState.prototype.draw = function()
{
	if(!this.valid)
	{
		var ctx = this.ctx;
		var shapes = this.shapes;
		this.clear();
		
		
		//draw shapes
		var l = shapes.length
		
		for(var shapeindex in shapes) 
		{	
			var shape = shapes[shapeindex];
			
			if(shape.x > this.width || shape.y > this.height || shape.x + shape.w < 0 || shape.y + shape.h < 0)
				continue;
			
			shape.draw(ctx)
		}
		
		this.valid = true
	}
}

CanvasState.prototype.clear = function(){
	this.ctx.clearRect(0,0,this.width, this.height);
}

var s = null;
function init() {
	s = new CanvasState(document.getElementById("canvaswf"));
	tick();
}




//***web socket

var extplayers = [];
var spawnedplayers = [];
var removedplayers = [];
var player = null;

//constant
var USER_JOIN = 1;
var USER_CLICK = 2
var USER_CHAT = 3;
var USER_LEFT = 4;
var WORLD_STATE = 5;
var SERVER_MOTD = 6;
var SEND_GUID = 7;


var socket = new WebSocket("ws://127.0.0.1:33334/websocket");

socket.onopen = function(e) 
{
	//alert("open");
}

socket.onmessage = function(e)
{

	//the fist char is the command
	var cmd = parseInt(e.data[0]);
	
	//convert binary data to protobuf object
	
	if(cmd == SEND_GUID)
	{
		var str = e.data.substr(1,e.data.length-1);
		var avatar = new NetworkMsg.Avatar;
		avatar.ParseFromStream(new PROTO.Base64Stream(str));
		player = avatar;
		s.addShape(new Shape(player.x,player.y,50,50, 'lightskyblue',player.id));
	}
	else if(cmd == WORLD_STATE)
	{
		if(player != null) // don't parse world state message if we haven't a player object
		{
			//get avatar list
			var worldState = new NetworkMsg.WorldState;
			var str = e.data.substr(1,e.data.length-1);
			worldState.ParseFromStream(new PROTO.Base64Stream(str));
			
			//check for spawned clients only if we have an empty spawned client array (to avoid double)
			if(spawnedplayers.length == 0)
			{
				for(var i = 0; i < worldState.player_array.length; i++)
				{
					if(worldState.player_array[i].id == player.id)
						continue;
						
					var exist = false
					
					for(var j = 0; j < extplayers.length; j++)
					{
						if(extplayers[j].id == worldState.player_array[i].id)
						{
							exist = true;
							break;
						}
					}
					
					if(!exist)
						spawnedplayers.push(worldState.player_array[i])
				}
			}
			
			if(removedplayers.length == 0)
			{
				//now check for disconnected clients
				for(var i = 0; i < extplayers.length; i++)
				{
					
					var exist = false;
					
					
					for(var j = 0; j < worldState.player_array.length; j++)
					{
						if(extplayers[i].id == worldState.player_array[j].id)
						{
							exist = true;
							break;
						}
					}
					
					if(!exist)
						removedplayers.push(extplayers[i].id)
				}
			}
			
			//finally, update the remaining clients
			for(var i = 0; i < extplayers.length; i++)
			{
				for(var j = 0; j < worldState.player_array.length; j++)
				{
					if(extplayers[i].id == worldState.player_array[j].id && removedplayers.indexOf(extplayers[i].id) == -1)
					{
						extplayers[i] = worldState.player_array[j];
						break;
					}
				}
			}
		}
		
	}

}

socket.onclose = function(e)
{
	alert("close");
}

socket.onerror = function(e)
{
	alert("error");
}