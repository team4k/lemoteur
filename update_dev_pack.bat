@ECHO --------------------------------
@ECHO Compilation animation converter
@ECHO --------------------------------

@MSBuild editor/AnimationConverter/AnimationConverter.sln /t:Rebuild /p:Configuration=Release

@ECHO --------------------------
@ECHO Copie animation converter
@ECHO --------------------------
@copy /Y editor\AnimationConverter\bin\Release\AnimationConverter.exe dev_pack\animation_converter\AnimationConverter.exe
@copy /Y editor\AnimationConverter\bin\Release\galefile.dll dev_pack\animation_converter\galefile.dll
@copy /Y editor\AnimationConverter\bin\Release\protobuf-net.dll dev_pack\animation_converter\protobuf-net.dll

@ECHO -------------------------
@ECHO Copie include dependances
@ECHO -------------------------


@rmdir /S /Q dev_pack\dep_includes
@mkdir dev_pack\dep_includes

@ECHO Chipmunk-6.2.1

@mkdir dev_pack\dep_includes\Chipmunk-6.2.1
@xcopy %COMMON_LIBS_ROOT%\Chipmunk-6.2.1\include dev_pack\dep_includes\Chipmunk-6.2.1\include /S /E /I

@ECHO Chipmunk-6.2.2

@mkdir dev_pack\dep_includes\Chipmunk-6.2.2
@xcopy %COMMON_LIBS_ROOT%\Chipmunk-6.2.2\include dev_pack\dep_includes\Chipmunk-6.2.2\include /S /E /I

@ECHO glew-1.9.0

@mkdir dev_pack\dep_includes\glew-1.9.0
@xcopy %COMMON_LIBS_ROOT%\glew-1.9.0\include dev_pack\dep_includes\glew-1.9.0\include /S /E /I

@ECHO glfw-3.1

@mkdir dev_pack\dep_includes\glfw-3.1
@xcopy %COMMON_LIBS_ROOT%\glfw_dev\include dev_pack\dep_includes\glfw-3.1\include /S /E /I

@ECHO libpng162

@mkdir dev_pack\dep_includes\libpng162

@ECHO OFF
@for %%x in (%COMMON_LIBS_ROOT%\libpng162\*.h) do (
	@copy /Y %%x dev_pack\dep_includes\libpng162
)
@ECHO ON

@ECHO libsndfile

@mkdir dev_pack\dep_includes\libsndfile
@mkdir dev_pack\dep_includes\libsndfile\include
@copy /Y %COMMON_LIBS_ROOT%\libsndfile\libsndfile-1.0.25\Win32\sndfile.h dev_pack\dep_includes\libsndfile\include\sndfile.h

@ECHO lua-5.3.1

@mkdir dev_pack\dep_includes\lua-5.3.1

@xcopy %COMMON_LIBS_ROOT%\lua-5.3.1\include dev_pack\dep_includes\lua-5.3.1\include /S /E /I

@ECHO portaudio

@mkdir dev_pack\dep_includes\portaudio
@xcopy %COMMON_LIBS_ROOT%\portaudio\include dev_pack\dep_includes\portaudio\include /S /E /I

@ECHO sqlite

@mkdir dev_pack\dep_includes\sqlite
@xcopy %COMMON_LIBS_ROOT%\sqlite\include dev_pack\dep_includes\sqlite\include /S /E /I

@ECHO -------------------------
@ECHO Copie includes
@ECHO -------------------------

@rmdir /S /Q dev_pack\include
@mkdir dev_pack\include
@xcopy engine\c\include dev_pack\include /S /E /I

@rmdir /S /Q dev_pack\include-msvc
@mkdir dev_pack\include-msvc
@xcopy engine\c\include-msvc dev_pack\include-msvc /S /E /I

@ECHO ------------------------------------
@ECHO compilation WafaEngine (emscripten)
@ECHO ------------------------------------

@MSBuild engine/c/WafaEngine.sln /p:Configuration=Release /p:Platform=Emscripten

@ECHO -------------------------------
@ECHO Copie libs emscripten (release)
@ECHO -------------------------------

@rmdir /S /Q dev_pack\libs-emscripten
@mkdir dev_pack\libs-emscripten
@xcopy engine\c\lib-emscripten dev_pack\libs-emscripten /S /E /I
@copy /Y engine\c\Emscripten\Release\WafaEngine.bc dev_pack\libs-emscripten\WafaEngine.bc

@ECHO ------------------------------------
@ECHO compilation WafaEngine (windows debug)
@ECHO ------------------------------------

@MSBuild engine/c/WafaEngine.sln /p:Configuration=Debug /p:Platform=Win32

@ECHO --------------------------
@ECHO Copie libs windows (debug)
@ECHO --------------------------

@rmdir /S /Q dev_pack\libs-msvc100-debug
@mkdir dev_pack\libs-msvc100-debug

@copy /Y engine\c\lib-msvc100\glew32.lib dev_pack\libs-msvc100-debug\glew32.lib
@copy /Y engine\c\lib-msvc100\glfw3dll.lib dev_pack\libs-msvc100-debug\glfw3dll.lib
@copy /Y engine\c\bin\Debug\WafaEngine.lib dev_pack\libs-msvc100-debug\WafaEngine.lib
@copy /Y engine\c\bin\Debug\WafaEngine.exp dev_pack\libs-msvc100-debug\WafaEngine.exp
@copy /Y engine\c\bin\Debug\WafaEngine.pdb dev_pack\libs-msvc100-debug\WafaEngine.pdb
@copy /Y engine\c\bin\Debug\WafaEngine.dll dev_pack\libs-msvc100-debug\WafaEngine.dll

@copy /Y engine\c\dependencies\glew32.dll dev_pack\libs-msvc100-debug\glew32.dll
@copy /Y engine\c\dependencies\glfw3.dll dev_pack\libs-msvc100-debug\glfw3.dll
@copy /Y engine\c\dependencies\sqlite3.dll dev_pack\libs-msvc100-debug\sqlite3.dll
@copy /Y engine\c\dependencies\libsndfile-1.dll dev_pack\libs-msvc100-debug\libsndfile-1.dll

@ECHO ------------------------------------
@ECHO compilation WafaEngine (windows release)
@ECHO ------------------------------------

@MSBuild engine/c/WafaEngine.sln /p:Configuration=Release /p:Platform=Win32

@ECHO ----------------------------
@ECHO Copie libs windows (release)
@ECHO ----------------------------

@rmdir /S /Q dev_pack\libs-msvc100-release
@mkdir dev_pack\libs-msvc100-release

@copy /Y engine\c\lib-release-msvc100\glew32.lib dev_pack\libs-msvc100-release\glew32.lib
@copy /Y engine\c\lib-release-msvc100\glfw3dll.lib dev_pack\libs-msvc100-release\glfw3dll.lib
@copy /Y engine\c\bin\Release\WafaEngine.lib dev_pack\libs-msvc100-release\WafaEngine.lib
@copy /Y engine\c\bin\Release\WafaEngine.exp dev_pack\libs-msvc100-release\WafaEngine.exp
@copy /Y engine\c\bin\Release\WafaEngine.pdb dev_pack\libs-msvc100-release\WafaEngine.pdb
@copy /Y engine\c\bin\Release\WafaEngine.dll dev_pack\libs-msvc100-release\WafaEngine.dll
@copy /Y engine\c\dependencies\msvcp100.dll dev_pack\libs-msvc100-release\msvcp100.dll
@copy /Y engine\c\dependencies\msvcr100.dll dev_pack\libs-msvc100-release\msvcr100.dll


@copy /Y engine\c\dependencies\glew32.dll dev_pack\libs-msvc100-release\glew32.dll
@copy /Y engine\c\dependencies\glfw3.dll dev_pack\libs-msvc100-release\glfw3.dll
@copy /Y engine\c\dependencies\sqlite3.dll dev_pack\libs-msvc100-release\sqlite3.dll
@copy /Y engine\c\dependencies\libsndfile-1.dll dev_pack\libs-msvc100-release\libsndfile-1.dll


@ECHO --------------------------------
@ECHO compilation WafaEngine (Android)
@ECHO --------------------------------
@cmd.exe /c %COMMON_LIBS_ROOT%\android-ndk\ndk-build.cmd -B V=1 -C engine\c

@ECHO ----------------------------
@ECHO Copie libs Android
@ECHO ----------------------------

@rmdir /S /Q dev_pack\libs-android
@mkdir dev_pack\libs-android

@copy /Y engine\c\libs\armeabi-v7a\libwafaengine.so dev_pack\libs-android\libwafaengine.so
@copy /Y engine\c\libs\armeabi-v7a\libcurl.so dev_pack\libs-android\libcurl.so
@copy /Y engine\c\libs\armeabi-v7a\libsndfile.so dev_pack\libs-android\libsndfile.so

@ECHO --------------------
@ECHO compilation editeur
@ECHO --------------------

@MSBuild editor/WafaEditorV2/WafaEditor.sln /p:Configuration=Release /p:Platform=Win32

@ECHO --------------------
@ECHO copie editeur
@ECHO --------------------

@copy /Y editor\WafaEditorV2\bin\Release\WafaEditorV2.exe dev_pack\wafaeditor_v2\WafaEditorV2.exe
@copy /Y editor\WafaEditorV2\bin\Release\config.lua dev_pack\wafaeditor_v2\config.lua
@copy /Y editor\WafaEditorV2\bin\Release\galefile.dll dev_pack\wafaeditor_v2\galefile.dll
@copy /Y editor\WafaEditorV2\bin\Release\glew32.dll dev_pack\wafaeditor_v2\glew32.dll 
@copy /Y editor\WafaEditorV2\bin\Release\glfw3.dll dev_pack\wafaeditor_v2\glfw3.dll 
@copy /Y editor\WafaEditorV2\bin\Release\libsndfile-1.dll dev_pack\wafaeditor_v2\libsndfile-1.dll
@copy /Y editor\WafaEditorV2\bin\Release\locale.db dev_pack\wafaeditor_v2\locale.db
@copy /Y editor\WafaEditorV2\bin\Release\save.db dev_pack\wafaeditor_v2\save.db
@copy /Y editor\WafaEditorV2\bin\Release\protobuf-net.dll dev_pack\wafaeditor_v2\protobuf-net.dll
@copy /Y editor\WafaEditorV2\bin\Release\sqlite3.dll dev_pack\wafaeditor_v2\sqlite3.dll
@copy /Y editor\WafaEditorV2\bin\Release\WafaEditorV2.exe.config dev_pack\wafaeditor_v2\WafaEditorV2.exe.config
@copy /Y editor\WafaEditorV2\bin\Release\WafaEngine.dll dev_pack\wafaeditor_v2\WafaEngine.dll
@copy /Y editor\WafaEditorV2\bin\Release\ICSharpCode.SharpZipLib.dll dev_pack\wafaeditor_v2\ICSharpCode.SharpZipLib.dll
@copy /Y editor\WafaEditorV2\bin\Release\SpriteSheetLibrary.dll dev_pack\wafaeditor_v2\SpriteSheetLibrary.dll

@ECHO ------------
@ECHO maj template
@ECHO ------------

@copy /Y client\c\WafaDummyGame\Gamemain.c dev_pack\game_template\dummygame\Gamemain.c
@copy /Y client\c\WafaDummyGame\Gamemain.h dev_pack\game_template\dummygame\Gamemain.h
@copy /Y client\c\WafaDummyGame\game_struct.h dev_pack\game_template\dummygame\game_struct.h
@copy /Y client\c\WafaDummyGame\script_funcs.c dev_pack\game_template\dummygame\script_funcs.c
@copy /Y client\c\WafaDummyGame\script_funcs.h dev_pack\game_template\dummygame\script_funcs.h
@copy /Y client\c\WafaDummyGame\common_include_game.h dev_pack\game_template\dummygame\common_include_game.h
@copy /Y client\c\WafaDummyGame\jni\Android.mk dev_pack\game_template\dummygame\jni\Android.mk
@copy /Y client\c\WafaDummyGame\jni\Application.mk dev_pack\game_template\dummygame\jni\Application.mk

@ECHO --------------------
@ECHO verif game template
@ECHO --------------------

@MSBuild dev_pack/game_template/game_template.sln /p:Configuration=Release /p:Platform=Win32 /p:OutDir=bin\tmp\
@MSBuild dev_pack/game_template/game_template.sln /p:Configuration=Release /p:Platform=Emscripten

@ECHO ------------------------------------
@ECHO compilation worker emscripten
@ECHO ------------------------------------

@cd engine\c

@call emcc_build_worker_release.bat

@cd ..\..

@ECHO --------------------
@ECHO copie documentation
@ECHO --------------------
@copy  /Y api_lua.txt dev_pack\doc_wafa\api_lua.txt
@copy  /Y baselib.lua dev_pack\doc_wafa\baselib.lua
@copy  /Y config_doc.txt dev_pack\doc_wafa\config_doc.txt
@copy  /Y doc.txt dev_pack\doc_wafa\doc.txt
@copy  /Y lua_entitycreation_doc.txt dev_pack\doc_wafa\lua_entitycreation_doc.txt
@copy  /Y shader_macro.txt dev_pack\doc_wafa\shader_macro.txt
@copy  /Y lights_values_doc.txt dev_pack\doc_wafa\lights_values_doc.txt

@ECHO -------------------------------
@ECHO suppression fichiers temporaire
@ECHO -------------------------------

@rmdir /S /Q dev_pack\game_template\bin\tmp
@rmdir /S /Q dev_pack\game_template\Emscripten
@rmdir /S /Q dev_pack\game_template\dummygame\bin
@rmdir /S /Q dev_pack\game_template\dummygame\Emscripten
@rmdir /S /Q dev_pack\game_template\dummygame\Release
@rmdir /S /Q dev_pack\game_template\Release


@ECHO -------------------------------
@ECHO zip dev pack
@ECHO -------------------------------

@DEL /Q dev_pack.zip

@REM Need powershell version 3 with .net framework 4.5, see http://www.microsoft.com/en-us/download/details.aspx?id=34595
@REM powershell.exe -nologo -noprofile -command "& {Add-Type -A 'System.IO.Compression.FileSystem'; [IO.Compression.ZipFile]::CreateFromDirectory('dev_pack','dev_pack.zip'); }"
@easyzip.exe dev_pack dev_pack.zip

@pause
