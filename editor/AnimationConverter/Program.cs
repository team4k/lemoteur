﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WafaEditorV2;
using PBInterface;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using System.Threading;

namespace AnimationConverter
{

    /// <summary>
    /// convert .gal files with animation to the format used by wafaengine
    /// </summary>
    class Program
    {

        //private const int TILE_SIZE = 32;
        private const int MAX_SIZE_PER_LINE = 512;//10
        private const int MAX_FRAME_PER_LINE = 16;
        private const int TILE_SPACE = 0;
        private static bool skip_png = false;


        /// <summary>
        /// first argument: directory containing animated assets
        /// second argument: directory containing static assets
        /// third argument: assets output directory 
        /// fourth argument: scripts output directory
        /// fifth argument: txt file containing filename to exclude of export process
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
           /* args = new string[5];

            args[0] = @"D:\devel\gamedevel\wastedfantasy\client\c\bin\raw_assets\animated_assets";
            args[1] = @"D:\devel\gamedevel\wastedfantasy\client\c\bin\raw_assets";
            args[2] = @"D:\devel\gamedevel\wastedfantasy\client\c\bin\Debug\assets";
            args[3] = @"D:\devel\gamedevel\wastedfantasy\client\c\bin\Debug\scripts";
            args[4] = @"D:\devel\gamedevel\wastedfantasy\client\c\bin\raw_assets\exclude_pc.txt";*/

             /*args = new string[5];

             args[0] = @"D:\devel\gamedevel\esprit_donjon\assets\animated_assets";
             args[1] = @"D:\devel\gamedevel\esprit_donjon\assets";
             args[2] = @"D:\devel\gamedevel\esprit_donjon\bin\assets";
             args[3] = @"D:\devel\gamedevel\esprit_donjon\bin\scripts";
             args[4] = @"D:\devel\gamedevel\esprit_donjon\assets\exclude_pc.txt";*/

            /*args = new string[5];

            args[0] = @"C:\Users\venice\Desktop\dev_pack\game_template\assets\animated_assets";
            args[1] = @"C:\Users\venice\Desktop\dev_pack\game_template\assets";
            args[2] = @"C:\Users\venice\Desktop\dev_pack\game_template\bin\assets";
            args[3] = @"C:\Users\venice\Desktop\dev_pack\game_template\bin\scripts";
            args[4] = @"C:\Users\venice\Desktop\dev_pack\game_template\assets\exclude_pc.txt";*/


            if (args.Length < 4)
            {
                Console.WriteLine("Invalid number of arguments, expected at least 4, provided : " + args.Length);
                return;
            }

            if (!Directory.Exists(args[0]))
            {
                Console.WriteLine("The animated assets input directory provided doesn't exist ! {0}", args[0]);
                return;
            }

            if (!Directory.Exists(args[1]))
            {
                Console.WriteLine("The static assets input directory provided doesn't exist ! {0}", args[1]);
                return;
            }

            if (!Directory.Exists(args[2]))
            {
                Console.WriteLine("The assets output directory provided doesn't exist ! {0}", args[2]);
                return;
            }

            if (!Directory.Exists(args[3]))
            {
                Console.WriteLine("The scripts output directory provided doesn't exist ! {0}", args[3]);
                return;
            }

            List<string> skip_list = new List<string>();

            if (args.Length > 4)
            {
                if(File.Exists(args[4]))
                {
                    TextReader exclude_config_file = new StreamReader(args[4]);

                    while (exclude_config_file.Peek() != -1)
                    {
                        string line = exclude_config_file.ReadLine();

                        if (line.StartsWith("--"))
                            continue;

                        if (line == "*.png")
                            skip_png = true;

                        skip_list.Add(line);
                    }
                }
                else
                {
                    Console.WriteLine("The exclude file provided is not valid! {0}", args[4]);
                }

            }



            string outputDir = args[2];
            string outputScriptDir = args[3];
            string inputDir = args[1];
            string inputanimatedDir = args[0];

            int pngCount = 0;
            int animationCount = 0;
            int animationFileCount = 0;

            int imgWidth = 0;
            int imgHeight = 0;

            //export static assets


            Dictionary<string, List<string>> group_static = new Dictionary<string, List<string>>();
            Dictionary<string, List<string>> export_config = new Dictionary<string, List<string>>();

            TextReader export_config_file = new StreamReader(Path.Combine(inputDir, "export_config.txt"));

             

            //regroup file using pack_config
            while (export_config_file.Peek() != -1)
            {
                string line = export_config_file.ReadLine();

                if (line.StartsWith("--"))
                    continue;

                string key = line.Substring(0, line.IndexOf('='));

                export_config.Add(key, new List<string>());

                string values = line.Substring(line.IndexOf('=') + 1);

                foreach (string val in values.Split(new char[] { ',' }))
                {
                    export_config[key].Add(val);
                }
            }

            foreach (string file in Directory.GetFiles(inputDir, "*.gal"))
            {
                if (skip_png)
                    break;

                  FileInfo info = new FileInfo(file);

                  if (skip_list.Contains(info.Name))
                      continue;

                string prefix = info.Name.Substring(0, info.Name.LastIndexOf('.'));
                group_static.Add(prefix, new List<string>());
                group_static[prefix].Add(file);
            }


          

            foreach (KeyValuePair<string, List<string>> group in group_static)
            {
                if (skip_png)
                    break;

                //récupération du contenu bitmap du fichier gal
                GaleReader gaReader = new GaleReader();

                string prefix = group.Key;

                List<Bitmap> bitmapList = new  List<Bitmap>(group.Value.Count);

                int totalTiles = 0;

                
                string fileName = group.Key;

                fileName += ".png";

                string destination_path = Path.Combine(outputDir, fileName);


                //check if asset need to be updated
                if (File.Exists(destination_path))
                {
                    DateTime MostRecentDate = DateTime.MinValue;
                    FileInfo destinationFile = new FileInfo(destination_path);

                    foreach (string file in group.Value)
                    {
                        FileInfo info = new FileInfo(file);

                        if (info.LastWriteTimeUtc > MostRecentDate)
                            MostRecentDate = info.LastWriteTimeUtc;
                    }

                    //if the destination file is more recent than the most recent file in our group, it's up to date
                    if (destinationFile.LastWriteTimeUtc >= MostRecentDate)
                    {
                        continue;
                    }
                }

              


               foreach (string file in group.Value)
               {

                   FileInfo info = new FileInfo(file);


                   try
                   {
                       gaReader.OpenFile(file);

                       imgWidth = gaReader.GetWidth();
                       imgHeight = gaReader.GetHeight();

                       int tilesPerRow = MAX_FRAME_PER_LINE;

                       if (export_config.ContainsKey(prefix))
                       {
                           if (export_config[prefix].Contains("one_line"))
                               tilesPerRow = -1;
                       }

                       Bitmap bmpPacked = gaReader.GetPackedContent(tilesPerRow, imgWidth, imgHeight,TILE_SPACE);

                       bitmapList.Add(bmpPacked);

                       int space_width =  (TILE_SPACE * tilesPerRow) + TILE_SPACE;

                       if(tilesPerRow == -1)
                           space_width = (bmpPacked.Width / (imgHeight + TILE_SPACE)) + TILE_SPACE;

                       int space_height = (bmpPacked.Height / (imgHeight + TILE_SPACE)) + TILE_SPACE;

                       if (bmpPacked.Height == imgHeight + (TILE_SPACE * 2))
                           space_height = TILE_SPACE * 2;

                       //if some offset exist, reduce the number of total tiles
                       totalTiles += (int)((bmpPacked.Width - space_width) * (bmpPacked.Height - space_height)) / (imgWidth * imgHeight);

                       

                   }
                   catch (Exception ex)
                   {
                       Console.Out.WriteLine(ex.ToString());
                   }
                   finally
                   {
                       if (gaReader.FileOpened)
                           gaReader.CloseFile();
                   }
               }

               //merge bitmap together

               int mergedWidth = 0;
               int mergedHeight = 0;

               if (totalTiles < MAX_FRAME_PER_LINE)
               {
                   mergedWidth = imgWidth * totalTiles;
                   mergedHeight = imgHeight;
               }
               else
               {
                   mergedWidth = imgWidth * MAX_FRAME_PER_LINE;
                   mergedHeight = ((int)Math.Ceiling((double)totalTiles / MAX_FRAME_PER_LINE)) * imgHeight;
               }

               Bitmap bmpToSave = null;





               if (bitmapList.Count > 1)
               {
                   //check if height is a power of two
                   if (export_config.ContainsKey(prefix) && export_config[prefix].Contains("power_of_2"))
                   {
                       if (!(mergedHeight != 0 && (mergedHeight & (mergedHeight - 1)) == 0))
                       {
                           mergedHeight--;
                           mergedHeight |= mergedHeight >> 1;
                           mergedHeight |= mergedHeight >> 2;
                           mergedHeight |= mergedHeight >> 4;
                           mergedHeight |= mergedHeight >> 8;
                           mergedHeight |= mergedHeight >> 16;
                           mergedHeight++;
                       }
                   }

                   bmpToSave = BitmapUtilities.MergeBitmaps(bitmapList.ToArray(), mergedWidth, mergedHeight, imgWidth, imgHeight);
               }
               else
               {
                   if (export_config.ContainsKey(prefix) && export_config[prefix].Contains("power_of_2"))
                   {

                       int bmpHeight = bitmapList[0].Height;

                       if (!(bmpHeight != 0 && (bmpHeight & (bmpHeight - 1)) == 0))
                       {
                           bmpHeight--;
                           bmpHeight |= bmpHeight >> 1;
                           bmpHeight |= bmpHeight >> 2;
                           bmpHeight |= bmpHeight >> 4;
                           bmpHeight |= bmpHeight >> 8;
                           bmpHeight |= bmpHeight >> 16;
                           bmpHeight++;


                           bmpToSave = new Bitmap(bitmapList[0].Width, bmpHeight);

                           Graphics gfx = Graphics.FromImage(bmpToSave);
                           gfx.DrawImage(bitmapList[0], 0, 0, bitmapList[0].Width, bitmapList[0].Height);
                           gfx.Dispose();
                       }
                       else
                       {
                           bmpToSave = bitmapList[0];
                       }
                   }
                   else
                   {
                       bmpToSave = bitmapList[0];
                   }

               }

               //write bitmap to disk
               using (var fileTileset = File.Create(destination_path))
               {
                   bmpToSave.Save(fileTileset, ImageFormat.Png);
                   pngCount++;
               }

               //duplicate if needed on vertical axe
               if (export_config.ContainsKey(prefix))
               {
                   if (export_config[prefix].Contains("duplicate_vertical"))
                   {
                       Bitmap bmpVertical = new Bitmap(bmpToSave.Height, bmpToSave.Width);
                       int xvert = 0;
                       int yvert = 0;
                       int basex = 0;
                       int basey = 0;

                       for (int cTile = 0; cTile < totalTiles; cTile++)
                       {
                           for (int x = basex; x < basex+imgWidth + TILE_SPACE; x++)
                           {
                               for (int y = basey; y < basey + imgHeight + TILE_SPACE; y++)
                               {
                                   bmpVertical.SetPixel(xvert, yvert++, bmpToSave.GetPixel(x, y));
                               }
                               xvert++;
                               yvert -= (imgHeight + TILE_SPACE);
                           }

                           if ((basex + imgWidth + TILE_SPACE) < bmpToSave.Width)
                               basex += imgWidth + TILE_SPACE;
                           else
                           {
                               basex = 0;
                               basey += imgHeight + TILE_SPACE;
                           }

                           xvert -= imgWidth + TILE_SPACE;
                           yvert += imgHeight + TILE_SPACE;

                       }

                       string fileName_duplicate = group.Key + "_duplicate.png";

                       string duplicate_destination_path = Path.Combine(outputDir, fileName_duplicate);

                        //write duplicated bitmap to disk
                        using (var fileTileset = File.Create(duplicate_destination_path))
                        {
                            bmpVertical.Save(fileTileset, ImageFormat.Png);
                            pngCount++;
                        }



                   }
               }
            }



            Dictionary<string, AnimationDataHolder> group_anim = new Dictionary<string, AnimationDataHolder>();


            //export animated assets

            TextReader config_file = new StreamReader(Path.Combine(inputanimatedDir, "pack_config.txt"));

            //regroup file using pack_config
            while (config_file.Peek() != -1)
            {
                string line = config_file.ReadLine();

                if (line.StartsWith("--"))
                    continue;

                string key = line.Substring(0, line.IndexOf('='));

                if (skip_list.Contains(key))
                    continue;

                group_anim.Add(key, new AnimationDataHolder());

                string values = line.Substring(line.IndexOf('=') +1);

                foreach(string val in values.Split(new char[]{','}))
                {
                    Avector sprite_offset = new Avector();
                    string file = val;
                    //get offset val for a animation
                    if (val.Contains('('))
                    {
                        int indx_offset = val.IndexOf('(')+1;
                        int end_indx_offset = val.IndexOf(')');
                        string offset_str = val.Substring(indx_offset, end_indx_offset - indx_offset);

                        string[] offset_vals = offset_str.Split(new char[] { ':' });

                        foreach (string vector_str in offset_vals)
                        {
                            if (vector_str[0] == 'x')
                                sprite_offset.X = int.Parse(vector_str.Substring(vector_str.IndexOf('=') + 1));

                            if (vector_str[0] == 'y')
                                sprite_offset.Y = int.Parse(vector_str.Substring(vector_str.IndexOf('=') + 1));
                        }

                        file = val.Substring(0, indx_offset - 1);
                    }

                    group_anim[key].file_list.Add(Path.Combine(inputanimatedDir, file));
                    group_anim[key].sprite_offset.Add(sprite_offset);
                }
            }

            //read animation config file
            TextReader anim_config_file = new StreamReader(Path.Combine(inputanimatedDir, "anim_config.txt"));

            while (anim_config_file.Peek() != -1)
            {
                string line = anim_config_file.ReadLine();

                if (line.StartsWith("--"))
                    continue;

                string key = line.Substring(0, line.IndexOf('='));

                if (skip_list.Contains(key))
                    continue;

                if (group_anim.ContainsKey(key))
                {
                    int start = line.IndexOf('[')+1;
                    string contact_points_data = line.Substring(start, line.LastIndexOf(']') - start);

                    string[] contact_points = contact_points_data.Split(new char[] { ',' });

                    foreach (string ctp in contact_points)
                    {
                        string contact_name = ctp.Substring(0,ctp.IndexOf('='));
                        string contact_value = ctp.Substring(ctp.IndexOf('=') + 1);

                        group_anim[key].contact_points.Add(Convert.ToInt32(contact_value,16), contact_name);
                    }
                }

            }

            

            foreach (KeyValuePair<string, AnimationDataHolder> group in group_anim)
            {
               // 
                if (skip_png)
                    break;

                AnimationList anim_list = new AnimationList();
                anim_list.Id = group.Key;

                GaleReader reader = new GaleReader();

                AnimationDef animDef;

               List<Bitmap> bitmapList = new  List<Bitmap>(group.Value.file_list.Count);

               int totalFrame = 0;

               string fileName = group.Key + ".png";

               string animationname = group.Key + ".awf";

               string destination_path = Path.Combine(outputDir, fileName);
               string animation_destination_path = Path.Combine(outputDir, animationname);

               //check if asset need to be updated
               if (File.Exists(destination_path) && File.Exists(animation_destination_path))
               {
                   DateTime MostRecentDate = DateTime.MinValue;
                   FileInfo destinationFile = new FileInfo(destination_path);
                   FileInfo destinationAnimation = new FileInfo(animation_destination_path);

                   foreach (string file in group.Value.file_list)
                   {
                       FileInfo info = new FileInfo(file);

                       if (info.LastWriteTimeUtc > MostRecentDate)
                           MostRecentDate = info.LastWriteTimeUtc;
                   }

                   //if the destination file is more recent than the most recent file in our group, it's up to date
                   if (destinationFile.LastWriteTimeUtc >= MostRecentDate && destinationAnimation.LastWriteTimeUtc >= MostRecentDate)
                   {
                       continue;
                   }
               }

                int offsetx = 0;
                int offsety = 0;
                int maxHeight = 0;
                int mergedWidth = 0;
                int mergedHeight = 0;

                for (int ifile = 0; ifile < group.Value.file_list.Count;ifile++)
                {
                   
                   // try
                //    {
                        FileInfo galFileInfo = new FileInfo(group.Value.file_list[ifile]);

                        reader.OpenFile(group.Value.file_list[ifile]);

                        imgWidth = reader.GetWidth();
                        imgHeight = reader.GetHeight();

                        Bitmap animBitmap = reader.GetPackedAnimations(imgWidth,imgHeight,offsetx,offsety, out animDef, totalFrame,group.Value.contact_points);

                        totalFrame += animDef.Frames.Length;

                        animDef.SpriteOffsetx = group.Value.sprite_offset[ifile].X;
                        animDef.SpriteOffsety = group.Value.sprite_offset[ifile].Y;

                        anim_list.AnimArrays.Add(animDef);

                        bitmapList.Add(animBitmap);

                        animationCount++;

                        if (mergedWidth < animBitmap.Width)
                            mergedWidth = animBitmap.Width;

                        mergedHeight += animBitmap.Height;

                        offsety += animBitmap.Height;

                        if (reader.FileOpened)
                            reader.CloseFile();

                   /* }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                    finally
                    {
                       
                    }*/
                }

                //merge bitmap together
                Bitmap bmpToSave = null;


                if (bitmapList.Count > 1)
                    bmpToSave = BitmapUtilities.MergeBitmaps(bitmapList.ToArray(), mergedWidth, mergedHeight,anim_list.AnimArrays);
                else
                    bmpToSave = bitmapList[0];

                //write bitmap to disk

                using (var fileTileset = File.Create(destination_path))
                {
                    bmpToSave.Save(fileTileset, ImageFormat.Png);
                    pngCount++;
                }


                //sauvegarde niveau
                using (var fileInfo = File.Create(animation_destination_path))
                {
                    ProtoBuf.Serializer.Serialize<PBInterface.AnimationList>(fileInfo, anim_list);

                    animationFileCount++;
                }

            }

            Console.WriteLine("Wrote " + pngCount + " png tileset, " + animationCount + " animation info in " + animationFileCount + " file(s)");


            int asset_count = 0;
            int script_count = 0;

            var filteredFiles = Directory.GetFiles(inputDir, "*.*").Where(file => file.ToLower().EndsWith("lwf") || file.ToLower().EndsWith("lua") || file.ToLower().EndsWith("ttf") || file.ToLower().EndsWith("ogg") || file.ToLower().EndsWith("shad") || file.ToLower().EndsWith("png") || file.ToLower().EndsWith("pack") || file.ToLower().EndsWith("db") || file.ToLower().EndsWith("wav") || file.ToLower().EndsWith("mwf") || file.ToLower().EndsWith("fwf") || file.ToLower().EndsWith("json")).ToList();

            //copy other assets to ouput directory
            foreach (string regular_asset in filteredFiles)
            {
                FileInfo reg_asset_info = new FileInfo(regular_asset);

                if (skip_list.Contains(reg_asset_info.Name))
                    continue;

                string out_path = Path.Combine(outputDir, reg_asset_info.Name);

                if(reg_asset_info.Extension == ".lua")
                    out_path = Path.Combine(outputScriptDir,reg_asset_info.Name);

                if (skip_png && reg_asset_info.Extension == ".png")
                    continue;

                

                //compare write date
                if (File.Exists(out_path))
                {
                    FileInfo out_asset_info = new FileInfo(out_path);

                    if (out_asset_info.LastWriteTimeUtc >= reg_asset_info.LastWriteTimeUtc)
                        continue;

                    //wait for the assets to be available
                    while (BitmapUtilities.IsFileLocked(out_asset_info))
                        Thread.Yield();

                }

                File.Copy(regular_asset, out_path, true);

                if (reg_asset_info.Extension == ".lua")
                    script_count++;
                else
                    asset_count++;
            }




            Console.WriteLine("Wrote {0} asset(s) in output directory {1}", asset_count, outputDir);

            Console.WriteLine("Wrote {0} scripts(s) in output directory {1}", script_count, outputScriptDir);

        }
    }
}
