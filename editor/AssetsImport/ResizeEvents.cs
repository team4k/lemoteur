﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AssetsImport
{
    public class ResizeEvents
    {

        public static ResizeCursor MouseDown(List<ResizeCursor> currentCursors, MouseEventArgs e)
        {
            ResizeCursor outcursor = null;

            foreach (ResizeCursor cursor in currentCursors)
            {
                if (cursor.Cursor.Contains(e.Location))
                {
                    cursor.SelectCursor();
                    outcursor = cursor;
                }
                else
                {
                    cursor.DeSelectCursor();
                }
            }

            return outcursor;
        }

        public static bool MouseUp(ref ResizeCursor selectedCursor)
        {
            if (selectedCursor != null)
            {
                selectedCursor.DeSelectCursor();
                selectedCursor = null;
                return true;
            }

            return false;
        }

        public static bool MouseMove(bool cursordrag, ResizeCursor selectedCursor, List<ResizeCursor> currentCursors, FrameBitmap currentframe,List<FrameBitmap> frames,MouseEventArgs e)
        {
            if (cursordrag && selectedCursor != null)
            {
                selectedCursor.MoveCursor(e.Location.X, e.Location.Y);

                ResizeCursor one = null;
                ResizeCursor two = null;

                switch (selectedCursor.Pos)
                {
                    case ResizeCursor.PosCursor.LeftUp:
                        {
                            one = currentCursors.First<ResizeCursor>(p => p.Pos == ResizeCursor.PosCursor.LeftDown);
                            two = currentCursors.First<ResizeCursor>(p => p.Pos == ResizeCursor.PosCursor.RightUp);

                        }

                        break;

                    case ResizeCursor.PosCursor.LeftDown:
                        {
                            one = currentCursors.First<ResizeCursor>(p => p.Pos == ResizeCursor.PosCursor.LeftUp);
                            two = currentCursors.First<ResizeCursor>(p => p.Pos == ResizeCursor.PosCursor.RightDown);
                        }


                        break;

                    case ResizeCursor.PosCursor.RightUp:
                        {
                            one = currentCursors.First<ResizeCursor>(p => p.Pos == ResizeCursor.PosCursor.RightDown);
                            two = currentCursors.First<ResizeCursor>(p => p.Pos == ResizeCursor.PosCursor.LeftUp);


                        }


                        break;

                    case ResizeCursor.PosCursor.RightDown:
                        {
                            one = currentCursors.First<ResizeCursor>(p => p.Pos == ResizeCursor.PosCursor.RightUp);
                            two = currentCursors.First<ResizeCursor>(p => p.Pos == ResizeCursor.PosCursor.LeftDown);
                        }

                        break;
                }

                if (one != null && two != null)
                {
                    one.MoveCursor(e.Location.X, one.Cursor.Y);
                    two.MoveCursor(two.Cursor.X, e.Location.Y);
                }



                //redéfinition du rectangle de clipping à partir des curseurs
                int minx = selectedCursor.Cursor.X, miny = selectedCursor.Cursor.Y, maxx = selectedCursor.Cursor.X, maxy = selectedCursor.Cursor.Y;

                foreach (ResizeCursor cursor in currentCursors)
                {
                    if (cursor == selectedCursor)
                        continue;

                    if (cursor.Cursor.X < minx)
                    {
                        minx = cursor.Cursor.X;
                    }
                    else if (cursor.Cursor.X > maxx)
                    {
                        maxx = cursor.Cursor.X;
                    }

                    if (cursor.Cursor.Y < miny)
                    {
                        miny = cursor.Cursor.Y;
                    }
                    else if (cursor.Cursor.Y > maxy)
                    {
                        maxy = cursor.Cursor.Y;
                    }
                }


                currentframe.ClipRectangle = new Rectangle(minx, miny, maxx - minx, maxy - miny);

                foreach (FrameBitmap frame in frames)
                {
                    if (frame == currentframe)
                    {
                        continue;
                    }

                    frame.ClipRectangle = new Rectangle(currentframe.ClipRectangle.X, currentframe.ClipRectangle.Y, currentframe.ClipRectangle.Width, currentframe.ClipRectangle.Height);
                }

                return true;
            }

            return false;
        }


        public static void Paint(PictureBox box, List<ResizeCursor> currentCursors, FrameBitmap currentframe, PaintEventArgs e)
        {
            if (box.Image == null)
                return;

            e.Graphics.InterpolationMode = InterpolationMode.NearestNeighbor;
            e.Graphics.SmoothingMode = SmoothingMode.None;

            //set the center of the pixel when drawing to 0.5 / 0.5 rather than the default 0 / 0 (default make the drawing start at -0.5)
            e.Graphics.PixelOffsetMode = PixelOffsetMode.Half;

            Pen pen = new Pen(Color.Red, 4);

            e.Graphics.DrawRectangle(pen, currentframe.ClipRectangle);

            if (currentCursors.Count == 0)
            {
                const int selectrectsize = 16;

                //dessin des rectangle d'accrochage
                Rectangle leftUp = new Rectangle(currentframe.ClipRectangle.Left, currentframe.ClipRectangle.Top, selectrectsize, selectrectsize),
                    rightUp = new Rectangle(currentframe.ClipRectangle.Right - selectrectsize, currentframe.ClipRectangle.Top, selectrectsize, selectrectsize),
                    leftDown = new Rectangle(currentframe.ClipRectangle.Left, currentframe.ClipRectangle.Bottom - selectrectsize, selectrectsize, selectrectsize),
                    rightDown = new Rectangle(currentframe.ClipRectangle.Right - selectrectsize, currentframe.ClipRectangle.Bottom - selectrectsize, selectrectsize, selectrectsize);

                currentCursors.Add(new ResizeCursor(leftUp, (Pen)pen.Clone(), ResizeCursor.PosCursor.LeftUp));
                currentCursors.Add(new ResizeCursor(rightUp, (Pen)pen.Clone(), ResizeCursor.PosCursor.RightUp));
                currentCursors.Add(new ResizeCursor(leftDown, (Pen)pen.Clone(), ResizeCursor.PosCursor.LeftDown));
                currentCursors.Add(new ResizeCursor(rightDown, (Pen)pen.Clone(), ResizeCursor.PosCursor.RightDown));
            }

            foreach (ResizeCursor cursor in currentCursors)
            {
                e.Graphics.DrawRectangle(cursor.CursorPen, cursor.Cursor);
            }
        }
    }
}
