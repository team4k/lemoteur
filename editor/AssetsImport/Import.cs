﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImageMagick;

namespace AssetsImport
{
    public class Import
    {
        private static bool conversionerror = false;

        public static void DataReceived(string data, IlogImport importer, bool iserror = false, bool threaded = false)
        {
            if (data == null)
            {
                return;
            }

            importer.AppendLog(data, iserror, threaded);

            if(iserror)
            {
                conversionerror = true;
            }
        }

        public static bool TraceAndGimpPng(string potrace_path,List<FileInfo> selectedframes, IlogImport importer,out List<FrameBitmap> frames,int previewwidth,int previewheight,bool isscan)
        {
            conversionerror = false;
            frames = new List<FrameBitmap>();

            //chargement des bitmap
            foreach (FileInfo inf in selectedframes)
            {
                //conversion de l'image en bmp avant passage dans potrace
                var imagebase = new MagickImage(inf.FullName);
                string tmpbmp = inf.FullName + ".bmp";
                imagebase.Format = MagickFormat.Bmp;

                imagebase.Write(tmpbmp);

                string tmppo = inf.FullName + ".svg";
                string tmpgimp = inf.FullName + ".svg.png";

                Bitmap scan = new Bitmap(Image.FromFile(inf.FullName));

                if (isscan)
                {
                    //passage des scans dans potrace
                    ProcessStartInfo PotraceInfo = new ProcessStartInfo(potrace_path, string.Format(@" {0} --svg -o {1}", tmpbmp, tmppo));
                    PotraceInfo.UseShellExecute = false;
                    PotraceInfo.RedirectStandardOutput = true;
                    PotraceInfo.RedirectStandardError = true;

                    Process Potraceproc = Process.Start(PotraceInfo);
                    Potraceproc.OutputDataReceived += (osender, oe) => Import.DataReceived(oe.Data, importer, threaded: true);
                    Potraceproc.ErrorDataReceived += (osender, oe) => Import.DataReceived(oe.Data, importer, iserror: true, threaded: true);
                    Potraceproc.BeginOutputReadLine();
                    Potraceproc.BeginErrorReadLine();

                    while (!Potraceproc.HasExited)
                        importer.DoEvents();

                    Potraceproc.Close();
                }
                else
                {
                    scan.Save(tmpgimp, ImageFormat.Png);
                }
                

                if (!conversionerror)
                {

                    try
                    {
                        if(isscan)
                        {
                            //conversion svg vers png avec gimp
                            string pathpython = string.Format(@"BatchConvertLms.py DoConvertSvg {0} {1}", tmppo, tmpgimp);

                            //double antislashes on windows, otherwise the batch will fail
                            pathpython = pathpython.Replace(@"\", @"\\");

                            ProcessStartInfo GimpConversionInfo = new ProcessStartInfo("python", pathpython);
                            GimpConversionInfo.UseShellExecute = false;
                            GimpConversionInfo.RedirectStandardOutput = true;
                            GimpConversionInfo.RedirectStandardError = true;
                            GimpConversionInfo.WorkingDirectory = Environment.CurrentDirectory;

                            Process Gimpproc = Process.Start(GimpConversionInfo);

                            while (!Gimpproc.HasExited)
                                importer.DoEvents();

                        }

                        Bitmap potrace = new Bitmap(tmpgimp);


                        Rectangle clip = new Rectangle(2, 2, potrace.Width - 4, potrace.Height - 4);

                        if (clip.Width > previewwidth)
                        {
                            clip.Width = previewwidth - 4;
                        }

                        if (clip.Height > previewheight)
                        {
                            clip.Height = previewheight - 4;
                        }

                        //center clip rectangle

                        clip.X = (previewwidth / 2) - (clip.Width / 2);
                        clip.Y = (previewheight / 2) - (clip.Height / 2);

                        if (clip.X < 2)
                            clip.X = 2;

                        if (clip.Y < 2)
                            clip.Y = 2;




                        frames.Add(new FrameBitmap(scan, potrace, clip));
                    }
                    catch (Exception ex)
                    {
                        importer.AppendText("Erreur lors de la conversion de svg vers png " + ex.ToString());
                        importer.AppendText(Environment.NewLine);
                        conversionerror = true;
                    }

                }


                if (conversionerror)
                {

                    break;
                }

            }

            //nettoyage fichiers temporaires
            foreach (FileInfo inf in selectedframes)
            {
                if (File.Exists(inf.FullName + ".bmp"))
                {
                    File.Delete(inf.FullName + ".bmp");
                }

                if (File.Exists(inf.FullName + ".svg"))
                {
                    File.Delete(inf.FullName + ".svg");
                }

                /*if (File.Exists(inf.FullName + ".svg.png"))
                {
                    File.Delete(inf.FullName + ".svg.png");
                }*/
            }

            if (!conversionerror)
            {
                importer.AppendText("Conversion terminée !");
                return true;
            }
            else
            {
                foreach (FrameBitmap bmp in frames)
                {
                    bmp.Dispose();
                }

                frames.Clear();



                importer.AppendText("Erreur lors de la conversion en svg, veuillez vérifier les logs...");
                return false;
            }
        }

        public static void ExportGimpToResizedPng(string infile,string outfile, IlogImport importer,int width,int height)
        {
            string pathpython = string.Format(@"BatchConvertLms.py DoMergeLayer {0} {1}", infile, outfile);

            //double antislashes on windows, otherwise the batch will fail
            pathpython = pathpython.Replace(@"\", @"\\");

            ProcessStartInfo GimpConversionInfo = new ProcessStartInfo("python", pathpython);
            GimpConversionInfo.UseShellExecute = false;
            GimpConversionInfo.RedirectStandardOutput = true;
            GimpConversionInfo.RedirectStandardError = true;
            GimpConversionInfo.WorkingDirectory = Environment.CurrentDirectory;

            Process Gimpproc = Process.Start(GimpConversionInfo);
            // Gimpproc.OutputDataReceived += (osender, oe) => this.AppendLog(oe.Data, threaded: true);
            //Gimpproc.ErrorDataReceived += (osender, oe) => this.AppendLog(oe.Data, iserror: true, threaded: true);
            Gimpproc.BeginOutputReadLine();
            //Gimpproc.BeginErrorReadLine();

            while (!Gimpproc.HasExited)
                importer.DoEvents();

            //resize final frame
            var image = new MagickImage(outfile, MagickFormat.Png);

            var size = new MagickGeometry(width, height);

            size.IgnoreAspectRatio = false;

            image.Resize(size);

            var memStream = new MemoryStream();

            image.Write(memStream, MagickFormat.Png);


            memStream.Position = 0;

            Bitmap newframe = new Bitmap(memStream);

            newframe.Save(outfile, ImageFormat.Png);

            image.Dispose();
            newframe.Dispose();
            memStream.Close();
        }

    }
}
