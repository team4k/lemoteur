﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetsImport
{
    public interface IlogImport
    {
        void AppendLog(string data, bool iserror = false, bool threaded = false);
        void DoEvents();
        void AppendText(string text);
    }
}
