﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using ImageMagick;

namespace AssetsImport
{
    public class FrameBitmap : IDisposable
    {
        private Bitmap _potracescan;
        private Bitmap _outframe;

        public Bitmap originalscan { get; }
        public Bitmap potracedscan
        {
            get
            {
                return _potracescan;
            }
        }

        public Bitmap outframe
        {
            get
            {
                return _outframe;
            }
        }

        public Rectangle ClipRectangle { get; set; }

        public FrameBitmap(Bitmap originalscan, Bitmap potracescan, Rectangle clip)
        {
            this.originalscan = originalscan;
            this._potracescan = potracescan;
            this.ClipRectangle = clip;
        }

        public Bitmap GetContent(bool orig)
        {
            if (orig)
                return originalscan;

            if (_outframe != null)
                return _outframe;
            else
                return _potracescan;

        }

        private Bitmap CloneBitmap(Bitmap toclone)
        {
            // var bmpclone = Image.FromHbitmap(this.GetContent(false).GetHbitmap());
            BitmapData curBitmapData = toclone.LockBits(new Rectangle(0, 0, toclone.Width, toclone.Height),
     ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
            Int32 stride = curBitmapData.Stride;
            byte[] content = new byte[stride * toclone.Height];
            Marshal.Copy(curBitmapData.Scan0, content, 0, content.Length);
            toclone.UnlockBits(curBitmapData);

            var bmpclone = new Bitmap(toclone.Width, toclone.Height);

            Int32 curRowOffs = 0;
            for (Int32 y = 0; y < toclone.Height; y++)
            {
                // Set offset to start of current row
                Int32 curOffs = curRowOffs;
                for (Int32 x = 0; x < toclone.Width; x++)
                {
                    // ARGB = bytes [B,G,R,A]
                    Byte b = content[curOffs];
                    Byte g = content[curOffs + 1];
                    Byte r = content[curOffs + 2];
                    Byte a = content[curOffs + 3];
                    Color col = Color.FromArgb(a, r, g, b);
                    bmpclone.SetPixel(x, y, col);
                    // Do whatever you want with your colour here
                    // ...

                    // Increase offset to next colour
                    curOffs += 4;
                }
                // Increase row offset
                curRowOffs += stride;
            }

            return bmpclone;
        }

        public void CropScan(int startcropx,int startcropy)
        {
            var bmpclone = this.CloneBitmap(this.GetContent(false));

            var memStream = new MemoryStream();
            bmpclone.Save(memStream, ImageFormat.Png);
            memStream.Position = 0;
            byte[] content2 = memStream.ToArray();
            memStream.Close();

            var image = new MagickImage(content2, MagickFormat.Png);
            var size = new MagickGeometry(startcropx, startcropy, ClipRectangle.Width, ClipRectangle.Height);

            size.IgnoreAspectRatio = false;

            image.Crop(size);

            memStream = new MemoryStream();

            image.Write(memStream);

            memStream.Position = 0;

            Bitmap newframe = new Bitmap(memStream);

            image.Dispose();
            memStream.Close();

            if (this._outframe != null)
            {
                this._outframe.Dispose();
                this._outframe = newframe;
            }
            else
            {
                this._potracescan.Dispose();
                this._potracescan = newframe;
            }
            
        }

        public void RedimScan(int newwidth,int newheight)
        {
            //var bmpclone = Image.FromHbitmap(_potracescan.GetHbitmap());
            var bmpclone = this.CloneBitmap(this.GetContent(false));

            var memStream = new MemoryStream();
            bmpclone.Save(memStream, ImageFormat.Png);
            memStream.Position = 0;
            byte[] content = memStream.ToArray();
            memStream.Close();

            var image = new MagickImage(content, MagickFormat.Png);

            var size = new MagickGeometry(newwidth, newheight);

            size.IgnoreAspectRatio = false;

            image.Resize(size);

            memStream = new MemoryStream();

            image.Write(memStream);

            memStream.Position = 0;

            Bitmap newframe = new Bitmap(memStream);
            
            image.Dispose();
            memStream.Close();

            if (this._outframe != null)
                this._outframe.Dispose();

            this._outframe = newframe;
        }

        public void SaveAll(string outscan,string outpotrace,string outframe)
        {
            //var bmpclone = Image.FromHbitmap(originalscan.GetHbitmap());
            var bmpclone = this.CloneBitmap(originalscan);
            bmpclone.Save(outscan, ImageFormat.Png);

            //bmpclone = Image.FromHbitmap(_potracescan.GetHbitmap());
            bmpclone = this.CloneBitmap(_potracescan);
            bmpclone.Save(outpotrace, ImageFormat.Png);

            //bmpclone = Image.FromHbitmap(GetContent(false).GetHbitmap());
            bmpclone = this.CloneBitmap(GetContent(false));
            bmpclone.Save(outframe, ImageFormat.Png);
        }

        public void SavePoTraced(string outpotraced)
        {
            //var bmpclone = Image.FromHbitmap(_potracescan.GetHbitmap());
            var bmpclone = this.CloneBitmap(_potracescan);
            bmpclone.Save(outpotraced, ImageFormat.Png);
        }

        public void SaveFrame(string outframe)
        {
            //var bmpclone = Image.FromHbitmap(GetContent(false).GetHbitmap());
            var bmpclone = this.CloneBitmap(GetContent(false));
            bmpclone.Save(outframe, ImageFormat.Png);
        }

        public void Dispose()
        {
            if(originalscan != null)
                originalscan.Dispose();

            if(potracedscan != null)
                potracedscan.Dispose();

            if (this._outframe != null)
                this._outframe.Dispose();
        }

    }
}
