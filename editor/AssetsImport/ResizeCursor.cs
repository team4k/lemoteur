﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetsImport
{
    public class ResizeCursor
    {
        public enum PosCursor { LeftUp, LeftDown, RightUp, RightDown };

        private Rectangle _cursor;
        private Pen _cursorPen;

        public Rectangle Cursor {
            get { return _cursor; }
        }

        public Pen CursorPen {
            get { return _cursorPen; }
        }

        public PosCursor Pos { get; }

        public ResizeCursor(Rectangle cursor,Pen cursorpen,PosCursor pos)
        {
            _cursor = cursor;
            _cursorPen = cursorpen;
            Pos = pos;
        }

        public void SelectCursor()
        {
            _cursorPen = new Pen(Color.Fuchsia, 4); 
        }

        public void DeSelectCursor()
        {
            _cursorPen = new Pen(Color.Red, 4);
        }

        /// <summary>
        /// Changement de la position du rectangle du curseur
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void MoveCursor(int x,int y)
        {
            _cursor.X = x;
            _cursor.Y = y;
        }
    }
}
