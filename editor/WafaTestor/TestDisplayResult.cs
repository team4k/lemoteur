﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WafaTestor
{
    public partial class TestDisplayResult : Form
    {

        public TestDisplayResult()
        {
            InitializeComponent();
        }

        public void AppendText(string text)
        {
            this.AppendText(text, rtbResult.ForeColor);
        }

        public  void AppendText(string text, Color color)
        {
            rtbResult.SelectionStart = rtbResult.TextLength;
            rtbResult.SelectionLength = 0;

            rtbResult.SelectionColor = color;
            rtbResult.AppendText(text);
            rtbResult.SelectionColor = rtbResult.ForeColor;
        }
    }
}
