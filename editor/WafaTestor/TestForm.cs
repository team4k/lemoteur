﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MoonSharp.Interpreter;
using MoonSharp.Interpreter.Loaders;
using PBInterface;
using WafaEditorV2;

/// <summary>
/// Projet to load and run test automatically
/// it work by loading an existing level file in lwf format which contain the specific game event to test
/// it use nunit and native function to run lua code to send command and to get test result back, allowing to run regression testing without user input
/// Allow game specific command must be run from a test lua function, this projet should be game agnostic
/// </summary>
namespace WafaTestor
{
    public partial class TestForm : Form
    {
        public string[] testfiles = null;
        public int cfile = 0;
        public int cfunc = 1;
            
        public Stopwatch cwatch = null;

        DynValue testfunctions;
        DynValue checkfunctions;
        DynValue teardownfunctions;
        string modulename;
        int timeout = -1;
        TestReport report = new TestReport();

        List<TestReport> reportslist = new List<TestReport>();
        string raw_assets_path;
        TestDisplayResult displayresult = new TestDisplayResult();

        bool testended = false;

        public TestForm()
        {
            #if DEBUG
                MessageBox.Show("En attente debug");
            #endif

            InitializeComponent();

            Application.Idle += Application_Idle;

            NativeFunc.editor_init();

            //-1- find base path (root of project path)
            string dir = AppDomain.CurrentDomain.BaseDirectory;

            string projectdir = Path.GetFullPath(Path.Combine(dir, ".."));


            //-2- define assets paths / open and read projet config file 
            raw_assets_path = Path.Combine(projectdir, "game_template", "assets");

            string export_asset_path = Path.Combine(projectdir, "game_template", "bin", "assets");

            string config_file_path = Path.Combine(projectdir, "game_template", "bin", "Release");

            Script configscript = new Script();
            configscript.DoFile(Path.Combine(config_file_path, "config.lua"));

            DynValue config = configscript.DoString("return config");

            string game_dll = Path.Combine(config_file_path, config.Table["gamemodule"].ToString());


            //config.Table[]

            //-3- initialise engine
            renderPanel.StartEngine(raw_assets_path, export_asset_path, game_dll, "visitor1.json", "prog_light", "prog_shadows", "prog_no_texture");
            renderPanel.Show();

            testfiles = Directory.GetFiles(Path.Combine(raw_assets_path, "tests"));

            report.TestFile = testfiles[0];
            LoadTestLevel(testfiles[0], raw_assets_path, out testfunctions, out checkfunctions,out teardownfunctions,out modulename);

            cwatch = new Stopwatch();
            cwatch.Start();

            renderPanel.Invalidate();
            NativeFunc.editor_change_mode(NativeFunc.wbool.wtrue);
            NativeFunc.editor_set_module(modulename);
            NativeFunc.editor_set_call_script(NativeFunc.wbool.wtrue);

            timeout = starttest(testfunctions, cfunc, modulename);
        }

        public bool LoadTestLevel(string testfile,string raw_assets_path,out DynValue testfunctions,out DynValue checkfunctions,out DynValue teardownfunctions,out string modulename)
        {
            testfunctions = null;
            checkfunctions = null;
            teardownfunctions = null;
            modulename = null;

            Level olevel = null;

            using (var file = File.OpenRead(testfile))
            {
                try
                {
                    olevel = ProtoBuf.Serializer.Deserialize<Level>(file);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erreur chargement fichier " + testfile + " exception : " + ex.ToString());
                    return false;
                }

                if (olevel != null)
                {
                    MemoryStream stream = new MemoryStream();
                    ProtoBuf.Serializer.Serialize<Level>(stream, olevel);
                    stream.Position = 0;
                    byte[] level_data_array = stream.ToArray();

                    NativeFunc.editor_load_level(level_data_array, (uint)stream.Length);

                    renderPanel.Invalidate();

                    NativeFunc.editor_update_collisions();

                    NativeFunc.editor_setlevel_name(olevel.LevelId);

                    NativeFunc.editor_level_loaded();

                    NativeFunc.editor_setzoom(renderPanel.Width, renderPanel.Height, olevel.MapArrays[0].ZoomValue);
                    NativeFunc.editor_changecollisionsvisibility(NativeFunc.wbool.wfalse);

                    //get test and check functions;
                    Script levelscript = new Script();

                    string dummyfuncs = @"    
		                            function config_getbool(mode)
			                            return false
		                            end
                                        
                                    function game_gettime()
                                        return 0
                                    end
                                    ";

                    levelscript.DoString(dummyfuncs);
                    ((ScriptLoaderBase)levelscript.Options.ScriptLoader).ModulePaths = new string[] { raw_assets_path + "/?", raw_assets_path + "/?.lua" };

                    try
                    {
                        levelscript.DoFile(Path.Combine(raw_assets_path, olevel.ScriptFile));
                    }
                    catch(MoonSharp.Interpreter.SyntaxErrorException ex)
                    {
                        MessageBox.Show(ex.DecoratedMessage);
                    }
                    
                    //get module name
                    modulename = Path.GetFileNameWithoutExtension(olevel.ScriptFile);

                    testfunctions = levelscript.DoString("return " + modulename + ".gettestfunctions('test')");
                    checkfunctions = levelscript.DoString("return " + modulename + ".gettestfunctions('check')");
                    teardownfunctions = levelscript.DoString("return " + modulename + ".gettestfunctions('teardown')");

                    StringBuilder buffer = new StringBuilder(1000);

                    //init level script
                    string function = string.Format("{0}.init()", modulename);
                    NativeFunc.editor_exec_string(function, buffer, buffer.Capacity);

                    if (buffer.Length != 0)
                    {
                        MessageBox.Show(buffer.ToString());
                        return false;
                    }

                    buffer.Clear();


                }

                return true;
            }
                    
        }


        public int starttest(DynValue testfunctions, int cfunc, string modulename)
        {
            StringBuilder buffer = new StringBuilder(1000);
            string testfunc = testfunctions.Table[cfunc].ToString();
            NativeFunc.editor_exec_string("return " + modulename + "." + testfunc + "()", buffer, buffer.Capacity);

            //get timeout
            int timeout = Convert.ToInt32(buffer.ToString());
            return timeout;
        }


        public bool checktest(DynValue testfunctions,DynValue checkfunctions,int cfunc,string modulename,TestReport creport)
        {
            StringBuilder buffer = new StringBuilder(1000);
            string testfunc = testfunctions.Table[cfunc].ToString();
            string checkfunc = checkfunctions.Table[cfunc].ToString();


            NativeFunc.editor_exec_string("return " + modulename + "." + checkfunc + "()", buffer, buffer.Capacity);

            bool result = Convert.ToBoolean(buffer.ToString());

            //test ok
            if (result)
            {
                creport.TestResult.Add(testfunc, true);
                return true;
            }


           return false;
        }

        public bool teardowntest(DynValue teardownfunctions,int cfunc)
        {
            StringBuilder buffer = new StringBuilder(1000);
            string teardownfunc =teardownfunctions.Table[cfunc].ToString();


            NativeFunc.editor_exec_string(modulename + "." + teardownfunc+ "()", buffer, buffer.Capacity);


            if (!string.IsNullOrEmpty(buffer.ToString()))
            {
                MessageBox.Show("Erreur lors du nettoyage du test " + buffer.ToString());
                return false;
            }

            return true;

        }

        public void ShowResult(List<TestReport> reports)
        {

            //-5- once every test ran open a report with all tests results
            foreach (TestReport report in reports)
            {
                displayresult.AppendText("Résultat des tests pour le niveau : " + report.TestFile + "\n");

                foreach (KeyValuePair<string, bool> dicpair in report.TestResult)
                {
                    displayresult.AppendText("Test : " + dicpair.Key + " ");
                    displayresult.AppendText((((dicpair.Value) ? "OK" : "KO") + "\n"), (dicpair.Value) ? Color.Green : Color.Red);
                }
            }

            displayresult.ShowDialog();
        }

        void Application_Idle(object sender, EventArgs e)
        {

            if (testended)
            {
                return;
            }

            // update render here
            renderPanel.Invalidate();//render a frame

            bool nexttest = (cwatch.Elapsed.TotalSeconds >= timeout || checktest(testfunctions, checkfunctions, cfunc, modulename, report));

            StringBuilder buffer = new StringBuilder(10000);
            NativeFunc.editor_get_lua_log(buffer, buffer.Capacity);

            if (buffer.Length != 0)
            {
                report.TestResult.Add(testfunctions.Table[cfunc].ToString(),false);
                MessageBox.Show(buffer.ToString(), "Erreur lors de l'exécution du test", MessageBoxButtons.OK, MessageBoxIcon.Error);
                nexttest = true;
            }

            if(nexttest)
            {
                //in case of timeout, add the test as failed
                if(cwatch.Elapsed.TotalSeconds >= timeout)
                {
                    report.TestResult.Add(testfunctions.Table[cfunc].ToString(), false);
                }

                reportslist.Add(report);

                teardowntest(teardownfunctions, cfunc);

                if (cfunc < testfunctions.Table.Length)
                {
                    cfunc++;
                }
                else if(++cfile < testfiles.Length)
                {
                    LoadTestLevel(testfiles[cfile], raw_assets_path, out testfunctions, out checkfunctions,out teardownfunctions, out modulename);
                    NativeFunc.editor_set_module(modulename);
                    NativeFunc.editor_set_call_script(NativeFunc.wbool.wtrue);
                    report = new TestReport();
                    report.TestFile = testfiles[cfile];
                    cfunc = 1;
                }
                else
                {
                    cwatch.Stop();
                    testended = true;
                    nexttest = false;
                    ShowResult(reportslist);
                }


                if(nexttest)
                {
                    timeout = starttest(testfunctions, cfunc, modulename);

                    cwatch.Reset();
                    cwatch.Start();
                }
                

            }

        }

        private void renderPanel_Paint(object sender, PaintEventArgs e)
        {

        }

    }
}
