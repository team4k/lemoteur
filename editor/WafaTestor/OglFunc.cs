﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Security;

namespace WafaEditorV2
{
    public class NativeFunc
    {
        public enum wbool { wfalse = 0, wtrue = 1 }

        public enum GAME_TYPE { GT_ENTITY = 0, GT_TRIGGER = 1, GT_TEXT = 2, GT_SOUND = 3, GT_LIGHT = 4, GT_PARTICLES = 5 };

        public enum Collisions_Values { NO_COLLISION = 0, COLLISION = 1, COL_DESTROY = 2, COL_SLOPE = 5, COL_SLOPE_INVERSE = 6, COL_EDITABLE = 7, COL_EDITABLE_2 = 8, COL_EDITABLE_3 = 9, COL_KILL = 10 };

        public const string enginePath = "WafaEngine";

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_send_inputmessage(uint msg, uint wparam, long lparam);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_draw_frame();

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_get_select_rectangle([In, Out] int[] rect_values);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern wbool editor_load_gamedll([MarshalAs(UnmanagedType.LPStr)] string game_dll_path);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_release_gamedll();

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_release_localedb();

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_load_localedb();

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_scroll(float mx, float my);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_set_scroll(float x, float y);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_focus_onplayer();

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_setplayer_pos(float x, float y);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_init();

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern wbool editor_start_engine(IntPtr hwnd, int width, int height, int player_start_x, int player_start_y, int startX, int startY, [MarshalAs(UnmanagedType.LPStr)] string asset_path, [MarshalAs(UnmanagedType.LPStr)] string game_dll_path, [MarshalAs(UnmanagedType.LPStr)] string font, [MarshalAs(UnmanagedType.LPStr)] string prog_light, [MarshalAs(UnmanagedType.LPStr)] string prog_shadows, [MarshalAs(UnmanagedType.LPStr)] string prog_mask, [MarshalAs(UnmanagedType.LPStr)] string raw_assets_path);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern wbool editor_load_level([In, Out] byte[] level_data, uint level_size);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_resize_screen(int width, int height);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_set_tilesize(int tile_size);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_shutdown_engine();

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_setcursor_pos(int x, int y, wbool endcursor_only = wbool.wfalse, wbool move_multi_selection = wbool.wfalse);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_settargetpos(int x, int y);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_setcursor_color(float r, float g, float b, float a);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_setfps_color(float r, float g, float b, float a);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_set_ambientcolor(float r, float g, float b, float a);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_setenable_lighting(wbool enable);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_setlight_progs([MarshalAs(UnmanagedType.LPStr)] string light_prog, [MarshalAs(UnmanagedType.LPStr)] string shadow_prog);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_create_tilemap(int tileWidth, int tileHeight, [In, Out] byte[] map_data, uint map_size);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_Tilemap_TextureChanged();

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_change_tile(int tileId, int tilePos, [MarshalAs(UnmanagedType.LPStr)] string layer_id);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_change_multi_tile([In, Out] int[] tiles_id, int start_pos, [MarshalAs(UnmanagedType.LPStr)] string layer_id);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_change_multi_tile2([In, Out] int[] tiles_id, int start_pos, [In, Out] int[] rect_values, [MarshalAs(UnmanagedType.LPStr)] string layer_id);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_change_animated_tile(int tileId, int tilePos, [MarshalAs(UnmanagedType.LPStr)] string layer_id, int current_frame);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_remove_animated_tile(int tilePos, [MarshalAs(UnmanagedType.LPStr)] string layer_id, int current_frame);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_goto_frame(int frame, [MarshalAs(UnmanagedType.LPStr)] string layer_id);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_get_tilearray([In, Out] int[] arrayData, [MarshalAs(UnmanagedType.LPStr)] string layer_id, [MarshalAs(UnmanagedType.LPStr)] string map_id);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int editor_get_num_multi_tiles();

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_get_multi_tiles([In, Out]int[] tileids, [MarshalAs(UnmanagedType.LPStr)] string layer_id);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_getcollisions_array([In, Out] int[] arrayData, [MarshalAs(UnmanagedType.LPStr)] string map_id);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_getcollision([In, Out]int[] tileid, int tilepos);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_update_collisions_data([In, Out] int[] arrayData);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_removecollisions();

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_changecollisionsvisibility(wbool visible);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_change_collision(int tileid, int tilepos);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_update_tilemap([In, Out] int[] arrayData, [MarshalAs(UnmanagedType.LPStr)] string layer_id);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_tilemaps_updatetilesize(int new_tile_size);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_clean_tilemap();


        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_addlayer([In, Out] byte[] layer_data, uint layer_size);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_removelayer([MarshalAs(UnmanagedType.LPStr)] string layer_id);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_update_layer([In, Out] byte[] layer_data, uint layer_size);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_update_layercontent([In, Out] byte[] layer_data, uint layer_size);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_changelayer_visibility(wbool visible, [MarshalAs(UnmanagedType.LPStr)] string layer_id);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_changecollisions_color(float a);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_changecollisions_color_forvalue(int value,float r,float g,float b,float a);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_changecollisions_color_forrange(int start_value,int end_value, float r, float g, float b, float a);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_change_collision_color(int tileid, int tilepos, float r, float g, float b, float a);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_get_tile([In, Out]int[] tileid, int tilepos, [MarshalAs(UnmanagedType.LPStr)] string layer_id);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_changelayer_id([MarshalAs(UnmanagedType.LPStr)] string old_layer, [MarshalAs(UnmanagedType.LPStr)] string new_layer);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_changelayer_order([MarshalAs(UnmanagedType.LPStr)] string layer_id, int new_draw_order);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_change_mode(wbool pmode);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_play_move(float mX, float mY);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_game_input(short input_event, wbool pressed);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_game_char(uint codepoint);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_update_collisions();

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_setlevel_name([MarshalAs(UnmanagedType.LPStr)] string level_name);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_switchmap([MarshalAs(UnmanagedType.LPStr)] string map_id);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_changemap_id([MarshalAs(UnmanagedType.LPStr)] string old_map, [MarshalAs(UnmanagedType.LPStr)] string new_map);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_setzoom(int width, int height, float zoomfactor);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_addtextureresx([MarshalAs(UnmanagedType.LPStr)] string texture_name, int width, int height, [In, Out] byte[] image_data, uint data_size);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_addanimresx([MarshalAs(UnmanagedType.LPStr)] string anim_name, [In, Out] byte[] anim_data, uint anim_size);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_updatetextureresx([MarshalAs(UnmanagedType.LPStr)] string texture_name, int width, int height, [In, Out] byte[] image_data, uint data_size);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_updateanimresx([MarshalAs(UnmanagedType.LPStr)] string anim_name, [In, Out] byte[] anim_data, uint anim_size);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_updatespriteanimations([MarshalAs(UnmanagedType.LPStr)] string anim_name);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_setpacker([In, Out] byte[] packer_data, uint packer_size);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_addsoundresx([MarshalAs(UnmanagedType.LPStr)]string sound_name, [MarshalAs(UnmanagedType.LPStr)] string sound_path);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_updatesoundresx([MarshalAs(UnmanagedType.LPStr)]string sound_name);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_addshaderresx([MarshalAs(UnmanagedType.LPStr)]string shader_name, [MarshalAs(UnmanagedType.LPStr)] string shader_path);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_updateshaderresx([MarshalAs(UnmanagedType.LPStr)]string shader_name);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_updatespritetextures([MarshalAs(UnmanagedType.LPStr)] string texture_name);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern float editor_getscrollX();

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern float editor_getscrollY();

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern wbool editor_resx_exist([MarshalAs(UnmanagedType.LPStr)] string resx_name);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_showcursor(wbool show_cursor, wbool show_selector);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_get_playerposition([In, Out] int[] array_pos);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_click(float targetX, float targetY, int button_id);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_endclick(float targetX, float targetY, int button_id);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_wheelpinch(float delta);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_setdestructible([MarshalAs(UnmanagedType.LPStr)] string layer_id, int tile_destruct_id);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_cleardestructible();

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_regen_textcoord([MarshalAs(UnmanagedType.LPStr)] string layer_id);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_exec_string([MarshalAs(UnmanagedType.LPStr)] string buffer, StringBuilder return_value, int return_size);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_set_call_script(wbool call);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_draw_debug_collisions(wbool show);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_get_lua_log(StringBuilder return_value,int return_size);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_reloadconfig();

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_reloadshaderconfig();

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_addentity([MarshalAs(UnmanagedType.LPStr)] string entity_id, [In, Out] byte[] entity_data, uint entity_size, [MarshalAs(UnmanagedType.LPStr)] string map_id);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_updateentity([MarshalAs(UnmanagedType.LPStr)] string entity_id, [In, Out] byte[] entity_data, uint entity_size);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_removeentity([MarshalAs(UnmanagedType.LPStr)] string entity_id, GAME_TYPE type);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_changeentityid([MarshalAs(UnmanagedType.LPStr)] string old_id, [MarshalAs(UnmanagedType.LPStr)] string new_id, GAME_TYPE type);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_selectentity([MarshalAs(UnmanagedType.LPStr)] string select_name, GAME_TYPE type, wbool add_to_selection);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_selectentity_pos(int posX, int posY, int z_order, wbool add_to_selection = wbool.wfalse);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_entity_unselect();

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern wbool editor_has_entity_selected();

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int editor_get_num_selection();

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern GAME_TYPE editor_entity_get_selection(StringBuilder buff, int num_selection,int buff_size);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_entity_update_pos([MarshalAs(UnmanagedType.LPStr)] string entity_id, int posX, int posY, GAME_TYPE type);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_entity_set_visible([MarshalAs(UnmanagedType.LPStr)] string entity_id, wbool visible, GAME_TYPE type);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_addtrigger([MarshalAs(UnmanagedType.LPStr)] string entity_id, [In, Out] byte[] entity_data, uint entity_size, [MarshalAs(UnmanagedType.LPStr)] string map_id, int zorder);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_updatetrigger([MarshalAs(UnmanagedType.LPStr)] string entity_id, [In, Out] byte[] entity_data, uint entity_size);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_addtextentity([MarshalAs(UnmanagedType.LPStr)] string entity_id, [In, Out] byte[] entity_data, uint entity_size, [MarshalAs(UnmanagedType.LPStr)] string map_id);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_updatetextentity([MarshalAs(UnmanagedType.LPStr)] string entity_id, [In, Out] byte[] entity_data, uint entity_size, [MarshalAs(UnmanagedType.LPStr)] string map_id);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_Tilemap_resize(int rowcount, int colcount, short row_direction, short col_direction);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_Tilemap_changeopacity([MarshalAs(UnmanagedType.LPStr)]string layer_id, float a);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_set_module([MarshalAs(UnmanagedType.LPStr)] string module_name);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern wbool editor_has_player();

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern wbool editor_hadlevelpending();

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern wbool editor_levelpendingisbinary();

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_getlevelpending(StringBuilder out_level_name);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int editor_getlevelpending_size();

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_getlevelpendingbinary([In, Out] byte[] out_level_data);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_clearlevelpending();


        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_change_color(float r, float g, float b, float a);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_grid_visibility(wbool show, float r, float g, float b, float a);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_set_selectioncolor(float r, float g, float b, float a);

        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_clean_level();

        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_update_level_content([In, Out] byte[] level_content, uint level_size);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_level_loaded();

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_entity_changezorder([MarshalAs(UnmanagedType.LPStr)] string entity_id, int new_zorder, GAME_TYPE type);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_entity_getrenderbox([MarshalAs(UnmanagedType.LPStr)] string entity_id, GAME_TYPE type, [In, Out] int[] render_box_size, [In, Out] float[] render_box_pos);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_setsoundvolume(short music_volume, short sfx_volume);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_addsound([MarshalAs(UnmanagedType.LPStr)] string sound_id, [In, Out] byte[] sound_data, uint sound_data_size, [MarshalAs(UnmanagedType.LPStr)] string map_id, int zorder);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_updatesound([MarshalAs(UnmanagedType.LPStr)] string sound_id, [In, Out] byte[] sound_data, uint sound_data_size);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_updatesoundposition([MarshalAs(UnmanagedType.LPStr)] string sound_id, float x, float y);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_removesound([MarshalAs(UnmanagedType.LPStr)] string sound_id);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_addlight([MarshalAs(UnmanagedType.LPStr)] string light_id, [In, Out] byte[] light_data, uint light_data_size, [MarshalAs(UnmanagedType.LPStr)] string map_id, int zorder);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_updatelight([MarshalAs(UnmanagedType.LPStr)] string light_id, [In, Out] byte[] light_data, uint light_data_size);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_updatelightposition([MarshalAs(UnmanagedType.LPStr)] string light_id, float x, float y);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_removelight([MarshalAs(UnmanagedType.LPStr)] string light_id);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_addparticles([MarshalAs(UnmanagedType.LPStr)] string particles_id, [In, Out] byte[] particles_data, uint particles_data_size, [MarshalAs(UnmanagedType.LPStr)] string map_id, int zorder);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_updateparticles([MarshalAs(UnmanagedType.LPStr)] string particles_id, [In, Out] byte[] particles_data, uint particles_data_size, [MarshalAs(UnmanagedType.LPStr)] string map_id);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_updateparticlesposition([MarshalAs(UnmanagedType.LPStr)] string particles_id, float x, float y);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void editor_removeparticles([MarshalAs(UnmanagedType.LPStr)] string particles_id);


    }
}
