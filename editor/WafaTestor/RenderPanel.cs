﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WafaEditorV2;
using WafaTestor.Properties;

namespace WafaTestor
{
    public partial class RenderPanel : Panel
    {
        private static bool _init = false;

        public RenderPanel() :base()
        {
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.DoubleBuffer, false);
            this.SetStyle(ControlStyles.Opaque, true);
            this.SetStyle(ControlStyles.ResizeRedraw, true);
            this.SetStyle(ControlStyles.UserPaint, true);
        }

        public void StartEngine(string raw_asset_path,string asset_path, string gamedll_path, string font, string light_prog, string shadow_prog, string mask_prog)
        {
            if (!_init)
            {
                if (NativeFunc.editor_start_engine(this.Handle, this.Width, this.Height, 0, 0, 0, 0, asset_path, gamedll_path, font, light_prog, shadow_prog, mask_prog, raw_asset_path) == NativeFunc.wbool.wfalse)
                {
                    MessageBox.Show("Erreur de chargement du moteur, les erreurs ont été enregistrées dans le fichier wafa.log, problème possible, différence entre la version de la game dll et la version du moteur", "Erreur chargement", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                NativeFunc.editor_set_tilesize(64);


                if (NativeFunc.editor_resx_exist("sound_high_icon_32.png") == NativeFunc.wbool.wfalse)
                {
                    byte[] sound_icon_content = BitmapUtilities.GetBitmapContent(Resources.sound_high_icon_32);
                    NativeFunc.editor_addtextureresx("sound_high_icon_32.png", 32, 32, sound_icon_content, (uint)sound_icon_content.Length);
                }

                if (NativeFunc.editor_resx_exist("lightbulb_icon_32.png") == NativeFunc.wbool.wfalse)
                {
                    byte[] light_icon_content = BitmapUtilities.GetBitmapContent(Resources.lightbulb_icon_32);
                    NativeFunc.editor_addtextureresx("lightbulb_icon_32.png", 32, 32, light_icon_content, (uint)light_icon_content.Length);
                }

                if (NativeFunc.editor_resx_exist("star_icon_32.png") == NativeFunc.wbool.wfalse)
                {
                    byte[] star_icon_content = BitmapUtilities.GetBitmapContent(Resources.star_icon_32);
                    NativeFunc.editor_addtextureresx("star_icon_32.png", 32, 32, star_icon_content, (uint)star_icon_content.Length);
                }

                if (NativeFunc.editor_resx_exist("waypoint.png") == NativeFunc.wbool.wfalse)
                {
                    byte[] waypoint_icon_content = BitmapUtilities.GetBitmapContent(Resources.waypoint_32);
                    NativeFunc.editor_addtextureresx("waypoint.png", 32, 32, waypoint_icon_content, (uint)waypoint_icon_content.Length);
                }


                if (NativeFunc.editor_resx_exist("collider.png") == NativeFunc.wbool.wfalse)
                {
                    byte[] collider_content = BitmapUtilities.GetBitmapContent(Resources.collider_32);
                    NativeFunc.editor_addtextureresx("collider.png", 32, 32, collider_content, (uint)collider_content.Length);
                }


                NativeFunc.editor_change_color(1.0f,1.0f,1.0f,1.0f);
                NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wfalse);

                NativeFunc.editor_setfps_color(1.0f, 0.0f, 0.0f, 1.0f);

                NativeFunc.editor_draw_debug_collisions(NativeFunc.wbool.wfalse);

                NativeFunc.editor_setenable_lighting(NativeFunc.wbool.wtrue);


                _init = true;
            }
        }

        public void StopEngine()
        {
            NativeFunc.editor_shutdown_engine();
            _init = false;
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            if (_init)
                NativeFunc.editor_draw_frame();
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {

        }
    }
}
