﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WafaTestor
{
    public class TestReport
    {
        public string TestFile { get; set; }
        public Dictionary<string,bool> TestResult { get; set; }

        public TestReport()
        {
            TestResult = new Dictionary<string,bool>();
        }

    }
}
