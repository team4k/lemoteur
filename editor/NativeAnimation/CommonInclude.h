#ifdef _MSC_VER
extern "C" {
#endif
#include "lauxlib.h"
#include "lualib.h"
#include <lua.h>

#ifdef _MSC_VER
}
#endif


#if defined(_WIN32)
#include <windows.h>
#else
#define __USE_BSD
#include <unistd.h>
#include <dlfcn.h>
#include <pthread.h>
#endif

#ifdef EMSCRIPTEN
#include <emscripten.h>
#include <SDL2/SDL.h>
#endif

#include <Render.h>


#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <time.h>
#include <wchar.h>
#include <math.h>
#include <string.h>
#include <sqlite3.h>
#include <locale.h>

#include <sndfile.h>

#ifdef EMSCRIPTEN
#include <SDL2/SDL_audio.h>
#else
#include <portaudio.h>
#endif

#include <Engine.h>

#include <Base/types.h>
#include <Base/functions.h>
#include <Base/Color.h>

#include <Base/geom.h>
#include <Utils/Hash.h>
#include <Utils/hashmap.h>
#include <Utils/tempallocator.h>
#include <Utils/circular_array.h>

#include <Threading/thread_func.h>
#include <Threading/threads.h>

#include <Render/render_manager.h>

#include <Graphics/TextureLoader.h>
#include <Resx/shader_loader.h>


#include <GameObjects/Characters.pb-c.h>

#include <Graphics/Animation.h>
#include <Graphics/Sprite.h>
#include <Graphics/Light.h>
#include <Graphics/LightManager.h>

#include <Graphics/TextureRegion.h>
#include <Graphics/SpriteBatch.h>
#include <Graphics/Mesh.h>

#include <Sound/Sound.h>
#include <Sound/snd_mngr.h>



#include <Scripting/ScriptEngine.h>
#include <Collisions/Collisions.h>
#include <Debug/geomdebug.h>
#include <Debug/Logprint.h>

#include <Graphics/FontLoader.h>

#include "GameObjects/Level.pb-c.h"

#include <Pathfinding/IndexPriorityQueue.h>
#include <Pathfinding/AStar.h>
#include <Pathfinding/pathfind_worker.h>


#include <Resx/Resources.h>
#include <Resx/localization.h>
#include <GameMngr/save_mngr.h>


#include <Graphics/TilemapLayer.h>
#include <GameObjects/Tilemap.h>

#include <Font/RenderText.h>

#include <chipmunk/chipmunk.h>
#include <Physics/Physics.h>


#include <GameObjects/entity.h>
#include <GameObjects/Trigger.h>
#include <GameObjects/text_entity.h>

#include <GameObjects/entity_group.h>

#include <GameObjects/level_stream.h>

#include <Config/config.h>

#include <GameMngr/input_mngr.h>
#include <game_functions.h>

#include <GameMngr/module_mngr.h>
#include <GameObjects/waypoint.h>
#include <GameObjects/Collider.h>
#include <GameObjects/map.h>

#include <Graphics/Effects/particle_object.h>
#include <GameObjects/particles_entity.h>

#include <GameObjects/Levelobject.h>

#include <Graphics/compound_sprite.h>

#include <Physics/Effects/shatter_effect.h>

#include <Graphics/Effects/screen_fill.h>
#include <Graphics/Effects/screen_fade.h>


#include <Graphics/geomdraw.h>

#include <Scripting/lua_wrap.h>

#include <Utils/url.h>
