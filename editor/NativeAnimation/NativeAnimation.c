#include <CommonInclude.h>
#include <NativeAnimation.h>

static lua_State* lua_state = NULL;
static render_manager_t rendermngr;
static resources_manager_t resx_mngr;
static sprite_t* animatedsprite = NULL;
static Vector_t spriteposition = vectorzero;
static texture_info* spritetexture = NULL;
static AnimationList* spriteanimation = NULL;
static sprite_t* onion_sprite = NULL;
static sprite_t* pin_sprite = NULL;
static wbool show_onion;
static wbool show_multi_sprite = wtrue;
static ColorA_t back_color;

char resources_path[260];
char config_path[260];
char script_path[260];
char app_path[WAFA_MAX_PATH];

const float MAX_FPS = 60.0f;
const float max_frame_time = 1000.0f / MAX_FPS;

TIME_PROF currentTime;
TIME_PROF end_time;
long time_elapsed;
long frameRenderTime;
long max_nsecframe_time;
long overtime;
long sleep_time;
int32_t num_fps;
int32_t count_frame;



wbool hasselectedcontact = wfalse;
uint32_t selected_contactpoint = 0;

hashmap_t multisprite_table;
hashmap_t multispritepos_table;


int32_t basewidth; 
int32_t baseheight; 
int16_t basetexturewidth; 
int16_t basetextureheight;

static void configure()
{
	lua_state = script_createstate();

}

static int init_render(render_manager_t* const render_mngr, HWND hwnd,int width,int height)
{
	get_app_path(app_path);
	logprint_setdir(app_path);
	
	render_init(render_mngr, width, height, "", "Demo Wafa engine", NULL, hwnd, wfalse, wfalse, filter_linear,60);


	//load shader
	shader_info* vert_shader = getshader_resx(&resx_mngr, "vertex_shader.shad", wfalse);
	shader_info* pix_shader = getshader_resx(&resx_mngr, "pixel_shader.shad", wtrue);


	SHADERPROGRAM_ID prog_id = render_create_program(render_mngr, "prog_common", vert_shader->shader_pointer, pix_shader->shader_pointer);

	render_use_program(render_mngr, prog_id);

	frameRenderTime = 0;

	overtime = 0;
	count_frame = 0;

	max_nsecframe_time = (long)(max_frame_time * 1000000L);

	time_elapsed = 0;

	sleep_time = 0;

	show_onion = wfalse;

	return 0;
}

void native_animation_init(HWND hwnd,int width,int height,const char* asset_path,wbool pshowonion)
{
	configure();
	init_resources(&resx_mngr, asset_path, &rendermngr);
	init_render(&rendermngr, hwnd, width, height);

	back_color = black_color;
	spriteposition.x = width * WFLOAT05;
	spriteposition.y = height * WFLOAT05;
	show_onion = pshowonion;

	hashmap_initalloc(&multisprite_table, 6, sizeof(sprite_t), 2);
	hashmap_initalloc(&multispritepos_table, 6, sizeof(Vector_t), 2);

}

wbool native_animation_editor_resx_exist(const char* resx_name)
{
	if (!hashtable_haskey(resx_mngr.resx_hash, resx_name))
		return wfalse;
	else
		return wtrue;
}



void native_animation_editor_addtextureresx(const char* texture_name, int16_t textureWidth, int16_t textureHeight, unsigned char* image_data, unsigned long data_size)
{
	add_resx(&resx_mngr, texture_name, texture, "", wfalse, NULL, NULL);
	settexture_resx(&resx_mngr, texture_name, textureWidth, textureHeight, image_data, data_size);
}

void native_animation_setonionvisibility(wbool pshowonion)
{
	show_onion = pshowonion;

	ColorA_t coloralpha = { 1.0f,1.0f,1.0f,0.75f };
	ColorA_t colorfull = { 1.0f,1.0f,1.0f,1.0f };

	if(show_onion)
		sprite_setcolor(animatedsprite, coloralpha);
	else
		sprite_setcolor(animatedsprite, colorfull);
}

void native_animation_setmultispritevisibility(wbool show)
{
	show_multi_sprite = show;
}


void native_animation_setbackgroundcolor(float r, float g, float b, float a)
{
	back_color.r = r;
	back_color.g = g;
	back_color.b = b;
	back_color.a = a;
}

void native_animation_loadsprite(int32_t width,int32_t height,int16_t texturewidth,int16_t textureheight, unsigned char* image_data, unsigned long data_size, unsigned char* anim_data, size_t anim_size)
{
	if (animatedsprite != NULL)
		return;

	animatedsprite = (sprite_t*)wf_calloc(1, sizeof(sprite_t));
	onion_sprite = (sprite_t*)wf_calloc(1, sizeof(sprite_t));

	if (spritetexture == NULL) {
		add_resx(&resx_mngr, "nativeanimation.png", texture, "", wfalse, NULL, NULL);
	}
	
	settexture_resx(&resx_mngr, "nativeanimation.png", texturewidth, textureheight, image_data, data_size);

	if (spriteanimation == NULL) {
		add_resx(&resx_mngr, "nativeanimation.awf", animation, "", wfalse, NULL, NULL);
	}

	setanimation_resx(&resx_mngr, "nativeanimation.awf", anim_data, anim_size);

	basewidth = width;
	baseheight = height;
	basetexturewidth = texturewidth;
	basetextureheight = textureheight;

	spritetexture = gettexture_resx(&resx_mngr, "nativeanimation.png");
	spriteanimation = getanimation_resx(&resx_mngr, "nativeanimation.awf");

	sprite_init(animatedsprite, width, height, texturewidth, textureheight, spritetexture, spriteanimation, 0, 0, &rendermngr, wfalse, 0, 1.0f, 1.0f,wtrue);
	sprite_init(onion_sprite, width, height, texturewidth, textureheight, spritetexture, spriteanimation, 0, 0, &rendermngr, wfalse, 0, 1.0f, 1.0f, wtrue);

	native_animation_setonionvisibility(show_onion);

	if (pin_sprite == NULL)
	{
		pin_sprite = (sprite_t*)wf_malloc(sizeof(sprite_t));
		memset(pin_sprite, 0, sizeof(sprite_t));

		sprite_init(pin_sprite, 24, 24, 24, 24, gettexture_resx(&resx_mngr, "push_pin_icon_24.png"), NULL, 0, 0, &rendermngr, wfalse, 0, 1.0f, 1.0f, wtrue);
	}
	
}

void native_animation_selectpin(uint32_t contactidx)
{
	selected_contactpoint = contactidx;
	hasselectedcontact = wtrue;
}

void native_animation_unselectpin()
{
	selected_contactpoint = 0;
	hasselectedcontact = wfalse;
}

wbool native_animation_canselectpin()
{
	if (animatedsprite == NULL || animatedsprite->current_anim == NULL)
		return wfalse;

	if (!animatedsprite->current_anim->ended) {
		return wfalse;
	}

	return wtrue;
}


wbool native_animation_movepin(float worldx, float worldy, float* newx, float* newy)
{
	if (animatedsprite == NULL || animatedsprite->current_anim == NULL)
		return wfalse;

	if (!animatedsprite->current_anim->ended) {
		return wfalse;
	}

	if (hasselectedcontact)
	{
		for (uint32_t contactidx = 0; contactidx < animatedsprite->current_anim->anim_def->attachpoint_values[animatedsprite->current_anim->frame_counter]->n_attachpointpos; contactidx++) {
			Avector* tmp = animatedsprite->current_anim->anim_def->attachpoint_values[animatedsprite->current_anim->frame_counter]->attachpointpos[contactidx];

			if (!tmp->isnull)
			{
				Vector_t vec = { spriteposition.x - (animatedsprite->width * 0.5f) + (wfloat)tmp->x, spriteposition.y - (animatedsprite->height * 0.5f) + (wfloat)tmp->y };

				Vector_t delta = { worldx - vec.x, worldy - vec.y};

				if (selected_contactpoint == contactidx) {
					tmp->x += delta.x;
					tmp->y += delta.y;

					//limit pin position to sprite bounds
					if (tmp->x < 0) {
						tmp->x = 0;
					}
					else if (tmp->x > animatedsprite->width) {
						tmp->x = animatedsprite->width;
					}

					if (tmp->y < 0) {
						tmp->y = 0;
					}
					else if (tmp->y > animatedsprite->height) {
						tmp->y = animatedsprite->height;
					}

					(*newx) = tmp->x;
					(*newy) = tmp->y;
					return wtrue;
				}
			}
		}
	}

	return wfalse;
}

void native_animation_unloadsprite()
{
	if (animatedsprite == NULL)
		return;

	sprite_free(animatedsprite);
	wf_free(animatedsprite);
	animatedsprite = NULL;

	sprite_free(onion_sprite);
	wf_free(onion_sprite);
	onion_sprite = NULL;
}

void native_animation_playanimation(const char* animation)
{
	if (animatedsprite == NULL)
		return;

	sprite_playanimation(animatedsprite, animation);
	sprite_playanimation(onion_sprite, animation);
	sprite_stopanimation(onion_sprite);
}

uint32_t native_animation_getframe()
{
	if (animatedsprite == NULL || animatedsprite->current_anim == NULL)
		return -1;


	return animatedsprite->current_anim->frame_counter;
}

void native_animation_setframe(int frame)
{
	if (animatedsprite == NULL || animatedsprite->current_anim == NULL)
		return;

	sprite_setframe(animatedsprite, frame, -1);

	int onionframe = frame + 1;

	if (onionframe > onion_sprite->current_anim->anim_def->frames[onion_sprite->current_anim->anim_def->n_frames - 1]) {
		onionframe = frame;
	}

	sprite_setframe(onion_sprite, frame + 1, -1);

}

void native_animation_stopanimation()
{
	if (animatedsprite == NULL)
		return;

	sprite_stopanimation(animatedsprite);
}

void native_animation_clearframe()
{
	render_clear_screen(&rendermngr);
}

void native_animation_addnewsprite(const char* name, float x, float y,const char* animation)
{
	if (hashmap_haskey(&multisprite_table, name)) {
		return;
	}

	sprite_t* newsprite = (sprite_t*)hashmap_push(&multisprite_table, name);

	sprite_init(newsprite, basewidth, baseheight, basetexturewidth, basetextureheight, spritetexture, spriteanimation, 0, 0, &rendermngr, wfalse, 0, 1.0f, 1.0f, wtrue);

	sprite_playanimation(newsprite, animation);

	Vector_t* newspritepos = (Vector_t*)hashmap_push(&multispritepos_table, name);
	newspritepos->x = x;
	newspritepos->y = y;
}

void native_animation_removesprite(const char* name)
{
	if (!hashmap_haskey(&multisprite_table, name)) {
		return;
	}

	hashmap_remove(&multisprite_table, name);
	hashmap_remove(&multispritepos_table, name);
}

void native_animation_setmultispriteposition(const char* name, float x, float y)
{
	if (!hashmap_haskey(&multispritepos_table, name)) {
		return;
	}

	Vector_t* newspritepos = (Vector_t*)hashmap_getvalue(&multispritepos_table, name);
	newspritepos->x = x;
	newspritepos->y = y;
}

void native_animation_setmultispriteanimation(const char* name, const char* animation)
{
	if (!hashmap_haskey(&multisprite_table, name)) {
		return;
	}

	sprite_t* newsprite = (sprite_t*)hashmap_getvalue(&multisprite_table, name);

	sprite_playanimation(newsprite, animation);
}

void native_animation_update_anim_def(unsigned char* anim_def_data, unsigned long data_size)
{
	AnimationDef* newdef = animation_def__unpack(NULL, data_size, anim_def_data);

	animatedsprite->current_anim->anim_def = newdef;
}

void native_animation_setzoom(int width, int height, float zoomfactor)
{
	rendermngr.zoom_factor = zoomfactor;
	rendermngr.screen_info.width = width;
	rendermngr.screen_info.height = height;

	spriteposition.x = (width * zoomfactor) * WFLOAT05;
	spriteposition.y = (height * zoomfactor) * WFLOAT05;

	render_update_screen(&rendermngr);
	//light_manager_resizerender(&editor_ptr->light_mngr, &editor_ptr->render_mngr);
}

void native_animation_renderframe()
{

	TIME_PROF newTime;

	time_get_time(&newTime);

	TIME_PROF ltime = time_diff(&currentTime, &newTime);

	time_elapsed = time_get_nsec(&ltime);

	float rnd = (float)frameRenderTime / 1000000L;

	if (time_elapsed >= 1000000000L)
	{
		num_fps = count_frame;
		count_frame = 0;
		time_get_time(&currentTime);
	}

	render_clear_screenwithcolor(&rendermngr, back_color);

	render_update_screen(&rendermngr);

	if (animatedsprite != NULL) {

		if (show_onion) {
			sprite_draw(onion_sprite, spriteposition.x, spriteposition.y, &rendermngr, NULL);
			sprite_update(onion_sprite, max_frame_time);
		}
		
		sprite_draw(animatedsprite, spriteposition.x, spriteposition.y, &rendermngr, NULL);
		sprite_update(animatedsprite, max_frame_time);


		if (show_multi_sprite)
		{
			uint32_t o_index = 0;
			uint32_t o_count = 0;


			for (sprite_t* c_sprite = (sprite_t*)hashmap_iterate(&multisprite_table, &o_index, &o_count); c_sprite != NULL; c_sprite = (sprite_t*)hashmap_iterate(&multisprite_table, &o_index, &o_count))
			{
				Vector_t* pos = (Vector_t*)hashmap_getvaluefromindex(&multispritepos_table, o_index - 1);

				Vector_t vec = { spriteposition.x + pos->x, spriteposition.y + pos->y };

				sprite_draw(c_sprite, vec.x, vec.y, &rendermngr, NULL);
			}
		}

		//show contact point for this frame
		if (animatedsprite->current_anim != NULL && animation_hascontactpoints(animatedsprite->current_anim)) {

			for (uint32_t contactidx = 0; contactidx < animatedsprite->current_anim->anim_def->attachpoint_values[animatedsprite->current_anim->frame_counter]->n_attachpointpos; contactidx++) {
				Avector* tmp = animatedsprite->current_anim->anim_def->attachpoint_values[animatedsprite->current_anim->frame_counter]->attachpointpos[contactidx];

				if (!tmp->isnull)
				{
					Vector_t vec = { spriteposition.x - (animatedsprite->width * 0.5f) + (wfloat)tmp->x, spriteposition.y - (animatedsprite->height * 0.5f) + (wfloat)tmp->y };

					if (hasselectedcontact && selected_contactpoint == contactidx) {
						ColorA_t bluecoloralpha = { 0.0f,0.83f,1.0f,1.0f };
						sprite_setcolor(pin_sprite, bluecoloralpha);
					}

					sprite_draw(pin_sprite, vec.x, vec.y, &rendermngr, NULL);

					sprite_setcolor(pin_sprite, white_color);
				}
			}

		}
	}
		

	render_swap_buffers(&rendermngr);

	time_get_time(&end_time);

	TIME_PROF frame_rnd_struct = time_diff(&newTime, &end_time);

	frameRenderTime = time_get_nsec(&frame_rnd_struct);

	if (frameRenderTime < max_nsecframe_time)
	{
		sleep_time = max_nsecframe_time - frameRenderTime;

		if (sleep_time < 0)
			sleep_time = 0;

		if (overtime > 0)
		{
			if (overtime > sleep_time)
			{
				overtime -= sleep_time;
				sleep_time = 0;
			}
			else
			{
				sleep_time -= overtime;
				overtime = 0;
			}
		}

		nano_sleep(sleep_time, wtrue);
	}
	else //over maximum frame time, store the difference to reduce sleep time of further frames
	{
		overtime += frameRenderTime - max_nsecframe_time;
	}

	count_frame++;
}

