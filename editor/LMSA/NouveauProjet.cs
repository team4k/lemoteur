﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LMS
{
    public partial class NouveauProjet : Form
    {
        public NouveauProjet()
        {
            InitializeComponent();
        }

        private void btncreerprojet_Click(object sender, EventArgs e)
        {
            if(txtNouveauProjet.Text.Trim().Length == 0)
            {
                MessageBox.Show("Le nom du projet ne doit pas être vide ! ");
                return;
            }

            this.DialogResult = DialogResult.OK;
        }
    }
}
