﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Security;

namespace LMS
{
    public class NativeAnimationFuncs
    {
        public enum wbool { wfalse = 0, wtrue = 1 }

        public const string nativeAnimationPath = "NativeAnimation";

        [SuppressUnmanagedCodeSecurity]
        [DllImport(nativeAnimationPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void native_animation_init(IntPtr hwnd, int width, int height, [MarshalAs(UnmanagedType.LPStr)] string asset_path, wbool showonion);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(nativeAnimationPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void native_animation_setonionvisibility(wbool showonion);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(nativeAnimationPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void native_animation_setmultispritevisibility(wbool showmultisprite);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(nativeAnimationPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void native_animation_setbackgroundcolor(float r, float g, float b, float a);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(nativeAnimationPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void native_animation_loadsprite(int width, int height, int texturewidth, int textureheight, [In, Out] byte[] image_data, uint data_size, [In, Out] byte[] anim_data, uint anim_size);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(nativeAnimationPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void native_animation_unloadsprite();

        [SuppressUnmanagedCodeSecurity]
        [DllImport(nativeAnimationPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void native_animation_playanimation([MarshalAs(UnmanagedType.LPStr)] string animation);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(nativeAnimationPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void native_animation_selectpin(uint contactidx);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(nativeAnimationPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void native_animation_unselectpin();

        [SuppressUnmanagedCodeSecurity]
        [DllImport(nativeAnimationPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern wbool native_animation_canselectpin();

        [SuppressUnmanagedCodeSecurity]
        [DllImport(nativeAnimationPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern wbool native_animation_movepin(float worldx,float worldy, [Out] out float newx, [Out] out float newy);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(nativeAnimationPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern uint native_animation_getframe();

        [SuppressUnmanagedCodeSecurity]
        [DllImport(nativeAnimationPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void native_animation_addnewsprite([MarshalAs(UnmanagedType.LPStr)] string name, float x, float y, [MarshalAs(UnmanagedType.LPStr)] string animation);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(nativeAnimationPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void native_animation_removesprite([MarshalAs(UnmanagedType.LPStr)] string name);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(nativeAnimationPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void native_animation_setmultispriteposition([MarshalAs(UnmanagedType.LPStr)] string name, float x, float y);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(nativeAnimationPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void native_animation_setmultispriteanimation([MarshalAs(UnmanagedType.LPStr)] string name, [MarshalAs(UnmanagedType.LPStr)] string animation);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(nativeAnimationPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void native_animation_setframe(int frame);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(nativeAnimationPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void native_animation_stopanimation();

        [SuppressUnmanagedCodeSecurity]
        [DllImport(nativeAnimationPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void native_animation_clearframe();

        [SuppressUnmanagedCodeSecurity]
        [DllImport(nativeAnimationPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void native_animation_setzoom(int width, int height, float zoomfactor);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(nativeAnimationPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void native_animation_renderframe();

        [SuppressUnmanagedCodeSecurity]
        [DllImport(nativeAnimationPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern wbool native_animation_editor_resx_exist([MarshalAs(UnmanagedType.LPStr)] string resx_name);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(nativeAnimationPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void native_animation_editor_addtextureresx([MarshalAs(UnmanagedType.LPStr)] string texture_name, int width, int height, [In, Out] byte[] image_data, uint data_size);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(nativeAnimationPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void native_animation_update_anim_def([In, Out] byte[] anim_def_data, uint data_size);
    }
}
