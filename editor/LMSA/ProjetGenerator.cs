﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using PBInterface;

namespace LMS
{
    public static class ProjetGenerator
    {

        public static XDocument GenerateProjectFromAnimationList(string nomprojet,string gamedir, AnimationList animationdata)
        {
            List<XElement> xanimations = new List<XElement>();

            foreach (AnimationDef def in animationdata.AnimArrays)
            {
                List<XElement> xframes = new List<XElement>();

                List<XElement> xanchornames = new List<XElement>();

                foreach(string anchorname in def.AttachpointIds)
                {
                    xanchornames.Add(new XElement("anchorname", anchorname));
                }



                for (int frame = 0; frame < def.Frames.Length; frame++)
                {
                    List<XElement> xanchors = new List<XElement>();

                    for (int apoint = 0; apoint < def.AttachpointValues[frame].Attachpointpos.Count;apoint++)
                    {
                        if (def.AttachpointValues[frame].Attachpointpos[apoint].Isnull)
                            continue;

                        xanchors.Add(new XElement("anchor",
                            new XAttribute("idname", apoint),
                            new XAttribute("posx", def.AttachpointValues[frame].Attachpointpos[apoint].X),
                            new XAttribute("posy", def.AttachpointValues[frame].Attachpointpos[apoint].Y)
                            ));
                    }


                    // def.Frames
                    int offsetx = def.Offsetx + (frame * def.Width);
                    int offsety = def.Offsety;

                    List<object> nodecontent = new List<object>();

                    nodecontent.Add(new XAttribute("id", def.Frames[frame]));
                    nodecontent.Add(new XAttribute("delay", def.Timesteps[frame]));
                    nodecontent.Add(new XElement("offsetx", offsetx));
                    nodecontent.Add(new XElement("offsety", offsety));
                    nodecontent.Add(new XElement("capturewidth", def.Width));
                    nodecontent.Add(new XElement("captureheight", def.Height));
                    nodecontent.Add(new XElement("baseimage", "frame" + def.Frames[frame] + ".png"));

                    if(xanchors.Count > 0)
                    {
                        nodecontent.Add(new XElement("anchors", xanchors.ToArray()));
                    }

                    XElement xframe = new XElement("frame", nodecontent.ToArray());

                    xframes.Add(xframe);
                }

                List<object> animnodecontent = new List<object>();

                animnodecontent.Add(new XAttribute("name", def.Name));
                animnodecontent.Add(new XAttribute("loop", def.Loop));

                if(xanchornames.Count > 0)
                {
                    animnodecontent.Add(new XElement("anchornames", xanchornames.ToArray()));
                }

                animnodecontent.Add(new XElement("frames",xframes.ToArray()));

                XElement xanim = new XElement("animation", animnodecontent.ToArray());

                xanimations.Add(xanim);
            }


            return new XDocument(
                    new XDeclaration("1.0", "utf-8", "yes"),
                    new XElement("lmsprojet",
                        new XElement("nom", nomprojet),
                        new XElement("datesauvegarde", DateTime.Now.ToString()),
                        new XElement("dernieroutput", DateTime.Now.ToString()),
                        new XElement("animations",
                        xanimations.ToArray())
                        )
                );

            
        }


        public static int GetMinFrame(XElement animation)
        {
            IEnumerable<XAttribute> all = from frame in animation.Element("frames").Elements("frame").Attributes("id") select frame;
            return Convert.ToInt32(all.Where<XAttribute>(a => Convert.ToInt32(a.Value) == all.Min<XAttribute>(x => Convert.ToInt32(x.Value))).SingleOrDefault<XAttribute>().Value);
        }

        public static int GetNextFrame(XDocument projet)
        {
            if (projet.Element("lmsprojet").Element("animations") == null)
            {
                return 0;
            }

            int maxframe = ProjetGenerator.GetMaxFrame(projet);

            return maxframe + 1;
        }

        public static int GetMaxFrame(XDocument projet)
        {
            if (projet.Element("lmsprojet").Element("animations") == null)
            {
                return 0;
            }

            IEnumerable<XElement> all = from frame in projet.Element("lmsprojet").Element("animations").Elements("animation").Elements<XElement>("frames").Elements<XElement>("frame") select frame;

            if (all.Count<XElement>() > 0)
                return Convert.ToInt32(all.Where<XElement>(a => Convert.ToInt32(a.Attribute("id").Value) == all.Max<XElement>(x => Convert.ToInt32(x.Attribute("id").Value))).SingleOrDefault<XElement>().Attribute("id").Value);

            return 0;
        }

        public static int GetMaxOffsetY(XDocument projet)
        {
            if (projet.Element("lmsprojet").Element("animations") == null)
            {
                return 0;
            }

            IEnumerable<XElement> all = from frame in projet.Element("lmsprojet").Element("animations").Elements("animation").Elements<XElement>("frames").Elements<XElement>("frame").Elements<XElement>("offsety") select frame;

            if (all.Count<XElement>() > 0)
                return Convert.ToInt32(all.Where<XElement>(a => Convert.ToInt32(a.Value) == all.Max<XElement>(x => Convert.ToInt32(x.Value))).FirstOrDefault<XElement>().Value);

            return 0;
        }

        public static int GetNextOffsetY(XDocument projet)
        {
            if (projet.Element("lmsprojet").Element("animations") == null)
            {
                return 0;
            }

            int maxy = ProjetGenerator.GetMaxOffsetY(projet);

            XElement captureheight = (from frame in projet.Element("lmsprojet").Element("animations").Elements("animation").Elements<XElement>("frames").Elements<XElement>("frame").Elements<XElement>("captureheight") select frame).FirstOrDefault<XElement>();

            if (captureheight != null)
            {
                return maxy + Convert.ToInt32(captureheight.Value);
            }

            return 0;
        }

        public static void AddAnimationToProject(XDocument projet,AnimationDef newanimation)
        {
            List<XElement> xframes = new List<XElement>();

            List<XElement> xanchornames = new List<XElement>();

            foreach (string anchorname in newanimation.AttachpointIds)
            {
                xanchornames.Add(new XElement("anchorname", anchorname));
            }


            for (int frame = 0; frame < newanimation.Frames.Length; frame++)
            {
                List<XElement> xanchors = new List<XElement>();

                if(newanimation.AttachpointValues.Count > frame)
                {
                    for (int apoint = 0; apoint < newanimation.AttachpointValues[frame].Attachpointpos.Count; apoint++)
                    {
                        if (newanimation.AttachpointValues[frame].Attachpointpos[apoint].Isnull)
                            continue;

                        xanchors.Add(new XElement("anchor",
                            new XAttribute("idname", apoint),
                            new XAttribute("posx", newanimation.AttachpointValues[frame].Attachpointpos[apoint].X),
                            new XAttribute("posy", newanimation.AttachpointValues[frame].Attachpointpos[apoint].Y)
                            ));
                    }
                }
                

                // def.Frames
                int offsetx = newanimation.Offsetx + (frame * newanimation.Width);
                int offsety = newanimation.Offsety;

                List<object> nodecontent = new List<object>();

                nodecontent.Add(new XAttribute("id", newanimation.Frames[frame]));
                nodecontent.Add(new XAttribute("delay", newanimation.Timesteps[frame]));
                nodecontent.Add(new XElement("offsetx", offsetx));
                nodecontent.Add(new XElement("offsety", offsety));
                nodecontent.Add(new XElement("capturewidth", newanimation.Width));
                nodecontent.Add(new XElement("captureheight", newanimation.Height));
                nodecontent.Add(new XElement("baseimage", "frame" + newanimation.Frames[frame] + ".png"));

                if (xanchors.Count > 0)
                {
                    nodecontent.Add(new XElement("anchors", xanchors.ToArray()));
                }

                XElement xframe = new XElement("frame", nodecontent.ToArray());

                xframes.Add(xframe);
            }

            List<object> animnodecontent = new List<object>();

            animnodecontent.Add(new XAttribute("name", newanimation.Name));
            animnodecontent.Add(new XAttribute("loop", newanimation.Loop));

            if (xanchornames.Count > 0)
            {
                animnodecontent.Add(new XElement("anchornames", xanchornames.ToArray()));
            }

            animnodecontent.Add(new XElement("frames", xframes.ToArray()));

            XElement xanim = new XElement("animation", animnodecontent.ToArray());

            if (projet.Element("lmsprojet").Element("animations") == null)
            {
                projet.Element("lmsprojet").Add(new XElement("animations"));
            }

            projet.Element("lmsprojet").Element("animations").Add(xanim);
        }

        public static void UpdateAnimation(XDocument projet,AnimationDef animation)
        {
            if (projet.Element("lmsprojet").Element("animations") == null)
            {
                return;
            }

            foreach (XElement element in projet.Element("lmsprojet").Element("animations").Elements())
            {
                if (element.Attribute("name").Value == animation.Name)
                {
                    element.SetAttributeValue("loop", animation.Loop);
                    break;
                }
            }
        }

        public static void ResizeFrames(XDocument projet, int newwidth,int newheight)
        {
            int newoffsetx = 0;
            int newoffsety = 0;

            foreach (XElement elementanim in projet.Element("lmsprojet").Element("animations").Elements())
            {
                foreach(XElement elementframe in elementanim.Element("frames").Elements())
                {
                    elementframe.Element("offsetx").Value = newoffsetx.ToString();
                    elementframe.Element("offsety").Value = newoffsety.ToString();
                    elementframe.Element("capturewidth").Value = newwidth.ToString();
                    elementframe.Element("captureheight").Value = newheight.ToString();
                    newoffsetx += newwidth;
                }
                newoffsetx = 0;
                newoffsety += newheight;
            }
        }

        public static AnimationList GetAnimationListFromProject(XDocument projet,string projetdirectory,out Bitmap fulltexture)
        {
            AnimationList list = new AnimationList();

            List<Bitmap> animationsTextures = new List<Bitmap>();

            list.Id = projet.Root.Element("nom").Value;

            string rootframepath = Path.Combine(projetdirectory, "3_frames");

            int fulltexturewidth = 0;
            int fulltextureheight = 0;


            foreach (XElement element in projet.Root.Elements())
            {
                if(element.Name == "animations")
                {
                    foreach(XElement animele in element.Elements())
                    {
                        AnimationDef def = new AnimationDef();
                        def.Name = animele.Attribute("name").Value;
                        def.Loop = Convert.ToBoolean(animele.Attribute("loop").Value);

                        List<int> frames = new List<int>();
                        List<float> delay = new List<float>();
                        List<Bitmap> framestextures = new List<Bitmap>();

                        int capturewidth = 0;
                        int captureheight = 0;

                        if (animele.Element("anchornames") != null)
                        {
                            foreach (XElement anchornameele in animele.Element("anchornames").Elements())
                            {
                                def.AttachpointIds.Add(anchornameele.Value);
                            }
                        }
                        

                        int iframe = 0;

                        foreach (XElement frameele in animele.Element("frames").Elements())
                        {
                            AnimPoints points = new AnimPoints();

                            for(int ianchor = 0;ianchor < def.AttachpointIds.Count;ianchor++)
                            {
                                Avector posanchor = new Avector();
                                posanchor.Isnull = true;
                                points.Attachpointpos.Add(posanchor);
                            }

                            def.AttachpointValues.Add(points);

                            if (frameele.Element("anchors") != null)
                            {
                                foreach (XElement anchorele in frameele.Element("anchors").Elements())
                                {
                                    int idname = Convert.ToInt32(anchorele.Attribute("idname").Value);
                                    int posx = Convert.ToInt32(anchorele.Attribute("posx").Value);
                                    int posy = Convert.ToInt32(anchorele.Attribute("posy").Value);

                                    def.AttachpointValues[iframe].Attachpointpos[idname].Isnull = false;
                                    def.AttachpointValues[iframe].Attachpointpos[idname].X = posx;
                                    def.AttachpointValues[iframe].Attachpointpos[idname].Y = posy;
                                }
                            }


                            int offsetx = Convert.ToInt32(frameele.Element("offsetx").Value);
                            int offsety = Convert.ToInt32(frameele.Element("offsety").Value);

                            if (frames.Count == 0)
                            {
                                def.Offsetx = offsetx;
                                def.Offsety = offsety;
                            }

                            frames.Add(Convert.ToInt32(frameele.Attribute("id").Value));

                            float fdelay = 0;

                            if(!float.TryParse(frameele.Attribute("delay").Value,NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out fdelay)) {
                                float.TryParse(frameele.Attribute("delay").Value, out fdelay);
                            }

                            delay.Add(fdelay); 
                            framestextures.Add(new Bitmap(Bitmap.FromFile(Path.Combine(rootframepath, frameele.Element("baseimage").Value))));

                            capturewidth = Convert.ToInt32(frameele.Element("capturewidth").Value);
                            captureheight = Convert.ToInt32(frameele.Element("captureheight").Value);


                            if (capturewidth + offsetx > fulltexturewidth)
                            {
                                fulltexturewidth = capturewidth + offsetx;
                            }

                            if (captureheight + offsety > fulltextureheight)
                            {
                                fulltextureheight = captureheight + offsety;
                            }

                            iframe++;
                        }

                        animationsTextures.Add(BitmapUtilities.MergeFrames(framestextures.ToArray(), capturewidth, captureheight));

                        //cleanup temporary bitmaps after merge
                        foreach(Bitmap tmpbmp in framestextures)
                        {
                            tmpbmp.Dispose();
                        }

                        def.Frames = frames.ToArray();
                        def.Timesteps = delay.ToArray();
                        def.Width = capturewidth;
                        def.Height = captureheight;

                        list.AnimArrays.Add(def);

                    }
                }
            }

            if(animationsTextures.Count > 0)
            {
                fulltexture = BitmapUtilities.MergeBitmaps(animationsTextures.ToArray(), fulltexturewidth, fulltextureheight, list.AnimArrays);
            }
            else
            {
                fulltexture = null;
            }
            

            //cleanup temporary bitmaps after merge
            foreach(Bitmap tmpbmp in animationsTextures)
            {
                tmpbmp.Dispose();
            }

            return list;
        }


        public static void AddAnchoredAnimationToProject(XDocument projet,AnchorData data)
        {
            if(projet.Element("lmsprojet").Element("multisprites") == null)
            {
                projet.Element("lmsprojet").Add(new XElement("multisprites"));
            }

            XElement xanchoranim = new XElement("sprite", 
                        new XAttribute("id", data.Id),
                        new XAttribute("animation", data.BaseAnimation),
                        new XAttribute("srcanchor", data.SrcAnchorName),
                        new XAttribute("attachanchor", data.AnchorName));

            projet.Element("lmsprojet").Element("multisprites").Add(xanchoranim);
        }

        public static void EditAnchoredAnimation(XDocument projet, AnchorData data)
        {
            if (projet.Element("lmsprojet").Element("multisprites") == null)
            {
                return;
            }

            foreach (XElement element in projet.Element("lmsprojet").Element("multisprites").Elements())
            {
                if (element.Attribute("id").Value == data.Id)
                {
                    element.Attribute("animation").SetValue(data.BaseAnimation);
                    element.Attribute("srcanchor").SetValue(data.SrcAnchorName);
                    element.Attribute("attachanchor").SetValue(data.AnchorName);
                }
            }
        }

        public static bool RemoveAnchoredAnimation(XDocument projet, string name)
        {
            if (projet.Element("lmsprojet").Element("multisprites") == null)
            {
                return false;
            }

            foreach (XElement element in projet.Element("lmsprojet").Element("multisprites").Elements())
            {
                if (element.Attribute("id").Value == name)
                {
                    element.Remove();
                    return true;
                }
            }

            return false;
        }

        public static bool UpdateFrame(XDocument project, FrameData data)
        {
            XElement updframe = (from frame in project.Element("lmsprojet").Element("animations").Elements("animation").Elements<XElement>("frames").Elements<XElement>("frame").Where<XElement>(a => Convert.ToInt32(a.Attribute("id").Value) == data.FramePos) select frame).FirstOrDefault<XElement>();

            if (updframe == null)
            {
                return false;
            }
            else
            {
                updframe.SetAttributeValue("delay", data.Delay);
                return true;
            }
        }

        public static bool RemoveFrame(XDocument project,int id)
        {
            XElement rmframe = (from frame in project.Element("lmsprojet").Element("animations").Elements("animation").Elements<XElement>("frames").Elements<XElement>("frame").Where<XElement>(a => Convert.ToInt32(a.Attribute("id").Value) == id) select frame).FirstOrDefault<XElement>();

            if(rmframe == null)
            {
                return false;
            }
            else
            {
                rmframe.Remove();
            }

            //renumber following frames

            IEnumerable<XElement> allframes = from frame in project.Element("lmsprojet").Element("animations").Elements("animation").Elements<XElement>("frames").Elements<XElement>("frame").Where<XElement>(a => Convert.ToInt32(a.Attribute("id").Value) > id) select frame;

            foreach(XElement nframe in allframes)
            {
                int framenumber = Convert.ToInt32(nframe.Attribute("id").Value);

                nframe.SetAttributeValue("id", ++framenumber);
            }

            return true;
        }

        public static Dictionary<string, AnchorData> GetAnchoredAnimationFromProject(XDocument projet, AnimationList _reflist)
        {
            Dictionary<string, AnchorData> data = new Dictionary<string, AnchorData>();


            foreach (XElement element in projet.Root.Elements())
            {
                if (element.Name == "multisprites")
                {
                    foreach (XElement sprite in element.Elements())
                    {
                        AnimationDef srcanimation = null;
                        //animation source
                        foreach (AnimationDef animation in _reflist.AnimArrays)
                        {
                            if (animation.Name == sprite.Attribute("animation").Value)
                            {
                                srcanimation = animation;
                                break;
                            }
                        }

                        Avector Srcvector = null;
                        //récupération de la position de l'ancre source
                        for (int anchorpos = 0; anchorpos < srcanimation.AttachpointIds.Count; anchorpos++)
                        {
                            if (srcanimation.AttachpointIds[anchorpos] == sprite.Attribute("srcanchor").Value)
                            {
                                Srcvector = srcanimation.AttachpointValues[0].Attachpointpos[anchorpos];
                                break;
                            }
                        }

                        data.Add(sprite.Attribute("id").Value, new AnchorData(sprite.Attribute("id").Value, sprite.Attribute("srcanchor").Value, sprite.Attribute("attachanchor").Value, Srcvector, sprite.Attribute("animation").Value));
                    }
                }
            }

            return data;
        }

        public static List<string> GetAllAnchorsFromProject(XDocument projet)
        {
            List<string> anchors = new List<string>();

            List<XElement> animations = projet.Element("lmsprojet").Element("animations").Elements("animation").ToList<XElement>();

            foreach(XElement ele in animations)
            {
                if(ele.Elements("anchornames").Count<XElement>() == 0)
                {
                    continue;
                }

                if (ele.Element("anchornames").Elements("anchorname").Count<XElement>() == 0)
                {
                    continue;
                }

                List <XElement> anchorsele =  ele.Element("anchornames").Elements("anchorname").ToList<XElement>();
                
                foreach(XElement anchor in anchorsele)
                {
                    if(!anchors.Contains(anchor.Value))
                    {
                        anchors.Add(anchor.Value);
                    }
                }

            }

            return anchors;

        }

        public static void SetNewAnchorPos(XDocument projet,string animationame,int frameid,int anchorid,int posx,int posy)
        {
            XElement animation = projet.Element("lmsprojet").Element("animations").Elements("animation").Where<XElement>(a => a.Attribute("name").Value == animationame).SingleOrDefault<XElement>();

            int minframe = ProjetGenerator.GetMinFrame(animation);

            IEnumerable<XElement> frames = from frame in animation.Element("frames").Elements("frame") select frame;

            if (frames.Count<XElement>() > 0)
            {
                IEnumerable<XElement> anchors = frames.Where<XElement>(a => Convert.ToInt32(a.Attribute("id").Value) == minframe + frameid).SingleOrDefault<XElement>().Element("anchors").Elements("anchor");

                if(anchors.Count<XElement>() > 0)
                {
                    XElement anchor = anchors.Where<XElement>(a => Convert.ToInt32(a.Attribute("idname").Value) == anchorid).SingleOrDefault<XElement>();

                    if(anchor != null)
                    {
                        anchor.SetAttributeValue("posx", posx);
                        anchor.SetAttributeValue("posy", posy);
                    }
                }
            }
                

        }

        public static void AddAnchorToAnimation(XDocument projet, string animationame, int posanchor, string anchorname, bool newanchor, int posx, int posy)
        {
            XElement animation = projet.Element("lmsprojet").Element("animations").Elements("animation").Where<XElement>(a => a.Attribute("name").Value == animationame).SingleOrDefault<XElement>();

            //ajout du nom de l'ancre sur chaque animation (même si non utilisée sur l'animation)
            if (newanchor)
            {
                IEnumerable<XElement> animations = projet.Element("lmsprojet").Element("animations").Elements("animation");

                foreach (XElement anim in animations)
                {
                    if(anim.Elements("anchornames").Count<XElement>() == 0)
                    {
                        XElement xanchors = new XElement("anchornames");
                        anim.Add(xanchors);
                    }

                    XElement xanchor = new XElement("anchorname");
                    xanchor.SetValue(anchorname);

                    anim.Element("anchornames").Add(xanchor);
                }
            }
            

            //ajout de l'ancre sur chaque frames de l'anim

            IEnumerable<XElement> frames = from frame in animation.Element("frames").Elements("frame") select frame;

            if (frames.Count<XElement>() > 0)
            {
                foreach (XElement frame in frames)
                {
                    if (frame.Elements("anchors").Count<XElement>() == 0)
                    {
                        XElement xanchors = new XElement("anchors");
                        frame.Add(xanchors);
                    }

                    XElement xanchordata = new XElement("anchor",
                      new XAttribute("idname", posanchor),
                      new XAttribute("posx", posx),
                      new XAttribute("posy", posy));

                    frame.Element("anchors").Add(xanchordata);
                }
            }
        }

        public static List<string> RemoveAnchor(XDocument projet, string anchorname)
        {
            List<string> multispritestorm = new List<string>();

            IEnumerable<XElement> animations = projet.Element("lmsprojet").Element("animations").Elements("animation");

            foreach (XElement anim in animations)
            {
                if (anim.Elements("anchornames").Count<XElement>() > 0)
                {
                    IEnumerable <XElement> anchornames = anim.Elements("anchornames").Elements("anchorname");
                    int anchorpos = -1;

                    for(int ianchor = 0;ianchor < anchornames.Count<XElement>();ianchor++)
                    {
                        if(anchornames.ElementAt<XElement>(ianchor).Value == anchorname)
                        {
                            anchorpos = ianchor;
                            anchornames.ElementAt<XElement>(ianchor).Remove();
                            break;
                        }
                    }

                    if(anchorpos == -1)
                    {
                        continue;
                    }

                    //mettre à jour les références à cette ancre dans chaque frame
                    IEnumerable<XElement> frames = anim.Elements("frames").Elements("frame");

                    foreach(XElement frame in frames)
                    {
                        IEnumerable<XElement> anchorsdef = frame.Elements("anchors").Elements("anchor");

                        foreach(XElement anchordef in anchorsdef)
                        {
                            int idname = Convert.ToInt32(anchordef.Attribute("idname").Value);

                            if(idname == anchorpos)
                            {
                                anchordef.Remove();
                            }
                            else if(idname > anchorpos)
                            {
                                idname--;
                                anchordef.Attribute("idname").Value = idname.ToString();
                            }

                        }
                    }

                }
            }
            

            //récupération des multisprites à supprimer (lié à notre ancre supprimée)
            IEnumerable<XElement> multisprites = projet.Element("lmsprojet").Element("multisprites").Elements("sprite");

            foreach(XElement sprite in multisprites)
            {
                if(sprite.Attribute("srcanchor").Value == anchorname || sprite.Attribute("attachanchor").Value == anchorname)
                {
                    multispritestorm.Add(sprite.Attribute("id").Value);
                }
            }

            return multispritestorm;
        }
    }
}
