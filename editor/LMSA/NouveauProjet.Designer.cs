﻿namespace LMS
{
    partial class NouveauProjet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNouveauProjet = new System.Windows.Forms.TextBox();
            this.lblnomproj = new System.Windows.Forms.Label();
            this.btncreerprojet = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtNouveauProjet
            // 
            this.txtNouveauProjet.Location = new System.Drawing.Point(142, 23);
            this.txtNouveauProjet.Name = "txtNouveauProjet";
            this.txtNouveauProjet.Size = new System.Drawing.Size(346, 20);
            this.txtNouveauProjet.TabIndex = 0;
            // 
            // lblnomproj
            // 
            this.lblnomproj.AutoSize = true;
            this.lblnomproj.Location = new System.Drawing.Point(12, 26);
            this.lblnomproj.Name = "lblnomproj";
            this.lblnomproj.Size = new System.Drawing.Size(124, 13);
            this.lblnomproj.TabIndex = 1;
            this.lblnomproj.Text = "Nom du nouveau projet :";
            // 
            // btncreerprojet
            // 
            this.btncreerprojet.Location = new System.Drawing.Point(181, 56);
            this.btncreerprojet.Name = "btncreerprojet";
            this.btncreerprojet.Size = new System.Drawing.Size(144, 23);
            this.btncreerprojet.TabIndex = 2;
            this.btncreerprojet.Text = "Créer un nouveau projet";
            this.btncreerprojet.UseVisualStyleBackColor = true;
            this.btncreerprojet.Click += new System.EventHandler(this.btncreerprojet_Click);
            // 
            // NouveauProjet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(520, 91);
            this.Controls.Add(this.btncreerprojet);
            this.Controls.Add(this.lblnomproj);
            this.Controls.Add(this.txtNouveauProjet);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NouveauProjet";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Créer un nouveau projet";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblnomproj;
        private System.Windows.Forms.Button btncreerprojet;
        public System.Windows.Forms.TextBox txtNouveauProjet;
    }
}