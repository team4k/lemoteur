﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LMS
{
    public partial class AnchorAdd : Form
    {
        private List<string> loadedanchors = null;

        public AnchorAdd(List<string> availablesanchors)
        {
            InitializeComponent();

            cmbAnchors.Items.AddRange(availablesanchors.ToArray());

            loadedanchors = availablesanchors;
        }

        private void btnok_Click(object sender, EventArgs e)
        {
            string anchor = string.Empty;

            if (cmbAnchors.SelectedItem != null)
            {
                anchor = cmbAnchors.SelectedItem.ToString();
            }
            else
            {
                anchor = cmbAnchors.Text;
            }

            if(string.IsNullOrEmpty(anchor))
            {
                MessageBox.Show("Veuillez sélectionner un nom d'ancre existant ou saisir un nouveau nom","Aucun nom d'ancre",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                return;
            }


            this.DialogResult = DialogResult.OK;
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            this.Close();
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
