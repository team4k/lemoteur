﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS
{
    public static class Utils
    {
        public static NativeAnimationFuncs.wbool convertbooltowbool(bool toconvert)
        {
            return (toconvert) ? NativeAnimationFuncs.wbool.wtrue : NativeAnimationFuncs.wbool.wfalse;
        }

    }
}
