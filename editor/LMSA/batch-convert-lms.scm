; GIMP plugin to convert bmp file created by LMS into a new gimp file
;https://www.gimp.org/tutorials/Basic_Batch/
;https://groups.csail.mit.edu/mac/ftpdir/scheme-7.4/doc-html/scheme_7.html
;https://stackoverflow.com/questions/50097345/concatenate-string-and-variable-in-script-fu-tinyscheme
;https://daviesmediadesign.com/how-to-install-scripts-in-gimp-3-easy-steps/
;https://stackoverflow.com/questions/9955834/how-do-i-output-info-to-the-console-in-a-gimp-python-script

;gimp -i -b '(batch-convert-lms "*.png")' -b '(gimp-quit 0)'


(define (batch-convert-lms c)
(let* ((filelist (cadr (file-glob pattern 1))))
 (while (not (null? filelist))
 (let* ((filename (car filelist))
 (image (car (gimp-file-load RUN-NONINTERACTIVE filename filename)))
 (drawable (car (gimp-image-get-active-layer image))))
 (string-append filename ".xcf")
 (gimp-file-save RUN-NONINTERACTIVE image drawable filename filename)
 (gimp-image-delete image))
 (display "hello" (current-output-port))
(set! filelist (cdr filelist)))))

