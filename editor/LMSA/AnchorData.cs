﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PBInterface;

namespace LMS
{
    public class AnchorData
    {
        private string _id;

        public string Id
        {
            get
            {
                return _id;
            }
        }

        private string _baseanimation;

        public string BaseAnimation
        {
            get
            {
                return _baseanimation;
            }
            set
            {
                _baseanimation = value;
            }
        }

        private string _SrcAnchorName;

        public string SrcAnchorName
        {
            get
            {
                return _SrcAnchorName;
            }
            set
            {
                _SrcAnchorName = value;
            }
        }

        private string _AnchorName;

        public string AnchorName
        {
            get
            {
                return _AnchorName;
            }
            set
            {
                _AnchorName = value;
            }
        }

        private Avector _SrcAnchor;

        public Avector SrcAnchor
        {
            get
            {
                return _SrcAnchor;
            }
            set
            {
                _SrcAnchor = value;
            }
        }

        public AnchorData(string pId,string pSrcAnchorName,string pAnchorName,Avector pSrcAnchor,string pBaseAnimation)
        {
            _baseanimation = pBaseAnimation;
            _id = pId;
            _SrcAnchorName = pSrcAnchorName;
            _AnchorName = pAnchorName;
            _SrcAnchor = pSrcAnchor;
        }
    }
}
