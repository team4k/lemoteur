﻿namespace LMS
{
    partial class Preferences
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.backgroundcolorDiag = new System.Windows.Forms.ColorDialog();
            this.btnbackgroundcolor = new System.Windows.Forms.Button();
            this.lblbackgroundcolor = new System.Windows.Forms.Label();
            this.btnokpref = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnbackgroundcolor
            // 
            this.btnbackgroundcolor.BackColor = System.Drawing.Color.Black;
            this.btnbackgroundcolor.Location = new System.Drawing.Point(155, 42);
            this.btnbackgroundcolor.Name = "btnbackgroundcolor";
            this.btnbackgroundcolor.Size = new System.Drawing.Size(24, 23);
            this.btnbackgroundcolor.TabIndex = 0;
            this.btnbackgroundcolor.UseVisualStyleBackColor = false;
            this.btnbackgroundcolor.Click += new System.EventHandler(this.btnbackgroundcolor_Click);
            // 
            // lblbackgroundcolor
            // 
            this.lblbackgroundcolor.AutoSize = true;
            this.lblbackgroundcolor.Location = new System.Drawing.Point(61, 47);
            this.lblbackgroundcolor.Name = "lblbackgroundcolor";
            this.lblbackgroundcolor.Size = new System.Drawing.Size(88, 13);
            this.lblbackgroundcolor.TabIndex = 1;
            this.lblbackgroundcolor.Text = "Couleur de fond :";
            // 
            // btnokpref
            // 
            this.btnokpref.Location = new System.Drawing.Point(214, 303);
            this.btnokpref.Name = "btnokpref";
            this.btnokpref.Size = new System.Drawing.Size(75, 23);
            this.btnokpref.TabIndex = 2;
            this.btnokpref.Text = "OK";
            this.btnokpref.UseVisualStyleBackColor = true;
            this.btnokpref.Click += new System.EventHandler(this.btnokpref_Click);
            // 
            // Preferences
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(667, 351);
            this.Controls.Add(this.btnokpref);
            this.Controls.Add(this.lblbackgroundcolor);
            this.Controls.Add(this.btnbackgroundcolor);
            this.Name = "Preferences";
            this.Text = "Preferences";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ColorDialog backgroundcolorDiag;
        private System.Windows.Forms.Button btnbackgroundcolor;
        private System.Windows.Forms.Label lblbackgroundcolor;
        private System.Windows.Forms.Button btnokpref;
    }
}