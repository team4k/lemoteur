﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using LMS.Properties;
using PBInterface;

namespace LMS
{
    public partial class NativeAnimationPanel : Panel
    {
        private int _pinselected = -1;

        private bool _init = false;

        private bool _showonion = false;

        private XDocument _refdoc = null;

        public bool IsInit
        {
            get
            {
                return _init;
            }
        }

        public bool ShowOnion
        {
            get
            {
                return _showonion;
            }
            set
            {

                _showonion = value;

                if (_init)
                {
                    NativeAnimationFuncs.native_animation_setonionvisibility(Utils.convertbooltowbool(_showonion));
                }
            }
        }

        private bool _showmultisprite = false;

        public bool ShowMultiSprite
        {
            get
            {
                return _showmultisprite;
            }
            set
            {
                _showmultisprite = value;

                if (_init)
                {
                    NativeAnimationFuncs.native_animation_setmultispritevisibility(Utils.convertbooltowbool(_showmultisprite));
                }
            }
        }

        private float zoomFactor = 1.0f;

        public float ZoomFactor
        {
            get
            {
                return zoomFactor;
            }
            set
            {
                zoomFactor = value;
            }
        }

        public int zoomedwidth
        {
            get
            {
                return Convert.ToInt32(this.Width * zoomFactor);
            }
        }

        public int zoomedheight
        {
            get
            {
                return Convert.ToInt32(this.Height * zoomFactor);
            }
        }

        private AnimationDef _def = null;

        public AnimationDef def
        {
            get
            {
                return _def;
            }
            set
            {
                _def = value;
            }
        }

        private Dictionary<string, AnchorData> _anchorsmap = new Dictionary<string, AnchorData>();

        public Dictionary<string, AnchorData> Anchorsmap
        {
            get
            {
                return _anchorsmap;
            }
        }

        public NativeAnimationPanel() : base()
        {
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.DoubleBuffer, false);
            this.SetStyle(ControlStyles.Opaque, true);
            this.SetStyle(ControlStyles.ResizeRedraw, true);
            this.SetStyle(ControlStyles.UserPaint, true);
        }

        public void SetAnchorMap(Dictionary<string,AnchorData> anchormap)
        {
            _anchorsmap = anchormap;
        }

        Label debug;

        public void setlabeldebug(Label pdebug)
        {
            debug = pdebug;
        }

        public void SetRefDoc(XDocument refdoc)
        {
            _refdoc = refdoc;
        }

        public void initrender(string AssetPath)
        {
            NativeAnimationFuncs.native_animation_init(this.Handle, this.Width, this.Height, AssetPath, Utils.convertbooltowbool(_showonion));

            NativeAnimationFuncs.native_animation_setbackgroundcolor((Settings.Default.backgroundcolor.R != 0) ? Settings.Default.backgroundcolor.R / 255.0f : 0.0f, (Settings.Default.backgroundcolor.G != 0) ? Settings.Default.backgroundcolor.G / 255.0f : 0.0f, (Settings.Default.backgroundcolor.B != 0) ? Settings.Default.backgroundcolor.B / 255.0f : 0.0f, 1.0f);

            if (NativeAnimationFuncs.native_animation_editor_resx_exist("push_pin_icon_24.png") == NativeAnimationFuncs.wbool.wfalse)
            {
                byte[] pin_icon_content = BitmapUtilities.GetBitmapContent(Resources.push_pin_icon_24);
                NativeAnimationFuncs.native_animation_editor_addtextureresx("push_pin_icon_24.png", 24, 24, pin_icon_content, (uint)pin_icon_content.Length);
            }

            _init = true;
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            //select attach point based on position

            uint framecounter = NativeAnimationFuncs.native_animation_getframe();

            bool foundpin = false;

            //debug.Text = "click on"+ e.Location.X + " " + e.Location.Y;

            if(NativeAnimationFuncs.native_animation_canselectpin() == NativeAnimationFuncs.wbool.wfalse)
            {
                this.Cursor = Cursors.No;
                base.OnMouseDown(e);
                return;
            }
            else
            {
                this.Cursor = Cursors.Default;
            }

            if (_def != null)
            {
                for (int icontact = 0; icontact < _def.AttachpointValues[(int)framecounter].Attachpointpos.Count; icontact++)
                {
                    Avector pos = _def.AttachpointValues[(int)framecounter].Attachpointpos[icontact];

                    if (!pos.Isnull)
                    {
                        int xpos = (this.Width / 2) - (Convert.ToInt32(_def.Width / zoomFactor)  / 2) + Convert.ToInt32(pos.X / zoomFactor);

                        int topy = (this.Height / 2) - (Convert.ToInt32(_def.Height / zoomFactor) / 2);

                        int maxy = topy + (Convert.ToInt32(_def.Height / zoomFactor));

                        int ypos = maxy - Convert.ToInt32(pos.Y / zoomFactor);

                        int recposx = xpos - Convert.ToInt32(12 / zoomFactor);
                        int recposy = ypos - Convert.ToInt32(12 / zoomFactor);
                        int recsize = Convert.ToInt32(24 / zoomFactor);

                        if (recposx < 0)
                            recposx = 0;

                        if (recposy < 0)
                            recposy = 0;


                        var boundingrect = new Rectangle(recposx , recposy, recsize, recsize);

                        if (boundingrect.Contains(e.Location))
                        {
                            //debug.Text = "bouding rect on left : " + boundingrect.Left + " top : " + boundingrect.Top + " right : " + boundingrect.Right + " bottom : " + boundingrect.Bottom;
                            NativeAnimationFuncs.native_animation_selectpin((uint)icontact);
                            foundpin = true;
                            _pinselected = icontact;
                            break;
                        }
                    }
                }
            }

            if (!foundpin)
            {
                NativeAnimationFuncs.native_animation_unselectpin();
                _pinselected = -1;
            }

            base.OnMouseDown(e);
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            _pinselected = -1;

            base.OnMouseUp(e);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (NativeAnimationFuncs.native_animation_canselectpin() == NativeAnimationFuncs.wbool.wfalse)
            {
                this.Cursor = Cursors.No;
                base.OnMouseMove(e);
                return;
            }
            else
            {
                this.Cursor = Cursors.Default;
            }

            //debug.Text = e.Location.X + " " + e.Location.Y;

            if (_pinselected != -1)
            {
                uint framecounter = NativeAnimationFuncs.native_animation_getframe();

                float newx = 0, newy = 0;

                float worldx = (float)(e.Location.X * zoomFactor);
                float worldy = (float)(this.Height - e.Location.Y)*zoomFactor;

                NativeAnimationFuncs.wbool result = NativeAnimationFuncs.native_animation_movepin(worldx, worldy, out newx, out newy);

                if(result == NativeAnimationFuncs.wbool.wtrue)
                {
                    for (int icontact = 0; icontact < _def.AttachpointValues[(int)framecounter].Attachpointpos.Count; icontact++)
                    {
                        if (icontact != _pinselected)
                        {
                            continue;
                        }

                        Avector pos = _def.AttachpointValues[(int)framecounter].Attachpointpos[icontact];
                        pos.X = (int)newx;
                        pos.Y = (int)newy;


                        _def.AttachpointValues[(int)framecounter].Attachpointpos.RemoveAt(icontact);
                        _def.AttachpointValues[(int)framecounter].Attachpointpos.Insert(icontact, pos);

                        ProjetGenerator.SetNewAnchorPos(_refdoc, _def.Name, (int)framecounter, icontact, pos.X, pos.Y);

                    }
                }
            }



            base.OnMouseMove(e);
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            if (_init)
            {
                uint framecounter = NativeAnimationFuncs.native_animation_getframe();

                if (_def != null)
                {
                    //update additionals sprite positions based on anchor map
                    foreach (KeyValuePair<string, AnchorData> value in _anchorsmap)
                    {
                        int baseanchorid = -1;

                        for (int anchorid = 0; anchorid < _def.AttachpointIds.Count; anchorid++)
                        {
                            if (value.Value.AnchorName == _def.AttachpointIds[anchorid])
                            {
                                baseanchorid = anchorid;
                                break;
                            }
                        }

                        if (baseanchorid != -1)
                        {
                            Avector pos = _def.AttachpointValues[(int)framecounter].Attachpointpos[baseanchorid];

                            if (!pos.Isnull)
                            {
                               NativeAnimationFuncs.native_animation_setmultispriteposition(value.Key, pos.X - value.Value.SrcAnchor.X, pos.Y - value.Value.SrcAnchor.Y);
                            }
                        }
                    }
                }


                NativeAnimationFuncs.native_animation_renderframe();
            }

        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {

        }
    }
}
