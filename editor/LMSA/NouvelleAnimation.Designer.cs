﻿namespace LMS
{
    partial class NouvelleAnimation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblnomanimation = new System.Windows.Forms.Label();
            this.txtanimation = new System.Windows.Forms.TextBox();
            this.lblselection = new System.Windows.Forms.Label();
            this.ofdframes = new System.Windows.Forms.OpenFileDialog();
            this.lbframes = new System.Windows.Forms.ListBox();
            this.btnselectframes = new System.Windows.Forms.Button();
            this.tcNouvelleanimation = new System.Windows.Forms.TabControl();
            this.etape1 = new System.Windows.Forms.TabPage();
            this.rtblog = new System.Windows.Forms.RichTextBox();
            this.btnnext1 = new System.Windows.Forms.Button();
            this.etape2 = new System.Windows.Forms.TabPage();
            this.btnRedim = new System.Windows.Forms.Button();
            this.btnraz = new System.Windows.Forms.Button();
            this.btnCrop = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.numY = new System.Windows.Forms.NumericUpDown();
            this.numX = new System.Windows.Forms.NumericUpDown();
            this.chkorig = new System.Windows.Forms.CheckBox();
            this.lblheight = new System.Windows.Forms.Label();
            this.lblwidth = new System.Windows.Forms.Label();
            this.numHeight = new System.Windows.Forms.NumericUpDown();
            this.numwidth = new System.Windows.Forms.NumericUpDown();
            this.btncreeranimation = new System.Windows.Forms.Button();
            this.btnleft = new System.Windows.Forms.Button();
            this.btnright = new System.Windows.Forms.Button();
            this.pbpreview = new System.Windows.Forms.PictureBox();
            this.chkisscan = new System.Windows.Forms.CheckBox();
            this.tcNouvelleanimation.SuspendLayout();
            this.etape1.SuspendLayout();
            this.etape2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numwidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbpreview)).BeginInit();
            this.SuspendLayout();
            // 
            // lblnomanimation
            // 
            this.lblnomanimation.AutoSize = true;
            this.lblnomanimation.Location = new System.Drawing.Point(405, 67);
            this.lblnomanimation.Name = "lblnomanimation";
            this.lblnomanimation.Size = new System.Drawing.Size(86, 13);
            this.lblnomanimation.TabIndex = 0;
            this.lblnomanimation.Text = "Nom animation : ";
            // 
            // txtanimation
            // 
            this.txtanimation.Location = new System.Drawing.Point(497, 64);
            this.txtanimation.Name = "txtanimation";
            this.txtanimation.Size = new System.Drawing.Size(164, 20);
            this.txtanimation.TabIndex = 1;
            // 
            // lblselection
            // 
            this.lblselection.AutoSize = true;
            this.lblselection.Location = new System.Drawing.Point(367, 159);
            this.lblselection.Name = "lblselection";
            this.lblselection.Size = new System.Drawing.Size(125, 13);
            this.lblselection.TabIndex = 2;
            this.lblselection.Text = "Sélectionner les frames : ";
            // 
            // ofdframes
            // 
            this.ofdframes.Filter = "Fichiers TIF|*.tif|Fichiers TGA |*.tga|Fichiers PNG|*.png";
            this.ofdframes.Multiselect = true;
            // 
            // lbframes
            // 
            this.lbframes.FormattingEnabled = true;
            this.lbframes.Location = new System.Drawing.Point(498, 159);
            this.lbframes.Name = "lbframes";
            this.lbframes.Size = new System.Drawing.Size(164, 238);
            this.lbframes.TabIndex = 3;
            // 
            // btnselectframes
            // 
            this.btnselectframes.Location = new System.Drawing.Point(680, 159);
            this.btnselectframes.Name = "btnselectframes";
            this.btnselectframes.Size = new System.Drawing.Size(218, 23);
            this.btnselectframes.TabIndex = 4;
            this.btnselectframes.Text = "Sélectionner les frames de l\'animation...";
            this.btnselectframes.UseVisualStyleBackColor = true;
            this.btnselectframes.Click += new System.EventHandler(this.btnselectframes_Click);
            // 
            // tcNouvelleanimation
            // 
            this.tcNouvelleanimation.Controls.Add(this.etape1);
            this.tcNouvelleanimation.Controls.Add(this.etape2);
            this.tcNouvelleanimation.Location = new System.Drawing.Point(12, 14);
            this.tcNouvelleanimation.Name = "tcNouvelleanimation";
            this.tcNouvelleanimation.SelectedIndex = 0;
            this.tcNouvelleanimation.Size = new System.Drawing.Size(1368, 758);
            this.tcNouvelleanimation.TabIndex = 5;
            // 
            // etape1
            // 
            this.etape1.Controls.Add(this.chkisscan);
            this.etape1.Controls.Add(this.rtblog);
            this.etape1.Controls.Add(this.btnnext1);
            this.etape1.Controls.Add(this.txtanimation);
            this.etape1.Controls.Add(this.btnselectframes);
            this.etape1.Controls.Add(this.lblnomanimation);
            this.etape1.Controls.Add(this.lbframes);
            this.etape1.Controls.Add(this.lblselection);
            this.etape1.Location = new System.Drawing.Point(4, 22);
            this.etape1.Name = "etape1";
            this.etape1.Padding = new System.Windows.Forms.Padding(3);
            this.etape1.Size = new System.Drawing.Size(1360, 732);
            this.etape1.TabIndex = 0;
            this.etape1.Text = "Etape 1 : nommage et sélection frames";
            this.etape1.UseVisualStyleBackColor = true;
            // 
            // rtblog
            // 
            this.rtblog.Location = new System.Drawing.Point(224, 454);
            this.rtblog.Name = "rtblog";
            this.rtblog.Size = new System.Drawing.Size(886, 183);
            this.rtblog.TabIndex = 6;
            this.rtblog.Text = "";
            // 
            // btnnext1
            // 
            this.btnnext1.Location = new System.Drawing.Point(497, 410);
            this.btnnext1.Name = "btnnext1";
            this.btnnext1.Size = new System.Drawing.Size(164, 23);
            this.btnnext1.TabIndex = 5;
            this.btnnext1.Text = "Etape suivante >";
            this.btnnext1.UseVisualStyleBackColor = true;
            this.btnnext1.Click += new System.EventHandler(this.btnnext1_Click);
            // 
            // etape2
            // 
            this.etape2.Controls.Add(this.btnRedim);
            this.etape2.Controls.Add(this.btnraz);
            this.etape2.Controls.Add(this.btnCrop);
            this.etape2.Controls.Add(this.label1);
            this.etape2.Controls.Add(this.label2);
            this.etape2.Controls.Add(this.numY);
            this.etape2.Controls.Add(this.numX);
            this.etape2.Controls.Add(this.chkorig);
            this.etape2.Controls.Add(this.lblheight);
            this.etape2.Controls.Add(this.lblwidth);
            this.etape2.Controls.Add(this.numHeight);
            this.etape2.Controls.Add(this.numwidth);
            this.etape2.Controls.Add(this.btncreeranimation);
            this.etape2.Controls.Add(this.btnleft);
            this.etape2.Controls.Add(this.btnright);
            this.etape2.Controls.Add(this.pbpreview);
            this.etape2.Location = new System.Drawing.Point(4, 22);
            this.etape2.Name = "etape2";
            this.etape2.Padding = new System.Windows.Forms.Padding(3);
            this.etape2.Size = new System.Drawing.Size(1360, 732);
            this.etape2.TabIndex = 1;
            this.etape2.Text = "Etape 2 : recadrage et dimensionnement";
            this.etape2.UseVisualStyleBackColor = true;
            // 
            // btnRedim
            // 
            this.btnRedim.Location = new System.Drawing.Point(530, 690);
            this.btnRedim.Name = "btnRedim";
            this.btnRedim.Size = new System.Drawing.Size(164, 23);
            this.btnRedim.TabIndex = 19;
            this.btnRedim.Text = "Redimensionner les frames...";
            this.btnRedim.UseVisualStyleBackColor = true;
            this.btnRedim.Click += new System.EventHandler(this.btnRedim_Click);
            // 
            // btnraz
            // 
            this.btnraz.Location = new System.Drawing.Point(700, 690);
            this.btnraz.Name = "btnraz";
            this.btnraz.Size = new System.Drawing.Size(164, 23);
            this.btnraz.TabIndex = 18;
            this.btnraz.Text = "Réinitialiser les frames";
            this.btnraz.UseVisualStyleBackColor = true;
            this.btnraz.Click += new System.EventHandler(this.btnraz_Click);
            // 
            // btnCrop
            // 
            this.btnCrop.Location = new System.Drawing.Point(360, 690);
            this.btnCrop.Name = "btnCrop";
            this.btnCrop.Size = new System.Drawing.Size(164, 23);
            this.btnCrop.TabIndex = 17;
            this.btnCrop.Text = "Rogner selon le rectangle";
            this.btnCrop.UseVisualStyleBackColor = true;
            this.btnCrop.Click += new System.EventHandler(this.btnCrop_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(383, 647);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Y rectangle : ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(171, 647);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "X rectangle : ";
            // 
            // numY
            // 
            this.numY.Location = new System.Drawing.Point(459, 645);
            this.numY.Maximum = new decimal(new int[] {
            8096,
            0,
            0,
            0});
            this.numY.Name = "numY";
            this.numY.Size = new System.Drawing.Size(120, 20);
            this.numY.TabIndex = 14;
            // 
            // numX
            // 
            this.numX.Location = new System.Drawing.Point(247, 645);
            this.numX.Maximum = new decimal(new int[] {
            8096,
            0,
            0,
            0});
            this.numX.Name = "numX";
            this.numX.Size = new System.Drawing.Size(120, 20);
            this.numX.TabIndex = 13;
            // 
            // chkorig
            // 
            this.chkorig.AutoSize = true;
            this.chkorig.Location = new System.Drawing.Point(1070, 648);
            this.chkorig.Name = "chkorig";
            this.chkorig.Size = new System.Drawing.Size(117, 17);
            this.chkorig.TabIndex = 12;
            this.chkorig.Text = "Voir image d\'origine";
            this.chkorig.UseVisualStyleBackColor = true;
            this.chkorig.CheckedChanged += new System.EventHandler(this.chkorig_CheckedChanged);
            // 
            // lblheight
            // 
            this.lblheight.AutoSize = true;
            this.lblheight.Location = new System.Drawing.Point(816, 645);
            this.lblheight.Name = "lblheight";
            this.lblheight.Size = new System.Drawing.Size(101, 13);
            this.lblheight.TabIndex = 11;
            this.lblheight.Text = "Hauteur rectangle : ";
            // 
            // lblwidth
            // 
            this.lblwidth.AutoSize = true;
            this.lblwidth.Location = new System.Drawing.Point(585, 647);
            this.lblwidth.Name = "lblwidth";
            this.lblwidth.Size = new System.Drawing.Size(99, 13);
            this.lblwidth.TabIndex = 10;
            this.lblwidth.Text = "Largeur rectangle : ";
            // 
            // numHeight
            // 
            this.numHeight.Location = new System.Drawing.Point(923, 645);
            this.numHeight.Maximum = new decimal(new int[] {
            8096,
            0,
            0,
            0});
            this.numHeight.Name = "numHeight";
            this.numHeight.Size = new System.Drawing.Size(120, 20);
            this.numHeight.TabIndex = 9;
            // 
            // numwidth
            // 
            this.numwidth.Location = new System.Drawing.Point(690, 645);
            this.numwidth.Maximum = new decimal(new int[] {
            8096,
            0,
            0,
            0});
            this.numwidth.Name = "numwidth";
            this.numwidth.Size = new System.Drawing.Size(120, 20);
            this.numwidth.TabIndex = 8;
            // 
            // btncreeranimation
            // 
            this.btncreeranimation.Location = new System.Drawing.Point(1110, 690);
            this.btncreeranimation.Name = "btncreeranimation";
            this.btncreeranimation.Size = new System.Drawing.Size(164, 23);
            this.btncreeranimation.TabIndex = 7;
            this.btncreeranimation.Text = "Créer la nouvelle animation";
            this.btncreeranimation.UseVisualStyleBackColor = true;
            this.btncreeranimation.Click += new System.EventHandler(this.btncreeranimation_Click);
            // 
            // btnleft
            // 
            this.btnleft.Image = global::LMS.Properties.Resources.playback_play_icon_48_invert;
            this.btnleft.Location = new System.Drawing.Point(6, 238);
            this.btnleft.Name = "btnleft";
            this.btnleft.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnleft.Size = new System.Drawing.Size(48, 48);
            this.btnleft.TabIndex = 6;
            this.btnleft.UseVisualStyleBackColor = true;
            this.btnleft.Click += new System.EventHandler(this.btnleft_Click);
            // 
            // btnright
            // 
            this.btnright.Image = global::LMS.Properties.Resources.playback_play_icon_48;
            this.btnright.Location = new System.Drawing.Point(1306, 262);
            this.btnright.Name = "btnright";
            this.btnright.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnright.Size = new System.Drawing.Size(48, 48);
            this.btnright.TabIndex = 5;
            this.btnright.UseVisualStyleBackColor = true;
            this.btnright.Click += new System.EventHandler(this.btnright_Click);
            // 
            // pbpreview
            // 
            this.pbpreview.BackColor = System.Drawing.Color.Gray;
            this.pbpreview.Location = new System.Drawing.Point(62, 6);
            this.pbpreview.Name = "pbpreview";
            this.pbpreview.Size = new System.Drawing.Size(1238, 619);
            this.pbpreview.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbpreview.TabIndex = 0;
            this.pbpreview.TabStop = false;
            this.pbpreview.Paint += new System.Windows.Forms.PaintEventHandler(this.pbpreview_Paint);
            this.pbpreview.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pbpreview_MouseClick);
            this.pbpreview.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pbpreview_MouseDown);
            this.pbpreview.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pbpreview_MouseMove);
            this.pbpreview.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pbpreview_MouseUp);
            // 
            // chkisscan
            // 
            this.chkisscan.AutoSize = true;
            this.chkisscan.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkisscan.Location = new System.Drawing.Point(432, 101);
            this.chkisscan.Name = "chkisscan";
            this.chkisscan.Size = new System.Drawing.Size(77, 17);
            this.chkisscan.TabIndex = 7;
            this.chkisscan.Text = "Scan bruts";
            this.chkisscan.UseVisualStyleBackColor = true;
            // 
            // NouvelleAnimation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1392, 784);
            this.Controls.Add(this.tcNouvelleanimation);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NouvelleAnimation";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "NouvelleAnimation";
            this.tcNouvelleanimation.ResumeLayout(false);
            this.etape1.ResumeLayout(false);
            this.etape1.PerformLayout();
            this.etape2.ResumeLayout(false);
            this.etape2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numwidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbpreview)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblnomanimation;
        private System.Windows.Forms.TextBox txtanimation;
        private System.Windows.Forms.Label lblselection;
        private System.Windows.Forms.OpenFileDialog ofdframes;
        private System.Windows.Forms.ListBox lbframes;
        private System.Windows.Forms.Button btnselectframes;
        private System.Windows.Forms.TabControl tcNouvelleanimation;
        private System.Windows.Forms.TabPage etape1;
        private System.Windows.Forms.TabPage etape2;
        private System.Windows.Forms.Button btnnext1;
        private System.Windows.Forms.PictureBox pbpreview;
        private System.Windows.Forms.Button btnright;
        private System.Windows.Forms.Button btnleft;
        private System.Windows.Forms.Button btncreeranimation;
        private System.Windows.Forms.NumericUpDown numHeight;
        private System.Windows.Forms.NumericUpDown numwidth;
        private System.Windows.Forms.Label lblheight;
        private System.Windows.Forms.Label lblwidth;
        private System.Windows.Forms.RichTextBox rtblog;
        private System.Windows.Forms.CheckBox chkorig;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numY;
        private System.Windows.Forms.NumericUpDown numX;
        private System.Windows.Forms.Button btnCrop;
        private System.Windows.Forms.Button btnraz;
        private System.Windows.Forms.Button btnRedim;
        private System.Windows.Forms.CheckBox chkisscan;
    }
}