﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PBInterface;

namespace LMS
{
    public partial class AddSprite : Form
    {
        private AnimationDef _refanimation;
        private AnimationList _reflist;

        public Avector Srcvector = null;

        public AddSprite(AnimationDef refanimation,AnimationList reflist,AnchorData data = null)
        {
            _refanimation = refanimation;
            _reflist = reflist;
            InitializeComponent();

            cmbAnchor.Items.AddRange(_refanimation.AttachpointIds.ToArray());

            cmbSrcAnchor.Items.AddRange(_refanimation.AttachpointIds.ToArray());

            foreach (AnimationDef anim in reflist.AnimArrays)
            {
                cmbAnimations.Items.Add(anim.Name);
            }

            if(data != null)
            {
                this.Tag = data;
                this.loadData(data);
            }

        }

        private void loadData(AnchorData data)
        {
            cmbSrcAnchor.SelectedItem = data.SrcAnchorName;
            cmbAnchor.SelectedItem = data.AnchorName;
            cmbAnimations.SelectedItem = data.BaseAnimation;
            txtid.Text = data.Id;
            txtid.Enabled = false;
        }

        private void btnok_Click(object sender, EventArgs e)
        {
            Avector anchorvector = null;

            //récupération de la position de l'ancre
            for (int anchorpos = 0;anchorpos < _refanimation.AttachpointIds.Count;anchorpos++)
            {
                if(_refanimation.AttachpointIds[anchorpos] == cmbAnchor.SelectedItem.ToString())
                {
                    anchorvector = _refanimation.AttachpointValues[(int)NativeAnimationFuncs.native_animation_getframe()].Attachpointpos[anchorpos];
                    break;
                }
            }

            float x = 0, y = 0;

            if(anchorvector != null)
            {
                x = anchorvector.X;
                y = anchorvector.Y;
            }


            AnimationDef srcanimation = null;
            //animation source
            foreach(AnimationDef animation in _reflist.AnimArrays)
            {
                if(animation.Name == cmbAnimations.SelectedItem.ToString())
                {
                    srcanimation = animation;
                    break;
                }
            }
            

            //récupération de la position de l'ancre source
            for (int anchorpos = 0; anchorpos < srcanimation.AttachpointIds.Count; anchorpos++)
            {
                if (srcanimation.AttachpointIds[anchorpos] == cmbSrcAnchor.SelectedItem.ToString())
                {
                    Srcvector = srcanimation.AttachpointValues[0].Attachpointpos[anchorpos];
                    break;
                }
            }



            if(Srcvector != null)
            {
                x -= Srcvector.X;
                y -= Srcvector.Y;
            }

            if(this.Tag != null)
            {
                AnchorData data = this.Tag as AnchorData;

                if(srcanimation != null)
                {
                    NativeAnimationFuncs.native_animation_setmultispriteanimation(txtid.Text, srcanimation.Name);
                }

                NativeAnimationFuncs.native_animation_setmultispriteposition(txtid.Text, x, y);

                data.SrcAnchorName = cmbSrcAnchor.SelectedItem.ToString();
                data.AnchorName = cmbAnchor.SelectedItem.ToString();
                data.BaseAnimation = cmbAnimations.SelectedItem.ToString();
            }
            else
            {
                NativeAnimationFuncs.native_animation_addnewsprite(txtid.Text, x, y, cmbAnimations.SelectedItem.ToString());
            }

            this.DialogResult = DialogResult.OK;
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            this.Close();
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
