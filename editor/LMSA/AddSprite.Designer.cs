﻿namespace LMS
{
    partial class AddSprite
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtid = new System.Windows.Forms.TextBox();
            this.lblname = new System.Windows.Forms.Label();
            this.cmbAnimations = new System.Windows.Forms.ComboBox();
            this.cmbAnchor = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnok = new System.Windows.Forms.Button();
            this.btncancel = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbSrcAnchor = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // txtid
            // 
            this.txtid.Location = new System.Drawing.Point(108, 12);
            this.txtid.Name = "txtid";
            this.txtid.Size = new System.Drawing.Size(199, 20);
            this.txtid.TabIndex = 0;
            // 
            // lblname
            // 
            this.lblname.AutoSize = true;
            this.lblname.Location = new System.Drawing.Point(44, 15);
            this.lblname.Name = "lblname";
            this.lblname.Size = new System.Drawing.Size(58, 13);
            this.lblname.TabIndex = 1;
            this.lblname.Text = "id du sprite";
            // 
            // cmbAnimations
            // 
            this.cmbAnimations.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAnimations.FormattingEnabled = true;
            this.cmbAnimations.Location = new System.Drawing.Point(108, 51);
            this.cmbAnimations.Name = "cmbAnimations";
            this.cmbAnimations.Size = new System.Drawing.Size(199, 21);
            this.cmbAnimations.TabIndex = 2;
            // 
            // cmbAnchor
            // 
            this.cmbAnchor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAnchor.FormattingEnabled = true;
            this.cmbAnchor.Location = new System.Drawing.Point(108, 127);
            this.cmbAnchor.Name = "cmbAnchor";
            this.cmbAnchor.Size = new System.Drawing.Size(199, 21);
            this.cmbAnchor.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "animation du sprite";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(44, 130);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Ancrer à";
            // 
            // btnok
            // 
            this.btnok.Location = new System.Drawing.Point(111, 168);
            this.btnok.Name = "btnok";
            this.btnok.Size = new System.Drawing.Size(75, 23);
            this.btnok.TabIndex = 6;
            this.btnok.Text = "OK";
            this.btnok.UseVisualStyleBackColor = true;
            this.btnok.Click += new System.EventHandler(this.btnok_Click);
            // 
            // btncancel
            // 
            this.btncancel.Location = new System.Drawing.Point(192, 168);
            this.btncancel.Name = "btncancel";
            this.btncancel.Size = new System.Drawing.Size(75, 23);
            this.btncancel.TabIndex = 7;
            this.btncancel.Text = "Cancel";
            this.btncancel.UseVisualStyleBackColor = true;
            this.btncancel.Click += new System.EventHandler(this.btncancel_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(32, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Ancre source";
            // 
            // cmbSrcAnchor
            // 
            this.cmbSrcAnchor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSrcAnchor.FormattingEnabled = true;
            this.cmbSrcAnchor.Location = new System.Drawing.Point(108, 87);
            this.cmbSrcAnchor.Name = "cmbSrcAnchor";
            this.cmbSrcAnchor.Size = new System.Drawing.Size(199, 21);
            this.cmbSrcAnchor.TabIndex = 8;
            // 
            // AddSprite
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(409, 208);
            this.Controls.Add(this.cmbSrcAnchor);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btncancel);
            this.Controls.Add(this.btnok);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbAnchor);
            this.Controls.Add(this.cmbAnimations);
            this.Controls.Add(this.lblname);
            this.Controls.Add(this.txtid);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddSprite";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AddSprite";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblname;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnok;
        private System.Windows.Forms.Button btncancel;
        public System.Windows.Forms.TextBox txtid;
        public System.Windows.Forms.ComboBox cmbAnchor;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.ComboBox cmbSrcAnchor;
        public System.Windows.Forms.ComboBox cmbAnimations;
    }
}