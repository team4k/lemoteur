﻿namespace LMS
{
    partial class MainForm
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuPrincipal = new System.Windows.Forms.MenuStrip();
            this.fichierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nouveauToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ouvrirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enregistrerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enregistreSousToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fermerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importerDepuisUnFichierDeSortieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.préférencesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ajouterAnimationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.regénérerFramesDepuisGimpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exporterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ouvrirFichier = new System.Windows.Forms.OpenFileDialog();
            this.cmbAnimations = new System.Windows.Forms.ComboBox();
            this.panelFrames = new System.Windows.Forms.Panel();
            this.labeldebug = new System.Windows.Forms.Label();
            this.chkloop = new System.Windows.Forms.CheckBox();
            this.numFrameDelay = new System.Windows.Forms.NumericUpDown();
            this.lbldelay = new System.Windows.Forms.Label();
            this.enregProjet = new System.Windows.Forms.SaveFileDialog();
            this.importFichier = new System.Windows.Forms.OpenFileDialog();
            this.exportProjet = new System.Windows.Forms.SaveFileDialog();
            this.chkonion = new System.Windows.Forms.CheckBox();
            this.cmbContactPoint = new System.Windows.Forms.ComboBox();
            this.AnimationPanel = new LMS.NativeAnimationPanel();
            this.chkshowhidemultisprite = new System.Windows.Forms.CheckBox();
            this.grpattachedsprites = new System.Windows.Forms.GroupBox();
            this.btnEditSprite = new System.Windows.Forms.Button();
            this.cmbattachedsprites = new System.Windows.Forms.ComboBox();
            this.btnminussprite = new System.Windows.Forms.Button();
            this.btnplussprite = new System.Windows.Forms.Button();
            this.grpcontactpoints = new System.Windows.Forms.GroupBox();
            this.copyonallframes = new System.Windows.Forms.Button();
            this.btneditanchor = new System.Windows.Forms.Button();
            this.btnsuppranchor = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnaddanchor = new System.Windows.Forms.Button();
            this.debug = new System.Windows.Forms.Label();
            this.btnremoveframe = new System.Windows.Forms.Button();
            this.btnaddframe = new System.Windows.Forms.Button();
            this.btnstop = new System.Windows.Forms.Button();
            this.btnplay = new System.Windows.Forms.Button();
            this.cmbZoom = new System.Windows.Forms.ComboBox();
            this.lbl_zoom = new System.Windows.Forms.Label();
            this.boutonactions = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonZoomplus = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonZoomminus = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonZoomreset = new System.Windows.Forms.ToolStripButton();
            this.redimensionnerEtRegénérerFramesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuPrincipal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numFrameDelay)).BeginInit();
            this.grpattachedsprites.SuspendLayout();
            this.grpcontactpoints.SuspendLayout();
            this.boutonactions.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuPrincipal
            // 
            this.menuPrincipal.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fichierToolStripMenuItem,
            this.editionToolStripMenuItem,
            this.exportToolStripMenuItem});
            this.menuPrincipal.Location = new System.Drawing.Point(0, 0);
            this.menuPrincipal.Name = "menuPrincipal";
            this.menuPrincipal.Size = new System.Drawing.Size(1380, 24);
            this.menuPrincipal.TabIndex = 0;
            this.menuPrincipal.Text = "menuPrincipal";
            // 
            // fichierToolStripMenuItem
            // 
            this.fichierToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nouveauToolStripMenuItem,
            this.ouvrirToolStripMenuItem,
            this.enregistrerToolStripMenuItem,
            this.enregistreSousToolStripMenuItem,
            this.fermerToolStripMenuItem,
            this.importerDepuisUnFichierDeSortieToolStripMenuItem});
            this.fichierToolStripMenuItem.Name = "fichierToolStripMenuItem";
            this.fichierToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.fichierToolStripMenuItem.Text = "Fichier";
            // 
            // nouveauToolStripMenuItem
            // 
            this.nouveauToolStripMenuItem.Name = "nouveauToolStripMenuItem";
            this.nouveauToolStripMenuItem.Size = new System.Drawing.Size(268, 22);
            this.nouveauToolStripMenuItem.Text = "Nouveau";
            this.nouveauToolStripMenuItem.Click += new System.EventHandler(this.nouveauToolStripMenuItem_Click);
            // 
            // ouvrirToolStripMenuItem
            // 
            this.ouvrirToolStripMenuItem.Name = "ouvrirToolStripMenuItem";
            this.ouvrirToolStripMenuItem.Size = new System.Drawing.Size(268, 22);
            this.ouvrirToolStripMenuItem.Text = "Ouvrir";
            this.ouvrirToolStripMenuItem.Click += new System.EventHandler(this.ouvrirToolStripMenuItem_Click);
            // 
            // enregistrerToolStripMenuItem
            // 
            this.enregistrerToolStripMenuItem.Name = "enregistrerToolStripMenuItem";
            this.enregistrerToolStripMenuItem.Size = new System.Drawing.Size(268, 22);
            this.enregistrerToolStripMenuItem.Text = "Enregistrer";
            this.enregistrerToolStripMenuItem.Click += new System.EventHandler(this.enregistrerToolStripMenuItem_Click);
            // 
            // enregistreSousToolStripMenuItem
            // 
            this.enregistreSousToolStripMenuItem.Name = "enregistreSousToolStripMenuItem";
            this.enregistreSousToolStripMenuItem.Size = new System.Drawing.Size(268, 22);
            this.enregistreSousToolStripMenuItem.Text = "Enregistrer sous";
            this.enregistreSousToolStripMenuItem.Click += new System.EventHandler(this.enregistreSousToolStripMenuItem_Click);
            // 
            // fermerToolStripMenuItem
            // 
            this.fermerToolStripMenuItem.Name = "fermerToolStripMenuItem";
            this.fermerToolStripMenuItem.Size = new System.Drawing.Size(268, 22);
            this.fermerToolStripMenuItem.Text = "Fermer";
            this.fermerToolStripMenuItem.Click += new System.EventHandler(this.fermerToolStripMenuItem_Click);
            // 
            // importerDepuisUnFichierDeSortieToolStripMenuItem
            // 
            this.importerDepuisUnFichierDeSortieToolStripMenuItem.Name = "importerDepuisUnFichierDeSortieToolStripMenuItem";
            this.importerDepuisUnFichierDeSortieToolStripMenuItem.Size = new System.Drawing.Size(268, 22);
            this.importerDepuisUnFichierDeSortieToolStripMenuItem.Text = "Importer depuis un fichier de sortie...";
            this.importerDepuisUnFichierDeSortieToolStripMenuItem.Click += new System.EventHandler(this.importerDepuisUnFichierDeSortieToolStripMenuItem_Click);
            // 
            // editionToolStripMenuItem
            // 
            this.editionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.préférencesToolStripMenuItem,
            this.ajouterAnimationToolStripMenuItem,
            this.regénérerFramesDepuisGimpToolStripMenuItem,
            this.redimensionnerEtRegénérerFramesToolStripMenuItem});
            this.editionToolStripMenuItem.Name = "editionToolStripMenuItem";
            this.editionToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.editionToolStripMenuItem.Text = "Edition";
            this.editionToolStripMenuItem.Click += new System.EventHandler(this.editionToolStripMenuItem_Click);
            // 
            // préférencesToolStripMenuItem
            // 
            this.préférencesToolStripMenuItem.Name = "préférencesToolStripMenuItem";
            this.préférencesToolStripMenuItem.Size = new System.Drawing.Size(265, 22);
            this.préférencesToolStripMenuItem.Text = "Préférences";
            this.préférencesToolStripMenuItem.Click += new System.EventHandler(this.préférencesToolStripMenuItem_Click);
            // 
            // ajouterAnimationToolStripMenuItem
            // 
            this.ajouterAnimationToolStripMenuItem.Name = "ajouterAnimationToolStripMenuItem";
            this.ajouterAnimationToolStripMenuItem.Size = new System.Drawing.Size(265, 22);
            this.ajouterAnimationToolStripMenuItem.Text = "Ajouter animation...";
            this.ajouterAnimationToolStripMenuItem.Click += new System.EventHandler(this.ajouterAnimationToolStripMenuItem_Click);
            // 
            // regénérerFramesDepuisGimpToolStripMenuItem
            // 
            this.regénérerFramesDepuisGimpToolStripMenuItem.Name = "regénérerFramesDepuisGimpToolStripMenuItem";
            this.regénérerFramesDepuisGimpToolStripMenuItem.Size = new System.Drawing.Size(265, 22);
            this.regénérerFramesDepuisGimpToolStripMenuItem.Text = "Regénérer frames depuis gimp";
            this.regénérerFramesDepuisGimpToolStripMenuItem.Click += new System.EventHandler(this.regénérerFramesDepuisGimpToolStripMenuItem_Click);
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exporterToolStripMenuItem});
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.exportToolStripMenuItem.Text = " Export";
            // 
            // exporterToolStripMenuItem
            // 
            this.exporterToolStripMenuItem.Name = "exporterToolStripMenuItem";
            this.exporterToolStripMenuItem.Size = new System.Drawing.Size(265, 22);
            this.exporterToolStripMenuItem.Text = "Exporter dans le format du moteur...";
            this.exporterToolStripMenuItem.Click += new System.EventHandler(this.exporterToolStripMenuItem_Click);
            // 
            // ouvrirFichier
            // 
            this.ouvrirFichier.DefaultExt = "awf";
            this.ouvrirFichier.Filter = "Projet LMS |*.xml";
            // 
            // cmbAnimations
            // 
            this.cmbAnimations.DisplayMember = "Key";
            this.cmbAnimations.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAnimations.FormattingEnabled = true;
            this.cmbAnimations.Location = new System.Drawing.Point(1067, 55);
            this.cmbAnimations.Name = "cmbAnimations";
            this.cmbAnimations.Size = new System.Drawing.Size(195, 21);
            this.cmbAnimations.TabIndex = 2;
            this.cmbAnimations.ValueMember = "Value";
            this.cmbAnimations.SelectedIndexChanged += new System.EventHandler(this.cmbAnimations_SelectedIndexChanged);
            // 
            // panelFrames
            // 
            this.panelFrames.AutoScroll = true;
            this.panelFrames.Location = new System.Drawing.Point(23, 55);
            this.panelFrames.Name = "panelFrames";
            this.panelFrames.Size = new System.Drawing.Size(972, 130);
            this.panelFrames.TabIndex = 3;
            // 
            // labeldebug
            // 
            this.labeldebug.AutoSize = true;
            this.labeldebug.Location = new System.Drawing.Point(186, 74);
            this.labeldebug.Name = "labeldebug";
            this.labeldebug.Size = new System.Drawing.Size(0, 13);
            this.labeldebug.TabIndex = 6;
            // 
            // chkloop
            // 
            this.chkloop.AutoSize = true;
            this.chkloop.Location = new System.Drawing.Point(1144, 100);
            this.chkloop.Name = "chkloop";
            this.chkloop.Size = new System.Drawing.Size(122, 17);
            this.chkloop.TabIndex = 7;
            this.chkloop.Text = "Animation en boucle";
            this.chkloop.UseVisualStyleBackColor = true;
            this.chkloop.CheckedChanged += new System.EventHandler(this.chkloop_CheckedChanged);
            // 
            // numFrameDelay
            // 
            this.numFrameDelay.DecimalPlaces = 2;
            this.numFrameDelay.Location = new System.Drawing.Point(374, 202);
            this.numFrameDelay.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numFrameDelay.Name = "numFrameDelay";
            this.numFrameDelay.Size = new System.Drawing.Size(82, 20);
            this.numFrameDelay.TabIndex = 8;
            this.numFrameDelay.ValueChanged += new System.EventHandler(this.numFrameDelay_ValueChanged);
            // 
            // lbldelay
            // 
            this.lbldelay.AutoSize = true;
            this.lbldelay.Location = new System.Drawing.Point(281, 204);
            this.lbldelay.Name = "lbldelay";
            this.lbldelay.Size = new System.Drawing.Size(60, 13);
            this.lbldelay.TabIndex = 9;
            this.lbldelay.Text = "Délai frame";
            // 
            // enregProjet
            // 
            this.enregProjet.DefaultExt = "xml";
            this.enregProjet.Filter = "Project File |*.xml";
            // 
            // importFichier
            // 
            this.importFichier.DefaultExt = "awf";
            this.importFichier.Filter = "Fichier animation |*.awf";
            // 
            // exportProjet
            // 
            this.exportProjet.DefaultExt = "awf";
            this.exportProjet.Filter = "Fichier animation |*.awf";
            // 
            // chkonion
            // 
            this.chkonion.AutoSize = true;
            this.chkonion.Location = new System.Drawing.Point(1144, 123);
            this.chkonion.Name = "chkonion";
            this.chkonion.Size = new System.Drawing.Size(138, 17);
            this.chkonion.TabIndex = 11;
            this.chkonion.Text = "Afficher la vue en onion";
            this.chkonion.UseVisualStyleBackColor = true;
            this.chkonion.CheckedChanged += new System.EventHandler(this.chkonion_CheckedChanged);
            // 
            // cmbContactPoint
            // 
            this.cmbContactPoint.DisplayMember = "Key";
            this.cmbContactPoint.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbContactPoint.FormattingEnabled = true;
            this.cmbContactPoint.Location = new System.Drawing.Point(6, 19);
            this.cmbContactPoint.Name = "cmbContactPoint";
            this.cmbContactPoint.Size = new System.Drawing.Size(362, 21);
            this.cmbContactPoint.TabIndex = 12;
            this.cmbContactPoint.ValueMember = "Value";
            this.cmbContactPoint.SelectedIndexChanged += new System.EventHandler(this.cmbContactPoint_SelectedIndexChanged);
            // 
            // AnimationPanel
            // 
            this.AnimationPanel.def = null;
            this.AnimationPanel.Location = new System.Drawing.Point(23, 242);
            this.AnimationPanel.Name = "AnimationPanel";
            this.AnimationPanel.ShowMultiSprite = false;
            this.AnimationPanel.ShowOnion = false;
            this.AnimationPanel.Size = new System.Drawing.Size(1330, 585);
            this.AnimationPanel.TabIndex = 1;
            this.AnimationPanel.ZoomFactor = 1F;
            // 
            // chkshowhidemultisprite
            // 
            this.chkshowhidemultisprite.AutoSize = true;
            this.chkshowhidemultisprite.Checked = true;
            this.chkshowhidemultisprite.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkshowhidemultisprite.Location = new System.Drawing.Point(1144, 146);
            this.chkshowhidemultisprite.Name = "chkshowhidemultisprite";
            this.chkshowhidemultisprite.Size = new System.Drawing.Size(161, 17);
            this.chkshowhidemultisprite.TabIndex = 15;
            this.chkshowhidemultisprite.Text = "Afficher les sprites attachées";
            this.chkshowhidemultisprite.UseVisualStyleBackColor = true;
            this.chkshowhidemultisprite.CheckedChanged += new System.EventHandler(this.chkshowhidemultisprite_CheckedChanged);
            // 
            // grpattachedsprites
            // 
            this.grpattachedsprites.Controls.Add(this.btnEditSprite);
            this.grpattachedsprites.Controls.Add(this.cmbattachedsprites);
            this.grpattachedsprites.Controls.Add(this.btnminussprite);
            this.grpattachedsprites.Controls.Add(this.labeldebug);
            this.grpattachedsprites.Controls.Add(this.btnplussprite);
            this.grpattachedsprites.Location = new System.Drawing.Point(251, 841);
            this.grpattachedsprites.Name = "grpattachedsprites";
            this.grpattachedsprites.Size = new System.Drawing.Size(306, 124);
            this.grpattachedsprites.TabIndex = 18;
            this.grpattachedsprites.TabStop = false;
            this.grpattachedsprites.Text = "Sprites attachés";
            // 
            // btnEditSprite
            // 
            this.btnEditSprite.Enabled = false;
            this.btnEditSprite.Image = global::LMS.Properties.Resources.pencil_icon_48;
            this.btnEditSprite.Location = new System.Drawing.Point(121, 46);
            this.btnEditSprite.Name = "btnEditSprite";
            this.btnEditSprite.Size = new System.Drawing.Size(69, 68);
            this.btnEditSprite.TabIndex = 16;
            this.btnEditSprite.UseVisualStyleBackColor = true;
            this.btnEditSprite.Click += new System.EventHandler(this.btnEditSprite_Click);
            // 
            // cmbattachedsprites
            // 
            this.cmbattachedsprites.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbattachedsprites.FormattingEnabled = true;
            this.cmbattachedsprites.Location = new System.Drawing.Point(6, 19);
            this.cmbattachedsprites.Name = "cmbattachedsprites";
            this.cmbattachedsprites.Size = new System.Drawing.Size(292, 21);
            this.cmbattachedsprites.TabIndex = 15;
            this.cmbattachedsprites.SelectedIndexChanged += new System.EventHandler(this.cmbattachedsprites_SelectedIndexChanged);
            // 
            // btnminussprite
            // 
            this.btnminussprite.Enabled = false;
            this.btnminussprite.Image = global::LMS.Properties.Resources.round_minus_icon_48;
            this.btnminussprite.Location = new System.Drawing.Point(196, 46);
            this.btnminussprite.Name = "btnminussprite";
            this.btnminussprite.Size = new System.Drawing.Size(69, 68);
            this.btnminussprite.TabIndex = 14;
            this.btnminussprite.UseVisualStyleBackColor = true;
            this.btnminussprite.Click += new System.EventHandler(this.btnminussprite_Click);
            // 
            // btnplussprite
            // 
            this.btnplussprite.Image = global::LMS.Properties.Resources.round_plus_icon_48;
            this.btnplussprite.Location = new System.Drawing.Point(46, 46);
            this.btnplussprite.Name = "btnplussprite";
            this.btnplussprite.Size = new System.Drawing.Size(69, 68);
            this.btnplussprite.TabIndex = 13;
            this.btnplussprite.UseVisualStyleBackColor = true;
            this.btnplussprite.Click += new System.EventHandler(this.btnplussprite_Click);
            // 
            // grpcontactpoints
            // 
            this.grpcontactpoints.Controls.Add(this.copyonallframes);
            this.grpcontactpoints.Controls.Add(this.btneditanchor);
            this.grpcontactpoints.Controls.Add(this.cmbContactPoint);
            this.grpcontactpoints.Controls.Add(this.btnsuppranchor);
            this.grpcontactpoints.Controls.Add(this.label1);
            this.grpcontactpoints.Controls.Add(this.btnaddanchor);
            this.grpcontactpoints.Location = new System.Drawing.Point(563, 841);
            this.grpcontactpoints.Name = "grpcontactpoints";
            this.grpcontactpoints.Size = new System.Drawing.Size(374, 124);
            this.grpcontactpoints.TabIndex = 19;
            this.grpcontactpoints.TabStop = false;
            this.grpcontactpoints.Text = "Points de contact / ancres";
            // 
            // copyonallframes
            // 
            this.copyonallframes.Enabled = false;
            this.copyonallframes.Image = global::LMS.Properties.Resources.share_icon_48;
            this.copyonallframes.Location = new System.Drawing.Point(183, 46);
            this.copyonallframes.Name = "copyonallframes";
            this.copyonallframes.Size = new System.Drawing.Size(69, 68);
            this.copyonallframes.TabIndex = 21;
            this.copyonallframes.UseVisualStyleBackColor = true;
            this.copyonallframes.Click += new System.EventHandler(this.copyonallframes_Click);
            // 
            // btneditanchor
            // 
            this.btneditanchor.Enabled = false;
            this.btneditanchor.Image = global::LMS.Properties.Resources.pencil_icon_48;
            this.btneditanchor.Location = new System.Drawing.Point(108, 46);
            this.btneditanchor.Name = "btneditanchor";
            this.btneditanchor.Size = new System.Drawing.Size(69, 68);
            this.btneditanchor.TabIndex = 20;
            this.btneditanchor.UseVisualStyleBackColor = true;
            this.btneditanchor.Click += new System.EventHandler(this.btneditanchor_Click);
            // 
            // btnsuppranchor
            // 
            this.btnsuppranchor.Enabled = false;
            this.btnsuppranchor.Image = global::LMS.Properties.Resources.round_minus_icon_48;
            this.btnsuppranchor.Location = new System.Drawing.Point(258, 46);
            this.btnsuppranchor.Name = "btnsuppranchor";
            this.btnsuppranchor.Size = new System.Drawing.Size(69, 68);
            this.btnsuppranchor.TabIndex = 19;
            this.btnsuppranchor.UseVisualStyleBackColor = true;
            this.btnsuppranchor.Click += new System.EventHandler(this.btnsuppranchor_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(194, 74);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 17;
            // 
            // btnaddanchor
            // 
            this.btnaddanchor.Image = global::LMS.Properties.Resources.round_plus_icon_48;
            this.btnaddanchor.Location = new System.Drawing.Point(33, 46);
            this.btnaddanchor.Name = "btnaddanchor";
            this.btnaddanchor.Size = new System.Drawing.Size(69, 68);
            this.btnaddanchor.TabIndex = 18;
            this.btnaddanchor.UseVisualStyleBackColor = true;
            this.btnaddanchor.Click += new System.EventHandler(this.btnaddanchor_Click);
            // 
            // debug
            // 
            this.debug.AutoSize = true;
            this.debug.Location = new System.Drawing.Point(74, 321);
            this.debug.Name = "debug";
            this.debug.Size = new System.Drawing.Size(0, 13);
            this.debug.TabIndex = 20;
            // 
            // btnremoveframe
            // 
            this.btnremoveframe.Image = global::LMS.Properties.Resources.round_minus_icon_24;
            this.btnremoveframe.Location = new System.Drawing.Point(611, 194);
            this.btnremoveframe.Name = "btnremoveframe";
            this.btnremoveframe.Size = new System.Drawing.Size(32, 32);
            this.btnremoveframe.TabIndex = 17;
            this.btnremoveframe.UseVisualStyleBackColor = true;
            this.btnremoveframe.Click += new System.EventHandler(this.btnremoveframe_Click);
            // 
            // btnaddframe
            // 
            this.btnaddframe.Image = global::LMS.Properties.Resources.round_plus_icon_24;
            this.btnaddframe.Location = new System.Drawing.Point(573, 194);
            this.btnaddframe.Name = "btnaddframe";
            this.btnaddframe.Size = new System.Drawing.Size(32, 32);
            this.btnaddframe.TabIndex = 16;
            this.btnaddframe.UseVisualStyleBackColor = true;
            this.btnaddframe.Click += new System.EventHandler(this.btnaddframe_Click);
            // 
            // btnstop
            // 
            this.btnstop.Image = global::LMS.Properties.Resources.playback_stop_icon_48;
            this.btnstop.Location = new System.Drawing.Point(1067, 100);
            this.btnstop.Name = "btnstop";
            this.btnstop.Size = new System.Drawing.Size(48, 48);
            this.btnstop.TabIndex = 5;
            this.btnstop.UseVisualStyleBackColor = true;
            this.btnstop.Click += new System.EventHandler(this.btnstop_Click);
            // 
            // btnplay
            // 
            this.btnplay.Image = global::LMS.Properties.Resources.playback_play_icon_48;
            this.btnplay.Location = new System.Drawing.Point(1013, 100);
            this.btnplay.Name = "btnplay";
            this.btnplay.Size = new System.Drawing.Size(48, 48);
            this.btnplay.TabIndex = 4;
            this.btnplay.UseVisualStyleBackColor = true;
            this.btnplay.Click += new System.EventHandler(this.btnplay_Click);
            // 
            // cmbZoom
            // 
            this.cmbZoom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmbZoom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbZoom.FormattingEnabled = true;
            this.cmbZoom.Items.AddRange(new object[] {
            "0,25",
            "0,5",
            "1,0",
            "2,0",
            "4,0",
            "8,0"});
            this.cmbZoom.Location = new System.Drawing.Point(1083, 194);
            this.cmbZoom.Name = "cmbZoom";
            this.cmbZoom.Size = new System.Drawing.Size(87, 21);
            this.cmbZoom.TabIndex = 22;
            this.cmbZoom.SelectedIndexChanged += new System.EventHandler(this.cmbZoom_SelectedIndexChanged);
            // 
            // lbl_zoom
            // 
            this.lbl_zoom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbl_zoom.AutoSize = true;
            this.lbl_zoom.Location = new System.Drawing.Point(1023, 197);
            this.lbl_zoom.Name = "lbl_zoom";
            this.lbl_zoom.Size = new System.Drawing.Size(40, 13);
            this.lbl_zoom.TabIndex = 21;
            this.lbl_zoom.Text = "Zoom :";
            // 
            // boutonactions
            // 
            this.boutonactions.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonZoomplus,
            this.toolStripButtonZoomminus,
            this.toolStripButtonZoomreset});
            this.boutonactions.Location = new System.Drawing.Point(0, 24);
            this.boutonactions.Name = "boutonactions";
            this.boutonactions.Size = new System.Drawing.Size(1380, 25);
            this.boutonactions.TabIndex = 23;
            this.boutonactions.Text = "toolStrip1";
            // 
            // toolStripButtonZoomplus
            // 
            this.toolStripButtonZoomplus.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonZoomplus.Enabled = false;
            this.toolStripButtonZoomplus.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonZoomplus.Image")));
            this.toolStripButtonZoomplus.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonZoomplus.Name = "toolStripButtonZoomplus";
            this.toolStripButtonZoomplus.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonZoomplus.Text = "Zoomer";
            this.toolStripButtonZoomplus.Click += new System.EventHandler(this.toolStripButtonZoomplus_Click);
            // 
            // toolStripButtonZoomminus
            // 
            this.toolStripButtonZoomminus.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonZoomminus.Enabled = false;
            this.toolStripButtonZoomminus.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonZoomminus.Image")));
            this.toolStripButtonZoomminus.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonZoomminus.Name = "toolStripButtonZoomminus";
            this.toolStripButtonZoomminus.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonZoomminus.Text = "Dézoomer";
            this.toolStripButtonZoomminus.Click += new System.EventHandler(this.toolStripButtonZoomminus_Click);
            // 
            // toolStripButtonZoomreset
            // 
            this.toolStripButtonZoomreset.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonZoomreset.Enabled = false;
            this.toolStripButtonZoomreset.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonZoomreset.Image")));
            this.toolStripButtonZoomreset.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonZoomreset.Name = "toolStripButtonZoomreset";
            this.toolStripButtonZoomreset.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonZoomreset.Text = "Zoom par défaut";
            this.toolStripButtonZoomreset.Click += new System.EventHandler(this.toolStripButtonZoomreset_Click);
            // 
            // redimensionnerEtRegénérerFramesToolStripMenuItem
            // 
            this.redimensionnerEtRegénérerFramesToolStripMenuItem.Name = "redimensionnerEtRegénérerFramesToolStripMenuItem";
            this.redimensionnerEtRegénérerFramesToolStripMenuItem.Size = new System.Drawing.Size(274, 22);
            this.redimensionnerEtRegénérerFramesToolStripMenuItem.Text = "Redimensionner et regénérer frames...";
            this.redimensionnerEtRegénérerFramesToolStripMenuItem.Click += new System.EventHandler(this.redimensionnerEtRegénérerFramesToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1380, 1002);
            this.Controls.Add(this.boutonactions);
            this.Controls.Add(this.cmbZoom);
            this.Controls.Add(this.lbl_zoom);
            this.Controls.Add(this.debug);
            this.Controls.Add(this.grpcontactpoints);
            this.Controls.Add(this.grpattachedsprites);
            this.Controls.Add(this.btnremoveframe);
            this.Controls.Add(this.btnaddframe);
            this.Controls.Add(this.chkshowhidemultisprite);
            this.Controls.Add(this.chkonion);
            this.Controls.Add(this.lbldelay);
            this.Controls.Add(this.numFrameDelay);
            this.Controls.Add(this.chkloop);
            this.Controls.Add(this.btnstop);
            this.Controls.Add(this.btnplay);
            this.Controls.Add(this.panelFrames);
            this.Controls.Add(this.cmbAnimations);
            this.Controls.Add(this.AnimationPanel);
            this.Controls.Add(this.menuPrincipal);
            this.MainMenuStrip = this.menuPrincipal;
            this.Name = "MainForm";
            this.Text = "Le Moteur S\'Anime";
            this.menuPrincipal.ResumeLayout(false);
            this.menuPrincipal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numFrameDelay)).EndInit();
            this.grpattachedsprites.ResumeLayout(false);
            this.grpattachedsprites.PerformLayout();
            this.grpcontactpoints.ResumeLayout(false);
            this.grpcontactpoints.PerformLayout();
            this.boutonactions.ResumeLayout(false);
            this.boutonactions.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuPrincipal;
        private System.Windows.Forms.ToolStripMenuItem fichierToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nouveauToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ouvrirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enregistrerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enregistreSousToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fermerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem préférencesToolStripMenuItem;
        private NativeAnimationPanel AnimationPanel;
        private System.Windows.Forms.OpenFileDialog ouvrirFichier;
        private System.Windows.Forms.ComboBox cmbAnimations;
        private System.Windows.Forms.Panel panelFrames;
        private System.Windows.Forms.Button btnplay;
        private System.Windows.Forms.Button btnstop;
        private System.Windows.Forms.Label labeldebug;
        private System.Windows.Forms.CheckBox chkloop;
        private System.Windows.Forms.NumericUpDown numFrameDelay;
        private System.Windows.Forms.Label lbldelay;
        private System.Windows.Forms.ToolStripMenuItem importerDepuisUnFichierDeSortieToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog enregProjet;
        private System.Windows.Forms.OpenFileDialog importFichier;
        private System.Windows.Forms.ToolStripMenuItem ajouterAnimationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem regénérerFramesDepuisGimpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exporterToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog exportProjet;
        private System.Windows.Forms.CheckBox chkonion;
        private System.Windows.Forms.ComboBox cmbContactPoint;
        private System.Windows.Forms.Button btnplussprite;
        private System.Windows.Forms.Button btnminussprite;
        private System.Windows.Forms.CheckBox chkshowhidemultisprite;
        private System.Windows.Forms.Button btnremoveframe;
        private System.Windows.Forms.Button btnaddframe;
        private System.Windows.Forms.GroupBox grpattachedsprites;
        private System.Windows.Forms.ComboBox cmbattachedsprites;
        private System.Windows.Forms.Button btnEditSprite;
        private System.Windows.Forms.GroupBox grpcontactpoints;
        private System.Windows.Forms.Button btneditanchor;
        private System.Windows.Forms.Button btnsuppranchor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnaddanchor;
        private System.Windows.Forms.Label debug;
        private System.Windows.Forms.Button copyonallframes;
        private System.Windows.Forms.ComboBox cmbZoom;
        private System.Windows.Forms.Label lbl_zoom;
        private System.Windows.Forms.ToolStrip boutonactions;
        private System.Windows.Forms.ToolStripButton toolStripButtonZoomplus;
        private System.Windows.Forms.ToolStripButton toolStripButtonZoomminus;
        private System.Windows.Forms.ToolStripButton toolStripButtonZoomreset;
        private System.Windows.Forms.ToolStripMenuItem redimensionnerEtRegénérerFramesToolStripMenuItem;
    }
}

