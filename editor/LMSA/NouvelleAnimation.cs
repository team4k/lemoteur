﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using AssetsImport;
using ImageMagick;
using LMS.Properties;
using PBInterface;

namespace LMS
{
    public partial class NouvelleAnimation : Form,IlogImport
    {
        List<FileInfo> selectedframes = new List<FileInfo>();
        List<FrameBitmap> animationframes = new List<FrameBitmap>();
        List<ResizeCursor> currentCursors = new List<ResizeCursor>();

        ResizeCursor selectedCursor = null;

        int animationindex = 0;

        bool showorig = false;

        bool cursordrag = false;

        XDocument refdocprojet = null;

        string refassetsdir = string.Empty;

        public NouvelleAnimation(XDocument document,string assetsdir)
        {
            refdocprojet = document;
            refassetsdir = assetsdir;
            InitializeComponent();
        }

        private void btnselectframes_Click(object sender, EventArgs e)
        {
            if (ofdframes.ShowDialog() == DialogResult.OK)
            {
                animationindex = 0;
                foreach (FrameBitmap bmp in animationframes)
                {
                    bmp.Dispose();
                }

                animationframes.Clear();

                selectedframes = new List<FileInfo>();

                foreach (string frame in ofdframes.FileNames)
                {
                    selectedframes.Add(new FileInfo(frame));
                }

                lbframes.DataSource = selectedframes;
                lbframes.DisplayMember = "Name";
            }
        }

        private void Dopotracing(bool isscan)
        {

            if(Import.TraceAndGimpPng(Settings.Default.potrace_path, selectedframes, this, out animationframes, pbpreview.Width, pbpreview.Height,isscan))
            {
                pbpreview.Image = animationframes[0].potracedscan;
                animationindex = 0;
            }
        }

        private void btnnext1_Click(object sender, EventArgs e)
        {
            if (txtanimation.Text.Trim().Length == 0)
            {
                MessageBox.Show("Le nom de l'animation ne peut pas être vide !");
                return;
            }

            if (selectedframes.Count == 0)
            {
                MessageBox.Show("Une animation doit avoir au moins une frame !");
                return;
            }

            if(chkisscan.Checked)
            {
                rtblog.AppendText("Conversion en svg en cours...");
            }
            else
            {
                rtblog.AppendText("Import en cours...");
            }

            this.Dopotracing(chkisscan.Checked);

            tcNouvelleanimation.SelectedTab = etape2;

        }

        public void AppendLog(string data,bool iserror = false, bool threaded = false)
        {
            if(data == null)
            {
                return;
            }

            Color textcolor = Color.Gray;

            if(iserror)
            {
                textcolor = Color.Red;
            }

            if (threaded)
            {
                this.BeginInvoke(new MethodInvoker(() =>
                {
                    rtblog.ForeColor = textcolor;
                    rtblog.AppendText(data);

                    rtblog.AppendText(Environment.NewLine);
                }));
            }
            else
            {
                rtblog.ForeColor = textcolor;
                rtblog.AppendText(data);

                rtblog.AppendText(Environment.NewLine);
            }
        }

        public void DoEvents()
        {
            Application.DoEvents();
        }

        public void AppendText(string data)
        {
            rtblog.AppendText(data);
        }

        private void btnright_Click(object sender, EventArgs e)
        {
            if (++animationindex >= animationframes.Count)
            {
                animationindex--;
                return;
            }

            pbpreview.Image = animationframes[animationindex].GetContent(showorig);
            numX.Value = animationframes[animationindex].ClipRectangle.X;
            numY.Value = animationframes[animationindex].ClipRectangle.Y;
            numwidth.Value = animationframes[animationindex].ClipRectangle.Width;
            numHeight.Value = animationframes[animationindex].ClipRectangle.Height;


        }

        private void btnleft_Click(object sender, EventArgs e)
        {
            if (--animationindex < 0)
            {
                animationindex++;
                return;
            }

            pbpreview.Image = animationframes[animationindex].GetContent(showorig);
            numX.Value = animationframes[animationindex].ClipRectangle.X;
            numY.Value = animationframes[animationindex].ClipRectangle.Y;
            numwidth.Value = animationframes[animationindex].ClipRectangle.Width;
            numHeight.Value = animationframes[animationindex].ClipRectangle.Height;
        }

        private void btncreeranimation_Click(object sender, EventArgs e)
        {
            AnimationDef animationdef = new AnimationDef();
            animationdef.Name = txtanimation.Text;
            animationdef.Offsetx = 0;
            animationdef.Offsety = ProjetGenerator.GetNextOffsetY(refdocprojet);
            List<int> framenumber = new List<int>();
            List<float> timesteps = new List<float>();

            //lookup max number of frames

            int startframe = ProjetGenerator.GetNextFrame(refdocprojet);

            //add all frames and save in assets directories
            foreach(FrameBitmap frame in animationframes)
            {
                string outscan = Path.Combine(refassetsdir, "0_scans", "frame" + startframe.ToString() + ".png");
                string outpotrace = Path.Combine(refassetsdir, "1_potrace", "frame" + startframe.ToString() + ".png");
                string outframe = Path.Combine(refassetsdir, "3_frames", "frame" + startframe.ToString() + ".png");
                framenumber.Add(startframe++);
                timesteps.Add(33.5f);

                //save generated bitmaps
                frame.SaveAll(outscan, outpotrace, outframe);
            }


            animationdef.Frames = framenumber.ToArray();
            animationdef.Timesteps = timesteps.ToArray();
            animationdef.Width = animationframes[0].GetContent(false).Width;
            animationdef.Height = animationframes[0].GetContent(false).Height;

            //call gimp conversion script to translate potraced scan to xcf

            string pathpython = string.Format(@"BatchConvertLms.py DoConvert {0} {1}\", Path.Combine(refassetsdir, "1_potrace", "*.png"), Path.Combine(refassetsdir, "2_gimp"));

            //double antislashes on windows, otherwise the batch will fail
            pathpython = pathpython.Replace(@"\", @"\\");

            ProcessStartInfo GimpConversionInfo = new ProcessStartInfo("python", pathpython);
            GimpConversionInfo.UseShellExecute = false;
            GimpConversionInfo.RedirectStandardOutput = true;
            GimpConversionInfo.RedirectStandardError = true;
            GimpConversionInfo.WorkingDirectory = Environment.CurrentDirectory;

            Process Gimpproc = Process.Start(GimpConversionInfo);
            Gimpproc.OutputDataReceived += (osender, oe) => this.AppendLog(oe.Data, threaded: true);
            Gimpproc.ErrorDataReceived += (osender, oe) => this.AppendLog(oe.Data, iserror: true, threaded: true);
            Gimpproc.BeginOutputReadLine();
            Gimpproc.BeginErrorReadLine();

            while (!Gimpproc.HasExited)
                Application.DoEvents();

            ProjetGenerator.AddAnimationToProject(refdocprojet, animationdef);

            this.DialogResult = DialogResult.OK;

        }

        private void chkorig_CheckedChanged(object sender, EventArgs e)
        {
            showorig = chkorig.Checked;

            pbpreview.Image = animationframes[animationindex].GetContent(showorig);
        }

        private void pbpreview_Paint(object sender, PaintEventArgs e)
        {
            ResizeEvents.Paint((PictureBox)sender, currentCursors, animationframes[animationindex], e);
        }



        private void pbpreview_MouseClick(object sender, MouseEventArgs e)
        {
           
        }

        private void pbpreview_MouseDown(object sender, MouseEventArgs e)
        {
            cursordrag = true;

            selectedCursor = ResizeEvents.MouseDown(currentCursors,e);

            pbpreview.Invalidate();
        }

        private void pbpreview_MouseUp(object sender, MouseEventArgs e)
        {
            if(ResizeEvents.MouseUp(ref selectedCursor))
            {
                pbpreview.Invalidate();
            }

            cursordrag = false;
        }

        private void pbpreview_MouseMove(object sender, MouseEventArgs e)
        {
            if(ResizeEvents.MouseMove(cursordrag,selectedCursor,currentCursors, animationframes[animationindex], animationframes,e))
            {
                numX.Value = animationframes[animationindex].ClipRectangle.X;
                numY.Value = animationframes[animationindex].ClipRectangle.Y;
                numwidth.Value = animationframes[animationindex].ClipRectangle.Width;
                numHeight.Value = animationframes[animationindex].ClipRectangle.Height;

                pbpreview.Invalidate();
            }
        }

        private void btnCrop_Click(object sender, EventArgs e)
        {

            int offsetx = animationframes[animationindex].GetContent(showorig).Width - pbpreview.Width;

            int offsety = animationframes[animationindex].GetContent(showorig).Height - pbpreview.Height;

            if (offsetx != 0)
                offsetx /= 2;

            if (offsety != 0)
                offsety /= 2;

            int startcropx = animationframes[animationindex].ClipRectangle.X + offsetx;
            int startcropy = animationframes[animationindex].ClipRectangle.Y + offsety;

            foreach (FrameBitmap bmp in animationframes)
            {
                bmp.CropScan(startcropx, startcropy);
            }

            pbpreview.Image = animationframes[animationindex].GetContent(showorig);

            pbpreview.Invalidate();

        }

        private void btnRedim_Click(object sender, EventArgs e)
        {
            //redimensionnement de toutes les frames
            ResizeWidget widget = new ResizeWidget();

            if (widget.ShowDialog() == DialogResult.OK)
            {
                foreach (FrameBitmap frame in animationframes)
                {
                    frame.RedimScan(Convert.ToInt32(widget.numwidth.Value), Convert.ToInt32(widget.numheight.Value));
                }

                pbpreview.Image = animationframes[animationindex].GetContent(showorig);

                pbpreview.Invalidate();
            }

        }

        private void btnraz_Click(object sender, EventArgs e)
        {
            foreach (FrameBitmap frame in animationframes)
            {
                frame.Dispose();
            }

            animationframes.Clear();

            this.Dopotracing(chkisscan.Checked);
        }
    }
}
