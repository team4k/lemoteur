﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS
{
    public class FrameData
    {
        public int BasePos { get; }
        public int NewPos { get; set; }
        public float Delay { get; set; }
        public int FramePos { get; }


        public FrameData(int basepos,float delay,int framepos)
        {
            BasePos = basepos;
            NewPos = basepos;
            Delay = delay;
            FramePos = framepos;
        }

    }
}
