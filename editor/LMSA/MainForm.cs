﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PBInterface;
using System.IO;
using System.Drawing.Drawing2D;
using System.Xml.Linq;
using System.Drawing.Imaging;
using System.Xml;
using System.Globalization;
using ImageMagick;
using System.Diagnostics;
using LMS.Properties;
using AssetsImport;

namespace LMS
{
    public partial class MainForm : Form
    {
        private const string title = "Le Moteur S'Anime";

        private bool animationloaded = false;
        private Bitmap loadedtexture = null;
        private AnimationList loadedanimation = null;


        private PictureBox selectedFrame = null;

        //dragdrop frames
        private bool framedragdrop = false;
        private int maxdrag = 0;
        private Point dragpoint = new Point();

        private const int FRAME_SIZE = 128;

        private List<FrameData> animationFrames = new List<FrameData>();

        private XDocument docprojet = null;
        private string nomprojet = string.Empty;
        private string assetsdir = string.Empty;
        private string pathprojet = string.Empty;

        public Dictionary<string, float> zoom_value = new Dictionary<string, float>() {
            {"100%",1.0f},
            {"150%",0.75f},
            {"200%",0.5f},
            {"400%",0.25f},
            {"800%",0.125f}};



        public MainForm()
        {
            InitializeComponent();

            string path = @"D:\devel\gamedevel\lombricgit\game_template\bin\assets";

            AnimationPanel.initrender(path);

            Application.Idle += Application_Idle;

            AnimationPanel.setlabeldebug(debug);

            cmbZoom.DataSource = zoom_value.ToArray<KeyValuePair<string, float>>();
            cmbZoom.DisplayMember = "Key";
            cmbZoom.ValueMember = "Value";

            

        }

        void Application_Idle(object sender, EventArgs e)
        {
            // update render here
            AnimationPanel.Invalidate();
        }

        private void nouveauToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NouveauProjet creerprojet = new NouveauProjet();

            if (creerprojet.ShowDialog() == DialogResult.OK)
            {
                docprojet = new XDocument(
                    new XDeclaration("1.0", "utf-8", "yes"),
                    new XElement("lmsprojet",
                        new XElement("nom", creerprojet.txtNouveauProjet.Text),
                        new XElement("datesauvegarde", DateTime.Now.ToString()),
                        new XElement("dernieroutput", DateTime.Now.ToString())
                        ));

                nomprojet = creerprojet.txtNouveauProjet.Text + ".xml";
                this.Text = title + " > " + nomprojet;
                pathprojet = string.Empty;
            }
        }

        private void enregistrerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (docprojet != null)
            {
                if(!string.IsNullOrEmpty(pathprojet))
                {
                    docprojet.Save(pathprojet);
                    MessageBox.Show("Sauvegarde effectuée !", "Résultat sauvegarde", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    this.enregistreSousToolStripMenuItem_Click(sender, e);
                }
                
            }
        }


        private void enregistreSousToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(docprojet != null)
            {
                if(enregProjet.ShowDialog() == DialogResult.OK)
                {
                    FileInfo enreginfo = new FileInfo(enregProjet.FileName);
                    docprojet.Save(enregProjet.FileName); //Save to file

                    //create assets directory
                    assetsdir = Path.Combine(enreginfo.DirectoryName, enreginfo.Name + "_assets");

                    if(!Directory.Exists(assetsdir))
                    {
                        Directory.CreateDirectory(assetsdir);

                        Directory.CreateDirectory(Path.Combine(assetsdir, "0_scans"));
                        Directory.CreateDirectory(Path.Combine(assetsdir,"1_potrace"));
                        Directory.CreateDirectory(Path.Combine(assetsdir, "2_gimp"));
                        Directory.CreateDirectory(Path.Combine(assetsdir, "3_frames"));

                    }

                    if(loadedanimation != null)
                    {
                        foreach (AnimationDef oanim in loadedanimation.AnimArrays)
                        {
                            //save frames
                            for (int frame = 0; frame < oanim.Frames.Length; frame++)
                            {
                                int offsetx = oanim.Offsetx + (frame * oanim.Width);
                                int offsety = oanim.Offsety;

                                Bitmap bmptosave = BitmapUtilities.GetFrameFromBitmap(loadedtexture, offsetx, offsety, oanim.Width, oanim.Height);
                                bmptosave.Save(Path.Combine(assetsdir, "3_frames", "frame" + oanim.Frames[frame].ToString() + ".png"), ImageFormat.Png);
                            }
                        }
                    }
                    

                    MessageBox.Show("Sauvegarde effectuée !", "Résultat sauvegarde", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    pathprojet = enregProjet.FileName;
                }
            }
        }

        private void OpenAnimationList(AnimationList animationdata,string animfile)
        {

            cmbAnimations.Items.Clear();

            foreach (AnimationDef oanim in animationdata.AnimArrays)
            {
                KeyValuePair<string, AnimationDef> animentry = new KeyValuePair<string, AnimationDef>(oanim.Name, oanim);
                cmbAnimations.Items.Add(animentry);

                if (oanim.Width == 0 || oanim.Height == 0)
                {
                    MessageBox.Show("Impossible de charger l'animation " + oanim.Name + "sans dimension de frame définie ! (Width ou height = 0)", "Erreur animation", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    cmbAnimations.Items.Clear();
                    return;
                }

            }

            FileInfo finfo = new FileInfo(animfile);

            string texturefile = finfo.DirectoryName + Path.DirectorySeparatorChar + finfo.Name.Substring(0, finfo.Name.LastIndexOf(finfo.Extension)) + ".png";

            if (loadedtexture != null)
                loadedtexture.Dispose();

            loadedtexture = new Bitmap(texturefile);
            byte[] texture = BitmapUtilities.GetBitmapContent(loadedtexture);

            byte[] anim = File.ReadAllBytes(animfile);

            NativeAnimationFuncs.native_animation_loadsprite(animationdata.AnimArrays[0].Width, animationdata.AnimArrays[0].Height, loadedtexture.Width, loadedtexture.Height, texture, (uint)texture.Length, anim, (uint)anim.Length);

            loadedanimation = animationdata;
            animationloaded = true;
        }

        private void OpenAnimationList(AnimationList animationdata, Bitmap fulltexture)
        {

            cmbAnimations.Items.Clear();

            foreach (AnimationDef oanim in animationdata.AnimArrays)
            {
                KeyValuePair<string, AnimationDef> animentry = new KeyValuePair<string, AnimationDef>(oanim.Name, oanim);
                cmbAnimations.Items.Add(animentry);

                if (oanim.Width == 0 || oanim.Height == 0)
                {
                    MessageBox.Show("Impossible de charger l'animation " + oanim.Name + "sans dimension de frame définie ! (Width ou height = 0)", "Erreur animation", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    cmbAnimations.Items.Clear();
                    return;
                }

            }

            if (loadedtexture != null)
                loadedtexture.Dispose();

            loadedtexture = fulltexture;
            byte[] texture = BitmapUtilities.GetBitmapContent(loadedtexture);

            MemoryStream dataStream = new MemoryStream();
            dataStream.Position = 0;

            ProtoBuf.Serializer.Serialize<PBInterface.AnimationList>(dataStream, animationdata);

            dataStream.Position = 0;
            byte[] anim = dataStream.ToArray();
            dataStream.Close();

            NativeAnimationFuncs.native_animation_loadsprite(animationdata.AnimArrays[0].Width, animationdata.AnimArrays[0].Height, loadedtexture.Width, loadedtexture.Height, texture, (uint)texture.Length, anim, (uint)anim.Length);

            loadedanimation = animationdata;
            animationloaded = true;
        }

        private void ouvrirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult choixfichier = ouvrirFichier.ShowDialog();

            if (choixfichier == DialogResult.OK)
            {
                if (animationloaded)
                {
                    NativeAnimationFuncs.native_animation_unloadsprite();
                }

                StreamReader reader = new StreamReader(ouvrirFichier.OpenFile());

                string content = reader.ReadToEnd();
                reader.Close();

                if (!(content.Contains("<lmsprojet>") && content.Contains("</lmsprojet>")))
                {
                    MessageBox.Show("Le fichier sélectionné n'est pas un projet LMS ! ", "Format projet non conforme", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                Stream open = ouvrirFichier.OpenFile();

                docprojet = XDocument.Load(open);
                open.Close();
                FileInfo projetinfo = new FileInfo(ouvrirFichier.FileName);

                Bitmap fulltexture = null;

                assetsdir = Path.Combine(projetinfo.DirectoryName, ouvrirFichier.SafeFileName + "_assets");

                AnimationList projetanimation = ProjetGenerator.GetAnimationListFromProject(docprojet, assetsdir, out fulltexture);
                

                   //Stream filestream = ouvrirFichier.OpenFile();
                  // AnimationList animationdata = ProtoBuf.Serializer.Deserialize<PBInterface.AnimationList>(filestream);

                   if (projetanimation.AnimArrays.Count > 0)
                   {
                    this.OpenAnimationList(projetanimation, fulltexture);
                   }

                this.Text = title + " > " + ouvrirFichier.SafeFileName;

                nomprojet = ouvrirFichier.SafeFileName;

                cmbattachedsprites.Items.Clear();

                Dictionary<string, AnchorData> tmp = ProjetGenerator.GetAnchoredAnimationFromProject(docprojet, projetanimation);
                
                foreach (KeyValuePair<string,AnchorData> data in tmp)
                {
                    NativeAnimationFuncs.native_animation_addnewsprite(data.Key, data.Value.SrcAnchor.X, data.Value.SrcAnchor.Y, data.Value.BaseAnimation);
                    cmbattachedsprites.Items.Add(data.Key);
                }

                AnimationPanel.SetAnchorMap(tmp);
                AnimationPanel.SetRefDoc(docprojet);

                pathprojet = ouvrirFichier.FileName;
               

            }
        }

        private void cmbAnimations_SelectedIndexChanged(object sender, EventArgs e)
        {
            NativeAnimationFuncs.native_animation_stopanimation();

            AnimationDef def = ((KeyValuePair<string, AnimationDef>)cmbAnimations.SelectedItem).Value;

            NativeAnimationFuncs.native_animation_playanimation(def.Name);

            chkloop.Checked = def.Loop;

            AnimationPanel.def = def;

            //création des picturebox pour chaque frame de notre animation
            selectedFrame = null;
            panelFrames.Controls.Clear();

            int positionx = 0;

            PictureBox lastframe = null;

            animationFrames.Clear();

            for (int frame = 0; frame < def.Frames.Length;frame++)
            {
                int offsetx = def.Offsetx + (frame * def.Width);
                int offsety = def.Offsety;

                var oframe = new FrameData(frame, def.Timesteps[frame],def.Frames[frame]);
                
                PictureBox picframe = new PictureBox();
                picframe.Width = FRAME_SIZE;
                picframe.Height = FRAME_SIZE;
                picframe.SizeMode = PictureBoxSizeMode.Zoom;
                picframe.Image = BitmapUtilities.GetFrameFromBitmap(loadedtexture, offsetx, offsety, def.Width, def.Height);
                picframe.Tag = oframe;

                picframe.Click += Picframe_Click;
                picframe.Paint += Picframe_Paint;
                picframe.MouseDown += Picframe_MouseDown;
                picframe.MouseUp += Picframe_MouseUp;
                picframe.MouseMove += Picframe_MouseMove;

                Point picpos = new Point(positionx, 0);
                positionx += FRAME_SIZE;
                picframe.Location = picpos;
                panelFrames.Controls.Add(picframe);

                if (selectedFrame == null)
                    selectedFrame = picframe;

                lastframe = picframe;

                animationFrames.Add(oframe);
            }

            cmbContactPoint.Items.Clear();

            int aid = 0;

            if(def.AttachpointValues.Count > 0)
            {
                foreach (Avector point in def.AttachpointValues[0].Attachpointpos)
                {
                    if (!point.Isnull)
                    {
                        cmbContactPoint.Items.Add(def.AttachpointIds[aid]);
                    }

                    aid++;
                }
            }
            

          

            if (lastframe != null)
            {
                maxdrag = animationFrames.Count * FRAME_SIZE;
            }
        }

        private void Picframe_MouseDown(object sender, MouseEventArgs e)
        {
            framedragdrop = true;
            PictureBox box = (PictureBox)sender;

            FrameData data = (FrameData)box.Tag;

            NativeAnimationFuncs.native_animation_setframe(data.BasePos);

            if (selectedFrame != null)
            {
                selectedFrame.Invalidate();
            }

            selectedFrame = box;

            lbldelay.Text = "Délai frame " + data.NewPos.ToString()+ " : ";
            numFrameDelay.Value = Convert.ToDecimal(data.Delay);

            dragpoint = e.Location;

            box.Invalidate();
        }

        private void Picframe_MouseMove(object sender, MouseEventArgs e)
        {
            if(selectedFrame != null && framedragdrop)
            {
                int newleft = selectedFrame.Left + (e.Location.X - dragpoint.X);

                if (newleft < 0)
                    newleft = 0;

                selectedFrame.Left = newleft;
                selectedFrame.BringToFront();

                //labeldebug.Text = string.Format("frame x : {0} frame y : {1} mouse x : {2} mouse y : {3} panel x : {4} panel y : {5}", selectedFrame.Left, selectedFrame.Top, e.Location.X, e.Location.Y, panelFrames.Left, panelFrames.Top);
            }

        }

        private void Picframe_MouseUp(object sender, MouseEventArgs e)
        {
            if (selectedFrame != null && framedragdrop)
            {
                selectedFrame.Enabled = false;
                //find control center
                Point centerpoint = new Point(selectedFrame.Left + (selectedFrame.Width / 2), selectedFrame.Top + (selectedFrame.Width / 2));

                Control newpic =  panelFrames.GetChildAtPoint(centerpoint, GetChildAtPointSkip.Disabled);

                FrameData odata = (FrameData)selectedFrame.Tag;

                if (newpic != null && newpic is PictureBox && newpic != selectedFrame && centerpoint.X < maxdrag)
                {
                    
                    FrameData odata2 = (FrameData)newpic.Tag;

                    int posinlist = animationFrames.FindIndex(p => p.BasePos == odata2.BasePos);

                    animationFrames.Remove(odata);
                    animationFrames.Insert(posinlist, odata);
                    int pos = 0;

                    foreach(FrameData data in animationFrames)
                    {
                        data.NewPos = pos++;
                    }

                    //repositionnement des picture box

                    foreach(Control opic in panelFrames.Controls)
                    {
                        if(opic is PictureBox)
                        {
                            FrameData picdata = (FrameData)opic.Tag;

                            Point loc = opic.Location;

                            loc.X = ((picdata.NewPos) * FRAME_SIZE) + panelFrames.AutoScrollPosition.X;

                            opic.Location = loc;
                        }
                    }
                }
                else
                {
                    Point loc = selectedFrame.Location;
                    loc.X = ((odata.NewPos) * FRAME_SIZE) + panelFrames.AutoScrollPosition.X;
                    selectedFrame.Location = loc;
                    //labeldebug.Text = "not found new pic set to " + frameorigleft.ToString();
                }



                selectedFrame.Enabled = true;
                
            }

            framedragdrop = false;

        }

        private void Picframe_Paint(object sender, PaintEventArgs e)
        {
            PictureBox box = (PictureBox)sender;

            if (selectedFrame == box)
            {

                e.Graphics.InterpolationMode = InterpolationMode.NearestNeighbor;
                e.Graphics.SmoothingMode = SmoothingMode.None;

                //set the center of the pixel when drawing to 0.5 / 0.5 rather than the default 0 / 0 (default make the drawing start at -0.5)
                e.Graphics.PixelOffsetMode = PixelOffsetMode.Half;
                e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(125, 0, 0, 255)), 0, 0, box.Width, box.Height);
            }

        }

        private void Picframe_Click(object sender, EventArgs e)
        {
            
        }

        private void btnplay_Click(object sender, EventArgs e)
        {
            if (cmbAnimations.SelectedItem == null)
            {
                MessageBox.Show("Aucune animation sélectionnée !", "Aucune animation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            AnimationDef def = ((KeyValuePair<string, AnimationDef>)cmbAnimations.SelectedItem).Value;

            NativeAnimationFuncs.native_animation_playanimation(def.Name);
        }

        private void btnstop_Click(object sender, EventArgs e)
        {
            NativeAnimationFuncs.native_animation_stopanimation();

            if(panelFrames.Controls.Count > 0)
                selectedFrame = panelFrames.Controls[0] as PictureBox;

        }

        private void chkloop_CheckedChanged(object sender, EventArgs e)
        {
            if (cmbAnimations.SelectedItem == null)
                return;

            AnimationDef def = ((KeyValuePair<string, AnimationDef>)cmbAnimations.SelectedItem).Value;

            def.Loop = chkloop.Checked;
            ProjetGenerator.UpdateAnimation(docprojet, def);

            MemoryStream dataStream = new MemoryStream();
            dataStream.Position = 0;

            ProtoBuf.Serializer.Serialize<PBInterface.AnimationDef>(dataStream, def);

            dataStream.Position = 0;
            byte[] anim_def = dataStream.ToArray();
            dataStream.Close();
            NativeAnimationFuncs.native_animation_update_anim_def(anim_def, (uint)anim_def.Length);
        }

        private void numFrameDelay_ValueChanged(object sender, EventArgs e)
        {
            if (selectedFrame == null)
                return;
            FrameData data = (FrameData)selectedFrame.Tag;

            data.Delay = Convert.ToSingle(numFrameDelay.Value);
            ProjetGenerator.UpdateFrame(docprojet, data);

            AnimationDef def = ((KeyValuePair<string, AnimationDef>)cmbAnimations.SelectedItem).Value;

            def.Timesteps[data.BasePos] = data.Delay;

            MemoryStream dataStream = new MemoryStream();
            dataStream.Position = 0;

            ProtoBuf.Serializer.Serialize<PBInterface.AnimationDef>(dataStream, def);

            dataStream.Position = 0;
            byte[] anim_def = dataStream.ToArray();
            dataStream.Close();

            NativeAnimationFuncs.native_animation_update_anim_def(anim_def, (uint)anim_def.Length);
        }

        /// <summary>
        /// permet de créer un projet depuis un fichier awf
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void importerDepuisUnFichierDeSortieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult choixfichier = importFichier.ShowDialog();

            if (choixfichier == DialogResult.OK)
            {
                Stream filestream = importFichier.OpenFile();
                AnimationList animationdata = ProtoBuf.Serializer.Deserialize<PBInterface.AnimationList>(filestream);

                FileInfo finfo = new FileInfo(importFichier.FileName);

                try
                {
                    docprojet = ProjetGenerator.GenerateProjectFromAnimationList(importFichier.SafeFileName, finfo.DirectoryName, animationdata);
                    this.OpenAnimationList(animationdata, importFichier.FileName);
                    MessageBox.Show("Import réussi !", "Résultat import", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch(Exception ex)
                {
                    MessageBox.Show("Erreur lors de l'importation !" + ex.ToString(), "Résultat import", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                
                
            }
        }

        private void fermerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Text = title;

            cmbAnimations.Items.Clear();

            loadedanimation = null;
            loadedtexture = null;
            animationFrames.Clear();

            if (animationloaded)
            {
                NativeAnimationFuncs.native_animation_unloadsprite();
                NativeAnimationFuncs.native_animation_clearframe();
            }

            animationloaded = false;
            selectedFrame = null;
            nomprojet = string.Empty;
            docprojet = null;
            panelFrames.Controls.Clear();

        }

        private void ajouterAnimationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(docprojet == null)
            {
                MessageBox.Show("Veuillez d'abord créer ou charger un projet !");
                return;
            }

            NouvelleAnimation animdialogue = new NouvelleAnimation(docprojet,assetsdir);

            if(animdialogue.ShowDialog() == DialogResult.OK)
            {
                Bitmap fulltexture = null;

                AnimationList projetanimation = ProjetGenerator.GetAnimationListFromProject(docprojet, assetsdir, out fulltexture);

                //Stream filestream = ouvrirFichier.OpenFile();
                // AnimationList animationdata = ProtoBuf.Serializer.Deserialize<PBInterface.AnimationList>(filestream);

                if (projetanimation.AnimArrays.Count > 0)
                {
                    this.OpenAnimationList(projetanimation, fulltexture);
                }
            }
        }

        private void editionToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void regénérerFramesDepuisGimpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (cmbAnimations.SelectedItem == null)
            {
                MessageBox.Show("Veuillez sélectionner une animation avant regénération");
                return;
            }

            AnimationDef def = ((KeyValuePair<string, AnimationDef>)cmbAnimations.SelectedItem).Value;

            this.AppendLog("Debut de la regénération des frames depuis GIMP");

            foreach (int frame in def.Frames)
            {
                //call gimp conversion script to regenerate output frames

                string outfile = Path.Combine(assetsdir, "3_frames", string.Format("frame{0}.png", frame));

                string pathpython = string.Format(@"BatchConvertLms.py DoMergeLayer {0} {1}", Path.Combine(assetsdir, "2_gimp", string.Format("frame{0}.xcf", frame)), outfile);

                //double antislashes on windows, otherwise the batch will fail
                pathpython = pathpython.Replace(@"\", @"\\");

                ProcessStartInfo GimpConversionInfo = new ProcessStartInfo("python", pathpython);
                GimpConversionInfo.UseShellExecute = false;
                GimpConversionInfo.RedirectStandardOutput = true;
                GimpConversionInfo.RedirectStandardError = true;
                GimpConversionInfo.WorkingDirectory = Environment.CurrentDirectory;

                Process Gimpproc = Process.Start(GimpConversionInfo);
                Gimpproc.OutputDataReceived += (osender, oe) => this.AppendLog(oe.Data, threaded: true);
                //Gimpproc.ErrorDataReceived += (osender, oe) => this.AppendLog(oe.Data, iserror: true, threaded: true);
                Gimpproc.BeginOutputReadLine();
                //Gimpproc.BeginErrorReadLine();

                while (!Gimpproc.HasExited)
                    Application.DoEvents();

                //resize final frame
                var image = new MagickImage(outfile, MagickFormat.Png);

                var size = new MagickGeometry(def.Width, def.Height);

                size.IgnoreAspectRatio = false;

                image.Resize(size);

                var memStream = new MemoryStream();

                image.Write(memStream, MagickFormat.Png);


                memStream.Position = 0;

                Bitmap newframe = new Bitmap(memStream);

                newframe.Save(outfile, ImageFormat.Png);

                image.Dispose();
                newframe.Dispose();
                memStream.Close();
            }

            //regen animation spritesheet
            if (animationloaded)
            {
                NativeAnimationFuncs.native_animation_unloadsprite();
            }

            Bitmap fulltexture = null;

            AnimationList projetanimation = ProjetGenerator.GetAnimationListFromProject(docprojet, assetsdir, out fulltexture);

            this.OpenAnimationList(projetanimation, fulltexture);

            this.AppendLog("Regénération des frames depuis GIMP terminée !");

        }

        private void redimensionnerEtRegénérerFramesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //redimensionnement de toutes les frames
            ResizeWidget widget = new ResizeWidget();

            if (widget.ShowDialog() == DialogResult.OK)
            {

                int newwidth = Convert.ToInt32(widget.numwidth.Value);
                int newheight = Convert.ToInt32(widget.numheight.Value);
                ProjetGenerator.ResizeFrames(docprojet, newwidth, newheight);

                //call gimp conversion script to regenerate output frames
                foreach(string file in Directory.GetFiles(Path.Combine(assetsdir, "3_frames")))
                {
                    string outfile = file;
                    int basenamelength = "frame".Length;
                    FileInfo inf = new FileInfo(file);
                    int frame = Convert.ToInt32(inf.Name.Substring(basenamelength, inf.Name.LastIndexOf(".png") - basenamelength));

                    string pathpython = string.Format(@"BatchConvertLms.py DoMergeLayer {0} {1}", Path.Combine(assetsdir, "2_gimp", string.Format("frame{0}.xcf", frame)), outfile);

                    //double antislashes on windows, otherwise the batch will fail
                    pathpython = pathpython.Replace(@"\", @"\\");

                    ProcessStartInfo GimpConversionInfo = new ProcessStartInfo("python", pathpython);
                    GimpConversionInfo.UseShellExecute = false;
                    GimpConversionInfo.RedirectStandardOutput = true;
                    GimpConversionInfo.RedirectStandardError = true;
                    GimpConversionInfo.WorkingDirectory = Environment.CurrentDirectory;

                    Process Gimpproc = Process.Start(GimpConversionInfo);
                    Gimpproc.OutputDataReceived += (osender, oe) => this.AppendLog(oe.Data, threaded: true);
                    //Gimpproc.ErrorDataReceived += (osender, oe) => this.AppendLog(oe.Data, iserror: true, threaded: true);
                    Gimpproc.BeginOutputReadLine();
                    //Gimpproc.BeginErrorReadLine();

                    while (!Gimpproc.HasExited)
                        Application.DoEvents();

                    //resize final frame
                    var image = new MagickImage(outfile, MagickFormat.Png);

                    var size = new MagickGeometry(newwidth, newheight);

                    size.IgnoreAspectRatio = false;

                    image.Resize(size);

                    var memStream = new MemoryStream();

                    image.Write(memStream, MagickFormat.Png);


                    memStream.Position = 0;

                    Bitmap newframe = new Bitmap(memStream);

                    newframe.Save(outfile, ImageFormat.Png);

                    image.Dispose();
                    newframe.Dispose();
                    memStream.Close();
                }


                //regen animation spritesheet
                if (animationloaded)
                {
                    NativeAnimationFuncs.native_animation_unloadsprite();
                }

                Bitmap fulltexture = null;

                AnimationList projetanimation = ProjetGenerator.GetAnimationListFromProject(docprojet, assetsdir, out fulltexture);

                this.OpenAnimationList(projetanimation, fulltexture);

                this.AppendLog("Redimensionnement et Regénération des frames depuis GIMP terminée !");
            }

        }

        private void AppendLog(string data, bool iserror = false, bool threaded = false)
        {
            if (data == null)
            {
                return;
            }

            Color textcolor = Color.Gray;

            if (iserror)
            {
                textcolor = Color.Red;
            }

           /* if (threaded)
            {
                this.BeginInvoke(new MethodInvoker(() =>
                {
                    rtbmainlog.ForeColor = textcolor;
                    rtbmainlog.AppendText(data);

                    rtbmainlog.AppendText(Environment.NewLine);
                }));
            }
            else
            {
                rtbmainlog.ForeColor = textcolor;
                rtbmainlog.AppendText(data);

                rtbmainlog.AppendText(Environment.NewLine);
            }*/
        }

        private void exporterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(loadedanimation == null || loadedtexture == null)
            {
                MessageBox.Show("Veuillez ouvrir un projet avec au moins une animation avant export", "Animation manquante", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if(exportProjet.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    string fileexport = exportProjet.FileName;

                    FileStream exportstream = new FileStream(fileexport, FileMode.Create);

                    ProtoBuf.Serializer.Serialize<PBInterface.AnimationList>(exportstream, loadedanimation);

                    exportstream.Close();

                    string textureexport = fileexport.Substring(0, fileexport.LastIndexOf('.'));

                    FileStream pngexportstream = new FileStream(textureexport + ".png", FileMode.Create);

                    loadedtexture.Save(pngexportstream, ImageFormat.Png);

                    pngexportstream.Close();

                    MessageBox.Show("Export effectué !");
                }
                catch(Exception ex)
                {
                    MessageBox.Show("Erreur lors de l'export ! " + ex.ToString());
                }
                
            }
        }

        private void chkonion_CheckedChanged(object sender, EventArgs e)
        {
            AnimationPanel.ShowOnion = chkonion.Checked;
        }


        private void btnplussprite_Click(object sender, EventArgs e)
        {
            AnimationDef def = ((KeyValuePair<string, AnimationDef>)cmbAnimations.SelectedItem).Value;


            AddSprite dialogadd = new AddSprite(def, loadedanimation);

            if(dialogadd.ShowDialog() == DialogResult.OK)
            {
                AnchorData data = new AnchorData(dialogadd.txtid.Text,dialogadd.cmbSrcAnchor.SelectedItem.ToString(),dialogadd.cmbAnchor.SelectedItem.ToString(), dialogadd.Srcvector, dialogadd.cmbAnimations.SelectedItem.ToString());

                ProjetGenerator.AddAnchoredAnimationToProject(docprojet, data);

                AnimationPanel.Anchorsmap.Add(dialogadd.txtid.Text, data);
                MessageBox.Show("Ajout d'un nouveau sprite effectué !");
            }
        }

        private void btnEditSprite_Click(object sender, EventArgs e)
        {
            AnimationDef def = ((KeyValuePair<string, AnimationDef>)cmbAnimations.SelectedItem).Value;

            string key = cmbattachedsprites.SelectedItem.ToString();

            AnchorData data = AnimationPanel.Anchorsmap[key];

            AddSprite dialogadd = new AddSprite(def, loadedanimation, data);

            if (dialogadd.ShowDialog() == DialogResult.OK)
            {
                ProjetGenerator.EditAnchoredAnimation(docprojet, data);
                MessageBox.Show("Modification d'un sprite effectué !");
            }
        }

        private void btnminussprite_Click(object sender, EventArgs e)
        {
            string key = cmbattachedsprites.SelectedItem.ToString();

            if (MessageBox.Show("Supprimer les sprite "+key+" ?","suppression sprite attaché",MessageBoxButtons.YesNo,MessageBoxIcon.Question) == DialogResult.Yes)
            {
                AnimationPanel.Anchorsmap.Remove(key);

                NativeAnimationFuncs.native_animation_removesprite(key);

                ProjetGenerator.RemoveAnchoredAnimation(docprojet, key);

                MessageBox.Show("Suppression du sprite "+ key + "effectué !");
            }
        }

        private void chkshowhidemultisprite_CheckedChanged(object sender, EventArgs e)
        {
            AnimationPanel.ShowMultiSprite = chkshowhidemultisprite.Checked;

        }

        private void btnaddframe_Click(object sender, EventArgs e)
        {

        }

        private void btnremoveframe_Click(object sender, EventArgs e)
        {
            PictureBox todelete = selectedFrame;

            if(todelete == null)
            {
                MessageBox.Show("Veuillez sélectionner une frame avant suppression !","Frame non sélectionnée",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
                return;
            }

            FrameData odatadelete = (FrameData)todelete.Tag;
            
            int delframepos = odatadelete.FramePos;

            ProjetGenerator.RemoveFrame(docprojet, odatadelete.FramePos);

            animationFrames.Remove(odatadelete);
            panelFrames.Controls.Remove(todelete);

            //reload animation

            NativeAnimationFuncs.native_animation_unloadsprite();

            Bitmap fulltexture = null;

            AnimationList projetanimation = ProjetGenerator.GetAnimationListFromProject(docprojet, assetsdir, out fulltexture);

            int index = cmbAnimations.SelectedIndex;

            if (projetanimation.AnimArrays.Count > 0)
            {
                this.OpenAnimationList(projetanimation, fulltexture);
            }

            Dictionary<string, AnchorData> tmp = ProjetGenerator.GetAnchoredAnimationFromProject(docprojet, projetanimation);

            AnimationPanel.SetAnchorMap(tmp);

            cmbAnimations.SelectedIndex = index;
        }

        private void cmbattachedsprites_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cmbattachedsprites.SelectedItem != null)
            {
                btnEditSprite.Enabled = true;
                btnminussprite.Enabled = true;
            }
                
        }

        #region Ancres

        private void btnaddanchor_Click(object sender, EventArgs e)
        {
            AnimationDef def = ((KeyValuePair<string, AnimationDef>)cmbAnimations.SelectedItem).Value;

            List<string> availablesanchors = ProjetGenerator.GetAllAnchorsFromProject(docprojet);

            int aid = 0;

            if (def.AttachpointValues.Count > 0)
            {
                foreach (Avector point in def.AttachpointValues[0].Attachpointpos)
                {
                    if (!point.Isnull)
                    {
                        availablesanchors.Remove(def.AttachpointIds[aid]);
                    }

                    aid++;
                }
            }

            AnchorAdd dialoganchor = new AnchorAdd(availablesanchors);

            if(dialoganchor.ShowDialog() == DialogResult.OK)
            {
                string anchor = string.Empty;

                if (dialoganchor.cmbAnchors.SelectedItem != null)
                {
                    anchor = dialoganchor.cmbAnchors.SelectedItem.ToString();
                }
                else
                {
                    anchor = dialoganchor.cmbAnchors.Text;
                }

                bool newanchor = false;
                int posinanchors = -1;

                if (def.AttachpointIds.Count == 0)
                {
                    def.AttachpointIds.AddRange(availablesanchors);

                    for (int aframe = 0; aframe < def.AttachpointValues.Count; aframe++)
                    {
                        foreach(string ap in def.AttachpointIds)
                        {
                            Avector vec = new Avector();
                            vec.Isnull = true;
                            def.AttachpointValues[aframe].Attachpointpos.Add(vec);
                        }
                        
                    }
                }

                if (availablesanchors.Contains(anchor))
                {
                    if (def.AttachpointValues.Count > 0)
                    {
                        for (int aframe = 0;aframe < def.AttachpointValues.Count;aframe++)
                        {
                            aid = 0;

                            foreach (Avector point in def.AttachpointValues[aframe].Attachpointpos)
                            {
                                if (def.AttachpointIds[aid] == anchor)
                                {
                                    if(posinanchors == -1)
                                    {
                                        posinanchors = aid;
                                    }

                                    def.AttachpointValues[aframe].Attachpointpos[aid].Isnull = false;
                                    def.AttachpointValues[aframe].Attachpointpos[aid].X = 10;
                                    def.AttachpointValues[aframe].Attachpointpos[aid].Y = 10;
                                }
                                aid++;
                            }
                            
                        }
                    }
                }
                else
                {
                    newanchor = true;

                    def.AttachpointIds.Add(anchor);

                    for (int aframe = 0; aframe < def.AttachpointValues.Count; aframe++)
                    {
                        Avector vec = new Avector();
                        vec.Isnull = false;
                        vec.X = 10;
                        vec.Y = 10;

                        if (posinanchors == -1)
                        {
                            posinanchors = def.AttachpointValues[aframe].Attachpointpos.Count;
                        }

                        def.AttachpointValues[aframe].Attachpointpos.Add(vec);
                    }

                }

                cmbContactPoint.Items.Add(anchor);

                MemoryStream dataStream = new MemoryStream();
                dataStream.Position = 0;

                ProtoBuf.Serializer.Serialize<PBInterface.AnimationDef>(dataStream, def);

                dataStream.Position = 0;
                byte[] anim_def = dataStream.ToArray();
                dataStream.Close();
                NativeAnimationFuncs.native_animation_update_anim_def(anim_def, (uint)anim_def.Length);

                ProjetGenerator.AddAnchorToAnimation(docprojet, def.Name, posinanchors, anchor, newanchor, 10, 10);
            }
        }

        private void cmbContactPoint_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cmbContactPoint.SelectedIndex == -1)
            {
                btneditanchor.Enabled = copyonallframes.Enabled = false;
                return;
            }

            uint framecounter = NativeAnimationFuncs.native_animation_getframe();
            AnimationDef def = ((KeyValuePair<string, AnimationDef>)cmbAnimations.SelectedItem).Value;

            for (int contactid = 0; contactid < def.AttachpointIds.Count; contactid++)
            {
                if (def.AttachpointIds[contactid] == cmbContactPoint.SelectedItem.ToString())
                {
                    NativeAnimationFuncs.native_animation_selectpin((uint)contactid);
                    btneditanchor.Enabled = copyonallframes.Enabled = true;
                }
            }


        }

        private void btneditanchor_Click(object sender, EventArgs e)
        {

        }

        private void copyonallframes_Click(object sender, EventArgs e)
        {
            AnimationDef def = ((KeyValuePair<string, AnimationDef>)cmbAnimations.SelectedItem).Value;

            //copie de la position de l'ancre actuelle sur toute les frames de l'animation courante

            string selectedcontactpoint = cmbContactPoint.SelectedItem.ToString();

            if(!string.IsNullOrEmpty(selectedcontactpoint))
            {
                PictureBox currentframe = selectedFrame;

                FrameData odataframe = (FrameData)currentframe.Tag;

                Avector basepos = null;
                int ccontact = -1;

                for (int contactid = 0; contactid < def.AttachpointIds.Count; contactid++)
                {
                    if (def.AttachpointIds[contactid] == cmbContactPoint.SelectedItem.ToString())
                    {
                        for(int iframe = 0; iframe < def.AttachpointValues.Count;iframe++)
                        {
                            if(iframe == odataframe.BasePos)
                            {
                                basepos = def.AttachpointValues[iframe].Attachpointpos[contactid];
                                ccontact = contactid;
                                break;
                            }
                        }

                        break;
                    }
                }

                if (basepos != null && ccontact != -1)
                {
                    for (int iframe = 0; iframe < def.AttachpointValues.Count; iframe++)
                    {
                        if (iframe != odataframe.BasePos)
                        {
                            def.AttachpointValues[iframe].Attachpointpos[ccontact].X = basepos.X;
                            def.AttachpointValues[iframe].Attachpointpos[ccontact].Y = basepos.Y;
                        }
                    }

                    MemoryStream dataStream = new MemoryStream();
                    dataStream.Position = 0;

                    ProtoBuf.Serializer.Serialize<PBInterface.AnimationDef>(dataStream, def);

                    dataStream.Position = 0;
                    byte[] anim_def = dataStream.ToArray();
                    dataStream.Close();

                    NativeAnimationFuncs.native_animation_update_anim_def(anim_def, (uint)anim_def.Length);
                }


            }

        }

        private void btnsuppranchor_Click(object sender, EventArgs e)
        {
            AnimationDef def = ((KeyValuePair<string, AnimationDef>)cmbAnimations.SelectedItem).Value;

            //suppression de l'ancre sélectionnée

            string selectedcontactpoint = cmbContactPoint.SelectedItem.ToString();

            if (string.IsNullOrEmpty(selectedcontactpoint))
            {
                MessageBox.Show("Veuillez sélectionner une ancre avant suppression!", "Pas de sélection", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if(MessageBox.Show("Supprimer l'ancre "+selectedcontactpoint+" ?","Confirmation suppression",MessageBoxButtons.YesNo,MessageBoxIcon.Question) == DialogResult.Yes)
            {
                int ccontact = -1;

                for (int contactid = 0; contactid < def.AttachpointIds.Count; contactid++)
                {
                    if (def.AttachpointIds[contactid] == selectedcontactpoint)
                    {

                        ccontact = contactid;
                        break;
                    }
                }

                if(ccontact != -1)
                {
                    def.AttachpointIds.RemoveAt(ccontact);

                    for (int iframe = 0; iframe < def.AttachpointValues.Count; iframe++)
                    {
                        def.AttachpointValues[iframe].Attachpointpos.RemoveAt(ccontact);
                    }

                    //suppression sur le projet
                    List<string> multispritetorm = ProjetGenerator.RemoveAnchor(docprojet, selectedcontactpoint);

                    MemoryStream dataStream = new MemoryStream();
                    dataStream.Position = 0;

                    ProtoBuf.Serializer.Serialize<PBInterface.AnimationDef>(dataStream, def);

                    dataStream.Position = 0;
                    byte[] anim_def = dataStream.ToArray();
                    dataStream.Close();

                    NativeAnimationFuncs.native_animation_update_anim_def(anim_def, (uint)anim_def.Length);

                    foreach(string sprite in multispritetorm)
                    {
                        AnimationPanel.Anchorsmap.Remove(sprite);

                        NativeAnimationFuncs.native_animation_removesprite(sprite);

                        ProjetGenerator.RemoveAnchoredAnimation(docprojet, sprite);
                    }
                    
                    MessageBox.Show("Suppression de l'ancre " + selectedcontactpoint + " effectuée !");
                }
            }
        }




        #endregion

        private void toolStripButtonZoomplus_Click(object sender, EventArgs e)
        {
            //valeur de zoom + importante
            int newselect = cmbZoom.SelectedIndex + 1;

            if (newselect >= cmbZoom.Items.Count)
                newselect = cmbZoom.Items.Count - 1;

            cmbZoom.SelectedIndex = newselect;
        }

        private void toolStripButtonZoomminus_Click(object sender, EventArgs e)
        {
            //valeur de zoom - importante
            int newselect = cmbZoom.SelectedIndex - 1;

            if (newselect < 0)
                newselect = 0;

            cmbZoom.SelectedIndex = newselect;
        }

        private void toolStripButtonZoomreset_Click(object sender, EventArgs e)
        {
            //valeur de zoom 100%
            cmbZoom.SelectedIndex = 0;
        }

        private void cmbZoom_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!AnimationPanel.IsInit)
                return;

            this.zoomEvent();
        }

        private void zoomEvent()
        {
            AnimationPanel.ZoomFactor = ((KeyValuePair<string, float>)cmbZoom.SelectedItem).Value;

            NativeAnimationFuncs.native_animation_setzoom(AnimationPanel.Width, AnimationPanel.Height, AnimationPanel.ZoomFactor);
            AnimationPanel.Invalidate();

            toolStripButtonZoomplus.Enabled = !(cmbZoom.SelectedIndex == cmbZoom.Items.Count - 1);
            toolStripButtonZoomminus.Enabled = !(cmbZoom.SelectedIndex == 0);
            toolStripButtonZoomreset.Enabled = !(AnimationPanel.ZoomFactor == 1.0f);
        }

        private void préférencesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Preferences prefdiag = new Preferences();

            if(prefdiag.ShowDialog() == DialogResult.OK)
            {
                NativeAnimationFuncs.native_animation_setbackgroundcolor((Settings.Default.backgroundcolor.R != 0) ? Settings.Default.backgroundcolor.R / 255.0f : 0.0f, (Settings.Default.backgroundcolor.G != 0) ? Settings.Default.backgroundcolor.G / 255.0f : 0.0f, (Settings.Default.backgroundcolor.B != 0) ? Settings.Default.backgroundcolor.B / 255.0f : 0.0f, 1.0f);
            }
        }


    }
}
