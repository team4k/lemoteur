#!/usr/bin/env python
# coding: iso-8859-1

#run with
#don't forget to escape antislashes on windows (\\ rather than \)
#make sure that gimp is in your path
#gimp-2.8 -idf --batch-interpreter python-fu-eval -b "import sys;sys.path=['.']+sys.path;import BatchConvertLms;BatchConvertLms.DoConvert('*.png',".")" -b "pdb.gimp_quit(1)"
#or
#python BatchConvertLms.py DoConvert *.png .

import os, sys, subprocess
import glob

sys.stderr = open( 'gimpstderr.txt', 'w')
sys.stdout = open( 'gimpstdout.txt', 'w')

def DoConvert(pattern,outdir):
	
	for file in glob.glob(pattern):
		 image = pdb.gimp_file_load(file, file, run_mode=RUN_NONINTERACTIVE)
		 drawable = image.active_layer
		 outfile = os.path.splitext(os.path.basename(file))[0]
		 outfile = outdir+outfile+".xcf"
		 
		 if not os.path.isfile(outfile):
			 pdb.gimp_file_save(image,drawable,outfile,outfile,run_mode=RUN_NONINTERACTIVE)
			 pdb.gimp_image_delete(image)

def DoMergeLayer(file,outfile):
	image = pdb.gimp_file_load(file, file, run_mode=RUN_NONINTERACTIVE)
	drawable = pdb.gimp_image_merge_visible_layers(image,CLIP_TO_IMAGE)
	pdb.gimp_file_save(image,drawable,outfile,outfile,run_mode=RUN_NONINTERACTIVE)
	pdb.gimp_image_delete(image)

def DoConvertSvg(file,outfile):
	image = pdb.gimp_file_load(file, file, run_mode=RUN_NONINTERACTIVE)
	drawable = image.active_layer
	pdb.gimp_file_save(image,drawable,outfile,outfile,run_mode=RUN_NONINTERACTIVE)
	pdb.gimp_image_delete(image)

def DoSingleConvert(file,outfile):
	image = pdb.gimp_file_load(file, file, run_mode=RUN_NONINTERACTIVE)
	drawable = image.active_layer
	
	if not os.path.isfile(outfile):
		pdb.gimp_file_save(image,drawable,outfile,outfile,run_mode=RUN_NONINTERACTIVE)
		pdb.gimp_image_delete(image)

if __name__ == "__main__":
	
	if len(sys.argv) < 2:
		print("you must specify a function to execute!")
		sys.exit(-1)

	scrdir = os.path.dirname(os.path.realpath(__file__))
	#escape antislashes on windows otherwise the batch will fail
	scrdir = scrdir.replace("\\","\\\\");
	scrname = os.path.splitext(os.path.basename(__file__))[0]
	shcode = "import sys;sys.path.insert(0, '" + scrdir + "');import " + scrname + ";" + scrname + "." + sys.argv[1] + str(tuple(sys.argv[2:]))
	shcode = "gimp-2.8 -idf --batch-interpreter python-fu-eval -b \"" + shcode + "\" -b \"pdb.gimp_quit(1)\""
	sys.exit(subprocess.call(shcode, shell=True))
else:
	from gimpfu import *