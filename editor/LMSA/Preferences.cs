﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LMS.Properties;

namespace LMS
{
    public partial class Preferences : Form
    {
        public Preferences()
        {
            InitializeComponent();
            btnbackgroundcolor.BackColor = Settings.Default.backgroundcolor;
        }

        private void btnbackgroundcolor_Click(object sender, EventArgs e)
        {
            if(backgroundcolorDiag.ShowDialog() == DialogResult.OK)
            {
                btnbackgroundcolor.BackColor = backgroundcolorDiag.Color;
                Settings.Default.backgroundcolor = backgroundcolorDiag.Color;
                Settings.Default.Save();
            }
        }

        private void btnokpref_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
