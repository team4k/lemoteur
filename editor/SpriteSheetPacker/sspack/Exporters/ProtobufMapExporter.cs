﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProtoBuf;
using PBInterface;
using System.IO;
using System.Drawing;

namespace sspack
{
    public class ProtobufMapExporter : IMapExporter
    {
        public string  MapExtension
        {
            get { return "pack"; }
        }

        public void  Save(string filename, Dictionary<string,System.Drawing.Rectangle> map)
        {
            pack_info info = new pack_info();

            //set the name of the packed texture file
            info.texture_pack_id = Path.GetFileNameWithoutExtension(filename) + ".png";

            //add texture_pack_info to our object
            foreach (KeyValuePair<string, Rectangle> entry in map)
            {
                pack_texture_info text_info = new pack_texture_info();
                text_info.texture_id = Path.GetFileName(entry.Key);
                text_info.texture_rectangle = new Wrectangle();
                text_info.texture_rectangle.width = entry.Value.Width;
                text_info.texture_rectangle.height = entry.Value.Height;
                text_info.texture_rectangle.position = new Wvector();
                text_info.texture_rectangle.position.X = entry.Value.X;
                text_info.texture_rectangle.position.Y = entry.Value.Y;

                info.texture_pack_list.Add(text_info);
            }

            //save protobuf file
            using (var file = File.Create(filename))
            {
                ProtoBuf.Serializer.Serialize<pack_info>(file, info);
            }


        }
    }
}
