﻿namespace WafaEditor
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tabMain = new System.Windows.Forms.TabControl();
            this.tabLevelEditor = new System.Windows.Forms.TabPage();
            this.btn_collide = new System.Windows.Forms.Button();
            this.lbl_colval = new System.Windows.Forms.Label();
            this.btn_nocol = new System.Windows.Forms.Button();
            this.btnEditCollisions = new System.Windows.Forms.Button();
            this.btnEditBackground = new System.Windows.Forms.Button();
            this.lblCollisionAlpha = new System.Windows.Forms.Label();
            this.numCollisionsAlpha = new System.Windows.Forms.NumericUpDown();
            this.chkCollisions = new System.Windows.Forms.CheckBox();
            this.chkBackground = new System.Windows.Forms.CheckBox();
            this.tabTileEditor = new System.Windows.Forms.TabPage();
            this.groupTools = new System.Windows.Forms.GroupBox();
            this.btnSelect = new System.Windows.Forms.Button();
            this.btnRedo = new System.Windows.Forms.Button();
            this.btnUndo = new System.Windows.Forms.Button();
            this.btnRectangle = new System.Windows.Forms.Button();
            this.btnLine = new System.Windows.Forms.Button();
            this.btnBucketTool = new System.Windows.Forms.Button();
            this.btnPointTool = new System.Windows.Forms.Button();
            this.groupPreviewMap = new System.Windows.Forms.GroupBox();
            this.pictureGridPreview = new System.Windows.Forms.PictureBox();
            this.groupPalette = new System.Windows.Forms.GroupBox();
            this.btnBlack = new System.Windows.Forms.Button();
            this.btnGreyDark = new System.Windows.Forms.Button();
            this.btnGreyLight = new System.Windows.Forms.Button();
            this.btnWhite = new System.Windows.Forms.Button();
            this.tileBox = new System.Windows.Forms.PictureBox();
            this.groupTileSet = new System.Windows.Forms.GroupBox();
            this.panelTileSet = new System.Windows.Forms.Panel();
            this.menuLevel = new System.Windows.Forms.MenuStrip();
            this.menuFileLevel = new System.Windows.Forms.ToolStripMenuItem();
            this.menuNewLevel = new System.Windows.Forms.ToolStripMenuItem();
            this.menuOpenLevel = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSaveLevel = new System.Windows.Forms.ToolStripMenuItem();
            this.menuTile = new System.Windows.Forms.MenuStrip();
            this.menuFileTile = new System.Windows.Forms.ToolStripMenuItem();
            this.menuNewTileSet = new System.Windows.Forms.ToolStripMenuItem();
            this.menuOpenTile = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSaveTileset = new System.Windows.Forms.ToolStripMenuItem();
            this.menuNewTile = new System.Windows.Forms.ToolStripMenuItem();
            this.openTileset = new System.Windows.Forms.OpenFileDialog();
            this.saveTileset = new System.Windows.Forms.SaveFileDialog();
            this.openLevel = new System.Windows.Forms.OpenFileDialog();
            this.saveLevel = new System.Windows.Forms.SaveFileDialog();
            this.panelEngine = new WafaEditor.OglPanel();
            this.tabMain.SuspendLayout();
            this.tabLevelEditor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numCollisionsAlpha)).BeginInit();
            this.tabTileEditor.SuspendLayout();
            this.groupTools.SuspendLayout();
            this.groupPreviewMap.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureGridPreview)).BeginInit();
            this.groupPalette.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tileBox)).BeginInit();
            this.groupTileSet.SuspendLayout();
            this.menuLevel.SuspendLayout();
            this.menuTile.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabMain
            // 
            this.tabMain.Controls.Add(this.tabLevelEditor);
            this.tabMain.Controls.Add(this.tabTileEditor);
            this.tabMain.Location = new System.Drawing.Point(228, 27);
            this.tabMain.Name = "tabMain";
            this.tabMain.SelectedIndex = 0;
            this.tabMain.Size = new System.Drawing.Size(1356, 624);
            this.tabMain.TabIndex = 6;
            this.tabMain.SelectedIndexChanged += new System.EventHandler(this.tabMain_SelectedIndexChanged);
            // 
            // tabLevelEditor
            // 
            this.tabLevelEditor.Controls.Add(this.btn_collide);
            this.tabLevelEditor.Controls.Add(this.lbl_colval);
            this.tabLevelEditor.Controls.Add(this.btn_nocol);
            this.tabLevelEditor.Controls.Add(this.btnEditCollisions);
            this.tabLevelEditor.Controls.Add(this.btnEditBackground);
            this.tabLevelEditor.Controls.Add(this.lblCollisionAlpha);
            this.tabLevelEditor.Controls.Add(this.numCollisionsAlpha);
            this.tabLevelEditor.Controls.Add(this.chkCollisions);
            this.tabLevelEditor.Controls.Add(this.chkBackground);
            this.tabLevelEditor.Controls.Add(this.panelEngine);
            this.tabLevelEditor.Location = new System.Drawing.Point(4, 22);
            this.tabLevelEditor.Name = "tabLevelEditor";
            this.tabLevelEditor.Padding = new System.Windows.Forms.Padding(3);
            this.tabLevelEditor.Size = new System.Drawing.Size(1348, 598);
            this.tabLevelEditor.TabIndex = 0;
            this.tabLevelEditor.Text = "Level";
            this.tabLevelEditor.UseVisualStyleBackColor = true;
            // 
            // btn_collide
            // 
            this.btn_collide.BackColor = System.Drawing.Color.Black;
            this.btn_collide.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_collide.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btn_collide.Location = new System.Drawing.Point(1269, 145);
            this.btn_collide.Name = "btn_collide";
            this.btn_collide.Size = new System.Drawing.Size(27, 25);
            this.btn_collide.TabIndex = 11;
            this.btn_collide.Tag = "1";
            this.btn_collide.Text = "1";
            this.btn_collide.UseVisualStyleBackColor = false;
            this.btn_collide.Click += new System.EventHandler(this.btn_change_collisionvalue_Click);
            // 
            // lbl_colval
            // 
            this.lbl_colval.AutoSize = true;
            this.lbl_colval.Location = new System.Drawing.Point(1117, 151);
            this.lbl_colval.Name = "lbl_colval";
            this.lbl_colval.Size = new System.Drawing.Size(113, 13);
            this.lbl_colval.TabIndex = 10;
            this.lbl_colval.Text = "Select Collision value :";
            // 
            // btn_nocol
            // 
            this.btn_nocol.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_nocol.Location = new System.Drawing.Point(1236, 145);
            this.btn_nocol.Name = "btn_nocol";
            this.btn_nocol.Size = new System.Drawing.Size(27, 25);
            this.btn_nocol.TabIndex = 9;
            this.btn_nocol.Tag = "0";
            this.btn_nocol.Text = "0";
            this.btn_nocol.UseVisualStyleBackColor = true;
            this.btn_nocol.Click += new System.EventHandler(this.btn_change_collisionvalue_Click);
            // 
            // btnEditCollisions
            // 
            this.btnEditCollisions.Location = new System.Drawing.Point(1001, 108);
            this.btnEditCollisions.Name = "btnEditCollisions";
            this.btnEditCollisions.Size = new System.Drawing.Size(75, 23);
            this.btnEditCollisions.TabIndex = 8;
            this.btnEditCollisions.Tag = "LAYER_COLLISIONS";
            this.btnEditCollisions.Text = "Edit";
            this.btnEditCollisions.UseVisualStyleBackColor = true;
            this.btnEditCollisions.Click += new System.EventHandler(this.btnEditLayer_Click);
            // 
            // btnEditBackground
            // 
            this.btnEditBackground.BackColor = System.Drawing.Color.GreenYellow;
            this.btnEditBackground.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditBackground.Location = new System.Drawing.Point(1001, 75);
            this.btnEditBackground.Name = "btnEditBackground";
            this.btnEditBackground.Size = new System.Drawing.Size(75, 23);
            this.btnEditBackground.TabIndex = 7;
            this.btnEditBackground.Tag = "LAYER_BACKGROUND";
            this.btnEditBackground.Text = "Edit";
            this.btnEditBackground.UseVisualStyleBackColor = false;
            this.btnEditBackground.Click += new System.EventHandler(this.btnEditLayer_Click);
            // 
            // lblCollisionAlpha
            // 
            this.lblCollisionAlpha.AutoSize = true;
            this.lblCollisionAlpha.Location = new System.Drawing.Point(1233, 113);
            this.lblCollisionAlpha.Name = "lblCollisionAlpha";
            this.lblCollisionAlpha.Size = new System.Drawing.Size(34, 13);
            this.lblCollisionAlpha.TabIndex = 6;
            this.lblCollisionAlpha.Text = "Alpha";
            // 
            // numCollisionsAlpha
            // 
            this.numCollisionsAlpha.Location = new System.Drawing.Point(1273, 111);
            this.numCollisionsAlpha.Name = "numCollisionsAlpha";
            this.numCollisionsAlpha.Size = new System.Drawing.Size(57, 20);
            this.numCollisionsAlpha.TabIndex = 5;
            this.numCollisionsAlpha.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.numCollisionsAlpha.ValueChanged += new System.EventHandler(this.numCollisionsAlpha_ValueChanged);
            // 
            // chkCollisions
            // 
            this.chkCollisions.AutoSize = true;
            this.chkCollisions.Checked = true;
            this.chkCollisions.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkCollisions.Location = new System.Drawing.Point(1105, 111);
            this.chkCollisions.Name = "chkCollisions";
            this.chkCollisions.Size = new System.Drawing.Size(98, 17);
            this.chkCollisions.TabIndex = 4;
            this.chkCollisions.Tag = "LAYER_COLLISIONS";
            this.chkCollisions.Text = "Collisions Layer";
            this.chkCollisions.UseVisualStyleBackColor = true;
            this.chkCollisions.CheckedChanged += new System.EventHandler(this.chkLayer_CheckedChanged);
            // 
            // chkBackground
            // 
            this.chkBackground.AutoSize = true;
            this.chkBackground.Checked = true;
            this.chkBackground.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBackground.Location = new System.Drawing.Point(1105, 78);
            this.chkBackground.Name = "chkBackground";
            this.chkBackground.Size = new System.Drawing.Size(72, 17);
            this.chkBackground.TabIndex = 2;
            this.chkBackground.Tag = "LAYER_BACKGROUND";
            this.chkBackground.Text = "Tile Layer";
            this.chkBackground.UseVisualStyleBackColor = true;
            this.chkBackground.CheckedChanged += new System.EventHandler(this.chkLayer_CheckedChanged);
            // 
            // tabTileEditor
            // 
            this.tabTileEditor.Controls.Add(this.groupTools);
            this.tabTileEditor.Controls.Add(this.groupPreviewMap);
            this.tabTileEditor.Controls.Add(this.groupPalette);
            this.tabTileEditor.Controls.Add(this.tileBox);
            this.tabTileEditor.Location = new System.Drawing.Point(4, 22);
            this.tabTileEditor.Name = "tabTileEditor";
            this.tabTileEditor.Padding = new System.Windows.Forms.Padding(3);
            this.tabTileEditor.Size = new System.Drawing.Size(1348, 598);
            this.tabTileEditor.TabIndex = 1;
            this.tabTileEditor.Text = "TileSet";
            this.tabTileEditor.UseVisualStyleBackColor = true;
            // 
            // groupTools
            // 
            this.groupTools.Controls.Add(this.btnSelect);
            this.groupTools.Controls.Add(this.btnRedo);
            this.groupTools.Controls.Add(this.btnUndo);
            this.groupTools.Controls.Add(this.btnRectangle);
            this.groupTools.Controls.Add(this.btnLine);
            this.groupTools.Controls.Add(this.btnBucketTool);
            this.groupTools.Controls.Add(this.btnPointTool);
            this.groupTools.Location = new System.Drawing.Point(561, 6);
            this.groupTools.Name = "groupTools";
            this.groupTools.Size = new System.Drawing.Size(271, 63);
            this.groupTools.TabIndex = 16;
            this.groupTools.TabStop = false;
            this.groupTools.Text = "Tools";
            // 
            // btnSelect
            // 
            this.btnSelect.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSelect.BackgroundImage")));
            this.btnSelect.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnSelect.Location = new System.Drawing.Point(6, 19);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(32, 32);
            this.btnSelect.TabIndex = 6;
            this.btnSelect.UseVisualStyleBackColor = true;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // btnRedo
            // 
            this.btnRedo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRedo.BackgroundImage")));
            this.btnRedo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnRedo.Enabled = false;
            this.btnRedo.Location = new System.Drawing.Point(233, 19);
            this.btnRedo.Name = "btnRedo";
            this.btnRedo.Size = new System.Drawing.Size(32, 32);
            this.btnRedo.TabIndex = 5;
            this.btnRedo.UseVisualStyleBackColor = true;
            this.btnRedo.Click += new System.EventHandler(this.btnRedo_Click);
            // 
            // btnUndo
            // 
            this.btnUndo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnUndo.BackgroundImage")));
            this.btnUndo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnUndo.Enabled = false;
            this.btnUndo.Location = new System.Drawing.Point(195, 19);
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(32, 32);
            this.btnUndo.TabIndex = 4;
            this.btnUndo.UseVisualStyleBackColor = true;
            this.btnUndo.Click += new System.EventHandler(this.btnUndo_Click);
            // 
            // btnRectangle
            // 
            this.btnRectangle.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRectangle.BackgroundImage")));
            this.btnRectangle.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnRectangle.Location = new System.Drawing.Point(157, 19);
            this.btnRectangle.Name = "btnRectangle";
            this.btnRectangle.Size = new System.Drawing.Size(32, 32);
            this.btnRectangle.TabIndex = 3;
            this.btnRectangle.UseVisualStyleBackColor = true;
            this.btnRectangle.Click += new System.EventHandler(this.btnRectangle_Click);
            // 
            // btnLine
            // 
            this.btnLine.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLine.BackgroundImage")));
            this.btnLine.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnLine.Location = new System.Drawing.Point(119, 19);
            this.btnLine.Name = "btnLine";
            this.btnLine.Size = new System.Drawing.Size(32, 32);
            this.btnLine.TabIndex = 2;
            this.btnLine.UseVisualStyleBackColor = true;
            this.btnLine.Click += new System.EventHandler(this.btnLine_Click);
            // 
            // btnBucketTool
            // 
            this.btnBucketTool.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnBucketTool.BackgroundImage")));
            this.btnBucketTool.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnBucketTool.Location = new System.Drawing.Point(81, 19);
            this.btnBucketTool.Name = "btnBucketTool";
            this.btnBucketTool.Size = new System.Drawing.Size(32, 32);
            this.btnBucketTool.TabIndex = 1;
            this.btnBucketTool.UseVisualStyleBackColor = true;
            this.btnBucketTool.Click += new System.EventHandler(this.btnBucketTool_Click);
            // 
            // btnPointTool
            // 
            this.btnPointTool.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPointTool.BackgroundImage")));
            this.btnPointTool.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnPointTool.Location = new System.Drawing.Point(43, 19);
            this.btnPointTool.Name = "btnPointTool";
            this.btnPointTool.Size = new System.Drawing.Size(32, 32);
            this.btnPointTool.TabIndex = 0;
            this.btnPointTool.UseVisualStyleBackColor = true;
            this.btnPointTool.Click += new System.EventHandler(this.btnPointTool_Click);
            // 
            // groupPreviewMap
            // 
            this.groupPreviewMap.Controls.Add(this.pictureGridPreview);
            this.groupPreviewMap.Location = new System.Drawing.Point(972, 87);
            this.groupPreviewMap.Name = "groupPreviewMap";
            this.groupPreviewMap.Size = new System.Drawing.Size(269, 282);
            this.groupPreviewMap.TabIndex = 15;
            this.groupPreviewMap.TabStop = false;
            this.groupPreviewMap.Text = "Grid preview";
            // 
            // pictureGridPreview
            // 
            this.pictureGridPreview.Location = new System.Drawing.Point(7, 20);
            this.pictureGridPreview.Name = "pictureGridPreview";
            this.pictureGridPreview.Size = new System.Drawing.Size(256, 256);
            this.pictureGridPreview.TabIndex = 0;
            this.pictureGridPreview.TabStop = false;
            this.pictureGridPreview.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureGridPreview_Paint);
            // 
            // groupPalette
            // 
            this.groupPalette.Controls.Add(this.btnBlack);
            this.groupPalette.Controls.Add(this.btnGreyDark);
            this.groupPalette.Controls.Add(this.btnGreyLight);
            this.groupPalette.Controls.Add(this.btnWhite);
            this.groupPalette.Location = new System.Drawing.Point(332, 6);
            this.groupPalette.Name = "groupPalette";
            this.groupPalette.Size = new System.Drawing.Size(200, 63);
            this.groupPalette.TabIndex = 12;
            this.groupPalette.TabStop = false;
            this.groupPalette.Text = "Palette";
            // 
            // btnBlack
            // 
            this.btnBlack.BackColor = System.Drawing.Color.Black;
            this.btnBlack.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnBlack.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnBlack.Location = new System.Drawing.Point(29, 19);
            this.btnBlack.Name = "btnBlack";
            this.btnBlack.Size = new System.Drawing.Size(32, 32);
            this.btnBlack.TabIndex = 8;
            this.btnBlack.UseVisualStyleBackColor = false;
            this.btnBlack.Click += new System.EventHandler(this.btncolor_Click);
            // 
            // btnGreyDark
            // 
            this.btnGreyDark.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.btnGreyDark.Location = new System.Drawing.Point(67, 19);
            this.btnGreyDark.Name = "btnGreyDark";
            this.btnGreyDark.Size = new System.Drawing.Size(32, 32);
            this.btnGreyDark.TabIndex = 9;
            this.btnGreyDark.UseVisualStyleBackColor = false;
            this.btnGreyDark.Click += new System.EventHandler(this.btncolor_Click);
            // 
            // btnGreyLight
            // 
            this.btnGreyLight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.btnGreyLight.Location = new System.Drawing.Point(105, 19);
            this.btnGreyLight.Name = "btnGreyLight";
            this.btnGreyLight.Size = new System.Drawing.Size(32, 32);
            this.btnGreyLight.TabIndex = 11;
            this.btnGreyLight.UseVisualStyleBackColor = false;
            this.btnGreyLight.Click += new System.EventHandler(this.btncolor_Click);
            // 
            // btnWhite
            // 
            this.btnWhite.BackColor = System.Drawing.Color.White;
            this.btnWhite.Location = new System.Drawing.Point(143, 19);
            this.btnWhite.Name = "btnWhite";
            this.btnWhite.Size = new System.Drawing.Size(32, 32);
            this.btnWhite.TabIndex = 10;
            this.btnWhite.UseVisualStyleBackColor = false;
            this.btnWhite.Click += new System.EventHandler(this.btncolor_Click);
            // 
            // tileBox
            // 
            this.tileBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tileBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tileBox.Cursor = System.Windows.Forms.Cursors.Default;
            this.tileBox.Location = new System.Drawing.Point(256, 75);
            this.tileBox.Name = "tileBox";
            this.tileBox.Size = new System.Drawing.Size(512, 512);
            this.tileBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.tileBox.TabIndex = 6;
            this.tileBox.TabStop = false;
            this.tileBox.Paint += new System.Windows.Forms.PaintEventHandler(this.tileBox_Paint);
            this.tileBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tileBox_MouseDown);
            this.tileBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.tileBox_MouseMove);
            this.tileBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tileBox_MouseUp);
            // 
            // groupTileSet
            // 
            this.groupTileSet.Controls.Add(this.panelTileSet);
            this.groupTileSet.Location = new System.Drawing.Point(12, 27);
            this.groupTileSet.Name = "groupTileSet";
            this.groupTileSet.Size = new System.Drawing.Size(200, 720);
            this.groupTileSet.TabIndex = 14;
            this.groupTileSet.TabStop = false;
            this.groupTileSet.Text = "Tileset";
            // 
            // panelTileSet
            // 
            this.panelTileSet.AutoScroll = true;
            this.panelTileSet.Location = new System.Drawing.Point(13, 19);
            this.panelTileSet.Name = "panelTileSet";
            this.panelTileSet.Size = new System.Drawing.Size(173, 695);
            this.panelTileSet.TabIndex = 13;
            // 
            // menuLevel
            // 
            this.menuLevel.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFileLevel});
            this.menuLevel.Location = new System.Drawing.Point(0, 0);
            this.menuLevel.Name = "menuLevel";
            this.menuLevel.Size = new System.Drawing.Size(1596, 24);
            this.menuLevel.TabIndex = 7;
            this.menuLevel.Text = "menu";
            // 
            // menuFileLevel
            // 
            this.menuFileLevel.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuNewLevel,
            this.menuOpenLevel,
            this.menuSaveLevel});
            this.menuFileLevel.Name = "menuFileLevel";
            this.menuFileLevel.Size = new System.Drawing.Size(37, 20);
            this.menuFileLevel.Text = "File";
            // 
            // menuNewLevel
            // 
            this.menuNewLevel.Name = "menuNewLevel";
            this.menuNewLevel.Size = new System.Drawing.Size(133, 22);
            this.menuNewLevel.Text = "New Level";
            this.menuNewLevel.Click += new System.EventHandler(this.menuNewLevel_Click);
            // 
            // menuOpenLevel
            // 
            this.menuOpenLevel.Name = "menuOpenLevel";
            this.menuOpenLevel.Size = new System.Drawing.Size(133, 22);
            this.menuOpenLevel.Text = "Open Level";
            this.menuOpenLevel.Click += new System.EventHandler(this.menuOpenLevel_Click);
            // 
            // menuSaveLevel
            // 
            this.menuSaveLevel.Enabled = false;
            this.menuSaveLevel.Name = "menuSaveLevel";
            this.menuSaveLevel.Size = new System.Drawing.Size(133, 22);
            this.menuSaveLevel.Text = "Save Level";
            this.menuSaveLevel.Click += new System.EventHandler(this.menuSaveLevel_Click);
            // 
            // menuTile
            // 
            this.menuTile.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFileTile});
            this.menuTile.Location = new System.Drawing.Point(0, 24);
            this.menuTile.Name = "menuTile";
            this.menuTile.Size = new System.Drawing.Size(1596, 24);
            this.menuTile.TabIndex = 8;
            this.menuTile.Text = "menu";
            this.menuTile.Visible = false;
            // 
            // menuFileTile
            // 
            this.menuFileTile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuNewTileSet,
            this.menuOpenTile,
            this.menuSaveTileset,
            this.menuNewTile});
            this.menuFileTile.Name = "menuFileTile";
            this.menuFileTile.Size = new System.Drawing.Size(37, 20);
            this.menuFileTile.Text = "File";
            // 
            // menuNewTileSet
            // 
            this.menuNewTileSet.Name = "menuNewTileSet";
            this.menuNewTileSet.Size = new System.Drawing.Size(140, 22);
            this.menuNewTileSet.Text = "New Tileset";
            this.menuNewTileSet.Click += new System.EventHandler(this.menuNewTileSet_Click);
            // 
            // menuOpenTile
            // 
            this.menuOpenTile.Name = "menuOpenTile";
            this.menuOpenTile.Size = new System.Drawing.Size(140, 22);
            this.menuOpenTile.Text = "Open Tileset";
            this.menuOpenTile.Click += new System.EventHandler(this.menuOpenTile_Click);
            // 
            // menuSaveTileset
            // 
            this.menuSaveTileset.Enabled = false;
            this.menuSaveTileset.Name = "menuSaveTileset";
            this.menuSaveTileset.Size = new System.Drawing.Size(140, 22);
            this.menuSaveTileset.Text = "Save Tileset";
            this.menuSaveTileset.Click += new System.EventHandler(this.menuSaveTileset_Click);
            // 
            // menuNewTile
            // 
            this.menuNewTile.Name = "menuNewTile";
            this.menuNewTile.Size = new System.Drawing.Size(140, 22);
            this.menuNewTile.Text = "Add Tile";
            this.menuNewTile.Click += new System.EventHandler(this.menuNewTile_Click);
            // 
            // openTileset
            // 
            this.openTileset.Filter = "PNG file|*.png";
            // 
            // saveTileset
            // 
            this.saveTileset.Filter = "PNG file|*.png";
            // 
            // openLevel
            // 
            this.openLevel.Filter = "Wafa Level file|*.lwf";
            // 
            // saveLevel
            // 
            this.saveLevel.Filter = "Wafa Level file|*.lwf";
            // 
            // panelEngine
            // 
            this.panelEngine.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelEngine.CurrentCollisionVal = WafaEditor.OglPanel.Collisions_Values.COLLISION;
            this.panelEngine.CurrentItem = null;
            this.panelEngine.CurrentLayer = WafaEditor.OglPanel.Layer_e.LAYER_BACKGROUND;
            this.panelEngine.Location = new System.Drawing.Point(316, 35);
            this.panelEngine.Name = "panelEngine";
            this.panelEngine.Size = new System.Drawing.Size(640, 480);
            this.panelEngine.TabIndex = 1;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1596, 763);
            this.Controls.Add(this.menuTile);
            this.Controls.Add(this.tabMain);
            this.Controls.Add(this.groupTileSet);
            this.Controls.Add(this.menuLevel);
            this.KeyPreview = true;
            this.Name = "Form1";
            this.Text = "Wafa Editor";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.tabMain.ResumeLayout(false);
            this.tabLevelEditor.ResumeLayout(false);
            this.tabLevelEditor.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numCollisionsAlpha)).EndInit();
            this.tabTileEditor.ResumeLayout(false);
            this.groupTools.ResumeLayout(false);
            this.groupPreviewMap.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureGridPreview)).EndInit();
            this.groupPalette.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tileBox)).EndInit();
            this.groupTileSet.ResumeLayout(false);
            this.menuLevel.ResumeLayout(false);
            this.menuLevel.PerformLayout();
            this.menuTile.ResumeLayout(false);
            this.menuTile.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabMain;
        private System.Windows.Forms.TabPage tabLevelEditor;
        private System.Windows.Forms.TabPage tabTileEditor;
        private System.Windows.Forms.Button btnBlack;
        private System.Windows.Forms.Button btnGreyLight;
        private System.Windows.Forms.PictureBox tileBox;
        private System.Windows.Forms.Button btnWhite;
        private System.Windows.Forms.Button btnGreyDark;
        private System.Windows.Forms.GroupBox groupPalette;
        private System.Windows.Forms.ToolStripMenuItem menuFileLevel;
        private System.Windows.Forms.ToolStripMenuItem menuNewLevel;
        private System.Windows.Forms.MenuStrip menuTile;
        private System.Windows.Forms.ToolStripMenuItem menuFileTile;
        private System.Windows.Forms.ToolStripMenuItem menuNewTileSet;
        private System.Windows.Forms.MenuStrip menuLevel;
        private System.Windows.Forms.ToolStripMenuItem menuOpenTile;
        private System.Windows.Forms.ToolStripMenuItem menuSaveTileset;
        private System.Windows.Forms.ToolStripMenuItem menuOpenLevel;
        private System.Windows.Forms.ToolStripMenuItem menuSaveLevel;
        private System.Windows.Forms.Panel panelTileSet;
        private System.Windows.Forms.GroupBox groupTileSet;
        private System.Windows.Forms.GroupBox groupPreviewMap;
        private System.Windows.Forms.PictureBox pictureGridPreview;
        private System.Windows.Forms.GroupBox groupTools;
        private System.Windows.Forms.Button btnPointTool;
        private System.Windows.Forms.Button btnBucketTool;
        private System.Windows.Forms.Button btnRectangle;
        private System.Windows.Forms.Button btnLine;
        private System.Windows.Forms.Button btnRedo;
        private System.Windows.Forms.Button btnUndo;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.OpenFileDialog openTileset;
        private System.Windows.Forms.ToolStripMenuItem menuNewTile;
        private System.Windows.Forms.SaveFileDialog saveTileset;
        private OglPanel panelEngine;
        private System.Windows.Forms.OpenFileDialog openLevel;
        private System.Windows.Forms.SaveFileDialog saveLevel;
        private System.Windows.Forms.CheckBox chkCollisions;
        private System.Windows.Forms.CheckBox chkBackground;
        private System.Windows.Forms.NumericUpDown numCollisionsAlpha;
        private System.Windows.Forms.Label lblCollisionAlpha;
        private System.Windows.Forms.Button btnEditCollisions;
        private System.Windows.Forms.Button btnEditBackground;
        private System.Windows.Forms.Label lbl_colval;
        private System.Windows.Forms.Button btn_nocol;
        private System.Windows.Forms.Button btn_collide;

    }
}

