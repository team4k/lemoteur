﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace WafaEditor
{
    public class TileItem :IDisposable
    {
        private Bitmap _tileBitmap;

        public Bitmap TileBitmap
        {
            get
            {
                return _tileBitmap;
            }
            set
            {
                _tileBitmap = value;
                _tilePreview.Image = value;
            }
        }

        private PictureBox _tilePreview;

        public PictureBox TilePreview
        {
            get
            {
                return _tilePreview;
            }
        }

        private int _tileId = 0;

        public int TileId
        {
            get
            {
                return _tileId;
            }
        }


        /// <summary>
        /// constructor for creating an empty tile
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="tileSize"></param>
        public TileItem(Panel parent, int tileSize, int posX, int posY, MouseEventHandler loadEmptyTile)
        {

            _tileBitmap = new Bitmap(tileSize, tileSize);


            for (int i = 0; i < _tileBitmap.Width; i++)
                for (int j = 0; j < _tileBitmap.Height; j++)
                    _tileBitmap.SetPixel(i, j, Color.White);


            _tilePreview = new PictureBox();
            _tilePreview.Location = new Point(posX, posY);
            _tilePreview.Size = new Size(tileSize, tileSize);
            _tilePreview.BorderStyle = BorderStyle.FixedSingle;
            _tilePreview.MouseDown += loadEmptyTile;
            _tilePreview.Tag = this;

            _tileId = parent.Controls.Count;//the tile id depend of the number of control in our preview box, since we add the same number of control than the number of tiles in our tileset

            parent.Controls.Add(_tilePreview);


            _tilePreview.Image = _tileBitmap;
        }


        public void Dispose()
        {
            _tilePreview.MouseDown -= null;
            _tileBitmap.Dispose();
        }
    }
}
