﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace WafaEditor
{

    public class OglPanel : Panel
    {
        public enum Layer_e { LAYER_FOREGROUND, LAYER_COLLISIONS, LAYER_BACKGROUND }

        public enum wbool  { wfalse = 0, wtrue = 1 } 

        public enum Collisions_Values {NO_COLLISION = 0, COLLISION = 1};


        public const string enginePath = "WafaEngine.dll";

        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        static extern void editor_draw_frame();

        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        static extern void editor_start_engine(IntPtr hwnd, int width, int height);

        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        static extern void editor_shutdown_engine();

        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        static extern void editor_setcursor_pos(int x, int y);

        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        static extern void editor_setcursor_color(float r, float g, float b, float a);

        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        static extern void editor_create_empty_tilemap(int rowCount, int colCount, int tileWidth, int tileHeight);

        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        static extern void editor_create_tilemap(int rowCount, int colCount, int tileWidth, int tileHeight, [In, Out] byte[] imageData, [In, Out] int[] backgroundFeed, [In, Out] int[] collisionsFeed, int textureWidth, int textureHeight);

        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        static extern void editor_Tilemap_update_texture(int textureWidth, int textureHeight, [In, Out] byte[] imageData);

        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        static extern void editor_change_tile(int tileId, int tilePos, Layer_e layer_type);

        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        static extern void editor_get_tilearray([In, Out] int[] arrayData, Layer_e layer_type);

        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        static extern void editor_update_tilemap([In, Out] int[] arrayData, Layer_e layer_type);

        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        static extern void editor_clean_tilemap();

         [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        static extern void editor_create_Tilemap_notex(int rowCount, int colCount, int tileWidth, int tileHeight, [In, Out] int[] backgroundFeed, [In, Out] int[] collisionsFeed);

        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        static extern void editor_changelayer_visibility(wbool visible,Layer_e layer_type);

        [DllImport(enginePath,CallingConvention=CallingConvention.Cdecl)]
        static extern void editor_changecollisions_color(float a);

        [DllImport(enginePath, CallingConvention = CallingConvention.Cdecl)]
        static extern void editor_get_tile([In, Out]int[] tileid, int tilepos, Layer_e layer_type);

        

        private bool _init = false;

        public bool Init
        {
            get
            {
                return _init;
            }
        }

        private TileItem currentItem;

        public TileItem CurrentItem
        {
            get
            {
                return currentItem;
            }
            set
            {
                currentItem = value;
            }
        }

        private int _ColCount;

        public int ColCount
        {
            get
            {
                return _ColCount;
            }
        }

        private int _RowCount;


        public int RowCount
        {
            get
            {
                return _RowCount;
            }
        }

        private Layer_e currentLayer = Layer_e.LAYER_BACKGROUND;

        public Layer_e CurrentLayer
        {
            get
            {
                return currentLayer;
            }
            set
            {
                currentLayer = value;
            }
        }


        private Collisions_Values currentCollisionVal = Collisions_Values.COLLISION;

        public Collisions_Values CurrentCollisionVal
        {
            get
            {
                return currentCollisionVal;
            }
            set
            {
                currentCollisionVal = value;
            }
        }

        public OglPanel() : base()
        {
            //this.SetStyle(ControlStyles.EnableNotifyMessage, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.DoubleBuffer, false);
            this.SetStyle(ControlStyles.Opaque, true);
	        this.SetStyle(ControlStyles.ResizeRedraw, true);
	        this.SetStyle(ControlStyles.UserPaint, true);
        }

        public void CreateEmptyTilemap(int rowCount, int colCount)
        {
            if (!_init)
            {
                editor_start_engine(this.Handle, this.Width, this.Height);
                _init = true;
            }
            else
            {
                editor_clean_tilemap();
            }

            this.Invalidate();

            _RowCount = rowCount;
            _ColCount = colCount;

            //create tileset
            editor_create_empty_tilemap(rowCount, colCount, Form1.TILE_SIZE, Form1.TILE_SIZE);

        }

        public void CleanTilemap()
        {
            editor_clean_tilemap();
        }

        public void CreateTilemapWithData(int rowCount, int colCount, int[] backgroundData,int[] collisionsData, byte[] imageData, int textureWidth, int textureHeight)
        {
            if (!_init)
            {
                editor_start_engine(this.Handle, this.Width, this.Height);
                _init = true;
            }
            else
            {
                editor_clean_tilemap();
            }

            _RowCount = rowCount;
            _ColCount = colCount;

            int[] arrayData1 = Array.ConvertAll<int, int>(backgroundData, p => (int)p);
            int[] arrayData2 = Array.ConvertAll<int, int>(collisionsData, p => (int)p);

            if(imageData != null)
                editor_create_tilemap(rowCount, colCount, Form1.TILE_SIZE, Form1.TILE_SIZE, imageData, arrayData1, arrayData2, textureWidth, textureHeight);
            else
                editor_create_Tilemap_notex(rowCount, colCount, Form1.TILE_SIZE, Form1.TILE_SIZE, arrayData1, arrayData2);

            this.Invalidate();
        }

        public int[] GetTilemapArray()
        {
            return this.GetTilemapArray(currentLayer); 
        }

        public int[] GetTilemapArray(Layer_e layer)
        {
            int[] arrayData = new int[RowCount * ColCount];
            editor_get_tilearray(arrayData, layer);

            return Array.ConvertAll<int, int>(arrayData, p => (int)p);
        }

        public void UpdateTilemap(int[] tilemapArray)
        {
             int[] arrayData = Array.ConvertAll<int, int>(tilemapArray, p => (int)p);

             editor_update_tilemap(arrayData,currentLayer);
        }

        public void UpdateTexture(int textureWidth, int textureHeight, byte[] imageData)
        {
            editor_Tilemap_update_texture(textureWidth, textureHeight, imageData);
        }

        public void StopOgl()
        {
            editor_shutdown_engine();
        }


        public void ChangeLayerVisibility(wbool visible, Layer_e layer)
        {
            editor_changelayer_visibility(visible, layer);
        }

        public void ChangeCollisionsColor(float a)
        {
            editor_changecollisions_color(a);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (!_init)
                return;

            //constraint the cursor pos to the tile grid
            int col_pos = 0;
            int row_pos = 0;

            int row_rm = 0;
            int map_height_size = _RowCount * Form1.TILE_SIZE;

            if (map_height_size >= this.Height)
                row_rm = _RowCount;
            else
                row_rm = ((this.Height - map_height_size) / Form1.TILE_SIZE) + _RowCount - 1;


            if (e.X > 0)
                col_pos = (int)Math.Floor((double)e.X / Form1.TILE_SIZE);

            if (e.Y > 0)
                row_pos = row_rm - (int)Math.Floor((double)e.Y / Form1.TILE_SIZE);

            editor_setcursor_pos(col_pos * Form1.TILE_SIZE, row_pos * Form1.TILE_SIZE);

            this.Invalidate();

            base.OnMouseMove(e);
        }



        protected override void OnMouseDown(MouseEventArgs e)
        {
            if (!_init || (currentItem == null && currentLayer != Layer_e.LAYER_COLLISIONS))
                return;


            //get cursor pos
            int col_pos = 0;
            int row_pos = 0;

            int row_rm = 0;
            int map_height_size = _RowCount * Form1.TILE_SIZE;

            if (map_height_size >= this.Height)
                row_rm = _RowCount;
            else
                row_rm = ((this.Height - map_height_size) / Form1.TILE_SIZE) + _RowCount - 1;

            if (e.X > 0)
                col_pos = (int)Math.Floor((double)e.X / Form1.TILE_SIZE);

            if (e.Y > 0)
                row_pos = row_rm - (int)Math.Floor((double)e.Y / Form1.TILE_SIZE);

            int tilePos = row_pos * _ColCount + col_pos;

            if (e.Button == System.Windows.Forms.MouseButtons.Left)//edit the current layer
            {
                if (currentLayer == Layer_e.LAYER_COLLISIONS)
                    editor_change_tile((int)currentCollisionVal, tilePos, currentLayer);
                else
                    editor_change_tile(currentItem.TileId, tilePos, currentLayer);
            }
            else if(e.Button == System.Windows.Forms.MouseButtons.Right)//select the current tile under the cursor
            {
                int[] tile = new int[1];
                editor_get_tile(tile, tilePos, currentLayer);

                if (currentLayer == Layer_e.LAYER_COLLISIONS)
                    currentCollisionVal = (tile[0] == 1) ? Collisions_Values.COLLISION : Collisions_Values.NO_COLLISION;
                else if (currentLayer == Layer_e.LAYER_BACKGROUND)
                {
                    Panel pTileSet = this.FindForm().Controls.Find("groupTileSet", false)[0].Controls[0] as Panel;

                    foreach (PictureBox pbox in pTileSet.Controls)
                    { 
                        TileItem tempItem = pbox.Tag as TileItem;

                        if (tempItem.TileId == tile[0])
                        {
                            currentItem = tempItem;
                            break;
                        }
                    }
                }
            }

            base.OnMouseDown(e);
        }


        protected override void OnPaint(PaintEventArgs e)
        {
            if (_init)
                editor_draw_frame();
        }

         protected override void OnPaintBackground(PaintEventArgs e) {

	        }
    }
}
