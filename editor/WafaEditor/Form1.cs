﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Runtime.InteropServices;
using System.IO;
using System.Diagnostics;
using System.Configuration;

namespace WafaEditor
{
    public enum Tools
    {
        Pencil,
        Bucket,
        Line,
        Rectangle,
        Select
    }

    public partial class Form1 : Form
    {
        public const int TILE_SIZE = 32;
        const int MAX_UNDO = 40;
        Bitmap currentBitmap;
        TileItem currentItem;
        const int TILE_PER_ROW = 10;

        Tools currentTool = Tools.Pencil;

        Button[] paletteArray;

        Button[] layerArray;

        Color selectedColor = Color.Black;

        Rectangle selectedBox = Rectangle.Empty;

        bool currentClick = false;

        Dictionary<string, MenuStrip> MenuArray;

        List<TileItem> tileSetList = new List<TileItem>();

        int tileSetOffsetX = 10;
        int tileSetOffsetY = 20;

        Point savePoint = Point.Empty;

        MouseEventArgs lastMouseEvent;

        List<byte[]> undoRedoList = new List<byte[]>(MAX_UNDO);

        int undoRedoCounter = 0;

        bool previewDrawn = false;

        private string oldTab = string.Empty;

        private const short pixelSize = 4;



        public Form1()
        {
            InitializeComponent();

            paletteArray = new Button[] { btnBlack,btnGreyDark,btnGreyLight,btnWhite };
            layerArray = new Button[] {btnEditBackground,btnEditCollisions };

            MenuArray = new Dictionary<string, MenuStrip>()
            {
                {"tabLevelEditor",menuLevel},
                {"tabTileEditor",menuTile}
            };

        }

        private void tileItem_MouseDown(object sender, MouseEventArgs e)
        {
            PictureBox cItem = sender as PictureBox;

            undoRedoList.Clear();
            btnUndo.Enabled = btnRedo.Enabled = false;
            undoRedoCounter = 0;

            tileBox.Image = currentBitmap = (Bitmap)cItem.Image;
            tileBox.Invalidate();

            currentItem = cItem.Tag as TileItem;

            panelEngine.CurrentItem = currentItem;
        }



        private void tileBox_Paint(object sender, PaintEventArgs e)
        {
            if (currentBitmap == null)
                return;

            e.Graphics.InterpolationMode = InterpolationMode.NearestNeighbor;
            e.Graphics.SmoothingMode = SmoothingMode.None;

            //set the center of the pixel when drawing to 0.5 / 0.5 rather than the default 0 / 0 (default make the drawing start at -0.5)
            e.Graphics.PixelOffsetMode = PixelOffsetMode.Half;

            e.Graphics.DrawImage(
                currentBitmap,
                new Rectangle(0, 0, tileBox.Width, tileBox.Height),
                // destination rectangle 
                0,
                0,           // upper-left corner of source rectangle
                currentBitmap.Width,       // width of source rectangle
                currentBitmap.Height,      // height of source rectangle
                GraphicsUnit.Pixel);

            if (!previewDrawn)
            {
                switch (currentTool)
                {
                    case Tools.Line:
                        this.PreviewLine(e.Graphics);
                        break;
                    case Tools.Rectangle:
                        this.PreviewRectangle(e.Graphics);
                        break;
                    case Tools.Select:
                        this.SelectBox(e.Graphics);
                        break;
                }
            }
        }

        private void tileBox_MouseDown(object sender, MouseEventArgs e)
        {
            if (currentBitmap == null)
                return;

            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {

                currentClick = true;
                //before any change to bitmap, save it to a byte array
                if (currentBitmap != null && undoRedoList.Count == 0 && currentTool != Tools.Select)
                {
                    SnapshotBeforeChange();
                }

                switch (currentTool)
                {
                    case Tools.Pencil:
                        this.PaintPixel(e);
                        break;

                    case Tools.Bucket:
                        this.FillPixels(e);
                        break;

                    case Tools.Line:
                        savePoint = e.Location;
                        lastMouseEvent = e;
                        previewDrawn = false;
                        tileBox.Invalidate();
                        break;

                    case Tools.Rectangle:
                        savePoint = e.Location;
                        lastMouseEvent = e;
                        previewDrawn = false;
                        tileBox.Invalidate();
                        break;
                    case Tools.Select:
                        savePoint = e.Location;
                        lastMouseEvent = e;
                        previewDrawn = false;
                        tileBox.Invalidate();
                        break;
                }
            }
            else
            {
                //select the color under the cursor

                Point ColorLocation = this.GetToolPointer(e);

                selectedColor = currentBitmap.GetPixel(ColorLocation.X, ColorLocation.Y);


                foreach (Button nselectBtn in paletteArray)
                {
                    if (nselectBtn.BackColor.ToArgb() != selectedColor.ToArgb())
                        nselectBtn.FlatStyle = FlatStyle.Standard;
                    else
                    {
                        nselectBtn.FlatStyle = FlatStyle.Flat;
                        nselectBtn.Focus();
                    }
                }

            }
        }

        private void tileBox_MouseMove(object sender, MouseEventArgs e)
        {
            switch (currentTool)
            {
                case Tools.Pencil:

                    if (currentClick)
                        this.PaintPixel(e);
                    break;
                case Tools.Line:
                    if (currentClick)
                    {
                        lastMouseEvent = e;
                        tileBox.Invalidate();
                    }
                    break;
                case Tools.Rectangle:
                    if (currentClick)
                    {
                        lastMouseEvent = e;
                        tileBox.Invalidate();
                    }
                    break;
                case Tools.Select:
                    if (currentClick)
                    {
                        lastMouseEvent = e;
                        tileBox.Invalidate();
                    }
                    break;
            }
        }


        private void tileBox_MouseUp(object sender, MouseEventArgs e)
        {
            switch (currentTool)
            {
                case Tools.Line:
                    this.DrawLine(e);
                    break;
                case Tools.Rectangle:
                    this.DrawRectangle(e);
                    break;
            }

            currentClick = false;


            if (currentTool != Tools.Select)
            {
                SnapshotAfterChange();
            }

        }

        private void SnapshotBeforeChange()
        {
            btnUndo.Enabled = true;

            MemoryStream ms = new MemoryStream();
            currentBitmap.Save(ms, ImageFormat.Bmp);
            ms.Position = 0;

            undoRedoList.Add(ms.ToArray());
            undoRedoCounter++;
        }

        private void SnapshotAfterChange()
        {
            //after any change to bitmap, save it to a byte array
            //we overwrite every action after the counter
            if (undoRedoCounter < undoRedoList.Count)
            {
                undoRedoList.RemoveRange(undoRedoCounter, undoRedoList.Count - undoRedoCounter);
                btnRedo.Enabled = false;
            }

            btnUndo.Enabled = true;

            MemoryStream ms = new MemoryStream();
            currentBitmap.Save(ms, ImageFormat.Bmp);
            ms.Position = 0;

            undoRedoList.Add(ms.ToArray());
            undoRedoCounter++;

            currentItem.TilePreview.Invalidate();
            pictureGridPreview.Invalidate();
        }

        private Point GetToolPointer(MouseEventArgs e)
        {
            return this.GetToolPointer(e.Location);
        }

        private Point GetToolPointer(Point e)
        {
            int x, y;

            //convert picturebox point to bitmap point
            double conX = ((double)e.X / tileBox.Width * TILE_SIZE);
            double leftX = conX - Math.Truncate(conX);

            if (leftX > 0.5)
                x = (int)Math.Ceiling(conX);
            else
                x = (int)Math.Floor(conX);


            double conY = ((double)e.Y / tileBox.Height * TILE_SIZE);
            double leftY = conY - Math.Truncate(conY);

            if (leftY > 0.5)
                y = (int)Math.Ceiling(conY);
            else
                y = (int)Math.Floor(conY);

            if (x < 0)
                x = 0;

            if (y < 0)
                y = 0;


            if (x >= currentBitmap.Width)
                x = currentBitmap.Width - 1;

            if (y >= currentBitmap.Height)
                y = currentBitmap.Height - 1;

            return new Point(x, y);
        }

        private void PaintPixel(MouseEventArgs e)
        {
            if (currentBitmap == null)
                return;

            Point toolPoint = this.GetToolPointer(e);

            currentBitmap.SetPixel(toolPoint.X, toolPoint.Y, selectedColor);
            tileBox.Invalidate();
        }

        private void FillPixels(MouseEventArgs e)
        {
            if (currentBitmap == null)
                return;

            Point toolPoint = this.GetToolPointer(e);

            //find all the neighbourg pixel that share the same color than the pointed color and recolor them
            Color PointedColor = currentBitmap.GetPixel(toolPoint.X, toolPoint.Y);

            this.floodFill(currentBitmap, toolPoint, PointedColor, selectedColor);

            tileBox.Invalidate();
            currentItem.TilePreview.Invalidate();
            pictureGridPreview.Invalidate();

        }

        private void PreviewLine(Graphics g)
        {
            if (currentBitmap == null || lastMouseEvent == null)
                return;

            Point toolPoint = lastMouseEvent.Location;
                //this.GetToolPointer(lastMouseEvent);

            if (savePoint == toolPoint)
                return;


            //preview the line without drawing it to the bitmap
            Bitmap bitmapPixel = new Bitmap(1, 1);
            bitmapPixel.SetPixel(0, 0, selectedColor);
            int size = tileBox.Width / TILE_SIZE;

            Point lStart = this.GetToolPointer(savePoint);
            Point lEnd = this.GetToolPointer(toolPoint);

            int x0 = lStart.X;
            int y0 = lStart.Y;
            int x1 = lEnd.X;
            int y1 = lEnd.Y;

            int dx = Math.Abs(x1 - x0), sx = x0 < x1 ? 1 : -1;
            int dy = Math.Abs(y1 - y0), sy = y0 < y1 ? 1 : -1;
            int err = (dx > dy ? dx : -dy) / 2, e2;

            for (; ; )
            {
                g.DrawImage(bitmapPixel, x0 * size, y0 * size, size, size);
                if (x0 == x1 && y0 == y1) break;
                e2 = err;
                if (e2 > -dx) { err -= dy; x0 += sx; }
                if (e2 < dy) { err += dx; y0 += sy; }
            }

            bitmapPixel.Dispose();


        }

        private void DrawLine(MouseEventArgs e)
        {
            if (currentBitmap == null)
                return;

         
            //points on bitmap
            Point lStart = this.GetToolPointer(savePoint);
            Point lEnd = this.GetToolPointer(e);


            //modify the bitmap with the line
            //Bresenham's line algorithm
            int x0 = lStart.X;
            int y0 = lStart.Y;
            int x1 = lEnd.X;
            int y1 = lEnd.Y;
 
          int dx = Math.Abs(x1-x0), sx = x0<x1 ? 1 : -1;
          int dy = Math.Abs(y1-y0), sy = y0<y1 ? 1 : -1; 
          int err = (dx>dy ? dx : -dy)/2, e2;
 
          for(;;){
            currentBitmap.SetPixel(x0,y0,selectedColor);
            if (x0==x1 && y0==y1) break;
            e2 = err;
            if (e2 >-dx) { err -= dy; x0 += sx; }
            if (e2 < dy) { err += dx; y0 += sy; }
          }

          previewDrawn = true;
          tileBox.Invalidate();
        }

        private void PreviewRectangle(Graphics g)
        {
            if (currentBitmap == null)
                return;

            Point toolPoint = lastMouseEvent.Location;

            if (savePoint == toolPoint)
                return;

            Point lStart = this.GetToolPointer(savePoint);
            Point lEnd = this.GetToolPointer(toolPoint);

            int x = lStart.X < lEnd.X ? lStart.X : lEnd.X;
            int y = lStart.Y < lEnd.Y ? lStart.Y : lEnd.Y;

            int width = Math.Abs(lEnd.X - lStart.X);
            int height = Math.Abs(lEnd.Y - lStart.Y);

            int size = tileBox.Width / TILE_SIZE;

            Pen BorderPen = new Pen(selectedColor, size);
            BorderPen.Alignment = PenAlignment.Inset;


            //preview the rectangle without drawing it to the picturebox control
            g.DrawRectangle(BorderPen, x * size, y * size, (width * size) + size, (height * size) + size);
            g.FillRectangle(new SolidBrush(selectedColor), x * size, y * size, (width * size) + size, (height * size) + size);
        }

        private void DrawRectangle(MouseEventArgs e)
        {
            if (currentBitmap == null)
                return;

            //modify the bitmap with the line
            Point lStart = this.GetToolPointer(savePoint);
            Point lEnd = this.GetToolPointer(lastMouseEvent.Location);

            int x = lStart.X < lEnd.X ? lStart.X : lEnd.X;
            int y = lStart.Y < lEnd.Y ? lStart.Y : lEnd.Y;

            int width = Math.Abs(lEnd.X - lStart.X);
            int height = Math.Abs(lEnd.Y - lStart.Y);


            Graphics g = Graphics.FromImage(currentBitmap);
            g.InterpolationMode = InterpolationMode.NearestNeighbor;

            Pen BorderPen = new Pen(selectedColor, 1.0f);

            g.DrawRectangle(BorderPen, x, y, width, height);
            g.FillRectangle(new SolidBrush(selectedColor), x , y , width, height);
            g.Dispose();


            previewDrawn = true;
            tileBox.Invalidate();
        }

        private void SelectBox(Graphics g)
        {
            if (currentBitmap == null)
                return;

            Point toolPoint = lastMouseEvent.Location;

            if (savePoint == toolPoint)
                return;

            Point lStart = this.GetToolPointer(savePoint);
            Point lEnd = this.GetToolPointer(toolPoint);

            int x = lStart.X < lEnd.X ? lStart.X : lEnd.X;
            int y = lStart.Y < lEnd.Y ? lStart.Y : lEnd.Y;

            int width = Math.Abs(lEnd.X - lStart.X);
            int height = Math.Abs(lEnd.Y - lStart.Y);

            int size = tileBox.Width / TILE_SIZE;

            //preview the rectangle without drawing it to the picturebox control
            g.DrawRectangle(new Pen(Color.Red, 1.0f), x * size, y * size, (width * size) + size, (height * size) + size);

            //update the selected box
            selectedBox = new Rectangle(x, y, width +1, height+1);
        }

        public void floodFill(Bitmap image, Point node, Color targetColor, Color replacementColor)
        {
            int width = image.Width;
            int height = image.Height;
            int target = targetColor.ToArgb();
            int replacement = replacementColor.ToArgb();

            if (target != replacement)
            {
                Queue<Point> queue = new Queue<Point>();
                do
                {
                    int x = node.X;
                    int y = node.Y;
                    while (x > 0 && image.GetPixel(x - 1, y).ToArgb() == target)
                    {
                        x--;
                    }
                    bool spanUp = false;
                    bool spanDown = false;
                    while (x < width && image.GetPixel(x, y).ToArgb() == target)
                    {
                        image.SetPixel(x, y, replacementColor);

                        if (!spanUp && y > 0 && image.GetPixel(x, y - 1).ToArgb() == target)
                        {
                            queue.Enqueue(new Point(x, y - 1));
                            spanUp = true;
                        }
                        else if (spanUp && y > 0 && image.GetPixel(x, y - 1).ToArgb() != target)
                        {
                            spanUp = false;
                        }
                        if (!spanDown && y < height - 1 && image.GetPixel(x, y + 1).ToArgb() == target)
                        {
                            queue.Enqueue(new Point(x, y + 1));
                            spanDown = true;
                        }
                        else if (spanDown && y < height - 1 && image.GetPixel(x, y + 1).ToArgb() != target)
                        {
                            spanDown = false;
                        }
                        x++;
                    }
                } while (queue.Count > 0 && (node = queue.Dequeue()) != null);
            }
        }

        private Bitmap ClipBitmap()
        {
            if (currentBitmap == null || selectedBox.IsEmpty)
                return null;

            Bitmap clipMap = new Bitmap(selectedBox.Width,selectedBox.Height);

            int destX = 0,destY = 0;

            //copy all pixel in selected box to the clipped bitmap


            for(int x = selectedBox.X;x < (selectedBox.X + selectedBox.Width);x++)
            {
                destY = 0;

                for(int y = selectedBox.Y;y < (selectedBox.Y + selectedBox.Height) ;y++)
                {
                    clipMap.SetPixel(destX, destY, currentBitmap.GetPixel(x, y));
                    destY++;
                }

                destX++;
            }

            return clipMap;
        }

        private void DrawClippedBitmap(Bitmap clipBitmap)
        {
            if (currentBitmap == null)
                return;

            int srcX = 0, srcY = 0;

            //copy all pixel in the clipped bitmap to the currentBitmap within selectedbox
            for (int x = selectedBox.X; x <= (selectedBox.X + selectedBox.Width); x++)
            {
                srcY = 0;

                for (int y = selectedBox.Y; y <= (selectedBox.Y + selectedBox.Height) ; y++)
                {
                    if (srcX < clipBitmap.Width && srcY < clipBitmap.Height)
                        currentBitmap.SetPixel(x, y, clipBitmap.GetPixel(srcX, srcY));

                    srcY++;
                }

                srcX++;
            }

            tileBox.Invalidate();
            currentItem.TilePreview.Invalidate();
            pictureGridPreview.Invalidate();
        }


        private void btncolor_Click(object sender, EventArgs e)
        {
            Button selectedButton = sender as Button;

            foreach (Button nselectBtn in paletteArray)
                nselectBtn.FlatStyle = FlatStyle.Standard;

            selectedButton.FlatStyle = FlatStyle.Flat;

            selectedColor = selectedButton.BackColor;

           
        }

        Bitmap tilesetBitmap = null;

        private byte[] GetBitmapContent(Bitmap tilesetBitmap)
        {
            int bmp_size = tilesetBitmap.Width * tilesetBitmap.Height * pixelSize;
            byte[] bitmapContent = new byte[bmp_size];

            int iBmp = 0;

            for (int y = 0; y < tilesetBitmap.Height; y++)
            {
                for (int x = 0; x < tilesetBitmap.Width; x++)
                {
                    bitmapContent[iBmp] = tilesetBitmap.GetPixel(x, y).R;
                    bitmapContent[iBmp + 1] = tilesetBitmap.GetPixel(x, y).G;
                    bitmapContent[iBmp + 2] = tilesetBitmap.GetPixel(x, y).B;
                    bitmapContent[iBmp + 3] = tilesetBitmap.GetPixel(x, y).A;
                    iBmp += pixelSize;
                }
            }

            return bitmapContent;
        }

        private void tabMain_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!MenuArray.ContainsKey(tabMain.SelectedTab.Name))
                return;

            //pack tile in tileset when leaving tileset tab
            if (oldTab == "tabTileEditor")
            {
                tilesetBitmap = this.packTiles(false);

                if (tilesetBitmap != null)
                {
                    //update level tilemap
                    byte[] bitmapContent = this.GetBitmapContent(tilesetBitmap);

                    panelEngine.UpdateTexture(Convert.ToInt16(tilesetBitmap.Width), Convert.ToInt16(tilesetBitmap.Height), bitmapContent);
                }
            }

            foreach(MenuStrip mStrip in MenuArray.Values)
            {
                mStrip.Visible = false;

                if(mStrip == MenuArray[tabMain.SelectedTab.Name])
                    mStrip.Visible = true;
            }

            oldTab = tabMain.SelectedTab.Name;


        }

        private void menuNewTile_Click(object sender, EventArgs e)
        {
            this.NewTile();
        }

        private void menuNewTileSet_Click(object sender, EventArgs e)
        {
           
            //-1- clear old elements
            panelTileSet.Controls.Clear();

            tileSetList.ForEach(delegate(TileItem item)
            {
                item.Dispose();
            });

            tileSetList.Clear();
            currentBitmap = null;
            tileBox.Image = null;
            tileBox.Invalidate();

            //-2- create our new tileset
            this.NewTile();

            menuSaveTileset.Enabled = true;

        }

        private void NewTile()
        {
              int lastY = tileSetOffsetY;

              if(tileSetList.Count > 0)
                  lastY += tileSetList[tileSetList.Count - 1].TilePreview.Height + tileSetOffsetY;

              //-1- add an item in the tileset panel
              TileItem cItem = new TileItem(panelTileSet, TILE_SIZE, tileSetOffsetX, lastY,this.tileItem_MouseDown);
              tileSetList.Add(cItem);
              currentItem = cItem;
              panelEngine.CurrentItem = currentItem;

              //-2- set our new bitmap to the main bitmap element
              tileBox.Image = currentBitmap = cItem.TileBitmap;

        }



        private void pictureGridPreview_Paint(object sender, PaintEventArgs e)
        {
            if (currentBitmap == null)
                return;

            int num_cols = pictureGridPreview.Width / currentBitmap.Width;
            int num_rows = pictureGridPreview.Height / currentBitmap.Height;

            e.Graphics.InterpolationMode = InterpolationMode.NearestNeighbor;

            for (int i = 0; i < num_rows; i++)
            {
                for (int j = 0; j < num_cols; j++)
                {
                    int idx = i * currentBitmap.Width;
                    int idy = j * currentBitmap.Height;

                    e.Graphics.DrawImage(
                      currentBitmap,
                      new Rectangle(idx, idy, currentBitmap.Width, currentBitmap.Height),
                                // destination rectangle 
                      0,
                      0,           // upper-left corner of source rectangle
                      currentBitmap.Width,       // width of source rectangle
                      currentBitmap.Height,      // height of source rectangle
                      GraphicsUnit.Pixel);
                }
            }

        }

        private void btnBucketTool_Click(object sender, EventArgs e)
        {
            currentTool = Tools.Bucket;
        }

        private void btnPointTool_Click(object sender, EventArgs e)
        {
            currentTool = Tools.Pencil;
        }

        private void btnLine_Click(object sender, EventArgs e)
        {
            currentTool = Tools.Line;
        }


        private void btnRectangle_Click(object sender, EventArgs e)
        {
            currentTool = Tools.Rectangle;
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            currentTool = Tools.Select;
        }


        private void btnUndo_Click(object sender, EventArgs e)
        {
            if (undoRedoCounter == 0)
                return;

            //update the current bitmap
            MemoryStream ms = new MemoryStream(undoRedoList[undoRedoCounter-2]);
            ms.Position = 0;
            currentBitmap = currentItem.TileBitmap = (Bitmap)Bitmap.FromStream(ms);
            

            if (--undoRedoCounter == 1)
                btnUndo.Enabled = false;

            if(undoRedoList.Count > undoRedoCounter)
                btnRedo.Enabled = true;

            tileBox.Invalidate();
            currentItem.TilePreview.Invalidate();
            pictureGridPreview.Invalidate();
        }

        private void btnRedo_Click(object sender, EventArgs e)
        {
            if (undoRedoCounter == undoRedoList.Count)
                return;

            //update the current bitmap
            MemoryStream ms = new MemoryStream(undoRedoList[undoRedoCounter]);
            ms.Position = 0;
            currentBitmap.Dispose();
            currentBitmap = currentItem.TileBitmap = (Bitmap)Bitmap.FromStream(ms);

            if (++undoRedoCounter == undoRedoList.Count)
                btnRedo.Enabled = false;

            if(undoRedoCounter > 0)
                btnUndo.Enabled = true;

            tileBox.Invalidate();
            currentItem.TilePreview.Invalidate();
            pictureGridPreview.Invalidate();
        }

        private void menuOpenTile_Click(object sender, EventArgs e)
        {
            if (openTileset.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.OpenTileSet(openTileset.FileName);

            }
        }


        private void OpenTileSet(string FilePath)
        {
            Bitmap tileSetmap = (Bitmap)Bitmap.FromFile(FilePath);

            //clean existing data
            panelTileSet.Controls.Clear();

            tileSetList.ForEach(delegate(TileItem item)
            {
                item.Dispose();
            });

            tileSetList.Clear();
            currentBitmap = null;
            tileBox.Image = null;
            tileBox.Invalidate();



            //get the number of tile in set (approx)
            int numTiles = 0;
            int tile_per_row = tileSetmap.Width / TILE_SIZE;

            numTiles = (tileSetmap.Width / TILE_SIZE) * (tileSetmap.Height / TILE_SIZE);

            int lastY = tileSetOffsetY;

            int xsrc = 0;
            int ysrc = 0;

            for (int i = 0; i < numTiles; i++)
            {
                if (tileSetList.Count > 0)
                    lastY += tileSetList[tileSetList.Count - 1].TilePreview.Height + tileSetOffsetY;

                //add an item in the tileset panel
                TileItem cItem = new TileItem(panelTileSet, TILE_SIZE, tileSetOffsetX, lastY, this.tileItem_MouseDown);
                tileSetList.Add(cItem);

                //retrieve bitmap information for the tile
                for (int x = 0; x < TILE_SIZE; x++)
                {
                    ysrc = (int)Math.Floor((double)(i / tile_per_row)) * TILE_SIZE;

                    for (int y = 0; y < TILE_SIZE; y++)
                    {
                        cItem.TileBitmap.SetPixel(x, y, tileSetmap.GetPixel(xsrc, ysrc++));
                    }

                    xsrc++;
                }

                if (xsrc >= tileSetmap.Width)
                    xsrc = 0;

                //peek the next tile color, if it's transparent, we reach the end of the tileset
                if (xsrc < tileSetmap.Width)
                {
                    Color peek_color = tileSetmap.GetPixel(xsrc + 1, (int)Math.Floor((double)(i / tile_per_row)) * TILE_SIZE);
                    if (peek_color.A == 0)
                        break;
                }
            }

            currentItem = tileSetList[0];
            //set our new bitmap to the main bitmap element
            tileBox.Image = currentBitmap = tileSetList[0].TileBitmap;
            tileBox.Invalidate();
            tileSetmap.Dispose();

            menuSaveTileset.Enabled = true;
        }

        private Bitmap packTiles(bool openglMode)
        {
            //pack the tilebitmap to a single bitmap, put 10 tile per line
            int width, height = 0;

            if (tileSetList.Count <= TILE_PER_ROW)
            {
                width = tileSetList.Count * TILE_SIZE;
                height = TILE_SIZE;
            }
            else
            {
                width = TILE_PER_ROW * TILE_SIZE;
                height = (int)Math.Ceiling((double)tileSetList.Count / TILE_PER_ROW) * TILE_SIZE;
            }

            if (width == 0 || height == 0)
                return null;

            Bitmap tilesetBitmap = new Bitmap(width, height);

            for (int x = 0; x < width; x++)
                for (int y = 0; y < height; y++)
                    tilesetBitmap.SetPixel(x, y, Color.Pink);

            tilesetBitmap.MakeTransparent(Color.Pink);

            int tset_x = 0;
            int tset_y = 0;




            for (int iTile = 0; iTile < tileSetList.Count; iTile++)
            {
                for (int i = 0; i < TILE_SIZE; i++)
                {
                    tset_y = (int)Math.Floor((double)(iTile / TILE_PER_ROW)) * TILE_SIZE;

                    //in opengl mode, invert the y axis

                    if (openglMode)
                    {
                        for (int j = TILE_SIZE - 1; j >= 0; j--)
                        {
                            tilesetBitmap.SetPixel(tset_x, tset_y, tileSetList[iTile].TileBitmap.GetPixel(i, j));
                            tset_y++;
                        }
                    }
                    else
                    {
                        for (int j = 0; j < TILE_SIZE; j++)
                        {
                            tilesetBitmap.SetPixel(tset_x, tset_y, tileSetList[iTile].TileBitmap.GetPixel(i, j));
                            tset_y++;
                        }
                    }

                    tset_x++;
                }

                    if ((iTile + 1) % TILE_PER_ROW == 0)
                    tset_x = 0;
            }

            return tilesetBitmap;
        }

        private void menuSaveTileset_Click(object sender, EventArgs e)
        {
            if (saveTileset.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {

                Bitmap tilesetBitmap = this.packTiles(false);

                try
                {
                    tilesetBitmap.Save(openTileset.FileName, ImageFormat.Png);
                    MessageBox.Show(string.Format("Fichier {0} Sauvegardé ! ",openTileset.FileName), "Sauvegarde ok", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(string.Format("Erreur lors de l'enregistrement du fichier {0} , Exception : {1} ",openTileset.FileName,ex.ToString()), "Sauvegarde ko", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }



        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Z && e.Control)
            {
                this.btnUndo_Click(sender, e);
            }
            else if (e.KeyCode == Keys.Y && e.Control)
            {
                this.btnRedo_Click(sender, e);
            }
            else if (e.KeyCode == Keys.C && e.Control)
            {
                if (currentTool == Tools.Select)
                    Clipboard.SetImage(this.ClipBitmap());

            }
            else if (e.KeyCode == Keys.V && e.Control)
            {
                if (currentTool == Tools.Select)
                {
                    this.SnapshotBeforeChange();
                    this.DrawClippedBitmap((Bitmap)Clipboard.GetImage());
                    this.SnapshotAfterChange();
                }
            }

        }


        private void menuNewLevel_Click(object sender, EventArgs e)
        {
            //ask for new level dimension
            NewLevel formLevel = new NewLevel();

            if (formLevel.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                //startup engine
                panelEngine.CreateEmptyTilemap(Convert.ToInt16(formLevel.numRows.Value), Convert.ToInt16(formLevel.numCols.Value));
                menuSaveLevel.Enabled = true;
            }
        }

        private void menuSaveLevel_Click(object sender, EventArgs e)
        {
            if (saveLevel.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                //create a new protobuf object for saving level
                PBInterface.Level cLevel = new PBInterface.Level();
                cLevel.tile_size = TILE_SIZE;
                cLevel.background_array.AddRange(panelEngine.GetTilemapArray(OglPanel.Layer_e.LAYER_BACKGROUND));
                cLevel.collisions_array.AddRange(panelEngine.GetTilemapArray(OglPanel.Layer_e.LAYER_COLLISIONS));
                cLevel.row_count = panelEngine.RowCount;
                cLevel.col_count = panelEngine.ColCount;

                this.tilesetBitmap = this.packTiles(false);

                string LevelFullPath = saveLevel.FileName;

                if (!LevelFullPath.EndsWith(".lwf"))
                    LevelFullPath += ".lwf";

                try
                {
                    using (var file = File.Create(LevelFullPath))
                    {
                        ProtoBuf.Serializer.Serialize<PBInterface.Level>(file, cLevel);
                    }

                    if (this.tilesetBitmap != null)
                    {
                        string tileFilePath = saveLevel.FileName.Substring(0, saveLevel.FileName.LastIndexOf("\\")) + "\\Tileset.png";

                        //save the associated tileset
                        this.tilesetBitmap.Save(tileFilePath, ImageFormat.Png);
                    }

                    MessageBox.Show(string.Format("Niveau {0} Sauvegardé ! ", openTileset.FileName), "Sauvegarde ok", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(string.Format("Erreur lors de l'enregistrement du Niveau {0} , Exception : {1} ", openTileset.FileName, ex.ToString()), "Sauvegarde ko", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void menuOpenLevel_Click(object sender, EventArgs e)
        {
            if (openLevel.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {

                PBInterface.Level LevelToLoad;

                using (var file = File.OpenRead(openLevel.FileName))
                {
                    LevelToLoad = ProtoBuf.Serializer.Deserialize<PBInterface.Level>(file);
                }

                string tileFilePath = openLevel.FileName.Substring(0, openLevel.FileName.LastIndexOf("\\")) + "\\Tileset.png";

                byte[] bitmapContent = null;
                short tilesetWidth = 0;
                short tilesetHeight = 0;

                //check if we have a tile set we can load
                if (File.Exists(tileFilePath))
                {
                    this.OpenTileSet(tileFilePath);
                    this.tilesetBitmap = this.packTiles(true);

                    if (this.tilesetBitmap != null)
                    {
                        bitmapContent = this.GetBitmapContent(tilesetBitmap);
                        tilesetWidth = Convert.ToInt16(tilesetBitmap.Width);
                        tilesetHeight = Convert.ToInt16(tilesetBitmap.Height);
                    }
                }

                //load the tilemap data
                panelEngine.CreateTilemapWithData(Convert.ToInt16(LevelToLoad.row_count), Convert.ToInt16(LevelToLoad.col_count), LevelToLoad.background_array.ToArray(), LevelToLoad.collisions_array.ToArray(), bitmapContent, tilesetWidth, tilesetHeight);


                menuSaveLevel.Enabled = true;
            }
        }

        private void btnEditLayer_Click(object sender, EventArgs e)
        {
            Button SelectedButton = sender as Button;
            string tag = SelectedButton.Tag.ToString();

            foreach (Button nselectBtn in layerArray)
            {
                if (nselectBtn.Tag != SelectedButton.Tag)
                {
                    nselectBtn.FlatStyle = FlatStyle.Standard;
                    nselectBtn.BackColor = Color.Transparent;
                }
                else
                {
                    nselectBtn.FlatStyle = FlatStyle.Flat;
                    nselectBtn.BackColor = Color.GreenYellow;
                    nselectBtn.Focus();
                }
            }


            panelEngine.CurrentLayer = (OglPanel.Layer_e)Enum.Parse(typeof(OglPanel.Layer_e), tag);


        }

        private void btn_change_collisionvalue_Click(object sender, EventArgs e)
        {
            Button SelectedButton = sender as Button;
            string tag = SelectedButton.Tag.ToString();

            panelEngine.CurrentCollisionVal = (OglPanel.Collisions_Values)Enum.Parse(typeof(OglPanel.Collisions_Values), tag);
        }

        private void chkLayer_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chkSelected = sender as CheckBox;
            string tag = chkSelected.Tag.ToString();

            panelEngine.ChangeLayerVisibility(chkSelected.Checked ? OglPanel.wbool.wtrue : OglPanel.wbool.wfalse, (OglPanel.Layer_e)Enum.Parse(typeof(OglPanel.Layer_e), tag));
        }

        private void numCollisionsAlpha_ValueChanged(object sender, EventArgs e)
        {
            NumericUpDown num_alpha = sender as NumericUpDown;

            float alpha_value = Convert.ToSingle(num_alpha.Value / 100);

            panelEngine.ChangeCollisionsColor(alpha_value);
        }

      

       




   

       

       



     


    }
}
