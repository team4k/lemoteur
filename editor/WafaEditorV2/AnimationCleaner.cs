﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PBInterface;

namespace WafaEditorV2
{
    public partial class AnimationCleaner : Form
    {
        OglPanel _ref_scene;

        int _max_frame;
        string _ref_layer_id;

        List<ContainerType> result;

        public AnimationCleaner(int max_tile, int max_frame, OglPanel ref_scene, string layer_id)
        {
            InitializeComponent();

            string[] types_anime = Enum.GetNames(typeof(GeneratorMovement));
            int[] types_anime_val = (int[])Enum.GetValues(typeof(GeneratorMovement));
            result = new List<ContainerType>(types_anime.Length);

            for (int i = 0; i < types_anime.Length; i++)
                result.Add(new ContainerType(types_anime[i], types_anime_val[i]));


            cmbTypeAnime.Items.AddRange(result.ToArray());
            cmbTypeAnime.DisplayMember = "Key";
            cmbTypeAnime.ValueMember = "Value";

            numFrameDebut.Maximum = max_frame;
            numFrameFin.Maximum = max_frame;
            _max_frame = max_frame;

            numStartTile.Maximum = max_tile;
            numEndTile.Maximum = max_tile;

            _ref_scene = ref_scene;
            _ref_layer_id = layer_id;
        }

        private void btnDebut_Click(object sender, EventArgs e)
        {
            this.Enabled = false;
            this.SendToBack();

            _ref_scene.OnPositionTileChanged += this._ref_scene_OnPositionTileChanged;
        }

        private void _ref_scene_OnPositionTileChanged(int newPos)
        {
            numStartTile.Value = newPos;
            _ref_scene.OnPositionTileChanged -= this._ref_scene_OnPositionTileChanged;
            this.Enabled = true;
            this.BringToFront();
        }

        private void btnFin_Click(object sender, EventArgs e)
        {
            this.Enabled = false;
            this.SendToBack();

            _ref_scene.OnPositionTileChanged += this._ref_scene_OnPositionTileChanged2;
        }


        private void _ref_scene_OnPositionTileChanged2(int newPos)
        {
            numEndTile.Value = newPos;
            _ref_scene.OnPositionTileChanged -= this._ref_scene_OnPositionTileChanged2;
            this.Enabled = true;
            this.BringToFront();
        }

        private void btnAnnuler_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnSupprimer_Click(object sender, EventArgs e)
        {
            int increment = 1;

            if (cmbTypeAnime.SelectedItem == null)
            {
                MessageBox.Show("Le type de mouvement est obligatoire !");
                return;
            }

            if (((ContainerType)cmbTypeAnime.SelectedItem).Value == (int)GeneratorMovement.LinearHorizontal)
            {

                if (numStartTile.Value > numEndTile.Value)
                    increment = -1;
            }
            else if (((ContainerType)cmbTypeAnime.SelectedItem).Value == (int)GeneratorMovement.LinearVertical)
            {
                increment = _ref_scene.ColCount;

                if (numStartTile.Value > numEndTile.Value)
                    increment = -_ref_scene.ColCount;
            }


            int stoptile = (int)numEndTile.Value + increment;

            int cFrame = (int)numFrameDebut.Value;

            int endFrame = (int)numFrameFin.Value;

            for (int i = (int)numStartTile.Value; i != stoptile; i += increment)
            {
                for (int j = cFrame-1; j < endFrame; j++)
                {
                    NativeFunc.editor_remove_animated_tile(i, _ref_layer_id, j);

                    if (_ref_scene.CurrentLayer.Frames[j].TileUpdates.Any(p => p.Index == i))
                    {
                        TileInfo oref = _ref_scene.CurrentLayer.Frames[j].TileUpdates.Find(p => p.Index == i);

                        _ref_scene.CurrentLayer.Frames[j].TileUpdates.Remove(oref);
                    }
                }
            }

            MessageBox.Show("Suppresion effectuée effectuée!");

            this.Close();
        }
    }
}
