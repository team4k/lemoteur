﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using PBInterface;

namespace WafaEditorV2
{
    public static class BitmapUtilities
    {
        private const short pixelSize = 4;

        public static byte[] GetBitmapContent(Bitmap tilesetBitmap)
        {
            int bmp_size = tilesetBitmap.Width * tilesetBitmap.Height * pixelSize;
            byte[] bitmapContent = new byte[bmp_size];

            int iBmp = 0;

            for (int y = 0; y < tilesetBitmap.Height; y++)
            {
                for (int x = 0; x < tilesetBitmap.Width; x++)
                {
                    bitmapContent[iBmp] = tilesetBitmap.GetPixel(x, y).R;
                    bitmapContent[iBmp + 1] = tilesetBitmap.GetPixel(x, y).G;
                    bitmapContent[iBmp + 2] = tilesetBitmap.GetPixel(x, y).B;
                    bitmapContent[iBmp + 3] = tilesetBitmap.GetPixel(x, y).A;
                    iBmp += pixelSize;
                }
            }

            return bitmapContent;
        }

        public static Bitmap GetFrameFromBitmap(Bitmap bmp, int StartX, int StartY, int width, int height)
        {
            Color[] colors = GetTilePixels(bmp, StartX, StartY, width, height);

            Bitmap frame = new Bitmap(width, height, PixelFormat.Format32bppArgb);

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    int arrayIndex = y * width + x;
                    Color c = colors[arrayIndex];
                    frame.SetPixel(x, y, c);
                }
            }

            return frame;
        }

        public static Bitmap GetBitmapCopy(Bitmap bmp)
        {
            Bitmap newbmp = new Bitmap(bmp.Width, bmp.Height, PixelFormat.Format32bppArgb);

            for (int y = 0; y < bmp.Height; y++)
            {
                for (int x = 0; x < bmp.Width; x++)
                {
                    Color c = bmp.GetPixel(x, y);
                    newbmp.SetPixel(x, y, c);
                }
            }

            return newbmp;
        }

        public static Color[] GetTilePixels(Bitmap bmp, int StartX, int StartY, int TILE_SIZE,int tile_height)
        {
            List<Color> colorList = new List<Color>(TILE_SIZE * tile_height);

            for (int bmpY = StartY; bmpY < StartY + tile_height; bmpY++)
            {
                for (int bmpX = StartX; bmpX < StartX + TILE_SIZE; bmpX++)
                {
                    colorList.Add(bmp.GetPixel(bmpX,bmpY));
                }
            }

            return colorList.ToArray();
        }


        public static Bitmap MergeBitmaps(Bitmap[] bitmapArray, int width, int height,int TILE_SIZE,int tile_height)
        {
            Bitmap MergedBitmap = new Bitmap(width, height);

            int GlobalX = 0;
            int GlobalY = 0;


            foreach (Bitmap bmp in bitmapArray)
            {
                int bmpX = 0;
                int bmpY = 0;

                while (bmpY < bmp.Height && bmpX < bmp.Width)
                {
                    Color[] tilepixels = BitmapUtilities.GetTilePixels(bmp, bmpX, bmpY, TILE_SIZE, tile_height);

                    bmpX += TILE_SIZE;

                    if (bmpX >= bmp.Width)
                    {
                        bmpY += tile_height;
                        bmpX = 0;
                    }

                    //copy pixels to merged bitmap
                    int copyLimit = GlobalX + TILE_SIZE;

                    for (int pixel = 0; pixel < tilepixels.Length; pixel++)
                    {
                        MergedBitmap.SetPixel(GlobalX, GlobalY, tilepixels[pixel]);
                        GlobalX++;

                        if (GlobalX >= copyLimit)
                        {
                            GlobalY++;
                            GlobalX = copyLimit - TILE_SIZE;
                        }
                    }

                    GlobalY -= tile_height;
                    GlobalX += TILE_SIZE;


                    if (GlobalX >= MergedBitmap.Width)
                    {
                        GlobalX = 0;
                        GlobalY += tile_height;

                    }

                }
            }

            return MergedBitmap;


        }



        public static Bitmap MergeBitmaps(Bitmap[] bitmapArray, int width, int height, List<AnimationDef> animation_def)
        {
            Bitmap MergedBitmap = new Bitmap(width,height);

            int GlobalX = 0;
            int GlobalY = 0;

            int bmp_pos = 0;

            int TILE_SIZE =  0;
            int tile_height = 0;

            foreach(Bitmap bmp in bitmapArray)
            {
                int bmpX = 0;
                int bmpY = 0;

                //goto new line 
                GlobalX = 0;
                GlobalY += tile_height;

                TILE_SIZE = animation_def[bmp_pos].Width;
                tile_height = animation_def[bmp_pos].Height;

                while (bmpY < bmp.Height && bmpX < bmp.Width)
                {
                   

                    Color[] tilepixels = BitmapUtilities.GetTilePixels(bmp, bmpX, bmpY, TILE_SIZE, tile_height);

                    bmpX += TILE_SIZE;

                    if (bmpX >= bmp.Width)
                    {
                        bmpY += tile_height;
                        bmpX = 0;
                    }

                    //copy pixels to merged bitmap
                    int copyLimit = GlobalX + TILE_SIZE;

                    for (int pixel = 0; pixel < tilepixels.Length; pixel++)
                    {
                        MergedBitmap.SetPixel(GlobalX, GlobalY, tilepixels[pixel]);
                        GlobalX++;

                        if (GlobalX >= copyLimit)
                        {
                            GlobalY++;
                            GlobalX = copyLimit - TILE_SIZE;
                        }
                    }

                    GlobalY -= tile_height;
                    GlobalX += TILE_SIZE;

                }

                bmp_pos++;
            }

            return MergedBitmap;
            

        }

        public static bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }


    }
}
