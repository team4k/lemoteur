﻿namespace WafaEditorV2
{
    partial class LightSourceDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAnnuler = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.numAlpha = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtidlight = new System.Windows.Forms.TextBox();
            this.light_id = new System.Windows.Forms.Label();
            this.btnColorLight = new System.Windows.Forms.Button();
            this.colorLight = new System.Windows.Forms.ColorDialog();
            this.numRadius = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.numRotationAngle = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.numOpenAngle = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbTypeLumi = new System.Windows.Forms.ComboBox();
            this.numConstant = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.numLinear = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.numQuadratic = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.numPowerFactor = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numAlpha)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRadius)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRotationAngle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numOpenAngle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numConstant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLinear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numQuadratic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPowerFactor)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAnnuler
            // 
            this.btnAnnuler.Location = new System.Drawing.Point(199, 478);
            this.btnAnnuler.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAnnuler.Name = "btnAnnuler";
            this.btnAnnuler.Size = new System.Drawing.Size(100, 28);
            this.btnAnnuler.TabIndex = 21;
            this.btnAnnuler.Text = "Annuler";
            this.btnAnnuler.UseVisualStyleBackColor = true;
            this.btnAnnuler.Click += new System.EventHandler(this.btnAnnuler_Click);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(91, 478);
            this.btnOK.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(100, 28);
            this.btnOK.TabIndex = 20;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // numAlpha
            // 
            this.numAlpha.Location = new System.Drawing.Point(172, 213);
            this.numAlpha.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.numAlpha.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numAlpha.Name = "numAlpha";
            this.numAlpha.Size = new System.Drawing.Size(64, 23);
            this.numAlpha.TabIndex = 19;
            this.numAlpha.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(20, 215);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(124, 17);
            this.label6.TabIndex = 18;
            this.label6.Text = "Intensité sur sprite";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(57, 170);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 17);
            this.label1.TabIndex = 16;
            this.label1.Text = "Couleur";
            // 
            // txtidlight
            // 
            this.txtidlight.Location = new System.Drawing.Point(148, 31);
            this.txtidlight.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtidlight.Name = "txtidlight";
            this.txtidlight.Size = new System.Drawing.Size(224, 23);
            this.txtidlight.TabIndex = 15;
            // 
            // light_id
            // 
            this.light_id.AutoSize = true;
            this.light_id.Location = new System.Drawing.Point(57, 34);
            this.light_id.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.light_id.Name = "light_id";
            this.light_id.Size = new System.Drawing.Size(69, 17);
            this.light_id.TabIndex = 14;
            this.light_id.Text = "Id lumière";
            // 
            // btnColorLight
            // 
            this.btnColorLight.BackColor = System.Drawing.Color.White;
            this.btnColorLight.Location = new System.Drawing.Point(172, 170);
            this.btnColorLight.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnColorLight.Name = "btnColorLight";
            this.btnColorLight.Size = new System.Drawing.Size(29, 28);
            this.btnColorLight.TabIndex = 25;
            this.btnColorLight.UseVisualStyleBackColor = false;
            this.btnColorLight.Click += new System.EventHandler(this.btnColorLight_Click);
            // 
            // colorLight
            // 
            this.colorLight.Color = System.Drawing.Color.White;
            // 
            // numRadius
            // 
            this.numRadius.Location = new System.Drawing.Point(169, 303);
            this.numRadius.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.numRadius.Maximum = new decimal(new int[] {
            500000,
            0,
            0,
            0});
            this.numRadius.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numRadius.Name = "numRadius";
            this.numRadius.Size = new System.Drawing.Size(64, 23);
            this.numRadius.TabIndex = 27;
            this.numRadius.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(55, 305);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 17);
            this.label2.TabIndex = 26;
            this.label2.Text = "Taille rayon";
            // 
            // numRotationAngle
            // 
            this.numRotationAngle.Location = new System.Drawing.Point(172, 102);
            this.numRotationAngle.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.numRotationAngle.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.numRotationAngle.Name = "numRotationAngle";
            this.numRotationAngle.Size = new System.Drawing.Size(64, 23);
            this.numRotationAngle.TabIndex = 29;
            this.numRotationAngle.Value = new decimal(new int[] {
            135,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(57, 105);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 17);
            this.label3.TabIndex = 28;
            this.label3.Text = "Rotation spot";
            // 
            // numOpenAngle
            // 
            this.numOpenAngle.Location = new System.Drawing.Point(172, 134);
            this.numOpenAngle.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.numOpenAngle.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.numOpenAngle.Name = "numOpenAngle";
            this.numOpenAngle.Size = new System.Drawing.Size(64, 23);
            this.numOpenAngle.TabIndex = 31;
            this.numOpenAngle.Value = new decimal(new int[] {
            210,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(57, 137);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 17);
            this.label4.TabIndex = 30;
            this.label4.Text = "Angle Spot";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(59, 70);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 17);
            this.label5.TabIndex = 32;
            this.label5.Text = "type lumière";
            // 
            // cmbTypeLumi
            // 
            this.cmbTypeLumi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTypeLumi.FormattingEnabled = true;
            this.cmbTypeLumi.Location = new System.Drawing.Point(148, 66);
            this.cmbTypeLumi.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbTypeLumi.Name = "cmbTypeLumi";
            this.cmbTypeLumi.Size = new System.Drawing.Size(224, 25);
            this.cmbTypeLumi.TabIndex = 33;
            this.cmbTypeLumi.SelectedIndexChanged += new System.EventHandler(this.cmbTypeLumi_SelectedIndexChanged);
            // 
            // numConstant
            // 
            this.numConstant.DecimalPlaces = 4;
            this.numConstant.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numConstant.Location = new System.Drawing.Point(169, 346);
            this.numConstant.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.numConstant.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.numConstant.Name = "numConstant";
            this.numConstant.Size = new System.Drawing.Size(91, 23);
            this.numConstant.TabIndex = 35;
            this.numConstant.Value = new decimal(new int[] {
            4,
            0,
            0,
            65536});
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 348);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(146, 17);
            this.label7.TabIndex = 34;
            this.label7.Text = "Atténuation constante";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // numLinear
            // 
            this.numLinear.DecimalPlaces = 4;
            this.numLinear.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numLinear.Location = new System.Drawing.Point(169, 384);
            this.numLinear.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.numLinear.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.numLinear.Name = "numLinear";
            this.numLinear.Size = new System.Drawing.Size(91, 23);
            this.numLinear.TabIndex = 37;
            this.numLinear.Value = new decimal(new int[] {
            30,
            0,
            0,
            65536});
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 386);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(130, 17);
            this.label8.TabIndex = 36;
            this.label8.Text = "Atténuation linéaire";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // numQuadratic
            // 
            this.numQuadratic.DecimalPlaces = 4;
            this.numQuadratic.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numQuadratic.Location = new System.Drawing.Point(169, 421);
            this.numQuadratic.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.numQuadratic.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.numQuadratic.Name = "numQuadratic";
            this.numQuadratic.Size = new System.Drawing.Size(91, 23);
            this.numQuadratic.TabIndex = 39;
            this.numQuadratic.Value = new decimal(new int[] {
            200,
            0,
            0,
            65536});
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(5, 423);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(160, 17);
            this.label9.TabIndex = 38;
            this.label9.Text = "Atténuation quadratique";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // numPowerFactor
            // 
            this.numPowerFactor.DecimalPlaces = 4;
            this.numPowerFactor.Location = new System.Drawing.Point(172, 261);
            this.numPowerFactor.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.numPowerFactor.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numPowerFactor.Name = "numPowerFactor";
            this.numPowerFactor.Size = new System.Drawing.Size(88, 23);
            this.numPowerFactor.TabIndex = 41;
            this.numPowerFactor.Value = new decimal(new int[] {
            10,
            0,
            0,
            65536});
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(20, 263);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(135, 17);
            this.label10.TabIndex = 40;
            this.label10.Text = "Puissance éclairage";
            // 
            // LightSourceDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(593, 646);
            this.Controls.Add(this.numPowerFactor);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.numQuadratic);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.numLinear);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.numConstant);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cmbTypeLumi);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.numOpenAngle);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.numRotationAngle);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.numRadius);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnColorLight);
            this.Controls.Add(this.btnAnnuler);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.numAlpha);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtidlight);
            this.Controls.Add(this.light_id);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LightSourceDialog";
            this.Text = "Ajouter une lumière";
            ((System.ComponentModel.ISupportInitialize)(this.numAlpha)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRadius)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRotationAngle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numOpenAngle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numConstant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLinear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numQuadratic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPowerFactor)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAnnuler;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.NumericUpDown numAlpha;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtidlight;
        private System.Windows.Forms.Label light_id;
        private System.Windows.Forms.Button btnColorLight;
        private System.Windows.Forms.ColorDialog colorLight;
        private System.Windows.Forms.NumericUpDown numRadius;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numRotationAngle;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numOpenAngle;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbTypeLumi;
        private System.Windows.Forms.NumericUpDown numConstant;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown numLinear;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown numQuadratic;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown numPowerFactor;
        private System.Windows.Forms.Label label10;
    }
}