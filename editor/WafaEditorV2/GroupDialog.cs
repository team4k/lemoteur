﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PBInterface;

namespace WafaEditorV2
{
    public partial class GroupDialog : Form
    {
        List<string> group_list = new List<string>();
        Layer ref_layer;

        public GroupDialog(Layer clayer)
        {
            InitializeComponent();
            ref_layer = clayer;

            group_list.AddRange((from u in clayer.GroupArrays select u.GroupId));

            cmbGroupe.Items.AddRange(group_list.ToArray());

            this.DialogResult = System.Windows.Forms.DialogResult.None;
        }

        private void btnAnnuler_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void GroupDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(cmbGroupe.Text))
            {
                MessageBox.Show("Veuillez préciser un nom de groupe!", "nom de groupe", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            GroupEntity  cgroup = ref_layer.GroupArrays.Find(p => p.GroupId == cmbGroupe.Text);

            if (cgroup == null)
            {
                cgroup = new GroupEntity ();
                cgroup.GroupId = cmbGroupe.Text;
                ref_layer.GroupArrays.Add(cgroup);
            }

            this.Tag = cgroup;

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }
    }
}
