﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using WafaEditorV2.Properties;
using PBInterface;
using System.IO;

namespace WafaEditorV2
{

    public class OglPanel : Panel
    {
       


        public static bool _init = false; 
        private float zoomFactor = 0.5f;

        public float ZoomFactor
        {
            get
            {
                return zoomFactor;
            }
            set
            {
                zoomFactor = value;
            }
        }

        private bool _mouseDowned = false;

        private bool _mouseDrag = false;

        private int _ColCount;

        public int ColCount
        {
            get
            {
                return _ColCount;
            }
            set
            {
                _ColCount = value;
            }
        }

        private int _RowCount;


        public int RowCount
        {
            get
            {
                return _RowCount;
            }
            set
            {
                _RowCount = value;
            }
        }

        private Layer currentLayer = null;

        public Layer CurrentLayer
        {
            get
            {
                return currentLayer;
            }
            set
            {
                currentLayer = value;
            }
        }

        private Map currentMap = null;

        public Map CurrentMap
        {
            get
            {
                return currentMap;
            }
            set
            {
                currentMap = value;
            }
        }

        private StackHistory _ref_history = null;

        public StackHistory ref_history
        {
            set
            {
                _ref_history = value;
            }
        }


        private int[] _tileSelected = new int[1]{0};

        public int[] TileSelected
        {
            get
            {
                return _tileSelected;
            }
            set
            {
                _tileSelected = value;
            }
        }

        public Tools _toolSelected = Tools.Pencil;

        public Tools ToolSelected
        {
            get
            {
                return _toolSelected;
            }
            set
            {
                _toolSelected = value;
            }
        }


        private int currentCollisionVal = (int)NativeFunc.Collisions_Values.COLLISION;

        public int CurrentCollisionVal
        {
            get
            {
                return currentCollisionVal;
            }
            set
            {
                currentCollisionVal = value;
            }
        }

        private Wcolor _currentWorldColor = new Wcolor();

        public Wcolor CurrentWorldColor
        {
            get
            {
                return _currentWorldColor;
            }
            set
            {
                _currentWorldColor = value;
            }
        }

        public static GlobalMode current_mode = GlobalMode.Editor;

        public delegate void SelectedTileChanged();

        public event SelectedTileChanged OnSelectedTileChanged;

        public delegate void PositionTileChanged(int newPos);

        public event PositionTileChanged OnPositionTileChanged;

        public delegate void SelectedItemChanged(string id, NativeFunc.GAME_TYPE? type);

        public event SelectedItemChanged OnSelectedItemChanged;

        public OglPanel() : base()
        {
            //this.SetStyle(ControlStyles.EnableNotifyMessage, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.DoubleBuffer, false);
            this.SetStyle(ControlStyles.Opaque, true);
	        this.SetStyle(ControlStyles.ResizeRedraw, true);
	        this.SetStyle(ControlStyles.UserPaint, true);
        }



        public void StartEngine(int player_start_x,int player_start_y,int startX,int startY)
        {
            if (!_init)
            {
                if (NativeFunc.editor_start_engine(this.Handle, (short)this.Width, (short)this.Height, player_start_x, player_start_y, startX, startY, Settings.Default.assets_path, Settings.Default.gamedll_path, Settings.Default.font, Settings.Default.light_prog, Settings.Default.shadow_prog, Settings.Default.mask_prog, Settings.Default.raw_assets_path) == NativeFunc.wbool.wfalse)
                {
                    MessageBox.Show("Erreur de chargement du moteur, les erreurs ont été enregistrées dans le fichier wafa.log, problème possible, différence entre la version de la game dll et la version du moteur", "Erreur chargement", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }


                NativeFunc.editor_set_tilesize(Settings.Default.TILE_SIZE);

                //ressources par défaut

                if (NativeFunc.editor_resx_exist("sound_high_icon_32.png") == NativeFunc.wbool.wfalse)
                {
                    byte[] sound_icon_content = BitmapUtilities.GetBitmapContent(Resources.sound_high_icon_32);
                    NativeFunc.editor_addtextureresx("sound_high_icon_32.png", 32, 32, sound_icon_content, (uint)sound_icon_content.Length);
                }

                if (NativeFunc.editor_resx_exist("lightbulb_icon_32.png") == NativeFunc.wbool.wfalse)
                {
                    byte[] light_icon_content = BitmapUtilities.GetBitmapContent(Resources.lightbulb_icon_32);
                    NativeFunc.editor_addtextureresx("lightbulb_icon_32.png", 32, 32, light_icon_content, (uint)light_icon_content.Length);
                }

                if (NativeFunc.editor_resx_exist("star_icon_32.png") == NativeFunc.wbool.wfalse)
                {
                    byte[] star_icon_content = BitmapUtilities.GetBitmapContent(Resources.star_icon_32);
                    NativeFunc.editor_addtextureresx("star_icon_32.png", 32, 32, star_icon_content, (uint)star_icon_content.Length);
                }

                if (NativeFunc.editor_resx_exist("waypoint.png") == NativeFunc.wbool.wfalse)
                {
                    byte[] waypoint_icon_content = BitmapUtilities.GetBitmapContent(Resources.waypoint_32);
                    NativeFunc.editor_addtextureresx("waypoint.png", 32, 32, waypoint_icon_content, (uint)waypoint_icon_content.Length);
                }

                if (NativeFunc.editor_resx_exist("collider.png") == NativeFunc.wbool.wfalse)
                {
                    byte[] collider_content = BitmapUtilities.GetBitmapContent(Resources.collider_32);
                    NativeFunc.editor_addtextureresx("collider.png", 32, 32, collider_content, (uint)collider_content.Length);
                }

                if (NativeFunc.editor_resx_exist("default_text.png") == NativeFunc.wbool.wfalse)
                {
                    byte[] content3 = BitmapUtilities.GetBitmapContent(Resources.default_text);

                    NativeFunc.editor_addtextureresx("default_text.png", Resources.default_text.Width, Resources.default_text.Height, content3, (uint)content3.Length);
                }

                if(NativeFunc.editor_resx_exist("default_tileset.png") == NativeFunc.wbool.wfalse)
                {
                    byte[] content_tileset = BitmapUtilities.GetBitmapContent(Resources.default_tileset);

                    NativeFunc.editor_addtextureresx("default_tileset.png", Resources.default_tileset.Width, Resources.default_tileset.Height, content_tileset, (uint)content_tileset.Length);
                }

                _init = true;
                NativeFunc.editor_setzoom(this.Width, this.Height, this.zoomFactor);
                NativeFunc.editor_change_color((float)Settings.Default.background_color.R / 255, (float)Settings.Default.background_color.G / 255, (float)Settings.Default.background_color.B / 255, (float)Settings.Default.background_color.A / 255);
                NativeFunc.editor_grid_visibility((Settings.Default.show_grid) ? NativeFunc.wbool.wtrue : NativeFunc.wbool.wfalse, (float)Settings.Default.grid_color.R / 255, (float)Settings.Default.grid_color.G / 255, (float)Settings.Default.grid_color.B / 255, (float)Settings.Default.grid_color.A / 255);

                NativeFunc.editor_set_selectioncolor((float)Settings.Default.selector_color.R / 255, (float)Settings.Default.selector_color.G / 255, (float)Settings.Default.selector_color.B / 255, (float)Settings.Default.selector_color.A / 255);

                NativeFunc.editor_setcursor_color((float)Settings.Default.cursor_color.R / 255, (float)Settings.Default.cursor_color.G / 255, (float)Settings.Default.cursor_color.B / 255, 0.5f);

                NativeFunc.editor_setfps_color((float)Settings.Default.colorFPS.R / 255, (float)Settings.Default.colorFPS.G / 255, (float)Settings.Default.colorFPS.B / 255, 1.0f);

                NativeFunc.editor_draw_debug_collisions((Settings.Default.draw_debug_collisions) ? NativeFunc.wbool.wtrue : NativeFunc.wbool.wfalse);

                NativeFunc.editor_draw_colliders_triggers_misc((Settings.Default.draw_colliders_trigger_misc) ? NativeFunc.wbool.wtrue : NativeFunc.wbool.wfalse);

                NativeFunc.editor_setsoundvolume(Settings.Default.music_volume, Settings.Default.sfx_volume);

                NativeFunc.editor_set_ambientcolor((float)Settings.Default.ambient_color.R / 255, (float)Settings.Default.ambient_color.G / 255, (float)Settings.Default.ambient_color.B / 255, (float)Settings.Default.ambient_alpha / 255);

                NativeFunc.editor_setenable_lighting((Settings.Default.enable_lighting) ? NativeFunc.wbool.wtrue : NativeFunc.wbool.wfalse);
            }
            else
            {

            }
        }

        public void RaiseSelectItemChangedEvent(string id,NativeFunc.GAME_TYPE type)
        {
            if (OnSelectedItemChanged != null)
                OnSelectedItemChanged(id, type);
        }

        public void CreateTilemap(Map oMap)
        {

            this.Invalidate();

            _RowCount = oMap.RowCount;
            _ColCount = oMap.ColCount;

            MemoryStream mapStream = new MemoryStream();
            mapStream.Position = 0;
            ProtoBuf.Serializer.Serialize<Map>(mapStream, oMap);

            mapStream.Position = 0;

            byte[] tmpmapArray = mapStream.ToArray();

            mapStream.Close();

            //create tileset
            NativeFunc.editor_create_tilemap(Settings.Default.TILE_SIZE, Settings.Default.TILE_SIZE, tmpmapArray, (uint)tmpmapArray.Length);

        }


        public void CleanTilemap()
        {
            NativeFunc.editor_clean_tilemap();
        }


        public int[] GetTilemapArray()
        {
            return this.GetTilemapArray(currentLayer,currentMap); 
        }

        public int[] GetTilemapArray(Layer oLayer,Map oMap)
        {
            int[] arrayData = new int[oMap.RowCount * oMap.ColCount];

            if (oLayer.Type == (int)TypeLayer.Collisions)
                NativeFunc.editor_getcollisions_array(arrayData, oMap.MapId);
            else
                NativeFunc.editor_get_tilearray(arrayData, oLayer.LayerId, oMap.MapId);

            return Array.ConvertAll<int, int>(arrayData, p => (int)p);
        }

        public void UpdateTilemap(int[] tilemapArray)
        {
             int[] arrayData = Array.ConvertAll<int, int>(tilemapArray, p => (int)p);

             if (currentLayer.Type == (int)TypeLayer.Collisions)
                 NativeFunc.editor_update_collisions_data(arrayData);
             else
                 NativeFunc.editor_update_tilemap(arrayData, currentLayer.LayerId);
        }

        public void UpdateTilemap(int[] tilemapArray,Layer oLayer)
        {
            int[] arrayData = Array.ConvertAll<int, int>(tilemapArray, p => (int)p);

            if (oLayer.Type == (int)TypeLayer.Collisions)
                NativeFunc.editor_update_collisions_data(arrayData);
            else
                NativeFunc.editor_update_tilemap(arrayData, oLayer.LayerId);
        }

        public void UpdateTexture()
        {
            NativeFunc.editor_Tilemap_TextureChanged();
        }

        public void StopOgl()
        {
            NativeFunc.editor_shutdown_engine();
        }


        public void ChangeLayerVisibility(NativeFunc.wbool visible, Layer layer_object)
        {
            layer_object.Visible = (visible == NativeFunc.wbool.wtrue);

            if (layer_object.Type == (int)TypeLayer.Collisions)
                NativeFunc.editor_changecollisionsvisibility(visible);
            else if (layer_object.Type == (int)TypeLayer.Normal)
                NativeFunc.editor_changelayer_visibility(visible, layer_object.LayerId);
            else if (layer_object.Type == (int)TypeLayer.Objects)
            {

                //show / hide all entities
                foreach (Entity oEntity in layer_object.EntityArrays)
                    NativeFunc.editor_entity_set_visible(oEntity.EntityId, visible, NativeFunc.GAME_TYPE.GT_ENTITY);

                //show / hide all trigger
                foreach (Trigger oTrigger in layer_object.TriggerArrays)
                    NativeFunc.editor_entity_set_visible(oTrigger.TriggerId, visible, NativeFunc.GAME_TYPE.GT_TRIGGER);

                //show / hide all texts
                foreach (TextObject oText in layer_object.TextArrays)
                    NativeFunc.editor_entity_set_visible(oText.TextId, visible, NativeFunc.GAME_TYPE.GT_TEXT);

                foreach(LightSource olight in layer_object.LightArrays)
                    NativeFunc.editor_entity_set_visible(olight.LightId, visible, NativeFunc.GAME_TYPE.GT_LIGHT);

                foreach (ParticlesData opart in layer_object.ParticlesArrays)
                    NativeFunc.editor_entity_set_visible(opart.Id, visible, NativeFunc.GAME_TYPE.GT_PARTICLES);

                foreach (Waypoint oway in layer_object.Waypoints)
                    NativeFunc.editor_entity_set_visible(oway.Id, visible, NativeFunc.GAME_TYPE.GT_WAYPOINT);

                foreach (Collider ocol in layer_object.Colliders)
                    NativeFunc.editor_entity_set_visible(ocol.Id, visible, NativeFunc.GAME_TYPE.GT_COLLIDER);


            }
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            //update screen size
            if(_init && Settings.Default.res_dynamique)
                NativeFunc.editor_resize_screen(this.Width, this.Height);


            base.OnSizeChanged(e);
        }


        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (!_init)
                return;

            if(currentLayer == null)
                return;

            float scrollX = NativeFunc.editor_getscrollX();
            float scrollY = NativeFunc.editor_getscrollY();

            //constraint the cursor pos to the tile grid
            int col_pos = 0;
            int row_pos = 0;


            if (e.X > 0)
                col_pos = (int)Math.Floor((double)((e.X * this.zoomFactor) - (scrollX * this.zoomFactor)) / Settings.Default.TILE_SIZE);

            int Y = this.Height - (int)(e.Y + scrollY);

            if (e.Y > 0)
                row_pos = (int)Math.Floor((double)(Y * this.zoomFactor)  / Settings.Default.TILE_SIZE) ;

            NativeFunc.wbool extend_selection = NativeFunc.wbool.wfalse;

            if (_tileSelected.Length == 1)
            {
                if (e.Button == System.Windows.Forms.MouseButtons.Right && CurrentLayer.Type == (int)TypeLayer.Normal)
                    extend_selection = (_mouseDowned) ? NativeFunc.wbool.wtrue : NativeFunc.wbool.wfalse;

                NativeFunc.editor_setcursor_pos(col_pos * Settings.Default.TILE_SIZE, row_pos * Settings.Default.TILE_SIZE, extend_selection);
            }
            else
            {
                //move tile selection
                NativeFunc.editor_setcursor_pos(col_pos * Settings.Default.TILE_SIZE, row_pos * Settings.Default.TILE_SIZE, NativeFunc.wbool.wfalse,NativeFunc.wbool.wtrue);
            }
           
            int worldposX,worldposY;
            this.GetWorldPos(e.X,e.Y,out worldposX,out worldposY);

           if(current_mode == GlobalMode.Play)
                NativeFunc.editor_settargetpos(worldposX, worldposY);

            if (_mouseDowned)
            {
                if (current_mode == GlobalMode.Editor)
                    NativeFunc.editor_settargetpos(worldposX, worldposY);

                _mouseDrag = true;

                if(e.Button ==  System.Windows.Forms.MouseButtons.Left)
                    this.OnMouseDown(e);
            }

            this.Invalidate();

            base.OnMouseMove(e);
        }

        protected override void OnMouseWheel(MouseEventArgs e)
        {
            NativeFunc.editor_wheelpinch(Convert.ToSingle(e.Delta));

            base.OnMouseWheel(e);
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            int worldPosX = 0;
            int worldPosY = 0;

            this.GetWorldPos(e.X, e.Y, out worldPosX, out worldPosY);

            NativeFunc.editor_endclick((float)worldPosX, (float)worldPosY, (e.Button == System.Windows.Forms.MouseButtons.Left) ? 0 : 1);

            if (_mouseDrag && e.Button == System.Windows.Forms.MouseButtons.Right && currentLayer.Type == (int)TypeLayer.Normal)
            {
                //get all tile under our selector
                _tileSelected = new int[NativeFunc.editor_get_num_multi_tiles()];

                NativeFunc.editor_get_multi_tiles(_tileSelected, currentLayer.LayerId);
            }

            _mouseDowned = false;
            _mouseDrag = false;

            base.OnMouseUp(e);
        }


        public int GetTilePos(int x, int y,out int row_pos, out int col_pos)
        {
            float scrollX = NativeFunc.editor_getscrollX();
            float scrollY = NativeFunc.editor_getscrollY();

            //constraint the cursor pos to the tile grid
            col_pos = 0;
            row_pos = 0;

            if (x > 0)
                col_pos = (int)Math.Floor((double)((x * this.zoomFactor) - (scrollX * this.zoomFactor)) / Settings.Default.TILE_SIZE);

            int Y = this.Height - (int)(y + scrollY);

            if (y > 0)
                row_pos = (int)Math.Floor((double)(Y * this.zoomFactor) / Settings.Default.TILE_SIZE);

            return (row_pos * currentMap.ColCount) + col_pos;
        }

        public void GetWorldPos(int x, int y, out int worldposX, out int worldposY,bool snap_to_grid = false)
        {
            worldposX = 0;
            worldposY = 0;

            float scrollX = NativeFunc.editor_getscrollX();
            float scrollY = NativeFunc.editor_getscrollY();

            if (!snap_to_grid)
            {
                if (x > 0)
                    worldposX = (int)((x * this.zoomFactor) - (scrollX * this.zoomFactor));
            }
            else
            {
                if (x > 0)
                    worldposX = (int)Math.Floor((double)((x * this.zoomFactor) - (scrollX * this.zoomFactor)) / Settings.Default.TILE_SIZE);

                worldposX *= Settings.Default.TILE_SIZE;
            }


            int Y = this.Height - (int)(y + scrollY);

            if (!snap_to_grid)
            {
                if (y > 0)
                    worldposY = (int)(Y * this.zoomFactor);
            }
            else
            {
                if (y > 0)
                    worldposY = (int)Math.Floor((double)(Y * this.zoomFactor) / Settings.Default.TILE_SIZE);

                worldposY *= Settings.Default.TILE_SIZE;
            }

        }

        


        protected override void OnMouseDown(MouseEventArgs e)
        {
            if (!_init)
                return;

            if (currentLayer == null || currentMap == null)
                return;

            //get cursor pos
            int col_pos = 0;
            int row_pos = 0;

            int worldPosX = 0;
            int worldPosY = 0;

            float scrollX = NativeFunc.editor_getscrollX();
            float scrollY = NativeFunc.editor_getscrollY();

            if (e.X > 0)
            {
                col_pos = (int)Math.Floor((double)((e.X * this.zoomFactor) - (scrollX * this.zoomFactor)) / Settings.Default.TILE_SIZE);

                worldPosX = (int)((e.X * this.zoomFactor) - (scrollX * this.zoomFactor));
            }


            int Y = this.Height - (int)(e.Y + scrollY);

            if (e.Y > 0)
            {
                row_pos = (int)Math.Floor((double)(Y* this.zoomFactor) / Settings.Default.TILE_SIZE);

                worldPosY = (int)(Y * this.zoomFactor);
            }


            int tilePos = row_pos * _ColCount + col_pos;

            bool outOfMap = (col_pos < 0 || row_pos < 0 || col_pos > _ColCount -1 || row_pos > _RowCount -1);

            if (current_mode == GlobalMode.WorldEditor)
            {
                if (e.Button == System.Windows.Forms.MouseButtons.Left)//edit the current layer
                {
                    if (OnPositionTileChanged != null && !outOfMap)
                        OnPositionTileChanged(tilePos);

                    //récupération de la couleur en fonction de la valeur



                    if (!outOfMap)
                    {
                        if (ToolSelected == Tools.Pencil)
                            NativeFunc.editor_change_collision_color((int)currentCollisionVal, tilePos, (CurrentWorldColor.R != 0) ? CurrentWorldColor.R / 255.0f : 0.0f, (CurrentWorldColor.G != 0) ? CurrentWorldColor.G / 255.0f : 0.0f, (CurrentWorldColor.B != 0) ? CurrentWorldColor.B / 255.0f : 0.0f, (CurrentWorldColor.A != 0) ? CurrentWorldColor.A / 255.0f : 0.0f);
                        else if (ToolSelected == Tools.FloodFill)
                        {
                            int[] tile = new int[1];
                            NativeFunc.editor_getcollision(tile, tilePos);

                            this.floodTileFill(row_pos, col_pos, tile[0], (int)currentCollisionVal, currentLayer.LayerId, true);
                        }
                    }
                }
                else if (e.Button == System.Windows.Forms.MouseButtons.Right)//select the current tile under the cursor
                {
                    NativeFunc.editor_setcursor_pos(col_pos * Settings.Default.TILE_SIZE, row_pos * Settings.Default.TILE_SIZE);


                    _tileSelected = new int[1];

                    int[] tile = new int[1];

                    NativeFunc.editor_getcollision(tile, tilePos);

                    currentCollisionVal = tile[0];


                    if (OnSelectedTileChanged != null)
                        OnSelectedTileChanged();

                }
            }
            else if (current_mode == GlobalMode.Editor || current_mode == GlobalMode.FragEditor)
            {

                if (e.Button == System.Windows.Forms.MouseButtons.Left)//edit the current layer
                {
                    if (OnPositionTileChanged != null && !outOfMap)
                        OnPositionTileChanged(tilePos);


                    if (currentLayer.Type == (int)TypeLayer.Collisions && !outOfMap)
                    {
                        if (ToolSelected == Tools.Pencil)
                            NativeFunc.editor_change_collision((int)currentCollisionVal, tilePos);
                        else if (ToolSelected == Tools.FloodFill)
                        {
                            int[] tile = new int[1];
                            NativeFunc.editor_getcollision(tile, tilePos);

                            this.floodTileFill(row_pos, col_pos, tile[0], (int)currentCollisionVal, currentLayer.LayerId, true);
                        }
                    }
                    else if (currentLayer.Type == (int)TypeLayer.Normal && !outOfMap)
                    {
                        if (ToolSelected == Tools.Pencil)
                        {
                            if (currentLayer.EditorCurrentFrame == 0)
                            {
                                if (TileSelected.Length == 1)
                                {
                                    int[] tile = new int[1];
                                    NativeFunc.editor_get_tile(tile, tilePos, currentLayer.LayerId);

                                    if (tile[0] != TileSelected[0])
                                    {

                                        object[] history_values = new object[] { TileSelected[0], tilePos, currentLayer.LayerId };

                                        change_tile_delegate change_tile_func = NativeFunc.editor_change_tile;

                                        HistoryEntry entry = new HistoryEntry(history_values, tile[0], HistoryAction.SetTile, change_tile_func);
                                        _ref_history.Push(entry);

                                        NativeFunc.editor_change_tile(TileSelected[0], tilePos, currentLayer.LayerId);
                                    }
                                }
                                else //paste tile buffer
                                {
                                    int[] old_tiles = new int[NativeFunc.editor_get_num_multi_tiles()];

                                    NativeFunc.editor_get_multi_tiles(old_tiles, currentLayer.LayerId);

                                    int[] rect_values = new int[4];

                                    NativeFunc.editor_get_select_rectangle(rect_values);

                                    object[] history_values = new object[] { TileSelected, tilePos, rect_values, currentLayer.LayerId };

                                    change_multi_tile_delegate change_tile_func = NativeFunc.editor_change_multi_tile2;

                                    HistoryEntry entry = new HistoryEntry(history_values, old_tiles, HistoryAction.SetMultiTile, change_tile_func);
                                    _ref_history.Push(entry);

                                    NativeFunc.editor_change_multi_tile(TileSelected, tilePos, currentLayer.LayerId);
                                    _tileSelected = new int[1];

                                    NativeFunc.editor_setcursor_pos(col_pos * Settings.Default.TILE_SIZE, row_pos * Settings.Default.TILE_SIZE);
                                }
                            }
                        }
                        else if (ToolSelected == Tools.FloodFill)
                        {
                            int[] tile = new int[1];
                            NativeFunc.editor_get_tile(tile, tilePos, currentLayer.LayerId);

                            int[] changed_tiles = this.floodTileFill(row_pos, col_pos, tile[0], TileSelected[0], currentLayer.LayerId);

                            object[] hist_value = new object[] { TileSelected, changed_tiles, currentLayer.LayerId };
                            change_tile_delegate func = NativeFunc.editor_change_tile;

                            HistoryEntry entry = new HistoryEntry(hist_value, tile[0], HistoryAction.FloodTile, func);
                            _ref_history.Push(entry);
                        }


                    }
                    else if (currentLayer.Type == (int)TypeLayer.Objects)
                    {
                        if (ToolSelected == Tools.Pencil) //select npc
                        {
                            //select objects in this order : 
                            // 1 - entity
                            // 2 - trigger
                            // 3 - text object 
                            // 4 - sound object
                            // 5 - light
                            // 6 - particle
                            // 7 - waypoint
                            // 8 - collider



                            string item_id = string.Empty;
                            NativeFunc.GAME_TYPE? item_type = null;

                            NativeFunc.wbool add_select = NativeFunc.wbool.wfalse;

                            if (Control.ModifierKeys.HasFlag(Keys.Control))
                                add_select = NativeFunc.wbool.wtrue;

                            NativeFunc.editor_selectentity_pos(worldPosX, worldPosY, currentLayer.Order, add_select);

                            if (!_mouseDrag && NativeFunc.editor_get_num_selection() == 1)
                            {
                                StringBuilder entity_id_buffer = new StringBuilder(1000);

                                NativeFunc.editor_entity_get_selection(entity_id_buffer, 0,entity_id_buffer.Capacity);


                                Entity ent_data = CurrentLayer.EntityArrays.Find(p => p.EntityId == entity_id_buffer.ToString());

                                if (ent_data != null)
                                {
                                    item_id = ent_data.EntityId;
                                    item_type = NativeFunc.GAME_TYPE.GT_ENTITY;
                                }


                                Trigger trig_data = CurrentLayer.TriggerArrays.Find(p => p.TriggerId == entity_id_buffer.ToString());

                                if (trig_data != null)
                                {
                                    item_id = trig_data.TriggerId;
                                    item_type = NativeFunc.GAME_TYPE.GT_TRIGGER;
                                }

                                TextObject obj_data = CurrentLayer.TextArrays.Find(p => p.TextId == entity_id_buffer.ToString());

                                if (obj_data != null)
                                {
                                    item_id = obj_data.TextId;
                                    item_type = NativeFunc.GAME_TYPE.GT_TEXT;
                                }

                                SoundSource snd_data = CurrentLayer.SoundArrays.Find(p => p.SoundId == entity_id_buffer.ToString());

                                if (snd_data != null)
                                {
                                    item_id = snd_data.SoundId;
                                    item_type = NativeFunc.GAME_TYPE.GT_SOUND;
                                }

                                LightSource light_data = CurrentLayer.LightArrays.Find(p => p.LightId == entity_id_buffer.ToString());

                                if (light_data != null)
                                {
                                    item_id = light_data.LightId;
                                    item_type = NativeFunc.GAME_TYPE.GT_LIGHT;
                                }

                                ParticlesData part_data = currentLayer.ParticlesArrays.Find(p => p.Id == entity_id_buffer.ToString());

                                if (part_data != null)
                                {
                                    item_id = part_data.Id;
                                    item_type = NativeFunc.GAME_TYPE.GT_PARTICLES;
                                }

                                Waypoint way_data = currentLayer.Waypoints.Find(p => p.Id == entity_id_buffer.ToString());

                                if(way_data != null)
                                {
                                    item_id = way_data.Id;
                                    item_type = NativeFunc.GAME_TYPE.GT_WAYPOINT;
                                }

                                Collider col_data = currentLayer.Colliders.Find(p => p.Id == entity_id_buffer.ToString());

                                if(col_data != null)
                                {
                                    item_id = col_data.Id;
                                    item_type = NativeFunc.GAME_TYPE.GT_COLLIDER;
                                }


                                if (ent_data == null && trig_data == null && obj_data == null && snd_data == null && light_data == null && part_data == null && !Control.ModifierKeys.HasFlag(Keys.Control))
                                    NativeFunc.editor_entity_unselect();

                                if (OnSelectedItemChanged != null)
                                    OnSelectedItemChanged(item_id, item_type);
                            }
                        }
                    }
                }
                else if (e.Button == System.Windows.Forms.MouseButtons.Right)//select the current tile under the cursor
                {
                    NativeFunc.editor_setcursor_pos(col_pos * Settings.Default.TILE_SIZE, row_pos * Settings.Default.TILE_SIZE);

                    _tileSelected = new int[1];

                    int[] tile = new int[1];

                    if (currentLayer.Type == (int)TypeLayer.Collisions && !outOfMap)
                        NativeFunc.editor_getcollision(tile, tilePos);
                    else if (currentLayer.Type == (int)TypeLayer.Normal && !outOfMap)
                        NativeFunc.editor_get_tile(tile, tilePos, currentLayer.LayerId);


                    if (currentLayer.Type == (int)TypeLayer.Collisions && !outOfMap)
                        currentCollisionVal = tile[0];
                    else if (currentLayer.Type == (int)TypeLayer.Normal && !outOfMap)
                    {
                        TileSelected[0] = tile[0];
                        if (OnSelectedTileChanged != null)
                            OnSelectedTileChanged();
                    }

                }
            }
            else if (current_mode == GlobalMode.Play)
            {
                if (!_mouseDowned)
                    NativeFunc.editor_click((float)worldPosX, (float)worldPosY, (e.Button == System.Windows.Forms.MouseButtons.Left) ? 0 : 1);
            }

            _mouseDowned = true;

            
            
            base.OnMouseDown(e);
        }


        public int[] floodTileFill(int rowpos, int colpos, int targetTile, int replacementTile,string layer,bool collision_layer = false)
        {
            int width = _ColCount;
            int height = _RowCount;
            int target = targetTile;
            int replacement = replacementTile;

            int[] node = new int[] {rowpos,colpos};

            List<int> changed_tile = new List<int>();

            if (target != replacement)
            {
                Queue<int[]> queue = new Queue<int[]>();

                do
                {
                    int row = node[0];
                    int col = node[1];

                    int[] tile = new int[1];
                    int[] uptile = new int[1];

                    if (collision_layer)
                        NativeFunc.editor_getcollision(tile, (row * _ColCount + (col - 1)));
                    else
                        NativeFunc.editor_get_tile(tile, (row * _ColCount + (col - 1)), layer);

                    while (col > 0 && tile[0] == target)
                    {
                        col--;

                        if(collision_layer)
                            NativeFunc.editor_getcollision(tile, (row * _ColCount + (col - 1)));
                        else
                            NativeFunc.editor_get_tile(tile, (row * _ColCount + (col - 1)), layer);
                    }

                    bool spanUp = false;
                    bool spanDown = false;

                    if(collision_layer)
                        NativeFunc.editor_getcollision(tile, (row * _ColCount + col));
                    else
                        NativeFunc.editor_get_tile(tile, (row * _ColCount + col), layer);

                    while (col < width && tile[0] == target)
                    {
                        if (collision_layer)
                            NativeFunc.editor_change_collision(replacementTile, (row * _ColCount + col));
                        else
                            NativeFunc.editor_change_tile(replacementTile, (row * _ColCount + col), layer);

                        changed_tile.Add((row * _ColCount + col));


                        if (collision_layer)
                        {
                            NativeFunc.editor_getcollision(tile, ((row - 1) * _ColCount + col));
                            NativeFunc.editor_getcollision(uptile, ((row + 1) * _ColCount + col));
                        }
                        else
                        {
                            NativeFunc.editor_get_tile(tile, ((row - 1) * _ColCount + col), layer);
                            NativeFunc.editor_get_tile(uptile, ((row + 1) * _ColCount + col), layer);
                        }


                        if (!spanUp && row > 0 && tile[0] == target)
                        {
                            queue.Enqueue(new int[] { row - 1, col });
                            spanUp = true;
                        }
                        else if (spanUp && row > 0 && tile[0] != target)
                        {
                            spanUp = false;
                        }
                        if (!spanDown && row < height - 1 && uptile[0] == target)
                        {
                            queue.Enqueue(new int[] { row + 1, col });
                            spanDown = true;
                        }
                        else if (spanDown && row < height - 1 && uptile[0] != target)
                        {
                            spanDown = false;
                        }
                        col++;

                        if(collision_layer)
                            NativeFunc.editor_getcollision(tile, (row * _ColCount + col));
                        else
                            NativeFunc.editor_get_tile(tile, (row * _ColCount + col), layer);
                    }
                } while (queue.Count > 0 && (node = queue.Dequeue()) != null);
            }

            return changed_tile.ToArray();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            if (_init && currentMap != null)
                NativeFunc.editor_draw_frame();
        }

         protected override void OnPaintBackground(PaintEventArgs e) {

	        }
    }
}
