﻿namespace WafaEditorV2
{
    partial class PrefWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnEnreg = new System.Windows.Forms.Button();
            this.btnAnnuler = new System.Windows.Forms.Button();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.txtAssets = new System.Windows.Forms.TextBox();
            this.lblAssets = new System.Windows.Forms.Label();
            this.assetsBrowse = new System.Windows.Forms.FolderBrowserDialog();
            this.btnBrowseDll = new System.Windows.Forms.Button();
            this.txt_gamdll = new System.Windows.Forms.TextBox();
            this.lbl_gamdll = new System.Windows.Forms.Label();
            this.gameDllBrowse = new System.Windows.Forms.OpenFileDialog();
            this.txtFont = new System.Windows.Forms.TextBox();
            this.lblFont = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnColorBackground = new System.Windows.Forms.Button();
            this.colorBack = new System.Windows.Forms.ColorDialog();
            this.chkShowGrid = new System.Windows.Forms.CheckBox();
            this.btnGridColor = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.trkMusic = new System.Windows.Forms.TrackBar();
            this.trkSfx = new System.Windows.Forms.TrackBar();
            this.localDbBrowse = new System.Windows.Forms.OpenFileDialog();
            this.btnSelectorColor = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.btnCursorColor = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.btnColorBackTileset = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.numSpaceTiles = new System.Windows.Forms.NumericUpDown();
            this.btnBrowseRaw = new System.Windows.Forms.Button();
            this.txt_rawassets = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.assetRawBrowse = new System.Windows.Forms.FolderBrowserDialog();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.numWidthWindow = new System.Windows.Forms.NumericUpDown();
            this.numHeightWindow = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.numTailleTile = new System.Windows.Forms.NumericUpDown();
            this.chklightsys = new System.Windows.Forms.CheckBox();
            this.btnAmbientColor = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.numAlphaAmbient = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.txtProgLight = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtProgShadow = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabGeneral = new System.Windows.Forms.TabPage();
            this.btnColorFPS = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.txtProgMask = new System.Windows.Forms.TextBox();
            this.chkResDynamique = new System.Windows.Forms.CheckBox();
            this.tabExport = new System.Windows.Forms.TabPage();
            this.chkCleanBuild = new System.Windows.Forms.CheckBox();
            this.chkSkipBuild = new System.Windows.Forms.CheckBox();
            this.numPackTexheight = new System.Windows.Forms.NumericUpDown();
            this.numPackTexwidth = new System.Windows.Forms.NumericUpDown();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.chkPacking = new System.Windows.Forms.CheckBox();
            this.txtGameName = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txtJDK = new System.Windows.Forms.TextBox();
            this.btnBrowseJDK = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.txtAnt = new System.Windows.Forms.TextBox();
            this.btnBrowseAnt = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.txtRootFolder = new System.Windows.Forms.TextBox();
            this.btnRootFolder = new System.Windows.Forms.Button();
            this.rootFolderBrowse = new System.Windows.Forms.FolderBrowserDialog();
            this.AntBrowse = new System.Windows.Forms.FolderBrowserDialog();
            this.JDKBrowse = new System.Windows.Forms.FolderBrowserDialog();
            this.btnBrowseLocaledb = new System.Windows.Forms.Button();
            this.txt_localedb = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPotrace = new System.Windows.Forms.TextBox();
            this.btnpotrace = new System.Windows.Forms.Button();
            this.label26 = new System.Windows.Forms.Label();
            this.ofdPotrace = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.trkMusic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trkSfx)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSpaceTiles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWidthWindow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHeightWindow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTailleTile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numAlphaAmbient)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabGeneral.SuspendLayout();
            this.tabExport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPackTexheight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPackTexwidth)).BeginInit();
            this.SuspendLayout();
            // 
            // btnEnreg
            // 
            this.btnEnreg.Location = new System.Drawing.Point(258, 791);
            this.btnEnreg.Name = "btnEnreg";
            this.btnEnreg.Size = new System.Drawing.Size(80, 22);
            this.btnEnreg.TabIndex = 0;
            this.btnEnreg.Text = "Enregistrer";
            this.btnEnreg.UseVisualStyleBackColor = true;
            this.btnEnreg.Click += new System.EventHandler(this.btnEnreg_Click);
            // 
            // btnAnnuler
            // 
            this.btnAnnuler.Location = new System.Drawing.Point(369, 791);
            this.btnAnnuler.Name = "btnAnnuler";
            this.btnAnnuler.Size = new System.Drawing.Size(80, 22);
            this.btnAnnuler.TabIndex = 1;
            this.btnAnnuler.Text = "Annuler";
            this.btnAnnuler.UseVisualStyleBackColor = true;
            this.btnAnnuler.Click += new System.EventHandler(this.btnAnnuler_Click);
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(594, 18);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(80, 22);
            this.btnBrowse.TabIndex = 17;
            this.btnBrowse.Text = "Parcourir...";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // txtAssets
            // 
            this.txtAssets.Location = new System.Drawing.Point(224, 20);
            this.txtAssets.Name = "txtAssets";
            this.txtAssets.ReadOnly = true;
            this.txtAssets.Size = new System.Drawing.Size(363, 20);
            this.txtAssets.TabIndex = 16;
            // 
            // lblAssets
            // 
            this.lblAssets.AutoSize = true;
            this.lblAssets.Location = new System.Drawing.Point(42, 22);
            this.lblAssets.Name = "lblAssets";
            this.lblAssets.Size = new System.Drawing.Size(164, 13);
            this.lblAssets.TabIndex = 15;
            this.lblAssets.Text = "Répertoire de données exportées";
            // 
            // assetsBrowse
            // 
            this.assetsBrowse.RootFolder = System.Environment.SpecialFolder.MyComputer;
            this.assetsBrowse.ShowNewFolderButton = false;
            // 
            // btnBrowseDll
            // 
            this.btnBrowseDll.Location = new System.Drawing.Point(594, 73);
            this.btnBrowseDll.Name = "btnBrowseDll";
            this.btnBrowseDll.Size = new System.Drawing.Size(80, 22);
            this.btnBrowseDll.TabIndex = 20;
            this.btnBrowseDll.Text = "Parcourir...";
            this.btnBrowseDll.UseVisualStyleBackColor = true;
            this.btnBrowseDll.Click += new System.EventHandler(this.btnBrowseDll_Click);
            // 
            // txt_gamdll
            // 
            this.txt_gamdll.Location = new System.Drawing.Point(224, 74);
            this.txt_gamdll.Name = "txt_gamdll";
            this.txt_gamdll.ReadOnly = true;
            this.txt_gamdll.Size = new System.Drawing.Size(363, 20);
            this.txt_gamdll.TabIndex = 19;
            // 
            // lbl_gamdll
            // 
            this.lbl_gamdll.AutoSize = true;
            this.lbl_gamdll.Location = new System.Drawing.Point(95, 77);
            this.lbl_gamdll.Name = "lbl_gamdll";
            this.lbl_gamdll.Size = new System.Drawing.Size(110, 13);
            this.lbl_gamdll.TabIndex = 18;
            this.lbl_gamdll.Text = "Chemin vers dll du jeu";
            // 
            // gameDllBrowse
            // 
            this.gameDllBrowse.FileName = "game dll";
            this.gameDllBrowse.Filter = "Game dll|*.dll";
            // 
            // txtFont
            // 
            this.txtFont.Location = new System.Drawing.Point(296, 243);
            this.txtFont.Name = "txtFont";
            this.txtFont.Size = new System.Drawing.Size(158, 20);
            this.txtFont.TabIndex = 21;
            // 
            // lblFont
            // 
            this.lblFont.AutoSize = true;
            this.lblFont.Location = new System.Drawing.Point(19, 246);
            this.lblFont.Name = "lblFont";
            this.lblFont.Size = new System.Drawing.Size(271, 13);
            this.lblFont.TabIndex = 22;
            this.lblFont.Text = "Fichier de config de police de caractères (ex: times.json)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(49, 285);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(153, 13);
            this.label1.TabIndex = 23;
            this.label1.Text = "Couleur de fond fenêtre niveau";
            // 
            // btnColorBackground
            // 
            this.btnColorBackground.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnColorBackground.Location = new System.Drawing.Point(232, 280);
            this.btnColorBackground.Name = "btnColorBackground";
            this.btnColorBackground.Size = new System.Drawing.Size(23, 22);
            this.btnColorBackground.TabIndex = 24;
            this.btnColorBackground.UseVisualStyleBackColor = false;
            this.btnColorBackground.Click += new System.EventHandler(this.Color_Click);
            // 
            // chkShowGrid
            // 
            this.chkShowGrid.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkShowGrid.Location = new System.Drawing.Point(100, 308);
            this.chkShowGrid.Name = "chkShowGrid";
            this.chkShowGrid.Size = new System.Drawing.Size(150, 24);
            this.chkShowGrid.TabIndex = 25;
            this.chkShowGrid.Text = "Afficher grille";
            this.chkShowGrid.UseVisualStyleBackColor = true;
            // 
            // btnGridColor
            // 
            this.btnGridColor.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnGridColor.Location = new System.Drawing.Point(232, 360);
            this.btnGridColor.Name = "btnGridColor";
            this.btnGridColor.Size = new System.Drawing.Size(23, 22);
            this.btnGridColor.TabIndex = 27;
            this.btnGridColor.UseVisualStyleBackColor = false;
            this.btnGridColor.Click += new System.EventHandler(this.Color_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(78, 365);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 26;
            this.label2.Text = "Couleur grille";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(72, 493);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 13);
            this.label3.TabIndex = 28;
            this.label3.Text = "Volume musique";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(72, 535);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 29;
            this.label4.Text = "Volume effets";
            // 
            // trkMusic
            // 
            this.trkMusic.Location = new System.Drawing.Point(235, 492);
            this.trkMusic.Name = "trkMusic";
            this.trkMusic.Size = new System.Drawing.Size(111, 45);
            this.trkMusic.TabIndex = 30;
            // 
            // trkSfx
            // 
            this.trkSfx.Location = new System.Drawing.Point(235, 535);
            this.trkSfx.Name = "trkSfx";
            this.trkSfx.Size = new System.Drawing.Size(111, 45);
            this.trkSfx.TabIndex = 31;
            // 
            // localDbBrowse
            // 
            this.localDbBrowse.FileName = "locale db";
            this.localDbBrowse.Filter = "locale db|*.db";
            // 
            // btnSelectorColor
            // 
            this.btnSelectorColor.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnSelectorColor.Location = new System.Drawing.Point(232, 332);
            this.btnSelectorColor.Name = "btnSelectorColor";
            this.btnSelectorColor.Size = new System.Drawing.Size(23, 22);
            this.btnSelectorColor.TabIndex = 36;
            this.btnSelectorColor.UseVisualStyleBackColor = false;
            this.btnSelectorColor.Click += new System.EventHandler(this.Color_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 395);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(181, 13);
            this.label6.TabIndex = 35;
            this.label6.Text = "Couleur curseur / rectangle sélection";
            // 
            // btnCursorColor
            // 
            this.btnCursorColor.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnCursorColor.Location = new System.Drawing.Point(232, 389);
            this.btnCursorColor.Name = "btnCursorColor";
            this.btnCursorColor.Size = new System.Drawing.Size(23, 22);
            this.btnCursorColor.TabIndex = 38;
            this.btnCursorColor.UseVisualStyleBackColor = false;
            this.btnCursorColor.Click += new System.EventHandler(this.Color_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(81, 338);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(134, 13);
            this.label7.TabIndex = 37;
            this.label7.Text = "Couleur surlignage entitées";
            // 
            // btnColorBackTileset
            // 
            this.btnColorBackTileset.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnColorBackTileset.Location = new System.Drawing.Point(673, 241);
            this.btnColorBackTileset.Name = "btnColorBackTileset";
            this.btnColorBackTileset.Size = new System.Drawing.Size(23, 22);
            this.btnColorBackTileset.TabIndex = 40;
            this.btnColorBackTileset.UseVisualStyleBackColor = false;
            this.btnColorBackTileset.Click += new System.EventHandler(this.Color_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(519, 246);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(128, 13);
            this.label8.TabIndex = 39;
            this.label8.Text = "Couleur arrière plan tileset";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(519, 285);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(126, 13);
            this.label9.TabIndex = 41;
            this.label9.Text = "espace entre tuiles tileset";
            // 
            // numSpaceTiles
            // 
            this.numSpaceTiles.Location = new System.Drawing.Point(673, 280);
            this.numSpaceTiles.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numSpaceTiles.Name = "numSpaceTiles";
            this.numSpaceTiles.Size = new System.Drawing.Size(49, 20);
            this.numSpaceTiles.TabIndex = 42;
            this.numSpaceTiles.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // btnBrowseRaw
            // 
            this.btnBrowseRaw.Location = new System.Drawing.Point(594, 46);
            this.btnBrowseRaw.Name = "btnBrowseRaw";
            this.btnBrowseRaw.Size = new System.Drawing.Size(80, 22);
            this.btnBrowseRaw.TabIndex = 45;
            this.btnBrowseRaw.Text = "Parcourir...";
            this.btnBrowseRaw.UseVisualStyleBackColor = true;
            this.btnBrowseRaw.Click += new System.EventHandler(this.btnBrowseRaw_Click);
            // 
            // txt_rawassets
            // 
            this.txt_rawassets.Location = new System.Drawing.Point(224, 48);
            this.txt_rawassets.Name = "txt_rawassets";
            this.txt_rawassets.ReadOnly = true;
            this.txt_rawassets.Size = new System.Drawing.Size(363, 20);
            this.txt_rawassets.TabIndex = 44;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(51, 53);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(147, 13);
            this.label10.TabIndex = 43;
            this.label10.Text = "Répertoire de données brutes";
            // 
            // assetRawBrowse
            // 
            this.assetRawBrowse.RootFolder = System.Environment.SpecialFolder.MyComputer;
            this.assetRawBrowse.ShowNewFolderButton = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(72, 588);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(68, 13);
            this.label11.TabIndex = 46;
            this.label11.Text = "Taille fenêtre";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(305, 588);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(14, 13);
            this.label12.TabIndex = 49;
            this.label12.Text = "X";
            // 
            // numWidthWindow
            // 
            this.numWidthWindow.Location = new System.Drawing.Point(235, 585);
            this.numWidthWindow.Maximum = new decimal(new int[] {
            4000,
            0,
            0,
            0});
            this.numWidthWindow.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numWidthWindow.Name = "numWidthWindow";
            this.numWidthWindow.Size = new System.Drawing.Size(49, 20);
            this.numWidthWindow.TabIndex = 50;
            this.numWidthWindow.Value = new decimal(new int[] {
            1024,
            0,
            0,
            0});
            // 
            // numHeightWindow
            // 
            this.numHeightWindow.Location = new System.Drawing.Point(334, 585);
            this.numHeightWindow.Maximum = new decimal(new int[] {
            4000,
            0,
            0,
            0});
            this.numHeightWindow.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numHeightWindow.Name = "numHeightWindow";
            this.numHeightWindow.Size = new System.Drawing.Size(49, 20);
            this.numHeightWindow.TabIndex = 51;
            this.numHeightWindow.Value = new decimal(new int[] {
            768,
            0,
            0,
            0});
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(72, 626);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(48, 13);
            this.label13.TabIndex = 52;
            this.label13.Text = "Taille tile";
            // 
            // numTailleTile
            // 
            this.numTailleTile.Location = new System.Drawing.Point(235, 625);
            this.numTailleTile.Maximum = new decimal(new int[] {
            128,
            0,
            0,
            0});
            this.numTailleTile.Minimum = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.numTailleTile.Name = "numTailleTile";
            this.numTailleTile.Size = new System.Drawing.Size(49, 20);
            this.numTailleTile.TabIndex = 53;
            this.numTailleTile.Value = new decimal(new int[] {
            32,
            0,
            0,
            0});
            // 
            // chklightsys
            // 
            this.chklightsys.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chklightsys.Location = new System.Drawing.Point(81, 418);
            this.chklightsys.Name = "chklightsys";
            this.chklightsys.Size = new System.Drawing.Size(167, 24);
            this.chklightsys.TabIndex = 54;
            this.chklightsys.Text = "activer système éclairage";
            this.chklightsys.UseVisualStyleBackColor = true;
            // 
            // btnAmbientColor
            // 
            this.btnAmbientColor.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnAmbientColor.Location = new System.Drawing.Point(232, 442);
            this.btnAmbientColor.Name = "btnAmbientColor";
            this.btnAmbientColor.Size = new System.Drawing.Size(23, 22);
            this.btnAmbientColor.TabIndex = 56;
            this.btnAmbientColor.UseVisualStyleBackColor = false;
            this.btnAmbientColor.Click += new System.EventHandler(this.Color_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(19, 449);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(135, 13);
            this.label14.TabIndex = 55;
            this.label14.Text = "Couleur ambiante éclairage";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(19, 471);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(118, 13);
            this.label15.TabIndex = 57;
            this.label15.Text = "Couleur ambiante alpha";
            // 
            // numAlphaAmbient
            // 
            this.numAlphaAmbient.Location = new System.Drawing.Point(232, 471);
            this.numAlphaAmbient.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numAlphaAmbient.Name = "numAlphaAmbient";
            this.numAlphaAmbient.Size = new System.Drawing.Size(49, 20);
            this.numAlphaAmbient.TabIndex = 58;
            this.numAlphaAmbient.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(49, 656);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(80, 13);
            this.label16.TabIndex = 60;
            this.label16.Text = "shader lumières";
            // 
            // txtProgLight
            // 
            this.txtProgLight.Location = new System.Drawing.Point(231, 653);
            this.txtProgLight.Name = "txtProgLight";
            this.txtProgLight.Size = new System.Drawing.Size(158, 20);
            this.txtProgLight.TabIndex = 59;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(48, 687);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(137, 13);
            this.label17.TabIndex = 62;
            this.label17.Text = "shader rendu final éclairage";
            // 
            // txtProgShadow
            // 
            this.txtProgShadow.Location = new System.Drawing.Point(231, 685);
            this.txtProgShadow.Name = "txtProgShadow";
            this.txtProgShadow.Size = new System.Drawing.Size(158, 20);
            this.txtProgShadow.TabIndex = 61;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabGeneral);
            this.tabControl1.Controls.Add(this.tabExport);
            this.tabControl1.Location = new System.Drawing.Point(3, 5);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(903, 780);
            this.tabControl1.TabIndex = 63;
            // 
            // tabGeneral
            // 
            this.tabGeneral.BackColor = System.Drawing.Color.White;
            this.tabGeneral.Controls.Add(this.label26);
            this.tabGeneral.Controls.Add(this.txtPotrace);
            this.tabGeneral.Controls.Add(this.btnpotrace);
            this.tabGeneral.Controls.Add(this.btnColorFPS);
            this.tabGeneral.Controls.Add(this.label25);
            this.tabGeneral.Controls.Add(this.label22);
            this.tabGeneral.Controls.Add(this.txtProgMask);
            this.tabGeneral.Controls.Add(this.chkResDynamique);
            this.tabGeneral.Controls.Add(this.lblAssets);
            this.tabGeneral.Controls.Add(this.label17);
            this.tabGeneral.Controls.Add(this.txtAssets);
            this.tabGeneral.Controls.Add(this.txtProgShadow);
            this.tabGeneral.Controls.Add(this.btnBrowse);
            this.tabGeneral.Controls.Add(this.label16);
            this.tabGeneral.Controls.Add(this.lbl_gamdll);
            this.tabGeneral.Controls.Add(this.txtProgLight);
            this.tabGeneral.Controls.Add(this.txt_gamdll);
            this.tabGeneral.Controls.Add(this.numAlphaAmbient);
            this.tabGeneral.Controls.Add(this.btnBrowseDll);
            this.tabGeneral.Controls.Add(this.label15);
            this.tabGeneral.Controls.Add(this.txtFont);
            this.tabGeneral.Controls.Add(this.btnAmbientColor);
            this.tabGeneral.Controls.Add(this.lblFont);
            this.tabGeneral.Controls.Add(this.label14);
            this.tabGeneral.Controls.Add(this.label1);
            this.tabGeneral.Controls.Add(this.chklightsys);
            this.tabGeneral.Controls.Add(this.btnColorBackground);
            this.tabGeneral.Controls.Add(this.numTailleTile);
            this.tabGeneral.Controls.Add(this.chkShowGrid);
            this.tabGeneral.Controls.Add(this.label13);
            this.tabGeneral.Controls.Add(this.label2);
            this.tabGeneral.Controls.Add(this.numHeightWindow);
            this.tabGeneral.Controls.Add(this.btnGridColor);
            this.tabGeneral.Controls.Add(this.numWidthWindow);
            this.tabGeneral.Controls.Add(this.label3);
            this.tabGeneral.Controls.Add(this.label12);
            this.tabGeneral.Controls.Add(this.label4);
            this.tabGeneral.Controls.Add(this.label11);
            this.tabGeneral.Controls.Add(this.trkMusic);
            this.tabGeneral.Controls.Add(this.btnBrowseRaw);
            this.tabGeneral.Controls.Add(this.trkSfx);
            this.tabGeneral.Controls.Add(this.txt_rawassets);
            this.tabGeneral.Controls.Add(this.label5);
            this.tabGeneral.Controls.Add(this.label10);
            this.tabGeneral.Controls.Add(this.txt_localedb);
            this.tabGeneral.Controls.Add(this.numSpaceTiles);
            this.tabGeneral.Controls.Add(this.btnBrowseLocaledb);
            this.tabGeneral.Controls.Add(this.label9);
            this.tabGeneral.Controls.Add(this.label6);
            this.tabGeneral.Controls.Add(this.btnColorBackTileset);
            this.tabGeneral.Controls.Add(this.btnSelectorColor);
            this.tabGeneral.Controls.Add(this.label8);
            this.tabGeneral.Controls.Add(this.label7);
            this.tabGeneral.Controls.Add(this.btnCursorColor);
            this.tabGeneral.Location = new System.Drawing.Point(4, 22);
            this.tabGeneral.Name = "tabGeneral";
            this.tabGeneral.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabGeneral.Size = new System.Drawing.Size(895, 754);
            this.tabGeneral.TabIndex = 0;
            this.tabGeneral.Text = "Général";
            // 
            // btnColorFPS
            // 
            this.btnColorFPS.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnColorFPS.Location = new System.Drawing.Point(673, 310);
            this.btnColorFPS.Name = "btnColorFPS";
            this.btnColorFPS.Size = new System.Drawing.Size(23, 22);
            this.btnColorFPS.TabIndex = 67;
            this.btnColorFPS.UseVisualStyleBackColor = false;
            this.btnColorFPS.Click += new System.EventHandler(this.Color_Click);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(505, 316);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(92, 13);
            this.label25.TabIndex = 66;
            this.label25.Text = "Couleur texte FPS";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(49, 718);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(116, 13);
            this.label22.TabIndex = 65;
            this.label22.Text = "shader masque ombres";
            // 
            // txtProgMask
            // 
            this.txtProgMask.Location = new System.Drawing.Point(232, 715);
            this.txtProgMask.Name = "txtProgMask";
            this.txtProgMask.Size = new System.Drawing.Size(158, 20);
            this.txtProgMask.TabIndex = 64;
            // 
            // chkResDynamique
            // 
            this.chkResDynamique.Location = new System.Drawing.Point(413, 583);
            this.chkResDynamique.Name = "chkResDynamique";
            this.chkResDynamique.Size = new System.Drawing.Size(150, 24);
            this.chkResDynamique.TabIndex = 63;
            this.chkResDynamique.Text = "Résolution dynamique";
            this.chkResDynamique.UseVisualStyleBackColor = true;
            this.chkResDynamique.CheckedChanged += new System.EventHandler(this.chkResDynamique_CheckedChanged);
            // 
            // tabExport
            // 
            this.tabExport.Controls.Add(this.chkCleanBuild);
            this.tabExport.Controls.Add(this.chkSkipBuild);
            this.tabExport.Controls.Add(this.numPackTexheight);
            this.tabExport.Controls.Add(this.numPackTexwidth);
            this.tabExport.Controls.Add(this.label23);
            this.tabExport.Controls.Add(this.label24);
            this.tabExport.Controls.Add(this.chkPacking);
            this.tabExport.Controls.Add(this.txtGameName);
            this.tabExport.Controls.Add(this.label21);
            this.tabExport.Controls.Add(this.label20);
            this.tabExport.Controls.Add(this.txtJDK);
            this.tabExport.Controls.Add(this.btnBrowseJDK);
            this.tabExport.Controls.Add(this.label19);
            this.tabExport.Controls.Add(this.txtAnt);
            this.tabExport.Controls.Add(this.btnBrowseAnt);
            this.tabExport.Controls.Add(this.label18);
            this.tabExport.Controls.Add(this.txtRootFolder);
            this.tabExport.Controls.Add(this.btnRootFolder);
            this.tabExport.Location = new System.Drawing.Point(4, 22);
            this.tabExport.Name = "tabExport";
            this.tabExport.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabExport.Size = new System.Drawing.Size(895, 672);
            this.tabExport.TabIndex = 1;
            this.tabExport.Text = "Exports";
            this.tabExport.UseVisualStyleBackColor = true;
            // 
            // chkCleanBuild
            // 
            this.chkCleanBuild.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkCleanBuild.Location = new System.Drawing.Point(33, 370);
            this.chkCleanBuild.Name = "chkCleanBuild";
            this.chkCleanBuild.Size = new System.Drawing.Size(186, 40);
            this.chkCleanBuild.TabIndex = 69;
            this.chkCleanBuild.Text = "Supprimer ancienne build (export android)";
            this.chkCleanBuild.UseVisualStyleBackColor = true;
            // 
            // chkSkipBuild
            // 
            this.chkSkipBuild.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkSkipBuild.Location = new System.Drawing.Point(33, 323);
            this.chkSkipBuild.Name = "chkSkipBuild";
            this.chkSkipBuild.Size = new System.Drawing.Size(186, 40);
            this.chkSkipBuild.TabIndex = 68;
            this.chkSkipBuild.Text = "Sauter étape de compilation";
            this.chkSkipBuild.UseVisualStyleBackColor = true;
            // 
            // numPackTexheight
            // 
            this.numPackTexheight.Location = new System.Drawing.Point(301, 274);
            this.numPackTexheight.Maximum = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.numPackTexheight.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numPackTexheight.Name = "numPackTexheight";
            this.numPackTexheight.Size = new System.Drawing.Size(49, 20);
            this.numPackTexheight.TabIndex = 67;
            this.numPackTexheight.Value = new decimal(new int[] {
            2048,
            0,
            0,
            0});
            // 
            // numPackTexwidth
            // 
            this.numPackTexwidth.Location = new System.Drawing.Point(202, 274);
            this.numPackTexwidth.Maximum = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.numPackTexwidth.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numPackTexwidth.Name = "numPackTexwidth";
            this.numPackTexwidth.Size = new System.Drawing.Size(49, 20);
            this.numPackTexwidth.TabIndex = 66;
            this.numPackTexwidth.Value = new decimal(new int[] {
            2048,
            0,
            0,
            0});
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(271, 278);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(14, 13);
            this.label23.TabIndex = 65;
            this.label23.Text = "X";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(38, 278);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(94, 13);
            this.label24.TabIndex = 64;
            this.label24.Text = "Taille pack texture";
            // 
            // chkPacking
            // 
            this.chkPacking.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkPacking.Location = new System.Drawing.Point(33, 226);
            this.chkPacking.Name = "chkPacking";
            this.chkPacking.Size = new System.Drawing.Size(186, 40);
            this.chkPacking.TabIndex = 29;
            this.chkPacking.Text = "Création pack texture";
            this.chkPacking.UseVisualStyleBackColor = true;
            // 
            // txtGameName
            // 
            this.txtGameName.BackColor = System.Drawing.SystemColors.Window;
            this.txtGameName.Location = new System.Drawing.Point(199, 20);
            this.txtGameName.Name = "txtGameName";
            this.txtGameName.Size = new System.Drawing.Size(363, 20);
            this.txtGameName.TabIndex = 28;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(30, 22);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(61, 13);
            this.label21.TabIndex = 27;
            this.label21.Text = "Nom du jeu";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 186);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(155, 13);
            this.label20.TabIndex = 24;
            this.label20.Text = "Répertoire JDK (export android)";
            // 
            // txtJDK
            // 
            this.txtJDK.Location = new System.Drawing.Point(201, 182);
            this.txtJDK.Name = "txtJDK";
            this.txtJDK.ReadOnly = true;
            this.txtJDK.Size = new System.Drawing.Size(363, 20);
            this.txtJDK.TabIndex = 25;
            // 
            // btnBrowseJDK
            // 
            this.btnBrowseJDK.Location = new System.Drawing.Point(580, 176);
            this.btnBrowseJDK.Name = "btnBrowseJDK";
            this.btnBrowseJDK.Size = new System.Drawing.Size(80, 22);
            this.btnBrowseJDK.TabIndex = 26;
            this.btnBrowseJDK.Text = "Parcourir...";
            this.btnBrowseJDK.UseVisualStyleBackColor = true;
            this.btnBrowseJDK.Click += new System.EventHandler(this.btnBrowseJDK_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 142);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(176, 13);
            this.label19.TabIndex = 21;
            this.label19.Text = "Répertoire Java ant (export android)";
            // 
            // txtAnt
            // 
            this.txtAnt.Location = new System.Drawing.Point(199, 138);
            this.txtAnt.Name = "txtAnt";
            this.txtAnt.ReadOnly = true;
            this.txtAnt.Size = new System.Drawing.Size(363, 20);
            this.txtAnt.TabIndex = 22;
            // 
            // btnBrowseAnt
            // 
            this.btnBrowseAnt.Location = new System.Drawing.Point(580, 137);
            this.btnBrowseAnt.Name = "btnBrowseAnt";
            this.btnBrowseAnt.Size = new System.Drawing.Size(80, 22);
            this.btnBrowseAnt.TabIndex = 23;
            this.btnBrowseAnt.Text = "Parcourir...";
            this.btnBrowseAnt.UseVisualStyleBackColor = true;
            this.btnBrowseAnt.Click += new System.EventHandler(this.btnBrowseAnt_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(30, 77);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(105, 13);
            this.label18.TabIndex = 18;
            this.label18.Text = "Répertoire racine jeu";
            // 
            // txtRootFolder
            // 
            this.txtRootFolder.Location = new System.Drawing.Point(201, 74);
            this.txtRootFolder.Name = "txtRootFolder";
            this.txtRootFolder.ReadOnly = true;
            this.txtRootFolder.Size = new System.Drawing.Size(363, 20);
            this.txtRootFolder.TabIndex = 19;
            // 
            // btnRootFolder
            // 
            this.btnRootFolder.Location = new System.Drawing.Point(582, 72);
            this.btnRootFolder.Name = "btnRootFolder";
            this.btnRootFolder.Size = new System.Drawing.Size(80, 22);
            this.btnRootFolder.TabIndex = 20;
            this.btnRootFolder.Text = "Parcourir...";
            this.btnRootFolder.UseVisualStyleBackColor = true;
            this.btnRootFolder.Click += new System.EventHandler(this.btnRootFolder_Click);
            // 
            // rootFolderBrowse
            // 
            this.rootFolderBrowse.RootFolder = System.Environment.SpecialFolder.MyComputer;
            this.rootFolderBrowse.ShowNewFolderButton = false;
            // 
            // AntBrowse
            // 
            this.AntBrowse.RootFolder = System.Environment.SpecialFolder.MyComputer;
            this.AntBrowse.ShowNewFolderButton = false;
            // 
            // JDKBrowse
            // 
            this.JDKBrowse.RootFolder = System.Environment.SpecialFolder.MyComputer;
            this.JDKBrowse.ShowNewFolderButton = false;
            // 
            // btnBrowseLocaledb
            // 
            this.btnBrowseLocaledb.Location = new System.Drawing.Point(594, 106);
            this.btnBrowseLocaledb.Name = "btnBrowseLocaledb";
            this.btnBrowseLocaledb.Size = new System.Drawing.Size(80, 22);
            this.btnBrowseLocaledb.TabIndex = 34;
            this.btnBrowseLocaledb.Text = "Parcourir...";
            this.btnBrowseLocaledb.UseVisualStyleBackColor = true;
            this.btnBrowseLocaledb.Click += new System.EventHandler(this.btnBrowseLocaledb_Click);
            // 
            // txt_localedb
            // 
            this.txt_localedb.Location = new System.Drawing.Point(224, 107);
            this.txt_localedb.Name = "txt_localedb";
            this.txt_localedb.ReadOnly = true;
            this.txt_localedb.Size = new System.Drawing.Size(363, 20);
            this.txt_localedb.TabIndex = 33;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(28, 111);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(178, 13);
            this.label5.TabIndex = 32;
            this.label5.Text = "Chemin vers fichier traduction du jeu";
            // 
            // txtPotrace
            // 
            this.txtPotrace.Location = new System.Drawing.Point(224, 135);
            this.txtPotrace.Name = "txtPotrace";
            this.txtPotrace.ReadOnly = true;
            this.txtPotrace.Size = new System.Drawing.Size(363, 20);
            this.txtPotrace.TabIndex = 69;
            // 
            // btnpotrace
            // 
            this.btnpotrace.Location = new System.Drawing.Point(594, 134);
            this.btnpotrace.Name = "btnpotrace";
            this.btnpotrace.Size = new System.Drawing.Size(80, 22);
            this.btnpotrace.TabIndex = 70;
            this.btnpotrace.Text = "Parcourir...";
            this.btnpotrace.UseVisualStyleBackColor = true;
            this.btnpotrace.Click += new System.EventHandler(this.btnpotrace_Click);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(101, 143);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(104, 13);
            this.label26.TabIndex = 68;
            this.label26.Text = "Chemin vers potrace";
            // 
            // ofdPotrace
            // 
            this.ofdPotrace.FileName = "potrace exe";
            this.ofdPotrace.Filter = "Potrace exe |*.exe";
            // 
            // PrefWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(940, 825);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btnAnnuler);
            this.Controls.Add(this.btnEnreg);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PrefWindow";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Préférences";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PrefWindow_FormClosing);
            this.Load += new System.EventHandler(this.PrefWindow_Load);
            ((System.ComponentModel.ISupportInitialize)(this.trkMusic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trkSfx)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSpaceTiles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWidthWindow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHeightWindow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTailleTile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numAlphaAmbient)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabGeneral.ResumeLayout(false);
            this.tabGeneral.PerformLayout();
            this.tabExport.ResumeLayout(false);
            this.tabExport.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPackTexheight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPackTexwidth)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnEnreg;
        private System.Windows.Forms.Button btnAnnuler;
        private System.Windows.Forms.Button btnBrowse;
        public System.Windows.Forms.TextBox txtAssets;
        private System.Windows.Forms.Label lblAssets;
        private System.Windows.Forms.FolderBrowserDialog assetsBrowse;
        private System.Windows.Forms.Button btnBrowseDll;
        public System.Windows.Forms.TextBox txt_gamdll;
        private System.Windows.Forms.Label lbl_gamdll;
        private System.Windows.Forms.OpenFileDialog gameDllBrowse;
        public System.Windows.Forms.TextBox txtFont;
        private System.Windows.Forms.Label lblFont;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnColorBackground;
        private System.Windows.Forms.ColorDialog colorBack;
        private System.Windows.Forms.CheckBox chkShowGrid;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnGridColor;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TrackBar trkMusic;
        private System.Windows.Forms.TrackBar trkSfx;
        private System.Windows.Forms.OpenFileDialog localDbBrowse;
        private System.Windows.Forms.Button btnSelectorColor;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnCursorColor;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnColorBackTileset;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown numSpaceTiles;
        private System.Windows.Forms.Button btnBrowseRaw;
        public System.Windows.Forms.TextBox txt_rawassets;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.FolderBrowserDialog assetRawBrowse;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown numWidthWindow;
        private System.Windows.Forms.NumericUpDown numHeightWindow;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown numTailleTile;
        private System.Windows.Forms.CheckBox chklightsys;
        private System.Windows.Forms.Button btnAmbientColor;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.NumericUpDown numAlphaAmbient;
        private System.Windows.Forms.Label label16;
        public System.Windows.Forms.TextBox txtProgLight;
        private System.Windows.Forms.Label label17;
        public System.Windows.Forms.TextBox txtProgShadow;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabGeneral;
        private System.Windows.Forms.TabPage tabExport;
        private System.Windows.Forms.Label label18;
        public System.Windows.Forms.TextBox txtRootFolder;
        private System.Windows.Forms.Button btnRootFolder;
        private System.Windows.Forms.FolderBrowserDialog rootFolderBrowse;
        private System.Windows.Forms.Label label19;
        public System.Windows.Forms.TextBox txtAnt;
        private System.Windows.Forms.Button btnBrowseAnt;
        private System.Windows.Forms.FolderBrowserDialog AntBrowse;
        private System.Windows.Forms.Label label20;
        public System.Windows.Forms.TextBox txtJDK;
        private System.Windows.Forms.Button btnBrowseJDK;
        private System.Windows.Forms.FolderBrowserDialog JDKBrowse;
        public System.Windows.Forms.TextBox txtGameName;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.CheckBox chkResDynamique;
        private System.Windows.Forms.Label label22;
        public System.Windows.Forms.TextBox txtProgMask;
        private System.Windows.Forms.CheckBox chkPacking;
        private System.Windows.Forms.NumericUpDown numPackTexheight;
        private System.Windows.Forms.NumericUpDown numPackTexwidth;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.CheckBox chkSkipBuild;
        private System.Windows.Forms.Button btnColorFPS;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.CheckBox chkCleanBuild;
        private System.Windows.Forms.Label label26;
        public System.Windows.Forms.TextBox txtPotrace;
        private System.Windows.Forms.Button btnpotrace;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.TextBox txt_localedb;
        private System.Windows.Forms.Button btnBrowseLocaledb;
        private System.Windows.Forms.OpenFileDialog ofdPotrace;
    }
}