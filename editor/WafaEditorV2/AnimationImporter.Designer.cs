﻿namespace WafaEditorV2
{
    partial class AnimationImporter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_nomcalque = new System.Windows.Forms.Label();
            this.cmb_calques = new System.Windows.Forms.ComboBox();
            this.lbl_fileimport = new System.Windows.Forms.Label();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.txtTileset = new System.Windows.Forms.TextBox();
            this.btn_import = new System.Windows.Forms.Button();
            this.openTileSet = new System.Windows.Forms.OpenFileDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbAnchor = new System.Windows.Forms.ComboBox();
            this.chkDuplicate = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // lbl_nomcalque
            // 
            this.lbl_nomcalque.AutoSize = true;
            this.lbl_nomcalque.Location = new System.Drawing.Point(23, 34);
            this.lbl_nomcalque.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_nomcalque.Name = "lbl_nomcalque";
            this.lbl_nomcalque.Size = new System.Drawing.Size(168, 17);
            this.lbl_nomcalque.TabIndex = 0;
            this.lbl_nomcalque.Text = "Nom du calque à utiliser :";
            // 
            // cmb_calques
            // 
            this.cmb_calques.FormattingEnabled = true;
            this.cmb_calques.Location = new System.Drawing.Point(199, 31);
            this.cmb_calques.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmb_calques.Name = "cmb_calques";
            this.cmb_calques.Size = new System.Drawing.Size(309, 25);
            this.cmb_calques.TabIndex = 2;
            // 
            // lbl_fileimport
            // 
            this.lbl_fileimport.AutoSize = true;
            this.lbl_fileimport.Location = new System.Drawing.Point(23, 105);
            this.lbl_fileimport.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_fileimport.Name = "lbl_fileimport";
            this.lbl_fileimport.Size = new System.Drawing.Size(126, 17);
            this.lbl_fileimport.TabIndex = 3;
            this.lbl_fileimport.Text = "Fichier à importer :";
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(409, 98);
            this.btnBrowse.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(100, 28);
            this.btnBrowse.TabIndex = 16;
            this.btnBrowse.Text = "Parcourir...";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // txtTileset
            // 
            this.txtTileset.Location = new System.Drawing.Point(200, 101);
            this.txtTileset.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtTileset.Name = "txtTileset";
            this.txtTileset.ReadOnly = true;
            this.txtTileset.Size = new System.Drawing.Size(196, 23);
            this.txtTileset.TabIndex = 15;
            // 
            // btn_import
            // 
            this.btn_import.Location = new System.Drawing.Point(147, 368);
            this.btn_import.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_import.Name = "btn_import";
            this.btn_import.Size = new System.Drawing.Size(251, 54);
            this.btn_import.TabIndex = 17;
            this.btn_import.Text = "Importer fond animé";
            this.btn_import.UseVisualStyleBackColor = true;
            this.btn_import.Click += new System.EventHandler(this.btn_import_Click);
            // 
            // openTileSet
            // 
            this.openTileSet.Filter = "Graphic Gals files|*.gal";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 165);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 17);
            this.label1.TabIndex = 18;
            this.label1.Text = "Ancre:";
            // 
            // cmbAnchor
            // 
            this.cmbAnchor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAnchor.FormattingEnabled = true;
            this.cmbAnchor.Items.AddRange(new object[] {
            "Top,Left"});
            this.cmbAnchor.Location = new System.Drawing.Point(199, 165);
            this.cmbAnchor.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbAnchor.Name = "cmbAnchor";
            this.cmbAnchor.Size = new System.Drawing.Size(197, 25);
            this.cmbAnchor.TabIndex = 19;
            // 
            // chkDuplicate
            // 
            this.chkDuplicate.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkDuplicate.Checked = true;
            this.chkDuplicate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkDuplicate.Location = new System.Drawing.Point(27, 225);
            this.chkDuplicate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkDuplicate.Name = "chkDuplicate";
            this.chkDuplicate.Size = new System.Drawing.Size(299, 30);
            this.chkDuplicate.TabIndex = 20;
            this.chkDuplicate.Text = "Dupliquer tiles";
            this.chkDuplicate.UseVisualStyleBackColor = true;
            // 
            // AnimationImporter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(597, 457);
            this.Controls.Add(this.chkDuplicate);
            this.Controls.Add(this.cmbAnchor);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_import);
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.txtTileset);
            this.Controls.Add(this.lbl_fileimport);
            this.Controls.Add(this.cmb_calques);
            this.Controls.Add(this.lbl_nomcalque);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AnimationImporter";
            this.ShowIcon = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Importer un fond animé";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_nomcalque;
        private System.Windows.Forms.ComboBox cmb_calques;
        private System.Windows.Forms.Label lbl_fileimport;
        private System.Windows.Forms.Button btnBrowse;
        public System.Windows.Forms.TextBox txtTileset;
        private System.Windows.Forms.Button btn_import;
        private System.Windows.Forms.OpenFileDialog openTileSet;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbAnchor;
        private System.Windows.Forms.CheckBox chkDuplicate;
    }
}