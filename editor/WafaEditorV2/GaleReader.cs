﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;
using System.Drawing;
using PBInterface;

namespace WafaEditorV2
{
    public class GaleReader
    {
        public const string galeLibPath = "galefile.dll";

        /// <summary>
        /// Opens a gal file. 
        /// </summary>
        /// <param name="apath">Filename of the gal file.</param>
        /// <returns>If the function succeeds, the return value is the address of the gale object.
        /// The gale object must be deleted by ggClose.If the function fails, the return value is NULL.</returns>
        [DllImport(galeLibPath, CallingConvention = CallingConvention.StdCall)]
        static extern IntPtr ggOpen([MarshalAs(UnmanagedType.LPStr)] string apath);


        /// <summary>
        ///   close a gale file.
        /// </summary>
        /// <param name="pFile">The address of the gale object.</param>
        /// <returns> If the function succeeds, the return value is 1.
        ///If the function fails, the return value is 0.</returns>
        [DllImport(galeLibPath, CallingConvention = CallingConvention.StdCall)]
        static extern int ggClose(IntPtr pFile);


        /// <summary>
        ///  Retrieves number of frame.
        /// </summary>
        /// <param name="pFile">The address of the gale object.</param>
        /// <returns> If the function succeeds, the return value is number of 
        ///frame.
        ///If the function fails, the return value is 0.</returns>
        [DllImport(galeLibPath, CallingConvention = CallingConvention.StdCall)]
        static extern UInt32 ggGetFrameCount(IntPtr pFile);


        /// <summary>
        /// Retrieves the name of specified frame.
        /// </summary>
        /// <param name="pFile">The address of the gale object.</param>
        /// <param name="frameNo">The frame index which begin from zero.</param>
        /// <param name="pName">The address of string buffer that receives the null-terminated string specifying the name. If it is NULL, return value is necessary size of buffer.</param>
        /// <param name="len">Specifies the size in byte of the buffer.</param>
        /// <returns>If the function succeeds, the return value is the length in byte, of the string copied to pName. If the function fails, the return value is 0.</returns>
        [DllImport(galeLibPath,CallingConvention=CallingConvention.StdCall)]
        static extern int ggGetFrameName(IntPtr pFile, int frameNo, [MarshalAs(UnmanagedType.LPStr)] StringBuilder pName, int len);


        /// <summary>
        ///  Retrieves information of gale object.
        /// </summary>
        /// <param name="pFile">The address of the gale object.</param>  Parameters: pFile = 
        /// <param name="nID"> Specifies information ID.
        ///     1 = Return value is background-color.
        ///     2 = If the palette is single, return value is 1.
        ///     3 = If the transparency of bottom layer is disabled, 
        ///            return value is 1.
        ///     4 = Return value is bpp(1,4,8,15,16,24).
        ///     5 = Return value is width of image by pixel.
        ///     6 = Return value is height of image by pixel.</param>
        /// <returns>See the Parameters.</returns>
        [DllImport(galeLibPath, CallingConvention = CallingConvention.StdCall)]
        static extern int ggGetInfo(IntPtr pFile, int nID);

        /// <summary>
        /// Retrieves information of specified frame.
        /// </summary>
        /// <param name="pFile">The address of the gale object.</param>
        /// <param name="frameNo">The frame index which begin from zero.</param>
        /// <param name="nID">Specifies information ID. 1 = Return value is the transparent color.
        ///          2 = Return value is the delay by milli-second.
        ///          3 = Return value is the disposal type after display.
        ///               0 = Not specified.
        ///               1 = Not disposed.
        ///               2 = Filled by the background-color.
        ///               3 = Restored to previous state.</param>
        /// <returns></returns>
        [DllImport(galeLibPath, CallingConvention = CallingConvention.StdCall)]
        static extern int ggGetFrameInfo(IntPtr pFile, int frameNo, int nID);

        /// <summary>
        /// Retrieves number of layer.
        /// </summary>
        /// <param name="pFile"> The address of the gale object</param>
        /// <param name="frameNo"> The frame index which begin from zero</param>
        /// <returns>If the function succeeds, the return value is number of layer of specified frame.If the function fails, the return value is 0</returns>
        [DllImport(galeLibPath, CallingConvention = CallingConvention.StdCall)]
        static extern UInt32 ggGetLayerCount(IntPtr pFile, int frameNo);

        /// <summary>
        /// Retrieves information of specified layer.
        /// </summary>
        /// <param name="pFile">The address of the gale object.</param>
        /// <param name="frameNo">The frame index which begin from zero.</param>
        /// <param name="layerNo">The layer index which begin from zero.</param>
        /// <param name="nID">Specifies information ID.   1 = If the layer is visible, return value is 1.
        ///2 = Return value is the transparent color.
        ///3 = Return value is the opacity(0~255).
        ///4 = If the alpha-channel is effective, return value is 1.</param>
        /// <returns> See the Parameters.</returns>
        [DllImport(galeLibPath, CallingConvention = CallingConvention.StdCall)]
        static extern int ggGetLayerInfo(IntPtr pFile,int frameNo,int layerNo,int nID);


        /// <summary>
        /// Retrieves the name of specified layer.
        /// </summary>
        /// <param name="pFile">The address of the gale object.</param>
        /// <param name="frameNo">The frame index which begin from zero.</param>
        /// <param name="layerNo">The layer index which begin from zero.</param>
        /// <param name="pName">The address of string buffer that receives the null-terminated string specifying the name. If it is NULL, return value is necessary size of buffer.</param>
        /// <param name="len">Specifies the size in byte of the buffer.</param>
        /// <returns> If the function succeeds, the return value is the length in byte, of the string copied to pName.If the function fails, the return value is 0.</returns>
        [DllImport(galeLibPath, CallingConvention = CallingConvention.StdCall)]
        static extern int ggGetLayerName(IntPtr pFile, int frameNo, int layerNo, [MarshalAs(UnmanagedType.LPStr)] StringBuilder pName, int len);


        /// <summary>
        ///Retrieves the handle of bitmap of specified frame and 
        ///layer. The handle must not be deleted.
        /// </summary>
        /// <param name="pFile">The address of the gale object.</param>
        /// <param name="frameNo">The frame index which begin from zero.</param>
        /// <param name="layerNo">The layer index which begin from zero.  If it is -1, combined image is retrieved.</param>
        /// <returns> If the function succeeds, the return value is the handle of 
        ///bitmap.
        ///If the function fails, the return value is 0.</returns>
        [DllImport(galeLibPath, CallingConvention = CallingConvention.StdCall)]
        static extern IntPtr ggGetBitmap(IntPtr pFile,int frameNo,int layerNo);


        /// <summary>
        /// Retrieves the handle of bitmap of alpha channel of specified 
        /// frame and layer. The handle must not be deleted.
        /// </summary>
        /// <param name="pFile">The address of the gale object.</param>
        /// <param name="frameNo">The frame index which begin from zero.</param>
        /// <param name="layerNo">The layer index which begin from zero.</param>
        /// <returns>If the function succeeds, the return value is the handle of bitmap. If the function fails, the return value is 0.</returns>
        [DllImport(galeLibPath, CallingConvention = CallingConvention.StdCall)]
        static extern IntPtr ggGetAlphaChannel(IntPtr pFile, int frameNo, int layerNo);

        private IntPtr _galeHandle;

        private string _galFilePath;

        public bool FileOpened
        {
            get
            {
                return (_galeHandle.ToInt32() != 0);
            }
        }


        public GaleReader()
        {


        }

        public void OpenFile(string galeFilePath)
        {
            _galeHandle = ggOpen(galeFilePath);


            if(_galeHandle.ToInt32() == 0)
                throw new IOException("Erreur à l'ouverture du fichier "+galeFilePath);

            _galFilePath = galeFilePath;
        }


        public void CloseFile()
        {
            if (_galeHandle.ToInt32() == 0)
                throw new Exception("Aucun fichier Gal ouvert !");


            if(ggClose(_galeHandle) == 0)
                throw new Exception("Erreur à la fermeture du fichier Gal");
        }

        public uint GetFrameCount()
        {
            if (_galeHandle.ToInt32() == 0)
                throw new Exception("Aucun fichier Gal ouvert !");

            return ggGetFrameCount(_galeHandle);
        }

        public float GetFrameDelay(int indexFrame)
        {
            if (_galeHandle.ToInt32() == 0)
                throw new Exception("Aucun fichier Gal ouvert !");

            return ggGetFrameInfo(_galeHandle, indexFrame, 2);
        }

        public int GetTransparentColor(int indexFrame, int indexLayer)
        {
            if (_galeHandle.ToInt32() == 0)
                throw new Exception("Aucun fichier Gal ouvert !");

            return ggGetLayerInfo(_galeHandle, indexFrame, indexLayer, 2);
        }

        public int GetWidth()
        {
            if (_galeHandle.ToInt32() == 0)
                throw new Exception("Aucun fichier Gal ouvert !");

            return ggGetInfo(_galeHandle, 5);
        }

        public int GetHeight()
        {
            if (_galeHandle.ToInt32() == 0)
                throw new Exception("Aucun fichier Gal ouvert !");

            return ggGetInfo(_galeHandle, 6);
        }

        public Bitmap GetPackedContent(int tilesPerRow, int TILE_SIZE,int tile_height,int tile_space_offset = 0,Color? tile_space_color= null)
        {
             if (_galeHandle.ToInt32() == 0)
                throw new Exception("Aucun fichier Gal ouvert !");


            //pack the tilebitmap to a single bitmap, put 10 tile per line
            int width, height = 0;

            uint tilesCount = ggGetFrameCount(_galeHandle);

            if (tilesPerRow == -1)
                tilesPerRow = (int)tilesCount;

            if (tilesCount <= tilesPerRow)
            {
                width = (int)tilesCount * TILE_SIZE;
                height = tile_height + (tile_space_offset * 2);

                if (tile_space_offset != 0)
                {
                    width += (((int)tilesCount + 1) * tile_space_offset);
                }
            }
            else
            {
                width = tilesPerRow * TILE_SIZE;

                height = (int)Math.Ceiling((double)tilesCount / tilesPerRow) * tile_height;


                if (tile_space_offset != 0)
                {
                    width += ((tilesPerRow + 1) * tile_space_offset);

                    int num_row = (int)Math.Ceiling((double)tilesCount / tilesPerRow);
                    height += ((num_row + 1) * tile_space_offset);
                }

            }

            if (width == 0 || height == 0)
                return null;

            Bitmap tilesetBitmap = new Bitmap(width, height);

            Color FullTransparent = Color.FromArgb(0, Color.White);

            Color backColor = (tile_space_color.HasValue) ? tile_space_color.Value : FullTransparent;



            for (int x = 0; x < width; x++)
                for (int y = 0; y < height; y++)
                    tilesetBitmap.SetPixel(x, y, backColor);


            int tset_x = 0;
            int tset_y = 0;


            for (int iTile = 0; iTile < tilesCount; iTile++)
            {
                if (tile_space_offset != 0)
                    tset_x += tile_space_offset;

                this.FillBitmap(tilesetBitmap, iTile, iTile, TILE_SIZE, tile_height, tilesPerRow, ref tset_y, ref tset_x, FullTransparent, tile_space_offset);

                
            }

            return tilesetBitmap;
        }


        private int FillBitmap(Bitmap bmpToFill, int iFrame, int iGlobalFrame, int TILE_SIZE,int tile_height, int tile_per_row, ref int tset_y, ref int tset_x, Color FullTransparent,int y_offset=0)
        {

            bool CheckForTransparency = true;
            int TransparentColor = ggGetFrameInfo(_galeHandle, 0,1);//we only read the transparency color of the first frame
            Color TransparentrgbColor = new Color();

            if (TransparentColor != -1)
            {
                string hexColor = TransparentColor.ToString("X6");

                TransparentrgbColor = Color.FromArgb(int.Parse(hexColor.Substring(0, 2), System.Globalization.NumberStyles.HexNumber), int.Parse(hexColor.Substring(2, 2), System.Globalization.NumberStyles.HexNumber), int.Parse(hexColor.Substring(4, 2), System.Globalization.NumberStyles.HexNumber));
            }
            else
            {
                CheckForTransparency = false;
            }

            int contact_layer_indx = -1;

            for (int iLayer = 0; iLayer < ggGetLayerCount(_galeHandle, iFrame); iLayer++)
            {
                if (ggGetLayerInfo(_galeHandle, iFrame, iLayer, 1) == 0) //non visible layer,skip
                    continue;

                StringBuilder layer_name = new StringBuilder(200);
                ggGetLayerName(_galeHandle, iFrame, iLayer, layer_name, 200);

                if (layer_name.ToString() == "contact") //contact point layer, skip
                {
                    contact_layer_indx = iLayer;
                    continue;
                }

                IntPtr currentBitmapHandle = ggGetBitmap(_galeHandle, iFrame, iLayer);

                if (currentBitmapHandle.ToInt32() == 0)
                    throw new Exception("Impossible de lire le contenu de la frame " + iFrame.ToString());

                Bitmap tmpBmp = Bitmap.FromHbitmap(currentBitmapHandle);
                Bitmap tmpAlpha = null;

                int Opacity = ggGetLayerInfo(_galeHandle, iFrame, iLayer, 3);

                int has_alpha_channel = ggGetLayerInfo(_galeHandle, iFrame, iLayer, 4);

                if (has_alpha_channel == 1)
                {
                    IntPtr currentAlphaHandle = ggGetAlphaChannel(_galeHandle,iFrame,iLayer);
                    tmpAlpha = Bitmap.FromHbitmap(currentAlphaHandle);
                }

                if ((tset_x + TILE_SIZE) > bmpToFill.Width)
                    tset_x -= TILE_SIZE;

                for (int i = 0; i < TILE_SIZE; i++)
                {
                    int row = (int)Math.Floor((double)(iGlobalFrame / tile_per_row));
                    tset_y = row * tile_height;

                    if (y_offset != 0)
                        tset_y += (y_offset * (row + 1));

                    for (int j = 0; j < tile_height; j++)
                    {
                        Color tmpPixel = tmpBmp.GetPixel(i, j);

                        if (tmpAlpha != null)
                        {
                            Color alpha_channel = tmpAlpha.GetPixel(i, j);
                            tmpPixel = Color.FromArgb(alpha_channel.R, tmpPixel);//we take the red channel for alpha values, with an alpha channel, color channels have the same values
                        }


                        if (CheckForTransparency)
                        {
                            if (tmpPixel.ToArgb() == TransparentrgbColor.ToArgb())
                            {
                                tmpPixel = FullTransparent;
                            }
                            else
                            {
                                if (Opacity != 255)
                                    tmpPixel = Color.FromArgb(Opacity, tmpPixel);
                            }
                        }
                        else
                        {
                            if (Opacity != 255)
                                tmpPixel = Color.FromArgb(Opacity, tmpPixel);
                        }

                        bmpToFill.SetPixel(tset_x, tset_y, tmpPixel);
                        tset_y++;
                    }

                    tset_x++;
                }

                if ((iFrame + 1) % tile_per_row == 0)
                    tset_x = 0;
            }

            return contact_layer_indx;
        }

        public Bitmap GetPackedAnimations(int TILE_SIZE, int tile_height, int offsetx, int offsety, out AnimationDef sAnimDef, int frameOffset, Dictionary<int, string> contact_points_def)
        {
            if (_galeHandle.ToInt32() == 0)
                throw new Exception("Aucun fichier Gal ouvert !");

            uint nbFrames = ggGetFrameCount(_galeHandle);

            sAnimDef = new AnimationDef();

            int width, height = 0;

             width = (int)nbFrames * TILE_SIZE;
             height = tile_height;

            if (width == 0 || height == 0)
                return null;

            Bitmap animationBitmap = new Bitmap(width, height);

            Color FullTransparent = Color.FromArgb(0, Color.White);

            for (int x = 0; x < width; x++)
                for (int y = 0; y < height; y++)
                    animationBitmap.SetPixel(x, y, FullTransparent);

            
            int tset_x = 0;
            int tset_y = 0;


           
            //récupération du nom de l'animation
            StringBuilder pname = new StringBuilder(200);
            ggGetFrameName(_galeHandle, 0, pname, 200);

            sAnimDef.Width = TILE_SIZE;
            sAnimDef.Height = tile_height;
            sAnimDef.Offsetx = offsetx;
            sAnimDef.Offsety = offsety;

            string anim_name = pname.ToString();
            int paramindex = anim_name.LastIndexOf("(");

            sAnimDef.Loop = !(anim_name.EndsWith("(noloop)"));

            if (paramindex != -1)
                sAnimDef.Name = anim_name.Substring(0, paramindex);
            else
                sAnimDef.Name = anim_name;


            Dictionary<string, List<Avector>> contact_points_dict = new Dictionary<string, List<Avector>>();

            int[] newframes = new int[nbFrames];
            float[] newtimesteps = new float[nbFrames];


            for (int indexFrame = 0; indexFrame < nbFrames; indexFrame++)
            {
                int globalFrame = (int)indexFrame;
                
                newframes[indexFrame] = globalFrame + frameOffset;

                //récupération des données de la bitmap
                int contact_layer_indx = this.FillBitmap(animationBitmap, indexFrame, globalFrame, TILE_SIZE, tile_height, (int)nbFrames, ref tset_y, ref tset_x, FullTransparent);

                //récupération du timestep
                int delay = ggGetFrameInfo(_galeHandle, indexFrame, 2);
                newtimesteps[indexFrame] = delay;

                //récupération des positions des points des contacts
                if (contact_layer_indx != -1)
                {

                    IntPtr currentBitmapHandle = ggGetBitmap(_galeHandle, indexFrame, contact_layer_indx);

                    if (currentBitmapHandle.ToInt32() == 0)
                        throw new Exception("Impossible de lire le contenu de la couche de points de contacts");

                    Bitmap tmpContact = Bitmap.FromHbitmap(currentBitmapHandle);

                    for (int xc = 0; xc < tmpContact.Width; xc++)
                    {
                        for (int yc = 0; yc < tmpContact.Height; yc++)
                        {
                            Color pixColor = tmpContact.GetPixel(xc, yc);
                            
                            string hex = pixColor.R.ToString("X2") + pixColor.G.ToString("X2") + pixColor.B.ToString("X2");

                            int hexcolor = int.Parse(hex, System.Globalization.NumberStyles.HexNumber);


                            if (contact_points_def.ContainsKey(hexcolor))
                            {
                                string id = contact_points_def[hexcolor];
                                Avector vec = new Avector();
                                vec.X = xc;
                                vec.Y = tmpContact.Height -  yc;
                                vec.Isnull = false;

                                if (!contact_points_dict.ContainsKey(id))
                                {
                                    List<Avector> new_l = new List<Avector>();
                                    new_l.Add(vec);
                                    contact_points_dict.Add(id, new_l);
                                }
                                else
                                {
                                    contact_points_dict[id].Add(vec);
                                }

                            }
                                

                        }
                    }
                }

                //fill non existing contact points with null values
                foreach (KeyValuePair<int, string> ctp_def in contact_points_def)
                {
                    if(contact_points_dict.ContainsKey(ctp_def.Value))
                    {
                        if (contact_points_dict[ctp_def.Value].Count == indexFrame)
                        {
                            Avector vec = new Avector();
                            vec.Isnull = true;
                            contact_points_dict[ctp_def.Value].Add(vec);
                            
                        }
                    }
                    else
                    {
                        List<Avector> new_l = new List<Avector>();
                        Avector vec = new Avector();
                        vec.Isnull = true;
                        new_l.Add(vec);
                        contact_points_dict.Add(ctp_def.Value,new_l);
                    }
                }

            }

            sAnimDef.Frames = newframes;
            sAnimDef.Timesteps = newtimesteps;

            //add attach points

            foreach (string key_o in contact_points_def.Values)
            {
                  List<Avector> ctp = contact_points_dict[key_o];
                  sAnimDef.AttachpointIds.Add(key_o);

                 if(sAnimDef.Frames.Length != ctp.Count)
                     throw new Exception("Le point de contact '" + key_o + "' n'a pas été définit sur chaque frame de l'animation '" + sAnimDef.Name + "' !");

                 for (int ifra = 0; ifra < sAnimDef.Frames.Length; ifra++)
                 {
                     AnimPoints ap = null;

                     if (sAnimDef.AttachpointValues.Count == ifra)
                     {
                         ap = new AnimPoints();
                         sAnimDef.AttachpointValues.Add(ap);
                     }
                     else
                     {
                         ap = sAnimDef.AttachpointValues[ifra];
                     }

                     ap.Attachpointpos.Add(ctp[ifra]);
                 }
            }
              

            return animationBitmap;

        }

        public Bitmap GetFrame(int frame)
        {
            IntPtr currentBitmapHandle = ggGetBitmap(_galeHandle, frame, 0);

            if (currentBitmapHandle.ToInt32() == 0)
            {
                throw new Exception("Impossible de lire le contenu de la frame " + frame.ToString());
            }

            Bitmap tmpBmp = Bitmap.FromHbitmap(currentBitmapHandle);

            return tmpBmp;
        }


    }

    public class AnimationDataHolder
    {
        public List<string> file_list = new List<string>();
        public Dictionary<int, string> contact_points = new Dictionary<int, string>();
        public List<Avector> sprite_offset = new List<Avector>();

        public AnimationDataHolder()
        {

        }

    }
}
