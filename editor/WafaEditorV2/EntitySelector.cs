﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PBInterface;

namespace WafaEditorV2
{
    public partial class EntitySelector : Form
    {
        public EntitySelector(Level reflevel)
        {
            InitializeComponent();

            cmbEntities.Items.Clear();

            cmbEntities.DisplayMember = "EntityId";

            foreach (Map omap in reflevel.MapArrays)
            {
                foreach(Layer olayer in omap.LayerArrays)
                {
                    if(olayer.Type == (int)TypeLayer.Objects)
                    {
                        cmbEntities.Items.AddRange(olayer.EntityArrays.ToArray());
                    }
                }
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            if (cmbEntities.SelectedItem == null)
            {
                MessageBox.Show("veuillez sélectionner une entité !", "Sélection obligatoire", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
                

            this.Tag = cmbEntities.SelectedItem;

            this.DialogResult = DialogResult.OK;
        }
    }
}
