﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PBInterface;
using System.IO;
using System.Diagnostics;

namespace WafaEditorV2
{
    public partial class TextDialog : Form
    {
        List<ContainerType> result;

        public DialogMode mode { get; set; }

        public Level reflevel { get; set; }

        List<string> ref_module_script;
        string ref_script_base_path;

        private List<ResxInfo> ref_game_resources;

        public TextDialog(Level preflevel, List<ResxInfo> game_resources,List<string> pmodule_script,string script_base_path)
        {
            InitializeComponent();

            reflevel = preflevel;

            ref_game_resources = game_resources;
            ref_module_script = pmodule_script;
            ref_script_base_path = script_base_path;

            if (!string.IsNullOrEmpty(script_base_path))
                cmbScript.Items.AddRange(ref_module_script.ToArray());
            else
            {
                cmbScript.Enabled = false;
                txtCtor.Enabled = false;
            }


            cmbFont.Items.AddRange(ref_game_resources.FindAll(p => p.type == ResxType.font).ToArray());
            cmbFont.DisplayMember = "ident";
            cmbFont.ValueMember = "ident";
            cmbFont.SelectedIndex = 0;

            string[] alignement_types = Enum.GetNames(typeof(PBInterface.TypeAlignement));
            int[] alignement_types_val = (int[])Enum.GetValues(typeof(PBInterface.TypeAlignement));

            result = new List<ContainerType>(alignement_types.Length);

            for (int i = 0; i < alignement_types.Length; i++)
                result.Add(new ContainerType(alignement_types[i], alignement_types_val[i]));

            cbAlignement.Items.AddRange(result.ToArray());
            cbAlignement.DisplayMember = "Key";
            cbAlignement.ValueMember = "Value";
            cbAlignement.SelectedIndex = 3;

        }

        public void FillData(TextObject data)
        {
            mode = DialogMode.Modif;
            this.Tag = data;

            this.Text = Editor.TextModif;

            txtidtexte.Text = data.TextId;
            cmbScript.Text = data.ScriptModule;
            txtCtor.Text = data.ScriptCtor;
            cmbFont.SelectedItem = ref_game_resources.Find(p => p.ident == data.FontName);
            numSizeFont.Value = data.FontSize;
            numwidth.Value = data.Width;
            numheight.Value = data.Height;
            chkCallback.Checked = data.HasCallback;
            rtbContent.Text = data.TextContent;

            num_parallaxx.Value = (decimal)data.ParallaxX;
            num_parallaxy.Value = (decimal)data.ParallaxY;
            chk_cull.Checked = data.NoCulling;
            chknotranslation.Checked = data.NoTranslation;
            chkAntialias.Checked = data.AntiAliasing;
            chkFixed.Checked = data.FixedSize;

            cbAlignement.SelectedItem = result.Find(p => p.Value == data.FontAlign);

            colorFont.Color = Color.FromArgb(data.TextColor.A,data.TextColor.R, data.TextColor.G, data.TextColor.B);

            chk_script_init_only.Checked = data.ScriptInitOnly;
        }

        private void btnColor_Click(object sender, EventArgs e)
        {
            colorFont.ShowDialog();

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            TextObject cEntity = null;

            if (mode == DialogMode.Ajout)
            {
                cEntity = new TextObject();
            }
            else
                cEntity = (TextObject)this.Tag;

            if (string.IsNullOrEmpty(txtidtexte.Text.Trim()))
            {
                MessageBox.Show("L'identifiant de l'objet texte est obligatoire !");
                return;
            }


            if (mode == DialogMode.Ajout)
            {
                if (EditorUtilities.ObjectIdExist(reflevel, txtidtexte.Text.Trim()))
                {
                    MessageBox.Show("Un objet avec cet identifiant existe déjà ! ");
                    return;
                }
            }
            else if (mode == DialogMode.Modif)
            {
                if (EditorUtilities.ObjectIdExist(reflevel, txtidtexte.Text.Trim(), cEntity))
                {
                    MessageBox.Show("Un objet avec cet identifiant existe déjà ! ");
                    return;
                }
            }

            if (cbAlignement.SelectedItem == null)
            {
                MessageBox.Show("Un fichier de font est obligatoire !");
                return;
            }



            cEntity.TextId = txtidtexte.Text;

            cEntity.ScriptModule = cmbScript.Text;
            cEntity.ScriptCtor = txtCtor.Text;

            cEntity.Width = (int)numwidth.Value;
            cEntity.Height = (int)numheight.Value;


            cEntity.HasCallback = chkCallback.Checked;

            cEntity.FontAlign = ((ContainerType)cbAlignement.SelectedItem).Value;
            cEntity.FontName = ((ResxInfo)cmbFont.SelectedItem).ident;
            cEntity.FontSize = (int)numSizeFont.Value;
            cEntity.TextContent = rtbContent.Text;
            cEntity.TextColor = new Wcolor();
            cEntity.TextColor.R = colorFont.Color.R;
            cEntity.TextColor.G = colorFont.Color.G;
            cEntity.TextColor.B = colorFont.Color.B;
            cEntity.TextColor.A = colorFont.Color.A;


            cEntity.ParallaxX = (float)num_parallaxx.Value;
            cEntity.ParallaxY = (float)num_parallaxy.Value;
            cEntity.NoCulling = chk_cull.Checked;
            cEntity.NoTranslation = chknotranslation.Checked;
            cEntity.AntiAliasing = chkAntialias.Checked;
            cEntity.FixedSize = chkFixed.Checked;
            cEntity.ScriptInitOnly = chk_script_init_only.Checked;

            this.Tag = cEntity;

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void TextDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
           
        }

        private void btnNewScript_Click(object sender, EventArgs e)
        {
            //création d'un nouveau module lua
            string lua_content = string.Format(ScriptUtility.LUA_TEMPLATE_MODULE, cmbScript.Text);

            string path = Path.Combine(ref_script_base_path, cmbScript.Text + ".lua");

            if (!File.Exists(path))
            {
                byte[] char_array = Encoding.UTF8.GetBytes(lua_content);
                FileStream stream = File.Create(path, char_array.Length);

                stream.Write(char_array, 0, char_array.Length);
                stream.Close();

                ref_module_script.Add(cmbScript.Text);

                try
                {
                    //ouverture du fichier
                    Process.Start(path);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erreur lors de l'ouverture du fichier, veuillez vérifier l'association des fichiers .lua "+ex.ToString(), "Erreur programme lua", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            else
            {
                MessageBox.Show("Impossible de créer le fichier " + path + " un fichier du même nom existe déjà !", "Fichier existant", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}
