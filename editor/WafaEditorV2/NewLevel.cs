﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PBInterface;
using System.IO;
using WafaEditorV2.Properties;
using System.Diagnostics;

namespace WafaEditorV2
{
    public partial class NewLevel : Form
    {
        DialogMode mode = DialogMode.Ajout;

        public NewLevel()
        {
            InitializeComponent();
        }

        public void Fill(Level data)
        {
            txtidniveau.Text = data.LevelId;
            txtScript.Text = data.EditorScriptFile;
            mode = DialogMode.Modif;
            this.Text = Editor.LevelModif;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            string alert = "L'identifiant du nouveau niveau est obligatoire !";

            if (mode == DialogMode.Modif)
            {
                alert = "L'identifiant du niveau est obligatoire !";
            }

            if (string.IsNullOrEmpty(txtidniveau.Text.Trim()))
            {
                MessageBox.Show(alert);
                return;
            }

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            if (this.openScript.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                txtScript.Text = this.openScript.FileName;
        }

        private void btnNewScript_Click(object sender, EventArgs e)
        {
            //création d'un nouveau module lua

            string lua_content = string.Format(ScriptUtility.LUA_TEMPLATE_SCENE_MODULE, txtidniveau.Text);

            string path = Path.Combine(Settings.Default.raw_assets_path, txtidniveau.Text + ".lua");

            if (!File.Exists(path))
            {
                byte[] char_array = Encoding.UTF8.GetBytes(lua_content);
                FileStream stream = File.Create(path, char_array.Length);

                stream.Write(char_array, 0, char_array.Length);
                stream.Close();

                try
                {
                    txtScript.Text = path;
                    this.openScript.FileName = path;
                    //ouverture du fichier
                    Process.Start(path);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erreur lors de l'ouverture du fichier, veuillez vérifier l'association des fichiers .lua " + ex.ToString(), "Erreur programme lua", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            else
            {
                MessageBox.Show("Impossible de créer le fichier " + path + " un fichier du même nom existe déjà !", "Fichier existant", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void txtidniveau_TextChanged(object sender, EventArgs e)
        {
            btnNewScript.Visible = !string.IsNullOrEmpty(txtidniveau.Text);
        }

   
    }
}
