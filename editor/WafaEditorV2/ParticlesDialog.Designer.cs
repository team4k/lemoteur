﻿namespace WafaEditorV2
{
    partial class ParticlesDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ParticlesDialog));
            this.txtident = new System.Windows.Forms.TextBox();
            this.lblid = new System.Windows.Forms.Label();
            this.lblmodule = new System.Windows.Forms.Label();
            this.chkCollisions = new System.Windows.Forms.CheckBox();
            this.chkConstraint = new System.Windows.Forms.CheckBox();
            this.tabEmitter = new System.Windows.Forms.TabControl();
            this.btnAddEmitter = new System.Windows.Forms.Button();
            this.BtnDel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.cmbScript = new System.Windows.Forms.ComboBox();
            this.btnNewScript = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtident
            // 
            this.txtident.Location = new System.Drawing.Point(227, 46);
            this.txtident.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtident.Name = "txtident";
            this.txtident.Size = new System.Drawing.Size(265, 23);
            this.txtident.TabIndex = 0;
            // 
            // lblid
            // 
            this.lblid.AutoSize = true;
            this.lblid.Location = new System.Drawing.Point(129, 49);
            this.lblid.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblid.Name = "lblid";
            this.lblid.Size = new System.Drawing.Size(69, 17);
            this.lblid.TabIndex = 1;
            this.lblid.Text = "Identifiant";
            // 
            // lblmodule
            // 
            this.lblmodule.AutoSize = true;
            this.lblmodule.Location = new System.Drawing.Point(107, 98);
            this.lblmodule.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblmodule.Name = "lblmodule";
            this.lblmodule.Size = new System.Drawing.Size(92, 17);
            this.lblmodule.TabIndex = 3;
            this.lblmodule.Text = "Module script";
            // 
            // chkCollisions
            // 
            this.chkCollisions.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkCollisions.Location = new System.Drawing.Point(111, 155);
            this.chkCollisions.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkCollisions.Name = "chkCollisions";
            this.chkCollisions.Size = new System.Drawing.Size(157, 30);
            this.chkCollisions.TabIndex = 4;
            this.chkCollisions.Text = "Avec collisions";
            this.chkCollisions.UseVisualStyleBackColor = true;
            this.chkCollisions.CheckedChanged += new System.EventHandler(this.chkCollisions_CheckedChanged);
            // 
            // chkConstraint
            // 
            this.chkConstraint.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkConstraint.Location = new System.Drawing.Point(83, 215);
            this.chkConstraint.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkConstraint.Name = "chkConstraint";
            this.chkConstraint.Size = new System.Drawing.Size(185, 30);
            this.chkConstraint.TabIndex = 5;
            this.chkConstraint.Text = "Contrainte physique";
            this.chkConstraint.UseVisualStyleBackColor = true;
            // 
            // tabEmitter
            // 
            this.tabEmitter.Location = new System.Drawing.Point(16, 319);
            this.tabEmitter.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabEmitter.Name = "tabEmitter";
            this.tabEmitter.SelectedIndex = 0;
            this.tabEmitter.Size = new System.Drawing.Size(680, 373);
            this.tabEmitter.TabIndex = 6;
            this.tabEmitter.SelectedIndexChanged += new System.EventHandler(this.tabEmitter_SelectedIndexChanged);
            // 
            // btnAddEmitter
            // 
            this.btnAddEmitter.Location = new System.Drawing.Point(156, 283);
            this.btnAddEmitter.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAddEmitter.Name = "btnAddEmitter";
            this.btnAddEmitter.Size = new System.Drawing.Size(163, 28);
            this.btnAddEmitter.TabIndex = 7;
            this.btnAddEmitter.Text = "Ajouter émetteur";
            this.btnAddEmitter.UseVisualStyleBackColor = true;
            this.btnAddEmitter.Click += new System.EventHandler(this.btnAddEmitter_Click);
            // 
            // BtnDel
            // 
            this.BtnDel.Location = new System.Drawing.Point(352, 283);
            this.BtnDel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnDel.Name = "BtnDel";
            this.BtnDel.Size = new System.Drawing.Size(141, 28);
            this.BtnDel.TabIndex = 8;
            this.BtnDel.Text = "Supprimer émetteur";
            this.BtnDel.UseVisualStyleBackColor = true;
            this.BtnDel.Click += new System.EventHandler(this.BtnDel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(133, 699);
            this.btnOK.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(163, 28);
            this.btnOK.TabIndex = 9;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(304, 699);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(163, 28);
            this.btnCancel.TabIndex = 10;
            this.btnCancel.Text = "Annuler";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // cmbScript
            // 
            this.cmbScript.FormattingEnabled = true;
            this.cmbScript.Location = new System.Drawing.Point(227, 95);
            this.cmbScript.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbScript.Name = "cmbScript";
            this.cmbScript.Size = new System.Drawing.Size(265, 25);
            this.cmbScript.TabIndex = 75;
            this.cmbScript.TextUpdate += new System.EventHandler(this.cmbScript_TextUpdate);
            // 
            // btnNewScript
            // 
            this.btnNewScript.Image = ((System.Drawing.Image)(resources.GetObject("btnNewScript.Image")));
            this.btnNewScript.Location = new System.Drawing.Point(513, 91);
            this.btnNewScript.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnNewScript.Name = "btnNewScript";
            this.btnNewScript.Size = new System.Drawing.Size(32, 30);
            this.btnNewScript.TabIndex = 74;
            this.btnNewScript.UseVisualStyleBackColor = true;
            this.btnNewScript.Visible = false;
            this.btnNewScript.Click += new System.EventHandler(this.btnNewScript_Click);
            // 
            // ParticlesDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(712, 753);
            this.Controls.Add(this.cmbScript);
            this.Controls.Add(this.btnNewScript);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.BtnDel);
            this.Controls.Add(this.btnAddEmitter);
            this.Controls.Add(this.tabEmitter);
            this.Controls.Add(this.chkConstraint);
            this.Controls.Add(this.chkCollisions);
            this.Controls.Add(this.lblmodule);
            this.Controls.Add(this.lblid);
            this.Controls.Add(this.txtident);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ParticlesDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ajouter un générateur de particules";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtident;
        private System.Windows.Forms.Label lblid;
        private System.Windows.Forms.Label lblmodule;
        private System.Windows.Forms.CheckBox chkCollisions;
        private System.Windows.Forms.CheckBox chkConstraint;
        private System.Windows.Forms.TabControl tabEmitter;
        private System.Windows.Forms.Button btnAddEmitter;
        private System.Windows.Forms.Button BtnDel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ComboBox cmbScript;
        private System.Windows.Forms.Button btnNewScript;
    }
}