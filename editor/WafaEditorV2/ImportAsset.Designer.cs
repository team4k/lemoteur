﻿namespace WafaEditorV2
{
    partial class ImportAsset
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnImport = new System.Windows.Forms.Button();
            this.pbPreview = new System.Windows.Forms.PictureBox();
            this.lblImport = new System.Windows.Forms.Label();
            this.ofdScan = new System.Windows.Forms.OpenFileDialog();
            this.txtAssets = new System.Windows.Forms.TextBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.rtbLog = new System.Windows.Forms.RichTextBox();
            this.btnCrop = new System.Windows.Forms.Button();
            this.btnRedim = new System.Windows.Forms.Button();
            this.txtasset = new System.Windows.Forms.TextBox();
            this.lblnomasset = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbPreview)).BeginInit();
            this.SuspendLayout();
            // 
            // btnImport
            // 
            this.btnImport.Location = new System.Drawing.Point(323, 792);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(95, 23);
            this.btnImport.TabIndex = 0;
            this.btnImport.Text = "Importer";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // pbPreview
            // 
            this.pbPreview.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pbPreview.Location = new System.Drawing.Point(12, 87);
            this.pbPreview.Name = "pbPreview";
            this.pbPreview.Size = new System.Drawing.Size(1087, 543);
            this.pbPreview.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbPreview.TabIndex = 1;
            this.pbPreview.TabStop = false;
            this.pbPreview.Paint += new System.Windows.Forms.PaintEventHandler(this.pbPreview_Paint);
            this.pbPreview.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pbPreview_MouseDown);
            this.pbPreview.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pbPreview_MouseMove);
            this.pbPreview.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pbPreview_MouseUp);
            // 
            // lblImport
            // 
            this.lblImport.AutoSize = true;
            this.lblImport.Location = new System.Drawing.Point(232, 64);
            this.lblImport.Name = "lblImport";
            this.lblImport.Size = new System.Drawing.Size(107, 13);
            this.lblImport.TabIndex = 2;
            this.lblImport.Text = "Sélectionner un scan";
            // 
            // ofdScan
            // 
            this.ofdScan.Filter = "Fichiers TIF|*.tif|Fichiers TGA |*.tga|Fichiers PNG|*.png";
            // 
            // txtAssets
            // 
            this.txtAssets.Location = new System.Drawing.Point(345, 61);
            this.txtAssets.Name = "txtAssets";
            this.txtAssets.ReadOnly = true;
            this.txtAssets.Size = new System.Drawing.Size(363, 20);
            this.txtAssets.TabIndex = 18;
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(715, 59);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(80, 22);
            this.btnBrowse.TabIndex = 19;
            this.btnBrowse.Text = "Parcourir...";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // rtbLog
            // 
            this.rtbLog.Location = new System.Drawing.Point(164, 636);
            this.rtbLog.Name = "rtbLog";
            this.rtbLog.ReadOnly = true;
            this.rtbLog.Size = new System.Drawing.Size(734, 150);
            this.rtbLog.TabIndex = 20;
            this.rtbLog.Text = "";
            // 
            // btnCrop
            // 
            this.btnCrop.Location = new System.Drawing.Point(424, 792);
            this.btnCrop.Name = "btnCrop";
            this.btnCrop.Size = new System.Drawing.Size(142, 23);
            this.btnCrop.TabIndex = 21;
            this.btnCrop.Text = "Rogner selon la sélection";
            this.btnCrop.UseVisualStyleBackColor = true;
            this.btnCrop.Click += new System.EventHandler(this.btnCrop_Click);
            // 
            // btnRedim
            // 
            this.btnRedim.Location = new System.Drawing.Point(572, 792);
            this.btnRedim.Name = "btnRedim";
            this.btnRedim.Size = new System.Drawing.Size(101, 23);
            this.btnRedim.TabIndex = 22;
            this.btnRedim.Text = "Redimensionner...";
            this.btnRedim.UseVisualStyleBackColor = true;
            this.btnRedim.Click += new System.EventHandler(this.btnRedim_Click);
            // 
            // txtasset
            // 
            this.txtasset.Location = new System.Drawing.Point(344, 26);
            this.txtasset.Name = "txtasset";
            this.txtasset.Size = new System.Drawing.Size(363, 20);
            this.txtasset.TabIndex = 23;
            // 
            // lblnomasset
            // 
            this.lblnomasset.AutoSize = true;
            this.lblnomasset.Location = new System.Drawing.Point(231, 29);
            this.lblnomasset.Name = "lblnomasset";
            this.lblnomasset.Size = new System.Drawing.Size(76, 13);
            this.lblnomasset.TabIndex = 24;
            this.lblnomasset.Text = "Nom de l\'asset";
            // 
            // ImportAsset
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1108, 822);
            this.Controls.Add(this.lblnomasset);
            this.Controls.Add(this.txtasset);
            this.Controls.Add(this.btnRedim);
            this.Controls.Add(this.btnCrop);
            this.Controls.Add(this.rtbLog);
            this.Controls.Add(this.txtAssets);
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.lblImport);
            this.Controls.Add(this.pbPreview);
            this.Controls.Add(this.btnImport);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ImportAsset";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Importer un nouvel asset depuis un scan";
            ((System.ComponentModel.ISupportInitialize)(this.pbPreview)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.PictureBox pbPreview;
        private System.Windows.Forms.Label lblImport;
        private System.Windows.Forms.OpenFileDialog ofdScan;
        public System.Windows.Forms.TextBox txtAssets;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.RichTextBox rtbLog;
        private System.Windows.Forms.Button btnCrop;
        private System.Windows.Forms.Button btnRedim;
        private System.Windows.Forms.TextBox txtasset;
        private System.Windows.Forms.Label lblnomasset;
    }
}