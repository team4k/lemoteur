﻿namespace WafaEditorV2
{
    partial class ZoneDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblname = new System.Windows.Forms.Label();
            this.lblstartval = new System.Windows.Forms.Label();
            this.lblendval = new System.Windows.Forms.Label();
            this.lblcolor = new System.Windows.Forms.Label();
            this.lblscriptgen = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnAnnul = new System.Windows.Forms.Button();
            this.txtNom = new System.Windows.Forms.TextBox();
            this.numStart = new System.Windows.Forms.NumericUpDown();
            this.numEnd = new System.Windows.Forms.NumericUpDown();
            this.btnColor = new System.Windows.Forms.Button();
            this.txtFunction = new System.Windows.Forms.TextBox();
            this.colorZone = new System.Windows.Forms.ColorDialog();
            this.cmbZoneType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numEnd)).BeginInit();
            this.SuspendLayout();
            // 
            // lblname
            // 
            this.lblname.AutoSize = true;
            this.lblname.Location = new System.Drawing.Point(71, 59);
            this.lblname.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblname.Name = "lblname";
            this.lblname.Size = new System.Drawing.Size(84, 17);
            this.lblname.TabIndex = 0;
            this.lblname.Text = "Nom zone : ";
            // 
            // lblstartval
            // 
            this.lblstartval.AutoSize = true;
            this.lblstartval.Location = new System.Drawing.Point(70, 146);
            this.lblstartval.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblstartval.Name = "lblstartval";
            this.lblstartval.Size = new System.Drawing.Size(73, 17);
            this.lblstartval.TabIndex = 1;
            this.lblstartval.Text = "Valeur 1 : ";
            // 
            // lblendval
            // 
            this.lblendval.AutoSize = true;
            this.lblendval.Location = new System.Drawing.Point(70, 191);
            this.lblendval.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblendval.Name = "lblendval";
            this.lblendval.Size = new System.Drawing.Size(73, 17);
            this.lblendval.TabIndex = 2;
            this.lblendval.Text = "Valeur 2 : ";
            // 
            // lblcolor
            // 
            this.lblcolor.AutoSize = true;
            this.lblcolor.Location = new System.Drawing.Point(70, 238);
            this.lblcolor.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblcolor.Name = "lblcolor";
            this.lblcolor.Size = new System.Drawing.Size(100, 17);
            this.lblcolor.TabIndex = 3;
            this.lblcolor.Text = "Couleur zone :";
            // 
            // lblscriptgen
            // 
            this.lblscriptgen.AutoSize = true;
            this.lblscriptgen.Location = new System.Drawing.Point(70, 283);
            this.lblscriptgen.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblscriptgen.Name = "lblscriptgen";
            this.lblscriptgen.Size = new System.Drawing.Size(166, 17);
            this.lblscriptgen.TabIndex = 4;
            this.lblscriptgen.Text = "Fonction de génération : ";
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(137, 350);
            this.btnOk.Margin = new System.Windows.Forms.Padding(4);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(100, 28);
            this.btnOk.TabIndex = 5;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnAnnul
            // 
            this.btnAnnul.Location = new System.Drawing.Point(257, 350);
            this.btnAnnul.Margin = new System.Windows.Forms.Padding(4);
            this.btnAnnul.Name = "btnAnnul";
            this.btnAnnul.Size = new System.Drawing.Size(100, 28);
            this.btnAnnul.TabIndex = 6;
            this.btnAnnul.Text = "Annuler";
            this.btnAnnul.UseVisualStyleBackColor = true;
            this.btnAnnul.Click += new System.EventHandler(this.btnAnnul_Click);
            // 
            // txtNom
            // 
            this.txtNom.Location = new System.Drawing.Point(237, 55);
            this.txtNom.Margin = new System.Windows.Forms.Padding(4);
            this.txtNom.Name = "txtNom";
            this.txtNom.Size = new System.Drawing.Size(159, 23);
            this.txtNom.TabIndex = 7;
            // 
            // numStart
            // 
            this.numStart.Location = new System.Drawing.Point(236, 143);
            this.numStart.Margin = new System.Windows.Forms.Padding(4);
            this.numStart.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.numStart.Name = "numStart";
            this.numStart.Size = new System.Drawing.Size(160, 23);
            this.numStart.TabIndex = 8;
            // 
            // numEnd
            // 
            this.numEnd.Location = new System.Drawing.Point(236, 189);
            this.numEnd.Margin = new System.Windows.Forms.Padding(4);
            this.numEnd.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numEnd.Name = "numEnd";
            this.numEnd.Size = new System.Drawing.Size(160, 23);
            this.numEnd.TabIndex = 9;
            // 
            // btnColor
            // 
            this.btnColor.BackColor = System.Drawing.Color.Black;
            this.btnColor.Location = new System.Drawing.Point(236, 232);
            this.btnColor.Margin = new System.Windows.Forms.Padding(4);
            this.btnColor.Name = "btnColor";
            this.btnColor.Size = new System.Drawing.Size(41, 28);
            this.btnColor.TabIndex = 10;
            this.btnColor.UseVisualStyleBackColor = false;
            this.btnColor.Click += new System.EventHandler(this.btnColorZone_Click);
            // 
            // txtFunction
            // 
            this.txtFunction.Location = new System.Drawing.Point(236, 280);
            this.txtFunction.Margin = new System.Windows.Forms.Padding(4);
            this.txtFunction.Name = "txtFunction";
            this.txtFunction.Size = new System.Drawing.Size(159, 23);
            this.txtFunction.TabIndex = 11;
            // 
            // cmbZoneType
            // 
            this.cmbZoneType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbZoneType.FormattingEnabled = true;
            this.cmbZoneType.Location = new System.Drawing.Point(237, 97);
            this.cmbZoneType.Name = "cmbZoneType";
            this.cmbZoneType.Size = new System.Drawing.Size(158, 25);
            this.cmbZoneType.TabIndex = 12;
            this.cmbZoneType.SelectedIndexChanged += new System.EventHandler(this.CmbZoneType_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(74, 97);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 17);
            this.label1.TabIndex = 13;
            this.label1.Text = "Type zone : ";
            // 
            // ZoneDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(485, 415);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbZoneType);
            this.Controls.Add(this.txtFunction);
            this.Controls.Add(this.btnColor);
            this.Controls.Add(this.numEnd);
            this.Controls.Add(this.numStart);
            this.Controls.Add(this.txtNom);
            this.Controls.Add(this.btnAnnul);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.lblscriptgen);
            this.Controls.Add(this.lblcolor);
            this.Controls.Add(this.lblendval);
            this.Controls.Add(this.lblstartval);
            this.Controls.Add(this.lblname);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ZoneDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Détail zone";
            ((System.ComponentModel.ISupportInitialize)(this.numStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numEnd)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblname;
        private System.Windows.Forms.Label lblstartval;
        private System.Windows.Forms.Label lblendval;
        private System.Windows.Forms.Label lblcolor;
        private System.Windows.Forms.Label lblscriptgen;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnAnnul;
        private System.Windows.Forms.TextBox txtNom;
        private System.Windows.Forms.NumericUpDown numStart;
        private System.Windows.Forms.NumericUpDown numEnd;
        private System.Windows.Forms.Button btnColor;
        private System.Windows.Forms.TextBox txtFunction;
        private System.Windows.Forms.ColorDialog colorZone;
        private System.Windows.Forms.ComboBox cmbZoneType;
        private System.Windows.Forms.Label label1;
    }
}