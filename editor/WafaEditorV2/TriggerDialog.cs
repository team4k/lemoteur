﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PBInterface;

namespace WafaEditorV2
{
    public partial class TriggerDialog : Form
    {
        public DialogMode mode { get; set; }

        public Level reflevel { get; set; }

        public TriggerDialog(Level preflevel)
        {
            InitializeComponent();

            mode = DialogMode.Ajout;

            reflevel = preflevel;

        }

        public void FillData(Trigger data)
        {
            txtidtrigger.Text = data.TriggerId;
            txtFunc.Text = data.CallbackFunc;
            chkRepeat.Checked = data.Repeat;
            txtParams.Text = data.Params;
            this.Tag = data;
            mode = DialogMode.Modif;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Trigger cTrig = null;

            if(mode == DialogMode.Ajout)
                cTrig = new Trigger();
            else
                cTrig = (Trigger)this.Tag;

            if (string.IsNullOrEmpty(txtidtrigger.Text.Trim()))
            {
                MessageBox.Show("L'identifiant du trigger est obligatoire !");
                return;
            }

            if (EditorUtilities.ObjectIdExist(reflevel, txtidtrigger.Text.Trim(), cTrig))
            {
                MessageBox.Show("L'identifiant existe déjà pour ce niveau !");
                return;
            }

            cTrig.Repeat = chkRepeat.Checked;
            cTrig.CallbackFunc = txtFunc.Text;
            cTrig.TriggerId = txtidtrigger.Text;
            cTrig.Params = txtParams.Text;

            if(mode == DialogMode.Ajout)
                cTrig.Width = cTrig.Height = 1;

            this.Tag = cTrig;

            this.DialogResult = System.Windows.Forms.DialogResult.OK;

            this.Close();

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        
    }
}
