config = {
start_level="intro.lwf",
fps_limit=60,
show_fps=true,
locale="en",
editor=true,
use_cursor = true,
cursor_follow_scroll=true,
key_move_left = 81,
key_move_right = 68,
key_move_up = 90,
key_move_down = 83,
key_escape = 27,
key_dig = 80,
key_stickwall = 82,
key_retry = 83,
key_dashattack = 84
}