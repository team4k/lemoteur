/**
 * Wafa javascript async loader
 */

function loadJsData(jsfile,fsize,gzsize,canvasid,successFunc,bartext,datatyp)
{
      var xhr = new XMLHttpRequest();
      xhr.open('GET', jsfile, true);
      
      if(datatyp == 'binary')
        xhr.responseType = 'arraybuffer';
        
      xhr.onprogress = function(evt) {
       if (evt.lengthComputable) 
         {
            drawProgressBar(evt.loaded,evt.total,canvasid,bartext);
         }
         else if(fsize != 0)
         {
             if($.browser.mozilla)//somehow firefox report gzipped loaded size while chrome report uncompressed size
                 drawProgressBar(evt.loaded,gzsize,canvasid,bartext);
             else
                drawProgressBar(evt.loaded,fsize,canvasid,bartext);
         }
      }
      xhr.onload = function(event) {
        if(datatyp == 'script')
        {
            var script_element = document.createElement("script"); 
            script_element.type = 'text/javascript';
            script_element.src = jsfile;
            //script_element.text = xhr.response;
            script_element.async = true;
            
            document.getElementsByTagName('head')[0].appendChild(script_element);
        }
        successFunc(xhr.response,'ok',xhr);
      };
      xhr.send(null);
}

function drawProgressBar(progressvalue,progressTotal,canvasid,bartext)
{
	var canvas = document.getElementById(canvasid);
	
	var ctx = canvas.getContext('2d');
	
	var w = canvas.width;
    var h = canvas.height;
	
	var barwidth = 300;
	var barheight = 32;

	var barx = (w / 2) - (barwidth / 2);

	var bary = (h / 2) - (barheight / 2);

	ctx.fillStyle="black";
	ctx.fillRect(0,0,w,h);
	
	ctx.fillStyle="white";
	ctx.font = "20px Georgia";
	ctx.fillText(bartext,barx , (h / 2) - 20  );
	
	var fillbarwidth = barwidth - 8;
        
	var progressbarsize = Math.floor((progressvalue / progressTotal) * fillbarwidth);
	
	ctx.fillStyle="#ffffff";
	ctx.fillRect(barx + 4,bary + 4,progressbarsize,barheight - 8);
	
	ctx.strokeStyle = "#ffffff";
	ctx.lineWidth=2;
	ctx.strokeRect(barx ,bary , barwidth , barheight);
}

function showGame()
{
	document.getElementById('progresscanvas').style.display = 'none';
	document.getElementById('application').style.display = '';
}

function LoadGame(data_name,js_name,game_size,data_size,game_gzip,data_gzip,loader_name)
{
	  loadJsData(data_name,data_size,data_gzip,'progresscanvas',function( data, textStatus, jqxhr ) 
	  {
            loadJsData(loader_name,1000,1000,'progresscanvas',function( data, textStatus, jqxhr ) { 
            
                loadJsData(js_name,game_size,game_gzip,'progresscanvas',function(data, textStatus, jqxhr) {
							drawProgressBar(100,100,'progresscanvas','Launching Game...');
					},'Downloading engine...','script');
             },'Downloading assets...','script');
	 },'Downloading assets...','binary');
					
}