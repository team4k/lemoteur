﻿namespace WafaEditorV2
{
    partial class TriggerDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtidtrigger = new System.Windows.Forms.TextBox();
            this.lblidtrig = new System.Windows.Forms.Label();
            this.chkRepeat = new System.Windows.Forms.CheckBox();
            this.txtFunc = new System.Windows.Forms.TextBox();
            this.lblFunc = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.txtParams = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtidtrigger
            // 
            this.txtidtrigger.Location = new System.Drawing.Point(156, 15);
            this.txtidtrigger.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtidtrigger.Name = "txtidtrigger";
            this.txtidtrigger.Size = new System.Drawing.Size(196, 23);
            this.txtidtrigger.TabIndex = 0;
            // 
            // lblidtrig
            // 
            this.lblidtrig.AutoSize = true;
            this.lblidtrig.Location = new System.Drawing.Point(15, 18);
            this.lblidtrig.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblidtrig.Name = "lblidtrig";
            this.lblidtrig.Size = new System.Drawing.Size(114, 17);
            this.lblidtrig.TabIndex = 5;
            this.lblidtrig.Text = "Identifiant trigger";
            // 
            // chkRepeat
            // 
            this.chkRepeat.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkRepeat.Location = new System.Drawing.Point(15, 126);
            this.chkRepeat.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkRepeat.Name = "chkRepeat";
            this.chkRepeat.Size = new System.Drawing.Size(229, 30);
            this.chkRepeat.TabIndex = 2;
            this.chkRepeat.Text = "Répéter exécution";
            this.chkRepeat.UseVisualStyleBackColor = true;
            // 
            // txtFunc
            // 
            this.txtFunc.Location = new System.Drawing.Point(156, 53);
            this.txtFunc.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtFunc.Name = "txtFunc";
            this.txtFunc.Size = new System.Drawing.Size(196, 23);
            this.txtFunc.TabIndex = 1;
            // 
            // lblFunc
            // 
            this.lblFunc.AutoSize = true;
            this.lblFunc.Location = new System.Drawing.Point(15, 57);
            this.lblFunc.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFunc.Name = "lblFunc";
            this.lblFunc.Size = new System.Drawing.Size(85, 17);
            this.lblFunc.TabIndex = 6;
            this.lblFunc.Text = "Fonction lua";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(193, 174);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 28);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Annuler";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(71, 174);
            this.btnOk.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(100, 28);
            this.btnOk.TabIndex = 3;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // txtParams
            // 
            this.txtParams.Location = new System.Drawing.Point(156, 91);
            this.txtParams.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtParams.Name = "txtParams";
            this.txtParams.Size = new System.Drawing.Size(196, 23);
            this.txtParams.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 95);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 17);
            this.label1.TabIndex = 8;
            this.label1.Text = "paramètres lua";
            // 
            // TriggerDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(383, 233);
            this.Controls.Add(this.txtParams);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.txtFunc);
            this.Controls.Add(this.lblFunc);
            this.Controls.Add(this.chkRepeat);
            this.Controls.Add(this.txtidtrigger);
            this.Controls.Add(this.lblidtrig);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TriggerDialog";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gestion trigger";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtidtrigger;
        private System.Windows.Forms.Label lblidtrig;
        private System.Windows.Forms.CheckBox chkRepeat;
        private System.Windows.Forms.TextBox txtFunc;
        private System.Windows.Forms.Label lblFunc;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.TextBox txtParams;
        private System.Windows.Forms.Label label1;
    }
}