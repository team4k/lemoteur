﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PBInterface;

namespace WafaEditorV2
{
    public partial class LightSourceDialog : Form
    {
        public Level reflevel { get; set; }
        public DialogMode mode { get; set; }

        List<ContainerType> result;

        public LightSourceDialog(Level preflevel)
        {
            reflevel = preflevel;
            InitializeComponent();

            string[] shape_types = Enum.GetNames(typeof(PBInterface.TypeLight));
            int[] shape_types_val = (int[])Enum.GetValues(typeof(PBInterface.TypeLight));

            result = new List<ContainerType>(shape_types.Length);

            for (int i = 0; i < shape_types.Length; i++)
                result.Add(new ContainerType(shape_types[i], shape_types_val[i]));


            cmbTypeLumi.Items.AddRange(result.ToArray());
            cmbTypeLumi.DisplayMember = "Key";
            cmbTypeLumi.ValueMember = "Value";
            cmbTypeLumi.SelectedIndex = 0;

        }

        public void FillData(LightSource data)
        {
            mode = DialogMode.Modif;
            this.Tag = data;
            this.Text = Editor.LumModif;

            txtidlight.Text = data.LightId;
            btnColorLight.BackColor = Color.FromArgb(255, data.LightColor.R, data.LightColor.G, data.LightColor.B);
            colorLight.Color = btnColorLight.BackColor;

            cmbTypeLumi.SelectedItem = result.Find(p => p.Value == data.LType);

            numRotationAngle.Value = (decimal)data.RotationAngle;
            numOpenAngle.Value = (decimal)data.OpenAngle;

            numAlpha.Value = data.LightColor.A;

            numRadius.Value = (decimal)data.Radius;

            numPowerFactor.Value = (decimal)data.PowerFactor;

            numConstant.Value = (decimal)data.Constant;
            numLinear.Value = (decimal)data.Linear;
            numQuadratic.Value = (decimal)data.Quadratic;
        }

        private void btnColorLight_Click(object sender, EventArgs e)
        {
            Button cButton = sender as Button;
            colorLight.Color = cButton.BackColor;

            if (colorLight.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                cButton.BackColor = colorLight.Color;
            }
        }

        private void btnAnnuler_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            LightSource light_source = null;

            if (mode == DialogMode.Ajout)
            {
                light_source = new LightSource();
            }
            else
                light_source = (LightSource)this.Tag;

            if (string.IsNullOrEmpty(txtidlight.Text.Trim()))
            {
                MessageBox.Show("L'identifiant de la source de lumière est obligatoire !");
                return;
            }


            if (mode == DialogMode.Ajout)
            {
                if (EditorUtilities.ObjectIdExist(reflevel, txtidlight.Text.Trim()))
                {
                    MessageBox.Show("Un objet avec cet identifiant existe déjà ! ");
                    return;
                }
            }
            else if (mode == DialogMode.Modif)
            {
                if (EditorUtilities.ObjectIdExist(reflevel, txtidlight.Text.Trim(), light_source))
                {
                    MessageBox.Show("Un objet avec cet identifiant existe déjà ! ");
                    return;
                }
            }

            Color l_col = Color.FromArgb((int)numAlpha.Value, colorLight.Color);

            light_source.LightId = txtidlight.Text;
            light_source.LightColor = new Wcolor();

            light_source.LightColor.R = l_col.R;
            light_source.LightColor.G = l_col.G;
            light_source.LightColor.B = l_col.B;
            light_source.LightColor.A = l_col.A;

            light_source.LType= ((ContainerType)cmbTypeLumi.SelectedItem).Value;
            light_source.RotationAngle = (float)numRotationAngle.Value;
            light_source.OpenAngle = (float)numOpenAngle.Value;
                
            light_source.Radius = (float)numRadius.Value;
            light_source.PowerFactor = (float)numPowerFactor.Value;

            light_source.Constant = (float)numConstant.Value;
            light_source.Linear = (float)numLinear.Value;
            light_source.Quadratic = (float)numQuadratic.Value;

            this.Tag = light_source;

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void cmbTypeLumi_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cmbTypeLumi.SelectedIndex != -1)
                numRotationAngle.Enabled = numOpenAngle.Enabled = (((ContainerType)cmbTypeLumi.SelectedItem).Value == (int)TypeLight.LDirectional);
        }
    }
}
