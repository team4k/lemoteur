﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PBInterface;

namespace WafaEditorV2
{
    public partial class FragmentDetail : Form
    {
        public FragmentDetail()
        {
            InitializeComponent();
        }

        public void Fill(Fragment ofrag)
        {
            this.txtidfragment.Text = ofrag.FragId;
            this.txtScript.Text = ofrag.GameScriptPath;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtidfragment.Text.Trim()))
            {
                MessageBox.Show("L'identifiant du fragment est obligatoire !");
                return;
            }

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                this.txtScript.Text = folderBrowserDialog1.SelectedPath;
            }
        }
    }
}
