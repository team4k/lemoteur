﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PBInterface;
using System.IO;
using System.Diagnostics;

namespace WafaEditorV2
{
    public partial class ParticlesDialog : Form
    {

        List<string> ref_module_script;
        string ref_script_base_path;

        private Level ref_level;
        public DialogMode mode { get; set; }

        private ParticleEmitter currentEmitterControl = null;

        public ParticlesDialog(Level olevel, List<string> pmodule_script,string script_base_path)
        {
            InitializeComponent();

            ref_script_base_path = script_base_path;
            ref_level = olevel;

            ref_module_script = pmodule_script;

            if (!string.IsNullOrEmpty(script_base_path))
                cmbScript.Items.AddRange(ref_module_script.ToArray());
            else
            {
                cmbScript.Enabled = false;
            }


            mode = DialogMode.Ajout;

        }

        public void FillData(ParticlesData data)
        {
            this.Tag = data;
            cmbScript.Text = data.ScriptModule;
            txtident.Text = data.Id;
            chkCollisions.Checked = data.UsePhysics;
            chkConstraint.Checked = data.ConstraintParticle;

            //load emitters data
            foreach (PBInterface.ParticleEmitter emitter in data.Emitters)
                this.CreateEmitterPage(emitter);

            mode = DialogMode.Modif;
        }

        public void CreateEmitterPage(PBInterface.ParticleEmitter data = null)
        {
            TabPage tab = new TabPage("Emitter "+(tabEmitter.TabPages.Count+1));

            ParticleEmitter emitter_control = new ParticleEmitter();
            emitter_control.Name = "emitterUC";

            if (data != null)
                emitter_control.FillData(data);

            tab.Controls.Add(emitter_control);

            tabEmitter.TabPages.Add(tab);
        }

        public void RemoveCurrentEmitterPage()
        {
            int tabpos = tabEmitter.SelectedIndex;
            tabEmitter.TabPages.Remove(tabEmitter.SelectedTab);
        }


        private void chkCollisions_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void btnNewScript_Click(object sender, EventArgs e)
        {
            //création d'un nouveau module lua

            string lua_content = string.Format(ScriptUtility.LUA_TEMPLATE_MODULE, cmbScript.Text);

            string path = Path.Combine(ref_script_base_path, cmbScript.Text + ".lua");

            if (!File.Exists(path))
            {
                byte[] char_array = Encoding.UTF8.GetBytes(lua_content);
                FileStream stream = File.Create(path, char_array.Length);

                stream.Write(char_array, 0, char_array.Length);
                stream.Close();

                ref_module_script.Add(cmbScript.Text);

                try
                {
                    //ouverture du fichier
                    Process.Start(path);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erreur lors de l'ouverture du fichier, veuillez vérifier l'association des fichiers .lua " + ex.ToString(), "Erreur programme lua", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            else
            {
                MessageBox.Show("Impossible de créer le fichier " + path + " un fichier du même nom existe déjà !", "Fichier existant", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        private void cmbScript_TextUpdate(object sender, EventArgs e)
        {
            btnNewScript.Visible = (!string.IsNullOrEmpty(cmbScript.Text) && !ref_module_script.Contains(cmbScript.Text));
        }

        private void btnAddEmitter_Click(object sender, EventArgs e)
        {
            this.CreateEmitterPage();
        }

        private void BtnDel_Click(object sender, EventArgs e)
        {
            this.RemoveCurrentEmitterPage();
        }

        private void tabEmitter_SelectedIndexChanged(object sender, EventArgs e)
        {
            //save the previous emitter control
            if (currentEmitterControl != null)
                currentEmitterControl.TmpSaveData();

            //find the emitter control
            Control[] emit_find = tabEmitter.SelectedTab.Controls.Find("emitterUC",true);

            if (emit_find != null && emit_find.Length > 0 && emit_find[0] is ParticleEmitter)
            {
                ParticleEmitter emitcontrol = emit_find[0] as ParticleEmitter;
                currentEmitterControl = emitcontrol;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            ParticlesData cParticles = null;

            if (mode == DialogMode.Ajout)
                cParticles = new ParticlesData();
            else
                cParticles = (ParticlesData)this.Tag;

            if (string.IsNullOrEmpty(txtident.Text))
            {
                MessageBox.Show("L'identifiant de l'objet de particules est obligatoire !");
                return;
            }

            if (tabEmitter.TabPages.Count == 0)
            {
                MessageBox.Show("L'objet de particules doit contenir au moins 1 émetteur !");
                return;
            }

            if (mode == DialogMode.Ajout)
            {
                if (EditorUtilities.ObjectIdExist(ref_level, txtident.Text.Trim()))
                {
                    MessageBox.Show("cet identifiant existe déjà ! ");
                    return;
                }
            }
            else if (mode == DialogMode.Modif)
            {
                if (EditorUtilities.ObjectIdExist(ref_level, txtident.Text.Trim(), cParticles))
                {
                    MessageBox.Show("cet identifiant existe déjà ! ");
                    return;
                }
            }


            if (currentEmitterControl != null)
                currentEmitterControl.TmpSaveData();

            cParticles.Id = txtident.Text;
            cParticles.ScriptModule = cmbScript.Text;
            cParticles.UsePhysics = chkCollisions.Checked;
            cParticles.ConstraintParticle = chkConstraint.Checked;

            cParticles.Emitters.Clear();

            foreach (TabPage tabs in tabEmitter.TabPages)
            {
                Control[] emit_find = tabs.Controls.Find("emitterUC", true);

                if (emit_find != null && emit_find.Length > 0 && emit_find[0] is ParticleEmitter)
                {
                    ParticleEmitter emitcontrol = emit_find[0] as ParticleEmitter;
                    cParticles.Emitters.Add(emitcontrol.GetData());
                }
            }



            this.Tag = cParticles;

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
    }
}
