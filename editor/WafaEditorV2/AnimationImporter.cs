﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PBInterface;
using WafaEditorV2.Properties;
using System.Security.Cryptography;
using System.IO;

namespace WafaEditorV2
{
    public partial class AnimationImporter : Form
    {
        List<Layer> _layer_list_ref;
        Map ref_map;
        Dictionary<string, Bitmap> _ref_tilesets;
        Dictionary<string, Bitmap> _view_tileset;

        public AnimationImporter(List<Layer> layer_list,Map oMap,Dictionary<string,Bitmap> ref_tilesets,Dictionary<string,Bitmap> view_tilesets)
        {
            InitializeComponent();

            cmb_calques.Items.AddRange(layer_list.FindAll(p => p.Type == (int)TypeLayer.Normal).ToArray());
            cmb_calques.DisplayMember = "LayerId";
            cmbAnchor.SelectedIndex = 0;
            _layer_list_ref = layer_list;
            ref_map = oMap;
            _ref_tilesets = ref_tilesets;
            _view_tileset =view_tilesets;
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            if (this.openTileSet.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                txtTileset.Text = this.openTileSet.FileName;
        }

        private void btn_import_Click(object sender, EventArgs e)
        {

            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;

            Layer selected_layer = _layer_list_ref.Find(p => p.LayerId == cmb_calques.Text) ;

            if (selected_layer != null)
            {
                if (MessageBox.Show(string.Format("Importer le fond animé sur le calque {0} ? Cela remplacera les tiles existantes de ce calque",selected_layer.LayerId), "Importer fond", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return;

            }
            else
            {
                if (MessageBox.Show(string.Format("Le calque {0} n'existe pas, le créer ?", cmb_calques.Text), "Importer fond", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                    return;
            }


            if (string.IsNullOrEmpty(txtTileset.Text))
            {
                if(MessageBox.Show("Aucun fichier à importer !", "Aucun fichier", MessageBoxButtons.OK, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.OK)
                    return;
            }

            //chargement du fichier gale

            GaleReader oGaleReader = new GaleReader();
            oGaleReader.OpenFile(txtTileset.Text);

           

            if ((oGaleReader.GetWidth() / Settings.Default.TILE_SIZE) > ref_map.ColCount || (oGaleReader.GetHeight() / Settings.Default.TILE_SIZE) > ref_map.RowCount)
            {
                if (MessageBox.Show(string.Format("L'image sélectionnée est trop grande pour être importée augmenter la taille de la tilemap !", Settings.Default.TILE_SIZE), "Taille incorrecte", MessageBoxButtons.OK, MessageBoxIcon.Error) == System.Windows.Forms.DialogResult.OK)
                    return;
            }

            int diffCol = (oGaleReader.GetWidth() / Settings.Default.TILE_SIZE) - ref_map.ColCount;
            int diffRow =  (oGaleReader.GetHeight() / Settings.Default.TILE_SIZE) - ref_map.RowCount;

            int anchor = cmbAnchor.SelectedIndex;


            uint framecount = oGaleReader.GetFrameCount();

            bool no_animations = (framecount == 1);

            if (framecount == 1)
                MessageBox.Show("Une seule frame existante dans le fichier sélectionné, aucune animation ne sera importée", "Aucune animation", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            Dictionary<string, TileInf> tile_hash = new Dictionary<string, TileInf>();

            List<int[]> frame_tile_data = new List<int[]>();

            int pixelSize = 4;

            Color TransparentrgbColor = new Color();

            int TransparentColor = oGaleReader.GetTransparentColor(0, 0);
            bool CheckForTransparency = false;

            if (TransparentColor != -1)
            {
                string hexColor = TransparentColor.ToString("X6");

                TransparentrgbColor = Color.FromArgb(int.Parse(hexColor.Substring(0, 2), System.Globalization.NumberStyles.HexNumber), int.Parse(hexColor.Substring(2, 2), System.Globalization.NumberStyles.HexNumber), int.Parse(hexColor.Substring(4, 2), System.Globalization.NumberStyles.HexNumber));
                CheckForTransparency = true;
            }

            Color FullTransparent = Color.FromArgb(0, Color.White);

            int final_tileset_index = 0;

            int num_tiles = (ref_map.RowCount * ref_map.ColCount);

            int widthintiles = (int)Math.Floor((double)oGaleReader.GetWidth() / Settings.Default.TILE_SIZE);
            int heightintile = (int)Math.Floor((double)oGaleReader.GetHeight() / Settings.Default.TILE_SIZE);

            int widthtilesdiff = ref_map.ColCount - widthintiles;

            string fullTransparentHash = string.Empty;

            for (int i = 0; i < framecount; i++)
            {
                //découpage de notre frame en tile 32x32
                Bitmap frame = oGaleReader.GetFrame(i);

                int[] tile_data = new int[num_tiles];

                int globalx = 0;
                int globaly = 0;


                for (int t_index = 0; t_index < num_tiles; t_index++)
                {
                    if (globalx >= oGaleReader.GetWidth() || globaly >= oGaleReader.GetHeight() )
                    {
                        //au delà des limite de la bitmap, on duplique les info de tiles

                        if (chkDuplicate.Checked)
                        {
                            if (globaly < oGaleReader.GetHeight())
                                tile_data[t_index] = tile_data[t_index - widthintiles];
                            else
                                tile_data[t_index] = tile_data[t_index - (ref_map.ColCount * heightintile)];
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(fullTransparentHash))
                                tile_data[t_index] = tile_hash[fullTransparentHash].position;
                            else
                                tile_data[t_index] = 0;
                        }

                        if (globalx < ((ref_map.ColCount * Settings.Default.TILE_SIZE) - Settings.Default.TILE_SIZE))
                        {
                            globalx += Settings.Default.TILE_SIZE;
                        }
                        else
                        {
                            globalx = 0;
                            globaly += Settings.Default.TILE_SIZE;
                        }

                        continue;
                    }


                    int iBmp = 0;
                    byte[] tmp_bytes = new byte[(Settings.Default.TILE_SIZE * Settings.Default.TILE_SIZE) * pixelSize];

                    bool isFullTransparent = true;

                    for (int x = globalx; x < (globalx + Settings.Default.TILE_SIZE); x++)
                    {
                        for (int y = (globaly + Settings.Default.TILE_SIZE) - 1; y >= globaly; y--)
                        {
                            Color tile_color = frame.GetPixel(x, (oGaleReader.GetHeight() - 1) - y);

                            if (CheckForTransparency)
                            {
                                if (tile_color.ToArgb() == TransparentrgbColor.ToArgb())
                                    tile_color = FullTransparent;
                                else
                                    isFullTransparent = false;
                            }
                            else
                                isFullTransparent = false;


                            tmp_bytes[iBmp] = tile_color.R;
                            tmp_bytes[iBmp + 1] = tile_color.G;
                            tmp_bytes[iBmp + 2] = tile_color.B;
                            tmp_bytes[iBmp + 3] = tile_color.A;
                            iBmp += pixelSize;
                        }
                    }

               

                    //vérification par rapport au dictionnaire de tile pour vérifier si la tile existe déjà
                    SHA256Managed hashstring = new SHA256Managed();
                    byte[] hash = hashstring.ComputeHash(tmp_bytes);
                    string hashString = string.Empty;

                    foreach (byte x in hash)
                        hashString += String.Format("{0:x2}", x);

                    TileInf inf  = null;

                    //remplissage du tableau d'index de tile de la frame
                    if (tile_hash.ContainsKey(hashString))
                    {
                        inf = tile_hash[hashString];
                    }
                    else
                    {

                        inf = new TileInf(tmp_bytes,final_tileset_index++);
                        tile_hash.Add(hashString,inf);

                        if (isFullTransparent)
                        {
                            fullTransparentHash = hashString;
                            int transpos = inf.position;
                            inf.position = 0;

                            //swap
                            foreach (TileInf oinf in tile_hash.Values)
                            {
                                if (oinf.position == 0)
                                {
                                    oinf.position = transpos;
                                    break;
                                }
                            }

                            foreach (int[] o_tile_data in frame_tile_data)
                            {
                                for (int i_t = 0; i_t < o_tile_data.Length; i_t++)
                                {
                                    if(o_tile_data[i_t] == 0)
                                        o_tile_data[i_t] = transpos;
                                }
                            }

                        }
                    }

                    tile_data[t_index] = inf.position;




                    if (globalx < ((ref_map.ColCount * Settings.Default.TILE_SIZE) - Settings.Default.TILE_SIZE))
                    {
                        globalx += Settings.Default.TILE_SIZE;
                    }
                    else
                    {
                        globalx = 0;
                        globaly += Settings.Default.TILE_SIZE;
                    }
                }

                frame_tile_data.Add(tile_data);

            }

            //génération tileset depuis notre dictionnaire de tile
            List<Bitmap> tile_bitmapArray = new List<Bitmap>(tile_hash.Count);

            List<TileInf> tile_infs = new List<TileInf>(tile_hash.Count);

            tile_infs.AddRange(tile_hash.Values.ToArray<TileInf>());

            tile_infs = (from u in tile_infs orderby u.position ascending select u).ToList<TileInf>();

            foreach(TileInf inf in tile_infs)
            {
                Bitmap cBitmap = new Bitmap(Settings.Default.TILE_SIZE,Settings.Default.TILE_SIZE);

                int iBmp = 0;

                for (int x = 0; x < Settings.Default.TILE_SIZE; x++)
                {
                    for (int y = 0; y < Settings.Default.TILE_SIZE; y++)
                    {
                        Color nColor = Color.FromArgb(inf.content[iBmp + 3],inf.content[iBmp],inf.content[iBmp + 1], inf.content[iBmp + 2]);
                        iBmp += pixelSize;
                        cBitmap.SetPixel(x,y,nColor);
                    }
                }

                tile_bitmapArray.Add(cBitmap);
            }


            int mergedWidth = 0;
            int mergedHeight = 0;

            if (tile_hash.Count < Settings.Default.NUMCOL_TILES_IMPORT)
            {
                mergedWidth = Settings.Default.TILE_SIZE * tile_hash.Count;
                mergedHeight = Settings.Default.TILE_SIZE;
            }
            else
            {
                mergedWidth = Settings.Default.TILE_SIZE * Settings.Default.NUMCOL_TILES_IMPORT;
                mergedHeight = ((int)Math.Ceiling((double)tile_hash.Count / Settings.Default.NUMCOL_TILES_IMPORT)) * Settings.Default.TILE_SIZE;
            }

           Bitmap tileset_bitmap  = null;



           if (tile_bitmapArray.Count > 1)
               tileset_bitmap = BitmapUtilities.MergeBitmaps(tile_bitmapArray.ToArray(), mergedWidth, mergedHeight, Settings.Default.TILE_SIZE, Settings.Default.TILE_SIZE);
           else
               tileset_bitmap = tile_bitmapArray[0];

           string outfilename = Path.Combine(Settings.Default.raw_assets_path, Path.GetFileNameWithoutExtension(txtTileset.Text) + ".png");
             //  .Substring(0,txtTileset.Text.LastIndexOf('.')) + ".png";

            //génération de notre couche avec les informations d'animations
            bool create_native_layer = false;

            if (selected_layer == null)
            {
                selected_layer = new Layer();
                selected_layer.Destructible = false;
                selected_layer.Order = _layer_list_ref.Count(p => p.Type == (int)TypeLayer.Normal);
                selected_layer.EditorOrder = _layer_list_ref.Count;
                selected_layer.Type = (int)TypeLayer.Normal;
                selected_layer.ParallaxX = 1.0f;
                selected_layer.ParallaxY = 1.0f;
                selected_layer.Opacity = 1.0f;
                selected_layer.LayerId = cmb_calques.Text;
                selected_layer.TilesetId = outfilename;
                create_native_layer = true;
            }

            selected_layer.TilesetId = outfilename;
            selected_layer.Animated = !no_animations;

            //on remet tout à zéro
            selected_layer.Frames.Clear();

            int[] ref_data = frame_tile_data[0];

            int map_size = (ref_map.ColCount * ref_map.RowCount);

            selected_layer.TileArrays = frame_tile_data[0];

           /* int left = map_size - ref_data.Length;

            if (left > 0)//la nouvelle couche à moins de tile que la map actuelle
            {
                //foreach(

                //while (left-- > 0)
                //{
                //  selected_layer.tile_array.Add(0);
                //}
            }
            else
            {
                
            }*/

            if (selected_layer.TileArrays.Length != map_size)
                throw new Exception("Différence entre la taille de la nouvelle couche et la taille de la tilemap");

            //génération info d'animations
            if (!no_animations)
            {
                for (int i = 1; i < frame_tile_data.Count; i++)
                {
                    TilemapFrame cFrame = new TilemapFrame();
                    cFrame.TimeStep = oGaleReader.GetFrameDelay(i);

                    //récupération des mises à jour de la tilemap à effectuer
                    for (int verif = 0; verif < map_size; verif++)
                    {
                        if (ref_data[verif] != frame_tile_data[i][verif])
                        {
                            TileInfo info = new TileInfo();
                            info.Index = verif;
                            info.IdTile = frame_tile_data[i][verif];
                            cFrame.TileUpdates.Add(info);
                        }
                    }

                    selected_layer.Frames.Add(cFrame);


                    ref_data = frame_tile_data[i];
                }
            }

            //upload nouvelle texture
            if (NativeFunc.editor_resx_exist(outfilename) == NativeFunc.wbool.wfalse)
            {
                NativeFunc.editor_addtextureresx(outfilename, tileset_bitmap.Width, tileset_bitmap.Height, BitmapUtilities.GetBitmapContent(tileset_bitmap), (uint)((tileset_bitmap.Width * tileset_bitmap.Height) * pixelSize));
            }

            //   NativeFunc.editor_updatetextureresx(outfilename, tileset_bitmap.Width, tileset_bitmap.Height, BitmapUtilities.GetBitmapContent(tileset_bitmap), (uint)((tileset_bitmap.Width * tileset_bitmap.Height) * pixelSize));

            tileset_bitmap.Save(outfilename);

            Bitmap editor_bitmap = new Bitmap(tileset_bitmap);

            if (!_ref_tilesets.ContainsKey(selected_layer.TilesetId))
                _ref_tilesets.Add(selected_layer.TilesetId, editor_bitmap);

            if (!_view_tileset.ContainsKey(selected_layer.TilesetId))
                _view_tileset.Add(selected_layer.TilesetId, editor_bitmap);


            tileset_bitmap.Dispose();

           
            //creation nouvelle frame
            MemoryStream layerStream = new MemoryStream();
            layerStream.Position = 0;
            ProtoBuf.Serializer.Serialize<Layer>(layerStream, selected_layer);

            layerStream.Position = 0;

            byte[] tmplayerArray = layerStream.ToArray();

            layerStream.Close();

            if (create_native_layer)
            {
                NativeFunc.editor_addlayer(tmplayerArray, (uint)tmplayerArray.Length);
                this.Tag = selected_layer;
            }
            else
            {
                NativeFunc.editor_update_layer(tmplayerArray, (uint)tmplayerArray.Length);
                NativeFunc.editor_update_layercontent(tmplayerArray, (uint)tmplayerArray.Length);
                this.Tag = null;
            }

            foreach (Bitmap obmp in tile_bitmapArray)
            {
                //if(obmp != tileset_bitmap)
                   obmp.Dispose();
            }

            tile_bitmapArray.Clear();
            oGaleReader.CloseFile();


            MessageBox.Show("Import animation terminée !");

            this.DialogResult = System.Windows.Forms.DialogResult.OK;

        }
    }

    public class TileInf
    {
        public byte[] content;
        public int position;

        public TileInf(byte[] p_content, int p_pos)
        {
            content = p_content;
            position = p_pos;
        }
    }
}
