﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PBInterface;

namespace WafaEditorV2
{
    public partial class ZoneDetail : Form
    {
        public DialogMode mode { get; set; }
        public World refworld { get; set; }

        public ZoneSlice parent { get; set; }

        public ZoneDetail()
        {
            InitializeComponent();

            string[] values = Enum.GetNames(typeof(TypeZone));

            foreach(string val in values)
            {
                cmbZoneType.Items.Add(val);
            }
        }

        public void FillForm(ZoneSlice data)
        {
            txtNom.Text = data.Name;
            txtFunction.Text = data.ScriptGenerator;
            numStart.Value = Convert.ToDecimal(data.StartValue);
            numEnd.Value = Convert.ToDecimal(data.EndValue);
            btnColor.BackColor = Color.FromArgb(255, data.Color.R, data.Color.G, data.Color.B);

            cmbZoneType.SelectedItem = Enum.GetName(typeof(TypeZone), (TypeZone)data.TypeZone);
        }

        private void btnColorZone_Click(object sender, EventArgs e)
        {
            Button cButton = sender as Button;
            colorZone.Color = cButton.BackColor;

            if (colorZone.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                cButton.BackColor = colorZone.Color;
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            ZoneSlice source = null;

            if (mode == DialogMode.Ajout)
            {
                source = new ZoneSlice();
            }
            else
            {
                source = (ZoneSlice)this.Tag;
            }

            source.StartValue = (int)numStart.Value;
            source.EndValue = (int)numEnd.Value;

            string tmptype = cmbZoneType.SelectedItem.ToString();

            TypeZone zn = TypeZone.ZRange;

            if (Enum.TryParse<TypeZone>(tmptype, out zn))
            {
                source.TypeZone = (int)zn;
            }
            else
            {
                if (source.StartValue == source.EndValue)
                    source.TypeZone = (int)TypeZone.ZSpec;
                else
                    source.TypeZone = (int)TypeZone.ZRange;
            }


            if (string.IsNullOrEmpty(txtNom.Text.Trim()))
            {
                MessageBox.Show("L'identifiant de la zone est obligatoire !");
                return;
            }

            string ignoreid = txtNom.Text.Trim();

            if (mode == DialogMode.Ajout)
            {
                if (EditorUtilities.ZoneExist(refworld, txtNom.Text.Trim()))
                {
                    MessageBox.Show("Une zone avec cet identifiant existe déjà ! ");
                    return;
                }
            }
            else if (mode == DialogMode.Modif)
            {
                ignoreid = source.Name;

                if (EditorUtilities.ZoneExist(refworld, txtNom.Text.Trim(), source))
                {
                    MessageBox.Show("Une zone avec cet identifiant existe déjà ! ");
                    return;
                }
            }

            if (source.TypeZone == (int)TypeZone.ZRange && (int)numStart.Value > (int)numEnd.Value)
            {
                MessageBox.Show("La valeur 1 doit être inférieure ou égale à la valeur 2");
                return;
            }


            if(parent != null && (parent.StartValue > (int)numStart.Value || parent.EndValue < (int)numEnd.Value))
            {
                MessageBox.Show(string.Format("Les valeurs 1 et 2 doivent être comprise dans l'intervale {0} - {1} de la zone parente",parent.StartValue, parent.EndValue));
                return;
            }


            if(parent == null)
            {
                List<ZoneSlice> slices = (from u in refworld.SliceDatas where u.StartValue <= (int)numStart.Value && u.EndValue >= (int)numStart.Value && u.Name != ignoreid select u).ToList<ZoneSlice>();
                //vérif interval existant
                if (slices.Count > 0)
                {
                    string slices_str = "";
                    foreach (ZoneSlice slc in slices)
                    {
                        slices_str += string.Format("{0} {1} - {2} , ", slc.Name, slc.StartValue, slc.EndValue);
                    }

                    MessageBox.Show("La valeur 1 est comprise dans un intervale existant " + slices_str);
                    return;
                }

                if(source.TypeZone == (int)TypeZone.ZRange)
                {
                    List<ZoneSlice> slicesend = (from u in refworld.SliceDatas where u.StartValue <= (int)numEnd.Value && u.EndValue >= (int)numEnd.Value && u.Name != ignoreid select u).ToList<ZoneSlice>();
                    //vérif interval existant
                    if (slicesend.Count > 0)
                    {
                        string slices_str = "";
                        foreach (ZoneSlice slc in slicesend)
                        {
                            slices_str += string.Format("{0} {1} - {2} , ", slc.Name, slc.StartValue, slc.EndValue);
                        }

                        MessageBox.Show("La valeur 2 est comprise dans un intervale existant " + slices_str);
                        return;
                    }
                }
                
            }
            


            if(source.TypeZone != (int)TypeZone.ZRange)
            {
                source.EndValue = source.StartValue;
            }


            source.Name = txtNom.Text;
            source.Color = new Wcolor();

            source.Color.R = btnColor.BackColor.R;
            source.Color.G = btnColor.BackColor.G;
            source.Color.B = btnColor.BackColor.B;
            source.Color.A = 255;

            source.ScriptGenerator = txtFunction.Text;


            if(parent != null)
            {
                source.ParentZone = parent.Name;
            }



            this.Tag = source;

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void btnAnnul_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CmbZoneType_SelectedIndexChanged(object sender, EventArgs e)
        {
            string val = cmbZoneType.SelectedItem.ToString();

            TypeZone zn = TypeZone.ZRange;

            if(Enum.TryParse<TypeZone>(val, out zn))
            {
                numEnd.Enabled = (zn == TypeZone.ZRange);
                lblscriptgen.Text = (zn == TypeZone.ZStaticlevel) ? "Niveau à charger" : "Fonction de génération";
            }
        }
    }
}
