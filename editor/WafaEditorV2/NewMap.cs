﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PBInterface;

namespace WafaEditorV2
{
    public partial class NewMap : Form
    {
        DialogMode mode = DialogMode.Ajout;

        public NewMap()
        {
            InitializeComponent();
            mode = DialogMode.Ajout;
            cmbSensAjoutCol.Enabled = cmbSensAjoutLigne.Enabled = false;

            cmbSensAjoutCol.SelectedIndex = cmbSensAjoutLigne.SelectedIndex = 0;

        }

        public void FillData(Map oMap)
        {
            mode = DialogMode.Modif;

            txtidmap.Text = oMap.MapId;
            txtTileset.Text = oMap.TilesetId;

            numCols.Value = oMap.ColCount;
            numRows.Value = oMap.RowCount;
            cmbSensAjoutCol.Enabled = cmbSensAjoutLigne.Enabled = true;
            this.Text = Editor.MapModif;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            string alert = "L'identifiant de la nouvelle map est obligatoire !";

            if(mode == DialogMode.Modif)
            {
                alert = "L'identifiant de la map est obligatoire !";
            }

            if (string.IsNullOrEmpty(txtidmap.Text.Trim()))
            {
                MessageBox.Show(alert);
                return;
            }

            if (string.IsNullOrEmpty(txtTileset.Text.Trim()))
            {
                MessageBox.Show("Un fichier de tiles est obligatoire !");
                return;
            }


            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            if (this.openTileSet.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                txtTileset.Text = this.openTileSet.FileName;
        }

    }
}
