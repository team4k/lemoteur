﻿namespace WafaEditorV2
{
    partial class ImportCalque
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblImport = new System.Windows.Forms.Label();
            this.cmbCalque = new System.Windows.Forms.ComboBox();
            this.btnImport = new System.Windows.Forms.Button();
            this.cmbAnchor = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbMap = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblImport
            // 
            this.lblImport.AutoSize = true;
            this.lblImport.Location = new System.Drawing.Point(3, 78);
            this.lblImport.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblImport.Name = "lblImport";
            this.lblImport.Size = new System.Drawing.Size(169, 17);
            this.lblImport.TabIndex = 0;
            this.lblImport.Text = "Sélectionner un calque :  ";
            // 
            // cmbCalque
            // 
            this.cmbCalque.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.cmbCalque.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCalque.FormattingEnabled = true;
            this.cmbCalque.Location = new System.Drawing.Point(181, 74);
            this.cmbCalque.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbCalque.Name = "cmbCalque";
            this.cmbCalque.Size = new System.Drawing.Size(397, 25);
            this.cmbCalque.TabIndex = 1;
            // 
            // btnImport
            // 
            this.btnImport.Location = new System.Drawing.Point(211, 194);
            this.btnImport.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(137, 28);
            this.btnImport.TabIndex = 2;
            this.btnImport.Text = "Importer";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // cmbAnchor
            // 
            this.cmbAnchor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAnchor.FormattingEnabled = true;
            this.cmbAnchor.Location = new System.Drawing.Point(181, 129);
            this.cmbAnchor.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbAnchor.Name = "cmbAnchor";
            this.cmbAnchor.Size = new System.Drawing.Size(197, 25);
            this.cmbAnchor.TabIndex = 21;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 129);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 17);
            this.label1.TabIndex = 20;
            this.label1.Text = "Ancre:";
            // 
            // cmbMap
            // 
            this.cmbMap.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.cmbMap.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMap.FormattingEnabled = true;
            this.cmbMap.Location = new System.Drawing.Point(181, 15);
            this.cmbMap.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbMap.Name = "cmbMap";
            this.cmbMap.Size = new System.Drawing.Size(397, 25);
            this.cmbMap.TabIndex = 23;
            this.cmbMap.SelectedIndexChanged += new System.EventHandler(this.cmbMap_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 18);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(162, 17);
            this.label2.TabIndex = 22;
            this.label2.Text = "Sélectionner une map :  ";
            // 
            // ImportCalque
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(596, 279);
            this.Controls.Add(this.cmbMap);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmbAnchor);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnImport);
            this.Controls.Add(this.cmbCalque);
            this.Controls.Add(this.lblImport);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ImportCalque";
            this.Text = "ImportCalque";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblImport;
        private System.Windows.Forms.ComboBox cmbCalque;
        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.ComboBox cmbAnchor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbMap;
        private System.Windows.Forms.Label label2;
    }
}