﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PBInterface;

namespace WafaEditorV2
{
    public partial class WaypointDialog : Form
    {
        private Level ref_level;
        public DialogMode mode { get; set; }

        public WaypointDialog(Level olevel)
        {
            InitializeComponent();
            ref_level = olevel;
            mode = DialogMode.Ajout;
        }


        public void FillData(Waypoint data)
        {
            this.Tag = data;
            txtident.Text = data.Id;

            mode = DialogMode.Modif;
        }


        private void btnOK_Click(object sender, EventArgs e)
        {
            Waypoint cWay = null;

            if (mode == DialogMode.Ajout)
                cWay = new Waypoint();
            else
                cWay = (Waypoint)this.Tag;

            if (string.IsNullOrEmpty(txtident.Text))
            {
                MessageBox.Show("L'identifiant de l'objet de waypoint est obligatoire !");
                return;
            }


            if (mode == DialogMode.Ajout)
            {
                if (EditorUtilities.ObjectIdExist(ref_level, txtident.Text.Trim()))
                {
                    MessageBox.Show("cet identifiant existe déjà ! ");
                    return;
                }
            }
            else if (mode == DialogMode.Modif)
            {
                if (EditorUtilities.ObjectIdExist(ref_level, txtident.Text.Trim(), cWay))
                {
                    MessageBox.Show("cet identifiant existe déjà ! ");
                    return;
                }
            }


            cWay.Id = txtident.Text;

            this.Tag = cWay;

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
    }
}
