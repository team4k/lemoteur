﻿namespace WafaEditorV2
{
    partial class RegenDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstGimpFiles = new System.Windows.Forms.ListBox();
            this.btnRegen = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lstGimpFiles
            // 
            this.lstGimpFiles.FormattingEnabled = true;
            this.lstGimpFiles.Location = new System.Drawing.Point(12, 12);
            this.lstGimpFiles.Name = "lstGimpFiles";
            this.lstGimpFiles.Size = new System.Drawing.Size(219, 277);
            this.lstGimpFiles.TabIndex = 0;
            // 
            // btnRegen
            // 
            this.btnRegen.Location = new System.Drawing.Point(52, 295);
            this.btnRegen.Name = "btnRegen";
            this.btnRegen.Size = new System.Drawing.Size(141, 23);
            this.btnRegen.TabIndex = 1;
            this.btnRegen.Text = "Regénérer la sélection";
            this.btnRegen.UseVisualStyleBackColor = true;
            this.btnRegen.Click += new System.EventHandler(this.btnRegen_Click);
            // 
            // RegenDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(242, 326);
            this.Controls.Add(this.btnRegen);
            this.Controls.Add(this.lstGimpFiles);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RegenDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Regénération depuis fichiers de travail";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lstGimpFiles;
        private System.Windows.Forms.Button btnRegen;
    }
}