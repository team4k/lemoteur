﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WafaEditorV2
{
    public partial class ExportLog : Form
    {
        public ExportLog()
        {
            InitializeComponent();
        }

        private void _AppendLog(string logdata, ExportLogType logtype)
        {
            string typestr = "INFO :";
            Color textcolor = Color.Gray;

            switch (logtype)
            {
                case ExportLogType.ERROR:
                    typestr = "ERROR :";
                    textcolor = Color.Red;
                    break;
                case ExportLogType.WARNING:
                    typestr = "WARNING :";
                    textcolor = Color.Yellow;
                    break;
            }

            rtbLog.ForeColor = textcolor;
            rtbLog.AppendText(typestr + " " + logdata);
            rtbLog.AppendText(Environment.NewLine);
        }

        public void AppendLog(string logdata,ExportLogType logtype = ExportLogType.INFO,bool threaded = false)
        {
            if (threaded)
            {
                this.BeginInvoke(new MethodInvoker(() =>
                {
                    _AppendLog(logdata, logtype);
                }));
            }
            else
            {
                _AppendLog(logdata, logtype);
            }
        }

     
    }

    public enum ExportLogType
    {
        INFO,
        WARNING,
        ERROR
    }
}
