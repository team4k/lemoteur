﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using PBInterface;
using System.Security.Cryptography;

namespace WafaEditorV2
{
    public partial class Fusion : Form
    {
        private PBInterface.Level fusLevel;
        private Level refLevel;


        private Color BackColorEquals = Color.LightGreen;
        private Color BackColorDiff = Color.LightSalmon;
        private Color BackColorNotExist = Color.LightBlue;

        public Fusion(Level ref_level)
        {
            InitializeComponent();
            refLevel = ProtoBuf.Serializer.DeepClone<Level>(ref_level);
        }

        private void btnFusion_Click(object sender, EventArgs e)
        {
            if (ofdFusion.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                using (var file = File.OpenRead(ofdFusion.FileName))
                {
                    fusLevel = ProtoBuf.Serializer.Deserialize<PBInterface.Level>(file);
                }

                //chargement contenu fichier niveau dans notre tree view

                foreach (Map omap in fusLevel.MapArrays)
                {
                    TreeNode mapnode = new TreeNode();
                    mapnode.Text = omap.MapId;
                    mapnode.Name = omap.MapId;


                    bool? diffval = null;

                    //recherche map existante
                    Map origMap = refLevel.MapArrays.Find(p => p.MapId == omap.MapId);

                    if (origMap == null)
                        mapnode.BackColor = BackColorNotExist;
                    else
                    {
                        diffval = this.Compare<Map>(omap, origMap);

                        if (diffval.Value)
                            mapnode.BackColor = BackColorEquals;
                        else
                            mapnode.BackColor = BackColorDiff;
                    }

                    mapnode.Tag = new MergeHolder<Map>(origMap, omap,refLevel.MapArrays);

                    foreach (Layer olayer in omap.LayerArrays)
                    {
                        TreeNode layernode = new TreeNode();
                        layernode.Text = olayer.LayerId;
                        layernode.Name = olayer.LayerId;

                        bool? diff_layer_val = null;

                        Layer origlayer = null;

                        if (diffval.HasValue)
                        {
                            if (!diffval.Value)
                            {
                                origlayer = origMap.LayerArrays.Find(p => p.LayerId == olayer.LayerId);

                                if (origlayer == null)
                                    layernode.BackColor = BackColorNotExist;
                                else
                                {
                                    diff_layer_val = this.Compare<Layer>(olayer, origlayer);

                                    if (diff_layer_val.Value)
                                        layernode.BackColor = BackColorEquals;
                                    else
                                        layernode.BackColor = BackColorDiff;
                                }
                            }
                            else
                                layernode.BackColor = BackColorEquals;
                        }
                        else
                            layernode.BackColor = BackColorNotExist;

                        List<Layer> oReflay = null;

                        if (origMap != null)
                            oReflay = origMap.LayerArrays;

                        layernode.Tag = new MergeHolder<Layer>(origlayer, olayer,oReflay);


                        if (olayer.Type == (int)TypeLayer.Objects)
                        {
                            TreeNode entities = new TreeNode();
                            entities.Text = "Entités";
                            entities.Tag = olayer.EntityArrays;
                            entities.Name = "__entities__";

                            layernode.Nodes.Add(entities);

                            TreeNode triggers = new TreeNode();
                            triggers.Text = "Déclencheur";
                            triggers.Tag = olayer.EntityArrays;
                            triggers.Name = "__triggers__";

                            layernode.Nodes.Add(triggers);

                            TreeNode texts = new TreeNode();
                            texts.Text = "Textes";
                            texts.Tag = olayer.EntityArrays;
                            texts.Name = "__texts__";

                            layernode.Nodes.Add(texts);

                            TreeNode lights = new TreeNode();
                            lights.Text = "Lumières";
                            lights.Tag = olayer.LightArrays;
                            lights.Name = "__lights__";

                            layernode.Nodes.Add(lights);

                            TreeNode sounds = new TreeNode();
                            sounds.Text = "Sons";
                            sounds.Tag = olayer.SoundArrays;
                            sounds.Name = "__sounds__";

                            layernode.Nodes.Add(sounds);

                            bool hasdiffentity = false;
                            bool hasdifftext = false;
                            bool hasdifftrigger = false;
                            bool hasdifflight = false;
                            bool hasdiffsound = false;

                            foreach (Entity oentity in olayer.EntityArrays)
                            {
                                TreeNode entitynode = new TreeNode();
                                entitynode.Text = oentity.EntityId;
                                entitynode.Name = oentity.EntityId;

                                Entity origEntity = null;

                                bool? diff_entity_val = null;

                                if (diff_layer_val.HasValue)
                                {
                                    if (!diff_layer_val.Value)
                                    {
                                        origEntity = origlayer.EntityArrays.Find(p => p.EntityId == oentity.EntityId);

                                        if (origEntity == null)
                                            entitynode.BackColor = BackColorNotExist;
                                        else
                                        {
                                            diff_entity_val = this.Compare<Entity>(oentity, origEntity);

                                            if (diff_entity_val.Value)
                                                entitynode.BackColor = BackColorEquals;
                                            else
                                                entitynode.BackColor = BackColorDiff;
                                        }
                                    }
                                    else
                                        entitynode.BackColor = BackColorEquals;

                                }
                                else
                                    entitynode.BackColor = BackColorNotExist;


                                if ((!diff_entity_val.HasValue || (diff_entity_val.HasValue && !diff_entity_val.Value)) && !hasdiffentity)
                                    hasdiffentity = true;

                                List<Entity> oRefEntities = null;

                                if (origlayer != null)
                                    oRefEntities = origlayer.EntityArrays;

                                entitynode.Tag = new MergeHolder<Entity>(origEntity, oentity,oRefEntities);

                                entities.Nodes.Add(entitynode);
                            }

                            foreach (Trigger otrigger in olayer.TriggerArrays)
                            {
                                TreeNode triggernode = new TreeNode();
                                triggernode.Text = otrigger.TriggerId;
                                triggernode.Name = otrigger.TriggerId;

                                Trigger origtrigger = null;

                                bool? diff_trigger_val = null;

                                if (diff_layer_val.HasValue)
                                {
                                    if (!diff_layer_val.Value)
                                    {
                                        origtrigger = origlayer.TriggerArrays.Find(p => p.TriggerId == otrigger.TriggerId);

                                        if (origtrigger == null)
                                            triggernode.BackColor = BackColorNotExist;
                                        else
                                        {
                                            diff_trigger_val = this.Compare<Trigger>(otrigger, origtrigger);

                                            if (diff_trigger_val.Value)
                                                triggernode.BackColor = BackColorEquals;
                                            else
                                                triggernode.BackColor = BackColorDiff;
                                        }
                                    }
                                    else
                                        triggernode.BackColor = BackColorEquals;

                                }
                                else
                                    triggernode.BackColor = BackColorNotExist;

                                if ((!diff_trigger_val.HasValue || (diff_trigger_val.HasValue && !diff_trigger_val.Value)) && !hasdifftrigger)
                                    hasdifftrigger = true;

                                List<Trigger> oRefTrigger= null;

                                if (origlayer != null)
                                    oRefTrigger = origlayer.TriggerArrays;

                                triggernode.Tag = new MergeHolder<Trigger>(origtrigger, otrigger,oRefTrigger);

                                triggers.Nodes.Add(triggernode);
                            }

                            foreach (TextObject otext in olayer.TextArrays)
                            {
                                TreeNode textnode = new TreeNode();
                                textnode.Text = otext.TextId;
                                textnode.Name = otext.TextId;

                                TextObject origtext = null;

                                bool? diff_text_val = null;

                                if (diff_layer_val.HasValue)
                                {
                                    if (!diff_layer_val.Value)
                                    {
                                        origtext = origlayer.TextArrays.Find(p => p.TextId == otext.TextId);

                                        if (origtext == null)
                                            textnode.BackColor = BackColorNotExist;
                                        else
                                        {
                                            diff_text_val = this.Compare<TextObject>(otext, origtext);

                                            if (diff_text_val.Value)
                                                textnode.BackColor = BackColorEquals;
                                            else
                                                textnode.BackColor = BackColorDiff;
                                        }
                                    }
                                    else
                                        textnode.BackColor = BackColorEquals;

                                }
                                else
                                    textnode.BackColor = BackColorNotExist;


                                if ((!diff_text_val.HasValue || (diff_text_val.HasValue && !diff_text_val.Value)) && !hasdifftext)
                                    hasdifftext = true;

                                List<TextObject> oRefText = null;

                                if (origlayer != null)
                                    oRefText = origlayer.TextArrays;

                                textnode.Tag = new MergeHolder<TextObject>(origtext, otext,oRefText);

                                texts.Nodes.Add(textnode);
                            }

                            foreach (LightSource olight in olayer.LightArrays)
                            {
                                TreeNode lightnode = new TreeNode();
                                lightnode.Text = olight.LightId;
                                lightnode.Name = olight.LightId;

                                LightSource origlight = null;

                                bool? diff_text_val = null;

                                if (diff_layer_val.HasValue)
                                {
                                    if (!diff_layer_val.Value)
                                    {
                                        origlight = origlayer.LightArrays.Find(p => p.LightId == olight.LightId);

                                        if (origlight == null)
                                            lightnode.BackColor = BackColorNotExist;
                                        else
                                        {
                                            diff_text_val = this.Compare<LightSource>(olight, origlight);

                                            if (diff_text_val.Value)
                                                lightnode.BackColor = BackColorEquals;
                                            else
                                                lightnode.BackColor = BackColorDiff;
                                        }
                                    }
                                    else
                                        lightnode.BackColor = BackColorEquals;

                                }
                                else
                                    lightnode.BackColor = BackColorNotExist;


                                if ((!diff_text_val.HasValue || (diff_text_val.HasValue && !diff_text_val.Value)) && !hasdifflight)
                                    hasdifflight = true;

                                List<LightSource> oRefLight = null;

                                if (origlayer != null)
                                    oRefLight = origlayer.LightArrays;

                                lightnode.Tag = new MergeHolder<LightSource>(origlight, olight, oRefLight);

                                lights.Nodes.Add(lightnode);
                            }

                            foreach (SoundSource osound in olayer.SoundArrays)
                            {
                                TreeNode soundnode = new TreeNode();
                                soundnode.Text = osound.SoundId;
                                soundnode.Name = osound.SoundId;

                                SoundSource origsound= null;

                                bool? diff_sound_val = null;

                                if (diff_layer_val.HasValue)
                                {
                                    if (!diff_layer_val.Value)
                                    {
                                        origsound = origlayer.SoundArrays.Find(p => p.SoundId == osound.SoundId);

                                        if (origsound == null)
                                            soundnode.BackColor = BackColorNotExist;
                                        else
                                        {
                                            diff_sound_val= this.Compare<SoundSource>(osound, origsound);

                                            if (diff_sound_val.Value)
                                                soundnode.BackColor = BackColorEquals;
                                            else
                                                soundnode.BackColor = BackColorDiff;
                                        }
                                    }
                                    else
                                        soundnode.BackColor = BackColorEquals;

                                }
                                else
                                    soundnode.BackColor = BackColorNotExist;


                                if ((!diff_sound_val.HasValue || (diff_sound_val.HasValue && !diff_sound_val.Value)) && !hasdiffsound)
                                    hasdiffsound = true;

                                List<SoundSource> oRefSound = null;

                                if (origlayer != null)
                                    oRefSound = origlayer.SoundArrays;

                                soundnode.Tag = new MergeHolder<SoundSource>(origsound, osound, oRefSound);

                                lights.Nodes.Add(soundnode);
                            }

                            entities.BackColor = (hasdiffentity) ? BackColorDiff : BackColorEquals;
                            triggers.BackColor = (hasdifftrigger) ? BackColorDiff : BackColorEquals;
                            texts.BackColor = (hasdifftext) ? BackColorDiff : BackColorEquals;
                            lights.BackColor = (hasdifflight) ? BackColorDiff : BackColorEquals;
                            sounds.BackColor = (hasdiffsound) ? BackColorDiff : BackColorEquals;
                        }

                        mapnode.Nodes.Add(layernode);
                    }


                    tvFusion.Nodes.Add(mapnode);

                }

            }
        }


        private bool Compare<T>(T protobufobject,T origObject)
        {
            MemoryStream objectStream = new MemoryStream();
            objectStream.Position = 0;

            ProtoBuf.Serializer.Serialize<T>(objectStream, protobufobject);

            objectStream.Position = 0;

            byte[] tmp_bytes = objectStream.ToArray();

            SHA256Managed hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(tmp_bytes);
            string hashString = string.Empty;

            foreach (byte x in hash)
                hashString += String.Format("{0:x2}", x);

            objectStream.Position = 0;

            ProtoBuf.Serializer.Serialize<T>(objectStream, origObject);

            objectStream.Position = 0;

            tmp_bytes = objectStream.ToArray();

            objectStream.Close();

            hash = hashstring.ComputeHash(tmp_bytes);
            string origHash = string.Empty;

            foreach (byte x in hash)
                origHash += String.Format("{0:x2}", x);

            return (hashString == origHash);
        }

        private void merge<T>(TreeNode cTreenode)
        {
            MergeHolder<T> holder = cTreenode.Tag as MergeHolder<T>;
            List<T> parentarray = holder._mergelist;

            if (holder._old != null)
            {
                parentarray.Remove(holder._old);
                holder._old = default(T);
            }
                    
            parentarray.Add(holder._new);
        }

        private void tvFusion_AfterSelect(object sender, TreeViewEventArgs e)
        {
           
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            this.Tag = refLevel;
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void tvFusion_DoubleClick(object sender, EventArgs e)
        {
            if (tvFusion.SelectedNode == null)
                return;

            if(tvFusion.SelectedNode.BackColor == BackColorEquals)
                return;

            if (MessageBox.Show("Fusionner l'objet " + tvFusion.SelectedNode.Text + " ?", "Fusion", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                TreeNode cnode = tvFusion.SelectedNode;

                if (cnode.Tag is MergeHolder<Entity>)
                    this.merge<Entity>(cnode);
                else if (cnode.Tag is MergeHolder<Trigger>)
                    this.merge<Trigger>(cnode);
                else if (cnode.Tag is MergeHolder<TextObject>)
                    this.merge<TextObject>(cnode);
                else if (cnode.Tag is MergeHolder<Map>)
                    this.merge<Map>(cnode);
                else if (cnode.Tag is MergeHolder<Layer>)
                    this.merge<Layer>(cnode);
                else if (cnode.Tag is MergeHolder<LightSource>)
                    this.merge<LightSource>(cnode);
                else if (cnode.Tag is MergeHolder<SoundSource>)
                    this.merge<SoundSource>(cnode);

                SetEqualNodes(cnode);
            }
        }

        private void SetEqualNodes(TreeNode node)
        {
            node.BackColor = BackColorEquals;

            foreach (TreeNode childnode in node.Nodes)
            {
                childnode.BackColor = BackColorEquals;

                if(childnode.Nodes.Count > 0)
                    SetEqualNodes(childnode);
            }
        }
    }

    public class MergeHolder<T>
    {
        public T _old;
        public T _new;
        public List<T> _mergelist;


        public MergeHolder(T oldobj, T newobj, List<T> mergelist)
        {
            _old = oldobj;
            _new = newobj;
            _mergelist = mergelist;
        }
    }
}
