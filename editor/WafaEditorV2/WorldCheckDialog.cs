﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PBInterface;
using WafaEditorV2.Properties;
using static WafaEditorV2.NativeFunc;

namespace WafaEditorV2
{
    public partial class WorldCheckDialog : Form
    {
        private Dictionary<WorldKey, Worldcheck> dictchange;

        public WorldCheckDialog(Dictionary<WorldKey, Worldcheck> comparedict)
        {
            InitializeComponent();

            foreach (KeyValuePair<WorldKey,Worldcheck> comp in comparedict)
            {
                if(comp.Value.mustchange)
                {
                    rtbCompare.AppendText("["+comp.Value.chunkfile+"]"+comp.Value.currentId + " en doublon ! changement proposé :" + comp.Value.fixedId+Environment.NewLine);
                }
                
            }

            dictchange = comparedict;
        }

        private void btnConfirmChange_Click(object sender, EventArgs e)
        {

            Dictionary<string, Level> chunktochange = new Dictionary<string, Level>();

            foreach (KeyValuePair<WorldKey, Worldcheck> comp in dictchange)
            {
                if (comp.Value.mustchange)
                {
                    Level staticlevel = null;

                    if(!chunktochange.ContainsKey(comp.Value.chunkfile))
                    {
                        using (var file = File.OpenRead(Path.Combine(Settings.Default.assets_path, comp.Value.chunkfile)))
                            staticlevel = ProtoBuf.Serializer.Deserialize<PBInterface.Level>(file);

                        chunktochange.Add(comp.Value.chunkfile, staticlevel);
                    }
                    else
                    {
                        staticlevel = chunktochange[comp.Value.chunkfile];
                    }

                    
                    foreach (Layer olayer in staticlevel.MapArrays[0].LayerArrays)
                    {
                        if (olayer.Type != (int)TypeLayer.Objects)
                        {
                            continue;
                        }

                        switch(comp.Key.type)
                        {
                            case GAME_TYPE.GT_ENTITY:
                                foreach (Entity ent in olayer.EntityArrays)
                                {
                                    if(ent.EntityId == comp.Value.currentId)
                                    {
                                        ent.EntityId = comp.Value.fixedId;
                                    }
                                }
                                break;
                            case GAME_TYPE.GT_TRIGGER:
                                foreach (Trigger trig in olayer.TriggerArrays)
                                {
                                    if (trig.TriggerId == comp.Value.currentId)
                                    {
                                        trig.TriggerId = comp.Value.fixedId;
                                    }
                                }
                                break;
                            case GAME_TYPE.GT_TEXT:
                                foreach (TextObject txt in olayer.TextArrays)
                                {
                                    if (txt.TextId == comp.Value.currentId)
                                    {
                                        txt.TextId = comp.Value.fixedId;
                                    }
                                }
                                break;
                            case GAME_TYPE.GT_SOUND:
                                foreach (SoundSource snd in olayer.SoundArrays)
                                {
                                    if (snd.SoundId == comp.Value.currentId)
                                    {
                                        snd.SoundId = comp.Value.fixedId;
                                    }
                                }
                                break;
                            case GAME_TYPE.GT_LIGHT:
                                foreach (LightSource light in olayer.LightArrays)
                                {
                                    if (light.LightId == comp.Value.currentId)
                                    {
                                        light.LightId = comp.Value.fixedId;
                                    }
                                }
                                break;
                            case GAME_TYPE.GT_PARTICLES:
                                foreach (ParticlesData prt in olayer.ParticlesArrays)
                                {
                                    if (prt.Id == comp.Value.currentId)
                                    {
                                        prt.Id = comp.Value.fixedId;
                                    }
                                }
                                break;
                            case GAME_TYPE.GT_WAYPOINT:
                                foreach (Waypoint way in olayer.Waypoints)
                                {
                                    if (way.Id == comp.Value.currentId)
                                    {
                                        way.Id = comp.Value.fixedId;
                                    }
                                }
                                break;
                            case GAME_TYPE.GT_COLLIDER:
                                foreach (Collider col in olayer.Colliders)
                                {
                                    if (col.Id == comp.Value.currentId)
                                    {
                                        col.Id = comp.Value.fixedId;
                                    }
                                }
                                break;
                        }
                    }
                }
            }


            foreach(KeyValuePair<string,Level> leveltosave in chunktochange)
            {
                //sauvegarde des modifications
                using (var file = File.Create(Path.Combine(Settings.Default.assets_path, leveltosave.Key)))
                {
                    ProtoBuf.Serializer.Serialize<PBInterface.Level>(file, leveltosave.Value);
                }
            }

            rtbCompare.Clear();

            MessageBox.Show("Doublon corrigé !");

            this.DialogResult = DialogResult.OK;
        }
    }
}
