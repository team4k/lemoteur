﻿namespace WafaEditorV2
{
    partial class AnimationGenerator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cmbTypeAnime = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnDebut = new System.Windows.Forms.Button();
            this.btnFin = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.pbPreview = new System.Windows.Forms.PictureBox();
            this.btnAjoutTile = new System.Windows.Forms.Button();
            this.btnSuppressionTile = new System.Windows.Forms.Button();
            this.trkPreview = new System.Windows.Forms.TrackBar();
            this.label5 = new System.Windows.Forms.Label();
            this.numFrameDebut = new System.Windows.Forms.NumericUpDown();
            this.BtnGenerer = new System.Windows.Forms.Button();
            this.btnAnnuler = new System.Windows.Forms.Button();
            this.numStartTile = new System.Windows.Forms.NumericUpDown();
            this.numEndTile = new System.Windows.Forms.NumericUpDown();
            this.btnModifTile = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.pbTileBascule = new System.Windows.Forms.PictureBox();
            this.btnBascule = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pbPreview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trkPreview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numFrameDebut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numStartTile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numEndTile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbTileBascule)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(40, 50);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Type de mouvement :";
            // 
            // cmbTypeAnime
            // 
            this.cmbTypeAnime.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTypeAnime.FormattingEnabled = true;
            this.cmbTypeAnime.Location = new System.Drawing.Point(195, 47);
            this.cmbTypeAnime.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbTypeAnime.Name = "cmbTypeAnime";
            this.cmbTypeAnime.Size = new System.Drawing.Size(247, 25);
            this.cmbTypeAnime.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(40, 119);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Tile de début :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(40, 159);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Tile de fin :";
            // 
            // btnDebut
            // 
            this.btnDebut.Location = new System.Drawing.Point(468, 107);
            this.btnDebut.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnDebut.Name = "btnDebut";
            this.btnDebut.Size = new System.Drawing.Size(133, 28);
            this.btnDebut.TabIndex = 4;
            this.btnDebut.Text = "Sélection";
            this.btnDebut.UseVisualStyleBackColor = true;
            this.btnDebut.Click += new System.EventHandler(this.btnDebut_Click);
            // 
            // btnFin
            // 
            this.btnFin.Location = new System.Drawing.Point(468, 159);
            this.btnFin.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnFin.Name = "btnFin";
            this.btnFin.Size = new System.Drawing.Size(133, 28);
            this.btnFin.TabIndex = 5;
            this.btnFin.Text = "Sélection";
            this.btnFin.UseVisualStyleBackColor = true;
            this.btnFin.Click += new System.EventHandler(this.btnFin_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(40, 249);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(125, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Séquence de tiles:";
            // 
            // pbPreview
            // 
            this.pbPreview.Location = new System.Drawing.Point(321, 284);
            this.pbPreview.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pbPreview.Name = "pbPreview";
            this.pbPreview.Size = new System.Drawing.Size(43, 39);
            this.pbPreview.TabIndex = 7;
            this.pbPreview.TabStop = false;
            this.pbPreview.Paint += new System.Windows.Forms.PaintEventHandler(this.pbPreview_Paint);
            // 
            // btnAjoutTile
            // 
            this.btnAjoutTile.Location = new System.Drawing.Point(227, 249);
            this.btnAjoutTile.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAjoutTile.Name = "btnAjoutTile";
            this.btnAjoutTile.Size = new System.Drawing.Size(100, 28);
            this.btnAjoutTile.TabIndex = 8;
            this.btnAjoutTile.Text = "Ajout tile";
            this.btnAjoutTile.UseVisualStyleBackColor = true;
            this.btnAjoutTile.Click += new System.EventHandler(this.btnAjoutTile_Click);
            // 
            // btnSuppressionTile
            // 
            this.btnSuppressionTile.Enabled = false;
            this.btnSuppressionTile.Location = new System.Drawing.Point(459, 249);
            this.btnSuppressionTile.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSuppressionTile.Name = "btnSuppressionTile";
            this.btnSuppressionTile.Size = new System.Drawing.Size(143, 28);
            this.btnSuppressionTile.TabIndex = 9;
            this.btnSuppressionTile.Text = "Suppression tile";
            this.btnSuppressionTile.UseVisualStyleBackColor = true;
            this.btnSuppressionTile.Click += new System.EventHandler(this.btnSuppressionTile_Click);
            // 
            // trkPreview
            // 
            this.trkPreview.Location = new System.Drawing.Point(273, 332);
            this.trkPreview.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.trkPreview.Maximum = 0;
            this.trkPreview.Name = "trkPreview";
            this.trkPreview.Size = new System.Drawing.Size(139, 56);
            this.trkPreview.TabIndex = 10;
            this.trkPreview.Visible = false;
            this.trkPreview.Scroll += new System.EventHandler(this.trkPreview_Scroll);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(40, 425);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(112, 17);
            this.label5.TabIndex = 11;
            this.label5.Text = "Frame de début:";
            // 
            // numFrameDebut
            // 
            this.numFrameDebut.Location = new System.Drawing.Point(195, 425);
            this.numFrameDebut.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.numFrameDebut.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numFrameDebut.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numFrameDebut.Name = "numFrameDebut";
            this.numFrameDebut.Size = new System.Drawing.Size(160, 23);
            this.numFrameDebut.TabIndex = 13;
            this.numFrameDebut.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // BtnGenerer
            // 
            this.BtnGenerer.Location = new System.Drawing.Point(109, 471);
            this.BtnGenerer.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnGenerer.Name = "BtnGenerer";
            this.BtnGenerer.Size = new System.Drawing.Size(100, 28);
            this.BtnGenerer.TabIndex = 15;
            this.BtnGenerer.Text = "Générer";
            this.BtnGenerer.UseVisualStyleBackColor = true;
            this.BtnGenerer.Click += new System.EventHandler(this.BtnGenerer_Click);
            // 
            // btnAnnuler
            // 
            this.btnAnnuler.Location = new System.Drawing.Point(227, 471);
            this.btnAnnuler.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAnnuler.Name = "btnAnnuler";
            this.btnAnnuler.Size = new System.Drawing.Size(100, 28);
            this.btnAnnuler.TabIndex = 16;
            this.btnAnnuler.Text = "Annuler";
            this.btnAnnuler.UseVisualStyleBackColor = true;
            this.btnAnnuler.Click += new System.EventHandler(this.btnAnnuler_Click);
            // 
            // numStartTile
            // 
            this.numStartTile.Enabled = false;
            this.numStartTile.Location = new System.Drawing.Point(227, 117);
            this.numStartTile.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.numStartTile.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numStartTile.Name = "numStartTile";
            this.numStartTile.Size = new System.Drawing.Size(160, 23);
            this.numStartTile.TabIndex = 17;
            // 
            // numEndTile
            // 
            this.numEndTile.Enabled = false;
            this.numEndTile.Location = new System.Drawing.Point(227, 162);
            this.numEndTile.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.numEndTile.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numEndTile.Name = "numEndTile";
            this.numEndTile.Size = new System.Drawing.Size(160, 23);
            this.numEndTile.TabIndex = 18;
            // 
            // btnModifTile
            // 
            this.btnModifTile.Enabled = false;
            this.btnModifTile.Location = new System.Drawing.Point(335, 249);
            this.btnModifTile.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnModifTile.Name = "btnModifTile";
            this.btnModifTile.Size = new System.Drawing.Size(116, 28);
            this.btnModifTile.TabIndex = 19;
            this.btnModifTile.Text = "Modifier tile";
            this.btnModifTile.UseVisualStyleBackColor = true;
            this.btnModifTile.Click += new System.EventHandler(this.btnModifTile_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(40, 372);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 17);
            this.label6.TabIndex = 20;
            this.label6.Text = "Tile bascule:";
            // 
            // pbTileBascule
            // 
            this.pbTileBascule.Location = new System.Drawing.Point(321, 378);
            this.pbTileBascule.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pbTileBascule.Name = "pbTileBascule";
            this.pbTileBascule.Size = new System.Drawing.Size(43, 39);
            this.pbTileBascule.TabIndex = 21;
            this.pbTileBascule.TabStop = false;
            this.pbTileBascule.Paint += new System.Windows.Forms.PaintEventHandler(this.pbTileBascule_Paint);
            // 
            // btnBascule
            // 
            this.btnBascule.Location = new System.Drawing.Point(468, 378);
            this.btnBascule.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnBascule.Name = "btnBascule";
            this.btnBascule.Size = new System.Drawing.Size(133, 28);
            this.btnBascule.TabIndex = 22;
            this.btnBascule.Text = "Sélectionner tile en cours";
            this.btnBascule.UseVisualStyleBackColor = true;
            this.btnBascule.Click += new System.EventHandler(this.btnBascule_Click);
            // 
            // AnimationGenerator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(617, 529);
            this.Controls.Add(this.btnBascule);
            this.Controls.Add(this.pbTileBascule);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnModifTile);
            this.Controls.Add(this.numEndTile);
            this.Controls.Add(this.numStartTile);
            this.Controls.Add(this.btnAnnuler);
            this.Controls.Add(this.BtnGenerer);
            this.Controls.Add(this.numFrameDebut);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.trkPreview);
            this.Controls.Add(this.btnSuppressionTile);
            this.Controls.Add(this.btnAjoutTile);
            this.Controls.Add(this.pbPreview);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnFin);
            this.Controls.Add(this.btnDebut);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmbTypeAnime);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AnimationGenerator";
            this.Text = "Génération d\'animations";
            this.Load += new System.EventHandler(this.AnimationGenerator_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbPreview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trkPreview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numFrameDebut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numStartTile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numEndTile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbTileBascule)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbTypeAnime;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnDebut;
        private System.Windows.Forms.Button btnFin;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pbPreview;
        private System.Windows.Forms.Button btnAjoutTile;
        private System.Windows.Forms.Button btnSuppressionTile;
        private System.Windows.Forms.TrackBar trkPreview;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown numFrameDebut;
        private System.Windows.Forms.Button BtnGenerer;
        private System.Windows.Forms.Button btnAnnuler;
        private System.Windows.Forms.NumericUpDown numStartTile;
        private System.Windows.Forms.NumericUpDown numEndTile;
        private System.Windows.Forms.Button btnModifTile;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox pbTileBascule;
        private System.Windows.Forms.Button btnBascule;
    }
}