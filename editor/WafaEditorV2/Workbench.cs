﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PBInterface;
using WafaEditorV2.Properties;
using System.Drawing.Drawing2D;
using System.IO;
using System.Threading;
using System.Drawing.Imaging;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.IO.Pipes;
using System.Security.Permissions;
using System.Windows.Forms.VisualStyles;
using ICSharpCode.SharpZipLib;
using System.Globalization;
using SpriteSheetLibrary;
using System.Data.SQLite;
using ProtoBuf;

namespace WafaEditorV2
{
    public enum DialogMode
    {
        Ajout,
        Modif
    }

    public enum GlobalMode
    {
        Play,
        Editor,
        WorldEditor,
        FragEditor
    }

    public enum Tools
    {
        Pencil,
        FloodFill,
        Waypoint,
        Trigger,
        Entity
    }

    public enum ResxType
    {
        texture,
        sound,
        animation,
        font,
        level,
        shader,
    }

    public enum AnchorType
    {
        TopLeft,
        BottomLeft
    }

    public partial class Workbench : Form, IMessageFilter
    {
        GlobalMode current_mode = GlobalMode.Editor;

        //Objets sérialisé via protobuf

        //le monde en cours d'édition
        private World worldObj;

        //le niveau en cours d'édition
        private Level levelObj;

        //le fragment en cours d'édition
        private Fragment fragObj;


        //le script du niveau
        private Dictionary<string, string> commands_ref;

        //la liste des bitmap pour chaque map du niveau
        private Dictionary<string, Bitmap> tileSets = new Dictionary<string, Bitmap>();

        //la liste des bitmap pour chaque map du niveau avec chaque tiles espacées
        private Dictionary<string, Bitmap> viewable_tileSets = new Dictionary<string, Bitmap>();

        //la liste des resources disponible dans le répertoire de resources
        private List<ResxInfo> game_resources = null;

        //la liste des modules lua disponibles
        private List<string> script_modules = new List<string>();

        private string script_base_path = string.Empty;

        //objet utilisé par le picturebox du tileset
        private bool renderCursor = true;

        private const int TILE_PER_ROW = 10;

        public const int TILE_SIZE = 32;

        public static int SCROLL_SPEED = 5;

        private const int MAX_FRAME_PER_LINE = 10;

        // private const int Settings.Default.tileset_offset = 2;

        private const int MAX_UNDOREDO_COUNT = 50;

        private int current_tile_offset = 0;

        private LuaCommands lua_comm;

        public delegate void TilesetboxSelectionChanged(int newTile, int x, int y);

        public event TilesetboxSelectionChanged OnTilesetboxSelectionChanged;

        //gestion du undo / redo
        StackHistory history = new StackHistory(MAX_UNDOREDO_COUNT);

        [DllImport("user32.dll")]
        public static extern int GetKeyboardState(byte[] keystate);

        public Dictionary<string, float> zoom_value = new Dictionary<string, float>() {
            {"25%",4.0f},
            {"50%",2.0f},
            {"100%",1.0f},
            {"150%",0.75f},
            {"200%",0.5f},
            {"400%",0.25f},
            {"800%",0.125f}};

        public Workbench()
        {
            InitializeComponent();


            NativeFunc.editor_init();

            Application.Idle += Application_Idle;
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

            Application.ThreadException += new ThreadExceptionEventHandler(Application_ThreadException);

            calquesList.TreeViewNodeSorter = new CalqueComparer();

            commands_ref = new Dictionary<string, string>();

            prefwin_OnChangeRenderSize();
            //renderPanel.Width = Settings.Default.render_width;
            //renderPanel.Height = Settings.Default.render_height;

            lua_comm = new LuaCommands(commands_ref);


            if (string.IsNullOrEmpty(Settings.Default.assets_path) || string.IsNullOrEmpty(Settings.Default.gamedll_path))
            {
                PrefWindow prefwin = new PrefWindow();

                System.Windows.Forms.DialogResult res = prefwin.ShowDialog();

                if (res == System.Windows.Forms.DialogResult.Cancel)
                    this.Close();
            }


            //read settings
            chkcollisions.Checked = Settings.Default.draw_debug_collisions;
            chksnapgrid.Checked = Settings.Default.snap_to_grid;
            chkDrawallitems.Checked = Settings.Default.draw_colliders_trigger_misc;

            tabWorldSlice.Hide();

            if (!string.IsNullOrEmpty(Settings.Default.localedb_path))
            {
                localedbWatcher.EnableRaisingEvents = true;
                localedbWatcher.Path = Settings.Default.localedb_path.Substring(0, Settings.Default.localedb_path.LastIndexOf(Path.DirectorySeparatorChar));
            }

            configWatcher.EnableRaisingEvents = true;
            configWatcher.Path = Application.StartupPath;

            renderPanel.OnSelectedTileChanged += new OglPanel.SelectedTileChanged(renderPanel_OnSelectedTileChanged);
            renderPanel.OnSelectedItemChanged += new OglPanel.SelectedItemChanged(renderPanel_OnSelectedItemChanged);

            history.OnUndo += new StackHistory.undo_event(history_OnUndo);

            history.OnRedo += new StackHistory.redo_event(history_OnRedo);
            history.OnPush += new StackHistory.push_event(history_OnPush);
            history.OnRemove += new StackHistory.remove_event(history_OnRemove);

            renderPanel.ref_history = history;

            cmbZoom.DataSource = zoom_value.ToArray<KeyValuePair<string, float>>();
            cmbZoom.DisplayMember = "Key";
            cmbZoom.ValueMember = "Value";

            current_tile_offset = Settings.Default.tileset_offset;

            SCROLL_SPEED = Settings.Default.SCROLL_SPEED;

            //InputLanguage.CurrentInputLanguage = InputLanguage.FromCulture(new CultureInfo("en-US"));

            if (Settings.Default.historique == null)
                Settings.Default.historique = new System.Collections.Specialized.StringCollection();

            //chargement historique fichiers ouverts
            foreach (string fichier_histo in Settings.Default.historique)
            {
                this.AddHisto(fichier_histo, true);
            }

            Application.AddMessageFilter(this);
        }


        #region Gestion Monde

        /// <summary>
        /// Création d'un nouveau monde utilisé pour donner les informations nécessaires au système de streaming
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void nouveauMondeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //si un niveau ou un monde est déjà ouvert, appel du code de fermeture
            if (levelObj != null)
                FermerNiveau(openLevelDialog.FileName);

            if (worldObj != null)
                FermerMonde(openWorldDialog.FileName);

            if (fragObj != null)
                FermerFragment(openFragmentDialog.FileName);


            WorldDetail formWorld = new WorldDetail();

            if (formWorld.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                worldObj = new World();
                worldObj.WorldId = formWorld.txtidworld.Text;
                worldObj.ChunkRows = Convert.ToInt32(formWorld.numRowsWorld.Value);
                worldObj.ChunkCols = Convert.ToInt32(formWorld.numColsWorld.Value);

                worldObj.EditorPos = new Wvector();

                worldObj.EditorPos.X = Convert.ToInt32((worldObj.ChunkRows * Settings.Default.TILE_SIZE) * 0.5f);
                worldObj.EditorPos.Y = Convert.ToInt32((worldObj.ChunkCols * Settings.Default.TILE_SIZE) * 0.5f);

                int[] emptytiles = new int[worldObj.ChunkRows * worldObj.ChunkCols];

                for (int zd = 0; zd < worldObj.ChunkRows * worldObj.ChunkCols; zd++)
                {
                    emptytiles[zd] = 0;
                }

                worldObj.ZoneDatas = emptytiles;

                //zone par défaut
                ZoneSlice defslice = new ZoneSlice();
                defslice.Name = "zone1";
                defslice.StartValue = 0;
                defslice.EndValue = 10;
                defslice.TypeZone = (int)TypeZone.ZRange;
                defslice.Color = new Wcolor();
                defslice.Color.R = 255;
                defslice.Color.G = 255;
                defslice.Color.B = 0;
                defslice.Color.A = 255;

                worldObj.SliceDatas.Add(defslice);

                this.chargerMonde(worldObj);
            }

        }

        private void btnselectzonevalue_Click(object sender, EventArgs e)
        {
            renderPanel.CurrentCollisionVal = (int)numSelectzonevalue.Value;

            //récupère la couleur pour la collision en cours
            renderPanel.CurrentWorldColor = getWorldColor(renderPanel.CurrentCollisionVal);

        }

        private ZoneSlice getWorldZone(int value)
        {
            List<ZoneSlice> tmpselect = (from u in worldObj.SliceDatas where u.StartValue <= value && u.EndValue >= value select u).ToList<ZoneSlice>();

            if (tmpselect.Count == 0)
            {
                return null;
            }

            if (tmpselect.Count == 1)
            {
                return tmpselect[0];
            }

            foreach (ZoneSlice slice in tmpselect)
            {

                if ((from u in tmpselect where u.ParentZone == slice.Name select u).Count<ZoneSlice>() == 0)
                {
                    return slice;
                }

            }

            return null;
        }

        private Wcolor getWorldColor(int value)
        {
            ZoneSlice select = getWorldZone(value);

            if (select != null)
            {
                return select.Color;
            }
            else
            {
                return new Wcolor();
            }
        }


        private ZoneSlice _modifier_zone(ZoneSlice slice, ZoneSlice parent = null)
        {
            ZoneDetail zonedialog = new ZoneDetail();

            zonedialog.FillForm(slice);
            zonedialog.refworld = worldObj;


            zonedialog.parent = parent;

            zonedialog.Tag = slice;
            zonedialog.mode = DialogMode.Modif;

            if (zonedialog.ShowDialog() == DialogResult.OK)
            {
                ZoneSlice result = (ZoneSlice)zonedialog.Tag;

                if (result.TypeZone == (int)TypeZone.ZRange)
                    NativeFunc.editor_changecollisions_color_forrange(result.StartValue, result.EndValue, (result.Color.R != 0) ? result.Color.R / 255.0f : 0.0f, (result.Color.G != 0) ? result.Color.G / 255.0f : 0.0f, (result.Color.B != 0) ? result.Color.B / 255.0f : 0.0f, 1.0f);
                else if (result.TypeZone == (int)TypeZone.ZSpec)
                    NativeFunc.editor_changecollisions_color_forvalue(result.StartValue, (result.Color.R != 0) ? result.Color.R / 255.0f : 0.0f, (result.Color.G != 0) ? result.Color.G / 255.0f : 0.0f, (result.Color.B != 0) ? result.Color.B / 255.0f : 0.0f, 1.0f);

                //maj couleur sélectionné
                renderPanel.CurrentWorldColor = getWorldColor(renderPanel.CurrentCollisionVal);

                return result;

            }
            else
            {
                return null;
            }
        }


        private void tvZone_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (tvZone.SelectedNode != null)
            {
                ZoneSlice parent = null;

                if (tvZone.SelectedNode.Parent != null)
                    parent = (ZoneSlice)tvZone.SelectedNode.Parent.Tag;


                ZoneSlice ret = _modifier_zone((ZoneSlice)tvZone.SelectedNode.Tag, parent);

                if (ret != null)
                    tvZone.SelectedNode.Tag = ret;
            }

        }

        private void tvZone_MouseDown(object sender, MouseEventArgs e)
        {
            ajouterZoneEnfant.Enabled = modifierZone.Enabled = supprimerZone.Enabled = (tvZone.SelectedNode != null);

            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                ctxMenuZone.Show(tvZone, e.Location);
            }


        }

        private void ajouterZone_Click(object sender, EventArgs e)
        {

            ZoneDetail zonedialog = new ZoneDetail();
            zonedialog.mode = DialogMode.Ajout;
            zonedialog.refworld = worldObj;

            if (zonedialog.ShowDialog() == DialogResult.OK)
            {
                ZoneSlice newzone = (ZoneSlice)zonedialog.Tag;

                worldObj.SliceDatas.Add(newzone);

                TreeNode slicenode = new TreeNode(newzone.Name);
                slicenode.Tag = newzone;
                tvZone.Nodes.Add(slicenode);
            }

        }

        private void ajouterZoneEnfant_Click(object sender, EventArgs e)
        {
            ZoneDetail zonedialog = new ZoneDetail();
            zonedialog.mode = DialogMode.Ajout;
            zonedialog.refworld = worldObj;
            zonedialog.parent = (ZoneSlice)tvZone.SelectedNode.Tag;

            if (zonedialog.ShowDialog() == DialogResult.OK)
            {
                ZoneSlice newzone = (ZoneSlice)zonedialog.Tag;

                worldObj.SliceDatas.Add(newzone);
                TreeNode slicenode = new TreeNode(newzone.Name);
                slicenode.Tag = newzone;
                tvZone.SelectedNode.Nodes.Add(slicenode);
            }
        }


        private void modifierZone_Click(object sender, EventArgs e)
        {
            if (tvZone.SelectedNode != null)
            {
                ZoneSlice parent = null;

                if (tvZone.SelectedNode.Parent != null)
                    parent = (ZoneSlice)tvZone.SelectedNode.Parent.Tag;


                ZoneSlice ret = _modifier_zone((ZoneSlice)tvZone.SelectedNode.Tag, parent);

                if (ret != null)
                    tvZone.SelectedNode.Tag = ret;
            }
        }

        private void supprimerZone_Click(object sender, EventArgs e)
        {
            if (tvZone.SelectedNode != null)
            {
                ZoneSlice zonedel = (ZoneSlice)tvZone.SelectedNode.Tag;

                if (MessageBox.Show(string.Format("Supprimer la zone {0} ?", zonedel.Name), "Confirmer suppression zone", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (tvZone.SelectedNode.Parent == null)
                    {
                        tvZone.Nodes.Remove(tvZone.SelectedNode);
                    }
                    else
                    {
                        tvZone.SelectedNode.Parent.Nodes.Remove(tvZone.SelectedNode);
                    }

                    tvZone.SelectedNode = null;
                    worldObj.SliceDatas.Remove(zonedel);
                }
            }
        }

        public void worldrenderPanel_OnSelectedTileChanged()
        {
            //refresh de la tilebox
            numSelectzonevalue.Value = renderPanel.CurrentCollisionVal;
            ZoneSlice select = getWorldZone(renderPanel.CurrentCollisionVal);

            if (select != null)
            {
                renderPanel.CurrentWorldColor = select.Color;
                lblcurrentzone.Text = select.Name;
            }
            else
            {
                lblcurrentzone.Text = "<aucune>";
            }

        }


        private void chargerMonde(string filename)
        {
            World oworld = null;

            using (var file = File.OpenRead(filename))
                oworld = ProtoBuf.Serializer.Deserialize<PBInterface.World>(file);

            if (oworld != null)
                chargerMonde(oworld);


        }


        private void buildzonetree(TreeNode tree, ZoneSlice parent_zone, List<ZoneSlice> allzone)
        {

            List<ZoneSlice> child_slices = (from u in allzone where u.ParentZone == parent_zone.Name select u).ToList<ZoneSlice>();

            foreach (ZoneSlice child in child_slices)
            {
                TreeNode slicenode = new TreeNode(child.Name);
                slicenode.Tag = child;

                //changement des couleur sur la grille
                if (child.TypeZone == (int)TypeZone.ZSpec || child.TypeZone == (int)TypeZone.ZStaticlevel)
                {
                    NativeFunc.editor_changecollisions_color_forvalue(child.StartValue, (child.Color.R != 0) ? child.Color.R / 255.0f : 0.0f, (child.Color.G != 0) ? child.Color.G / 255.0f : 0.0f, (child.Color.B != 0) ? child.Color.B / 255.0f : 0.0f, 1.0f);
                }
                else if (child.TypeZone == (int)TypeZone.ZRange)
                {
                    NativeFunc.editor_changecollisions_color_forrange(child.StartValue, child.EndValue, (child.Color.R != 0) ? child.Color.R / 255.0f : 0.0f, (child.Color.G != 0) ? child.Color.G / 255.0f : 0.0f, (child.Color.B != 0) ? child.Color.B / 255.0f : 0.0f, 1.0f);
                }

                this.buildzonetree(slicenode, child, allzone);

                tree.Nodes.Add(slicenode);
            }

        }

        private void chargerMonde(World worldObj)
        {
            tabLayout.Hide();
            tabWorldSlice.Show();

            this.worldObj = worldObj;

            current_mode = GlobalMode.WorldEditor;


            renderPanel.StartEngine(worldObj.EditorPos.X, worldObj.EditorPos.Y, worldObj.EditorPos.X, worldObj.EditorPos.Y);

            //dummy map object
            Map mapObj = new Map();
            mapObj.MapId = "dummy_world_map";
            mapObj.ColCount = worldObj.ChunkCols;
            mapObj.RowCount = worldObj.ChunkRows;
            mapObj.TilesetId = "default_text.png";
            mapObj.EditorPos = new Wvector();
            mapObj.EditorPlayerPos = new Wvector();

            Layer cLayer = new Layer();
            cLayer.Order = 0;
            cLayer.LayerId = "background";
            cLayer.Type = (int)TypeLayer.Normal;
            cLayer.ParallaxX = 1.0f;
            cLayer.ParallaxY = 1.0f;
            cLayer.Opacity = 1.0f;
            cLayer.Visible = true;

            mapObj.LayerArrays.Add(cLayer);

            Layer colLayer = new Layer();
            colLayer.Order = 0;
            colLayer.Type = (int)TypeLayer.Collisions;
            colLayer.LayerId = "collisions";
            colLayer.ParallaxX = 1.0f;
            colLayer.ParallaxY = 1.0f;
            colLayer.Opacity = 0.7f;
            colLayer.Visible = true;

            mapObj.LayerArrays.Add(colLayer);

            int[] tilesempty = new int[worldObj.ZoneDatas.Length];
            int[] tilezone = new int[worldObj.ZoneDatas.Length];
            int[] collisions = new int[worldObj.ZoneDatas.Length];

            for (int i = 0; i < worldObj.ZoneDatas.Length; i++)
            {
                tilesempty[i] = 0;
                tilezone[i] = worldObj.ZoneDatas[i];
                collisions[i] = worldObj.ZoneDatas[i]; ;
            }

            cLayer.TileArrays = tilesempty;
            colLayer.TileArrays = tilezone;
            mapObj.CollisionsArrays = collisions;


            enregistrerMondeToolStripMenuItem.Enabled = true;
            fermerMondeToolStripMenuItem.Enabled = true;

            renderPanel.CreateTilemap(mapObj);

            MemoryStream layerStream = new MemoryStream();
            layerStream.Position = 0;
            ProtoBuf.Serializer.Serialize<Layer>(layerStream, cLayer);

            layerStream.Position = 0;

            byte[] tmplayerArray = layerStream.ToArray();

            NativeFunc.editor_addlayer(tmplayerArray, (uint)tmplayerArray.Length);


            layerStream.Position = 0;
            ProtoBuf.Serializer.Serialize<Layer>(layerStream, colLayer);
            layerStream.Position = 0;

            tmplayerArray = layerStream.ToArray();

            layerStream.Close();

            NativeFunc.editor_addlayer(tmplayerArray, (uint)tmplayerArray.Length);



            NativeFunc.editor_showcursor(NativeFunc.wbool.wtrue, NativeFunc.wbool.wfalse);
            renderPanel.UpdateTexture();

            renderPanel.Invalidate();

            NativeFunc.editor_changelayer_visibility(NativeFunc.wbool.wfalse, cLayer.LayerId);

            NativeFunc.editor_draw_frame();

            //chargement des différentes zone
            tvZone.Nodes.Clear();

            TreeNode firstnode = null;

            //sélection de toutes les zone sans aucun parent (zone de premier rang
            List<ZoneSlice> firstorderslices = (from u in worldObj.SliceDatas where string.IsNullOrEmpty(u.ParentZone) select u).ToList<ZoneSlice>();

            foreach (ZoneSlice slice in firstorderslices)
            {
                TreeNode slicenode = new TreeNode(slice.Name);
                slicenode.Tag = slice;


                if (firstnode == null)
                {
                    firstnode = slicenode;
                }

                //changement des couleur sur la grille
                if (slice.TypeZone == (int)TypeZone.ZSpec || slice.TypeZone == (int)TypeZone.ZStaticlevel)
                {
                    NativeFunc.editor_changecollisions_color_forvalue(slice.StartValue, (slice.Color.R != 0) ? slice.Color.R / 255.0f : 0.0f, (slice.Color.G != 0) ? slice.Color.G / 255.0f : 0.0f, (slice.Color.B != 0) ? slice.Color.B / 255.0f : 0.0f, 1.0f);
                }
                else if (slice.TypeZone == (int)TypeZone.ZRange)
                {
                    NativeFunc.editor_changecollisions_color_forrange(slice.StartValue, slice.EndValue, (slice.Color.R != 0) ? slice.Color.R / 255.0f : 0.0f, (slice.Color.G != 0) ? slice.Color.G / 255.0f : 0.0f, (slice.Color.B != 0) ? slice.Color.B / 255.0f : 0.0f, 1.0f);
                }



                //selection des zone enfants
                this.buildzonetree(slicenode, slice, worldObj.SliceDatas);


                tvZone.Nodes.Add(slicenode);

            }

            tvZone.SelectedNode = firstnode;


            renderPanel.CurrentLayer = colLayer;

            renderPanel.CurrentMap = mapObj;

            renderPanel.CurrentCollisionVal = 0;


            renderPanel.CurrentWorldColor = getWorldColor(renderPanel.CurrentCollisionVal);

            renderPanel.OnSelectedTileChanged += new OglPanel.SelectedTileChanged(worldrenderPanel_OnSelectedTileChanged);
            lblcurrentzone.Text = "<aucune>";

            OglPanel.current_mode = current_mode;
        }

        private void FermerMonde(string filename)
        {
            worldObj = null;

            tabWorldSlice.Hide();

            enregistrerMondeToolStripMenuItem.Enabled = false;
            fermerMondeToolStripMenuItem.Enabled = false;
        }

        private void chargerMondeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string oldfilename = openWorldDialog.FileName;

            if (openWorldDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string filename = openWorldDialog.FileName;
                //si un monde est déjà ouvert, appel du code de fermeture
                if (worldObj != null)
                    FermerMonde(oldfilename);
                //même chose si un niveau est ouvert

                if (levelObj != null)
                    FermerNiveau(openLevelDialog.FileName);



                openWorldDialog.FileName = filename;

                this.chargerMonde(filename);
            }
        }

        private void enregistrerMondeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveWorldDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.EnregistrerFichierMonde(saveWorldDialog.FileName, false);
            }
        }

        private void EnregistrerFichierMonde(string path, bool silent, bool updatefromnative = true)
        {
            //pour chaque map, on met à jour les info de tilemap


            string WorldFullPath = path;

            if (!WorldFullPath.EndsWith(".mwf"))
                WorldFullPath += ".mwf";

            FileInfo worldFileInfo = new FileInfo(WorldFullPath);

            if (updatefromnative)
            {
                worldObj.ZoneDatas = renderPanel.GetTilemapArray(renderPanel.CurrentLayer, renderPanel.CurrentMap);

            }


            try
            {
                //sauvegarde niveau
                using (var file = File.Create(WorldFullPath))
                {
                    ProtoBuf.Serializer.Serialize<PBInterface.World>(file, worldObj);
                }

                if (!silent)
                    MessageBox.Show(string.Format("Monde {0} Sauvegardé ! ", worldObj.WorldId), "Sauvegarde ok", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                {
                    LogTextToBox(string.Format("Monde {0} Sauvegardé !", worldObj.WorldId));

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Erreur lors de l'enregistrement du monde {0} , Exception : {1} ", worldObj.WorldId, ex.ToString()), "Sauvegarde ko", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region Gestion fragments

        /// <summary>
        /// Création d'un nouveau fragment / props
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void nouveauFragmentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //si un niveau ou un monde est déjà ouvert, appel du code de fermeture
            if (levelObj != null)
                FermerNiveau(openLevelDialog.FileName);

            if (worldObj != null)
                FermerMonde(openWorldDialog.FileName);

            if (fragObj != null)
                FermerFragment(openFragmentDialog.FileName);

            FragmentDetail formFragment = new FragmentDetail();


            if (formFragment.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                fragObj = new Fragment();
                fragObj.FragId = formFragment.txtidfragment.Text;
                fragObj.GameScriptPath = formFragment.txtScript.Text;

                this.chargerFragment_base(fragObj);
            }
        }


        private void chargerFragment_base(string filename)
        {
            Fragment ofrag = null;

            using (var file = File.OpenRead(filename))
                ofrag = ProtoBuf.Serializer.Deserialize<PBInterface.Fragment>(file);

            if (ofrag != null)
                chargerFragment_base(ofrag);


        }

        private void chargerFragment_base(Fragment fragObj)
        {

            //load textures resources list
            if (game_resources == null)
            {
                game_resources = this.LoadGameResourcesList();
            }

            List<string> script_module = new List<string>();
            List<string> script_funcs = new List<string>();

            this.chargerFragment(fragObj, script_funcs, script_module);

            if (!string.IsNullOrEmpty(fragObj.GameScriptPath))
            {
                this.ajouter_chemin_module(fragObj.GameScriptPath);
            }

            foreach (string module in script_module)
            {
                this.charger_module(module, false);

            }

            StringBuilder buffer = new StringBuilder(10000);

            foreach (string func in script_funcs)
            {

                NativeFunc.editor_exec_string(func, buffer, buffer.Capacity);

                if (buffer.Length == 0)
                    LogTextToBox("[LUA] fonction " + func + " OK");
                else
                    LogTextToBox(buffer.ToString());
            }
        }

        private void chargerFragment(Fragment fragObj, List<string> script_funcs, List<string> script_module)
        {
            tabLayout.Show();
            tabWorldSlice.Hide();

            lua_comm.Hide();

            commands_ref.Clear();

            tabMaps.Show();
            tilesetBox.Show();
            richlog.Show();
            btnshowlogdetail.Show();
            renderPanel.Show();
            panelactions.Show();
            tablistitems.Show();

            tabLayout.TabPages.Remove(tabCalques);

            this.fragObj = fragObj;

            current_mode = GlobalMode.FragEditor;


            renderPanel.StartEngine(0, 0, 0, 0);

            //dummy level object
            Level olevel = new Level();
            olevel.LevelId = "dummy_fragment";
            olevel.TileSize = 32;


            //dummy map object
            Map mapObj = new Map();
            mapObj.MapId = "dummy_fragment_map";
            mapObj.ColCount = 2;
            mapObj.RowCount = 2;
            mapObj.TilesetId = "default_tileset.png";
            mapObj.EditorPos = new Wvector();
            mapObj.EditorPlayerPos = new Wvector();

            Layer cLayer = new Layer();
            cLayer.Order = 0;
            cLayer.LayerId = "background";
            cLayer.Type = (int)TypeLayer.Normal;
            cLayer.ParallaxX = 1.0f;
            cLayer.ParallaxY = 1.0f;
            cLayer.Opacity = 1.0f;
            cLayer.Visible = true;

            mapObj.LayerArrays.Add(cLayer);

            Layer colLayer = new Layer();
            colLayer.Order = 1;
            colLayer.Type = (int)TypeLayer.Collisions;
            colLayer.LayerId = "collisions";
            colLayer.ParallaxX = 1.0f;
            colLayer.ParallaxY = 1.0f;
            colLayer.Opacity = 0.7f;
            colLayer.Visible = true;

            mapObj.LayerArrays.Add(colLayer);

            Layer entityLayer = new Layer();
            entityLayer.Order = 2;
            entityLayer.Type = (int)TypeLayer.Objects;
            entityLayer.LayerId = "entities";
            entityLayer.ParallaxX = 1.0f;
            entityLayer.ParallaxY = 1.0f;
            entityLayer.Opacity = 1.0f;
            entityLayer.Visible = true;

            mapObj.LayerArrays.Add(entityLayer);

            olevel.MapArrays.Add(mapObj);

            levelObj = olevel;


            entityLayer.EntityArrays.AddRange(fragObj.EntityArrays);
            entityLayer.TextArrays.AddRange(fragObj.TextArrays);

            int[] emptytiles = new int[(mapObj.RowCount * mapObj.ColCount)];

            for (int i = 0; i < (mapObj.RowCount * mapObj.ColCount); i++)
            {
                emptytiles[i] = 0;
            }

            cLayer.TileArrays = emptytiles;
            colLayer.TileArrays = (int[])emptytiles.Clone();
            mapObj.CollisionsArrays = (int[])emptytiles.Clone();


            enregistrerFragmentToolStripMenuItem.Enabled = true;
            fermerFragmentToolStripMenuItem.Enabled = true;

            renderPanel.CreateTilemap(mapObj);

            MemoryStream layerStream = new MemoryStream();
            layerStream.Position = 0;
            ProtoBuf.Serializer.Serialize<Layer>(layerStream, cLayer);

            layerStream.Position = 0;

            byte[] tmplayerArray = layerStream.ToArray();

            NativeFunc.editor_addlayer(tmplayerArray, (uint)tmplayerArray.Length);


            layerStream.Position = 0;
            ProtoBuf.Serializer.Serialize<Layer>(layerStream, colLayer);
            layerStream.Position = 0;

            tmplayerArray = layerStream.ToArray();

            layerStream.Close();

            NativeFunc.editor_addlayer(tmplayerArray, (uint)tmplayerArray.Length);



            NativeFunc.editor_showcursor(NativeFunc.wbool.wtrue, NativeFunc.wbool.wfalse);
            renderPanel.UpdateTexture();

            renderPanel.Invalidate();

            NativeFunc.editor_changelayer_visibility(NativeFunc.wbool.wfalse, cLayer.LayerId);

            NativeFunc.editor_draw_frame();


            ItemsList.Nodes["Nodeentity"].Nodes.Clear();
            ItemsList.Nodes["Nodetrigger"].Nodes.Clear();
            ItemsList.Nodes["NodeText"].Nodes.Clear();
            ItemsList.Nodes["NodeSound"].Nodes.Clear();
            ItemsList.Nodes["NodeLight"].Nodes.Clear();
            ItemsList.Nodes["NodeParticles"].Nodes.Clear();
            ItemsList.Nodes["NodeCollider"].Nodes.Clear();
            ItemsList.Nodes["NodeWaypoint"].Nodes.Clear();

            //chargement des entités
            foreach (Entity ent in fragObj.EntityArrays)
            {

                if (NativeFunc.editor_resx_exist(ent.Tileset) == NativeFunc.wbool.wfalse)
                {
                    byte[] content3 = BitmapUtilities.GetBitmapContent(Resources.default_text);

                    NativeFunc.editor_addtextureresx(ent.Tileset, Resources.default_text.Width, Resources.default_text.Height, content3, (uint)content3.Length);
                }

                MemoryStream entityStream = new MemoryStream();
                entityStream.Position = 0;
                ProtoBuf.Serializer.Serialize<Entity>(entityStream, ent);

                entityStream.Position = 0;


                byte[] tmpentityArray = entityStream.ToArray();

                entityStream.Close();

                NativeFunc.editor_addentity(ent.EntityId, tmpentityArray, (uint)tmpentityArray.Length, mapObj.MapId);

                if (!string.IsNullOrEmpty(ent.ScriptModule))
                {
                    if (!script_module.Contains(ent.ScriptModule))
                        script_module.Add(ent.ScriptModule);
                }

                if (!string.IsNullOrEmpty(ent.ScriptCtor))
                {
                    StringBuilder str = new StringBuilder(10000);


                    script_funcs.Add(ScriptUtility.genconstruct(ent));
                }

                TreeNode cNode = new TreeNode();
                cNode.Text = ent.EntityId;
                cNode.Tag = ent;
                cNode.Name = ent.EntityId;

                ItemsList.Nodes["Nodeentity"].Nodes.Add(cNode);
            }

            //chargement des text entity
            foreach (TextObject ent in fragObj.TextArrays)
            {

                MemoryStream entityStream = new MemoryStream();
                entityStream.Position = 0;
                ProtoBuf.Serializer.Serialize<TextObject>(entityStream, ent);

                entityStream.Position = 0;


                byte[] tmpentityArray = entityStream.ToArray();

                entityStream.Close();

                NativeFunc.editor_addtextentity(ent.TextId, tmpentityArray, (uint)tmpentityArray.Length, mapObj.MapId);

                if (!string.IsNullOrEmpty(ent.ScriptModule))
                {
                    if (!script_module.Contains(ent.ScriptModule))
                        script_module.Add(ent.ScriptModule);
                }

                if (!string.IsNullOrEmpty(ent.ScriptCtor))
                {
                    StringBuilder str = new StringBuilder(10000);

                    script_funcs.Add(ScriptUtility.gentextconstruct(ent));
                }


                TreeNode cNode = new TreeNode();
                cNode.Text = ent.TextId;
                cNode.Tag = ent;
                cNode.Name = ent.TextId;

                ItemsList.Nodes["NodeText"].Nodes.Add(cNode);
            }


            renderPanel.CurrentLayer = entityLayer;

            renderPanel.CurrentMap = mapObj;

            renderPanel.CurrentCollisionVal = 0;

            renderPanel.ZoomFactor = 1.0f;


            if (((KeyValuePair<string, float>)cmbZoom.SelectedItem).Value == renderPanel.ZoomFactor)
            {
                this.zoomEvent();
            }
            else
            {
                cmbZoom.SelectedValue = renderPanel.ZoomFactor;
            }

            NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wtrue);

            NativeFunc.editor_set_scroll((float)0.0f, (float)0.0f);

            OglPanel.current_mode = current_mode;
        }

        private void chargerFragmentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string oldfilename = openFragmentDialog.FileName;

            if (openFragmentDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string filename = openFragmentDialog.FileName;
                //si un fragment est déjà ouvert, appel du code de fermeture
                if (fragObj != null)
                    FermerFragment(oldfilename);
                //même chose si un niveau est ouvert

                if (levelObj != null)
                    FermerNiveau(openLevelDialog.FileName);



                openFragmentDialog.FileName = filename;

                this.chargerFragment_base(filename);
            }
        }


        private void préférencesFragmentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FragmentDetail formFragment = new FragmentDetail();

            formFragment.Fill(this.fragObj);


            if (formFragment.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.fragObj.GameScriptPath = formFragment.txtScript.Text;
                this.fragObj.FragId = formFragment.txtidfragment.Text;
            }
        }

        private void enregistrerFragmentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFragmentDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.EnregistrerFichierFrag(saveFragmentDialog.FileName, false);
            }
        }

        private void EnregistrerFichierFrag(string path, bool silent, bool updatefromnative = true)
        {
            string FragFullPath = path;

            if (!FragFullPath.EndsWith(".fwf"))
                FragFullPath += ".fwf";

            FileInfo fragFileInfo = new FileInfo(FragFullPath);

            foreach (Layer dummylayer in renderPanel.CurrentMap.LayerArrays)
            {
                if (dummylayer.Type == (int)TypeLayer.Objects)
                {
                    fragObj.EntityArrays.Clear();
                    fragObj.TextArrays.Clear();
                    fragObj.EntityArrays.AddRange(dummylayer.EntityArrays);
                    fragObj.TextArrays.AddRange(dummylayer.TextArrays);
                }
            }


            try
            {
                //sauvegarde fragment
                using (var file = File.Create(FragFullPath))
                {
                    ProtoBuf.Serializer.Serialize<PBInterface.Fragment>(file, fragObj);
                }

                if (!silent)
                    MessageBox.Show(string.Format("Fragment {0} Sauvegardé ! ", fragObj.FragId), "Sauvegarde ok", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                {
                    LogTextToBox(string.Format("Fragment {0} Sauvegardé !", fragObj.FragId));

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Erreur lors de l'enregistrement du fragment {0} , Exception : {1} ", fragObj.FragId, ex.ToString()), "Sauvegarde ko", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void fermerFragmentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FermerFragment(openFragmentDialog.FileName);
        }

        private void FermerFragment(string filename)
        {
            fragObj = null;

            enregistrerFragmentToolStripMenuItem.Enabled = false;
            fermerFragmentToolStripMenuItem.Enabled = false;

            script_modules.Clear();
            script_base_path = string.Empty;

            //supprimer données natives
            NativeFunc.editor_clean_level();

            CleanWorkbench();

            renderPanel.CurrentLayer = null;
            renderPanel.CurrentMap = null;

            tilesetBox.Image = null;
            current_tile_offset = Settings.Default.tileset_offset;

            tabLayout.Hide();
            tabMaps.Hide();
            tilesetBox.Hide();

            richlog.Hide();
            btnshowlogdetail.Hide();
            renderPanel.Hide();

            panelactions.Hide();

            grpObjects.Hide();

            grpCollisions.Hide();

            //supprimer données managés
            levelObj = null;

            openFragmentDialog.FileName = string.Empty;

            if (commands_ref.Count > 0)
            {
                if (MessageBox.Show("Copier le contenu de la console lua dans le presse papier ?", "Copiez code lua", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    StringBuilder lua_comm = new StringBuilder();
                    foreach (KeyValuePair<string, string> lua_commands in commands_ref)
                    {
                        lua_comm.AppendLine(lua_commands.Value);
                    }

                    Clipboard.SetText(lua_comm.ToString());
                }
            }

            commands_ref.Clear();

        }

        #endregion


        #region Gestion Niveau

        private void NouveauNiveau(object sender, EventArgs e)
        {
            //si un niveau est déjà ouvert, appel du code de fermeture
            if (levelObj != null)
                FermerNiveau(openLevelDialog.FileName);

            if (worldObj != null)
                FermerMonde(openWorldDialog.FileName);

            tabLayout.Show();
            tabMaps.Show();
            tilesetBox.Show();
            tabCalques.Show();
            tablistitems.Show();

            richlog.Show();
            btnshowlogdetail.Show();
            renderPanel.Show();

            panelactions.Show();


            if (lua_comm.IsDisposed)
                lua_comm = new LuaCommands(commands_ref);

            lua_comm.Hide();

            NewLevel formLevel = new NewLevel();
            commands_ref.Clear();

            lua_comm.LoadCommands();

            if (formLevel.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                levelObj = new Level();
                levelObj.LevelId = formLevel.txtidniveau.Text;
                levelObj.TileSize = Settings.Default.TILE_SIZE;
                //creation d'une première map
                this.AjouterMap(sender, e);

                NativeFunc.editor_update_collisions();

                supprimerMapToolStripMenuItem.Enabled = false;

                if (!string.IsNullOrEmpty(formLevel.txtScript.Text))
                {
                    FileInfo scriptFileInfo = new FileInfo(formLevel.txtScript.Text);
                    levelObj.ScriptFile = scriptFileInfo.Name;
                    levelObj.EditorScriptFile = formLevel.txtScript.Text;

                    scriptWatcher.EnableRaisingEvents = true;
                    scriptWatcher.Path = scriptFileInfo.Directory.FullName;
                    this.ajouter_chemin_module(scriptWatcher.Path);

                    this.charger_module(levelObj.ScriptFile.Substring(0, levelObj.ScriptFile.LastIndexOf('.')), false);

                    NativeFunc.editor_set_module(levelObj.ScriptFile.Substring(0, levelObj.ScriptFile.LastIndexOf('.')));

                    NativeFunc.editor_set_call_script(NativeFunc.wbool.wtrue);
                }

                this.MajniveauBackup();

                preftoolStripMenuItem.Enabled = fermerNiveauToolStripMenuItem.Enabled = true;
            }

        }

        private void FermerNiveau(string oldfilename)
        {
            script_modules.Clear();
            script_base_path = string.Empty;

            this.AddHisto(oldfilename);

            //supprimer données natives
            NativeFunc.editor_clean_level();

            CleanWorkbench();

            renderPanel.CurrentLayer = null;
            renderPanel.CurrentMap = null;

            tilesetBox.Image = null;
            current_tile_offset = Settings.Default.tileset_offset;

            tabMaps.SelectedIndex = -1;

            tabLayout.Hide();
            tabMaps.Hide();
            tilesetBox.Hide();

            richlog.Hide();
            btnshowlogdetail.Hide();
            renderPanel.Hide();

            panelactions.Hide();

            grpObjects.Hide();

            grpCollisions.Hide();

            //supprimer données managés
            levelObj = null;

            preftoolStripMenuItem.Enabled = fermerNiveauToolStripMenuItem.Enabled = enregistrerNiveauToolStripMenuItem.Enabled = toolStripButtonSaveLevel.Enabled = false;
            openLevelDialog.FileName = string.Empty;

            if (commands_ref.Count > 0)
            {
                if (MessageBox.Show("Copier le contenu de la console lua dans le presse papier ?", "Copiez code lua", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    StringBuilder lua_comm = new StringBuilder();
                    foreach (KeyValuePair<string, string> lua_commands in commands_ref)
                    {
                        lua_comm.AppendLine(lua_commands.Value);
                    }

                    Clipboard.SetText(lua_comm.ToString());
                }
            }

            commands_ref.Clear();
        }

        private void CleanWorkbench()
        {
            //suppression des tab pages
            tabMaps.TabPages.Clear();

            history.Reset();

            //suppression des tileset chargé

            foreach (Bitmap obmp in tileSets.Values)
                obmp.Dispose();

            foreach (Bitmap obmp in viewable_tileSets.Values)
                obmp.Dispose();


            ItemsList.Nodes["Nodeentity"].Nodes.Clear();
            ItemsList.Nodes["Nodetrigger"].Nodes.Clear();
            ItemsList.Nodes["NodeText"].Nodes.Clear();
            ItemsList.Nodes["NodeSound"].Nodes.Clear();
            ItemsList.Nodes["NodeLight"].Nodes.Clear();
            ItemsList.Nodes["NodeParticles"].Nodes.Clear();
            ItemsList.Nodes["NodeCollider"].Nodes.Clear();
            ItemsList.Nodes["NodeWaypoint"].Nodes.Clear();


            tileSets.Clear();
            viewable_tileSets.Clear();

            //suppression de la liste des calques
            calquesList.Nodes.Clear();
        }


        private void ChargerNiveau(object sender, EventArgs e)
        {
            string oldfilename = openLevelDialog.FileName;

            if (openLevelDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string filename = openLevelDialog.FileName;
                //si un niveau est déjà ouvert, appel du code de fermeture
                if (levelObj != null)
                    FermerNiveau(oldfilename);

                openLevelDialog.FileName = filename;

                this.ChargerNiveau_base(filename);
            }
        }

        private void ChargerNiveau_base(string filename)
        {
            Level olevel = null;

            using (var file = File.OpenRead(filename))
                olevel = ProtoBuf.Serializer.Deserialize<PBInterface.Level>(file);

            if (olevel != null)
                ChargerNiveau_base(olevel);

            this.RemoveHisto(filename);
        }

        private void ChargerNiveau_base(Level olevel)
        {
            current_mode = GlobalMode.Editor;

            levelObj = olevel;

            if (levelObj.TileSize == 0)
                levelObj.TileSize = Settings.Default.TILE_SIZE;


            if (lua_comm.IsDisposed)
                lua_comm = new LuaCommands(commands_ref);


            lua_comm.Hide();

            commands_ref.Clear();

            tabLayout.Show();
            tabMaps.Show();
            tilesetBox.Show();
            richlog.Show();
            btnshowlogdetail.Show();
            renderPanel.Show();
            panelactions.Show();
            tabCalques.Show();
            tablistitems.Show();

            if (!tabLayout.TabPages.Contains(tabCalques))
                tabLayout.TabPages.Insert(0, tabCalques);


            List<string> script_funcs = new List<string>();
            List<string> script_module = new List<string>();

            foreach (Map omap in levelObj.MapArrays)
            {
                this.ChargerMap(omap, script_funcs, script_module);
            }

            if (levelObj.MapArrays.Count > 1)
                supprimerMapToolStripMenuItem.Enabled = true;
            else
                supprimerMapToolStripMenuItem.Enabled = false;


            renderPanel.CurrentLayer = levelObj.MapArrays[0].LayerArrays[0];

            renderPanel.CurrentMap = levelObj.MapArrays[0];

            renderPanel.ZoomFactor = levelObj.MapArrays[0].ZoomValue;

            if (renderPanel.ZoomFactor == 0.0f)
                renderPanel.ZoomFactor = 1.0f;


            if (((KeyValuePair<string, float>)cmbZoom.SelectedItem).Value == renderPanel.ZoomFactor)
            {
                this.zoomEvent();
            }
            else
            {
                cmbZoom.SelectedValue = renderPanel.ZoomFactor;
            }

            if (renderPanel.CurrentLayer.Type == (int)TypeLayer.Normal || renderPanel.CurrentLayer.Type == (int)TypeLayer.Collisions)
                NativeFunc.editor_showcursor(NativeFunc.wbool.wtrue, NativeFunc.wbool.wfalse);
            else if (renderPanel.CurrentLayer.Type == (int)TypeLayer.Objects)
                NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wtrue);

            NativeFunc.editor_set_scroll((float)renderPanel.CurrentMap.EditorPos.X, (float)renderPanel.CurrentMap.EditorPos.Y);

            renderPanel.Invalidate();

            renderPanel.OnSelectedTileChanged += new OglPanel.SelectedTileChanged(renderPanel_OnSelectedTileChanged);

            NativeFunc.editor_update_collisions();

            NativeFunc.editor_setlevel_name(olevel.LevelId);

            NativeFunc.editor_level_loaded();


            enregistrerNiveauToolStripMenuItem.Enabled = toolStripButtonSaveLevel.Enabled = true;

            tabMaps.SelectedIndex = 0;

            EventArgs e = new EventArgs();
            tabMaps_SelectedIndexChanged(tabMaps, e);



            lua_comm.LoadCommands();

            if (!string.IsNullOrEmpty(levelObj.ScriptFile))
            {

                FileInfo scriptFileInfo = new FileInfo(levelObj.EditorScriptFile);

                scriptWatcher.EnableRaisingEvents = true;

                //string true_script = Path.Combine(Settings.Default.raw_assets_path, Path.GetFileName(scriptFileInfo.Directory.FullName));

                scriptWatcher.Path = Settings.Default.raw_assets_path;

                this.ajouter_chemin_module(scriptWatcher.Path);

                string level_module = levelObj.ScriptFile.Substring(0, levelObj.ScriptFile.LastIndexOf('.'));
                this.charger_module(level_module, true);

                NativeFunc.editor_set_module(levelObj.ScriptFile.Substring(0, levelObj.ScriptFile.LastIndexOf('.')));

                NativeFunc.editor_set_call_script(NativeFunc.wbool.wtrue);

                foreach (string module in script_module)
                {
                    this.charger_module(module, false);

                }

                StringBuilder buffer = new StringBuilder(10000);

                UInt32 script_id = NativeFunc.editor_get_script_id();

                string initscript = "init_default_pointer(" + script_id.ToString() + ")";

                NativeFunc.editor_exec_string(initscript, buffer, buffer.Capacity);

                if (buffer.Length == 0)
                    LogTextToBox("[LUA] Pointer par défaut initialisé");
                else
                    LogTextToBox(buffer.ToString());

                buffer.Clear();

                foreach (string func in script_funcs)
                {
                    NativeFunc.editor_exec_string(func, buffer, buffer.Capacity);

                    if (buffer.Length == 0)
                        LogTextToBox("[LUA] fonction " + func + " OK");
                    else
                        LogTextToBox(buffer.ToString());
                }

                //fonction init
                buffer.Clear();
                string function = string.Format("{0}.init()", level_module);
                NativeFunc.editor_exec_string(function, buffer, buffer.Capacity);

                if (buffer.Length == 0)
                    LogTextToBox("[LUA] fonction " + function + " OK");
                else
                    LogTextToBox(buffer.ToString());


            }

            this.MajniveauBackup();

            preftoolStripMenuItem.Enabled = fermerNiveauToolStripMenuItem.Enabled = true;
            OglPanel.current_mode = current_mode;
        }

        private void EnregistrerNiveau(object sender, EventArgs e)
        {

            if (saveLevelDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.EnregistrerFichierNiveau(saveLevelDialog.FileName, false);
            }
        }

        private void MajniveauBackup()
        {
            MemoryStream levelStream = new MemoryStream();
            levelStream.Position = 0;
            ProtoBuf.Serializer.Serialize<Level>(levelStream, levelObj);

            levelStream.Position = 0;

            byte[] tmplevelArray = levelStream.ToArray();

            levelStream.Close();

            NativeFunc.editor_update_level_content(tmplevelArray, (uint)tmplevelArray.Length);
        }


        private void EnregistrerFichierNiveau(string path, bool silent, bool updatefromnative = true)
        {
            //pour chaque map, on met à jour les info de tilemap

            List<string> SaveTilesets = new List<string>(levelObj.MapArrays.Count);


            string LevelFullPath = path;

            if (!LevelFullPath.EndsWith(".lwf"))
                LevelFullPath += ".lwf";

            FileInfo levelFileInfo = new FileInfo(LevelFullPath);

            if (updatefromnative)
            {
                int map_index = 0;

                foreach (Map oMap in levelObj.MapArrays)
                {

                    foreach (Layer oLayer in oMap.LayerArrays)
                    {

                        if (oLayer.Type == (int)TypeLayer.Normal)
                            oLayer.TileArrays = renderPanel.GetTilemapArray(oLayer, oMap);
                        else if (oLayer.Type == (int)TypeLayer.Collisions)
                        {
                            oLayer.TileArrays = renderPanel.GetTilemapArray(oLayer, oMap);
                            oMap.CollisionsArrays = renderPanel.GetTilemapArray(oLayer, oMap);
                        }
                        else if (oLayer.Type == (int)TypeLayer.Objects)
                        {

                            //vérif group entité, suppr entité non valides,ajout entité valide non présentes
                            foreach (GroupEntity ogroup in oLayer.GroupArrays)
                            {
                                List<string> bad_id = (from u in ogroup.IdArrays where !oLayer.EntityArrays.Any<Entity>(p => p.EntityId == u) select u).ToList<string>();

                                ogroup.IdArrays.RemoveAll(p => bad_id.Contains(p));

                                List<string> missing_id = (from u in oLayer.EntityArrays where u.GroupId == ogroup.GroupId && !ogroup.IdArrays.Contains(u.EntityId) select u.EntityId).ToList<string>();

                                ogroup.IdArrays.AddRange(missing_id);
                            }
                        }
                    }

                    //déprécié,uniquement ancien fichier .gal sauvegarde fichier de tile au format png pour chaque map dans le même répertoire que le fichier de niveau
                    if (!SaveTilesets.Contains(oMap.TilesetId) && Path.GetExtension(oMap.TilesetId) == ".gal")
                    {
                        FileInfo tilesetFileInfo = new FileInfo(oMap.TilesetId);

                        int posExt = tilesetFileInfo.Name.LastIndexOf(tilesetFileInfo.Extension);

                        string tilesetname = tilesetFileInfo.Name.Substring(0, posExt) + ".png";

                        using (var file = File.Create(Path.Combine(levelFileInfo.DirectoryName, tilesetname)))
                        {
                            tileSets[oMap.TilesetId].Save(file, ImageFormat.Png);
                        }

                    }

                    map_index++;
                }


                this.MajniveauBackup();
            }

            try
            {


                //sauvegarde niveau
                using (var file = File.Create(LevelFullPath))
                {
                    ProtoBuf.Serializer.Serialize<PBInterface.Level>(file, levelObj);
                }

                if (!silent)
                    MessageBox.Show(string.Format("Niveau {0} Sauvegardé ! ", levelObj.LevelId), "Sauvegarde ok", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                {
                    LogTextToBox(string.Format("Niveau {0} Sauvegardé !", levelObj.LevelId));

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Erreur lors de l'enregistrement du Niveau {0} , Exception : {1} ", levelObj.LevelId, ex.ToString()), "Sauvegarde ko", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void preftoolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (levelObj != null)
            {
                NewLevel formLevel = new NewLevel();

                formLevel.Fill(levelObj);


                if (formLevel.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    levelObj.LevelId = formLevel.txtidniveau.Text;

                    if (!string.IsNullOrEmpty(formLevel.txtScript.Text) && formLevel.txtScript.Text != levelObj.ScriptFile)
                    {
                        FileInfo scriptFileInfo = new FileInfo(formLevel.txtScript.Text);
                        levelObj.ScriptFile = scriptFileInfo.Name;
                        levelObj.EditorScriptFile = formLevel.txtScript.Text;

                        scriptWatcher.EnableRaisingEvents = true;
                        scriptWatcher.Path = scriptFileInfo.Directory.FullName;

                        this.ajouter_chemin_module(scriptWatcher.Path);

                        this.charger_module(levelObj.ScriptFile.Substring(0, levelObj.ScriptFile.LastIndexOf('.')), false);

                        NativeFunc.editor_set_call_script(NativeFunc.wbool.wtrue);
                    }
                }
            }
        }


        #endregion

        #region Gestion Maps

        private void AjouterMap(object sender, EventArgs e)
        {
            NewMap formMap = new NewMap();

            if (formMap.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                //ajout données map avec un calque par défaut
                Map mapObj = new Map();
                mapObj.MapId = formMap.txtidmap.Text;
                mapObj.RowCount = Convert.ToInt32(formMap.numRows.Value);
                mapObj.ColCount = Convert.ToInt32(formMap.numCols.Value);
                mapObj.TilesetId = formMap.txtTileset.Text;

                mapObj.EditorPlayerPos = new Wvector();
                mapObj.EditorPos = new Wvector();

                bool init_watcher = OglPanel._init;

                renderPanel.StartEngine(mapObj.EditorPlayerPos.X, mapObj.EditorPlayerPos.Y, mapObj.EditorPos.X, mapObj.EditorPos.Y);

                //load textures resources list
                if (game_resources == null)
                {
                    game_resources = this.LoadGameResourcesList();
                }

                if (!init_watcher)
                {
                    resxWatcher.Path = Settings.Default.assets_path;
                    resxWatcher.EnableRaisingEvents = true;
                }

                //création bitmap si la tileset n'existe pas déjà
                if (!viewable_tileSets.ContainsKey(mapObj.TilesetId))
                {
                    Bitmap bmpPacked = null;
                    Bitmap bmpView = null;
                   

                    //gestion tilmap ancienne version, déprécié, à supprimer dans une future version
                    if (Path.GetExtension(mapObj.TilesetId) == ".gal")
                    {
                        //récupération du contenu bitmap du fichier gal
                        GaleReader gaReader = new GaleReader();

                        try
                        {
                            gaReader.OpenFile(mapObj.TilesetId);


                            bmpPacked = gaReader.GetPackedContent(TILE_PER_ROW, gaReader.GetWidth(), gaReader.GetHeight());
                            bmpView = gaReader.GetPackedContent(TILE_PER_ROW, gaReader.GetWidth(), gaReader.GetHeight(), Settings.Default.tileset_offset, Settings.Default.tileset_back_color);
                            
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                        }
                        finally
                        {
                            if (gaReader.FileOpened)
                                gaReader.CloseFile();
                        }
                    }
                    else
                    {
                        Bitmap origbitmap = new Bitmap(mapObj.TilesetId);

                        try
                        {
                            bmpPacked = BitmapUtilities.GetBitmapCopy(origbitmap);
                            bmpView = (Bitmap)bmpPacked.Clone();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                        }
                        finally
                        {
                            origbitmap.Dispose();
                        }
                    }


                    byte[] content = BitmapUtilities.GetBitmapContent(bmpPacked);

                    if (bmpPacked != null && bmpView != null)
                    {
                        tileSets.Add(mapObj.TilesetId, bmpPacked);
                        viewable_tileSets.Add(mapObj.TilesetId, bmpView);
                    }
                    else
                        return;

                 


                    if (NativeFunc.editor_resx_exist(mapObj.TilesetId) == NativeFunc.wbool.wfalse)
                    {
                        NativeFunc.editor_addtextureresx(mapObj.TilesetId, bmpPacked.Width, bmpPacked.Height, content, (uint)content.Length);
                    }
                    else
                    {
                        NativeFunc.editor_updatetextureresx(mapObj.TilesetId, bmpPacked.Width, bmpPacked.Height, content, (uint)content.Length);
                    }

                    FileInfo tilesetInfo = new FileInfo(mapObj.TilesetId);

                    tilsetWatcher.Path = tilesetInfo.Directory.FullName;
                    tilsetWatcher.EnableRaisingEvents = true;
                }



                tilesetBox.Image = viewable_tileSets[mapObj.TilesetId];
                current_tile_offset = Settings.Default.tileset_offset;


                calquesList.Nodes.Clear();

                Layer cLayer = new Layer();
                cLayer.Order = 0;
                cLayer.LayerId = "background";
                cLayer.Type = (int)TypeLayer.Normal;
                cLayer.ParallaxX = 1.0f;
                cLayer.ParallaxY = 1.0f;
                cLayer.Opacity = 1.0f;
                cLayer.Visible = true;

                mapObj.LayerArrays.Add(cLayer);

                Layer colLayer = new Layer();
                colLayer.Order = 0;
                colLayer.Type = (int)TypeLayer.Collisions;
                colLayer.LayerId = "collisions";
                colLayer.ParallaxX = 1.0f;
                colLayer.ParallaxY = 1.0f;
                colLayer.Opacity = 0.7f;
                colLayer.Visible = true;

                mapObj.LayerArrays.Add(colLayer);


                TreeNode cNode = new TreeNode();
                cNode.Checked = true;
                cNode.Text = cLayer.LayerId;
                cNode.Name = cLayer.LayerId;
                cNode.Tag = cLayer;

                calquesList.Nodes.Add(cNode);

                TreeNode colNode = new TreeNode();
                colNode.Checked = true;
                colNode.Text = colLayer.LayerId;
                colNode.Name = colLayer.LayerId;
                colNode.Tag = colLayer;

                calquesList.Nodes.Add(colNode);


                tabMaps.TabPages.Add(mapObj.MapId, mapObj.MapId);

                int[] emptytiles = new int[mapObj.RowCount * mapObj.ColCount];

                for (int i = 0; i < mapObj.RowCount * mapObj.ColCount; i++)
                {
                    emptytiles[i] = 0;
                }


                cLayer.TileArrays = emptytiles;
                colLayer.TileArrays = (int[])emptytiles.Clone();
                mapObj.CollisionsArrays = (int[])emptytiles.Clone();


                renderPanel.CreateTilemap(mapObj);

                MemoryStream layerStream = new MemoryStream();
                layerStream.Position = 0;
                ProtoBuf.Serializer.Serialize<Layer>(layerStream, cLayer);

                layerStream.Position = 0;

                byte[] tmplayerArray = layerStream.ToArray();


                NativeFunc.editor_addlayer(tmplayerArray, (uint)tmplayerArray.Length);

                layerStream.Position = 0;
                ProtoBuf.Serializer.Serialize<Layer>(layerStream, colLayer);
                layerStream.Position = 0;

                tmplayerArray = layerStream.ToArray();

                layerStream.Close();

                NativeFunc.editor_addlayer(tmplayerArray, (uint)tmplayerArray.Length);


                NativeFunc.editor_showcursor(NativeFunc.wbool.wtrue, NativeFunc.wbool.wfalse);
                renderPanel.UpdateTexture();

                renderPanel.CurrentLayer = cLayer;

                enregistrerNiveauToolStripMenuItem.Enabled = toolStripButtonSaveLevel.Enabled = true;

                levelObj.MapArrays.Add(mapObj);

                renderPanel.CurrentMap = mapObj;

                calquesList.SelectedNode = cNode;

            }



        }

        private void ChargerMap(Map oMap, List<string> script_funcs, List<string> script_module)
        {
            //récupération du contenu bitmap du fichier gal
            GaleReader gaReader = new GaleReader();

            Bitmap bmpPacked = null;
            Bitmap bmpView = null;

            bool init_watcher = OglPanel._init;

            renderPanel.StartEngine(oMap.EditorPlayerPos.X, oMap.EditorPlayerPos.Y, oMap.EditorPos.X, oMap.EditorPos.Y);

            //load textures resources list
            if (game_resources == null)
            {
                game_resources = this.LoadGameResourcesList();
            }

            if (!init_watcher)
            {
                resxWatcher.Path = Settings.Default.assets_path;
                resxWatcher.EnableRaisingEvents = true;
            }

            string true_id = Path.Combine(Settings.Default.raw_assets_path, Path.GetFileName(oMap.TilesetId));
            oMap.TilesetId = true_id;
            
            //déprécié
            if(Path.GetExtension(true_id) == ".gal")
            {
                try
                {
                    gaReader.OpenFile(true_id);

                    bmpPacked = gaReader.GetPackedContent(TILE_PER_ROW, gaReader.GetWidth(), gaReader.GetHeight());
                    bmpView = gaReader.GetPackedContent(TILE_PER_ROW, gaReader.GetWidth(), gaReader.GetHeight(), Settings.Default.tileset_offset, Settings.Default.tileset_back_color);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());

                    //use default texture
                    bmpPacked = Resources.default_text;
                    bmpView = Resources.default_text;
                }
                finally
                {
                    if (gaReader.FileOpened)
                        gaReader.CloseFile();
                }
            }
            else
            {
                Bitmap origbitmap = new Bitmap(true_id);

                try
                {
                    bmpPacked = BitmapUtilities.GetBitmapCopy(origbitmap);
                    bmpView = (Bitmap)bmpPacked.Clone();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                finally
                {
                    origbitmap.Dispose();
                }
            }
           

            if (bmpPacked != null)
            {
                if (!tileSets.ContainsKey(true_id))
                {
                    tileSets.Add(true_id, bmpPacked);
                    viewable_tileSets.Add(true_id, bmpView);
                }
            }
            else
                return;

            FileInfo tilesetInfo = new FileInfo(true_id);

            tilsetWatcher.Path = tilesetInfo.Directory.FullName;
            tilsetWatcher.EnableRaisingEvents = true;

            tilesetBox.Image = viewable_tileSets[true_id];
            current_tile_offset = Settings.Default.tileset_offset;


            tabMaps.TabPages.Add(oMap.MapId, oMap.MapId);


            byte[] content = BitmapUtilities.GetBitmapContent(tileSets[true_id]);


            if (NativeFunc.editor_resx_exist(true_id) == NativeFunc.wbool.wfalse)
            {
                NativeFunc.editor_addtextureresx(true_id, tileSets[true_id].Width, tileSets[true_id].Height, content, (uint)content.Length);
            }
            else
            {
                NativeFunc.editor_updatetextureresx(true_id, tileSets[true_id].Width, tileSets[true_id].Height, content, (uint)content.Length);
            }

            List<Layer> ordered_list = (from u in oMap.LayerArrays orderby u.Order select u).ToList<Layer>();

            if (oMap.EditorPlayerPos == null)
                oMap.EditorPlayerPos = new Wvector();

            if (oMap.EditorPos == null)
            {
                oMap.EditorPos = new Wvector();
                oMap.EditorPos.X = oMap.EditorPlayerPos.X;
                oMap.EditorPos.Y = oMap.EditorPlayerPos.Y;
            }

            renderPanel.CreateTilemap(oMap);

            foreach (Layer oLayer in ordered_list)
            {

                if (oLayer.Type == (int)TypeLayer.Normal || oLayer.Type == (int)TypeLayer.Collisions)
                {
                    if (oLayer.Type == (int)TypeLayer.Collisions)
                    {
                        oLayer.TileArrays = (int[])oMap.CollisionsArrays.Clone();
                    }

                    //if we have a tileset specified for this layer, check if it's in resources (only straight png are supported, no gal file!)
                    if (!string.IsNullOrEmpty(oLayer.TilesetId))
                    {
                        //old system, deprecated but will be kept in place for now, the tilesetid is the full path to the file
                        if (oLayer.TilesetId.Contains(Path.DirectorySeparatorChar))
                        {
                            string true_id_lay = Path.Combine(Settings.Default.raw_assets_path, Path.GetFileName(oLayer.TilesetId));
                            oLayer.TilesetId = true_id_lay;
                        }
                        else
                        {
                            //new system, the tileset used is the name of the resource without any path information
                            string true_id_lay = Path.Combine(Settings.Default.assets_path, oLayer.TilesetId);
                            oLayer.TilesetId = true_id_lay;
                        }

                        if (File.Exists(oLayer.TilesetId))
                        {
                            Bitmap cbmp = new Bitmap(oLayer.TilesetId);
                            byte[] layer_content = BitmapUtilities.GetBitmapContent(cbmp);

                            //bool dispose_bmp = true;

                            if (!tileSets.ContainsKey(oLayer.TilesetId))
                            {
                                Bitmap editor_bmp = new Bitmap(cbmp);
                                tileSets.Add(oLayer.TilesetId, editor_bmp);
                                viewable_tileSets.Add(oLayer.TilesetId, editor_bmp);
                            }

                            if (NativeFunc.editor_resx_exist(oLayer.TilesetId) == NativeFunc.wbool.wfalse)
                            {
                                NativeFunc.editor_addtextureresx(oLayer.TilesetId, cbmp.Width, cbmp.Height, layer_content, (uint)layer_content.Length);
                            }
                            else
                            {
                                NativeFunc.editor_updatetextureresx(oLayer.TilesetId, cbmp.Width, cbmp.Height, layer_content, (uint)layer_content.Length);
                            }

                            // if(dispose_bmp)
                            // cbmp.Dispose();
                            cbmp.Dispose();
                        }
                        else
                        {
                            LogTextToBox("Can't find tileset! " + oLayer.TilesetId);
                        }
                    }

                    MemoryStream layerStream = new MemoryStream();
                    layerStream.Position = 0;
                    ProtoBuf.Serializer.Serialize<Layer>(layerStream, oLayer);

                    layerStream.Position = 0;

                    byte[] tmplayerArray = layerStream.ToArray();

                    layerStream.Close();


                    NativeFunc.editor_addlayer(tmplayerArray, (uint)tmplayerArray.Length);

                    if (oLayer.Destructible)
                        NativeFunc.editor_setdestructible(oLayer.LayerId, oLayer.DestructTileId);

                }
                else if (oLayer.Type == (int)TypeLayer.Objects)
                {

                    //chargement des triggers
                    foreach (Trigger trig in oLayer.TriggerArrays)
                    {
                        MemoryStream trigStream = new MemoryStream();
                        trigStream.Position = 0;
                        ProtoBuf.Serializer.Serialize<Trigger>(trigStream, trig);

                        trigStream.Position = 0;


                        byte[] tmptrigArray = trigStream.ToArray();

                        trigStream.Close();

                        NativeFunc.editor_addtrigger(trig.TriggerId, tmptrigArray, (uint)tmptrigArray.Length, oMap.MapId, oLayer.Order);
                    }

                    //chargement des entités
                    foreach (Entity ent in oLayer.EntityArrays)
                    {

                        if (NativeFunc.editor_resx_exist(ent.Tileset) == NativeFunc.wbool.wfalse)
                        {
                            byte[] content3 = BitmapUtilities.GetBitmapContent(Resources.default_text);

                            NativeFunc.editor_addtextureresx(ent.Tileset, Resources.default_text.Width, Resources.default_text.Height, content3, (uint)content3.Length);
                        }

                        MemoryStream entityStream = new MemoryStream();
                        entityStream.Position = 0;
                        ProtoBuf.Serializer.Serialize<Entity>(entityStream, ent);

                        entityStream.Position = 0;


                        byte[] tmpentityArray = entityStream.ToArray();

                        entityStream.Close();

                        NativeFunc.editor_addentity(ent.EntityId, tmpentityArray, (uint)tmpentityArray.Length, oMap.MapId);

                        if (!string.IsNullOrEmpty(ent.ScriptModule))
                        {
                            if (!script_module.Contains(ent.ScriptModule))
                                script_module.Add(ent.ScriptModule);
                        }

                        if (!string.IsNullOrEmpty(ent.ScriptCtor))
                        {
                            StringBuilder str = new StringBuilder(10000);

                            script_funcs.Add(ScriptUtility.genconstruct(ent));
                        }
                    }

                    //chargement des text entity
                    foreach (TextObject ent in oLayer.TextArrays)
                    {

                        MemoryStream entityStream = new MemoryStream();
                        entityStream.Position = 0;
                        ProtoBuf.Serializer.Serialize<TextObject>(entityStream, ent);

                        entityStream.Position = 0;


                        byte[] tmpentityArray = entityStream.ToArray();

                        entityStream.Close();

                        NativeFunc.editor_addtextentity(ent.TextId, tmpentityArray, (uint)tmpentityArray.Length, oMap.MapId);

                        if (!string.IsNullOrEmpty(ent.ScriptModule))
                        {
                            if (!script_module.Contains(ent.ScriptModule))
                                script_module.Add(ent.ScriptModule);
                        }

                        if (!string.IsNullOrEmpty(ent.ScriptCtor))
                        {
                            StringBuilder str = new StringBuilder(10000);

                            script_funcs.Add(ScriptUtility.gentextconstruct(ent));
                        }
                    }

                    //chargement des son
                    foreach (SoundSource snd_src in oLayer.SoundArrays)
                    {
                        MemoryStream soundStream = new MemoryStream();
                        soundStream.Position = 0;
                        ProtoBuf.Serializer.Serialize<SoundSource>(soundStream, snd_src);
                        soundStream.Position = 0;

                        byte[] tmpsoundarray = soundStream.ToArray();
                        soundStream.Close();

                        NativeFunc.editor_addsound(snd_src.SoundId, tmpsoundarray, (uint)tmpsoundarray.Length, oMap.MapId, oLayer.Order);
                    }

                    //chargement des lumières
                    foreach (LightSource light_src in oLayer.LightArrays)
                    {
                        MemoryStream lightStream = new MemoryStream();
                        lightStream.Position = 0;
                        ProtoBuf.Serializer.Serialize<LightSource>(lightStream, light_src);
                        lightStream.Position = 0;

                        byte[] tmplightarray = lightStream.ToArray();
                        lightStream.Close();

                        NativeFunc.editor_addlight(light_src.LightId, tmplightarray, (uint)tmplightarray.Length, oMap.MapId, oLayer.Order);
                    }

                    //chargement des particules
                    foreach (ParticlesData particles_data in oLayer.ParticlesArrays)
                    {
                        MemoryStream particleStream = new MemoryStream();
                        particleStream.Position = 0;
                        ProtoBuf.Serializer.Serialize<ParticlesData>(particleStream, particles_data);
                        particleStream.Position = 0;

                        byte[] tmpparticlearray = particleStream.ToArray();
                        particleStream.Close();

                        NativeFunc.editor_addparticles(particles_data.Id, tmpparticlearray, (uint)tmpparticlearray.Length, oMap.MapId, oLayer.Order);
                    }

                    //chargement des waypoint
                    foreach (Waypoint waydata in oLayer.Waypoints)
                    {
                        MemoryStream waypointStream = new MemoryStream();
                        waypointStream.Position = 0;
                        ProtoBuf.Serializer.Serialize<Waypoint>(waypointStream, waydata);
                        waypointStream.Position = 0;

                        byte[] tmpwaypointarray = waypointStream.ToArray();
                        waypointStream.Close();

                        NativeFunc.editor_addwaypoint(waydata.Id, tmpwaypointarray, (uint)tmpwaypointarray.Length, oMap.MapId, oLayer.Order);
                    }

                    //chargement des collider
                    foreach (Collider colliderdata in oLayer.Colliders)
                    {
                        MemoryStream colliderStream = new MemoryStream();
                        colliderStream.Position = 0;
                        ProtoBuf.Serializer.Serialize<Collider>(colliderStream, colliderdata);
                        colliderStream.Position = 0;

                        byte[] tmpcolliderarray = colliderStream.ToArray();
                        colliderStream.Close();

                        NativeFunc.editor_addcollider(colliderdata.Id, tmpcolliderarray, (uint)tmpcolliderarray.Length, oMap.MapId, oLayer.Order);
                    }

                }
            }

        }

        public void renderPanel_OnSelectedTileChanged()
        {
            //refresh de la tilebox
            tilesetBox.Invalidate();

        }

        private void tabMaps_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabMaps.SelectedIndex == -1)
                return;

            Map mapObj = levelObj.MapArrays[tabMaps.SelectedIndex];

            NativeFunc.editor_switchmap(mapObj.MapId);
            renderPanel.CurrentLayer = mapObj.LayerArrays[0];
            renderPanel.CurrentMap = mapObj;
            renderPanel.RowCount = mapObj.RowCount;
            renderPanel.ColCount = mapObj.ColCount;

            tilesetBox.Image = viewable_tileSets[mapObj.TilesetId];
            current_tile_offset = Settings.Default.tileset_offset;
            tilesetBox.Invalidate();

            calquesList.Nodes.Clear();

            List<Layer> ordered_list = (from u in mapObj.LayerArrays orderby u.Order select u).ToList<Layer>();

            string firstnodeid = string.Empty;

            foreach (Layer oLayer in ordered_list)
            {
                TreeNode cNode = new TreeNode();
                cNode.Checked = oLayer.Visible;
                cNode.Text = oLayer.LayerId;
                cNode.Tag = oLayer;
                cNode.Name = oLayer.LayerId;

                if (string.IsNullOrEmpty(firstnodeid))
                    firstnodeid = oLayer.LayerId;

                NativeFunc.wbool l_visible = (oLayer.Visible) ? NativeFunc.wbool.wtrue : NativeFunc.wbool.wfalse;
                renderPanel.ChangeLayerVisibility(l_visible, oLayer);

                calquesList.Nodes.Add(cNode);
            }

            if (!string.IsNullOrEmpty(firstnodeid))
                calquesList.SelectedNode = calquesList.Nodes.Find(firstnodeid, false)[0];

            ItemsList.Nodes["Nodeentity"].Nodes.Clear();
            ItemsList.Nodes["Nodetrigger"].Nodes.Clear();
            ItemsList.Nodes["NodeText"].Nodes.Clear();
            ItemsList.Nodes["NodeSound"].Nodes.Clear();
            ItemsList.Nodes["NodeLight"].Nodes.Clear();
            ItemsList.Nodes["NodeParticles"].Nodes.Clear();
            ItemsList.Nodes["NodeWaypoint"].Nodes.Clear();
            ItemsList.Nodes["NodeCollider"].Nodes.Clear();

            List<Entity> ordered_entity = (from u in ordered_list[0].EntityArrays orderby u.EntityId select u).ToList<Entity>();
            List<Trigger> ordered_trigger = (from u in ordered_list[0].TriggerArrays orderby u.TriggerId select u).ToList<Trigger>();
            List<TextObject> ordered_text = (from u in ordered_list[0].TextArrays orderby u.TextId select u).ToList<TextObject>();
            List<GroupEntity> ordered_groupe = (from u in ordered_list[0].GroupArrays orderby u.GroupId select u).ToList<GroupEntity>();
            List<SoundSource> ordered_sound = (from u in ordered_list[0].SoundArrays orderby u.SoundId select u).ToList<SoundSource>();

            List<LightSource> ordered_light = (from u in ordered_list[0].LightArrays orderby u.LightId select u).ToList<LightSource>();

            List<ParticlesData> ordered_part = (from u in ordered_list[0].ParticlesArrays orderby u.Id select u).ToList<ParticlesData>();
            List<Waypoint> ordered_way = (from u in ordered_list[0].Waypoints orderby u.Id select u).ToList<Waypoint>();
            List<Collider> ordered_col = (from u in ordered_list[0].Colliders orderby u.Id select u).ToList<Collider>();


            foreach (GroupEntity ogroup in ordered_groupe)
            {
                TreeNode cNode = new TreeNode();
                cNode.Text = ogroup.GroupId;
                cNode.Tag = ogroup;
                cNode.Name = "group_" + ogroup.GroupId;

                ItemsList.Nodes["Nodeentity"].Nodes.Add(cNode);
            }

            foreach (Entity oent in ordered_entity)
            {
                TreeNode cNode = new TreeNode();
                cNode.Text = oent.EntityId;
                cNode.Tag = oent;
                cNode.Name = oent.EntityId;

                if (string.IsNullOrEmpty(oent.GroupId))
                    ItemsList.Nodes["Nodeentity"].Nodes.Add(cNode);
                else
                    ItemsList.Nodes["Nodeentity"].Nodes["group_" + oent.GroupId].Nodes.Add(cNode);
            }

            foreach (Trigger otrig in ordered_trigger)
            {
                TreeNode cNode = new TreeNode();
                cNode.Text = otrig.TriggerId;
                cNode.Tag = otrig;
                cNode.Name = otrig.TriggerId;

                ItemsList.Nodes["Nodetrigger"].Nodes.Add(cNode);
            }

            foreach (TextObject otext in ordered_text)
            {
                TreeNode cNode = new TreeNode();
                cNode.Text = otext.TextId;
                cNode.Tag = otext;
                cNode.Name = otext.TextId;

                ItemsList.Nodes["NodeText"].Nodes.Add(cNode);
            }

            foreach (SoundSource osound in ordered_sound)
            {
                TreeNode cNode = new TreeNode();
                cNode.Text = osound.SoundId;
                cNode.Tag = osound;
                cNode.Name = osound.SoundId;

                ItemsList.Nodes["NodeSound"].Nodes.Add(cNode);
            }

            foreach (LightSource olight in ordered_light)
            {
                TreeNode cNode = new TreeNode();
                cNode.Text = olight.LightId;
                cNode.Tag = olight;
                cNode.Name = olight.LightId;

                ItemsList.Nodes["NodeLight"].Nodes.Add(cNode);
            }

            foreach (ParticlesData opart in ordered_part)
            {
                TreeNode cNode = new TreeNode();
                cNode.Text = opart.Id;
                cNode.Tag = opart;
                cNode.Name = opart.Id;

                ItemsList.Nodes["NodeParticles"].Nodes.Add(cNode);
            }


            foreach (Waypoint oway in ordered_way)
            {
                TreeNode cNode = new TreeNode();
                cNode.Text = oway.Id;
                cNode.Tag = oway;
                cNode.Name = oway.Id;

                ItemsList.Nodes["NodeWaypoint"].Nodes.Add(cNode);
            }


            foreach (Collider ocol in ordered_col)
            {
                TreeNode cNode = new TreeNode();
                cNode.Text = ocol.Id;
                cNode.Tag = ocol;
                cNode.Name = ocol.Id;

                ItemsList.Nodes["NodeCollider"].Nodes.Add(cNode);
            }


            //view on point
            NativeFunc.editor_set_scroll(mapObj.EditorPos.X, mapObj.EditorPos.Y);
            NativeFunc.editor_setplayer_pos(mapObj.EditorPlayerPos.X, mapObj.EditorPlayerPos.Y);
        }


        private void ajouterMapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (levelObj.MapArrays.Count > 0)
                supprimerMapToolStripMenuItem.Enabled = true;

            AjouterMap(sender, e);
            tabMaps.SelectedIndex = tabMaps.TabPages.Count - 1;
            this.MajniveauBackup();
        }

        private void modifierMapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewMap formMap = new NewMap();
            formMap.FillData(renderPanel.CurrentMap);

            if (formMap.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                int oldRowCount = renderPanel.CurrentMap.RowCount;
                int oldColCount = renderPanel.CurrentMap.ColCount;
                //change map value and redimension it if needed
                NativeFunc.editor_changemap_id(renderPanel.CurrentMap.MapId, formMap.txtidmap.Text);
                renderPanel.CurrentMap.MapId = formMap.txtidmap.Text;
                tabMaps.SelectedTab.Text = renderPanel.CurrentMap.MapId;
                //change native map id



                renderPanel.CurrentMap.RowCount = renderPanel.RowCount = Convert.ToInt32(formMap.numRows.Value);
                renderPanel.CurrentMap.ColCount = renderPanel.ColCount = Convert.ToInt32(formMap.numCols.Value);
                renderPanel.CurrentMap.TilesetId = formMap.txtTileset.Text;

                int diff_col = renderPanel.CurrentMap.ColCount - oldColCount;
                int diff_row = renderPanel.CurrentMap.RowCount - oldRowCount;


                if (oldRowCount != renderPanel.CurrentMap.RowCount || oldColCount != renderPanel.CurrentMap.ColCount)
                {
                    if (formMap.cmbSensAjoutLigne.SelectedIndex == -1)
                        formMap.cmbSensAjoutLigne.SelectedIndex = 0;

                    if (formMap.cmbSensAjoutCol.SelectedIndex == -1)
                        formMap.cmbSensAjoutCol.SelectedIndex = 0;

                    NativeFunc.editor_Tilemap_resize(renderPanel.CurrentMap.RowCount, renderPanel.CurrentMap.ColCount, (short)formMap.cmbSensAjoutLigne.SelectedIndex, (short)formMap.cmbSensAjoutCol.SelectedIndex);


                    //maj valeur animation
                    foreach (Layer clayer in renderPanel.CurrentMap.LayerArrays)
                    {
                        if (clayer.Animated)
                        {
                            for (int iframe = 0; iframe < clayer.Frames.Count; iframe++)
                            {
                                for (int inupd = 0; inupd < clayer.Frames[iframe].TileUpdates.Count; inupd++)
                                {
                                    int aniRow = (int)Math.Floor((double)clayer.Frames[iframe].TileUpdates[inupd].Index / oldColCount);//ligne Y
                                    int aniCol = clayer.Frames[iframe].TileUpdates[inupd].Index - (oldColCount * aniRow);//ligne X

                                    if (formMap.cmbSensAjoutCol.SelectedIndex == 0)
                                        clayer.Frames[iframe].TileUpdates[inupd].Index += (diff_col * aniRow);
                                    else if (formMap.cmbSensAjoutCol.SelectedIndex == 1)
                                        clayer.Frames[iframe].TileUpdates[inupd].Index += (diff_col * (1 + aniRow));

                                    if (formMap.cmbSensAjoutLigne.SelectedIndex == 1)
                                        clayer.Frames[iframe].TileUpdates[inupd].Index += (diff_row * oldColCount);
                                }
                            }
                        }
                    }


                    //si la map est redimensionnée vers la gauche et/ou vers le bas, repositionner également les entités
                    if (formMap.cmbSensAjoutLigne.SelectedIndex == 1 || formMap.cmbSensAjoutCol.SelectedIndex == 1)
                    {
                        int resize_left = (formMap.cmbSensAjoutCol.SelectedIndex == 1) ? renderPanel.CurrentMap.ColCount - oldColCount : 0;

                        resize_left *= Settings.Default.TILE_SIZE;

                        int resize_bottom = (formMap.cmbSensAjoutLigne.SelectedIndex == 1) ? renderPanel.CurrentMap.RowCount - oldRowCount : 0;

                        resize_bottom *= Settings.Default.TILE_SIZE;


                        foreach (Layer clayer in renderPanel.CurrentMap.LayerArrays)
                        {

                            foreach (Entity centity in clayer.EntityArrays)
                            {
                                NativeFunc.editor_entity_update_pos(centity.EntityId, centity.Position.X + resize_left, centity.Position.Y + resize_bottom, NativeFunc.GAME_TYPE.GT_ENTITY);
                                centity.Position.X = centity.Position.X + resize_left;
                                centity.Position.Y = centity.Position.Y + resize_bottom;
                            }

                            foreach (Trigger centity in clayer.TriggerArrays)
                            {
                                NativeFunc.editor_entity_update_pos(centity.TriggerId, centity.Position.X + resize_left, centity.Position.Y + resize_bottom, NativeFunc.GAME_TYPE.GT_TRIGGER);
                                centity.Position.X = centity.Position.X + resize_left;
                                centity.Position.Y = centity.Position.Y + resize_bottom;
                            }

                            foreach (TextObject centity in clayer.TextArrays)
                            {
                                NativeFunc.editor_entity_update_pos(centity.TextId, centity.Position.X + resize_left, centity.Position.Y + resize_bottom, NativeFunc.GAME_TYPE.GT_TEXT);
                                centity.Position.X = centity.Position.X + resize_left;
                                centity.Position.Y = centity.Position.Y + resize_bottom;
                            }

                            foreach (SoundSource sound in clayer.SoundArrays)
                            {
                                NativeFunc.editor_entity_update_pos(sound.SoundId, sound.Position.X + resize_left, sound.Position.Y + resize_bottom, NativeFunc.GAME_TYPE.GT_SOUND);
                                sound.Position.X = sound.Position.X + resize_left;
                                sound.Position.X = sound.Position.X + resize_bottom;
                            }

                            foreach (LightSource light in clayer.LightArrays)
                            {
                                NativeFunc.editor_entity_update_pos(light.LightId, light.Position.X + resize_left, light.Position.Y + resize_bottom, NativeFunc.GAME_TYPE.GT_LIGHT);
                                light.Position.X = light.Position.X + resize_left;
                                light.Position.X = light.Position.X + resize_bottom;
                            }

                            foreach (ParticlesData particles in clayer.ParticlesArrays)
                            {
                                NativeFunc.editor_entity_update_pos(particles.Id, particles.Position.X + resize_left, particles.Position.Y + resize_bottom, NativeFunc.GAME_TYPE.GT_PARTICLES);
                                particles.Position.X = particles.Position.X + resize_left;
                                particles.Position.X = particles.Position.X + resize_bottom;
                            }

                            foreach (Waypoint way in clayer.Waypoints)
                            {
                                NativeFunc.editor_entity_update_pos(way.Id, way.Position.X + resize_left, way.Position.Y + resize_bottom, NativeFunc.GAME_TYPE.GT_WAYPOINT);
                                way.Position.X = way.Position.X + resize_left;
                                way.Position.X = way.Position.X + resize_bottom;
                            }

                            foreach (Collider collider in clayer.Colliders)
                            {
                                NativeFunc.editor_updatecolliderposition(collider.Id, (float)collider.Position.X + resize_left, (float)collider.Position.Y + resize_bottom);
                                collider.Position.X = collider.Position.X + resize_left;
                                collider.Position.X = collider.Position.X + resize_bottom;
                            }

                        }

                    }


                }

                //création bitmap si la tileset n'existe pas déjà
                if (!viewable_tileSets.ContainsKey(renderPanel.CurrentMap.TilesetId))
                {
                    //récupération du contenu bitmap du fichier gal
                    GaleReader gaReader = new GaleReader();

                    Bitmap bmpPacked = null;
                    Bitmap bmpView = null;

                    if(Path.GetExtension(renderPanel.CurrentMap.TilesetId) == ".gal")
                    {
                        try
                        {
                            gaReader.OpenFile(renderPanel.CurrentMap.TilesetId);


                            bmpPacked = gaReader.GetPackedContent(TILE_PER_ROW, gaReader.GetWidth(), gaReader.GetHeight());
                            bmpView = gaReader.GetPackedContent(TILE_PER_ROW, gaReader.GetWidth(), gaReader.GetHeight(), Settings.Default.tileset_offset, Settings.Default.tileset_back_color);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                        }
                        finally
                        {
                            if (gaReader.FileOpened)
                                gaReader.CloseFile();
                        }
                    }
                    else
                    {
                        bmpPacked = new Bitmap(renderPanel.CurrentMap.TilesetId);
                        bmpView = (Bitmap)bmpPacked.Clone();
                    }
                    

                    if (bmpPacked != null && bmpView != null)
                    {
                        tileSets.Add(renderPanel.CurrentMap.TilesetId, bmpPacked);
                        viewable_tileSets.Add(renderPanel.CurrentMap.TilesetId, bmpView);
                    }
                    else
                        return;

                    byte[] content = BitmapUtilities.GetBitmapContent(bmpPacked);


                    if (NativeFunc.editor_resx_exist(renderPanel.CurrentMap.TilesetId) == NativeFunc.wbool.wfalse)
                    {
                        NativeFunc.editor_addtextureresx(renderPanel.CurrentMap.TilesetId, bmpPacked.Width, bmpPacked.Height, content, (uint)content.Length);
                    }
                    else
                    {
                        NativeFunc.editor_updatetextureresx(renderPanel.CurrentMap.TilesetId, bmpPacked.Width, bmpPacked.Height, content, (uint)content.Length);
                    }

                    FileInfo tilesetInfo = new FileInfo(renderPanel.CurrentMap.TilesetId);

                    tilsetWatcher.Path = tilesetInfo.Directory.FullName;
                    tilsetWatcher.EnableRaisingEvents = true;
                }



                tilesetBox.Image = viewable_tileSets[renderPanel.CurrentMap.TilesetId];

                this.MajniveauBackup();
            }
        }

        private void supprimerMapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (tabMaps.SelectedIndex == -1)
            {
                MessageBox.Show("Aucune map sélectionnée!");
                return;
            }




            if (MessageBox.Show("Supprimer la map " + levelObj.MapArrays[tabMaps.SelectedIndex].MapId + " ?", "Suppression déclencheur", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {

                levelObj.MapArrays.RemoveAt(tabMaps.SelectedIndex);
                tabMaps.TabPages.RemoveAt(tabMaps.SelectedIndex);

                tabMaps.SelectedIndex = 0;

                tabMaps_SelectedIndexChanged(this, e);

                this.MajniveauBackup();

                if (levelObj.MapArrays.Count == 1)
                    supprimerMapToolStripMenuItem.Enabled = false;
            }

        }


        #endregion

        #region Fonction globale workbench

        private void LogTextToBox(string texttolog)
        {
            richlog.AppendText(DateTime.Now.TimeOfDay.ToString(@"hh\:mm\:ss") + " " + texttolog);
            richlog.AppendText(Environment.NewLine);
            richlog.ScrollToCaret();
        }

        private void btnshowlogdetail_Click(object sender, EventArgs e)
        {
            LogDetails logdetail = new LogDetails(richlog.Text);

            logdetail.ShowDialog();
        }

        float movex = 0.0f, movey = 0.0f;

        private void Workbench_KeyPress(object sender, KeyPressEventArgs e)
        {
            movex = movey = 0.0f;

            switch (e.KeyChar)
            {
                case (char)122://z
                    movey = 1;
                    break;
                case (char)115://s
                    movey = -1;
                    break;
                case (char)113://q
                    movex = -1;
                    break;
                case (char)100://d
                    movex = 1;
                    break;
                case (char)43://+
                    SCROLL_SPEED += 5;
                    Settings.Default.SCROLL_SPEED = SCROLL_SPEED;
                    Settings.Default.Save();
                    break;
                case (char)45://-
                    if (SCROLL_SPEED > 5)
                    {
                        SCROLL_SPEED -= 5;
                    }

                    Settings.Default.SCROLL_SPEED = SCROLL_SPEED;
                    Settings.Default.Save();
                    break;

            }

            if (current_mode != GlobalMode.Play)
            {
                if (levelObj != null && levelObj.MapArrays.Count > 0)
                {
                    int index = tabMaps.SelectedIndex;

                    if (index < 0)
                    {
                        index = 0;
                    }


                    NativeFunc.editor_scroll(movex * SCROLL_SPEED, movey * SCROLL_SPEED);
                    Map mapObj = levelObj.MapArrays[index];
                    mapObj.EditorPos.X += (int)movex * SCROLL_SPEED;
                    mapObj.EditorPos.Y += (int)movey * SCROLL_SPEED;
                    renderPanel.Invalidate();
                }
                else if (worldObj != null)
                {
                    NativeFunc.editor_scroll(movex * SCROLL_SPEED, movey * SCROLL_SPEED);

                    worldObj.EditorPos.X += (int)movex * SCROLL_SPEED;
                    worldObj.EditorPos.Y += (int)movey * SCROLL_SPEED;
                    renderPanel.Invalidate();
                }
            }


        }

        private void Workbench_KeyUp(object sender, KeyEventArgs e)
        {

            if (e.KeyValue == (int)Keys.Enter && renderPanel.ToolSelected == Tools.Waypoint && current_mode == GlobalMode.Editor)
            {
                renderPanel.ToolSelected = Tools.Pencil;
            }
        }

        public int Getcopypasteid(ref string baseid, out string new_id)
        {
            int num_copy = 0;
            int adder = 1;
            int i = 0;

            //check is the last value is a number
            for (i = baseid.Length - 1; i >= 0; i--)
            {
                char val = baseid[i];

                if (char.IsNumber(val))
                {
                    num_copy += Convert.ToInt32(val.ToString()) * adder;
                    adder = (adder << 1) + (adder << 3);//bitshit to move from 1 to 10 to 100 etc
                }
                else
                    break;

            }

            num_copy++;
            baseid = baseid.Substring(0, i + 1);
            new_id = baseid + num_copy.ToString();
            return num_copy;
        }

        private void Workbench_KeyDown(object sender, KeyEventArgs e)
        {
            //copy / cut data
            if ((e.KeyCode == Keys.C || e.KeyCode == Keys.X) && e.Control)
            {
                bool cut = (e.KeyCode == Keys.X);

                if (NativeFunc.editor_has_entity_selected() == NativeFunc.wbool.wtrue)
                {
                    DataObject clipdata = new DataObject();


                    List<IExtensible> data_to_copy = new List<IExtensible>(NativeFunc.editor_get_num_selection());
                    List<NativeFunc.GAME_TYPE> datatype = new List<NativeFunc.GAME_TYPE>(data_to_copy.Capacity);


                    for (int i_copy = 0; i_copy < NativeFunc.editor_get_num_selection(); i_copy++)
                    {
                        StringBuilder entity_name = new StringBuilder(1000);

                        NativeFunc.GAME_TYPE selected_type = NativeFunc.editor_entity_get_selection(entity_name, i_copy, entity_name.Capacity);

                        if (selected_type == NativeFunc.GAME_TYPE.GT_ENTITY)
                        {
                            Entity cEntity = renderPanel.CurrentLayer.EntityArrays.Find(p => p.EntityId == entity_name.ToString());

                            data_to_copy.Add(cEntity);
                        }
                        else if (selected_type == NativeFunc.GAME_TYPE.GT_TRIGGER)
                        {
                            Trigger cTrigger = renderPanel.CurrentLayer.TriggerArrays.Find(p => p.TriggerId == entity_name.ToString());

                            data_to_copy.Add(cTrigger);
                        }
                        else if (selected_type == NativeFunc.GAME_TYPE.GT_TEXT)
                        {
                            TextObject cText = renderPanel.CurrentLayer.TextArrays.Find(p => p.TextId == entity_name.ToString());

                            data_to_copy.Add(cText);

                        }
                        else if (selected_type == NativeFunc.GAME_TYPE.GT_SOUND)
                        {
                            SoundSource cSound = renderPanel.CurrentLayer.SoundArrays.Find(p => p.SoundId == entity_name.ToString());

                            data_to_copy.Add(cSound);

                        }
                        else if (selected_type == NativeFunc.GAME_TYPE.GT_LIGHT)
                        {
                            LightSource cLight = renderPanel.CurrentLayer.LightArrays.Find(p => p.LightId == entity_name.ToString());

                            data_to_copy.Add(cLight);
                        }

                        datatype.Add(selected_type);
                    }

                    if (cut)
                    {
                        //remove old data if needed

                        NativeFunc.editor_entity_unselect();

                        foreach (object ent_obj in data_to_copy)
                        {
                            if (ent_obj is Entity)
                                this.SupprEnt(((Entity)ent_obj).EntityId);
                            else if (ent_obj is Trigger)
                                this.SupprTrig(((Trigger)ent_obj).TriggerId);
                            else if (ent_obj is TextObject)
                                this.SupprTxt(((TextObject)ent_obj).TextId);
                            else if (ent_obj is SoundSource)
                                this.SupprSound(((SoundSource)ent_obj).SoundId);
                            else if (ent_obj is LightSource)
                                this.SupprLight(((LightSource)ent_obj).LightId);

                        }
                    }

                    List<byte[]> databyte = new List<byte[]>();


                    for (int icop = 0; icop < data_to_copy.Count; icop++)
                    {
                        IExtensible copy = data_to_copy[icop];

                        MemoryStream dataStream = new MemoryStream();
                        dataStream.Position = 0;

                        if (datatype[icop] == NativeFunc.GAME_TYPE.GT_ENTITY)
                        {
                            ProtoBuf.Serializer.Serialize<Entity>(dataStream, (Entity)copy);
                        }
                        else if (datatype[icop] == NativeFunc.GAME_TYPE.GT_TRIGGER)
                        {
                            ProtoBuf.Serializer.Serialize<Trigger>(dataStream, (Trigger)copy);
                        }
                        else if (datatype[icop] == NativeFunc.GAME_TYPE.GT_TEXT)
                        {
                            ProtoBuf.Serializer.Serialize<TextObject>(dataStream, (TextObject)copy);
                        }
                        else if (datatype[icop] == NativeFunc.GAME_TYPE.GT_SOUND)
                        {
                            ProtoBuf.Serializer.Serialize<SoundSource>(dataStream, (SoundSource)copy);
                        }
                        else if (datatype[icop] == NativeFunc.GAME_TYPE.GT_LIGHT)
                        {
                            ProtoBuf.Serializer.Serialize<LightSource>(dataStream, (LightSource)copy);
                        }

                        dataStream.Position = 0;

                        byte[] datarArray = dataStream.ToArray();
                        dataStream.Close();

                        databyte.Add(datarArray);
                    }


                    clipdata.SetData("listcopy", databyte);

                    clipdata.SetData("listtype", datatype);

                    Clipboard.SetDataObject(clipdata);


                }

            }


            //paste data
            if (e.KeyCode == Keys.V && e.Control)
            {

                IDataObject data = Clipboard.GetDataObject();

                if (data != null && data.GetDataPresent("listcopy") && data.GetDataPresent("listtype") && renderPanel.CurrentLayer.Type == (int)TypeLayer.Objects)
                {
                    List<byte[]> databyte = data.GetData("listcopy") as List<byte[]>;
                    List<NativeFunc.GAME_TYPE> datatype = data.GetData("listtype") as List<NativeFunc.GAME_TYPE>;
                    List<IExtensible> copied_array = new List<IExtensible>();

                    int iv = 0;
                    foreach (byte[] dt in databyte)
                    {
                        IExtensible msg = null;
                        MemoryStream dataStream = new MemoryStream();
                        dataStream.Write(dt, 0, dt.Length);
                        dataStream.Position = 0;


                        if (datatype[iv] == NativeFunc.GAME_TYPE.GT_ENTITY)
                        {
                            msg = ProtoBuf.Serializer.Deserialize<Entity>(dataStream);
                        }
                        else if (datatype[iv] == NativeFunc.GAME_TYPE.GT_TRIGGER)
                        {
                            msg = ProtoBuf.Serializer.Deserialize<Trigger>(dataStream);
                        }
                        else if (datatype[iv] == NativeFunc.GAME_TYPE.GT_TEXT)
                        {
                            msg = ProtoBuf.Serializer.Deserialize<TextObject>(dataStream);
                        }
                        else if (datatype[iv] == NativeFunc.GAME_TYPE.GT_SOUND)
                        {
                            msg = ProtoBuf.Serializer.Deserialize<SoundSource>(dataStream);
                        }
                        else if (datatype[iv] == NativeFunc.GAME_TYPE.GT_LIGHT)
                        {
                            msg = ProtoBuf.Serializer.Deserialize<LightSource>(dataStream);
                        }

                        dataStream.Close();

                        copied_array.Add(msg);
                    }

                    foreach (IExtensible copied in copied_array)
                    {

                        if (copied is Entity)
                        {
                            Entity copied_entity = copied as Entity;


                            Entity new_entity = ProtoBuf.Serializer.DeepClone<Entity>(copied_entity);
                            new_entity.ZOrder = renderPanel.CurrentLayer.Order;
                            string base_id = new_entity.EntityId;
                            string new_id;

                            //make a new Entity with a different name only if it doesn't already exist
                            if (EditorUtilities.ObjectIdExist(levelObj, base_id))
                            {
                                int num_copy = Getcopypasteid(ref base_id, out new_id);
                                new_entity.EntityId = new_id;

                                while (EditorUtilities.ObjectIdExist(levelObj, new_entity.EntityId))
                                    new_entity.EntityId = base_id + (++num_copy).ToString();
                            }


                            entDiag = new EntityDialog(levelObj, game_resources, renderPanel.CurrentLayer, script_modules, script_base_path);
                            entDiag.Tag = new_entity;

                            MouseEventArgs Mousee = new MouseEventArgs(System.Windows.Forms.MouseButtons.Left, 1, Cursor.Position.X, Cursor.Position.Y, 0);
                            this.entity_addentity(this, Mousee);
                        }
                        else if (copied is Trigger)
                        {
                            Trigger copied_trigger = copied as Trigger;

                            //make a new Entity with a different name
                            Trigger new_trigger = ProtoBuf.Serializer.DeepClone<Trigger>(copied_trigger);
                            string base_id = new_trigger.TriggerId;
                            string new_id;

                            if (EditorUtilities.ObjectIdExist(levelObj, base_id))
                            {
                                int num_copy = Getcopypasteid(ref base_id, out new_id);
                                new_trigger.TriggerId = new_id;

                                while (EditorUtilities.ObjectIdExist(levelObj, new_trigger.TriggerId))
                                    new_trigger.TriggerId = base_id + (++num_copy).ToString();
                            }

                            trigDiag = new TriggerDialog(levelObj);
                            trigDiag.Tag = new_trigger;

                            MouseEventArgs Mousee = new MouseEventArgs(System.Windows.Forms.MouseButtons.Left, 1, Cursor.Position.X, Cursor.Position.Y, 0);
                            this.trigger_startlaying(this, Mousee);
                            this.trigger_endlay(this, Mousee);
                        }
                        else if (copied is TextObject)
                        {
                            TextObject copied_text = copied as TextObject;

                            //make a new text object with a different name
                            TextObject new_text = ProtoBuf.Serializer.DeepClone<TextObject>(copied_text);
                            string base_id = new_text.TextId;
                            string new_id;

                            if (EditorUtilities.ObjectIdExist(levelObj, base_id))
                            {
                                int num_copy = Getcopypasteid(ref base_id, out new_id);
                                new_text.TextId = new_id;
                                new_text.ZOrder = renderPanel.CurrentLayer.Order;

                                while (EditorUtilities.ObjectIdExist(levelObj, new_text.TextId))
                                    new_text.TextId = base_id + (++num_copy).ToString();
                            }


                            txtDiag = new TextDialog(levelObj, game_resources, script_modules, script_base_path);
                            txtDiag.Tag = new_text;

                            MouseEventArgs Mousee = new MouseEventArgs(System.Windows.Forms.MouseButtons.Left, 1, Cursor.Position.X, Cursor.Position.Y, 0);
                            txtentity_addentity(this, Mousee);
                        }
                        else if (copied is SoundSource)
                        {
                            SoundSource copied_snd = copied as SoundSource;

                            SoundSource new_sound = ProtoBuf.Serializer.DeepClone<SoundSource>(copied_snd);
                            string base_id = new_sound.SoundId;
                            string new_id;

                            if (EditorUtilities.ObjectIdExist(levelObj, base_id))
                            {
                                int num_copy = Getcopypasteid(ref base_id, out new_id);

                                new_sound.SoundId = new_id;

                                while (EditorUtilities.ObjectIdExist(levelObj, new_sound.SoundId))
                                    new_sound.SoundId = base_id + (++num_copy).ToString();
                            }

                            sndDiag = new SoundSourceDialog(levelObj, game_resources);
                            sndDiag.Tag = new_sound;

                            MouseEventArgs Mousee = new MouseEventArgs(System.Windows.Forms.MouseButtons.Left, 1, Cursor.Position.X, Cursor.Position.Y, 0);
                            sndsource_add(this, Mousee);
                        }
                        else if (copied is LightSource)
                        {
                            LightSource copied_light = copied as LightSource;

                            LightSource new_light = ProtoBuf.Serializer.DeepClone<LightSource>(copied_light);
                            string base_id = new_light.LightId;
                            string new_id;

                            if (EditorUtilities.ObjectIdExist(levelObj, base_id))
                            {
                                int num_copy = Getcopypasteid(ref base_id, out new_id);

                                new_light.LightId = new_id;

                                while (EditorUtilities.ObjectIdExist(levelObj, new_light.LightId))
                                    new_light.LightId = base_id + (++num_copy).ToString();
                            }

                            lightDiag = new LightSourceDialog(levelObj);
                            lightDiag.Tag = new_light;


                            MouseEventArgs Mousee = new MouseEventArgs(System.Windows.Forms.MouseButtons.Left, 1, Cursor.Position.X, Cursor.Position.Y, 0);
                            lightsource_add(this, Mousee);

                        }
                    }
                }
            }

            //save current level
            if (e.KeyCode == Keys.S && e.Control && !e.Alt)
            {
                if (!string.IsNullOrEmpty(openLevelDialog.FileName))
                {
                    this.EnregistrerFichierNiveau(openLevelDialog.FileName, true);
                }
                else if (!string.IsNullOrEmpty(openWorldDialog.FileName))
                {
                    this.EnregistrerFichierMonde(openWorldDialog.FileName, true);
                }
                else if (!string.IsNullOrEmpty(openFragmentDialog.FileName))
                {
                    this.EnregistrerFichierFrag(openFragmentDialog.FileName, true);
                }
                else
                {
                    if (saveLevelDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        this.EnregistrerFichierNiveau(saveLevelDialog.FileName, false);
                        openLevelDialog.FileName = saveLevelDialog.FileName;
                    }
                }
            }

            //undo last action
            if (e.KeyCode == Keys.Z && e.Control)
                history.UndoAction();

            //redo last action in stack
            if (e.KeyCode == Keys.Y && e.Control)
                history.RedoAction();

            if (e.KeyCode == Keys.F5)
                btnChangeMode_Click(this, e);
        }


        private void btnChangeMode_Click(object sender, EventArgs e)
        {

            NativeFunc.editor_change_mode(current_mode == GlobalMode.Play ? NativeFunc.wbool.wfalse : NativeFunc.wbool.wtrue);

            NativeFunc.editor_update_collisions();

            if (current_mode == GlobalMode.Editor)
            {
                renderPanel.ToolSelected = Tools.Pencil;

                current_mode = GlobalMode.Play;

                btnChangeMode.Text = "Mode éditeur";

                NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wfalse);

                //on récupère les données de tiles pour les couche destructibles afin de les réinjecter lors du retour en mode edition
                Map mapObj = levelObj.MapArrays[tabMaps.SelectedIndex];

                foreach (Layer oLayer in mapObj.LayerArrays)
                {
                    if (oLayer.Destructible)
                    {
                        oLayer.TileArrays = renderPanel.GetTilemapArray(oLayer, mapObj);
                    }
                }

                if (NativeFunc.editor_has_player() == NativeFunc.wbool.wtrue)
                    NativeFunc.editor_focus_onplayer();
                else
                    NativeFunc.editor_set_scroll(mapObj.EditorPlayerPos.X, mapObj.EditorPlayerPos.Y);
            }
            else
            {
                current_mode = GlobalMode.Editor;
                btnChangeMode.Text = "Mode Jeu";

                //récup la position de l'avatar principal
                int[] array_position = new int[2];

                Map mapObj = levelObj.MapArrays[tabMaps.SelectedIndex];

                if (NativeFunc.editor_has_player() == NativeFunc.wbool.wtrue)
                {
                    NativeFunc.editor_get_playerposition(array_position);

                    mapObj.EditorPlayerPos.X = array_position[0];
                    mapObj.EditorPlayerPos.Y = array_position[1];
                }

                //on réinitialise les données de tiles pour les couche destructible
                foreach (Layer oLayer in mapObj.LayerArrays)
                {
                    if (oLayer.Destructible)
                    {
                        renderPanel.UpdateTilemap(oLayer.TileArrays, oLayer);
                        NativeFunc.editor_regen_textcoord(oLayer.LayerId);
                    }
                }

                //on appel la fonction lua de réinitialisation des objets du niveau
                if (!string.IsNullOrEmpty(levelObj.ScriptFile))
                {
                    StringBuilder buffer = new StringBuilder(10000);

                    string modname = levelObj.ScriptFile.Substring(0, levelObj.ScriptFile.LastIndexOf('.'));

                    NativeFunc.editor_exec_string(modname + ".level_reset_state()", buffer, buffer.Capacity);

                    if (buffer.Length != 0)
                    {
                        LogTextToBox(buffer.ToString());

                    }
                }


                NativeFunc.editor_set_scroll(mapObj.EditorPos.X, mapObj.EditorPos.Y);

                if (renderPanel.CurrentLayer.Type == (int)TypeLayer.Normal || renderPanel.CurrentLayer.Type == (int)TypeLayer.Collisions)
                    NativeFunc.editor_showcursor(NativeFunc.wbool.wtrue, NativeFunc.wbool.wfalse);
                else
                    NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wtrue);
            }

            OglPanel.current_mode = current_mode;

        }



        void Application_Idle(object sender, EventArgs e)
        {
            if (current_mode != GlobalMode.Play)
                return;


            // update render here
            renderPanel.Invalidate();//render a frame

            //get lua log
            StringBuilder buffer = new StringBuilder(10000);
            NativeFunc.editor_get_lua_log(buffer, buffer.Capacity);

            //check if we have to load from code (used to test level switchs)
            if (NativeFunc.editor_hadlevelpending() == NativeFunc.wbool.wtrue)
            {
                if (levelObj != null)
                    this.FermerNiveau(openLevelDialog.FileName);

                if (NativeFunc.editor_levelpendingisbinary() == NativeFunc.wbool.wtrue)
                {
                    int size_level = NativeFunc.editor_getlevelpending_size();

                    byte[] level_data_array = new byte[size_level];


                    NativeFunc.editor_getlevelpendingbinary(level_data_array);

                    MemoryStream levelStream = new MemoryStream();
                    levelStream.Write(level_data_array, 0, size_level);
                    levelStream.Position = 0;

                    Level olevel = ProtoBuf.Serializer.Deserialize<Level>(levelStream);

                    levelStream.Close();

                    ChargerNiveau_base(olevel);
                }
                else
                {
                    StringBuilder level_name = new StringBuilder(512);
                    NativeFunc.editor_getlevelpending(level_name);

                    this.ChargerNiveau_base(level_name.ToString());
                }

                NativeFunc.editor_clearlevelpending();
            }


            if (buffer.Length != 0)
            {
                LogTextToBox(buffer.ToString());

            }

        }

        void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            HandleException(e.Exception);
        }

        [SecurityPermission(SecurityAction.Demand, ControlAppDomain = true)]
        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception ex;

            if (e.ExceptionObject is Exception)
                ex = e.ExceptionObject as Exception;
            else
                ex = new Exception("Non Csharp exception");

            HandleException(ex);
        }

        void HandleException(Exception ex)
        {
            FileInfo crashinfo = new FileInfo(Application.ExecutablePath);

            string crashlog = crashinfo.DirectoryName + "\\crashlog.log";

            StreamWriter crashstream = null;

            if (!File.Exists(crashlog))
                crashstream = File.CreateText(crashlog);
            else
                crashstream = File.AppendText(crashlog);

            crashstream.Write(ex.ToString());

            crashstream.Close();


            if (levelObj != null)
            {
                string level_path = openLevelDialog.FileName;

                if (string.IsNullOrEmpty(level_path))
                    level_path = Application.ExecutablePath;

                //création fichier backup
                FileInfo info = new FileInfo(level_path);
                string file_backup = info.DirectoryName + "\\crashback0.lwf";

                int number = 0;

                while (File.Exists(file_backup))
                    file_backup = info.DirectoryName + "\\crashback" + (++number).ToString() + ".lwf";

                this.EnregistrerFichierNiveau(file_backup, true);

                MessageBox.Show("Exception non gérée dans l'éditeur ! Un fichier de sauvegarde du niveau en cours à été enregistré dans" + file_backup, "Erreur éditeur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Exception non gérée dans l'éditeur !", "Erreur éditeur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }


        private void Quitter(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Workbench_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.AddHisto(openLevelDialog.FileName);


            if (commands_ref.Count > 0)
            {
                if (MessageBox.Show("Copier le contenu de la console lua dans le presse papier ?", "Copiez code lua", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    StringBuilder lua_comm = new StringBuilder();
                    foreach (KeyValuePair<string, string> lua_commands in commands_ref)
                    {
                        lua_comm.AppendLine(lua_commands.Value);
                    }

                    Clipboard.SetText(lua_comm.ToString());
                }
            }

            commands_ref.Clear();

#if !DEBUG
            if (MessageBox.Show("Fermer l'éditeur de niveau ?", "Fermeture editeur", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                e.Cancel = true;
#endif
        }

        private void preferencesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PrefWindow prefwin = new PrefWindow();
            prefwin.OnChangeTilesetProperties += new PrefWindow.change_tileset_prop(prefwin_OnChangeTilesetProperties);
            prefwin.OnChangeRenderSize += new PrefWindow.change_render_size(prefwin_OnChangeRenderSize);

            if (prefwin.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (!string.IsNullOrEmpty(Settings.Default.localedb_path))
                {
                    localedbWatcher.EnableRaisingEvents = true;
                    localedbWatcher.Path = Settings.Default.localedb_path.Substring(0, Settings.Default.localedb_path.LastIndexOf(Path.DirectorySeparatorChar));
                }
                else
                {
                    localedbWatcher.EnableRaisingEvents = false;
                    localedbWatcher.Path = string.Empty;
                }

                if (levelObj != null)
                    levelObj.TileSize = Settings.Default.TILE_SIZE;
            }
        }

        private void prefwin_OnChangeTilesetProperties()
        {
            //update viewable tilesets
            GaleReader gaReader = new GaleReader();

            Dictionary<string, Bitmap> newViewDic = new Dictionary<string, Bitmap>();

            foreach (KeyValuePair<string, Bitmap> bmpkvp in viewable_tileSets)
            {
                Bitmap bmpView = null;

                try
                {
                    gaReader.OpenFile(bmpkvp.Key);

                    bmpView = gaReader.GetPackedContent(TILE_PER_ROW, gaReader.GetWidth(), gaReader.GetHeight(), Settings.Default.tileset_offset, Settings.Default.tileset_back_color);

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                finally
                {
                    if (gaReader.FileOpened)
                        gaReader.CloseFile();
                }

                if (bmpView != null)
                    bmpkvp.Value.Dispose();
                else
                    bmpView = bmpkvp.Value;

                newViewDic.Add(bmpkvp.Key, bmpView);
            }

            viewable_tileSets.Clear();

            viewable_tileSets = newViewDic;

            if (!string.IsNullOrEmpty(renderPanel.CurrentLayer.TilesetId))
            {
                tilesetBox.Image = viewable_tileSets[renderPanel.CurrentLayer.TilesetId];
                current_tile_offset = 0;
            }
            else
            {
                tilesetBox.Image = viewable_tileSets[renderPanel.CurrentMap.TilesetId];
                current_tile_offset = Settings.Default.tileset_offset;
            }

            tilesetBox.Invalidate();
        }

        private void prefwin_OnChangeRenderSize()
        {
            if (!Settings.Default.res_dynamique)
            {
                renderPanel.Width = Settings.Default.render_width;
                renderPanel.Height = Settings.Default.render_height;

                if (OglPanel._init)
                    NativeFunc.editor_resize_screen(this.Width, this.Height);

                renderPanel.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left);
            }
            else
            {
                renderPanel.Width = tabLayout.Location.X - renderPanel.Location.X;
                renderPanel.Height = panelactions.Location.Y - renderPanel.Location.Y;
                //renderPanel.Height = Settings.Default.render_height;
                this.renderPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            }
        }


        private void zoomEvent()
        {
            renderPanel.ZoomFactor = ((KeyValuePair<string, float>)cmbZoom.SelectedItem).Value;
            NativeFunc.editor_setzoom(renderPanel.Width, renderPanel.Height, renderPanel.ZoomFactor);
            renderPanel.Invalidate();
            renderPanel.CurrentMap.ZoomValue = renderPanel.ZoomFactor;

            toolStripButtonZoomplus.Enabled = !(cmbZoom.SelectedIndex == cmbZoom.Items.Count - 1);
            toolStripButtonZoomminus.Enabled = !(cmbZoom.SelectedIndex == 0);
            toolStripButtonZoomreset.Enabled = !(renderPanel.ZoomFactor == 1.0f);
        }

        private void cmbZoom_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!OglPanel._init)
                return;

            this.zoomEvent();
        }


        private void focusJoueurToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Map mapObj = levelObj.MapArrays[tabMaps.SelectedIndex];
            int[] array_position = new int[2];
            NativeFunc.editor_get_playerposition(array_position);

            mapObj.EditorPlayerPos.X = array_position[0];
            mapObj.EditorPlayerPos.Y = array_position[1];

            NativeFunc.editor_set_scroll(mapObj.EditorPlayerPos.X, mapObj.EditorPlayerPos.Y);
            mapObj.EditorPos.X = mapObj.EditorPlayerPos.X;
            mapObj.EditorPos.Y = mapObj.EditorPlayerPos.Y;
        }


        private void placerJoueurToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Map mapObj = levelObj.MapArrays[tabMaps.SelectedIndex];

            if (NativeFunc.editor_has_player() == NativeFunc.wbool.wtrue)
                NativeFunc.editor_setplayer_pos((float)mapObj.EditorPos.X, (float)mapObj.EditorPos.Y);

            mapObj.EditorPlayerPos.X = mapObj.EditorPos.X;
            mapObj.EditorPlayerPos.Y = mapObj.EditorPos.Y;
        }


        private void consoleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (lua_comm.IsDisposed)
                lua_comm = new LuaCommands(commands_ref);

            lua_comm.Show();
        }

        private void réinitialiserLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richlog.Clear();
        }

        private void rechargerDLLJeuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (current_mode == GlobalMode.Play)
            {
                MessageBox.Show("Impossible de recharger la dll en mode jeu, repasser en mode éditeur en premier!", "Action immpossible", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return;
            }

            NativeFunc.editor_release_gamedll();

            if (MessageBox.Show("Recompile la dll de jeu, puis clique sur OK", "Recompile la dll", MessageBoxButtons.OK, MessageBoxIcon.Exclamation) == System.Windows.Forms.DialogResult.OK)
            {
                if (NativeFunc.editor_load_gamedll(Settings.Default.gamedll_path) == NativeFunc.wbool.wtrue)
                {
                    rechargerScriptScèneToolStripMenuItem_Click(sender, e);
                }
                else
                {
                    MessageBox.Show("La dll de jeu à été compilée avec une précédente version du moteur de jeu, impossible de la charger", "Différence de version", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }


        private void fermerNiveauToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FermerNiveau(openLevelDialog.FileName);
        }


        private void AddHisto(string nomfichier, bool addtoolstriponly = false)
        {
            if (string.IsNullOrEmpty(nomfichier))
                return;

            if (!Settings.Default.historique.Contains(nomfichier) || addtoolstriponly)
            {
                if (!addtoolstriponly)
                    Settings.Default.historique.Add(nomfichier);

                ToolStripMenuItem histotool = new ToolStripMenuItem(nomfichier);
                histotool.Name = nomfichier;
                histotool.Click += new EventHandler(histotool_Click);

                if (niveauxRécentsToolStripMenuItem.DropDownItems.Count == 0)
                    niveauxRécentsToolStripMenuItem.Enabled = true;

                niveauxRécentsToolStripMenuItem.DropDownItems.Insert(0, histotool);

                Settings.Default.Save();
            }
        }
        private void histotool_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem clickedtool = sender as ToolStripMenuItem;

            //si un niveau est déjà ouvert, appel du code de fermeture
            if (levelObj != null)
                FermerNiveau(openLevelDialog.FileName);

            openLevelDialog.FileName = clickedtool.Name;

            this.ChargerNiveau_base(openLevelDialog.FileName);
        }

        private void RemoveHisto(string nomfichier)
        {
            if (string.IsNullOrEmpty(nomfichier))
                return;

            if (Settings.Default.historique.Contains(nomfichier))
            {
                Settings.Default.historique.Remove(nomfichier);

                if (niveauxRécentsToolStripMenuItem.DropDownItems.Count == 1)
                    niveauxRécentsToolStripMenuItem.Enabled = false;

                niveauxRécentsToolStripMenuItem.DropDownItems.RemoveAt(niveauxRécentsToolStripMenuItem.DropDownItems.IndexOfKey(nomfichier));

                Settings.Default.Save();
            }
        }


        private List<ResxInfo> LoadGameResourcesList()
        {
            List<ResxInfo> game_resx_list = new List<ResxInfo>();

            string[] file_array = Directory.GetFiles(Settings.Default.assets_path);

            foreach (string file in file_array)
            {
                FileInfo finfo = new FileInfo(file);

                ResxType type = ResxType.texture;

                switch (finfo.Extension)
                {
                    case ".png":
                        type = ResxType.texture;
                        break;
                    case ".awf":
                        type = ResxType.animation;
                        break;
                    case ".lwf":
                        type = ResxType.level;
                        break;
                    case ".ogg":
                    case ".wav":
                        type = ResxType.sound;
                        break;
                    case ".json":
                        type = ResxType.font;
                        break;
                    case ".shad":
                        type = ResxType.shader;
                        break;
                }

                if (finfo.Extension == ".pack")
                {
                    using (var file_stream = File.OpenRead(file))
                    {
                        //add virtual textures
                        PackInfo info = ProtoBuf.Serializer.Deserialize<PackInfo>(file_stream);

                        foreach (PackTextureInfo texture_info in info.TexturePackLists)
                        {
                            game_resx_list.Add(new ResxInfo(ResxType.texture, texture_info.TextureId, Path.Combine(Settings.Default.assets_path, texture_info.TextureId)));
                        }
                    }

                }
                else
                {
                    game_resx_list.Add(new ResxInfo(type, finfo.Name, finfo.FullName));
                }
            }

            game_resx_list = (from u in game_resx_list orderby u.ident ascending select u).ToList<ResxInfo>();

            return game_resx_list;
        }

        Dictionary<string, Point> old_positions;
        Point start_multimov_pos;


        private void déplacerÉlémentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            renderPanel.MouseDown += this.multientity_setrepos;
            renderPanel.ToolSelected = Tools.Entity;
            NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wfalse);
        }

        private void multientity_setrepos(object sender, MouseEventArgs e)
        {
            old_positions = new Dictionary<string, Point>();

            int worldposx = 0;
            int worldposy = 0;
            renderPanel.GetWorldPos(e.X, e.Y, out worldposx, out worldposy, Settings.Default.snap_to_grid);
            start_multimov_pos.X = worldposx;
            start_multimov_pos.Y = worldposy;

            StringBuilder entity_id_buff = new StringBuilder(250);

            for (int i_sel = 0; i_sel < NativeFunc.editor_get_num_selection(); i_sel++)
            {
                NativeFunc.GAME_TYPE type = NativeFunc.editor_entity_get_selection(entity_id_buff, i_sel, entity_id_buff.Capacity);
                string entity_id = null;
                Wvector entity_pos = null;

                switch (type)
                {
                    case NativeFunc.GAME_TYPE.GT_ENTITY:
                        Entity cEntity = renderPanel.CurrentLayer.EntityArrays.Find(p => p.EntityId == entity_id_buff.ToString());
                        entity_id = cEntity.EntityId;
                        entity_pos = cEntity.Position;
                        break;
                    case NativeFunc.GAME_TYPE.GT_TEXT:
                        TextObject cText = renderPanel.CurrentLayer.TextArrays.Find(p => p.TextId == entity_id_buff.ToString());
                        entity_id = cText.TextId;
                        entity_pos = cText.Position;
                        break;
                    case NativeFunc.GAME_TYPE.GT_TRIGGER:
                        Trigger cTrigger = renderPanel.CurrentLayer.TriggerArrays.Find(p => p.TriggerId == entity_id_buff.ToString());
                        entity_id = cTrigger.TriggerId;
                        entity_pos = cTrigger.Position;
                        break;
                    case NativeFunc.GAME_TYPE.GT_SOUND:
                        SoundSource cSound = renderPanel.CurrentLayer.SoundArrays.Find(p => p.SoundId == entity_id_buff.ToString());
                        entity_id = cSound.SoundId;
                        entity_pos = cSound.Position;
                        break;
                    case NativeFunc.GAME_TYPE.GT_LIGHT:
                        LightSource cLight = renderPanel.CurrentLayer.LightArrays.Find(p => p.LightId == entity_id_buff.ToString());
                        entity_id = cLight.LightId;
                        entity_pos = cLight.Position;
                        break;
                    case NativeFunc.GAME_TYPE.GT_PARTICLES:
                        ParticlesData cpart = renderPanel.CurrentLayer.ParticlesArrays.Find(p => p.Id == entity_id_buff.ToString());
                        entity_id = cpart.Id;
                        entity_pos = cpart.Position;
                        break;
                    case NativeFunc.GAME_TYPE.GT_WAYPOINT:
                        Waypoint cway = renderPanel.CurrentLayer.Waypoints.Find(p => p.Id == entity_id_buff.ToString());
                        entity_id = cway.Id;
                        entity_pos = cway.Position;
                        break;
                    case NativeFunc.GAME_TYPE.GT_COLLIDER:
                        Collider ccol = renderPanel.CurrentLayer.Colliders.Find(p => p.Id == entity_id_buff.ToString());
                        entity_id = ccol.Id;
                        entity_pos = ccol.Position;
                        break;
                }

                old_positions.Add(type.ToString() + entity_id_buff.ToString(), new Point(entity_pos.X, entity_pos.Y));
            }

            multientity_setpos(sender, e);
            renderPanel.MouseDown -= this.multientity_setrepos;
            renderPanel.MouseMove += this.multientity_setpos;
            renderPanel.MouseUp += this.multientity_endrepos;
        }

        private void multientity_setpos(object sender, MouseEventArgs e)
        {
            int worldposx, worldposy;
            renderPanel.GetWorldPos(e.X, e.Y, out worldposx, out worldposy, Settings.Default.snap_to_grid);

            StringBuilder entity_id_buff = new StringBuilder(250);

            for (int i_sel = 0; i_sel < NativeFunc.editor_get_num_selection(); i_sel++)
            {
                NativeFunc.GAME_TYPE type = NativeFunc.editor_entity_get_selection(entity_id_buff, i_sel, entity_id_buff.Capacity);

                string entity_id = null;
                Wvector entity_pos = null;

                switch (type)
                {
                    case NativeFunc.GAME_TYPE.GT_ENTITY:
                        Entity cEntity = renderPanel.CurrentLayer.EntityArrays.Find(p => p.EntityId == entity_id_buff.ToString());
                        entity_id = cEntity.EntityId;
                        entity_pos = cEntity.Position;
                        break;
                    case NativeFunc.GAME_TYPE.GT_TEXT:
                        TextObject cText = renderPanel.CurrentLayer.TextArrays.Find(p => p.TextId == entity_id_buff.ToString());
                        entity_id = cText.TextId;
                        entity_pos = cText.Position;
                        break;
                    case NativeFunc.GAME_TYPE.GT_TRIGGER:
                        Trigger cTrigger = renderPanel.CurrentLayer.TriggerArrays.Find(p => p.TriggerId == entity_id_buff.ToString());
                        entity_id = cTrigger.TriggerId;
                        entity_pos = cTrigger.Position;
                        break;
                    case NativeFunc.GAME_TYPE.GT_SOUND:
                        SoundSource cSound = renderPanel.CurrentLayer.SoundArrays.Find(p => p.SoundId == entity_id_buff.ToString());
                        entity_id = cSound.SoundId;
                        entity_pos = cSound.Position;
                        break;
                    case NativeFunc.GAME_TYPE.GT_LIGHT:
                        LightSource cLight = renderPanel.CurrentLayer.LightArrays.Find(p => p.LightId == entity_id_buff.ToString());
                        entity_id = cLight.LightId;
                        entity_pos = cLight.Position;
                        break;
                    case NativeFunc.GAME_TYPE.GT_PARTICLES:
                        ParticlesData cpart = renderPanel.CurrentLayer.ParticlesArrays.Find(p => p.Id == entity_id_buff.ToString());
                        entity_id = cpart.Id;
                        entity_pos = cpart.Position;
                        break;
                    case NativeFunc.GAME_TYPE.GT_WAYPOINT:
                        Waypoint cway = renderPanel.CurrentLayer.Waypoints.Find(p => p.Id == entity_id_buff.ToString());
                        entity_id = cway.Id;
                        entity_pos = cway.Position;
                        break;
                    case NativeFunc.GAME_TYPE.GT_COLLIDER:
                        Collider ccol = renderPanel.CurrentLayer.Colliders.Find(p => p.Id == entity_id_buff.ToString());
                        entity_id = ccol.Id;
                        entity_pos = ccol.Position;
                        break;
                }

                int diffx = worldposx - start_multimov_pos.X;
                int diffy = worldposy - start_multimov_pos.Y;

                entity_pos.X += diffx;
                entity_pos.Y += diffy;

                NativeFunc.editor_entity_update_pos(entity_id, entity_pos.X, entity_pos.Y, type);
            }

            start_multimov_pos.X = worldposx;
            start_multimov_pos.Y = worldposy;
        }

        private void multientity_endrepos(object sender, MouseEventArgs e)
        {
            renderPanel.MouseMove -= this.multientity_setpos;
            renderPanel.MouseUp -= this.multientity_endrepos;

            StringBuilder entity_id_buff = new StringBuilder(250);

            NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wtrue);

            for (int i_sel = 0; i_sel < NativeFunc.editor_get_num_selection(); i_sel++)
            {
                NativeFunc.editor_entity_get_selection(entity_id_buff, i_sel, entity_id_buff.Capacity);

                NativeFunc.GAME_TYPE type = NativeFunc.editor_entity_get_selection(entity_id_buff, i_sel, entity_id_buff.Capacity);

                string entity_id = null;
                Wvector entity_pos = null;
                HistoryEntry entry = null;

                Point oldpos_mul = old_positions[type.ToString() + entity_id_buff.ToString()];

                switch (type)
                {
                    case NativeFunc.GAME_TYPE.GT_ENTITY:
                        Entity cEntity = renderPanel.CurrentLayer.EntityArrays.Find(p => p.EntityId == entity_id_buff.ToString());
                        entity_id = cEntity.EntityId;
                        entity_pos = cEntity.Position;
                        Point newpos = new Point(entity_pos.X, entity_pos.Y);
                        object[] hist_data = new object[] { newpos, entity_id_buff.ToString() };

                        entity_setpos_delegate func = this.entity_setpos2;
                        entry = new HistoryEntry(hist_data, oldpos_mul, HistoryAction.SetPosition, func);
                        break;
                    case NativeFunc.GAME_TYPE.GT_TEXT:
                        TextObject cText = renderPanel.CurrentLayer.TextArrays.Find(p => p.TextId == entity_id_buff.ToString());
                        entity_id = cText.TextId;
                        entity_pos = cText.Position;
                        Point newpos2 = new Point(entity_pos.X, entity_pos.Y);
                        object[] hist_data2 = new object[] { newpos2, entity_id_buff.ToString() };

                        entity_setpos_delegate func2 = this.textentity_setpos2;
                        entry = new HistoryEntry(hist_data2, oldpos_mul, HistoryAction.SetPosition, func2);
                        break;
                    case NativeFunc.GAME_TYPE.GT_TRIGGER:
                        Trigger cTrigger = renderPanel.CurrentLayer.TriggerArrays.Find(p => p.TriggerId == entity_id_buff.ToString());
                        entity_id = cTrigger.TriggerId;
                        entity_pos = cTrigger.Position;
                        Point newpos3 = new Point(entity_pos.X, entity_pos.Y);
                        object[] hist_data3 = new object[] { newpos3, entity_id_buff.ToString() };

                        entity_setpos_delegate func3 = this.trigger_setpos2;
                        entry = new HistoryEntry(hist_data3, oldpos_mul, HistoryAction.SetPosition, func3);
                        break;
                    case NativeFunc.GAME_TYPE.GT_SOUND:
                        SoundSource cSound = renderPanel.CurrentLayer.SoundArrays.Find(p => p.SoundId == entity_id_buff.ToString());
                        entity_id = cSound.SoundId;
                        entity_pos = cSound.Position;
                        Point newpos4 = new Point(entity_pos.X, entity_pos.Y);
                        object[] hist_data4 = new object[] { newpos4, entity_id, entity_id_buff.ToString() };

                        entity_setpos_delegate func4 = this.sndsource_setpos2;
                        entry = new HistoryEntry(hist_data4, oldpos_mul, HistoryAction.SetPosition, func4);
                        break;
                    case NativeFunc.GAME_TYPE.GT_LIGHT:
                        LightSource cLight = renderPanel.CurrentLayer.LightArrays.Find(p => p.LightId == entity_id_buff.ToString());
                        entity_id = cLight.LightId;
                        entity_pos = cLight.Position;
                        Point newpos5 = new Point(entity_pos.X, entity_pos.Y);
                        object[] hist_data5 = new object[] { newpos5, entity_id, entity_id_buff.ToString() };

                        entity_setpos_delegate func5 = this.lightsource_setpos2;
                        entry = new HistoryEntry(hist_data5, oldpos_mul, HistoryAction.SetPosition, func5);
                        break;
                    case NativeFunc.GAME_TYPE.GT_PARTICLES:
                        ParticlesData cPart = renderPanel.CurrentLayer.ParticlesArrays.Find(p => p.Id == entity_id_buff.ToString());
                        entity_id = cPart.Id;
                        entity_pos = cPart.Position;
                        Point newpos6 = new Point(entity_pos.X, entity_pos.Y);
                        object[] hist_data6 = new object[] { newpos6, entity_id, entity_id_buff.ToString() };

                        entity_setpos_delegate func6 = this.particles_setpos2;
                        entry = new HistoryEntry(hist_data6, oldpos_mul, HistoryAction.SetPosition, func6);
                        break;
                    case NativeFunc.GAME_TYPE.GT_WAYPOINT:
                        Waypoint cWay = renderPanel.CurrentLayer.Waypoints.Find(p => p.Id == entity_id_buff.ToString());
                        entity_id = cWay.Id;
                        entity_pos = cWay.Position;
                        Point newpos7 = new Point(entity_pos.X, entity_pos.Y);
                        object[] hist_data7 = new object[] { newpos7, entity_id, entity_id_buff.ToString() };

                        entity_setpos_delegate func7 = this.waypoint_setpos2;
                        entry = new HistoryEntry(hist_data7, oldpos_mul, HistoryAction.SetPosition, func7);
                        break;
                    case NativeFunc.GAME_TYPE.GT_COLLIDER:
                        Collider cCol = renderPanel.CurrentLayer.Colliders.Find(p => p.Id == entity_id_buff.ToString());
                        entity_id = cCol.Id;
                        entity_pos = cCol.Position;
                        Point newpos8 = new Point(entity_pos.X, entity_pos.Y);
                        object[] hist_data8 = new object[] { newpos8, entity_id, entity_id_buff.ToString() };

                        entity_setpos_delegate func8 = this.collider_setpos2;
                        entry = new HistoryEntry(hist_data8, oldpos_mul, HistoryAction.SetPosition, func8);
                        break;
                }

                history.Push(entry);
            }

            this.MajniveauBackup();

            renderPanel.ToolSelected = Tools.Pencil;
        }


        private void supprimerÉlémentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //suppression de tout les éléments sélectionnés
            int num_ele = NativeFunc.editor_get_num_selection();
            StringBuilder build = new StringBuilder(256);

            if (MessageBox.Show("Supprimer " + num_ele + " entité(s) ?", "Suppression entités", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {

                for (int i = 0; i < num_ele; i++)
                {
                    NativeFunc.GAME_TYPE type = NativeFunc.editor_entity_get_selection(build, i, build.Capacity);

                    switch (type)
                    {
                        case NativeFunc.GAME_TYPE.GT_ENTITY:
                            this.SupprEnt(build.ToString());
                            break;
                        case NativeFunc.GAME_TYPE.GT_TEXT:
                            this.SupprTxt(build.ToString());
                            break;
                        case NativeFunc.GAME_TYPE.GT_TRIGGER:
                            this.SupprTrig(build.ToString());
                            break;
                        case NativeFunc.GAME_TYPE.GT_SOUND:
                            this.SupprSound(build.ToString());
                            break;
                        case NativeFunc.GAME_TYPE.GT_LIGHT:
                            this.SupprLight(build.ToString());
                            break;
                        case NativeFunc.GAME_TYPE.GT_PARTICLES:
                            this.SupprParticles(build.ToString());
                            break;
                        case NativeFunc.GAME_TYPE.GT_WAYPOINT:
                            this.SupprWaypoint(build.ToString());
                            break;
                        case NativeFunc.GAME_TYPE.GT_COLLIDER:
                            this.SupprCollider(build.ToString());
                            break;
                    }

                }

                NativeFunc.editor_entity_unselect();

                this.MajniveauBackup();
            }
        }


        private void grouperÉlémentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //regroupement de tout les éléments sélectionnés
            int num_ele = NativeFunc.editor_get_num_selection();
            StringBuilder build = new StringBuilder(256);

            GroupDialog grpdiag = new GroupDialog(renderPanel.CurrentLayer);

            if (grpdiag.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                GroupEntity grp = grpdiag.Tag as GroupEntity;

                for (int i = 0; i < num_ele; i++)
                {
                    NativeFunc.GAME_TYPE type = NativeFunc.editor_entity_get_selection(build, i, build.Capacity);

                    if (type == NativeFunc.GAME_TYPE.GT_ENTITY)
                    {
                        Entity cent = renderPanel.CurrentLayer.EntityArrays.Find(p => p.EntityId == build.ToString());

                        string old_group_id = cent.GroupId;
                        cent.GroupId = grp.GroupId;

                        if (old_group_id != cent.GroupId)
                        {
                            TreeNode cnode = null;

                            if (!string.IsNullOrEmpty(old_group_id))
                            {
                                GroupEntity grp_old = renderPanel.CurrentLayer.GroupArrays.Find(p => p.GroupId == old_group_id);
                                grp_old.IdArrays.Remove(cent.EntityId);

                                if (grp_old.IdArrays.Count == 0)
                                    renderPanel.CurrentLayer.GroupArrays.Remove(grp_old);

                                cnode = ItemsList.Nodes["Nodeentity"].Nodes["group_" + old_group_id].Nodes[cent.EntityId];
                                ItemsList.Nodes["Nodeentity"].Nodes["group_" + old_group_id].Nodes.Remove(cnode);

                                if (ItemsList.Nodes["Nodeentity"].Nodes["group_" + old_group_id].Nodes.Count == 0)
                                    ItemsList.Nodes["Nodeentity"].Nodes.Remove(ItemsList.Nodes["Nodeentity"].Nodes["group_" + old_group_id]);
                            }
                            else
                            {
                                cnode = ItemsList.Nodes["Nodeentity"].Nodes[cent.EntityId];
                            }

                            if (!grp.IdArrays.Contains(cent.EntityId))
                                grp.IdArrays.Add(cent.EntityId);

                            if (!ItemsList.Nodes["Nodeentity"].Nodes.ContainsKey("group_" + cent.GroupId))
                            {
                                TreeNode group_node = new TreeNode();
                                group_node.Text = grp.GroupId;
                                group_node.Tag = grp;
                                group_node.Name = "group_" + grp.GroupId;

                                ItemsList.Nodes["Nodeentity"].Nodes.Add(group_node);
                            }

                            ItemsList.Nodes["Nodeentity"].Nodes["group_" + cent.GroupId].Nodes.Add(cnode);
                        }

                    }

                }

                NativeFunc.editor_entity_unselect();

                this.MajniveauBackup();
            }
        }

        #endregion

        #region Gestion outils édition

        private void LayTileButton_Click(object sender, EventArgs e)
        {
            renderPanel.ToolSelected = Tools.Pencil;
            LayTileButton.Checked = true;
            FillTilesButton.Checked = false;
        }

        private void FillTilesButton_Click(object sender, EventArgs e)
        {
            renderPanel.ToolSelected = Tools.FloodFill;
            LayTileButton.Checked = false;
            FillTilesButton.Checked = true;
        }


        private void UndoButton_Click(object sender, EventArgs e)
        {
            history.UndoAction();
            renderPanel.Invalidate();
        }

        private void RedoButton_Click(object sender, EventArgs e)
        {
            history.RedoAction();
            renderPanel.Invalidate();
        }

        private void history_OnRemove()
        {
            RedoButton.Enabled = rétablirToolStripMenuItem.Enabled = false;
            UndoButton.Enabled = annulerToolStripMenuItem.Enabled = history.CanUndo;
        }

        private void history_OnPush()
        {
            UndoButton.Enabled = annulerToolStripMenuItem.Enabled = true;
            RedoButton.Enabled = rétablirToolStripMenuItem.Enabled = false;
        }

        private void history_OnRedo()
        {
            UndoButton.Enabled = annulerToolStripMenuItem.Enabled = true;
            RedoButton.Enabled = rétablirToolStripMenuItem.Enabled = history.CanRedo;
        }

        private void history_OnUndo()
        {
            RedoButton.Enabled = rétablirToolStripMenuItem.Enabled = true;
            UndoButton.Enabled = annulerToolStripMenuItem.Enabled = history.CanUndo;
        }

        private void toolStripButtonZoomplus_Click(object sender, EventArgs e)
        {
            //valeur de zoom + importante
            int newselect = cmbZoom.SelectedIndex + 1;

            if (newselect >= cmbZoom.Items.Count)
                newselect = cmbZoom.Items.Count - 1;

            cmbZoom.SelectedIndex = newselect;

        }

        private void toolStripButtonZoomminus_Click(object sender, EventArgs e)
        {
            //valeur de zoom - importante
            int newselect = cmbZoom.SelectedIndex - 1;

            if (newselect < 0)
                newselect = 0;

            cmbZoom.SelectedIndex = newselect;
        }

        private void toolStripButtonZoomreset_Click(object sender, EventArgs e)
        {
            //valeur de zoom 100%
            cmbZoom.SelectedIndex = 2;
        }

        private void toolStripButtonNewLevel_Click(object sender, EventArgs e)
        {
            NouveauNiveau(sender, e);
        }

        private void toolStripButtonOpenLevel_Click(object sender, EventArgs e)
        {
            ChargerNiveau(sender, e);
        }

        private void toolStripButtonSaveLevel_Click(object sender, EventArgs e)
        {
            EnregistrerNiveau(sender, e);
        }




        #endregion


        #region Fonction de rendu / sélection du tileset
        private void tilesetBox_Paint(object sender, PaintEventArgs e)
        {
            if (tileSets.Count == 0 || tabMaps.SelectedIndex == -1)
                return;



            Bitmap cBitmap = (Bitmap)tilesetBox.Image;
            //tileSets[tabMaps.SelectedIndex];

            if (tilesetBox.Height < cBitmap.Height)
                tilesetBox.Height = cBitmap.Height;

            e.Graphics.InterpolationMode = InterpolationMode.NearestNeighbor;
            e.Graphics.SmoothingMode = SmoothingMode.None;

            //set the center of the pixel when drawing to 0.5 / 0.5 rather than the default 0 / 0 (default make the drawing start at -0.5)
            e.Graphics.PixelOffsetMode = PixelOffsetMode.Half;

            e.Graphics.DrawImage(
                cBitmap,
                new Rectangle(0, 0, cBitmap.Width, cBitmap.Height),
                // destination rectangle 
                0,
                0,           // upper-left corner of source rectangle
                cBitmap.Width,       // width of source rectangle
                cBitmap.Height,      // height of source rectangle
                GraphicsUnit.Pixel);

            if (renderCursor)
            {
                int tileSelected = renderPanel.TileSelected[0];

                Map currentmap = levelObj.MapArrays[tabMaps.SelectedIndex];

                string tileset_id = currentmap.TilesetId;

                if (!string.IsNullOrEmpty(renderPanel.CurrentLayer.TilesetId) && renderPanel.CurrentLayer.TilesetId != tileset_id)
                    tileset_id = renderPanel.CurrentLayer.TilesetId;

                int textColCount = tileSets[tileset_id].Width / (Settings.Default.TILE_SIZE);


                int y = (int)Math.Floor((double)(tileSelected / textColCount));
                int x = tileSelected - (y * textColCount);

                e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(125, 0, 0, 255)), (x * Settings.Default.TILE_SIZE) + (current_tile_offset * (x + 1)), (y * Settings.Default.TILE_SIZE) + (current_tile_offset * (y + 1)), Settings.Default.TILE_SIZE, Settings.Default.TILE_SIZE);

            }

        }


        private void tilesetBox_MouseDown(object sender, MouseEventArgs e)
        {
            if (tileSets.Count == 0 || tabMaps.SelectedIndex == -1)
                return;

            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                Map currentmap = levelObj.MapArrays[tabMaps.SelectedIndex];

                string tileset_id = currentmap.TilesetId;

                if (!string.IsNullOrEmpty(renderPanel.CurrentLayer.TilesetId) && renderPanel.CurrentLayer.TilesetId != tileset_id)
                    tileset_id = renderPanel.CurrentLayer.TilesetId;

                Bitmap cBitmap = tileSets[tileset_id];

                int x = (int)Math.Floor((double)e.Location.X / (Settings.Default.TILE_SIZE + (current_tile_offset)));
                int y = (int)Math.Floor((double)e.Location.Y / (Settings.Default.TILE_SIZE + (current_tile_offset)));

                int textColCount = cBitmap.Width / Settings.Default.TILE_SIZE;

                renderPanel.TileSelected[0] = x + (y * textColCount);

                tilesetBox.Invalidate();

                if (OnTilesetboxSelectionChanged != null)
                    OnTilesetboxSelectionChanged(renderPanel.TileSelected[0], x, y);
            }
        }

        #endregion


        #region Fonction de gestion du hook sur les fichier tileset et script du niveau

        Dictionary<string, DateTime> update_watcher = new Dictionary<string, DateTime>();

        private void tilsetWatcher_Changed(object sender, FileSystemEventArgs e)
        {
            if (Path.GetExtension(e.FullPath) != ".png" && Path.GetExtension(e.FullPath) != ".gal")
                return;

            FileInfo resxFile = new FileInfo(e.FullPath);


            DateTime last_update = File.GetLastWriteTime(e.FullPath);


            if (levelObj == null || levelObj.MapArrays.Count == 0 || tabMaps.SelectedIndex == -1)
                return;

            Map current_map = levelObj.MapArrays[tabMaps.SelectedIndex];

            List<string> tileset_id = new List<string>();

            foreach (Map mapObj in levelObj.MapArrays)
            {
                if (!string.IsNullOrEmpty(mapObj.TilesetId) && !tileset_id.Contains(mapObj.TilesetId))
                    tileset_id.Add(mapObj.TilesetId);

                foreach (Layer olay in mapObj.LayerArrays)
                    if (!string.IsNullOrEmpty(olay.TilesetId) && !tileset_id.Contains(olay.TilesetId))
                        tileset_id.Add(olay.TilesetId);
            }


            foreach (string t_id in tileset_id)
            {
                if (e.ChangeType == WatcherChangeTypes.Changed && e.FullPath == t_id)
                {
                    if (update_watcher.ContainsKey(resxFile.Name) && last_update == update_watcher[resxFile.Name])
                        continue;

                    FileInfo bitmapFile = new FileInfo(t_id);

                    while (BitmapUtilities.IsFileLocked(bitmapFile))
                        Thread.Yield();

                    Bitmap bmpPacked = null;
                    Bitmap bmpView = null;

                    if (bitmapFile.Extension == ".gal")
                    {
                        //récupération du contenu bitmap du fichier gal
                        GaleReader gaReader = new GaleReader();

                        gaReader.OpenFile(bitmapFile.FullName);

                        bmpPacked = gaReader.GetPackedContent(TILE_PER_ROW, gaReader.GetWidth(), gaReader.GetHeight());
                        bmpView = gaReader.GetPackedContent(TILE_PER_ROW, gaReader.GetWidth(), gaReader.GetHeight(), Settings.Default.tileset_offset, Settings.Default.tileset_back_color);

                        gaReader.CloseFile();
                    }
                    else
                    {
                        Bitmap tmp = new Bitmap(t_id);
                        bmpPacked = new Bitmap(tmp);
                        bmpView = bmpPacked;
                        tmp.Dispose();
                    }

                    //rechargement du tileset
                    tileSets[t_id].Dispose();
                    tileSets[t_id] = bmpPacked;

                    viewable_tileSets[t_id].Dispose();
                    viewable_tileSets[t_id] = bmpView;


                    if ((t_id == current_map.TilesetId && string.IsNullOrEmpty(renderPanel.CurrentLayer.TilesetId)) || renderPanel.CurrentLayer.TilesetId == t_id)
                    {
                        tilesetBox.Image = viewable_tileSets[t_id];
                        renderPanel.TileSelected[0] = 0;
                        tilesetBox.Invalidate();
                    }

                    byte[] bmpcontent = BitmapUtilities.GetBitmapContent(bmpPacked);

                    //rechargement de la texture sur la map
                    NativeFunc.editor_updatetextureresx(t_id, bmpPacked.Width, bmpPacked.Height, bmpcontent, (uint)bmpcontent.Length);

                    renderPanel.UpdateTexture();

                    if (!update_watcher.ContainsKey(resxFile.Name))
                        update_watcher.Add(resxFile.Name, last_update);
                    else
                        update_watcher[resxFile.Name] = last_update;

                    break;
                }
            }
        }



        private void resxWatcher_Changed(object sender, FileSystemEventArgs e)
        {
            if (!OglPanel._init)
                return;

            if (e.ChangeType == WatcherChangeTypes.Changed || e.ChangeType == WatcherChangeTypes.Created)
            {
                FileInfo resxFile = new FileInfo(e.FullPath);

                while (BitmapUtilities.IsFileLocked(resxFile))
                    Thread.Yield();


                DateTime last_update = File.GetLastWriteTime(e.FullPath);


                if (update_watcher.ContainsKey(resxFile.Name) && last_update == update_watcher[resxFile.Name])
                    return;

                bool create_resx = (NativeFunc.editor_resx_exist(resxFile.Name) == NativeFunc.wbool.wfalse);

                //rechargement de la resource
                if (resxFile.Extension == ".png")
                {
                    Bitmap nResx = new Bitmap(e.FullPath);

                    byte[] resx_content = BitmapUtilities.GetBitmapContent(nResx);

                    if (create_resx)
                    {
                        NativeFunc.editor_addtextureresx(resxFile.Name, nResx.Width, nResx.Height, resx_content, (uint)resx_content.Length);
                        game_resources.Add(new ResxInfo(ResxType.texture, resxFile.Name, resxFile.FullName));
                        game_resources = (from u in game_resources orderby u.ident ascending select u).ToList<ResxInfo>();
                    }
                    else
                    {
                        NativeFunc.editor_updatetextureresx(resxFile.Name, nResx.Width, nResx.Height, resx_content, (uint)resx_content.Length);
                        NativeFunc.editor_updatespritetextures(resxFile.Name);
                    }

                    nResx.Dispose();

                }
                else if (resxFile.Extension == ".awf")
                {
                    using (var file = File.OpenRead(e.FullPath))
                    {
                        byte[] aBuffer = new byte[file.Length];

                        file.Read(aBuffer, 0, aBuffer.Length);

                        if (create_resx)
                        {
                            NativeFunc.editor_addanimresx(resxFile.Name, aBuffer, (uint)aBuffer.Length);
                            game_resources.Add(new ResxInfo(ResxType.animation, resxFile.Name, resxFile.FullName));
                            game_resources = (from u in game_resources orderby u.ident ascending select u).ToList<ResxInfo>();
                        }
                        else
                        {
                            NativeFunc.editor_updateanimresx(resxFile.Name, aBuffer, (uint)aBuffer.Length);
                            NativeFunc.editor_updatespriteanimations(resxFile.Name);
                        }
                    }
                }
                else if (resxFile.Extension == ".pack")
                {
                    using (var file = File.OpenRead(e.FullPath))
                    {
                        byte[] aBuffer = new byte[file.Length];

                        file.Read(aBuffer, 0, aBuffer.Length);

                        NativeFunc.editor_setpacker(aBuffer, (uint)aBuffer.Length);
                    }
                }
                else if (resxFile.Extension == ".ogg")
                {
                    string path = Path.GetDirectoryName(e.FullPath);

                    if (create_resx)
                        NativeFunc.editor_addsoundresx(resxFile.Name, path);
                    else
                        NativeFunc.editor_updatesoundresx(resxFile.Name);
                }
                else if (resxFile.Extension == ".shad")
                {
                    string path = Path.GetDirectoryName(e.FullPath);

                    if (create_resx)
                        NativeFunc.editor_addshaderresx(resxFile.Name, path);
                    else
                        NativeFunc.editor_updateshaderresx(resxFile.Name);
                }

                if (!update_watcher.ContainsKey(resxFile.Name))
                    update_watcher.Add(resxFile.Name, last_update);
                else
                    update_watcher[resxFile.Name] = last_update;

            }


        }

        private void scriptWatcher_Changed(object sender, FileSystemEventArgs e)
        {
            if (levelObj == null || levelObj.MapArrays.Count == 0 || tabMaps.SelectedIndex == -1)
                return;

            Map mapObj = levelObj.MapArrays[tabMaps.SelectedIndex];

            FileInfo cFile = new FileInfo(e.FullPath);

            FileInfo scriptFile = new FileInfo(e.FullPath);

            DateTime last_update = File.GetLastWriteTime(e.FullPath);

            if (update_watcher.ContainsKey(scriptFile.Name) && last_update == update_watcher[scriptFile.Name])
                return;

            if (e.ChangeType == WatcherChangeTypes.Changed && (e.FullPath == levelObj.EditorScriptFile || this.IsEntityModule(cFile.Name.Substring(0, cFile.Name.LastIndexOf('.'))) || this.IsTextEntityModule(cFile.Name.Substring(0, cFile.Name.LastIndexOf('.'))) || cFile.Name.Substring(0, cFile.Name.LastIndexOf('.')) == "shader_config"))
            {
                while (BitmapUtilities.IsFileLocked(scriptFile))
                    Thread.Yield();

                //rechargement du fichier de script
                string level_module = scriptFile.Name.Substring(0, scriptFile.Name.LastIndexOf('.'));

                if (level_module == "shader_config")
                {
                    NativeFunc.editor_reloadshaderconfig();
                }
                else
                {
                    this.charger_module(level_module, true);


                    if (e.FullPath == levelObj.EditorScriptFile)
                    {
                        //appel fonction init uniquement pour le script de niveau
                        StringBuilder buffer = new StringBuilder(1000);
                        string function = string.Format("{0}.init()", level_module);
                        NativeFunc.editor_exec_string(function, buffer, buffer.Capacity);

                        if (buffer.Length == 0)
                            LogTextToBox("[LUA] fonction " + function + " OK");
                        else
                            LogTextToBox(buffer.ToString());
                    }
                }

            }
            else if (e.ChangeType == WatcherChangeTypes.Created)
            {
                //check if this file is a proper lua module
                string module_name = string.Empty;
                if (this.verif_module_script(e.FullPath, out module_name) && !script_modules.Contains(module_name))
                    script_modules.Add(module_name);
            }

            if (!update_watcher.ContainsKey(scriptFile.Name))
                update_watcher.Add(scriptFile.Name, last_update);
            else
                update_watcher[scriptFile.Name] = last_update;
        }

        private void localedbWatcher_Changed(object sender, FileSystemEventArgs e)
        {
            if (e.ChangeType == WatcherChangeTypes.Changed && (e.FullPath == Settings.Default.localedb_path))
            {
                NativeFunc.editor_release_localedb();


                string app_path = Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf(Path.DirectorySeparatorChar));
                //replace existing locale db with new locale db
                File.Copy(Settings.Default.localedb_path, app_path + Path.DirectorySeparatorChar + "locale.db", true);

                NativeFunc.editor_load_localedb();
            }
        }


        #endregion


        #region Gestion Calques

        SolidBrush lightBlueBrush = new SolidBrush(Color.FromArgb(255, 10, 116, 255));

        //taille d'une checkbox = 13


        private void calquesList_DrawNode(object sender, DrawTreeNodeEventArgs e)
        {
            if (e.Node.IsSelected)
                e.Graphics.FillRectangle(lightBlueBrush, e.Bounds);
            else
                e.Graphics.FillRectangle(Brushes.White, e.Bounds);

            if (e.Node.TreeView.CheckBoxes)
            {
                Point checkLocation = e.Bounds.Location;

                int halfsizeH = e.Node.TreeView.ItemHeight / 2;

                checkLocation.X += e.Node.TreeView.Indent + 2;
                checkLocation.Y += (halfsizeH) - 6;

                CheckBoxRenderer.DrawCheckBox(e.Graphics, checkLocation, (e.Node.Checked) ? System.Windows.Forms.VisualStyles.CheckBoxState.CheckedNormal : System.Windows.Forms.VisualStyles.CheckBoxState.UncheckedNormal);
            }

            if (e.Node.Nodes.Count > 0)
            {
                Point glyph_location = e.Bounds.Location;

                glyph_location.X += e.Node.TreeView.Indent * e.Node.Level;

                Rectangle rectangle1 = new Rectangle(glyph_location, new Size(16, 16));
                VisualStyleRenderer renderer = new VisualStyleRenderer((e.Node.IsExpanded) ? VisualStyleElement.TreeView.Glyph.Opened : VisualStyleElement.TreeView.Glyph.Closed);
                renderer.DrawBackground(e.Graphics, rectangle1);
            }

            TextRenderer.DrawText(e.Graphics,
                                   e.Node.Text,
                                   e.Node.TreeView.Font,
                                   e.Node.Bounds,
                                   e.Node.ForeColor);

        }


        private void calquesList_AfterSelect(object sender, TreeViewEventArgs e)
        {
            //reset selection if we select a different layer
            if (renderPanel.CurrentLayer != ((Layer)e.Node.Tag))
                NativeFunc.editor_entity_unselect();

            renderPanel.CurrentLayer = ((Layer)e.Node.Tag);

            if (renderPanel.CurrentLayer.Type == (int)TypeLayer.Normal)
            {
                tilesetBox.Visible = trackAlpha.Visible = lblAlpha.Visible = true;
                grpCollisions.Visible = grpObjects.Visible = false;
                NativeFunc.editor_showcursor(NativeFunc.wbool.wtrue, NativeFunc.wbool.wfalse);


                trackAlpha.Value = (int)(renderPanel.CurrentLayer.Opacity * 10);

                if (!string.IsNullOrEmpty(renderPanel.CurrentLayer.TilesetId))
                {
                    tilesetBox.Image = viewable_tileSets[renderPanel.CurrentLayer.TilesetId];
                    current_tile_offset = 0;
                    tilesetBox.Invalidate();
                }
                else
                {
                    tilesetBox.Image = viewable_tileSets[renderPanel.CurrentMap.TilesetId];
                    current_tile_offset = Settings.Default.tileset_offset;
                    tilesetBox.Invalidate();
                }

                if (renderPanel.CurrentLayer.Animated)
                {
                    trkCurrentFrame.Visible = true;
                    numCurrentFrame.Visible = true;
                    lblCurrentFrame.Visible = true;
                    numCurrentFrame.Value = trkCurrentFrame.Value;

                    trkCurrentFrame.Maximum = renderPanel.CurrentLayer.Frames.Count;
                    trkCurrentFrame.Value = renderPanel.CurrentLayer.EditorCurrentFrame;
                }
                else
                {
                    trkCurrentFrame.Visible = false;
                    numCurrentFrame.Visible = false;
                }
            }
            else if (renderPanel.CurrentLayer.Type == (int)TypeLayer.Collisions)
            {
                tilesetBox.Visible = grpObjects.Visible = lblCurrentFrame.Visible = trkCurrentFrame.Visible = numCurrentFrame.Visible = false;
                grpCollisions.Visible = true;
                lblAlpha.Visible = trackAlpha.Visible = true;
                trackAlpha.Value = (int)(renderPanel.CurrentLayer.Opacity * 10);
                NativeFunc.editor_showcursor(NativeFunc.wbool.wtrue, NativeFunc.wbool.wfalse);
            }
            else if (renderPanel.CurrentLayer.Type == (int)TypeLayer.Objects)
            {
                lblAlpha.Visible = trackAlpha.Visible = tilesetBox.Visible = grpCollisions.Visible = trkCurrentFrame.Visible = numCurrentFrame.Visible = lblCurrentFrame.Visible = false;
                grpObjects.Visible = true;
                NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wtrue);
            }

            //update elements list
            ItemsList.Nodes["Nodeentity"].Nodes.Clear();
            ItemsList.Nodes["Nodetrigger"].Nodes.Clear();
            ItemsList.Nodes["NodeText"].Nodes.Clear();
            ItemsList.Nodes["NodeSound"].Nodes.Clear();
            ItemsList.Nodes["NodeLight"].Nodes.Clear();
            ItemsList.Nodes["NodeParticles"].Nodes.Clear();
            ItemsList.Nodes["NodeWaypoint"].Nodes.Clear();
            ItemsList.Nodes["NodeCollider"].Nodes.Clear();

            List<Entity> ordered_entity = (from u in renderPanel.CurrentLayer.EntityArrays orderby u.EntityId select u).ToList<Entity>();
            List<Trigger> ordered_trigger = (from u in renderPanel.CurrentLayer.TriggerArrays orderby u.TriggerId select u).ToList<Trigger>();
            List<TextObject> ordered_text = (from u in renderPanel.CurrentLayer.TextArrays orderby u.TextId select u).ToList<TextObject>();
            List<GroupEntity> ordered_group = (from u in renderPanel.CurrentLayer.GroupArrays orderby u.GroupId select u).ToList<GroupEntity>();
            List<SoundSource> ordered_sound = (from u in renderPanel.CurrentLayer.SoundArrays orderby u.SoundId select u).ToList<SoundSource>();

            List<LightSource> ordered_light = (from u in renderPanel.CurrentLayer.LightArrays orderby u.LightId select u).ToList<LightSource>();
            List<ParticlesData> ordered_particles = (from u in renderPanel.CurrentLayer.ParticlesArrays orderby u.Id select u).ToList<ParticlesData>();

            List<Waypoint> ordered_way = (from u in renderPanel.CurrentLayer.Waypoints orderby u.Id select u).ToList<Waypoint>();
            List<Collider> ordered_col = (from u in renderPanel.CurrentLayer.Colliders orderby u.Id select u).ToList<Collider>();

            foreach (GroupEntity ogroup in ordered_group)
            {
                TreeNode cNode = new TreeNode();
                cNode.Text = ogroup.GroupId;
                cNode.Tag = ogroup;
                cNode.Name = "group_" + ogroup.GroupId;

                ItemsList.Nodes["Nodeentity"].Nodes.Add(cNode);
            }

            foreach (Entity oent in ordered_entity)
            {
                TreeNode cNode = new TreeNode();
                cNode.Text = oent.EntityId;
                cNode.Tag = oent;
                cNode.Name = oent.EntityId;

                if (string.IsNullOrEmpty(oent.GroupId))
                    ItemsList.Nodes["Nodeentity"].Nodes.Add(cNode);
                else
                    ItemsList.Nodes["Nodeentity"].Nodes["group_" + oent.GroupId].Nodes.Add(cNode);
            }

            foreach (Trigger otrig in ordered_trigger)
            {
                TreeNode cNode = new TreeNode();
                cNode.Text = otrig.TriggerId;
                cNode.Tag = otrig;
                cNode.Name = otrig.TriggerId;

                ItemsList.Nodes["Nodetrigger"].Nodes.Add(cNode);
            }

            foreach (TextObject otext in ordered_text)
            {
                TreeNode cNode = new TreeNode();
                cNode.Text = otext.TextId;
                cNode.Tag = otext;
                cNode.Name = otext.TextId;
                ItemsList.Nodes["NodeText"].Nodes.Add(cNode);
            }

            foreach (SoundSource osound in ordered_sound)
            {
                TreeNode cNode = new TreeNode();
                cNode.Text = osound.SoundId;
                cNode.Tag = osound;
                cNode.Name = osound.SoundId;
                ItemsList.Nodes["NodeSound"].Nodes.Add(cNode);
            }


            foreach (LightSource olight in ordered_light)
            {
                TreeNode cNode = new TreeNode();
                cNode.Text = olight.LightId;
                cNode.Tag = olight;
                cNode.Name = olight.LightId;
                ItemsList.Nodes["NodeLight"].Nodes.Add(cNode);
            }

            foreach (ParticlesData opart in ordered_particles)
            {
                TreeNode cNode = new TreeNode();
                cNode.Text = opart.Id;
                cNode.Tag = opart;
                cNode.Name = opart.Id;
                ItemsList.Nodes["NodeParticles"].Nodes.Add(cNode);
            }

            foreach (Waypoint oway in ordered_way)
            {
                TreeNode cNode = new TreeNode();
                cNode.Text = oway.Id;
                cNode.Tag = oway;
                cNode.Name = oway.Id;

                ItemsList.Nodes["NodeWaypoint"].Nodes.Add(cNode);
            }


            foreach (Collider ocol in ordered_col)
            {
                TreeNode cNode = new TreeNode();
                cNode.Text = ocol.Id;
                cNode.Tag = ocol;
                cNode.Name = ocol.Id;

                ItemsList.Nodes["NodeCollider"].Nodes.Add(cNode);
            }

        }

        private void btnNoCol_Click(object sender, EventArgs e)
        {
            btncoleditable.FlatStyle = FlatStyle.Standard;
            btnNoCol.FlatStyle = FlatStyle.Flat;
            btnCol.FlatStyle = FlatStyle.Standard;
            btnColDestruct.FlatStyle = FlatStyle.Standard;
            btnSlope.FlatStyle = FlatStyle.Standard;
            btncoleditable_2.FlatStyle = FlatStyle.Standard;
            btnSlope2.FlatStyle = FlatStyle.Standard;
            btncoleditable_3.FlatStyle = FlatStyle.Standard;
            btnKill.FlatStyle = FlatStyle.Standard;
            renderPanel.CurrentCollisionVal = (int)NativeFunc.Collisions_Values.NO_COLLISION;
        }

        private void btnCol_Click(object sender, EventArgs e)
        {
            btncoleditable.FlatStyle = FlatStyle.Standard;
            btnCol.FlatStyle = FlatStyle.Flat;
            btnNoCol.FlatStyle = FlatStyle.Standard;
            btnColDestruct.FlatStyle = FlatStyle.Standard;
            btncoleditable_2.FlatStyle = FlatStyle.Standard;
            btnSlope.FlatStyle = FlatStyle.Standard;
            btnSlope2.FlatStyle = FlatStyle.Standard;
            btncoleditable_3.FlatStyle = FlatStyle.Standard;
            btnKill.FlatStyle = FlatStyle.Standard;
            renderPanel.CurrentCollisionVal = (int)NativeFunc.Collisions_Values.COLLISION;
        }

        private void btnColDestruct_Click(object sender, EventArgs e)
        {
            btncoleditable.FlatStyle = FlatStyle.Standard;
            btnCol.FlatStyle = FlatStyle.Standard;
            btnNoCol.FlatStyle = FlatStyle.Standard;
            btnColDestruct.FlatStyle = FlatStyle.Flat;
            btncoleditable_2.FlatStyle = FlatStyle.Standard;
            btnSlope.FlatStyle = FlatStyle.Standard;
            btnSlope2.FlatStyle = FlatStyle.Standard;
            btncoleditable_3.FlatStyle = FlatStyle.Standard;
            btnKill.FlatStyle = FlatStyle.Standard;
            renderPanel.CurrentCollisionVal = (int)NativeFunc.Collisions_Values.COL_DESTROY;

        }

        private void btnSlope_Click(object sender, EventArgs e)
        {
            btncoleditable.FlatStyle = FlatStyle.Standard;
            btnCol.FlatStyle = FlatStyle.Standard;
            btnNoCol.FlatStyle = FlatStyle.Standard;
            btnColDestruct.FlatStyle = FlatStyle.Standard;
            btncoleditable_2.FlatStyle = FlatStyle.Standard;
            btnSlope.FlatStyle = FlatStyle.Flat;
            btncoleditable_3.FlatStyle = FlatStyle.Standard;
            btnKill.FlatStyle = FlatStyle.Standard;
            btnSlope2.FlatStyle = FlatStyle.Standard;

            renderPanel.CurrentCollisionVal = (int)NativeFunc.Collisions_Values.COL_SLOPE;
        }

        private void btnSlope2_Click(object sender, EventArgs e)
        {
            btncoleditable.FlatStyle = FlatStyle.Standard;
            btnCol.FlatStyle = FlatStyle.Standard;
            btnNoCol.FlatStyle = FlatStyle.Standard;
            btnColDestruct.FlatStyle = FlatStyle.Standard;
            btnSlope.FlatStyle = FlatStyle.Standard;
            btncoleditable_2.FlatStyle = FlatStyle.Standard;
            btncoleditable_3.FlatStyle = FlatStyle.Standard;
            btnKill.FlatStyle = FlatStyle.Standard;
            btnSlope2.FlatStyle = FlatStyle.Flat;

            renderPanel.CurrentCollisionVal = (int)NativeFunc.Collisions_Values.COL_SLOPE_INVERSE;
        }


        private void btncoleditable_Click(object sender, EventArgs e)
        {
            btnCol.FlatStyle = FlatStyle.Standard;
            btnNoCol.FlatStyle = FlatStyle.Standard;
            btnColDestruct.FlatStyle = FlatStyle.Standard;
            btnSlope.FlatStyle = FlatStyle.Standard;
            btnSlope2.FlatStyle = FlatStyle.Standard;
            btncoleditable_2.FlatStyle = FlatStyle.Standard;
            btncoleditable_3.FlatStyle = FlatStyle.Standard;
            btnKill.FlatStyle = FlatStyle.Standard;
            btncoleditable.FlatStyle = FlatStyle.Flat;


            renderPanel.CurrentCollisionVal = (int)NativeFunc.Collisions_Values.COL_EDITABLE;
        }

        private void btncoleditable_2_Click(object sender, EventArgs e)
        {
            btnCol.FlatStyle = FlatStyle.Standard;
            btnNoCol.FlatStyle = FlatStyle.Standard;
            btnColDestruct.FlatStyle = FlatStyle.Standard;
            btnSlope.FlatStyle = FlatStyle.Standard;
            btnSlope2.FlatStyle = FlatStyle.Standard;
            btncoleditable.FlatStyle = FlatStyle.Standard;
            btncoleditable_3.FlatStyle = FlatStyle.Standard;
            btnKill.FlatStyle = FlatStyle.Standard;
            btncoleditable_2.FlatStyle = FlatStyle.Flat;

            renderPanel.CurrentCollisionVal = (int)NativeFunc.Collisions_Values.COL_EDITABLE_2;
        }

        private void btncoleditable_3_Click(object sender, EventArgs e)
        {
            btnCol.FlatStyle = FlatStyle.Standard;
            btnNoCol.FlatStyle = FlatStyle.Standard;
            btnColDestruct.FlatStyle = FlatStyle.Standard;
            btnSlope.FlatStyle = FlatStyle.Standard;
            btnSlope2.FlatStyle = FlatStyle.Standard;
            btncoleditable.FlatStyle = FlatStyle.Standard;
            btncoleditable_2.FlatStyle = FlatStyle.Standard;
            btnKill.FlatStyle = FlatStyle.Standard;
            btncoleditable_3.FlatStyle = FlatStyle.Flat;

            renderPanel.CurrentCollisionVal = (int)NativeFunc.Collisions_Values.COL_EDITABLE_3;
        }

        private void btnKill_Click(object sender, EventArgs e)
        {
            btnCol.FlatStyle = FlatStyle.Standard;
            btnNoCol.FlatStyle = FlatStyle.Standard;
            btnColDestruct.FlatStyle = FlatStyle.Standard;
            btnSlope.FlatStyle = FlatStyle.Standard;
            btnSlope2.FlatStyle = FlatStyle.Standard;
            btncoleditable.FlatStyle = FlatStyle.Standard;
            btncoleditable_2.FlatStyle = FlatStyle.Standard;
            btncoleditable_3.FlatStyle = FlatStyle.Standard;
            btnKill.FlatStyle = FlatStyle.Flat;

            renderPanel.CurrentCollisionVal = (int)NativeFunc.Collisions_Values.COL_KILL;
        }




        private void ajouterUnCalqueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AjouterCalque();
            this.MajniveauBackup();
        }

        private void importerUnCalqueDepuisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ImporterCalque();
            this.MajniveauBackup();
        }

        private void supprimerUnCalqueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SupprimerCalque();
            this.MajniveauBackup();
        }



        private void calquesList_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                ctxMenuCalques.Show(calquesList, e.Location);
            }
        }

        private void calquesList_AfterCheck(object sender, TreeViewEventArgs e)
        {
            renderPanel.ChangeLayerVisibility(e.Node.Checked ? NativeFunc.wbool.wtrue : NativeFunc.wbool.wfalse, (Layer)e.Node.Tag);
        }


        private void trackAlpha_ValueChanged(object sender, EventArgs e)
        {
            if (calquesList.SelectedNode == null)
                return;

            Layer oLayer = calquesList.SelectedNode.Tag as Layer;

            oLayer.Opacity = (float)trackAlpha.Value * 0.1f;

            if (oLayer.Type == (int)TypeLayer.Collisions)
                NativeFunc.editor_changecollisions_color(oLayer.Opacity);
            else if (oLayer.Type == (int)TypeLayer.Normal)
                NativeFunc.editor_Tilemap_changeopacity(renderPanel.CurrentLayer.LayerId, oLayer.Opacity);

            renderPanel.Invalidate();
        }


        private void modifierCalqueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ModifierCalque();
            this.MajniveauBackup();
        }

        private void dupliquerCalqueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DupliquerCalque();
            this.MajniveauBackup();
        }

        private void AjouterCalque()
        {
            AjoutCalque formCalque = new AjoutCalque(levelObj.MapArrays[tabMaps.SelectedIndex], game_resources);


            if (formCalque.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Map mapObj = levelObj.MapArrays[tabMaps.SelectedIndex];

                Layer cLayer = formCalque.Tag as Layer;

                TreeNode cNode = new TreeNode();
                cNode.Checked = true;
                cNode.Text = cLayer.LayerId;
                cNode.Tag = cLayer;

                calquesList.Nodes.Add(cNode);
            }
        }

        private void ImporterCalque()
        {
            string tmpfilename = openLevelDialog.FileName;

            //sélection d'un niveau pour import
            if (openLevelDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Level importlevel = null;

                using (var file = File.OpenRead(openLevelDialog.FileName))
                {
                    importlevel = ProtoBuf.Serializer.Deserialize<PBInterface.Level>(file);
                }

                if (importlevel != null)
                {
                    ImportCalque newformImport = new ImportCalque(importlevel, levelObj.MapArrays[tabMaps.SelectedIndex].ColCount, levelObj.MapArrays[tabMaps.SelectedIndex].RowCount);

                    if (newformImport.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        Layer importedLayer = newformImport.Tag as Layer;

                        Map mapObj = levelObj.MapArrays[tabMaps.SelectedIndex];

                        string base_id = importedLayer.LayerId;
                        string new_id;
                        int num_copy = Getcopypasteid(ref base_id, out new_id);
                        importedLayer.LayerId = new_id;

                        while (mapObj.LayerArrays.Find(p => p.LayerId == importedLayer.LayerId) != null)
                            importedLayer.LayerId = base_id + (++num_copy).ToString();

                        importedLayer.Order = mapObj.LayerArrays.Count(p => p.Type == (int)TypeLayer.Normal);
                        importedLayer.EditorOrder = mapObj.LayerArrays.Count();

                        //check if the layer texture (if any exist)

                        if (importedLayer.Type == (int)TypeLayer.Normal && !string.IsNullOrEmpty(importedLayer.TilesetId) && NativeFunc.editor_resx_exist(importedLayer.TilesetId) == NativeFunc.wbool.wfalse)
                        {
                            //récupération du contenu bitmap du fichier gal
                            GaleReader gaReader = new GaleReader();

                            Bitmap bmpPacked = null;
                            Bitmap bmpView = null;

                            try
                            {
                                gaReader.OpenFile(importedLayer.TilesetId);

                                bmpPacked = gaReader.GetPackedContent(TILE_PER_ROW, gaReader.GetWidth(), gaReader.GetHeight());
                                bmpView = gaReader.GetPackedContent(TILE_PER_ROW, gaReader.GetWidth(), gaReader.GetHeight(), Settings.Default.tileset_offset, Settings.Default.tileset_back_color);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.ToString());
                            }
                            finally
                            {
                                if (gaReader.FileOpened)
                                    gaReader.CloseFile();
                            }


                            byte[] content = BitmapUtilities.GetBitmapContent(bmpPacked);
                            NativeFunc.editor_addtextureresx(importedLayer.TilesetId, bmpPacked.Width, bmpPacked.Height, content, (uint)content.Length);
                        }

                        //native data
                        MemoryStream layerStream = new MemoryStream();
                        layerStream.Position = 0;
                        ProtoBuf.Serializer.Serialize<Layer>(layerStream, importedLayer);

                        layerStream.Position = 0;

                        byte[] tmplayerArray = layerStream.ToArray();
                        layerStream.Close();

                        NativeFunc.editor_addlayer(tmplayerArray, (uint)tmplayerArray.Length);


                        mapObj.LayerArrays.Add(importedLayer);


                        TreeNode cNode = new TreeNode();
                        cNode.Checked = true;
                        cNode.Text = importedLayer.LayerId;
                        cNode.Tag = importedLayer;

                        calquesList.Nodes.Add(cNode);
                    }

                }
                else
                {
                    MessageBox.Show("Erreur lors de l'ouverture du fichier .lwf à importer");
                }

            }

            openLevelDialog.FileName = tmpfilename;
        }

        private void ModifierCalque()
        {
            if (calquesList.SelectedNode == null)
                return;

            Layer oLayer = calquesList.SelectedNode.Tag as Layer;

            AjoutCalque formCalque = new AjoutCalque(levelObj.MapArrays[tabMaps.SelectedIndex], game_resources);

            formCalque.FillData(oLayer);

            formCalque.mode = DialogMode.Modif;

            if (formCalque.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (oLayer.LayerId != calquesList.SelectedNode.Text)
                {
                    //raz historique lié à l'ancien id
                    history.RemoveActionFromID(calquesList.SelectedNode.Text, HistoryAction.SetTile);
                    history.RemoveActionFromID(calquesList.SelectedNode.Text, HistoryAction.FloodTile);

                    calquesList.SelectedNode.Text = oLayer.LayerId;
                }

                updateLayerZorder(oLayer);
            }
        }

        private void DupliquerCalque()
        {
            if (calquesList.SelectedNode == null)
                return;

            Layer oLayer = calquesList.SelectedNode.Tag as Layer;

            Layer newLayer = ProtoBuf.Serializer.DeepClone<Layer>(oLayer);

            Map mapObj = levelObj.MapArrays[tabMaps.SelectedIndex];

            string base_id = newLayer.LayerId;
            string new_id;
            int num_copy = Getcopypasteid(ref base_id, out new_id);
            newLayer.LayerId = new_id;

            while (mapObj.LayerArrays.Find(p => p.LayerId == newLayer.LayerId) != null)
                newLayer.LayerId = base_id + (++num_copy).ToString();

            newLayer.Order = mapObj.LayerArrays.Count(p => p.Type == (int)TypeLayer.Normal);
            newLayer.EditorOrder = mapObj.LayerArrays.Count();

            //native data
            MemoryStream layerStream = new MemoryStream();
            layerStream.Position = 0;
            ProtoBuf.Serializer.Serialize<Layer>(layerStream, newLayer);

            layerStream.Position = 0;

            byte[] tmplayerArray = layerStream.ToArray();
            layerStream.Close();

            NativeFunc.editor_addlayer(tmplayerArray, (uint)tmplayerArray.Length);


            mapObj.LayerArrays.Add(newLayer);


            TreeNode cNode = new TreeNode();
            cNode.Checked = true;
            cNode.Text = newLayer.LayerId;
            cNode.Tag = newLayer;

            calquesList.Nodes.Add(cNode);

        }

        private void SupprimerCalque()
        {
            if (calquesList.SelectedNode == null)
                return;

            Layer oLayer = calquesList.SelectedNode.Tag as Layer;
            Map mapObj = levelObj.MapArrays[tabMaps.SelectedIndex];

            if (MessageBox.Show("Supprimer le calque " + oLayer.LayerId + " ?", "Suppression Calque", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {

                history.RemoveActionFromID(oLayer.LayerId, HistoryAction.FloodTile);
                history.RemoveActionFromID(oLayer.LayerId, HistoryAction.SetTile);

                if (oLayer.Type == (int)TypeLayer.Normal)
                    NativeFunc.editor_removelayer(oLayer.LayerId);
                else if (oLayer.Type == (int)TypeLayer.Collisions)
                    NativeFunc.editor_removecollisions();
                else if (oLayer.Type == (int)TypeLayer.Objects)
                {
                    foreach (Entity oentity in oLayer.EntityArrays)
                        NativeFunc.editor_removeentity(oentity.EntityId, NativeFunc.GAME_TYPE.GT_ENTITY);

                    foreach (TextObject oentity in oLayer.TextArrays)
                        NativeFunc.editor_removeentity(oentity.TextId, NativeFunc.GAME_TYPE.GT_TEXT);

                    foreach (Trigger oentity in oLayer.TriggerArrays)
                        NativeFunc.editor_removeentity(oentity.TriggerId, NativeFunc.GAME_TYPE.GT_TRIGGER);

                    foreach (LightSource osource in oLayer.LightArrays)
                        NativeFunc.editor_removeentity(osource.LightId, NativeFunc.GAME_TYPE.GT_LIGHT);

                    foreach (ParticlesData opart in oLayer.ParticlesArrays)
                        NativeFunc.editor_removeentity(opart.Id, NativeFunc.GAME_TYPE.GT_PARTICLES);

                    foreach (Waypoint oway in oLayer.Waypoints)
                        NativeFunc.editor_removeentity(oway.Id, NativeFunc.GAME_TYPE.GT_WAYPOINT);

                    foreach (Collider ocol in oLayer.Colliders)
                        NativeFunc.editor_removeentity(ocol.Id, NativeFunc.GAME_TYPE.GT_COLLIDER);
                }

                mapObj.LayerArrays.Remove(oLayer);


                TreeNode sNode = calquesList.SelectedNode;

                calquesList.Nodes.Remove(sNode);

                calquesList.SelectedNode = null;
            }
        }

        //changement ordre des calques
        private void btnOrderUp_Click(object sender, EventArgs e)
        {
            if (calquesList.SelectedNode == null)
                return;


            if (calquesList.SelectedNode.PrevNode == null)
                return;

            Map mapObj = levelObj.MapArrays[tabMaps.SelectedIndex];

            TreeNode tmp_select = calquesList.SelectedNode;

            Layer oLayer = calquesList.SelectedNode.Tag as Layer;

            oLayer.EditorOrder--;

            Layer prevLayer = calquesList.SelectedNode.PrevNode.Tag as Layer;

            prevLayer.EditorOrder++;


            if ((oLayer.Type == (int)TypeLayer.Normal || oLayer.Type == (int)TypeLayer.Objects) && (prevLayer.Type == (int)TypeLayer.Normal || prevLayer.Type == (int)TypeLayer.Objects))
            {
                if (!oLayer.Topmost)
                {
                    oLayer.Order--;
                    prevLayer.Order++;

                    if(oLayer.Type == (int)TypeLayer.Normal)
                    {
                        NativeFunc.editor_changelayer_order(oLayer.LayerId, oLayer.Order);
                    }
                        
                    if (prevLayer.Type == (int)TypeLayer.Normal)
                    {
                        NativeFunc.editor_changelayer_order(prevLayer.LayerId, prevLayer.Order);
                    }  

                    updateLayerZorder(prevLayer);
                }
                else
                {
                    oLayer.Order = Settings.Default.MAX_ZORDER;
                    updateLayerZorder(oLayer);
                }

            }

            this.MajniveauBackup();

            calquesList.Sort();

            calquesList.SelectedNode = tmp_select;

        }

        private void btnOrderDown_Click(object sender, EventArgs e)
        {
            if (calquesList.SelectedNode == null)
                return;

            if (calquesList.SelectedNode.NextNode == null)
                return;

            Layer oLayer = calquesList.SelectedNode.Tag as Layer;

            oLayer.EditorOrder++;

            TreeNode tmp_select = calquesList.SelectedNode;

            Layer nextLayer = calquesList.SelectedNode.NextNode.Tag as Layer;
            nextLayer.EditorOrder--;

            if ((oLayer.Type == (int)TypeLayer.Normal || oLayer.Type == (int)TypeLayer.Objects) && (nextLayer.Type == (int)TypeLayer.Normal || nextLayer.Type == (int)TypeLayer.Objects) && !oLayer.Topmost)
            {
                if (!oLayer.Topmost)
                {
                    oLayer.Order++;

                    if (oLayer.Type == (int)TypeLayer.Normal)
                    {
                        NativeFunc.editor_changelayer_order(oLayer.LayerId, oLayer.Order);
                    }

                    updateLayerZorder(oLayer);
                    nextLayer.Order--;

                    if (oLayer.Type == (int)TypeLayer.Normal)
                    {
                        NativeFunc.editor_changelayer_order(nextLayer.LayerId, nextLayer.Order);
                    }

                    updateLayerZorder(nextLayer);

                }
                else
                {
                    oLayer.Order = Settings.Default.MAX_ZORDER;
                    updateLayerZorder(oLayer);
                }
            }

            this.MajniveauBackup();

            calquesList.Sort();

            calquesList.SelectedNode = tmp_select;
        }

        private void btnReorder_Click(object sender, EventArgs e)
        {
            int editor_order = 0;
            int draw_order = 0;

            TreeNode tmp_select = calquesList.SelectedNode;

            foreach (TreeNode node in calquesList.Nodes)
            {
                Layer oLayer = node.Tag as Layer;

                oLayer.EditorOrder = editor_order++;

                if ((oLayer.Type == (int)TypeLayer.Normal || oLayer.Type == (int)TypeLayer.Objects))
                {
                    oLayer.Order = (oLayer.Topmost) ? Settings.Default.MAX_ZORDER : draw_order++;

                    if (oLayer.Type == (int)TypeLayer.Normal && !oLayer.Topmost)
                        NativeFunc.editor_changelayer_order(oLayer.LayerId, oLayer.Order);

                    updateLayerZorder(oLayer);
                }
            }

            this.MajniveauBackup();

            calquesList.Sort();

            calquesList.SelectedNode = tmp_select;
        }

        private void updateLayerZorder(Layer oLayer)
        {
            if (oLayer.Type == (int)TypeLayer.Objects)
            {
                oLayer.EntityArrays.ForEach(delegate (Entity oent) { oent.ZOrder = oLayer.Order; NativeFunc.editor_entity_changezorder(oent.EntityId, oent.ZOrder, NativeFunc.GAME_TYPE.GT_ENTITY); });
                oLayer.TextArrays.ForEach(delegate (TextObject oent) { oent.ZOrder = oLayer.Order; NativeFunc.editor_entity_changezorder(oent.TextId, oent.ZOrder, NativeFunc.GAME_TYPE.GT_TEXT); });
                oLayer.TriggerArrays.ForEach(delegate (Trigger oent) { NativeFunc.editor_entity_changezorder(oent.TriggerId, oLayer.Order, NativeFunc.GAME_TYPE.GT_TRIGGER); });
                oLayer.LightArrays.ForEach(delegate (LightSource oent) { NativeFunc.editor_entity_changezorder(oent.LightId, oLayer.Order, NativeFunc.GAME_TYPE.GT_LIGHT); });
                oLayer.SoundArrays.ForEach(delegate (SoundSource oent) { NativeFunc.editor_entity_changezorder(oent.SoundId, oLayer.Order, NativeFunc.GAME_TYPE.GT_SOUND); });
                oLayer.ParticlesArrays.ForEach(delegate (ParticlesData oent) { oent.ZOrder = oLayer.Order; NativeFunc.editor_entity_changezorder(oent.Id, oLayer.Order, NativeFunc.GAME_TYPE.GT_PARTICLES); });
            }
        }

        //sélection d'un item depuis la liste des éléments
        private void ItemsList_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            NativeFunc.editor_entity_unselect();

            if (e.Node.Tag == null)
                return;

            if (e.Node.Tag is GroupEntity)
            {
                //sélection de toutes les entités du groupe
                foreach (TreeNode node in e.Node.Nodes)
                    NativeFunc.editor_selectentity(node.Text, NativeFunc.GAME_TYPE.GT_ENTITY, NativeFunc.wbool.wtrue);
            }
            else
            {
                NativeFunc.GAME_TYPE? selected_type = null;
                string id = e.Node.Text;


                if (e.Node.Tag is Entity)
                    selected_type = NativeFunc.GAME_TYPE.GT_ENTITY;
                else if (e.Node.Tag is TextObject)
                    selected_type = NativeFunc.GAME_TYPE.GT_TEXT;
                else if (e.Node.Tag is Trigger)
                    selected_type = NativeFunc.GAME_TYPE.GT_TRIGGER;
                else if (e.Node.Tag is LightSource)
                    selected_type = NativeFunc.GAME_TYPE.GT_LIGHT;
                else if (e.Node.Tag is SoundSource)
                    selected_type = NativeFunc.GAME_TYPE.GT_SOUND;
                else if (e.Node.Tag is ParticlesData)
                    selected_type = NativeFunc.GAME_TYPE.GT_PARTICLES;
                else if (e.Node.Tag is Waypoint)
                    selected_type = NativeFunc.GAME_TYPE.GT_WAYPOINT;
                else if (e.Node.Tag is Collider)
                    selected_type = NativeFunc.GAME_TYPE.GT_COLLIDER;


                if (selected_type.HasValue)
                    NativeFunc.editor_selectentity(id, selected_type.Value, NativeFunc.wbool.wfalse);
            }

            renderPanel.Invalidate();
        }




        #endregion


        #region Gestion scripts


        private bool verif_module_script(string filepath, out string module_name)
        {
            FileInfo finfo = new FileInfo(filepath);

            module_name = Path.GetFileNameWithoutExtension(filepath);

            using (StreamReader read = finfo.OpenText())
            {
                string data = read.ReadToEnd();

                if (data.IndexOf("return " + module_name) != -1 && data.IndexOf(module_name + " = {}") != -1 && data.IndexOf("function " + module_name + ".new(") != -1 && data.IndexOf("function " + module_name + ".init(") != -1)
                    return true;
            }

            return false;
        }

        private void ajouter_chemin_module(string chemin)
        {
            script_modules.Clear();

            StringBuilder load_modstr = new StringBuilder();

            load_modstr.AppendFormat(@"package.path = '{0}\\?.lua;' .. package.path", chemin.Replace(@"\", @"\\"));

            StringBuilder buffer = new StringBuilder(10000);

            NativeFunc.editor_exec_string(load_modstr.ToString(), buffer, buffer.Capacity);


            if (!string.IsNullOrEmpty(buffer.ToString()))
                LogTextToBox(buffer.ToString());
            else
            {

                //récupérer l'ensemble des module disponible
                if (Directory.Exists(chemin))
                {
                    LogTextToBox("[LUA] Chemin " + chemin + " ajouté au path");

                    string module_name = string.Empty;

                    foreach (string file in Directory.GetFiles(chemin, "*.lua"))
                    {
                        //check if this file is a proper lua module
                        if (this.verif_module_script(file, out module_name))
                            script_modules.Add(module_name);
                    }

                    script_base_path = chemin;
                }
                else
                {
                    LogTextToBox("[LUA] le chemin " + chemin + " n'est pas un répertoire valide");
                }

            }


        }

        private void charger_module(string nom_module, bool reload)
        {
            StringBuilder load_modstr = new StringBuilder();


            if (reload)
                load_modstr.AppendFormat("package.loaded['{0}'] = nil \n", nom_module);

            load_modstr.AppendFormat("require('{0}')", nom_module);

            StringBuilder buffer = new StringBuilder(10000);

            NativeFunc.editor_exec_string(load_modstr.ToString(), buffer, buffer.Capacity);


            if (!string.IsNullOrEmpty(buffer.ToString()))
                LogTextToBox(buffer.ToString());
            else
            {
                if (!reload)
                    LogTextToBox("[LUA] Module " + nom_module + " ajouté");
                else
                    LogTextToBox("[LUA] Module " + nom_module + " rechargé");
            }


        }

        #endregion

        #region gestion triggers

        TriggerDialog trigDiag;
        Size tmpoldsize;
        Point tmpoldpos;

        private void btnTrigger_Click(object sender, EventArgs e)
        {
            trigDiag = new TriggerDialog(levelObj);

            NativeFunc.editor_entity_unselect();

            if (trigDiag.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                renderPanel.MouseDown += this.trigger_startlaying;
                renderPanel.ToolSelected = Tools.Trigger;
            }
        }


        private void trigger_startlaying(object sender, MouseEventArgs e)
        {
            int worldposx, worldposy;
            renderPanel.GetWorldPos(e.X, e.Y, out worldposx, out worldposy);

            Trigger cTrigger = trigDiag.Tag as Trigger;
            cTrigger.Position = new Wvector();
            cTrigger.Position.X = worldposx;
            cTrigger.Position.Y = worldposy;

            renderPanel.CurrentLayer.TriggerArrays.Add(cTrigger);

            MemoryStream triggerStream = new MemoryStream();
            triggerStream.Position = 0;
            ProtoBuf.Serializer.Serialize<Trigger>(triggerStream, cTrigger);

            triggerStream.Position = 0;


            byte[] tmpTriggerArray = triggerStream.ToArray();

            triggerStream.Close();

            NativeFunc.editor_addtrigger(cTrigger.TriggerId, tmpTriggerArray, (uint)tmpTriggerArray.Length, renderPanel.CurrentMap.MapId, renderPanel.CurrentLayer.Order);
            NativeFunc.editor_selectentity(cTrigger.TriggerId, NativeFunc.GAME_TYPE.GT_TRIGGER, NativeFunc.wbool.wfalse);
            renderPanel.MouseDown -= this.trigger_startlaying;
            renderPanel.MouseMove += this.trigger_resize;
            renderPanel.MouseUp += this.trigger_endlay;

            NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wfalse);

            TreeNode cnode = new TreeNode();
            cnode.Text = cTrigger.TriggerId;
            cnode.Tag = cTrigger;
            cnode.Name = cTrigger.TriggerId;

            ItemsList.Nodes["Nodetrigger"].Nodes.Add(cnode);
            ItemsList.SelectedNode = cnode;

            this.MajniveauBackup();
        }

        private void trigger_setpos(object sender, MouseEventArgs e)
        {
            int worldposx, worldposy;
            renderPanel.GetWorldPos(e.X, e.Y, out worldposx, out worldposy, Settings.Default.snap_to_grid);

            StringBuilder trigger_id_buff = new StringBuilder(250);

            NativeFunc.editor_entity_get_selection(trigger_id_buff, 0, trigger_id_buff.Capacity);

            Trigger cTrigger = renderPanel.CurrentLayer.TriggerArrays.Find(p => p.TriggerId == trigger_id_buff.ToString());

            //in case we snap to grid, match the top left corner of the npc with the top left corner of the tile
            if (Settings.Default.snap_to_grid)
            {
                worldposx += (int)(cTrigger.Width * 0.5f);
                worldposy += (int)(cTrigger.Height * 0.5f);
            }

            cTrigger.Position = new Wvector();
            cTrigger.Position.X = worldposx;
            cTrigger.Position.Y = worldposy;

            NativeFunc.editor_entity_update_pos(cTrigger.TriggerId, cTrigger.Position.X, cTrigger.Position.Y, NativeFunc.GAME_TYPE.GT_TRIGGER);


        }

        private void trigger_setpos2(Point pos, string trigger_id)
        {
            Trigger cTrigger = renderPanel.CurrentLayer.TriggerArrays.Find(p => p.TriggerId == trigger_id);

            cTrigger.Position = new Wvector();
            cTrigger.Position.X = pos.X;
            cTrigger.Position.Y = pos.Y;

            NativeFunc.editor_entity_update_pos(cTrigger.TriggerId, cTrigger.Position.X, cTrigger.Position.Y, NativeFunc.GAME_TYPE.GT_TRIGGER);
            this.MajniveauBackup();
        }

        private void trigger_setresize(object sender, MouseEventArgs e)
        {
            renderPanel.MouseDown -= this.trigger_setresize;
            renderPanel.MouseMove += this.trigger_resize;
            renderPanel.MouseUp += this.trigger_endlay;

            StringBuilder trigger_id_buff = new StringBuilder(250);

            NativeFunc.editor_entity_get_selection(trigger_id_buff, 0, trigger_id_buff.Capacity);

            NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wfalse);

            Trigger cTrigger = renderPanel.CurrentLayer.TriggerArrays.Find(p => p.TriggerId == trigger_id_buff.ToString());

            tmpoldsize = new Size(cTrigger.Width, cTrigger.Height);
        }

        private void trigger_setrepos(object sender, MouseEventArgs e)
        {
            trigger_setpos(sender, e);
            renderPanel.MouseDown -= this.trigger_setrepos;
            renderPanel.MouseMove += this.trigger_setpos;
            renderPanel.MouseUp += this.trigger_endrepos;

            StringBuilder trigger_id_buff = new StringBuilder(250);

            NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wfalse);

            NativeFunc.editor_entity_get_selection(trigger_id_buff, 0, trigger_id_buff.Capacity);

            Trigger cTrigger = renderPanel.CurrentLayer.TriggerArrays.Find(p => p.TriggerId == trigger_id_buff.ToString());

            tmpoldpos = new Point(cTrigger.Position.X, cTrigger.Position.Y);
        }

        private void trigger_resize(object sender, MouseEventArgs e)
        {
            int worldposx, worldposy;
            renderPanel.GetWorldPos(e.X, e.Y, out worldposx, out worldposy);

            StringBuilder trigger_id_buff = new StringBuilder(250);

            NativeFunc.editor_entity_get_selection(trigger_id_buff, 0, trigger_id_buff.Capacity);

            Trigger cTrigger = renderPanel.CurrentLayer.TriggerArrays.Find(p => p.TriggerId == trigger_id_buff.ToString());
            cTrigger.Width = Math.Abs((worldposx - cTrigger.Position.X));
            cTrigger.Height = Math.Abs((worldposy - cTrigger.Position.Y));

            MemoryStream triggerStream = new MemoryStream();
            triggerStream.Position = 0;
            ProtoBuf.Serializer.Serialize<Trigger>(triggerStream, cTrigger);

            triggerStream.Position = 0;


            byte[] tmpTriggerArray = triggerStream.ToArray();

            triggerStream.Close();

            NativeFunc.editor_updatetrigger(cTrigger.TriggerId, tmpTriggerArray, (uint)tmpTriggerArray.Length);
        }

        private void trigger_resize2(Size trig_size, string trigger_id)
        {
            Trigger cTrigger = renderPanel.CurrentLayer.TriggerArrays.Find(p => p.TriggerId == trigger_id);
            cTrigger.Width = trig_size.Width;
            cTrigger.Height = trig_size.Height;

            MemoryStream triggerStream = new MemoryStream();
            triggerStream.Position = 0;
            ProtoBuf.Serializer.Serialize<Trigger>(triggerStream, cTrigger);

            triggerStream.Position = 0;


            byte[] tmpTriggerArray = triggerStream.ToArray();

            triggerStream.Close();

            NativeFunc.editor_updatetrigger(cTrigger.TriggerId, tmpTriggerArray, (uint)tmpTriggerArray.Length);

            this.MajniveauBackup();
        }

        private void trigger_endlay(object sender, MouseEventArgs e)
        {
            renderPanel.MouseMove -= this.trigger_resize;
            renderPanel.MouseUp -= this.trigger_endlay;

            NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wtrue);

            if (renderPanel.ToolSelected != Tools.Trigger)
            {
                StringBuilder trigger_id_buff = new StringBuilder(250);

                NativeFunc.editor_entity_get_selection(trigger_id_buff, 0, trigger_id_buff.Capacity);

                Trigger cTrigger = renderPanel.CurrentLayer.TriggerArrays.Find(p => p.TriggerId == trigger_id_buff.ToString());

                System.Drawing.Size newsize = new System.Drawing.Size(cTrigger.Width, cTrigger.Height);

                object[] data = new object[] { newsize, trigger_id_buff.ToString() };

                trigger_resize_delegate func = this.trigger_resize2;

                HistoryEntry entry = new HistoryEntry(data, tmpoldsize, HistoryAction.SetTriggerSize, func);

                history.Push(entry);

                this.MajniveauBackup();
            }

            renderPanel.ToolSelected = Tools.Pencil;
        }

        private void trigger_endrepos(object sender, MouseEventArgs e)
        {
            renderPanel.MouseMove -= this.trigger_setpos;
            renderPanel.MouseUp -= this.trigger_endrepos;

            StringBuilder trigger_id_buff = new StringBuilder(250);

            NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wtrue);

            NativeFunc.editor_entity_get_selection(trigger_id_buff, 0, trigger_id_buff.Capacity);

            Trigger cTrigger = renderPanel.CurrentLayer.TriggerArrays.Find(p => p.TriggerId == trigger_id_buff.ToString());

            Point newpos = new Point(cTrigger.Position.X, cTrigger.Position.Y);

            object[] data = new object[] { newpos, trigger_id_buff.ToString() };

            entity_setpos_delegate func = this.trigger_setpos2;

            HistoryEntry entry = new HistoryEntry(data, tmpoldpos, HistoryAction.SetPosition, func);

            history.Push(entry);

            renderPanel.ToolSelected = Tools.Pencil;

            this.MajniveauBackup();
        }

        private void redimensionnerTriggerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            renderPanel.MouseDown += this.trigger_setresize;
            renderPanel.ToolSelected = Tools.Trigger;
        }

        private void repositionnerTriggerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            renderPanel.MouseDown += this.trigger_setrepos;
            renderPanel.ToolSelected = Tools.Trigger;
        }

        private void modifierTriggerToolStripMenuItem_Click(object sender, EventArgs e)
        {

            trigDiag = new TriggerDialog(levelObj);

            StringBuilder trigger_id_buff = new StringBuilder(250);

            NativeFunc.editor_entity_get_selection(trigger_id_buff, 0, trigger_id_buff.Capacity);

            string old_id = trigger_id_buff.ToString();

            Trigger cTrigger = renderPanel.CurrentLayer.TriggerArrays.Find(p => p.TriggerId == trigger_id_buff.ToString());

            if (cTrigger == null)
                return;

            trigDiag.FillData(cTrigger);

            if (trigDiag.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                cTrigger = trigDiag.Tag as Trigger;

                if (old_id != cTrigger.TriggerId)
                {
                    history.RemoveActionFromID(old_id, HistoryAction.SetPosition);
                    history.RemoveActionFromID(old_id, HistoryAction.SetTriggerSize);
                    NativeFunc.editor_changeentityid(old_id, cTrigger.TriggerId, NativeFunc.GAME_TYPE.GT_TRIGGER);
                }

                MemoryStream triggerStream = new MemoryStream();
                triggerStream.Position = 0;
                ProtoBuf.Serializer.Serialize<Trigger>(triggerStream, cTrigger);

                triggerStream.Position = 0;


                byte[] tmpTriggerArray = triggerStream.ToArray();

                triggerStream.Close();

                NativeFunc.editor_updatetrigger(cTrigger.TriggerId, tmpTriggerArray, (uint)tmpTriggerArray.Length);

                ItemsList.SelectedNode.Text = cTrigger.TriggerId;
                ItemsList.SelectedNode.Name = cTrigger.TriggerId;

                this.MajniveauBackup();

            }

        }

        private void supprimerTriggerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StringBuilder trigger_id_buff = new StringBuilder(250);

            NativeFunc.editor_entity_get_selection(trigger_id_buff, 0, trigger_id_buff.Capacity);

            if (MessageBox.Show("Supprimer le déclencheur " + trigger_id_buff.ToString() + " ?", "Suppression déclencheur", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {

                this.SupprTrig(trigger_id_buff.ToString());
                NativeFunc.editor_entity_unselect();
                this.MajniveauBackup();
            }
        }

        public void SupprTrig(string trigger_id)
        {
            history.RemoveActionFromID(trigger_id, HistoryAction.SetPosition);
            history.RemoveActionFromID(trigger_id, HistoryAction.SetTriggerSize);

            renderPanel.CurrentLayer.TriggerArrays.RemoveAll(p => p.TriggerId == trigger_id);

            NativeFunc.editor_removeentity(trigger_id, NativeFunc.GAME_TYPE.GT_TRIGGER);

            TreeNode cnode = ItemsList.SelectedNode;

            ItemsList.Nodes["Nodetrigger"].Nodes.Remove(cnode);

        }

        #endregion

        #region gestion entity

        EntityDialog entDiag;
        Point oldpos;

        private bool IsEntityModule(string module)
        {
            Map mapObj = levelObj.MapArrays[tabMaps.SelectedIndex];

            foreach (Layer oLayer in mapObj.LayerArrays)
                if (oLayer.Type == (int)TypeLayer.Objects)
                    foreach (Entity ent in oLayer.EntityArrays)
                        if (ent.ScriptModule == module)
                            return true;

            return false;

        }

        private void btnEntities_Click(object sender, EventArgs e)
        {
            entDiag = new EntityDialog(levelObj, game_resources, renderPanel.CurrentLayer, script_modules, script_base_path);
            NativeFunc.editor_entity_unselect();

            if (entDiag.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                renderPanel.MouseDown += this.entity_addentity;
                renderPanel.ToolSelected = Tools.Entity;
                NativeFunc.editor_showcursor(NativeFunc.wbool.wtrue, NativeFunc.wbool.wfalse);
                // NativeFunc.editor_setcursor_color(1.0f, 0.0f, 0.0f, 0.5f);
            }
            else
            {
                NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wtrue);
                //NativeFunc.editor_setcursor_color(0.0f, 0.0f, 1.0f, 0.5f);
            }

        }

        private void entity_addentity(object sender, MouseEventArgs e)
        {
            int worldposx, worldposy;
            renderPanel.GetWorldPos(e.X, e.Y, out worldposx, out worldposy);

            Entity cEnt = entDiag.Tag as Entity;

            cEnt.Position = new Wvector();
            cEnt.Position.X = worldposx;
            cEnt.Position.Y = worldposy;
            cEnt.ZOrder = renderPanel.CurrentLayer.Order;

            renderPanel.CurrentLayer.EntityArrays.Add(cEnt);
            GroupEntity cgroup = null;

            if (!string.IsNullOrEmpty(cEnt.GroupId))
            {
                cgroup = renderPanel.CurrentLayer.GroupArrays.Find(p => p.GroupId == cEnt.GroupId);

                if (cgroup != null)
                {
                    if (!cgroup.IdArrays.Contains(cEnt.EntityId))
                        cgroup.IdArrays.Add(cEnt.EntityId);
                }
                else
                {
                    cgroup = new GroupEntity();
                    cgroup.GroupId = cEnt.GroupId;
                    cgroup.IdArrays.Add(cEnt.EntityId);
                    renderPanel.CurrentLayer.GroupArrays.Add(cgroup);
                }
            }

            bool textureExist = (NativeFunc.editor_resx_exist(cEnt.Tileset) == NativeFunc.wbool.wtrue);


            if (!textureExist)
                LogTextToBox("La texture " + cEnt.Tileset + " n'existe pas dans la liste de resources !");


            if (!string.IsNullOrEmpty(cEnt.AnimationFile))
            {
                bool animExist = (NativeFunc.editor_resx_exist(cEnt.AnimationFile) == NativeFunc.wbool.wtrue);


                if (!animExist && !string.IsNullOrEmpty(cEnt.StartAnimation))
                    LogTextToBox("Aucun fichier d'animation " + cEnt.AnimationFile + " n'existe pas dans la liste de resources ! l'entité " + cEnt.EntityId + " ne sera pas animée");
            }

            if (!string.IsNullOrEmpty(cEnt.ScriptModule))
            {
                this.charger_module(cEnt.ScriptModule, false);
            }

            MemoryStream entityStream = new MemoryStream();
            entityStream.Position = 0;
            ProtoBuf.Serializer.Serialize<Entity>(entityStream, cEnt);

            entityStream.Position = 0;


            byte[] tmpEntityArray = entityStream.ToArray();

            entityStream.Close();

            NativeFunc.editor_addentity(cEnt.EntityId, tmpEntityArray, (uint)tmpEntityArray.Length, renderPanel.CurrentMap.MapId);
            NativeFunc.editor_selectentity(cEnt.EntityId, NativeFunc.GAME_TYPE.GT_ENTITY, NativeFunc.wbool.wfalse);

            if (!string.IsNullOrEmpty(cEnt.ScriptCtor))
            {
                StringBuilder str = new StringBuilder(10000);


                NativeFunc.editor_exec_string(ScriptUtility.genconstruct(cEnt), str, str.Capacity);

                if (str.Length == 0)
                    LogTextToBox("[LUA] Entity " + cEnt.EntityId + " créé");
                else
                    LogTextToBox(str.ToString());


            }

            TreeNode cnode = new TreeNode();
            cnode.Text = cEnt.EntityId;
            cnode.Tag = cEnt;
            cnode.Name = cEnt.EntityId;

            if (string.IsNullOrEmpty(cEnt.GroupId))
            {
                ItemsList.Nodes["Nodeentity"].Nodes.Add(cnode);
            }
            else
            {
                if (!ItemsList.Nodes["Nodeentity"].Nodes.ContainsKey("group_" + cEnt.GroupId))
                {
                    TreeNode group_node = new TreeNode();
                    group_node.Text = cgroup.GroupId;
                    group_node.Tag = cgroup;
                    group_node.Name = "group_" + cgroup.GroupId;

                    ItemsList.Nodes["Nodeentity"].Nodes.Add(group_node);
                }

                ItemsList.Nodes["Nodeentity"].Nodes["group_" + cEnt.GroupId].Nodes.Add(cnode);
            }


            ItemsList.SelectedNode = cnode;

            renderPanel.MouseDown -= this.entity_addentity;

            renderPanel.ToolSelected = Tools.Pencil;

            NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wtrue);
            // NativeFunc.editor_setcursor_color(0.0f, 0.0f, 1.0f, 0.5f);
            this.MajniveauBackup();
        }

        private void entity_setrepos(object sender, MouseEventArgs e)
        {
            StringBuilder entity_id_buff = new StringBuilder(250);

            NativeFunc.editor_entity_get_selection(entity_id_buff, 0, entity_id_buff.Capacity);

            Entity cEntity = renderPanel.CurrentLayer.EntityArrays.Find(p => p.EntityId == entity_id_buff.ToString());

            oldpos = new Point(cEntity.Position.X, cEntity.Position.Y);

            NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wfalse);

            entity_setpos(sender, e);
            renderPanel.MouseDown -= this.entity_setrepos;
            renderPanel.MouseMove += this.entity_setpos;
            renderPanel.MouseUp += this.entity_endrepos;
        }

        private void focusEntitéToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Map mapObj = levelObj.MapArrays[tabMaps.SelectedIndex];

            int[] array_position = new int[2];

            StringBuilder entity_id_buff = new StringBuilder(250);

            NativeFunc.editor_entity_get_selection(entity_id_buff, 0, entity_id_buff.Capacity);

            Entity cEntity = renderPanel.CurrentLayer.EntityArrays.Find(p => p.EntityId == entity_id_buff.ToString());

            if (cEntity != null)
            {
                NativeFunc.editor_set_scroll((float)cEntity.Position.X, (float)cEntity.Position.Y);
                mapObj.EditorPos.X = cEntity.Position.X;
                mapObj.EditorPos.Y = cEntity.Position.Y;
            }
        }

        private void entity_setpos(object sender, MouseEventArgs e)
        {
            int worldposx, worldposy;
            renderPanel.GetWorldPos(e.X, e.Y, out worldposx, out worldposy, Settings.Default.snap_to_grid);

            StringBuilder entity_id_buff = new StringBuilder(250);

            NativeFunc.editor_entity_get_selection(entity_id_buff, 0, entity_id_buff.Capacity);

            Entity cEntity = renderPanel.CurrentLayer.EntityArrays.Find(p => p.EntityId == entity_id_buff.ToString());

            if (Settings.Default.snap_to_grid)
            {
                worldposx += (int)(cEntity.Width * 0.5f);
                worldposy += (int)(cEntity.Height * 0.5f);
            }

            cEntity.Position = new Wvector();
            cEntity.Position.X = worldposx;
            cEntity.Position.Y = worldposy;

            NativeFunc.editor_entity_update_pos(cEntity.EntityId, cEntity.Position.X, cEntity.Position.Y, NativeFunc.GAME_TYPE.GT_ENTITY);
        }

        private void entity_setpos2(Point newpos, string entity_id)
        {
            Entity cEntity = renderPanel.CurrentLayer.EntityArrays.Find(p => p.EntityId == entity_id);

            cEntity.Position = new Wvector();
            cEntity.Position.X = newpos.X;
            cEntity.Position.Y = newpos.Y;

            NativeFunc.editor_entity_update_pos(cEntity.EntityId, cEntity.Position.X, cEntity.Position.Y, NativeFunc.GAME_TYPE.GT_ENTITY);
            this.MajniveauBackup();
        }

        private void entity_endrepos(object sender, MouseEventArgs e)
        {
            renderPanel.MouseMove -= this.entity_setpos;
            renderPanel.MouseUp -= this.entity_endrepos;

            NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wtrue);

            StringBuilder entity_id_buff = new StringBuilder(250);

            NativeFunc.editor_entity_get_selection(entity_id_buff, 0, entity_id_buff.Capacity);

            Entity cEntity = renderPanel.CurrentLayer.EntityArrays.Find(p => p.EntityId == entity_id_buff.ToString());

            Point newpos = new Point(cEntity.Position.X, cEntity.Position.Y);

            object[] hist_data = new object[] { newpos, entity_id_buff.ToString() };

            entity_setpos_delegate func = this.entity_setpos2;

            HistoryEntry entry = new HistoryEntry(hist_data, oldpos, HistoryAction.SetPosition, func);

            history.Push(entry);

            this.MajniveauBackup();

            renderPanel.ToolSelected = Tools.Pencil;
        }


        private void repositionnerEntityToolStripMenuItem_Click(object sender, EventArgs e)
        {
            renderPanel.MouseDown += this.entity_setrepos;
            renderPanel.ToolSelected = Tools.Entity;
        }

        private void modifierEntityToolStripMenuItem_Click(object sender, EventArgs e)
        {
            entDiag = new EntityDialog(levelObj, game_resources, renderPanel.CurrentLayer, script_modules, script_base_path);

            StringBuilder entity_id_buff = new StringBuilder(250);

            NativeFunc.editor_entity_get_selection(entity_id_buff, 0, entity_id_buff.Capacity);

            Entity ent_data = renderPanel.CurrentLayer.EntityArrays.Find(p => p.EntityId == entity_id_buff.ToString());

            if (ent_data == null)
                return;

            entDiag.FillData(ent_data);

            string old_module = ent_data.ScriptModule;
            string old_creation = ent_data.ScriptCtor;
            string old_group = ent_data.GroupId;
            bool oldscriptonly = ent_data.ScriptInitOnly;

            if (entDiag.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                //check first if our linked resources exist

                bool textureExist = (NativeFunc.editor_resx_exist(ent_data.Tileset) == NativeFunc.wbool.wtrue);


                if (!textureExist)
                    LogTextToBox("La texture " + ent_data.Tileset + " n'existe pas dans la liste de resources !");


                if (!string.IsNullOrEmpty(ent_data.AnimationFile))
                {
                    bool animExist = (NativeFunc.editor_resx_exist(ent_data.AnimationFile) == NativeFunc.wbool.wtrue);


                    if (!animExist && !string.IsNullOrEmpty(ent_data.StartAnimation))
                        LogTextToBox("Aucun fichier d'animation " + ent_data.StartAnimation + " n'existe pas dans la liste de resources ! l'entité " + ent_data.EntityId + " ne sera pas animée");
                }

                if (!string.IsNullOrEmpty(ent_data.ScriptModule) && old_module != ent_data.ScriptModule)
                {
                    this.charger_module(ent_data.ScriptModule, true);
                }

                //check if we need group update

                //change npc data if needed
                if (entity_id_buff.ToString() != ent_data.EntityId)
                {
                    history.RemoveActionFromID(entity_id_buff.ToString(), HistoryAction.SetPosition);
                    NativeFunc.editor_changeentityid(entity_id_buff.ToString(), ent_data.EntityId, NativeFunc.GAME_TYPE.GT_ENTITY);

                    ItemsList.SelectedNode.Text = ent_data.EntityId;
                    ItemsList.SelectedNode.Name = ent_data.EntityId;
                }

                if (old_group != ent_data.GroupId)
                {
                    TreeNode cnode = ItemsList.SelectedNode;

                    //maj treeview
                    if (!string.IsNullOrEmpty(old_group))
                    {
                        ItemsList.Nodes["Nodeentity"].Nodes["group_" + old_group].Nodes.Remove(cnode);

                        if (ItemsList.Nodes["Nodeentity"].Nodes["group_" + old_group].Nodes.Count == 0)
                            ItemsList.Nodes["Nodeentity"].Nodes.Remove(ItemsList.Nodes["Nodeentity"].Nodes["group_" + old_group]);
                    }
                    else
                    {
                        ItemsList.Nodes["Nodeentity"].Nodes.Remove(cnode);
                    }

                    if (string.IsNullOrEmpty(ent_data.GroupId))
                    {
                        ItemsList.Nodes["Nodeentity"].Nodes.Add(cnode);
                    }
                    else
                    {
                        if (!ItemsList.Nodes["Nodeentity"].Nodes.ContainsKey("group_" + ent_data.GroupId))
                        {
                            TreeNode groupnode = new TreeNode();
                            groupnode.Text = ent_data.GroupId;
                            groupnode.Name = "group_" + ent_data.GroupId;
                            groupnode.Tag = renderPanel.CurrentLayer.GroupArrays.Find(p => p.GroupId == ent_data.GroupId);

                            ItemsList.Nodes["Nodeentity"].Nodes.Add(groupnode);
                        }

                        ItemsList.Nodes["Nodeentity"].Nodes["group_" + ent_data.GroupId].Nodes.Add(cnode);
                    }
                }

                MemoryStream entStream = new MemoryStream();
                entStream.Position = 0;
                ProtoBuf.Serializer.Serialize<Entity>(entStream, ent_data);

                entStream.Position = 0;

                byte[] tmpentArray = entStream.ToArray();

                entStream.Close();

                //remove ant existing lua data
                if (!string.IsNullOrEmpty(ent_data.ScriptCtor) && old_creation != ent_data.ScriptCtor && !oldscriptonly)
                {
                    StringBuilder str = new StringBuilder(10000);

                    string suppr_func = "remove_entity('" + ent_data.EntityId + "')";

                    NativeFunc.editor_exec_string(suppr_func, str, str.Capacity);

                    if (str.Length == 0)
                        LogTextToBox("[LUA] entité" + ent_data.EntityId + " supprimée");
                    else
                        LogTextToBox(str.ToString());
                }

                NativeFunc.editor_updateentity(ent_data.EntityId, tmpentArray, (uint)tmpentArray.Length);

                if (!string.IsNullOrEmpty(ent_data.ScriptCtor) && old_creation != ent_data.ScriptCtor)
                {
                    StringBuilder str = new StringBuilder(10000);

                    NativeFunc.editor_exec_string(ScriptUtility.genconstruct(ent_data), str, str.Capacity);

                    if (str.Length == 0)
                        LogTextToBox("[LUA] entité" + ent_data.EntityId + " recréé");
                    else
                        LogTextToBox(str.ToString());


                }

                this.MajniveauBackup();

            }
        }

        private void supprimerEntityToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StringBuilder entity_id_buff = new StringBuilder(250);

            NativeFunc.editor_entity_get_selection(entity_id_buff, 0, entity_id_buff.Capacity);

            if (MessageBox.Show("Supprimer l'entité " + entity_id_buff.ToString() + " ?", "Suppression entité", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {

                this.SupprEnt(entity_id_buff.ToString());
                NativeFunc.editor_entity_unselect();
                this.MajniveauBackup();
            }


        }

        private void SupprEnt(string entity_id)
        {
            history.RemoveActionFromID(entity_id, HistoryAction.SetPosition);
            //suppression objet script
            Entity entity_data = renderPanel.CurrentLayer.EntityArrays.Find(p => p.EntityId == entity_id);

            string delete_func = "remove_entity('" + entity_id + "')";

            if (!string.IsNullOrEmpty(entity_data.ScriptCtor) && !entity_data.ScriptInitOnly)
            {
                StringBuilder str = new StringBuilder(10000);
                NativeFunc.editor_exec_string(delete_func, str, str.Capacity);

                if (str.Length == 0)
                    LogTextToBox("[LUA] entité " + entity_data.EntityId + " supprimé");
                else
                    LogTextToBox(str.ToString());


            }

            if (!string.IsNullOrEmpty(entity_data.GroupId))
            {
                GroupEntity grp = renderPanel.CurrentLayer.GroupArrays.Find(p => p.GroupId == entity_data.GroupId);
                grp.IdArrays.Remove(entity_data.EntityId);

                if (grp.IdArrays.Count == 0)
                    renderPanel.CurrentLayer.GroupArrays.Remove(grp);
            }


            //suppression objet editeur
            renderPanel.CurrentLayer.EntityArrays.RemoveAll(p => p.EntityId == entity_id);

            //suppression objet moteur
            NativeFunc.editor_removeentity(entity_id, NativeFunc.GAME_TYPE.GT_ENTITY);

            TreeNode[] cnode = ItemsList.Nodes.Find(entity_data.EntityId, true);

            if (!string.IsNullOrEmpty(entity_data.GroupId))
            {
                ItemsList.Nodes["Nodeentity"].Nodes["group_" + entity_data.GroupId].Nodes.Remove(cnode[0]);

                if (ItemsList.Nodes["Nodeentity"].Nodes["group_" + entity_data.GroupId].Nodes.Count == 0)
                    ItemsList.Nodes["Nodeentity"].Nodes.Remove(ItemsList.Nodes["Nodeentity"].Nodes["group_" + entity_data.GroupId]);
            }
            else
            {
                ItemsList.Nodes["Nodeentity"].Nodes.Remove(cnode[0]);
            }
        }

        private void recréerObjetScriptToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StringBuilder entity_id_buff = new StringBuilder(250);

            NativeFunc.editor_entity_get_selection(entity_id_buff, 0, entity_id_buff.Capacity);

            Entity ent_data = renderPanel.CurrentLayer.EntityArrays.Find(p => p.EntityId == entity_id_buff.ToString());

            if (ent_data == null)
                return;

            //remove then add entity
            StringBuilder str = new StringBuilder(10000);

            if (!ent_data.ScriptInitOnly)
            {
                string delete_func = "remove_entity('" + ent_data.EntityId + "')";

                NativeFunc.editor_exec_string(delete_func, str, str.Capacity);
            }


            NativeFunc.editor_exec_string(ScriptUtility.genconstruct(ent_data), str, str.Capacity);

            if (str.Length == 0)
                LogTextToBox("[LUA] entité " + ent_data.EntityId + " recréé");
            else
                LogTextToBox(str.ToString());


        }


        #endregion

        #region gestion text entities

        TextDialog txtDiag;
        Point oldpos_text;

        private bool IsTextEntityModule(string module)
        {
            Map mapObj = levelObj.MapArrays[tabMaps.SelectedIndex];

            foreach (Layer oLayer in mapObj.LayerArrays)
                if (oLayer.Type == (int)TypeLayer.Objects)
                    foreach (TextObject ent in oLayer.TextArrays)
                        if (ent.ScriptModule == module)
                            return true;

            return false;
        }

        private void btnTexte_Click(object sender, EventArgs e)
        {
            txtDiag = new TextDialog(levelObj, game_resources, script_modules, script_base_path);
            NativeFunc.editor_entity_unselect();

            if (txtDiag.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                renderPanel.MouseDown += this.txtentity_addentity;
                renderPanel.ToolSelected = Tools.Entity;
                NativeFunc.editor_showcursor(NativeFunc.wbool.wtrue, NativeFunc.wbool.wfalse);
                //  NativeFunc.editor_setcursor_color(1.0f, 0.0f, 0.0f, 0.5f);
            }
            else
            {
                NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wtrue);
                //NativeFunc.editor_setcursor_color(0.0f, 0.0f, 1.0f, 0.5f);
            }
        }

        private void txtentity_addentity(object sender, MouseEventArgs e)
        {
            int worldposx, worldposy;
            renderPanel.GetWorldPos(e.X, e.Y, out worldposx, out worldposy);

            TextObject cEnt = txtDiag.Tag as TextObject;

            cEnt.Position = new Wvector();
            cEnt.Position.X = worldposx;
            cEnt.Position.Y = worldposy;
            cEnt.ZOrder = renderPanel.CurrentLayer.Order;

            renderPanel.CurrentLayer.TextArrays.Add(cEnt);


            if (!string.IsNullOrEmpty(cEnt.ScriptModule))
            {
                this.charger_module(cEnt.ScriptModule, false);
            }

            MemoryStream entityStream = new MemoryStream();
            entityStream.Position = 0;
            ProtoBuf.Serializer.Serialize<TextObject>(entityStream, cEnt);

            entityStream.Position = 0;


            byte[] tmpEntityArray = entityStream.ToArray();

            entityStream.Close();

            NativeFunc.editor_addtextentity(cEnt.TextId, tmpEntityArray, (uint)tmpEntityArray.Length, renderPanel.CurrentMap.MapId);
            NativeFunc.editor_selectentity(cEnt.TextId, NativeFunc.GAME_TYPE.GT_TEXT, NativeFunc.wbool.wfalse);

            if (!string.IsNullOrEmpty(cEnt.ScriptCtor))
            {
                StringBuilder str = new StringBuilder(10000);

                NativeFunc.editor_exec_string(ScriptUtility.gentextconstruct(cEnt), str, str.Capacity);

                if (str.Length == 0)
                    LogTextToBox("[LUA] texte " + cEnt.TextId + " créé");
                else
                    LogTextToBox(str.ToString());


            }

            TreeNode cnode = new TreeNode();
            cnode.Text = cEnt.TextId;
            cnode.Tag = cEnt;
            cnode.Name = cEnt.TextId;

            ItemsList.Nodes["NodeText"].Nodes.Add(cnode);
            ItemsList.SelectedNode = cnode;


            renderPanel.MouseDown -= this.txtentity_addentity;

            renderPanel.ToolSelected = Tools.Pencil;
            NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wtrue);
            // NativeFunc.editor_setcursor_color(0.0f, 0.0f, 1.0f, 0.5f);

            this.MajniveauBackup();
        }

        private void textentity_setrepos(object sender, MouseEventArgs e)
        {
            StringBuilder entity_id_buff = new StringBuilder(250);

            NativeFunc.editor_entity_get_selection(entity_id_buff, 0, entity_id_buff.Capacity);

            TextObject cEntity = renderPanel.CurrentLayer.TextArrays.Find(p => p.TextId == entity_id_buff.ToString());

            oldpos_text = new Point(cEntity.Position.X, cEntity.Position.Y);

            NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wfalse);

            textentity_setpos(sender, e);
            renderPanel.MouseDown -= this.textentity_setrepos;
            renderPanel.MouseMove += this.textentity_setpos;
            renderPanel.MouseUp += this.textentity_endrepos;
        }

        private void textentity_endrepos(object sender, MouseEventArgs e)
        {
            renderPanel.MouseMove -= this.textentity_setpos;
            renderPanel.MouseUp -= this.textentity_endrepos;

            StringBuilder entity_id_buff = new StringBuilder(250);

            NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wtrue);

            NativeFunc.editor_entity_get_selection(entity_id_buff, 0, entity_id_buff.Capacity);

            TextObject cEntity = renderPanel.CurrentLayer.TextArrays.Find(p => p.TextId == entity_id_buff.ToString());

            Point newpos = new Point(cEntity.Position.X, cEntity.Position.Y);

            object[] hist_data = new object[] { newpos, entity_id_buff.ToString() };

            entity_setpos_delegate func = this.textentity_setpos2;

            HistoryEntry entry = new HistoryEntry(hist_data, oldpos_text, HistoryAction.SetPosition, func);

            history.Push(entry);

            this.MajniveauBackup();

            renderPanel.ToolSelected = Tools.Pencil;
        }


        private void textentity_setpos(object sender, MouseEventArgs e)
        {
            int worldposx, worldposy;
            renderPanel.GetWorldPos(e.X, e.Y, out worldposx, out worldposy, Settings.Default.snap_to_grid);

            StringBuilder entity_id_buff = new StringBuilder(250);

            NativeFunc.editor_entity_get_selection(entity_id_buff, 0, entity_id_buff.Capacity);

            TextObject cEntity = renderPanel.CurrentLayer.TextArrays.Find(p => p.TextId == entity_id_buff.ToString());

            if (cEntity == null)
                return;

            int[] render_box_size = new int[2];
            float[] render_box_pos = new float[2];

            NativeFunc.editor_entity_getrenderbox(entity_id_buff.ToString(), NativeFunc.GAME_TYPE.GT_TEXT, render_box_size, render_box_pos);

            if (Settings.Default.snap_to_grid)
            {
                // worldposx += (int)(render_box_size[0] * 0.5f);
                //(int)(cEntity.width * 0.5f);
                // worldposy += (int)(render_box_size[1] * 0.5f);
            }

            cEntity.Position = new Wvector();
            cEntity.Position.X = worldposx;
            cEntity.Position.Y = worldposy;

            NativeFunc.editor_entity_update_pos(cEntity.TextId, cEntity.Position.X, cEntity.Position.Y, NativeFunc.GAME_TYPE.GT_TEXT);
        }

        private void textentity_setpos2(Point pos, string text_entity_id)
        {
            TextObject cEntity = renderPanel.CurrentLayer.TextArrays.Find(p => p.TextId == text_entity_id);

            if (cEntity == null)
                return;

            cEntity.Position = new Wvector();
            cEntity.Position.X = pos.X;
            cEntity.Position.Y = pos.Y;

            NativeFunc.editor_entity_update_pos(cEntity.TextId, cEntity.Position.X, cEntity.Position.Y, NativeFunc.GAME_TYPE.GT_TEXT);
        }



        private void txt_obj_repos_Click(object sender, EventArgs e)
        {
            renderPanel.MouseDown += this.textentity_setrepos;
            renderPanel.ToolSelected = Tools.Entity;
        }

        private void txt_obj_update_Click(object sender, EventArgs e)
        {
            txtDiag = new TextDialog(levelObj, game_resources, script_modules, script_base_path);

            StringBuilder entity_id_buff = new StringBuilder(250);

            NativeFunc.editor_entity_get_selection(entity_id_buff, 0, entity_id_buff.Capacity);

            TextObject ent_data = renderPanel.CurrentLayer.TextArrays.Find(p => p.TextId == entity_id_buff.ToString());

            if (ent_data == null)
                return;

            txtDiag.FillData(ent_data);

            string old_module = ent_data.ScriptModule;
            string old_creation = ent_data.ScriptCtor;
            bool old_scriptonly = ent_data.ScriptInitOnly;

            if (txtDiag.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {

                if (!string.IsNullOrEmpty(ent_data.ScriptModule) && old_module != ent_data.ScriptModule)
                {
                    this.charger_module(ent_data.ScriptModule, true);
                }

                if (!string.IsNullOrEmpty(old_creation) && !old_scriptonly)
                {
                    StringBuilder str1 = new StringBuilder(1000);

                    string remove_func = "remove_text('" + ent_data.TextId + "')";

                    NativeFunc.editor_exec_string(remove_func, str1, str1.Capacity);
                }


                //change npc data if needed
                if (entity_id_buff.ToString() != ent_data.TextId)
                {
                    history.RemoveActionFromID(entity_id_buff.ToString(), HistoryAction.SetPosition);
                    NativeFunc.editor_changeentityid(entity_id_buff.ToString(), ent_data.TextId, NativeFunc.GAME_TYPE.GT_TEXT);
                }

                MemoryStream entStream = new MemoryStream();
                entStream.Position = 0;
                ProtoBuf.Serializer.Serialize<TextObject>(entStream, ent_data);

                entStream.Position = 0;

                byte[] tmpentArray = entStream.ToArray();

                entStream.Close();

                NativeFunc.editor_updatetextentity(ent_data.TextId, tmpentArray, (uint)tmpentArray.Length, renderPanel.CurrentMap.MapId);

                ItemsList.SelectedNode.Text = ent_data.TextId;
                ItemsList.SelectedNode.Name = ent_data.TextId;

                if (!string.IsNullOrEmpty(ent_data.ScriptCtor))
                {
                    StringBuilder str = new StringBuilder(10000);

                    NativeFunc.editor_exec_string(ScriptUtility.gentextconstruct(ent_data), str, str.Capacity);

                    if (str.Length == 0)
                        LogTextToBox("[LUA] texte " + ent_data.TextId + " recréé");
                    else
                        LogTextToBox(str.ToString());
                }

                this.MajniveauBackup();

            }
        }

        private void txt_obj_suppr_Click(object sender, EventArgs e)
        {
            StringBuilder entity_id_buff = new StringBuilder(250);

            NativeFunc.editor_entity_get_selection(entity_id_buff, 0, entity_id_buff.Capacity);

            if (MessageBox.Show("Supprimer l'entité texte " + entity_id_buff.ToString() + " ?", "Suppression entité texte", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {

                this.SupprTxt(entity_id_buff.ToString());
                NativeFunc.editor_entity_unselect();
                this.MajniveauBackup();
            }

        }

        private void SupprTxt(string txt_id)
        {
            history.RemoveActionFromID(txt_id, HistoryAction.SetPosition);
            renderPanel.CurrentLayer.TextArrays.RemoveAll(p => p.TextId == txt_id);

            NativeFunc.editor_removeentity(txt_id, NativeFunc.GAME_TYPE.GT_TEXT);

            TreeNode[] cnode = ItemsList.Nodes.Find(txt_id, true);

            ItemsList.Nodes["NodeText"].Nodes.Remove(cnode[0]);
        }

        private void txt_obj_script_Click(object sender, EventArgs e)
        {
            StringBuilder entity_id_buff = new StringBuilder(250);

            NativeFunc.editor_entity_get_selection(entity_id_buff, 0, entity_id_buff.Capacity);

            TextObject ent_data = renderPanel.CurrentLayer.TextArrays.Find(p => p.TextId == entity_id_buff.ToString());

            if (ent_data == null)
                return;

            StringBuilder str = new StringBuilder(10000);

            if (!ent_data.ScriptInitOnly)
            {
                string delete_func = "remove_text('" + ent_data.TextId + "')";

                NativeFunc.editor_exec_string(delete_func, str, str.Capacity);
            }

            NativeFunc.editor_exec_string(ScriptUtility.gentextconstruct(ent_data), str, str.Capacity);

            if (str.Length == 0)
                LogTextToBox("[LUA] texte " + ent_data.TextId + " recréé");
            else
                LogTextToBox(str.ToString());


        }




        #endregion

        #region gestion entités son

        SoundSourceDialog sndDiag;
        Point oldpos_sound;

        private void btnSon_Click(object sender, EventArgs e)
        {
            sndDiag = new SoundSourceDialog(levelObj, game_resources);


            if (sndDiag.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                renderPanel.MouseDown += this.sndsource_add;
                renderPanel.ToolSelected = Tools.Entity;
                NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wfalse);
            }
            else
            {
                NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wtrue);
            }
        }

        private void sndsource_add(object sender, MouseEventArgs e)
        {
            int worldposx, worldposy;
            renderPanel.GetWorldPos(e.X, e.Y, out worldposx, out worldposy);

            SoundSource cEnt = sndDiag.Tag as SoundSource;

            cEnt.Position = new Wvector();
            cEnt.Position.X = worldposx;
            cEnt.Position.Y = worldposy;

            renderPanel.CurrentLayer.SoundArrays.Add(cEnt);


            MemoryStream entityStream = new MemoryStream();
            entityStream.Position = 0;
            ProtoBuf.Serializer.Serialize<SoundSource>(entityStream, cEnt);

            entityStream.Position = 0;


            byte[] tmpEntityArray = entityStream.ToArray();

            entityStream.Close();

            NativeFunc.editor_addsound(cEnt.SoundId, tmpEntityArray, (uint)tmpEntityArray.Length, renderPanel.CurrentMap.MapId, renderPanel.CurrentLayer.Order);

            TreeNode cnode = new TreeNode();
            cnode.Text = cEnt.SoundId;
            cnode.Tag = cEnt;
            cnode.Name = cEnt.SoundId;

            ItemsList.Nodes["NodeSound"].Nodes.Add(cnode);
            ItemsList.SelectedNode = cnode;


            renderPanel.MouseDown -= this.sndsource_add;

            renderPanel.ToolSelected = Tools.Pencil;
            NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wtrue);

            this.MajniveauBackup();
        }

        private void snd_modif_Click(object sender, EventArgs e)
        {
            sndDiag = new SoundSourceDialog(levelObj, game_resources);

            StringBuilder entity_id_buff = new StringBuilder(250);

            NativeFunc.editor_entity_get_selection(entity_id_buff, 0, entity_id_buff.Capacity);

            SoundSource sounddata = renderPanel.CurrentLayer.SoundArrays.Find(p => p.SoundId == entity_id_buff.ToString());

            if (sounddata == null)
                return;

            sndDiag.FillData(sounddata);

            if (sndDiag.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                SoundSource ent_data = sndDiag.Tag as SoundSource;

                //change id if needed
                if (entity_id_buff.ToString() != ent_data.SoundId)
                {
                    history.RemoveActionFromID(entity_id_buff.ToString(), HistoryAction.SetPosition);
                    NativeFunc.editor_changeentityid(entity_id_buff.ToString(), ent_data.SoundId, NativeFunc.GAME_TYPE.GT_SOUND);
                }

                MemoryStream entStream = new MemoryStream();
                entStream.Position = 0;
                ProtoBuf.Serializer.Serialize<SoundSource>(entStream, ent_data);

                entStream.Position = 0;

                byte[] tmpentArray = entStream.ToArray();

                entStream.Close();

                NativeFunc.editor_updatesound(ent_data.SoundId, tmpentArray, (uint)tmpentArray.Length);

                ItemsList.SelectedNode.Text = ent_data.SoundId;
                ItemsList.SelectedNode.Name = ent_data.SoundId;

            }
        }

        private void snd_repos_Click(object sender, EventArgs e)
        {
            renderPanel.MouseDown += this.sndsource_setrepos;
            renderPanel.ToolSelected = Tools.Entity;
        }

        private void sndsource_setrepos(object sender, MouseEventArgs e)
        {
            StringBuilder entity_id_buff = new StringBuilder(250);

            NativeFunc.editor_entity_get_selection(entity_id_buff, 0, entity_id_buff.Capacity);

            SoundSource cEntity = renderPanel.CurrentLayer.SoundArrays.Find(p => p.SoundId == entity_id_buff.ToString());

            oldpos_sound = new Point(cEntity.Position.X, cEntity.Position.Y);

            NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wfalse);

            sndsource_setpos(sender, e);
            renderPanel.MouseDown -= this.sndsource_setrepos;
            renderPanel.MouseMove += this.sndsource_setpos;
            renderPanel.MouseUp += this.soundsource_endrepos;
        }

        private void sndsource_setpos(object sender, MouseEventArgs e)
        {
            int worldposx, worldposy;
            renderPanel.GetWorldPos(e.X, e.Y, out worldposx, out worldposy, Settings.Default.snap_to_grid);

            StringBuilder entity_id_buff = new StringBuilder(250);

            NativeFunc.editor_entity_get_selection(entity_id_buff, 0, entity_id_buff.Capacity);

            SoundSource cEntity = renderPanel.CurrentLayer.SoundArrays.Find(p => p.SoundId == entity_id_buff.ToString());

            if (cEntity == null)
                return;

            cEntity.Position = new Wvector();
            cEntity.Position.X = worldposx;
            cEntity.Position.Y = worldposy;

            NativeFunc.editor_updatesoundposition(cEntity.SoundId, (float)cEntity.Position.X, (float)cEntity.Position.Y);
        }

        private void sndsource_setpos2(Point pos, string sound_entity_id)
        {
            SoundSource cEntity = renderPanel.CurrentLayer.SoundArrays.Find(p => p.SoundId == sound_entity_id);

            if (cEntity == null)
                return;

            cEntity.Position = new Wvector();
            cEntity.Position.X = pos.X;
            cEntity.Position.Y = pos.Y;

            NativeFunc.editor_updatesoundposition(cEntity.SoundId, cEntity.Position.X, cEntity.Position.Y);

            this.MajniveauBackup();
        }

        private void soundsource_endrepos(object sender, MouseEventArgs e)
        {
            renderPanel.MouseMove -= this.sndsource_setpos;
            renderPanel.MouseUp -= this.soundsource_endrepos;

            StringBuilder entity_id_buff = new StringBuilder(250);

            NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wtrue);

            NativeFunc.editor_entity_get_selection(entity_id_buff, 0, entity_id_buff.Capacity);

            SoundSource cEntity = renderPanel.CurrentLayer.SoundArrays.Find(p => p.SoundId == entity_id_buff.ToString());

            Point newpos = new Point(cEntity.Position.X, cEntity.Position.Y);

            object[] hist_data = new object[] { newpos, entity_id_buff.ToString() };

            entity_setpos_delegate func = this.sndsource_setpos2;

            HistoryEntry entry = new HistoryEntry(hist_data, oldpos_sound, HistoryAction.SetPosition, func);

            history.Push(entry);

            this.MajniveauBackup();

            renderPanel.ToolSelected = Tools.Pencil;
        }


        private void snd_suppr_Click(object sender, EventArgs e)
        {
            StringBuilder entity_id_buff = new StringBuilder(250);

            NativeFunc.editor_entity_get_selection(entity_id_buff, 0, entity_id_buff.Capacity);

            if (MessageBox.Show("Supprimer la source sonore " + entity_id_buff.ToString() + " ?", "Suppression source sonore", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {

                this.SupprSound(entity_id_buff.ToString());
                NativeFunc.editor_entity_unselect();
                this.MajniveauBackup();
            }
        }

        private void SupprSound(string snd_id)
        {
            history.RemoveActionFromID(snd_id, HistoryAction.SetPosition);
            renderPanel.CurrentLayer.SoundArrays.RemoveAll(p => p.SoundId == snd_id);

            NativeFunc.editor_removesound(snd_id);

            TreeNode[] cnode = ItemsList.Nodes.Find(snd_id, true);

            ItemsList.Nodes["NodeSound"].Nodes.Remove(cnode[0]);
        }


        #endregion

        #region gestion entités lumière

        LightSourceDialog lightDiag;
        Point oldpos_light;

        private void btnLight_Click(object sender, EventArgs e)
        {
            lightDiag = new LightSourceDialog(levelObj);

            if (lightDiag.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                renderPanel.MouseDown += this.lightsource_add;
                renderPanel.ToolSelected = Tools.Entity;
                NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wfalse);
            }
            else
            {
                NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wtrue);
            }
        }

        private void lightsource_add(object sender, MouseEventArgs e)
        {
            int worldposx, worldposy;
            renderPanel.GetWorldPos(e.X, e.Y, out worldposx, out worldposy);

            LightSource cEnt = lightDiag.Tag as LightSource;

            cEnt.Position = new Wvector();
            cEnt.Position.X = worldposx;
            cEnt.Position.Y = worldposy;

            renderPanel.CurrentLayer.LightArrays.Add(cEnt);


            MemoryStream entityStream = new MemoryStream();
            entityStream.Position = 0;
            ProtoBuf.Serializer.Serialize<LightSource>(entityStream, cEnt);

            entityStream.Position = 0;


            byte[] tmpEntityArray = entityStream.ToArray();

            entityStream.Close();

            NativeFunc.editor_addlight(cEnt.LightId, tmpEntityArray, (uint)tmpEntityArray.Length, renderPanel.CurrentMap.MapId, renderPanel.CurrentLayer.Order);

            TreeNode cnode = new TreeNode();
            cnode.Text = cEnt.LightId;
            cnode.Tag = cEnt;
            cnode.Name = cEnt.LightId;

            ItemsList.Nodes["NodeLight"].Nodes.Add(cnode);
            ItemsList.SelectedNode = cnode;


            renderPanel.MouseDown -= this.lightsource_add;

            renderPanel.ToolSelected = Tools.Pencil;
            NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wtrue);

            this.MajniveauBackup();

        }

        private void light_repos_Click(object sender, EventArgs e)
        {
            renderPanel.MouseDown += this.lightsource_setrepos;
            renderPanel.ToolSelected = Tools.Entity;
        }

        private void lightsource_setrepos(object sender, MouseEventArgs e)
        {
            StringBuilder entity_id_buff = new StringBuilder(250);

            NativeFunc.editor_entity_get_selection(entity_id_buff, 0, entity_id_buff.Capacity);

            LightSource cEntity = renderPanel.CurrentLayer.LightArrays.Find(p => p.LightId == entity_id_buff.ToString());

            oldpos_light = new Point(cEntity.Position.X, cEntity.Position.Y);

            NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wfalse);

            lightsource_setpos(sender, e);
            renderPanel.MouseDown -= this.lightsource_setrepos;
            renderPanel.MouseMove += this.lightsource_setpos;
            renderPanel.MouseUp += this.lightsource_endrepos;
        }

        private void lightsource_setpos(object sender, MouseEventArgs e)
        {
            int worldposx, worldposy;
            renderPanel.GetWorldPos(e.X, e.Y, out worldposx, out worldposy, Settings.Default.snap_to_grid);

            StringBuilder entity_id_buff = new StringBuilder(250);

            NativeFunc.editor_entity_get_selection(entity_id_buff, 0, entity_id_buff.Capacity);

            LightSource cEntity = renderPanel.CurrentLayer.LightArrays.Find(p => p.LightId == entity_id_buff.ToString());

            if (cEntity == null)
                return;

            cEntity.Position = new Wvector();
            cEntity.Position.X = worldposx;
            cEntity.Position.Y = worldposy;

            NativeFunc.editor_updatelightposition(cEntity.LightId, (float)cEntity.Position.X, (float)cEntity.Position.Y);
        }

        private void lightsource_setpos2(Point pos, string sound_entity_id)
        {
            LightSource cEntity = renderPanel.CurrentLayer.LightArrays.Find(p => p.LightId == sound_entity_id);

            if (cEntity == null)
                return;

            cEntity.Position = new Wvector();
            cEntity.Position.X = pos.X;
            cEntity.Position.Y = pos.Y;

            NativeFunc.editor_updatelightposition(cEntity.LightId, cEntity.Position.X, cEntity.Position.Y);

            this.MajniveauBackup();
        }

        private void lightsource_endrepos(object sender, MouseEventArgs e)
        {
            renderPanel.MouseMove -= this.lightsource_setpos;
            renderPanel.MouseUp -= this.lightsource_endrepos;

            StringBuilder entity_id_buff = new StringBuilder(250);

            NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wtrue);

            NativeFunc.editor_entity_get_selection(entity_id_buff, 0, entity_id_buff.Capacity);

            LightSource cEntity = renderPanel.CurrentLayer.LightArrays.Find(p => p.LightId == entity_id_buff.ToString());

            Point newpos = new Point(cEntity.Position.X, cEntity.Position.Y);

            object[] hist_data = new object[] { newpos, entity_id_buff.ToString() };

            entity_setpos_delegate func = this.lightsource_setpos2;

            HistoryEntry entry = new HistoryEntry(hist_data, oldpos_light, HistoryAction.SetPosition, func);

            history.Push(entry);

            this.MajniveauBackup();

            renderPanel.ToolSelected = Tools.Pencil;
        }

        private void light_modif_Click(object sender, EventArgs e)
        {
            lightDiag = new LightSourceDialog(levelObj);

            StringBuilder entity_id_buff = new StringBuilder(250);

            NativeFunc.editor_entity_get_selection(entity_id_buff, 0, entity_id_buff.Capacity);

            LightSource lightdata = renderPanel.CurrentLayer.LightArrays.Find(p => p.LightId == entity_id_buff.ToString());

            if (lightdata == null)
                return;

            lightDiag.FillData(lightdata);

            if (lightDiag.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                LightSource ent_data = lightDiag.Tag as LightSource;

                //change id if needed
                if (entity_id_buff.ToString() != ent_data.LightId)
                {
                    history.RemoveActionFromID(entity_id_buff.ToString(), HistoryAction.SetPosition);
                    NativeFunc.editor_changeentityid(entity_id_buff.ToString(), ent_data.LightId, NativeFunc.GAME_TYPE.GT_LIGHT);
                }


                MemoryStream entStream = new MemoryStream();
                entStream.Position = 0;
                ProtoBuf.Serializer.Serialize<LightSource>(entStream, ent_data);

                entStream.Position = 0;

                byte[] tmpentArray = entStream.ToArray();

                entStream.Close();

                NativeFunc.editor_updatelight(ent_data.LightId, tmpentArray, (uint)tmpentArray.Length);

                ItemsList.SelectedNode.Text = ent_data.LightId;
                ItemsList.SelectedNode.Name = ent_data.LightId;

            }
        }

        private void light_suppr_Click(object sender, EventArgs e)
        {
            StringBuilder entity_id_buff = new StringBuilder(250);

            NativeFunc.editor_entity_get_selection(entity_id_buff, 0, entity_id_buff.Capacity);

            if (MessageBox.Show("Supprimer la source de lumière " + entity_id_buff.ToString() + " ?", "Suppression source lumière", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {

                this.SupprLight(entity_id_buff.ToString());
                NativeFunc.editor_entity_unselect();
                this.MajniveauBackup();
            }
        }

        private void SupprLight(string light_id)
        {
            history.RemoveActionFromID(light_id, HistoryAction.SetPosition);
            renderPanel.CurrentLayer.LightArrays.RemoveAll(p => p.LightId == light_id);

            NativeFunc.editor_removelight(light_id);

            TreeNode[] cnode = ItemsList.Nodes.Find(light_id, true);

            ItemsList.Nodes["NodeLight"].Nodes.Remove(cnode[0]);
        }

        #endregion

        #region gestion entités particules

        ParticlesDialog partDiag;
        Point oldpos_particles;

        private void btnParticles_Click(object sender, EventArgs e)
        {
            partDiag = new ParticlesDialog(levelObj, script_modules, script_base_path);

            if (partDiag.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                renderPanel.MouseDown += this.particles_add;
                renderPanel.ToolSelected = Tools.Entity;
                NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wfalse);
            }
            else
            {
                NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wtrue);
            }
        }

        private void particles_add(object sender, MouseEventArgs e)
        {
            int worldposx, worldposy;
            renderPanel.GetWorldPos(e.X, e.Y, out worldposx, out worldposy);

            ParticlesData cEnt = partDiag.Tag as ParticlesData;

            cEnt.Position = new Wvector();
            cEnt.Position.X = worldposx;
            cEnt.Position.Y = worldposy;
            cEnt.ZOrder = renderPanel.CurrentLayer.Order;

            renderPanel.CurrentLayer.ParticlesArrays.Add(cEnt);


            MemoryStream entityStream = new MemoryStream();
            entityStream.Position = 0;
            ProtoBuf.Serializer.Serialize<ParticlesData>(entityStream, cEnt);

            entityStream.Position = 0;


            byte[] tmpEntityArray = entityStream.ToArray();

            entityStream.Close();

            NativeFunc.editor_addparticles(cEnt.Id, tmpEntityArray, (uint)tmpEntityArray.Length, renderPanel.CurrentMap.MapId, renderPanel.CurrentLayer.Order);

            TreeNode cnode = new TreeNode();
            cnode.Text = cEnt.Id;
            cnode.Tag = cEnt;
            cnode.Name = cEnt.Id;

            ItemsList.Nodes["NodeParticles"].Nodes.Add(cnode);
            ItemsList.SelectedNode = cnode;


            renderPanel.MouseDown -= this.particles_add;

            renderPanel.ToolSelected = Tools.Pencil;
            NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wtrue);

            this.MajniveauBackup();

        }

        private void particles_repos_Click(object sender, EventArgs e)
        {
            renderPanel.MouseDown += this.particles_setrepos;
            renderPanel.ToolSelected = Tools.Entity;
        }

        private void particles_setrepos(object sender, MouseEventArgs e)
        {
            StringBuilder entity_id_buff = new StringBuilder(250);

            NativeFunc.editor_entity_get_selection(entity_id_buff, 0, entity_id_buff.Capacity);

            ParticlesData cEntity = renderPanel.CurrentLayer.ParticlesArrays.Find(p => p.Id == entity_id_buff.ToString());

            oldpos_particles = new Point(cEntity.Position.X, cEntity.Position.Y);

            NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wfalse);

            particles_setpos(sender, e);
            renderPanel.MouseDown -= this.particles_setrepos;
            renderPanel.MouseMove += this.particles_setpos;
            renderPanel.MouseUp += this.particles_endrepos;
        }

        private void particles_setpos(object sender, MouseEventArgs e)
        {
            int worldposx, worldposy;
            renderPanel.GetWorldPos(e.X, e.Y, out worldposx, out worldposy, Settings.Default.snap_to_grid);

            StringBuilder entity_id_buff = new StringBuilder(250);

            NativeFunc.editor_entity_get_selection(entity_id_buff, 0, entity_id_buff.Capacity);

            ParticlesData cEntity = renderPanel.CurrentLayer.ParticlesArrays.Find(p => p.Id == entity_id_buff.ToString());

            if (cEntity == null)
                return;

            cEntity.Position = new Wvector();
            cEntity.Position.X = worldposx;
            cEntity.Position.Y = worldposy;

            NativeFunc.editor_updateparticlesposition(cEntity.Id, (float)cEntity.Position.X, (float)cEntity.Position.Y);
        }

        private void particles_setpos2(Point pos, string part_entity_id)
        {
            ParticlesData cEntity = renderPanel.CurrentLayer.ParticlesArrays.Find(p => p.Id == part_entity_id);

            if (cEntity == null)
                return;

            cEntity.Position = new Wvector();
            cEntity.Position.X = pos.X;
            cEntity.Position.Y = pos.Y;

            NativeFunc.editor_updateparticlesposition(cEntity.Id, cEntity.Position.X, cEntity.Position.Y);

            this.MajniveauBackup();
        }

        private void particles_endrepos(object sender, MouseEventArgs e)
        {
            renderPanel.MouseMove -= this.particles_setpos;
            renderPanel.MouseUp -= this.particles_endrepos;

            StringBuilder entity_id_buff = new StringBuilder(250);

            NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wtrue);

            NativeFunc.editor_entity_get_selection(entity_id_buff, 0, entity_id_buff.Capacity);

            ParticlesData cEntity = renderPanel.CurrentLayer.ParticlesArrays.Find(p => p.Id == entity_id_buff.ToString());

            Point newpos = new Point(cEntity.Position.X, cEntity.Position.Y);

            object[] hist_data = new object[] { newpos, entity_id_buff.ToString() };

            entity_setpos_delegate func = this.particles_setpos2;

            HistoryEntry entry = new HistoryEntry(hist_data, oldpos_particles, HistoryAction.SetPosition, func);

            history.Push(entry);

            this.MajniveauBackup();

            renderPanel.ToolSelected = Tools.Pencil;
        }

        private void particles_modif_Click(object sender, EventArgs e)
        {
            partDiag = new ParticlesDialog(levelObj, script_modules, script_base_path);

            StringBuilder entity_id_buff = new StringBuilder(250);

            NativeFunc.editor_entity_get_selection(entity_id_buff, 0, entity_id_buff.Capacity);

            ParticlesData partdata = renderPanel.CurrentLayer.ParticlesArrays.Find(p => p.Id == entity_id_buff.ToString());

            if (partdata == null)
                return;

            partDiag.FillData(partdata);

            if (partDiag.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                ParticlesData ent_data = partDiag.Tag as ParticlesData;

                MemoryStream entStream = new MemoryStream();
                entStream.Position = 0;
                ProtoBuf.Serializer.Serialize<ParticlesData>(entStream, ent_data);

                entStream.Position = 0;

                byte[] tmpentArray = entStream.ToArray();

                entStream.Close();

                NativeFunc.editor_updateparticles(ent_data.Id, tmpentArray, (uint)tmpentArray.Length, renderPanel.CurrentMap.MapId);

                ItemsList.SelectedNode.Text = ent_data.Id;
                ItemsList.SelectedNode.Name = ent_data.Id;

            }
        }

        private void particles_suppr_Click(object sender, EventArgs e)
        {
            StringBuilder entity_id_buff = new StringBuilder(250);

            NativeFunc.editor_entity_get_selection(entity_id_buff, 0, entity_id_buff.Capacity);

            if (MessageBox.Show("Supprimer l'object particules " + entity_id_buff.ToString() + " ?", "Suppression particules", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {

                this.SupprParticles(entity_id_buff.ToString());
                NativeFunc.editor_entity_unselect();
                this.MajniveauBackup();
            }
        }

        private void SupprParticles(string part_id)
        {
            history.RemoveActionFromID(part_id, HistoryAction.SetPosition);
            renderPanel.CurrentLayer.ParticlesArrays.RemoveAll(p => p.Id == part_id);

            NativeFunc.editor_removeparticles(part_id);

            TreeNode[] cnode = ItemsList.Nodes.Find(part_id, true);

            ItemsList.Nodes["NodeParticles"].Nodes.Remove(cnode[0]);
        }


        #endregion


        #region gestion waypoints

        WaypointDialog wayDiag;
        Point oldpos_waypoint;

        private void btnWaypoint_Click(object sender, EventArgs e)
        {
            wayDiag = new WaypointDialog(levelObj);

            if (wayDiag.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                renderPanel.MouseDown += this.waypoint_add;
                renderPanel.ToolSelected = Tools.Entity;
                NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wfalse);
            }
            else
            {
                NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wtrue);
            }
        }

        private void waypoint_add(object sender, MouseEventArgs e)
        {
            int worldposx, worldposy;
            renderPanel.GetWorldPos(e.X, e.Y, out worldposx, out worldposy);

            Waypoint cEnt = wayDiag.Tag as Waypoint;

            cEnt.Position = new Wvector();
            cEnt.Position.X = worldposx;
            cEnt.Position.Y = worldposy;

            renderPanel.CurrentLayer.Waypoints.Add(cEnt);


            MemoryStream entityStream = new MemoryStream();
            entityStream.Position = 0;
            ProtoBuf.Serializer.Serialize<Waypoint>(entityStream, cEnt);

            entityStream.Position = 0;


            byte[] tmpEntityArray = entityStream.ToArray();

            entityStream.Close();

            NativeFunc.editor_addwaypoint(cEnt.Id, tmpEntityArray, (uint)tmpEntityArray.Length, renderPanel.CurrentMap.MapId, renderPanel.CurrentLayer.Order);

            TreeNode cnode = new TreeNode();
            cnode.Text = cEnt.Id;
            cnode.Tag = cEnt;
            cnode.Name = cEnt.Id;

            ItemsList.Nodes["NodeWaypoint"].Nodes.Add(cnode);
            ItemsList.SelectedNode = cnode;


            renderPanel.MouseDown -= this.waypoint_add;

            renderPanel.ToolSelected = Tools.Pencil;
            NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wtrue);

            this.MajniveauBackup();

        }

        private void waypoint_repos_Click(object sender, EventArgs e)
        {
            renderPanel.MouseDown += this.waypoint_setrepos;
            renderPanel.ToolSelected = Tools.Entity;
        }

        private void waypoint_setrepos(object sender, MouseEventArgs e)
        {
            StringBuilder entity_id_buff = new StringBuilder(250);

            NativeFunc.editor_entity_get_selection(entity_id_buff, 0, entity_id_buff.Capacity);

            Waypoint cEntity = renderPanel.CurrentLayer.Waypoints.Find(p => p.Id == entity_id_buff.ToString());

            oldpos_waypoint = new Point(cEntity.Position.X, cEntity.Position.Y);

            NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wfalse);

            particles_setpos(sender, e);
            renderPanel.MouseDown -= this.waypoint_setrepos;
            renderPanel.MouseMove += this.waypoint_setpos;
            renderPanel.MouseUp += this.waypoint_endrepos;
        }

        private void waypoint_setpos(object sender, MouseEventArgs e)
        {
            int worldposx, worldposy;
            renderPanel.GetWorldPos(e.X, e.Y, out worldposx, out worldposy, Settings.Default.snap_to_grid);

            StringBuilder entity_id_buff = new StringBuilder(250);

            NativeFunc.editor_entity_get_selection(entity_id_buff, 0, entity_id_buff.Capacity);

            Waypoint cEntity = renderPanel.CurrentLayer.Waypoints.Find(p => p.Id == entity_id_buff.ToString());

            if (cEntity == null)
                return;

            cEntity.Position = new Wvector();
            cEntity.Position.X = worldposx;
            cEntity.Position.Y = worldposy;

            NativeFunc.editor_updatewaypointposition(cEntity.Id, (float)cEntity.Position.X, (float)cEntity.Position.Y);
        }

        private void waypoint_setpos2(Point pos, string part_entity_id)
        {
            Waypoint cEntity = renderPanel.CurrentLayer.Waypoints.Find(p => p.Id == part_entity_id);

            if (cEntity == null)
                return;

            cEntity.Position = new Wvector();
            cEntity.Position.X = pos.X;
            cEntity.Position.Y = pos.Y;

            NativeFunc.editor_updatewaypointposition(cEntity.Id, cEntity.Position.X, cEntity.Position.Y);

            this.MajniveauBackup();
        }

        private void waypoint_endrepos(object sender, MouseEventArgs e)
        {
            renderPanel.MouseMove -= this.waypoint_setpos;
            renderPanel.MouseUp -= this.waypoint_endrepos;

            StringBuilder entity_id_buff = new StringBuilder(250);

            NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wtrue);

            NativeFunc.editor_entity_get_selection(entity_id_buff, 0, entity_id_buff.Capacity);

            Waypoint cEntity = renderPanel.CurrentLayer.Waypoints.Find(p => p.Id == entity_id_buff.ToString());

            Point newpos = new Point(cEntity.Position.X, cEntity.Position.Y);

            object[] hist_data = new object[] { newpos, entity_id_buff.ToString() };

            entity_setpos_delegate func = this.particles_setpos2;

            HistoryEntry entry = new HistoryEntry(hist_data, oldpos_waypoint, HistoryAction.SetPosition, func);

            history.Push(entry);

            this.MajniveauBackup();

            renderPanel.ToolSelected = Tools.Pencil;
        }

        private void waypoint_modif_Click(object sender, EventArgs e)
        {
            wayDiag = new WaypointDialog(levelObj);

            StringBuilder entity_id_buff = new StringBuilder(250);

            NativeFunc.editor_entity_get_selection(entity_id_buff, 0, entity_id_buff.Capacity);

            Waypoint waydata = renderPanel.CurrentLayer.Waypoints.Find(p => p.Id == entity_id_buff.ToString());

            if (waydata == null)
                return;

            wayDiag.FillData(waydata);


            if (wayDiag.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                waydata = wayDiag.Tag as Waypoint;

                MemoryStream entStream = new MemoryStream();
                entStream.Position = 0;
                ProtoBuf.Serializer.Serialize<Waypoint>(entStream, waydata);

                entStream.Position = 0;

                byte[] tmpentArray = entStream.ToArray();

                entStream.Close();

                NativeFunc.editor_changeentityid(entity_id_buff.ToString(), waydata.Id, NativeFunc.GAME_TYPE.GT_WAYPOINT);

                ItemsList.SelectedNode.Text = waydata.Id;
                ItemsList.SelectedNode.Name = waydata.Id;

            }
        }

        private void waypoint_suppr_Click(object sender, EventArgs e)
        {
            StringBuilder entity_id_buff = new StringBuilder(250);

            NativeFunc.editor_entity_get_selection(entity_id_buff, 0, entity_id_buff.Capacity);

            if (MessageBox.Show("Supprimer l'object waypoint " + entity_id_buff.ToString() + " ?", "Suppression waypoint", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {

                this.SupprWaypoint(entity_id_buff.ToString());
                NativeFunc.editor_entity_unselect();
                this.MajniveauBackup();
            }
        }

        private void SupprWaypoint(string way_id)
        {
            history.RemoveActionFromID(way_id, HistoryAction.SetPosition);
            renderPanel.CurrentLayer.Waypoints.RemoveAll(p => p.Id == way_id);

            NativeFunc.editor_removewaypoint(way_id);

            TreeNode[] cnode = ItemsList.Nodes.Find(way_id, true);

            ItemsList.Nodes["NodeWaypoint"].Nodes.Remove(cnode[0]);
        }


        #endregion

        #region gestion collider

        ColliderDialog colDiag;
        Point oldpos_col;
        Size tmpoldsize_col;

        private void btnCollider_Click(object sender, EventArgs e)
        {
            colDiag = new ColliderDialog(levelObj);

            if (colDiag.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                renderPanel.MouseDown += this.collider_startlaying;
                renderPanel.ToolSelected = Tools.Trigger;
                NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wfalse);
            }
            else
            {
                NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wtrue);
            }
        }

        private void collider_startlaying(object sender, MouseEventArgs e)
        {
            int worldposx, worldposy;
            renderPanel.GetWorldPos(e.X, e.Y, out worldposx, out worldposy);

            Collider cEnt = colDiag.Tag as Collider;

            cEnt.Position = new Wvector();
            cEnt.Position.X = worldposx;
            cEnt.Position.Y = worldposy;

            renderPanel.CurrentLayer.Colliders.Add(cEnt);


            MemoryStream entityStream = new MemoryStream();
            entityStream.Position = 0;
            ProtoBuf.Serializer.Serialize<Collider>(entityStream, cEnt);

            entityStream.Position = 0;


            byte[] tmpEntityArray = entityStream.ToArray();

            entityStream.Close();

            NativeFunc.editor_addcollider(cEnt.Id, tmpEntityArray, (uint)tmpEntityArray.Length, renderPanel.CurrentMap.MapId, renderPanel.CurrentLayer.Order);
            NativeFunc.editor_selectentity(cEnt.Id, NativeFunc.GAME_TYPE.GT_COLLIDER, NativeFunc.wbool.wfalse);
            renderPanel.MouseDown -= this.collider_startlaying;
            renderPanel.MouseMove += this.collider_resize;
            renderPanel.MouseUp += this.collider_endlay;

            TreeNode cnode = new TreeNode();
            cnode.Text = cEnt.Id;
            cnode.Tag = cEnt;
            cnode.Name = cEnt.Id;

            ItemsList.Nodes["NodeCollider"].Nodes.Add(cnode);
            ItemsList.SelectedNode = cnode;

            NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wfalse);

            this.MajniveauBackup();

        }

        private void collider_setresize(object sender, MouseEventArgs e)
        {
            renderPanel.MouseDown -= this.collider_setresize;
            renderPanel.MouseMove += this.collider_resize;
            renderPanel.MouseUp += this.collider_endlay;

            StringBuilder collider_id_buff = new StringBuilder(250);

            NativeFunc.editor_entity_get_selection(collider_id_buff, 0, collider_id_buff.Capacity);

            NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wfalse);

            Collider cCollider = renderPanel.CurrentLayer.Colliders.Find(p => p.Id == collider_id_buff.ToString());

            tmpoldsize_col = new Size(cCollider.Width, cCollider.Height);
        }

        private void collider_resize(object sender, MouseEventArgs e)
        {
            int worldposx, worldposy;
            renderPanel.GetWorldPos(e.X, e.Y, out worldposx, out worldposy);

            StringBuilder collider_id_buff = new StringBuilder(250);

            NativeFunc.editor_entity_get_selection(collider_id_buff, 0, collider_id_buff.Capacity);

            Collider cCollider = renderPanel.CurrentLayer.Colliders.Find(p => p.Id == collider_id_buff.ToString());
            cCollider.Width = Math.Abs((worldposx - cCollider.Position.X));
            cCollider.Height = Math.Abs((worldposy - cCollider.Position.Y));

            NativeFunc.editor_updatecollidersize(cCollider.Id, (ushort)cCollider.Width, (ushort)cCollider.Height);

        }

        private void collider_endlay(object sender, MouseEventArgs e)
        {
            renderPanel.MouseMove -= this.collider_resize;
            renderPanel.MouseUp -= this.collider_endlay;

            NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wtrue);

            if (renderPanel.ToolSelected != Tools.Trigger)
            {
                StringBuilder collider_id_buff = new StringBuilder(250);

                NativeFunc.editor_entity_get_selection(collider_id_buff, 0, collider_id_buff.Capacity);

                Collider cCollider = renderPanel.CurrentLayer.Colliders.Find(p => p.Id == collider_id_buff.ToString());

                MemoryStream colliderStream = new MemoryStream();
                colliderStream.Position = 0;
                ProtoBuf.Serializer.Serialize<Collider>(colliderStream, cCollider);

                colliderStream.Position = 0;


                byte[] tmpColliderArray = colliderStream.ToArray();

                colliderStream.Close();

                NativeFunc.editor_updatecollider(cCollider.Id, tmpColliderArray, (uint)tmpColliderArray.Length, renderPanel.CurrentMap.MapId);

                System.Drawing.Size newsize = new System.Drawing.Size(cCollider.Width, cCollider.Height);

                object[] data = new object[] { newsize, collider_id_buff.ToString() };

                trigger_resize_delegate func = this.collider_resize2;

                HistoryEntry entry = new HistoryEntry(data, tmpoldsize_col, HistoryAction.SetTriggerSize, func);

                history.Push(entry);

                this.MajniveauBackup();
            }

            renderPanel.ToolSelected = Tools.Pencil;
        }

        private void collider_resize2(Size collider_size, string collider_id)
        {
            Collider cCollider = renderPanel.CurrentLayer.Colliders.Find(p => p.Id == collider_id);
            cCollider.Width = collider_size.Width;
            cCollider.Height = collider_size.Height;

            MemoryStream colliderStream = new MemoryStream();
            colliderStream.Position = 0;
            ProtoBuf.Serializer.Serialize<Collider>(colliderStream, cCollider);

            colliderStream.Position = 0;


            byte[] tmpColliderArray = colliderStream.ToArray();

            colliderStream.Close();

            NativeFunc.editor_updatecollider(cCollider.Id, tmpColliderArray, (uint)tmpColliderArray.Length, renderPanel.CurrentMap.MapId);

            this.MajniveauBackup();
        }


        private void collider_repos_Click(object sender, EventArgs e)
        {
            renderPanel.MouseDown += this.collider_setrepos;
            renderPanel.ToolSelected = Tools.Entity;
        }

        private void collider_setrepos(object sender, MouseEventArgs e)
        {
            StringBuilder entity_id_buff = new StringBuilder(250);

            NativeFunc.editor_entity_get_selection(entity_id_buff, 0, entity_id_buff.Capacity);

            Collider cEntity = renderPanel.CurrentLayer.Colliders.Find(p => p.Id == entity_id_buff.ToString());

            oldpos_col = new Point(cEntity.Position.X, cEntity.Position.Y);

            NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wfalse);

            particles_setpos(sender, e);
            renderPanel.MouseDown -= this.collider_setrepos;
            renderPanel.MouseMove += this.collider_setpos;
            renderPanel.MouseUp += this.collider_endrepos;
        }

        private void collider_setpos(object sender, MouseEventArgs e)
        {
            int worldposx, worldposy;
            renderPanel.GetWorldPos(e.X, e.Y, out worldposx, out worldposy, Settings.Default.snap_to_grid);

            StringBuilder entity_id_buff = new StringBuilder(250);

            NativeFunc.editor_entity_get_selection(entity_id_buff, 0, entity_id_buff.Capacity);

            Collider cEntity = renderPanel.CurrentLayer.Colliders.Find(p => p.Id == entity_id_buff.ToString());

            if (cEntity == null)
                return;

            cEntity.Position = new Wvector();
            cEntity.Position.X = worldposx;
            cEntity.Position.Y = worldposy;

            NativeFunc.editor_updatecolliderposition(cEntity.Id, (float)cEntity.Position.X, (float)cEntity.Position.Y);
        }

        private void collider_setpos2(Point pos, string part_entity_id)
        {
            Collider cEntity = renderPanel.CurrentLayer.Colliders.Find(p => p.Id == part_entity_id);

            if (cEntity == null)
                return;

            cEntity.Position = new Wvector();
            cEntity.Position.X = pos.X;
            cEntity.Position.Y = pos.Y;

            NativeFunc.editor_updatecolliderposition(cEntity.Id, cEntity.Position.X, cEntity.Position.Y);

            this.MajniveauBackup();
        }

        private void collider_endrepos(object sender, MouseEventArgs e)
        {
            renderPanel.MouseMove -= this.collider_setpos;
            renderPanel.MouseUp -= this.collider_endrepos;

            StringBuilder entity_id_buff = new StringBuilder(250);

            NativeFunc.editor_showcursor(NativeFunc.wbool.wfalse, NativeFunc.wbool.wtrue);

            NativeFunc.editor_entity_get_selection(entity_id_buff, 0, entity_id_buff.Capacity);

            Collider cEntity = renderPanel.CurrentLayer.Colliders.Find(p => p.Id == entity_id_buff.ToString());

            Point newpos = new Point(cEntity.Position.X, cEntity.Position.Y);

            object[] hist_data = new object[] { newpos, entity_id_buff.ToString() };

            entity_setpos_delegate func = this.particles_setpos2;

            HistoryEntry entry = new HistoryEntry(hist_data, oldpos_col, HistoryAction.SetPosition, func);

            history.Push(entry);

            this.MajniveauBackup();

            renderPanel.ToolSelected = Tools.Pencil;
        }

        private void redimensionnerColliderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            renderPanel.MouseDown += this.collider_setresize;
            renderPanel.ToolSelected = Tools.Trigger;
        }

        private void repositionnerColliderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            renderPanel.MouseDown += this.collider_setrepos;
            renderPanel.ToolSelected = Tools.Trigger;
        }

        private void collider_modif_Click(object sender, EventArgs e)
        {
            colDiag = new ColliderDialog(levelObj);

            StringBuilder entity_id_buff = new StringBuilder(250);

            NativeFunc.editor_entity_get_selection(entity_id_buff, 0, entity_id_buff.Capacity);

            Collider coldata = renderPanel.CurrentLayer.Colliders.Find(p => p.Id == entity_id_buff.ToString());

            if (coldata == null)
                return;

            colDiag.FillData(coldata);

            if (colDiag.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Collider ent_data = colDiag.Tag as Collider;

                MemoryStream entStream = new MemoryStream();
                entStream.Position = 0;
                ProtoBuf.Serializer.Serialize<Collider>(entStream, ent_data);

                entStream.Position = 0;

                byte[] tmpentArray = entStream.ToArray();

                entStream.Close();

                NativeFunc.editor_updatecollider(entity_id_buff.ToString(), tmpentArray, (uint)tmpentArray.Length, renderPanel.CurrentMap.MapId);

                ItemsList.SelectedNode.Text = ent_data.Id;
                ItemsList.SelectedNode.Name = ent_data.Id;

            }
        }

        private void collider_suppr_Click(object sender, EventArgs e)
        {
            StringBuilder entity_id_buff = new StringBuilder(250);

            NativeFunc.editor_entity_get_selection(entity_id_buff, 0, entity_id_buff.Capacity);

            if (MessageBox.Show("Supprimer l'object collisions " + entity_id_buff.ToString() + " ?", "Suppression collisions", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {

                this.SupprCollider(entity_id_buff.ToString());
                NativeFunc.editor_entity_unselect();
                this.MajniveauBackup();
            }
        }

        private void SupprCollider(string col_id)
        {
            history.RemoveActionFromID(col_id, HistoryAction.SetPosition);
            renderPanel.CurrentLayer.Colliders.RemoveAll(p => p.Id == col_id);

            NativeFunc.editor_removewaypoint(col_id);

            TreeNode[] cnode = ItemsList.Nodes.Find(col_id, true);

            ItemsList.Nodes["NodeCollider"].Nodes.Remove(cnode[0]);
        }


        #endregion

        #region Outils

        private void recréerToutLesObjetsScriptsToolStripMenuItem_Click(object sender, EventArgs e)
        {

            List<Map> mapArray = levelObj.MapArrays;

            foreach (Map oMap in mapArray)
            {
                foreach (Layer oLayer in oMap.LayerArrays)
                {
                    foreach (Entity oEntity in oLayer.EntityArrays)
                    {
                        if (!string.IsNullOrEmpty(oEntity.ScriptModule) && !string.IsNullOrEmpty(oEntity.ScriptCtor))
                        {
                            //remove then add entity
                            StringBuilder str = new StringBuilder(10000);

                            if (!oEntity.ScriptInitOnly)
                            {
                                string delete_func = "remove_entity('" + oEntity.EntityId + "')";

                                NativeFunc.editor_exec_string(delete_func, str, str.Capacity);
                            }

                            NativeFunc.editor_exec_string(ScriptUtility.genconstruct(oEntity), str, str.Capacity);

                            if (str.Length == 0)
                                LogTextToBox("[LUA] entité " + oEntity.EntityId + " recréé");
                            else
                                LogTextToBox(str.ToString());
                        }
                    }

                    foreach (TextObject oText in oLayer.TextArrays)
                    {
                        if (!string.IsNullOrEmpty(oText.ScriptModule) && !string.IsNullOrEmpty(oText.ScriptCtor))
                        {
                            StringBuilder str = new StringBuilder(10000);

                            if (!oText.ScriptInitOnly)
                            {
                                string delete_func = "remove_text('" + oText.TextId + "')";

                                NativeFunc.editor_exec_string(delete_func, str, str.Capacity);
                            }

                            NativeFunc.editor_exec_string(ScriptUtility.gentextconstruct(oText), str, str.Capacity);

                            if (str.Length == 0)
                                LogTextToBox("[LUA] texte " + oText.TextId + " recréé");
                            else
                                LogTextToBox(str.ToString());
                        }
                    }
                }
            }

            MessageBox.Show("Objets scripts recréés !");
        }

        private void rechargerScriptScèneToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (levelObj != null && !string.IsNullOrEmpty(levelObj.EditorScriptFile))
            {
                if (File.Exists(levelObj.EditorScriptFile))
                {
                    FileInfo scriptFile = new FileInfo(levelObj.EditorScriptFile);

                    while (BitmapUtilities.IsFileLocked(scriptFile))
                        Thread.Yield();

                    //rechargement du fichier de script
                    string level_module = scriptFile.Name.Substring(0, scriptFile.Name.LastIndexOf('.'));
                    this.charger_module(level_module, true);

                    //appel fonction init uniquement pour le script de niveau
                    StringBuilder buffer = new StringBuilder(1000);
                    string function = string.Format("{0}.init()", level_module);
                    NativeFunc.editor_exec_string(function, buffer, buffer.Capacity);

                    if (buffer.Length == 0)
                        LogTextToBox("[LUA] fonction " + function + " OK");
                    else
                        LogTextToBox(buffer.ToString());

                    MessageBox.Show("Script global du niveau rechargé !");
                }
                else
                {
                    LogTextToBox(string.Format("Le fichier de script {0} n'existe pas !", levelObj.EditorScriptFile));

                }




            }
        }


        private void afficherFichierLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start(Path.GetDirectoryName(Application.ExecutablePath) + "\\wafa.log");
        }



        private void fusionAvecAutreNiveauToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(openLevelDialog.FileName))
            {
                MessageBox.Show("Veuillez enregister le niveau actuel avant de faire une fusion !");
                return;
            }

            //avant chaque fusion, on enregistre le contenu du fichier courant
            this.EnregistrerFichierNiveau(openLevelDialog.FileName, true);

            Fusion fusForm = new Fusion(levelObj);

            if (fusForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                levelObj = fusForm.Tag as Level;

                string path = openLevelDialog.FileName;

                //sauvegarder et recharger le fichier de niveau
                this.EnregistrerFichierNiveau(path, true, false);
                this.FermerNiveau(path);
                this.ChargerNiveau_base(path);
                openLevelDialog.FileName = path;
            }
        }

        private void exportDansUnFichierStreamToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(openLevelDialog.FileName))
            {
                MessageBox.Show("Veuillez enregister le niveau actuel avant de faire un export dans un fichier stream !");
                return;
            }

            //avant chaque export, on enregistre le contenu du fichier courant
            this.EnregistrerFichierNiveau(openLevelDialog.FileName, true);

            if (exportToStreamFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                //SQLiteConnection 


                SQLiteConnection StreamConnection = new SQLiteConnection("Data Source = " + exportToStreamFile.FileName + "; Version = 3;UseUTF16Encoding = True;");
                SQLiteCommand StreamCommand = new SQLiteCommand(StreamConnection);

                try
                {
                    StreamConnection.Open();

                    StreamCommand.CommandText = "CREATE TABLE chunks( row_num int64, col_num int64,chunkid varchar(255),content BLOB)";

                    StreamCommand.ExecuteNonQuery();

                    StreamCommand.CommandText = "INSERT INTO chunks VALUES(@row,@col,@chunkid,@content)";

                    Int64 rownum = -1;
                    Int64 colnum = -1;

                    foreach (Map oMap in levelObj.MapArrays)
                    {
                        SQLiteParameter paramrow = new SQLiteParameter();
                        paramrow.DbType = DbType.Int64;
                        paramrow.ParameterName = "@row";
                        paramrow.Value = rownum;

                        StreamCommand.Parameters.Add(paramrow);

                        SQLiteParameter paramcol = new SQLiteParameter();
                        paramcol.DbType = DbType.Int64;
                        paramcol.ParameterName = "@col";
                        paramcol.Value = colnum;

                        StreamCommand.Parameters.Add(paramcol);

                        SQLiteParameter paramid = new SQLiteParameter();
                        paramid.DbType = DbType.StringFixedLength;
                        paramid.ParameterName = "@chunkid";
                        paramid.Value = Guid.NewGuid().ToString();

                        StreamCommand.Parameters.Add(paramid);

                        MemoryStream mapmemory = new MemoryStream();
                        mapmemory.Position = 0;
                        ProtoBuf.Serializer.Serialize<Map>(mapmemory, oMap);

                        mapmemory.Position = 0;

                        byte[] tmpmapArray = mapmemory.ToArray();

                        mapmemory.Close();

                        SQLiteParameter paramcontent = new SQLiteParameter();
                        paramcontent.DbType = DbType.Binary;
                        paramcontent.ParameterName = "@content";
                        paramcontent.Value = tmpmapArray;

                        StreamCommand.Parameters.Add(paramcontent);

                        StreamCommand.ExecuteNonQuery();

                        colnum++;

                        if (colnum > 1)
                        {
                            rownum++;
                            colnum = -1;
                        }
                    }

                    LogTextToBox("Export effectué !");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erreur lors de l'export dans un fichier stream ! " + ex.ToString());
                }
                finally
                {
                    if (StreamConnection.State != ConnectionState.Closed)
                    {
                        StreamConnection.Close();
                    }
                }

                //exportToStreamFile.OpenFile();
            }
        }


        private void importerAssetStatiqueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ImportAsset importdialog = new ImportAsset();

            importdialog.ShowDialog();
        }

        private void regénérerUnAssetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RegenDialog regendiag = new RegenDialog();

            regendiag.ShowDialog();
        }


        #endregion

        #region Export jeu

        private void windowsExport_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(Settings.Default.rootFolder))
            {
                MessageBox.Show("Les répertoires pour l'export Windows ne sont pas défini ! abandon...");
                return;
            }

            if (string.IsNullOrEmpty(Settings.Default.gamename))
            {
                MessageBox.Show("Le nom du jeu n'est pas défini dans les options d'export ! abandon...");
                return;
            }

            if (current_mode == GlobalMode.Play)
            {
                MessageBox.Show("Impossible de lancer l'export en mode jeu ! passez en mode Edition");
                return;
            }

            //recherche fichier sln
            string[] solutionFiles = Directory.GetFiles(Settings.Default.rootFolder + @"\game_template\", "*.sln");

            if (solutionFiles.Length == 0)
            {
                MessageBox.Show("Aucune fichier .sln dans le répertoire game_template ! abandon...");
                return;
            }

            //recherche fichier de config
            if (!File.Exists(string.Format(@"{0}\game_template\assets\config_pc.lua", Settings.Default.rootFolder)))
            {
                MessageBox.Show("Aucune fichier config_pc.lua dans le répertoire d'assets ! abandon");
                return;
            }

            exportLocation.Filter = "Fichier zip|*.zip";
            //ouverture fenêtre destination fichier
            if (exportLocation.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                return;

            //affichage fenêtre export
            ExportLog exportlogwindow = new ExportLog();
            exportlogwindow.Text = "Export Windows en cours...";

            exportlogwindow.Show();

            if (solutionFiles.Length > 1)
                exportlogwindow.AppendLog("Le répertoire game_template contient plus qu'un fichier .sln, le premier fichier " + solutionFiles[0] + " sera utilisé", ExportLogType.WARNING);

            if (OglPanel._init)
            {
                //-1- déchargement game DLL (pour export)
                exportlogwindow.AppendLog("Déchargement DLL...");
                NativeFunc.editor_release_gamedll();
            }


            //-2- compilation game template (DLL plus client)
            exportlogwindow.AppendLog("Compilation du fichier projet...");
            ProcessStartInfo CompilationInfo = new ProcessStartInfo("cmd.exe", string.Format(@"/c MSBuild {0} /p:Configuration=Release /p:Platform=Win32", solutionFiles[0]));
            CompilationInfo.UseShellExecute = false;
            CompilationInfo.RedirectStandardOutput = true;

            Process CompilationProc = Process.Start(CompilationInfo);
            CompilationProc.OutputDataReceived += (osender, oe) => exportlogwindow.AppendLog(oe.Data, threaded: true);
            CompilationProc.BeginOutputReadLine();

            while (!CompilationProc.HasExited)
                Application.DoEvents();


            string temp_directory = string.Format(@"{0}\game_template\temp_windows", Settings.Default.rootFolder);

            if (Directory.Exists(temp_directory))
                Directory.Delete(temp_directory, true);

            Directory.CreateDirectory(temp_directory);
            Directory.CreateDirectory(temp_directory + @"\assets");
            Directory.CreateDirectory(temp_directory + @"\scripts");

            //-3- export assets, save.db et locale.db
            exportlogwindow.AppendLog("Exports assets et fichiers scripts...");
            ProcessStartInfo exportAssetsInfo = new ProcessStartInfo("cmd.exe", string.Format(@"/c {0}\animation_converter\AnimationConverter.exe {0}\game_template\assets\animated_assets {0}\game_template\assets {0}\game_template\temp_windows\assets {0}\game_template\temp_windows\scripts {0}\game_template\assets\exclude_pc.txt", Settings.Default.rootFolder));
            exportAssetsInfo.UseShellExecute = false;
            exportAssetsInfo.RedirectStandardOutput = true;

            Process exportAssetsProc = Process.Start(exportAssetsInfo);
            exportAssetsProc.OutputDataReceived += (osender, oe) => exportlogwindow.AppendLog(oe.Data, threaded: true);
            exportAssetsProc.BeginOutputReadLine();

            while (!exportAssetsProc.HasExited)
                Application.DoEvents();



            try
            {
                exportlogwindow.AppendLog("Copie fichier sauvegarde...");
                File.Copy(string.Format(@"{0}\game_template\assets\save.db", Settings.Default.rootFolder), string.Format(@"{0}\game_template\bin\Release\save.db", Settings.Default.rootFolder), true);
            }
            catch (Exception ex)
            {
                exportlogwindow.AppendLog("Erreur lors de la copie du fichier de sauvegarde " + ex.ToString(), ExportLogType.ERROR);
            }

            try
            {
                exportlogwindow.AppendLog("Copie fichier locale...");
                File.Copy(string.Format(@"{0}\game_template\assets\locale.db", Settings.Default.rootFolder), string.Format(@"{0}\game_template\bin\Release\locale.db", Settings.Default.rootFolder), true);
            }
            catch (Exception ex)
            {
                exportlogwindow.AppendLog("Erreur lors de la copie de locale " + ex.ToString(), ExportLogType.ERROR);
            }

            //-4- copie fichiers dans répertoire temporaire
            string tempDirectory = string.Format(@"{0}\game_template\bin\Releasetemp\", Settings.Default.rootFolder);

            try
            {
                exportlogwindow.AppendLog("Création répertoire temporaire...");
                Directory.CreateDirectory(tempDirectory);

                exportlogwindow.AppendLog("Copie fichiers...");

                foreach (string file in Directory.GetFiles(string.Format(@"{0}\game_template\bin\Release", Settings.Default.rootFolder)))
                {
                    FileInfo inf = new FileInfo(file);

                    if (inf.Extension != ".log" && inf.Extension != ".pdb" && inf.Extension != ".exp" && inf.Extension != ".lib")
                        File.Copy(file, tempDirectory + inf.Name, true);
                }

                exportlogwindow.AppendLog("Copie librairie standard...");

                File.Copy(string.Format(@"{0}\libs-msvc\msvcp100.dll", Settings.Default.rootFolder), tempDirectory + "msvcp100.dll", true);
                File.Copy(string.Format(@"{0}\libs-msvc\msvcr100.dll", Settings.Default.rootFolder), tempDirectory + "msvcr100.dll", true);

            }
            catch (Exception ex)
            {
                exportlogwindow.AppendLog("Erreur lors de la création du répertoire temporaire " + ex.ToString(), ExportLogType.ERROR);
            }

            int packer_result = 0;

            //-4.1- packing texture si nécessaire
            if (Settings.Default.packtexture)
            {
                exportlogwindow.AppendLog("Packing texture...");

                string[] exclude = File.ReadAllLines(string.Format(@"{0}\game_template\assets\exclude_packer.txt", Settings.Default.rootFolder));

                List<string> list_exclude = new List<string>(exclude);


                packer_result = SpriteSheetPack.Pack(string.Format(@"{0}\game_template\temp_windows\assets\", Settings.Default.rootFolder), true, (Settings.Default.packtextureheight == Settings.Default.packtexturewidth), (int)Settings.Default.packtexturewidth, (int)Settings.Default.packtextureheight, 2, string.Format(@"{0}\game_template\temp_windows\assets\{1}", Settings.Default.rootFolder, "packtexture.png"), string.Format(@"{0}\game_template\temp_windows\assets\{1}", Settings.Default.rootFolder, "packtexture.pack"), true, list_exclude);

                if (packer_result != 0)
                {
                    exportlogwindow.AppendLog("Erreur lors du packing de texture ", ExportLogType.ERROR);
                }
            }

            //-5- zip assets
            List<string> assets_folder = new List<string>()
            {
                string.Format(@"{0}\game_template\temp_windows\assets", Settings.Default.rootFolder),
                string.Format(@"{0}\game_template\temp_windows\scripts", Settings.Default.rootFolder)
            };

            try
            {
                exportlogwindow.AppendLog("Compression assets...");
                ZipUtils.CreateArchive(string.Format(@"{0}\game_template\bin\Releasetemp\data.zip", Settings.Default.rootFolder), assets_folder, new List<string>());

            }
            catch (Exception ex)
            {
                exportlogwindow.AppendLog("Erreur lors de la compression des assets " + ex.ToString(), ExportLogType.ERROR);
            }

            try
            {
                exportlogwindow.AppendLog("Modification fichier config...");
                //-6- modif chemin vers assets dans fichier de config
                var text = File.ReadAllText(string.Format(@"{0}\game_template\assets\config_pc.lua", Settings.Default.rootFolder));

                int has_asset = text.IndexOf("asset_path");

                if (has_asset != -1)
                {
                    int start_asset = text.IndexOf('"', has_asset);
                    int end_asset = text.IndexOf('"', start_asset + 1) + 1;

                    text = text.Remove(start_asset, end_asset - start_asset);

                    text = text.Insert(start_asset, "\"data.zip\"");
                }

                int has_scripts = text.IndexOf("script_path");

                if (has_scripts != -1)
                {
                    int start_script = text.IndexOf('"', has_scripts);
                    int end_script = text.IndexOf('"', start_script + 1) + 1;

                    text = text.Remove(start_script, end_script - start_script);

                    text = text.Insert(start_script, "\"data.zip\"");
                }

                if (Settings.Default.packtexture && packer_result == 0)
                {
                    int first_pos = text.IndexOf('{') + 1;

                    text = text.Insert(first_pos, "\npacker = \"packtexture.pack\",");
                }

                File.WriteAllText(string.Format(@"{0}\game_template\bin\Releasetemp\config.lua", Settings.Default.rootFolder), text);
            }
            catch (Exception ex)
            {
                exportlogwindow.AppendLog("Erreur lors de la modification du fichier de config " + ex.ToString(), ExportLogType.ERROR);
            }

            //-7- zip dossier jeu dans destination finale 

            List<string> _gamefolder = new List<string>()
            {
              tempDirectory
            };


            List<string> _replace_name = new List<string>()
            {
              Settings.Default.gamename
            };

            try
            {
                exportlogwindow.AppendLog("Compression dossier jeu...");
                ZipUtils.CreateArchive(exportLocation.FileName, _gamefolder, _replace_name);

            }
            catch (Exception ex)
            {
                exportlogwindow.AppendLog("Erreur lors de la compression du dossier jeu" + ex.ToString(), ExportLogType.ERROR);
            }

            //-8- suppression dossier temporaire
            try
            {
                exportlogwindow.AppendLog("Suppression répertoire temporaire...");
                Directory.Delete(tempDirectory, true);
                Directory.Delete(temp_directory, true);

            }
            catch (Exception ex)
            {
                exportlogwindow.AppendLog("Erreur lors de la suppression du répertoire temporaire" + ex.ToString(), ExportLogType.ERROR);
            }

            exportlogwindow.AppendLog("Export terminé !");
            exportlogwindow.Text = "Export Windows terminé !";

            if (OglPanel._init)
            {
                if (NativeFunc.editor_load_gamedll(Settings.Default.gamedll_path) == NativeFunc.wbool.wfalse)
                {
                    MessageBox.Show("La dll de jeu à été compilée avec une précédente version du moteur de jeu, impossible de la charger", "Différence de version", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }

        }

        private void androidExport_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(Settings.Default.rootFolder) || string.IsNullOrWhiteSpace(Settings.Default.antFolder) || string.IsNullOrWhiteSpace(Settings.Default.jdkFolder))
            {
                MessageBox.Show("Les répertoires pour l'export android ne sont pas défini ! abandon...");
                return;
            }

            if (string.IsNullOrEmpty(Settings.Default.gamename))
            {
                MessageBox.Show("Le nom du jeu n'est pas défini dans les options d'export ! abandon...");
                return;
            }

            if (!File.Exists(string.Format(@"{0}\game_template\assets\config_android.lua", Settings.Default.rootFolder)))
            {
                MessageBox.Show("Aucune fichier config_android.lua dans le répertoire d'assets ! abandon");
                return;
            }

            if (current_mode == GlobalMode.Play)
            {
                MessageBox.Show("Impossible de lancer l'export en mode jeu ! passez en mode Edition");
                return;
            }

            exportLocation.Filter = "Fichier apk|*.apk";
            //ouverture fenêtre destination fichier
            if (exportLocation.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                return;

            ExportLog logObject = new ExportLog();
            logObject.Show();

            logObject.Text = "Export Android en cours...";

            //création des répertoire nécessaire à l'export android si ils n'existent pas
            if (!Directory.Exists(string.Format(@"{0}\game_template\template_android\Assets", Settings.Default.rootFolder)))
                Directory.CreateDirectory(string.Format(@"{0}\game_template\template_android\Assets", Settings.Default.rootFolder));

            if (!Directory.Exists(string.Format(@"{0}\game_template\template_android\bin", Settings.Default.rootFolder)))
                Directory.CreateDirectory(string.Format(@"{0}\game_template\template_android\bin", Settings.Default.rootFolder));

            //suppression du contenu du répertoire assets
            foreach (string deletefile in Directory.GetFiles(string.Format(@"{0}\game_template\template_android\Assets", Settings.Default.rootFolder)))
                File.Delete(deletefile);

            //-1-export assets,save.db et locale.db
            logObject.AppendLog("Exports assets et fichiers scripts..");
            ProcessStartInfo exportAssetsInfo = new ProcessStartInfo("cmd.exe", string.Format(@"/c {0}\animation_converter\AnimationConverter.exe {0}\game_template\assets\animated_assets {0}\game_template\assets {0}\game_template\template_android\Assets {0}\game_template\template_android\Assets {0}\game_template\assets\exclude_android.txt", Settings.Default.rootFolder));
            exportAssetsInfo.UseShellExecute = false;
            exportAssetsInfo.RedirectStandardOutput = true;

            Process exportAssetsProc = Process.Start(exportAssetsInfo);
            exportAssetsProc.OutputDataReceived += (osender, oe) => logObject.AppendLog(oe.Data, threaded: true);
            exportAssetsProc.BeginOutputReadLine();

            while (!exportAssetsProc.HasExited)
                Application.DoEvents();

            try
            {
                logObject.AppendLog("Copie fichier sauvegarde...");
                File.Copy(string.Format(@"{0}\game_template\assets\save.db", Settings.Default.rootFolder), string.Format(@"{0}\game_template\template_android\Assets\save.db", Settings.Default.rootFolder), true);
            }
            catch (Exception ex)
            {
                logObject.AppendLog("Erreur lors de la copie du fichier de sauvegarde " + ex.ToString(), ExportLogType.ERROR);
            }



            try
            {
                logObject.AppendLog("Copie fichier locale...");
                File.Copy(string.Format(@"{0}\game_template\assets\locale.db", Settings.Default.rootFolder), string.Format(@"{0}\game_template\template_android\Assets\locale.db", Settings.Default.rootFolder), true);
            }
            catch (Exception ex)
            {
                logObject.AppendLog("Erreur lors de la copie de locale " + ex.ToString(), ExportLogType.ERROR);
            }

            int packer_result = 0;

            //-4.1- packing texture si nécessaire
            if (Settings.Default.packtexture)
            {
                logObject.AppendLog("Packing texture...");

                string[] exclude = File.ReadAllLines(string.Format(@"{0}\game_template\assets\exclude_packer.txt", Settings.Default.rootFolder));

                List<string> list_exclude = new List<string>(exclude);

                packer_result = SpriteSheetPack.Pack(string.Format(@"{0}\game_template\template_android\Assets\", Settings.Default.rootFolder), true, (Settings.Default.packtextureheight == Settings.Default.packtexturewidth), (int)Settings.Default.packtexturewidth, (int)Settings.Default.packtextureheight, 2, string.Format(@"{0}\game_template\template_android\Assets\{1}", Settings.Default.rootFolder, "packtexture.png"), string.Format(@"{0}\game_template\template_android\Assets\{1}", Settings.Default.rootFolder, "packtexture.pack"), true, list_exclude);

                if (packer_result != 0)
                {
                    logObject.AppendLog("Erreur lors du packing de texture ", ExportLogType.ERROR);
                }
            }


            try
            {
                logObject.AppendLog("Modification fichier config...");

                var text = File.ReadAllText(string.Format(@"{0}\game_template\assets\config_android.lua", Settings.Default.rootFolder));

                if (Settings.Default.packtexture && packer_result == 0)
                {
                    int first_pos = text.IndexOf('{') + 1;

                    text = text.Insert(first_pos, "\npacker = \"packtexture.pack\",");
                }

                File.WriteAllText(string.Format(@"{0}\game_template\template_android\Assets\config.lua", Settings.Default.rootFolder), text);
            }
            catch (Exception ex)
            {
                logObject.AppendLog("Erreur lors de la modification du fichier de config " + ex.ToString(), ExportLogType.ERROR);
            }


            //-2- appel ant pour création apk

            string build_params = "debug";

            if (Settings.Default.exportcleanbuild)
                build_params = "clean " + build_params;

            ProcessStartInfo antbuildInfo = new ProcessStartInfo("cmd.exe", string.Format(@"/c ""{0}\bin\ant"" -f {1}\game_template\template_android {2}", Settings.Default.antFolder, Settings.Default.rootFolder, build_params));
            antbuildInfo.EnvironmentVariables["JAVA_HOME"] = Settings.Default.jdkFolder;
            antbuildInfo.UseShellExecute = false;
            antbuildInfo.RedirectStandardOutput = true;
            antbuildInfo.RedirectStandardError = true;

            Process antbuildProc = Process.Start(antbuildInfo);
            antbuildProc.OutputDataReceived += (osender, oe) => logObject.AppendLog(oe.Data, threaded: true);
            antbuildProc.ErrorDataReceived += (osender2, oe2) => logObject.AppendLog(oe2.Data, threaded: true);
            antbuildProc.BeginOutputReadLine();
            antbuildProc.BeginErrorReadLine();

            while (!antbuildProc.HasExited)
                Application.DoEvents();

            //-3- copie de l'apk dans le répertoire cible
            string[] apk_list = Directory.GetFiles(string.Format(@"{0}\game_template\template_android\bin", Settings.Default.rootFolder), "*.apk");
            string generated_apk = string.Empty;

            foreach (string apk_file in apk_list)
            {
                FileInfo finfo = new FileInfo(apk_file);

                if (!finfo.Name.Contains("-unaligned"))
                {
                    if (string.IsNullOrEmpty(generated_apk))
                        generated_apk = apk_file;
                    else if (!finfo.Name.Contains("-debug"))
                        generated_apk = apk_file;
                }
            }

            try
            {
                logObject.AppendLog("Copie de l'apk dans le répertoire cible");
                File.Copy(generated_apk, exportLocation.FileName, true);
            }
            catch (Exception ex)
            {
                logObject.AppendLog("Erreur lors de la copie de l'apk dans le répertoire cible " + ex.ToString(), ExportLogType.ERROR);
            }

            logObject.Text = "Export Android terminé !";
        }

        private void webGLExport_Click(object sender, EventArgs e)
        {
            //-1- sélection destination export
            if (string.IsNullOrWhiteSpace(Settings.Default.rootFolder))
            {
                MessageBox.Show("Les répertoires pour l'export WebGL ne sont pas défini ! abandon...");
                return;
            }

            if (string.IsNullOrEmpty(Settings.Default.gamename))
            {
                MessageBox.Show("Le nom du jeu n'est pas défini dans les options d'export ! abandon...");
                return;
            }

            if (current_mode == GlobalMode.Play)
            {
                MessageBox.Show("Impossible de lancer l'export en mode jeu ! passez en mode Edition");
                return;
            }

            //recherche fichier sln
            string[] solutionFiles = Directory.GetFiles(Settings.Default.rootFolder + @"\game_template\", "*.sln");

            if (solutionFiles.Length == 0)
            {
                MessageBox.Show("Aucune fichier .sln dans le répertoire game_template ! abandon...");
                return;
            }

            //recherche fichier de config
            if (!File.Exists(string.Format(@"{0}\game_template\assets\config_pc.lua", Settings.Default.rootFolder)))
            {
                MessageBox.Show("Aucune fichier config_pc.lua dans le répertoire d'assets ! abandon");
                return;
            }

            exportWebgl.SelectedPath = Settings.Default.rootFolder;

            //ouverture fenêtre destination
            if (exportWebgl.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                return;


            //-2- compilation emcc

            //affichage fenêtre export
            ExportLog exportlogwindow = new ExportLog();
            exportlogwindow.Text = "Export WebGL en cours...";

            exportlogwindow.Show();

            if (solutionFiles.Length > 1)
                exportlogwindow.AppendLog("Le répertoire game_template contient plus qu'un fichier .sln, le premier fichier " + solutionFiles[0] + " sera utilisé", ExportLogType.WARNING);

            exportlogwindow.AppendLog("Compilation du fichier projet...");
            ProcessStartInfo CompilationInfo = new ProcessStartInfo("cmd.exe", string.Format(@"/c MSBuild {0} /p:Configuration=Release /p:Platform=Emscripten", solutionFiles[0]));
            CompilationInfo.UseShellExecute = false;
            CompilationInfo.RedirectStandardOutput = true;

            if (!Settings.Default.exportskipbuild)
            {
                Process CompilationProc = Process.Start(CompilationInfo);
                CompilationProc.OutputDataReceived += (osender, oe) => exportlogwindow.AppendLog(oe.Data, threaded: true);
                CompilationProc.BeginOutputReadLine();

                while (!CompilationProc.HasExited)
                    Application.DoEvents();
            }
            else
            {
                exportlogwindow.AppendLog("Etape de compilation ignorée...");
            }


            int start_name = solutionFiles[0].LastIndexOf('\\') + 1;
            int end_name = solutionFiles[0].LastIndexOf('.');
            string export_file_name = solutionFiles[0].Substring(start_name, end_name - start_name);


            string temp_directory = string.Format(@"{0}\game_template\temp_emscripten", Settings.Default.rootFolder);

            if (Directory.Exists(temp_directory))
                Directory.Delete(temp_directory, true);

            Directory.CreateDirectory(temp_directory);
            Directory.CreateDirectory(temp_directory + @"\assets");
            Directory.CreateDirectory(temp_directory + @"\scripts");


            //-3- packaging fichier assets + config

            try
            {
                exportlogwindow.AppendLog("Copie fichier sauvegarde...");
                File.Copy(string.Format(@"{0}\game_template\assets\save.db", Settings.Default.rootFolder), string.Format(@"{0}\game_template\temp_emscripten\save.db", Settings.Default.rootFolder), true);
            }
            catch (Exception ex)
            {
                exportlogwindow.AppendLog("Erreur lors de la copie du fichier de sauvegarde " + ex.ToString(), ExportLogType.ERROR);
            }

            try
            {
                exportlogwindow.AppendLog("Copie fichier locale...");
                File.Copy(string.Format(@"{0}\game_template\assets\locale.db", Settings.Default.rootFolder), string.Format(@"{0}\game_template\temp_emscripten\locale.db", Settings.Default.rootFolder), true);
            }
            catch (Exception ex)
            {
                exportlogwindow.AppendLog("Erreur lors de la copie de locale " + ex.ToString(), ExportLogType.ERROR);
            }



            exportlogwindow.AppendLog("Exports assets et fichiers scripts...");

            ProcessStartInfo exportAssetsInfo = new ProcessStartInfo("cmd.exe", string.Format(@"/c {0}\animation_converter\AnimationConverter.exe {0}\game_template\assets\animated_assets {0}\game_template\assets {0}\game_template\temp_emscripten\assets {0}\game_template\temp_emscripten\scripts {0}\game_template\assets\exclude_pc.txt", Settings.Default.rootFolder));
            exportAssetsInfo.UseShellExecute = false;
            exportAssetsInfo.RedirectStandardOutput = true;

            Process exportAssetsProc = Process.Start(exportAssetsInfo);
            exportAssetsProc.OutputDataReceived += (osender, oe) => exportlogwindow.AppendLog(oe.Data, threaded: true);
            exportAssetsProc.BeginOutputReadLine();

            while (!exportAssetsProc.HasExited)
                Application.DoEvents();



            int packer_result = 0;

            //-4.1- packing texture si nécessaire
            if (Settings.Default.packtexture)
            {
                exportlogwindow.AppendLog("Packing texture...");

                string[] exclude = File.ReadAllLines(string.Format(@"{0}\game_template\assets\exclude_packer.txt", Settings.Default.rootFolder));

                List<string> list_exclude = new List<string>(exclude);

                packer_result = SpriteSheetPack.Pack(string.Format(@"{0}\game_template\temp_emscripten\assets\", Settings.Default.rootFolder), true, (Settings.Default.packtextureheight == Settings.Default.packtexturewidth), (int)Settings.Default.packtexturewidth, (int)Settings.Default.packtextureheight, 2, string.Format(@"{0}\game_template\temp_emscripten\assets\{1}", Settings.Default.rootFolder, "packtexture.png"), string.Format(@"{0}\game_template\temp_emscripten\assets\{1}", Settings.Default.rootFolder, "packtexture.pack"), true, list_exclude);

                if (packer_result != 0)
                {
                    exportlogwindow.AppendLog("Erreur lors du packing de texture ", ExportLogType.ERROR);
                }
            }

            try
            {
                exportlogwindow.AppendLog("Modification fichier config...");

                var text = File.ReadAllText(string.Format(@"{0}\game_template\assets\config_pc.lua", Settings.Default.rootFolder));

                int has_asset = text.IndexOf("asset_path");

                if (has_asset != -1)
                {
                    int start_asset = text.IndexOf('"', has_asset);
                    int end_asset = text.IndexOf('"', start_asset + 1) + 1;

                    text = text.Remove(start_asset, end_asset - start_asset);

                    text = text.Insert(start_asset, "\"assets\"");
                }

                int has_scripts = text.IndexOf("script_path");

                if (has_scripts != -1)
                {
                    int start_script = text.IndexOf('"', has_scripts);
                    int end_script = text.IndexOf('"', start_script + 1) + 1;

                    text = text.Remove(start_script, end_script - start_script);

                    text = text.Insert(start_script, "\"scripts\"");
                }

                if (Settings.Default.packtexture && packer_result == 0)
                {
                    int first_pos = text.IndexOf('{') + 1;

                    text = text.Insert(first_pos, "\npacker = \"packtexture.pack\",");
                }

                File.WriteAllText(string.Format(@"{0}\game_template\temp_emscripten\config.lua", Settings.Default.rootFolder), text);
            }
            catch (Exception ex)
            {
                exportlogwindow.AppendLog("Erreur lors de la modification du fichier de config " + ex.ToString(), ExportLogType.ERROR);
            }



            //-3.1- appel packager emscripten
            //recherche chemin emcc
            exportlogwindow.AppendLog("Création fichier data...");
            ProcessStartInfo findemccinfo = new ProcessStartInfo("cmd.exe", @"/c where emcc"); //which sous linux
            findemccinfo.UseShellExecute = false;
            findemccinfo.RedirectStandardOutput = true;

            string emcc_lines = string.Empty;

            Process findemccproc = Process.Start(findemccinfo);
            findemccproc.OutputDataReceived += (osender, oe) =>
            {
                if (string.IsNullOrEmpty(emcc_lines))
                    emcc_lines = oe.Data;
            };

            findemccproc.BeginOutputReadLine();
            findemccproc.WaitForExit();

            //récup cheminp
            string emscriptenpath = emcc_lines.Substring(0, emcc_lines.LastIndexOf("emcc"));

            //packing emscripten
            ProcessStartInfo packemscripteninfo = new ProcessStartInfo("cmd.exe", string.Format(@"/c python {0}\tools\file_packager.py {1}_assets.data --preload assets scripts save.db locale.db config.lua > {1}_assets.js", emscriptenpath, export_file_name));
            packemscripteninfo.WorkingDirectory = Settings.Default.rootFolder + @"\game_template\temp_emscripten";
            packemscripteninfo.UseShellExecute = false;
            packemscripteninfo.RedirectStandardOutput = true;

            Process packemscriptenproc = Process.Start(packemscripteninfo);
            packemscriptenproc.OutputDataReceived += (osender, oe) => exportlogwindow.AppendLog(oe.Data, threaded: true);
            packemscriptenproc.BeginOutputReadLine();

            while (!packemscriptenproc.HasExited)
                Application.DoEvents();

            //-4- calcul taille js et data générées + modification fichier template HTML 
            exportlogwindow.AppendLog("Calcul taille fichiers générés...");
            long game_size, data_size, game_gzip = 0, data_gzip = 0;

            FileInfo sizeinf = new FileInfo(Settings.Default.rootFolder + @"\game_template\Emscripten\Release\" + export_file_name + ".js");

            game_size = sizeinf.Length;

            sizeinf = new FileInfo(Settings.Default.rootFolder + @"\game_template\temp_emscripten\" + export_file_name + "_assets.data");
            data_size = sizeinf.Length;

            ProcessStartInfo gzinfo = new ProcessStartInfo("cmd.exe", string.Format(@"/c gzip -c {0}\game_template\Emscripten\Release\{1}.js | wc -c ", Settings.Default.rootFolder, export_file_name));
            gzinfo.UseShellExecute = false;
            gzinfo.RedirectStandardInput = true;
            gzinfo.RedirectStandardOutput = true;
            gzinfo.RedirectStandardError = true;

            Process gzproc = Process.Start(gzinfo);

            StreamReader myOutput = gzproc.StandardOutput;

            game_gzip = Convert.ToInt64(myOutput.ReadToEnd());

            gzproc.Close();


            gzinfo = new ProcessStartInfo("cmd.exe", string.Format(@"/c gzip -c {0}\game_template\temp_emscripten\{1}_assets.data | wc -c", Settings.Default.rootFolder, export_file_name));
            gzinfo.UseShellExecute = false;
            gzinfo.RedirectStandardOutput = true;

            gzproc = Process.Start(gzinfo);
            myOutput = gzproc.StandardOutput;

            data_gzip = Convert.ToInt64(myOutput.ReadToEnd());

            gzproc.Close();


            exportlogwindow.AppendLog("Création HTML depuis template...");

            StringBuilder html_template_content = new StringBuilder(Resources.emscripten_template);

            html_template_content.Replace("Template_game", Settings.Default.gamename);
            html_template_content.Replace("_HERE_GAME_SIZE_", game_size.ToString());
            html_template_content.Replace("_HERE_DATA_SIZE_", data_size.ToString());
            html_template_content.Replace("_HERE_GAME_GZSIZE_", game_gzip.ToString());
            html_template_content.Replace("_HERE_DATA_GZSIZE_", data_gzip.ToString());
            html_template_content.Replace("_HERE_GAMEJS_NAME_", "\"" + export_file_name + ".js\"");
            html_template_content.Replace("_HERE_DATA_NAME_", "\"" + export_file_name + "_assets.data\"");
            html_template_content.Replace("_HERE_ASSETS_LOADER_", "\"" + export_file_name + "_assets.js\"");



            //-5- copie vers répertoire de destination
            try
            {
                exportlogwindow.AppendLog("Copie des fichiers dans le répertoire de destination...");
                File.Copy(Settings.Default.rootFolder + @"\game_template\bin\astar_worker.js", exportWebgl.SelectedPath + @"\astar_worker.js", true);
                File.Copy(Settings.Default.rootFolder + @"\game_template\bin\astar_worker.js.mem", exportWebgl.SelectedPath + @"\astar_worker.js.mem", true);
                File.Copy(Settings.Default.rootFolder + @"\game_template\temp_emscripten\" + export_file_name + "_assets.data", exportWebgl.SelectedPath + @"\" + export_file_name + "_assets.data", true);
                File.Copy(Settings.Default.rootFolder + @"\game_template\temp_emscripten\" + export_file_name + "_assets.js", exportWebgl.SelectedPath + @"\" + export_file_name + "_assets.js", true);

                File.Copy(Settings.Default.rootFolder + @"\game_template\Emscripten\Release\" + export_file_name + ".js", exportWebgl.SelectedPath + @"\" + export_file_name + ".js", true);

                if (File.Exists(Settings.Default.rootFolder + @"\game_template\Emscripten\Release\" + export_file_name + ".html.mem"))
                    File.Copy(Settings.Default.rootFolder + @"\game_template\Emscripten\Release\" + export_file_name + ".html.mem", exportWebgl.SelectedPath + @"\" + export_file_name + ".html.mem", true);

                File.WriteAllText(exportWebgl.SelectedPath + @"\" + export_file_name + ".html", html_template_content.ToString());

                if (Directory.Exists(exportWebgl.SelectedPath + @"\style"))
                    Directory.Delete(exportWebgl.SelectedPath + @"\style", true);

                Directory.CreateDirectory(exportWebgl.SelectedPath + @"\style");

                if (Directory.Exists(exportWebgl.SelectedPath + @"\scripts"))
                    Directory.Delete(exportWebgl.SelectedPath + @"\scripts", true);

                Directory.CreateDirectory(exportWebgl.SelectedPath + @"\scripts");


                File.WriteAllText(exportWebgl.SelectedPath + @"\style\main.css", Resources.main);
                File.WriteAllText(exportWebgl.SelectedPath + @"\scripts\jquery-2.1.4.min.js", Resources.jquery_2_1_4_min);
                File.WriteAllText(exportWebgl.SelectedPath + @"\scripts\jquery.browser.min.js", Resources.jquery_browser_min);
                File.WriteAllText(exportWebgl.SelectedPath + @"\scripts\game_loader.js", Resources.game_loader);
            }
            catch (Exception ex)
            {
                exportlogwindow.AppendLog("Erreur lors de la copie des fichiers dans le répertoire de destination " + ex.ToString());
            }

            //suppression répertoire temporaires
            Directory.Delete(temp_directory, true);

            exportlogwindow.AppendLog("Export WebGL terminé !");
            exportlogwindow.Text = "Export WebGL terminé !";
        }



        #endregion


        private void renderPanel_OnSelectedItemChanged(string id, NativeFunc.GAME_TYPE? type)
        {
            TreeNode rootnode = null;

            txtselectedentity.Text = id;

            if (type.HasValue)
            {
                switch (type)
                {
                    case NativeFunc.GAME_TYPE.GT_ENTITY:
                        Entity cent = renderPanel.CurrentLayer.EntityArrays.Find(p => p.EntityId == id);

                        if (string.IsNullOrEmpty(cent.GroupId))
                            rootnode = ItemsList.Nodes["Nodeentity"];
                        else
                            rootnode = ItemsList.Nodes["Nodeentity"].Nodes["group_" + cent.GroupId];

                        break;
                    case NativeFunc.GAME_TYPE.GT_TRIGGER:
                        rootnode = ItemsList.Nodes["Nodetrigger"];
                        break;
                    case NativeFunc.GAME_TYPE.GT_TEXT:
                        rootnode = ItemsList.Nodes["NodeText"];
                        break;
                    case NativeFunc.GAME_TYPE.GT_SOUND:
                        rootnode = ItemsList.Nodes["NodeSound"];
                        break;
                    case NativeFunc.GAME_TYPE.GT_LIGHT:
                        rootnode = ItemsList.Nodes["NodeLight"];
                        break;
                    case NativeFunc.GAME_TYPE.GT_PARTICLES:
                        rootnode = ItemsList.Nodes["NodeParticles"];
                        break;
                    case NativeFunc.GAME_TYPE.GT_COLLIDER:
                        rootnode = ItemsList.Nodes["NodeCollider"];
                        break;
                    case NativeFunc.GAME_TYPE.GT_WAYPOINT:
                        rootnode = ItemsList.Nodes["NodeWaypoint"];
                        break;
                }
            }

            if (rootnode != null)
            {
                foreach (TreeNode childnode in rootnode.Nodes)
                {
                    if (childnode.Name == id)
                    {
                        ItemsList.SelectedNode = childnode;
                        break;
                    }
                }
            }
            else
            {
                ItemsList.SelectedNode = null;
            }

        }


        private void renderPanel_MouseDown(object sender, MouseEventArgs e)
        {
            if (current_mode == GlobalMode.Editor || current_mode == GlobalMode.FragEditor)
            {
                if (e.Button == System.Windows.Forms.MouseButtons.Right && NativeFunc.editor_has_entity_selected() == NativeFunc.wbool.wtrue && renderPanel.ToolSelected == Tools.Pencil)
                {
                    if (NativeFunc.editor_get_num_selection() == 1)
                    {
                        StringBuilder selected = new StringBuilder(1000);

                        NativeFunc.GAME_TYPE selected_type = NativeFunc.editor_entity_get_selection(selected, 0, selected.Capacity);

                        switch (selected_type)
                        {
                            case NativeFunc.GAME_TYPE.GT_TRIGGER:
                                ctxMenuTrigger.Show(renderPanel, e.Location);
                                break;
                            case NativeFunc.GAME_TYPE.GT_ENTITY:
                                ctxMenuEntity.Show(renderPanel, e.Location);
                                break;
                            case NativeFunc.GAME_TYPE.GT_TEXT:
                                ctxTextObject.Show(renderPanel, e.Location);
                                break;
                            case NativeFunc.GAME_TYPE.GT_SOUND:
                                ctxSoundSource.Show(renderPanel, e.Location);
                                break;
                            case NativeFunc.GAME_TYPE.GT_LIGHT:
                                ctxLightSource.Show(renderPanel, e.Location);
                                break;
                            case NativeFunc.GAME_TYPE.GT_PARTICLES:
                                ctxParticles.Show(renderPanel, e.Location);
                                break;
                            case NativeFunc.GAME_TYPE.GT_WAYPOINT:
                                ctxWaypoint.Show(renderPanel, e.Location);
                                break;
                            case NativeFunc.GAME_TYPE.GT_COLLIDER:
                                ctxCollider.Show(renderPanel, e.Location);
                                break;
                        }

                        //change selected item
                        renderPanel.RaiseSelectItemChangedEvent(selected.ToString(), selected_type);

                    }
                    else //multi sélection
                    {
                        ctxMultiSelection.Show(renderPanel, e.Location);
                    }

                }
            }

        }


        private void renderPanel_MouseMove(object sender, MouseEventArgs e)
        {
            int worldposx, worldposy;
            renderPanel.GetWorldPos(e.X, e.Y, out worldposx, out worldposy);
            int row_pos, col_pos;
            int tilepos = renderPanel.GetTilePos(e.X, e.Y, out row_pos, out col_pos);

            lblCursorPos.Text = string.Format("X : {0} Y : {1} Tile : {2}", worldposx, worldposy, tilepos);
            lblRowPos.Text = string.Format("Row : {0} Col : {1}", row_pos, col_pos);
        }

        private void chkcollisions_CheckedChanged(object sender, EventArgs e)
        {
            Settings.Default.draw_debug_collisions = chkcollisions.Checked;
            NativeFunc.editor_draw_debug_collisions((Settings.Default.draw_debug_collisions) ? NativeFunc.wbool.wtrue : NativeFunc.wbool.wfalse);
            Settings.Default.Save();
        }

        private void chkDrawallitems_CheckedChanged(object sender, EventArgs e)
        {
            Settings.Default.draw_colliders_trigger_misc = chkDrawallitems.Checked;
            NativeFunc.editor_draw_colliders_triggers_misc((Settings.Default.draw_colliders_trigger_misc) ? NativeFunc.wbool.wtrue : NativeFunc.wbool.wfalse);
            Settings.Default.Save();
        }


        private void chksnapgrid_CheckedChanged(object sender, EventArgs e)
        {
            Settings.Default.snap_to_grid = chksnapgrid.Checked;
            Settings.Default.Save();
        }

        private void trkCurrentFrame_Scroll(object sender, EventArgs e)
        {
            renderPanel.CurrentLayer.EditorCurrentFrame = trkCurrentFrame.Value;
        }

        private void trkCurrentFrame_ValueChanged(object sender, EventArgs e)
        {
            renderPanel.CurrentLayer.EditorCurrentFrame = trkCurrentFrame.Value;

            if (renderPanel.CurrentLayer.EditorCurrentFrame != 0)
                NativeFunc.editor_goto_frame((renderPanel.CurrentLayer.EditorCurrentFrame - 1), renderPanel.CurrentLayer.LayerId);
            else
                NativeFunc.editor_regen_textcoord(renderPanel.CurrentLayer.LayerId);

            numCurrentFrame.Value = trkCurrentFrame.Value;

            renderPanel.Invalidate();
        }

        private void numCurrentFrame_ValueChanged(object sender, EventArgs e)
        {
            trkCurrentFrame.Value = (int)numCurrentFrame.Value;
        }

        private void générerUneAnimationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (renderPanel.CurrentLayer.Animated)
            {
                Map currentmap = levelObj.MapArrays[tabMaps.SelectedIndex];

                AnimationGenerator ogen = new AnimationGenerator(renderPanel.ColCount * renderPanel.RowCount, trkCurrentFrame.Maximum, renderPanel, this, tileSets[currentmap.TilesetId], renderPanel.CurrentLayer.LayerId);

                ogen.Show();
            }
            else
            {
                MessageBox.Show("Impossible de générer une animation sur une couche non animée !");
            }
        }

        private void supprimerUneAnimationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (renderPanel.CurrentLayer.Animated)
            {
                AnimationCleaner oclean = new AnimationCleaner(renderPanel.ColCount * renderPanel.RowCount, trkCurrentFrame.Maximum, renderPanel, renderPanel.CurrentLayer.LayerId);
                oclean.Show();
            }
            else
            {
                MessageBox.Show("Impossible de supprimer un animation sur une couche non animée !");
            }
        }

        private void importerUnFondAniméToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AnimationImporter oimporter = new AnimationImporter(renderPanel.CurrentMap.LayerArrays, renderPanel.CurrentMap, tileSets, viewable_tileSets);

            if (oimporter.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (oimporter.Tag != null)
                {
                    Layer oLayer = oimporter.Tag as Layer;

                    TreeNode cNode = new TreeNode();
                    cNode.Checked = true;
                    cNode.Text = oLayer.LayerId;
                    cNode.Tag = oLayer;

                    calquesList.Nodes.Add(cNode);

                    renderPanel.CurrentMap.LayerArrays.Add(oLayer);

                    tilesetBox.Image = viewable_tileSets[oLayer.TilesetId];
                }
            }
        }

        private void effacerHistoriqueNiveauxToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Settings.Default.historique != null)
            {
                niveauxRécentsToolStripMenuItem.DropDownItems.Clear();
                niveauxRécentsToolStripMenuItem.Enabled = false;
                Settings.Default.historique.Clear();
                Settings.Default.Save();
            }
        }

        private void configWatcher_Changed(object sender, FileSystemEventArgs e)
        {
            //reload config
            NativeFunc.editor_reloadconfig();
        }

        private void renderPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        /// <summary>
        /// Fonction de vérification des ID world, affiche une fenêtre avec le récapitulatif des id à changer + bouton pour faire la modif en auto
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCheckIds_Click(object sender, EventArgs e)
        {
            if(current_mode != GlobalMode.WorldEditor)
            {
                MessageBox.Show("Impossible de vérifier les Ids en dehors du chargement d'un fichier monde !");
                return;
            }

            Dictionary<WorldKey, Worldcheck> comparedict = EditorUtilities.CheckWorld(worldObj);

            bool hasdoublon = false;

            foreach(KeyValuePair<WorldKey, Worldcheck> compare in comparedict)
            {
                if(compare.Value.mustchange)
                {
                    hasdoublon = true;
                    break;
                }
            }

            if(!hasdoublon)
            {
                MessageBox.Show("Aucun doublon trouvé sur les entités du monde !");
                return;
            }


            WorldCheckDialog dialog = new WorldCheckDialog(comparedict);

            dialog.ShowDialog();
        }

        public bool PreFilterMessage(ref Message m)
        {
            if (current_mode == GlobalMode.Play) //prevent key event to be dispatched to form in play mode
            {
                if (lua_comm != null && lua_comm.Visible && lua_comm.TextHasFocus())
                    return false;


                //post message to native handle
                if (m.Msg == (int)WM.KEYDOWN || m.Msg == (int)WM.KEYUP || m.Msg == (int)WM.CHAR
                    || m.Msg == (int)WM.LBUTTONDBLCLK || m.Msg == (int)WM.LBUTTONDOWN || m.Msg == (int)WM.LBUTTONUP
                    || m.Msg == (int)WM.MBUTTONDBLCLK || m.Msg == (int)WM.MBUTTONDOWN || m.Msg == (int)WM.MBUTTONUP)
                {

                    NativeFunc.editor_send_inputmessage((uint)m.Msg, (uint)m.WParam.ToInt32(), m.LParam.ToInt64());
                }


                switch (m.Msg)
                {
                    case (int)WM.KEYDOWN:
                        if ((Keys)m.WParam.ToInt32() == Keys.F5)
                        {
                            EventArgs e = new EventArgs();
                            btnChangeMode_Click(this, e);
                        }
                        else if ((Keys)m.WParam.ToInt32() == Keys.F1)
                        {
                            EventArgs e = new EventArgs();
                            consoleToolStripMenuItem_Click(this, e);
                        }

                        return true;

                    case (int)WM.KEYUP:
                    case (int)WM.CHAR:
                    case (int)WM.LBUTTONDBLCLK:
                    case (int)WM.LBUTTONDOWN:
                    case (int)WM.LBUTTONUP:
                    case (int)WM.MBUTTONDBLCLK:
                    case (int)WM.MBUTTONDOWN:
                    case (int)WM.MBUTTONUP:
                        return true;
                    default:
                        return false;
                }
            }

            return false;
        }






    }

    public class ResxInfo
    {
        public ResxType type
        {
            get;
            set;
        }

        public string ident
        {
            get;
            set;
        }

        public string path
        {
            get;
            set;
        }

        public ResxInfo(ResxType ptype, string pident, string ppath)
        {
            type = ptype;
            ident = pident;
            path = ppath;
        }
    }

    public class CalqueComparer : IComparer
    {
        /*RAPPEL : le résultat renvoyé par CompareTo est :
            * -1 objx est inférieur à objy
            * 0 objx est égal à objy
            * 1 objx*/
        public int Compare(object x, object y)
        {
            if (x == null)
                return -1;



            Layer lx = ((TreeNode)x).Tag as Layer;
            Layer ly = ((TreeNode)y).Tag as Layer;

            if (lx.EditorOrder < ly.EditorOrder)
                return -1;

            if (lx.EditorOrder == ly.EditorOrder)
                return 0;

            if (lx.EditorOrder > ly.EditorOrder)
                return 1;

            return 0;
        }
    }
}
