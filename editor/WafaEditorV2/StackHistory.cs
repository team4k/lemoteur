﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace WafaEditorV2
{
    public enum HistoryAction
    {
        SetTile,
        FloodTile,
        SetTriggerSize,
        SetMultiTile,
        SetPosition
    }

    public enum UndoRedo
    {
        Undo,
        Redo
    }

    //liste de références au différentes fonctions des actions
    public delegate void change_tile_delegate(int tile_id, int tilepos, string layer_id);
    public delegate void trigger_resize_delegate(Size trig_size, string trigger_id);
    public delegate void entity_setpos_delegate(Point pos,string entity_id);
    public delegate void change_multi_tile_delegate(int[] tiles_id,int start_tile,int[] rectangle_values,string layer_id);

    public class HistoryEntry
    {
        object OldValueElement;
        object[] ValueElements;
        HistoryAction TypeElement;
        Delegate entry_delegate;

        public HistoryAction pTypeElement
        {
            get
            {
                return TypeElement;
            }
        }

        public object[] pValueElements
        {
            get
            {
                return ValueElements;
            }
        }

        public HistoryEntry(object[] value,object oldvalue, HistoryAction action,Delegate func)
        {
            ValueElements = value;
            TypeElement = action;
            OldValueElement = oldvalue;
            entry_delegate = func;
        }

        public void UndoRedo(UndoRedo value)
        {
            object setElement = null;
            object unsetElement = null;

            if (value == WafaEditorV2.UndoRedo.Undo)
            {
                setElement = OldValueElement;
                unsetElement = ValueElements[0];
            }
            else if (value == WafaEditorV2.UndoRedo.Redo)
            {
                setElement = ValueElements[0];
                unsetElement = OldValueElement;
            }

            switch (TypeElement)
            {
                case HistoryAction.SetTile:
                    entry_delegate.DynamicInvoke((int)setElement, (int)ValueElements[1], (string)ValueElements[2]);
                    break;
                case HistoryAction.FloodTile:
                    int[] tiles_elements = (int[])ValueElements[1];

                    foreach (int tile in tiles_elements)
                        entry_delegate.DynamicInvoke((int)setElement, tile, (string)ValueElements[2]);

                    break;
                case HistoryAction.SetTriggerSize:
                    entry_delegate.DynamicInvoke((Size)setElement, (string)ValueElements[1]);
                    break;
                case HistoryAction.SetPosition:
                    entry_delegate.DynamicInvoke((Point)setElement, (string)ValueElements[1]);
                    break;
                case HistoryAction.SetMultiTile:
                    entry_delegate.DynamicInvoke((int[])setElement, (int)ValueElements[1], (int[])ValueElements[2], (string)ValueElements[3]);
                    break;
            }
        }

    }

    public class StackHistory
    {
        List<HistoryEntry> _stack;
        int current_index = 0;

        public delegate void redo_event();
        public delegate void undo_event();
        public delegate void push_event();
        public delegate void remove_event();

        public event redo_event OnRedo;
        public event undo_event OnUndo;
        public event push_event OnPush;
        public event remove_event OnRemove;
        

        public bool CanUndo
        {
            get
            {
                return (current_index < _stack.Count);
            }
        }

        public bool CanRedo
        {
            get
            {
                return (current_index > 0);
            }
        }

        public StackHistory(int max_stack_count)
        {
            _stack = new List<HistoryEntry>(max_stack_count);
            current_index = 0;
        }

        public void Push(HistoryEntry entry)
        {
            //suppression de toutes les valeur qui sont avant le current index
            while (current_index > 0)
                _stack.RemoveAt(--current_index);

            _stack.Insert(0, entry);
            current_index = 0;

            //suppression de la valeur la plus ancienne si on dépasse la capacité maximum
            if (_stack.Count == _stack.Capacity)
                _stack.RemoveAt(_stack.Count - 1);

            if (OnPush != null)
                OnPush();
        }

        public void UndoAction()
        {
            if (!this.CanUndo)
                return;

            HistoryEntry entry =  _stack[this.current_index];

            entry.UndoRedo(UndoRedo.Undo);
            current_index++;

            if (this.OnUndo != null)
                this.OnUndo();
        }

        public void RedoAction()
        {
            if (!this.CanRedo)
                return;

            current_index--;
            HistoryEntry entry = _stack[this.current_index];

            entry.UndoRedo(UndoRedo.Redo);

            if (this.OnRedo != null)
                this.OnRedo();

        }

        public void Reset()
        {
            _stack.Clear();
            current_index = 0;

            if (OnRemove != null)
                OnRemove();
        }

        public void RemoveActionFromID(string id, HistoryAction type_action)
        {
            List<HistoryEntry> rm_list = (from u in _stack where u.pTypeElement == type_action select u).ToList<HistoryEntry>();

            if (rm_list.Count > 0)
            {
                bool has_removed = false;
                switch (type_action)
                {
                    case HistoryAction.SetTile:
                    case HistoryAction.FloodTile:
                        foreach (HistoryEntry entry in rm_list)
                        {
                            if ((string)entry.pValueElements[2] == id)
                            {
                                _stack.Remove(entry);
                                has_removed = true;
                            }
                        }
                        break;
                    case HistoryAction.SetPosition:
                        foreach (HistoryEntry entry in rm_list)
                        {
                            if ((string)entry.pValueElements[1] == id)
                            {
                                _stack.Remove(entry);
                                has_removed = true;
                            }
                        }
                        break;
                }

                if (has_removed)
                {
                    current_index = 0;

                    if (OnRemove != null)
                        OnRemove();
                }
            }
        }




    }
}
