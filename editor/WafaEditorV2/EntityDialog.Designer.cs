﻿namespace WafaEditorV2
{
    partial class EntityDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EntityDialog));
            this.txtCtor = new System.Windows.Forms.TextBox();
            this.lblCtor = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.lblTileset = new System.Windows.Forms.Label();
            this.lblAnim = new System.Windows.Forms.Label();
            this.lblScript = new System.Windows.Forms.Label();
            this.txtidentity = new System.Windows.Forms.TextBox();
            this.lblidentity = new System.Windows.Forms.Label();
            this.numwidth = new System.Windows.Forms.NumericUpDown();
            this.numheight = new System.Windows.Forms.NumericUpDown();
            this.lblwidth = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblheight = new System.Windows.Forms.Label();
            this.chkController = new System.Windows.Forms.CheckBox();
            this.chkCallback = new System.Windows.Forms.CheckBox();
            this.numOffsetx = new System.Windows.Forms.NumericUpDown();
            this.lbloffy = new System.Windows.Forms.Label();
            this.numOffsetY = new System.Windows.Forms.NumericUpDown();
            this.lbloffx = new System.Windows.Forms.Label();
            this.numRepeatX = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.numRepeatY = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblstartframe = new System.Windows.Forms.Label();
            this.numStartFrame = new System.Windows.Forms.NumericUpDown();
            this.cmbResxTextures = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cmbGroup = new System.Windows.Forms.ComboBox();
            this.cmbAnim = new System.Windows.Forms.ComboBox();
            this.txtProgramShader = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.btnNewScript = new System.Windows.Forms.Button();
            this.cmbScript = new System.Windows.Forms.ComboBox();
            this.tabentite = new System.Windows.Forms.TabControl();
            this.tabBaseData = new System.Windows.Forms.TabPage();
            this.chkdupliquetexture = new System.Windows.Forms.CheckBox();
            this.chknodraw = new System.Windows.Forms.CheckBox();
            this.chk_script_init_only = new System.Windows.Forms.CheckBox();
            this.lblspritecount = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.numSprite = new System.Windows.Forms.NumericUpDown();
            this.cmb3D = new System.Windows.Forms.ComboBox();
            this.chk3D = new System.Windows.Forms.CheckBox();
            this.tabCollisions = new System.Windows.Forms.TabPage();
            this.panelMultiCol = new System.Windows.Forms.Panel();
            this.lblindexcol = new System.Windows.Forms.Label();
            this.numIndexCol = new System.Windows.Forms.NumericUpDown();
            this.btnSupprCollision = new System.Windows.Forms.Button();
            this.btnNextCol = new System.Windows.Forms.Button();
            this.btnPrevCol = new System.Windows.Forms.Button();
            this.btnajoutmulticol = new System.Windows.Forms.Button();
            this.cmbTypmcol = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.grpMulticol = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.numwidthmcol = new System.Windows.Forms.NumericUpDown();
            this.numOffxmcol = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.numHeightmcol = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.numOffymcol = new System.Windows.Forms.NumericUpDown();
            this.panelNormalCol = new System.Windows.Forms.Panel();
            this.numColRadius = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.chkCustomCol = new System.Windows.Forms.CheckBox();
            this.grpparallax = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.numparallaxx = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.numparallaxy = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.chkHasCollisions = new System.Windows.Forms.CheckBox();
            this.grpCustomCollisions = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.numColWidth = new System.Windows.Forms.NumericUpDown();
            this.lblColHeight = new System.Windows.Forms.Label();
            this.numColHeight = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.lblColWidth = new System.Windows.Forms.Label();
            this.chkKinematicBody = new System.Windows.Forms.CheckBox();
            this.lblshapetype = new System.Windows.Forms.Label();
            this.cbShapeType = new System.Windows.Forms.ComboBox();
            this.tabShadersParams = new System.Windows.Forms.TabPage();
            this.panelInt = new System.Windows.Forms.Panel();
            this.numUniformInt = new System.Windows.Forms.NumericUpDown();
            this.label26 = new System.Windows.Forms.Label();
            this.panelConstantes = new System.Windows.Forms.Panel();
            this.txtExtraParam = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.cmbConst = new System.Windows.Forms.ComboBox();
            this.panelLumiere = new System.Windows.Forms.Panel();
            this.chkLightNormalize = new System.Windows.Forms.CheckBox();
            this.label30 = new System.Windows.Forms.Label();
            this.cmbLightList = new System.Windows.Forms.ComboBox();
            this.panelTexture = new System.Windows.Forms.Panel();
            this.chkSize = new System.Windows.Forms.CheckBox();
            this.chkSetOffset = new System.Windows.Forms.CheckBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.numSlot = new System.Windows.Forms.NumericUpDown();
            this.cmbTexture = new System.Windows.Forms.ComboBox();
            this.btnSupprParam = new System.Windows.Forms.Button();
            this.btnNextParam = new System.Windows.Forms.Button();
            this.btnPrevParam = new System.Windows.Forms.Button();
            this.BtnAjoutParam = new System.Windows.Forms.Button();
            this.panelFloat = new System.Windows.Forms.Panel();
            this.numComponents = new System.Windows.Forms.NumericUpDown();
            this.lblparamw = new System.Windows.Forms.Label();
            this.numWA = new System.Windows.Forms.NumericUpDown();
            this.lblparamz = new System.Windows.Forms.Label();
            this.numZB = new System.Windows.Forms.NumericUpDown();
            this.lblparamy = new System.Windows.Forms.Label();
            this.numYG = new System.Windows.Forms.NumericUpDown();
            this.lblparamx = new System.Windows.Forms.Label();
            this.numXR = new System.Windows.Forms.NumericUpDown();
            this.cmbParamsType = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.txtParamName = new System.Windows.Forms.TextBox();
            this.tabPass = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkWorld = new System.Windows.Forms.CheckBox();
            this.chkProj = new System.Windows.Forms.CheckBox();
            this.chkView = new System.Windows.Forms.CheckBox();
            this.lblzindxpass = new System.Windows.Forms.Label();
            this.numzindexpass = new System.Windows.Forms.NumericUpDown();
            this.SupprPass = new System.Windows.Forms.Button();
            this.btnAddpass = new System.Windows.Forms.Button();
            this.listExtraPass = new System.Windows.Forms.ListBox();
            this.btnCopycollisions = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numwidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numheight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numOffsetx)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numOffsetY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRepeatX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRepeatY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numStartFrame)).BeginInit();
            this.tabentite.SuspendLayout();
            this.tabBaseData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSprite)).BeginInit();
            this.tabCollisions.SuspendLayout();
            this.panelMultiCol.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numIndexCol)).BeginInit();
            this.grpMulticol.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numwidthmcol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numOffxmcol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHeightmcol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numOffymcol)).BeginInit();
            this.panelNormalCol.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numColRadius)).BeginInit();
            this.grpparallax.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numparallaxx)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numparallaxy)).BeginInit();
            this.grpCustomCollisions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numColWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numColHeight)).BeginInit();
            this.tabShadersParams.SuspendLayout();
            this.panelInt.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUniformInt)).BeginInit();
            this.panelConstantes.SuspendLayout();
            this.panelLumiere.SuspendLayout();
            this.panelTexture.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSlot)).BeginInit();
            this.panelFloat.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numComponents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numZB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numYG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numXR)).BeginInit();
            this.tabPass.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numzindexpass)).BeginInit();
            this.SuspendLayout();
            // 
            // txtCtor
            // 
            this.txtCtor.Location = new System.Drawing.Point(194, 111);
            this.txtCtor.Name = "txtCtor";
            this.txtCtor.Size = new System.Drawing.Size(289, 20);
            this.txtCtor.TabIndex = 3;
            // 
            // lblCtor
            // 
            this.lblCtor.AutoSize = true;
            this.lblCtor.Location = new System.Drawing.Point(81, 118);
            this.lblCtor.Name = "lblCtor";
            this.lblCtor.Size = new System.Drawing.Size(67, 13);
            this.lblCtor.TabIndex = 18;
            this.lblCtor.Text = "Constructeur";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(240, 682);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 22);
            this.btnCancel.TabIndex = 15;
            this.btnCancel.Text = "Annuler";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(142, 682);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(80, 22);
            this.btnOk.TabIndex = 14;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // lblTileset
            // 
            this.lblTileset.AutoSize = true;
            this.lblTileset.Location = new System.Drawing.Point(80, 179);
            this.lblTileset.Name = "lblTileset";
            this.lblTileset.Size = new System.Drawing.Size(68, 13);
            this.lblTileset.TabIndex = 20;
            this.lblTileset.Text = "Fichier tileset";
            // 
            // lblAnim
            // 
            this.lblAnim.AutoSize = true;
            this.lblAnim.Location = new System.Drawing.Point(73, 212);
            this.lblAnim.Name = "lblAnim";
            this.lblAnim.Size = new System.Drawing.Size(101, 13);
            this.lblAnim.TabIndex = 19;
            this.lblAnim.Text = "Animation de départ";
            // 
            // lblScript
            // 
            this.lblScript.AutoSize = true;
            this.lblScript.Location = new System.Drawing.Point(80, 86);
            this.lblScript.Name = "lblScript";
            this.lblScript.Size = new System.Drawing.Size(59, 13);
            this.lblScript.TabIndex = 17;
            this.lblScript.Text = "Module lua";
            // 
            // txtidentity
            // 
            this.txtidentity.Location = new System.Drawing.Point(194, 22);
            this.txtidentity.Name = "txtidentity";
            this.txtidentity.Size = new System.Drawing.Size(158, 20);
            this.txtidentity.TabIndex = 0;
            // 
            // lblidentity
            // 
            this.lblidentity.AutoSize = true;
            this.lblidentity.Location = new System.Drawing.Point(81, 25);
            this.lblidentity.Name = "lblidentity";
            this.lblidentity.Size = new System.Drawing.Size(82, 13);
            this.lblidentity.TabIndex = 16;
            this.lblidentity.Text = "Identifiant entité";
            // 
            // numwidth
            // 
            this.numwidth.Location = new System.Drawing.Point(194, 274);
            this.numwidth.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numwidth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numwidth.Name = "numwidth";
            this.numwidth.Size = new System.Drawing.Size(128, 20);
            this.numwidth.TabIndex = 7;
            this.numwidth.Value = new decimal(new int[] {
            32,
            0,
            0,
            0});
            // 
            // numheight
            // 
            this.numheight.Location = new System.Drawing.Point(193, 300);
            this.numheight.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numheight.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numheight.Name = "numheight";
            this.numheight.Size = new System.Drawing.Size(128, 20);
            this.numheight.TabIndex = 8;
            this.numheight.Value = new decimal(new int[] {
            32,
            0,
            0,
            0});
            // 
            // lblwidth
            // 
            this.lblwidth.AutoSize = true;
            this.lblwidth.Location = new System.Drawing.Point(74, 275);
            this.lblwidth.Name = "lblwidth";
            this.lblwidth.Size = new System.Drawing.Size(43, 13);
            this.lblwidth.TabIndex = 21;
            this.lblwidth.Text = "Largeur";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(74, 420);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 43;
            // 
            // lblheight
            // 
            this.lblheight.AutoSize = true;
            this.lblheight.Location = new System.Drawing.Point(73, 304);
            this.lblheight.Name = "lblheight";
            this.lblheight.Size = new System.Drawing.Size(45, 13);
            this.lblheight.TabIndex = 22;
            this.lblheight.Text = "Hauteur";
            // 
            // chkController
            // 
            this.chkController.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkController.Location = new System.Drawing.Point(54, 499);
            this.chkController.Name = "chkController";
            this.chkController.Size = new System.Drawing.Size(154, 24);
            this.chkController.TabIndex = 11;
            this.chkController.Text = "Avec controleur";
            this.chkController.UseVisualStyleBackColor = true;
            // 
            // chkCallback
            // 
            this.chkCallback.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkCallback.Location = new System.Drawing.Point(18, 513);
            this.chkCallback.Name = "chkCallback";
            this.chkCallback.Size = new System.Drawing.Size(190, 36);
            this.chkCallback.TabIndex = 13;
            this.chkCallback.Text = "Avec fonction de rappel (callback)";
            this.chkCallback.UseVisualStyleBackColor = true;
            // 
            // numOffsetx
            // 
            this.numOffsetx.Location = new System.Drawing.Point(194, 422);
            this.numOffsetx.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numOffsetx.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numOffsetx.Name = "numOffsetx";
            this.numOffsetx.Size = new System.Drawing.Size(128, 20);
            this.numOffsetx.TabIndex = 54;
            // 
            // lbloffy
            // 
            this.lbloffy.AutoSize = true;
            this.lbloffy.Location = new System.Drawing.Point(70, 448);
            this.lbloffy.Name = "lbloffy";
            this.lbloffy.Size = new System.Drawing.Size(73, 13);
            this.lbloffy.TabIndex = 57;
            this.lbloffy.Text = "Sprite offset Y";
            // 
            // numOffsetY
            // 
            this.numOffsetY.Location = new System.Drawing.Point(194, 446);
            this.numOffsetY.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numOffsetY.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numOffsetY.Name = "numOffsetY";
            this.numOffsetY.Size = new System.Drawing.Size(128, 20);
            this.numOffsetY.TabIndex = 55;
            // 
            // lbloffx
            // 
            this.lbloffx.AutoSize = true;
            this.lbloffx.Location = new System.Drawing.Point(70, 424);
            this.lbloffx.Name = "lbloffx";
            this.lbloffx.Size = new System.Drawing.Size(73, 13);
            this.lbloffx.TabIndex = 56;
            this.lbloffx.Text = "Sprite offset X";
            // 
            // numRepeatX
            // 
            this.numRepeatX.Location = new System.Drawing.Point(194, 346);
            this.numRepeatX.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.numRepeatX.Name = "numRepeatX";
            this.numRepeatX.Size = new System.Drawing.Size(128, 20);
            this.numRepeatX.TabIndex = 59;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(83, 372);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 62;
            this.label1.Text = "dupliquer en Y";
            // 
            // numRepeatY
            // 
            this.numRepeatY.Location = new System.Drawing.Point(194, 370);
            this.numRepeatY.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.numRepeatY.Name = "numRepeatY";
            this.numRepeatY.Size = new System.Drawing.Size(128, 20);
            this.numRepeatY.TabIndex = 60;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(84, 353);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 61;
            this.label4.Text = "dupliquer en X";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(78, 344);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 13);
            this.label5.TabIndex = 58;
            // 
            // lblstartframe
            // 
            this.lblstartframe.AutoSize = true;
            this.lblstartframe.Location = new System.Drawing.Point(74, 325);
            this.lblstartframe.Name = "lblstartframe";
            this.lblstartframe.Size = new System.Drawing.Size(84, 13);
            this.lblstartframe.TabIndex = 63;
            this.lblstartframe.Text = "Frame de départ";
            // 
            // numStartFrame
            // 
            this.numStartFrame.Location = new System.Drawing.Point(193, 324);
            this.numStartFrame.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numStartFrame.Name = "numStartFrame";
            this.numStartFrame.Size = new System.Drawing.Size(128, 20);
            this.numStartFrame.TabIndex = 64;
            // 
            // cmbResxTextures
            // 
            this.cmbResxTextures.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbResxTextures.FormattingEnabled = true;
            this.cmbResxTextures.Location = new System.Drawing.Point(194, 179);
            this.cmbResxTextures.Name = "cmbResxTextures";
            this.cmbResxTextures.Size = new System.Drawing.Size(158, 21);
            this.cmbResxTextures.TabIndex = 65;
            this.cmbResxTextures.SelectedIndexChanged += new System.EventHandler(this.cmbResxTextures_SelectedIndexChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(81, 53);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(89, 13);
            this.label11.TabIndex = 67;
            this.label11.Text = "Identifiant groupe";
            // 
            // cmbGroup
            // 
            this.cmbGroup.FormattingEnabled = true;
            this.cmbGroup.Location = new System.Drawing.Point(194, 50);
            this.cmbGroup.Name = "cmbGroup";
            this.cmbGroup.Size = new System.Drawing.Size(158, 21);
            this.cmbGroup.TabIndex = 68;
            // 
            // cmbAnim
            // 
            this.cmbAnim.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAnim.FormattingEnabled = true;
            this.cmbAnim.Location = new System.Drawing.Point(193, 209);
            this.cmbAnim.Name = "cmbAnim";
            this.cmbAnim.Size = new System.Drawing.Size(158, 21);
            this.cmbAnim.TabIndex = 69;
            // 
            // txtProgramShader
            // 
            this.txtProgramShader.Location = new System.Drawing.Point(193, 243);
            this.txtProgramShader.Name = "txtProgramShader";
            this.txtProgramShader.Size = new System.Drawing.Size(158, 20);
            this.txtProgramShader.TabIndex = 70;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(79, 249);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(95, 13);
            this.label12.TabIndex = 71;
            this.label12.Text = "Programme shader";
            // 
            // btnNewScript
            // 
            this.btnNewScript.Image = ((System.Drawing.Image)(resources.GetObject("btnNewScript.Image")));
            this.btnNewScript.Location = new System.Drawing.Point(362, 76);
            this.btnNewScript.Name = "btnNewScript";
            this.btnNewScript.Size = new System.Drawing.Size(26, 24);
            this.btnNewScript.TabIndex = 72;
            this.btnNewScript.UseVisualStyleBackColor = true;
            this.btnNewScript.Visible = false;
            this.btnNewScript.Click += new System.EventHandler(this.btnNewScript_Click);
            // 
            // cmbScript
            // 
            this.cmbScript.FormattingEnabled = true;
            this.cmbScript.Location = new System.Drawing.Point(194, 81);
            this.cmbScript.Name = "cmbScript";
            this.cmbScript.Size = new System.Drawing.Size(157, 21);
            this.cmbScript.TabIndex = 73;
            this.cmbScript.TextUpdate += new System.EventHandler(this.cmbScript_TextUpdate);
            // 
            // tabentite
            // 
            this.tabentite.Controls.Add(this.tabBaseData);
            this.tabentite.Controls.Add(this.tabCollisions);
            this.tabentite.Controls.Add(this.tabShadersParams);
            this.tabentite.Controls.Add(this.tabPass);
            this.tabentite.Location = new System.Drawing.Point(13, 12);
            this.tabentite.Name = "tabentite";
            this.tabentite.SelectedIndex = 0;
            this.tabentite.Size = new System.Drawing.Size(543, 655);
            this.tabentite.TabIndex = 74;
            // 
            // tabBaseData
            // 
            this.tabBaseData.Controls.Add(this.chkdupliquetexture);
            this.tabBaseData.Controls.Add(this.chknodraw);
            this.tabBaseData.Controls.Add(this.chk_script_init_only);
            this.tabBaseData.Controls.Add(this.lblspritecount);
            this.tabBaseData.Controls.Add(this.label23);
            this.tabBaseData.Controls.Add(this.numSprite);
            this.tabBaseData.Controls.Add(this.cmb3D);
            this.tabBaseData.Controls.Add(this.chk3D);
            this.tabBaseData.Controls.Add(this.lblidentity);
            this.tabBaseData.Controls.Add(this.cmbScript);
            this.tabBaseData.Controls.Add(this.txtidentity);
            this.tabBaseData.Controls.Add(this.btnNewScript);
            this.tabBaseData.Controls.Add(this.lblScript);
            this.tabBaseData.Controls.Add(this.txtProgramShader);
            this.tabBaseData.Controls.Add(this.lblAnim);
            this.tabBaseData.Controls.Add(this.label12);
            this.tabBaseData.Controls.Add(this.lblTileset);
            this.tabBaseData.Controls.Add(this.cmbAnim);
            this.tabBaseData.Controls.Add(this.lblCtor);
            this.tabBaseData.Controls.Add(this.cmbGroup);
            this.tabBaseData.Controls.Add(this.txtCtor);
            this.tabBaseData.Controls.Add(this.label11);
            this.tabBaseData.Controls.Add(this.numwidth);
            this.tabBaseData.Controls.Add(this.numheight);
            this.tabBaseData.Controls.Add(this.cmbResxTextures);
            this.tabBaseData.Controls.Add(this.lblwidth);
            this.tabBaseData.Controls.Add(this.numStartFrame);
            this.tabBaseData.Controls.Add(this.label2);
            this.tabBaseData.Controls.Add(this.lblstartframe);
            this.tabBaseData.Controls.Add(this.lblheight);
            this.tabBaseData.Controls.Add(this.numRepeatX);
            this.tabBaseData.Controls.Add(this.label1);
            this.tabBaseData.Controls.Add(this.numRepeatY);
            this.tabBaseData.Controls.Add(this.label4);
            this.tabBaseData.Controls.Add(this.label5);
            this.tabBaseData.Controls.Add(this.chkController);
            this.tabBaseData.Controls.Add(this.numOffsetx);
            this.tabBaseData.Controls.Add(this.chkCallback);
            this.tabBaseData.Controls.Add(this.lbloffy);
            this.tabBaseData.Controls.Add(this.lbloffx);
            this.tabBaseData.Controls.Add(this.numOffsetY);
            this.tabBaseData.Location = new System.Drawing.Point(4, 22);
            this.tabBaseData.Name = "tabBaseData";
            this.tabBaseData.Padding = new System.Windows.Forms.Padding(3);
            this.tabBaseData.Size = new System.Drawing.Size(535, 629);
            this.tabBaseData.TabIndex = 0;
            this.tabBaseData.Text = "données entitée";
            this.tabBaseData.UseVisualStyleBackColor = true;
            // 
            // chkdupliquetexture
            // 
            this.chkdupliquetexture.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkdupliquetexture.Checked = true;
            this.chkdupliquetexture.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkdupliquetexture.Location = new System.Drawing.Point(54, 393);
            this.chkdupliquetexture.Name = "chkdupliquetexture";
            this.chkdupliquetexture.Size = new System.Drawing.Size(154, 24);
            this.chkdupliquetexture.TabIndex = 79;
            this.chkdupliquetexture.Text = "dupliquer texture";
            this.chkdupliquetexture.UseVisualStyleBackColor = true;
            // 
            // chknodraw
            // 
            this.chknodraw.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chknodraw.Location = new System.Drawing.Point(6, 564);
            this.chknodraw.Name = "chknodraw";
            this.chknodraw.Size = new System.Drawing.Size(202, 24);
            this.chknodraw.TabIndex = 78;
            this.chknodraw.Text = "Invisble (pas de rendu graphique)";
            this.chknodraw.UseVisualStyleBackColor = true;
            // 
            // chk_script_init_only
            // 
            this.chk_script_init_only.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chk_script_init_only.Location = new System.Drawing.Point(50, 134);
            this.chk_script_init_only.Name = "chk_script_init_only";
            this.chk_script_init_only.Size = new System.Drawing.Size(158, 39);
            this.chk_script_init_only.TabIndex = 77;
            this.chk_script_init_only.Text = "Initialisation uniquement (pas d\'objet lua créé)";
            this.chk_script_init_only.UseVisualStyleBackColor = true;
            // 
            // lblspritecount
            // 
            this.lblspritecount.AutoSize = true;
            this.lblspritecount.Location = new System.Drawing.Point(70, 472);
            this.lblspritecount.Name = "lblspritecount";
            this.lblspritecount.Size = new System.Drawing.Size(87, 13);
            this.lblspritecount.TabIndex = 76;
            this.lblspritecount.Text = "Nombre de sprite";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(70, 594);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(68, 13);
            this.label23.TabIndex = 75;
            this.label23.Text = "Template 3D";
            // 
            // numSprite
            // 
            this.numSprite.Location = new System.Drawing.Point(194, 471);
            this.numSprite.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numSprite.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numSprite.Name = "numSprite";
            this.numSprite.Size = new System.Drawing.Size(128, 20);
            this.numSprite.TabIndex = 75;
            this.numSprite.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // cmb3D
            // 
            this.cmb3D.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb3D.Enabled = false;
            this.cmb3D.FormattingEnabled = true;
            this.cmb3D.Location = new System.Drawing.Point(184, 594);
            this.cmb3D.Name = "cmb3D";
            this.cmb3D.Size = new System.Drawing.Size(158, 21);
            this.cmb3D.TabIndex = 76;
            // 
            // chk3D
            // 
            this.chk3D.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chk3D.Location = new System.Drawing.Point(43, 542);
            this.chk3D.Name = "chk3D";
            this.chk3D.Size = new System.Drawing.Size(165, 24);
            this.chk3D.TabIndex = 74;
            this.chk3D.Text = "Utiliser un objet 3D";
            this.chk3D.UseVisualStyleBackColor = true;
            this.chk3D.CheckedChanged += new System.EventHandler(this.chk3D_CheckedChanged);
            // 
            // tabCollisions
            // 
            this.tabCollisions.Controls.Add(this.btnCopycollisions);
            this.tabCollisions.Controls.Add(this.panelMultiCol);
            this.tabCollisions.Controls.Add(this.panelNormalCol);
            this.tabCollisions.Controls.Add(this.chkKinematicBody);
            this.tabCollisions.Controls.Add(this.lblshapetype);
            this.tabCollisions.Controls.Add(this.cbShapeType);
            this.tabCollisions.Location = new System.Drawing.Point(4, 22);
            this.tabCollisions.Name = "tabCollisions";
            this.tabCollisions.Padding = new System.Windows.Forms.Padding(3);
            this.tabCollisions.Size = new System.Drawing.Size(535, 629);
            this.tabCollisions.TabIndex = 1;
            this.tabCollisions.Text = "Collisions";
            this.tabCollisions.UseVisualStyleBackColor = true;
            // 
            // panelMultiCol
            // 
            this.panelMultiCol.Controls.Add(this.lblindexcol);
            this.panelMultiCol.Controls.Add(this.numIndexCol);
            this.panelMultiCol.Controls.Add(this.btnSupprCollision);
            this.panelMultiCol.Controls.Add(this.btnNextCol);
            this.panelMultiCol.Controls.Add(this.btnPrevCol);
            this.panelMultiCol.Controls.Add(this.btnajoutmulticol);
            this.panelMultiCol.Controls.Add(this.cmbTypmcol);
            this.panelMultiCol.Controls.Add(this.label14);
            this.panelMultiCol.Controls.Add(this.grpMulticol);
            this.panelMultiCol.Location = new System.Drawing.Point(23, 87);
            this.panelMultiCol.Name = "panelMultiCol";
            this.panelMultiCol.Size = new System.Drawing.Size(429, 238);
            this.panelMultiCol.TabIndex = 75;
            this.panelMultiCol.Visible = false;
            // 
            // lblindexcol
            // 
            this.lblindexcol.AutoSize = true;
            this.lblindexcol.Location = new System.Drawing.Point(3, 16);
            this.lblindexcol.Name = "lblindexcol";
            this.lblindexcol.Size = new System.Drawing.Size(42, 13);
            this.lblindexcol.TabIndex = 73;
            this.lblindexcol.Text = "Index : ";
            // 
            // numIndexCol
            // 
            this.numIndexCol.Location = new System.Drawing.Point(46, 11);
            this.numIndexCol.Name = "numIndexCol";
            this.numIndexCol.Size = new System.Drawing.Size(62, 20);
            this.numIndexCol.TabIndex = 72;
            this.numIndexCol.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numIndexCol_KeyPress);
            // 
            // btnSupprCollision
            // 
            this.btnSupprCollision.Location = new System.Drawing.Point(9, 110);
            this.btnSupprCollision.Name = "btnSupprCollision";
            this.btnSupprCollision.Size = new System.Drawing.Size(114, 22);
            this.btnSupprCollision.TabIndex = 71;
            this.btnSupprCollision.Text = "Suppr collision";
            this.btnSupprCollision.UseVisualStyleBackColor = true;
            this.btnSupprCollision.Click += new System.EventHandler(this.btnSupprCollision_Click);
            // 
            // btnNextCol
            // 
            this.btnNextCol.Enabled = false;
            this.btnNextCol.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnNextCol.Location = new System.Drawing.Point(82, 37);
            this.btnNextCol.Name = "btnNextCol";
            this.btnNextCol.Size = new System.Drawing.Size(26, 22);
            this.btnNextCol.TabIndex = 70;
            this.btnNextCol.Text = "►";
            this.btnNextCol.UseVisualStyleBackColor = true;
            this.btnNextCol.Click += new System.EventHandler(this.btnNextCol_Click);
            // 
            // btnPrevCol
            // 
            this.btnPrevCol.Enabled = false;
            this.btnPrevCol.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnPrevCol.Location = new System.Drawing.Point(46, 37);
            this.btnPrevCol.Name = "btnPrevCol";
            this.btnPrevCol.Size = new System.Drawing.Size(26, 22);
            this.btnPrevCol.TabIndex = 69;
            this.btnPrevCol.Text = "◄";
            this.btnPrevCol.UseVisualStyleBackColor = true;
            this.btnPrevCol.Click += new System.EventHandler(this.btnPrevCol_Click);
            // 
            // btnajoutmulticol
            // 
            this.btnajoutmulticol.Location = new System.Drawing.Point(9, 78);
            this.btnajoutmulticol.Name = "btnajoutmulticol";
            this.btnajoutmulticol.Size = new System.Drawing.Size(114, 22);
            this.btnajoutmulticol.TabIndex = 68;
            this.btnajoutmulticol.Text = "Ajout collision";
            this.btnajoutmulticol.UseVisualStyleBackColor = true;
            this.btnajoutmulticol.Click += new System.EventHandler(this.btnajoutmulticol_Click);
            // 
            // cmbTypmcol
            // 
            this.cmbTypmcol.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTypmcol.FormattingEnabled = true;
            this.cmbTypmcol.Location = new System.Drawing.Point(254, 16);
            this.cmbTypmcol.Name = "cmbTypmcol";
            this.cmbTypmcol.Size = new System.Drawing.Size(129, 21);
            this.cmbTypmcol.TabIndex = 60;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(212, 18);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(27, 13);
            this.label14.TabIndex = 61;
            this.label14.Text = "type";
            // 
            // grpMulticol
            // 
            this.grpMulticol.Controls.Add(this.label15);
            this.grpMulticol.Controls.Add(this.numwidthmcol);
            this.grpMulticol.Controls.Add(this.numOffxmcol);
            this.grpMulticol.Controls.Add(this.label16);
            this.grpMulticol.Controls.Add(this.label20);
            this.grpMulticol.Controls.Add(this.numHeightmcol);
            this.grpMulticol.Controls.Add(this.label17);
            this.grpMulticol.Controls.Add(this.label21);
            this.grpMulticol.Controls.Add(this.label18);
            this.grpMulticol.Controls.Add(this.numOffymcol);
            this.grpMulticol.Enabled = false;
            this.grpMulticol.Location = new System.Drawing.Point(148, 48);
            this.grpMulticol.Name = "grpMulticol";
            this.grpMulticol.Size = new System.Drawing.Size(242, 142);
            this.grpMulticol.TabIndex = 62;
            this.grpMulticol.TabStop = false;
            this.grpMulticol.Text = "Définir Collisions";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(14, 146);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(0, 13);
            this.label15.TabIndex = 54;
            // 
            // numwidthmcol
            // 
            this.numwidthmcol.Location = new System.Drawing.Point(76, 33);
            this.numwidthmcol.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numwidthmcol.Name = "numwidthmcol";
            this.numwidthmcol.Size = new System.Drawing.Size(128, 20);
            this.numwidthmcol.TabIndex = 45;
            // 
            // numOffxmcol
            // 
            this.numOffxmcol.Location = new System.Drawing.Point(74, 84);
            this.numOffxmcol.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numOffxmcol.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numOffxmcol.Name = "numOffxmcol";
            this.numOffxmcol.Size = new System.Drawing.Size(128, 20);
            this.numOffxmcol.TabIndex = 64;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(14, 58);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(45, 13);
            this.label16.TabIndex = 48;
            this.label16.Text = "Hauteur";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(14, 110);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(43, 13);
            this.label20.TabIndex = 67;
            this.label20.Text = "offset Y";
            // 
            // numHeightmcol
            // 
            this.numHeightmcol.Location = new System.Drawing.Point(76, 58);
            this.numHeightmcol.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numHeightmcol.Name = "numHeightmcol";
            this.numHeightmcol.Size = new System.Drawing.Size(128, 20);
            this.numHeightmcol.TabIndex = 46;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(14, 82);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(0, 13);
            this.label17.TabIndex = 49;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(15, 86);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(43, 13);
            this.label21.TabIndex = 66;
            this.label21.Text = "offset X";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(14, 33);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(43, 13);
            this.label18.TabIndex = 47;
            this.label18.Text = "Largeur";
            // 
            // numOffymcol
            // 
            this.numOffymcol.Location = new System.Drawing.Point(74, 107);
            this.numOffymcol.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numOffymcol.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numOffymcol.Name = "numOffymcol";
            this.numOffymcol.Size = new System.Drawing.Size(128, 20);
            this.numOffymcol.TabIndex = 65;
            // 
            // panelNormalCol
            // 
            this.panelNormalCol.Controls.Add(this.numColRadius);
            this.panelNormalCol.Controls.Add(this.label13);
            this.panelNormalCol.Controls.Add(this.chkCustomCol);
            this.panelNormalCol.Controls.Add(this.grpparallax);
            this.panelNormalCol.Controls.Add(this.chkHasCollisions);
            this.panelNormalCol.Controls.Add(this.grpCustomCollisions);
            this.panelNormalCol.Location = new System.Drawing.Point(65, 87);
            this.panelNormalCol.Name = "panelNormalCol";
            this.panelNormalCol.Size = new System.Drawing.Size(337, 317);
            this.panelNormalCol.TabIndex = 75;
            // 
            // numColRadius
            // 
            this.numColRadius.DecimalPlaces = 2;
            this.numColRadius.Location = new System.Drawing.Point(153, 280);
            this.numColRadius.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numColRadius.Name = "numColRadius";
            this.numColRadius.Size = new System.Drawing.Size(128, 20);
            this.numColRadius.TabIndex = 62;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(23, 283);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(110, 13);
            this.label13.TabIndex = 63;
            this.label13.Text = "arrondi boite collisions";
            // 
            // chkCustomCol
            // 
            this.chkCustomCol.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkCustomCol.Location = new System.Drawing.Point(26, 12);
            this.chkCustomCol.Name = "chkCustomCol";
            this.chkCustomCol.Size = new System.Drawing.Size(154, 24);
            this.chkCustomCol.TabIndex = 59;
            this.chkCustomCol.Text = "Collisions personnalisé";
            this.chkCustomCol.UseVisualStyleBackColor = true;
            this.chkCustomCol.CheckedChanged += new System.EventHandler(this.chkCustomCol_CheckedChanged);
            // 
            // grpparallax
            // 
            this.grpparallax.Controls.Add(this.label7);
            this.grpparallax.Controls.Add(this.numparallaxx);
            this.grpparallax.Controls.Add(this.label8);
            this.grpparallax.Controls.Add(this.numparallaxy);
            this.grpparallax.Controls.Add(this.label9);
            this.grpparallax.Controls.Add(this.label10);
            this.grpparallax.Enabled = false;
            this.grpparallax.Location = new System.Drawing.Point(77, 177);
            this.grpparallax.Name = "grpparallax";
            this.grpparallax.Size = new System.Drawing.Size(242, 82);
            this.grpparallax.TabIndex = 61;
            this.grpparallax.TabStop = false;
            this.grpparallax.Text = "Définir parallax";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 146);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(0, 13);
            this.label7.TabIndex = 54;
            // 
            // numparallaxx
            // 
            this.numparallaxx.DecimalPlaces = 1;
            this.numparallaxx.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numparallaxx.Location = new System.Drawing.Point(76, 33);
            this.numparallaxx.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numparallaxx.Name = "numparallaxx";
            this.numparallaxx.Size = new System.Drawing.Size(128, 20);
            this.numparallaxx.TabIndex = 45;
            this.numparallaxx.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(14, 58);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 13);
            this.label8.TabIndex = 48;
            this.label8.Text = "parallax Y";
            // 
            // numparallaxy
            // 
            this.numparallaxy.DecimalPlaces = 1;
            this.numparallaxy.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numparallaxy.Location = new System.Drawing.Point(76, 58);
            this.numparallaxy.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numparallaxy.Name = "numparallaxy";
            this.numparallaxy.Size = new System.Drawing.Size(128, 20);
            this.numparallaxy.TabIndex = 46;
            this.numparallaxy.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(14, 82);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(0, 13);
            this.label9.TabIndex = 49;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(14, 33);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 13);
            this.label10.TabIndex = 47;
            this.label10.Text = "parallax X";
            // 
            // chkHasCollisions
            // 
            this.chkHasCollisions.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkHasCollisions.Location = new System.Drawing.Point(22, 145);
            this.chkHasCollisions.Name = "chkHasCollisions";
            this.chkHasCollisions.Size = new System.Drawing.Size(151, 24);
            this.chkHasCollisions.TabIndex = 58;
            this.chkHasCollisions.Text = "Pas de collisions";
            this.chkHasCollisions.UseVisualStyleBackColor = true;
            this.chkHasCollisions.CheckedChanged += new System.EventHandler(this.chkHasCollisions_CheckedChanged);
            // 
            // grpCustomCollisions
            // 
            this.grpCustomCollisions.Controls.Add(this.label6);
            this.grpCustomCollisions.Controls.Add(this.numColWidth);
            this.grpCustomCollisions.Controls.Add(this.lblColHeight);
            this.grpCustomCollisions.Controls.Add(this.numColHeight);
            this.grpCustomCollisions.Controls.Add(this.label3);
            this.grpCustomCollisions.Controls.Add(this.lblColWidth);
            this.grpCustomCollisions.Enabled = false;
            this.grpCustomCollisions.Location = new System.Drawing.Point(77, 48);
            this.grpCustomCollisions.Name = "grpCustomCollisions";
            this.grpCustomCollisions.Size = new System.Drawing.Size(242, 82);
            this.grpCustomCollisions.TabIndex = 60;
            this.grpCustomCollisions.TabStop = false;
            this.grpCustomCollisions.Text = "Définir Collisions";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 146);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 13);
            this.label6.TabIndex = 54;
            // 
            // numColWidth
            // 
            this.numColWidth.Location = new System.Drawing.Point(76, 33);
            this.numColWidth.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numColWidth.Minimum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.numColWidth.Name = "numColWidth";
            this.numColWidth.Size = new System.Drawing.Size(128, 20);
            this.numColWidth.TabIndex = 45;
            this.numColWidth.Value = new decimal(new int[] {
            32,
            0,
            0,
            0});
            // 
            // lblColHeight
            // 
            this.lblColHeight.AutoSize = true;
            this.lblColHeight.Location = new System.Drawing.Point(14, 58);
            this.lblColHeight.Name = "lblColHeight";
            this.lblColHeight.Size = new System.Drawing.Size(45, 13);
            this.lblColHeight.TabIndex = 48;
            this.lblColHeight.Text = "Hauteur";
            // 
            // numColHeight
            // 
            this.numColHeight.Location = new System.Drawing.Point(76, 58);
            this.numColHeight.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numColHeight.Minimum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.numColHeight.Name = "numColHeight";
            this.numColHeight.Size = new System.Drawing.Size(128, 20);
            this.numColHeight.TabIndex = 46;
            this.numColHeight.Value = new decimal(new int[] {
            32,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 49;
            // 
            // lblColWidth
            // 
            this.lblColWidth.AutoSize = true;
            this.lblColWidth.Location = new System.Drawing.Point(14, 33);
            this.lblColWidth.Name = "lblColWidth";
            this.lblColWidth.Size = new System.Drawing.Size(43, 13);
            this.lblColWidth.TabIndex = 47;
            this.lblColWidth.Text = "Largeur";
            // 
            // chkKinematicBody
            // 
            this.chkKinematicBody.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkKinematicBody.Location = new System.Drawing.Point(69, 43);
            this.chkKinematicBody.Name = "chkKinematicBody";
            this.chkKinematicBody.Size = new System.Drawing.Size(151, 38);
            this.chkKinematicBody.TabIndex = 64;
            this.chkKinematicBody.Text = "Kinematic (masse infinie, controle par le code)";
            this.chkKinematicBody.UseVisualStyleBackColor = true;
            // 
            // lblshapetype
            // 
            this.lblshapetype.AutoSize = true;
            this.lblshapetype.Location = new System.Drawing.Point(89, 17);
            this.lblshapetype.Name = "lblshapetype";
            this.lblshapetype.Size = new System.Drawing.Size(65, 13);
            this.lblshapetype.TabIndex = 59;
            this.lblshapetype.Text = "Forme entité";
            // 
            // cbShapeType
            // 
            this.cbShapeType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbShapeType.FormattingEnabled = true;
            this.cbShapeType.Location = new System.Drawing.Point(205, 17);
            this.cbShapeType.Name = "cbShapeType";
            this.cbShapeType.Size = new System.Drawing.Size(129, 21);
            this.cbShapeType.TabIndex = 58;
            this.cbShapeType.SelectedIndexChanged += new System.EventHandler(this.cbShapeType_SelectedIndexChanged);
            // 
            // tabShadersParams
            // 
            this.tabShadersParams.Controls.Add(this.panelInt);
            this.tabShadersParams.Controls.Add(this.panelConstantes);
            this.tabShadersParams.Controls.Add(this.panelLumiere);
            this.tabShadersParams.Controls.Add(this.panelTexture);
            this.tabShadersParams.Controls.Add(this.btnSupprParam);
            this.tabShadersParams.Controls.Add(this.btnNextParam);
            this.tabShadersParams.Controls.Add(this.btnPrevParam);
            this.tabShadersParams.Controls.Add(this.BtnAjoutParam);
            this.tabShadersParams.Controls.Add(this.panelFloat);
            this.tabShadersParams.Controls.Add(this.cmbParamsType);
            this.tabShadersParams.Controls.Add(this.label22);
            this.tabShadersParams.Controls.Add(this.label19);
            this.tabShadersParams.Controls.Add(this.txtParamName);
            this.tabShadersParams.Location = new System.Drawing.Point(4, 22);
            this.tabShadersParams.Name = "tabShadersParams";
            this.tabShadersParams.Size = new System.Drawing.Size(535, 629);
            this.tabShadersParams.TabIndex = 2;
            this.tabShadersParams.Text = "Paramètres shaders";
            this.tabShadersParams.UseVisualStyleBackColor = true;
            // 
            // panelInt
            // 
            this.panelInt.Controls.Add(this.numUniformInt);
            this.panelInt.Controls.Add(this.label26);
            this.panelInt.Location = new System.Drawing.Point(22, 561);
            this.panelInt.Name = "panelInt";
            this.panelInt.Size = new System.Drawing.Size(463, 55);
            this.panelInt.TabIndex = 77;
            this.panelInt.Visible = false;
            // 
            // numUniformInt
            // 
            this.numUniformInt.Location = new System.Drawing.Point(161, 26);
            this.numUniformInt.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numUniformInt.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numUniformInt.Name = "numUniformInt";
            this.numUniformInt.Size = new System.Drawing.Size(128, 20);
            this.numUniformInt.TabIndex = 75;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(47, 26);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(72, 13);
            this.label26.TabIndex = 74;
            this.label26.Text = "Valeur entière";
            // 
            // panelConstantes
            // 
            this.panelConstantes.Controls.Add(this.txtExtraParam);
            this.panelConstantes.Controls.Add(this.label24);
            this.panelConstantes.Controls.Add(this.label29);
            this.panelConstantes.Controls.Add(this.cmbConst);
            this.panelConstantes.Location = new System.Drawing.Point(22, 467);
            this.panelConstantes.Name = "panelConstantes";
            this.panelConstantes.Size = new System.Drawing.Size(463, 90);
            this.panelConstantes.TabIndex = 75;
            // 
            // txtExtraParam
            // 
            this.txtExtraParam.Location = new System.Drawing.Point(161, 58);
            this.txtExtraParam.Name = "txtExtraParam";
            this.txtExtraParam.Size = new System.Drawing.Size(158, 20);
            this.txtExtraParam.TabIndex = 76;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(47, 61);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(103, 13);
            this.label24.TabIndex = 76;
            this.label24.Text = "Paramètre additionel";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(47, 26);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(79, 13);
            this.label29.TabIndex = 74;
            this.label29.Text = "Nom constante";
            // 
            // cmbConst
            // 
            this.cmbConst.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbConst.FormattingEnabled = true;
            this.cmbConst.Items.AddRange(new object[] {
            "resolution",
            "time",
            "time_msec",
            "yoyo_time",
            "light_0_pos",
            "light_0_color",
            "light_0_radius",
            "light_0_falloff",
            "light_0_info",
            "ambient_color",
            "mirror_val",
            "rotation_val",
            "inrange_lights",
            "shadow_mask"});
            this.cmbConst.Location = new System.Drawing.Point(161, 24);
            this.cmbConst.Name = "cmbConst";
            this.cmbConst.Size = new System.Drawing.Size(158, 21);
            this.cmbConst.TabIndex = 75;
            // 
            // panelLumiere
            // 
            this.panelLumiere.Controls.Add(this.chkLightNormalize);
            this.panelLumiere.Controls.Add(this.label30);
            this.panelLumiere.Controls.Add(this.cmbLightList);
            this.panelLumiere.Location = new System.Drawing.Point(38, 334);
            this.panelLumiere.Name = "panelLumiere";
            this.panelLumiere.Size = new System.Drawing.Size(463, 127);
            this.panelLumiere.TabIndex = 74;
            // 
            // chkLightNormalize
            // 
            this.chkLightNormalize.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkLightNormalize.Location = new System.Drawing.Point(50, 64);
            this.chkLightNormalize.Name = "chkLightNormalize";
            this.chkLightNormalize.Size = new System.Drawing.Size(185, 24);
            this.chkLightNormalize.TabIndex = 74;
            this.chkLightNormalize.Text = "Normalisation position";
            this.chkLightNormalize.UseVisualStyleBackColor = true;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(47, 26);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(88, 13);
            this.label30.TabIndex = 66;
            this.label30.Text = "identifiant lumière";
            // 
            // cmbLightList
            // 
            this.cmbLightList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLightList.FormattingEnabled = true;
            this.cmbLightList.Location = new System.Drawing.Point(161, 18);
            this.cmbLightList.Name = "cmbLightList";
            this.cmbLightList.Size = new System.Drawing.Size(158, 21);
            this.cmbLightList.TabIndex = 67;
            // 
            // panelTexture
            // 
            this.panelTexture.Controls.Add(this.chkSize);
            this.panelTexture.Controls.Add(this.chkSetOffset);
            this.panelTexture.Controls.Add(this.label28);
            this.panelTexture.Controls.Add(this.label27);
            this.panelTexture.Controls.Add(this.numSlot);
            this.panelTexture.Controls.Add(this.cmbTexture);
            this.panelTexture.Location = new System.Drawing.Point(22, 185);
            this.panelTexture.Name = "panelTexture";
            this.panelTexture.Size = new System.Drawing.Size(463, 142);
            this.panelTexture.TabIndex = 65;
            // 
            // chkSize
            // 
            this.chkSize.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkSize.Location = new System.Drawing.Point(35, 114);
            this.chkSize.Name = "chkSize";
            this.chkSize.Size = new System.Drawing.Size(246, 24);
            this.chkSize.TabIndex = 76;
            this.chkSize.Text = "définir taille dans shader";
            this.chkSize.UseVisualStyleBackColor = true;
            // 
            // chkSetOffset
            // 
            this.chkSetOffset.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkSetOffset.Location = new System.Drawing.Point(35, 89);
            this.chkSetOffset.Name = "chkSetOffset";
            this.chkSetOffset.Size = new System.Drawing.Size(246, 24);
            this.chkSetOffset.TabIndex = 75;
            this.chkSetOffset.Text = "définir offset dans shader";
            this.chkSetOffset.UseVisualStyleBackColor = true;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(32, 64);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(106, 13);
            this.label28.TabIndex = 73;
            this.label28.Text = "Emplacement texture";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(47, 26);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(87, 13);
            this.label27.TabIndex = 66;
            this.label27.Text = "identifiant texture";
            // 
            // numSlot
            // 
            this.numSlot.Location = new System.Drawing.Point(161, 62);
            this.numSlot.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numSlot.Name = "numSlot";
            this.numSlot.Size = new System.Drawing.Size(128, 20);
            this.numSlot.TabIndex = 72;
            // 
            // cmbTexture
            // 
            this.cmbTexture.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTexture.FormattingEnabled = true;
            this.cmbTexture.Location = new System.Drawing.Point(161, 18);
            this.cmbTexture.Name = "cmbTexture";
            this.cmbTexture.Size = new System.Drawing.Size(158, 21);
            this.cmbTexture.TabIndex = 67;
            // 
            // btnSupprParam
            // 
            this.btnSupprParam.Enabled = false;
            this.btnSupprParam.Location = new System.Drawing.Point(362, 75);
            this.btnSupprParam.Name = "btnSupprParam";
            this.btnSupprParam.Size = new System.Drawing.Size(114, 22);
            this.btnSupprParam.TabIndex = 75;
            this.btnSupprParam.Text = "Suppr param";
            this.btnSupprParam.UseVisualStyleBackColor = true;
            this.btnSupprParam.Click += new System.EventHandler(this.btnSupprParam_Click);
            // 
            // btnNextParam
            // 
            this.btnNextParam.Enabled = false;
            this.btnNextParam.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnNextParam.Location = new System.Drawing.Point(422, 14);
            this.btnNextParam.Name = "btnNextParam";
            this.btnNextParam.Size = new System.Drawing.Size(26, 22);
            this.btnNextParam.TabIndex = 74;
            this.btnNextParam.Text = "►";
            this.btnNextParam.UseVisualStyleBackColor = true;
            this.btnNextParam.Click += new System.EventHandler(this.btnNextParam_Click);
            // 
            // btnPrevParam
            // 
            this.btnPrevParam.Enabled = false;
            this.btnPrevParam.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnPrevParam.Location = new System.Drawing.Point(386, 14);
            this.btnPrevParam.Name = "btnPrevParam";
            this.btnPrevParam.Size = new System.Drawing.Size(26, 22);
            this.btnPrevParam.TabIndex = 73;
            this.btnPrevParam.Text = "◄";
            this.btnPrevParam.UseVisualStyleBackColor = true;
            this.btnPrevParam.Click += new System.EventHandler(this.btnPrevParam_Click);
            // 
            // BtnAjoutParam
            // 
            this.BtnAjoutParam.Location = new System.Drawing.Point(362, 42);
            this.BtnAjoutParam.Name = "BtnAjoutParam";
            this.BtnAjoutParam.Size = new System.Drawing.Size(114, 22);
            this.BtnAjoutParam.TabIndex = 72;
            this.BtnAjoutParam.Text = "Ajout param";
            this.BtnAjoutParam.UseVisualStyleBackColor = true;
            this.BtnAjoutParam.Click += new System.EventHandler(this.BtnAjoutParam_Click);
            // 
            // panelFloat
            // 
            this.panelFloat.Controls.Add(this.numComponents);
            this.panelFloat.Controls.Add(this.lblparamw);
            this.panelFloat.Controls.Add(this.numWA);
            this.panelFloat.Controls.Add(this.lblparamz);
            this.panelFloat.Controls.Add(this.numZB);
            this.panelFloat.Controls.Add(this.lblparamy);
            this.panelFloat.Controls.Add(this.numYG);
            this.panelFloat.Controls.Add(this.lblparamx);
            this.panelFloat.Controls.Add(this.numXR);
            this.panelFloat.Location = new System.Drawing.Point(38, 102);
            this.panelFloat.Name = "panelFloat";
            this.panelFloat.Size = new System.Drawing.Size(463, 166);
            this.panelFloat.TabIndex = 64;
            // 
            // numComponents
            // 
            this.numComponents.Location = new System.Drawing.Point(348, 16);
            this.numComponents.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numComponents.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numComponents.Name = "numComponents";
            this.numComponents.Size = new System.Drawing.Size(90, 20);
            this.numComponents.TabIndex = 72;
            this.numComponents.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numComponents.ValueChanged += new System.EventHandler(this.numComponents_ValueChanged);
            // 
            // lblparamw
            // 
            this.lblparamw.AutoSize = true;
            this.lblparamw.Location = new System.Drawing.Point(32, 136);
            this.lblparamw.Name = "lblparamw";
            this.lblparamw.Size = new System.Drawing.Size(69, 13);
            this.lblparamw.TabIndex = 71;
            this.lblparamw.Text = "Param W / A";
            this.lblparamw.Visible = false;
            // 
            // numWA
            // 
            this.numWA.DecimalPlaces = 5;
            this.numWA.Location = new System.Drawing.Point(161, 134);
            this.numWA.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numWA.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numWA.Name = "numWA";
            this.numWA.Size = new System.Drawing.Size(128, 20);
            this.numWA.TabIndex = 70;
            this.numWA.Visible = false;
            // 
            // lblparamz
            // 
            this.lblparamz.AutoSize = true;
            this.lblparamz.Location = new System.Drawing.Point(32, 97);
            this.lblparamz.Name = "lblparamz";
            this.lblparamz.Size = new System.Drawing.Size(65, 13);
            this.lblparamz.TabIndex = 69;
            this.lblparamz.Text = "Param Z / B";
            this.lblparamz.Visible = false;
            // 
            // numZB
            // 
            this.numZB.DecimalPlaces = 5;
            this.numZB.Location = new System.Drawing.Point(161, 94);
            this.numZB.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numZB.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numZB.Name = "numZB";
            this.numZB.Size = new System.Drawing.Size(128, 20);
            this.numZB.TabIndex = 68;
            this.numZB.Visible = false;
            // 
            // lblparamy
            // 
            this.lblparamy.AutoSize = true;
            this.lblparamy.Location = new System.Drawing.Point(32, 59);
            this.lblparamy.Name = "lblparamy";
            this.lblparamy.Size = new System.Drawing.Size(66, 13);
            this.lblparamy.TabIndex = 67;
            this.lblparamy.Text = "Param Y / G";
            this.lblparamy.Visible = false;
            // 
            // numYG
            // 
            this.numYG.DecimalPlaces = 5;
            this.numYG.Location = new System.Drawing.Point(161, 57);
            this.numYG.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numYG.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numYG.Name = "numYG";
            this.numYG.Size = new System.Drawing.Size(128, 20);
            this.numYG.TabIndex = 66;
            this.numYG.Visible = false;
            // 
            // lblparamx
            // 
            this.lblparamx.AutoSize = true;
            this.lblparamx.Location = new System.Drawing.Point(32, 22);
            this.lblparamx.Name = "lblparamx";
            this.lblparamx.Size = new System.Drawing.Size(66, 13);
            this.lblparamx.TabIndex = 65;
            this.lblparamx.Text = "Param X / R";
            // 
            // numXR
            // 
            this.numXR.DecimalPlaces = 5;
            this.numXR.Location = new System.Drawing.Point(161, 21);
            this.numXR.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numXR.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.numXR.Name = "numXR";
            this.numXR.Size = new System.Drawing.Size(128, 20);
            this.numXR.TabIndex = 0;
            // 
            // cmbParamsType
            // 
            this.cmbParamsType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbParamsType.Enabled = false;
            this.cmbParamsType.FormattingEnabled = true;
            this.cmbParamsType.Location = new System.Drawing.Point(148, 65);
            this.cmbParamsType.Name = "cmbParamsType";
            this.cmbParamsType.Size = new System.Drawing.Size(158, 21);
            this.cmbParamsType.TabIndex = 62;
            this.cmbParamsType.SelectedIndexChanged += new System.EventHandler(this.cmbParamsType_SelectedIndexChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Enabled = false;
            this.label22.Location = new System.Drawing.Point(35, 65);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(81, 13);
            this.label22.TabIndex = 63;
            this.label22.Text = "Type paramètre";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Enabled = false;
            this.label19.Location = new System.Drawing.Point(35, 34);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(94, 13);
            this.label19.TabIndex = 18;
            this.label19.Text = "Nom de paramètre";
            // 
            // txtParamName
            // 
            this.txtParamName.Enabled = false;
            this.txtParamName.Location = new System.Drawing.Point(148, 30);
            this.txtParamName.Name = "txtParamName";
            this.txtParamName.Size = new System.Drawing.Size(158, 20);
            this.txtParamName.TabIndex = 17;
            // 
            // tabPass
            // 
            this.tabPass.Controls.Add(this.groupBox1);
            this.tabPass.Controls.Add(this.lblzindxpass);
            this.tabPass.Controls.Add(this.numzindexpass);
            this.tabPass.Controls.Add(this.SupprPass);
            this.tabPass.Controls.Add(this.btnAddpass);
            this.tabPass.Controls.Add(this.listExtraPass);
            this.tabPass.Location = new System.Drawing.Point(4, 22);
            this.tabPass.Name = "tabPass";
            this.tabPass.Size = new System.Drawing.Size(535, 629);
            this.tabPass.TabIndex = 3;
            this.tabPass.Text = "Paramètres rendu";
            this.tabPass.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkWorld);
            this.groupBox1.Controls.Add(this.chkProj);
            this.groupBox1.Controls.Add(this.chkView);
            this.groupBox1.Location = new System.Drawing.Point(73, 293);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(260, 116);
            this.groupBox1.TabIndex = 83;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Matrices additionelles";
            // 
            // chkWorld
            // 
            this.chkWorld.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkWorld.Location = new System.Drawing.Point(22, 18);
            this.chkWorld.Name = "chkWorld";
            this.chkWorld.Size = new System.Drawing.Size(209, 24);
            this.chkWorld.TabIndex = 80;
            this.chkWorld.Text = "Matrice World";
            this.chkWorld.UseVisualStyleBackColor = true;
            // 
            // chkProj
            // 
            this.chkProj.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkProj.Location = new System.Drawing.Point(22, 78);
            this.chkProj.Name = "chkProj";
            this.chkProj.Size = new System.Drawing.Size(209, 24);
            this.chkProj.TabIndex = 82;
            this.chkProj.Text = "Matrice Proj";
            this.chkProj.UseVisualStyleBackColor = true;
            // 
            // chkView
            // 
            this.chkView.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkView.Location = new System.Drawing.Point(22, 48);
            this.chkView.Name = "chkView";
            this.chkView.Size = new System.Drawing.Size(209, 24);
            this.chkView.TabIndex = 81;
            this.chkView.Text = "Matrice View";
            this.chkView.UseVisualStyleBackColor = true;
            // 
            // lblzindxpass
            // 
            this.lblzindxpass.AutoSize = true;
            this.lblzindxpass.Location = new System.Drawing.Point(94, 198);
            this.lblzindxpass.Name = "lblzindxpass";
            this.lblzindxpass.Size = new System.Drawing.Size(127, 13);
            this.lblzindxpass.TabIndex = 79;
            this.lblzindxpass.Text = "z index passe additionelle";
            this.lblzindxpass.Visible = false;
            // 
            // numzindexpass
            // 
            this.numzindexpass.Location = new System.Drawing.Point(236, 196);
            this.numzindexpass.Maximum = new decimal(new int[] {
            19,
            0,
            0,
            0});
            this.numzindexpass.Name = "numzindexpass";
            this.numzindexpass.Size = new System.Drawing.Size(50, 20);
            this.numzindexpass.TabIndex = 78;
            this.numzindexpass.Visible = false;
            this.numzindexpass.ValueChanged += new System.EventHandler(this.numzindexpass_ValueChanged);
            // 
            // SupprPass
            // 
            this.SupprPass.Enabled = false;
            this.SupprPass.Location = new System.Drawing.Point(359, 82);
            this.SupprPass.Name = "SupprPass";
            this.SupprPass.Size = new System.Drawing.Size(114, 22);
            this.SupprPass.TabIndex = 77;
            this.SupprPass.Text = "Suppr pass rendu";
            this.SupprPass.UseVisualStyleBackColor = true;
            this.SupprPass.Click += new System.EventHandler(this.SupprPass_Click);
            // 
            // btnAddpass
            // 
            this.btnAddpass.Location = new System.Drawing.Point(359, 50);
            this.btnAddpass.Name = "btnAddpass";
            this.btnAddpass.Size = new System.Drawing.Size(114, 22);
            this.btnAddpass.TabIndex = 76;
            this.btnAddpass.Text = "Ajout passe rendu";
            this.btnAddpass.UseVisualStyleBackColor = true;
            this.btnAddpass.Click += new System.EventHandler(this.btnAddpass_Click);
            // 
            // listExtraPass
            // 
            this.listExtraPass.FormattingEnabled = true;
            this.listExtraPass.Location = new System.Drawing.Point(170, 50);
            this.listExtraPass.Name = "listExtraPass";
            this.listExtraPass.Size = new System.Drawing.Size(87, 69);
            this.listExtraPass.TabIndex = 0;
            this.listExtraPass.SelectedIndexChanged += new System.EventHandler(this.listExtraPass_SelectedIndexChanged);
            // 
            // btnCopycollisions
            // 
            this.btnCopycollisions.Location = new System.Drawing.Point(364, 15);
            this.btnCopycollisions.Name = "btnCopycollisions";
            this.btnCopycollisions.Size = new System.Drawing.Size(117, 23);
            this.btnCopycollisions.TabIndex = 76;
            this.btnCopycollisions.Text = "Copier depuis entité...";
            this.btnCopycollisions.UseVisualStyleBackColor = true;
            this.btnCopycollisions.Click += new System.EventHandler(this.btnCopycollisions_Click);
            // 
            // EntityDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(569, 719);
            this.Controls.Add(this.tabentite);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EntityDialog";
            this.ShowIcon = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Ajouter une entité";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.EntityDialog_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.numwidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numheight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numOffsetx)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numOffsetY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRepeatX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRepeatY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numStartFrame)).EndInit();
            this.tabentite.ResumeLayout(false);
            this.tabBaseData.ResumeLayout(false);
            this.tabBaseData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSprite)).EndInit();
            this.tabCollisions.ResumeLayout(false);
            this.tabCollisions.PerformLayout();
            this.panelMultiCol.ResumeLayout(false);
            this.panelMultiCol.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numIndexCol)).EndInit();
            this.grpMulticol.ResumeLayout(false);
            this.grpMulticol.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numwidthmcol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numOffxmcol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHeightmcol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numOffymcol)).EndInit();
            this.panelNormalCol.ResumeLayout(false);
            this.panelNormalCol.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numColRadius)).EndInit();
            this.grpparallax.ResumeLayout(false);
            this.grpparallax.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numparallaxx)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numparallaxy)).EndInit();
            this.grpCustomCollisions.ResumeLayout(false);
            this.grpCustomCollisions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numColWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numColHeight)).EndInit();
            this.tabShadersParams.ResumeLayout(false);
            this.tabShadersParams.PerformLayout();
            this.panelInt.ResumeLayout(false);
            this.panelInt.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUniformInt)).EndInit();
            this.panelConstantes.ResumeLayout(false);
            this.panelConstantes.PerformLayout();
            this.panelLumiere.ResumeLayout(false);
            this.panelLumiere.PerformLayout();
            this.panelTexture.ResumeLayout(false);
            this.panelTexture.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSlot)).EndInit();
            this.panelFloat.ResumeLayout(false);
            this.panelFloat.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numComponents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numZB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numYG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numXR)).EndInit();
            this.tabPass.ResumeLayout(false);
            this.tabPass.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numzindexpass)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtCtor;
        private System.Windows.Forms.Label lblCtor;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Label lblTileset;
        private System.Windows.Forms.Label lblAnim;
        private System.Windows.Forms.Label lblScript;
        private System.Windows.Forms.TextBox txtidentity;
        private System.Windows.Forms.Label lblidentity;
        private System.Windows.Forms.NumericUpDown numwidth;
        private System.Windows.Forms.NumericUpDown numheight;
        private System.Windows.Forms.Label lblwidth;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblheight;
        private System.Windows.Forms.CheckBox chkController;
        private System.Windows.Forms.CheckBox chkCallback;
        private System.Windows.Forms.NumericUpDown numOffsetx;
        private System.Windows.Forms.Label lbloffy;
        private System.Windows.Forms.NumericUpDown numOffsetY;
        private System.Windows.Forms.Label lbloffx;
        private System.Windows.Forms.NumericUpDown numRepeatX;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numRepeatY;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblstartframe;
        private System.Windows.Forms.NumericUpDown numStartFrame;
        private System.Windows.Forms.ComboBox cmbResxTextures;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cmbGroup;
        private System.Windows.Forms.ComboBox cmbAnim;
        private System.Windows.Forms.TextBox txtProgramShader;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnNewScript;
        private System.Windows.Forms.ComboBox cmbScript;
        private System.Windows.Forms.TabControl tabentite;
        private System.Windows.Forms.TabPage tabBaseData;
        private System.Windows.Forms.TabPage tabCollisions;
        private System.Windows.Forms.Label lblshapetype;
        private System.Windows.Forms.ComboBox cbShapeType;
        private System.Windows.Forms.NumericUpDown numOffxmcol;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.NumericUpDown numOffymcol;
        private System.Windows.Forms.GroupBox grpMulticol;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.NumericUpDown numwidthmcol;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cmbTypmcol;
        private System.Windows.Forms.Button btnajoutmulticol;
        private System.Windows.Forms.Panel panelMultiCol;
        private System.Windows.Forms.Panel panelNormalCol;
        private System.Windows.Forms.NumericUpDown numColRadius;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox chkCustomCol;
        private System.Windows.Forms.GroupBox grpparallax;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown numparallaxx;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown numparallaxy;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox chkHasCollisions;
        private System.Windows.Forms.GroupBox grpCustomCollisions;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numColWidth;
        private System.Windows.Forms.Label lblColHeight;
        private System.Windows.Forms.NumericUpDown numColHeight;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblColWidth;
        private System.Windows.Forms.Button btnNextCol;
        private System.Windows.Forms.Button btnPrevCol;
        private System.Windows.Forms.NumericUpDown numHeightmcol;
        private System.Windows.Forms.Button btnSupprCollision;
        private System.Windows.Forms.TabPage tabShadersParams;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtParamName;
        private System.Windows.Forms.ComboBox cmbParamsType;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Panel panelFloat;
        private System.Windows.Forms.Label lblparamx;
        private System.Windows.Forms.NumericUpDown numXR;
        private System.Windows.Forms.Label lblparamw;
        private System.Windows.Forms.NumericUpDown numWA;
        private System.Windows.Forms.Label lblparamz;
        private System.Windows.Forms.NumericUpDown numZB;
        private System.Windows.Forms.Label lblparamy;
        private System.Windows.Forms.NumericUpDown numYG;
        private System.Windows.Forms.Panel panelTexture;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.ComboBox cmbTexture;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.NumericUpDown numSlot;
        private System.Windows.Forms.Panel panelLumiere;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.ComboBox cmbLightList;
        private System.Windows.Forms.CheckBox chkLightNormalize;
        private System.Windows.Forms.Panel panelConstantes;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox cmbConst;
        private System.Windows.Forms.Button btnSupprParam;
        private System.Windows.Forms.Button btnNextParam;
        private System.Windows.Forms.Button btnPrevParam;
        private System.Windows.Forms.Button BtnAjoutParam;
        private System.Windows.Forms.NumericUpDown numComponents;
        private System.Windows.Forms.TabPage tabPass;
        private System.Windows.Forms.ListBox listExtraPass;
        private System.Windows.Forms.Button SupprPass;
        private System.Windows.Forms.Button btnAddpass;
        private System.Windows.Forms.Label lblzindxpass;
        private System.Windows.Forms.NumericUpDown numzindexpass;
        private System.Windows.Forms.CheckBox chk3D;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox cmb3D;
        private System.Windows.Forms.CheckBox chkProj;
        private System.Windows.Forms.CheckBox chkView;
        private System.Windows.Forms.CheckBox chkWorld;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chkSetOffset;
        private System.Windows.Forms.CheckBox chkSize;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtExtraParam;
        private System.Windows.Forms.Panel panelInt;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.NumericUpDown numUniformInt;
        private System.Windows.Forms.Label lblspritecount;
        private System.Windows.Forms.NumericUpDown numSprite;
        private System.Windows.Forms.CheckBox chkKinematicBody;
        private System.Windows.Forms.Label lblindexcol;
        private System.Windows.Forms.NumericUpDown numIndexCol;
        private System.Windows.Forms.CheckBox chk_script_init_only;
        private System.Windows.Forms.CheckBox chknodraw;
        private System.Windows.Forms.CheckBox chkdupliquetexture;
        private System.Windows.Forms.Button btnCopycollisions;
    }
}