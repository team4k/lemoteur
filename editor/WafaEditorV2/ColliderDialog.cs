﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PBInterface;

namespace WafaEditorV2
{
    public partial class ColliderDialog : Form
    {
        private Level ref_level;
        public DialogMode mode { get; set; }

        public ColliderDialog(Level olevel)
        {
            InitializeComponent();
            ref_level = olevel;
            mode = DialogMode.Ajout;
        }

        public void FillData(Collider data)
        {
            this.Tag = data;
            txtident.Text = data.Id;
            numLayer.Value = Convert.ToDecimal(data.CollisionLayer);
            numMask.Value = Convert.ToDecimal(data.CollisionMask);
            numTypeCollision.Value = Convert.ToDecimal(data.CollisionType);

            mode = DialogMode.Modif;
        }


        private void btnOK_Click(object sender, EventArgs e)
        {
            Collider cCol = null;

            if (mode == DialogMode.Ajout)
                cCol = new Collider();
            else
                cCol = (Collider)this.Tag;

            if (string.IsNullOrEmpty(txtident.Text))
            {
                MessageBox.Show("L'identifiant de l'objet de collision est obligatoire !");
                return;
            }


            if (mode == DialogMode.Ajout)
            {
                if (EditorUtilities.ObjectIdExist(ref_level, txtident.Text.Trim()))
                {
                    MessageBox.Show("cet identifiant existe déjà ! ");
                    return;
                }
            }
            else if (mode == DialogMode.Modif)
            {
                if (EditorUtilities.ObjectIdExist(ref_level, txtident.Text.Trim(), cCol))
                {
                    MessageBox.Show("cet identifiant existe déjà ! ");
                    return;
                }
            }


            cCol.Id = txtident.Text;
            cCol.CollisionLayer = Convert.ToInt32(numLayer.Value);
            cCol.CollisionMask = Convert.ToInt32(numMask.Value);
            cCol.CollisionType = Convert.ToInt32(numTypeCollision.Value);

            this.Tag = cCol;

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
    }
}
