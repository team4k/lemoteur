﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AssetsImport;
using WafaEditorV2.Properties;

namespace WafaEditorV2
{
    public partial class ImportAsset : Form, IlogImport
    {
        FrameBitmap currentframe = null;
        List<ResizeCursor> currentCursors = new List<ResizeCursor>();

        bool cursordrag = false;

        bool errorimport = false;

        ResizeCursor selectedCursor = null;

        public ImportAsset()
        {
            InitializeComponent();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            if(ofdScan.ShowDialog() == DialogResult.OK)
            {
                List<FileInfo> selectscan = new List<FileInfo>();
                selectscan.Add(new FileInfo(ofdScan.FileName));

                List<FrameBitmap> outscan = new List<FrameBitmap>();

                if (currentframe != null)
                {
                    currentframe.Dispose();
                }


                if (Import.TraceAndGimpPng(Settings.Default.potrace_path, selectscan, this, out outscan, pbPreview.Width, pbPreview.Height,true))
                {
                    pbPreview.Image = outscan[0].potracedscan;
                    currentframe = outscan[0];
                }
            }
        }

        public void AppendLog(string data, bool iserror = false, bool threaded = false)
        {
            if (data == null)
            {
                return;
            }

            Color textcolor = Color.Gray;

            if (iserror)
            {
                textcolor = Color.Red;
                errorimport = true;
            }

            if (threaded)
            {
                this.BeginInvoke(new MethodInvoker(() =>
                {
                    rtbLog.ForeColor = textcolor;
                    rtbLog.AppendText(data);

                    rtbLog.AppendText(Environment.NewLine);
                }));
            }
            else
            {
                rtbLog.ForeColor = textcolor;
                rtbLog.AppendText(data);

                rtbLog.AppendText(Environment.NewLine);
            }
        }

        public void DoEvents()
        {
            Application.DoEvents();
        }

        public void AppendText(string text)
        {
            rtbLog.AppendText(text);
        }

        private void pbPreview_Paint(object sender, PaintEventArgs e)
        {
            if (currentframe == null)
                return;

            ResizeEvents.Paint((PictureBox)sender, currentCursors, currentframe, e);
        }

        private void pbPreview_MouseUp(object sender, MouseEventArgs e)
        {
            if (currentframe == null)
                return;

            if (ResizeEvents.MouseUp(ref selectedCursor))
            {
                pbPreview.Invalidate();
            }

            cursordrag = false;
        }

        private void pbPreview_MouseDown(object sender, MouseEventArgs e)
        {
            if (currentframe == null)
                return;

            cursordrag = true;

            selectedCursor = ResizeEvents.MouseDown(currentCursors, e);

            pbPreview.Invalidate();
        }

        private void pbPreview_MouseMove(object sender, MouseEventArgs e)
        {
            if (currentframe == null)
                return;

            if (ResizeEvents.MouseMove(cursordrag, selectedCursor, currentCursors, currentframe, new List<FrameBitmap>(new FrameBitmap[]{ currentframe }), e))
            {
                pbPreview.Invalidate();
            }
        }

        private void btnCrop_Click(object sender, EventArgs e)
        {
            int offsetx = currentframe.GetContent(false).Width - pbPreview.Width;

            int offsety = currentframe.GetContent(false).Height - pbPreview.Height;

            if (offsetx != 0)
                offsetx /= 2;

            if (offsety != 0)
                offsety /= 2;

            int startcropx = currentframe.ClipRectangle.X + offsetx;
            int startcropy = currentframe.ClipRectangle.Y + offsety;

            currentframe.CropScan(startcropx, startcropy);


            pbPreview.Image = currentframe.GetContent(false);

            pbPreview.Invalidate();
        }

        private void btnRedim_Click(object sender, EventArgs e)
        {
            //redimensionnement de toutes les frames
            ResizeWidget widget = new ResizeWidget();

            if (widget.ShowDialog() == DialogResult.OK)
            {
                currentframe.RedimScan(Convert.ToInt32(widget.numwidth.Value), Convert.ToInt32(widget.numheight.Value));

                pbPreview.Image = currentframe.GetContent(false);

                pbPreview.Invalidate();
            }
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            errorimport = false;
            if(File.Exists(Path.Combine(Settings.Default.assets_path, txtasset.Text + ".png")))
            {
                if(MessageBox.Show("Un fichier nommé" + txtasset.Text + ".png existe déjà, écraser le fichier existant ?", "Ficher existant", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    MessageBox.Show("Aucun import effectué");
                    return;
                }
            }

            string inpath = Path.Combine(Settings.Default.raw_assets_path, txtasset.Text + ".png");
            currentframe.SaveFrame(inpath);

            string outdir = Path.Combine(Settings.Default.raw_assets_path, "work_assets");

            if (!Directory.Exists(outdir))
            {
                Directory.CreateDirectory(outdir);
            }

            string potracedpath = Path.Combine(outdir, Path.Combine(outdir, txtasset.Text + ".png"));

            string workasset = Path.Combine(outdir, txtasset.Text + ".xcf");

            if (File.Exists(workasset))
            {
                File.Delete(workasset);
            }

            currentframe.SavePoTraced(potracedpath);

            //save under work_assets folder
            string pathpython = string.Format(@"BatchConvertLms.py DoSingleConvert {0} {1}", potracedpath, workasset);

            //double antislashes on windows, otherwise the batch will fail
            pathpython = pathpython.Replace(@"\", @"\\");

            ProcessStartInfo GimpConversionInfo = new ProcessStartInfo("python", pathpython);
            GimpConversionInfo.UseShellExecute = false;
            GimpConversionInfo.RedirectStandardOutput = true;
            GimpConversionInfo.RedirectStandardError = true;
            GimpConversionInfo.WorkingDirectory = Environment.CurrentDirectory;

            Process Gimpproc = Process.Start(GimpConversionInfo);
            Gimpproc.OutputDataReceived += (osender, oe) => this.AppendLog(oe.Data, threaded: true);
            //Gimpproc.ErrorDataReceived += (osender, oe) => this.AppendLog(oe.Data, iserror: true, threaded: true);
            Gimpproc.BeginOutputReadLine();
            Gimpproc.BeginErrorReadLine();

            while (!Gimpproc.HasExited)
                Application.DoEvents();

            //copie dans le répertoire de sortie
            File.Copy(inpath, Path.Combine(Settings.Default.assets_path, txtasset.Text + ".png"),true);

            File.Delete(potracedpath);

            if (!errorimport)
            {
                MessageBox.Show("Import d'un nouvel asset effectué !", "Import effectué !", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            else
            {
                MessageBox.Show("Erreur lors de l'import d'un nouvel asset !", "Erreur lors de l'import", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}
