﻿namespace WafaEditorV2
{
    partial class NewLevel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewLevel));
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblidniveau = new System.Windows.Forms.Label();
            this.txtidniveau = new System.Windows.Forms.TextBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.txtScript = new System.Windows.Forms.TextBox();
            this.lblScript = new System.Windows.Forms.Label();
            this.openScript = new System.Windows.Forms.OpenFileDialog();
            this.btnNewScript = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnOk
            // 
            resources.ApplyResources(this.btnOk, "btnOk");
            this.btnOk.Name = "btnOk";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            resources.ApplyResources(this.btnCancel, "btnCancel");
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblidniveau
            // 
            resources.ApplyResources(this.lblidniveau, "lblidniveau");
            this.lblidniveau.Name = "lblidniveau";
            // 
            // txtidniveau
            // 
            resources.ApplyResources(this.txtidniveau, "txtidniveau");
            this.txtidniveau.Name = "txtidniveau";
            this.txtidniveau.TextChanged += new System.EventHandler(this.txtidniveau_TextChanged);
            // 
            // btnBrowse
            // 
            resources.ApplyResources(this.btnBrowse, "btnBrowse");
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // txtScript
            // 
            resources.ApplyResources(this.txtScript, "txtScript");
            this.txtScript.Name = "txtScript";
            this.txtScript.ReadOnly = true;
            // 
            // lblScript
            // 
            resources.ApplyResources(this.lblScript, "lblScript");
            this.lblScript.Name = "lblScript";
            // 
            // openScript
            // 
            resources.ApplyResources(this.openScript, "openScript");
            // 
            // btnNewScript
            // 
            resources.ApplyResources(this.btnNewScript, "btnNewScript");
            this.btnNewScript.Name = "btnNewScript";
            this.btnNewScript.UseVisualStyleBackColor = true;
            this.btnNewScript.Click += new System.EventHandler(this.btnNewScript_Click);
            // 
            // NewLevel
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.btnNewScript);
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.txtScript);
            this.Controls.Add(this.lblScript);
            this.Controls.Add(this.txtidniveau);
            this.Controls.Add(this.lblidniveau);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NewLevel";
            this.ShowIcon = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblidniveau;
        public System.Windows.Forms.TextBox txtidniveau;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.Label lblScript;
        private System.Windows.Forms.OpenFileDialog openScript;
        public System.Windows.Forms.TextBox txtScript;
        private System.Windows.Forms.Button btnNewScript;
    }
}