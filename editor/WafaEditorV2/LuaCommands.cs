﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WafaEditorV2
{
    public partial class LuaCommands : Form
    {
        private Dictionary<string, string> commands_ref;

        private List<string> commands_reminder = new List<string>();

        private int reminder_index = -1;

        public LuaCommands(Dictionary<string,string> commands)
        {
            commands_ref = commands;
            InitializeComponent();
            LoadCommands();
        }

        public void LoadCommands()
        {
            rtbLog.Clear();
            rtbCommands.Clear();
            commands_reminder.Clear();

            StringBuilder out_buffer = new StringBuilder(10000);

            string value = string.Empty;

            foreach (KeyValuePair<string, string> cmds in commands_ref)
            {

                NativeFunc.editor_exec_string(cmds.Value, out_buffer,out_buffer.Capacity);

                if (!string.IsNullOrEmpty(out_buffer.ToString()))
                {
                    rtbLog.AppendText(out_buffer.ToString());
                    rtbLog.AppendText(Environment.NewLine);
                }
                else
                {
                    rtbLog.AppendText(cmds.Value+">> OK");
                    rtbLog.AppendText(Environment.NewLine);
                    value = cmds.Value;
                }

                commands_reminder.Add(cmds.Value);
            }

            rtbCommands.AppendText(value);
            rtbCommands.AppendText(Environment.NewLine);
            rtbLog.ScrollToCaret();

            reminder_index = commands_reminder.Count - 1;
        }

        private void rtbCommands_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string value = rtbCommands.Lines[0].Trim();
                StringBuilder out_buffer = new StringBuilder(10000);
                NativeFunc.editor_exec_string(value, out_buffer,out_buffer.Capacity);

                if (!string.IsNullOrEmpty(out_buffer.ToString()))
                {
                    rtbLog.AppendText(out_buffer.ToString());
                    rtbLog.AppendText(Environment.NewLine);
                }
                else
                {
                    //on récupère le nom de la commande
                    int indx = value.IndexOf("(");
                    string cmd_name = value.Substring(0, indx);

                    if (cmd_name.StartsWith("npc_") || cmd_name.StartsWith("Entity _"))
                    {
                        //on récupère l'id du npc
                        string npc_name = value.Substring(indx + 2, value.IndexOf("'", indx + 2) - (indx + 2));
                        cmd_name = cmd_name + npc_name;
                    }

                    if (commands_ref.ContainsKey(cmd_name))
                        commands_ref[cmd_name] = value;
                    else
                        commands_ref.Add(cmd_name, value);

                   

                    rtbLog.AppendText(value+" >> OK");
                    rtbLog.AppendText(Environment.NewLine);
                }

                rtbLog.ScrollToCaret();

                commands_reminder.Add(value);

                reminder_index = commands_reminder.Count - 1;
            }
            else if (e.KeyCode == Keys.Up)
            {
                if (reminder_index != -1)
                {
                     rtbCommands.Select(0, rtbCommands.TextLength);
                     rtbCommands.SelectedText = string.Empty;

                    string value = commands_reminder[reminder_index--];
                    rtbCommands.AppendText(value);

                }

            }
        }

        public bool TextHasFocus()
        {
            return this.ContainsFocus;
        }

     /*   public void DoKeyDown(KeyEventArgs e)
        {
            rtbCommands.DoKeyDown(e);
            base.OnKeyDown(e);
        }

        public void DoKeyUp(KeyEventArgs e)
        {
            rtbCommands.DoKeyUp(e);
            base.OnKeyUp(e);
        }

        public void DoKeyPress(KeyPressEventArgs e)
        {
            rtbCommands.DoKeyPress(e);
            base.OnKeyPress(e);
        }*/



        private void btnClear_Click(object sender, EventArgs e)
        {
            commands_ref.Clear();
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {

            if (commands_ref.Count > 0)
            {
                if (MessageBox.Show("Copier le contenu de la console lua dans le presse papier ?", "Copiez code lua", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    StringBuilder lua_comm = new StringBuilder();
                    foreach (KeyValuePair<string, string> lua_commands in commands_ref)
                    {
                        lua_comm.AppendLine(lua_commands.Value);
                    }

                    Clipboard.SetText(lua_comm.ToString());
                }
            }
        }

    }
}
