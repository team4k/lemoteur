﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PBInterface;

namespace WafaEditorV2
{
    public partial class SoundSourceDialog : Form
    {
        public DialogMode mode { get; set; }

        public Level reflevel { get; set; }

        private List<ResxInfo> ref_game_resources;


        public SoundSourceDialog(Level preflevel, List<ResxInfo> game_resources)
        {
            InitializeComponent();


            reflevel = preflevel;

            ref_game_resources = game_resources;

            cmbSon.Items.AddRange(ref_game_resources.FindAll(p => p.type == ResxType.sound).ToArray());
            cmbSon.DisplayMember = "ident";
            cmbSon.ValueMember = "ident";

            if(cmbSon.Items.Count > 0)
                cmbSon.SelectedIndex = 0;


        }

        public void FillData(SoundSource data)
        {
            mode = DialogMode.Modif;
            this.Tag = data;

            txtidson.Text = data.SoundId;
            cmbSon.SelectedItem = ref_game_resources.Find(p => p.ident == data.SndFileName);

            this.Text = Editor.SonModif;

            numFalloff.Value = (decimal)data.Falloffarea;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            SoundSource snd_source = null;

            if (mode == DialogMode.Ajout)
            {
                snd_source = new SoundSource();
            }
            else
                snd_source = (SoundSource)this.Tag;

            if (string.IsNullOrEmpty(txtidson.Text.Trim()))
            {
                MessageBox.Show("L'identifiant de la source sonore est obligatoire !");
                return;
            }


            if (mode == DialogMode.Ajout)
            {
                if (EditorUtilities.ObjectIdExist(reflevel, txtidson.Text.Trim()))
                {
                    MessageBox.Show("Un objet avec cet identifiant existe déjà ! ");
                    return;
                }
            }
            else if (mode == DialogMode.Modif)
            {
                if (EditorUtilities.ObjectIdExist(reflevel, txtidson.Text.Trim(), snd_source))
                {
                    MessageBox.Show("Un objet avec cet identifiant existe déjà ! ");
                    return;
                }
            }

            if (cmbSon.SelectedItem == null)
            {
                MessageBox.Show("Un fichier son est obligatoire !");
                return;
            }

            snd_source.SoundId = txtidson.Text;
            snd_source.SndFileName = ((ResxInfo)cmbSon.SelectedItem).ident;

            snd_source.Falloffarea = (float)numFalloff.Value;

            this.Tag = snd_source;

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void btnAnnuler_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
    }
}
