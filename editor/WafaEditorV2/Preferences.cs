﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WafaEditorV2.Properties;
using System.IO;

namespace WafaEditorV2
{
    public partial class PrefWindow : Form
    {
        public delegate void change_tileset_prop();

        public event change_tileset_prop OnChangeTilesetProperties;

        public delegate void change_render_size();

        public event change_render_size OnChangeRenderSize;

        private bool old_dyn = false;

        public PrefWindow()
        {
            InitializeComponent();
        }

        private void PrefWindow_Load(object sender, EventArgs e)
        {
            //chargement des préférences depuis le fichier app.config
            txtAssets.Text = Settings.Default.assets_path;
            txt_gamdll.Text = Settings.Default.gamedll_path;
            txtFont.Text = Settings.Default.font;
            txt_localedb.Text = Settings.Default.localedb_path;
            txtPotrace.Text = Settings.Default.potrace_path;

            btnColorBackground.BackColor = Settings.Default.background_color;
            btnGridColor.BackColor = Settings.Default.grid_color;
            chkShowGrid.Checked = Settings.Default.show_grid;

            trkMusic.Value = Settings.Default.music_volume;
            trkSfx.Value = Settings.Default.sfx_volume;
            btnSelectorColor.BackColor = Settings.Default.selector_color;
            btnCursorColor.BackColor = Settings.Default.cursor_color;

            btnColorBackTileset.BackColor = Settings.Default.tileset_back_color;
            numSpaceTiles.Value = Settings.Default.tileset_offset;
            txt_rawassets.Text = Settings.Default.raw_assets_path;
            numWidthWindow.Value = Settings.Default.render_width;
            numHeightWindow.Value = Settings.Default.render_height;
            numTailleTile.Value = Settings.Default.TILE_SIZE;

            chklightsys.Checked = Settings.Default.enable_lighting;
            btnAmbientColor.BackColor = Settings.Default.ambient_color;
            numAlphaAmbient.Value = Settings.Default.ambient_alpha;
            txtProgLight.Text = Settings.Default.light_prog;
            txtProgShadow.Text = Settings.Default.shadow_prog;
            txtProgMask.Text = Settings.Default.mask_prog;
            btnColorFPS.BackColor = Settings.Default.colorFPS;


            txtRootFolder.Text = Settings.Default.rootFolder;
            txtAnt.Text = Settings.Default.antFolder;
            txtJDK.Text = Settings.Default.jdkFolder;
            txtGameName.Text = Settings.Default.gamename;
            chkResDynamique.Checked = Settings.Default.res_dynamique;
            old_dyn = Settings.Default.res_dynamique;

            chkPacking.Checked = Settings.Default.packtexture;
            numPackTexwidth.Value = (decimal)Settings.Default.packtexturewidth;
            numPackTexheight.Value = (decimal)Settings.Default.packtextureheight;
            chkSkipBuild.Checked = Settings.Default.exportskipbuild;
            chkCleanBuild.Checked = Settings.Default.exportcleanbuild;


        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            if (assetsBrowse.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (!Directory.Exists(assetsBrowse.SelectedPath))
                {
                    MessageBox.Show("Le répertoire de données doit être valide ! ");
                    return;
                }

                txtAssets.Text = assetsBrowse.SelectedPath;
            }
        }

        private void btnEnreg_Click(object sender, EventArgs e)
        {
            if (Directory.Exists(txtAssets.Text))
            {
                Settings.Default.assets_path = txtAssets.Text;
                Settings.Default.gamedll_path = txt_gamdll.Text;
                Settings.Default.font = txtFont.Text;
                Settings.Default.background_color = btnColorBackground.BackColor;
                Settings.Default.show_grid = chkShowGrid.Checked;
                Settings.Default.grid_color = Color.FromArgb(127,btnGridColor.BackColor);
                Settings.Default.music_volume = (short)trkMusic.Value;
                Settings.Default.sfx_volume = (short)trkSfx.Value;
                Settings.Default.localedb_path = txt_localedb.Text;
                Settings.Default.selector_color = btnSelectorColor.BackColor;
                Settings.Default.cursor_color = btnCursorColor.BackColor;
                Settings.Default.gamename = txtGameName.Text;
                Settings.Default.res_dynamique = chkResDynamique.Checked;
                Settings.Default.potrace_path = txtPotrace.Text;

                bool tilesetpropChanged = (Settings.Default.tileset_back_color != btnColorBackTileset.BackColor || Settings.Default.tileset_offset != (int)numSpaceTiles.Value);

                Settings.Default.tileset_back_color = btnColorBackTileset.BackColor;
                Settings.Default.tileset_offset = (int)numSpaceTiles.Value;
                Settings.Default.raw_assets_path = txt_rawassets.Text;
                Settings.Default.colorFPS = btnColorFPS.BackColor;


                bool render_size_changed = (Settings.Default.render_width != Convert.ToInt32(numWidthWindow.Value) || Settings.Default.render_height != Convert.ToInt32(numHeightWindow.Value) || old_dyn != Settings.Default.res_dynamique);

                Settings.Default.render_width = Convert.ToInt32(numWidthWindow.Value);
                Settings.Default.render_height = Convert.ToInt32(numHeightWindow.Value);

                int new_tilesize = Convert.ToInt32(numTailleTile.Value);
                if (Settings.Default.TILE_SIZE != new_tilesize)
                    NativeFunc.editor_tilemaps_updatetilesize(new_tilesize);

                Settings.Default.TILE_SIZE = new_tilesize;
                Settings.Default.enable_lighting = chklightsys.Checked;
                Settings.Default.ambient_color = btnAmbientColor.BackColor;
                Settings.Default.ambient_alpha = (int)numAlphaAmbient.Value;

                Settings.Default.shadow_prog = txtProgShadow.Text;
                Settings.Default.light_prog = txtProgLight.Text;
                Settings.Default.mask_prog = txtProgMask.Text;
                Settings.Default.exportskipbuild = chkSkipBuild.Checked;
                Settings.Default.exportcleanbuild = chkCleanBuild.Checked;

                //export parameters
                Settings.Default.rootFolder = txtRootFolder.Text;
                Settings.Default.antFolder = txtAnt.Text;
                Settings.Default.jdkFolder = txtJDK.Text;
                Settings.Default.packtexture = chkPacking.Checked;
                Settings.Default.packtexturewidth = Convert.ToUInt32(numPackTexwidth.Value);
                Settings.Default.packtextureheight = Convert.ToUInt32(numPackTexheight.Value);


                Settings.Default.Save();

                NativeFunc.editor_change_color((float)Settings.Default.background_color.R / 255, (float)Settings.Default.background_color.G / 255, (float)Settings.Default.background_color.B / 255, (float)Settings.Default.background_color.A / 255);

                NativeFunc.editor_grid_visibility((Settings.Default.show_grid) ? NativeFunc.wbool.wtrue : NativeFunc.wbool.wfalse, (float)Settings.Default.grid_color.R / 255, (float)Settings.Default.grid_color.G / 255, (float)Settings.Default.grid_color.B / 255, (float)Settings.Default.grid_color.A / 255);

                NativeFunc.editor_set_selectioncolor((float)Settings.Default.selector_color.R / 255, (float)Settings.Default.selector_color.G / 255, (float)Settings.Default.selector_color.B / 255, (float)Settings.Default.selector_color.A / 255);

                NativeFunc.editor_setcursor_color((float)Settings.Default.cursor_color.R / 255, (float)Settings.Default.cursor_color.G / 255, (float)Settings.Default.cursor_color.B / 255, 0.5f);

                NativeFunc.editor_setfps_color((float)Settings.Default.colorFPS.R / 255, (float)Settings.Default.colorFPS.G / 255, (float)Settings.Default.colorFPS.B / 255, 1.0f);

                NativeFunc.editor_set_ambientcolor((float)Settings.Default.ambient_color.R / 255, (float)Settings.Default.ambient_color.G / 255, (float)Settings.Default.ambient_color.B / 255, (float)Settings.Default.ambient_alpha / 255);

                NativeFunc.editor_setenable_lighting((Settings.Default.enable_lighting) ? NativeFunc.wbool.wtrue : NativeFunc.wbool.wfalse);

                NativeFunc.editor_setlight_progs(Settings.Default.light_prog, Settings.Default.shadow_prog);

                NativeFunc.editor_setsoundvolume(Settings.Default.music_volume, Settings.Default.sfx_volume);

                NativeFunc.editor_set_tilesize(Settings.Default.TILE_SIZE);

                if(tilesetpropChanged && OnChangeTilesetProperties != null)
                    OnChangeTilesetProperties();

                if (render_size_changed && OnChangeRenderSize != null)
                    OnChangeRenderSize();


                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }

            this.Close();

        }

        private void btnAnnuler_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void PrefWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == System.Windows.Forms.DialogResult.OK && (string.IsNullOrEmpty(Settings.Default.assets_path.Trim()) || string.IsNullOrEmpty(Settings.Default.gamedll_path.Trim())))
            {
                e.Cancel = true;
                MessageBox.Show("Le répertoire de donnée exportées et le chemin vers la dll de jeu sont obligatoires !"); 

            }
           
        }

        private void btnBrowseDll_Click(object sender, EventArgs e)
        {
            if (gameDllBrowse.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (!File.Exists(gameDllBrowse.FileName))
                {
                    MessageBox.Show("Le chemin vers la dll de jeu doit être valide ! ");
                    return;
                }

                txt_gamdll.Text = gameDllBrowse.FileName;
            }
        }

        private void Color_Click(object sender, EventArgs e)
        {
            Button cButton = sender as Button;
            colorBack.Color = cButton.BackColor;

            if (colorBack.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                cButton.BackColor = colorBack.Color;
            }

        }

        private void btnBrowseLocaledb_Click(object sender, EventArgs e)
        {
            if (localDbBrowse.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (!File.Exists(localDbBrowse.FileName))
                {
                    MessageBox.Show("Le chemin vers la dll de jeu doit être valide ! ");
                    return;
                }

                txt_localedb.Text = localDbBrowse.FileName;
            }
        }

        private void btnBrowseRaw_Click(object sender, EventArgs e)
        {
            if (assetRawBrowse.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (!Directory.Exists(assetRawBrowse.SelectedPath))
                {
                    MessageBox.Show("Le répertoire de données brute doit être valide ! ");
                    return;
                }

                txt_rawassets.Text = assetRawBrowse.SelectedPath;
            }
        }

        private void btnRootFolder_Click(object sender, EventArgs e)
        {
            if (rootFolderBrowse.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (!Directory.Exists(rootFolderBrowse.SelectedPath))
                {
                    MessageBox.Show("Le répertoire source du jeu n'existe pas !");
                    return;
                }

                txtRootFolder.Text = rootFolderBrowse.SelectedPath;
            }
        }

        private void btnBrowseAnt_Click(object sender, EventArgs e)
        {
            if (AntBrowse.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (!Directory.Exists(AntBrowse.SelectedPath))
                {
                    MessageBox.Show("Le répertoire Ant n'existe pas !");
                    return;
                }

                txtAnt.Text = AntBrowse.SelectedPath;
            }
        }

        private void btnBrowseJDK_Click(object sender, EventArgs e)
        {
            if (JDKBrowse.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (!Directory.Exists(JDKBrowse.SelectedPath))
                {
                    MessageBox.Show("Le répertoire du JDK n'existe pas !");
                    return;
                }

                txtJDK.Text = JDKBrowse.SelectedPath;
            }
        }

        private void chkResDynamique_CheckedChanged(object sender, EventArgs e)
        {
            numWidthWindow.Enabled = numHeightWindow.Enabled = !chkResDynamique.Checked;
        }

        private void btnpotrace_Click(object sender, EventArgs e)
        {
            if (ofdPotrace.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (!File.Exists(ofdPotrace.FileName))
                {
                    MessageBox.Show("Le chemin vers l'exécutable potrace doit être valide !");
                    return;
                }

                txtPotrace.Text = ofdPotrace.FileName;
            }
        }
    }
}
