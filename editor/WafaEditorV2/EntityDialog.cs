﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PBInterface;
using System.IO;
using System.Security.AccessControl;
using System.Diagnostics;

namespace WafaEditorV2
{
    public partial class EntityDialog : Form
    {

        const int WORLD = 1;
        const int VIEW = 2;
        const int PROJ = 4;

        public DialogMode mode { get; set; }

        public Level reflevel { get; set; }


        List<ContainerType> result;

        List<ContainerType> shader_type;

        List<ContainerType> mesh_type;

        List<MultiShapeInfo> shapeinfo_list = new List<MultiShapeInfo>();
        int shape_info_indx = -1;

        List<ShadersParameters> shader_params_list = new List<ShadersParameters>();
        int shader_param_indx = -1;

        List<RenderPass> extra_pass = new List<RenderPass>();


        List<string> group_list = new List<string>();
        Layer ref_layer;

        List<string> ref_module_script;
        string ref_script_base_path;

        private List<ResxInfo> ref_game_resources;

        List<string> animation_id = new List<string>();

        List<string> light_id = new List<string>();

        public EntityDialog(Level preflevel,List<ResxInfo> game_resources,Layer current_layer,List<string> pmodule_script,string script_base_path)
        {
            InitializeComponent();

            ref_game_resources = game_resources;
            ref_module_script = pmodule_script;
            ref_script_base_path = script_base_path;

            cmbResxTextures.Items.AddRange(ref_game_resources.FindAll(p => p.type == ResxType.texture).ToArray());
            cmbResxTextures.DisplayMember = "ident";
            cmbResxTextures.ValueMember = "ident";
            cmbResxTextures.SelectedIndex = 0;

            if (!string.IsNullOrEmpty(script_base_path))
                cmbScript.Items.AddRange(ref_module_script.ToArray());
            else
            {
                cmbScript.Enabled = false;
                txtCtor.Enabled = false;
            }


            mode = DialogMode.Ajout;

            reflevel = preflevel;
            ref_layer = current_layer;

            string[] shape_types = Enum.GetNames(typeof(PBInterface.TypePhysics));
            int[] shape_types_val = (int[])Enum.GetValues(typeof(PBInterface.TypePhysics));

            result = new List<ContainerType>(shape_types.Length);

            for (int i = 0; i < shape_types.Length; i++)
                result.Add(new ContainerType(shape_types[i], shape_types_val[i]));

            cbShapeType.Items.AddRange(result.ToArray());
            cbShapeType.DisplayMember = "Key";
            cbShapeType.ValueMember = "Value";
            cbShapeType.SelectedIndex = 0;

            cmbTypmcol.Items.AddRange(result.ToArray());
            cmbTypmcol.DisplayMember = "Key";
            cmbTypmcol.ValueMember = "Value";
            cmbTypmcol.SelectedIndex = 0;

            string[] template_3d_types = Enum.GetNames(typeof(PBInterface.TypeMesh));
            int[] template_3d_val = (int[])Enum.GetValues(typeof(PBInterface.TypeMesh));

            mesh_type = new List<ContainerType>(template_3d_types.Length);

            for (int i = 0; i < template_3d_types.Length; i++)
            {
                if ((TypeMesh)i != TypeMesh.LNone)
                    mesh_type.Add(new ContainerType(template_3d_types[i], template_3d_val[i]));
            }

            cmb3D.Items.AddRange(mesh_type.ToArray());
            cmb3D.DisplayMember = "Key";
            cmb3D.ValueMember = "Value";
            cmb3D.SelectedIndex = 0;

            //shader params tab

            string[] shader_types = Enum.GetNames(typeof(PBInterface.TypeParamShader));
            int[] shader_types_val = (int[])Enum.GetValues(typeof(PBInterface.TypeParamShader));

            shader_type = new List<ContainerType>(shader_types.Length);

            for (int i = 0; i < shader_types.Length; i++)
                shader_type.Add(new ContainerType(shader_types[i], shader_types_val[i]));


            cmbParamsType.Items.AddRange(shader_type.ToArray());
            cmbParamsType.DisplayMember = "Key";
            cmbParamsType.ValueMember = "Value";
            cmbParamsType.SelectedIndex = 0;


            cmbTexture.Items.AddRange(ref_game_resources.FindAll(p => p.type == ResxType.texture).ToArray());
            cmbTexture.DisplayMember = "ident";
            cmbTexture.ValueMember = "ident";
            cmbTexture.SelectedIndex = 0;

            panelConstantes.Visible = panelTexture.Visible = panelLumiere.Visible = false;

            group_list.AddRange((from u in current_layer.GroupArrays select u.GroupId));

            cmbGroup.Items.AddRange(group_list.ToArray());

            light_id.AddRange((from u in current_layer.LightArrays select u.LightId));

            cmbLightList.Items.AddRange(light_id.ToArray());

            shapeinfo_list.Clear();
            shader_params_list.Clear();

            //position auto panel
            Point newpos = new Point(panelFloat.Location.X, panelFloat.Location.Y);
            panelConstantes.Location = panelTexture.Location = panelLumiere.Location =  panelInt.Location = newpos;
            panelFloat.Enabled = false;

            listExtraPass.DisplayMember = "PassId";

        }

        public void FillData(Entity data)
        {
            mode = DialogMode.Modif;
            this.Tag = data;

            this.Text = Editor.EntiteModif;

            ResxInfo entity_texture = ref_game_resources.Find(p => p.ident == data.Tileset);
            
            if(entity_texture != null)
                cmbResxTextures.SelectedItem = entity_texture;
                
            cmbScript.Text = data.ScriptModule;
            txtCtor.Text = data.ScriptCtor;
            txtidentity.Text = data.EntityId;

            if (cmbResxTextures.SelectedItem != null && !string.IsNullOrEmpty(data.StartAnimation) && !string.IsNullOrEmpty(data.AnimationFile))
            {
                //load animation resx
                ResxInfo anim_info = ref_game_resources.Find(p => p.ident == data.AnimationFile);

                if (anim_info != null)
                    cmbAnim.SelectedItem = data.StartAnimation;
            }

            numheight.Value = data.Height;
            numwidth.Value = data.Width;

            cbShapeType.SelectedItem = result.Find(p => p.Value == data.ShapeType);

            if(data.HasMesh)
                cmb3D.SelectedItem = mesh_type.Find(p => p.Value == data.MeshObj);

            chk3D.Checked = data.HasMesh;

            cmbGroup.Text = data.GroupId;
            chkController.Checked = data.HasController;
            chkHasCollisions.Checked = data.NoCollisions;
            chkCallback.Checked = data.HasCallback;
            chknodraw.Checked = data.Nodraw;

            chkCustomCol.Checked = data.CustomCollisions;
            chkKinematicBody.Checked = data.KinematicBody;
            numOffsetx.Value = data.Offsetx;
            numOffsetY.Value = data.Offsety;
            numRepeatX.Value = data.RepeatX;
            numRepeatY.Value = data.RepeatY;
            numStartFrame.Value = data.StartFrame;
            txtProgramShader.Text = data.ShaderProgram;
            numColRadius.Value = (decimal)data.ColRadius;

            if (data.CustomCollisions)
            {
                numColWidth.Value = data.ColWidth;
                numColHeight.Value = data.ColHeight;
            }

            if (data.NoCollisions)
            {
                numparallaxx.Value = (decimal)data.ParallaxX;
                numparallaxy.Value = (decimal)data.ParallaxY;
            }

            shapeinfo_list.AddRange(data.CustomShapesDefs);

            if (shapeinfo_list.Count > 0)
            {
                loadcustomcol(0);
            }

            if (shapeinfo_list.Count > 1)
                btnNextCol.Enabled = true;

            shader_params_list.AddRange(data.Parameters);

            if (shader_params_list.Count > 0)
            {
                loadshaderparams(0);
                btnSupprParam.Enabled = true;
            }

            if (shader_params_list.Count > 1)
                btnNextParam.Enabled = true;

            extra_pass.AddRange(data.ExtraPasses);

            if (extra_pass.Count > 0)
            {
                listExtraPass.Items.AddRange(extra_pass.ToArray());
            }

            chkWorld.Checked = ((data.MatFlag & WORLD) > 0);
            chkView.Checked = ((data.MatFlag & VIEW) > 0);
            chkProj.Checked = ((data.MatFlag & PROJ) > 0);

            if(data.SpriteCount > 1)
            {
                numSprite.Value = (decimal)data.SpriteCount;
            }

            chk_script_init_only.Checked = data.ScriptInitOnly;

            chkdupliquetexture.Checked = data.DuplicateTextureOnly;
            

            this.Tag = data;
            mode = DialogMode.Modif;
        }


        private void loadshaderparams(int oshader_param_indx)
        {
            ShadersParameters c_param = shader_params_list[oshader_param_indx];

            shader_param_indx = oshader_param_indx;

            cmbParamsType.SelectedItem = shader_type.Find(p => p.Value == c_param.ParamType);

            if(cmbParamsType.SelectedItem == null)
            {
                cmbParamsType.SelectedItem = shader_type.Find(p => p.Value == (int)TypeParamShader.UniformFloat);
                c_param.ParamType =  (int)TypeParamShader.UniformFloat;
            }

            txtParamName.Text = c_param.ParamName;

            panelFloat.Visible = panelConstantes.Visible = panelTexture.Visible = panelLumiere.Visible = panelInt.Visible = false;

            switch (c_param.ParamType)
            {
                case (int)TypeParamShader.UniformFloat:

                    numComponents.Value = (decimal)c_param.UniformFloat.NumComponent;

                    for (int vi = 0; vi <c_param.UniformFloat.NumComponent;vi++)
                    {
                        if (vi == 0)
                            numXR.Value = (decimal)c_param.UniformFloat.V1;
                        else if (vi == 1)
                            numYG.Value = (decimal)c_param.UniformFloat.V2;
                        else if (vi == 2)
                            numZB.Value = (decimal)c_param.UniformFloat.V3;
                        else if (vi == 3)
                            numWA.Value  = (decimal)c_param.UniformFloat.V4;
                    }

                    panelFloat.Visible = panelFloat.Enabled =  true;

                    break;
                case (int)TypeParamShader.UniformInt:
                    numUniformInt.Value = (decimal)c_param.UniformInt.V1;
                    panelInt.Visible = panelInt.Enabled = true;
                    break;
                case (int)TypeParamShader.Texture:

                    cmbTexture.SelectedItem = ref_game_resources.Find(p => p.ident == c_param.UniformTexture.TextureId);
                    numSlot.Value = (decimal)c_param.UniformTexture.BindSlot;
                    panelTexture.Visible = panelTexture.Enabled =  true;
                    chkSetOffset.Checked = c_param.UniformTexture.SetOffset;
                    chkSize.Checked = c_param.UniformTexture.SetSize;
                    break;
                case (int)TypeParamShader.Light:
                    cmbLightList.SelectedItem = c_param.UniformLight.LightId;
                    chkLightNormalize.Checked = c_param.UniformLight.Normalized;
                    panelLumiere.Visible = true;
                    break;
                case (int)TypeParamShader.Constant:

                    cmbConst.SelectedItem = c_param.UniformConst.ConstName;
                    panelConstantes.Visible = panelConstantes.Enabled = true;

                    if (!string.IsNullOrEmpty(c_param.UniformConst.ExtraParam))
                        txtExtraParam.Text = c_param.UniformConst.ExtraParam;
                    else
                        txtExtraParam.Text = string.Empty;

                    break;
            }

            label19.Enabled = label22.Enabled = txtParamName.Enabled = cmbParamsType.Enabled = true;

        }

        private void saveshaderparams(int oshader_param_indx)
        {
            ShadersParameters c_param = shader_params_list[oshader_param_indx];

            c_param.ParamType = ((ContainerType)cmbParamsType.SelectedItem).Value;

            c_param.ParamName = txtParamName.Text;

            switch (c_param.ParamType)
            {
                case (int)TypeParamShader.UniformFloat:
                    if (c_param.UniformFloat == null)
                        c_param.UniformFloat = new ShaderFloat();

                   c_param.UniformFloat.NumComponent =  Convert.ToInt32(numComponents.Value);

                    for (int vi = 0; vi < (int)numComponents.Value; vi++)
                    {
                        if (vi == 0)
                           c_param.UniformFloat.V1 =  (float)numXR.Value;
                        else if (vi == 1)
                            c_param.UniformFloat.V2 = (float)numYG.Value;
                        else if (vi == 2)
                            c_param.UniformFloat.V3 = (float)numZB.Value;
                        else if (vi == 3)
                            c_param.UniformFloat.V4 = (float)numWA.Value;
                    }

                    break;

                case (int)TypeParamShader.UniformInt:
                    if (c_param.UniformInt == null)
                        c_param.UniformInt = new ShaderInt();

                    c_param.UniformInt.V1 = Convert.ToInt32(numUniformInt.Value);
                    break;
                case (int)TypeParamShader.Texture:
                    if (c_param.UniformTexture == null)
                        c_param.UniformTexture = new ShaderTexture();

                    c_param.UniformTexture.TextureId = ((ResxInfo)cmbTexture.SelectedItem).ident;
                    c_param.UniformTexture.BindSlot  = Convert.ToInt32(numSlot.Value);
                    c_param.UniformTexture.SetOffset = chkSetOffset.Checked;
                    c_param.UniformTexture.SetSize = chkSize.Checked;
                    break;
                case (int)TypeParamShader.Light:
                    if (c_param.UniformLight == null)
                        c_param.UniformLight = new ShaderLight();

                    c_param.UniformLight.LightId = cmbLightList.SelectedItem.ToString();
                    c_param.UniformLight.Normalized = chkLightNormalize.Checked;
                    break;
                case (int)TypeParamShader.Constant:
                    if (c_param.UniformConst == null)
                        c_param.UniformConst = new ShaderConst();

                    c_param.UniformConst.ConstName = cmbConst.SelectedItem.ToString();

                    if (!string.IsNullOrEmpty(txtExtraParam.Text))
                        c_param.UniformConst.ExtraParam = txtExtraParam.Text;
                    else
                        c_param.UniformConst.ExtraParam = string.Empty;

                    break;
            }
        }

        private void loadcustomcol(int multi_shape_indx)
        {
            cmbTypmcol.SelectedItem = result.Find(p => p.Value == shapeinfo_list[multi_shape_indx].Type);

            if (cmbTypmcol.SelectedItem == null)
                cmbTypmcol.SelectedItem = result.Find(p => p.Value == (int)TypePhysics.Box);

            numwidthmcol.Value = shapeinfo_list[multi_shape_indx].Width;
            numHeightmcol.Value = shapeinfo_list[multi_shape_indx].Height;

            if (shapeinfo_list[multi_shape_indx].Offset != null)
            {
                numOffxmcol.Value = shapeinfo_list[multi_shape_indx].Offset.X;
                numOffymcol.Value = shapeinfo_list[multi_shape_indx].Offset.Y;
            }
            else
            {
                numOffxmcol.Value = 0;
                numOffymcol.Value = 0;
                shapeinfo_list[multi_shape_indx].Offset = new Wvector();
            }

            shape_info_indx = multi_shape_indx;

            grpMulticol.Enabled = true;
            cmbTypmcol.Enabled = true;
        }

        private void savecustomcol(int multi_shape_indx)
        {
            shapeinfo_list[multi_shape_indx].Type = ((ContainerType)cmbTypmcol.SelectedItem).Value;
            shapeinfo_list[multi_shape_indx].Width = (int)numwidthmcol.Value;
            shapeinfo_list[multi_shape_indx].Height = (int)numHeightmcol.Value;

            if (shapeinfo_list[multi_shape_indx].Offset == null)
                shapeinfo_list[multi_shape_indx].Offset = new Wvector();

            shapeinfo_list[multi_shape_indx].Offset.X = (int)numOffxmcol.Value;
            shapeinfo_list[multi_shape_indx].Offset.Y = (int)numOffymcol.Value;
        }


        private void btnOk_Click(object sender, EventArgs e)
        {
            Entity cEntity = null;

            if (mode == DialogMode.Ajout)
                cEntity = new Entity();
            else
                cEntity = (Entity)this.Tag;

            if (string.IsNullOrEmpty(txtidentity.Text.Trim()))
            {
                MessageBox.Show("L'identifiant de l'entité est obligatoire !");
                return;
            }

            if (cmbResxTextures.SelectedIndex == -1)
            {
                MessageBox.Show("un fichier de tiles est obligatoire ! ");
                return;
            }

            if (mode == DialogMode.Ajout)
            {
                if (EditorUtilities.ObjectIdExist(reflevel, txtidentity.Text.Trim()))
                {
                    MessageBox.Show("cet identifiant existe déjà ! ");
                    return;
                }
            }
            else if (mode == DialogMode.Modif)
            {
                if (EditorUtilities.ObjectIdExist(reflevel, txtidentity.Text.Trim(), cEntity))
                {
                    MessageBox.Show("cet identifiant existe déjà ! ");
                    return;
                }
            }

            if (shapeinfo_list.Count > 0)
            {
                this.savecustomcol(shape_info_indx);
                cEntity.CustomShapesDefs.Clear();
                cEntity.CustomShapesDefs.AddRange(shapeinfo_list);
            }
            else
            {
                cEntity.CustomShapesDefs.Clear();
            }

            if (shader_params_list.Count > 0)
            {
                this.saveshaderparams(shader_param_indx);
                cEntity.Parameters.Clear();
                cEntity.Parameters.AddRange(shader_params_list);
            }
            else
            {
                cEntity.Parameters.Clear();
            }

            cEntity.ExtraPasses.Clear();

            if (extra_pass.Count > 0)
            {
                //check extra pass validity

                List<int> zindex_list = new List<int>();

                foreach (RenderPass pass in extra_pass)
                {
                    if (zindex_list.Contains(pass.PassZindex))
                    {
                        MessageBox.Show("Les passes de rendu additionelles ne sont pas valides ! Au moins 2 passes ont le même z-index");
                        return;
                    }

                    zindex_list.Add(pass.PassZindex);
                }
               
                cEntity.ExtraPasses.AddRange(extra_pass);
            }

            

            cEntity.EntityId = txtidentity.Text;
            cEntity.Tileset = ((ResxInfo)cmbResxTextures.SelectedItem).ident;
            cEntity.EditorTileset = string.Empty;


            cEntity.ScriptModule = cmbScript.Text;
            cEntity.ScriptCtor = txtCtor.Text;


            string animation_filename = cEntity.Tileset.Substring(0,cEntity.Tileset.Length - 4) + ".awf";

            if (ref_game_resources.Any<ResxInfo>(p => p.ident == animation_filename && p.type == ResxType.animation))
                cEntity.AnimationFile = animation_filename;
            else
                cEntity.AnimationFile = string.Empty;

            cEntity.Width = (int)numwidth.Value;
            cEntity.Height = (int)numheight.Value;

            cEntity.StartAnimation = (string)cmbAnim.SelectedItem;

            if (cEntity.StartAnimation == null)
                cEntity.StartAnimation = string.Empty;

            cEntity.HasController = chkController.Checked;
            cEntity.HasCallback = chkCallback.Checked;

            cEntity.ShapeType = ((ContainerType)cbShapeType.SelectedItem).Value;
            cEntity.NoCollisions = chkHasCollisions.Checked;
            cEntity.KinematicBody = chkKinematicBody.Checked;
            cEntity.Nodraw = chknodraw.Checked;

            cEntity.HasMesh = chk3D.Checked;

            if (cEntity.HasMesh)
                cEntity.MeshObj = ((ContainerType)cmb3D.SelectedItem).Value;

            cEntity.MatFlag = (((chkWorld.Checked) ? WORLD : 0) | ((chkView.Checked) ? VIEW : 0) | ((chkProj.Checked) ? PROJ : 0));

            cEntity.CustomCollisions = chkCustomCol.Checked;

            cEntity.Offsetx = (int)numOffsetx.Value;
            cEntity.Offsety = (int)numOffsetY.Value;
            cEntity.RepeatX = (int)numRepeatX.Value;
            cEntity.RepeatY = (int)numRepeatY.Value;
            cEntity.StartFrame = (int)numStartFrame.Value;
            cEntity.ColRadius = (float)numColRadius.Value;

            cEntity.ShaderProgram = txtProgramShader.Text;

            cEntity.SpriteCount = (int)numSprite.Value;

            cmbGroup.SelectedText.Trim();

            string old_group_id = cEntity.GroupId;

            cEntity.GroupId = cmbGroup.Text;

            if (mode == DialogMode.Modif)
            {
                if (old_group_id != cEntity.GroupId)
                {
                    if (!string.IsNullOrEmpty(old_group_id))
                    {
                        GroupEntity grp = ref_layer.GroupArrays.Find(p => p.GroupId== old_group_id);
                        grp.IdArrays.Remove(cEntity.EntityId);

                        if (grp.IdArrays.Count == 0)
                            ref_layer.GroupArrays.Remove(grp);
                    }

                    if (!string.IsNullOrEmpty(cEntity.GroupId))
                    {
                        GroupEntity cgroup = ref_layer.GroupArrays.Find(p => p.GroupId == cEntity.GroupId);

                        if (cgroup != null)
                        {
                            if (!cgroup.IdArrays.Contains(cEntity.EntityId))
                                cgroup.IdArrays.Add(cEntity.EntityId);
                        }
                        else
                        {
                            cgroup = new GroupEntity();
                            cgroup.GroupId = cEntity.GroupId;
                            cgroup.IdArrays.Add(cEntity.EntityId);
                            ref_layer.GroupArrays.Add(cgroup);
                        }
                    }
                }
            }

            

            if (cEntity.CustomCollisions)
            {
                cEntity.ColWidth = (int)numColWidth.Value;
                cEntity.ColHeight = (int)numColHeight.Value;
            }

            if (cEntity.NoCollisions)
            {
                cEntity.ParallaxX = (float)numparallaxx.Value;
                cEntity.ParallaxY = (float)numparallaxy.Value;
            }

            cEntity.ScriptInitOnly = chk_script_init_only.Checked;

            cEntity.DuplicateTextureOnly = chkdupliquetexture.Checked;

            this.Tag = cEntity;

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }


        private void chkCustomCol_CheckedChanged(object sender, EventArgs e)
        {
            grpCustomCollisions.Enabled = chkCustomCol.Checked;
        }

        private void chkHasCollisions_CheckedChanged(object sender, EventArgs e)
        {
            grpparallax.Enabled = chkHasCollisions.Checked;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void EntityDialog_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void cmbResxTextures_SelectedIndexChanged(object sender, EventArgs e)
        {
            //load animation resx
            string ident =  ((ResxInfo)cmbResxTextures.SelectedItem).ident;

            ResxInfo anim_info = ref_game_resources.Find(p => p.ident == ident.Substring(0,ident.LastIndexOf('.'))+".awf");

            animation_id.Clear();
            cmbAnim.Items.Clear();

            if (anim_info != null)
            {
                using (var file_stream = File.OpenRead(anim_info.path))
                {
                    //add virtual textures
                    AnimationList anim_def = ProtoBuf.Serializer.Deserialize<AnimationList>(file_stream);
                    animation_id.AddRange((from u in anim_def.AnimArrays select u.Name));
                }

                cmbAnim.Items.AddRange(animation_id.ToArray());
            }
        }

        private void btnNewScript_Click(object sender, EventArgs e)
        {
            //création d'un nouveau module lua

            string lua_content = string.Format(ScriptUtility.LUA_TEMPLATE_MODULE, cmbScript.Text);

            string path = Path.Combine(ref_script_base_path,cmbScript.Text+".lua");

            if (!File.Exists(path))
            {
                byte[] char_array = Encoding.UTF8.GetBytes(lua_content);
                FileStream stream = File.Create(path, char_array.Length);

                stream.Write(char_array, 0, char_array.Length);
                stream.Close();

                ref_module_script.Add(cmbScript.Text);

                try
                {
                    //ouverture du fichier
                    Process.Start(path);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erreur lors de l'ouverture du fichier, veuillez vérifier l'association des fichiers .lua "+ex.ToString(), "Erreur programme lua", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            else
            {
                MessageBox.Show("Impossible de créer le fichier " + path + " un fichier du même nom existe déjà !","Fichier existant",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
            }

        }

        private void cmbScript_TextUpdate(object sender, EventArgs e)
        {
            btnNewScript.Visible = (!string.IsNullOrEmpty(cmbScript.Text) && !ref_module_script.Contains(cmbScript.Text));
        }

        #region custom collisions

        private void cbShapeType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (((ContainerType)cbShapeType.SelectedItem).Value == (int)TypePhysics.MultiShapes)
            {
                panelMultiCol.Visible = true;
                panelNormalCol.Visible = false;
            }
            else
            {
                panelMultiCol.Visible = false;
                panelNormalCol.Visible = true;
                shape_info_indx = -1;
                shapeinfo_list.Clear();
                grpMulticol.Enabled = false;
                cmbTypmcol.Enabled = false;
                btnPrevCol.Enabled = false;
                btnNextCol.Enabled = false;
            }
        }

        private void btnPrevCol_Click(object sender, EventArgs e)
        {
            this.savecustomcol(shape_info_indx);
            btnPrevCol.Enabled = (--shape_info_indx != 0);
            this.loadcustomcol(shape_info_indx);
            numIndexCol.Value = shape_info_indx;
            btnNextCol.Enabled = true;
        }

        private void btnNextCol_Click(object sender, EventArgs e)
        {
            this.savecustomcol(shape_info_indx);
            btnNextCol.Enabled = (++shape_info_indx != shapeinfo_list.Count-1);
            numIndexCol.Value = shape_info_indx;
            this.loadcustomcol(shape_info_indx);
            btnPrevCol.Enabled = true;
        }


        private void numIndexCol_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == '\r')
            {
                int selectedindx = Convert.ToInt32(numIndexCol.Value);

                if(selectedindx >= 0 || Convert.ToInt32(numIndexCol.Value) < shapeinfo_list.Count)
                {
                    this.savecustomcol(shape_info_indx);
                    this.loadcustomcol(selectedindx);
                    shape_info_indx = selectedindx;
                    btnNextCol.Enabled = (selectedindx != shapeinfo_list.Count - 1);
                    btnPrevCol.Enabled = (selectedindx != 0);
                }
                else
                {
                    numIndexCol.Value = shape_info_indx;
                }
                
            }

        }

        private void btnajoutmulticol_Click(object sender, EventArgs e)
        {
            shapeinfo_list.Add(new MultiShapeInfo());

            if(shape_info_indx != -1)
                this.savecustomcol(shape_info_indx);

            shape_info_indx = shapeinfo_list.Count - 1;

            loadcustomcol(shape_info_indx);
            numIndexCol.Value = shape_info_indx;

            if (shape_info_indx > 0)
                btnPrevCol.Enabled = true;

        }

        private void btnSupprCollision_Click(object sender, EventArgs e)
        {
            if (shape_info_indx != -1)
            {
                shapeinfo_list.RemoveAt(shape_info_indx);

                if (shapeinfo_list.Count > 0)
                {
                    if(shape_info_indx >= shapeinfo_list.Count)
                        shape_info_indx--;

                    this.loadcustomcol(shape_info_indx);
                }
                else
                {
                    cmbTypmcol.SelectedItem = result.Find(p => p.Value == (int)TypePhysics.Box);

                    numwidthmcol.Value = 0.0m;
                    numHeightmcol.Value = 0.0m;



                        numOffxmcol.Value = 0;
                        numOffymcol.Value = 0;


                    grpMulticol.Enabled = true;
                    cmbTypmcol.Enabled = true;
                }
            }
        }

        #endregion



        #region Shader params

        private void btnPrevParam_Click(object sender, EventArgs e)
        {
            this.saveshaderparams(shader_param_indx);
            btnPrevParam.Enabled = (--shader_param_indx != 0);
            this.loadshaderparams(shader_param_indx);
            btnNextParam.Enabled = true;
        }

        private void btnNextParam_Click(object sender, EventArgs e)
        {
            this.saveshaderparams(shader_param_indx);
            btnNextParam.Enabled = (++shader_param_indx != shader_params_list.Count - 1);
            this.loadshaderparams(shader_param_indx);
            btnPrevParam.Enabled = true;
        }



        private void cmbParamsType_SelectedIndexChanged(object sender, EventArgs e)
        {
            int current_type = ((ContainerType)cmbParamsType.SelectedItem).Value;

            panelFloat.Visible = panelFloat.Enabled = (current_type == (int)TypeParamShader.UniformFloat);
            panelConstantes.Visible = panelConstantes.Enabled = (current_type == (int)TypeParamShader.Constant);
            panelTexture.Visible = panelTexture.Enabled =  (current_type == (int)TypeParamShader.Texture);
            panelLumiere.Visible = panelLumiere.Enabled =  (current_type == (int)TypeParamShader.Light);
            panelInt.Visible = panelInt.Enabled = (current_type == (int)TypeParamShader.UniformInt);

            if (shader_param_indx != -1 && shader_params_list.Count > 0)
            {
                shader_params_list[shader_param_indx].UniformFloat = (current_type == (int)TypeParamShader.UniformFloat) ? shader_params_list[shader_param_indx].UniformFloat : null;

                shader_params_list[shader_param_indx].UniformConst = (current_type == (int)TypeParamShader.Constant) ? shader_params_list[shader_param_indx].UniformConst : null;

                shader_params_list[shader_param_indx].UniformTexture = (current_type == (int)TypeParamShader.Texture) ? shader_params_list[shader_param_indx].UniformTexture : null;

                shader_params_list[shader_param_indx].UniformLight = (current_type == (int)TypeParamShader.Light) ? shader_params_list[shader_param_indx].UniformLight : null;

                shader_params_list[shader_param_indx].UniformInt = (current_type == (int)TypeParamShader.UniformInt) ? shader_params_list[shader_param_indx].UniformInt : null;
            }
        }

        private void BtnAjoutParam_Click(object sender, EventArgs e)
        {
            shader_params_list.Add(new ShadersParameters());

            if (shader_param_indx != -1)
                this.saveshaderparams(shader_param_indx);

            shader_param_indx = shader_params_list.Count - 1;

            //default values
            shader_params_list[shader_param_indx].ParamType = (int)TypeParamShader.UniformFloat;
            shader_params_list[shader_param_indx].UniformFloat = new ShaderFloat();
            shader_params_list[shader_param_indx].UniformFloat.NumComponent = 1;
            panelFloat.Enabled = true;
            label19.Enabled = label22.Enabled = txtParamName.Enabled = cmbParamsType.Enabled = true;

            loadshaderparams(shader_param_indx);

            btnSupprParam.Enabled = true;

            if (shader_param_indx > 0)
                btnPrevParam.Enabled = true;
        }

        private void btnSupprParam_Click(object sender, EventArgs e)
        {
            if (shader_param_indx != -1 && shader_params_list.Count > 0)
            {
                shader_params_list.RemoveAt(shader_param_indx);

                if (shader_params_list.Count > 0)
                {
                    if (shader_param_indx >= shader_params_list.Count)
                        shader_param_indx--;

                    this.loadshaderparams(shader_param_indx);
                }
                else
                {
                    label19.Enabled = label22.Enabled = txtParamName.Enabled = cmbParamsType.Enabled = false;

                    cmbParamsType.SelectedItem = shader_type.Find(p => p.Value == (int)TypeParamShader.UniformFloat);

                    numXR.Value = 0.0m;
                    numYG.Value = 0.0m;

                    numZB.Value = 0.0m;
                    numWA.Value = 0.0m;

                    numComponents.Value = 1.0m;
                }

                if (shader_params_list.Count == 0)
                    btnSupprParam.Enabled = false;
            }
        }

        #endregion

        private void numComponents_ValueChanged(object sender, EventArgs e)
        {
            lblparamy.Visible =  numYG.Visible = ((int)numComponents.Value >= 2);
            lblparamz.Visible =  numZB.Visible = ((int)numComponents.Value >= 3);
            lblparamw.Visible =  numWA.Visible = ((int)numComponents.Value >= 4);

        }


        private void btnAddpass_Click(object sender, EventArgs e)
        {
            RenderPass rpass = new RenderPass();
            rpass.PassId = "pass"+(extra_pass.Count + 1).ToString();
            rpass.PassZindex = 1;
            extra_pass.Add(rpass);
            listExtraPass.Items.Add(rpass);

            listExtraPass.SelectedItem = rpass;

            lblzindxpass.Visible = numzindexpass.Visible = true;
            numzindexpass.Value = (decimal)rpass.PassZindex;
        }

        private void SupprPass_Click(object sender, EventArgs e)
        {
            if (listExtraPass.SelectedItem == null)
                return;

            RenderPass rpass = listExtraPass.SelectedItem as RenderPass;
            extra_pass.Remove(rpass);
            listExtraPass.Items.Remove(listExtraPass.SelectedItem);

            //reorg pass id

            uint ipass = 1;

            foreach (RenderPass pass in extra_pass)
                pass.PassId = "pass" + (ipass++);

            listExtraPass.SelectedItem = null;
            lblzindxpass.Visible = numzindexpass.Visible = false;

        }

        private void listExtraPass_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listExtraPass.SelectedItem == null)
                return;

            RenderPass rpass = listExtraPass.SelectedItem as RenderPass;
            lblzindxpass.Visible = numzindexpass.Visible = SupprPass.Enabled =  true;
            numzindexpass.Value = (decimal)rpass.PassZindex;
        }

        private void numzindexpass_ValueChanged(object sender, EventArgs e)
        {
            if (listExtraPass.SelectedItem == null)
                return;


            RenderPass rpass = listExtraPass.SelectedItem as RenderPass;

            rpass.PassZindex = Convert.ToInt32(numzindexpass.Value);
        }

        private void chk3D_CheckedChanged(object sender, EventArgs e)
        {
            cmb3D.Enabled = chk3D.Checked;
        }

        private void btnCopycollisions_Click(object sender, EventArgs e)
        {
            EntitySelector selector = new EntitySelector(reflevel);

            if(selector.ShowDialog() == DialogResult.OK)
            {
                Entity selectedentity = selector.Tag as Entity;

                cbShapeType.SelectedItem = result.Find(p => p.Value == selectedentity.ShapeType);

                chkCustomCol.Checked = selectedentity.CustomCollisions;
                chkKinematicBody.Checked = selectedentity.KinematicBody;
             
                numColRadius.Value = (decimal)selectedentity.ColRadius;

                if (selectedentity.CustomCollisions)
                {
                    numColWidth.Value = selectedentity.ColWidth;
                    numColHeight.Value = selectedentity.ColHeight;
                }

                if (selectedentity.NoCollisions)
                {
                    numparallaxx.Value = (decimal)selectedentity.ParallaxX;
                    numparallaxy.Value = (decimal)selectedentity.ParallaxY;
                }

                shapeinfo_list.Clear();

                shapeinfo_list.AddRange(selectedentity.CustomShapesDefs);

                if (shapeinfo_list.Count > 0)
                {
                    loadcustomcol(0);
                }

                if (shapeinfo_list.Count > 1)
                    btnNextCol.Enabled = true;

            }

        }
    }
}
