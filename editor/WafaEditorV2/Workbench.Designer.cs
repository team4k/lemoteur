﻿namespace WafaEditorV2
{
    partial class Workbench
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Workbench));
            System.Windows.Forms.TreeNode treeNode17 = new System.Windows.Forms.TreeNode("Entités");
            System.Windows.Forms.TreeNode treeNode18 = new System.Windows.Forms.TreeNode("déclencheurs");
            System.Windows.Forms.TreeNode treeNode19 = new System.Windows.Forms.TreeNode("Textes");
            System.Windows.Forms.TreeNode treeNode20 = new System.Windows.Forms.TreeNode("Sources sonores");
            System.Windows.Forms.TreeNode treeNode21 = new System.Windows.Forms.TreeNode("Source lumières");
            System.Windows.Forms.TreeNode treeNode22 = new System.Windows.Forms.TreeNode("Particules");
            System.Windows.Forms.TreeNode treeNode23 = new System.Windows.Forms.TreeNode("Waypoint");
            System.Windows.Forms.TreeNode treeNode24 = new System.Windows.Forms.TreeNode("Collision");
            this.mainMenu = new System.Windows.Forms.MenuStrip();
            this.fichierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nouveauNiveauToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chargerNiveauToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.niveauxRécentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.preftoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enregistrerNiveauToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fermerNiveauToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionFragmentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nouveauFragmentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chargerFragmentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.préférencesFragmentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enregistrerFragmentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fermerFragmentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionMondeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nouveauMondeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chargerMondeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enregistrerMondeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fermerMondeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.preferencesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.annulerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rétablirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sélectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.focusJoueurToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.placerJoueurToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ajouterMapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modifierMapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.supprimerMapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.calquesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ajouterUnCalqueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.supprimerUnCalqueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importerUnCalqueDepuisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.animationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.générerUneAnimationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.supprimerUneAnimationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importerUnFondAniméToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outilsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importerAssetStatiqueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.regénérerUnAssetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consoleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.afficherFichierLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.réinitialiserLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rechargerDLLJeuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recréerToutLesObjetsScriptsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rechargerScriptScèneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fusionAvecAutreNiveauToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.effacerHistoriqueNiveauxToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportDansUnFichierStreamToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.windowsExport = new System.Windows.Forms.ToolStripMenuItem();
            this.androidExport = new System.Windows.Forms.ToolStripMenuItem();
            this.webGLExport = new System.Windows.Forms.ToolStripMenuItem();
            this.tabMaps = new System.Windows.Forms.TabControl();
            this.tilesetBox = new System.Windows.Forms.PictureBox();
            this.tabLayout = new System.Windows.Forms.TabControl();
            this.tabCalques = new System.Windows.Forms.TabPage();
            this.numCurrentFrame = new System.Windows.Forms.NumericUpDown();
            this.lblCurrentFrame = new System.Windows.Forms.Label();
            this.trkCurrentFrame = new System.Windows.Forms.TrackBar();
            this.btnReorder = new System.Windows.Forms.Button();
            this.btnOrderDown = new System.Windows.Forms.Button();
            this.btnOrderUp = new System.Windows.Forms.Button();
            this.calquesList = new System.Windows.Forms.TreeView();
            this.lblAlpha = new System.Windows.Forms.Label();
            this.trackAlpha = new System.Windows.Forms.TrackBar();
            this.tablistitems = new System.Windows.Forms.TabPage();
            this.ItemsList = new System.Windows.Forms.TreeView();
            this.actionBar = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonNewLevel = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonOpenLevel = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonSaveLevel = new System.Windows.Forms.ToolStripButton();
            this.LayTileButton = new System.Windows.Forms.ToolStripButton();
            this.FillTilesButton = new System.Windows.Forms.ToolStripButton();
            this.UndoButton = new System.Windows.Forms.ToolStripButton();
            this.RedoButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonZoomplus = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonZoomminus = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonZoomreset = new System.Windows.Forms.ToolStripButton();
            this.tilsetWatcher = new System.IO.FileSystemWatcher();
            this.saveLevelDialog = new System.Windows.Forms.SaveFileDialog();
            this.openLevelDialog = new System.Windows.Forms.OpenFileDialog();
            this.ctxMenuCalques = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.modifierCalqueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dupliquerCalqueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.grpCollisions = new System.Windows.Forms.GroupBox();
            this.btnKill = new System.Windows.Forms.Button();
            this.btncoleditable_3 = new System.Windows.Forms.Button();
            this.btncoleditable_2 = new System.Windows.Forms.Button();
            this.btncoleditable = new System.Windows.Forms.Button();
            this.btnSlope2 = new System.Windows.Forms.Button();
            this.btnSlope = new System.Windows.Forms.Button();
            this.btnColDestruct = new System.Windows.Forms.Button();
            this.btnCol = new System.Windows.Forms.Button();
            this.btnNoCol = new System.Windows.Forms.Button();
            this.grpObjects = new System.Windows.Forms.GroupBox();
            this.btnCollider = new System.Windows.Forms.Button();
            this.btnWaypoint = new System.Windows.Forms.Button();
            this.btnParticles = new System.Windows.Forms.Button();
            this.btnLight = new System.Windows.Forms.Button();
            this.btnSon = new System.Windows.Forms.Button();
            this.btnTexte = new System.Windows.Forms.Button();
            this.btnEntities = new System.Windows.Forms.Button();
            this.btnTrigger = new System.Windows.Forms.Button();
            this.btnChangeMode = new System.Windows.Forms.Button();
            this.chkcollisions = new System.Windows.Forms.CheckBox();
            this.lbl_zoom = new System.Windows.Forms.Label();
            this.cmbZoom = new System.Windows.Forms.ComboBox();
            this.panelactions = new System.Windows.Forms.Panel();
            this.chkDrawallitems = new System.Windows.Forms.CheckBox();
            this.lblselectedentity = new System.Windows.Forms.Label();
            this.txtselectedentity = new System.Windows.Forms.TextBox();
            this.lblRowPos = new System.Windows.Forms.Label();
            this.chksnapgrid = new System.Windows.Forms.CheckBox();
            this.lblCursorPos = new System.Windows.Forms.Label();
            this.scriptWatcher = new System.IO.FileSystemWatcher();
            this.ctxMenuTrigger = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.redimensionnerTriggerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.repositionnerTriggerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modifierTriggerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.supprimerTriggerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ctxMenuEntity = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.repositionnerEntityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modifierEntityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.supprimerEntityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recréerObjetScriptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.focusEntitéToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ctxTextObject = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.txt_obj_repos = new System.Windows.Forms.ToolStripMenuItem();
            this.txt_obj_update = new System.Windows.Forms.ToolStripMenuItem();
            this.txt_obj_suppr = new System.Windows.Forms.ToolStripMenuItem();
            this.txt_obj_script = new System.Windows.Forms.ToolStripMenuItem();
            this.panelStretch = new System.Windows.Forms.Panel();
            this.resxWatcher = new System.IO.FileSystemWatcher();
            this.localedbWatcher = new System.IO.FileSystemWatcher();
            this.ctxMultiSelection = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.déplacerÉlémentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.grouperÉlémentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.supprimerÉlémentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configWatcher = new System.IO.FileSystemWatcher();
            this.richlog = new System.Windows.Forms.RichTextBox();
            this.ctxSoundSource = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.snd_repos = new System.Windows.Forms.ToolStripMenuItem();
            this.snd_modif = new System.Windows.Forms.ToolStripMenuItem();
            this.snd_suppr = new System.Windows.Forms.ToolStripMenuItem();
            this.ctxLightSource = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.light_repos = new System.Windows.Forms.ToolStripMenuItem();
            this.light_modif = new System.Windows.Forms.ToolStripMenuItem();
            this.light_suppr = new System.Windows.Forms.ToolStripMenuItem();
            this.exportLocation = new System.Windows.Forms.SaveFileDialog();
            this.exportWebgl = new System.Windows.Forms.FolderBrowserDialog();
            this.ctxParticles = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.particles_repos = new System.Windows.Forms.ToolStripMenuItem();
            this.particles_modif = new System.Windows.Forms.ToolStripMenuItem();
            this.particles_suppr = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToStreamFile = new System.Windows.Forms.SaveFileDialog();
            this.openWorldDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveWorldDialog = new System.Windows.Forms.SaveFileDialog();
            this.tabWorldSlice = new System.Windows.Forms.Panel();
            this.lblcurrentzone = new System.Windows.Forms.Label();
            this.lblselectzone = new System.Windows.Forms.Label();
            this.tvZone = new System.Windows.Forms.TreeView();
            this.btnselectzonevalue = new System.Windows.Forms.Button();
            this.numSelectzonevalue = new System.Windows.Forms.NumericUpDown();
            this.lblselectzonevalue = new System.Windows.Forms.Label();
            this.ctxMenuZone = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ajouterZone = new System.Windows.Forms.ToolStripMenuItem();
            this.ajouterZoneEnfant = new System.Windows.Forms.ToolStripMenuItem();
            this.modifierZone = new System.Windows.Forms.ToolStripMenuItem();
            this.supprimerZone = new System.Windows.Forms.ToolStripMenuItem();
            this.openFragmentDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFragmentDialog = new System.Windows.Forms.SaveFileDialog();
            this.renderPanel = new WafaEditorV2.OglPanel();
            this.ctxWaypoint = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.ctxCollider = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.redimensionnerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.btnshowlogdetail = new System.Windows.Forms.Button();
            this.btnCheckIds = new System.Windows.Forms.Button();
            this.mainMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tilesetBox)).BeginInit();
            this.tabLayout.SuspendLayout();
            this.tabCalques.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numCurrentFrame)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trkCurrentFrame)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackAlpha)).BeginInit();
            this.tablistitems.SuspendLayout();
            this.actionBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tilsetWatcher)).BeginInit();
            this.ctxMenuCalques.SuspendLayout();
            this.grpCollisions.SuspendLayout();
            this.grpObjects.SuspendLayout();
            this.panelactions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scriptWatcher)).BeginInit();
            this.ctxMenuTrigger.SuspendLayout();
            this.ctxMenuEntity.SuspendLayout();
            this.ctxTextObject.SuspendLayout();
            this.panelStretch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.resxWatcher)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.localedbWatcher)).BeginInit();
            this.ctxMultiSelection.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.configWatcher)).BeginInit();
            this.ctxSoundSource.SuspendLayout();
            this.ctxLightSource.SuspendLayout();
            this.ctxParticles.SuspendLayout();
            this.tabWorldSlice.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSelectzonevalue)).BeginInit();
            this.ctxMenuZone.SuspendLayout();
            this.ctxWaypoint.SuspendLayout();
            this.ctxCollider.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainMenu
            // 
            this.mainMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fichierToolStripMenuItem,
            this.editionToolStripMenuItem,
            this.sélectionToolStripMenuItem,
            this.mapToolStripMenuItem,
            this.calquesToolStripMenuItem,
            this.animationsToolStripMenuItem,
            this.outilsToolStripMenuItem,
            this.exportToolStripMenuItem});
            this.mainMenu.Location = new System.Drawing.Point(0, 0);
            this.mainMenu.Name = "mainMenu";
            this.mainMenu.Size = new System.Drawing.Size(1525, 24);
            this.mainMenu.TabIndex = 0;
            this.mainMenu.Text = "menuStrip1";
            // 
            // fichierToolStripMenuItem
            // 
            this.fichierToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nouveauNiveauToolStripMenuItem,
            this.chargerNiveauToolStripMenuItem,
            this.niveauxRécentsToolStripMenuItem,
            this.preftoolStripMenuItem,
            this.enregistrerNiveauToolStripMenuItem,
            this.fermerNiveauToolStripMenuItem,
            this.gestionFragmentToolStripMenuItem,
            this.gestionMondeToolStripMenuItem,
            this.quitterToolStripMenuItem});
            this.fichierToolStripMenuItem.Name = "fichierToolStripMenuItem";
            this.fichierToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.fichierToolStripMenuItem.Text = "Fichier";
            // 
            // nouveauNiveauToolStripMenuItem
            // 
            this.nouveauNiveauToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("nouveauNiveauToolStripMenuItem.Image")));
            this.nouveauNiveauToolStripMenuItem.Name = "nouveauNiveauToolStripMenuItem";
            this.nouveauNiveauToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.nouveauNiveauToolStripMenuItem.Size = new System.Drawing.Size(235, 26);
            this.nouveauNiveauToolStripMenuItem.Text = "Nouveau niveau";
            this.nouveauNiveauToolStripMenuItem.Click += new System.EventHandler(this.NouveauNiveau);
            // 
            // chargerNiveauToolStripMenuItem
            // 
            this.chargerNiveauToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("chargerNiveauToolStripMenuItem.Image")));
            this.chargerNiveauToolStripMenuItem.Name = "chargerNiveauToolStripMenuItem";
            this.chargerNiveauToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.chargerNiveauToolStripMenuItem.Size = new System.Drawing.Size(235, 26);
            this.chargerNiveauToolStripMenuItem.Text = "Charger niveau";
            this.chargerNiveauToolStripMenuItem.Click += new System.EventHandler(this.ChargerNiveau);
            // 
            // niveauxRécentsToolStripMenuItem
            // 
            this.niveauxRécentsToolStripMenuItem.Enabled = false;
            this.niveauxRécentsToolStripMenuItem.Name = "niveauxRécentsToolStripMenuItem";
            this.niveauxRécentsToolStripMenuItem.Size = new System.Drawing.Size(235, 26);
            this.niveauxRécentsToolStripMenuItem.Text = "Niveaux récents";
            // 
            // preftoolStripMenuItem
            // 
            this.preftoolStripMenuItem.Enabled = false;
            this.preftoolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("preftoolStripMenuItem.Image")));
            this.preftoolStripMenuItem.Name = "preftoolStripMenuItem";
            this.preftoolStripMenuItem.Size = new System.Drawing.Size(235, 26);
            this.preftoolStripMenuItem.Text = "Préférences niveau";
            this.preftoolStripMenuItem.Click += new System.EventHandler(this.preftoolStripMenuItem_Click);
            // 
            // enregistrerNiveauToolStripMenuItem
            // 
            this.enregistrerNiveauToolStripMenuItem.Enabled = false;
            this.enregistrerNiveauToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("enregistrerNiveauToolStripMenuItem.Image")));
            this.enregistrerNiveauToolStripMenuItem.Name = "enregistrerNiveauToolStripMenuItem";
            this.enregistrerNiveauToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.S)));
            this.enregistrerNiveauToolStripMenuItem.Size = new System.Drawing.Size(235, 26);
            this.enregistrerNiveauToolStripMenuItem.Text = "Enregistrer niveau";
            this.enregistrerNiveauToolStripMenuItem.Click += new System.EventHandler(this.EnregistrerNiveau);
            // 
            // fermerNiveauToolStripMenuItem
            // 
            this.fermerNiveauToolStripMenuItem.Enabled = false;
            this.fermerNiveauToolStripMenuItem.Name = "fermerNiveauToolStripMenuItem";
            this.fermerNiveauToolStripMenuItem.Size = new System.Drawing.Size(235, 26);
            this.fermerNiveauToolStripMenuItem.Text = "Fermer niveau";
            this.fermerNiveauToolStripMenuItem.Click += new System.EventHandler(this.fermerNiveauToolStripMenuItem_Click);
            // 
            // gestionFragmentToolStripMenuItem
            // 
            this.gestionFragmentToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nouveauFragmentToolStripMenuItem,
            this.chargerFragmentToolStripMenuItem,
            this.préférencesFragmentsToolStripMenuItem,
            this.enregistrerFragmentToolStripMenuItem,
            this.fermerFragmentToolStripMenuItem});
            this.gestionFragmentToolStripMenuItem.Name = "gestionFragmentToolStripMenuItem";
            this.gestionFragmentToolStripMenuItem.Size = new System.Drawing.Size(235, 26);
            this.gestionFragmentToolStripMenuItem.Text = "Gestion fragment";
            // 
            // nouveauFragmentToolStripMenuItem
            // 
            this.nouveauFragmentToolStripMenuItem.Name = "nouveauFragmentToolStripMenuItem";
            this.nouveauFragmentToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.nouveauFragmentToolStripMenuItem.Text = "Nouveau fragment";
            this.nouveauFragmentToolStripMenuItem.Click += new System.EventHandler(this.nouveauFragmentToolStripMenuItem_Click);
            // 
            // chargerFragmentToolStripMenuItem
            // 
            this.chargerFragmentToolStripMenuItem.Name = "chargerFragmentToolStripMenuItem";
            this.chargerFragmentToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.chargerFragmentToolStripMenuItem.Text = "Charger fragment";
            this.chargerFragmentToolStripMenuItem.Click += new System.EventHandler(this.chargerFragmentToolStripMenuItem_Click);
            // 
            // préférencesFragmentsToolStripMenuItem
            // 
            this.préférencesFragmentsToolStripMenuItem.Name = "préférencesFragmentsToolStripMenuItem";
            this.préférencesFragmentsToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.préférencesFragmentsToolStripMenuItem.Text = "Préférences fragments";
            this.préférencesFragmentsToolStripMenuItem.Click += new System.EventHandler(this.préférencesFragmentsToolStripMenuItem_Click);
            // 
            // enregistrerFragmentToolStripMenuItem
            // 
            this.enregistrerFragmentToolStripMenuItem.Name = "enregistrerFragmentToolStripMenuItem";
            this.enregistrerFragmentToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.enregistrerFragmentToolStripMenuItem.Text = "Enregistrer fragment";
            this.enregistrerFragmentToolStripMenuItem.Click += new System.EventHandler(this.enregistrerFragmentToolStripMenuItem_Click);
            // 
            // fermerFragmentToolStripMenuItem
            // 
            this.fermerFragmentToolStripMenuItem.Name = "fermerFragmentToolStripMenuItem";
            this.fermerFragmentToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.fermerFragmentToolStripMenuItem.Text = "Fermer fragment";
            this.fermerFragmentToolStripMenuItem.Click += new System.EventHandler(this.fermerFragmentToolStripMenuItem_Click);
            // 
            // gestionMondeToolStripMenuItem
            // 
            this.gestionMondeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nouveauMondeToolStripMenuItem,
            this.chargerMondeToolStripMenuItem,
            this.enregistrerMondeToolStripMenuItem,
            this.fermerMondeToolStripMenuItem});
            this.gestionMondeToolStripMenuItem.Name = "gestionMondeToolStripMenuItem";
            this.gestionMondeToolStripMenuItem.Size = new System.Drawing.Size(235, 26);
            this.gestionMondeToolStripMenuItem.Text = "Gestion monde";
            // 
            // nouveauMondeToolStripMenuItem
            // 
            this.nouveauMondeToolStripMenuItem.Name = "nouveauMondeToolStripMenuItem";
            this.nouveauMondeToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.nouveauMondeToolStripMenuItem.Text = "Nouveau monde";
            this.nouveauMondeToolStripMenuItem.Click += new System.EventHandler(this.nouveauMondeToolStripMenuItem_Click);
            // 
            // chargerMondeToolStripMenuItem
            // 
            this.chargerMondeToolStripMenuItem.Name = "chargerMondeToolStripMenuItem";
            this.chargerMondeToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.chargerMondeToolStripMenuItem.Text = "Charger monde";
            this.chargerMondeToolStripMenuItem.Click += new System.EventHandler(this.chargerMondeToolStripMenuItem_Click);
            // 
            // enregistrerMondeToolStripMenuItem
            // 
            this.enregistrerMondeToolStripMenuItem.Enabled = false;
            this.enregistrerMondeToolStripMenuItem.Name = "enregistrerMondeToolStripMenuItem";
            this.enregistrerMondeToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.enregistrerMondeToolStripMenuItem.Text = "Enregistrer monde";
            this.enregistrerMondeToolStripMenuItem.Click += new System.EventHandler(this.enregistrerMondeToolStripMenuItem_Click);
            // 
            // fermerMondeToolStripMenuItem
            // 
            this.fermerMondeToolStripMenuItem.Enabled = false;
            this.fermerMondeToolStripMenuItem.Name = "fermerMondeToolStripMenuItem";
            this.fermerMondeToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.fermerMondeToolStripMenuItem.Text = "Fermer monde";
            this.fermerMondeToolStripMenuItem.Click += new System.EventHandler(this.supprimerZone_Click);
            // 
            // quitterToolStripMenuItem
            // 
            this.quitterToolStripMenuItem.Name = "quitterToolStripMenuItem";
            this.quitterToolStripMenuItem.Size = new System.Drawing.Size(235, 26);
            this.quitterToolStripMenuItem.Text = "Quitter";
            this.quitterToolStripMenuItem.Click += new System.EventHandler(this.Quitter);
            // 
            // editionToolStripMenuItem
            // 
            this.editionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.preferencesToolStripMenuItem,
            this.annulerToolStripMenuItem,
            this.rétablirToolStripMenuItem});
            this.editionToolStripMenuItem.Name = "editionToolStripMenuItem";
            this.editionToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.editionToolStripMenuItem.Text = "Edition";
            // 
            // preferencesToolStripMenuItem
            // 
            this.preferencesToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("preferencesToolStripMenuItem.Image")));
            this.preferencesToolStripMenuItem.Name = "preferencesToolStripMenuItem";
            this.preferencesToolStripMenuItem.Size = new System.Drawing.Size(161, 26);
            this.preferencesToolStripMenuItem.Text = "Préférences";
            this.preferencesToolStripMenuItem.Click += new System.EventHandler(this.preferencesToolStripMenuItem_Click);
            // 
            // annulerToolStripMenuItem
            // 
            this.annulerToolStripMenuItem.Enabled = false;
            this.annulerToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("annulerToolStripMenuItem.Image")));
            this.annulerToolStripMenuItem.Name = "annulerToolStripMenuItem";
            this.annulerToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.annulerToolStripMenuItem.Size = new System.Drawing.Size(161, 26);
            this.annulerToolStripMenuItem.Text = "Annuler";
            this.annulerToolStripMenuItem.Click += new System.EventHandler(this.UndoButton_Click);
            // 
            // rétablirToolStripMenuItem
            // 
            this.rétablirToolStripMenuItem.Enabled = false;
            this.rétablirToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("rétablirToolStripMenuItem.Image")));
            this.rétablirToolStripMenuItem.Name = "rétablirToolStripMenuItem";
            this.rétablirToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y)));
            this.rétablirToolStripMenuItem.Size = new System.Drawing.Size(161, 26);
            this.rétablirToolStripMenuItem.Text = "Rétablir";
            this.rétablirToolStripMenuItem.Click += new System.EventHandler(this.RedoButton_Click);
            // 
            // sélectionToolStripMenuItem
            // 
            this.sélectionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.focusJoueurToolStripMenuItem,
            this.placerJoueurToolStripMenuItem});
            this.sélectionToolStripMenuItem.Name = "sélectionToolStripMenuItem";
            this.sélectionToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.sélectionToolStripMenuItem.Text = "Sélection";
            // 
            // focusJoueurToolStripMenuItem
            // 
            this.focusJoueurToolStripMenuItem.Name = "focusJoueurToolStripMenuItem";
            this.focusJoueurToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.focusJoueurToolStripMenuItem.Text = "Focus joueur";
            this.focusJoueurToolStripMenuItem.Click += new System.EventHandler(this.focusJoueurToolStripMenuItem_Click);
            // 
            // placerJoueurToolStripMenuItem
            // 
            this.placerJoueurToolStripMenuItem.Name = "placerJoueurToolStripMenuItem";
            this.placerJoueurToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.placerJoueurToolStripMenuItem.Text = "Placer joueur";
            this.placerJoueurToolStripMenuItem.Click += new System.EventHandler(this.placerJoueurToolStripMenuItem_Click);
            // 
            // mapToolStripMenuItem
            // 
            this.mapToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ajouterMapToolStripMenuItem,
            this.modifierMapToolStripMenuItem,
            this.supprimerMapToolStripMenuItem});
            this.mapToolStripMenuItem.Name = "mapToolStripMenuItem";
            this.mapToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.mapToolStripMenuItem.Text = "Map";
            // 
            // ajouterMapToolStripMenuItem
            // 
            this.ajouterMapToolStripMenuItem.Name = "ajouterMapToolStripMenuItem";
            this.ajouterMapToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.ajouterMapToolStripMenuItem.Text = "Ajouter map";
            this.ajouterMapToolStripMenuItem.Click += new System.EventHandler(this.ajouterMapToolStripMenuItem_Click);
            // 
            // modifierMapToolStripMenuItem
            // 
            this.modifierMapToolStripMenuItem.Name = "modifierMapToolStripMenuItem";
            this.modifierMapToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.modifierMapToolStripMenuItem.Text = "Modifier map";
            this.modifierMapToolStripMenuItem.Click += new System.EventHandler(this.modifierMapToolStripMenuItem_Click);
            // 
            // supprimerMapToolStripMenuItem
            // 
            this.supprimerMapToolStripMenuItem.Name = "supprimerMapToolStripMenuItem";
            this.supprimerMapToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.supprimerMapToolStripMenuItem.Text = "Supprimer map";
            this.supprimerMapToolStripMenuItem.Click += new System.EventHandler(this.supprimerMapToolStripMenuItem_Click);
            // 
            // calquesToolStripMenuItem
            // 
            this.calquesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ajouterUnCalqueToolStripMenuItem,
            this.supprimerUnCalqueToolStripMenuItem,
            this.importerUnCalqueDepuisToolStripMenuItem});
            this.calquesToolStripMenuItem.Name = "calquesToolStripMenuItem";
            this.calquesToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.calquesToolStripMenuItem.Text = "Calques";
            // 
            // ajouterUnCalqueToolStripMenuItem
            // 
            this.ajouterUnCalqueToolStripMenuItem.Name = "ajouterUnCalqueToolStripMenuItem";
            this.ajouterUnCalqueToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.ajouterUnCalqueToolStripMenuItem.Text = "Ajouter un calque";
            this.ajouterUnCalqueToolStripMenuItem.Click += new System.EventHandler(this.ajouterUnCalqueToolStripMenuItem_Click);
            // 
            // supprimerUnCalqueToolStripMenuItem
            // 
            this.supprimerUnCalqueToolStripMenuItem.Name = "supprimerUnCalqueToolStripMenuItem";
            this.supprimerUnCalqueToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.supprimerUnCalqueToolStripMenuItem.Text = "Supprimer un calque";
            this.supprimerUnCalqueToolStripMenuItem.Click += new System.EventHandler(this.supprimerUnCalqueToolStripMenuItem_Click);
            // 
            // importerUnCalqueDepuisToolStripMenuItem
            // 
            this.importerUnCalqueDepuisToolStripMenuItem.Name = "importerUnCalqueDepuisToolStripMenuItem";
            this.importerUnCalqueDepuisToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.importerUnCalqueDepuisToolStripMenuItem.Text = "Importer un calque...";
            this.importerUnCalqueDepuisToolStripMenuItem.Click += new System.EventHandler(this.importerUnCalqueDepuisToolStripMenuItem_Click);
            // 
            // animationsToolStripMenuItem
            // 
            this.animationsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.générerUneAnimationToolStripMenuItem,
            this.supprimerUneAnimationToolStripMenuItem,
            this.importerUnFondAniméToolStripMenuItem});
            this.animationsToolStripMenuItem.Name = "animationsToolStripMenuItem";
            this.animationsToolStripMenuItem.Size = new System.Drawing.Size(80, 20);
            this.animationsToolStripMenuItem.Text = "Animations";
            // 
            // générerUneAnimationToolStripMenuItem
            // 
            this.générerUneAnimationToolStripMenuItem.Name = "générerUneAnimationToolStripMenuItem";
            this.générerUneAnimationToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.générerUneAnimationToolStripMenuItem.Text = "Générer une animation";
            this.générerUneAnimationToolStripMenuItem.Click += new System.EventHandler(this.générerUneAnimationToolStripMenuItem_Click);
            // 
            // supprimerUneAnimationToolStripMenuItem
            // 
            this.supprimerUneAnimationToolStripMenuItem.Name = "supprimerUneAnimationToolStripMenuItem";
            this.supprimerUneAnimationToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.supprimerUneAnimationToolStripMenuItem.Text = "Supprimer une animation";
            this.supprimerUneAnimationToolStripMenuItem.Click += new System.EventHandler(this.supprimerUneAnimationToolStripMenuItem_Click);
            // 
            // importerUnFondAniméToolStripMenuItem
            // 
            this.importerUnFondAniméToolStripMenuItem.Name = "importerUnFondAniméToolStripMenuItem";
            this.importerUnFondAniméToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.importerUnFondAniméToolStripMenuItem.Text = "Importer un fond animé";
            this.importerUnFondAniméToolStripMenuItem.Click += new System.EventHandler(this.importerUnFondAniméToolStripMenuItem_Click);
            // 
            // outilsToolStripMenuItem
            // 
            this.outilsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.importerAssetStatiqueToolStripMenuItem,
            this.regénérerUnAssetToolStripMenuItem,
            this.consoleToolStripMenuItem,
            this.afficherFichierLogToolStripMenuItem,
            this.réinitialiserLogToolStripMenuItem,
            this.rechargerDLLJeuToolStripMenuItem,
            this.recréerToutLesObjetsScriptsToolStripMenuItem,
            this.rechargerScriptScèneToolStripMenuItem,
            this.fusionAvecAutreNiveauToolStripMenuItem,
            this.effacerHistoriqueNiveauxToolStripMenuItem,
            this.exportDansUnFichierStreamToolStripMenuItem});
            this.outilsToolStripMenuItem.Name = "outilsToolStripMenuItem";
            this.outilsToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.outilsToolStripMenuItem.Text = "Outils";
            // 
            // importerAssetStatiqueToolStripMenuItem
            // 
            this.importerAssetStatiqueToolStripMenuItem.Name = "importerAssetStatiqueToolStripMenuItem";
            this.importerAssetStatiqueToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.importerAssetStatiqueToolStripMenuItem.Text = "Importer asset statique...";
            this.importerAssetStatiqueToolStripMenuItem.Click += new System.EventHandler(this.importerAssetStatiqueToolStripMenuItem_Click);
            // 
            // regénérerUnAssetToolStripMenuItem
            // 
            this.regénérerUnAssetToolStripMenuItem.Name = "regénérerUnAssetToolStripMenuItem";
            this.regénérerUnAssetToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.regénérerUnAssetToolStripMenuItem.Text = "Regénérer un asset...";
            this.regénérerUnAssetToolStripMenuItem.Click += new System.EventHandler(this.regénérerUnAssetToolStripMenuItem_Click);
            // 
            // consoleToolStripMenuItem
            // 
            this.consoleToolStripMenuItem.Name = "consoleToolStripMenuItem";
            this.consoleToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.consoleToolStripMenuItem.Text = "Console";
            this.consoleToolStripMenuItem.Click += new System.EventHandler(this.consoleToolStripMenuItem_Click);
            // 
            // afficherFichierLogToolStripMenuItem
            // 
            this.afficherFichierLogToolStripMenuItem.Name = "afficherFichierLogToolStripMenuItem";
            this.afficherFichierLogToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.afficherFichierLogToolStripMenuItem.Text = "Afficher fichier log";
            this.afficherFichierLogToolStripMenuItem.Click += new System.EventHandler(this.afficherFichierLogToolStripMenuItem_Click);
            // 
            // réinitialiserLogToolStripMenuItem
            // 
            this.réinitialiserLogToolStripMenuItem.Name = "réinitialiserLogToolStripMenuItem";
            this.réinitialiserLogToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.réinitialiserLogToolStripMenuItem.Text = "Réinitialiser log";
            this.réinitialiserLogToolStripMenuItem.Click += new System.EventHandler(this.réinitialiserLogToolStripMenuItem_Click);
            // 
            // rechargerDLLJeuToolStripMenuItem
            // 
            this.rechargerDLLJeuToolStripMenuItem.Name = "rechargerDLLJeuToolStripMenuItem";
            this.rechargerDLLJeuToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.rechargerDLLJeuToolStripMenuItem.Text = "Recharger DLL jeu";
            this.rechargerDLLJeuToolStripMenuItem.Click += new System.EventHandler(this.rechargerDLLJeuToolStripMenuItem_Click);
            // 
            // recréerToutLesObjetsScriptsToolStripMenuItem
            // 
            this.recréerToutLesObjetsScriptsToolStripMenuItem.Name = "recréerToutLesObjetsScriptsToolStripMenuItem";
            this.recréerToutLesObjetsScriptsToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.recréerToutLesObjetsScriptsToolStripMenuItem.Text = "Recréer tout les objets scripts";
            this.recréerToutLesObjetsScriptsToolStripMenuItem.Click += new System.EventHandler(this.recréerToutLesObjetsScriptsToolStripMenuItem_Click);
            // 
            // rechargerScriptScèneToolStripMenuItem
            // 
            this.rechargerScriptScèneToolStripMenuItem.Name = "rechargerScriptScèneToolStripMenuItem";
            this.rechargerScriptScèneToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.rechargerScriptScèneToolStripMenuItem.Text = "Recharger script scène";
            this.rechargerScriptScèneToolStripMenuItem.Click += new System.EventHandler(this.rechargerScriptScèneToolStripMenuItem_Click);
            // 
            // fusionAvecAutreNiveauToolStripMenuItem
            // 
            this.fusionAvecAutreNiveauToolStripMenuItem.Name = "fusionAvecAutreNiveauToolStripMenuItem";
            this.fusionAvecAutreNiveauToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.fusionAvecAutreNiveauToolStripMenuItem.Text = "Fusion avec autre niveau...";
            this.fusionAvecAutreNiveauToolStripMenuItem.Click += new System.EventHandler(this.fusionAvecAutreNiveauToolStripMenuItem_Click);
            // 
            // effacerHistoriqueNiveauxToolStripMenuItem
            // 
            this.effacerHistoriqueNiveauxToolStripMenuItem.Name = "effacerHistoriqueNiveauxToolStripMenuItem";
            this.effacerHistoriqueNiveauxToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.effacerHistoriqueNiveauxToolStripMenuItem.Text = "Effacer historique niveaux";
            this.effacerHistoriqueNiveauxToolStripMenuItem.Click += new System.EventHandler(this.effacerHistoriqueNiveauxToolStripMenuItem_Click);
            // 
            // exportDansUnFichierStreamToolStripMenuItem
            // 
            this.exportDansUnFichierStreamToolStripMenuItem.Name = "exportDansUnFichierStreamToolStripMenuItem";
            this.exportDansUnFichierStreamToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.exportDansUnFichierStreamToolStripMenuItem.Text = "Export dans un fichier stream...";
            this.exportDansUnFichierStreamToolStripMenuItem.Click += new System.EventHandler(this.exportDansUnFichierStreamToolStripMenuItem_Click);
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.windowsExport,
            this.androidExport,
            this.webGLExport});
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.exportToolStripMenuItem.Text = "Export";
            // 
            // windowsExport
            // 
            this.windowsExport.Name = "windowsExport";
            this.windowsExport.Size = new System.Drawing.Size(152, 22);
            this.windowsExport.Text = "Windows...";
            this.windowsExport.Click += new System.EventHandler(this.windowsExport_Click);
            // 
            // androidExport
            // 
            this.androidExport.Name = "androidExport";
            this.androidExport.Size = new System.Drawing.Size(152, 22);
            this.androidExport.Text = "Android...";
            this.androidExport.Click += new System.EventHandler(this.androidExport_Click);
            // 
            // webGLExport
            // 
            this.webGLExport.Name = "webGLExport";
            this.webGLExport.Size = new System.Drawing.Size(152, 22);
            this.webGLExport.Text = "WebGL...";
            this.webGLExport.Click += new System.EventHandler(this.webGLExport_Click);
            // 
            // tabMaps
            // 
            this.tabMaps.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabMaps.Location = new System.Drawing.Point(13, 48);
            this.tabMaps.Name = "tabMaps";
            this.tabMaps.SelectedIndex = 0;
            this.tabMaps.Size = new System.Drawing.Size(1103, 18);
            this.tabMaps.TabIndex = 1;
            this.tabMaps.Visible = false;
            this.tabMaps.SelectedIndexChanged += new System.EventHandler(this.tabMaps_SelectedIndexChanged);
            // 
            // tilesetBox
            // 
            this.tilesetBox.InitialImage = null;
            this.tilesetBox.Location = new System.Drawing.Point(-2, 3);
            this.tilesetBox.Name = "tilesetBox";
            this.tilesetBox.Size = new System.Drawing.Size(412, 138);
            this.tilesetBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.tilesetBox.TabIndex = 2;
            this.tilesetBox.TabStop = false;
            this.tilesetBox.Visible = false;
            this.tilesetBox.Paint += new System.Windows.Forms.PaintEventHandler(this.tilesetBox_Paint);
            this.tilesetBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tilesetBox_MouseDown);
            // 
            // tabLayout
            // 
            this.tabLayout.Alignment = System.Windows.Forms.TabAlignment.Bottom;
            this.tabLayout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tabLayout.Controls.Add(this.tabCalques);
            this.tabLayout.Controls.Add(this.tablistitems);
            this.tabLayout.Location = new System.Drawing.Point(1126, 68);
            this.tabLayout.Multiline = true;
            this.tabLayout.Name = "tabLayout";
            this.tabLayout.SelectedIndex = 0;
            this.tabLayout.Size = new System.Drawing.Size(406, 394);
            this.tabLayout.TabIndex = 0;
            this.tabLayout.Visible = false;
            // 
            // tabCalques
            // 
            this.tabCalques.BackColor = System.Drawing.Color.White;
            this.tabCalques.Controls.Add(this.numCurrentFrame);
            this.tabCalques.Controls.Add(this.lblCurrentFrame);
            this.tabCalques.Controls.Add(this.trkCurrentFrame);
            this.tabCalques.Controls.Add(this.btnReorder);
            this.tabCalques.Controls.Add(this.btnOrderDown);
            this.tabCalques.Controls.Add(this.btnOrderUp);
            this.tabCalques.Controls.Add(this.calquesList);
            this.tabCalques.Controls.Add(this.lblAlpha);
            this.tabCalques.Controls.Add(this.trackAlpha);
            this.tabCalques.Location = new System.Drawing.Point(4, 4);
            this.tabCalques.Name = "tabCalques";
            this.tabCalques.Padding = new System.Windows.Forms.Padding(3);
            this.tabCalques.Size = new System.Drawing.Size(398, 368);
            this.tabCalques.TabIndex = 0;
            this.tabCalques.Text = "Calques";
            // 
            // numCurrentFrame
            // 
            this.numCurrentFrame.Location = new System.Drawing.Point(334, 329);
            this.numCurrentFrame.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numCurrentFrame.Name = "numCurrentFrame";
            this.numCurrentFrame.Size = new System.Drawing.Size(51, 20);
            this.numCurrentFrame.TabIndex = 12;
            this.numCurrentFrame.Visible = false;
            this.numCurrentFrame.ValueChanged += new System.EventHandler(this.numCurrentFrame_ValueChanged);
            // 
            // lblCurrentFrame
            // 
            this.lblCurrentFrame.AutoSize = true;
            this.lblCurrentFrame.Location = new System.Drawing.Point(4, 318);
            this.lblCurrentFrame.Name = "lblCurrentFrame";
            this.lblCurrentFrame.Size = new System.Drawing.Size(36, 13);
            this.lblCurrentFrame.TabIndex = 10;
            this.lblCurrentFrame.Text = "Frame";
            this.lblCurrentFrame.Visible = false;
            // 
            // trkCurrentFrame
            // 
            this.trkCurrentFrame.LargeChange = 2;
            this.trkCurrentFrame.Location = new System.Drawing.Point(49, 329);
            this.trkCurrentFrame.Name = "trkCurrentFrame";
            this.trkCurrentFrame.Size = new System.Drawing.Size(278, 45);
            this.trkCurrentFrame.TabIndex = 9;
            this.trkCurrentFrame.Visible = false;
            this.trkCurrentFrame.Scroll += new System.EventHandler(this.trkCurrentFrame_Scroll);
            this.trkCurrentFrame.ValueChanged += new System.EventHandler(this.trkCurrentFrame_ValueChanged);
            // 
            // btnReorder
            // 
            this.btnReorder.Location = new System.Drawing.Point(9, 244);
            this.btnReorder.Name = "btnReorder";
            this.btnReorder.Size = new System.Drawing.Size(34, 31);
            this.btnReorder.TabIndex = 8;
            this.btnReorder.Text = "RE";
            this.btnReorder.UseVisualStyleBackColor = true;
            this.btnReorder.Click += new System.EventHandler(this.btnReorder_Click);
            // 
            // btnOrderDown
            // 
            this.btnOrderDown.Location = new System.Drawing.Point(9, 206);
            this.btnOrderDown.Name = "btnOrderDown";
            this.btnOrderDown.Size = new System.Drawing.Size(34, 31);
            this.btnOrderDown.TabIndex = 7;
            this.btnOrderDown.Text = "▼";
            this.btnOrderDown.UseVisualStyleBackColor = true;
            this.btnOrderDown.Click += new System.EventHandler(this.btnOrderDown_Click);
            // 
            // btnOrderUp
            // 
            this.btnOrderUp.Location = new System.Drawing.Point(9, 170);
            this.btnOrderUp.Name = "btnOrderUp";
            this.btnOrderUp.Size = new System.Drawing.Size(34, 31);
            this.btnOrderUp.TabIndex = 6;
            this.btnOrderUp.Text = "▲";
            this.btnOrderUp.UseVisualStyleBackColor = true;
            this.btnOrderUp.Click += new System.EventHandler(this.btnOrderUp_Click);
            // 
            // calquesList
            // 
            this.calquesList.CheckBoxes = true;
            this.calquesList.DrawMode = System.Windows.Forms.TreeViewDrawMode.OwnerDrawAll;
            this.calquesList.FullRowSelect = true;
            this.calquesList.HideSelection = false;
            this.calquesList.Location = new System.Drawing.Point(49, 84);
            this.calquesList.Name = "calquesList";
            this.calquesList.ShowLines = false;
            this.calquesList.Size = new System.Drawing.Size(278, 235);
            this.calquesList.TabIndex = 5;
            this.calquesList.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.calquesList_AfterCheck);
            this.calquesList.DrawNode += new System.Windows.Forms.DrawTreeNodeEventHandler(this.calquesList_DrawNode);
            this.calquesList.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.calquesList_AfterSelect);
            this.calquesList.MouseClick += new System.Windows.Forms.MouseEventHandler(this.calquesList_MouseClick);
            // 
            // lblAlpha
            // 
            this.lblAlpha.AutoSize = true;
            this.lblAlpha.Location = new System.Drawing.Point(6, 6);
            this.lblAlpha.Name = "lblAlpha";
            this.lblAlpha.Size = new System.Drawing.Size(44, 13);
            this.lblAlpha.TabIndex = 4;
            this.lblAlpha.Text = "Opacité";
            this.lblAlpha.Visible = false;
            // 
            // trackAlpha
            // 
            this.trackAlpha.LargeChange = 2;
            this.trackAlpha.Location = new System.Drawing.Point(49, 24);
            this.trackAlpha.Name = "trackAlpha";
            this.trackAlpha.Size = new System.Drawing.Size(278, 45);
            this.trackAlpha.TabIndex = 3;
            this.trackAlpha.Visible = false;
            this.trackAlpha.ValueChanged += new System.EventHandler(this.trackAlpha_ValueChanged);
            // 
            // tablistitems
            // 
            this.tablistitems.Controls.Add(this.ItemsList);
            this.tablistitems.Location = new System.Drawing.Point(4, 4);
            this.tablistitems.Name = "tablistitems";
            this.tablistitems.Padding = new System.Windows.Forms.Padding(3);
            this.tablistitems.Size = new System.Drawing.Size(398, 368);
            this.tablistitems.TabIndex = 1;
            this.tablistitems.Text = "Liste éléments";
            this.tablistitems.UseVisualStyleBackColor = true;
            // 
            // ItemsList
            // 
            this.ItemsList.DrawMode = System.Windows.Forms.TreeViewDrawMode.OwnerDrawAll;
            this.ItemsList.FullRowSelect = true;
            this.ItemsList.HideSelection = false;
            this.ItemsList.Location = new System.Drawing.Point(61, 55);
            this.ItemsList.Name = "ItemsList";
            treeNode17.Name = "Nodeentity";
            treeNode17.Text = "Entités";
            treeNode18.Name = "Nodetrigger";
            treeNode18.Text = "déclencheurs";
            treeNode19.Name = "NodeText";
            treeNode19.Text = "Textes";
            treeNode20.Name = "NodeSound";
            treeNode20.Text = "Sources sonores";
            treeNode21.Name = "NodeLight";
            treeNode21.Text = "Source lumières";
            treeNode22.Name = "NodeParticles";
            treeNode22.Text = "Particules";
            treeNode23.Name = "NodeWaypoint";
            treeNode23.Text = "Waypoint";
            treeNode24.Name = "NodeCollider";
            treeNode24.Text = "Collision";
            this.ItemsList.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode17,
            treeNode18,
            treeNode19,
            treeNode20,
            treeNode21,
            treeNode22,
            treeNode23,
            treeNode24});
            this.ItemsList.Size = new System.Drawing.Size(278, 271);
            this.ItemsList.TabIndex = 6;
            this.ItemsList.DrawNode += new System.Windows.Forms.DrawTreeNodeEventHandler(this.calquesList_DrawNode);
            this.ItemsList.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.ItemsList_NodeMouseClick);
            // 
            // actionBar
            // 
            this.actionBar.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.actionBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonNewLevel,
            this.toolStripButtonOpenLevel,
            this.toolStripButtonSaveLevel,
            this.LayTileButton,
            this.FillTilesButton,
            this.UndoButton,
            this.RedoButton,
            this.toolStripButtonZoomplus,
            this.toolStripButtonZoomminus,
            this.toolStripButtonZoomreset});
            this.actionBar.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.actionBar.Location = new System.Drawing.Point(0, 24);
            this.actionBar.Name = "actionBar";
            this.actionBar.Size = new System.Drawing.Size(1525, 27);
            this.actionBar.TabIndex = 3;
            this.actionBar.Text = "toolStrip1";
            // 
            // toolStripButtonNewLevel
            // 
            this.toolStripButtonNewLevel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonNewLevel.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonNewLevel.Image")));
            this.toolStripButtonNewLevel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonNewLevel.Name = "toolStripButtonNewLevel";
            this.toolStripButtonNewLevel.Size = new System.Drawing.Size(24, 24);
            this.toolStripButtonNewLevel.Text = "Nouveau niveau";
            this.toolStripButtonNewLevel.Click += new System.EventHandler(this.toolStripButtonNewLevel_Click);
            // 
            // toolStripButtonOpenLevel
            // 
            this.toolStripButtonOpenLevel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonOpenLevel.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonOpenLevel.Image")));
            this.toolStripButtonOpenLevel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonOpenLevel.Name = "toolStripButtonOpenLevel";
            this.toolStripButtonOpenLevel.Size = new System.Drawing.Size(24, 24);
            this.toolStripButtonOpenLevel.Text = "Ouvrir niveau";
            this.toolStripButtonOpenLevel.Click += new System.EventHandler(this.toolStripButtonOpenLevel_Click);
            // 
            // toolStripButtonSaveLevel
            // 
            this.toolStripButtonSaveLevel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonSaveLevel.Enabled = false;
            this.toolStripButtonSaveLevel.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonSaveLevel.Image")));
            this.toolStripButtonSaveLevel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonSaveLevel.Name = "toolStripButtonSaveLevel";
            this.toolStripButtonSaveLevel.Size = new System.Drawing.Size(24, 24);
            this.toolStripButtonSaveLevel.Text = "Sauvegarder niveau";
            this.toolStripButtonSaveLevel.Click += new System.EventHandler(this.toolStripButtonSaveLevel_Click);
            // 
            // LayTileButton
            // 
            this.LayTileButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.LayTileButton.Image = ((System.Drawing.Image)(resources.GetObject("LayTileButton.Image")));
            this.LayTileButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.LayTileButton.Name = "LayTileButton";
            this.LayTileButton.Size = new System.Drawing.Size(24, 24);
            this.LayTileButton.Text = "Poser une tuile";
            this.LayTileButton.Click += new System.EventHandler(this.LayTileButton_Click);
            // 
            // FillTilesButton
            // 
            this.FillTilesButton.CheckOnClick = true;
            this.FillTilesButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.FillTilesButton.Image = ((System.Drawing.Image)(resources.GetObject("FillTilesButton.Image")));
            this.FillTilesButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.FillTilesButton.Name = "FillTilesButton";
            this.FillTilesButton.Size = new System.Drawing.Size(24, 24);
            this.FillTilesButton.Text = "Remplir tuiles";
            this.FillTilesButton.Click += new System.EventHandler(this.FillTilesButton_Click);
            // 
            // UndoButton
            // 
            this.UndoButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.UndoButton.Enabled = false;
            this.UndoButton.Image = ((System.Drawing.Image)(resources.GetObject("UndoButton.Image")));
            this.UndoButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.UndoButton.Name = "UndoButton";
            this.UndoButton.Size = new System.Drawing.Size(24, 24);
            this.UndoButton.Text = "Annuler action";
            this.UndoButton.Click += new System.EventHandler(this.UndoButton_Click);
            // 
            // RedoButton
            // 
            this.RedoButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.RedoButton.Enabled = false;
            this.RedoButton.Image = ((System.Drawing.Image)(resources.GetObject("RedoButton.Image")));
            this.RedoButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.RedoButton.Name = "RedoButton";
            this.RedoButton.Size = new System.Drawing.Size(24, 24);
            this.RedoButton.Text = "Refaire action";
            this.RedoButton.Click += new System.EventHandler(this.RedoButton_Click);
            // 
            // toolStripButtonZoomplus
            // 
            this.toolStripButtonZoomplus.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonZoomplus.Enabled = false;
            this.toolStripButtonZoomplus.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonZoomplus.Image")));
            this.toolStripButtonZoomplus.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonZoomplus.Name = "toolStripButtonZoomplus";
            this.toolStripButtonZoomplus.Size = new System.Drawing.Size(24, 24);
            this.toolStripButtonZoomplus.Text = "Zoomer";
            this.toolStripButtonZoomplus.Click += new System.EventHandler(this.toolStripButtonZoomplus_Click);
            // 
            // toolStripButtonZoomminus
            // 
            this.toolStripButtonZoomminus.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonZoomminus.Enabled = false;
            this.toolStripButtonZoomminus.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonZoomminus.Image")));
            this.toolStripButtonZoomminus.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonZoomminus.Name = "toolStripButtonZoomminus";
            this.toolStripButtonZoomminus.Size = new System.Drawing.Size(24, 24);
            this.toolStripButtonZoomminus.Text = "Dézoomer";
            this.toolStripButtonZoomminus.Click += new System.EventHandler(this.toolStripButtonZoomminus_Click);
            // 
            // toolStripButtonZoomreset
            // 
            this.toolStripButtonZoomreset.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonZoomreset.Enabled = false;
            this.toolStripButtonZoomreset.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonZoomreset.Image")));
            this.toolStripButtonZoomreset.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonZoomreset.Name = "toolStripButtonZoomreset";
            this.toolStripButtonZoomreset.Size = new System.Drawing.Size(24, 24);
            this.toolStripButtonZoomreset.Text = "Zoom par défaut";
            this.toolStripButtonZoomreset.Click += new System.EventHandler(this.toolStripButtonZoomreset_Click);
            // 
            // tilsetWatcher
            // 
            this.tilsetWatcher.EnableRaisingEvents = true;
            this.tilsetWatcher.NotifyFilter = System.IO.NotifyFilters.LastWrite;
            this.tilsetWatcher.SynchronizingObject = this;
            this.tilsetWatcher.Changed += new System.IO.FileSystemEventHandler(this.tilsetWatcher_Changed);
            // 
            // saveLevelDialog
            // 
            this.saveLevelDialog.Filter = "Level files|*.lwf";
            // 
            // openLevelDialog
            // 
            this.openLevelDialog.Filter = "Level files|*.lwf";
            // 
            // ctxMenuCalques
            // 
            this.ctxMenuCalques.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.ctxMenuCalques.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.modifierCalqueToolStripMenuItem,
            this.dupliquerCalqueToolStripMenuItem});
            this.ctxMenuCalques.Name = "ctxMenuCalques";
            this.ctxMenuCalques.Size = new System.Drawing.Size(167, 48);
            // 
            // modifierCalqueToolStripMenuItem
            // 
            this.modifierCalqueToolStripMenuItem.Name = "modifierCalqueToolStripMenuItem";
            this.modifierCalqueToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.modifierCalqueToolStripMenuItem.Text = "Modifier Calque";
            this.modifierCalqueToolStripMenuItem.Click += new System.EventHandler(this.modifierCalqueToolStripMenuItem_Click);
            // 
            // dupliquerCalqueToolStripMenuItem
            // 
            this.dupliquerCalqueToolStripMenuItem.Name = "dupliquerCalqueToolStripMenuItem";
            this.dupliquerCalqueToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.dupliquerCalqueToolStripMenuItem.Text = "Dupliquer Calque";
            this.dupliquerCalqueToolStripMenuItem.Click += new System.EventHandler(this.dupliquerCalqueToolStripMenuItem_Click);
            // 
            // grpCollisions
            // 
            this.grpCollisions.Controls.Add(this.btnKill);
            this.grpCollisions.Controls.Add(this.btncoleditable_3);
            this.grpCollisions.Controls.Add(this.btncoleditable_2);
            this.grpCollisions.Controls.Add(this.btncoleditable);
            this.grpCollisions.Controls.Add(this.btnSlope2);
            this.grpCollisions.Controls.Add(this.btnSlope);
            this.grpCollisions.Controls.Add(this.btnColDestruct);
            this.grpCollisions.Controls.Add(this.btnCol);
            this.grpCollisions.Controls.Add(this.btnNoCol);
            this.grpCollisions.Location = new System.Drawing.Point(25, 14);
            this.grpCollisions.Name = "grpCollisions";
            this.grpCollisions.Size = new System.Drawing.Size(338, 90);
            this.grpCollisions.TabIndex = 4;
            this.grpCollisions.TabStop = false;
            this.grpCollisions.Text = "Valeur de collision";
            this.grpCollisions.Visible = false;
            // 
            // btnKill
            // 
            this.btnKill.BackColor = System.Drawing.Color.Red;
            this.btnKill.ForeColor = System.Drawing.Color.Black;
            this.btnKill.Location = new System.Drawing.Point(64, 56);
            this.btnKill.Name = "btnKill";
            this.btnKill.Size = new System.Drawing.Size(34, 31);
            this.btnKill.TabIndex = 8;
            this.btnKill.Text = "10";
            this.btnKill.UseVisualStyleBackColor = false;
            this.btnKill.Click += new System.EventHandler(this.btnKill_Click);
            // 
            // btncoleditable_3
            // 
            this.btncoleditable_3.BackColor = System.Drawing.Color.DarkGray;
            this.btncoleditable_3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btncoleditable_3.BackgroundImage")));
            this.btncoleditable_3.ForeColor = System.Drawing.Color.Black;
            this.btncoleditable_3.Location = new System.Drawing.Point(23, 56);
            this.btncoleditable_3.Name = "btncoleditable_3";
            this.btncoleditable_3.Size = new System.Drawing.Size(34, 31);
            this.btncoleditable_3.TabIndex = 7;
            this.btncoleditable_3.Text = "9";
            this.btncoleditable_3.UseVisualStyleBackColor = false;
            this.btncoleditable_3.Click += new System.EventHandler(this.btncoleditable_3_Click);
            // 
            // btncoleditable_2
            // 
            this.btncoleditable_2.BackColor = System.Drawing.Color.DarkGray;
            this.btncoleditable_2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btncoleditable_2.BackgroundImage")));
            this.btncoleditable_2.ForeColor = System.Drawing.Color.Black;
            this.btncoleditable_2.Location = new System.Drawing.Point(266, 18);
            this.btncoleditable_2.Name = "btncoleditable_2";
            this.btncoleditable_2.Size = new System.Drawing.Size(34, 31);
            this.btncoleditable_2.TabIndex = 6;
            this.btncoleditable_2.Text = "8";
            this.btncoleditable_2.UseVisualStyleBackColor = false;
            this.btncoleditable_2.Click += new System.EventHandler(this.btncoleditable_2_Click);
            // 
            // btncoleditable
            // 
            this.btncoleditable.BackColor = System.Drawing.Color.DarkGray;
            this.btncoleditable.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btncoleditable.BackgroundImage")));
            this.btncoleditable.ForeColor = System.Drawing.Color.Black;
            this.btncoleditable.Location = new System.Drawing.Point(226, 18);
            this.btncoleditable.Name = "btncoleditable";
            this.btncoleditable.Size = new System.Drawing.Size(34, 31);
            this.btncoleditable.TabIndex = 5;
            this.btncoleditable.Text = "7";
            this.btncoleditable.UseVisualStyleBackColor = false;
            this.btncoleditable.Click += new System.EventHandler(this.btncoleditable_Click);
            // 
            // btnSlope2
            // 
            this.btnSlope2.BackColor = System.Drawing.Color.DarkGray;
            this.btnSlope2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSlope2.BackgroundImage")));
            this.btnSlope2.ForeColor = System.Drawing.Color.Black;
            this.btnSlope2.Location = new System.Drawing.Point(186, 18);
            this.btnSlope2.Name = "btnSlope2";
            this.btnSlope2.Size = new System.Drawing.Size(34, 31);
            this.btnSlope2.TabIndex = 4;
            this.btnSlope2.Text = "6";
            this.btnSlope2.UseVisualStyleBackColor = false;
            this.btnSlope2.Click += new System.EventHandler(this.btnSlope2_Click);
            // 
            // btnSlope
            // 
            this.btnSlope.BackColor = System.Drawing.Color.DarkGray;
            this.btnSlope.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSlope.BackgroundImage")));
            this.btnSlope.ForeColor = System.Drawing.Color.Black;
            this.btnSlope.Location = new System.Drawing.Point(145, 18);
            this.btnSlope.Name = "btnSlope";
            this.btnSlope.Size = new System.Drawing.Size(34, 31);
            this.btnSlope.TabIndex = 3;
            this.btnSlope.Text = "5";
            this.btnSlope.UseVisualStyleBackColor = false;
            this.btnSlope.Click += new System.EventHandler(this.btnSlope_Click);
            // 
            // btnColDestruct
            // 
            this.btnColDestruct.BackColor = System.Drawing.Color.YellowGreen;
            this.btnColDestruct.ForeColor = System.Drawing.Color.White;
            this.btnColDestruct.Location = new System.Drawing.Point(105, 18);
            this.btnColDestruct.Name = "btnColDestruct";
            this.btnColDestruct.Size = new System.Drawing.Size(34, 31);
            this.btnColDestruct.TabIndex = 2;
            this.btnColDestruct.Text = "2";
            this.btnColDestruct.UseVisualStyleBackColor = false;
            this.btnColDestruct.Click += new System.EventHandler(this.btnColDestruct_Click);
            // 
            // btnCol
            // 
            this.btnCol.BackColor = System.Drawing.Color.Black;
            this.btnCol.ForeColor = System.Drawing.Color.White;
            this.btnCol.Location = new System.Drawing.Point(64, 18);
            this.btnCol.Name = "btnCol";
            this.btnCol.Size = new System.Drawing.Size(34, 31);
            this.btnCol.TabIndex = 1;
            this.btnCol.Text = "1";
            this.btnCol.UseVisualStyleBackColor = false;
            this.btnCol.Click += new System.EventHandler(this.btnCol_Click);
            // 
            // btnNoCol
            // 
            this.btnNoCol.BackColor = System.Drawing.Color.White;
            this.btnNoCol.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnNoCol.Location = new System.Drawing.Point(23, 18);
            this.btnNoCol.Name = "btnNoCol";
            this.btnNoCol.Size = new System.Drawing.Size(34, 31);
            this.btnNoCol.TabIndex = 0;
            this.btnNoCol.Text = "0";
            this.btnNoCol.UseVisualStyleBackColor = false;
            this.btnNoCol.Click += new System.EventHandler(this.btnNoCol_Click);
            // 
            // grpObjects
            // 
            this.grpObjects.Controls.Add(this.btnCollider);
            this.grpObjects.Controls.Add(this.btnWaypoint);
            this.grpObjects.Controls.Add(this.btnParticles);
            this.grpObjects.Controls.Add(this.btnLight);
            this.grpObjects.Controls.Add(this.btnSon);
            this.grpObjects.Controls.Add(this.btnTexte);
            this.grpObjects.Controls.Add(this.btnEntities);
            this.grpObjects.Controls.Add(this.btnTrigger);
            this.grpObjects.Location = new System.Drawing.Point(14, 14);
            this.grpObjects.Name = "grpObjects";
            this.grpObjects.Size = new System.Drawing.Size(330, 112);
            this.grpObjects.TabIndex = 2;
            this.grpObjects.TabStop = false;
            this.grpObjects.Text = "Objets";
            this.grpObjects.Visible = false;
            // 
            // btnCollider
            // 
            this.btnCollider.Image = ((System.Drawing.Image)(resources.GetObject("btnCollider.Image")));
            this.btnCollider.Location = new System.Drawing.Point(65, 70);
            this.btnCollider.Name = "btnCollider";
            this.btnCollider.Size = new System.Drawing.Size(44, 40);
            this.btnCollider.TabIndex = 8;
            this.btnCollider.UseVisualStyleBackColor = true;
            this.btnCollider.Click += new System.EventHandler(this.btnCollider_Click);
            // 
            // btnWaypoint
            // 
            this.btnWaypoint.Image = ((System.Drawing.Image)(resources.GetObject("btnWaypoint.Image")));
            this.btnWaypoint.Location = new System.Drawing.Point(13, 70);
            this.btnWaypoint.Name = "btnWaypoint";
            this.btnWaypoint.Size = new System.Drawing.Size(44, 40);
            this.btnWaypoint.TabIndex = 7;
            this.btnWaypoint.UseVisualStyleBackColor = true;
            this.btnWaypoint.Click += new System.EventHandler(this.btnWaypoint_Click);
            // 
            // btnParticles
            // 
            this.btnParticles.Image = ((System.Drawing.Image)(resources.GetObject("btnParticles.Image")));
            this.btnParticles.Location = new System.Drawing.Point(270, 25);
            this.btnParticles.Name = "btnParticles";
            this.btnParticles.Size = new System.Drawing.Size(44, 40);
            this.btnParticles.TabIndex = 6;
            this.btnParticles.UseVisualStyleBackColor = true;
            this.btnParticles.Click += new System.EventHandler(this.btnParticles_Click);
            // 
            // btnLight
            // 
            this.btnLight.Image = ((System.Drawing.Image)(resources.GetObject("btnLight.Image")));
            this.btnLight.Location = new System.Drawing.Point(217, 25);
            this.btnLight.Name = "btnLight";
            this.btnLight.Size = new System.Drawing.Size(44, 40);
            this.btnLight.TabIndex = 5;
            this.btnLight.UseVisualStyleBackColor = true;
            this.btnLight.Click += new System.EventHandler(this.btnLight_Click);
            // 
            // btnSon
            // 
            this.btnSon.Image = ((System.Drawing.Image)(resources.GetObject("btnSon.Image")));
            this.btnSon.Location = new System.Drawing.Point(164, 25);
            this.btnSon.Name = "btnSon";
            this.btnSon.Size = new System.Drawing.Size(44, 40);
            this.btnSon.TabIndex = 4;
            this.btnSon.UseVisualStyleBackColor = true;
            this.btnSon.Click += new System.EventHandler(this.btnSon_Click);
            // 
            // btnTexte
            // 
            this.btnTexte.Image = ((System.Drawing.Image)(resources.GetObject("btnTexte.Image")));
            this.btnTexte.Location = new System.Drawing.Point(114, 25);
            this.btnTexte.Name = "btnTexte";
            this.btnTexte.Size = new System.Drawing.Size(44, 40);
            this.btnTexte.TabIndex = 3;
            this.btnTexte.UseVisualStyleBackColor = true;
            this.btnTexte.Click += new System.EventHandler(this.btnTexte_Click);
            // 
            // btnEntities
            // 
            this.btnEntities.Image = ((System.Drawing.Image)(resources.GetObject("btnEntities.Image")));
            this.btnEntities.Location = new System.Drawing.Point(13, 24);
            this.btnEntities.Name = "btnEntities";
            this.btnEntities.Size = new System.Drawing.Size(44, 40);
            this.btnEntities.TabIndex = 2;
            this.btnEntities.UseVisualStyleBackColor = true;
            this.btnEntities.Click += new System.EventHandler(this.btnEntities_Click);
            // 
            // btnTrigger
            // 
            this.btnTrigger.Image = ((System.Drawing.Image)(resources.GetObject("btnTrigger.Image")));
            this.btnTrigger.Location = new System.Drawing.Point(64, 25);
            this.btnTrigger.Name = "btnTrigger";
            this.btnTrigger.Size = new System.Drawing.Size(44, 40);
            this.btnTrigger.TabIndex = 1;
            this.btnTrigger.UseVisualStyleBackColor = true;
            this.btnTrigger.Click += new System.EventHandler(this.btnTrigger_Click);
            // 
            // btnChangeMode
            // 
            this.btnChangeMode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnChangeMode.AutoSize = true;
            this.btnChangeMode.Location = new System.Drawing.Point(297, 9);
            this.btnChangeMode.Name = "btnChangeMode";
            this.btnChangeMode.Size = new System.Drawing.Size(86, 26);
            this.btnChangeMode.TabIndex = 5;
            this.btnChangeMode.Text = "Mode Jeu";
            this.btnChangeMode.UseVisualStyleBackColor = true;
            this.btnChangeMode.Click += new System.EventHandler(this.btnChangeMode_Click);
            // 
            // chkcollisions
            // 
            this.chkcollisions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkcollisions.AutoSize = true;
            this.chkcollisions.Checked = true;
            this.chkcollisions.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkcollisions.Location = new System.Drawing.Point(702, 11);
            this.chkcollisions.Name = "chkcollisions";
            this.chkcollisions.Size = new System.Drawing.Size(88, 17);
            this.chkcollisions.TabIndex = 10;
            this.chkcollisions.Text = "voir collisions";
            this.chkcollisions.UseVisualStyleBackColor = true;
            this.chkcollisions.CheckedChanged += new System.EventHandler(this.chkcollisions_CheckedChanged);
            // 
            // lbl_zoom
            // 
            this.lbl_zoom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbl_zoom.AutoSize = true;
            this.lbl_zoom.Location = new System.Drawing.Point(82, 12);
            this.lbl_zoom.Name = "lbl_zoom";
            this.lbl_zoom.Size = new System.Drawing.Size(40, 13);
            this.lbl_zoom.TabIndex = 11;
            this.lbl_zoom.Text = "Zoom :";
            // 
            // cmbZoom
            // 
            this.cmbZoom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmbZoom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbZoom.FormattingEnabled = true;
            this.cmbZoom.Items.AddRange(new object[] {
            "0,25",
            "0,5",
            "1,0",
            "2,0",
            "4,0",
            "8,0"});
            this.cmbZoom.Location = new System.Drawing.Point(142, 9);
            this.cmbZoom.Name = "cmbZoom";
            this.cmbZoom.Size = new System.Drawing.Size(87, 21);
            this.cmbZoom.TabIndex = 12;
            this.cmbZoom.SelectedIndexChanged += new System.EventHandler(this.cmbZoom_SelectedIndexChanged);
            // 
            // panelactions
            // 
            this.panelactions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panelactions.Controls.Add(this.chkDrawallitems);
            this.panelactions.Controls.Add(this.lblselectedentity);
            this.panelactions.Controls.Add(this.txtselectedentity);
            this.panelactions.Controls.Add(this.lblRowPos);
            this.panelactions.Controls.Add(this.chksnapgrid);
            this.panelactions.Controls.Add(this.lblCursorPos);
            this.panelactions.Controls.Add(this.chkcollisions);
            this.panelactions.Controls.Add(this.btnChangeMode);
            this.panelactions.Controls.Add(this.cmbZoom);
            this.panelactions.Controls.Add(this.lbl_zoom);
            this.panelactions.Location = new System.Drawing.Point(14, 805);
            this.panelactions.Name = "panelactions";
            this.panelactions.Size = new System.Drawing.Size(1100, 42);
            this.panelactions.TabIndex = 13;
            this.panelactions.Visible = false;
            // 
            // chkDrawallitems
            // 
            this.chkDrawallitems.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkDrawallitems.AutoSize = true;
            this.chkDrawallitems.Checked = true;
            this.chkDrawallitems.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkDrawallitems.Location = new System.Drawing.Point(796, 11);
            this.chkDrawallitems.Name = "chkDrawallitems";
            this.chkDrawallitems.Size = new System.Drawing.Size(198, 17);
            this.chkDrawallitems.TabIndex = 21;
            this.chkDrawallitems.Text = "Voir tout les éléments ( icones / etc.)";
            this.chkDrawallitems.UseVisualStyleBackColor = true;
            this.chkDrawallitems.CheckedChanged += new System.EventHandler(this.chkDrawallitems_CheckedChanged);
            // 
            // lblselectedentity
            // 
            this.lblselectedentity.AutoSize = true;
            this.lblselectedentity.Location = new System.Drawing.Point(415, 13);
            this.lblselectedentity.Name = "lblselectedentity";
            this.lblselectedentity.Size = new System.Drawing.Size(106, 13);
            this.lblselectedentity.TabIndex = 20;
            this.lblselectedentity.Text = "Entité sélectionnée : ";
            // 
            // txtselectedentity
            // 
            this.txtselectedentity.Location = new System.Drawing.Point(528, 10);
            this.txtselectedentity.Name = "txtselectedentity";
            this.txtselectedentity.ReadOnly = true;
            this.txtselectedentity.Size = new System.Drawing.Size(158, 20);
            this.txtselectedentity.TabIndex = 19;
            // 
            // lblRowPos
            // 
            this.lblRowPos.AutoSize = true;
            this.lblRowPos.Location = new System.Drawing.Point(3, 22);
            this.lblRowPos.Name = "lblRowPos";
            this.lblRowPos.Size = new System.Drawing.Size(57, 13);
            this.lblRowPos.TabIndex = 18;
            this.lblRowPos.Text = "lblRowPos";
            // 
            // chksnapgrid
            // 
            this.chksnapgrid.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chksnapgrid.AutoSize = true;
            this.chksnapgrid.Checked = true;
            this.chksnapgrid.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chksnapgrid.Location = new System.Drawing.Point(994, 11);
            this.chksnapgrid.Name = "chksnapgrid";
            this.chksnapgrid.Size = new System.Drawing.Size(110, 17);
            this.chksnapgrid.TabIndex = 17;
            this.chksnapgrid.Text = "Aligner sur la grille";
            this.chksnapgrid.UseVisualStyleBackColor = true;
            this.chksnapgrid.CheckedChanged += new System.EventHandler(this.chksnapgrid_CheckedChanged);
            // 
            // lblCursorPos
            // 
            this.lblCursorPos.AutoSize = true;
            this.lblCursorPos.Location = new System.Drawing.Point(3, 9);
            this.lblCursorPos.Name = "lblCursorPos";
            this.lblCursorPos.Size = new System.Drawing.Size(65, 13);
            this.lblCursorPos.TabIndex = 16;
            this.lblCursorPos.Text = "lblCursorPos";
            // 
            // scriptWatcher
            // 
            this.scriptWatcher.EnableRaisingEvents = true;
            this.scriptWatcher.Filter = "*.lua";
            this.scriptWatcher.NotifyFilter = System.IO.NotifyFilters.LastWrite;
            this.scriptWatcher.SynchronizingObject = this;
            this.scriptWatcher.Changed += new System.IO.FileSystemEventHandler(this.scriptWatcher_Changed);
            // 
            // ctxMenuTrigger
            // 
            this.ctxMenuTrigger.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.ctxMenuTrigger.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.redimensionnerTriggerToolStripMenuItem,
            this.repositionnerTriggerToolStripMenuItem,
            this.modifierTriggerToolStripMenuItem,
            this.supprimerTriggerToolStripMenuItem});
            this.ctxMenuTrigger.Name = "ctxMenuTrigger";
            this.ctxMenuTrigger.Size = new System.Drawing.Size(161, 92);
            // 
            // redimensionnerTriggerToolStripMenuItem
            // 
            this.redimensionnerTriggerToolStripMenuItem.Name = "redimensionnerTriggerToolStripMenuItem";
            this.redimensionnerTriggerToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.redimensionnerTriggerToolStripMenuItem.Text = "Redimensionner";
            this.redimensionnerTriggerToolStripMenuItem.Click += new System.EventHandler(this.redimensionnerTriggerToolStripMenuItem_Click);
            // 
            // repositionnerTriggerToolStripMenuItem
            // 
            this.repositionnerTriggerToolStripMenuItem.Name = "repositionnerTriggerToolStripMenuItem";
            this.repositionnerTriggerToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.repositionnerTriggerToolStripMenuItem.Text = "Repositionner";
            this.repositionnerTriggerToolStripMenuItem.Click += new System.EventHandler(this.repositionnerTriggerToolStripMenuItem_Click);
            // 
            // modifierTriggerToolStripMenuItem
            // 
            this.modifierTriggerToolStripMenuItem.Name = "modifierTriggerToolStripMenuItem";
            this.modifierTriggerToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.modifierTriggerToolStripMenuItem.Text = "Modifier";
            this.modifierTriggerToolStripMenuItem.Click += new System.EventHandler(this.modifierTriggerToolStripMenuItem_Click);
            // 
            // supprimerTriggerToolStripMenuItem
            // 
            this.supprimerTriggerToolStripMenuItem.Name = "supprimerTriggerToolStripMenuItem";
            this.supprimerTriggerToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.supprimerTriggerToolStripMenuItem.Text = "Supprimer";
            this.supprimerTriggerToolStripMenuItem.Click += new System.EventHandler(this.supprimerTriggerToolStripMenuItem_Click);
            // 
            // ctxMenuEntity
            // 
            this.ctxMenuEntity.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.ctxMenuEntity.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.repositionnerEntityToolStripMenuItem,
            this.modifierEntityToolStripMenuItem,
            this.supprimerEntityToolStripMenuItem,
            this.recréerObjetScriptToolStripMenuItem,
            this.focusEntitéToolStripMenuItem});
            this.ctxMenuEntity.Name = "ctxMenuEntity";
            this.ctxMenuEntity.Size = new System.Drawing.Size(176, 114);
            // 
            // repositionnerEntityToolStripMenuItem
            // 
            this.repositionnerEntityToolStripMenuItem.Name = "repositionnerEntityToolStripMenuItem";
            this.repositionnerEntityToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.repositionnerEntityToolStripMenuItem.Text = "Repositionner";
            this.repositionnerEntityToolStripMenuItem.Click += new System.EventHandler(this.repositionnerEntityToolStripMenuItem_Click);
            // 
            // modifierEntityToolStripMenuItem
            // 
            this.modifierEntityToolStripMenuItem.Name = "modifierEntityToolStripMenuItem";
            this.modifierEntityToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.modifierEntityToolStripMenuItem.Text = "Modifier";
            this.modifierEntityToolStripMenuItem.Click += new System.EventHandler(this.modifierEntityToolStripMenuItem_Click);
            // 
            // supprimerEntityToolStripMenuItem
            // 
            this.supprimerEntityToolStripMenuItem.Name = "supprimerEntityToolStripMenuItem";
            this.supprimerEntityToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.supprimerEntityToolStripMenuItem.Text = "Supprimer";
            this.supprimerEntityToolStripMenuItem.Click += new System.EventHandler(this.supprimerEntityToolStripMenuItem_Click);
            // 
            // recréerObjetScriptToolStripMenuItem
            // 
            this.recréerObjetScriptToolStripMenuItem.Name = "recréerObjetScriptToolStripMenuItem";
            this.recréerObjetScriptToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.recréerObjetScriptToolStripMenuItem.Text = "Recréer objet script";
            this.recréerObjetScriptToolStripMenuItem.Click += new System.EventHandler(this.recréerObjetScriptToolStripMenuItem_Click);
            // 
            // focusEntitéToolStripMenuItem
            // 
            this.focusEntitéToolStripMenuItem.Name = "focusEntitéToolStripMenuItem";
            this.focusEntitéToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.focusEntitéToolStripMenuItem.Text = "Focus entité";
            this.focusEntitéToolStripMenuItem.Click += new System.EventHandler(this.focusEntitéToolStripMenuItem_Click);
            // 
            // ctxTextObject
            // 
            this.ctxTextObject.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.ctxTextObject.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.txt_obj_repos,
            this.txt_obj_update,
            this.txt_obj_suppr,
            this.txt_obj_script});
            this.ctxTextObject.Name = "ctxMenuEntity";
            this.ctxTextObject.Size = new System.Drawing.Size(176, 92);
            // 
            // txt_obj_repos
            // 
            this.txt_obj_repos.Name = "txt_obj_repos";
            this.txt_obj_repos.Size = new System.Drawing.Size(175, 22);
            this.txt_obj_repos.Text = "Repositionner";
            this.txt_obj_repos.Click += new System.EventHandler(this.txt_obj_repos_Click);
            // 
            // txt_obj_update
            // 
            this.txt_obj_update.Name = "txt_obj_update";
            this.txt_obj_update.Size = new System.Drawing.Size(175, 22);
            this.txt_obj_update.Text = "Modifier";
            this.txt_obj_update.Click += new System.EventHandler(this.txt_obj_update_Click);
            // 
            // txt_obj_suppr
            // 
            this.txt_obj_suppr.Name = "txt_obj_suppr";
            this.txt_obj_suppr.Size = new System.Drawing.Size(175, 22);
            this.txt_obj_suppr.Text = "Supprimer";
            this.txt_obj_suppr.Click += new System.EventHandler(this.txt_obj_suppr_Click);
            // 
            // txt_obj_script
            // 
            this.txt_obj_script.Name = "txt_obj_script";
            this.txt_obj_script.Size = new System.Drawing.Size(175, 22);
            this.txt_obj_script.Text = "Recréer objet script";
            this.txt_obj_script.Click += new System.EventHandler(this.txt_obj_script_Click);
            // 
            // panelStretch
            // 
            this.panelStretch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelStretch.AutoScroll = true;
            this.panelStretch.Controls.Add(this.grpObjects);
            this.panelStretch.Controls.Add(this.grpCollisions);
            this.panelStretch.Controls.Add(this.tilesetBox);
            this.panelStretch.Location = new System.Drawing.Point(1126, 484);
            this.panelStretch.Name = "panelStretch";
            this.panelStretch.Size = new System.Drawing.Size(395, 236);
            this.panelStretch.TabIndex = 14;
            // 
            // resxWatcher
            // 
            this.resxWatcher.EnableRaisingEvents = true;
            this.resxWatcher.NotifyFilter = System.IO.NotifyFilters.LastWrite;
            this.resxWatcher.SynchronizingObject = this;
            this.resxWatcher.Changed += new System.IO.FileSystemEventHandler(this.resxWatcher_Changed);
            this.resxWatcher.Created += new System.IO.FileSystemEventHandler(this.resxWatcher_Changed);
            // 
            // localedbWatcher
            // 
            this.localedbWatcher.EnableRaisingEvents = true;
            this.localedbWatcher.Filter = "*.db";
            this.localedbWatcher.NotifyFilter = System.IO.NotifyFilters.LastWrite;
            this.localedbWatcher.SynchronizingObject = this;
            this.localedbWatcher.Changed += new System.IO.FileSystemEventHandler(this.localedbWatcher_Changed);
            // 
            // ctxMultiSelection
            // 
            this.ctxMultiSelection.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.ctxMultiSelection.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.déplacerÉlémentsToolStripMenuItem,
            this.grouperÉlémentsToolStripMenuItem,
            this.supprimerÉlémentsToolStripMenuItem});
            this.ctxMultiSelection.Name = "ctxMultiSelection";
            this.ctxMultiSelection.Size = new System.Drawing.Size(181, 70);
            // 
            // déplacerÉlémentsToolStripMenuItem
            // 
            this.déplacerÉlémentsToolStripMenuItem.Name = "déplacerÉlémentsToolStripMenuItem";
            this.déplacerÉlémentsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.déplacerÉlémentsToolStripMenuItem.Text = "Déplacer éléments";
            this.déplacerÉlémentsToolStripMenuItem.Click += new System.EventHandler(this.déplacerÉlémentsToolStripMenuItem_Click);
            // 
            // grouperÉlémentsToolStripMenuItem
            // 
            this.grouperÉlémentsToolStripMenuItem.Name = "grouperÉlémentsToolStripMenuItem";
            this.grouperÉlémentsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.grouperÉlémentsToolStripMenuItem.Text = "Grouper éléments";
            this.grouperÉlémentsToolStripMenuItem.Click += new System.EventHandler(this.grouperÉlémentsToolStripMenuItem_Click);
            // 
            // supprimerÉlémentsToolStripMenuItem
            // 
            this.supprimerÉlémentsToolStripMenuItem.Name = "supprimerÉlémentsToolStripMenuItem";
            this.supprimerÉlémentsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.supprimerÉlémentsToolStripMenuItem.Text = "Supprimer éléments";
            this.supprimerÉlémentsToolStripMenuItem.Click += new System.EventHandler(this.supprimerÉlémentsToolStripMenuItem_Click);
            // 
            // configWatcher
            // 
            this.configWatcher.EnableRaisingEvents = true;
            this.configWatcher.Filter = "*.lua";
            this.configWatcher.NotifyFilter = System.IO.NotifyFilters.LastWrite;
            this.configWatcher.SynchronizingObject = this;
            this.configWatcher.Changed += new System.IO.FileSystemEventHandler(this.configWatcher_Changed);
            // 
            // richlog
            // 
            this.richlog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.richlog.Location = new System.Drawing.Point(1122, 756);
            this.richlog.Name = "richlog";
            this.richlog.ReadOnly = true;
            this.richlog.Size = new System.Drawing.Size(399, 98);
            this.richlog.TabIndex = 6;
            this.richlog.Text = "";
            this.richlog.Visible = false;
            // 
            // ctxSoundSource
            // 
            this.ctxSoundSource.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.ctxSoundSource.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.snd_repos,
            this.snd_modif,
            this.snd_suppr});
            this.ctxSoundSource.Name = "ctxSoundSource";
            this.ctxSoundSource.Size = new System.Drawing.Size(148, 70);
            // 
            // snd_repos
            // 
            this.snd_repos.Name = "snd_repos";
            this.snd_repos.Size = new System.Drawing.Size(147, 22);
            this.snd_repos.Text = "Repositionner";
            this.snd_repos.Click += new System.EventHandler(this.snd_repos_Click);
            // 
            // snd_modif
            // 
            this.snd_modif.Name = "snd_modif";
            this.snd_modif.Size = new System.Drawing.Size(147, 22);
            this.snd_modif.Text = "Modifier";
            this.snd_modif.Click += new System.EventHandler(this.snd_modif_Click);
            // 
            // snd_suppr
            // 
            this.snd_suppr.Name = "snd_suppr";
            this.snd_suppr.Size = new System.Drawing.Size(147, 22);
            this.snd_suppr.Text = "Supprimer";
            this.snd_suppr.Click += new System.EventHandler(this.snd_suppr_Click);
            // 
            // ctxLightSource
            // 
            this.ctxLightSource.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.ctxLightSource.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.light_repos,
            this.light_modif,
            this.light_suppr});
            this.ctxLightSource.Name = "ctxSoundSource";
            this.ctxLightSource.Size = new System.Drawing.Size(148, 70);
            // 
            // light_repos
            // 
            this.light_repos.Name = "light_repos";
            this.light_repos.Size = new System.Drawing.Size(147, 22);
            this.light_repos.Text = "Repositionner";
            this.light_repos.Click += new System.EventHandler(this.light_repos_Click);
            // 
            // light_modif
            // 
            this.light_modif.Name = "light_modif";
            this.light_modif.Size = new System.Drawing.Size(147, 22);
            this.light_modif.Text = "Modifier";
            this.light_modif.Click += new System.EventHandler(this.light_modif_Click);
            // 
            // light_suppr
            // 
            this.light_suppr.Name = "light_suppr";
            this.light_suppr.Size = new System.Drawing.Size(147, 22);
            this.light_suppr.Text = "Supprimer";
            this.light_suppr.Click += new System.EventHandler(this.light_suppr_Click);
            // 
            // exportLocation
            // 
            this.exportLocation.Filter = "Game package|*.zip";
            // 
            // exportWebgl
            // 
            this.exportWebgl.RootFolder = System.Environment.SpecialFolder.MyComputer;
            // 
            // ctxParticles
            // 
            this.ctxParticles.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.ctxParticles.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.particles_repos,
            this.particles_modif,
            this.particles_suppr});
            this.ctxParticles.Name = "ctxSoundSource";
            this.ctxParticles.Size = new System.Drawing.Size(148, 70);
            // 
            // particles_repos
            // 
            this.particles_repos.Name = "particles_repos";
            this.particles_repos.Size = new System.Drawing.Size(147, 22);
            this.particles_repos.Text = "Repositionner";
            this.particles_repos.Click += new System.EventHandler(this.particles_repos_Click);
            // 
            // particles_modif
            // 
            this.particles_modif.Name = "particles_modif";
            this.particles_modif.Size = new System.Drawing.Size(147, 22);
            this.particles_modif.Text = "Modifier";
            this.particles_modif.Click += new System.EventHandler(this.particles_modif_Click);
            // 
            // particles_suppr
            // 
            this.particles_suppr.Name = "particles_suppr";
            this.particles_suppr.Size = new System.Drawing.Size(147, 22);
            this.particles_suppr.Text = "Supprimer";
            this.particles_suppr.Click += new System.EventHandler(this.particles_suppr_Click);
            // 
            // exportToStreamFile
            // 
            this.exportToStreamFile.Filter = "StreamFile|*.db";
            // 
            // openWorldDialog
            // 
            this.openWorldDialog.Filter = "World files|*.mwf";
            // 
            // saveWorldDialog
            // 
            this.saveWorldDialog.Filter = "World files|*.mwf";
            // 
            // tabWorldSlice
            // 
            this.tabWorldSlice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tabWorldSlice.Controls.Add(this.btnCheckIds);
            this.tabWorldSlice.Controls.Add(this.lblcurrentzone);
            this.tabWorldSlice.Controls.Add(this.lblselectzone);
            this.tabWorldSlice.Controls.Add(this.tvZone);
            this.tabWorldSlice.Controls.Add(this.btnselectzonevalue);
            this.tabWorldSlice.Controls.Add(this.numSelectzonevalue);
            this.tabWorldSlice.Controls.Add(this.lblselectzonevalue);
            this.tabWorldSlice.Location = new System.Drawing.Point(1132, 58);
            this.tabWorldSlice.Name = "tabWorldSlice";
            this.tabWorldSlice.Size = new System.Drawing.Size(386, 397);
            this.tabWorldSlice.TabIndex = 15;
            // 
            // lblcurrentzone
            // 
            this.lblcurrentzone.AutoSize = true;
            this.lblcurrentzone.Location = new System.Drawing.Point(122, 372);
            this.lblcurrentzone.Name = "lblcurrentzone";
            this.lblcurrentzone.Size = new System.Drawing.Size(55, 13);
            this.lblcurrentzone.TabIndex = 5;
            this.lblcurrentzone.Text = "<aucune>";
            // 
            // lblselectzone
            // 
            this.lblselectzone.AutoSize = true;
            this.lblselectzone.Location = new System.Drawing.Point(10, 370);
            this.lblselectzone.Name = "lblselectzone";
            this.lblselectzone.Size = new System.Drawing.Size(104, 13);
            this.lblselectzone.TabIndex = 4;
            this.lblselectzone.Text = "Zone sélectionnée : ";
            // 
            // tvZone
            // 
            this.tvZone.Location = new System.Drawing.Point(7, 14);
            this.tvZone.Name = "tvZone";
            this.tvZone.Size = new System.Drawing.Size(390, 310);
            this.tvZone.TabIndex = 0;
            this.tvZone.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.tvZone_MouseDoubleClick);
            this.tvZone.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tvZone_MouseDown);
            // 
            // btnselectzonevalue
            // 
            this.btnselectzonevalue.Location = new System.Drawing.Point(256, 344);
            this.btnselectzonevalue.Name = "btnselectzonevalue";
            this.btnselectzonevalue.Size = new System.Drawing.Size(103, 22);
            this.btnselectzonevalue.TabIndex = 3;
            this.btnselectzonevalue.Text = "changer valeur";
            this.btnselectzonevalue.UseVisualStyleBackColor = true;
            this.btnselectzonevalue.Click += new System.EventHandler(this.btnselectzonevalue_Click);
            // 
            // numSelectzonevalue
            // 
            this.numSelectzonevalue.Location = new System.Drawing.Point(160, 349);
            this.numSelectzonevalue.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numSelectzonevalue.Name = "numSelectzonevalue";
            this.numSelectzonevalue.Size = new System.Drawing.Size(81, 20);
            this.numSelectzonevalue.TabIndex = 2;
            // 
            // lblselectzonevalue
            // 
            this.lblselectzonevalue.AutoSize = true;
            this.lblselectzonevalue.Location = new System.Drawing.Point(10, 351);
            this.lblselectzonevalue.Name = "lblselectzonevalue";
            this.lblselectzonevalue.Size = new System.Drawing.Size(135, 13);
            this.lblselectzonevalue.TabIndex = 1;
            this.lblselectzonevalue.Text = "Valeur de zone du chunk : ";
            // 
            // ctxMenuZone
            // 
            this.ctxMenuZone.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.ctxMenuZone.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ajouterZone,
            this.ajouterZoneEnfant,
            this.modifierZone,
            this.supprimerZone});
            this.ctxMenuZone.Name = "ctxMenuZone";
            this.ctxMenuZone.Size = new System.Drawing.Size(202, 92);
            // 
            // ajouterZone
            // 
            this.ajouterZone.Name = "ajouterZone";
            this.ajouterZone.Size = new System.Drawing.Size(201, 22);
            this.ajouterZone.Text = "Ajouter une zone";
            this.ajouterZone.Click += new System.EventHandler(this.ajouterZone_Click);
            // 
            // ajouterZoneEnfant
            // 
            this.ajouterZoneEnfant.Name = "ajouterZoneEnfant";
            this.ajouterZoneEnfant.Size = new System.Drawing.Size(201, 22);
            this.ajouterZoneEnfant.Text = "Ajouter une zone enfant";
            this.ajouterZoneEnfant.Click += new System.EventHandler(this.ajouterZoneEnfant_Click);
            // 
            // modifierZone
            // 
            this.modifierZone.Name = "modifierZone";
            this.modifierZone.Size = new System.Drawing.Size(201, 22);
            this.modifierZone.Text = "Modifier la zone";
            this.modifierZone.Click += new System.EventHandler(this.modifierZone_Click);
            // 
            // supprimerZone
            // 
            this.supprimerZone.Name = "supprimerZone";
            this.supprimerZone.Size = new System.Drawing.Size(201, 22);
            this.supprimerZone.Text = "Supprimer la zone";
            this.supprimerZone.Click += new System.EventHandler(this.supprimerZone_Click);
            // 
            // openFragmentDialog
            // 
            this.openFragmentDialog.Filter = "Fragment files|*.fwf";
            // 
            // saveFragmentDialog
            // 
            this.saveFragmentDialog.Filter = "Fragment files|*.fwf";
            // 
            // renderPanel
            // 
            this.renderPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.renderPanel.ColCount = 0;
            this.renderPanel.CurrentCollisionVal = 1;
            this.renderPanel.CurrentLayer = null;
            this.renderPanel.CurrentMap = null;
            this.renderPanel.CurrentWorldColor = null;
            this.renderPanel.Location = new System.Drawing.Point(14, 72);
            this.renderPanel.Name = "renderPanel";
            this.renderPanel.RowCount = 0;
            this.renderPanel.Size = new System.Drawing.Size(1100, 726);
            this.renderPanel.TabIndex = 7;
            this.renderPanel.TileSelected = new int[] {
        0};
            this.renderPanel.ToolSelected = WafaEditorV2.Tools.Pencil;
            this.renderPanel.Visible = false;
            this.renderPanel.ZoomFactor = 0.5F;
            this.renderPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.renderPanel_Paint);
            this.renderPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.renderPanel_MouseDown);
            this.renderPanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.renderPanel_MouseMove);
            // 
            // ctxWaypoint
            // 
            this.ctxWaypoint.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.ctxWaypoint.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem2,
            this.toolStripMenuItem3});
            this.ctxWaypoint.Name = "ctxSoundSource";
            this.ctxWaypoint.Size = new System.Drawing.Size(148, 70);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(147, 22);
            this.toolStripMenuItem1.Text = "Repositionner";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.waypoint_repos_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(147, 22);
            this.toolStripMenuItem2.Text = "Modifier";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.waypoint_modif_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(147, 22);
            this.toolStripMenuItem3.Text = "Supprimer";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.waypoint_suppr_Click);
            // 
            // ctxCollider
            // 
            this.ctxCollider.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.ctxCollider.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.redimensionnerToolStripMenuItem,
            this.toolStripMenuItem4,
            this.toolStripMenuItem5,
            this.toolStripMenuItem6});
            this.ctxCollider.Name = "ctxSoundSource";
            this.ctxCollider.Size = new System.Drawing.Size(161, 92);
            // 
            // redimensionnerToolStripMenuItem
            // 
            this.redimensionnerToolStripMenuItem.Name = "redimensionnerToolStripMenuItem";
            this.redimensionnerToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.redimensionnerToolStripMenuItem.Text = "Redimensionner";
            this.redimensionnerToolStripMenuItem.Click += new System.EventHandler(this.redimensionnerColliderToolStripMenuItem_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(160, 22);
            this.toolStripMenuItem4.Text = "Repositionner";
            this.toolStripMenuItem4.Click += new System.EventHandler(this.repositionnerColliderToolStripMenuItem_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(160, 22);
            this.toolStripMenuItem5.Text = "Modifier";
            this.toolStripMenuItem5.Click += new System.EventHandler(this.collider_modif_Click);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(160, 22);
            this.toolStripMenuItem6.Text = "Supprimer";
            this.toolStripMenuItem6.Click += new System.EventHandler(this.collider_suppr_Click);
            // 
            // btnshowlogdetail
            // 
            this.btnshowlogdetail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnshowlogdetail.Location = new System.Drawing.Point(1393, 727);
            this.btnshowlogdetail.Name = "btnshowlogdetail";
            this.btnshowlogdetail.Size = new System.Drawing.Size(122, 23);
            this.btnshowlogdetail.TabIndex = 16;
            this.btnshowlogdetail.Text = "Afficher détail des logs";
            this.btnshowlogdetail.UseVisualStyleBackColor = true;
            this.btnshowlogdetail.Click += new System.EventHandler(this.btnshowlogdetail_Click);
            // 
            // btnCheckIds
            // 
            this.btnCheckIds.Location = new System.Drawing.Point(256, 371);
            this.btnCheckIds.Name = "btnCheckIds";
            this.btnCheckIds.Size = new System.Drawing.Size(103, 22);
            this.btnCheckIds.TabIndex = 6;
            this.btnCheckIds.Text = "Vérifier doublon...";
            this.btnCheckIds.UseVisualStyleBackColor = true;
            this.btnCheckIds.Click += new System.EventHandler(this.btnCheckIds_Click);
            // 
            // Workbench
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(1525, 858);
            this.Controls.Add(this.btnshowlogdetail);
            this.Controls.Add(this.tabWorldSlice);
            this.Controls.Add(this.panelStretch);
            this.Controls.Add(this.panelactions);
            this.Controls.Add(this.renderPanel);
            this.Controls.Add(this.actionBar);
            this.Controls.Add(this.tabLayout);
            this.Controls.Add(this.tabMaps);
            this.Controls.Add(this.mainMenu);
            this.Controls.Add(this.richlog);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MainMenuStrip = this.mainMenu;
            this.Name = "Workbench";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Wafa Editor";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Workbench_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Workbench_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Workbench_KeyPress);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Workbench_KeyUp);
            this.mainMenu.ResumeLayout(false);
            this.mainMenu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tilesetBox)).EndInit();
            this.tabLayout.ResumeLayout(false);
            this.tabCalques.ResumeLayout(false);
            this.tabCalques.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numCurrentFrame)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trkCurrentFrame)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackAlpha)).EndInit();
            this.tablistitems.ResumeLayout(false);
            this.actionBar.ResumeLayout(false);
            this.actionBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tilsetWatcher)).EndInit();
            this.ctxMenuCalques.ResumeLayout(false);
            this.grpCollisions.ResumeLayout(false);
            this.grpObjects.ResumeLayout(false);
            this.panelactions.ResumeLayout(false);
            this.panelactions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scriptWatcher)).EndInit();
            this.ctxMenuTrigger.ResumeLayout(false);
            this.ctxMenuEntity.ResumeLayout(false);
            this.ctxTextObject.ResumeLayout(false);
            this.panelStretch.ResumeLayout(false);
            this.panelStretch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.resxWatcher)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.localedbWatcher)).EndInit();
            this.ctxMultiSelection.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.configWatcher)).EndInit();
            this.ctxSoundSource.ResumeLayout(false);
            this.ctxLightSource.ResumeLayout(false);
            this.ctxParticles.ResumeLayout(false);
            this.tabWorldSlice.ResumeLayout(false);
            this.tabWorldSlice.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSelectzonevalue)).EndInit();
            this.ctxMenuZone.ResumeLayout(false);
            this.ctxWaypoint.ResumeLayout(false);
            this.ctxCollider.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mainMenu;
        private System.Windows.Forms.ToolStripMenuItem fichierToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nouveauNiveauToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chargerNiveauToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enregistrerNiveauToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem preferencesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ajouterMapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem supprimerMapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem calquesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ajouterUnCalqueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem supprimerUnCalqueToolStripMenuItem;
        private System.Windows.Forms.TabControl tabMaps;
        private System.Windows.Forms.PictureBox tilesetBox;
        private System.Windows.Forms.TabControl tabLayout;
        private System.Windows.Forms.TabPage tablistitems;
        private System.Windows.Forms.ToolStrip actionBar;
        private System.IO.FileSystemWatcher tilsetWatcher;
        private System.Windows.Forms.SaveFileDialog saveLevelDialog;
        private System.Windows.Forms.OpenFileDialog openLevelDialog;
        private System.Windows.Forms.ContextMenuStrip ctxMenuCalques;
        private System.Windows.Forms.ToolStripMenuItem modifierCalqueToolStripMenuItem;
        private System.Windows.Forms.GroupBox grpCollisions;
        private System.Windows.Forms.Button btnNoCol;
        private System.Windows.Forms.Button btnCol;
        private System.Windows.Forms.Button btnChangeMode;
        private System.Windows.Forms.RichTextBox richlog;
        private OglPanel renderPanel;
        private System.Windows.Forms.GroupBox grpObjects;
        private System.Windows.Forms.Button btnTrigger;
        private System.Windows.Forms.Button btnColDestruct;
        private System.Windows.Forms.CheckBox chkcollisions;
        private System.Windows.Forms.ToolStripButton LayTileButton;
        private System.Windows.Forms.ToolStripButton FillTilesButton;
        private System.Windows.Forms.Label lbl_zoom;
        private System.Windows.Forms.ComboBox cmbZoom;
        private System.Windows.Forms.Panel panelactions;
        private System.IO.FileSystemWatcher scriptWatcher;
        private System.Windows.Forms.ToolStripMenuItem preftoolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip ctxMenuTrigger;
        private System.Windows.Forms.ToolStripMenuItem redimensionnerTriggerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem repositionnerTriggerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modifierTriggerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem supprimerTriggerToolStripMenuItem;
        private System.Windows.Forms.Button btnEntities;
        private System.Windows.Forms.ContextMenuStrip ctxMenuEntity;
        private System.Windows.Forms.ToolStripMenuItem repositionnerEntityToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modifierEntityToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem supprimerEntityToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modifierMapToolStripMenuItem;
        private System.Windows.Forms.Label lblCursorPos;
        private System.Windows.Forms.ToolStripMenuItem recréerObjetScriptToolStripMenuItem;
        private System.Windows.Forms.Button btnSlope;
        private System.Windows.Forms.Button btnSlope2;
        private System.Windows.Forms.ContextMenuStrip ctxTextObject;
        private System.Windows.Forms.ToolStripMenuItem txt_obj_repos;
        private System.Windows.Forms.ToolStripMenuItem txt_obj_update;
        private System.Windows.Forms.ToolStripMenuItem txt_obj_suppr;
        private System.Windows.Forms.ToolStripMenuItem txt_obj_script;
        private System.Windows.Forms.Button btnTexte;
        private System.Windows.Forms.Button btncoleditable;
        private System.Windows.Forms.TabPage tabCalques;
        private System.Windows.Forms.Label lblCurrentFrame;
        private System.Windows.Forms.TrackBar trkCurrentFrame;
        private System.Windows.Forms.Button btnReorder;
        private System.Windows.Forms.Button btnOrderDown;
        private System.Windows.Forms.Button btnOrderUp;
        private System.Windows.Forms.TreeView calquesList;
        private System.Windows.Forms.Label lblAlpha;
        private System.Windows.Forms.TrackBar trackAlpha;
        private System.Windows.Forms.Panel panelStretch;
        private System.Windows.Forms.ToolStripMenuItem animationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem générerUneAnimationToolStripMenuItem;
        private System.Windows.Forms.NumericUpDown numCurrentFrame;
        private System.Windows.Forms.ToolStripMenuItem supprimerUneAnimationToolStripMenuItem;
        private System.Windows.Forms.CheckBox chksnapgrid;
        private System.Windows.Forms.ToolStripMenuItem fermerNiveauToolStripMenuItem;
        private System.Windows.Forms.TreeView ItemsList;
        private System.Windows.Forms.Button btncoleditable_2;
        private System.Windows.Forms.ToolStripMenuItem dupliquerCalqueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importerUnFondAniméToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importerUnCalqueDepuisToolStripMenuItem;
        private System.IO.FileSystemWatcher resxWatcher;
        private System.Windows.Forms.ToolStripMenuItem focusEntitéToolStripMenuItem;
        private System.IO.FileSystemWatcher localedbWatcher;
        private System.Windows.Forms.Button btncoleditable_3;
        private System.Windows.Forms.Button btnKill;
        private System.Windows.Forms.ToolStripMenuItem sélectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem focusJoueurToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem placerJoueurToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outilsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consoleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem réinitialiserLogToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rechargerDLLJeuToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip ctxMultiSelection;
        private System.Windows.Forms.ToolStripMenuItem déplacerÉlémentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem supprimerÉlémentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem grouperÉlémentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recréerToutLesObjetsScriptsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rechargerScriptScèneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem niveauxRécentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton UndoButton;
        private System.Windows.Forms.ToolStripButton RedoButton;
        private System.Windows.Forms.ToolStripMenuItem annulerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rétablirToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButtonZoomplus;
        private System.Windows.Forms.ToolStripButton toolStripButtonZoomminus;
        private System.Windows.Forms.ToolStripButton toolStripButtonZoomreset;
        private System.Windows.Forms.ToolStripButton toolStripButtonNewLevel;
        private System.Windows.Forms.ToolStripButton toolStripButtonOpenLevel;
        private System.Windows.Forms.ToolStripButton toolStripButtonSaveLevel;
        private System.Windows.Forms.ToolStripMenuItem afficherFichierLogToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fusionAvecAutreNiveauToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem effacerHistoriqueNiveauxToolStripMenuItem;
        private System.IO.FileSystemWatcher configWatcher;
        private System.Windows.Forms.Button btnLight;
        private System.Windows.Forms.Button btnSon;
        private System.Windows.Forms.ContextMenuStrip ctxSoundSource;
        private System.Windows.Forms.ToolStripMenuItem snd_repos;
        private System.Windows.Forms.ToolStripMenuItem snd_modif;
        private System.Windows.Forms.ToolStripMenuItem snd_suppr;
        private System.Windows.Forms.ContextMenuStrip ctxLightSource;
        private System.Windows.Forms.ToolStripMenuItem light_repos;
        private System.Windows.Forms.ToolStripMenuItem light_modif;
        private System.Windows.Forms.ToolStripMenuItem light_suppr;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem windowsExport;
        private System.Windows.Forms.ToolStripMenuItem androidExport;
        private System.Windows.Forms.ToolStripMenuItem webGLExport;
        private System.Windows.Forms.SaveFileDialog exportLocation;
        private System.Windows.Forms.FolderBrowserDialog exportWebgl;
        private System.Windows.Forms.Button btnParticles;
        private System.Windows.Forms.ContextMenuStrip ctxParticles;
        private System.Windows.Forms.ToolStripMenuItem particles_repos;
        private System.Windows.Forms.ToolStripMenuItem particles_modif;
        private System.Windows.Forms.ToolStripMenuItem particles_suppr;
        private System.Windows.Forms.ToolStripMenuItem exportDansUnFichierStreamToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog exportToStreamFile;
        private System.Windows.Forms.OpenFileDialog openWorldDialog;
        private System.Windows.Forms.SaveFileDialog saveWorldDialog;
        private System.Windows.Forms.Panel tabWorldSlice;
        private System.Windows.Forms.TreeView tvZone;
        private System.Windows.Forms.Label lblselectzonevalue;
        private System.Windows.Forms.NumericUpDown numSelectzonevalue;
        private System.Windows.Forms.Button btnselectzonevalue;
        private System.Windows.Forms.ContextMenuStrip ctxMenuZone;
        private System.Windows.Forms.ToolStripMenuItem ajouterZone;
        private System.Windows.Forms.ToolStripMenuItem ajouterZoneEnfant;
        private System.Windows.Forms.ToolStripMenuItem modifierZone;
        private System.Windows.Forms.ToolStripMenuItem supprimerZone;
        private System.Windows.Forms.Label lblselectzone;
        private System.Windows.Forms.Label lblcurrentzone;
        private System.Windows.Forms.ToolStripMenuItem gestionFragmentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nouveauFragmentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chargerFragmentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enregistrerFragmentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fermerFragmentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gestionMondeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nouveauMondeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chargerMondeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enregistrerMondeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fermerMondeToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFragmentDialog;
        private System.Windows.Forms.SaveFileDialog saveFragmentDialog;
        private System.Windows.Forms.ToolStripMenuItem préférencesFragmentsToolStripMenuItem;
        private System.Windows.Forms.Label lblRowPos;
        private System.Windows.Forms.Button btnCollider;
        private System.Windows.Forms.Button btnWaypoint;
        private System.Windows.Forms.ContextMenuStrip ctxWaypoint;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ContextMenuStrip ctxCollider;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem redimensionnerToolStripMenuItem;
        private System.Windows.Forms.TextBox txtselectedentity;
        private System.Windows.Forms.Label lblselectedentity;
        private System.Windows.Forms.ToolStripMenuItem importerAssetStatiqueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem regénérerUnAssetToolStripMenuItem;
        private System.Windows.Forms.Button btnshowlogdetail;
        private System.Windows.Forms.CheckBox chkDrawallitems;
        private System.Windows.Forms.Button btnCheckIds;
    }
}

