﻿namespace WafaEditorV2
{
    partial class WorldDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WorldDetail));
            this.txtidworld = new System.Windows.Forms.TextBox();
            this.lblidworld = new System.Windows.Forms.Label();
            this.numRowsWorld = new System.Windows.Forms.NumericUpDown();
            this.lblRowsWorld = new System.Windows.Forms.Label();
            this.numColsWorld = new System.Windows.Forms.NumericUpDown();
            this.lblColsworld = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numRowsWorld)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numColsWorld)).BeginInit();
            this.SuspendLayout();
            // 
            // txtidworld
            // 
            resources.ApplyResources(this.txtidworld, "txtidworld");
            this.txtidworld.Name = "txtidworld";
            // 
            // lblidworld
            // 
            resources.ApplyResources(this.lblidworld, "lblidworld");
            this.lblidworld.Name = "lblidworld";
            // 
            // numRowsWorld
            // 
            resources.ApplyResources(this.numRowsWorld, "numRowsWorld");
            this.numRowsWorld.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.numRowsWorld.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numRowsWorld.Name = "numRowsWorld";
            this.numRowsWorld.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // lblRowsWorld
            // 
            resources.ApplyResources(this.lblRowsWorld, "lblRowsWorld");
            this.lblRowsWorld.Name = "lblRowsWorld";
            // 
            // numColsWorld
            // 
            resources.ApplyResources(this.numColsWorld, "numColsWorld");
            this.numColsWorld.Maximum = new decimal(new int[] {
            600,
            0,
            0,
            0});
            this.numColsWorld.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numColsWorld.Name = "numColsWorld";
            this.numColsWorld.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // lblColsworld
            // 
            resources.ApplyResources(this.lblColsworld, "lblColsworld");
            this.lblColsworld.Name = "lblColsworld";
            // 
            // btnCancel
            // 
            resources.ApplyResources(this.btnCancel, "btnCancel");
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            resources.ApplyResources(this.btnOk, "btnOk");
            this.btnOk.Name = "btnOk";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // WorldDetail
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.numRowsWorld);
            this.Controls.Add(this.lblRowsWorld);
            this.Controls.Add(this.numColsWorld);
            this.Controls.Add(this.lblColsworld);
            this.Controls.Add(this.txtidworld);
            this.Controls.Add(this.lblidworld);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "WorldDetail";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.numRowsWorld)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numColsWorld)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox txtidworld;
        private System.Windows.Forms.Label lblidworld;
        public System.Windows.Forms.NumericUpDown numRowsWorld;
        private System.Windows.Forms.Label lblRowsWorld;
        public System.Windows.Forms.NumericUpDown numColsWorld;
        private System.Windows.Forms.Label lblColsworld;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOk;
    }
}