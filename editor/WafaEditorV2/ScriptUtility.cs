﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PBInterface;

namespace WafaEditorV2
{
    public static class ScriptUtility
    {
        public static readonly string LUA_TEMPLATE_MODULE = "{0} = {{}}\n" +
                                                            "require(\"lib_vector\")\n" +
                                                            "require(\"entities_data\")\n\n" +
                                                            "function {0}.new(params)\n" +
                                                            "\treturn {{modulename = \"{0}\"}}\n" +
                                                            "end\n\n" +
                                                            "function {0}.init(obj,id)\n" +
                                                            "\tobj.id = id\n" +
                                                            "end\n\n" +
                                                            "function {0}.doEvents(obj,elapsed)\n" +
                                                            "end\n\n" +
                                                            "function {0}.onreset(obj)\n" +
                                                            "end\n\n" +
                                                            "function {0}.clean(obj)\n" +
                                                            "end\n\n" +
                                                            "return {0}";

        public static readonly string LUA_TEMPLATE_SCENE_MODULE = "{0} = {{}}\n" +
                                                            "{0}.editor_mode = config_getbool(\"editor\")\n"+
                                                            "require(\"lib_vector\")\n" +
                                                            "require(\"entities_data\")\n\n" +
                                                            "function {0}.init()\n" +
                                                            "\n" +
                                                            "end\n\n" +
                                                            "function {0}.doEvents(elapsed)\n" +
                                                            "\tupdate_entities(elapsed,nil)\n"+
                                                            "\tupdate_text(elapsed,nil)\n" +
                                                            "end\n\n" +
                                                            "function {0}.completelevel()\n" +
                                                            "end\n\n" +
                                                            "function {0}.level_reset_state()\n" +
                                                            "end\n\n" +
                                                            "return {0}";


        public static string genconstruct(Entity ent)
        {
            string creation_func = "";

            if (!ent.ScriptInitOnly)
            {
                creation_func = "add_entity(" + ent.ScriptCtor + ",'" + ent.EntityId + "')";
            }
            else
            {
                string construct = ent.ScriptCtor.Substring(0, ent.ScriptCtor.Length-1);

                if(construct.Last<char>() == '(')
                {
                    creation_func = construct + "'" + ent.EntityId + "')";
                }
                else
                {
                    creation_func = construct + ",'" + ent.EntityId + "')";
                }
                
            }

            return creation_func;
        }

        public static string gentextconstruct(TextObject text)
        {
            string creation_func = "";

            if (!text.ScriptInitOnly)
            {
                creation_func = "add_text(" + text.ScriptCtor + ",'" + text.TextId + "')";
            }
            else
            {
                string construct = text.ScriptCtor.Substring(0, text.ScriptCtor.Length - 1);

                if (construct.Last<char>() == '(')
                {
                    creation_func = construct + "'" + text.TextId + "')";
                }
                else
                {
                    creation_func = construct + ",'" + text.TextId + "')";
                }

            }

            return creation_func;
        }


    }
}
