﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PBInterface;

namespace WafaEditorV2
{
    public partial class ParticleEmitter : UserControl
    {
        public ParticleEmitter()
        {
            InitializeComponent();
        }

        public void FillData(PBInterface.ParticleEmitter data)
        {
            numrate.Value = (decimal)data.Rate;
            btnColor.BackColor = Color.FromArgb(data.Color.A, data.Color.R, data.Color.G, data.Color.B);
            numapha.Value = (decimal)data.Color.A;
            numwidth.Value = (decimal)data.WidthParticle;
            numHeight.Value = (decimal)data.HeightParticle;
            NumMaxParticles.Value = (decimal)data.MaxParticle;
            numLifetime.Value = (decimal)data.LifeTime;
            numTotalParticles.Value = (decimal)data.TotalParticle;
            numoffsetX.Value = (decimal)data.Offset.X;
            numoffsetY.Value = (decimal)data.Offset.Y;
            numDirX.Value = (decimal)data.Direction.X;
            numDirY.Value = (decimal)data.Direction.Y;
            numSpeed.Value = (decimal)data.Speed;
            this.Tag = data;
        }

        public void TmpSaveData()
        {
            this.Tag = this.GetData();
        }

        public PBInterface.ParticleEmitter GetData()
        {
            PBInterface.ParticleEmitter rdata = null;

            if (this.Tag == null)
                rdata = new PBInterface.ParticleEmitter();
            else
                rdata = (PBInterface.ParticleEmitter)this.Tag;

            rdata.Rate = Convert.ToInt32(numrate.Value);
            rdata.Color = new Wcolor();
            rdata.Color.R = btnColor.BackColor.R;
            rdata.Color.G = btnColor.BackColor.G;
            rdata.Color.B = btnColor.BackColor.B;
            rdata.Color.A = Convert.ToInt32(numapha.Value);

            rdata.WidthParticle = Convert.ToInt32(numwidth.Value);
            rdata.HeightParticle = Convert.ToInt32(numHeight.Value);
            rdata.MaxParticle = Convert.ToInt32(NumMaxParticles.Value);
            rdata.LifeTime = Convert.ToSingle(numLifetime.Value);
            rdata.TotalParticle = Convert.ToInt32(numTotalParticles.Value);
            rdata.Offset = new Wvector();
            rdata.Offset.X = Convert.ToInt32(numoffsetX.Value);
            rdata.Offset.Y = Convert.ToInt32(numoffsetY.Value);

            rdata.Direction = new Wvector();
            rdata.Direction.X = Convert.ToInt32(numDirX.Value);
            rdata.Direction.Y = Convert.ToInt32(numDirY.Value);
            rdata.Speed = Convert.ToSingle(numSpeed.Value);

            return rdata;

        }

    }
}
