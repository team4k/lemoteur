﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using PBInterface;
using WafaEditorV2.Properties;
using static WafaEditorV2.NativeFunc;

namespace WafaEditorV2
{
    public class ContainerType
    {
        public string Key { get; set; }
        public int Value { get; set; }

        public ContainerType(string pkey, int pvalue)
        {
            Key = pkey;
            Value = pvalue;
        }
    }

    public class WorldKey : IEquatable<WorldKey>
    {
        public GAME_TYPE type { get; }
        public string id { get; }

        public WorldKey(GAME_TYPE ptype, string pid)
        {
            type = ptype;
            id = pid;
        }

        public bool Equals(WorldKey other)
        {
            return (other.id == this.id && other.type == this.type);
        }

        public override int GetHashCode()
        {
            return this.id.GetHashCode() * 17 + this.type.GetHashCode();
            //return base.GetHashCode();
        }
    }

    public class Worldcheck
    {
        public string currentId { get; set; }
        public string fixedId { get; set; }
        public string chunkfile { get; set; }
        public bool mustchange { get; set; }

        public Worldcheck(string pcurrid,string pfixedid,string pchunkfile)
        {
            currentId = pcurrid;
            fixedId = pfixedid;
            chunkfile = pchunkfile;
            mustchange = (currentId != fixedId);
        }
    }


    public static class EditorUtilities
    {
        public static bool ObjectIdExist(Level olevel,string idtocheck,dynamic refobject = null)
        {
            foreach (Map omap in olevel.MapArrays)
            {
                foreach (Layer olayer in omap.LayerArrays)
                {
                    int num = 0;

                    num = olayer.EntityArrays.Count(p => p.EntityId.ToUpper() == idtocheck.ToUpper() && p != refobject);

                    num += olayer.TriggerArrays.Count(p => p.TriggerId.ToUpper() == idtocheck.ToUpper() && p != refobject);
 
                    num += olayer.TextArrays.Count(p => p.TextId.ToUpper() == idtocheck.ToUpper() && p != refobject);

                    num += olayer.SoundArrays.Count(p => p.SoundId.ToUpper() == idtocheck.ToUpper() && p != refobject);

                    num += olayer.LightArrays.Count(p => p.LightId.ToUpper() == idtocheck.ToUpper() && p != refobject);

                    num += olayer.ParticlesArrays.Count(p => p.Id.ToUpper() == idtocheck.ToUpper() && p != refobject);

                    num += olayer.Waypoints.Count(p => p.Id.ToUpper() == idtocheck.ToUpper() && p != refobject);

                    num += olayer.Colliders.Count(p => p.Id.ToUpper() == idtocheck.ToUpper() && p != refobject);


                    if (num != 0)
                        return true;
                    
                }
            }

            return false;
        }

        public static bool ZoneExist(World oworld,string idtocheck, ZoneSlice slice = null)
        {
            return (oworld.SliceDatas.Count(p => p.Name.ToUpper() == idtocheck.ToUpper() && p != slice) > 0);
        }

        private static void CheckKey(Dictionary<WorldKey, Worldcheck> checkresult,GAME_TYPE type,string id,string level,string chunkfile)
        {
            WorldKey newid = new WorldKey(type, id);

            if (checkresult.ContainsKey(newid))
            {
                newid = new WorldKey(type, level + "_" + id);
            }

            checkresult.Add(newid, new Worldcheck(id, newid.id, chunkfile));
        }

        public static Dictionary<WorldKey, Worldcheck> CheckWorld(World oworld)
        {
            Dictionary<WorldKey, Worldcheck> checkresult = new Dictionary<WorldKey, Worldcheck>();

            foreach(ZoneSlice zone in  oworld.SliceDatas)
            {
                if(zone.TypeZone == (int)TypeZone.ZStaticlevel)
                {
                    string level = zone.ScriptGenerator;

                    Level staticlevel = null;

                    using (var file = File.OpenRead(Path.Combine(Settings.Default.assets_path,level)))
                        staticlevel = ProtoBuf.Serializer.Deserialize<PBInterface.Level>(file);

                    foreach (Layer olayer in  staticlevel.MapArrays[0].LayerArrays)
                    {
                        if (olayer.Type != (int)TypeLayer.Objects)
                        {
                            continue; 
                        }

                        foreach(Entity ent in  olayer.EntityArrays)
                        {
                            CheckKey(checkresult, GAME_TYPE.GT_ENTITY, ent.EntityId, staticlevel.LevelId,level);
                        }

                        foreach (Trigger trig in olayer.TriggerArrays)
                        {
                            CheckKey(checkresult, GAME_TYPE.GT_TRIGGER, trig.TriggerId, staticlevel.LevelId,level);
                        }

                        foreach (TextObject txt in olayer.TextArrays)
                        {
                            CheckKey(checkresult, GAME_TYPE.GT_TEXT, txt.TextId, staticlevel.LevelId,level);
                        }

                        foreach (SoundSource snd in olayer.SoundArrays)
                        {
                            CheckKey(checkresult, GAME_TYPE.GT_SOUND, snd.SoundId, staticlevel.LevelId,level);
                        }

                        foreach (LightSource light in olayer.LightArrays)
                        {
                            CheckKey(checkresult, GAME_TYPE.GT_LIGHT, light.LightId, staticlevel.LevelId,level);
                        }

                        foreach (ParticlesData prt in olayer.ParticlesArrays)
                        {
                            CheckKey(checkresult, GAME_TYPE.GT_PARTICLES, prt.Id, staticlevel.LevelId,level);
                        }

                        foreach (Waypoint way in olayer.Waypoints)
                        {
                            CheckKey(checkresult, GAME_TYPE.GT_WAYPOINT, way.Id, staticlevel.LevelId,level);
                        }

                        foreach (Collider col in olayer.Colliders)
                        {
                            CheckKey(checkresult, GAME_TYPE.GT_COLLIDER, col.Id, staticlevel.LevelId,level);
                        }
                    }
                }
            }

            return checkresult;
        }

    }
}
