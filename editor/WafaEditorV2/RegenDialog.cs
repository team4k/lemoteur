﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WafaEditorV2.Properties;
using AssetsImport;

namespace WafaEditorV2
{
    public partial class RegenDialog : Form, IlogImport
    {
        public RegenDialog()
        {
            InitializeComponent();

            lstGimpFiles.Items.Clear();
            //get gimp files in raw assets dir / work dir and check if a corresponding png file exist in assets dir

            string workdir = Path.Combine(Settings.Default.raw_assets_path, "work_assets");

            if(!Directory.Exists(workdir))
            {
                MessageBox.Show("Impossible de charger la liste des assets regénérable car aucun répertoir work_assets existe dans le répertoire assets de travail");
                return;
            }

            foreach(string gimpfile in Directory.GetFiles(workdir))
            {
                FileInfo inf = new FileInfo(gimpfile);
                
                if (inf.Extension == ".xcf")
                {
                    //check if the same file exist with a png extension
                    if(File.Exists(Path.Combine(Settings.Default.assets_path,inf.Name.Substring(0,inf.Name.LastIndexOf('.'))+".png")))
                    {
                        lstGimpFiles.Items.Add(inf.Name);
                    }
                }
            }

        }

        public void AppendLog(string data, bool iserror = false, bool threaded = false)
        {
           // throw new NotImplementedException();
        }

        public void AppendText(string text)
        {
            //throw new NotImplementedException();
        }

        public void DoEvents()
        {
            Application.DoEvents();
        }

        private void btnRegen_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (object item in lstGimpFiles.SelectedItems)
                {
                    string finalimagepath = Path.Combine(Settings.Default.assets_path, item.ToString().Substring(0, item.ToString().LastIndexOf('.')) + ".png");
                    //get final texture dimension
                    Bitmap finalimage = new Bitmap(finalimagepath);

                    string workdir = Path.Combine(Settings.Default.raw_assets_path, "work_assets");

                    int width = finalimage.Width;
                    int height = finalimage.Height;
                    finalimage.Dispose();

                    string outfile = finalimagepath;

                    Import.ExportGimpToResizedPng(Path.Combine(workdir, item.ToString()), finalimagepath, this, width, height);
                }

                MessageBox.Show("regénération effectuée !");
                this.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Erreur lors de la regénération !" + ex.ToString());
            }
        }
    }
}
