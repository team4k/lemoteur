﻿namespace WafaEditorV2
{
    partial class TextDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TextDialog));
            this.chkCallback = new System.Windows.Forms.CheckBox();
            this.lblheight = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblwidth = new System.Windows.Forms.Label();
            this.numheight = new System.Windows.Forms.NumericUpDown();
            this.numwidth = new System.Windows.Forms.NumericUpDown();
            this.txtCtor = new System.Windows.Forms.TextBox();
            this.lblCtor = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.lblTileset = new System.Windows.Forms.Label();
            this.lblScript = new System.Windows.Forms.Label();
            this.txtidtexte = new System.Windows.Forms.TextBox();
            this.lblidtexte = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.numSizeFont = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.rtbContent = new System.Windows.Forms.RichTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbAlignement = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.colorFont = new System.Windows.Forms.ColorDialog();
            this.btnColor = new System.Windows.Forms.Button();
            this.num_parallaxy = new System.Windows.Forms.NumericUpDown();
            this.num_parallaxx = new System.Windows.Forms.NumericUpDown();
            this.lbl_parallaxy = new System.Windows.Forms.Label();
            this.lbl_parallaxx = new System.Windows.Forms.Label();
            this.chk_cull = new System.Windows.Forms.CheckBox();
            this.chknotranslation = new System.Windows.Forms.CheckBox();
            this.cmbFont = new System.Windows.Forms.ComboBox();
            this.chkAntialias = new System.Windows.Forms.CheckBox();
            this.cmbScript = new System.Windows.Forms.ComboBox();
            this.btnNewScript = new System.Windows.Forms.Button();
            this.chkFixed = new System.Windows.Forms.CheckBox();
            this.chk_script_init_only = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.numheight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numwidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSizeFont)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_parallaxy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_parallaxx)).BeginInit();
            this.SuspendLayout();
            // 
            // chkCallback
            // 
            this.chkCallback.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkCallback.Location = new System.Drawing.Point(31, 248);
            this.chkCallback.Name = "chkCallback";
            this.chkCallback.Size = new System.Drawing.Size(216, 26);
            this.chkCallback.TabIndex = 56;
            this.chkCallback.Text = "Avec fonction de rappel (callback)";
            this.chkCallback.UseVisualStyleBackColor = true;
            // 
            // lblheight
            // 
            this.lblheight.AutoSize = true;
            this.lblheight.Location = new System.Drawing.Point(30, 221);
            this.lblheight.Name = "lblheight";
            this.lblheight.Size = new System.Drawing.Size(88, 13);
            this.lblheight.TabIndex = 65;
            this.lblheight.Text = "Hauteur minimale";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(30, 221);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 68;
            // 
            // lblwidth
            // 
            this.lblwidth.AutoSize = true;
            this.lblwidth.Location = new System.Drawing.Point(30, 172);
            this.lblwidth.Name = "lblwidth";
            this.lblwidth.Size = new System.Drawing.Size(86, 13);
            this.lblwidth.TabIndex = 64;
            this.lblwidth.Text = "Largeur minimale";
            // 
            // numheight
            // 
            this.numheight.Location = new System.Drawing.Point(147, 214);
            this.numheight.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numheight.Minimum = new decimal(new int[] {
            32,
            0,
            0,
            0});
            this.numheight.Name = "numheight";
            this.numheight.Size = new System.Drawing.Size(128, 20);
            this.numheight.TabIndex = 51;
            this.numheight.Value = new decimal(new int[] {
            32,
            0,
            0,
            0});
            // 
            // numwidth
            // 
            this.numwidth.Location = new System.Drawing.Point(147, 172);
            this.numwidth.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numwidth.Minimum = new decimal(new int[] {
            32,
            0,
            0,
            0});
            this.numwidth.Name = "numwidth";
            this.numwidth.Size = new System.Drawing.Size(128, 20);
            this.numwidth.TabIndex = 50;
            this.numwidth.Value = new decimal(new int[] {
            32,
            0,
            0,
            0});
            // 
            // txtCtor
            // 
            this.txtCtor.Location = new System.Drawing.Point(147, 76);
            this.txtCtor.Name = "txtCtor";
            this.txtCtor.Size = new System.Drawing.Size(289, 20);
            this.txtCtor.TabIndex = 46;
            // 
            // lblCtor
            // 
            this.lblCtor.AutoSize = true;
            this.lblCtor.Location = new System.Drawing.Point(34, 82);
            this.lblCtor.Name = "lblCtor";
            this.lblCtor.Size = new System.Drawing.Size(67, 13);
            this.lblCtor.TabIndex = 61;
            this.lblCtor.Text = "Constructeur";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(194, 782);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 22);
            this.btnCancel.TabIndex = 58;
            this.btnCancel.Text = "Annuler";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(96, 782);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(80, 22);
            this.btnOk.TabIndex = 57;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // lblTileset
            // 
            this.lblTileset.AutoSize = true;
            this.lblTileset.Location = new System.Drawing.Point(33, 135);
            this.lblTileset.Name = "lblTileset";
            this.lblTileset.Size = new System.Drawing.Size(99, 13);
            this.lblTileset.TabIndex = 63;
            this.lblTileset.Text = "Police de caractère";
            // 
            // lblScript
            // 
            this.lblScript.AutoSize = true;
            this.lblScript.Location = new System.Drawing.Point(33, 50);
            this.lblScript.Name = "lblScript";
            this.lblScript.Size = new System.Drawing.Size(59, 13);
            this.lblScript.TabIndex = 60;
            this.lblScript.Text = "Module lua";
            // 
            // txtidtexte
            // 
            this.txtidtexte.Location = new System.Drawing.Point(147, 12);
            this.txtidtexte.Name = "txtidtexte";
            this.txtidtexte.Size = new System.Drawing.Size(158, 20);
            this.txtidtexte.TabIndex = 44;
            // 
            // lblidtexte
            // 
            this.lblidtexte.AutoSize = true;
            this.lblidtexte.Location = new System.Drawing.Point(34, 14);
            this.lblidtexte.Name = "lblidtexte";
            this.lblidtexte.Size = new System.Drawing.Size(79, 13);
            this.lblidtexte.TabIndex = 59;
            this.lblidtexte.Text = "Identifiant texte";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 293);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 70;
            this.label1.Text = "Taille caractère";
            // 
            // numSizeFont
            // 
            this.numSizeFont.Location = new System.Drawing.Point(147, 293);
            this.numSizeFont.Maximum = new decimal(new int[] {
            72,
            0,
            0,
            0});
            this.numSizeFont.Minimum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.numSizeFont.Name = "numSizeFont";
            this.numSizeFont.Size = new System.Drawing.Size(128, 20);
            this.numSizeFont.TabIndex = 69;
            this.numSizeFont.Value = new decimal(new int[] {
            12,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(30, 334);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 71;
            this.label3.Text = "Texte à afficher";
            // 
            // rtbContent
            // 
            this.rtbContent.Location = new System.Drawing.Point(147, 337);
            this.rtbContent.Name = "rtbContent";
            this.rtbContent.Size = new System.Drawing.Size(289, 92);
            this.rtbContent.TabIndex = 72;
            this.rtbContent.Text = "";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(30, 451);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 73;
            this.label4.Text = "Alignement";
            // 
            // cbAlignement
            // 
            this.cbAlignement.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAlignement.FormattingEnabled = true;
            this.cbAlignement.Location = new System.Drawing.Point(146, 451);
            this.cbAlignement.Name = "cbAlignement";
            this.cbAlignement.Size = new System.Drawing.Size(129, 21);
            this.cbAlignement.TabIndex = 74;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(33, 496);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 75;
            this.label5.Text = "Couleur";
            // 
            // btnColor
            // 
            this.btnColor.Location = new System.Drawing.Point(146, 491);
            this.btnColor.Name = "btnColor";
            this.btnColor.Size = new System.Drawing.Size(128, 22);
            this.btnColor.TabIndex = 76;
            this.btnColor.Text = "Choisir Couleur";
            this.btnColor.UseVisualStyleBackColor = true;
            this.btnColor.Click += new System.EventHandler(this.btnColor_Click);
            // 
            // num_parallaxy
            // 
            this.num_parallaxy.DecimalPlaces = 2;
            this.num_parallaxy.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.num_parallaxy.Location = new System.Drawing.Point(146, 582);
            this.num_parallaxy.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.num_parallaxy.Name = "num_parallaxy";
            this.num_parallaxy.Size = new System.Drawing.Size(128, 20);
            this.num_parallaxy.TabIndex = 80;
            this.num_parallaxy.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // num_parallaxx
            // 
            this.num_parallaxx.DecimalPlaces = 2;
            this.num_parallaxx.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.num_parallaxx.Location = new System.Drawing.Point(146, 533);
            this.num_parallaxx.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.num_parallaxx.Name = "num_parallaxx";
            this.num_parallaxx.Size = new System.Drawing.Size(128, 20);
            this.num_parallaxx.TabIndex = 79;
            this.num_parallaxx.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lbl_parallaxy
            // 
            this.lbl_parallaxy.AutoSize = true;
            this.lbl_parallaxy.Location = new System.Drawing.Point(30, 582);
            this.lbl_parallaxy.Name = "lbl_parallaxy";
            this.lbl_parallaxy.Size = new System.Drawing.Size(75, 13);
            this.lbl_parallaxy.TabIndex = 78;
            this.lbl_parallaxy.Text = "Parallaxe en Y";
            // 
            // lbl_parallaxx
            // 
            this.lbl_parallaxx.AutoSize = true;
            this.lbl_parallaxx.Location = new System.Drawing.Point(33, 539);
            this.lbl_parallaxx.Name = "lbl_parallaxx";
            this.lbl_parallaxx.Size = new System.Drawing.Size(75, 13);
            this.lbl_parallaxx.TabIndex = 77;
            this.lbl_parallaxx.Text = "Parallaxe en X";
            // 
            // chk_cull
            // 
            this.chk_cull.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chk_cull.Location = new System.Drawing.Point(38, 622);
            this.chk_cull.Name = "chk_cull";
            this.chk_cull.Size = new System.Drawing.Size(209, 26);
            this.chk_cull.TabIndex = 81;
            this.chk_cull.Text = "Pas de culling";
            this.chk_cull.UseVisualStyleBackColor = true;
            // 
            // chknotranslation
            // 
            this.chknotranslation.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chknotranslation.Location = new System.Drawing.Point(38, 654);
            this.chknotranslation.Name = "chknotranslation";
            this.chknotranslation.Size = new System.Drawing.Size(209, 26);
            this.chknotranslation.TabIndex = 82;
            this.chknotranslation.Text = "Pas de traduction";
            this.chknotranslation.UseVisualStyleBackColor = true;
            // 
            // cmbFont
            // 
            this.cmbFont.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFont.FormattingEnabled = true;
            this.cmbFont.Location = new System.Drawing.Point(145, 132);
            this.cmbFont.Name = "cmbFont";
            this.cmbFont.Size = new System.Drawing.Size(129, 21);
            this.cmbFont.TabIndex = 83;
            // 
            // chkAntialias
            // 
            this.chkAntialias.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkAntialias.Location = new System.Drawing.Point(38, 687);
            this.chkAntialias.Name = "chkAntialias";
            this.chkAntialias.Size = new System.Drawing.Size(209, 26);
            this.chkAntialias.TabIndex = 84;
            this.chkAntialias.Text = "Anti aliasing";
            this.chkAntialias.UseVisualStyleBackColor = true;
            // 
            // cmbScript
            // 
            this.cmbScript.FormattingEnabled = true;
            this.cmbScript.Location = new System.Drawing.Point(148, 47);
            this.cmbScript.Name = "cmbScript";
            this.cmbScript.Size = new System.Drawing.Size(157, 21);
            this.cmbScript.TabIndex = 85;
            // 
            // btnNewScript
            // 
            this.btnNewScript.Image = ((System.Drawing.Image)(resources.GetObject("btnNewScript.Image")));
            this.btnNewScript.Location = new System.Drawing.Point(311, 44);
            this.btnNewScript.Name = "btnNewScript";
            this.btnNewScript.Size = new System.Drawing.Size(26, 24);
            this.btnNewScript.TabIndex = 86;
            this.btnNewScript.UseVisualStyleBackColor = true;
            this.btnNewScript.Visible = false;
            this.btnNewScript.Click += new System.EventHandler(this.btnNewScript_Click);
            // 
            // chkFixed
            // 
            this.chkFixed.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkFixed.Location = new System.Drawing.Point(38, 720);
            this.chkFixed.Name = "chkFixed";
            this.chkFixed.Size = new System.Drawing.Size(209, 26);
            this.chkFixed.TabIndex = 87;
            this.chkFixed.Text = "Taile fixe (pas de zoom)";
            this.chkFixed.UseVisualStyleBackColor = true;
            // 
            // chk_script_init_only
            // 
            this.chk_script_init_only.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chk_script_init_only.Location = new System.Drawing.Point(12, 98);
            this.chk_script_init_only.Name = "chk_script_init_only";
            this.chk_script_init_only.Size = new System.Drawing.Size(235, 34);
            this.chk_script_init_only.TabIndex = 88;
            this.chk_script_init_only.Text = "Initialisation uniquement (pas d\'objet lua créé)";
            this.chk_script_init_only.UseVisualStyleBackColor = true;
            // 
            // TextDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(484, 816);
            this.Controls.Add(this.chk_script_init_only);
            this.Controls.Add(this.chkFixed);
            this.Controls.Add(this.btnNewScript);
            this.Controls.Add(this.cmbScript);
            this.Controls.Add(this.chkAntialias);
            this.Controls.Add(this.cmbFont);
            this.Controls.Add(this.chknotranslation);
            this.Controls.Add(this.chk_cull);
            this.Controls.Add(this.num_parallaxy);
            this.Controls.Add(this.num_parallaxx);
            this.Controls.Add(this.lbl_parallaxy);
            this.Controls.Add(this.lbl_parallaxx);
            this.Controls.Add(this.btnColor);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cbAlignement);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.rtbContent);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.numSizeFont);
            this.Controls.Add(this.chkCallback);
            this.Controls.Add(this.lblheight);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblwidth);
            this.Controls.Add(this.numheight);
            this.Controls.Add(this.numwidth);
            this.Controls.Add(this.txtCtor);
            this.Controls.Add(this.lblCtor);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.lblTileset);
            this.Controls.Add(this.lblScript);
            this.Controls.Add(this.txtidtexte);
            this.Controls.Add(this.lblidtexte);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TextDialog";
            this.ShowIcon = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Ajouter un texte";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TextDialog_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.numheight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numwidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSizeFont)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_parallaxy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_parallaxx)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chkCallback;
        private System.Windows.Forms.Label lblheight;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblwidth;
        private System.Windows.Forms.NumericUpDown numheight;
        private System.Windows.Forms.NumericUpDown numwidth;
        private System.Windows.Forms.TextBox txtCtor;
        private System.Windows.Forms.Label lblCtor;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Label lblTileset;
        private System.Windows.Forms.Label lblScript;
        private System.Windows.Forms.TextBox txtidtexte;
        private System.Windows.Forms.Label lblidtexte;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numSizeFont;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RichTextBox rtbContent;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbAlignement;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ColorDialog colorFont;
        private System.Windows.Forms.Button btnColor;
        public System.Windows.Forms.NumericUpDown num_parallaxy;
        public System.Windows.Forms.NumericUpDown num_parallaxx;
        private System.Windows.Forms.Label lbl_parallaxy;
        private System.Windows.Forms.Label lbl_parallaxx;
        private System.Windows.Forms.CheckBox chk_cull;
        private System.Windows.Forms.CheckBox chknotranslation;
        private System.Windows.Forms.ComboBox cmbFont;
        private System.Windows.Forms.CheckBox chkAntialias;
        private System.Windows.Forms.ComboBox cmbScript;
        private System.Windows.Forms.Button btnNewScript;
        private System.Windows.Forms.CheckBox chkFixed;
        private System.Windows.Forms.CheckBox chk_script_init_only;
    }
}