﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PBInterface;

namespace WafaEditorV2
{
    public partial class ImportCalque : Form
    {
        Level importLevel;
        int refnum_cols;
        int refnum_rows;

        List<ContainerType> result;

        public ImportCalque(Level oLevel,int num_col,int num_row)
        {
            InitializeComponent();

            importLevel = oLevel;
            refnum_cols = num_col;
            refnum_rows = num_row;

            cmbMap.DataSource = importLevel.MapArrays;
            cmbMap.DisplayMember = "MapId";

            cmbCalque.DataSource = importLevel.MapArrays[0].LayerArrays;

            cmbCalque.DisplayMember = "LayerId";

            string[] shape_types = Enum.GetNames(typeof(AnchorType));
            int[] shape_types_val = (int[])Enum.GetValues(typeof(AnchorType));

            result = new List<ContainerType>(shape_types.Length);

            for (int i = 0; i < shape_types.Length; i++)
                result.Add(new ContainerType(shape_types[i], shape_types_val[i]));


            cmbAnchor.DataSource = result;

            cmbAnchor.DisplayMember = "Key";
            cmbAnchor.ValueMember = "Value";
            cmbAnchor.SelectedIndex = 0;
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            Layer newLayer = ProtoBuf.Serializer.DeepClone<Layer>((Layer)cmbCalque.SelectedItem);

            //redimensionnement de la couche importée
            if (importLevel.MapArrays[0].ColCount != refnum_cols || importLevel.MapArrays[0].RowCount != refnum_rows)
            {
                List<int> redim = new List<int>(refnum_cols * refnum_rows);

                int iOld = 0;
                int iNew = 0;

                //reset update flag
                foreach (TilemapFrame frm in newLayer.Frames)
                    foreach (TileInfo frm_info in frm.TileUpdates)
                        frm_info.EditorUpdated = false;



                if (((ContainerType)cmbAnchor.SelectedItem).Value == (int)AnchorType.TopLeft)
                {
                    int diff_row = importLevel.MapArrays[0].RowCount - refnum_rows;

                    if(diff_row > 0)
                    {
                        iOld = (importLevel.MapArrays[0].ColCount * diff_row);
                    }
                }

                Dictionary<int, int> animation_changed_index = new Dictionary<int, int>();

                for(int iRow = 0; iRow < refnum_rows;iRow++)
                {

                    for(int iCol = 0;iCol <refnum_cols;iCol++)
                    {
                        if (iCol < importLevel.MapArrays[0].ColCount && iOld < newLayer.TileArrays.Length)
                        {
                            redim.Add(newLayer.TileArrays[iOld++]);//[iCol + (iRow * refnum_rows)] =

                            //set new animations index
                            if (newLayer.Animated && (iOld - 1) != iNew)
                                animation_changed_index.Add((iOld - 1), iNew);
                        }
                        else
                            redim.Add(0); //[iCol + (iRow * refnum_rows)] = 0;

                        iNew++;
                               
                    }

                    if (refnum_cols < importLevel.MapArrays[0].ColCount)
                    {

                        //remove all animation in old range

                        List<TileInfo> rm_updates = new List<TileInfo>();

                        foreach (TilemapFrame frm in newLayer.Frames)
                        {
                            foreach (TileInfo frm_info in frm.TileUpdates)
                            {
                                if (frm_info.Index >= iOld && frm_info.Index < (iOld + (importLevel.MapArrays[0].ColCount - refnum_cols)))
                                {
                                    rm_updates.Add(frm_info);
                                }
                            }

                            foreach (TileInfo rm_info in rm_updates)
                                frm.TileUpdates.Remove(rm_info);

                            rm_updates.Clear();
                        }

                        iOld += (importLevel.MapArrays[0].ColCount - refnum_cols);

                    }
                }

                //update animations index
                foreach (KeyValuePair<int, int> update_anim_index in animation_changed_index)
                {
                    foreach (TilemapFrame frm in newLayer.Frames)
                    {
                        foreach (TileInfo frm_info in frm.TileUpdates)
                        {
                            if (!frm_info.EditorUpdated && frm_info.Index == update_anim_index.Key)
                            {
                                frm_info.Index = update_anim_index.Value;
                                frm_info.EditorUpdated = true;
                                break;
                            }
                        }
                    }
                }


                newLayer.TileArrays = redim.ToArray();

                if (newLayer.Animated)
                {
                    List<TileInfo> rm_updates = new List<TileInfo>();

                    //remove out of bounds frame updates
                    foreach (TilemapFrame frm in newLayer.Frames)
                    {
                        foreach (TileInfo frm_info in frm.TileUpdates)
                        {
                            if (frm_info.Index >= iNew)
                            {
                                rm_updates.Add(frm_info);
                            }
                        }

                        foreach (TileInfo rm_info in rm_updates)
                            frm.TileUpdates.Remove(rm_info);

                        rm_updates.Clear();

                    }
                }
                
            }


            


            this.Tag = newLayer;

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void cmbMap_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmbCalque.DataSource = ((Map)cmbMap.SelectedItem).LayerArrays;
        }


    }
}
