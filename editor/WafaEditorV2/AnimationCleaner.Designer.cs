﻿namespace WafaEditorV2
{
    partial class AnimationCleaner
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AnimationCleaner));
            this.numEndTile = new System.Windows.Forms.NumericUpDown();
            this.numStartTile = new System.Windows.Forms.NumericUpDown();
            this.btnFin = new System.Windows.Forms.Button();
            this.btnDebut = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.numFrameDebut = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.numFrameFin = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.btnAnnuler = new System.Windows.Forms.Button();
            this.BtnSupprimer = new System.Windows.Forms.Button();
            this.cmbTypeAnime = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numEndTile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numStartTile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numFrameDebut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numFrameFin)).BeginInit();
            this.SuspendLayout();
            // 
            // numEndTile
            // 
            resources.ApplyResources(this.numEndTile, "numEndTile");
            this.numEndTile.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numEndTile.Name = "numEndTile";
            // 
            // numStartTile
            // 
            resources.ApplyResources(this.numStartTile, "numStartTile");
            this.numStartTile.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numStartTile.Name = "numStartTile";
            // 
            // btnFin
            // 
            resources.ApplyResources(this.btnFin, "btnFin");
            this.btnFin.Name = "btnFin";
            this.btnFin.UseVisualStyleBackColor = true;
            this.btnFin.Click += new System.EventHandler(this.btnFin_Click);
            // 
            // btnDebut
            // 
            resources.ApplyResources(this.btnDebut, "btnDebut");
            this.btnDebut.Name = "btnDebut";
            this.btnDebut.UseVisualStyleBackColor = true;
            this.btnDebut.Click += new System.EventHandler(this.btnDebut_Click);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // numFrameDebut
            // 
            resources.ApplyResources(this.numFrameDebut, "numFrameDebut");
            this.numFrameDebut.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numFrameDebut.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numFrameDebut.Name = "numFrameDebut";
            this.numFrameDebut.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // numFrameFin
            // 
            resources.ApplyResources(this.numFrameFin, "numFrameFin");
            this.numFrameFin.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numFrameFin.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numFrameFin.Name = "numFrameFin";
            this.numFrameFin.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // btnAnnuler
            // 
            resources.ApplyResources(this.btnAnnuler, "btnAnnuler");
            this.btnAnnuler.Name = "btnAnnuler";
            this.btnAnnuler.UseVisualStyleBackColor = true;
            this.btnAnnuler.Click += new System.EventHandler(this.btnAnnuler_Click);
            // 
            // BtnSupprimer
            // 
            resources.ApplyResources(this.BtnSupprimer, "BtnSupprimer");
            this.BtnSupprimer.Name = "BtnSupprimer";
            this.BtnSupprimer.UseVisualStyleBackColor = true;
            this.BtnSupprimer.Click += new System.EventHandler(this.BtnSupprimer_Click);
            // 
            // cmbTypeAnime
            // 
            this.cmbTypeAnime.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTypeAnime.FormattingEnabled = true;
            resources.ApplyResources(this.cmbTypeAnime, "cmbTypeAnime");
            this.cmbTypeAnime.Name = "cmbTypeAnime";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // AnimationCleaner
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.cmbTypeAnime);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnAnnuler);
            this.Controls.Add(this.BtnSupprimer);
            this.Controls.Add(this.numFrameFin);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.numFrameDebut);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.numEndTile);
            this.Controls.Add(this.numStartTile);
            this.Controls.Add(this.btnFin);
            this.Controls.Add(this.btnDebut);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AnimationCleaner";
            ((System.ComponentModel.ISupportInitialize)(this.numEndTile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numStartTile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numFrameDebut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numFrameFin)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown numEndTile;
        private System.Windows.Forms.NumericUpDown numStartTile;
        private System.Windows.Forms.Button btnFin;
        private System.Windows.Forms.Button btnDebut;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numFrameDebut;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown numFrameFin;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAnnuler;
        private System.Windows.Forms.Button BtnSupprimer;
        private System.Windows.Forms.ComboBox cmbTypeAnime;
        private System.Windows.Forms.Label label4;
    }
}