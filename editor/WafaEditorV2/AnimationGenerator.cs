﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using PBInterface;

namespace WafaEditorV2
{
    public enum GeneratorMovement
    {
        LinearHorizontal,
        LinearVertical
    }

    public partial class AnimationGenerator : Form
    {
        public class TileSequenceContainer
        {
            public int tile_value;
            public int x;
            public int y;

            public TileSequenceContainer(int tile_value, int x, int y)
            {
                this.tile_value = tile_value;
                this.x = x;
                this.y = y;
            }
        }


        List<ContainerType> result;
        List<TileSequenceContainer> tile_sequence = new List<TileSequenceContainer>();

        OglPanel _ref_scene;
        Workbench _ref_wrkbench;
        Bitmap _tilemap;

        int _max_frame;
        string _ref_layer_id;

        TileSequenceContainer tile_bascule = null;

        public AnimationGenerator(int max_tile,int max_frame,OglPanel ref_scene,Workbench ref_wrkbench,Bitmap reftilemap,string layer_id)
        {
            InitializeComponent();

            string[] types_anime = Enum.GetNames(typeof(GeneratorMovement));
            int[] types_anime_val = (int[])Enum.GetValues(typeof(GeneratorMovement));
            result = new List<ContainerType>(types_anime.Length);

            for (int i = 0; i < types_anime.Length; i++)
                result.Add(new ContainerType(types_anime[i], types_anime_val[i]));


            cmbTypeAnime.Items.AddRange(result.ToArray());
            cmbTypeAnime.DisplayMember = "Key";
            cmbTypeAnime.ValueMember = "Value";

            numFrameDebut.Maximum = max_frame;
            _max_frame = max_frame;

            numStartTile.Maximum = max_tile;
            numEndTile.Maximum = max_tile;

            _ref_scene = ref_scene;
            _ref_wrkbench = ref_wrkbench;
            _tilemap = reftilemap;
            _ref_layer_id = layer_id;


          
        }

        private void btnDebut_Click(object sender, EventArgs e)
        {
            this.Enabled = false;
            this.SendToBack();

            _ref_scene.OnPositionTileChanged += this._ref_scene_OnPositionTileChanged;
        }

        private void _ref_scene_OnPositionTileChanged(int newPos)
        {
            numStartTile.Value = newPos;
            _ref_scene.OnPositionTileChanged -= this._ref_scene_OnPositionTileChanged;
            this.Enabled = true;
            this.BringToFront();
        }

        private void btnFin_Click(object sender, EventArgs e)
        {
            this.Enabled = false;
            this.SendToBack();

            _ref_scene.OnPositionTileChanged += this._ref_scene_OnPositionTileChanged2;
        }


        private void _ref_scene_OnPositionTileChanged2(int newPos)
        {
            numEndTile.Value = newPos;
            _ref_scene.OnPositionTileChanged -= this._ref_scene_OnPositionTileChanged2;
            this.Enabled = true;
            this.BringToFront();
        }

        private void btnAnnuler_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnGenerer_Click(object sender, EventArgs e)
        {
            if(cmbTypeAnime.SelectedItem == null)
            {
                 MessageBox.Show("Le type de mouvement est obligatoire !");
                return;
            }

            /*int val = (int)Math.Abs(numEndTile.Value - numStartTile.Value) * tile_sequence.Count  -  (int)(_max_frame - numFrameDebut.Value);

            if(val > 0)
            {
                MessageBox.Show("Le nombre de frames à générer pour la séquence en cours dépasse le nombre de frames maximum du calque, veuillez augmenter le nombre de frames maximum ou choisir une frame de début inférieur");
                MessageBox.Show("Il manque " + val + " frames");
                return;
            }*/

            if(tile_bascule == null)
            {
                MessageBox.Show("Vous devez définir une tile de bascule!");
                return;
            }

            int increment = 1;

            if(((ContainerType)cmbTypeAnime.SelectedItem).Value == (int)GeneratorMovement.LinearHorizontal)
            {

                if(numStartTile.Value > numEndTile.Value)
                    increment = -1;
            }
            else if (((ContainerType)cmbTypeAnime.SelectedItem).Value == (int)GeneratorMovement.LinearVertical)
            {
                increment = _ref_scene.ColCount;

                 if(numStartTile.Value > numEndTile.Value)
                    increment = -_ref_scene.ColCount;
            }


            int stoptile = (int)numEndTile.Value + increment;

            int cFrame = (int)numFrameDebut.Value;

            bool doBascule = false;
            int basculeval = 0;





            for (int i = (int)numStartTile.Value; i != stoptile; i += increment)
            {
                for (int j = basculeval; j < tile_sequence.Count; j++)
                {
                    if (cFrame > _max_frame)
                    {
                        MessageBox.Show("La génération à dépassée le nombre de frames maximum, impossible de générer toutes les frames demandées", "Nombre maximum de frame dépassé", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        break;
                    }


                    TileSequenceContainer cont = tile_sequence[j];

                    NativeFunc.editor_change_animated_tile(cont.tile_value, i, _ref_layer_id, cFrame-1);

                    if (_ref_scene.CurrentLayer.Frames[cFrame - 1].TileUpdates.Any(p => p.Index == i))
                        _ref_scene.CurrentLayer.Frames[cFrame - 1].TileUpdates.Find(p => p.Index == i).IdTile = cont.tile_value;
                    else
                    {
                        TileInfo ntinfo = new TileInfo();
                        ntinfo.IdTile = cont.tile_value;
                        ntinfo.Index = i;
                        _ref_scene.CurrentLayer.Frames[cFrame - 1].TileUpdates.Add(ntinfo);
                    }

                    if (cont == tile_bascule && i != (int)numEndTile.Value)
                    {
                        basculeval = 0;
                        doBascule = true;
                    }

                    if (doBascule)
                    {
                        TileSequenceContainer cont2 = tile_sequence[basculeval];

                        int basc_pos = i + increment;

                        NativeFunc.editor_change_animated_tile(cont2.tile_value, basc_pos, _ref_layer_id, cFrame - 1);

                        if (_ref_scene.CurrentLayer.Frames[cFrame - 1].TileUpdates.Any(p => p.Index == basc_pos))
                            _ref_scene.CurrentLayer.Frames[cFrame - 1].TileUpdates.Find(p => p.Index == basc_pos).IdTile = cont2.tile_value;
                        else
                        {
                            TileInfo ntinfo = new TileInfo();
                            ntinfo.IdTile = cont2.tile_value;
                            ntinfo.Index = basc_pos;
                            _ref_scene.CurrentLayer.Frames[cFrame - 1].TileUpdates.Add(ntinfo);
                        }


                        basculeval++;
                    }

                    cFrame++;
                }

                doBascule = false;
            }

            //
            MessageBox.Show("Génération effectuée!");


            this.Close();
        }

        private void btnAjoutTile_Click(object sender, EventArgs e)
        {
            this.Enabled = false;
            this.SendToBack();
            _ref_wrkbench.OnTilesetboxSelectionChanged += this._ref_wrkbench_OnTilesetboxSelectionChanged;
        }

        private void _ref_wrkbench_OnTilesetboxSelectionChanged(int newTile, int x, int y)
        {
            tile_sequence.Add(new TileSequenceContainer(newTile,x,y));

            if (tile_sequence.Count > 1)
            {
                trkPreview.Maximum++;
                trkPreview.Visible = true;
            }

            _ref_wrkbench.OnTilesetboxSelectionChanged -= this._ref_wrkbench_OnTilesetboxSelectionChanged;
            pbPreview.Invalidate();

            btnModifTile.Enabled = btnSuppressionTile.Enabled = true;

            this.Enabled = true;
            this.BringToFront();

            
        }

        private void btnSuppressionTile_Click(object sender, EventArgs e)
        {
            if (tile_sequence.Count == 0)
                return;

            tile_sequence.RemoveAt(trkPreview.Value);
            trkPreview.Maximum--;
            pbPreview.Invalidate();

            if (trkPreview.Maximum == 0)
            {
                trkPreview.Visible = false;
                btnModifTile.Enabled = btnSuppressionTile.Enabled = false;
            }
        }

        private void btnModifTile_Click(object sender, EventArgs e)
        {
            if (tile_sequence.Count == 0)
                return;

            this.Enabled = false;
            this.SendToBack();

            _ref_wrkbench.OnTilesetboxSelectionChanged += this._ref_wrkbench_OnTilesetboxSelectionChanged2;
           
        }

        private void _ref_wrkbench_OnTilesetboxSelectionChanged2(int newTile, int x, int y)
        {
            tile_sequence[trkPreview.Value].tile_value = newTile;
             tile_sequence[trkPreview.Value].x = x;
             tile_sequence[trkPreview.Value].y = y;
            _ref_wrkbench.OnTilesetboxSelectionChanged -= this._ref_wrkbench_OnTilesetboxSelectionChanged2;
            pbPreview.Invalidate();
            this.Enabled = true;
            this.BringToFront();
        }

        private void trkPreview_Scroll(object sender, EventArgs e)
        {
            pbPreview.Invalidate();
        }

        private void pbPreview_Paint(object sender, PaintEventArgs e)
        {
            if (tile_sequence.Count == 0)
                return;

            e.Graphics.InterpolationMode = InterpolationMode.NearestNeighbor;
            e.Graphics.SmoothingMode = SmoothingMode.None;

            //set the center of the pixel when drawing to 0.5 / 0.5 rather than the default 0 / 0 (default make the drawing start at -0.5)
            e.Graphics.PixelOffsetMode = PixelOffsetMode.Half;

            e.Graphics.DrawImage(
                _tilemap,
                new Rectangle(0, 0, pbPreview.Width, pbPreview.Height),
                // destination rectangle 
                tile_sequence[trkPreview.Value].x * pbPreview.Width,
                tile_sequence[trkPreview.Value].y * pbPreview.Height,           // upper-left corner of source rectangle
                pbPreview.Width,       // width of source rectangle
                pbPreview.Height,      // height of source rectangle
                GraphicsUnit.Pixel);
        }

        private void AnimationGenerator_Load(object sender, EventArgs e)
        {

        }

        private void btnBascule_Click(object sender, EventArgs e)
        {
            if (tile_sequence.Count != 0)
            {
                tile_bascule = tile_sequence[trkPreview.Value];
                pbTileBascule.Invalidate();
            }
        }

        private void pbTileBascule_Paint(object sender, PaintEventArgs e)
        {
            if (tile_bascule == null)
                return;

            e.Graphics.InterpolationMode = InterpolationMode.NearestNeighbor;
            e.Graphics.SmoothingMode = SmoothingMode.None;

            //set the center of the pixel when drawing to 0.5 / 0.5 rather than the default 0 / 0 (default make the drawing start at -0.5)
            e.Graphics.PixelOffsetMode = PixelOffsetMode.Half;

            e.Graphics.DrawImage(
                _tilemap,
                new Rectangle(0, 0, pbTileBascule.Width, pbTileBascule.Height),
                // destination rectangle 
                tile_bascule.x * pbTileBascule.Width,
                tile_bascule.y * pbTileBascule.Height,           // upper-left corner of source rectangle
                pbTileBascule.Width,       // width of source rectangle
                pbTileBascule.Height,      // height of source rectangle
                GraphicsUnit.Pixel);
        }




    }
}
