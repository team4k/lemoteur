﻿namespace WafaEditorV2
{
    partial class WorldCheckDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rtbCompare = new System.Windows.Forms.RichTextBox();
            this.btnConfirmChange = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // rtbCompare
            // 
            this.rtbCompare.Location = new System.Drawing.Point(12, 12);
            this.rtbCompare.Name = "rtbCompare";
            this.rtbCompare.Size = new System.Drawing.Size(716, 535);
            this.rtbCompare.TabIndex = 0;
            this.rtbCompare.Text = "";
            // 
            // btnConfirmChange
            // 
            this.btnConfirmChange.Location = new System.Drawing.Point(247, 556);
            this.btnConfirmChange.Name = "btnConfirmChange";
            this.btnConfirmChange.Size = new System.Drawing.Size(106, 23);
            this.btnConfirmChange.TabIndex = 1;
            this.btnConfirmChange.Text = "Confirmer Maj";
            this.btnConfirmChange.UseVisualStyleBackColor = true;
            this.btnConfirmChange.Click += new System.EventHandler(this.btnConfirmChange_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(359, 556);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Annuler";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // WorldCheckDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(740, 591);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnConfirmChange);
            this.Controls.Add(this.rtbCompare);
            this.Name = "WorldCheckDialog";
            this.Text = "Vérification doublon monde";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtbCompare;
        private System.Windows.Forms.Button btnConfirmChange;
        private System.Windows.Forms.Button btnCancel;
    }
}