﻿namespace WafaEditorV2
{
    partial class NewMap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOk = new System.Windows.Forms.Button();
            this.lblCols = new System.Windows.Forms.Label();
            this.numCols = new System.Windows.Forms.NumericUpDown();
            this.numRows = new System.Windows.Forms.NumericUpDown();
            this.lblRows = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblidmap = new System.Windows.Forms.Label();
            this.txtidmap = new System.Windows.Forms.TextBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.txtTileset = new System.Windows.Forms.TextBox();
            this.lblTileset = new System.Windows.Forms.Label();
            this.openTileSet = new System.Windows.Forms.OpenFileDialog();
            this.cmbSensAjoutCol = new System.Windows.Forms.ComboBox();
            this.cmbSensAjoutLigne = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numCols)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRows)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(74, 148);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(80, 22);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // lblCols
            // 
            this.lblCols.AutoSize = true;
            this.lblCols.Location = new System.Drawing.Point(13, 42);
            this.lblCols.Name = "lblCols";
            this.lblCols.Size = new System.Drawing.Size(105, 13);
            this.lblCols.TabIndex = 2;
            this.lblCols.Text = "Nombre de colonnes";
            // 
            // numCols
            // 
            this.numCols.Location = new System.Drawing.Point(128, 40);
            this.numCols.Maximum = new decimal(new int[] {
            600,
            0,
            0,
            0});
            this.numCols.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numCols.Name = "numCols";
            this.numCols.Size = new System.Drawing.Size(157, 20);
            this.numCols.TabIndex = 3;
            this.numCols.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // numRows
            // 
            this.numRows.Location = new System.Drawing.Point(127, 77);
            this.numRows.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.numRows.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numRows.Name = "numRows";
            this.numRows.Size = new System.Drawing.Size(158, 20);
            this.numRows.TabIndex = 5;
            this.numRows.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // lblRows
            // 
            this.lblRows.AutoSize = true;
            this.lblRows.Location = new System.Drawing.Point(13, 78);
            this.lblRows.Name = "lblRows";
            this.lblRows.Size = new System.Drawing.Size(89, 13);
            this.lblRows.TabIndex = 4;
            this.lblRows.Text = "Nombre de lignes";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(173, 148);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 22);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Annuler";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblidmap
            // 
            this.lblidmap.AutoSize = true;
            this.lblidmap.Location = new System.Drawing.Point(14, 12);
            this.lblidmap.Name = "lblidmap";
            this.lblidmap.Size = new System.Drawing.Size(76, 13);
            this.lblidmap.TabIndex = 7;
            this.lblidmap.Text = "Identifiant map";
            // 
            // txtidmap
            // 
            this.txtidmap.Location = new System.Drawing.Point(127, 9);
            this.txtidmap.Name = "txtidmap";
            this.txtidmap.Size = new System.Drawing.Size(158, 20);
            this.txtidmap.TabIndex = 8;
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(294, 114);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(80, 22);
            this.btnBrowse.TabIndex = 14;
            this.btnBrowse.Text = "Parcourir...";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // txtTileset
            // 
            this.txtTileset.Location = new System.Drawing.Point(127, 114);
            this.txtTileset.Name = "txtTileset";
            this.txtTileset.ReadOnly = true;
            this.txtTileset.Size = new System.Drawing.Size(158, 20);
            this.txtTileset.TabIndex = 13;
            // 
            // lblTileset
            // 
            this.lblTileset.AutoSize = true;
            this.lblTileset.Location = new System.Drawing.Point(13, 114);
            this.lblTileset.Name = "lblTileset";
            this.lblTileset.Size = new System.Drawing.Size(68, 13);
            this.lblTileset.TabIndex = 12;
            this.lblTileset.Text = "Fichier tileset";
            // 
            // openTileSet
            // 
            this.openTileSet.Filter = "PNG|*.png|Legacy|*.gal";
            // 
            // cmbSensAjoutCol
            // 
            this.cmbSensAjoutCol.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSensAjoutCol.Enabled = false;
            this.cmbSensAjoutCol.FormattingEnabled = true;
            this.cmbSensAjoutCol.Items.AddRange(new object[] {
            "Droite",
            "Gauche"});
            this.cmbSensAjoutCol.Location = new System.Drawing.Point(375, 39);
            this.cmbSensAjoutCol.Name = "cmbSensAjoutCol";
            this.cmbSensAjoutCol.Size = new System.Drawing.Size(109, 21);
            this.cmbSensAjoutCol.TabIndex = 15;
            // 
            // cmbSensAjoutLigne
            // 
            this.cmbSensAjoutLigne.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSensAjoutLigne.Enabled = false;
            this.cmbSensAjoutLigne.FormattingEnabled = true;
            this.cmbSensAjoutLigne.Items.AddRange(new object[] {
            "Haut",
            "Bas"});
            this.cmbSensAjoutLigne.Location = new System.Drawing.Point(375, 76);
            this.cmbSensAjoutLigne.Name = "cmbSensAjoutLigne";
            this.cmbSensAjoutLigne.Size = new System.Drawing.Size(109, 21);
            this.cmbSensAjoutLigne.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(307, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Sens Ajout";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(307, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "Sens Ajout";
            // 
            // NewMap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(553, 182);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbSensAjoutLigne);
            this.Controls.Add(this.cmbSensAjoutCol);
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.txtTileset);
            this.Controls.Add(this.lblTileset);
            this.Controls.Add(this.txtidmap);
            this.Controls.Add(this.lblidmap);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.numRows);
            this.Controls.Add(this.lblRows);
            this.Controls.Add(this.numCols);
            this.Controls.Add(this.lblCols);
            this.Controls.Add(this.btnOk);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NewMap";
            this.ShowIcon = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ajouter nouvelle map";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.numCols)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRows)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Label lblCols;
        private System.Windows.Forms.Label lblRows;
        private System.Windows.Forms.Button btnCancel;
        public System.Windows.Forms.NumericUpDown numCols;
        public System.Windows.Forms.NumericUpDown numRows;
        private System.Windows.Forms.Label lblidmap;
        public System.Windows.Forms.TextBox txtidmap;
        private System.Windows.Forms.Button btnBrowse;
        public System.Windows.Forms.TextBox txtTileset;
        private System.Windows.Forms.Label lblTileset;
        private System.Windows.Forms.OpenFileDialog openTileSet;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.ComboBox cmbSensAjoutCol;
        public System.Windows.Forms.ComboBox cmbSensAjoutLigne;
    }
}