﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PBInterface;
using System.IO;

namespace WafaEditorV2
{
    public partial class AjoutCalque : Form
    {

        private const int MAX_CALQUES_NORMAL = 32;
        public DialogMode mode { get; set; }

        public Map refMap;

        List<ContainerType> result;
        List<float> timestep = new List<float>();

        private List<ResxInfo> ref_game_resources;


        public AjoutCalque(Map pRefMap, List<ResxInfo> game_resources)
        {
            refMap = pRefMap;

            InitializeComponent();

            mode = DialogMode.Ajout;

            string[] types_calques = Enum.GetNames(typeof(PBInterface.TypeLayer));
            int[] types_calques_val =  (int[])Enum.GetValues(typeof(PBInterface.TypeLayer));

           result = new List<ContainerType>(types_calques.Length);

            for(int i = 0; i < types_calques.Length; i++)
                result.Add(new ContainerType(types_calques[i], types_calques_val[i]));


            cmbType.Items.AddRange(result.ToArray());
            cmbType.DisplayMember = "Key";
            cmbType.ValueMember = "Value";

            ref_game_resources = game_resources;


            timestep.Add(200);
            timestep.Add(200);

            cmbResxTextures.Items.AddRange(ref_game_resources.FindAll(p => p.type == ResxType.texture).ToArray());
            cmbResxTextures.DisplayMember = "ident";
            cmbResxTextures.ValueMember = "ident";
            cmbResxTextures.SelectedIndex = 0;

        }


        public void FillData(Layer oLayer)
        {
            timestep.Clear();
            mode = DialogMode.Modif;

            this.Tag = oLayer;
            txtidcalque.Text = oLayer.LayerId;
            chkDestroy.Checked = oLayer.Destructible;

            if (chkDestroy.Checked)
                numDestruct.Value = oLayer.DestructTileId;

            cmbType.SelectedItem = result.Find(p => p.Value == oLayer.Type);

            chkAnimated.Checked = oLayer.Animated;

            chktopmost.Checked = oLayer.Topmost;
            chkeditoronly.Checked = oLayer.EditorOnly;

            num_parallaxx.Value = (decimal)oLayer.ParallaxX;
            num_parallaxy.Value = (decimal)oLayer.ParallaxY;
            txtProgramShader.Text = oLayer.ShaderProgram;

            if (oLayer.Type == (int)TypeLayer.Normal)
            {
                //fix tileset id, use only the filename in the resources
                if (oLayer.TilesetId.Contains(Path.DirectorySeparatorChar))
                    oLayer.TilesetId = Path.GetFileName(oLayer.TilesetId);

                cmbResxTextures.SelectedItem = ref_game_resources.Find(p => p.ident == oLayer.TilesetId);
                chkrand.Checked = oLayer.RandomTiles;
            }

            this.Text = Editor.CalqueModif;

            EventArgs e = new EventArgs();
            cmbType_SelectedIndexChanged(this, e);
            chkDestroy_CheckedChanged(this, e);

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;

            Layer cLayer = null;

            if(mode == DialogMode.Ajout)
                cLayer = new Layer();
             else
                cLayer = (Layer)this.Tag;

            if (string.IsNullOrEmpty(txtidcalque.Text.Trim()))
            {
                MessageBox.Show("L'identifiant du calque est obligatoire !");
                return;
            }

            if(cmbType.SelectedItem == null)
            {
                 MessageBox.Show("Le type du calque est obligatoire !");
                return;
            }


            //vérification que seul un calque de collision existe
            if (((ContainerType)cmbType.SelectedItem).Value == (int)TypeLayer.Collisions)
            {
                if (mode == DialogMode.Ajout)
                {
                    bool num = refMap.LayerArrays.Any(p => p.Type == (int)TypeLayer.Collisions);

                    if (num)
                    {
                        MessageBox.Show("Un seul calque de type collision peut exister pour une map !");
                        return;
                    }
                }
                else if(mode == DialogMode.Modif)
                {

                    bool num = refMap.LayerArrays.Any(p => p != cLayer && p.Type == (int)TypeLayer.Collisions);

                    if (num)
                    {
                        MessageBox.Show("Un seul calque de type collision peut exister pour une map !");
                        return;
                    }
                }
            }

            //vérification que l'on en dépasse pas le nombre max de claques d'affichage
            if (((ContainerType)cmbType.SelectedItem).Value == (int)TypeLayer.Normal)
            {
                if (mode == DialogMode.Ajout)
                {
                    int num = refMap.LayerArrays.Count(p => p.Type == (int)TypeLayer.Normal);

                    if (num >= MAX_CALQUES_NORMAL)
                    {
                        MessageBox.Show("Le nombre max de calques de tiles autorisé est : " + MAX_CALQUES_NORMAL);
                        return;
                    }
                }
                else if (mode == DialogMode.Modif)
                {
                    int num = refMap.LayerArrays.Count(p => p != cLayer && p.Type == (int)TypeLayer.Normal);

                    if (num > MAX_CALQUES_NORMAL - 1)
                    {
                        MessageBox.Show("Le nombre max de calques de tiles autorisé est : " + MAX_CALQUES_NORMAL);
                        return;
                    }
                }
            }

            //vérification que seul un calque destructible existe
            if (((ContainerType)cmbType.SelectedItem).Value == (int)TypeLayer.Normal && chkDestroy.Checked)
            {
                if (mode == DialogMode.Ajout)
                {
                    int num = refMap.LayerArrays.Count(p => p.Destructible == true);

                    if (num > 0)
                    {
                        MessageBox.Show("Un seul calque destructible est autorisé par map !");
                        return;
                    }
                }
                else if (mode == DialogMode.Modif)
                {
                    int num = refMap.LayerArrays.Count(p => p != cLayer && p.Destructible == true);

                    if (num > 0)
                    {
                        MessageBox.Show("Un seul calque destructible est autorisé par map !");
                        return;
                    }
                }
            }

            //vérification qu'un seul calque est au premier plan
            if (chktopmost.Checked)
            {
                if (mode == DialogMode.Ajout)
                {
                    int num = refMap.LayerArrays.Count(p => p.Topmost == true);

                    if (num > 0)
                    {
                        MessageBox.Show("Un seull calque permier plan est autorisé par map !");
                        return;
                    }
                }
                else if (mode == DialogMode.Modif)
                {
                    int num = refMap.LayerArrays.Count(p => p != cLayer && p.Topmost == true);

                    if (num > 0)
                    {
                        MessageBox.Show("Un seul calque permier plan est autorisé par map !");
                        return;
                    }
                }
            }

            if(chktopmost.Checked && ((ContainerType)cmbType.SelectedItem).Value != (int)TypeLayer.Objects)
            {
                MessageBox.Show("Un calque permier plan doit obligatoirement être une calque objet !");
                return;
            }


            this.DialogResult = System.Windows.Forms.DialogResult.OK;

            cLayer.Type = ((ContainerType)cmbType.SelectedItem).Value;

            if (mode == DialogMode.Ajout)
            {
                cLayer.Order = refMap.LayerArrays.Count(p => (p.Type == (int)TypeLayer.Normal  || p.Type == (int)TypeLayer.Objects));
                cLayer.EditorOrder = refMap.LayerArrays.Count();
                cLayer.Visible = true;
                cLayer.Opacity = 1.0f;
            }

            cLayer.Animated = chkAnimated.Checked;

            cLayer.ParallaxX = (float)num_parallaxx.Value;
            cLayer.ParallaxY = (float)num_parallaxy.Value;

            cLayer.NoScrollX = (cLayer.ParallaxX == 0.0f);
            cLayer.NoScrollY = (cLayer.ParallaxY == 0.0f);

            cLayer.ShaderProgram = txtProgramShader.Text;
            



            if (((ContainerType)cmbType.SelectedItem).Value == (int)TypeLayer.Objects)
            {
                if (chktopmost.Checked)
                {
                    cLayer.Order = Properties.Settings.Default.MAX_ZORDER;
                }
                else if (cLayer.Topmost != chktopmost.Checked)
                {
                    cLayer.Order = refMap.LayerArrays.Count(p => (p.Type == (int)TypeLayer.Normal || p.Type == (int)TypeLayer.Objects));
                }

                
            }

            cLayer.Topmost = chktopmost.Checked;
            cLayer.EditorOnly = chkeditoronly.Checked;

            if (((ContainerType)cmbType.SelectedItem).Value == (int)TypeLayer.Normal)
            {
                if (chkDestroy.Checked && !cLayer.Destructible)
                {
                    cLayer.DestructTileId = Convert.ToInt32(numDestruct.Value);
                    NativeFunc.editor_setdestructible(cLayer.LayerId, cLayer.DestructTileId);
                }
                else if (!chkDestroy.Checked && cLayer.Destructible)
                {
                    cLayer.DestructTileId = 0;
                    NativeFunc.editor_cleardestructible();
                }

                cLayer.Destructible = chkDestroy.Checked;
                cLayer.RandomTiles = chkrand.Checked;
            }
           

            if (mode == DialogMode.Ajout)
            {
                cLayer.LayerId = txtidcalque.Text;

                int[] emptytiles = new int[refMap.ColCount * refMap.RowCount];
                //initialize tile values
                for (int i = 0; i < refMap.ColCount * refMap.RowCount; i++)
                    emptytiles[i] = 0;


                cLayer.TileArrays = emptytiles;

                refMap.LayerArrays.Add(cLayer);
            }

            MemoryStream layerStream = new MemoryStream();
            layerStream.Position = 0;
            ProtoBuf.Serializer.Serialize<Layer>(layerStream, cLayer);

            layerStream.Position = 0;

            byte[] tmplayerArray = layerStream.ToArray();
            layerStream.Close();


            if (cLayer.Type == (int)TypeLayer.Normal)
            {
                if (!cLayer.Animated)
                {
                    if(cLayer.Frames != null)
                        cLayer.Frames.Clear();
                }

                if(cmbResxTextures.SelectedItem != null)
                {
                    cLayer.TilesetId = ((ResxInfo)cmbResxTextures.SelectedItem).ident;
                }
                
            }

            if (cLayer.Type != (int)TypeLayer.Objects)
            {
                if (mode == DialogMode.Ajout)
                    NativeFunc.editor_addlayer(tmplayerArray, (uint)tmplayerArray.Length);
                else if (mode == DialogMode.Modif)
                    NativeFunc.editor_update_layer(tmplayerArray, (uint)tmplayerArray.Length);
            }


            if (mode == DialogMode.Modif)
            {
                if (cLayer.LayerId != txtidcalque.Text)
                {
                    if(cLayer.Type == (int)TypeLayer.Normal)
                        NativeFunc.editor_changelayer_id(cLayer.LayerId, txtidcalque.Text);

                    cLayer.LayerId = txtidcalque.Text;
                }
            }

            this.Tag = cLayer;

            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
         
        private void cmbType_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblrand.Enabled = chkrand.Enabled = lblDestroy.Enabled = chkDestroy.Enabled = lblAnimated.Enabled = chkAnimated.Enabled = num_parallaxx.Enabled = num_parallaxy.Enabled = chkDestroy.Enabled = lbl_parallaxx.Enabled = lbl_parallaxy.Enabled = lblProgShader.Enabled = txtProgramShader.Enabled = cmbResxTextures.Enabled =  (((ContainerType)cmbType.SelectedItem).Value == (int)TypeLayer.Normal);

            if (((ContainerType)cmbType.SelectedItem).Value != (int)TypeLayer.Normal)
            {
                if (chkDestroy.Checked)
                {
                    chkDestroy.Checked = false;
                    chkDestroy_CheckedChanged(this, e);
                }

                if (chkAnimated.Checked)
                {
                    chkAnimated.Checked = false;
                }

                if(chkrand.Checked)
                {
                    chkrand.Checked = false;
                }

            }

            chktopmost.Enabled = lbltopmost.Enabled = chkeditoronly.Enabled = lbleditoronly.Enabled = (((ContainerType)cmbType.SelectedItem).Value == (int)TypeLayer.Objects);

        }

        private void chkDestroy_CheckedChanged(object sender, EventArgs e)
        {
            numDestruct.Enabled = lblIdDestruct.Enabled = chkDestroy.Checked;
        }
    }
}
