﻿namespace WafaEditorV2
{
    partial class Fusion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tvFusion = new System.Windows.Forms.TreeView();
            this.btnFusion = new System.Windows.Forms.Button();
            this.ofdFusion = new System.Windows.Forms.OpenFileDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tvFusion
            // 
            this.tvFusion.Location = new System.Drawing.Point(16, 15);
            this.tvFusion.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tvFusion.Name = "tvFusion";
            this.tvFusion.Size = new System.Drawing.Size(641, 506);
            this.tvFusion.TabIndex = 0;
            this.tvFusion.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvFusion_AfterSelect);
            this.tvFusion.DoubleClick += new System.EventHandler(this.tvFusion_DoubleClick);
            // 
            // btnFusion
            // 
            this.btnFusion.Location = new System.Drawing.Point(16, 618);
            this.btnFusion.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnFusion.Name = "btnFusion";
            this.btnFusion.Size = new System.Drawing.Size(192, 28);
            this.btnFusion.TabIndex = 1;
            this.btnFusion.Text = "Fichier niveau à importer...";
            this.btnFusion.UseVisualStyleBackColor = true;
            this.btnFusion.Click += new System.EventHandler(this.btnFusion_Click);
            // 
            // ofdFusion
            // 
            this.ofdFusion.Filter = "Fichier niveau|*.lwf";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 545);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Légende: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.LightGreen;
            this.label2.Location = new System.Drawing.Point(139, 545);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(140, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "égal à l\'objet existant";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.LightSalmon;
            this.label3.Location = new System.Drawing.Point(293, 545);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(173, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "différent de l\'objet existant";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.LightBlue;
            this.label4.Location = new System.Drawing.Point(481, 545);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(123, 17);
            this.label4.TabIndex = 5;
            this.label4.Text = "l\'objet n\'existe pas";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(299, 618);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(276, 28);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "Sauvegarder et recharger niveau";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // Fusion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(687, 726);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnFusion);
            this.Controls.Add(this.tvFusion);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Fusion";
            this.Text = "Fusion";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView tvFusion;
        private System.Windows.Forms.Button btnFusion;
        private System.Windows.Forms.OpenFileDialog ofdFusion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnSave;
    }
}