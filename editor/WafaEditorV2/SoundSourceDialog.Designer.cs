﻿namespace WafaEditorV2
{
    partial class SoundSourceDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sound_id = new System.Windows.Forms.Label();
            this.txtidson = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbSon = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.numFalloff = new System.Windows.Forms.NumericUpDown();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnAnnuler = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numFalloff)).BeginInit();
            this.SuspendLayout();
            // 
            // sound_id
            // 
            this.sound_id.AutoSize = true;
            this.sound_id.Location = new System.Drawing.Point(32, 52);
            this.sound_id.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.sound_id.Name = "sound_id";
            this.sound_id.Size = new System.Drawing.Size(46, 17);
            this.sound_id.TabIndex = 0;
            this.sound_id.Text = "Id son";
            // 
            // txtidson
            // 
            this.txtidson.Location = new System.Drawing.Point(147, 48);
            this.txtidson.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtidson.Name = "txtidson";
            this.txtidson.Size = new System.Drawing.Size(224, 23);
            this.txtidson.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 87);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Fichier son";
            // 
            // cmbSon
            // 
            this.cmbSon.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSon.FormattingEnabled = true;
            this.cmbSon.Location = new System.Drawing.Point(147, 87);
            this.cmbSon.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbSon.Name = "cmbSon";
            this.cmbSon.Size = new System.Drawing.Size(224, 25);
            this.cmbSon.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(27, 139);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 17);
            this.label6.TabIndex = 10;
            this.label6.Text = "Distance effet";
            // 
            // numFalloff
            // 
            this.numFalloff.DecimalPlaces = 2;
            this.numFalloff.Location = new System.Drawing.Point(147, 130);
            this.numFalloff.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.numFalloff.Maximum = new decimal(new int[] {
            3000000,
            0,
            0,
            0});
            this.numFalloff.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numFalloff.Name = "numFalloff";
            this.numFalloff.Size = new System.Drawing.Size(64, 23);
            this.numFalloff.TabIndex = 11;
            this.numFalloff.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(77, 190);
            this.btnOK.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(100, 28);
            this.btnOK.TabIndex = 12;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnAnnuler
            // 
            this.btnAnnuler.Location = new System.Drawing.Point(185, 190);
            this.btnAnnuler.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAnnuler.Name = "btnAnnuler";
            this.btnAnnuler.Size = new System.Drawing.Size(100, 28);
            this.btnAnnuler.TabIndex = 13;
            this.btnAnnuler.Text = "Annuler";
            this.btnAnnuler.UseVisualStyleBackColor = true;
            this.btnAnnuler.Click += new System.EventHandler(this.btnAnnuler_Click);
            // 
            // SoundSourceDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(405, 239);
            this.Controls.Add(this.btnAnnuler);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.numFalloff);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cmbSon);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtidson);
            this.Controls.Add(this.sound_id);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SoundSourceDialog";
            this.ShowIcon = false;
            this.Text = "Ajouter une source sonore";
            ((System.ComponentModel.ISupportInitialize)(this.numFalloff)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label sound_id;
        private System.Windows.Forms.TextBox txtidson;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbSon;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numFalloff;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnAnnuler;
    }
}