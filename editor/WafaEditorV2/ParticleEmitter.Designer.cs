﻿namespace WafaEditorV2
{
    partial class ParticleEmitter
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblRate = new System.Windows.Forms.Label();
            this.numrate = new System.Windows.Forms.NumericUpDown();
            this.lblColor = new System.Windows.Forms.Label();
            this.btnColor = new System.Windows.Forms.Button();
            this.colorBack = new System.Windows.Forms.ColorDialog();
            this.numwidth = new System.Windows.Forms.NumericUpDown();
            this.lblwidth = new System.Windows.Forms.Label();
            this.numHeight = new System.Windows.Forms.NumericUpDown();
            this.lblheight = new System.Windows.Forms.Label();
            this.numapha = new System.Windows.Forms.NumericUpDown();
            this.lblalpha = new System.Windows.Forms.Label();
            this.numLifetime = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.NumMaxParticles = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.numTotalParticles = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.numoffsetX = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.numoffsetY = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.numDirX = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.numDirY = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.numSpeed = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numrate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numwidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numapha)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLifetime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumMaxParticles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTotalParticles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numoffsetX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numoffsetY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDirX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDirY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSpeed)).BeginInit();
            this.SuspendLayout();
            // 
            // lblRate
            // 
            this.lblRate.AutoSize = true;
            this.lblRate.Location = new System.Drawing.Point(26, 21);
            this.lblRate.Name = "lblRate";
            this.lblRate.Size = new System.Drawing.Size(80, 13);
            this.lblRate.TabIndex = 0;
            this.lblRate.Text = "Débit particules";
            // 
            // numrate
            // 
            this.numrate.Location = new System.Drawing.Point(156, 19);
            this.numrate.Maximum = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.numrate.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numrate.Name = "numrate";
            this.numrate.Size = new System.Drawing.Size(120, 20);
            this.numrate.TabIndex = 1;
            this.numrate.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblColor
            // 
            this.lblColor.AutoSize = true;
            this.lblColor.Location = new System.Drawing.Point(26, 62);
            this.lblColor.Name = "lblColor";
            this.lblColor.Size = new System.Drawing.Size(91, 13);
            this.lblColor.TabIndex = 2;
            this.lblColor.Text = "Couleur particules";
            // 
            // btnColor
            // 
            this.btnColor.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnColor.Location = new System.Drawing.Point(189, 62);
            this.btnColor.Name = "btnColor";
            this.btnColor.Size = new System.Drawing.Size(22, 23);
            this.btnColor.TabIndex = 41;
            this.btnColor.UseVisualStyleBackColor = false;
            // 
            // numwidth
            // 
            this.numwidth.Location = new System.Drawing.Point(156, 123);
            this.numwidth.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numwidth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numwidth.Name = "numwidth";
            this.numwidth.Size = new System.Drawing.Size(120, 20);
            this.numwidth.TabIndex = 43;
            this.numwidth.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblwidth
            // 
            this.lblwidth.AutoSize = true;
            this.lblwidth.Location = new System.Drawing.Point(26, 125);
            this.lblwidth.Name = "lblwidth";
            this.lblwidth.Size = new System.Drawing.Size(91, 13);
            this.lblwidth.TabIndex = 42;
            this.lblwidth.Text = "Largeur particules";
            // 
            // numHeight
            // 
            this.numHeight.Location = new System.Drawing.Point(156, 149);
            this.numHeight.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numHeight.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numHeight.Name = "numHeight";
            this.numHeight.Size = new System.Drawing.Size(120, 20);
            this.numHeight.TabIndex = 45;
            this.numHeight.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblheight
            // 
            this.lblheight.AutoSize = true;
            this.lblheight.Location = new System.Drawing.Point(24, 149);
            this.lblheight.Name = "lblheight";
            this.lblheight.Size = new System.Drawing.Size(93, 13);
            this.lblheight.TabIndex = 44;
            this.lblheight.Text = "Hauteur particules";
            // 
            // numapha
            // 
            this.numapha.Location = new System.Drawing.Point(156, 97);
            this.numapha.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numapha.Name = "numapha";
            this.numapha.Size = new System.Drawing.Size(120, 20);
            this.numapha.TabIndex = 47;
            this.numapha.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // lblalpha
            // 
            this.lblalpha.AutoSize = true;
            this.lblalpha.Location = new System.Drawing.Point(26, 99);
            this.lblalpha.Name = "lblalpha";
            this.lblalpha.Size = new System.Drawing.Size(82, 13);
            this.lblalpha.TabIndex = 46;
            this.lblalpha.Text = "Alpha particules";
            // 
            // numLifetime
            // 
            this.numLifetime.DecimalPlaces = 2;
            this.numLifetime.Location = new System.Drawing.Point(156, 175);
            this.numLifetime.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numLifetime.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numLifetime.Name = "numLifetime";
            this.numLifetime.Size = new System.Drawing.Size(120, 20);
            this.numLifetime.TabIndex = 49;
            this.numLifetime.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 177);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 13);
            this.label1.TabIndex = 48;
            this.label1.Text = "temps survie particules";
            // 
            // NumMaxParticles
            // 
            this.NumMaxParticles.Location = new System.Drawing.Point(156, 201);
            this.NumMaxParticles.Maximum = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.NumMaxParticles.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.NumMaxParticles.Name = "NumMaxParticles";
            this.NumMaxParticles.Size = new System.Drawing.Size(120, 20);
            this.NumMaxParticles.TabIndex = 51;
            this.NumMaxParticles.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 203);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(131, 13);
            this.label2.TabIndex = 50;
            this.label2.Text = "Maximum particules visible";
            // 
            // numTotalParticles
            // 
            this.numTotalParticles.Location = new System.Drawing.Point(156, 227);
            this.numTotalParticles.Maximum = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.numTotalParticles.Minimum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.numTotalParticles.Name = "numTotalParticles";
            this.numTotalParticles.Size = new System.Drawing.Size(120, 20);
            this.numTotalParticles.TabIndex = 53;
            this.numTotalParticles.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 229);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(142, 13);
            this.label3.TabIndex = 52;
            this.label3.Text = "Maximum particules émission";
            // 
            // numoffsetX
            // 
            this.numoffsetX.Location = new System.Drawing.Point(380, 19);
            this.numoffsetX.Maximum = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.numoffsetX.Minimum = new decimal(new int[] {
            50000,
            0,
            0,
            -2147483648});
            this.numoffsetX.Name = "numoffsetX";
            this.numoffsetX.Size = new System.Drawing.Size(120, 20);
            this.numoffsetX.TabIndex = 55;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(309, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 54;
            this.label4.Text = "offset X";
            // 
            // numoffsetY
            // 
            this.numoffsetY.Location = new System.Drawing.Point(380, 45);
            this.numoffsetY.Maximum = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.numoffsetY.Minimum = new decimal(new int[] {
            50000,
            0,
            0,
            -2147483648});
            this.numoffsetY.Name = "numoffsetY";
            this.numoffsetY.Size = new System.Drawing.Size(120, 20);
            this.numoffsetY.TabIndex = 57;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(309, 52);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 56;
            this.label5.Text = "offset Y";
            // 
            // numDirX
            // 
            this.numDirX.DecimalPlaces = 4;
            this.numDirX.Location = new System.Drawing.Point(380, 71);
            this.numDirX.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numDirX.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.numDirX.Name = "numDirX";
            this.numDirX.Size = new System.Drawing.Size(120, 20);
            this.numDirX.TabIndex = 59;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(309, 78);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 13);
            this.label6.TabIndex = 58;
            this.label6.Text = "Direction X";
            // 
            // numDirY
            // 
            this.numDirY.DecimalPlaces = 4;
            this.numDirY.Location = new System.Drawing.Point(380, 97);
            this.numDirY.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numDirY.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.numDirY.Name = "numDirY";
            this.numDirY.Size = new System.Drawing.Size(120, 20);
            this.numDirY.TabIndex = 61;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(309, 104);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 13);
            this.label7.TabIndex = 60;
            this.label7.Text = "Direction Y";
            // 
            // numSpeed
            // 
            this.numSpeed.Location = new System.Drawing.Point(156, 253);
            this.numSpeed.Maximum = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.numSpeed.Name = "numSpeed";
            this.numSpeed.Size = new System.Drawing.Size(120, 20);
            this.numSpeed.TabIndex = 63;
            this.numSpeed.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 255);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(84, 13);
            this.label8.TabIndex = 62;
            this.label8.Text = "Vitesse émission";
            // 
            // ParticleEmitter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.numSpeed);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.numDirY);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.numDirX);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.numoffsetY);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.numoffsetX);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.numTotalParticles);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.NumMaxParticles);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.numLifetime);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.numapha);
            this.Controls.Add(this.lblalpha);
            this.Controls.Add(this.numHeight);
            this.Controls.Add(this.lblheight);
            this.Controls.Add(this.numwidth);
            this.Controls.Add(this.lblwidth);
            this.Controls.Add(this.btnColor);
            this.Controls.Add(this.lblColor);
            this.Controls.Add(this.numrate);
            this.Controls.Add(this.lblRate);
            this.Name = "ParticleEmitter";
            this.Size = new System.Drawing.Size(510, 303);
            ((System.ComponentModel.ISupportInitialize)(this.numrate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numwidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numapha)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLifetime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumMaxParticles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTotalParticles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numoffsetX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numoffsetY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDirX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDirY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSpeed)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblRate;
        private System.Windows.Forms.NumericUpDown numrate;
        private System.Windows.Forms.Label lblColor;
        private System.Windows.Forms.Button btnColor;
        private System.Windows.Forms.ColorDialog colorBack;
        private System.Windows.Forms.NumericUpDown numwidth;
        private System.Windows.Forms.Label lblwidth;
        private System.Windows.Forms.NumericUpDown numHeight;
        private System.Windows.Forms.Label lblheight;
        private System.Windows.Forms.NumericUpDown numapha;
        private System.Windows.Forms.Label lblalpha;
        private System.Windows.Forms.NumericUpDown numLifetime;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown NumMaxParticles;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numTotalParticles;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numoffsetX;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown numoffsetY;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown numDirX;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numDirY;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown numSpeed;
        private System.Windows.Forms.Label label8;
    }
}
