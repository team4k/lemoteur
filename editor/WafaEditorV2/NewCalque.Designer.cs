﻿namespace WafaEditorV2
{
    partial class AjoutCalque
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtidcalque = new System.Windows.Forms.TextBox();
            this.lblidcalque = new System.Windows.Forms.Label();
            this.lblType = new System.Windows.Forms.Label();
            this.cmbType = new System.Windows.Forms.ComboBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.lblDestroy = new System.Windows.Forms.Label();
            this.chkDestroy = new System.Windows.Forms.CheckBox();
            this.lblIdDestruct = new System.Windows.Forms.Label();
            this.numDestruct = new System.Windows.Forms.NumericUpDown();
            this.chkAnimated = new System.Windows.Forms.CheckBox();
            this.lblAnimated = new System.Windows.Forms.Label();
            this.lbl_parallaxx = new System.Windows.Forms.Label();
            this.lbl_parallaxy = new System.Windows.Forms.Label();
            this.num_parallaxx = new System.Windows.Forms.NumericUpDown();
            this.num_parallaxy = new System.Windows.Forms.NumericUpDown();
            this.txtProgramShader = new System.Windows.Forms.TextBox();
            this.lblProgShader = new System.Windows.Forms.Label();
            this.lblTileset = new System.Windows.Forms.Label();
            this.cmbResxTextures = new System.Windows.Forms.ComboBox();
            this.chktopmost = new System.Windows.Forms.CheckBox();
            this.lbltopmost = new System.Windows.Forms.Label();
            this.chkeditoronly = new System.Windows.Forms.CheckBox();
            this.lbleditoronly = new System.Windows.Forms.Label();
            this.chkrand = new System.Windows.Forms.CheckBox();
            this.lblrand = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numDestruct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_parallaxx)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_parallaxy)).BeginInit();
            this.SuspendLayout();
            // 
            // txtidcalque
            // 
            this.txtidcalque.Location = new System.Drawing.Point(188, 12);
            this.txtidcalque.Name = "txtidcalque";
            this.txtidcalque.Size = new System.Drawing.Size(158, 20);
            this.txtidcalque.TabIndex = 10;
            // 
            // lblidcalque
            // 
            this.lblidcalque.AutoSize = true;
            this.lblidcalque.Location = new System.Drawing.Point(117, 15);
            this.lblidcalque.Name = "lblidcalque";
            this.lblidcalque.Size = new System.Drawing.Size(64, 13);
            this.lblidcalque.TabIndex = 9;
            this.lblidcalque.Text = "Nom calque";
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(114, 56);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(66, 13);
            this.lblType.TabIndex = 11;
            this.lblType.Text = "Type calque";
            // 
            // cmbType
            // 
            this.cmbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbType.FormattingEnabled = true;
            this.cmbType.Location = new System.Drawing.Point(188, 56);
            this.cmbType.Name = "cmbType";
            this.cmbType.Size = new System.Drawing.Size(158, 21);
            this.cmbType.TabIndex = 12;
            this.cmbType.SelectedIndexChanged += new System.EventHandler(this.cmbType_SelectedIndexChanged);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(159, 457);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 22);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "Annuler";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(60, 457);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(80, 22);
            this.btnOk.TabIndex = 13;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // lblDestroy
            // 
            this.lblDestroy.AutoSize = true;
            this.lblDestroy.Enabled = false;
            this.lblDestroy.Location = new System.Drawing.Point(119, 270);
            this.lblDestroy.Name = "lblDestroy";
            this.lblDestroy.Size = new System.Drawing.Size(63, 13);
            this.lblDestroy.TabIndex = 15;
            this.lblDestroy.Text = "Destructible";
            // 
            // chkDestroy
            // 
            this.chkDestroy.AutoSize = true;
            this.chkDestroy.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkDestroy.Enabled = false;
            this.chkDestroy.Location = new System.Drawing.Point(192, 270);
            this.chkDestroy.Name = "chkDestroy";
            this.chkDestroy.Size = new System.Drawing.Size(15, 14);
            this.chkDestroy.TabIndex = 16;
            this.chkDestroy.UseVisualStyleBackColor = true;
            this.chkDestroy.CheckedChanged += new System.EventHandler(this.chkDestroy_CheckedChanged);
            // 
            // lblIdDestruct
            // 
            this.lblIdDestruct.AutoSize = true;
            this.lblIdDestruct.Enabled = false;
            this.lblIdDestruct.Location = new System.Drawing.Point(70, 308);
            this.lblIdDestruct.Name = "lblIdDestruct";
            this.lblIdDestruct.Size = new System.Drawing.Size(113, 13);
            this.lblIdDestruct.TabIndex = 17;
            this.lblIdDestruct.Text = "N° tile post destruction";
            // 
            // numDestruct
            // 
            this.numDestruct.Enabled = false;
            this.numDestruct.Location = new System.Drawing.Point(192, 308);
            this.numDestruct.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numDestruct.Name = "numDestruct";
            this.numDestruct.Size = new System.Drawing.Size(128, 20);
            this.numDestruct.TabIndex = 18;
            // 
            // chkAnimated
            // 
            this.chkAnimated.AutoSize = true;
            this.chkAnimated.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkAnimated.Enabled = false;
            this.chkAnimated.Location = new System.Drawing.Point(192, 341);
            this.chkAnimated.Name = "chkAnimated";
            this.chkAnimated.Size = new System.Drawing.Size(15, 14);
            this.chkAnimated.TabIndex = 20;
            this.chkAnimated.UseVisualStyleBackColor = true;
            // 
            // lblAnimated
            // 
            this.lblAnimated.AutoSize = true;
            this.lblAnimated.Enabled = false;
            this.lblAnimated.Location = new System.Drawing.Point(146, 341);
            this.lblAnimated.Name = "lblAnimated";
            this.lblAnimated.Size = new System.Drawing.Size(36, 13);
            this.lblAnimated.TabIndex = 19;
            this.lblAnimated.Text = "Animé";
            // 
            // lbl_parallaxx
            // 
            this.lbl_parallaxx.AutoSize = true;
            this.lbl_parallaxx.Enabled = false;
            this.lbl_parallaxx.Location = new System.Drawing.Point(107, 182);
            this.lbl_parallaxx.Name = "lbl_parallaxx";
            this.lbl_parallaxx.Size = new System.Drawing.Size(75, 13);
            this.lbl_parallaxx.TabIndex = 27;
            this.lbl_parallaxx.Text = "Parallaxe en X";
            // 
            // lbl_parallaxy
            // 
            this.lbl_parallaxy.AutoSize = true;
            this.lbl_parallaxy.Enabled = false;
            this.lbl_parallaxy.Location = new System.Drawing.Point(107, 229);
            this.lbl_parallaxy.Name = "lbl_parallaxy";
            this.lbl_parallaxy.Size = new System.Drawing.Size(75, 13);
            this.lbl_parallaxy.TabIndex = 28;
            this.lbl_parallaxy.Text = "Parallaxe en Y";
            // 
            // num_parallaxx
            // 
            this.num_parallaxx.DecimalPlaces = 2;
            this.num_parallaxx.Enabled = false;
            this.num_parallaxx.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.num_parallaxx.Location = new System.Drawing.Point(192, 180);
            this.num_parallaxx.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.num_parallaxx.Name = "num_parallaxx";
            this.num_parallaxx.Size = new System.Drawing.Size(128, 20);
            this.num_parallaxx.TabIndex = 29;
            this.num_parallaxx.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // num_parallaxy
            // 
            this.num_parallaxy.DecimalPlaces = 2;
            this.num_parallaxy.Enabled = false;
            this.num_parallaxy.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.num_parallaxy.Location = new System.Drawing.Point(192, 229);
            this.num_parallaxy.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.num_parallaxy.Name = "num_parallaxy";
            this.num_parallaxy.Size = new System.Drawing.Size(128, 20);
            this.num_parallaxy.TabIndex = 30;
            this.num_parallaxy.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // txtProgramShader
            // 
            this.txtProgramShader.Location = new System.Drawing.Point(190, 142);
            this.txtProgramShader.Name = "txtProgramShader";
            this.txtProgramShader.Size = new System.Drawing.Size(158, 20);
            this.txtProgramShader.TabIndex = 72;
            // 
            // lblProgShader
            // 
            this.lblProgShader.AutoSize = true;
            this.lblProgShader.Location = new System.Drawing.Point(88, 145);
            this.lblProgShader.Name = "lblProgShader";
            this.lblProgShader.Size = new System.Drawing.Size(95, 13);
            this.lblProgShader.TabIndex = 73;
            this.lblProgShader.Text = "Programme shader";
            // 
            // lblTileset
            // 
            this.lblTileset.AutoSize = true;
            this.lblTileset.Location = new System.Drawing.Point(116, 365);
            this.lblTileset.Name = "lblTileset";
            this.lblTileset.Size = new System.Drawing.Size(68, 13);
            this.lblTileset.TabIndex = 74;
            this.lblTileset.Text = "Fichier tileset";
            // 
            // cmbResxTextures
            // 
            this.cmbResxTextures.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbResxTextures.FormattingEnabled = true;
            this.cmbResxTextures.Location = new System.Drawing.Point(192, 365);
            this.cmbResxTextures.Name = "cmbResxTextures";
            this.cmbResxTextures.Size = new System.Drawing.Size(158, 21);
            this.cmbResxTextures.TabIndex = 75;
            // 
            // chktopmost
            // 
            this.chktopmost.AutoSize = true;
            this.chktopmost.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chktopmost.Enabled = false;
            this.chktopmost.Location = new System.Drawing.Point(190, 93);
            this.chktopmost.Name = "chktopmost";
            this.chktopmost.Size = new System.Drawing.Size(15, 14);
            this.chktopmost.TabIndex = 77;
            this.chktopmost.UseVisualStyleBackColor = true;
            // 
            // lbltopmost
            // 
            this.lbltopmost.AutoSize = true;
            this.lbltopmost.Enabled = false;
            this.lbltopmost.Location = new System.Drawing.Point(115, 94);
            this.lbltopmost.Name = "lbltopmost";
            this.lbltopmost.Size = new System.Drawing.Size(65, 13);
            this.lbltopmost.TabIndex = 76;
            this.lbltopmost.Text = "Premier plan";
            // 
            // chkeditoronly
            // 
            this.chkeditoronly.AutoSize = true;
            this.chkeditoronly.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkeditoronly.Enabled = false;
            this.chkeditoronly.Location = new System.Drawing.Point(190, 117);
            this.chkeditoronly.Name = "chkeditoronly";
            this.chkeditoronly.Size = new System.Drawing.Size(15, 14);
            this.chkeditoronly.TabIndex = 79;
            this.chkeditoronly.UseVisualStyleBackColor = true;
            // 
            // lbleditoronly
            // 
            this.lbleditoronly.AutoSize = true;
            this.lbleditoronly.Enabled = false;
            this.lbleditoronly.Location = new System.Drawing.Point(31, 117);
            this.lbleditoronly.Name = "lbleditoronly";
            this.lbleditoronly.Size = new System.Drawing.Size(153, 13);
            this.lbleditoronly.TabIndex = 78;
            this.lbleditoronly.Text = "Visible seulement dans l\'éditeur";
            // 
            // chkrand
            // 
            this.chkrand.AutoSize = true;
            this.chkrand.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkrand.Enabled = false;
            this.chkrand.Location = new System.Drawing.Point(192, 401);
            this.chkrand.Name = "chkrand";
            this.chkrand.Size = new System.Drawing.Size(15, 14);
            this.chkrand.TabIndex = 81;
            this.chkrand.UseVisualStyleBackColor = true;
            // 
            // lblrand
            // 
            this.lblrand.AutoSize = true;
            this.lblrand.Enabled = false;
            this.lblrand.Location = new System.Drawing.Point(2, 398);
            this.lblrand.Name = "lblrand";
            this.lblrand.Size = new System.Drawing.Size(188, 26);
            this.lblrand.TabIndex = 80;
            this.lblrand.Text = "Tuiles aléatoires \r\n(nécessite une fonction de génération)";
            this.lblrand.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // AjoutCalque
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(397, 491);
            this.Controls.Add(this.chkrand);
            this.Controls.Add(this.lblrand);
            this.Controls.Add(this.chkeditoronly);
            this.Controls.Add(this.lbleditoronly);
            this.Controls.Add(this.chktopmost);
            this.Controls.Add(this.lbltopmost);
            this.Controls.Add(this.lblTileset);
            this.Controls.Add(this.cmbResxTextures);
            this.Controls.Add(this.txtProgramShader);
            this.Controls.Add(this.lblProgShader);
            this.Controls.Add(this.num_parallaxy);
            this.Controls.Add(this.num_parallaxx);
            this.Controls.Add(this.lbl_parallaxy);
            this.Controls.Add(this.lbl_parallaxx);
            this.Controls.Add(this.chkAnimated);
            this.Controls.Add(this.lblAnimated);
            this.Controls.Add(this.numDestruct);
            this.Controls.Add(this.lblIdDestruct);
            this.Controls.Add(this.chkDestroy);
            this.Controls.Add(this.lblDestroy);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.cmbType);
            this.Controls.Add(this.lblType);
            this.Controls.Add(this.txtidcalque);
            this.Controls.Add(this.lblidcalque);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AjoutCalque";
            this.ShowIcon = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ajouter Calque";
            ((System.ComponentModel.ISupportInitialize)(this.numDestruct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_parallaxx)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_parallaxy)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox txtidcalque;
        private System.Windows.Forms.Label lblidcalque;
        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOk;
        public System.Windows.Forms.ComboBox cmbType;
        private System.Windows.Forms.Label lblDestroy;
        public System.Windows.Forms.CheckBox chkDestroy;
        private System.Windows.Forms.Label lblIdDestruct;
        public System.Windows.Forms.NumericUpDown numDestruct;
        public System.Windows.Forms.CheckBox chkAnimated;
        private System.Windows.Forms.Label lblAnimated;
        private System.Windows.Forms.Label lbl_parallaxx;
        private System.Windows.Forms.Label lbl_parallaxy;
        public System.Windows.Forms.NumericUpDown num_parallaxx;
        public System.Windows.Forms.NumericUpDown num_parallaxy;
        private System.Windows.Forms.TextBox txtProgramShader;
        private System.Windows.Forms.Label lblProgShader;
        private System.Windows.Forms.Label lblTileset;
        private System.Windows.Forms.ComboBox cmbResxTextures;
        public System.Windows.Forms.CheckBox chktopmost;
        private System.Windows.Forms.Label lbltopmost;
        public System.Windows.Forms.CheckBox chkeditoronly;
        private System.Windows.Forms.Label lbleditoronly;
        public System.Windows.Forms.CheckBox chkrand;
        private System.Windows.Forms.Label lblrand;
    }
}