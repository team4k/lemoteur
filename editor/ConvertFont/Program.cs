﻿//convert level files using old font type in .ttf format to new msdf format using json config + png image file

using System;
using System.IO;
using System.Text;
using ProtoBuf;
using PBInterface;

//compile with csc /reference:..\editor\WafaEditorV2\packages\System.Memory.4.5.4\lib\net461\System.Memory.dll /reference:..\editor\WafaEditorV2\packages\protobuf-net.Core.3.0.101\lib\net461\protobuf-net.Core.dll /reference:..\editor\WafaEditorV2\packages\protobuf-net.3.0.101\lib\net461\protobuf-net.dll ..\editor\WafaEditorV2\Level.cs convert_font_type.cs -platform:x86

class FontConverter
{
    public static void Main(string[] args)
    {
        if (args.Length == 0)
        {
            Console.WriteLine("No Path to .lwf files to convert, abort...");
            return;
        }

        string levelspath = args[0];

        if (!Directory.Exists(levelspath))
        {
            Console.WriteLine("No directory exists on path " + levelspath);
            return;
        }

        var filteredFiles = Directory.GetFiles(levelspath, "*.lwf");
        int updatedlevel = 0;

        foreach (string levelfile in filteredFiles)
        {
            //open level file and change every path to a ttf file to a json file
            Level olevel = null;

            using (var file = File.OpenRead(levelfile))
                olevel = ProtoBuf.Serializer.Deserialize<PBInterface.Level>(file);

            if (olevel != null)
            {
                bool onettf = false;
                //iterate on each textboject
                foreach (Map omap in olevel.MapArrays)
                {
                    foreach (Layer oLayer in omap.LayerArrays)
                    {
                        if (oLayer.Type != (int)TypeLayer.Objects)
                            continue;

                        foreach (TextObject ent in oLayer.TextArrays)
                        {
                            if (ent.FontName.EndsWith(".ttf"))
                            {
                                string newfontname = ent.FontName.Replace(".ttf", ".json");
                                ent.FontName = newfontname;
                                onettf = true;
                            }
                        }
                    }
                }

                //save updated level data
                if (onettf)
                {
                    using (var file = File.Create(levelfile))
                    {
                        ProtoBuf.Serializer.Serialize<PBInterface.Level>(file, olevel);
                    }

                    updatedlevel++;
                }

            }


        }

        Console.WriteLine("Updated {0} level files to new font format", updatedlevel);
    }
}